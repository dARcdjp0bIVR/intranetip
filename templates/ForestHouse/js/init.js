var isPowerClass = 1;
var debugMsgJS = {
	vars: {
		allowConsole: false,
		clcolor: "FUCHSIA",
		container: "#devobj"
	},
	listeners: {},
	func: {
		showlog: function(msg, color) {
			if (window.console && window.console.log) {
				var txtColor = "SILVER";
				var txtBGColor = "BLACK";
				if (typeof color != "undefined") {
					txtColor = color;
				}
				var log_msg = "%c[-]%c";
				var log_color1 = "background:" + txtBGColor + ";color:WHITE;";
				var log_color2 = "background:" + txtBGColor + ";color:" + txtColor + ";";
				var log_type = "log";
				if (msg.indexOf(' AJAX ') > -1) {
					log_msg = "%c[A]%c";
					log_color1 = "background:#FFCE54;color:BLACK;";
				} else if (msg.indexOf('.ltr.') > -1) {
					msg = msg.replace(".ltr.", " >> Listener >> ");
					log_msg = "%c[L]%c";
					log_color1 = "background:WHITE;color:BLACK;";
					log_type = "info";
				} else if (msg.indexOf('.cfn.') > -1) {
					msg = msg.replace(".cfn.", " :: Function :: ");
					log_msg = "%c[F]%c";
					log_type = "log";
				}
				log_msg = log_msg + msg + " ";
				
				log_color2 = "background:" + txtBGColor + ";color:" + txtColor + ";";
				switch (log_type) {
					case "warn":
						console.warn(log_msg, log_color1, log_color2);
						break;
					case "info":
						console.info(log_msg, log_color1, log_color2);
						break;
					default:
						console.log(log_msg, log_color1, log_color2);
						break;
				}
			}
			return false;
		}
	}
}
var custCommonJS = {
	vrs: {
		menuObj: "#blkTab > div > ul > li",
		container: "#custFrameBody",
		currHash: "",
		color: "DarkSeaGreen",
		frameLink: ".framelink"
	},
	ltr: {
		disableHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custCommonJS.ltr.disableHandler", custCommonJS.vrs.color);
			e.preventDefault();
		},
		logoutHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custCommonJS.ltr.logoutHandler", custCommonJS.vrs.color);
			e.preventDefault();
			logout();
		},
		flClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custCommonJS.ltr.flClickHandler", custCommonJS.vrs.color);
			e.preventDefault();
			$(".frameLink_Selected").removeClass('frameLink_Selected');
			$(this).parent().addClass('frameLink_Selected');
			var ID = $(this).attr('id');
			if (typeof ID != "undefined") {
				custCommonJS.cfn.loadPage(ID.replace("m_", ""), $(this).attr('href'));
				if ($(".lblInfo.shown").length > 0) {
					$(".lblInfo.shown").removeClass('shown');
					$('#blkUserSubMenu').hide();
				}
			}
		}
	},
	cfn: {
		getHash: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custCommonJS.cfn.getHash", custCommonJS.vrs.color);
			if ($(custCommonJS.vrs.menuObj).length > 0) {
				var firstObj = $(custCommonJS.vrs.menuObj).eq(0);
				if (typeof firstObj != "undefined") {
					var objID = firstObj.attr('id');
					if (typeof objID != "undefined") {
						return objID.replace("m_", "");
					}
				}
			}
			return "";
		},
		setLocationHash: function(strHash) {
			window.location.hash = strHash;
		},
		loadPage: function(strHash, strLink) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custCommonJS.cfn.loadPage", custCommonJS.vrs.color);
			if (typeof strHash != "undefined" && strHash != "undefined" && strHash != "" && strLink != "#") {
				var rel = strLink;
				if (typeof strLink == "undefined") {
					rel = $("#m_" + strHash).attr('rel');
				}
				if (typeof rel != "undefined" && rel != "") {
					custTemplateJS.vrs.currURL = rel;
					$(custCommonJS.vrs.container).attr('src', rel);
					$(custCommonJS.vrs.menuObj).each(function() {
						$(this).attr('data-rel', '');
					});
					if (custCommonJS.vrs.currHash != strHash) {
						$(custCommonJS.vrs.menuObj).removeClass('active');
						if ($("#m_" + strHash).length > 0) {
							if ($(".framelink.active").length > 0) {
								$(".framelink.active").removeClass('active');
								$(".frameLink_Selected").removeClass('frameLink_Selected');
							}
							$("#m_" + strHash).addClass('active');
							
							if ($(".framelink.active").length > 0 && $(".frameLink_Selected").length == 0) {
								$(".framelink.active").parent().addClass('frameLink_Selected');
							}
							custCommonJS.cfn.setLocationHash(strHash);
						}
						custCommonJS.vrs.currHash = strHash;
					}
				}
			}
			return false;
		},
		isTouchDevice: function() {
		    return 'ontouchstart' in document.documentElement;
		},
		getClickEventAction: function() {
			if (custCommonJS.cfn.isTouchDevice()) {
				return "touchstart mousedown";
			} else {
				return "click";
			}
		},
		init: function() {
			
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custCommonJS.cfn.init", custCommonJS.vrs.color);
			$('a[href="#"]').unbind(custCommonJS.cfn.getClickEventAction(), custCommonJS.ltr.disableHandler);
			$('a[href="#"]').bind(custCommonJS.cfn.getClickEventAction(), custCommonJS.ltr.disableHandler);

			$('li.logout a').unbind(custCommonJS.cfn.getClickEventAction(), custCommonJS.ltr.logoutHandler);
			$('li.logout a').bind(custCommonJS.cfn.getClickEventAction(), custCommonJS.ltr.logoutHandler);
		
			if ($(custCommonJS.vrs.container).length > 0) {
				var strHash = window.location.hash;
				if ($.trim(strHash) == "") {
					strHash = custCommonJS.cfn.getHash();
				}
				custCommonJS.cfn.loadPage(strHash.replace("#", ""));
				
				if ($(custCommonJS.vrs.frameLink).length > 0) {
					$(custCommonJS.vrs.frameLink).unbind(custCommonJS.cfn.getClickEventAction(), custCommonJS.ltr.flClickHandler);
					$(custCommonJS.vrs.frameLink).bind(custCommonJS.cfn.getClickEventAction(), custCommonJS.ltr.flClickHandler);
				}
			}
			if ($(".accordion-page").length > 0) {
				var animateTime = 300, $accordion = $('.accordion');
				$accordion.bind(custCommonJS.cfn.getClickEventAction(), function()
				{
					if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" accordion > Click", custCommonJS.vrs.color);
					var $accordionPage = $(this).next('.accordion-page');
					// $(this).toggleClass("collapse");
					
					if($accordionPage.height() === 0) {
						autoHeightAnimate($accordionPage, animateTime);
						$(this).removeClass('collapse');
					} else {
						$accordionPage.stop().animate({ height: '0' }, animateTime);
						$(this).addClass('collapse');
					}
				});
			}
			$(".portalIcon").hover(function()
			{
				$(this).find(".portalIcon_text > span > div:first-child").addClass("hover");
			}, function(){
				$(this).find(".portalIcon_text > span > div:first-child").removeClass("hover");
			});

			$(".portalIcon .addButton").hover(function()
			{
				$(this).closest(".portalIcon_text").find("> span > div:first-child").toggleClass("hover");
			});
		}
	}
};
var custTemplateJS = {
	vrs: {
		container: "#blkTab > div > ul > li:not(.newtab)",
		color: "Thistle",
		currURL: "",
		timer: ""
	},
	ltr: {
		infoClickHandle: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.ltr.infoClickHandle", custTemplateJS.vrs.color);
			if ($("#btnUserMenu .lblInfo").hasClass("shown")) {
				$("#btnUserMenu .lblInfo").removeClass("shown");
				$('#blkUserSubMenu').hide();
			} else {
				$("#btnUserMenu .lblInfo").addClass("shown");
				$('#blkUserSubMenu').show();
				$("#btnUserMenu").unbind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.infoClickHandle);
				custTemplateJS.vrs.timer = setTimeout("custTemplateJS.cfn.hiddenSubMenu()", 3000);
			}
			e.preventDefault();
		},
		windowLoadHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.ltr.windowLoadHandler", custTemplateJS.vrs.color);
			custTemplateJS.cfn.winResizeHeightHandle();
		},
		resizeHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.ltr.resizeHandler", custTemplateJS.vrs.color);
			custTemplateJS.cfn.winResizeHeightHandle();
		},
		menuClickHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.ltr.menuClickHandler", custTemplateJS.vrs.color);
			var rel = $(this).attr('rel');
			var id = $(this).attr('id');
			if (typeof id != "undefined" && typeof rel != "undefined" && id != "" && rel != "") {
				custCommonJS.cfn.loadPage(id.replace("m_", ""));
			}
			e.preventDefault();
		},
		PowerLessonLinkHandler: function(e) {
			if ($("#powerLessonLogin").length > 0) {
				$("#powerLessonLogin").submit();
			}
		},
		appSettingsHandler: function(e) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.ltr.appSettingsHandler", custTemplateJS.vrs.color);
			var strHash = window.top.location.hash;
			strHash = "#m_" + strHash.replace("#", "");
			var parentObjElement = $(strHash, window.parent.document);
			if (typeof parentObjElement != "undefined") {
				if (!parentObjElement.hasClass('ignoreback')) {
					parentObjElement.attr('data-rel', 'isAppSetting');
				}
			}
			location.href = $(this).attr('href');
			e.preventDefault();
		},
		GoogleAppsListHandler: function(e) {
			if ($("#blkGoogleApps").length > 0) {
				$("#blkGoogleApps").toggleClass("hide");
			}
		}
	},
	cfn: {
		hiddenSubMenu: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.cfn.hiddenSubMenu", custTemplateJS.vrs.color);
			$("#btnUserMenu .lblInfo").removeClass("shown");
			$('#blkUserSubMenu').hide();
			$("#btnUserMenu").bind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.infoClickHandle);
		},
		winResizeHeightHandle: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.cfn.winResizeHeightHandle", custTemplateJS.vrs.color);
			// Get the dimensions of the viewport
		    var width = window.innerWidth ||
		                document.documentElement.clientWidth ||
		                document.body.clientWidth;
		    var height = window.innerHeight ||
		                 document.documentElement.clientHeight ||
		                 document.body.clientHeight;

		    height = height - parseInt($('#blkTop').height()) - parseInt($("#blkTab").height());
		    $('#frameWrap').height(height);// Display the height
		    $('#frameWrap').css({ "margin-bottom": "10px"});
		},
		addBackButton: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.cfn.addBackButton", custTemplateJS.vrs.color);
			var url = window.location.href;
			var baseURL = custTemplateJS.cfn.baseName(url);
			var strHash = window.top.location.hash;
			strHash = "#m_" + strHash.replace("#", "");
			if ($("#header").length > 0) {
				var parentObjElement = $(strHash, window.parent.document);
				if (typeof parentObjElement != "undefined") {
					if (!parentObjElement.hasClass('ignoreback')) {
						var rel = parentObjElement.attr('rel');
					}
				}
				if (custTemplateJS.cfn.baseName(rel) == "app" && baseURL != "app_settings") {
					var datarel = $(strHash, window.parent.document).attr('data-rel');
					if (typeof datarel != "undefined" && datarel == "isAppSetting") {
						rel = rel.replace("app.php", "app_settings.php");
						$(".selectorWrapper.modulePageSwitcher").remove();
					}
				}
				if (typeof rel != "undefined") {
					$("#header .btnCustBack").remove();
					var backTxt = "Back";
					if ($(".lang_b5.lang_button_current", window.parent.document).length > 0) {
						backTxt = "返回";
					}
					var btnHTML = '<div class="backButton font-light pointer"><a href="' + rel + '">' + backTxt + '</a></div>';
					$("#header").append(btnHTML);
					var footerHTML = '<a href="http://eclass.com.hk" title="PowerClass" target="_blank"><img id="imgPC" src="/templates/ForestHouse/images/powerClassLogo_w.png"></a><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="/templates/ForestHouse/images/eClassLogo_w.png"></a>';
					$("#eclass_main_frame_table  > tbody > tr:nth-child(2) > td").html(footerHTML);
					$('#eclass_main_frame_table  > tbody > tr:nth-child(2) > td').attr('id', 'lbleClass');
					$('#eclass_main_frame_table  > tbody > tr:nth-child(2) > td').removeClass('footer');
					$("#header").addClass("pageHeader");
					$('#module_title').addClass("pageSubject font-bold");
				}
			} else {
				if (baseURL == "app") {
					$(strHash, window.parent.document).attr('data-rel', '');
				}
			}
		},
		baseName: function (str)
		{
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.cfn.baseName", custTemplateJS.vrs.color);
			if (typeof str != "undefined" && str != "") {
				var base = new String(str).substring(str.lastIndexOf('/') + 1); 
				if(base.lastIndexOf(".") != -1)       
					base = base.substring(0, base.lastIndexOf("."));
				return base;
			} else {
				return str;
			}
		},
		init:function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" custTemplateJS.cfn.init", custTemplateJS.vrs.color);
			/*************************************************************************/
			$("#btnUserMenu").unbind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.infoClickHandle);
			$("#btnUserMenu").bind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.infoClickHandle);
			
			$(custTemplateJS.vrs.container).unbind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.menuClickHandler);
			$(custTemplateJS.vrs.container).bind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.menuClickHandler);
			
			$("a.PowerLesson2").bind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.PowerLessonLinkHandler);
			$('#btnAppSettings').bind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.appSettingsHandler);
			$("#btnGoogleApps").bind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.GoogleAppsListHandler);
			$("#m_PowerClassSchoolNews").unbind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.menuClickHandler);
			$("#m_PowerClassSchoolNews").bind(custCommonJS.cfn.getClickEventAction(), custTemplateJS.ltr.menuClickHandler);
			
			custTemplateJS.cfn.addBackButton();
			
			$(window).bind('load', custTemplateJS.ltr.windowLoadHandler);
			$(window).bind('resize', custTemplateJS.ltr.resizeHandler);
			/*************************************************************************/
			custCommonJS.cfn.init();
		}
	}
};
var chartJS = {
	vrs: {
		lang: [],
		color: "Gold",
		currIndex: 0,
	},
	ltr: {
		
	},
	cfn: {
		setLang: function(lang) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" chartJS.cfn.setLang", chartJS.vrs.color);
			chartJS.vrs.lang = lang;
		},
		createChart: function(data) {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" chartJS.cfn.createChart", chartJS.vrs.color);
			if (typeof data != "undefined") {
				chartJS.vrs.data = data;
			} else {
				data = chartJS.vrs.data;
			}
			$("#blkAppTab ul li").removeClass('active');
			var index = $("#blkAppTab ul li.ui-tabs-active").index();
			if (index <= 0) index = 0;
			$("#blkAppTab ul li:nth-child("+(index+1)+")").addClass('active');
			
			var lang = chartJS.vrs.lang;
			var txtLabel = lang['label'][index] || "Today";
			if (typeof data[index] != "undefined") {
				Highcharts.chart('graph', {
				    chart: { type: 'area' },
				    title: { text: "" },
				    subtitle: { text: "" },
				    xAxis: {
				        categories: data[index]["categories"],
				        tickmarkPlacement: 'on',
				        title: {
				            enabled: false
				        }
				    },
				    colors: [ "#fcb704" ],
				    yAxis: {
				        title: { text: lang["NumberOfAccess"] },
				        labels: {
				            formatter: function () {
				                return this.value;
				            }
				        }
				    },
				    tooltip: {
				        split: true,
				        valueSuffix: ''
				    },
				    plotOptions: {
				        area: {
				            stacking: 'normal',
				            lineColor: '#666666',
				            lineWidth: 1,
				            marker: {
				                lineWidth: 1,
				                lineColor: '#666666'
				            }
				        }
				    },
				    credits: {
				        enabled: false
				    },
				    legend: {
				    	align: "right",
				    	verticalAlign: "top"
				    },
				    series: [{
				        name: lang["NumberOfAccess"],
				        data: data[index]["data"]
				    }]
				});
			}
		},
		init: function() {
			if (debugMsgJS.vars.allowConsole) debugMsgJS.func.showlog(" chartJS.cfn.init", chartJS.vrs.color);
			if ($("#blkAppTab").length > 0) {
				var lang = chartJS.vrs.lang;
				var graphTitle = lang["graphTitle"] || "Login Record";
				$("#blkAppTab").tabs({
					show: 'fade',
					hide: 'fade',
					activate: function(event, ui){
						if ($('#blkAppTab').hasClass('hasChart')) {
						// 	var $activeTab = $('#tabs').tabs('option', 'active');
							var oldStrHTML = "<div class='chrt_body'><div class='graph_body'></div></div></div>";
							var strHTML = "<div class='chrt_body'><div id='graph' class='graph_body'></div></div></div>";
							ui.oldPanel.html(oldStrHTML);
							ui.newPanel.html(strHTML);
							chartJS.cfn.createChart();
						}
				  	}
			  	});
			}
			
		}
	}
};
/* Function to animate height: auto */
function autoHeightAnimate(element, time)
{
	var curHeight = element.height(), // Get Default Height
        autoHeight = element.css('height', 'auto').height(); // Get Auto Height
    element.height(curHeight); // Reset to Default Height
    element.stop().animate({ height: autoHeight }, time); // Animate to Auto Height
}
/*Tab*/
(function(a){a.easytabs=function(j,e){var f=this,q=a(j),i={animate:true,panelActiveClass:"active",tabActiveClass:"active",defaultTab:"li:first-child",animationSpeed:"normal",tabs:"> ul > li",updateHash:true,cycle:false,collapsible:false,collapsedClass:"collapsed",collapsedByDefault:true,uiTabs:false,transitionIn:"fadeIn",transitionOut:"fadeOut",transitionInEasing:"swing",transitionOutEasing:"swing",transitionCollapse:"slideUp",transitionUncollapse:"slideDown",transitionCollapseEasing:"swing",transitionUncollapseEasing:"swing",containerClass:"",tabsClass:"",tabClass:"",panelClass:"",cache:true,event:"click",panelContext:q},h,l,v,m,d,t={fast:200,normal:400,slow:600},r;f.init=function(){f.settings=r=a.extend({},i,e);r.bind_str=r.event+".easytabs";if(r.uiTabs){r.tabActiveClass="ui-tabs-selected";r.containerClass="ui-tabs ui-widget ui-widget-content ui-corner-all";r.tabsClass="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all";r.tabClass="ui-state-default ui-corner-top";r.panelClass="ui-tabs-panel ui-widget-content ui-corner-bottom"}if(r.collapsible&&e.defaultTab!==undefined&&e.collpasedByDefault===undefined){r.collapsedByDefault=false}if(typeof(r.animationSpeed)==="string"){r.animationSpeed=t[r.animationSpeed]}a("a.anchor").remove().prependTo("body");q.data("easytabs",{});f.setTransitions();f.getTabs();b();g();w();n();c();q.attr("data-easytabs",true)};f.setTransitions=function(){v=(r.animate)?{show:r.transitionIn,hide:r.transitionOut,speed:r.animationSpeed,collapse:r.transitionCollapse,uncollapse:r.transitionUncollapse,halfSpeed:r.animationSpeed/2}:{show:"show",hide:"hide",speed:0,collapse:"hide",uncollapse:"show",halfSpeed:0}};f.getTabs=function(){var x;f.tabs=q.find(r.tabs),f.panels=a(),f.tabs.each(function(){var A=a(this),z=A.children("a"),y=A.children("a").data("target");A.data("easytabs",{});if(y!==undefined&&y!==null){A.data("easytabs").ajax=z.attr("href")}else{y=z.attr("href")}y=y.match(/#([^\?]+)/)[1];x=r.panelContext.find("#"+y);if(x.length){x.data("easytabs",{position:x.css("position"),visibility:x.css("visibility")});x.not(r.panelActiveClass).hide();f.panels=f.panels.add(x);A.data("easytabs").panel=x}else{f.tabs=f.tabs.not(A);if("console" in window){console.warn("Warning: tab without matching panel for selector '#"+y+"' removed from set")}}})};f.selectTab=function(x,C){var y=window.location,B=y.hash.match(/^[^\?]*/)[0],z=x.parent().data("easytabs").panel,A=x.parent().data("easytabs").ajax;if(r.collapsible&&!d&&(x.hasClass(r.tabActiveClass)||x.hasClass(r.collapsedClass))){f.toggleTabCollapse(x,z,A,C)}else{if(!x.hasClass(r.tabActiveClass)||!z.hasClass(r.panelActiveClass)){o(x,z,A,C)}else{if(!r.cache){o(x,z,A,C)}}}};f.toggleTabCollapse=function(x,y,z,A){f.panels.stop(true,true);if(u(q,"easytabs:before",[x,y,r])){f.tabs.filter("."+r.tabActiveClass).removeClass(r.tabActiveClass).children().removeClass(r.tabActiveClass);if(x.hasClass(r.collapsedClass)){if(z&&(!r.cache||!x.parent().data("easytabs").cached)){q.trigger("easytabs:ajax:beforeSend",[x,y]);y.load(z,function(C,B,D){x.parent().data("easytabs").cached=true;q.trigger("easytabs:ajax:complete",[x,y,C,B,D])})}x.parent().removeClass(r.collapsedClass).addClass(r.tabActiveClass).children().removeClass(r.collapsedClass).addClass(r.tabActiveClass);y.addClass(r.panelActiveClass)[v.uncollapse](v.speed,r.transitionUncollapseEasing,function(){q.trigger("easytabs:midTransition",[x,y,r]);if(typeof A=="function"){A()}})}else{x.addClass(r.collapsedClass).parent().addClass(r.collapsedClass);y.removeClass(r.panelActiveClass)[v.collapse](v.speed,r.transitionCollapseEasing,function(){q.trigger("easytabs:midTransition",[x,y,r]);if(typeof A=="function"){A()}})}}};f.matchTab=function(x){return f.tabs.find("[href='"+x+"'],[data-target='"+x+"']").first()};f.matchInPanel=function(x){return(x&&f.validId(x)?f.panels.filter(":has("+x+")").first():[])};f.validId=function(x){return x.substr(1).match(/^[A-Za-z]+[A-Za-z0-9\-_:\.].$/)};f.selectTabFromHashChange=function(){var y=window.location.hash.match(/^[^\?]*/)[0],x=f.matchTab(y),z;if(r.updateHash){if(x.length){d=true;f.selectTab(x)}else{z=f.matchInPanel(y);if(z.length){y="#"+z.attr("id");x=f.matchTab(y);d=true;f.selectTab(x)}else{if(!h.hasClass(r.tabActiveClass)&&!r.cycle){if(y===""||f.matchTab(m).length||q.closest(y).length){d=true;f.selectTab(l)}}}}}};f.cycleTabs=function(x){if(r.cycle){x=x%f.tabs.length;$tab=a(f.tabs[x]).children("a").first();d=true;f.selectTab($tab,function(){setTimeout(function(){f.cycleTabs(x+1)},r.cycle)})}};f.publicMethods={select:function(x){var y;if((y=f.tabs.filter(x)).length===0){if((y=f.tabs.find("a[href='"+x+"']")).length===0){if((y=f.tabs.find("a"+x)).length===0){if((y=f.tabs.find("[data-target='"+x+"']")).length===0){if((y=f.tabs.find("a[href$='"+x+"']")).length===0){a.error("Tab '"+x+"' does not exist in tab set")}}}}}else{y=y.children("a").first()}f.selectTab(y)}};var u=function(A,x,z){var y=a.Event(x);A.trigger(y,z);return y.result!==false};var b=function(){q.addClass(r.containerClass);f.tabs.parent().addClass(r.tabsClass);f.tabs.addClass(r.tabClass);f.panels.addClass(r.panelClass)};var g=function(){var y=window.location.hash.match(/^[^\?]*/)[0],x=f.matchTab(y).parent(),z;if(x.length===1){h=x;r.cycle=false}else{z=f.matchInPanel(y);if(z.length){y="#"+z.attr("id");h=f.matchTab(y).parent()}else{h=f.tabs.parent().find(r.defaultTab);if(h.length===0){a.error("The specified default tab ('"+r.defaultTab+"') could not be found in the tab set ('"+r.tabs+"') out of "+f.tabs.length+" tabs.")}}}l=h.children("a").first();p(x)};var p=function(z){var y,x;if(r.collapsible&&z.length===0&&r.collapsedByDefault){h.addClass(r.collapsedClass).children().addClass(r.collapsedClass)}else{y=a(h.data("easytabs").panel);x=h.data("easytabs").ajax;if(x&&(!r.cache||!h.data("easytabs").cached)){q.trigger("easytabs:ajax:beforeSend",[l,y]);y.load(x,function(B,A,C){h.data("easytabs").cached=true;q.trigger("easytabs:ajax:complete",[l,y,B,A,C])})}h.data("easytabs").panel.show().addClass(r.panelActiveClass);h.addClass(r.tabActiveClass).children().addClass(r.tabActiveClass)}q.trigger("easytabs:initialised",[l,y])};var w=function(){f.tabs.children("a").bind(r.bind_str,function(x){r.cycle=false;d=false;f.selectTab(a(this));x.preventDefault?x.preventDefault():x.returnValue=false})};var o=function(z,D,E,H){f.panels.stop(true,true);if(u(q,"easytabs:before",[z,D,r])){var A=f.panels.filter(":visible"),y=D.parent(),F,x,C,G,B=window.location.hash.match(/^[^\?]*/)[0];if(r.animate){F=s(D);x=A.length?k(A):0;C=F-x}m=B;G=function(){q.trigger("easytabs:midTransition",[z,D,r]);if(r.animate&&r.transitionIn=="fadeIn"){if(C<0){y.animate({height:y.height()+C},v.halfSpeed).css({"min-height":""})}}if(r.updateHash&&!d){window.location.hash="#"+D.attr("id")}else{d=false}D[v.show](v.speed,r.transitionInEasing,function(){y.css({height:"","min-height":""});q.trigger("easytabs:after",[z,D,r]);if(typeof H=="function"){H()}})};if(E&&(!r.cache||!z.parent().data("easytabs").cached)){q.trigger("easytabs:ajax:beforeSend",[z,D]);D.load(E,function(J,I,K){z.parent().data("easytabs").cached=true;q.trigger("easytabs:ajax:complete",[z,D,J,I,K])})}if(r.animate&&r.transitionOut=="fadeOut"){if(C>0){y.animate({height:(y.height()+C)},v.halfSpeed)}else{y.css({"min-height":y.height()})}}f.tabs.filter("."+r.tabActiveClass).removeClass(r.tabActiveClass).children().removeClass(r.tabActiveClass);f.tabs.filter("."+r.collapsedClass).removeClass(r.collapsedClass).children().removeClass(r.collapsedClass);z.parent().addClass(r.tabActiveClass).children().addClass(r.tabActiveClass);f.panels.filter("."+r.panelActiveClass).removeClass(r.panelActiveClass);D.addClass(r.panelActiveClass);if(A.length){A[v.hide](v.speed,r.transitionOutEasing,G)}else{D[v.uncollapse](v.speed,r.transitionUncollapseEasing,G)}}};var s=function(z){if(z.data("easytabs")&&z.data("easytabs").lastHeight){return z.data("easytabs").lastHeight}var B=z.css("display"),y,x;try{y=a("<div></div>",{position:"absolute",visibility:"hidden",overflow:"hidden"})}catch(A){y=a("<div></div>",{visibility:"hidden",overflow:"hidden"})}x=z.wrap(y).css({position:"relative",visibility:"hidden",display:"block"}).outerHeight();z.unwrap();z.css({position:z.data("easytabs").position,visibility:z.data("easytabs").visibility,display:B});z.data("easytabs").lastHeight=x;return x};var k=function(y){var x=y.outerHeight();if(y.data("easytabs")){y.data("easytabs").lastHeight=x}else{y.data("easytabs",{lastHeight:x})}return x};var n=function(){if(typeof a(window).hashchange==="function"){a(window).hashchange(function(){f.selectTabFromHashChange()})}else{if(a.address&&typeof a.address.change==="function"){a.address.change(function(){f.selectTabFromHashChange()})}}};var c=function(){var x;if(r.cycle){x=f.tabs.index(h);setTimeout(function(){f.cycleTabs(x+1)},r.cycle)}};f.init()};a.fn.easytabs=function(c){var b=arguments;return this.each(function(){var e=a(this),d=e.data("easytabs");if(undefined===d){d=new a.easytabs(this,c);e.data("easytabs",d)}if(d.publicMethods[c]){return d.publicMethods[c](Array.prototype.slice.call(b,1))}})}})(jQuery);

$(document).ready(function() {
	custTemplateJS.cfn.init();
});

function tb_remove() {
	if (typeof window.frames[0] != "undefined") {
		window.frames[0].tb_remove();
	}
}