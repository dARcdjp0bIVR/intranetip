<?php 
	session_start();
	$_SESSION['PowerClass_PAGE'] = "schoolnews";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $Lang["PageTitle"]; ?></title>
	<link href="<?php echo $source_path; ?>css/main.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portal.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/portalIcon.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $source_path; ?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/3.2.1/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="<?php echo $source_path; ?>js/script.js"></script>
	<link rel="shortcut icon" href="<?php echo $source_path; ?>images/favicon.gif" type="image/x-icon">
</head>
<body>
	<div class="CustCommon CustMainContent PowerCLassPortal">
		<div class="wrapper">
					
    				<!--
    					Replace #custFrameBody with an iFrame
    					Place .pageHeader, #blkModuleContainer & #lbleClass inside the iFrame
    					Place module in #blkModuleContainer
    				-->
    		<div id="custFrameBody">
					<div id="blkModuleContainer" class="bgColor-2">
						<div id="lblSchoolNews" class="font-bold"><?php echo $Lang["PowerClass"]["SchoolNews"]; ?></div>
						<ul>
							<?php foreach($schoolnews as $news){ ?>
							<li class="schoolNews-item">
								<img src="<?php echo $source_path; ?>images/icon-news.png">
								<div>
									<div>
										<?php if($news[3]==1){?>
										<div class="update-label">New</div>
										<?php }?>
										<a class="schoolNews-subject" href="/home/view_announcement.php?AnnouncementID=<?=$news[0]?>" target="_blank"><?php echo $news[2]; ?></a>
									</div>
									<div class="schoolNews-content">
										<?php echo htmlspecialchars_decode($news[7]); ?>
									</div>
									<div>
										<?=$Lang['SysMgr']['SchoolNews']['StartDate']?>:<span class="dateTime-label"><?php echo $news[1]; ?></span>
									</div>
								</div>
							</li>
							<?php }?>
						</ul>
    						<div id="blkMoreSchoolNews"><a href='schoolnews_more.php'><?=$Lang['General']['More']?></a></div>
    			</div>
    			<div id="lbleClass"><a href="http://eclass.com.hk" title="PowerClass" target="_blank"><img id="imgPC" src="<?php echo $source_path; ?>images/powerClassLogo_w.png"></a><span>Powered by</span><a href="http://eclass.com.hk" title="eClass" target="_blank"><img src="<?php echo $source_path; ?>images/eClassLogo_w.png"></a></div>
			</div>
		</div>		
	</div>
</body>
</html>