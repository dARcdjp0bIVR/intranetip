<style>
body, html {
	font-size: 11px;
	font-weight: normal;
}
select {
	font-size: 11px;
}
.textbox {
	font-size: 11px;
}
.button {
	font-size: 11px;
}
.button_main_act {
	font-size: 11px;
}
.button_main_act_selected {
	font-size: 11px;
	font-weight: bold;
}
#top_login_btn span{
	font-size: 10px;
	font-weight: normal;
	text-decoration: none;
}
#top_login_btn a{
	font-size: 10px;
	font-weight: normal;
	text-decoration: none;
}
#top_school_slogan span {
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
}

#top_menu a {
	font-size: 12px;
	font-weight: normal;
	text-decoration: none;
}
#top_menu a span{
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
}

/* Commented Backslash Hack hides rule from IE5-Mac \*/


#top_menu #current a span{
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
}


#top_menu a:hover span{
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
}
#top_weather_temp {
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
#top_weather_API_title{
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;
}

#top_weather_API_item{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
#footer_logo_eclass{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
#module_content_header_title{
	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
}

#module_content_header_title a{

	font-size: 12px;
	font-weight: bold;
	text-decoration: none;
}

#module_content_relate{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;

}
#module_content_relate a{

	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
#btn_link a{
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;
}
.portal_cal_active_event_title{
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;

}
#portal_cal_active_event a{
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;
}
.cal_agenda_title {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;

}
.cal_agenda_entry a {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;
}
.bulletin_portal_entry a {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;
}

.portal_entry_more a {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;
}

.day_time{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.imail_entry_top{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.imail_entry_top a{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.imail_entry_unread {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;

}
.imail_entry_read {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;

}
.imail_entry_unread_link {
	font-size: 11px;
	font-weight: bold;
	text-decoration: none;

}
.imail_entry_read_link {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;

}
a.imail_entry_sender   {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;

}
.list_option{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.list_option a{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.list_option span{
	font-weight: bold;
}
.list_thumbnail{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.list_thumbnail a{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.list_thumbnail_on{
	font-weight: bold;
}

a.portal_student_name {
	font-size: 11px;
	font-weight: normal;
	text-decoration: none;
}

a.portal_student_new {
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.student_list_top {
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;

}
.student_list_top a{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;

}
.cal_week_title{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.cal_week_title_today{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
}
.textarea_desc {
	font-size: 11px;
}
</style>