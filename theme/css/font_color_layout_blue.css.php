<style>
@charset "utf-8";
/* CSS Document */
.textbox {
	color: #000000;
	border: 1px solid #cfcfcf;
}
select {
	color: #000000;
	border: 1px solid #cfcfcf;
}
.button {
	color: #000000;
	border: 1px solid #757575;
	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/btn_off.gif);
	background-repeat: repeat-x;
	background-position: center;
}
.button:hover {

	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/btn_on.gif);
	background-repeat: repeat-x;
	background-position: center;
}
.button:active{
	color: #FF0000;

}
.button_main_act {
	color: #000000;
	border: 1px solid #647194;
	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/btn_main_act_off.gif);
	background-repeat: repeat-x;
	background-position: center;
}
.button_main_act:hover {

	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/btn_main_act_on.gif);
	background-repeat: repeat-x;
	background-position: center;
}
.button_main_act:active{
	color: #FF0000;

}

.button_main_act_selected {
	color: #003399;
	border: 1px solid #003399;
	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/btn_main_act_selected.gif);
	background-repeat: repeat-x;
	background-position: center;

}
.dotline {
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #999999;
}
#top_login_btn span{
	color: #FFFFFF;
}
#top_login_btn a{
	color: #fffd56;
}
#top_login_btn a:hover{
	color: #FF0000;
}
#top_school_slogan span {
	color: #FFFFFF;
}
#top_menu a {
	color: #FFFFFF;
}
#top_menu a span{
	color: #FFFFFF;
}

/* Commented Backslash Hack hides rule from IE5-Mac \*/

#top_menu #current a span{
	color: #ffd44f;
}


#top_menu a:hover span{
	color: #ffd44f;
}
#top_weather_temp {
	color: #4b4b4b;
}

#footer_logo_eclass{
	color: #4b4b4b;
}
#module_content_header_myclass{
	color: #fff600;
	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/header_bg_myclass.gif);
	border: 2px solid #679e4f;
}
#module_content_header{
	color: #000000;
	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/header_bg.gif);
	border: 2px solid #3b95c8;
}
#module_content_header_title{
	color: #000000;
}
#module_content_header_title a{
	color: #000000;
}
#module_content_header_title a:hover{
	color: #fffd56;
}

#module_content_relate{
	color: #595959;

}
#module_content_relate a{
	color: #FFFFFF;

}
#module_content_relate a:hover{
	color: #fffd56;

}
#btn_link a{
	color: #258002;
}
#btn_link a:hover{
	color: #FF0000;
}
#portal_cal_active_event{
	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>layout_blue/portal_active_event_bg.gif);
}
a.invite{
	color: #15a905;
	text-decoration: none;
}
a.notify{
	color: #1641a3;
	text-decoration: none;
}
a.invite:hover{
	color: #FF0000;
}
a.notify:hover{
	color: #FF0000;
}
#portal_cal_active_event a:hover{
	color: #FF0000;
}
.cal_agenda_title {
	color: #757575;

}
.cal_agenda_entry a{
	color: #0033CC;
}
.bulletin_portal_entry a{
	color: #0033CC;
}
.bulletin_portal_entry a:hover {
	color: #FF0000;
}

.cal_agenda_entry a:hover {
	color: #FF0000;
}
.bulletin_entry{
	color: #0033CC;
}
.bulletin_entry a:hover {
	color: #FF0000;
}
.day_time{
	color: #a6a6a6;
}
.imail_entry_selected  {
	background-color: #ebf6d4;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;
}
.imail_entry_unread   {
	background-color: #FFFFFF;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;

}
.imail_entry_unread_link {
	color: #4360aa;
}
.imail_entry_unread_link:hover {
	color: #FF0000;
}
.imail_entry_read   {
	background-color: #f3f5f9;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;

}
.imail_entry_top   {
	background-color: #9bb5cd;

}

.imail_entry_top a{
	color: #ffffff;
}
.imail_entry_top a:hover{
	color: #ff0000;
}
.imail_entry_read_link {
	color: #4360aa;
}
.imail_entry_read_link:hover {
	color: #FF0000;
}
a.imail_entry_sender   {
	color: #4360aa;
}
a.imail_entry_sender :hover {
	color: #FF0000;
}
#portal_right{
	padding-top:3px;
	padding-left:3px;
	border-left-width: 1px;
	border-left-style: solid;
	border-left-color: #cfcfcf;
}
#portal_search_student{
	padding:2px;
	font-size: 11px;
	color: #000000;
}

#portal_search_staff{
	padding:2px;
	font-size: 11px;
	color: #000000;
}
.search_icon{
	width:20px;
	float:left;
	padding-top:2px;
}
.portal_right_search_box{
	width:270px;
	float:left;
	padding:3px;
	height: 25px;
}
.search_btn{
	width:30px;
	float:left;
	padding:2px
}
#portal_choose_class{
	float:left;
	padding-top:3px;}
.list_option{
	color: #1b2a9b;
	padding-top:5px;
}

.list_option span{
	color: #1b2a9b;
}
.list_option a{
	color: #1b2a9b;
}
.list_option a:hover{
	color: #FF0000;
}
a.portal_student_name {
	color: #225eb9;
}
a.portal_student_name:hover{
	color: #FF0000;

}

a.portal_student_new {
	color: #ff1111;
}

a.portal_student_new:hover {
	color: #000000;
}
.student_photo{
	border: 1px solid #aeaeae;
}



.student_list_top {

	background-color: #f2f2f2;

}
.student_list_top a{
	color: #000000;
}
.student_list_top a:hover{
	color: #FF0000;
}
.student_list_entry{
	background-color: #FFFFFF;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #dfdfdf;

}
.student_list_entry_total{
	background-color: #FFFFFF;
	border-top-width: 1px;
	border-top-style: solid;
	border-top-color: #A8A8A8;
}
.cal_week_title{
	color: #6a6a6a;
	background-color: #b6cae4;
	height: 15px;
}

.cal_week_title_today{
	color: #FFFFFF;
	background-color: #b5cab8;
	height: 15px;
}
.portal_cal_normal_day{
	width: 54px;
	height: 30px;
	text-align: right;
	background-color: #FFFFFF;
}
.portal_cal_normal_day a{
	display: block;
	width: 52px;
	height: 28px;
	font-size: 9px;
	color: #6a6a6a;
	font-weight: noraml;
	text-decoration: none;
	border: 1px solid #FFFFFF;
}
.portal_cal_normal_day a span{
	padding: 1px;
}
.portal_cal_normal_day a:hover{
	display: block;
	width: 52px;
	height: 28px;
	background-color: #d1f2f2;
	border: 1px solid #000000;}



.portal_cal_normal_today{
	width: 54px;
	height: 30px;
	text-align: right;
	background-color: #fdffba;
}
.portal_cal_normal_today a{
	display: block;
	width: 52px;
	height: 28px;
	font-size: 9px;
	color: #000000;
	font-weight: bold;
	text-decoration: none;
	border: 1px solid #fdffba;
}
.portal_cal_normal_today a span{
	padding: 1px;
}
.portal_cal_normal_today a:hover{
	display: block;
	width: 52px;
	height: 28px;
	background-color: #d1f2f2;
	border: 1px solid #000000;
}
.portal_cal_week_day{
	font-size: 9px;
	width: 44px;
	height: 16px;
	background-color: #FFFFFF;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
	padding: 1px;
	vertical-align: top;
}
.portal_cal_week_today{
	font-size: 9px;
	width: 44px;
	height: 16px;
	background-color: #fdffba;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
	padding: 1px;

}
.portal_cal_week_day_whole_today{
	font-size: 9px;
	width: 44px;
	height: 15px;
	background-color: #9fafb6;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
	padding: 1px;

}
.portal_cal_week_day_whole{
	font-size: 9px;
	width: 44px;
	height: 15px;
	background-color: #a5adc7;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
	padding: 1px;

}
.portal_cal_week_time{
	width: 30px;
	font-size: 9px;
	color: #939393;
	font-weight: noraml;
	text-decoration: none;
	background-color: #f2f2f2;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC;
}
.portal_week_event_item{
	position:absolute;
	width:44px;
	z-index:1;
	visibility: visible;
}
.portal_week_event_item a{
	display:block;
 	width:44px
	font-size: 9px;
	color: #FBFDFF;
	text-decoration: none;
	background-color: #8DB7E9;
	border: 1px solid #999999;
	text-align: center;
}

.portal_week_event_item a:hover{
	color: #000000;
	background-color: #BADACF;
	border: 1px solid #4BA8C2;
}

.portal_week_event_item_whole{
	display:block;
 	width:44px;
	font-size: 9px;
	color: #FBFDFF;
	text-decoration: none;
	background-color: #8DB7E9;
	border: 1px solid #999999;
	text-align: center;
}

.portal_week_event_item_whole:hover{
	color: #000000;
	background-color: #BADACF;
	border: 1px solid #4BA8C2;
}
.portal_cal_month_event{
	text-align:left;
	height:14px;
	color:#0066CC;
	font-weight:bold;
	padding-left:2px
}

.list_option a:hover{
	color: #FF0000;
}
</style>