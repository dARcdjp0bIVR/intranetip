<style>
.warning {
     font-size:11px;
	 font-weight:normal;
	 color:#fd0404;
	 padding-top:10px;
	 padding-bottom:10px;
}
.updated {
     float:right;
	 clear:right;
     font-size:11px;
	 font-weight:normal;
	 color:#fd0404;
	 padding:5px;
	 border: 1px solid #fd0404;
}
.updated_left1{
     float:left;
	 clear:right;
     font-size:11px;
	 font-weight:normal;
	 color:#fd0404;
	 padding:5px;
	 border: 1px solid #fd0404;
}
.updated_left2{
     float:left;
	 clear:right;
     font-size:11px;
	 font-weight:normal;
	 color:#459e14;
	 padding:5px;
	 border: 1px solid #459e14;
}

#heading{
	margin-left: 10px;
	width:auto;
	margin-right:12px;
	margin-bottom: 5px;
	padding-top:5px;	
}

.vertical_line {
   border-right-width: 1px;
   border-right-style: solid;
   border-right-color: #999999;
}

.vertical_line2 {
   border-left-width: 1px;
   border-left-style: solid;
   border-left-color: #afd7ef;
}

.hori_line {
   border-top-width: 1px;
   border-top-style: solid;
   border-top-color: #999999;
}

.hori_line2 {
   border-top-width: 1px;
   border-top-style: solid;
   border-top-color: #dedede;
}
.hori_line3 {
   border-bottom-width: 1px;
   border-bottom-style: solid;
   border-bottom-color: #afd7ef;
}
.content{
	font-size:11px;
	font-weight:normal;
	color:#000000;
}
.content a{
	color: #4360aa;
	text-decoration:none;
}

.content a:hover {
	color: #FF0000;
	text-decoration:none;
}
.content_grey{
	font-size:11px;
	font-weight:normal;
	color:#999999;
}
.content_bold{
	font-size:11px;
	font-weight:bold;
	color:#000000;
}
#table_bottom{
 width: 100%;
}
 
#table_bottom_left{
   font-size:9px;
	font-weight:normal;
	color:#000000;
	float: left;
	width: 30%;
	text-align:left;
}
#table_bottom_right{
   font-size:9px;
	font-weight:normal;
	color:#000000;
	float: right;
	width: 60%;
	text-align:right;
	padding-right: 10px;
			
}

/*===============================================================================================================================================================
================================================================Bulletin content =====================================================================================
===============================================================================================================================================================*/

.search_result{
     float:left;
	 clear:right;
     font-size:11px;
	 font-weight:bold;
	 color:#666666;
	 padding:5px;
	 text-align:left;
}

#bulletin_title{
	font-size: 11px;
	font-weight: bold;
	color: #000000;
	clear: both;
	float: left;
	width: 95%;
	padding: 5px;
}

.bulletin_board{
	width: 935px;
}

#bulletin_supended{
    margin-right:240px;
	margin-bottom:10px;
   	background-color: #ffcaca;
	font-size: 11px;
	color: #000000;
	text-decoration: none;
	padding:8px;
	border: dashed 1px #999999;
}	

#bulletin_current_left{
	padding: 10px;
	clear: left;
	float: left;
	width: 15%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
}
#bulletin_history_righttop{
	padding: 2px;
	clear: right;
	float: right;
	width: 80%;
	margin-top: 10px;
	margin-right: 5px;
	margin-left: 10px;
	font-size: 11px;
	color: #000000;
	text-decoration: none;
	text-align:right;
}
#bulletin_current_right{
	border: 4px solid #FFFFFF;
	padding: 5px;
	padding-left: 10px;
	clear: right;
	float: right;
	width: 78%;
	background-color: #f4f6f9 ;
	margin: 10px;
}

#bulletin_recordtop{
	margin-left: 10px;
	width:auto;
	margin-right:12px;
	margin-bottom: 5px;
	height:25px;
	padding-top:10px;	
}


.bulletin_recordtopleft{
	float:left;
	clear:left; 
	padding:3px;
	width:auto;
}

#bulletin_recordtopright{
	width: 30%;
	clear: right;
	float: right;
	text-align:right;
}

.textbox_bulletinsearch {
	width: 150px;
	padding-right: 3px;
	padding-left: 3px;
	color: #000000;
	border: 1px solid #cfcfcf;
	font-size: 11px;
	color: #a0a0a0;
	text-decoration: none;

}

#bulletin_record{
    padding: 2px;
	background-color: #FFFFFF;
	margin-left: 8px;
	margin-right:10px;
	width: 917px;	
}

#bulletin_current_left ul{
	list-style-type: none;
	padding-top: 5px;
	padding-right: 5px;
	padding-bottom: 5px;
	padding-left: 15px;
	margin: 0px;
	
	
}
#bulletin_current_left ul li{
	display:block;
}

.table_top{
	padding-left: 15px;
	padding-right: 10px;
	padding-top: 4px;
	padding-bottom: 4px;
	background-color: #9bb5cd;
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
	color: #ffffff;
}

.table_top a{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
	color: #ffffff;
}

.table_top a:hover{
	color: #ff0000;
}
.table_subtop{
	padding-left: 15px;
	padding-right: 10px;
	padding-top: 4px;
	padding-bottom: 4px;
	background-color: #cdeafb;
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
	color: #000000;
}

.table_subtop a{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
	color: #000000;
}

.table_subtop a:hover{
	color: #ff0000;
}

.table_bottom_right{
	padding-right: 15px;
	padding-top: 4px;
	padding-bottom: 4px;
	background-color: #9bb5cd;
	font-size: 9px;
	text-align:right;
	font-weight: normal;
	text-decoration: none;
}	

.bulletin_portal_entry {
	padding: 3px;
}


.bulletin_entry_icon {

	padding: 5px;
	clear: left;
	float: left;
	width: 40px;	
}

.bulletin_entry_content {
	clear: none;
	float: left;
	padding-top:7px;
	padding-bottom:7px;
	padding-left:7px;
	padding-right:15px;
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #CCCCCC;
	font-size: 11px;
	color: #000000;
	text-decoration: none;
}
.bulletin_entry_content a{
	font-size: 11px;
	color: #003399;
	text-decoration: none;
}
.bulletin_entry_content a:hover{
	color: #FF0000;
}

.bulletin_icon_link {
	vertical-align:top;
	padding-top:7px;
}

.bulletin_link {
	border-bottom-width: 1px;
	border-bottom-style: dashed;
	border-bottom-color: #CCCCCC;
	font-size: 11px;
	color: #000000;
	text-decoration: none;
	vertical-align:top;
	padding-top:7px;
}

.bulletin_link a{
	font-size: 11px;
	color: #4360aa;
	text-decoration: none;
}
.bulletin_link a:hover{
	color: #FF0000;
}

#bulletin_form_content_board{
	background-color: #FFFFFF;
	float:left;
	clear:both;
	width: 85%;
	border: 1px solid #cfcfcf;

}

.bulletin_draft{
	padding-left: 15px;
	padding-right:10px;
	height:30px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d3d3d3;
}

.bulletin_draft a{
	color: #4360aa;
	text-decoration:none;
}

.bulletin_draft a:hover {
	color: #FF0000;
	text-decoration:none;
}

.bulletin_pending{
	padding-left: 15px;
	padding-right:10px;
	height:30px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d3d3d3;
	background-color: #e7f8cc;
}

.bulletin_pending a:link{
	color: #4360aa;
	text-decoration:none;
}

.bulletin_pending a:hover {
	color: #FF0000;
	text-decoration:none;
}

.bulletin_publish{
	padding-left: 15px;
	padding-right:10px;
	height:30px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d3d3d3;
	background-color: #ebe9ea;
}

.bulletin_publish a{
	color: #4360aa;
	text-decoration:none;
}

.bulletin_publish a:hover {
	color: #FF0000;
	text-decoration:none;
}

.bulletin_reject{
	padding-left: 15px;
	padding-right:10px;
	height:30px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d3d3d3;
	background-color: #ffcaca;
}

.bulletin_reject a{
	color: #4360aa;
	text-decoration:none;
}

.bulletin_reject a:hover {
	color: #FF0000;
	text-decoration:none;
}

.bulletin_msg{
	padding: 3px;
}
.bulletin_msgborder{
border: 1px solid #cfcfcf;
}

/*
==================================================  demographic common  ================================================================================
*/

#demo_photoborder{
	clear: right;
	float: right;
	width: 135px;
	height:166px; 
	margin-left: 10px ;
	margin-top: 5px ;
	margin-bottom: 10px ;
	margin-right: 20px ;
	background-image: url(<?=$SYS_CONFIG['Image']['Abs']?>demographic/photo_bkg.jpg);
   }
#demo_dotline{
    clear: right;
	float: right;	
	margin-left: 10px ;
	margin-right: 10px ;	
	width: 145px;
	height: 5px ;
	background-image: url((<?=$SYS_CONFIG['Image']['Abs']?>demographic/dot_line.gif);
 }   
.dotlinebottom{
	height: 5px;
	border-bottom-style: dashed;
	border-bottom-width: 1px;
	border-bottom-color: #999999;
	padding-right: 0;
	
}
 
#demo_photo{
	margin-top: 6px ;
	margin-left: 7px ;
	margin-right: 8px ;
	margin-bottom: 10px ;
   }   
   
#demo_sidemenu{
	clear: right;
	float:right;
    margin-left: 10px ;
	margin-right: 10px ;
	margin-top: 8px;
	width: 140px;
	border-bottom:dotted 1px #919bae;
} 

.demo_sidemenucontent{
	font-size:10px;
	color:#687091;
	text-align: left;
	text-decoration: none;	
		
} 
.demo_sidemenucontent:hover{
	font-size:10px;
	color:#043d47;
	font-weight: normal;
	text-decoration: none;
	} 
	
.demo_sidemenucontentbold{
	font-size:10px;
	color:#043d47;
	font-weight: bold;
	text-decoration: none;
	}     

#studentselection_main{
	padding: 2px;

}

#selection {
	clear: right;
	float: right;
	padding-top: 5px;
	padding-right: 0px;
	padding-bottom: 5px;
	padding-left: 0px;
}

.prenext{
	font-size: 9px;
	font-weight: normal;
	color: #999999;
	text-decoration: none;
}
.prenext a{
	color: #4360aa;
	text-decoration: none;
}
.prenext a:hover{
	color: #FF0000;

}

#demo_info{
	padding: 10px;
	clear: left;
	float: left;
	width: 75%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
}

#demo_info_header{
	padding-top: 5px;
	padding-right: 0px;
	padding-bottom: 5px;
	padding-left: 15px;
	background-color: #c5d5f1;
	clear: left;
	float: left;
	width: 720px;
	font-size:11px;
	font-weight:bold;
	color:#000000;
}

#demo_backtotop{
	padding-top: 5px;
	padding-right: 10px;
	padding-bottom: 10px;
	clear: left;
	float: left;
	width: 720px;
	font-size:11px;
	font-weight:bold;
	color:#2b57a5;
	border-top:dotted 1px #919bae;
	text-align:right;
}
#demo_backtotop a{
	font-size:11px;
	font-weight:bold;
	color:#2b57a5;
	text-decoration:none;
}
#demo_backtotop a:hover{
	font-size:11px;
	font-weight:bold;
	color:#045d9f;
	text-decoration:none;
}


#demo_info_header2{
	padding-top: 5px;
	padding-right: 0px;
	padding-bottom: 5px;
	padding-left: 15px;
	background-color: #dfe4ed;
	clear: left;
	float: left;
	width: 720px;
	font-size:11px;
	font-weight:bold;
	color:#000000;
}

#demo_info_medicalheader{
	clear: left;
	float: left;
	width: 45%;
	font-size:11px;
	font-weight:bold;
	color:#000000;
}

#demo_info_medicalalert{
	clear: right;
	float: right;
	width: 45%;
	font-size:11px;
	font-weight:bold;
	color:#bd2222;
	padding-right: 15px;
	text-align:right;
}

.medical_red{
  	vertical-align: middle;
	font-size:11px;
	font-weight:bold;
	color:#bd2222;
}

.medical_blue{
    vertical-align: middle;
	font-size:11px;
	font-weight:bold;
	color:#025198;
}

# headerleft {
	clear: left;
	float: left;
	font-size:11px;
	font-weight:bold;
	color:#000000;
	width: 200px;
}

#demo_info_subheader{
	padding-top: 5px;
	padding-right: 0px;
	padding-bottom: 3px;
	padding-left: 15px;
	clear: left;
	float: left;
	width: 720px;
	font-size:11px;
	font-weight:bold;
	color:#000000;
	border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #715bae; 
}

#demo_form{
	padding-right: 0px;
	padding-left: 0px;
	clear: left;
	float: left;
	width: 720px;
}

#demo_searchresult{
	padding: 2px;
	float:left;
	clear: left;
	margin-top: 5px;
	margin-bottom: 5px;
	margin-left: 14px;
	margin-right: 14px;
	width: 910px;
}	

#demo_formspacing{
	height: 2px;
	clear: left;
	float: left;
}
#demo_formspacingsmall{
	height: 5px;
	clear: left;
	float: left;
}	

.demo_content_title{
	padding-top: 3px;
	padding-right: 0px;
	padding-bottom: 3px;
	padding-left: 15px;
	width: 25%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	vertical-align:top;
}
.demo_content_none{
	padding: 3px;
	width: 25%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	vertical-align:top; 
}

.demo_content_titlebold{
	padding-top: 3px;
	padding-right: 0px;
	padding-bottom: 3px;
	padding-left: 15px;
	width: 25%;
	font-size:11px;
	font-weight:bold;
	color:#000000;
	vertical-align:top
}
.demo_content_titlehighlight{
	padding-top: 3px;
	padding-right: 0px;
	padding-bottom: 3px;
	font-size:11px;
	font-weight:bold;
	color:#bd2222;
	vertical-align:top
}

.demo_content{
	padding: 3px;
	width: 25%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f4f6f9;
	vertical-align:top;
	text-decoration:none; 
}
.demo_content a:hover{
	text-decoration:underline; 
}

/*
.demo_testtitle1{
	padding-top: 5px;
	padding-left: 15px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#4a3582;
	background-color: #ebe6f8;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC; 
	
}
.demo_testtitle{
	padding-top: 5px;
	padding-left: 5px;
	padding-right: 15px;
	padding-bottom: 5px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#4a3582;
	background-color: #ebe6f8;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC; 
}
*/
.demo_testtitle1lline{
	padding-top: 5px;
	padding-left: 15px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#4a3582;
	background-color: #ebe6f8;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC; 
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC; 
}

.demo_testtitle2{
	padding-top: 5px;
	padding-left: 5px;
	padding-right: 15px;
	padding-bottom: 5px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#4a3582;
	background-color: #ebe6f8;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC;
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC; 
}


#demo_testinfo1{
    clear:left;
	float:left;
	padding-top: 5px;
	padding-left: 15px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:10px;
	font-weight:bold;
	color:#818082;
	vertical-align:top;
}
#demo_testinfo{
    clear:right;
	float:right;
	padding-top: 5px;
	padding-left: 15px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:10px;
	font-weight:bold;
	text-align:right;
	color:#818082;
	vertical-align:top;
}
.demo_testcolumnrline{
	padding-top: 3px;
	padding-left: 5px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f5f2fc;
	vertical-align:top;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC; 
	border-right-width: 1px;
	border-right-style: solid;
	border-right-color: #CCCCCC; 
}
.demo_testcolumn1{
	padding-top: 3px;
	padding-left: 15px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f5f2fc;
	vertical-align:top;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC; 
}
.demo_testcolumn{
	padding-top: 3px;
	padding-left: 5px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f5f2fc;
	vertical-align:top;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC; 
}
/*
.demo_testcolumnoline1{
	padding-top: 3px;
	padding-left: 15px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f5f2fc;
	vertical-align:top;
}

.demo_testcolumnoline{
	padding-top: 3px;
	padding-left: 5px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f5f2fc;
	vertical-align:top;
	
}
*/
.demo_testcolumnresult1{
	padding-top: 3px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:normal;
	color:#4a3582;
	vertical-align:top;
}

.demo_testcolumnresult{
	padding-top: 3px;
	padding-right: 15px;
	padding-bottom: 3px;
	width: auto;
	font-size:11px;
	font-weight:bold;
	color:#4a3582;
	vertical-align:top;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #CCCCCC; 
	
}

.demo_contentlong{
	padding: 3px;
	width: 75%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f4f6f9;
	vertical-align:top; 
}		

.demo_contentbold{
	padding: 3px;
	width: 25%;
	font-size:11px;
	font-weight:bold;
	color:#000000;
	background-color: #f4f6f9;
	vertical-align:top; 
}	

.demo_content_highlight{
	padding: 3px;
	width: 25%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #fee5e6;
	vertical-align:top
}

.demo_content_highlight2{
	padding: 3px;
	width: 25%;
	font-size:11px;
	font-weight:bold;
	color:#bd2222;
	background-color: #f4f6f9;
	vertical-align:top
}

#sub_layer_demo_medical{
	width:240px;
	position: absolute;
	visibility: visible;
	
}	
#sub_layer_demo_medical2{
	position: absolute;
	visibility: hidden;
	left: 640px;
	top: 570px;
	width: 220px;
	
}	

#demo_form{
	 float:left; 
	 margin:10px;
	 padding:3px; 
	 width:90%;	 
}

#demo_form_line{
	 margin:10px;
	 padding:3px; 
	 width:90%;	 
}

.demotextbox {
	font-size:11px;
	border:1px solid #cfcfcf;
}

.search_top{
    font-size:9px;
	font-weight:normal;
	color:#000000;
	text-decoration:none;
	background-color: #cccccc;
	padding-left:15px;
	padding-top:5px;
	padding-bottom:5px;
}
.search_top a{
	font-size: 9px;
	font-weight: normal;
	text-decoration: none;
	color: #000000;
}

.search_top a:hover{
	color: #ff0000;
}	

.search_row{
    font-size:11px;
	font-weight:normal;
	color:#000000;
	padding-left:15px;
	padding-right:8px;
	padding-top:5px;
	padding-bottom:5px;
	text-decoration:none;
	background-color: #ffffff;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d3d3d3;
}
.search_row a{
	color: #4360aa;
	text-decoration:none;
}

.search_row a:hover {
	color: #FF0000;
	text-decoration:none;
}

.search_divider{
    border-top-width: 2px;
	border-top-style: solid;
	border-top-color: #5a5a5a;
	width:100%; 
} 
.search_subheader{
	padding-right: 0px;
	clear: left;
	float: left;
	width: 100%;
	font-size:11px;
	font-weight:bold;
	color:#000000;
}

.search_title{
   font-size:11px;
   font-weight:normal;
   color:#000000;
   padding: 5px;
   width:20%; 
}

.report_title{
    border-bottom-width: 2px;
	border-bottom-style: solid;
	border-bottom-color: #5a5a5a;
	width:100%; 
} 

.select_studentlist {
	width:100%;
}

.report_bkg1{
     background-color: #dde6f1;
}    

.report_bkg2{
     background-color: #cadef4;
} 
.report_whitebkg{
     background-color: #ffffff;
} 

#report{
	margin-left: 20px;
	margin-right: 20px;
	width: 96%;
	clear: both;
	font-size:11px;
	line-height:normal;
}

.report_content{
	padding: 3px;
	width: 25%;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #ffffff;
	vertical-align:top;
	text-decoration:none;
	padding-left:20px;
	
}

#report_list{
	width: 100%;
}


/*
==================================================  Medical Room Log  ================================================================================
*/
.medical_subcontent{
	font-size:9px;
	font-weight:normal;
	color:#e46aa0;
	padding-left:30px;
}
.medical_subcontent2{
	font-size:9px;
	font-weight:normal;
	color:#5a97d0;
	padding-left:20px;
}

#medicalroom_log {
	 margin:10px;
	 padding:3px; 
	 width:95%;	 
}
#medical_header {
	padding-top: 3px;
	padding-right: 0px;
	padding-bottom: 3px;
	padding-left: 15px;
	margin:10px;
	background-color: #c5d5f1;
	width: 95%;
	font-size:11px;
	font-weight:bold;
    color:#000000;
 }
 .medical_form{
	padding: 3px;
	width: 25%;
	vertical-align:top;
	font-size:11px;
	font-weight:normal;
    color:#000000;
}
 .medical_form2{
	padding: 3px;
	width: 75%;
	vertical-align:top;
	font-size:11px;
	font-weight:normal;
    color:#000000;
}
 .indicator{
	font-size:11px;
	font-weight:normal;
    color:#fb0505;
}

.medical_row{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;
	background-color: #ffffff;	
}

.medical_row a{
	color: #4360aa;
	text-decoration:none;
}

.medical_row a:hover {
	color: #FF0000;
	text-decoration:none;
}

#medical_subtitle {
	margin-left: 10px;
	width:auto;
	margin-right:12px;
	margin-top: 3px;
	margin-bottom: 3px;
	padding:2px;
	height: 12px;
	
}

.subtitle {
  font-size:11px;
  font-weight:bold;
  color:#003399;
  text-decoration:none;
} 

.subtitle a{
  font-size:11px;
  font-weight:normal;
  color:#666666;
  text-decoration:none;
} 

.subtitle a:hover{
  font-size:11px;
  font-weight:bold;
  color:#003399;
  text-decoration:none;
} 

/*
==================================================  CAS  ================================================================================
*/

.cas_row{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;
	background-color: #ffffff;	
}

.cas_row a{
	color: #4360aa;
	text-decoration:none;
}

.cas_row a:hover {
	color: #FF0000;
	text-decoration:none;
}

.cas_row2{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #d8b4d8;
	background-color: #faf0fa;	
}

.cas_row2 a{
	color: #4360aa;
	text-decoration:none;
}

.cas_row2 a:hover {
	color: #FF0000;
	text-decoration:none;
}

.cas_row3{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;
	background-color: #f1f9fe;	
}

.cas_row3 a{
	color: #4360aa;
	text-decoration:none;
}

.cas_row3 a:hover {
	color: #FF0000;
	text-decoration:none;
}

.cas_rowhighlight{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;
	background-color: #fdfdcd;	
}

.cas_rowhighlight a{
	color: #4360aa;
	text-decoration:none;
}

.cas_rowhighlight a:hover {
	color: #FF0000;
	text-decoration:none;
}

.cas_subrow{
    padding-top: 3px;
	padding-left: 2px;
	padding-right:10px;
	padding-bottom: 3px;
	height:25px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #ffffff;	
}

.cas_subrow a{
	color: #4360aa;
	text-decoration:none;
}

.cas_subrow a:hover {
	color: #FF0000;
	text-decoration:none;
}

.cas_subrow3{
    padding-top: 3px;
	padding-left: 2px;
	padding-right:10px;
	padding-bottom: 3px;
	height:25px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #f1f9fe;	
}

.cas_subrow3 a{
	color: #4360aa;
	text-decoration:none;
}

.cas_subrow3 a:hover {
	color: #FF0000;
	text-decoration:none;
}

.cas_subrowhighlight{
    padding-top: 3px;
	padding-left: 2px;
	padding-right:10px;
	padding-bottom: 3px;
	height:25px;
	vertical-align: middle;
	font-size:11px;
	font-weight:normal;
	color:#000000;
	background-color: #fdfdcd;	
}

.cas_subrowhighlight a{
	color: #4360aa;
	text-decoration:none;
}

.cas_subrowhighlight a:hover {
	color: #FF0000;
	text-decoration:none;
}

.cas_rowdone{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:normal;
	color:#666666;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #9bb5cd;
	background-color: #eeeeee;	
}

.cas_rowdone a{
	color: #4360aa;
	text-decoration:none;
}

.cas_rowdone a:hover {
	color: #FF0000;
	text-decoration:none;
}

#cas_regbtn{
    margin-left: 10px;
	width:auto;
	margin-right:12px;
	margin-bottom: 5px;
	padding-top:5px;
	text-align:right;
	
}

#cas_top{
    padding-top:5px;
	margin-left: 10px;
	width:auto;
	margin-right:12px;
	margin-bottom: 5px;
	height: 25px;	
}

.show_hide{
	font-size: 9px;
	font-weight: bold;
	text-decoration: none;
	color: #1b2a9b;
}

.show_hide a{
	color: #1b2a9b;
	font-weight:normal;
	text-decoration: none;
}

.show_hide a:hover {
	color: #FF0000;
	text-decoration: none;
}

/*
==================================================  System Management  ================================================================================
*/
.system_row2{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:bold;
	color:#000000;
	background-color:#ececee;
	border-bottom-width: 1px;
	border-bottom-style: solid;
	border-bottom-color: #999999;
	
}
.system_row2 a{
	color: #4360aa;
	text-decoration:none;
}

.system_row2 a:hover {
	color: #FF0000;
	text-decoration:none;
}
.subsystem_row{
	padding-left: 25px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:normal;
	color:#000000;
	background-color:#f1f9fe;
}
.subsystem_row a{
	color: #4360aa;
	text-decoration:none;
}

.subsystem_row a:hover {
	color: #FF0000;
	text-decoration:none;
}

.system_row{
	padding-left: 15px;
	padding-right:10px;
	padding-top: 7px;
	padding-bottom: 7px;
	vertical-align: middle;
	font-size:11px;
	text-decoration:none;
	font-weight:normal;
	color:#000000;
	background-color:#f1f9fe;
}
.system_row a{
	color: #4360aa;
	text-decoration:none;
}

.system_row a:hover {
	color: #FF0000;
	text-decoration:none;
}

.hori_line_blue {
   border-bottom-width: 1px;
   border-bottom-style: solid;
   border-bottom-color: #9bb5cd;
}

.hori_line_dashed {
   border-bottom-width: 1px;
   border-bottom-style: dashed;
   border-bottom-color: #afd7ef;
}

#system_bkg{
    padding: 2px;
	margin-left: 50px;
	margin-right:50px;
	text-align:center;
	
}
#system_bkg2{
    padding: 2px;
	margin-left: 50px;
	margin-right:50px;
}

/*
==================================================  ARR  ================================================================================
*/

.bkgcolor{
    padding: 10px;
    border: 1px dashed  #999999; 
}

#arr_head{
    padding-left:15px;
	padding-right:60px;
</style>