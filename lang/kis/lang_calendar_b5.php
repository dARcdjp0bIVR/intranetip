<?
$kis_lang['month']			= "月";
$kis_lang['year']			= "年";
$kis_lang['comingmonths']		= "未過月份";
$kis_lang['allmonths']			= "所有月份";
$kis_lang['publicholiday']		= "公眾假期";
$kis_lang['schoolholiday']		= "學校假期";
$kis_lang['sun']			= "日";
$kis_lang['mon']			= "一";
$kis_lang['tue']			= "二";
$kis_lang['wed']			= "三";
$kis_lang['thu']			= "四";
$kis_lang['fri']			= "五";
$kis_lang['sat']			= "六";

?>