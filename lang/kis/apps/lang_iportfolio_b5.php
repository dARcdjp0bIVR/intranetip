<?
/*
* using by : 
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/
$kis_lang['studentaccount'] 	= "學生用戶";
$kis_lang['studentinfo'] 	= "學生資料";
$kis_lang['studentlist'] 		= "學生清單";
$kis_lang['learningportfolio']		= "學習檔案";
$kis_lang['schoolrecords'] 		= "學校記錄";
$kis_lang['assessmentreport'] 		= "評估報告";
$kis_lang['sbs'] 		= "校本計劃";
$kis_lang['awards'] 			= "獎項";
$kis_lang['activities'] 		= "活動";
$kis_lang['readingrecord'] 		= "閱讀報告";
$kis_lang['assessments'] 		= "評估";
$kis_lang['assessmentlist'] 		= "評估清單";
$kis_lang["portfoliodata"] = "幼兒學習歷程評量";
$kis_lang['schoolaward'] = "學校獎項";
$kis_lang['synctocees'] = "資料處理 (轉移至中央電子教育系統模組)";

$kis_lang['emergencycontactno']		= "緊急聯絡號碼";
$kis_lang['homeaddress']		= "住址";
$kis_lang['schoolyear']			= "學業年度";
$kis_lang['admissiondate']		= "入學日期";
$kis_lang['relationship']		= "關係";
$kis_lang['guardian']			= "監護人";
$kis_lang['guardianinformation']	= "監護人資料";
$kis_lang['mainguardian']		= "主要監護人";
$kis_lang['basicinformation']		= "基本資料";
$kis_lang['please_select']		= "請選擇";
$kis_lang['please_fill_in']		= "請填上";
$kis_lang['new_awards'] = "新增獎項";
$kis_lang['edit_awards'] = "編輯獎項";
$kis_lang['new_schoolaward'] = "新增學校獎項";
$kis_lang['edit_schoolaward'] = "編輯學校獎項";
$kis_lang['new_activities'] = "新增活動";
$kis_lang['edit_activities'] = "編輯活動";
$kis_lang['new_assessment'] = "新增評估";
$kis_lang['edit_assessment'] = "編輯評估";
$kis_lang['class_number'] = "學號";
$kis_lang['enter_student_name'] = "輸入學生姓名";

$kis_lang['msg']['are_you_sure_to_delete'] = "你是否確認刪除?";
$kis_lang['msg']['please_select_student'] = "請選擇學生。";

$kis_lang['ImportSourceFile'] = "資料檔案";
$kis_lang['CSVFileFormat'] = '(.csv 或 .txt 檔案)';
$kis_lang['SelectCSVFile'] = "選擇CSV檔案";
$kis_lang['CSVConfirmation'] = "確定";
$kis_lang['ImportResult'] = "匯入結果";
$kis_lang['Remark'] = "備註";
$kis_lang['CSVSample'] = '範本檔案';
$kis_lang['ClickHereToDownloadSample'] = '按此下載範例';
$kis_lang['DataColumn'] = "資料欄";
$kis_lang['Column'] = "欄位";
$kis_lang['next'] = "下一步";
$kis_lang['confirm'] = "確認";
$kis_lang['success'] = "成功";
$kis_lang['failed'] = "失敗";
$kis_lang['done'] = "完成";
$kis_lang['Import']['msg']['PleaseSelectCSVorTXT'] = "請選擇CSV檔(.csv) 或純文字檔(.txt)";
$kis_lang['invalidHeader'] = "標題格式不正確";
$kis_lang['CSVFileNoData'] = "沒有資料於檔案內";
$kis_lang['Row'] = "行#";
$kis_lang['ImportSuccessful'] = "資料匯入成功";

#Table Field
$kis_lang['assessment_title'] = "標題";
$kis_lang['release_date'] = "開放日期";
$kis_lang['classname'] = "班別";
$kis_lang['class_number'] = "學號";
$kis_lang['last_update_date'] = "最後更新時間";
$kis_lang['school_record_updated'] = "已更新學校紀錄";
$kis_lang['lpf_updated'] = "已更新學習檔案";
$kis_lang['assessment_report_updated'] = "已更新評估報告";
$kis_lang['no_of_students'] = "學生總數";
$kis_lang['grouplist_title'] = "小組名稱";
$kis_lang['member_total'] = "成員數目";
$kis_lang['member_name'] = "成員名稱";
$kis_lang['identity'] = "身分";
$kis_lang['studentgroup'] = "學生小組";
$kis_lang['teachergroup'] = "課程主任/校務主任";
$kis_lang['subjectpanel'] = "級主任";
$kis_lang['student'] = "學生";
$kis_lang['teacher'] = "教學職務員工";
$kis_lang['subject'] = "科目";
$kis_lang['form'] = "級別";


$kis_lang['term'] = "學期";
$kis_lang['role'] = "職位";
$kis_lang['activity_name'] = "活動名稱";
$kis_lang['performance'] = "表現";
$kis_lang['organization'] = "舉辦機構";
$kis_lang['award_title'] = "獎項";
$kis_lang['award_date'] = "日期";
$kis_lang['award_organization'] = "頒發機構";
$kis_lang['subject_area'] = "項目名稱";
$kis_lang['remarks'] = "備註";
$kis_lang['uploaded_date'] = "上載日期";
$kis_lang['activities_count'] = "活動總數";
$kis_lang['awards_count'] = "獎項總數";

#Assessment Report
$kis_lang['Assessment'] = array();
$kis_lang['Assessment']['NewAssessment'] = "新增評估";
$kis_lang['Assessment']['Assessment'] = "評估";
$kis_lang['Assessment']['EditAssessment'] = "編輯評估";
$kis_lang['Assessment']['Title'] = "標題";
$kis_lang['Assessment']['ReleaseDate'] = "開放日期";
$kis_lang['Assessment']['AllClasses'] = "所有班別";
$kis_lang['Assessment']['Target'] = "對象";
$kis_lang['Assessment']['ReleaseStatus'][1] = "已開放";
$kis_lang['Assessment']['ReleaseStatus'][2] = "未開放";
$kis_lang['Assessment']['UploadStatus'][1] = "已上載";
$kis_lang['Assessment']['UploadStatus'][2] = "未上載";
$kis_lang['Assessment']['Search'] = "搜尋....";
$kis_lang['Assessment']['NoOfUploads'] = "上載總數 / 學生總數";
$kis_lang['Assessment']['AssessmentList'] = "評估列表";
$kis_lang['Assessment']['AssessmentFile'] = "評估檔案";
$kis_lang['Assessment']['AssessmentTitle'] = "評估標題";
$kis_lang['Assessment']['UploadedDate'] = "上載日期";
$kis_lang['Assessment']['EditBy'][0] = "由";
$kis_lang['Assessment']['EditBy'][1] = "上載";
$kis_lang['Assessment']['Upload'] = "上載";
$kis_lang['Assessment']['LastUpdated'] = "最後更新時間";

#Student Account
$kis_lang['StudentAccount'] = array();
$kis_lang['StudentAccount']['ClassList'] = "班別列表";
$kis_lang['StudentAccount']['Relation']['01'] = "父親";
$kis_lang['StudentAccount']['Relation']['02'] = "母親";
$kis_lang['StudentAccount']['Relation']['03'] = "祖父";
$kis_lang['StudentAccount']['Relation']['04'] = "祖母";
$kis_lang['StudentAccount']['Relation']['05'] = "兄/弟";
$kis_lang['StudentAccount']['Relation']['06'] = "姊/妹";
$kis_lang['StudentAccount']['Relation']['07'] = "親戚";
$kis_lang['StudentAccount']['Relation']['08'] = "其他";
$kis_lang['StudentAccount']['StutdentList'] = "學生列表";
$kis_lang['StudentAccount']['StudentPhoto'] = "學生相片";
$kis_lang['StudentAccount']['ViewBy'] = "檢視";	
$kis_lang['StudentAccount']['View'] = "檢視";	
$kis_lang['StudentAccount']['PersonalInformation'] = "個人資料";
$kis_lang['StudentAccount']['schoolrecord'] = "學校紀錄";

#Activities
$kis_lang['Activity'] = array();
$kis_lang['Activity']['WholeYear']		= "全年";

#School-Based Scheme
$kis_lang['SchoolBasedScheme'] = array();
$kis_lang['SchoolBasedScheme']['Phase']			= "階段";
$kis_lang['SchoolBasedScheme']['Title']			= "標題";
$kis_lang['SchoolBasedScheme']['From']			= "開始";
$kis_lang['SchoolBasedScheme']['to']				= "結束";
$kis_lang['SchoolBasedScheme']['Target']			= "參加者";
$kis_lang['SchoolBasedScheme']['Status']			= "狀態";
$kis_lang['SchoolBasedScheme']['Finished']		= "完成";
$kis_lang['SchoolBasedScheme']['InProgress']		= "進行中";
$kis_lang['SchoolBasedScheme']['Pending']		= "未開始";
$kis_lang['SchoolBasedScheme']['NoRecord']		= "暫時仍未有任何紀錄";	
$kis_lang['SchoolBasedScheme']['NoAnswer']		= "沒作答";	

#Learning Portfolio
$kis_lang['LearningPortfolio'] = array();
$kis_lang['LearningPortfolio']['LastModified']	= "最後修改時間";
$kis_lang['LearningPortfolio']['Published']	= "已發佈";
$kis_lang['LearningPortfolio']['Publish']	= "發佈";
$kis_lang['LearningPortfolio']['Drafted']	= "已起草";
$kis_lang['LearningPortfolio']['Comments']	= "意見";
$kis_lang['LearningPortfolio']['FillIn']	= "填寫";

#Portfolio Data
$kis_lang['PortfolioData'] = array();
$kis_lang['PortfolioData']['Birthday'] = '出生日期';
$kis_lang['PortfolioData']['StartSchool'] = '開學';
$kis_lang['PortfolioData']['EndSchool'] = '散學';
$kis_lang['PortfolioData']['Ages'] = '年齡';
$kis_lang['PortfolioData']['Age']['Year'] = '歲';
$kis_lang['PortfolioData']['Age']['Month'] = '個月';
$kis_lang['PortfolioData']['Weight'] = '體重';
$kis_lang['PortfolioData']['WeightKg'] = '千克';
$kis_lang['PortfolioData']['Height'] = '體高';
$kis_lang['PortfolioData']['HeightCm'] = '厘米';
$kis_lang['PortfolioData']['TermDayCount'] = '本學期授課';
$kis_lang['PortfolioData']['PresentDayCount'] = '該生上課';
$kis_lang['PortfolioData']['SickLeaveDayCount'] = '病假';
$kis_lang['PortfolioData']['OthersLeaveDayCount'] = '事假';
$kis_lang['PortfolioData']['LateDayCount'] = '遲到';
$kis_lang['PortfolioData']['EarlyLeaveDayCount'] = '早退';
$kis_lang['PortfolioData']['Award'] = '獎項';
$kis_lang['PortfolioData']['Remarks'] = '特錄';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '<font color=red>*</font>班別';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '<font color=red>*</font>班號';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '開學時年齡(歲)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '開學時年齡(月)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '開學時體重';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '開學時體高';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '散學時年齡(歲)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '散學時年齡(月)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '散學時體重';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '散學時體高';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '本學期授課日數';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '學生上課日數';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '病假日數';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '事假日數';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '遲到日數';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '早退日數';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '獎項';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '特錄';
$kis_lang['PortfolioData']['Msg']['Import']['invalidClass'] = '找不到此班別';
$kis_lang['PortfolioData']['Msg']['Import']['invalidStudent'] = '找不到此學生';

$kis_lang['Settings']['Description'] = '簡介';
$kis_lang['Settings']['NewGrouplist'] = '新增小組';
$kis_lang['Settings']['NewStudent'] = '新增學生';
$kis_lang['Settings']['NewTeacher'] = '新增老師';
$kis_lang['Settings']['SelectedUser'] = '已選擇用戶';
$kis_lang['Settings']['CtrlMultiSelectMessage'] = '(按下CTRL鍵並點選項目以選擇多項。)';
?>