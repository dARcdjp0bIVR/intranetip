<?php 
/*
* using by : 
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/
# InterSchoolActivity
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Title'] = '聯校活動';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Date'] = '日期';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Event'] = '項目';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Venue'] = '地點';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Guest'] = '嘉賓/講者';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ParticipatedSchool'] = '參與學校';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Target'] = '對象';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['TotalParticipant'] = '人數';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ReportStartDate'] = '報告開始日期';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ReportEndDate'] = '報告結束日期';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['RetrieveDataFromSystem'] = '從系統匯入資料';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['RetrieveDataRow'] = '記錄總數';

# Intra School Activity
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Title'] = '';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Date'] = '日期';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'] = '項目';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['EventName'] = '活動名稱';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Venue'] = '地點';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Guest'] = '嘉賓/講者';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportStartDate'] = '報告開始日期';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportEndDate'] = '報告結束日期';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Participant'] = '參與者 / 人數';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'] = '獎項';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['TotalParticipant'] = '人數';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Name'] = '姓名';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Position'] = '職銜';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'] = '參賽組別';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'] = '主辦機構';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'] = '頒獎日期';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportStartDate'] = '報告開始日期';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportEndDate'] = '報告結束日期';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Class'] = '班別';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Target'] = '對象';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['RetrieveDataFromSystem'] = '從系統匯入資料';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['RetrieveDataRow'] = '記錄總數';


# Monthly Activity Report
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title'] = '每月活動報告';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'] = '日期';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'] = '頒獎日期';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'] = '項目';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['EventName'] = '活動名稱';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'] = '地點';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'] = '嘉賓/講者';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedSchool'] = '參與學校';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'] = '參與者 / 人數';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['TotalParticipant'] = '人數';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'] = '備註';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'] = '主辦機構';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'] = '參賽組別';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Name'] = '姓名';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Class'] = '班別';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Position'] = '職銜';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'] = '獎項';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ReportDate'] = '報告日期';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Target'] = '對象';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['RetrieveDataFromSystem'] = '從系統匯入資料';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['RetrieveDataRow'] = '記錄總數';

# For Header
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] = '學校活動報告';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section1'] = '範疇一';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section2'] = '範疇二';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section3'] = '範疇三';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section4'] = '範疇四';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ManagementAndOrganization'] = '管理與組織';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'] = '學與教';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport'] = '學生支援';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAndStudentPerformance'] = '學校及學生表現';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'] = '教師專業發展及培訓';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterflowAndSharing'] = '對外交流及分享';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InsideSchoolActivity'] = '校內活動';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OutsideSchoolActivity'] = ' 校外活動';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentSchoolActivity'] = '家校活動';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentEducation'] = '家長教育';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OtherSupportActivity'] = '其他支援活動';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Others'] = '其他';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAward'] = '學校獎項';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StaffAward'] = '職員獎項';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentAward'] = '學生獎項';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'] = '各校';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'] = '聯校';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAdministration'] = '學校行政';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentArcheivement'] = '學生成就';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_Category'] = '活動類型(活動報告用)';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_AllowSync'] = '允許同步至活動報告';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Row'] = '項';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['CancelSubmit'] = '取消呈交';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Month'] = '月份';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['NewReport'] = '新增報告';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SelectReportPeriod'] = '選擇報告時段';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetReport'] = '目標報告';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TotalParticipant'] = '人數';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ActivityTarget'] = '活動對象';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Guest'] = '嘉賓';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParticipatedSchool'] = '參與學校';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetSection'] = '目標範疇';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['MonthlyActivityReport'] = '每月活動報告'; 
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromeEnrolment'] = "從課外活動管理匯入";
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromiPortoflio'] = "從電子學習檔案管理匯入";
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync'] = '同步';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['LastModificatedDate'] = '最後更新時間';

# Hint
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['OtherSupportActivity'] = '如入學適龐，幼小銜接、有特殊需要兒童';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['SchoolAdministration'] = '如﹕ 典禮、進行會議及活動';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ProfessionalDevelopmentAndTraning'] = '由校方安排及推廌之培訓活動';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['IfExists'] = '如有';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['IfAvailable'] = '如適用';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ConfirmRemoveReport'] = '是否確定刪除此報告?(此動作無法復原)';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ConfirmImport'] = '確認匯入';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['AlreadySubmitted'] = '此報告已被呈交至中央平台';
$Lang['CEES']['Managemnet']['SchoolActivityReport']['Hint']['SyncEnrolmentAlert'] = '同步動作將會覆蓋現存之報告紀錄，請確認是否繼續進行﹖';

# Msg
$Lang['CEES']['Management']['SchoolActivityReport']['Msg']['Success'] = '成功';
$Lang['CEES']['Management']['SchoolActivityReport']['Msg']['ImportCount'] = '匯入行數';
$Lang['CEES']['Management']['SchoolActivityReport']['Msg']['ImportEmptyError'] = '錯誤﹕ CSV內容不能為空白';
?>