<?php
/*
* using by : 
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/
$kis_lang['studentaccount'] 	= "Student Account";
$kis_lang['studentinfo'] 	= "Student Information";
$kis_lang['studentlist'] 		= "Student List";
$kis_lang['learningportfolio'] 		= "Learning Portfolio";
$kis_lang['schoolrecords'] 		= "School Record";
$kis_lang['assessmentreport'] 		= "Assessment Report";
$kis_lang['sbs'] 		= "School-Based Scheme";
$kis_lang['awards'] 			= "Awards";
$kis_lang['activities'] 		= "Activities";
$kis_lang['readingrecord'] 		= "Reading Record";
$kis_lang['assessments'] 		= "Assessments";
$kis_lang['assessmentlist'] 		= "Assessment List";
$kis_lang["portfoliodata"] = "Portfolio Data";
$kis_lang['schoolaward'] = "School Award";
$kis_lang['synctocees'] = "Data Handling (Transfer to CEES Module)";

$kis_lang['emergencycontactno']		= "Emergency Contact No.";
$kis_lang['homeaddress']		= "Home Address";
$kis_lang['schoolyear']			= "School Year";
$kis_lang['admissiondate']		= "Admission Date";
$kis_lang['relationship']		= "Relationship";
$kis_lang['guardian']			= "Guardian";
$kis_lang['guardianinformation']	= "Guardian Information";
$kis_lang['mainguardian']		= "Main Guardian";
$kis_lang['basicinformation']		= "Basic Information";
$kis_lang['please_select']		= "Please Select";
$kis_lang['please_fill_in']		= "Please fill in ";
$kis_lang['new_awards'] = "New Award";
$kis_lang['edit_awards'] = "Edit Award";
$kis_lang['new_schoolaward'] = "New School Award";
$kis_lang['edit_schoolaward'] = "Edit School Award";
$kis_lang['new_activities'] = "New Activity";
$kis_lang['edit_activities'] = "Edit Activity";
$kis_lang['new_assessment'] = "New Assessment";
$kis_lang['edit_assessment'] = "Edit Assessment";
$kis_lang['class_number'] = "Class Number";
$kis_lang['enter_student_name'] = "Enter Student Name";

$kis_lang['msg']['are_you_sure_to_delete'] = "Are you sure to delete this?";
$kis_lang['msg']['please_select_student'] = "Please select student.";

$kis_lang['ImportSourceFile'] = "CSV Scoure File";
$kis_lang['CSVFileFormat'] = '(.csv or .txt file)';
$kis_lang['SelectCSVFile'] = "Select CSV file";
$kis_lang['CSVConfirmation'] = "Confirmation";
$kis_lang['ImportResult'] = "Imported Result";
$kis_lang['Remark'] = "Remark";
$kis_lang['CSVSample'] = 'Sample File';
$kis_lang['ClickHereToDownloadSample'] = 'Click here to download sample';
$kis_lang['DataColumn'] = "Data Column";
$kis_lang['Column'] = "Column";
$kis_lang['next'] = "Next";
$kis_lang['confirm'] = "Confirm";
$kis_lang['success'] = "Success";
$kis_lang['failed'] = "Failed";
$kis_lang['done'] = "Done";
$kis_lang['Import']['msg']['PleaseSelectCSVorTXT'] = "Please select CSV(.csv) or Text(.txt) file";
$kis_lang['invalidHeader'] = "Invalid header format";
$kis_lang['CSVFileNoData'] = "No data in the uploaded file";
$kis_lang['Row'] = "Row#";
$kis_lang['ImportSuccessful'] = "Data imported successfully.";

#Table Field
$kis_lang['assessment_title'] = "Title";
$kis_lang['release_date'] = "Release Date";
$kis_lang['classname'] = "Class";
$kis_lang['class_number'] = "Class Number";
$kis_lang['last_update_date'] = "Last Updated";
$kis_lang['school_record_updated'] = "School Record Updated";
$kis_lang['lpf_updated'] = "Learning Portfolio Updated";
$kis_lang['assessment_report_updated'] = "Assessment Report Updated";
$kis_lang['no_of_students'] = "No.of Students";
$kis_lang['grouplist_title'] = "Group Name";
$kis_lang['member_total'] = "Number of Group Members";
$kis_lang['member_name'] = "Name of Member";
$kis_lang['identity'] = "Identity";
$kis_lang['studentgroup'] = "Student Group";
$kis_lang['teachergroup'] = "Head Teacher";
$kis_lang['subjectpanel'] = "Senior Teacher";
$kis_lang['student'] = "Student";
$kis_lang['teacher'] = "Teaching Staff";
$kis_lang['subject'] = "Subject";
$kis_lang['form'] = "Form";




$kis_lang['term'] = "Term";
$kis_lang['role'] = "Role";
$kis_lang['activity_name'] = "Activity Name";
$kis_lang['performance'] = "Performance";
$kis_lang['organization'] = "Organization";
$kis_lang['award_title'] = "Award Title";
$kis_lang['award_date'] = "Date";
$kis_lang['award_organization'] = "Organization";
$kis_lang['subject_area'] = "Subject Area";
$kis_lang['remarks'] = "Remarks";
$kis_lang['uploaded_date'] = "Uploaded Date";
$kis_lang['activities_count'] = "No. of Activities";
$kis_lang['awards_count'] = "No. of Awards";

#Assessment Report
$kis_lang['Assessment'] = array();
$kis_lang['Assessment']['NewAssessment'] = "New Assessment";
$kis_lang['Assessment']['Assessment'] = "Assessment";
$kis_lang['Assessment']['EditAssessment'] = "Edit Assessment";
$kis_lang['Assessment']['Title'] = "Title";
$kis_lang['Assessment']['ReleaseDate'] = "Release Date";
$kis_lang['Assessment']['AllClasses'] = "All Classes";
$kis_lang['Assessment']['Target'] = "Target";
$kis_lang['Assessment']['ReleaseStatus'][1] = "Released";
$kis_lang['Assessment']['ReleaseStatus'][2] = "Not Released";
$kis_lang['Assessment']['UploadStatus'][1] = "Uploaded";
$kis_lang['Assessment']['UploadStatus'][2] = "Not Uploaded";
$kis_lang['Assessment']['Search'] = "Search....";
$kis_lang['Assessment']['NoOfUploads'] = "No.of Uploads / No. of Students";
$kis_lang['Assessment']['AssessmentList'] = "Assessment List";
$kis_lang['Assessment']['AssessmentFile'] = "Assessment File";
$kis_lang['Assessment']['AssessmentTitle'] = "Assessment Title";
$kis_lang['Assessment']['UploadedDate'] = "Uploaded Date";
$kis_lang['Assessment']['EditBy'][0] = "by ";
$kis_lang['Assessment']['EditBy'][1] = "";
$kis_lang['Assessment']['Upload'] = "Upload";
$kis_lang['Assessment']['LastUpdated'] = "Last Updated";


#Student Account
$kis_lang['StudentAccount'] = array();
$kis_lang['StudentAccount']['ClassList'] = "Class List";
$kis_lang['StudentAccount']['Relation']['01'] = "Father";
$kis_lang['StudentAccount']['Relation']['02'] = "Mother";
$kis_lang['StudentAccount']['Relation']['03'] = "Grandfather";
$kis_lang['StudentAccount']['Relation']['04'] = "Grandmother";
$kis_lang['StudentAccount']['Relation']['05'] = "Brother";
$kis_lang['StudentAccount']['Relation']['06'] = "Sister";
$kis_lang['StudentAccount']['Relation']['07'] = "Relative";
$kis_lang['StudentAccount']['Relation']['08'] = "Other";
$kis_lang['StudentAccount']['StutdentList'] = "Stutdent List";
$kis_lang['StudentAccount']['StudentPhoto'] = "Student Photo";
$kis_lang['StudentAccount']['ViewBy'] = "View By";
$kis_lang['StudentAccount']['View'] = "View";
$kis_lang['StudentAccount']['PersonalInformation'] = "Personal Information";
$kis_lang['StudentAccount']['schoolrecord'] = "School Record";
	
	
 
#Activities
$kis_lang['Activity'] = array();
$kis_lang['Activity']['WholeYear']	= "Whole Year";

#School-Based Scheme
$kis_lang['SchoolBasedScheme'] = array();
$kis_lang['SchoolBasedScheme']['Phase']			= "Phase";
$kis_lang['SchoolBasedScheme']['Title']			= "Title";
$kis_lang['SchoolBasedScheme']['From']			= "From";
$kis_lang['SchoolBasedScheme']['to']				= "to";
$kis_lang['SchoolBasedScheme']['Target']			= "Target";
$kis_lang['SchoolBasedScheme']['Status']			= "Status";
$kis_lang['SchoolBasedScheme']['Finished']		= "Finished";
$kis_lang['SchoolBasedScheme']['InProgress']	= "In Progress";
$kis_lang['SchoolBasedScheme']['Pending']		= "Pending";
$kis_lang['SchoolBasedScheme']['NoRecord']		= "There is no record at the moment.";	
$kis_lang['SchoolBasedScheme']['NoAnswer']		= "Not answered";	

#Learning Portfolio
$kis_lang['LearningPortfolio'] = array();
$kis_lang['LearningPortfolio']['LastModified']	= "Last modified";
$kis_lang['LearningPortfolio']['Published']	= "Published";
$kis_lang['LearningPortfolio']['Publish']	= "Publish";
$kis_lang['LearningPortfolio']['Drafted']	= "Drafted";
$kis_lang['LearningPortfolio']['Comments']	= "Comments";
$kis_lang['LearningPortfolio']['FillIn']	= "Fill In";

#Portfolio Data
$kis_lang['PortfolioData'] = array();
$kis_lang['PortfolioData']['Birthday'] = 'Birthday';
$kis_lang['PortfolioData']['StartSchool'] = 'School Begins';
$kis_lang['PortfolioData']['EndSchool'] = 'School Ends';
$kis_lang['PortfolioData']['Ages'] = 'Age';
$kis_lang['PortfolioData']['Age']['Year'] = 'Year(s)';
$kis_lang['PortfolioData']['Age']['Month'] = 'Month(s)';
$kis_lang['PortfolioData']['Weight'] = 'Weight';
$kis_lang['PortfolioData']['WeightKg'] = 'kg';
$kis_lang['PortfolioData']['Height'] = 'Height';
$kis_lang['PortfolioData']['HeightCm'] = 'cm';
$kis_lang['PortfolioData']['TermDayCount'] = 'Teaching days';
$kis_lang['PortfolioData']['PresentDayCount'] = 'Present days';
$kis_lang['PortfolioData']['SickLeaveDayCount'] = 'Sick Leave days';
$kis_lang['PortfolioData']['OthersLeaveDayCount'] = 'Others Leave days';
$kis_lang['PortfolioData']['LateDayCount'] = 'Late days';
$kis_lang['PortfolioData']['EarlyLeaveDayCount'] = 'Early Leave days';
$kis_lang['PortfolioData']['Award'] = 'Awards';
$kis_lang['PortfolioData']['Remarks'] = 'Remarks';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '<font color=red>*</font>Class Name';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = '<font color=red>*</font>Class Number';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Start age (Year)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Start age (Month)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Start weight';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Start Height';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'End age (Year)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'End age (Month)';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'End weight';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'End Height';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'School Days';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Present Days';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Sick Leave Days';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Other Leave Days';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Late Days';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Early Leave Days';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Award';
$kis_lang['PortfolioData']['Import']['PortfolioDataField'][] = 'Remarks';
$kis_lang['PortfolioData']['Msg']['Import']['invalidClass'] = 'Class not exists.';
$kis_lang['PortfolioData']['Msg']['Import']['invalidStudent'] = 'Student not exists.';

$kis_lang['Settings']['Description'] = 'Description';
$kis_lang['Settings']['NewGrouplist'] = 'New Group';
$kis_lang['Settings']['NewStudent'] = 'New Students';
$kis_lang['Settings']['NewTeacher'] = 'New Teachers';
$kis_lang['Settings']['SelectedUser'] = 'User(s) Selected';
$kis_lang['Settings']['CtrlMultiSelectMessage'] = '(Hold down the CTRL key while clicking on items to select multiple.)';
?>