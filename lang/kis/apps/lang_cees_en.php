<?php
/*
* using by : 
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/
# UniSchool Activity
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Title'] = 'Inter-School Activity';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Date'] = 'Date';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Event'] = 'Event';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Venue'] = 'Location';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Guest'] = 'Guest / Speaker';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ParticipatedSchool'] = 'Participated School';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['Target'] = 'Target';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['TotalParticipant'] = 'Participant';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ReportStartDate'] = 'Report Start Date';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['ReportEndDate'] = 'Report End Date';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['RetrieveDataFromSystem'] = 'Retrieve Data From System';
$Lang['CEES']['Management']['SchoolActivityReport']['InterSchool']['RetrieveDataRow'] = 'Retrieve Data Row';

# Intra School Activity
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Title'] = '';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Date'] = 'Date';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Event'] = 'Event';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['EventName'] = 'Activity Name';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Venue'] = 'Location';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Guest'] = 'Guest / Speaker';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportStartDate'] = 'Report Start Date';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportEndDate'] = 'Report End Date';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Participant'] = 'Participant';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Award'] = 'Award';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['TotalParticipant'] = 'TotalParticipant';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Name'] = 'Name';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Position'] = 'Job Title';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ParticipatedGroup'] = 'Participated Group';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Organization'] = 'Organized By';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['AwardDate'] = 'Award Date';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportStartDate'] = 'Report Start Date';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['ReportEndDate'] = 'Report End Date';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Class'] = 'Class';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['Target'] = 'Target';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['RetrieveDataFromSystem'] = 'Retrieve Data From System';
$Lang['CEES']['Management']['SchoolActivityReport']['IntraSchool']['RetrieveDataRow'] = 'Retrieve Data Row';

# Monthly Activity Report
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Title'] = 'Monthly Activity Report';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Date'] = 'Date';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['AwardDate'] = 'Award Date';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Event'] = 'Event';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['EventName'] = 'Activity Name';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Venue'] = 'Location';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Guest'] = 'Guest / Speaker';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedSchool'] = 'Participated School';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Participant'] = 'Participant';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['TotalParticipant'] = 'Participant';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Remark'] = 'Remark';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Organization'] = 'Organized By';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ParticipatedGroup'] = 'Participated Group';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Name'] = 'Name';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Position'] = 'Job Title';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Award'] = 'Award';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['ReportDate'] = 'Report Date';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['Target'] = 'Target';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['RetrieveDataFromSystem'] = 'Retrieve Data From System';
$Lang['CEES']['Management']['SchoolActivityReport']['MonthlyActivityReport']['RetrieveDataRow'] = 'Retrieve Data Row';

# For Header
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolActivityReport'] = 'School Activity Report';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section1'] = 'Section 1';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section2'] = 'Section 2';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section3'] = 'Section 3';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Section4'] = 'Section 4';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ManagementAndOrganization'] = 'Management and Organization';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['LearnAndTeach'] = 'Learning and Teaching';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentSupport'] = 'Student Support';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAndStudentPerformance'] = 'School and Student Performance';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ProfessionalDevelopmentAndTrainning'] = 'Professional Development and Training of Teacher';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterflowAndSharing'] = 'Interflow and Sharing';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InsideSchoolActivity'] = 'Intra-School Activity';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OutsideSchoolActivity'] = 'Extracurricular Activity';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentSchoolActivity'] = 'Parent School Activity';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParentEducation'] = 'Parent Education';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['OtherSupportActivity'] = 'Other Support Activity';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Others'] = 'Others';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAward'] = 'School Award';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StaffAward'] = 'Staff Award';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentAward'] = 'Student Award';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['IntraSchool'] = 'Each School';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['InterSchool'] = 'Joint School';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SchoolAdministration'] = 'School Administration';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['StudentArcheivement'] = 'Student Achievement';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_Category'] = 'Category (For ECA Report)';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SDAS_AllowSync'] = 'Allow sync to ECA Report';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Row'] = 'Row';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['CancelSubmit'] = 'Cancel Submission';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Month'] = 'Month';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['NewReport'] = 'New Report';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['SelectReportPeriod'] = 'Select Report Period';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetReport'] = 'Target Report';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TotalParticipant'] = 'Number of Participant';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ActivityTarget'] = 'Event Target';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Guest'] = 'Guest';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['ParticipatedSchool'] = 'Participated School';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['TargetSection'] = 'Target Section';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['MonthlyActivityReport'] = 'Monthly Activity Report';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromeEnrolment'] = "Retrieve Data From eEnrolment";
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['RetrieveDataFromiPortoflio'] = "Retrieve Data From iPortfolio";
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['Sync'] = 'Sync';
$Lang['CEES']['Management']['SchoolActivityReport']['Category']['LastModificatedDate'] = 'Last Modification Date';

# Hint
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['OtherSupportActivity'] = 'e.g.: Admission Adoption, Promotion, Special Needs';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['SchoolAdministration'] = 'e.g.: Ceremony, Conference And Event';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ProfessionalDevelopmentAndTraning'] = 'Tranning Arranged By School';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['IfExists'] = 'If Exists';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['IfAvailable'] = 'If Available';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ConfirmRemoveReport'] = 'Are you sure to remove this report?(This action cannot be undo)';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['ConfirmImport'] = 'Confirm Import';
$Lang['CEES']['Management']['SchoolActivityReport']['Hint']['AlreadySubmitted'] = 'This report has been submitted to Central Site';
$Lang['CEES']['Managemnet']['SchoolActivityReport']['Hint']['SyncEnrolmentAlert'] = 'The sync action will replace the existing record of report, please confirm if continue to process.';


# Msg
$Lang['CEES']['Management']['SchoolActivityReport']['Msg']['Success'] = 'Success';
$Lang['CEES']['Management']['SchoolActivityReport']['Msg']['ImportCount'] = 'Import Row(s)';
$Lang['CEES']['Management']['SchoolActivityReport']['Msg']['ImportEmptyError'] = 'Error: CSV content cannot be empty';

?>