<?
$kis_lang['studentlist'] 		= "學生清單";
$kis_lang['learningprofile']		= "學習檔案";
$kis_lang['schoolrecords'] 		= "學校記錄";
$kis_lang['assessmentreports'] 		= "評估報告";
$kis_lang['schoolbasescheme'] 		= "校本計劃";
$kis_lang['awards'] 			= "獎項";
$kis_lang['activities'] 		= "活動";
$kis_lang['readingrecord'] 		= "閱讀報告";
$kis_lang['assessments'] 		= "評估";
$kis_lang['assessmentlist'] 		= "評估清單";
$kis_lang['englishname']		= "英文名稱";
$kis_lang['chinesename']		= "中文名稱";
$kis_lang['dateofbirth']		= "出生日期";
$kis_lang['placeofbirth']		= "出生地點";
$kis_lang['nationality']		= "國籍";
$kis_lang['homephoneno']		= "住所電話號碼";
$kis_lang['phoneno']			= "電話號碼";
$kis_lang['emergencycontactno']		= "緊急聯絡號碼";
$kis_lang['homeaddress']		= "住址";
$kis_lang['schoolyear']			= "學業年度";
$kis_lang['admissiondate']		= "入學日期";
$kis_lang['relationship']		= "關係";
$kis_lang['guardian']			= "監護人";
$kis_lang['guardianinformation']	= "監護人資料";
$kis_lang['mainguardian']		= "主要監護人";
$kis_lang['basicinformation']		= "基本資料";
$kis_lang['father']			= "父親";
$kis_lang['mother']			= "母親";
$kis_lang['grandfather']		= "祖父";
$kis_lang['grandmother']		= "祖母";
$kis_lang['brother']			= "兄弟";
$kis_lang['sister']			= "姊妹";
$kis_lang['relative']			= "親戚";

?>