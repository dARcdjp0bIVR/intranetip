<?
$kis_lang['month']			= "Month";
$kis_lang['year']			= "Year";
$kis_lang['comingmonths']		= "Coming Months";
$kis_lang['allmonths']			= "All Months";
$kis_lang['publicholiday']		= "Public Holiday";
$kis_lang['schoolholiday']		= "School Holiday";
$kis_lang['sun']			= "S";
$kis_lang['mon']			= "M";
$kis_lang['tue']			= "T";
$kis_lang['wed']			= "W";
$kis_lang['thu']			= "T";
$kis_lang['fri']			= "F";
$kis_lang['sat']			= "S";

?>