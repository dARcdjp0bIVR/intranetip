<?php
// Modifing by
$eLib_plus["html"]["title"] = "Title";
$eLib_plus["html"]["add"] = "Add";
$eLib_plus["html"]["all"] = "All";
$eLib_plus["html"]["order"] = "Order";
$eLib_plus["html"]["book"] = "Book";
$eLib_plus["html"]["ebook"] = "eBook";
$eLib_plus["html"]["advanced"] = "Advanced";
$eLib_plus["html"]["circulations"] = "Circulations";
$eLib_plus["html"]["adminsetting"] = "Admin Setting";
$eLib_plus["html"]["themesetting"] = "Theme Setting";
$eLib_plus["html"]["background"] = "Background";
$eLib_plus["html"]["bookshelf"] = "Bookshelf";
$eLib_plus["html"]["myrecord"] = "My Record";
$eLib_plus["html"]["bookcategory"] = "Book Category";
$eLib_plus["html"]["reports"] = "Reports";
$eLib_plus["html"]["pconly"] = "PC only";
$eLib_plus["html"]["pcipad"] = "PC + iPad";
$eLib_plus["html"]["listallbooks"] = "List all books";
$eLib_plus["html"]["ranking"] = "Ranking";
$eLib_plus["html"]["calender"] = "Calendar";
$eLib_plus["html"]["announcement"] = "Announcement";
$eLib_plus["html"]["rules"] = "Rules";
$eLib_plus["html"]["staff"] = "Duty Staff";
$eLib_plus["html"]["news"] = "News";
$eLib_plus["html"]["more"] = "more";
$eLib_plus["html"]["manage"] = "Manage";
$eLib_plus["html"]["recommend"] = "Recommended";
$eLib_plus["html"]["new"] = "New";
$eLib_plus["html"]["readnow"] = "Read Now";
$eLib_plus["html"]["bookdetail"] = "Book Detail";
$eLib_plus["html"]["youve"] = "You've";
$eLib_plus["html"]["thisweek"] = "This Week";
$eLib_plus["html"]["thismonth"] = "This Month";
$eLib_plus["html"]["thisyear"] = "This Year";
$eLib_plus["html"]["accumulated"] = "Accumulated";
$eLib_plus["html"]["times"] = "times";
$eLib_plus["html"]["reviews"] = "reviews";
$eLib_plus["html"]["location"] = "Location";
$eLib_plus["html"]["status"] = "Status";
$eLib_plus["html"]["hits"] = "Hits";
$eLib_plus["html"]["hitsofreading"] = "hits of reading";
$eLib_plus["html"]["addtofavourite"] = "Add to Favourite";
$eLib_plus["html"]["removefavourite"] = "Remove Favourite";
$eLib_plus["html"]["requesthold"] = "Reserve";
$eLib_plus["html"]["cancelhold"] = "Cancel Reserve";
$eLib_plus["html"]["cannothold"] = "You cannot reserve any more books of this type!";
$eLib_plus["html"]["cannothold_borrowed"] = "You cannot reserve this book because it is now being loaned by you.";
$eLib_plus["html"]["cannothold_over_quota"] = "You have no quota to reserve this book.";
$eLib_plus["html"]["cannothold_book_disable"] = "The book is not allowed to reserve!";
$eLib_plus["html"]["cannothold_book_available"] = "You cannot reserve this book because it is available for loan now.";
$eLib_plus["html"]["writereview"] = "Write Review";
$eLib_plus["html"]["yourreview"] = "Your Review";
$eLib_plus["html"]["unlike"] = "unlike";
$eLib_plus["html"]["like"] = "like!";
$eLib_plus["html"]["likes"] = "Likes";
$eLib_plus["html"]["lessthan1minago"] = 'less than 1 minute ago';
$eLib_plus["html"]["minsago"] = 'minutes ago';
$eLib_plus["html"]["hoursago"] = 'hours ago';
$eLib_plus["html"]["today"] = 'today';
$eLib_plus["html"]["yesterday"] = 'yesterday';
$eLib_plus["html"]["daysago"] = "days ago";
$eLib_plus["html"]["available"] = "Available";
$eLib_plus["html"]["notavailable"] = "Not available";
$eLib_plus["html"]["allbookcategories"] = "All Book Categories";
$eLib_plus["html"]["viewby"] = "View by";
$eLib_plus["html"]["booklist"] = "Book List";
$eLib_plus["html"]["bookcover"] = "Book Cover";
$eLib_plus["html"]["showmore"] = "Show more";
$eLib_plus["html"]["nomorebooks"] = "No More Books";
$eLib_plus["html"]["eng"] = "English eBooks";
$eLib_plus["html"]["chi"] = "Chinese eBooks";
$eLib_plus["html"]["others"] = "Physical Books";
$eLib_plus["html"]["tags_all_books"] = "Tags (All Books)";
$eLib_plus["html"]["mostloanbook"] = "Most Loan Book";
$eLib_plus["html"]["mosthitbook"] = "Most Hit Book";
$eLib_plus["html"]["bestreview"] = "Best Review";
$eLib_plus["html"]["mostactivereviewers"] = "Most Active Reviewers";
$eLib_plus["html"]["mostactiveborrowers"] = "Most Active Borrowers";
$eLib_plus["html"]["mosthelpfulreview"] = "Most Helpful Reviews";
$eLib_plus["html"]["edition"] = "Edition";
$eLib_plus["html"]["noofcopyavailable"]  = "No. of Copy Available";
$eLib_plus["html"]["noofcopyavailable_short"]  = "Available";
$eLib_plus["html"]["noofcopyborrowed_short"]  = "Borrowed";
$eLib_plus["html"]["noofcopyreserved_short"]  = "Reserved";
$eLib_plus["html"]["tags"] = "Tags";
$eLib_plus["html"]["tag"] = "Tag";
$eLib_plus["html"]["noreviews"] = "No Reviews";
$eLib_plus["html"]["hitrate"] = "Hit Rate";
$eLib_plus["html"]["currentstatus"] = "Current Status";
$eLib_plus["html"]["loanrecord"] = "Loan Record";
$eLib_plus["html"]["penaltyrecord"] = "Penalty Record";
$eLib_plus["html"]["lostrecord"] = "Lost Record";	//20140509-lost-book-list
$eLib_plus["html"]["quota"] = "Quota";
$eLib_plus["html"]["checkedout"] = "Checked Out";
$eLib_plus["html"]["checkedoutdate"] = "Checked Out Date";
$eLib_plus["html"]["duedate"] = "Due Date";
$eLib_plus["html"]["overdue"] = "Overdue";
$eLib_plus["html"]["renew"] = "Renew";
$eLib_plus["html"]["days"] = "days";
$eLib_plus["html"]["requesthold"] = "Reserve";
$eLib_plus["html"]["requesteddate"] = "Requested Date";
$eLib_plus["html"]["readyforpickup"] = "Ready for pick up";
$eLib_plus["html"]["notreturnedyet"] = "Not returned yet";
$eLib_plus["html"]["myloanbookrecord"] = "Loan Book Record";
$eLib_plus["html"]["ebookrecord"] = "eBook Record";
$eLib_plus["html"]["myfavourite"] = "My Favourite";
$eLib_plus["html"]["myreview"] = "My Review";
$eLib_plus["html"]["reviewer"] = "Reviewer";
$eLib_plus["html"]["norecord"] = "No Record";
$eLib_plus["html"]["borrowed"] = "Borrowed";
$eLib_plus["html"]["borrowed2"] = "borrowed";
$eLib_plus["html"]["reserved"] = "Reserved";
$eLib_plus["html"]["returndate"] = "Return Date";
$eLib_plus["html"]["reason"] = "Reason";
$eLib_plus["html"]["totalpenalty"] = "Total Penalty";
$eLib_plus["html"]["outstandingpenalty"] = "Outstanding Penalty";
$eLib_plus["html"]["bestrate"] = "Best Rate";
$eLib_plus["html"]["mosthit"] = "Most Hit";
$eLib_plus["html"]["mostloan"] = "Most Loan";
$eLib_plus["html"]["borrow"] = "Borrow";
$eLib_plus["html"]["books_unit"] = "books";
$eLib_plus["html"]["borrows_unit"] = "items";
$eLib_plus["html"]["lastreview"] = "Last Review";
$eLib_plus["html"]["noofbooksborrowed"] = "No. of Books Borrowed";
$eLib_plus["html"]["areusuretocancelthereservation"] = "Are you sure to cancel the reservation?";
$eLib_plus["html"]["areusuretorenewthebook"] = "Are you sure to renew the book?";
$eLib_plus["html"]["areusuretoremovethereview"] = "Are you sure to remove the review?";
$eLib_plus["html"]["failedtorenew"] = "Renew failed";
$eLib_plus["html"]["renewcount"] = "Renew Count";
$eLib_plus["html"]["booklost"] = "Book Lost";
$eLib_plus["html"]["openinghours"] = "Opening Hours";
$eLib_plus["html"]["closed"] = "Closed";
$eLib_plus["html"]["closing"] = "Closed";
$eLib_plus["html"]["opening"] = "Opened";
$eLib_plus["html"]["specialtime"] = "Special Time";
$eLib_plus["html"]["weekday"]['Monday'] = "Mon";
$eLib_plus["html"]["weekday"]['Tuesday'] = "Tue";
$eLib_plus["html"]["weekday"]['Wednesday'] = "Wed";
$eLib_plus["html"]["weekday"]['Thursday'] = "Thu";
$eLib_plus["html"]["weekday"]['Friday'] = "Fri";
$eLib_plus["html"]["weekday"]['Saturday'] = "Sat";
$eLib_plus["html"]["weekday"]['Sunday'] = "Sun";
$eLib_plus["html"]["anonymous"] = "Anonymous";
$eLib_plus["html"]["recommendbookto"] = "Recommend book to";
$eLib_plus["html"]["content"] = "Content";
$eLib_plus["html"]["attachment"] = "Attachment";
$eLib_plus["html"]["attachfile"] = "Attach file";
$eLib_plus["html"]["attachedfile"] = "Attached file";
$eLib_plus["html"]["replaceattachedfile"] = "Replace attached file?";
$eLib_plus["html"]["areusuretoremovetherecommendation"] = "Are you sure to remove the recommendation?";
$eLib_plus["html"]["pleaseentercontent"] = 'Please enter content';
$eLib_plus["html"]["pleasechooseclasslevels"] = 'Please choose class levels';
$eLib_plus["html"]["librarywillopenedon"] = 'Library will be opened on %s';
$eLib_plus["html"]["libraryclosedsince"] = 'Library is closed since %s';
$eLib_plus["html"]["export"]["review_statistics"] = "Export Reviews Statistics";
$eLib_plus["html"]["export"]["reviews"] = "Export Reviews";
$eLib_plus["html"]["export"]["Title"] = "Title";
$eLib_plus["html"]["export"]["Identity"] = "Identity";
$eLib_plus["html"]["export"]["UserLogin"] = "UserLogin";
$eLib_plus["html"]["export"]["EnglishName"] = "EnglishName";
$eLib_plus["html"]["export"]["ChineseName"] = "ChineseName";
$eLib_plus["html"]["export"]["ClassName"] = "ClassName";
$eLib_plus["html"]["export"]["ClassNumber"] = "ClassNumber";
$eLib_plus["html"]["export"]["Rating"] = "Rating";
$eLib_plus["html"]["export"]["Content"] = "Content";
$eLib_plus["html"]["export"]["DateModified"] = "DateModified";
$eLib_plus["html"]["expiredBook"]["onCoverRemark"] = "Discontinued";


$eLib["eLibrary"] = "<a href=\"/home/eLearning/elibrary/\" style=\"text-decoration:none\" class=\"title\">eLibrary</a>";
$eLib["html"]["home"] = "Home";
$eLib["html"]["elibrary_settings"] = "eLibrary  Settings";
$eLib["html"]["portal_display_settings"] = "Portal Page Setup";

$eLib["html"]["book_name"] = "Book Name";
$eLib["html"]["book_cover"] = "Book Cover";

$eLib["html"]["records"] = "Records";
$eLib["html"]["display_top"] = "Display : Top";
$eLib["html"]["catalogue"] = "Catalogue";
$eLib["html"]["books"] = "Books";
$eLib["html"]["display"] = "Display : ";
$eLib["html"]["display2"] = "Display";
$eLib["html"]["page"] = "Page";
$eLib["html"]["view"] = "View";

$eLib["html"]["most_active_reviewers"] = "Most active Reviewers";
$eLib["html"]["most_useful_reviews"] = "Most helpful Reviews";
$eLib["html"]["list_all"] = "List All";
$eLib["html"]["last_week"] = "Last Week";
$eLib["html"]["accumulated"] = "Accumulated";

$eLib["html"]["recommended_books"] = "School recommended Books";
$eLib["html"]["bookS_with_highest_hit_rate"] = "Books with highest Hit Rate";
$eLib["html"]["recommended_reason"] = "Recommended Reason";

$eLib["html"]["save"] = "Save";
$eLib["html"]["submit"] = "Submit";
$eLib["html"]["reset"] = "Reset";
$eLib["html"]["cancel"] = "Cancel";
$eLib["html"]["confirm"] = "Confirm";
$eLib["html"]["Close"] = "Close";
$eLib["html"]["Add"] = "Add>>";
$eLib["html"]["Remove"] = "<<Remove";
$eLib["html"]["Delete"] = "Delete";
$eLib["html"]["Success"] = "Success";
$eLib["html"]["Fail"] = "Fail";
$eLib["html"]["Cancel_License"] = "Cancel License";

$eLib["html"]["settings"] = "Settings";

$eLib["html"]["personal"] = "Personal";
$eLib["html"]["my_reading_history"] = "My Reading Record";
$eLib["html"]["my_books"] = "My Books";
$eLib["html"]["my_favourites"] = "My Favourites";
$eLib["html"]["my_reviews"] = "My Reviews";
$eLib["html"]["my_notes"] = "My Notes";
$eLib["html"]["public"] = "Public";
$eLib["html"]["all_reviews"] = "All Reviews";
$eLib["html"]["all_reviews_2"] = "All Reviews";
$eLib["html"]["student_summary"] = "Reading & Review figures of all students";
$eLib["html"]["chinese"] = "Chinese";
$eLib["html"]["english"] = "English";
$eLib["html"]["show_all"] = "Show All";

$eLib["html"]["advance_search"] = "Advanced Search";
$eLib["html"]["search_result"] = "Search Result";

$eLib["html"]["title"] = "Title";
$eLib["html"]["subtitle"] = "Sub Title";
$eLib["html"]["author"] = "Author";
$eLib["html"]["source"] = "Source";
$eLib["html"]["category"] = "Category";
$eLib["html"]["level"] = "Level in the Series";
$eLib["html"]["date"] = "Date";
$eLib["html"]["book_input_date"] = "Book input date";
$eLib["html"]["last_added"] = "Last modified";
$eLib["html"]["with_worksheets"] = "With Worksheets";
$eLib["html"]["sort_by"] = "Sort by";
$eLib['Book']["Code"] = "Code";
$eLib["html"]["Average"] = "Average";

$eLib["html"]["book_title"] = "Book Title";
$eLib["html"]["publisher"] = "Publisher";
$eLib["html"]["publish_year"] = "Year of Publishing";
$eLib["html"]["publish_place"] = "Publication Place";

$eLib["html"]["all_category"] = "All Categories";
$eLib["html"]["all_level"] = "All Levels in Series";

$eLib["html"]["report"] = "Report";
$eLib["html"]["record"] = "Records";
$eLib["html"]["total"] = "Total";

$eLib["html"]["keywords"] = "Keywords";
$eLib["html"]["no_record"] = "No record is found !";
$eLib["html"]["description"] = "Description";
$eLib["html"]["rating"] = "Rating";

$eLib["html"]["click_to_read"] = "Click to Read";
$eLib["html"]["reviews"] = "Reviews";
$eLib["html"]["reviewsUnit"] = "";
$eLib["html"]["add_to_my_favourite"] = "Add to My Favourite";
$eLib["html"]["remove_my_favourite"] = "Remove from My Favourite";

$eLib["html"]["recommend_this_book"] = "Recommend this Book";
$eLib["html"]["add_new_review"] = "Add New Review";

$eLib["html"]["yes"] = "Yes";
$eLib["html"]["no"] = "No";
$eLib["html"]["was_this_review_helpful"] = "Was this review helpful to you?";
$eLib["html"]["of"] = "of";
$eLib["html"]["people_found_this_review_helpful"] = "people found this Review helpful.";

$eLib["html"]["review_content"] = "Review Content";

$eLib["html"]["personal_records"] = "Personal Records";

$eLib["html"]["last_read"] = "Last accessed";
$eLib["html"]["last_modified"] = "Last modified";
$eLib["html"]["remove"] = "Remove";

$eLib["html"]["confirm_remove_msg"] = "Are you sure you want to remove the record?";
$eLib["html"]["notes_content"] = "Notes Content";

$eLib["html"]["all_books"] = "All Books";
$eLib["html"]["all_categories"] = "All Categories";

$eLib["html"]["num_of"] = "No. of";

$eLib["html"]["num_of_review"] = "No. of Reviews";
$eLib["html"]["last_review_date"] = "Last Review date";
$eLib["html"]["last_submitted"] = "Last submitted";

$eLib["html"]["student_name"] = "Student Name";
$eLib["html"]["num_of_book_read"] = "No. of accessed Books";
$eLib["html"]["num_of_book_hit"] = "Total hits";

$eLib["html"]["name"] = "Name";

$eLib["html"]["class"] = "Class";
$eLib["html"]["class_number"] = "Class Number";
$eLib["html"]["all_class"] = "All Classes";
$eLib["html"]["student"] = "Student";
$eLib["html"]["class_name"] = "Class Name";

$eLib["html"]["book_read"] = "Book Read";
$eLib["html"]["review"] = "Review";

$eLib["html"]["book_with_hit_rate_accumulated"] = "Accumulated highest";
$eLib["html"]["book_with_hit_rate_last_week"] = "Highest in Last Week";

$eLib["html"]["whole_school"] = "Whole School";

$eLib["html"]["recommend_to"] = "Recommended as &quot;School Recommended Books&quot; to :";
$eLib["html"]["reason_to_recommend"] = "Reason(s) for recommending this Book";

$eLib["html"]["language"] = "Language";
$eLib["html"]["subcategory"] = "Sub-category";
$eLib["html"]["english_books"] = "English Books";
$eLib["html"]["chinese_books"] = "Chinese Books";

$eLib["html"]["all_english_books"] = "All English Books";
$eLib["html"]["all_chinese_books"] = "All Chinese Books";

$eLib["html"]["all_english_categories"] = "All English Categories";
$eLib["html"]["all_chinese_categories"] = "All Chinese Categories";
$eLib["html"]["search_input"] = "Search for..";
$eLib["html"]["search"] = "Search";
$eLib["html"]["please_enter_keywords"] = "Please enter keywords";
$eLib["html"]["publish"] = "Publish";
$eLib["html"]["unpublish"] = "Unpublish";

$eLib["html"]["please_enter_title"] = "Please enter Title !";
$eLib["html"]["please_enter_author"] = "Please enter Author !";
$eLib["html"]["please_upload_csvfile"] = "Please upload CSV File !";

$eLib["html"]["with_books_cover"] = "With Books' Cover";
$eLib["html"]["table_list"] = "Table List";

$eLib["html"]["no_recommend_book"] = "No books recommended.";

$eLib['ManageBook']["ImportZIPDescribeHeading"] = "The ZIP file should contain the following files:";

$eLib['ManageBook']["ImportZIPDescribeChinese"] = "
				Chinese book:<br />
				<b>content.text</b> - the text file to be converted info XML. <br />
				<b>Folder \"image\"</b> - the folder contains the images that will be used the the book. <br />
				<b>control.text</b> - the text file to control the conversion process. <br />
													";

$eLib['ManageBook']["ImportZIPDescribeEnglish"] = "
				English book:<br />
				<b>content.xml</b> - the xml file to be converted info XML. <br />
				<b>Folder \"image\"</b> - the folder contains the images that will be used the the book. <br />
													";
//ebook by Josephine
$eLib["html"]["recommended_books_2"] = "Recommended Book";
$eLib["html"]["num_of_students"] = "No.of Students";
$eLib["html"]["num_of_completed_books"] = "No.of Completed Books";
$eLib["html"]['Total'] = "Total";
$eLib["html"]['Progress'] = "Progress";
$eLib["html"]['ReadTimes'] = "Read Times";
$eLib["html"]['AddToFavourite']	= "Add this book to My Favourite";
$eLib["html"]['AddBook'] = "Add Book";
$eLib["html"]['MyFavourite'] = "My Favourite";
$eLib["html"]['MyRecommendation'] = "My Recommendation";

$eLib["html"]["class_summary"] = "Reading & Review figures of all classes";
$eLib["html"]["Books_Read"]	= "Reading History";
$eLib["html"]["Incorrect_Date"]	= "The period you entered is invalid. Please make sure the end date is later then the start date";


//admin book license
$eLib['admin']['Enable'] = "Enable";
$eLib['admin']['Enabled_Quota'] = "Enabled Quota";
$eLib['license']['ManageBookLicense'] = "License Authorization";

$eLib['admin']['Assigned_Quota'] = "Used Quota";
$eLib['admin']['Available_Quota'] = "Remaining/Total Quota";
$eLib['admin']['Quota_Left'] = "Remaining Quota";
$eLib['admin']['Assign_Student'] = "License Authorization";
$eLib['admin']['View_Assign_Student'] = "View/Edit Licensed Students";

$eLib['admin']['Confirm_Assign_Student'] = "Are you sure you want to authorize selected student(s) for license?";
$eLib['admin']['Confirm_Remove_Assign_Student'] = "Are you sure you want to cancel license for selected student(s)?";
$eLib['admin']['Delete_Assign_Student_Instruction'] = "Note: Only students licensed within the last 48 hours can be cancelled.";
$eLib['admin']['SelectedStudent'] = "Student Selected";
$eLib['admin']['Students_Licensed'] = "Students Licensed";
$eLib['admin']['Licensed_Students'] = "Licensed Student";
$eLib['SystemMsg']['NoMoreAvailableQuota'] = "No more available quota.";
$eLib['SystemMsg']['AccessDenied'] = "Warning: Access denied.";
$eLib['SystemMsg']['Err_NoReadRight'] = "You do not have the right to read this book.";
$eLib['SystemMsg']['Err_NoStudentsSelected'] = "Please select one or more students.";
$elib['SystemMsg']['Err_ExistInvalidQuota'] = "<font color='red'>Quota is invalidated due to unknown modifications</font>";

$eLib['SystemMsg']['NoMyBook'] = "You do not have any books";

$eLib["html"]["Average"] = "Average";
$eLib["html"]["Period"] = "Period";
$eLib["html"]["Apply"] = "Apply";

# eLibrary
$i_eLibrary_System = "eLibrary";
$i_eLibrary_System_Admin_User_Setting = "Set Admin User";
$i_eLibrary_System_Current_Batch = "Current Batch";
$i_eLibrary_System_Update_Batch = "Update Batch";
$i_eLibrary_System_Updated_Batch = "Updated Batch";
$i_eLibrary_System_Reset_Fail = "Reset Book Status Failed.";
$i_eLibrary_System_Updated_Records = "Updated Records";
$eLib["AdminUser"] = "Admin User";

$eLib['ManageBook']["Title"] = "Book Management";
$eLib['ManageBook']["ConvertBook"] = "Convert Book";
$eLib['ManageBook']["ImportView"] = "Import Content";
$eLib['ManageBook']["ContentManage"] = "Manage Content";
$eLib['ManageBook']["ContentView"] = "View Content";
$eLib['ManageBook']["ImportBook"] = "Import Book List";
$eLib['ManageBook']["ImportCSV"] = "Import CSV File";
$eLib['ManageBook']["ImportXML"] = "Import XML File";
$eLib['ManageBook']["ImportZIP"] = "Import ZIP File";
$eLib['ManageBook']["ImportZIPDescribe"] = "
				The ZIP file should contain the following files: <br />
				content.text - the text file to be converted info XML. <br />
				Folder \"image\" - the folder contains the images that will be used the the book. <br />
				control.text - the text file to control the conversion process. <br />
													";

$eLib['Book']["Author"] = "Author";
$eLib['Book']["SeriesEditor"] = "Series Editor";
$eLib['Book']["Category"] = "Category";
$eLib['Book']["Subcategory"] = "Subcategory";
$eLib['Book']["DateModified"] = "Date Modified";
$eLib['Book']["Description"] = "Description";
$eLib['Book']["Language"] = "Language";
$eLib['Book']["Level"] = "Level";
$eLib['Book']["AdultContent"] = "Sensitive Content";
$eLib['Book']["Title"] = "Title";
$eLib['Book']["Publisher"] = "Publisher";
$eLib['Book']["TableOfContent"] = "Table of Content";
$eLib['Book']["Copyright"] = "Copyright";

$eLib["SourceFrom"] = "Source From";
$eLib['Source']["green"] = "Green Apple";
$eLib['Source']["cup"] = "Cambridge University Press";

$eLib['EditBook']["IsChapterStart"] = "Chapter Start?";
$eLib['EditBook']["ChapterID"] = "Chapter ID";

$i_junior_leftmenu_hide="H<br/>I<br/>D<br/>E";
$button_submit = "Submit";
$button_cancel = "Cancel";
$i_QB_LangSelectEnglish = "English";
$i_QB_LangSelectChinese = "Chinese";
$button_upload = "Upload";
$i_QB_LangSelect = "Language";
$i_status_all = "All";
$button_edit = "Edit";
$button_remove = "Remove";
$button_new = "New";
$list_total = "Total";
$list_page = "Page";
$i_general_EachDisplay = "Display";
$i_general_PerPage = "/Page";
$button_previous_page = "Previous Page";
$button_next_page = "Next Page";



$eLib["html"]["home"] = "Home";
$eLib["html"]["elibrary_settings"] = "eBook Settings";
$eLib["html"]["portal_display_settings"] = "Portal Page Setup";

$eLib["html"]["book_name"] = "Book Name";
$eLib["html"]["book_cover"] = "Book Cover";

$eLib["html"]["records"] = "Records";
$eLib["html"]["display_top"] = "Display : Top";
$eLib["html"]["catalogue"] = "Catalogue";
$eLib["html"]["books"] = "Books";
$eLib["html"]["display"] = "Display : ";
$eLib["html"]["display2"] = "Display";
$eLib["html"]["page"] = "Page";
$eLib["html"]["view"] = "View";

$eLib["html"]["most_active_reviewers"] = "Most Active Reviewers";
$eLib["html"]["most_useful_reviews"] = "Most Helpful Reviews";
$eLib["html"]["list_all"] = "List All";
$eLib["html"]["last_week"] = "Last Week";
$eLib["html"]["accumulated"] = "Accumulated";

$eLib["html"]["recommended_books"] = "School Recommended Books";
$eLib["html"]["bookS_with_highest_hit_rate"] = "Books with Highest Hit Rate";
$eLib["html"]["recommended_reason"] = "Recommended Reason";

$eLib["html"]["save"] = "Save";
$eLib["html"]["submit"] = "Submit";
$eLib["html"]["reset"] = "Reset";
$eLib["html"]["cancel"] = "Cancel";
$eLib["html"]["confirm"] = "Confirm";
$eLib["html"]["Close"] = "Close";
$eLib["html"]["Add"] = "Add>>";
$eLib["html"]["Remove"] = "<<Remove";
$eLib["html"]["Delete"] = "Delete";
$eLib["html"]["Success"] = "Success";
$eLib["html"]["Fail"] = "Fail";
$eLib["html"]["Cancel_License"] = "Cancel License";

$eLib["html"]["settings"] = "Settings";

$eLib["html"]["personal"] = "Personal";
$eLib["html"]["my_reading_history"] = "My Reading Record";
$eLib["html"]["my_books"] = "My Books";
$eLib["html"]["my_favourites"] = "My Favourites";
$eLib["html"]["my_reviews"] = "My Reviews";
$eLib["html"]["my_notes"] = "My Notes";
$eLib["html"]["public"] = "Public";
$eLib["html"]["all_reviews"] = "All Reviews";
$eLib["html"]["all_reviews_2"] = "All Reviews";
$eLib["html"]["student_summary"] = "Reading & Review figures of all students";
$eLib["html"]["chinese"] = "Chinese";
$eLib["html"]["english"] = "English";
$eLib["html"]["show_all"] = "Show All";

$eLib["html"]["advance_search"] = "Advanced Search";
$eLib["html"]["search_result"] = "Search Result";

$eLib["html"]["title"] = "Title";
$eLib["html"]["author"] = "Author";
$eLib["html"]["source"] = "Source";
$eLib["html"]["category"] = "Category";
$eLib["html"]["level"] = "Level in the Series";
$eLib["html"]["date"] = "Date";
$eLib["html"]["book_input_date"] = "Book input date";
$eLib["html"]["last_added"] = "Last modified";
$eLib["html"]["with_worksheets"] = "With Worksheets";
$eLib["html"]["sort_by"] = "Sort by";
$eLib['Book']["Code"] = "Code";
$eLib['Book']["ePUB"] = "ePUB / zip File ";
$eLib['Book']["ePUBIsImageBook"] = "Is ePUB an image book";
$eLib['Book']["ePUBImageWidth"] = "ePUB image width";
$eLib['Book']["ePUBImageWidthUnit"] = "px";
$eLib['Book']["Format"] = "Format";
$eLib["html"]["Average"] = "Average";

$eLib["html"]["book_title"] = "Book Title";
$eLib["html"]["publisher"] = "Publisher";

$eLib["html"]["all_category"] = "All Categories";
$eLib["html"]["all_level"] = "All Levels in Series";

$eLib["html"]["report"] = "Report";
$eLib["html"]["record"] = "Records";
$eLib["html"]["total"] = "Total";

$eLib["html"]["keywords"] = "Keywords";
$eLib["html"]["no_record"] = "No record is found !";
$eLib["html"]["description"] = "Description";
$eLib["html"]["rating"] = "Rating";

$eLib["html"]["click_to_read"] = "Click to Read";
$eLib["html"]["reviews"] = "Reviews";
$eLib["html"]["reviewsUnit"] = "";
$eLib["html"]["add_to_my_favourite"] = "Add to My Favourite";
$eLib["html"]["remove_my_favourite"] = "Remove from My Favourite";

$eLib["html"]["recommend_this_book"] = "Recommend this Book";
$eLib["html"]["add_new_review"] = "Add New Review";

$eLib["html"]["yes"] = "Yes";
$eLib["html"]["no"] = "No";
$eLib["html"]["was_this_review_helpful"] = "Was this review helpful to you?";
$eLib["html"]["of"] = "of";
$eLib["html"]["people_found_this_review_helpful"] = "people found this Review helpful.";

$eLib["html"]["review_content"] = "Review Content";

$eLib["html"]["personal_records"] = "Personal Records";

$eLib["html"]["last_read"] = "Last Accessed";
$eLib["html"]["last_modified"] = "Last Modified";
$eLib["html"]["remove"] = "Remove";

$eLib["html"]["confirm_remove_msg"] = "Are you sure you want to remove the record?";
$eLib["html"]["notes_content"] = "Notes Content";

$eLib["html"]["all_books"] = "All Books";
$eLib["html"]["all_categories"] = "All Categories";

$eLib["html"]["num_of"] = "No. of";

$eLib["html"]["num_of_review"] = "No. of Reviews";
$eLib["html"]["last_review_date"] = "Last Review date";
$eLib["html"]["last_submitted"] = "Last submitted";

$eLib["html"]["student_name"] = "Student Name";
$eLib["html"]["num_of_book_read"] = "No. of accessed Books";

$eLib["html"]["name"] = "Name";

$eLib["html"]["class"] = "Class";
$eLib["html"]["class_number"] = "Class Number";
$eLib["html"]["all_class"] = "All Classes";
$eLib["html"]["student"] = "Student";
$eLib["html"]["class_name"] = "Class Name";

$eLib["html"]["book_read"] = "Book Read";
$eLib["html"]["review"] = "Review";

$eLib["html"]["book_with_hit_rate_accumulated"] = "Accumulated Highest";
$eLib["html"]["book_with_hit_rate_last_week"] = "Highest in Last Week";

$eLib["html"]["whole_school"] = "Whole School";

$eLib["html"]["recommend_to"] = "Recommended as &quot;School Recommended Books&quot; to :";
$eLib["html"]["reason_to_recommend"] = "Reason(s) for recommending this Book";

$eLib["html"]["language"] = "Language";
$eLib["html"]["subcategory"] = "Sub-category";
$eLib["html"]["english_books"] = "English Books";
$eLib["html"]["chinese_books"] = "Chinese Books";

$eLib["html"]["all_english_books"] = "All English Books";
$eLib["html"]["all_chinese_books"] = "All Chinese Books";

$eLib["html"]["all_english_categories"] = "All English Categories";
$eLib["html"]["all_chinese_categories"] = "All Chinese Categories";
$eLib["html"]["search_input"] = "Search for ...";
$eLib["html"]["search"] = "Search";
$eLib["html"]["please_enter_keywords"] = "Please enter keywords";
$eLib["html"]["publish"] = "Publish";
$eLib["html"]["unpublish"] = "Unpublish";

$eLib["html"]["please_enter_title"] = "Please enter Title !";
$eLib["html"]["please_enter_author"] = "Please enter Author !";

$eLib["html"]["with_books_cover"] = "With Books' Cover";
$eLib["html"]["table_list"] = "Table List";

$eLib["html"]["no_recommend_book"] = "No books recommended.";

$eLib['ManageBook']["ImportZIPDescribeHeading"] = "The ZIP file should contain the following files:";

$eLib['ManageBook']["ImportZIPDescribeChinese"] = "
				Chinese book:<br />
				<b>content.text</b> - the text file to be converted info XML. <br />
				<b>Folder \"image\"</b> - the folder contains the images that will be used the the book. <br />
				<b>control.text</b> - the text file to control the conversion process. <br />
													";

$eLib['ManageBook']["ImportZIPDescribeEnglish"] = "
				English book:<br />
				<b>content.xml</b> - the xml file to be converted info XML. <br />
				<b>Folder \"image\"</b> - the folder contains the images that will be used the the book. <br />
													";
//ebook by Josephine
$eLib["html"]["recommended_books_2"] = "Recommended Book";
$eLib["html"]["num_of_students"] = "No.of Students";
$eLib["html"]["num_of_completed_books"] = "No.of Completed Books";
$eLib["html"]['Total'] = "Total";
$eLib["html"]['Progress'] = "Progress";
$eLib["html"]['ReadTimes'] = "Read Times";
$eLib["html"]['AddToFavourite']	= "Add this book to My Favourite";
$eLib["html"]['AddBook'] = "Add Book";
$eLib["html"]['MyFavourite'] = "My Favourite";
$eLib["html"]['MyFavouriteTime'] = "Added on";
$eLib["html"]['MyRecommendation'] = "My Recommendation";

$eLib["html"]["class_summary"] = "Reading & Review figures of all classes";
$eLib["html"]["Books_Read"]	= "Reading History";
$eLib["html"]["Incorrect_Date"]	= "The period you entered is invalid. Please make sure the end date is later then the start date";


//admin book license
$eLib['admin']['Enable'] = "Enable";
$eLib['admin']['Enabled_Quota'] = "Enabled Quota";
$eLib['license']['ManageBookLicense'] = "License Authorization";

$eLib['admin']['Assigned_Quota'] = "Used Quota";
$eLib['admin']['Available_Quota'] = "Remaining/Total Quota";
$eLib['admin']['Quota_Left'] = "Remaining Quota";
$eLib['admin']['Assign_Student'] = "License Authorization";
$eLib['admin']['View_Assign_Student'] = "View/Edit Licensed Students";

$eLib['admin']['Confirm_Assign_Student'] = "Are you sure you want to authorize selected student(s) for license?";
$eLib['admin']['Confirm_Remove_Assign_Student'] = "Are you sure you want to cancel license for selected student(s)?";
$eLib['admin']['Delete_Assign_Student_Instruction'] = "Note: Only students licensed within the last 48 hours can be cancelled.";
$eLib['admin']['SelectedStudent'] = "Student Selected";
$eLib['admin']['Students_Licensed'] = "Students Licensed";
$eLib['admin']['Licensed_Students'] = "Licensed Student";
$eLib['SystemMsg']['NoMoreAvailableQuota'] = "No more available quota.";
$eLib['SystemMsg']['AccessDenied'] = "Warning: Access denied.";
$eLib['SystemMsg']['Err_NoReadRight'] = "You do not have the right to read this book.";
$eLib['SystemMsg']['Err_NoStudentsSelected'] = "Please select one or more students.";
$elib['SystemMsg']['Err_ExistInvalidQuota'] = "<font color='red'>Quota is invalidated due to unknown modifications</font>";

$eLib['SystemMsg']['NoMyBook'] = "You do not have any books";

$eLib['Source']["-"] = "-";
$eLib['Source']["breakthrough"] = "突破";
$eLib['Source']["hkcrown"] = "皇冠";
$eLib['Source']["enrichculture"] = "天窗";
$eLib['Source']["rightman"] = "正文社";
$eLib['Source']['witman'] = "Witman";

$Lang['SysMgr']['RemoveReview'] = "Are you sure you want to delete this book review?";
$Lang['SysMgr']['WarnEmptyReview'] = "Please type your review content!";

$eLib["html"]['hits_accumulated'] = "<HITS> hits of reading";
$eLib["html"]['hits'] = "Hits";


$eLib["action"]['open_book'] = "read now";
$eLib["action"]['view_details'] = "view book information & review";
$eLib["action"]['next_book'] = "Next";
$eLib["action"]['previous_book'] = "Previous";

$eLib["html"]['ISBN'] = "ISBN";
$eLib["html"]['NoOfPage'] = "Number of pages";
$eLib["html"]['RelevantSubject'] = "Relevant subject";
$eLib["html"]['Subject'] = "Subject";

$Lang['Btn']['Preview'] = "Preview";

$Lang["Btn"]['Save'] = "Save";
$i_From = "from";
$i_To = "to";
$ip20TopMenu['eLibrary'] = $eLib["eLibrary"];
$Lang['StaffAttendance']['InvalidDateRange'] = "Invalid Date Range";

//----------------  2012-09-12 (CharlesMa) ------------------------
$eLib['Book']["forceToOnePageMode"] = "Force to one page mode?";
$eLib['Book']["onePageModeWidth"] = "One Page mode: Width";
$eLib['Book']["onePageModeHeight"] = "One Page mode: Height";
$eLib['Book']["onePageModeCover"] = "Book Cover";

//----------------  2013-05-30 (CharlesMa) ------------------------
$eLib['Book']["FirstPublished"] = "First Published";
$eLib['Book']["ePublisher"] = "ePublisher";
$eLib['Book']["CopyrightYear"] = "Copyright Year";
$eLib['Book']["CopyrightStatement"] = "Copyright Statement";
$eLib['Book']["IndexPageImage"] = "Index Page - Image";
$eLib['Book']["Publisher"] = "Publisher";

//----------------  2013-06-14 (CharlesMa) ------------------------
$eLib['Book']["WorkSheet"] = "WorkSheet";
$eLib['Book']["WorkSheetAns"] = "WorkSheet (with answers)";

//----------------  2013-04-29 (CharlesMa) ------------------------
$eLib_plus["html"]["video_btn"] = "";

//----------------  2013-09-03 (Siuwan) ------------------------
$eLib_plus["html"]['call_number'] = "Call Number";

//----------------  2013-09-27 (CharlesMa) ------------------------
$eLib_plus["html"]['stats'] = "Statistics";

//----------------  2013-10-16 (CharlesMa) ------------------------
$eLib['Book']["FirstPageBlank"] = "Blank First Page";
$eLib['Book']["BlankFirstPage"] = "Set First Page Blank";
$eLib['Book']["UnblankFirstPage"] = "Reset First Page";

$eLib["html"]["num_of_books"] = "Books";

//----------------  2013-11-07 (Siuwan) ------------------------
$eLib["html"]["book_status"] = "Book Status";

//----------------  2013-11-12 (CharlesMa) ------------------------
$eLib['Book']["DLPDF"] = "PDF Version";

//----------------  2013-11-14 (CharlesMa) 		2013-11-14-deleteallbookcomment ------------------------
$eLib["html"]["remove_all_reviews"] = "Remove All Reviews";
$eLib_plus["html"]["areusuretoremoveallthereview"] = "Are you sure to remove all the review?";


$eLib['Book']["Renewal"][0] = "No renewal allowed";
$eLib['Book']["Renewal"][-3] = "Reached your renewal limit";
$eLib['Book']["RenewalHoldBook"] = "The book is reserved by other reader";
//----------------  2013-12-27 (CharlesMa) 		20131213-eBookTW ------------------------
$eLib['Book']["BookID"] = "Book ID";
$eLib['Book']["CLC"] = "CLC";
$eLib['Book']["DDC"] = "DDC";
$eLib['Book']["Edition"] = "Edition";
$eLib['Book']["ISBN"] = "ISBN";
$eLib['Book']["New"] = "New";
$eLib['Book']["Update"] = "Update";
$eLib['Book']["BeforeImportMsg"] = "Please copy the corresponding files to the server before importation.";
$eLib['Book']["IsFirstPageBlank"] = "First Page Blank";
$eLib['Book']["IsRightToLeft"] = "From Right to Left";
$eLib['Book']["IsLeftToRight"] = "From Left to Right";
?>