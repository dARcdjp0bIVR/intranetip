<?php


$eReportCard['GradingScheme'] = "等級計劃";

$eReportCard['SchemeSettings'] = "計劃設定";
$eReportCard['SchemeSettings_GradingSchemes'] = "等級計劃設定";
$eReportCard['SchemeSettings_SubjectGradings'] = "科目等級設定";
$eReportCard['SchemeSettings_CalMethod'] = "計分設定";
$eReportCard['SchemeSettings_Highlights'] = "強調款式設定";

$eReportCard['ReportBuilder'] = "報告建立";
$eReportCard['ReportBuilder_Template'] = "報告範本建立";
$eReportCard['ReportBuilder_TitlesCustomization'] = "報告標題自訂";

$eReportCard['MonitorMarksheetSubmissionProgress'] = "分紙呈交進度";
$eReportCard['MonitorResultVerificationProgress'] = "成績核對進度";
$eReportCard['ReportGeneration'] = "報告製作";
$eReportCard['MarksheetCollection'] = "分紙收集";
$eReportCard['MarksheetCollectionPeriodSetting'] = "分紙收集設定";
$eReportCard['ResultVerificationPeriodSetting'] = "成績核對設定";
$eReportCard['DefaultSubjectGradings'] = "預設科目等級";
$eReportCard['SpecialSubjectGradings'] = "特別科目等級";
$eReportCard['ViewGradingSchemeDetails'] = "檢視等級計劃詳情";
$eReportCard['GradingSchemeDetails'] = "等級計劃詳情";
$eReportCard['MarksheetSubmission'] = "分紙呈交";
$eReportCard['VerifyAssessmentResult'] = "成績核對";
$eReportCard['MarksheetRevision'] = "分紙修改";

$eReportCard['CalculationMethod'] = "計算方法";
$eReportCard['CalculationMethodRule'] = "計算比重前把每科總分調節至";
$eReportCard['CalculationOrder'] = "計算次序";
$eReportCard['CalculationOrderHorizontal'] = "水平";
$eReportCard['CalculationOrderVertical'] = "垂直";

$eReportCard['Scheme'] = "計劃";
$eReportCard['Pass'] = "及格";
$eReportCard['Fail'] = "不及格";
$eReportCard['LowerLimit'] = "下限";
$eReportCard['Grade'] = "等級";
$eReportCard['GradePoint'] = "積點分";
$eReportCard['Distinction'] = "優異";
$eReportCard['HighlightOverall'] = "應用於";
$eReportCard['Weight'] = "比重";
$eReportCard['WeightingGrandTotal'] = "科目比重";
$eReportCard['AddMore'] = "更多";
$eReportCard['AddNewRecord'] = "新增另一項紀錄";
$eReportCard['Form'] = "級別";
$eReportCard['Forms'] = "級別";
$eReportCard['SelectForms'] = "選擇級別：";
$eReportCard['FullYear'] = "全年";
$eReportCard['ReportType'] = "報告類型";
$eReportCard['WeightedOverallResult'] = "比重總成績";
$eReportCard['AddSubject'] = "新增科目";
$eReportCard['Beginning'] = "開端";
$eReportCard['SetBasicInfo'] = "設定基本資料";
$eReportCard['DeleteAllSubject'] = "刪除全部科目";
$eReportCard['Private'] = "保密";
$eReportCard['Public'] = "公開";
$eReportCard['Column'] = "欄";
$eReportCard['AddColumn'] = "新增欄";
$eReportCard['RemoveColumn'] = "刪除欄";
$eReportCard['EditColumn'] = "更改欄";
$eReportCard['ColumnTitle'] = "欄名稱";
$eReportCard['ShowFullMark'] = "顯示滿分";
$eReportCard['HideOverallResult'] = "不顯示總成績";
$eReportCard['ShowPosition'] = "顯示名次";
$eReportCard['ShowOverallPosition'] = "顯示總名次";
$eReportCard['HidePosition'] = "不顯示名次";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";
$eReportCard['PositionRange'] = "名次範圍";
$eReportCard['EditSubjectPos'] = "更改科目位置";
$eReportCard['ColumnManage'] = "管理欄";
$eReportCard['SubjectManage'] = "管理科目";
$eReportCard['NoRecord'] = "沒有紀錄";
$eReportCard['StartDate'] = "開始日期";
$eReportCard['EndDate'] = "終結日期";
$eReportCard['Bold'] = "粗體";
$eReportCard['Italic'] = "斜體";
$eReportCard['Underline'] = "加底線";
$eReportCard['Color'] = "顏色";
$eReportCard['Red'] = "紅";
$eReportCard['Green'] = "綠";
$eReportCard['Blue'] = "藍";
$eReportCard['Notification'] = "通知";
$eReportCard['Feedback'] = "回應";
$eReportCard['Feedbacks'] = "個回應";
$eReportCard['StudentFeedback'] = "學生回應";
$eReportCard['MyRequest'] = "我的請求";
$eReportCard['Subject'] = "科目";
$eReportCard['SubjectTitle'] = "科目";
$eReportCard['Class'] = "班別";
$eReportCard['NotifyStudent'] = "通知學生";
$eReportCard['HideNotification'] = "隱藏通知";
$eReportCard['SelectSubject'] = "選擇科目";
$eReportCard['SubmitMarksheet'] = "呈交分紙";
$eReportCard['OverallResult'] = "總成績";
$eReportCard['File'] = "檔案";
$eReportCard['DownloadCSVFile'] = "下載 CSV 檔案範本";
$eReportCard['MainMarksheet'] = "主分紙";
$eReportCard['Deadline'] = "限期";
$eReportCard['Appeal'] = "上訴";
$eReportCard['Appeals'] = "上訴";
$eReportCard['ViewResult'] = "檢視成績";
$eReportCard['Close'] = "關閉";
$eReportCard['SendingDate'] = "寄出日期";
$eReportCard['EditMarksheet'] = "更改分紙";
$eReportCard['MarkAllRead'] = "設定所有項目為已檢視";
$eReportCard['Report'] = "報告";
$eReportCard['Reports'] = "報告";
$eReportCard['Confirmed'] = "已完成登分";
$eReportCard['RankingType'] = "如何顯示相同名次";
$eReportCard['ResultDisplayType'] = "成績顯示類型";
$eReportCard['MarksheetSubmissionPeriod'] = "分紙呈交時段";
$eReportCard['ResultVerificationPeriod'] = "成績核對時段";
$eReportCard['ImportMarks'] = "匯入分數";
$eReportCard['ExportMarks'] = "匯出分數";
$eReportCard['GradingType'] = "級別類型";
$eReportCard['SummaryTable'] = "總結表";
$eReportCard['SetTableDetails'] = "設定成績表內容";
$eReportCard['TableDetails'] = "成績表內容";
$eReportCard['CalculateOverallResult'] = "計算總成績";
$eReportCard['ConfirmMarksheet'] = "完成登分";
$eReportCard['ShowMainMarksheet'] = "顯示主分紙";
$eReportCard['ShowAppealStudentOnly'] = "只顯示上訴學生";
$eReportCard['ShowClassStudentStatus'] = "顯示全班學生狀況";
$eReportCard['Generate'] = "產生";
$eReportCard['LastGenerated'] = "最後產生日期";
$eReportCard['ReportPrinting'] = "報告列印";
$eReportCard['VerificationReportPriting'] = "成績核對報告列印";
$eReportCard['Transit'] = "過渡";
$eReportCard['TransitData'] = "資料過渡";
$eReportCard['TransitDataRemind'] = "請在開始新學期成績表前進行此動作！成績資料將會被保存。";
$eReportCard['NoReport'] = "暫時未有報告。";
$eReportCard['LastModifiedDate'] = "最後修改日期";
$eReportCard['LastModifiedBy'] = "最後修改人";
$eReportCard['ViewScoreList'] = "檢視大分紙";
$eReportCard['ViewScoreSummary'] = "檢視分數統計";
$eReportCard['ScoreList'] = "大分紙";
$eReportCard['ScoreSummary'] = "分數統計";
$eReportCard['PrintPage'] = "可列印頁";
$eReportCard['TeacherComment'] = "老師評語";
$eReportCard['AllowClassTeacherComment'] = "允許班主任評語";
$eReportCard['AllowSubjectTeacherComment'] = "允許科目老師評語";
$eReportCard['Comment'] = "評語";
$eReportCard['ImportClassTeacherComment'] = "匯入班主任評語";
$eReportCard['ImportSubjectTeacherComment'] = "匯入科目老師評語";
$eReportCard['TextInputMode'] = "文字輸入模式";
$eReportCard['SelectionListMode'] = "清單選擇模式";
$eReportCard['CalOverallComponentResult'] = "根據科目分卷計算主科目成績";
$eReportCard['ComponentSubjectSettingMsg'] = "若報告中包含科目分卷：";
$eReportCard['ResultDisplayType1'] = "同時顯示主科目及科目分卷之總成績";
$eReportCard['ResultDisplayType2'] = "只顯示主科目之總成績";
$eReportCard['ResultDisplayType3'] = "只顯示科目分卷之總成績";
$eReportCard['RankingType1'] = "名次排列方法一 (例: 1, 2, 3, 3, 4, 5)";
$eReportCard['RankingType2'] = "名次排列方法二 (例: 1, 2, 3, 3, 5, 6)";
$eReportCard['ResultCalType'] = "成績計算方法";
$eReportCard['ParentSubjectCalMethod1'] = "以科目分卷的來源分數計算主科目成績";
$eReportCard['ParentSubjectCalMethod2'] = "以科目分卷的總成績計算主科目成績";
$eReportCard['DataDeletion'] = "資料刪除";
$eReportCard['DataDeletionRemind'] = "此動作將會刪除成績資料包括老師評語。";
$eReportCard['Period'] = "時期";
$eReportCard['NumberOfRecord'] = "資料項數";
$eReportCard['SubjectTitleDisplay'] = "科目名稱顯示";
$eReportCard['SubjectTitleDisplayBoth'] = "顯示中英文名稱";
$eReportCard['SubjectTitleDisplayChi'] = "只顯示中文名稱";
$eReportCard['SubjectTitleDisplayEng'] = "只顯示英文名稱";
$eReportCard['ShowColumnPercentage'] = "顯示欄位百分比";
$eReportCard['NotAllowToInput'] = "不允許輸入資料";
### New ###
$eReportCard['SelectAllAs'] = "全部選擇為";
$eReportCard['DefaultTitle'] = "預設標題";
$eReportCard['CustomTitle'] = "自訂標題";
$eReportCard['Chi'] = "中";
$eReportCard['Eng'] = "英";
$eReportCard['InfoUpload'] = "資料上載";
$eReportCard['SummaryInfoUpload'] = "總結資料上載";
$eReportCard['AwardRecordUpload'] = "獎項紀錄上載";
$eReportCard['MeritRecordUpload'] = "獎懲紀錄上載";
$eReportCard['ECARecordUpload'] = "課外活動紀錄上載";
$eReportCard['RemarkUpload'] = "備註上載";

$eReportCard['InterSchoolCompetitionUpload'] = "聯校比賽紀錄上載";
$eReportCard['SchoolServiceUpload'] = "學校服務紀錄上載";
$eReportCard['InterSchoolCompetition'] = "聯校比賽紀錄";
$eReportCard['SchoolService'] = "學校服務紀錄";

$eReportCard['DownloadCSVTemplate'] = "下載 CSV 檔案範本";
$eReportCard['FileUpload'] = "檔案上載";
$eReportCard['FileUploadRemind'] = "注意：上載之檔案必須以年份、學期及班別命名。(例子：2006_1_1A.csv。第一個底線(_)後的數字代表學期︰0 代表全年；1 代表第一個學期；2 代表第二個學期；如此類推)。第二個底線(_)後的代表班別，若班別名稱有'/'號則不用輸入'/'。(例子：班別名稱：1A/Art，檔案名稱：2006_0_1AArt.csv)";
$eReportCard['ImportErrorWrongFormat'] = "錯誤的匯入格式";
$eReportCard['FileRemoveConfirm'] = "你是否確定要刪除檔案？";
$eReportCard['FileRemoveAllConfirm'] = "你是否確定要刪除全部檔案？";
$eReportCard['MarkTypeDisplay'] = "分數顯示類型<br />(只適用於以分數評核的科目)";
$eReportCard['SubMSDisplay'] = "顯示附屬分紙分數";
$eReportCard['Marks'] = "成績";
$eReportCard['Header'] = "頁首";
$eReportCard['Footer'] = "頁尾";
$eReportCard['BilingualReportTitle'] = "以中英對照顯示標題";

$eReportCard['NotNoHeader'] = "附標準頁首資訊";
$eReportCard['IsNoHeader'] = "沒有頁首資訊。";
$eReportCard['NoHeaderBr'] = "空白行數目：";
$eReportCard['LineHeight'] = "行高";
$eReportCard['JSLineHeight'] = "行高必須是大於 0 的數字。";
$eReportCard['SignatureWidth'] = "簽名欄闊度";
$eReportCard['JSSignatureWidth'] = "簽名欄闊度必須是大於 0 的數字。";
$eReportCard['AllowClassTeacherUploadCSV'] = "允許班主任上載資料";
$eReportCard['PresetComment'] = "預設字句";
$eReportCard['ClassPresetComment'] = "班主任";
$eReportCard['SubjectPresetComment'] = "學科主任";

$eReportCard['ReportCard'] = "成績表";
$eReportCard['PositionReport'] = "名次報告";
$eReportCard['ImprovementReport'] = "進步報告";
$eReportCard['HonourStudent'] = "優異學生";
$eReportCard['Statistic'] = "統計";
$eReportCard['TopX'] = "最高";
$eReportCard['PositionTitle'] = "位";
$eReportCard['ByTotal'] = "總分";
$eReportCard['ByGPA'] = "平均績點分";
$eReportCard['OrderBy'] = "排序條件";
$eReportCard['AllFormTopX'] = "級別首 <!--TopX--> 位";
$eReportCard['AllClassTopX'] = "班別首 <!--TopX--> 位";
$eReportCard['jsNonzeroAlert'] = "請輸入一個大於零的數字。";
$eReportCard['headcount'] = "學生人數";
$eReportCard['MarkRange'] = "分數";
$eReportCard['GradeStat'] = "等級統計";
$eReportCard['MarkStat'] = "分數統計";
$eReportCard['MarkStat'] = "平均分統計";
$eReportCard['Semester'] = "學期";
$eReportCard['ResultStat'] = " 成績統計";
$eReportCard['JudgeBy'] = "學業成績";
$eReportCard['Over'] = "超過";
$eReportCard['morethan'] = "多於";
$eReportCard['morethanOrEqual'] = "多於或等如";
$eReportCard['lessthan'] = "少於";
$eReportCard['NoOf'] = "<!--Item-->數目";
$eReportCard['OverMark'] = "分";
$eReportCard['DateInvalid'] = "日期不正確。";
$eReportCard['AcademicYear'] = "學年";
$eReportCard['DecimalPoint'] = "小數位";

$eReportCard['AcademicResults'] = "學業成績";
$eReportCard['ConductAssessment'] = "操行評估";
$eReportCard['Activities'] = "活動";
$eReportCard['days'] = "天";
$eReportCard['merits'] = "獎";
$eReportCard['demerits'] = "罰";
### New ###

$eReportCard['DataRemovalMsg'] = "你是否確定要刪除資料？";
$eReportCard['TransitDataMsg'] = "你是否確定要過渡資料？";
$eReportCard['RemoveItemAlert'] = "你是否確定刪除項目？";
$eReportCard['RemoveAllItemAlert'] = "你是否確定刪除所有項目？";
$eReportCard['HideNotifyAlert'] = "你是否確定要隱藏通知？";
$eReportCard['InvalidDateFormatAlert']="日期格式錯誤！";
$eReportCard['ConfirmResultAlert'] = "你是否確定已核對成績？";
$eReportCard['ConfirmMarksheetAlert'] = "你是否確定已呈交分紙？";
$eReportCard['InvalidPeriod'] = "終結日期不能早於開始日期！";
$eReportCard['ConfirmedMsg'] = "已確定成績！";
$eReportCard['SubmitMarksheetConfirm'] = "此頁的成績將會被儲存，你是否確定要繼續？";
$eReportCard['ChangeInputModeConfirm'] = "請在轉變輸入模式前確定已儲存所有紀錄，你是否確定要繼續？";
$eReportCard['MarkRemind'] = "注意︰\"abs\" 代表缺席； \"/\" 代表沒有選收或豁免。";
$eReportCard['MarksheetCollectionPeriodRemind'] = "請設定分紙呈交期限，老師只能夠在期限內呈交分紙。";
$eReportCard['MarksheetVerificationPeriodRemind'] = "請設定成績核對期限，學生只能夠在期限內核對成績。";
$eReportCard['NoReportBuiltMsg'] = "暫時未有報告。";
$eReportCard['SaveAndUpdateMainMS'] = "儲存及更新主分紙";
$eReportCard['InvalidInputMsg'] = "輸入不正確！";

$i_con_msg_notify = "<font color=green>已通知學生。</font>\n";
$i_con_msg_notify_failed = "<font color=red>通知學生失敗。</font>\n";
$i_con_msg_hide_notify = "<font color=green>已隱藏通知。</font>\n";
$i_con_msg_hide_notify_failed = "<font color=red>隱藏通知失敗。</font>\n";
$i_con_msg_report_generate = "<font color=green>已製作報告。</font>\n";
$i_con_msg_report_generate_failed = "<font color=red>製作報告失敗，找不到任何成績。</font>\n";
$i_con_msg_data_transit = "<font color=green>資料已過渡。</font>\n";
$i_con_msg_data_transit_failed = "<font color=red>資料不能過渡。</font>\n";
$i_con_msg_upload = "成功上載檔案";
$i_con_msg_upload_failed = "上載檔案失敗";
$i_con_msg_wrong_row = "<font color=red>第 %s 行的資料不正確.</font>\n";
$i_con_msg_wrong_csv_header = "<font color=red>CSV 標頭錯誤</font>\n";
$eReportCard['NeedToReGenAfterUpdate'] = "<font color=red>已更新紀錄，請重新儲存相關分紙及製作相關成績表。</font>\n";

$eReportCard['CalWeightedMark'] = "根據來源分數計算比重分數";
$eReportCard['CalComponentMark'] = "根據科目分卷分數計算主科分數";
$eReportCard['GoToBottom'] = "↓跳至最底";
$eReportCard['GoToTop'] = "↑跳至最頂";
$eReportCard['MarksheetCollectionNotInPeriodMsg'] = "現時不能輸入分數。";
$eReportCard['ResultVerificationNotInPeriodMsg'] = "現時不能核對成績。";
$eReportCard['VerifyResultRemind'] = "<b><font color=red>請檢查清楚你的分數。於核對限期後提出的上訴將不被接納。</font></b>";
$eReportCard['DuplicatedClassReportRemind'] = "<b><font color=red>相同級別不可以在兩份或以上公開成績表出現。以下是重覆的級別。</font></b>";
$eReportCard['MarksheetConfirmationRemind'] = "<b><font color=red>請必須在登分後按「確定分紙」以確定完成登分。</font></b>";
$eReportCard['SubMS'] = "附屬分紙";
$eReportCard['SubMarksheet'] = "附屬分紙";
$eReportCard['ShowSubMarksheet'] = "顯示附屬分紙";
$eReportCard['RawMarks'] = "來源分數";
$eReportCard['WeightedMarks'] = "比重分數";
$eReportCard['ConvertedGrade'] = "變換等級";
$eReportCard['InsertAfter'] = "插於後";
$eReportCard['Enabled'] = "Enabled";
$eReportCard['Scale'] = "量度";
$eReportCard['ScoreScale'] = "以分數評核";
$eReportCard['GradeScale'] = "以等級評核";
$eReportCard['Honor'] = "榮譽";
$eReportCard['PassFail'] = "及格/不及格";
$eReportCard['HonorBased'] = "榮譽";
$eReportCard['PassFailBased'] = "及格/不及格";
$eReportCard['CommonTest'] = "普通測驗";
$eReportCard['FinalExam'] = "期終試";
$eReportCard['CourseMark'] = "課程分數";
$eReportCard['Custom'] = "自訂";
$eReportCard['MarkSummary'] = "小結總分 (系統自動計算)";
$eReportCard['WeightMarkSummary'] = "比重 (小結總分)";
$eReportCard['WeightTotalSummary'] = "比重 (全年總分)";
$eReportCard['Enabled'] = "Enabled";
$eReportCard['HideNotEnrolledSubject'] = "不顯示未有紀錄之科目";
$eReportCard['Submitting'] = "登分中";
$eReportCard['Submitted'] = "已呈交";
$eReportCard['GetPreviousSemesterResult'] = "擷取上一個學期的成績";
$eReportCard['Schemes_FullMark'] = "滿分";
$eReportCard['StudentInfo'] = "學生資料";
$eReportCard['Summary'] = "總結";
$eReportCard['Misc'] = "其他";

$eReportCard['GradingSchemeSubjectsOnly'] = "以下選擇只包含已設定等級計劃設定之學科。";
$eReportCard['UpdateGradingSchemeUpdateTemplate'] = "請更新報告範本，科目等級設定才會生效。";


##################################################################################
######################## Wording Report Settings and Titles ######################
$eReportCard['Name'] = "姓名";
$eReportCard['ClassName'] = "班別";
$eReportCard['ClassNumber'] = "學號";
$eReportCard['ClassTeacher'] = "班主任";
$eReportCard['DateOfIssue'] = "派發日期";
$eReportCard['StudentNo'] = "學生編號";
$eReportCard['DateOfBirth'] = "生日日期";
$eReportCard['Gender'] = "性別";
$eReportCard['EngSubject'] = "Subject";
$eReportCard['ChiSubject'] = "科目";
$eReportCard['FullMark'] = "滿分";
$eReportCard['SubjectUnit'] = "單位";
$eReportCard['SubjectTaken'] = "修科";
$eReportCard['EngFullMark'] = "Full Mark";
$eReportCard['ChiFullMark'] = "滿分";
$eReportCard['OverallResult'] = "總成績";
$eReportCard['WholeTerm'] = "學期成績";
$eReportCard['Mark'] = "分數";
$eReportCard['Position'] = "名次";
$eReportCard['GrandTotal'] = "總分";
$eReportCard['GPA'] = "平均績點分";
$eReportCard['AverageMark'] = "平均分";
$eReportCard['FormPosition'] = "級名次";
$eReportCard['ClassPosition'] = "班名次";
$eReportCard['ClassPupilNumber'] = "全班人數";
$eReportCard['FormPupilNumber'] = "全級人數";
$eReportCard['Conduct'] = "操行";
$eReportCard['Politeness'] = "禮貌";
$eReportCard['Behaviour'] = "行為";
$eReportCard['Application'] = "專心";
$eReportCard['Tidiness'] = "整潔";

$eReportCard['Motivation'] = "主動學習";
$eReportCard['SelfConfidence'] = "自信";
$eReportCard['SelfDiscipline'] = "自律";
$eReportCard['Courtesy'] = "殷勤";
$eReportCard['Honesty'] = "誠實";
$eReportCard['Responsibility'] = "責任心";
$eReportCard['Cooperation'] = "合作";


$eReportCard['DaysAbsent'] = "缺課日數";
$eReportCard['TimesLate'] = "遲到次數";
$eReportCard['AbsentWOLeave'] = "曠課";
$eReportCard['AbsentWOReason'] = "曠課";
$eReportCard['ClassTeacherComment'] = "班主任評語";
$eReportCard['SubjectTeacherComment'] = "科目老師評語";
$eReportCard['ClassHighestAverage'] = "全班最高平均分";
$eReportCard['Awards'] = "獎項";
$eReportCard['MeritsAndDemerits'] = "獎懲";
$eReportCard['ECA'] = "課外活動";
$eReportCard['AbsenceFromECA'] = "課外活動曠課";
$eReportCard['Remark'] = "備註";
$eReportCard['Signature'] = "簽署";
$eReportCard['Principal'] = "校長";
$eReportCard['ParentGuardian'] = "家長/監護人";
$eReportCard['SchoolChop'] = "校印";
$eReportCard['Attendance'] = "考勤";
$eReportCard['Merit'] = "優點";
$eReportCard['MinorCredit'] = '小優';
$eReportCard['MajorCredit'] = '大優';
$eReportCard['Demerit'] = "缺點";
$eReportCard['MinorFault'] = "小過";
$eReportCard['MajorFault'] = "大過";
$eReportCard['SchoolReopenDay'] = "學校重開日期";
$eReportCard['Merits'] = "優點";
$eReportCard['Demerits'] = "缺點";
$eReportCard['Offence'] = "過";
$eReportCard['Tardiness'] = "緩慢";
$eReportCard['TardinessOffence'] = "緩慢";
$eReportCard['Competitions'] = "比賽";
$eReportCard['Performances'] = "表現";
$eReportCard['Services'] = "服務";
$eReportCard['Forgetfulness'] = "善忘";
$eReportCard['Misbehaviour'] = "不良行為";
$eReportCard['PFRemarks'] = "備註";


$eReportCard_ReportTitleArray["b5"] = array("SchoolChop"=>"校印", "Name"=>"姓名", "ClassName"=>"班別", 
										"ClassNumber"=>"學號", "ClassTeacher"=>"班主任", 
										"AcademicYear"=>"年度", "DateOfIssue"=>"派發日期", "StudentNo"=>"學生編號",
										"DateOfBirth"=>"生日日期", "Gender"=>"性別",
										"Subject"=>"科目", "FullMark"=>"滿分", "OverallResult"=>"總成績", 
										"Mark"=>"分數", "Position"=>"名次",
										"GrandTotal"=>"總分", "GPA"=>"平均績點分",
										"AverageMark"=>"平均分", "FormPosition"=>"級名次", 
										"ClassPosition"=>"班名次", "ClassPupilNumber"=>"全班人數", "FormPupilNumber"=>"全級人數",										
										"Conduct"=>"操行", "Politeness"=>"禮貌", "Behaviour"=>"行為", "Application"=>"專心", "Tidiness"=>"整潔",
										"Motivation"=>"主動學習", "SelfConfidence"=>"自信", "SelfDiscipline"=>"自律", "Courtesy"=>"殷勤", 
										"Honesty"=>"誠實", "Responsibility"=>"責任心", "Cooperation"=>"合作",
										"DaysAbsent"=>"缺課日數", "TimesLate"=>"遲到次數", "AbsentWOLeave"=>"曠課", "ClassTeacherComment"=>"班主任評語",
										"SubjectTeacherComment"=>"科目老師評語", "ClassHighestAverage"=>"全班最高平均分",
										"Awards"=>"獎項", "MeritsAndDemerits"=>"獎懲", "ECA"=>"課外活動", "Remark"=>"備註", 
										"InterSchoolCompetition"=>"聯校比賽", "SchoolService"=>"校內服務", 
										"Signature"=>"簽署", "Principal"=>"校長", "ParentGuardian"=>"家長/監護人",
										"Attendance"=>"考勤", "Merit"=>"優點", "Demerit"=>"缺點",
										"MinorCredit"=>'小優', "MajorCredit"=>'大優', "MinorFault"=>"小過", "MajorFault"=>"大過"
										);
$eReportCard_ReportTitleArray["en"] = array("SchoolChop"=>"School Chop", "Name"=>"Name", "ClassName"=>"Class Name", 
										"ClassNumber"=>"Class Number", "ClassTeacher"=>"Class Teacher", 
										"AcademicYear"=>"Academic Year", "DateOfIssue"=>"Date of Issue", "StudentNo"=>"Student No.",
										"DateOfBirth"=>"Date of Birth", "Gender"=>"Gender",
										"Subject"=>"Subject", "FullMark"=>"Full Mark", "OverallResult"=>"Overall Result", 
										"Mark"=>"Mark", "Position"=>"Position",
										"GrandTotal"=>"Grand Total", "GPA"=>"Grade Point Average (GPA)",
										"AverageMark"=>"Average Mark", "FormPosition"=>"Position in Form", 
										"ClassPosition"=>"Position in Class", "ClassPupilNumber"=>"No. of Pupils in Class", "FormPupilNumber"=>"No. of Pupils in Form",
										"Conduct"=>"Conduct", "Politeness"=>"Politeness", "Behaviour"=>"Behaviour", "Application"=>"Application",
										"Tidiness"=>"Tidiness",
										"Motivation"=>"Motivation in Learning", "SelfConfidence"=>"Self-confidence",
										"SelfDiscipline"=>"Self-discipline","Courtesy"=>"Courtesy","Honesty"=>"Honesty",
										"Responsibility"=>"Sense of Responsibility","Cooperation"=>"Co-operation",										
										"DaysAbsent"=>"Days Absent", "TimesLate"=>"Times Late", 
										"AbsentWOLeave"=>"Periods Absent w/o Leave", "ClassTeacherComment"=>"Class Teacher Comment",
										"SubjectTeacherComment"=>"Subject Teacher Comment", "ClassHighestAverage"=>"Highest Average in Class",
										"Awards"=>"Awards", "MeritsAndDemerits"=>"Merits & Demerits", "ECA"=>"ECA", "Remark"=>"Remark",
										"InterSchoolCompetition"=>"Inter-school Competitions Record",
										"SchoolService"=>"School Service Record",
										"Signature"=>"Signature", "Principal"=>"Principal", "ParentGuardian"=>"Parent/Guardian",
										"Attendance"=>"Attendance", "Merit"=>"Merit", "Demerit"=>"Demerit",
										"MinorCredit"=>"Minor Credit", "MajorCredit"=>"Major Credit", "MinorFault"=>"Minor Fault", "MajorFault"=>"Major Fault");

### For Report Card Bilingual Setting

	$eReportCardBilingual['Name'] = "姓名 Name";
	$eReportCardBilingual['ClassName'] = "班別 Class Name";
	$eReportCardBilingual['ClassNumber'] = "學號 Class Number";
	$eReportCardBilingual['ClassTeacher'] = "班主任<br />Class Teacher";
	$eReportCardBilingual['DateOfIssue'] = "派發日期 Date of Issue";
	$eReportCardBilingual['DateOfBirth'] = "生日日期 Date of Birth";
	$eReportCardBilingual['Gender'] = "性別 Gender";
	$eReportCardBilingual['StudentNo'] = "學生編號 Student No.";
	$eReportCardBilingual['FullMark'] = "滿分<br />Full Mark";
	$eReportCardBilingual['OverallResult'] = "總成績<br />Overall Result";
	$eReportCardBilingual['Mark'] = "分數 Mark";
	$eReportCardBilingual['Position'] = "名次 Position";
	$eReportCardBilingual['GrandTotal'] = "總分 Grand Total";
	$eReportCardBilingual['GPA'] = "平均績點分 Grade Point Average (GPA)";
	$eReportCardBilingual['AverageMark'] = "平均分 Average Mark";
	$eReportCardBilingual['FormPosition'] = "級名次 Position in Form";
	$eReportCardBilingual['ClassPosition'] = "班名次 Position in Class";
	$eReportCardBilingual['ClassPupilNumber'] = "全班人數 No. of Pupils in Class";
	$eReportCardBilingual['FormPupilNumber'] = "全級人數 No. of Pupils in Form";
	$eReportCardBilingual['Conduct'] = "操行 Conduct";
	$eReportCardBilingual['Politeness'] = "禮貌 Politeness";
	$eReportCardBilingual['Behaviour'] = "行為 Behaviour";
	$eReportCardBilingual['Application'] = "專心 Application";
	$eReportCardBilingual['Tidiness'] = "整潔 Tidiness";

	$eReportCardBilingual['Motivation'] = "主動學習 Motivation in Learning";
	$eReportCardBilingual['SelfConfidence'] = "自信 Self-confidence";
	$eReportCardBilingual['SelfDiscipline'] = "自律 Self-discipline";
	$eReportCardBilingual['Courtesy'] = "殷勤 Courtesy";
	$eReportCardBilingual['Honesty'] = "誠實 Honesty";
	$eReportCardBilingual['Responsibility'] = "責任心 Sense of Responsibility";
	$eReportCardBilingual['Cooperation'] = "合作 Co-operation";

	$eReportCardBilingual['DaysAbsent'] = "缺課日數 Days Absent";
	$eReportCardBilingual['TimesLate'] = "遲到次數 Times Late";
	$eReportCardBilingual['AbsentWOLeave'] = "曠課 Periods Absent w/o Leave";
	$eReportCardBilingual['ClassTeacherComment'] = "班主任評語 Class Teacher Comment";
	$eReportCardBilingual['SubjectTeacherComment'] = "科目老師評語<br />Subject Teacher Comment";
	$eReportCardBilingual['ClassHighestAverage'] = "全班最高平均分 Highest Average in Class";
	$eReportCardBilingual['Awards'] = "獎項 Awards";
	$eReportCardBilingual['MeritsAndDemerits'] = "獎懲<br />Merits & Demerits";
	$eReportCardBilingual['ECA'] = "課外活動 ECA";
	$eReportCardBilingual['Remark'] = "備註 Remark";
	$eReportCardBilingual['InterSchoolCompetition'] = "聯校比賽紀錄 Inter-school Competitions Record";
	$eReportCardBilingual['SchoolService'] = "學校服務紀錄 School Service Record";
	$eReportCardBilingual['Signature'] = "簽署<br />Signature";
	$eReportCardBilingual['Principal'] = "校長<br />Principal";
	$eReportCardBilingual['ParentGuardian'] = "家長/監護人<br />Parent/Guardian";
	$eReportCardBilingual['SchoolChop'] = "校印<br />School Chop";
	$eReportCardBilingual['Attendance'] = "考勤<br />Attendance";
	$eReportCardBilingual['Merit'] = "優點<br />Merits";
	$eReportCardBilingual['MinorCredit'] = '小優'."<br />Minor Credit";
	$eReportCardBilingual['MajorCredit'] = '大優'."<br />Major Credit";
	$eReportCardBilingual['Demerit'] = "缺點<br />Demerits";
	$eReportCardBilingual['MinorFault'] = "小過<br />Minor Fault";
	$eReportCardBilingual['MajorFault'] = "大過<br />Major Fault";
	$eReportCardBilingual['AcademicYear'] = "學年 Academic Year";

######

$eReportCard['SetIssueDate'] = "設定".$eReportCard['DateOfIssue'];

######

$eReportCard['ScoreSummaryTitle'][0] = "測考人數";
$eReportCard['ScoreSummaryTitle'][1] = "0-9分";
$eReportCard['ScoreSummaryTitle'][2] = "10-19分";
$eReportCard['ScoreSummaryTitle'][3] = "20-29分";
$eReportCard['ScoreSummaryTitle'][4] = "30-39分";
$eReportCard['ScoreSummaryTitle'][5] = "40-49分";
$eReportCard['ScoreSummaryTitle'][6] = "50-59分";
$eReportCard['ScoreSummaryTitle'][7] = "60-69分";
$eReportCard['ScoreSummaryTitle'][8] = "70-79分";
$eReportCard['ScoreSummaryTitle'][9] = "80-89分";
$eReportCard['ScoreSummaryTitle'][10] = "90分或以上";
$eReportCard['ScoreSummaryTitle'][11] = "最高成績";
$eReportCard['ScoreSummaryTitle'][12] = "最低成績";
$eReportCard['ScoreSummaryTitle'][13] = "優秀人數";
$eReportCard['ScoreSummaryTitle'][14] = "優秀率";
$eReportCard['ScoreSummaryTitle'][15] = "及格人數";
$eReportCard['ScoreSummaryTitle'][16] = "及格率";
$eReportCard['ScoreSummaryTitle'][17] = "不及格人數";
$eReportCard['ScoreSummaryTitle'][18] = "不及格率";
$eReportCard['ScoreSummaryTitle'][19] = "人均成績";
$eReportCard['ScoreSummaryTitle'][20] = "標準差";

###### eReportCard 2008 ######
$eReportCard['SubjectSettings'] = '科目設定';
$eReportCard['FormSettings'] = '班級設定';
####### ###### ###### ######## 

######

$eReportCard['DisplaySettingsArray']["StudentInfo"] = array("Name", "ClassName", "ClassNumber", "StudentNo", "ClassTeacher", "AcademicYear", "DateOfIssue", "DateOfBirth", "Gender");
//$eReportCard['DisplaySettingsArray']["Summary"] = array("GrandTotal", "GPA", "AverageMark", "FormPosition", "ClassPosition", "ClassPupilNumber", "FormPupilNumber", "ClassHighestAverage", "Conduct", "Politeness", "Behaviour", "Application", "Tidiness", "Motivation", "SelfConfidence", "SelfDiscipline", "Courtesy", "Honesty", "Responsibility", "Cooperation", "DaysAbsent", "TimesLate", "AbsentWOLeave", "Merits", "Attendance", "Competitions", "Performances", "Services", "Demerits", "Forgetfulness", "Tardiness", "Misbehaviour");
//$eReportCard['DisplaySettingsArray']["Summary"] = array("GrandTotal", "GPA", "AverageMark", "FormPosition", "ClassPosition", "ClassPupilNumber", "FormPupilNumber", "ClassHighestAverage", "Conduct", "Politeness", "Tidiness", "Application", "AbsenceFromECA", "Motivation", "SelfConfidence", "SelfDiscipline", "Courtesy", "Honesty", "Responsibility", "Cooperation", "DaysAbsent", "TimesLate", "AbsentWOReason", "Merits", "Attendance", "Competitions", "Performances", "Services", "Demerits", "Forgetfulness", "Tardiness", "Misbehaviour");
$eReportCard['DisplaySettingsArray']["Summary"] = array("GrandTotal", "GPA", "AverageMark", "FormPosition", "ClassPosition", "ClassPupilNumber", "FormPupilNumber", "ClassHighestAverage", "PFRemarks", "Conduct", "Politeness", "Tidiness", "Application", "AbsenceFromECA", "Motivation", "SelfConfidence", "SelfDiscipline", "Courtesy", "Honesty", "Responsibility", "Cooperation", "DaysAbsent", "TimesLate", "AbsentWOReason", "Merits", "Attendance", "Competitions", "Performances", "Services", "Demerits", "Tardiness", "Forgetfulness", "Misbehaviour", "Offence", "TardinessOffence");
$eReportCard['DisplaySettingsArray']["Misc"] = array("ClassTeacherComment", "SubjectTeacherComment", "Awards", "MeritsAndDemerits", "MinorCredit", "MajorCredit", "MinorFault", "MajorFault", "ECA", "Remark", "InterSchoolCompetition", "SchoolService");
$eReportCard['DisplaySettingsArray']["Signature"] = array("Principal", "ClassTeacher", "ParentGuardian", "SchoolChop");

$ReportTitleFilePath = $PATH_WRT_ROOT."file/reportcard/lang.b5.php";
if(file_exists($ReportTitleFilePath))
{
	include_once($ReportTitleFilePath);
}

##################################################################################
##################################################################################

?>
