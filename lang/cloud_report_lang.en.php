<?php 
/********************** Change Log ***********************/
/*
 * 2019-03-21 Isaac
 * Changed some of the wordings
 */
/***********************************************************/

### eclass on the cloud report page ###
$Lang['Cloud']['Report']['Title']['Greeting']='Welcome to eClass on the Cloud service';
$Lang['Cloud']['Report']['Title']['UsedStorage']='Storage';
$Lang['Cloud']['Report']['Title']['Traffic']='';
$Lang['Cloud']['Report']['CurrentPackage']='Current storage plan';
$Lang['Cloud']['Report']['Plan']='Plan';
$Lang['Cloud']['Report']['Used']='Used';
$Lang['Cloud']['Report']['Usage'] = '<!--usedStorage--> <!--capacityUnit--> of <!--availableStorage--> <!--capacityUnit--> '.$Lang['Cloud']['Report']['Used'];
$Lang['Cloud']['Report']['ValidTill']='valid till';

##alert messages##
$Lang['Cloud']['Report']['StorageWarningMessage']='Used storage reached <!--usedStoragePercentage-->% of the limit. Please consider upgrading your plan.';

$Lang['Cloud']['Report']['LegendAry']['intranet']='Intranet';
$Lang['Cloud']['Report']['LegendAry']['classroom']='Classroom';
$Lang['Cloud']['Report']['LegendAry']['imail']='iMail';
$Lang['Cloud']['Report']['LegendAry']['ifolder']='iFolder';
$Lang['Cloud']['Report']['LegendAry']['mysql']='MySQL';
$Lang['Cloud']['Report']['LegendAry']['other']='Other';
$Lang['Cloud']['Report']['Intro']='Thank you for supporting eClass On the Cloud service. 
We would like to provide more information about current package plan and eClass system status information.';
$Lang['Cloud']['Report']['RecommandationHeadingAry']['Recommendations']='How to clear up disk space?';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Intranet']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Classroom']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Imail']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Ifolder']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Mysql']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Others']='';
$Lang['Cloud']['Report']['RecommandationAry']['Recommendations']='-	Admin delete or ask users to delete old or unwanted files from “Classroom”.<br>
-	Ask users to delete old emails.<br>
-	Admin delete or ask users to delete old or unwanted data from “eCommunity”.<br>
-	Admin delete or ask users to delete all old files and unwanted data stored in eClass on the Cloud.
';
// $Lang['Cloud']['Report']['RecommandationAry']['Intranet']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Classroom']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Imail']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Ifolder']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Mysql']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Others']='';
?>