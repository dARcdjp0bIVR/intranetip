<?php
# Editing by 

$eReportCard['SchemesFullMark'] = "Full Mark";

$eReportCard['Template']['StudentInfo']['NameEn'] = "Name in English";
$eReportCard['Template']['StudentInfo']['NameCh'] = "Name in Chinese";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "Class No";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "Student Reference No";
$eReportCard['Template']['StudentInfo']['DOB'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['DOI'] = "Date of Issue";

$eReportCard['Template']['AcademicResults'] = "Academic Results";
$eReportCard['Template']['ConductAssessment'] = "Conduct Assessment";

$eReportCard['Template']['ClassTeacher'] = "Class Teacher's Signature";
$eReportCard['Template']['Principal'] = "Principal's Signature";
$eReportCard['Template']['ParentGuardian'] = "Parent's / Guardian's Signature";
$eReportCard['Template']['IssueDate'] = "Date of Issue";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "---";
$eReportCard['DisplayOverallResult'] = "Display Overall Result";
$eReportCard['DisplayGrandAvg'] = "Display Average Mark";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";

$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");
?>