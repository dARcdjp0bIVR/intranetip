<?php
# Editing by ivan

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

$eReportCard['Template']['SchoolNameEn'] = "Munsang College";
$eReportCard['Template']['SchoolNameCh'] = "民 生 書 院";
$eReportCard['Template']['ReportTitleCh'] = "學生成績表";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "No.";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

# Marks Table
$eReportCard['Template']['FirstExamination'] = "First Examination";
$eReportCard['Template']['FirstExam'] = "First Exam";
$eReportCard['Template']['SecondExam'] = "Second Exam";
$eReportCard['Template']['YearResult'] = "Year Result";

$eReportCard['Template']['Subject'] = "Subject";
$eReportCard['Template']['Score'] = "Score";
$eReportCard['Template']['Grade'] = "Grade";
$eReportCard['Template']['Position'] = "Position";
$eReportCard['Template']['NoOfPupils'] = "No. of Pupils";
$eReportCard['Template']['Marks'] = "marks";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

$eReportCard['Template']['DiligenceEn'] = "Diligence";
$eReportCard['Template']['DisciplineEn'] = "Discipline";
$eReportCard['Template']['MannerEn'] = "Manner";
$eReportCard['Template']['SociabilityEn'] = "Sociability";
$eReportCard['Template']['TimeEn'] = "time";
$eReportCard['Template']['DayEn'] = "day";
$eReportCard['Template']['MeritEn'] = "Merit";
$eReportCard['Template']['MinorAchievementEn'] = "Minor achievement";
$eReportCard['Template']['MajorAchievementEn'] = "Major achievement";
$eReportCard['Template']['ECAEn'] = "Co-Curricular Activities";
# Signature Table
$eReportCard['Template']['ClassTeacher'] = "Form Teacher";
$eReportCard['Template']['Principal'] = "Principal";
$eReportCard['Template']['PrincipalChop'] = "School Chop and Principal";
$eReportCard['Template']['ParentGuardian'] = "Parent's/Guardian's Signature";
$eReportCard['Template']['IssueDate'] = "Date";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

$eReportCard['Template']['KeyToGrade'] = "Key to grade";
$eReportCard['Template']['Excellent'] = "A = Excellent 優";
$eReportCard['Template']['VeryGood'] = "B = Very good 良";
$eReportCard['Template']['Good'] = "C = Good 常";
$eReportCard['Template']['Fair'] = "D = Fair 可";
$eReportCard['Template']['Unsatisfactory'] = "E = Unsatisfactory 差";


### Conduct Grade
$eReportCard['Template']['ConductGrade']['Excellent'] = 'Excellent';
$eReportCard['Template']['ConductGrade']['VeryGood'] = 'Very Good';
$eReportCard['Template']['ConductGrade']['Good'] = 'Good';
$eReportCard['Template']['ConductGrade']['FairlyGood'] = 'Fairly Good';
$eReportCard['Template']['ConductGrade']['Fair'] = 'Fair';
$eReportCard['Template']['ConductGrade']['NeedsImprovement'] = 'Needs Improvement';
$eReportCard['Template']['ConductGrade']['Poor'] = 'Poor';


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");


# Other info remarks
$eReportCard['Template']['OtherInfoRemarks']['attendance'] = 'Report Card displays attendance records from Student Attendance by default.';
?>