<?php
# Editing by 

### Munsang College (Primary) language file ###

$eReportCard['SchoolServiceUpload'] = "職務";

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['Class'] = "班別(學號)";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";
$eReportCard['Template']['StudentInfo']['ClassTeacher'] = "班主任";

$eReportCard['Template']['StudentInfo']['Name_Bi'] = "姓名,Name";
$eReportCard['Template']['StudentInfo']['Gender_Bi'] = "性別,Sex";
$eReportCard['Template']['StudentInfo']['Class_Bi'] = "班別(學號),Class(No.)";
$eReportCard['Template']['StudentInfo']['STRN_Bi'] = "學生編號,STRN";
$eReportCard['Template']['StudentInfo']['ClassTeacher_Bi'] = "班主任,Teacher";
$eReportCard['Template']['StudentInfo']['DateOfIssue_Bi'] = "派發期,Date of Issue";

$eReportCard['Template']['StudentInfo']['Teacher_Chi'] = "老師";

$eReportCard['Template']['FirstTerm'] = "First Term<br />上學期";
$eReportCard['Template']['SecondTerm'] = "Second Term<br />下學期";
$eReportCard['Template']['FirstTerm_Teacher'] = "第一學期<br />First Term";
$eReportCard['Template']['SecondTerm_Teacher'] = "第二學期<br />Second Term";
$eReportCard['Template']['Subjects'] = "科目";
$eReportCard['Template']['SubjectsEn'] = "Subjects";
$eReportCard['Template']['SubjectsChi'] = "科目";

$eReportCard['Template']['OverallResultEn'] = "Grand Total";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average";
$eReportCard['Template']['AvgMarkCh'] = "百分比";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "全班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Level";
$eReportCard['Template']['FormPositionCh'] = "全級名次";
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave";
$eReportCard['Template']['EarlyLeaveCh'] = "早退次數";
# Summary Info
$eReportCard['Template']['Diligence'] = "勤學";
$eReportCard['Template']['Discipline'] = "紀律";
$eReportCard['Template']['Politeness'] = "禮貌";
$eReportCard['Template']['Sociability'] = "群性";
$eReportCard['Template']['Tidness'] = "整潔";
$eReportCard['Template']['Conduct'] = "操行";

$eReportCard['Template']['DiligenceEn'] = "Diligence";
$eReportCard['Template']['DiligenceCh'] = "勤學";
$eReportCard['Template']['DisciplineEn'] = "Discipline";
$eReportCard['Template']['DisciplineCh'] = "紀律";
$eReportCard['Template']['PolitenessEn'] = "Politeness";
$eReportCard['Template']['PolitenessCh'] = "禮貌";
$eReportCard['Template']['SociabilityEn'] = "Sociability";
$eReportCard['Template']['SociabilityCh'] = "群性";
$eReportCard['Template']['TidnessEn'] = "Tidness";
$eReportCard['Template']['TidnessCh'] = "整潔";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";

$eReportCard['Template']['PositionOfServicesEn'] = "Position of Services";
$eReportCard['Template']['PositionOfServicesCh'] = "職務";
$eReportCard['Template']['EcaEn'] = "Extra-curricular Activities";
$eReportCard['Template']['EcaCh'] = "課外活動";
$eReportCard['Template']['Merit&DemeritEn'] = "Merit & Demerit";
$eReportCard['Template']['Merit&DemeritCh'] = "獎懲";
$eReportCard['Template']['RemarksEn'] = "Remarks";
$eReportCard['Template']['RemarksCh'] = "備註";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['FinalResult'] = "Final Result<br>全年成績";
$eReportCard['Template']['FinalResult_Teacher'] = "全年成績<br>Final Result";
$eReportCard['Template']['GrandTotal'] = "總分";
$eReportCard['Template']['AverageMark'] = "百分比";
$eReportCard['Template']['GrandMarksheet'] = "學生成績總表";

$eReportCard['Template']['ClassPosition'] = "全班名次";
$eReportCard['Template']['FormPosition'] = "全級名次";

$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";
$eReportCard['Template']['eca'] = "ECA 課外活動";

$eReportCard['RemarkAbsent'] = "ABS";
$eReportCard['RemarkAbsentZeorMark'] = "ABS";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "N.A.";
$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";

$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";

$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

$eReportCard['ReportView'] = "成績表版面";
$eReportCard['Student'] = "學生";
$eReportCard['Teacher'] = "教師";

$eReportCard['Template']['NumStudentsOfForm'] = "全級人數";

?>