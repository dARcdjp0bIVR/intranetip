<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

# School Info
$eReportCard['Template']['SchoolInfo']['Email'] = "Email(電郵): info@pooitun.edu.hk";
$eReportCard['Template']['SchoolInfo']['Telephone'] = "Tel.(電話): 2326 5211";
$eReportCard['Template']['SchoolInfo']['Fax'] = "Fax.(傳真): 2320 1344";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "NAME";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "CLASS";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班別";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "CLASS NO.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "班號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "STUDENT NO.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "學號";
$eReportCard['Template']['StudentInfo']['ClassTeacherEn'] = "CLASS TEACHER";
$eReportCard['Template']['StudentInfo']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['StudentInfo']['GenderEn'] = "GENDER";
$eReportCard['Template']['StudentInfo']['GenderCh'] = "性別";
$eReportCard['Template']['StudentInfo']['DateOfBirthEn'] = "DATE OF BIRTH";
$eReportCard['Template']['StudentInfo']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "DATE OF ISSUE";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "派發日期";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['FullMarkEn'] = "Full Marks";
$eReportCard['Template']['FullMarkCh'] = "滿分";
$eReportCard['Template']['Term1En'] = "Term 1";
$eReportCard['Template']['Term1Ch'] = "第一學期";
$eReportCard['Template']['Term2En'] = "Term 2";
$eReportCard['Template']['Term2Ch'] = "第二學期";
$eReportCard['Template']['AverageEn'] = "Average";
$eReportCard['Template']['AverageCh'] = "平均分";
$eReportCard['Template']['RankEn'] = "Rank";
$eReportCard['Template']['RankCh'] = "名次";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Grand Average";
$eReportCard['Template']['AvgMarkCh'] = "總平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "全班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "全級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";
$eReportCard['Template']['PassingMarkEn'] = "Passing Marks";
$eReportCard['Template']['PassingMarkCh'] = "合格分數";
$eReportCard['Template']['MarkEn'] = "Mark";
$eReportCard['Template']['MarkCh'] = "分數";
$eReportCard['Template']['CommentStudent'] = "同學";


$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "N.A.";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Absence (Days)";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['TimesLateEn'] = "Lateness (Times)";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操 行"; 
$eReportCard['Template']['MajorMeritEn'] = "Major Merit";
$eReportCard['Template']['MajorMeritCh'] = "大 功";
$eReportCard['Template']['MinorMeritEn'] = "Minor Merit";
$eReportCard['Template']['MinorMeritCh'] = "小 功";
$eReportCard['Template']['GoodPointEn'] = "Good Point";
$eReportCard['Template']['GoodPointCh'] = "優 點";
$eReportCard['Template']['MajorDemeritEn'] = "Major Demerit";
$eReportCard['Template']['MajorDemeritCh'] = "大 過";
$eReportCard['Template']['MinorDemeritEn'] = "Minor Demerit";
$eReportCard['Template']['MinorDemeritCh'] = "小 過";
$eReportCard['Template']['BadPointEn'] = "Bad Point";
$eReportCard['Template']['BadPointCh'] = "缺 點";
$eReportCard['Template']['PromotedToEn'] = "Promoted To";
$eReportCard['Template']['PromotedToCh'] = "升上";
$eReportCard['Template']['DetainedInEn'] = "Detained In";
$eReportCard['Template']['DetainedInCh'] = "重讀";

$eReportCard['Template']['Term1RemarksEn'] = "Term 1 Remarks";
$eReportCard['Template']['Term1RemarksCh'] = "第一學期評語";
$eReportCard['Template']['Term2RemarksEn'] = "Term 2 Remarks";
$eReportCard['Template']['Term2RemarksCh'] = "第二學期評語";
$eReportCard['Template']['RemarksEn'] = "Remark";
$eReportCard['Template']['RemarksCh'] = "備註";
$eReportCard['Template']['CommentEn'] = "Comment";
$eReportCard['Template']['CommentCh'] = "評語";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER (班主任)";
$eReportCard['Template']['Principal'] = "PRINCIPAL (校長)";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN (家長/監護人)";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");


# Grading Table
$eReportCard['Template']['GradingEn'] = 'GRADING';
$eReportCard['Template']['GradingCh'] = '評分等第';
$eReportCard['Template']['GradingArr']['A']['En'] = 'EXCELLENT';
$eReportCard['Template']['GradingArr']['A']['Ch'] = '優異';
$eReportCard['Template']['GradingArr']['B']['En'] = 'GOOD';
$eReportCard['Template']['GradingArr']['B']['Ch'] = '良好';
$eReportCard['Template']['GradingArr']['C']['En'] = 'SATISFACTORY';
// $eReportCard['Template']['GradingArr']['C']['Ch'] = '滿意';
// $eReportCard['Template']['GradingArr']['D']['En'] = 'Fair';
// $eReportCard['Template']['GradingArr']['D']['Ch'] = '尚可';
// $eReportCard['Template']['GradingArr']['E']['En'] = 'Unsatisfactory';
// $eReportCard['Template']['GradingArr']['E']['Ch'] = '不合格';
$eReportCard['Template']['GradingArr']['C']['Ch'] = '尚可';
$eReportCard['Template']['GradingArr']['D']['En'] = 'UNSATISFACTORY';
$eReportCard['Template']['GradingArr']['D']['Ch'] = '不及格';

$eReportCard['Template']['FailedEn'] = 'Failed';
$eReportCard['Template']['FailedCh'] = '不合格';
$eReportCard['Template']['NotApplicableEn'] = 'Not Applicable';
$eReportCard['Template']['NotApplicableCh'] = '不適用';

# Extra Term Report Comment
$eReportCard['ExtraTermReportComment'][1][0] = "期中測驗成績非常低落，請督促　貴子弟勤加溫習，全力提升學業成績，否則有留級之可能。";
$eReportCard['ExtraTermReportComment'][1][1] = "期中測驗成績低落，未達水平，請督促　貴子弟勤加溫習，全力提升學業成績。";
$eReportCard['ExtraTermReportComment'][2][0] = "期中測驗成績非常低落，請督促　貴子弟勤加溫習，全力提升學業成績，否則有留級之可能。";
$eReportCard['ExtraTermReportComment'][2][1] = "期中測驗成績低落，未達水平，請督促　貴子弟勤加溫習，全力提升學業成績。";
$eReportCard['ExtraTermReportComment'][3][0] = "期中測驗成績非常低落，請督促　貴子弟勤加溫習，全力提升學業成績。";
$eReportCard['ExtraTermReportComment'][3][1] = "期中測驗成績低落，未達水平，請督促　貴子弟勤加溫習，全力提升學業成績。";
$eReportCard['ExtraTermReportComment'][4][0] = "期中測驗成績非常低落，請督促　貴子弟勤加溫習，提升成績，否則將須退修部分科目或留級。";
$eReportCard['ExtraTermReportComment'][4][1] = "期中測驗成績低落，未達水平，請督促　貴子弟勤加溫習，提升成績，否則將須退修部分科目。";

$eReportCard['ExtraTermReportPassScoreRemark'][0] = "備註：五十分為合格，一百分為滿分";
$eReportCard['ExtraTermReportPassScoreRemark'][1] = "備註：四十分為合格，一百分為滿分";
$eReportCard['ExtraTermReportPassScoreRemark'][2] = "備註：四十五分為合格，一百分為滿分";

$eReportCard['Template']['PrincipalSignatureCh'] = "校長簽署";
$eReportCard['Template']['ParentSignatureCh'] = "家長簽署";
?>