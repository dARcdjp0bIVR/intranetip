<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

### CSV Tags
$eReportCard['OthersUpload'] = "缺考";
$eReportCard['OtherInfoArr']['others'] = "缺考";

### School Info
$eReportCard['Template']['SchoolInfo']['SchoolNameCh'] = "孔  聖  堂  中  學";
$eReportCard['Template']['SchoolInfo']['SchoolNameEn'] = "Confucius Hall Secondary School";
//$eReportCard['Template']['SchoolInfo']['Principal'] = "古澤芬先生 Mr. Ku Chak Fun";
//2013-0131-1232-18140
//$eReportCard['Template']['SchoolInfo']['Principal'] = "楊永漢先生 Mr. Yeung Wing Hon";
$eReportCard['Template']['SchoolInfo']['Principal'] = "楊永漢博士 Dr. Yeung Wing Hon";

### ColHeader 
$eReportCard['Template']['ColHeader']['FullMarkCh'] = "滿分";
$eReportCard['Template']['ColHeader']['ResultCh'] = "成績";
$eReportCard['Template']['ColHeader']['RankCh'] = "名次";
$eReportCard['Template']['ColHeader']['PositionCh'] = "名次";
$eReportCard['Template']['ColHeader']['AnnualResultCh'] = "全年成績";

$eReportCard['Template']['ColHeader']['FullMarkEn'] = "Full Mark";
$eReportCard['Template']['ColHeader']['ResultEn'] = "Result";
$eReportCard['Template']['ColHeader']['RankEn'] = "Rank";
$eReportCard['Template']['ColHeader']['PositionEn'] = "Position";
$eReportCard['Template']['ColHeader']['AnnualResultEn'] = "Annual Result";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名(Name)";
$eReportCard['Template']['StudentInfo']['Class'] = "班別(Class)";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號(Reg No.)";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總　分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Rank in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Rank in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "N.A.";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Absence(Days)";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Tardiness(Times)";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave(Times)";
$eReportCard['Template']['EarlyLeaveCh'] = "早退次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['Conduct AverageEn'] = "Conduct Average";
$eReportCard['Template']['Conduct AverageCh'] = "操行總評";
$eReportCard['Template']['Conduct Average'] = "操行總評";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "優點";
$eReportCard['Template']['Demerits'] = "缺點";
$eReportCard['Template']['MinorCredit'] = "小功";
$eReportCard['Template']['MajorCredit'] = "大功";
$eReportCard['Template']['MinorFault'] = "小過";
$eReportCard['Template']['MajorFault'] = "大過";
$eReportCard['Template']['MeritsEn'] = "Merits";
$eReportCard['Template']['DemeritsEn'] = "Demerits";
$eReportCard['Template']['MinorCreditEn'] = "Minor Credit";
$eReportCard['Template']['MajorCreditEn'] = "Major Credit";
$eReportCard['Template']['MinorFaultEn'] = "Minor Fault";
$eReportCard['Template']['MajorFaultEn'] = "Major Fault";
$eReportCard['Template']['Remark'] = "備註 Remark";  
$eReportCard['Template']['PersonalConduct'] = "行為表現   Conduct";
$eReportCard['Template']['eca'] = "服務 / 課外活動 Services / Extra-curricular activities";
$eReportCard['Template']['ClassTeacherComment'] = "評語 Comments";

$eReportCard['Template']['Conduct AverageCh'] = "操行總評";
$eReportCard['Template']['PolitenessCh'] = "禮 貌";
$eReportCard['Template']['ResponsibilityCh'] = "責 任 感 ";
$eReportCard['Template']['Study AttitudeCh'] = "學習態度";
$eReportCard['Template']['DisciplineCh'] = "遵守紀律";
$eReportCard['Template']['MeritDemeritCh'] = "獎懲紀錄";

$eReportCard['Template']['Conduct AverageEn'] = "Conduct Average";
$eReportCard['Template']['PolitenessEn'] = "Politeness";
$eReportCard['Template']['ResponsibilityEn'] = "Responsibility";
$eReportCard['Template']['Study AttitudeEn'] = "Learning Attitude";
$eReportCard['Template']['DisciplineEn'] = "Discipline";
$eReportCard['Template']['MeritDemeritEn'] = "Merits & Demerits";
$eReportCard['Template']['MeritDemeritTime'] = "[ 次數 Time(s) ]";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "班主任 Class Teacher";
$eReportCard['Template']['Principal'] = "校長 Principal";
$eReportCard['Template']['ParentGuardian'] = "監護人 Guardian";
$eReportCard['Template']['IssueDate'] = "日期 Date";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


?>