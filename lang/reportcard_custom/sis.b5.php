<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "Teacher's Signature";
$eReportCard['Template']['Principal'] = "Principal's Signature";
$eReportCard['Template']['ParentGuardian'] = "Parent's Signature";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE";
//$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP";
//$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
//$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
//$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
//$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
//$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Statistics
$eReportCard['SubjectType'] = "科目總類";
$eReportCard['AllSubjectType'] = "所有科目種類";
$eReportCard['ExamSubject'] = "考試科目";
$eReportCard['NonExamSubject'] = "非考試科目";
$eReportCard['SubjectComponent'] = "科目分卷";



# Transcript
$eReportCard['TranscriptSIS']['StudentEnglishName'] = "學生英文姓名";
$eReportCard['TranscriptSIS']['AdmissionNumber'] = "入學號碼";
$eReportCard['TranscriptSIS']['EngName'] = "英文姓名";
$eReportCard['TranscriptSIS']['ChiName'] = "中文姓名";
$eReportCard['TranscriptSIS']['AdmNo'] = "入學號碼";
$eReportCard['ReportsArr']['TranscriptArr']['TitleArr'][1] = "SINGAPORE INTERNATIONAL SCHOOL";
$eReportCard['ReportsArr']['TranscriptArr']['TitleArr'][2] = "STUDENT TRANSCRIPT";
$eReportCard['ReportsArr']['TranscriptArr']['StudentName'] = "Student Name";
$eReportCard['ReportsArr']['TranscriptArr']['StudentNumber'] = "Student Number";
$eReportCard['ReportsArr']['TranscriptArr']['SchoolYear'] = "School Year";
$eReportCard['ReportsArr']['TranscriptArr']['ClassName'] = "Class Name";
$eReportCard['ReportsArr']['TranscriptArr']['Total'] = "Total";
$eReportCard['ReportsArr']['TranscriptArr']['Grade'] = "Grade";
$eReportCard['ReportsArr']['TranscriptArr']['Overall'] = "Overall";
$eReportCard['ReportsArr']['TranscriptArr']['Attendance'] = "Attendance";
$eReportCard['ReportsArr']['TranscriptArr']['Conduct'] = "Conduct";
$eReportCard['ReportsArr']['TranscriptArr']['Remarks'] = "Remarks";
//$eReportCard['ReportsArr']['TranscriptArr']['TitleArr'][1] = "新加坡國際學校";
//$eReportCard['ReportsArr']['TranscriptArr']['TitleArr'][2] = "學生成績單";
//$eReportCard['ReportsArr']['TranscriptArr']['StudentName'] = "姓名";
//$eReportCard['ReportsArr']['TranscriptArr']['StudentNumber'] = "學號";
//$eReportCard['ReportsArr']['TranscriptArr']['SchoolYear'] = "學年";
//$eReportCard['ReportsArr']['TranscriptArr']['ClassName'] = "班別";
//$eReportCard['ReportsArr']['TranscriptArr']['Total'] =  "總分";
//$eReportCard['ReportsArr']['TranscriptArr']['Grade'] = "等級";
//$eReportCard['ReportsArr']['TranscriptArr']['Overall'] = "整體";
//$eReportCard['ReportsArr']['TranscriptArr']['Attendance'] = "考勤";
//$eReportCard['ReportsArr']['TranscriptArr']['Conduct'] = "操行";
//$eReportCard['ReportsArr']['TranscriptArr']['Remarks'] = "評語";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級排名","學生進度","全班名次(升班用)");
$eReportCard['GrandMarksheetTypeAlert2'] = "所有科目的總分不能只以一個學期計算";
$eReportCard['GrandMarksheetAlertArr']['CSV_Export_Not_Support_All_Subject_Type'] = "CSV 滙出不支援所有科目種類.";
$eReportCard['GrandMarksheetAlertArr']['PleaseSelectSubject'] = "Please select a subject.";
$eReportCard['GrandMarksheetAlertArr']['CSV_Export_Not_Support_Multiple_Subject'] = "CSV export does not support multiple subjects selection.";

# Generate report by csv
$eReportCard['GenerateReportFromCSV'] = "從CSV建立報告";

# MarkSheet Revision & Edit
//$eReportCard['TeacherComment'] = "Teacher Remarks";
//$eReportCard['Template']['SubjectTeacherComment'] = "Subject Teacher's Remarks";
//$eReportCard['MasterReport']['DataShortName']['SubjectTeacherComment'] = $eReportCard['Template']['SubjectTeacherComment'];

# Teacher Remarks
//$eReportCard['CommentContent'] = "Remark Content";

# Basic Settings 
//$eReportCard['Style'] = "Highlight";

# Class Level 
$eReportCard['Form']  = "級";

# Gen & print 
$eReportCard['Preview'] = $eReportCard['TemplatePreview'];

# Left Side Menu 
//$eReportCard['Settings_ReportCardTemplates'] = "Report Card";
//$eReportCard['ClassTeacherComment'] = "Class Teacher's Remarks";
//$eReportCard['Management_ClassTeacherComment'] = $eReportCard['ClassTeacherComment'];
$eReportCard['Management_OtherInfo'] = "特別註記";

# Other Info (Special Remarks Tab Name)
$eReportCard['OtherInfoArr']['specialRemark'] = $eReportCard['Management_OtherInfo'];

# Personal Characteristics (Learning Attiude)
$eReportCard['PersonalCharacteristics'] = "學習態度";
$eReportCard['PersonalCharacteristicsPool'] = "學習態度庫";
$eReportCard['PersonalCharacteristicsFormSettings'] = "學習態度設定";
//$eReportCard['Scale'] = "Scale"; No Changes
$eReportCard['NewPersonalCharacteristics'] = "新增學習態度";
$eReportCard['EditPersonalCharacteristics'] = "編輯學習態度";
$eReportCard['NoPersonalCharacteristicsSetting'] = "未有學習態度設定.";


?>