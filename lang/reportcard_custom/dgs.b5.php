<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

# School Info
$eReportCard['Template']['SchoolInfo']['Principal'] = 'Mrs. Stella Lau, JP';
$eReportCard['Template']['SchoolInfo']['SchoolName'] = "DIOCESAN GIRLS' SCHOOL";

# Colheader
$eReportCard['TeachersComment'] = "REMARKS";
$eReportCard['Teacher'] = "TEACHER";
$eReportCard['Template']['TermMark'] = "Term Mark";
$eReportCard['Template']['ExamMark'] = "Exam Mark";

# MSTableFooter
$eReportCard['Template']['FullMark'] = "Full Marks: 100";
$eReportCard['Template']['80%above'] = "80% or above: in red";
$eReportCard['Template']['Grade'] = "GRADE: ";
$eReportCard['Template']['Distinction'] = "A=Distinction";
$eReportCard['Template']['Credit'] = "B,C=Credit";
$eReportCard['Template']['Pass'] = "D,E=Pass";
$eReportCard['Template']['Failure'] = "F=Failure";
$eReportCard['Template']['Absent'] = "ABS=Absent";
$eReportCard['Template']['Exempt'] = "EXM=Exempted";
$eReportCard['Template']['GeneralSkills'] = "*General Skills encompass reading, Speaking, Listening and Integrated Skills";
$eReportCard['Template']['PleaseNote'] = "Please note: To facilitate administration, students are assigned to one of 5 classes
(U, W, X, Y and Z) which are grouped under two blocks of matching competency.
Classes U, W, X are placed in Block I and streamed in English, Chinese and
Mathematics according to their abilities (Sets A - D). Classes Y and Z are similarly
streamed in Block II in the same core subjects (Sets E - G).";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Secondary";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "SUBJECT";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "---";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "No. of days absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "No. of times late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['eca'] = "EXTRA-CURRICULAR ACTIVITIES";
$eReportCard['Template']['SpecialDuty'] = "SPECIAL DUTIES";
$eReportCard['Template']['ClassTeacherComment'] = "OVERALL COMMENTS";
$eReportCard['Template']['Award'] = "Awards/Prizes";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "Form Teachers";
$eReportCard['Template']['Principal'] = "Headmistress<br>".$eReportCard['Template']['SchoolInfo']['Principal'];
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "School Chop";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


?>