<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "满分";

$eReportCard['Template']['StudentInfo']['Name'] = "Name 姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "Class 班别";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender 性别";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN 学生编号";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "总分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "级名次";
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺课日数";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "迟到次数";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
// $eReportCard['Template']['Remark'] = "Remark 备注";
// $eReportCard['Template']['RemarkStr'] = "1. 中国语文分数比例: 120, 120, 30, 30','2. 英国语文分数比例: 70, 70, 70, 70, 20', '3. \"abs\"=0分, \"--\"=合理缺席', '4. 英文网上学习: P.S.=合格, NI=有待改善'.'5.该生于普通话科总测验缺席, 只考得了口试, 得分为 13/30'.'6.非华语学生以特别卷作答'";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>总成绩";
$eReportCard['Template']['GrandTotal'] = "总分";
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校长";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家长/监护人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派发日期";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 奖惩";
$eReportCard['Template']['Merits'] = "Merits 优点";
$eReportCard['Template']['Demerits'] = "Demerits 缺点";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小优";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大优";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小过";
$eReportCard['Template']['MajorFault'] = "Major Fault 大过";
$eReportCard['Template']['Remark'] = "Remark 备注";  
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任评语";
$eReportCard['Template']['eca'] = "ECA 课外活动";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['DisplayOverallResult'] = "显示总成绩";
$eReportCard['DisplayGrandAvg'] = "显示平均分";
$eReportCard['ShowClassPosition'] = "显示班名次";
$eReportCard['ShowFormPosition'] = "显示级名次";

$eReportCard['GrandMarksheetTypeOption'] = array("班别总结", "全级名次");

?>