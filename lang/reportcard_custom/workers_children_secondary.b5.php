<?php
# Editing by Connie

$eReportCard['Template']['semTitleCh'] = "統測成績報告";

$eReportCard['Template']['StudentNameCh'] = "學生姓名";
$eReportCard['Template']['StudentNameEn'] = "Name";

$eReportCard['Template']['STRNnoCh'] = "學生編號";
$eReportCard['Template']['STRNnoEn'] = "STRN";
 
$eReportCard['Template']['ClassCh'] = "班別";
$eReportCard['Template']['ClassEn'] = "Class";

$eReportCard['Template']['SexCh'] = "性別";
$eReportCard['Template']['SexEn'] = "Sex";

$eReportCard['Template']['GenderM_Ch'] = "男";
$eReportCard['Template']['GenderF_Ch'] = "女";

$eReportCard['Template']['BdateCh']="出生日期";
$eReportCard['Template']['BdateEn']="Date of Birth";

$eReportCard['Template']['IssueDateCh']="派發日期";
$eReportCard['Template']['IssueDateEn']="Date of Issue";

$eReportCard['Template']['SubjectsCh'] = "科目";
$eReportCard['Template']['SubjectsEn'] = "Subjects";

$eReportCard['Template']['ScoreCh'] = "分數";
$eReportCard['Template']['ScoreEn'] = "Score";

$eReportCard['Template']['AnnualCh'] = "全學年";
$eReportCard['Template']['AnnualEn'] = "Annual";

$eReportCard['Template']['PercentileCh'] = "百分位數";
$eReportCard['Template']['PercentileEn'] = "Percentile";

$eReportCard['Template']['AverageCh'] = "平均";
$eReportCard['Template']['AverageEn'] = "Average";

$eReportCard['Template']['ClassPositionCh'] = "全班名次";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";

$eReportCard['Template']['FormPositionCh'] = "全級名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";

$eReportCard['Template']['PersonalLeaveCh'] = "事假節數";
$eReportCard['Template']['PersonalLeaveEn'] = "Personal Leave(periods)";

$eReportCard['Template']['SickLeaveCh'] = "病假節數";
$eReportCard['Template']['SickLeaveEn'] = "Sick Leave(periods)";

$eReportCard['Template']['LateCh'] = "遲到次數";
$eReportCard['Template']['LateEn'] = "Late";

$eReportCard['Template']['AbsenceLeaveCh'] = "曠課節數";
$eReportCard['Template']['AbsenceLeaveEn'] = "Absence w/o Leave(periods)";

$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['ConductEn'] = "Conduct";

$eReportCard['Template']['PraiseCh'] = "表揚";
$eReportCard['Template']['PraiseEn'] = "Praise";

$eReportCard['Template']['GoodPointCh'] = "優點";
$eReportCard['Template']['GoodPointEn'] = "Good Point(s)";

$eReportCard['Template']['MinorMeritCh'] = "小功";
$eReportCard['Template']['MinorMeritEn'] = "Minor Merit(s)";

$eReportCard['Template']['MajorMeritCh'] = "大功";
$eReportCard['Template']['MajorMeritEn'] = "Major Merit(s)";

$eReportCard['Template']['WarningCh'] = "警告";
$eReportCard['Template']['WarningEn'] = "Warning(s)";

$eReportCard['Template']['BlackMarkCh'] = "缺點";
$eReportCard['Template']['BlackMarkEn'] = "Black Mark(s)";

$eReportCard['Template']['MinorDemeritCh'] = "小過";
$eReportCard['Template']['MinorDemeritEn'] = "Minor Demerit(s)";

$eReportCard['Template']['MajorDemeritCh'] = "大過";
$eReportCard['Template']['MajorDemeritEn'] = "Major Demerit(s)";

$eReportCard['Template']['CommentCh'] = "評語";
$eReportCard['Template']['CommentEn'] = "Comments";

$eReportCard['Template']['PromotionCh'] = "備註";
$eReportCard['Template']['PromotionEn'] = "Remarks";

$eReportCard['Template']['FormTeacherCh'] = "班主任";
$eReportCard['Template']['FormTeacherEn'] = "Class Teacher";

$eReportCard['Template']['PrincipalCh']="校長";
$eReportCard['Template']['PrincipalEn']="Principal";

$eReportCard['Template']['FirstSemCh']="上學期";
$eReportCard['Template']['FirstSemEn']="Term 1"; 
  
$eReportCard['Template']['SecondSemCh']="下學期";
$eReportCard['Template']['SecondSemEn']="Term 2"; 

$eReportCard['Template']['ParentSignEn']="Parent/Guardian";
$eReportCard['Template']['ParentSignCh']="家長/監護人";

$eReportCard['Template']['RemarkCh'] = "備註";

$eReportCard['Template']['RemarkArr']['line1']['S1toS3_Ch']="各科以100分為滿分，50分以下以( )表示。";
$eReportCard['Template']['RemarkArr']['line1']['S4toS6_Ch']="各科以100分為滿分，45分以下以( )表示。";

$eReportCard['Template']['RemarkArr']['line2_Ch']="ABS = 缺席；EX = 豁免；-- = 不適用。";


$eReportCard['RemarkExempted'] = "EX";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "--";
$eReportCard['RemarkDropped'] = "--";
?>