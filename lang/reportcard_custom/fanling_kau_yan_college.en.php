<?php
# Editing by

### Student Info
$eReportCard['Template']['NameCh'] = "姓名";
$eReportCard['Template']['NameEn'] = "Name";
$eReportCard['Template']['StudentNameCh'] = "學生姓名";
$eReportCard['Template']['StudentNameEn'] = "Name";
$eReportCard['Template']['ClassCh'] = "班別";
$eReportCard['Template']['ClassEn'] = "Class";
$eReportCard['Template']['NoEn'] = "No.";
$eReportCard['Template']['ClassNumberCh'] = "學號";
$eReportCard['Template']['ClassNumberEn'] = "Class No.";
$eReportCard['Template']['GenderCh'] = "性別";
$eReportCard['Template']['GenderEn'] = "Gender";
$eReportCard['Template']['StudentNoCh'] = "學生編號";
$eReportCard['Template']['StudentNoEn'] = "Code";
$eReportCard['Template']['FKYC_StudentNo'] = "FKYC 學生編號";
$eReportCard['Template']['DOBCh'] = "出生日期";
$eReportCard['Template']['DOBEn'] = "Date of Birth";
$eReportCard['Template']['IssueDateCh'] = "報告日期";
$eReportCard['Template']['IssueDateEn'] = "Date of Issue";
$eReportCard['Template']['IssueDate_AssessmentCh'] = "派發日期";

$eReportCard['Template']['SchoolTitleCh'] = "粉 嶺 救 恩 書 院";
$eReportCard['Template']['SchoolTitleEn'] = "Fanling Kau Yan College";

### Class Teacher Comment & Attendance Table
$eReportCard['Template']['ClassTeacherCommentCh'] = "班主任評語";
$eReportCard['Template']['ClassTeacherCommentEn'] = "Class Tutors' Comments";
$eReportCard['Template']['AttendanceCh'] = "出席";
$eReportCard['Template']['AttendanceEn'] = "Attendance";
$eReportCard['Template']['LateCh'] = "遲到日數";
$eReportCard['Template']['LateEn'] = "Days Late";
$eReportCard['Template']['SickLeaveCh'] = "病假日數";
$eReportCard['Template']['SickLeaveEn'] = "Sick Leave";
$eReportCard['Template']['LeaveWithReasonCh'] = "事假日數";
$eReportCard['Template']['LeaveWithReasonEn'] = "Casual Leave";
$eReportCard['Template']['TruantCh'] = "曠課日數";
$eReportCard['Template']['TruantEn'] = "Truancy";
$eReportCard['Template']['EarlyLeaveCh'] = "早退日數";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave";
$eReportCard['Template']['DisapprovedLeaveCh'] = "不獲批核缺課";
$eReportCard['Template']['DisapprovedLeaveEn'] = "Disapproved Leave";
$eReportCard['Template']['DayCh'] = "天";
$eReportCard['Template']['DayEn'] = "Day(s)";
$eReportCard['Template']['ConductGradesCh'] = "操行評級";
$eReportCard['Template']['ConductGradesEn'] = "Conduct Grades";
$eReportCard['Template']['PunctualityCh'] = "守時";
$eReportCard['Template']['PunctualityEn'] = "Punctuality";
$eReportCard['Template']['AppearanceCh'] = "儀容";
$eReportCard['Template']['AppearanceEn'] = "Appearance";
$eReportCard['Template']['ConductCh'] = "行為";
$eReportCard['Template']['ConductEn'] = "Conduct";

### Subject Teacher Comment Table
$eReportCard['Template']['SubjectTeacherCommentCh'] = "學科老師評語";
$eReportCard['Template']['SubjectTeacherCommentEn'] = "Subject Teachers' Comments";

### Post Table
$eReportCard['Template']['SchoolManagementPostCh'] = "學生校園管理職務";
$eReportCard['Template']['SchoolManagementPostEn'] = "Student Council Duties";
$eReportCard['Template']['OthersPostCh'] = "其他職務";
$eReportCard['Template']['OthersPostEn'] = "Other Duties";
$eReportCard['Template']['Column1NameCh'] = "Column 1 (Chinese Name)";
$eReportCard['Template']['Column1NameEn'] = "Column 1 (English Name)";
$eReportCard['Template']['Column1Width'] = "Column 1 (Width %)";
$eReportCard['Template']['Column2NameCh'] = "Column 2 (Chinese Name)";
$eReportCard['Template']['Column2NameEn'] = "Column 2 (English Name)";
$eReportCard['Template']['Column2Width'] = "Column 2 (Width %)";

### Signature Table
$eReportCard['Template']['ClassTeacherNameCh'] = "班主任姓名";
$eReportCard['Template']['ClassTeacherNameEn'] = "Class Tutors";
$eReportCard['Template']['Colon'] = "：";
$eReportCard['Template']['ClassTeacherSignatureCh'] = "班主任簽署";
$eReportCard['Template']['ClassTeacherSignatureEn'] = "Signatures";
$eReportCard['Template']['PrincipalNameCh'] = "校長姓名";
$eReportCard['Template']['PrincipalNameEn'] = "Principal";
$eReportCard['Template']['PrincipalSignatureCh'] = "校長簽署";
$eReportCard['Template']['PrincipalSignatureEn'] = "Signature";
$eReportCard['Template']['PrincipalCh'] = "邱潔瑩";
$eReportCard['Template']['PrincipalEn'] = "YAU Kit-ying";

### Academic Result Table
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['AcademicResultCh'] = "學業成績";
$eReportCard['Template']['AcademicResultEn'] = "Academic Performance";
$eReportCard['Template']['LearningAttitudeCh'] = "學習態度";
$eReportCard['Template']['LearningAttitudeEn'] = "Learning Attitude";
$eReportCard['Template']['LearningHabitCh'] = "學習習慣";
$eReportCard['Template']['LearningHabitEn'] = "Learning Habit";
$eReportCard['Template']['RemarksCh'] = "備註";
$eReportCard['Template']['RemarksEn'] = "Remarks";
$eReportCard['Template']['AnnualOverallCh'] = "全年總分";
$eReportCard['Template']['AnnualOverallEn'] = "Total";
$eReportCard['Template']['GradeDescriptionCh'] = "等第說明";

$eReportCard['GradeDescriptionArr']['5*/5**'] = "表現出色，自學能力高，常具創意";
$eReportCard['GradeDescriptionArr']['5'] = "學科表現理想，具自學能力及創意";
$eReportCard['GradeDescriptionArr']['4'] = "學業表現已達滿意水平，具自學能力";
$eReportCard['GradeDescriptionArr']['3'] = "學業表現尚可，自學能力偶有不足";
$eReportCard['GradeDescriptionArr']['2'] = "學業表現達基本要求，自學能力不足";
$eReportCard['GradeDescriptionArr']['1'] = "學業表現未達基本要求，欠自學能力";

$eReportCard['PersonalCharDescriptionArr']['A*'] = "表現出色，自學能力高，常具創意";
$eReportCard['PersonalCharDescriptionArr']['A'] = "學科表現理想，具自學能力及創意";
$eReportCard['PersonalCharDescriptionArr']['B'] = "學業表現已達滿意水平，具自學能力";
$eReportCard['PersonalCharDescriptionArr']['C'] = "學業表現尚可，自學能力偶有不足";
$eReportCard['PersonalCharDescriptionArr']['D'] = "學業表現達基本要求，自學能力不足";
$eReportCard['PersonalCharDescriptionArr']['E'] = "學業表現未達基本要求，欠自學能力";

### Personal Characteristics
$eReportCard['A*_en'] = "A*";
$eReportCard['A_en'] = "A";
$eReportCard['B_en'] = "B";
$eReportCard['C_en'] = "C";
$eReportCard['D_en'] = "D";
$eReportCard['E_en'] = "E";
$eReportCard['A*_b5'] = "A*";
$eReportCard['A_b5'] = "A";
$eReportCard['B_b5'] = "B";
$eReportCard['C_b5'] = "C";
$eReportCard['D_b5'] = "D";
$eReportCard['E_b5'] = "E";

### Special Case Display
$eReportCard['RemarkNotAssessed'] = "N.A.";
$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "Drop";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
// $eReportCard['RemarkAbsentZeorMark'] = "0";
$eReportCard['RemarkAbsentZeorMark'] = "ABS";

### Joint Test Report
$eReportCard['Template']['JointTestReportCh'] = "測驗成績表";
$eReportCard['Template']['JointTestReportEn'] = "Joint Test Report";

$eReportCard['Template']['TestSubjectsCh'] = "測驗科目";
$eReportCard['Template']['TestSubjectsEn'] = "Subjects";
$eReportCard['Template']['ResultsCh'] = "成績";
$eReportCard['Template']['ResultsEn'] = "Results";

$eReportCard['Template']['AssessmentReport']['PrincipalSignatureCh'] = "校長";
$eReportCard['Template']['AssessmentReport']['PrincipalSignatureEn'] = "Principal";
$eReportCard['Template']['AssessmentReport']['ClassTeacherSignatureCh'] = "班主任";
$eReportCard['Template']['AssessmentReport']['ClassTeacherSignatureEn'] = "Co-tutors";
$eReportCard['Template']['AssessmentReport']['GuardianSignatureCh'] = "家長/監護人";
$eReportCard['Template']['AssessmentReport']['GuardianSignatureEn'] = "Parent / Guardian";

### OLE and Awards
$eReportCard['Template']['RecordOfOLEEn'] = "Record of Other Learning Experiences";
$eReportCard['Template']['RecordOfOLECh'] = "其他學習經歷紀錄";
$eReportCard['Template']['ActivitiesEn'] = "Activities";
$eReportCard['Template']['ActivitiesCh'] = "活 動";
$eReportCard['Template']['CoCurricularActivitiesEn'] = "Co-curricular Activities";
$eReportCard['Template']['CoCurricularActivitiesCh'] = "聯課活動";
$eReportCard['Template']['EventsEn'] = "Events";
$eReportCard['Template']['EventsCh'] = "事項名稱";
$eReportCard['Template']['OrganisersEn'] = "Organisers";
$eReportCard['Template']['OrganisersCh'] = "主辦機構";
$eReportCard['Template']['PartnerOrganizationsEn'] = "Partner Organizations";
$eReportCard['Template']['PartnerOrganizationsCh'] = "合辦機構";
$eReportCard['Template']['ComponentOfOleEn'] = "Component of OLE";
$eReportCard['Template']['ComponentOfOleCh'] = "其他學習經歷範疇";
$eReportCard['Template']['ComponentOfOleSeparator'] = "、";
$eReportCard['Template']['RolesEn'] = "Roles";
$eReportCard['Template']['RolesCh'] = "參與角色";
$eReportCard['Template']['HoursEn'] = "Hours";
$eReportCard['Template']['HoursCh'] = "時數";
$eReportCard['Template']['AwardsTitleEn'] = "Awards";
$eReportCard['Template']['AwardsTitleCh'] = "獎 項";
$eReportCard['Template']['AwardsEn'] = "Awards";
$eReportCard['Template']['AwardsCh'] = "獎項名稱";

$eReportCard['Template']['SelfAccCh'] = "學生自述";
$eReportCard['Template']['SelfAccEn'] = "Self Account";

$Lang['eReportCard']['PersonalCharArr']['Comment'] = "Reason for all A* or E";
$eReportCard['PersonalChar']['ExportTitle']['Comment']['En'] = "Reason for all A* or E";
$eReportCard['PersonalChar']['ExportTitle']['Comment']['Ch'] = "全A*、全E原因";

# Award Certificate
$eReportCard['AwardCertificate']['PrincipalCH'] = '校長';
$eReportCard['AwardCertificate']['PrincipalEN'] = 'Principal';
$eReportCard['AwardCertificate']['PrincipalNameDisplay'] = "YAU Kit-ying, Veronica";

$eReportCard['AwardCertificate']['Gender_M'] = 'his';
$eReportCard['AwardCertificate']['Gender_F'] = 'her';
$eReportCard['AwardCertificate']['AwardsDisplayCh'] = "獎";

$eReportCard['AwardCertificate']['ContentEN_1'] = '
This<br/>
CERTIFICATE<br/>
is awarded to<br/>
<b><!--StudentName--></b> (<!--ClassName-->)';
$eReportCard['AwardCertificate']['ContentEN_2'] = '
in recognition of <!--Gender--> outstanding<br/>
academic performance in';
$eReportCard['AwardCertificate']['ContentEN_3'] = '
<!--AwardName--><br/>
in <!--YearName-->';

$eReportCard['AwardCertificate']['ContentCH_1'] = '
　　茲證明 <!--StudentName--> 同學(<!--ClassName-->班)於<br/>
<!--YearName-->第<!--TermNumber-->學期榮獲<br/>';
$eReportCard['AwardCertificate']['ContentCH_2'] = '
<!--學業成績優異獎﹕<br/>-->
<span style="line-height: 0.2;"><br/>&nbsp;<br/></span>
<!--AwardName-->';

$eReportCard['AwardCertificate']['NumberCH'] = array('零', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十');
$eReportCard['AwardCertificate']['YearCH'] = '年';
$eReportCard['AwardCertificate']['MonthCH'] = '月';
$eReportCard['AwardCertificate']['DateCH'] = '日';
$eReportCard['AwardCertificate']['YearTermCH'] = '<!--StartYear-->至<!--EndYear-->年度';

# Award Mark Summary
$eReportCard['AwardMarkSummary']['EstimatedMark'] = 'Estimated Mark';

?>