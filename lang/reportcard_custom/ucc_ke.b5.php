<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender 性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "Student Number";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "Class Number";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
// $eReportCard['Template']['Remark'] = "Remark 備註";
// $eReportCard['Template']['RemarkStr'] = "1. 中國語文分數比例: 120, 120, 30, 30','2. 英國語文分數比例: 70, 70, 70, 70, 20', '3. \"abs\"=0分, \"--\"=合理缺席', '4. 英文網上學習: P.S.=合格, NI=有待改善'.'5.該生於普通話科總測驗缺席, 只考得了口試, 得分為 13/30'.'6.非華語學生以特別卷作答'";

$eReportCard['Template']['SubjectOverall'] = "Overall";
$eReportCard['Template']['Annual'] = "Annual";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "Principal";
$eReportCard['Template']['ParentGuardian'] = "Parent / Guardian";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['FormTeacher'] = "Form Teacher";
$eReportCard['Template']['SchoolChop'] = "School Chop";
$eReportCard['Template']['Signature'] = "Signature";
$eReportCard['Template']['Position'] = "Position";
$eReportCard['Template']['Effort'] = "Effort";
$eReportCard['Template']['ProgressPerformance'] = "Progress<br/>Performance";
$eReportCard['Template']['ProgressChart'] = "Progress Chart";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";
$eReportCard['Template']['eca'] = "ECA 課外活動";

//email "From Lau Sir (UCCKE)" 2015-12-15 13:47:56 (Tuesday)
//$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkExempted'] = "----";
//
//$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkDropped'] = "----";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "N.A.";
// 2014-0703-0932-14140
$eReportCard['RemarkAbsentZeorMark'] = "U";

$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";

$eReportCard['Template']['Promotion'] = "Promotion";
$eReportCard['Template']['Retention'] = "Retention";
$eReportCard['Template']['ToBePromotedTo'] = "To be promoted to ";
$eReportCard['Template']['ByConcession'] = " by concession";
$eReportCard['Template']['ToBeRetainedIn'] = "To be retained in ";

$eReportCard['ExtraInfoLabel'] = "努力";
$eReportCard['AdjustPositionLabal'] = "名次";

$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

# otherInfo
$eReportCard['Template']['Reading'] = "閱讀";
$eReportCard['StudentResultSummary']['Reading'] = "閱讀";

$eReportCard['Template']['StudentCertficateEn1'] = "Honour Student";
$eReportCard['Template']['StudentCertficateEn2'] = "Certificate";
$eReportCard['Template']['StudentCertficateCh'] = "獎狀";
$eReportCard['Template']['AcademicCertficateEn'] = "Academic Certificate";
$eReportCard['Template']['AcademicCertficateCh'] = "學業獎狀";

$eReportCard['FormTeacherCommentReport']['YearlyTheme'] = "年度主題";
$eReportCard['FormTeacherCommentReport']['ThemeVerse'] = "主題經文";
?>