<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['SchoolNameEn'] = "WAH YAN COLLEGE, KOWLOON";
$eReportCard['Template']['SchoolNameCh'] = "九龍華仁書院";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['IssueDateEn'] = "Date";
$eReportCard['Template']['StudentInfo']['IssueDateCh'] = "日期";

# Marks Table
$eReportCard['Template']['KeyLearningAreasEn'] = "Key Learning Areas";
$eReportCard['Template']['KeyLearningAreasCh'] = "學習領域";
$eReportCard['Template']['CategoriesEn'] = "Categories";
$eReportCard['Template']['CategoriesCh'] = "類別";
$eReportCard['Template']['SubjectsAndComponentsEn'] = "Subjects and Components";
$eReportCard['Template']['SubjectsAndComponentsCh'] = "科目及範疇";
$eReportCard['Template']['WeightingsEn'] = "Weightings";
$eReportCard['Template']['WeightingsCh'] = "比重";
$eReportCard['Template']['CW|ExamEn'] = "(CW | Exam) %";
$eReportCard['Template']['CourseworkEn'] = "Coursework (CW)";
$eReportCard['Template']['CourseworkCh'] = "課業評估";
$eReportCard['Template']['ContinueAssessmentEn'] = "Continuous Assessment";
$eReportCard['Template']['ContinueAssessmentCh'] = "第一學期評估";
$eReportCard['Template']['ExaminationEn'] = "Examination";
$eReportCard['Template']['ExaminationCh'] = "考試成績";
$eReportCard['Template']['OverallResultsEn'] = "Overall Results";
$eReportCard['Template']['OverallResultsCh'] = "總成績";
$eReportCard['Template']['OverallResult2En'] = "Overall Result";
$eReportCard['Template']['OverallResult2Ch'] = "總成績";
$eReportCard['Template']['SubjectTeachersEn'] = "Subject Teachers";
$eReportCard['Template']['SubjectTeachersCh'] = "科目教師";
$eReportCard['Template']['CommentsEn'] = "Comments";
$eReportCard['Template']['CommentsCh'] = "評語";


$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Total";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "本班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "Abs";
$eReportCard['RemarkAbsentZeorMark'] = "Abs";
$eReportCard['RemarkNotAssessed'] = "-";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Number of times late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['AttendanceDaysEn'] = "Attendance (days)";
$eReportCard['Template']['AttendanceDaysCh'] = "出席日數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['RemarkEn'] = "Remarks";
$eReportCard['Template']['RemarkCh'] = "備注";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";
$eReportCard['Template']['ClassTeacherCommentEn'] = "Form Teacher's Comments";
$eReportCard['Template']['ClassTeacherCommentCh'] = "班主任評語";
$eReportCard['Template']['NumberOfServiceHoursEn'] = "Number of service hours";
$eReportCard['Template']['NumberOfServiceHoursCh'] = "服務時數";

# Signature Table
$eReportCard['Template']['ClassTeacherEn'] = "Form Teacher";
$eReportCard['Template']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['ParentGuardianEn'] = "Parent / Guardian";
$eReportCard['Template']['ParentGuardianCh'] = "家長 / 監護人";
//2014-0106-1613-50177
//$eReportCard['Template']['PrincipalNameEn'] = "Dr. Tan Kang John";
$eReportCard['Template']['PrincipalNameEn'] = "Mr. Chung Wai Leung";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

# Other Info
$eReportCard['OtherInfoArr']['summary'] = "操行";
$eReportCard['OtherInfoArr']['schoolservice'] = "服務時數";

$eReportCard['Template']['HardCodedSubjectNameEn']['80'] = "SCORE IN CHINESE";
$eReportCard['Template']['HardCodedSubjectNameCh']['80'] = "中文得分";
$eReportCard['Template']['HardCodedSubjectNameEn']['165'] = "SCORE IN ENGLISH";
$eReportCard['Template']['HardCodedSubjectNameCh']['165'] = "英文得分";
$eReportCard['Template']['HardCodedSubjectNameEn']['280'] = "SCORE IN MATHEMATICS";
$eReportCard['Template']['HardCodedSubjectNameCh']['280'] = "數學得分";

$eReportCard['Template']['CoreEn'] = "Core Subjects";
$eReportCard['Template']['CoreCh'] = "核心科目";
$eReportCard['Template']['ElectiveEn'] = "Elective Subjects";
$eReportCard['Template']['ElectiveCh'] = "選修科目";
$eReportCard['Template']['OthersEn'] = "Others";
$eReportCard['Template']['OthersCh'] = "其他";

?>