<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['SecondarySchool'] = "Secondary School";
$eReportCard['Template']['GradeSeparator'] = "—";
$eReportCard['Template']['PrincipalNameEn'] = "Dr. Chan Wai Kai";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";
$eReportCard['Template']['StudentInfo']['Date'] = "Date";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Student Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";
$eReportCard['Template']['StudentInfo']['StudentIDEn'] = "Student ID";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";
$eReportCard['Template']['HighestScoreEn'] = "Highest Score";
$eReportCard['Template']['LevelEn'] = "Level";
$eReportCard['Template']['ScoreEn'] = "Score";
$eReportCard['Template']['RemarksEn'] = "Remarks";
$eReportCard['Template']['AnnualTotalEn'] = "Annual Total";
$eReportCard['Template']['AreaEn'] = "Area";
$eReportCard['Template']['IndicatorDescriptionEn'] = "Indicator Description";

$eReportCard['Template']['Term1En'] = "Term 1";
$eReportCard['Template']['Term2En'] = "Term 2";
$eReportCard['Template']['TermP+2En'] = "Term P + Term 2";
$eReportCard['Template']['Module1Module2En'] = "Module 1 / Module 2";

$eReportCard['Template']['GradeRemarksArr']['*5'] = "Excellent";
$eReportCard['Template']['GradeRemarksArr']['4'] = "Good";
$eReportCard['Template']['GradeRemarksArr']['3'] = "Fair";
$eReportCard['Template']['GradeRemarksArr']['2'] = "Needs Improvement";
$eReportCard['Template']['GradeRemarksArr']['1'] = "Unsatisfactory";
$eReportCard['Template']['GradeRemarksArr']['N/A'] = "Not Applicable";

$eReportCard['Template']['GradeCategoryArr'][10] = "Top 10%";
$eReportCard['Template']['GradeCategoryArr'][25] = "11-25%";
$eReportCard['Template']['GradeCategoryArr'][50] = "26-50%";
$eReportCard['Template']['GradeCategoryArr'][75] = "51-75%";
$eReportCard['Template']['GradeCategoryArr'][100] = "Bottom 25%";
$eReportCard['Template']['AcademicPerformanceInEn'] = "Academic Performance in ";
$eReportCard['Template']['CategoryRemarksEn'] = "Category: Top 10%, 11-25%, 26-50%, 51-75%, Bottom 25%";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkNotAssessed'] = "N/A";
$eReportCard['RemarkExempted'] = "/";
//2014-0710-1548-29073
//$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkDropped'] = "N/A";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

# CSV Info
$eReportCard['OtherInfoArr']['summary'] = "操行";
$eReportCard['OtherInfoArr']['attendance'] = "考勤";
$eReportCard['OtherInfoArr']['award'] = "獎項 ";
$eReportCard['OtherInfoArr']['awardproject'] = "獎項 (計劃)";
$eReportCard['OtherInfoArr']['projectinfo'] = "計劃資料";
$eReportCard['OtherInfoArr']['remark'] = "升級狀況";

$eReportCard['Template']['MeritsEn'] = "Merit";
$eReportCard['Template']['DemeritsEn'] = "Demerit";

$eReportCard['Template']['DaysAbsentEn'] = "Day(s) of Absence";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "Class Teacher's Signature";
$eReportCard['Template']['Principal'] = "Chief Principal's Signature";
$eReportCard['Template']['ParentGuardian'] = "Parent's Signature";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "School Chop";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

### Generic Skills
$eReportCard['Template']['GenericSkillsEn'] = "Generic Skills";
$eReportCard['Template']['LearningPerformanceEn'] = "Learning Performance";
$eReportCard['Template']['InitiativeEn'] = "Initiative";
$eReportCard['Template']['CooperationWithOthersEn'] = "Cooperation with Others";
$eReportCard['Template']['HomeworkCompletionEn'] = "Homework Completion";
$eReportCard['Template']['ConflictResolutionEn'] = "Conflict Resolution";
$eReportCard['Template']['ClassParticipationEn'] = "Class Participation";
$eReportCard['Template']['ProblemSolvingEn'] = "Problem Solving";
// [2017-0915-1655-04235]
$eReportCard['Template']['ValuesAndAttitudesEn'] = "Values and attitudes";
$eReportCard['Template']['SenseOfResponsibilityEn'] = "Sense of responsibility";
$eReportCard['Template']['RespectForOthersEn'] = "Respect for others";
$eReportCard['Template']['CollaborationWithOthersEn'] = "Collaboration with others";
$eReportCard['Template']['SelfManagementEn'] = "Self-management";
$eReportCard['Template']['ProblemSolvingEn'] = "Problem solving";

$eReportCard['Template']['SkillsGradeRemarksArr']['E'] = "Excellent";
$eReportCard['Template']['SkillsGradeRemarksArr']['G'] = "Good";
$eReportCard['Template']['SkillsGradeRemarksArr']['S'] = "Satisfactory";
$eReportCard['Template']['SkillsGradeRemarksArr']['N'] = "Needs Improvement";

### Other Learning Experiences
$eReportCard['Template']['OtherLearningExperiencesEn'] = "Other Learning Experiences";
$eReportCard['Template']['ActivitySocietyClubTeamEn'] = "Activity / Society / Club / Team";
$eReportCard['Template']['SchoolAndCommunityServicesEn'] = "School and Community Services";

### Awards and Achievement
//2014-0213-1415-41073
//$eReportCard['Template']['AwardsAndAchievementEn'] = "Awards and Achievement";
$eReportCard['Template']['AwardsAndAchievementEn'] = "Awards and Achievements";

### Conduct
$eReportCard['Template']['ConductGradeRemarksArr']['A / A-'] = "Excellent";
$eReportCard['Template']['ConductGradeRemarksArr']['B+ / B / B-'] = "Good";
$eReportCard['Template']['ConductGradeRemarksArr']['C+ / C / C-'] = "Fair";
$eReportCard['Template']['ConductGradeRemarksArr']['D'] = "Needs Improvement";
$eReportCard['Template']['ConductGradeRemarksArr']['E'] = "Unsatisfactory";

### Project Based
$eReportCard['Template']['ProjectTitleEn'] = "Project Title";
$eReportCard['Template']['NameOfSupervisingTeacherEn'] = "Name of Supervising Teacher";
$eReportCard['Template']['OverallPerformanceEn'] = "Overall Performance";
$eReportCard['Template']['ProjectBasedLearningEn'] = "Project Based Learning";
$eReportCard['Template']['Level5En'] = "Level 5";
$eReportCard['Template']['ReferToProjectReportEn'] = "Refer to the Project based learning report.";

### Class Teacher Comment
$eReportCard['Template']['ClassTeacherCommentEn'] = "Class Teacher's Comments";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


$Lang['eReportCard']['templateChangeDescAry']['2012-06-25']['2012-0206-1620-05069'][] = "1. 「缺席」及「遲到」於「eAttendance」取得資料。";
$Lang['eReportCard']['templateChangeDescAry']['2012-06-25']['2012-0206-1620-05069'][] = "2. 「Other Learning Experiences > Activity/ Society/ Club/ Team」於「eEnrolment > 學會」取得資料，並由系統自動產生學會出席備註。";
$Lang['eReportCard']['templateChangeDescAry']['2012-06-25']['2012-0206-1620-05069'][] = "3. 「Other Learning Experiences > School and Community Services」於「Admin Console > 學生檔案 > 服務」取得資料。";


$Lang['eReportCard']['ReportArr']['TranscriptArr']['ReportTitleEn'] = "Transcript of Academic Record (Secondary School)";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfIssueEn'] = "Date of Issue";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentParticularsEn'] = "Student Particulars";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentNameEn'] = "Student Name";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['EnglishEn'] = "English";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['ChineseEn'] = "Chinese";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfBirthEn'] = "Date of Birth";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['HKIDEn'] = "HKID No.";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['SexEn'] = "Sex";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentNoEn'] = "Student No.";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['AcademicPerformanceEn'] = "Academic Performance";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['juniorGradeEn'] = "Junior Grades";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['seniorGradeEn'] = "Senior Grades";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['SchoolYearEn'] = "School Year";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['SubjectsEn'] = "Subjects";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['HighestEn'] = "Highest";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['ScoreLevelEn'] = "Score/ Level*";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['AnnualTotalEn'] = "Annual Total#";
?>