<?php
# Editing by ivan

$eReportCard['ReportCard'] = "Report Card";

// System message after form submit, must have the prefix "$i_con_msg_"
$i_con_msg_wrong_row = "<font color=red>Invalid data on row %s.</font>\n";
$i_con_msg_wrong_csv_header = "<font color=red>Wrong CSV header.</font>\n";
$i_con_msg_report_generate = "<font color=green>Report Generated.</font>\n";
$i_con_msg_report_generate_failed = "<font color=red>Failed to Generate Report! Cannot find any result.</font>\n";
$i_con_msg_delete_comment_cat_success = "<font color=green>Comment Category deleted.</font>\n";
$i_con_msg_delete_comment_cat_failed = "<font color=red>Failed to delete Comment Category.</font>\n";
$i_con_msg_num_records_updated = "Record(s) Updated.";

// Menu
$eReportCard['Management'] = "Management";
$eReportCard['Management_Schedule'] = "Schedule";
$eReportCard['Management_Progress'] = "Progress";
$eReportCard['Management_MarksheetRevision'] = "Marksheet Revision";
$eReportCard['Management_MarksheetSubmission'] = "Marksheet Submission";
$eReportCard['Management_ClassTeacherComment'] = "Class Teacher's Comment";
$eReportCard['Management_ConductMark'] = "Conduct Mark"; // for li sing tai hang use only
$eReportCard['Management_OtherInfo'] = "Other Info";
$eReportCard['Management_OtherStudentInfo'] = "Other Student Info"; // for SIS use only
$eReportCard['Management_DataHandling'] = "Data Handling";

$eReportCard['Reports'] = "Reports";
$eReportCard['Reports_GenerateReport'] = "Generate & Print Report Card";
$eReportCard['Reports_GrandMarksheet'] = "Grand Marksheet";
$eReportCard['Reports_OtherReports'] = "Other Reports";
$eReportCard['Reports_ClassHonourReport'] = "Honour Report";
$eReportCard['Reports_SubjectPrizeReport'] = "Subject Prize Report";
$eReportCard['Reports_GPATop3Rank'] = "GPA Top3 Ranking";

$eReportCard['Statistics'] = "Statistics";

$eReportCard['Settings'] = "Settings";
$eReportCard['Settings_BasicSettings'] = "Basic Settings";
$eReportCard['Settings_CommentBank'] = "Comment Bank";
$eReportCard['Settings_SubjectsAndForm'] = "Subjects & Class Level";
$eReportCard['Settings_ReportCardTemplates'] = "Report Card Templates";
$eReportCard['Settings_ExcludeStudents'] = "Exclude Students from Ranking";

// Tabs
#$eReportCard['HighlightSettings'] = "Highlight";
$eReportCard['StyleSettings'] = "Style";
$eReportCard['MarkStorageAndDisplay'] = "Mark Storage & Display";
$eReportCard['AbsentAndExemptSettings'] = "Absent & Exempt";
$eReportCard['CalculationSettings'] = "Calculation";
$eReportCard['AccessSettings'] = "Access Settings";

$eReportCard['SubjectSettings'] = 'Subject Settings';
$eReportCard['FormSettings'] = 'Class Level Settings';

// Calculation settings
$eReportCard['Term'] = "Term";
$eReportCard['WholeYear'] = "Whole Year";

$eReportCard['Absent'] = "Absent";
$eReportCard['AbsentExcludeWeight'] = "Exclude the weight of the absent assessment";
$eReportCard['AbsentExcludeFullMark'] = "Exclude the full mark of the absent assessment";
$eReportCard['AbsentTreatAsZeroMark'] = "Treat the raw mark of the absent assessment as 0 mark";

$eReportCard['Exempt'] = "Exempt";
$eReportCard['ExemptExcludeWeight'] = "Exclude the weight of the exempted assessment ";
$eReportCard['ExemptExcludeFullMark'] = "Exclude the full mark of the exempted assessment";
$eReportCard['ExemptTreatAsZeroMark'] = "Treat the raw mark of the exempted assessment as 0 mark";

$eReportCard['CalculationMethod'] = "Calculation Method";
$eReportCard['ConsolidateMark'] = "Consolidate marks to weighted marks for subject records (excluding overall marks)";
$eReportCard['AdjustFullMark'] = "Adjust Full mark of each subject to 100 before applying weight";

$eReportCard['CalculationOrder'] = "Calculation Order";
$eReportCard['UseTermMarkFromAll'] = "Use term subject mark calculated from all components";
$eReportCard['UseTermAvgMarkFromAll'] = " Use term average mark calculated from all subjects";
$eReportCard['RightDown'] = "Right-down (R-D)";
$eReportCard['DownRight'] = "Down-right (D-R)";
$eReportCard['TermRightDown'] = "Calculate term subject average for each subject first";
$eReportCard['TermDownRight'] = "Calculate subject component averages across all subjects first";
$eReportCard['YearRightDown'] = "Calculate whole-year average for each subject first";
$eReportCard['YearDownRight'] = "Calculate term averages across all subjects first";
$eReportCard['TermPreviewNotApplicable'] = "The Preview function is not applicable for the report which calculates subject component averages across all subjects first";
$eReportCard['YearPreviewNotApplicable'] = "The Preview function is not applicable for the report which calculates term averages across all subjects first";

// Highlight
$eReportCard['Style'] = "Style";
$eReportCard['Format'] = "Format";
$eReportCard['SpecialSymbol'] = "Special Symbol";
$eReportCard['Sample'] = "Sample";
$eReportCard['Mark'] = "Mark";

// Mark Storage & Display 
$eReportCard['AssessmentMark'] = "Assessment Mark";
$eReportCard['SubjectOverallMark'] = "Subject Overall Mark";
$eReportCard['OverallAverage'] = "Overall Average";
$eReportCard['OverallTotal'] = "Overall Total";
$eReportCard['Integer'] = "Integer";
$eReportCard['OneDecimalPlace'] = "1 decimal place";
$eReportCard['TwoDecimalPlaces'] = "2 decimal places";

//Access Settings
$eReportCard['AllowClassTeacherUploadCSV'] = "Allow Class Teacher upload CSV file to Other Info";
$eReportCard['AllowTeacherAccessGrandMS'] = "Allow Teacher access Grand Marksheet";

// Comment Bank
$eReportCard['Subject'] = "Subject";
$eReportCard['Class'] = "Class";
$eReportCard['Code'] = "Code";
$eReportCard['CommentContent'] = "Comment Content";
$eReportCard['CommentContentChi'] = "Comment Content (Chinese)";
$eReportCard['CommentContentEng'] = "Comment Content (English)";
$eReportCard['Category'] = "Category";
$eReportCard['Type'] = "Type";
$eReportCard['AllTeachers'] = "All Teachers";
$eReportCard['ClassTeacher'] = "Class Teacher";
$eReportCard['SubjectTeacher'] = "Subject Teacher";
$eReportCard['AllCategories'] = "All Categories";
$eReportCard['AlertEnterCommentCode'] = "Please enter the Code";
$eReportCard['AlertDuplicateCommentCode'] = "The Code is already exist, please enter a new one.";
$eReportCard['AlertEnterCommentCategory'] = "Please enter the Category";
$eReportCard['AlertEnterCommentContent'] = "Please enter the Comment Content";
$eReportCard['AlertSelectFile'] = "Please select a file first";
$eReportCard['DeleteSelectedCategory'] = "Delete Selected Category";
$eReportCard['AlertDeleteSelectedCategory'] = "Delete selected category will also remove the category proporty of all comment associated to it. Procced deleting?";
$eReportCard['SelectComment'] = "Select Comment";
$eReportCard['CommentList'] = "Comment List";
$eReportCard['Language'] = "Language";

// Grading Differentiation
$eReportCard['Distinction'] = 'Distinction';
$eReportCard['Pass'] = 'Pass';
$eReportCard['Fail'] = 'Fail';

$eReportCard['Removing'] = 'Removing';
$eReportCard['Saving'] = 'Saving';

$eReportCard['Grade'] = 'Grade';
$eReportCard['SelectScheme'] = 'Select Scheme';
$eReportCard['GradePoint'] = "Grade Point";
$eReportCard['GradingRange'] = 'Grading Range';
$eReportCard['GradingScheme'] = 'Grading Scheme';
$eReportCard['GradingTitle'] = "Grading Title";
$eReportCard['GradingType'] = "Grading Type";
$eReportCard['HonorBased'] = "Honour Based";
$eReportCard['UpperLimit'] = "Upper Limit";
$eReportCard['LowerLimit'] = "Lower Limit";
$eReportCard['InputScaleType'] = 'Input Scale';
$eReportCard['PassFailBased'] = "Pass/Fail Based";
$eReportCard['ResultDisplayType'] = 'Display Result';
$eReportCard['SchemesFullMark'] = "Full Mark";
$eReportCard['StudentsFromTop'] = "Students<br>From Top";
$eReportCard['StudentsFromTop_nowrap'] = "Students From Top";
$eReportCard['Mark'] = 'Mark';
$eReportCard['MarkRange'] = 'Mark Range';
$eReportCard['Next'] = 'Next';
$eReportCard['SchemesPassingMark'] = 'Passing Mark';
$eReportCard['PercentageRangeForAllStudents'] = '% Range for all students';
$eReportCard['PercentageRangeForPassedStudents'] = '% Range for passed students';

$eReportCard['AddMore'] = "Add More";
$eReportCard['CopyFrom'] = "Copy from";
$eReportCard['Sift'] = 'Sift';
$eReportCard['ClickToSift'] = 'Click to Sift';
$eReportCard['ClickToUnsift'] = 'Click to Unsift';
$eReportCard['jsAlertNoSubjectSelect'] = "No subject is selected in this Form";
$eReportCard['MoveUp'] = 'Move Up';
$eReportCard['MoveDown'] = 'Move Down';
$eReportCard['NextForm'] = 'Next Class Level';
$eReportCard['PreviousForm'] = 'Previous Class Level';

$eReportCard['jsAlertNoSubjectAssigned'] = 'No Class Level Settings available for copying';

// added on 24 Apr 08
$eReportCard['CopyClassLevelSettingsResult'] = 'Class Level Settings Copy Result';
$eReportCard['GradingSchemeExist'] = 'The Grading Scheme has been assigned.';
$eReportCard['SubmitMarksheetConfirm'] = "Results in this page will be saved. Are you sure you want to continue?";

$eReportCard['RemoveSubMarksheetColumn'] = "Results in this column will be deleted together. Are you sure you want to delete this assessment?";

// ReportCard Template
$eReportCard['InputBasicInformation'] = "Input Basic Information";
$eReportCard['InputSubjectWeight'] = "Input Subject Weight";
$eReportCard['InputOverallSubjectWeight'] = "Input Overall Subject Weight";
$eReportCard['InputAssessmentRatio'] = "Input Assessment Ratio";
$eReportCard['AssessmentRatio'] = "Assessment ratio";
$eReportCard['Ratio'] = "Ratio";
$eReportCard['InputTermsWeight'] = "Input Terms Weight";
$eReportCard['InputTermsRatio'] = "Input Terms Ratio";
$eReportCard['InputAssessmentWeight'] = "Input Assessment Weight";
$eReportCard['InputReportDisplayInformation'] = "Input Report Display Information";
$eReportCard['Finishandpreview'] = "Finish and preview";
$eReportCard['ReportTitle'] = "Report Title";
$eReportCard['SelectTerm'] = "Select Term";
$eReportCard['TermReport'] = "Term Report";
$eReportCard['WholeYearReport'] = "Consolidated Report";

$eReportCard['ShowAllAssessments'] = "Show All Assessments";
$eReportCard['ShowTermTotalOnly'] = "Show Term Total only";
$eReportCard['ReportType'] = "Report Type";
$eReportCard['SettingUpdateReminder'] = "Setting is updated. Please remember to re-generate it again.";
$eReportCard['NewReport'] = "New Report";
$eReportCard['SkipStep'] = "Are you sure skip this step?";
$eReportCard['RatioBetweenAssesments'] = "Ratio Between Assessments";
$eReportCard['Use'] = "Use";
$eReportCard['MoveLeft'] = "Move to left";
$eReportCard['MoveRight'] = "Move to right";
$eReportCard['NoTermSelected'] = "No Term is selected at this momnent.";
$eReportCard['NoAssesment'] = "No Assessment at this momnent.";
$eReportCard['AddTerm'] = "Add Term";
$eReportCard['AddAssesment'] = "Add Assessment";
$eReportCard['AtBegin'] = "at the beginning";
$eReportCard['NoSubjectSettings'] = "No Subject settings.";
$eReportCard['ColumnTitle'] = "Assessment title";
$eReportCard['MissingDefaultWeight'] = "Missing default weight.";
$eReportCard['DefaultWeightNumeric'] = "The default weight should be in numeric.";
$eReportCard['MissingWeight'] = "Missing weight.";
$eReportCard['WeightNumeric'] = "The weight should be in numeric.";
$eReportCard['SubjectTotleNot100'] = "The subject total weight is not 100%.  Continue?";
$eReportCard['InsertAfter'] = "Insert After";
$eReportCard['ShowPosition'] = "Show Position";
$eReportCard['DonotShowPosition'] = "Do not Show Position";
$eReportCard['ShowPositionInClass'] = "Show Position in Class";
$eReportCard['ShowPositioninClassLevel'] = "Show Position in Class Level";
$eReportCard['AssignToAll'] = "Assign to all";
$eReportCard['OverallSubjectWeight'] = "Overall Subject Weight";
$eReportCard['MissingData'] = "Missing data.";
$eReportCard['RatioNumeric'] = "The ratio should be in numeric.";
$eReportCard['RatioSumNot100'] = "The sum of ratio is not 100%.  Continue?";
$eReportCard['LineHeight'] = "Line Height";
$eReportCard['Header'] = "Header";
$eReportCard['WithDefaul Header'] = "With Default Header";
$eReportCard['NoHeader'] = "No Header.";
$eReportCard['NumberOfEmptyLine'] = "Number of empty line";
$eReportCard['Footer'] = "Footer";
$eReportCard['DisplayStudentInformation'] = "Display Student Information";
$eReportCard['DisplayColumn'] = "Display Column";
$eReportCard['ShowSubjectFullMark'] = "Show Subject Full Mark";
$eReportCard['ShowSubjectOverall'] = "Show Subject Overall";
$eReportCard['DisplayOverallResult'] = "Display Overall Result";
$eReportCard['DisplayGrandAvg'] = "Display Average Mark";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowClassNumOfStudent'] = "Show Number of Students in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";
$eReportCard['ShowFormNumOfStudent'] = "Show Number of Students in Form";
# added on 01 Jan 2008
$eReportCard['MaximumPosition'] = "Maximum Position to be shown";
$eReportCard['MaximumPositionRemarks'] = "(all positions will be shown if this value is set to 0)";

$eReportCard['CommentFrom'] = "Comment From";
$eReportCard['ClassTeache'] = "Class Teacher";
$eReportCard['SubjectTeacher'] = "Subject Teacher";
$eReportCard['Success'] = "Success";
$eReportCard['SuccessWithMissing'] = "Success - some subject(s) is missing";
$eReportCard['TemplateExists'] = "Template is already exists.";
$eReportCard['CopyTo'] = "Copy to";
$eReportCard['CopyFrom'] = "Copy from";
$eReportCard['CopyResult'] = "Report Card Templates Copy Result";
$eReportCard['SubmissionEndDateNotReached'] = "Submission End Date has not reached yet.";
$eReportCard['VerificationEndDateNotReached'] = "Verification End Date has not reached yet.";
$eReportCard['ClassCommentNotCompleted'] = "Class Comment is not completed.";
$eReportCard['MSNotCompleted'] = "Marksheet is not completed.";
$eReportCard['TemplatePreview'] = "Template Preview";
$eReportCard['Generate'] = "Generate";
$eReportCard['Generation'] = "Generation";
$eReportCard['ManualAdjustment'] = "Manual Adjustment";
$eReportCard['LastGenerate'] = "Last Generate";
$eReportCard['LastAdjust'] = "Last Adjust";
$eReportCard['AdjustmentList'] = "Adjustment List";
$eReportCard['LastPrint'] = "Last Print";
$eReportCard['MarkModifiedOn'] = "Mark has been modified on";
$eReportCard['LastGenerationDate'] = "Last Generation Date";
$eReportCard['LastTransferDate'] = "Last Transfer Date";
$eReportCard['Semester'] = "Semester";
$eReportCard['Transfer'] = "Transfer";
$eReportCard['AllTypes'] = "All Types";

// Schedule
$eReportCard['AllSemesters'] = "All Semesters";
$eReportCard['AllReportCards'] = "All Report Cards";
$eReportCard['AllForms'] = "All Class Levels";
$eReportCard['ReportTitle'] = "Report Title";
$eReportCard['Submission'] = "Submission";
$eReportCard['Verification'] = "Verification";
$eReportCard['IssueDate'] = "Issue Date";
$eReportCard['Start'] = "Start";
$eReportCard['End'] = "End";
$eReportCard['NoSchedule'] = "No Schedules";
$eReportCard['SubmissionPeriod'] = "Submission Period";
$eReportCard['VerificationPeriod'] = "Verification Period";
$eReportCard['AlertComparePeriod'] = "Start Date must not be greater than End Date!";
$eReportCard['AlertNoSubStartDate'] = "Start Date of Submission Period is empty or invalid, please enter again.";
$eReportCard['AlertNoIssueDate'] = "Issue Date is empty or invalid, please enter again.";
$eReportCard['AlertCompareSubToVer'] = "Verification Period must not be earlier than Submission Period!";

//Other Info
########### Need to be added if more categories are required ###########
$eReportCard['SummaryInfoUpload'] = "Summary Info";
$eReportCard['AwardRecordUpload'] = "Awards Record";
$eReportCard['MeritRecordUpload'] = "Merits & Demerits Record";
$eReportCard['ECARecordUpload'] = "ECA Record";
$eReportCard['RemarkUpload'] = "Remark";
$eReportCard['InterSchoolCompetitionUpload'] = " Inter-school Competitions Record";
$eReportCard['SchoolServiceUpload'] = "School Service Record";
$eReportCard['AttendanceUpload'] = "Attendance Record";
$eReportCard['AdditinalCommentUpload'] = "Additional Comment";
$eReportCard['DemeritUpload'] = "Punishment Record";
$eReportCard['OLEUpload'] = "OLE Record";


########################################################################
$eReportCard['File'] = "File";
$eReportCard['AllSchoolYears'] = "All School Years";
$eReportCard['AllTerms'] = "All Terms";
$eReportCard['AllClasses'] = "All Classes";
$eReportCard['FileRemoveConfirm'] = "Are you sure you want to remove the file?";
$eReportCard['FileRemoveAllConfirm'] = "Are you sure you want to remove all files?";
$eReportCard['NoClass'] = "No Class";
$eReportCard['NoSemester'] = "No Semester";
$eReportCard['NoForm'] = "No Class Level";
$eReportCard['Form'] = "Class Level";
$eReportCard['FormName'] = "Form";
$eReportCard['AlertExistFile'] = "File already exist for the same class on the same semester.\\nOverwrite it with the new file?";

// MarkSheet Revision & Edit
$eReportCard['Marksheet'] = "Marksheet";
$eReportCard['TeacherComment'] = "Teacher Comment";
$eReportCard['Feedback'] = "Feedback";
$eReportCard['LastModifiedDate'] = "Last Modified Date";
$eReportCard['LastModifiedBy'] = "Last Modified By";
$eReportCard['Confirmed'] = "Completed";
$eReportCard['Total'] = "Total";
$eReportCard['RawMarks'] = "Raw Marks";
$eReportCard['WeightedMarks'] = "Weighted Marks";
$eReportCard['ConvertedGrade'] = "Converted Grade";
$eReportCard['InputMarks'] = "Input Marks";
$eReportCard['PreviewMarks'] = "Preview Marks";
$eReportCard['PreviewGrades'] = "Preview Grades";
$eReportCard['Import'] = "Import";
$eReportCard['Export'] = "Export";
//$eReportCard['MarkRemind'] = "Remark: \"abs\" stands for absent; \"/\" for no enrollment or exempt.";
$eReportCard['MarkRemindSet1'] = "Remark: \"+\" stands for absent (zero mark); \"-\" for absent (not considered); \"*\" for dropped; \"/\" for exempt; \"N.A.\" for not assessed.";
$eReportCard['MarkRemindSet2'] = "Remark: \"abs\" stands for absent; \"*\" for dropped; \"/\" for exempt; \"N.A.\" for not assessed.";
$eReportCard['SubMS'] = "Sub-MS";
$eReportCard['SubMarksheet'] = "Sub-Marksheet";
$eReportCard['Student'] = "Student";
$eReportCard['StudentNo_short'] = "Class No";
$eReportCard['TermSubjectMark'] = "Term Subject Mark";
$eReportCard['OverallSubjectMark'] = "Overall Subject Mark";
$eReportCard['CmpSubjectMarksheet'] = "Component Subject Marksheet";
$eReportCard['CmpSubjects'] = "Component Subjects";

$eReportCard['TeachersComment'] = "Teacher's Comment";
$eReportCard['Period'] = "Period";

$eReportCard['NotCompleted'] = "Not Completed";

$eReportCard['ImportMarks'] = "Import Marks";
$eReportCard['ExportMarks'] = "Export Marks";
$eReportCard['DownloadCSVFile'] = "Download CSV file template";
$eReportCard['CSVMarksheetTemplateReminder'] = "In the CSV file template, any input mark under the column, which column title contains (N/A), will be ignored when processing.";

$eReportCard['RemarkAbsent'] = "Absent";
$eReportCard['RemarkAbsentZeorMark'] = "+ : Absent (zero mark)";
$eReportCard['RemarkAbsentNotConsidered'] = "- : Absent (not considered)";
$eReportCard['RemarkDropped'] = "* : Dropped";
$eReportCard['RemarkExempted'] = "/ : Exempted";
$eReportCard['RemarkNotAssessed'] = "N.A. : Not Assessed";

$eReportCard['jsFillGradingSchemeTitle'] = 'Please fill in the Grading Scheme Title.';
$eReportCard['jsConfirmDeleteGradingScheme'] = 'Are you sure to delete the selected grading scheme. It would affect other subjects which have used this scheme.';
$eReportCard['jsCheckDisplayResult'] = 'The Type of Display Result of %1$s is not valid as the type of Input Scale is Grade.';
$eReportCard['jsSthMustBePositive'] = 'The %1$s must be positive';
$eReportCard['jsSthMustBeNumericalNumber'] = 'The %1$s must be in form of numerical number.';
$eReportCard['jsSthCannotBeLargerThanFullMark'] = 'The %1$s cannot be larger than the full mark of grading scheme.';
$eReportCard['jsInvalidSth'] = 'Invalid %1$s';
$eReportCard['jsCheckTotalUpperLimit'] = 'The total of UpperLimit should be equal to 100%.';
$eReportCard['jsCheckOverallSubjectWeightedMark'] = 'The Overall Weighted Subject Mark cannot be larger than the full mark.';
# added on 15 Dec 2008
$eReportCard['jsSthCannotBeLessThanPassingMark'] = 'The %1$s cannot be less than the passing mark of grading scheme.';
# added on 16 Dec 2008
$eReportCard['jsLowerLimitCannotBeTheSame'] = 'The lower limits cannot be the same with each other.';


# added on 29 Apr 2008
$eReportCard['jsAlertToCompleteMarksheet'] = 'Are you sure to complete marksheet(s)?';
$eReportCard['jsAlertToIncompleteMarksheet'] = 'The status of Marksheet will be changed to incomplete. Are you sure to process?';
$eReportCard['jsInputMarkInvalid'] = 'Invalid input mark.';
$eReportCard['jsInputMarkCannotBeNegative'] = 'The input mark cannot be negative.';
$eReportCard['jsInputMarkCannotBeLargerThanFullMark'] = 'The input mark cannot be larger than the full mark.';
$eReportCard['jsConfirmMarksheetToIncomplete'] = 'This Marksheet has already been completed, are you sure to update the marksheet? if Yes, it will change the status of marksheet to \"Not Completed\".';
$eReportCard['jsConfirmCmpSubjectMarksheetToIncomplete'] = 'Certain Component Marksheet(s) has/have already been completed, are you sure to update the marksheet? if Yes, it will change the status of marksheet to \"Not Completed\".';
$eReportCard['jsAlertDuplicateGradingTitle'] = 'The Grading Title is already exist, please enter a new title.';
# added on 6 Nov 2008
$eReportCard['jsCheckTotalUpperLimitDistinction&Pass'] = 'The total of UpperLimit of Distinction and Pass should be equal to 100%.';
$eReportCard['jsCheckTotalUpperLimitFail'] = 'The total of UpperLimit of Fail should be equal to 100%.';
# added on 25 Nov 2008
$eReportCard['jsMarkEditedFromSubMS'] = 'Marks were updated according to the Sub-Marksheet. Please click the "Save" button to confirm the changes.';

# added on 30 Apr 2008
// Marksheet Confirm Complete Status
$eReportCard['MarksheetNotCompleted'] = 'The Marksheet has not completed.';
$eReportCard['MarksheetOfTermReportNotCompleted'] = 'The Marksheet(s) of Term Report(s) has/have not completed.';
$eReportCard['MarksheetOfCmpSubjectNotCompleted'] = 'The Marksheet of Component Subject(s) has/have not completed.';

// Extra Info

######################

// Class Teacher's Comment
$eReportCard['ChangeAllTo'] = "Change All to";

// Progress
$eReportCard['NotSet'] = "Not set";
$eReportCard['SendNotification'] = "Send Notification";
$eReportCard['NotStartedYet'] = "Not Started Yet";
$eReportCard['InProgress'] = "In Progress";
$eReportCard['Feeback'] = "Feeback";
$eReportCard['SetToComplete'] = "Set to Complete";
$eReportCard['SetToNotComplete'] = "Set to not Complete";

// Common 
$eReportCard['SettingsNotCompleted'] = "Incomplete";
$eReportCard['SettingsCompleted'] = "Completed";
$eReportCard['SettingsProgress'] = "Setting Progress";


// Report Template
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['ClassIndexNo'] = "Class Index No.";
$eReportCard['Template']['StudentInfo']['StudentAdmNo'] = "Student Adm. No.";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassName'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "Class No.";
$eReportCard['Template']['StudentInfo']['StudentNo'] = "Student No.";
$eReportCard['Template']['StudentInfo']['ClassTeacher'] = "Class Teacher";
$eReportCard['Template']['StudentInfo']['AcademicYear'] = "Academic Year";
$eReportCard['Template']['StudentInfo']['DateOfIssue'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfBirth'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['SubjectEng'] = "Subject";
$eReportCard['Template']['SubjectChn'] = "科目";
$eReportCard['Template']['SubjectTeacherComment'] = "Subject Teacher's Comment";
$eReportCard['Template']['Ranking'] = "Ranking";
$eReportCard['Template']['ClassTeacher'] = "Class Teacher";
$eReportCard['Template']['Principal'] = "Principal";
$eReportCard['Template']['ParentGuardian'] = "Parent / Guardian";
$eReportCard['Template']['SchoolChop'] = "School Chop";
$eReportCard['Template']['TeacherSignature'] = "Teacher's Signature";
$eReportCard['Template']['PrincipalSignature'] = "Principal's Signature";
$eReportCard['Template']['ParentSignature'] = "Parent's Signature";
$eReportCard['Template']['SubjectOverall'] = "Subject Overall";
$eReportCard['Template']['FirstCombined'] = "First Combined";
$eReportCard['Template']['SecondCombined'] = "Second Combined";
$eReportCard['Template']['OverallCombined'] = "Overall";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['GrandTotal2'] = "總分";
$eReportCard['Template']['AverageMark'] = "Average Mark";
$eReportCard['Template']['AverageMark2'] = "平均分";
$eReportCard['Template']['Position'] = "Position";
$eReportCard['Template']['Position2'] = "排名";
$eReportCard['Template']['NoOfStudent'] = "No. of students";
$eReportCard['Template']['NoOfStudent2'] = "學生數目";
$eReportCard['Template']['Grade'] = "Grade";
$eReportCard['Template']['Mark'] = "Mark";
$eReportCard['Template']['Remarks'] = "Remarks";
$eReportCard['Template']['AchievementBand'] = "Achieve't Band";
$eReportCard['Template']['AchievementGrade'] = "Achieve't Grade";
$eReportCard['Template']['1SemestralAssessment'] = "< 1st Semestral Assessment >";
$eReportCard['Template']['2SemestralAssessment'] = "< 2nd Semestral Assessment >";
$eReportCard['Template']['Overall'] = "< --- --- --- Overall --- --- --- >";
$eReportCard['Template']['Attendance'] = "Attendance";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['Total'] = "Total";
$eReportCard['Template']['Percentage'] = "Percentage";
$eReportCard['Template']['Passed'] = "Passed";
$eReportCard['Template']['Failed'] = "Failed";
$eReportCard['Template']['Promoted'] = "Promoted";
$eReportCard['Template']['Retained'] = "Retained";
$eReportCard['Template']['Advanced'] = "Advanced";
$eReportCard['Template']['1Sem'] = "1st Sem ";
$eReportCard['Template']['2Sem'] = "2nd Sem";
$eReportCard['Template']['Assessment'] = "Assess't";
$eReportCard['Template']['CCA'] = "CCA";
$eReportCard['Template']['NAPFA'] = "NAPFA";
$eReportCard['Template']['SSPA_Footer'] = 
"注意:<br />
(一) 本表績分須呈教統局升中派位組，請特別留意該績分是否與試卷相同。<br />
(二) 本績分表只填寫考試分數，不包括常分在內。<br />
(三) 合計後如出現小數位，則將小數位四捨五入，音樂及視覺藝術為五分之差。
";
$eReportCard['Template']['SSPA_Signature'] = "教師署名：______________________";
$eReportCard['Template']['ClassPosition'] = "Position in Class";
$eReportCard['Template']['FormPosition'] = "Position in Level";
$eReportCard['Template']['GrandMarksheet'] = "Grand Marksheet";
$eReportCard['Template']['GrandAverage'] = "Grand Average";
$eReportCard['Template']['GrandAverageEn'] = "Grand Average";
$eReportCard['Template']['GrandAverageCh'] = "總平均分";
$eReportCard['Template']['Annual'] = "Annual";

// Other Student Info	# added on 28 Apr 2008
$eReportCard['OtherStudentInfo'] = 'Other Student Info';
$eReportCard['ChangeAllTo'] = "Change All to";
$eReportCard['OtherStudentInfo_Attendance'] = array(1=>'Regular', 2=>'Irregular');
$eReportCard['OtherStudentInfo_Conduct'] = array(4=>'Excellent', 3=>'Very Good', 2=>'Good', 1=>'Satisfactory');
$eReportCard['OtherStudentInfo_NAPFA'] = array(1=>'Nil', 2=>'Gold', 3=>'Silver', 4=>'Bronze', 5=>'Exempted');
$eReportCard['OtherStudentInfoValue_CIPHours'] = 'CIP Hours';
$eReportCard['OtherStudentInfoValue_Attendance'] = 'Attendance';
$eReportCard['OtherStudentInfoValue_Conduct'] = 'Conduct';
$eReportCard['OtherStudentInfoValue_NAPFA'] = 'NAPFA';
$eReportCard['OtherStudentInfoValue_CCARemarks'] = 'CCA Remarks';
$eReportCard['jsCIPHourCannotBeNegative'] = 'The CIP hours value cannot be negative.';
$eReportCard['jsCIPHourInvalid'] = 'Invalid CIP hours';

// Geneate Report
$eReportCard['jsConfirmToGenerateReport'] = 'Are you sure to generate report?';
$eReportCard['ReportPrinting'] = "Report Printing";
$eReportCard['Promotion'] = "Promotion";
$eReportCard['PromotionEdit'] = "Edit Promotion";
$eReportCard['NewClassLevel'] = "New Class Level";
$eReportCard['NewClassLevelAlert'] = "New Class Level must be two characters long";

// Grand Marksheet
$eReportCard['SelectCriteria'] = "Select Criteria";
$eReportCard['AllSubjects'] = "All Subjects";
$eReportCard['GrandMarksheetType'] = "Grand Marksheet Type";
$eReportCard['NoReportAvailable'] = "No Report Available";
$eReportCard['NoTermAvailable'] = "No Term Available";

//$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking", "Pupils' Progress");
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");
$eReportCard['GrandMarksheetTypeAlert1'] = "Pupils' Progress cannot be calculated for Report with only one term.";
# added on 15 Oct 2008
$eReportCard['ShowSummaryInfo'] = "Show Summary Info";
$eReportCard['ShowSubjectComponent'] = "Show Subject Component(s)";
$eReportCard['ShowGrandTotal'] = "Show Grand Total";
$eReportCard['ShowGrandAverage'] = "Show Grand Average";
$eReportCard['StudentNameDisplay'] = "Student Name Display";
$eReportCard['RankingDisplay'] = "Ranking Display";
# added on 16 Oct 2008
$eReportCard['For_SSPA'] = "For SSPA";
# added on 19 Dec 2008
$eReportCard['ViewFormat'] = "View Format";
$eReportCard['HTML'] = "HTML";
$eReportCard['CSV'] = "CSV";
$eReportCard['EnglishName'] = "English Name";
$eReportCard['ChineseName'] = "Chinese Name";
$eReportCard['ClassRanking'] = "Class Ranking";
$eReportCard['OverallRanking'] = "Overall Ranking";
# added on 22 Dec 2008
$eReportCard['Template']['Average'] = "Average";
$eReportCard['Template']['SD'] = "Standard Deviation";
$eReportCard['Template']['Variance'] = "Variance";
$eReportCard['Template']['HighestMark'] = "Highest Mark";
$eReportCard['Template']['LowestMark'] = "Lowest Mark";
$eReportCard['Template']['PassingRate'] = "Passing Rate";
# added on 24 Dec 2008
$eReportCard['ShowStatistics'] = "Show Statistics";
$eReportCard['Formula'] = "Formula";



// Data Handling
$eReportCard['DataTransition'] = "Data Transition";
$eReportCard['DataDeletion'] = "Data Deletion";
$eReportCard['DataTransfer_To_iPortfolio'] = "Data Transfer (to iPortfolio)";
$eReportCard['DataTransitionWarning1'] = "Please proceed this action before:<br />- start using eReportCard for a new academic year, or,<br />- restore eReportCard to use the data of previous academic years";
$eReportCard['DataTransitionWarning2'] = "The data from the current active academic year will be preserved.";
$eReportCard['ActiveAcademicYear'] = "Active Academic Year";
$eReportCard['CurrentAcademicYear'] = "Current Academic Year";
$eReportCard['Transition'] = "Transition";
$eReportCard['NewAcademicYear'] = "New academic year";
$eReportCard['OtherAcademicYears'] = "Other academic years";
$eReportCard['ConfirmTransition'] = "Proceed to do selected data transition?";
$eReportCard['DataDeletionWarning'] = "This process will remove selected report card data (including marks and teacher comments).";
$eReportCard['ConfirmDeletion'] = "Proceed to delete selected report card data?";

$button_promotion = "Promotion";
$button_finish_preview = "Finish and Preview";

$eReportCard['DisplayOverallResult'] = "Display Overall Result";
$eReportCard['DisplayGrandAvg'] = "Display Average Mark";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";

# added on 09 Dec 2008
$eReportCard['ManualAdjustMarkInstruction'] = "This page is optional. If you do not input the subject class position for your students, the system will generate the class position according to the marks which you have submitted.";

# added on 5 Jan 2008
$eReportCard['SubmissionStartDate'] = "Submission Start Date";
$eReportCard['SubmissionEndDate'] = "Submission End Date";

# added on 3 Feb 2009
$eReportCard['FillAllEmptyMarksWithNA'] = "Fill all empty marks with N.A.";
$eReportCard['TotalRecords'] = "Total";

# added on 27 Apr 2009
$eReportCard['Warning'] = "Warning";
$eReportCard['SubjectConductMarkWarning'] = "eReportCard System will display the finalized conduct grade, instead of the converted conduct grade, in the report card once you have submitted this page.";

// Data Handling (to iPortfolio)
$eReportCard['jsConfirmToTransferToiPortfolio'] = 'Are you sure to transfer the marks to iPortfolio?';
$eReportCard['CurrentAcadermicYear'] = 'Current Acadermic Year';

# 20090627 yatwoon
$eReportCard['NoFormSetting'] = 'There is no Form setting at the moment.';
$eReportCard['orAbove'] = 'or Above';
$eReportCard['AllPass'] = 'All Pass';

// Honour Report
$eReportCard['GPA'] = 'GPA';
$eReportCard['Honour'][1] = 'First Class Honour';
$eReportCard['Honour'][2] = 'Second Class Honour (Division 1)';
$eReportCard['Honour'][3] = 'Second Class Honour (Division 2)';

$eReportCard['AwardName'] = 'Award Name';
$eReportCard['StudentNameEn'] = 'Student Name (Eng)';
$eReportCard['StudentNameCh'] = 'Student Name (Chi)';
$eReportCard['Criteria'] = 'Criteria';
$eReportCard['Template']['GPA'] = 'GPA';




#############################################################
?>
