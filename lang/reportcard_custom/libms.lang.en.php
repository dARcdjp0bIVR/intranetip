<?php
# being modified by: Yuen

/******************************************* Changes log **********************************************
 *
 * IP 2.5 VERSION
 * 
 ******************************************************************************************************/


$Lang['libms']['library'] = "eLibrary <i>plus</i>";

$Lang['libms']['SQL']['UserNameFeild'] = "EnglishName";


#######################################################################//sqlfeild####
$Lang["libms"]["sql_field"]["Description"] = "DescriptionEn";
$Lang["libms"]["sql_field"]["User"]["name"] = "EnglishName";
#################################################
$Lang["libms"]["library"] = "eLibrary <i>plus</i>";
$Lang["libms"]["status"]["na"] = "N/A";
$Lang["libms"]["general"]["new"] = "New";
$Lang["libms"]["general"]["edit"] = "Edit";
$Lang["libms"]["general"]["del"] = "Delete";
$Lang["libms"]["general"]["auto"] = "AUTO";
$Lang["libms"]["general"]["customize"] = "Customize";
$Lang["libms"]["general"]["select"] = "Select";
$Lang["libms"]["management"]["title"] = "Management";
$Lang["libms"]["bookmanagement"]["book_list"] = "Book";
$Lang['libms']['bookmanagement']['item_list'] = "Item";
$Lang["libms"]["bookmanagement"]["book_import"] = "Import";
$Lang["libms"]["bookmanagement"]["book_label"] = "Label";
$Lang["libms"]["bookmanagement"]["book_labelformat"] = "Label Paper";
$Lang['libms']['bookmanagement']['book_reserve'] = "Current Reservation";
$Lang['libms']['bookmanagement']['book_details'] = "Book Details";
$Lang['libms']['bookmanagement']['circulation_details'] = "Circulations";
$Lang['libms']['bookmanagement']['purchase_details'] = "Purchases";
$Lang['libms']['bookmanagement']['ebook_tags'] = "eBook Tags";
$Lang['libms']['bookmanagement']['ebook'] = "eBook";
$Lang['libms']['bookmanagement']['expired_book_reserve'] = "Expired Reservation";

$Lang['libms']['batch_edit']['menu'] = "Batch Edit";
$Lang['libms']['batch_edit']['edit_return_date'] = "Due Date";
$Lang['libms']['batch_edit']['edit_penal_sum'] = "Penalty Sum";
$Lang['libms']['batch_edit']['edit_book_info'] = "Book Info";
$Lang['libms']['batch_edit']['edit_tags'] = "Tags";
$Lang['libms']['batch_edit']['edit_subject'] = "Subject";
$Lang['libms']['import_book_cover'] = "Import Book Cover";

$Lang['libms']['batch_edit']['edit_book_info_remark'] = "leave it blank for no change";

$Lang['libms']['batch_edit']['edit_return_date_same'] = "Change to a particular date";
$Lang['libms']['batch_edit']['edit_return_date_defer'] = "Defer for";
$Lang['libms']['batch_edit']['edit_return_date_early'] = "To advance of";
$Lang['libms']['batch_edit']['record_found'] = " %s loan record(s) have/has been found,";
$Lang['libms']['batch_edit']['modify_due_date'] = "Change the Due Date to";
$Lang["libms"]["batch_edit"]["Day"] = "Day(s)";
$Lang["libms"]["batch_edit"]["send_email"] = "Send Notification Email";
$Lang["libms"]["batch_edit"]["send_email_yes"] = "Yes";
$Lang["libms"]["batch_edit"]["send_email_no"] = "No";
$Lang["libms"]["batch_edit"]["DueDate"] = "Due Date";
$Lang["libms"]["batch_edit"]["Bak_DueDate"] = "Original Due Date";
$Lang['libms']['batch_edit']['record_updated'] = " %s loan record(s) have/has been updated";
$Lang['libms']['batch_edit']['PleaseSelectGroup'] = "Please select at least one group";
$Lang['libms']['batch_edit']['PleaseSelectClass'] = "Please select at least one class";
$Lang['libms']['batch_edit']['PleaseSelectDateAfterToday'] = "Please enter the date on or after today";

$Lang['libms']['import_format'] = "Data Format";
$Lang['libms']['import_marc21'] = "MARC 21";
$Lang['libms']['import_CSV'] = "CSV";
$Lang['libms']['import_TXT'] = "TXT";


$Lang['libms']['import_book_cover_file'] = "Cover Image";
$Lang['libms']['import_book_cover_file_types'] = "(jpg / png / zip)";
$Lang['libms']['import_book_cover_file_remark'] = "Image filename must be the book's ACNO (e.g. bok002138.jpg). To upload multiple files at one time, please use zip.";
$Lang['libms']['select_image_file'] = "Select JPG/PNG/ZIP file";
$Lang['libms']['invalid_acno'] = "ACNO is not found";
$Lang['libms']['correct'] = "Correct";
$Lang['libms']['invalid_book_cover_file_types'] = "Please select Image(.jpg,.png) or Zip(.zip) file";

## Stock Take
$Lang['libms']['stocktake_status']['FOUND'] = "Found";
$Lang['libms']['stocktake_status']['WRITEOFF'] = "Write-off";
$Lang['libms']['stocktake_status']['NOTTAKE'] = "Not Take";
$Lang['libms']['bookmanagement']['ImportTXT'] = "TXT Scoure File";
$Lang['libms']['bookmanagement']['TXTFormat'] = "(.txt file)";
$Lang['libms']['bookmanagement']['LastFive'] = "Last Five Record";
$Lang['libms']['bookmanagement']['Relocate'] = "Relocate";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample1'] = "Click here to download sample1";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample2'] = "Click here to download sample2";

$Lang['libms']['bookmanagement']['Reason'] = "Reason";
$Lang['libms']['bookmanagement']['WriteOffDate'] = "Write-off Date";
$Lang["libms"]["bookmanagement"]["ImportStockTake"] = array("<font color = red>*</font>Barcode","New Location");
$Lang["libms"]["bookmanagement"]["ImportWriteOff"] = array("<font color = red>*</font>Barcode","Write-off Date","Reason");
$Lang["libms"]["bookmanagement"]["BarcodeNotExist"] = "Barcode is not exist";
$Lang["libms"]["bookmanagement"]["import"] = "Import";

$Lang['libms']['bookmanagement']['stocktakeAndWriteOff'] = "Stock-take & Write-off";
$Lang['libms']['bookmanagement']['stock-take'] = "Stock-take";
$Lang['libms']['bookmanagement']['stocktakeRecord'] = "Stock-take Record";
$Lang['libms']['bookmanagement']['location'] = "Location";
$Lang['libms']['bookmanagement']['total'] = "Total";
$Lang['libms']['bookmanagement']['progress'] = "Progress";
$Lang['libms']['bookmanagement']['lastRecord'] = "Last record";
$Lang['libms']['bookmanagement']['lastTakenBy'] = "Last taken-by";
$Lang['libms']['bookmanagement']['overall'] = "Overall";

$Lang['libms']['bookmanagement']['stocktakeList'] = "Stock-take List";
$Lang['libms']['bookmanagement']['barcode'] = "Barcode";
$Lang['libms']['bookmanagement']['bookTitle'] = "Book Title";
$Lang['libms']['bookmanagement']['bookStatus'] = "Book Status";
$Lang['libms']['bookmanagement']['result'] = "Result";
$Lang['libms']['bookmanagement']['originalLocation'] = "Original Location";
$Lang['libms']['bookmanagement']['relocate'] = "Relocate to here";
$Lang['libms']['bookmanagement']['EnterOrScanItemBarcode'] = "Enter or scan item barcode";
$Lang['libms']['bookmanagement']['BookLocationUpdateFailed'] = "Book location update failed";
$Lang['libms']['bookmanagement']['BookLocationUpdateSuccessfully'] = "Book Location update successfully";
$Lang['libms']['bookmanagement']['BookLocationUpdated'] = "Updated";
$Lang['libms']['bookmanagement']['SelectAnotherLocation'] = "Select another location";
$Lang['libms']['bookmanagement']['StocktakeStatus'] = "Stock-take Status"; 
$Lang['libms']['bookmanagement']['scope'] = "Scope"; 
$Lang['libms']['bookmanagement']['allBooks'] = "All Books";
$Lang['libms']['bookmanagement']['selectedBooks'] = "Selected Books";
$Lang['libms']['bookmanagement']['lastStockTake'] = "Last stock-take";
$Lang['libms']['bookmanagement']['writeOff'] = "Write-off";
$Lang['libms']['bookmanagement']['ConfirmWriteOff'] = "Do you confirm to write-off the book(s)?";
$Lang['libms']['bookmanagement']['cancelWriteOff'] = "Cancel Write-off";
$Lang['libms']['bookmanagement']['WriteOffBy'] = "Write-off By";
$Lang['libms']['bookmanagement']['WriteOffTime'] = "Write-off Time";
$Lang['libms']['bookmanagement']['totalBorrow'] = "Loans";
$Lang['libms']['bookmanagement']['last_borrow_time'] = "Last Loan Date";
$Lang['libms']['bookmanagement']['last_borrow_user'] = "Last Loaned By";
$Lang['libms']['bookmanagement']['ClassName'] = "Class";
$Lang['libms']['bookmanagement']['ClassNumber'] = "Class Number";
$Lang['libms']['bookmanagement']['LoanDate'] = "Loan Date";
$Lang['libms']['bookmanagement']['DueDate'] = "Due Date";
$Lang['libms']['bookmanagement']['ReturnDate'] = "Return Date";
$Lang['libms']['bookmanagement']['deleted_user_legend'] = '<font style="color:red;">*</font> Representing that the person has been deleted or left already.';

$Lang['libms']['settings']['stocktake']['from']= "from";
$Lang['libms']['settings']['stocktake']['to']= "to";
$Lang['libms']['settings']['stocktake']['LastUpdated'] = "Last Updated";
$Lang['libms']['reporting']['date'] = "Date:";
$Lang['libms']['reporting']['dateRange'] = "Date Range";
$Lang['libms']['reporting']['stocktakeReport'] = "Stock-take Report";

### Stock Take End


$Lang['libms']["settings"]['notification']['title'] = "Notification";
$Lang['libms']["settings"]['notification']['reserve_email'] = "Notify user when the reserved book is available";
$Lang['libms']["settings"]['notification']['overdue_email'] = "Notify user when overdue happens";
$Lang['libms']["settings"]['notification']['due_date_reminder_email'] = "Notify user when due date is coming soon";
$Lang['libms']["settings"]['notification']['yes'] = "Yes";
$Lang['libms']["settings"]['notification']['no'] = "No";

$Lang['libms']["settings"]['system']['yes'] = "Yes";
$Lang['libms']["settings"]['system']['no'] = "No";
$Lang['libms']["settings"]['system']['max_overdue_limit'] = "Overdue Grace Period( -1 = No Limit ) ";
$Lang['libms']["settings"]['system']['override_password'] = "Override Password";
$Lang['libms']["settings"]['system']['global_lockout'] = "Global Lockout";
$Lang['libms']["settings"]['system']['batch_return_handle_overdue'] = "Batch Book Return Handles Overdues";
$Lang['libms']["settings"]['system']['max_book_reserved_limit'] = "Reserved Book Day limit (0 means unlimit)";
$Lang['libms']["settings"]['system']['school_display_name'] = "Display Name of the School";
$Lang['libms']["settings"]['system']['due_date_reminder'] = "Day(s) of the Email Reminder before Due Date (0 means not remind)";
$Lang['libms']["settings"]['system']['circulation_student_by_selection'] = "Using class and class number to select student in Circulation Management";
$Lang['libms']["settings"]['system']['circulation_duedate_method'] = "Counting school days only to calculate due date (\"No\" means holidays and non-open days will be counted)";

$Lang['libms']["settings"]['system']['circulation_duedate_method'] = "Due-date calculation"; //"以學校日子來計算還書日期（「否」代表計算假期及不開放的日子）";
$Lang['libms']["settings"]['system']['circulation_duedate_method_yes'] = "counting on opening days only";
$Lang['libms']["settings"]['system']['circulation_duedate_method_no'] = "counting on all days";

$Lang["libms"]["btn"]["enter"] = "Enter";

$Lang['libms']["settings"]['system']['overdue_epayment'] = "Fine Payment Method";

$Lang['libms']["settings"]['system']['overdue_epayment_a'] = "Cash";
$Lang['libms']["settings"]['system']['overdue_epayment_b'] = "via ePayment";

$Lang['libms']["settings_remark"]['global_lockout'] = "Only the administrator can access and use this module if Global Lockout is set";
$Lang['libms']["settings_alert"]['batch_return_handle_overdue'] = "Overdue return is disallowed here! <br />Please use <u>Circulation for a Patron</u>.";
$Lang['libms']["settings_alert"]['batch_return_handle_overdue_fail'] = "Failed";


$Lang["libms"]["settings"]["title"] = "Settings";
$Lang["libms"]["settings"]["basic"] = "Basic Settings";
$Lang["libms"]["settings"]["holidays"] = "Holiday";
$Lang["libms"]["settings"]["opentime"] = "Open Hour";
$Lang["libms"]["settings"]["PageSettingsSpecialOpenTime"] = "Special Time";
$Lang["libms"]["settings"]["circulation_type"] = "Circulation Type";
$Lang["libms"]["settings"]["resources_type"] = "Resources Type";
$Lang["libms"]["settings"]["book_category"] = "Book Category";
$Lang["libms"]["settings"]["book_location"] = "Location";
$Lang["libms"]["settings"]["responsibility"] = "Responsibility";
$Lang["libms"]["settings"]["stock-take"] = "Stock-take";

$Lang['libms']['settings']['msg']['duplicated'] = 'Duplicated Code';


$Lang["libms"]["reporting"]["title"] = "Reports";
$Lang["libms"]["reporting"]["span"]['table'] = "Table";
$Lang["libms"]["reporting"]["span"]['graph'] = "Graph";
$Lang["libms"]["reporting"]["PageReportingOverdue"] = "Overdue Notice";
$Lang["libms"]["reporting"]["PageReportingFine"] = "Penalty Report";
$Lang["libms"]["reporting"]["PageReportingCirculation"] = "Loan Report";
$Lang["libms"]["reporting"]["PageReportingReaderRanking"] = "Patron Ranking";
$Lang["libms"]["reporting"]["PageReportingReaderInactive"] = "Inactive Patron";
$Lang["libms"]["reporting"]["PageReportingBookRanking"] = "Book Ranking";

$Lang["libms"]["statistics"]["title"] = "Statistics";
$Lang["libms"]["reporting"]["PageReportingIntragration"] = "Summary";
$Lang["libms"]["reporting"]["PageReportingLending"] = "General Stats";
$Lang["libms"]["reporting"]["PageReportingBookReport"] = "Book Report";
$Lang['libms']['reporting']['PageReportingLostBookReport'] = "Lost Report";
$Lang['libms']['reporting']['PageReportingPenaltyReport'] = "Penalty Stats";
$Lang['libms']['reporting']['PageReportingCategoryReport'] = "Category vs Class/Form";
$Lang['libms']['reporting']['PageReportingCategoryReport1'] = "Statistics (by book category)";
$Lang['libms']['reporting']['PageReportingCategoryReport2'] = "Statistics (by subject)";
$Lang['libms']['reporting']['PageReportingCategoryReport3'] = "Statistics (by language)";
$Lang["libms"]["reporting"]["ReadingFrequency"] = "Frequency by Form";
$Lang['libms']['reporting']['stock-take'] = "Stock-take Report";
$Lang['libms']['reporting']['accessionReport'] = "Accession Report";

$Lang["libms"]["action"]["book"] = "Book Management";
$Lang["libms"]["action"]["new_book"] = "New book";
$Lang["libms"]["action"]["edit_book"] = "Edit book";

$Lang["libms"]["action"]["item"] = "Item Management";
$Lang["libms"]["action"]["new_item"] = "New Item";
$Lang["libms"]["action"]["edit_item"] = "Edit Item";

$Lang["libms"]["action"]["periodical"] = "Periodical Management";

$Lang["libms"]["open_time"]["not_open_by_group"] = "<p><font color='red' face='simhei'>Not allow to loan now!</font></p> This user can loan ";
$Lang["libms"]["open_time"]["not_open_yet"] = "<p><font color='red' face='simhei'>Not allow to loan!</font></p> Loan service is available ";
$Lang["libms"]["open_time"]["system_period"] = "Period (school year or term)";
$Lang["libms"]["open_time"]["system_period_remark"] = "If set, start date and last return date of circulations will be controlled.";
$Lang["libms"]["open_time"]["system_period_open"] = "starting on";
$Lang["libms"]["open_time"]["system_period_end"] = "till";
$Lang["libms"]["open_time"]["system_period_full"] = "";  # no word is needed in English version!


$Lang["libms"]["open_time"]["weekday"] = "Open Hours";
$Lang["libms"]["open_time"]["opentime"] = "From";
$Lang["libms"]["open_time"]["closetime"] = "To";
$Lang["libms"]["open_time"]["day1"] = "Monday";
$Lang["libms"]["open_time"]["day2"] = "Tuesday";
$Lang["libms"]["open_time"]["day3"] = "Wednesday";
$Lang["libms"]["open_time"]["day4"] = "Thursday";
$Lang["libms"]["open_time"]["day5"] = "Friday";
$Lang["libms"]["open_time"]["day6"] = "Saturday";
$Lang["libms"]["open_time"]["day7"] = "Sunday";
$Lang["libms"]["open_time"]["close"] = "Close";
$Lang["libms"]["open_time"]["open"] = "Open";
$Lang["libms"]["open_time"]["time_alertmsg"] = "24 hours format(HH:MM:SS)";
$Lang["libms"]["open_time"]["time_timemsg"] = "Invalid time format, please use 24 hours format(HH:MM:SS)!";
$Lang["libms"]["open_time"]["time_hourmsg"] = "Invalid hour";
$Lang["libms"]["open_time"]["time_minsmsg"] = "Invalid minute";
$Lang["libms"]["open_time"]["time_secmsg"] = "Invalid second";
$Lang["libms"]["S_OpenTime"]["editall"] = "Batch edit";
$Lang["libms"]["S_OpenTime"]["Date"] = "Date (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["DateFrom"] = "Start date (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["DateEnd"] = "End date (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["Time"] = " Time (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["OpenTime"] = "Open hour (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["CloseTime"] = "Close hour (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["close"] = "Close";
$Lang["libms"]["S_OpenTime"]["open"] = "Open";
$Lang["libms"]["S_OpenTime"]["dateformat"] = "Date format (YYYY-MM-DD)";
$Lang["libms"]["open_time"]["datemsg"] = "Invalid date format, please follow (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["yearmsg"] = "Invalid year";
$Lang["libms"]["S_OpenTime"]["monthsmsg"] = "Invalid month";
$Lang["libms"]["S_OpenTime"]["daymsg"] = "Invalid day";
$Lang["libms"]["S_OpenTime"]["remark"] = "<span class='tabletextrequire'>*</span> For special open/close setting, please go to 'Special Time'.";
$Lang["libms"]["open_time"]["time_alertmsg"] = "24 hours format(HH:MM:SS)";
$Lang["libms"]["holiday"]["Event"] = "Holiday";
$Lang["libms"]["holiday"]["DateFrom"] = "Start date";
$Lang["libms"]["holiday"]["DateEnd"] = "End date";
$Lang['libms']['holiday']['disclaimer'] = "<span class='tabletextrequire'>*</span> Refering to School Settings > School Calendar > Holidays, please contact eClass System Admin should you need further assistance.";
$Lang['libms']['holiday']['disclaimer_ej'] = "<span class='tabletextrequire'>*</span> Refering to eClass Administrative Console > Intranet > Admin Mgmt > School Calendar > Holidays, please contact eClass System Admin should you need further assistance.";

$Lang["libms"]["circulation_type"]["code"] = "Circulation Code";
$Lang["libms"]["circulation_type"]["description_en"] = "Title in English";
$Lang["libms"]["circulation_type"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["resources_type"]["code"] = "Resources Code";
$Lang["libms"]["resources_type"]["description_en"] = "Title in English";
$Lang["libms"]["resources_type"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["book_category"]["code"] = "Category Code";
$Lang["libms"]["book_category"]["description_en"] = "Title in English";
$Lang["libms"]["book_category"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["book_location"]["code"] = "Location Code";
$Lang["libms"]["book_location"]["description_en"] = "Title in English";
$Lang["libms"]["book_location"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["responsibility"]["code"] = "Responsibility Code";
$Lang["libms"]["responsibility"]["description_en"] = "Title in English";
$Lang["libms"]["responsibility"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["book"]["info"] = "Basic information";
$Lang["libms"]["book"]["info_category"] = "Categorization";
$Lang["libms"]["book"]["info_circulation"] = "Circulation";
$Lang["libms"]["book"]["info_publish"] = "Publishing details";
$Lang["libms"]["book"]["info_purchase"] = "Purchase details";
$Lang["libms"]["book"]["info_other"] = "Miscellaneous";
$Lang["libms"]["book"]["info_book"] = "Barcode";
$Lang["libms"]["book"]["code"] = "ACNO";
$Lang["libms"]["book"]["title"] = "Book title";
$Lang["libms"]["book"]["subtitle"] = "Book title 2";
$Lang["libms"]["book"]["introduction"] = "Description";
$Lang["libms"]["book"]["call_number"] = "Call number";
$Lang["libms"]["book"]["call_number_desc"] = "Classification Code";
$Lang["libms"]["book"]["call_number2"] = "Call number 2";
$Lang["libms"]["book"]["call_number2_desc"] = "Author Code";
$Lang["libms"]["book"]["ISBN"] = "ISBN";
$Lang["libms"]["book"]["ISBN2"] = "ISBN 2";
$Lang["libms"]["book"]["barcode"] = "Barcode";
$Lang["libms"]["book"]["language"] = "Language";
$Lang["libms"]["book"]["country"] = "Country";
$Lang["libms"]["book"]["edition"] = "Edition";
$Lang["libms"]["book"]["publisher"] = "Publisher";
$Lang["libms"]["book"]["publish_place"] = "Publication place";
$Lang["libms"]["book"]["publish_year"] = "Year of publishing";
$Lang["libms"]["book"]["number_of_page"] = "Number of pages";
$Lang["libms"]["book"]["number_of_copy"] = "Number of copies";
$Lang["libms"]["book"]["number_of_copy_available"] = "Available";
$Lang["libms"]["book"]["book_status"] = "Status";
$Lang["libms"]["book"]["purchase_date"] = "Date of purchase";
$Lang["libms"]["book"]["distributor"] = "Distributor";
$Lang["libms"]["book"]["purchase_note"] = "Note of purchase";
$Lang["libms"]["book"]["purchase_by_department"] = "Purchased by department";
$Lang["libms"]["book"]["list_price"] = "List price";
$Lang["libms"]["book"]["discount"] = "Discount";
$Lang["libms"]["book"]["purchase_price"] = "Purchase price";
$Lang["libms"]["book"]["account_date"] = "Account date";
$Lang["libms"]["book"]["subject"] = "Subject";
$Lang["libms"]["book"]["series"] = "Series";
$Lang["libms"]["book"]["series_number"] = "Series number";
$Lang["libms"]["book"]["responsibility_code1"] = "Responsibility ";
$Lang["libms"]["book"]["responsibility_by1"] = "Responsibility by ";
$Lang["libms"]["book"]["responsibility_code2"] = "Responsibility 2";
$Lang["libms"]["book"]["responsibility_by2"] = "Responsibility by 2";
$Lang["libms"]["book"]["responsibility_code3"] = "Responsibility 3";
$Lang["libms"]["book"]["responsibility_by3"] = "Responsibility by 3";
$Lang["libms"]["book"]["remark_internal"] = "Remark (for administrator)";
$Lang["libms"]["book"]["remark_to_user"] = "Note for circulation";
$Lang["libms"]["book"]["URL"] = "Relevant link";
$Lang["libms"]["book"]["cover_image"] = "Book cover";
$Lang["libms"]["book"]["borrow_reserve"] = "Allow";
$Lang["libms"]["book"]["open_borrow"] = "Loan";
$Lang["libms"]["book"]["open_reserve"] = "Reserve";
$Lang["libms"]["book"]["tags"] = "Tags";
$Lang["libms"]["book"]["tag_books"] = "Books";
$Lang["libms"]["book"]["last_modified"] = "Last Modified";
$Lang["libms"]["book"]["multiple_selection"] = "Multiple";
$Lang["libms"]["book"]["book_total"] = "book(s)";
$Lang["libms"]["book"]["warning_no_new_tag"] = "Please input a new tag!";
$Lang["libms"]["book"]["warning_tag_edit_confirm"] = "Are you sure you want to make the change?";
$Lang["libms"]["book"]["title_origin"] = "Taget tag(s)";
$Lang["libms"]["book"]["title_new"] = "New tag";
$Lang["libms"]["book"]["tags_input"] = "* please press [Enter] to seperate tags";
$Lang["libms"]["book"]["add"] = "Add copy";
$Lang["libms"]["book"]["delete"] = "Remove copy";
$Lang["libms"]["book"]["code_format"] = "must start with letters and end with numbers, e.g. \"FICTION000021\"";
$Lang["libms"]["book"]["lost_date"] = "Reported Date";
$Lang["libms"]["book"]["user_name"] = "Name";
$Lang["libms"]["book"]["class"] = "Class Number";
$Lang["libms"]["book"]["reserve_time"] = "Reservation Time";
$Lang["libms"]["book"]["reserve_sum"] = "Reservation Count";
$Lang['libms']['book']['invoice_number'] = "Invoice Number";
$Lang['libms']['book']['use_acno_as_barcode'] = "Use ACNO";
$Lang['libms']['book']['item_account_date'] = "Account Date";
$Lang['libms']['book']['view_item'] = "View Item";
$Lang['libms']['book']['all_cir_type'] = "All Circulation Type";
$Lang['libms']['book']['all_res_type'] = "All Resource Type";
$Lang["libms"]["book"]["alert"]["numeric"] = 'Please enter correct number';
$Lang["libms"]["book"]["alert"]["item_book_title"] = 'Book title is not correct';
$Lang["libms"]["book"]["alert"]["incorrect_anco"] = 'ACNO is not correct';
$Lang["libms"]["book"]["alert"]["fillin_anco"] = 'Please fill in ACNO';
$Lang["libms"]["book"]["setting"]["filter_submit"] = 'Apply';
$Lang['libms']['book']['select']['Book_Status'] = 'Book Status';
$Lang['libms']['book']['select']['BookCategoryCode'] = 'Book Category';
$Lang['libms']['book']['select']['ResourcesTypeCode'] = 'Resources Type';
$Lang['libms']['book']['select']['CirculationTypeCode'] = 'Circulation Type';
$Lang['libms']['book']['btn']['SubmitAndAddItem'] = "Submit & Add Item";
$Lang['libms']['book']['status']['remark'] = "represents the book is set to disallow loaning (no matter what the item status is). You may edit the book details to make change if necessary.";

$Lang['libms']['book']['reservation_disallowed'] = "Disallowed reservation";
$Lang['libms']['book']['borrow_disallowed'] = "Disallowed loan ";
$Lang['libms']['book']['updateBookStatusTo'] = "Update Book Status to";
$Lang['libms']['book']['expiredAndPassedToNext'] = "Expired and passed to next";
$Lang['libms']['book']['expired'] = "Expired";
$Lang['libms']['book']['allRecord'] = "All";
$Lang['libms']['book']['iteminfo'] = "Item Information";

$Lang["libms"]["label"]["labelformat_list"] = "Label paper";
$Lang["libms"]["label"]["name"] = "Template title";
$Lang["libms"]["label"]["paper-size-width"] = "Width of paper";
$Lang["libms"]["label"]["paper-size-height"] = "Height of paper";
$Lang["libms"]["label"]["NX"] = "Number of labels per row";
$Lang["libms"]["label"]["NY"] = "Number of labels per column";
$Lang["libms"]["label"]["metric"] = "Unit";
$Lang["libms"]["label"]["metric_inch"] = "inch";
$Lang["libms"]["label"]["metric_mm"] = "mm";
$Lang["libms"]["label"]["unit"]["in"] = "inch";
$Lang["libms"]["label"]["unit"]["mm"] = "mm";
$Lang["libms"]["label"]["piece"] = "piece";
$Lang["libms"]["label"]["SpaceX"] = "Horizontal space between labels";
$Lang["libms"]["label"]["SpaceY"] = "Vertical space between labels";
$Lang["libms"]["label"]["width"] = "Width of label";
$Lang["libms"]["label"]["height"] = "Height of label";
$Lang["libms"]["label"]["font_size"] = "Font size";
$Lang["libms"]["label"]["printing_margin_h"] = "Left margin between label and text";
$Lang["libms"]["label"]["printing_margin_v"] = "Top margin between label and text";
$Lang["libms"]["label"]["max_barcode_width"] = "Width of barcode";
$Lang["libms"]["label"]["max_barcode_height"] = "Height of barcode";
$Lang["libms"]["label"]["lMargin"] = "Left margin between paper and label";
$Lang["libms"]["label"]["tMargin"] = "Top margin between paper and label";
$Lang["libms"]["label"]["input_msg"] = "Please input valid number, e.g.(10.75)";
$Lang["libms"]["label"]["lineHeight"] = "Line height";
$Lang["libms"]["label"]["info_order"] = "Order of label information (For Barcode Label only)";
$Lang["libms"]["label"]["BookTitle"] = "Book title";
$Lang["libms"]["label"]["LocationCode"] = "Location";
$Lang["libms"]["label"]["CallNumCallNum2"] = "Call number";
$Lang["libms"]["label"]["BarCode"] = "Barcode";
$Lang["libms"]["label"]["BookCode"] = "ACNO";
$Lang["libms"]["label"]["print_school_name"] = "School name";
$Lang["libms"]["label"]["msg"]["labelInfoError"] = "The order of the label information must not be equal with each other!";
$Lang["libms"]["label"]["msg"]["labelInfoErrorFillAll"] = "Please select all Order of label information! If no needed, select all the fields with \"NA\"!";
$Lang["libms"]["label"]["msg"]["no_template"] = "Please add at least one template in Label Paper!";

$Lang['libms']['import']['msg']['error_required_missing']='missing required field. column:';
$Lang['libms']['import']['msg']['error_format_mismatch']='format mismatch. column:';
$Lang['libms']['import']['msg']['dup_key'] = 'Duplicated Code. column:';

$Lang["libms"]["import_book"]["file_format"] = "Import format - ISO2709";
$Lang["libms"]["import_book"]["file_charset"] = "File charact set";
$Lang["libms"]["import_book"]["BIG5"] = "BIG5";
$Lang["libms"]["import_book"]["BIG5HKSCS"] = "BIG5HKSCS";
$Lang["libms"]["import_book"]["UTF8"] = "UTF-8";
$Lang["libms"]["import_book"]["matched_by_acno"] = "mapped by ACNO";
$Lang["libms"]["import_book"]["total_record"] = "Number of entries ";
$Lang["libms"]["import_book"]["total_record_modified"] = "Number of entries updated ";
$Lang["libms"]["import_book"]["preview"] = "Preview";
$Lang["libms"]["import_book"]["title"] = "Title:";
$Lang["libms"]["import_book"]["author"] = "Author:";
$Lang["libms"]["import_book"]["upload_fail"] = "Filed to upload:";
$Lang["libms"]["import_book"]["contact_admin"] = ", please contact your administrator";
$Lang["libms"]["import_book"]["processing_db"] = "Import in progress:";
$Lang["libms"]["import_book"]["process_count"] = "Number of records import:";
$Lang["libms"]["import_book"]["upload_success_ready_to_import"] = "Data validated, number of records to import: ";
$Lang["libms"]["import_book"]["Result"]["Success"] = "Import Completed";
$Lang["libms"]["import_book"]["Result"]["Fail"] = "Import failed";
$Lang["libms"]["import_book"]["TypeInvalid"] = "Invalid type";
$Lang["libms"]["import_book"]["DataMissing"] = "Missing data";
$Lang["libms"]["import_book"]["BookCodeAlreadyExist"] = "same ACNO is used by current record";
$Lang["libms"]["import_book"]["ACNO_not_found"] = "ACNO doesn't exist";
$Lang["libms"]["import_book"]["ACNO_no_AUTO"] = "\"AUTO\" must not be used";
$Lang["libms"]["import_book"]["BarcodeAlreadyExist"] = "same barcode is used by current record";
$Lang["libms"]["import_book"]["BarcodeAlreadyBeUsed"] = "same barcode is used by other record";
$Lang["libms"]["import_book"]["BarcodeMissing"] = "barcode is missing";
$Lang["libms"]["import_book"]["AncoWrongFormat"] = "ACNO number in wrong format";
$Lang["libms"]["import_book"]["BookTitleEmpty"] = "Book title is empty";
$Lang["libms"]["import_book"]["ResponsibilityNotExist"] = "Responsibility code is not exist";
$Lang["libms"]["import_book"]["BookCategoryNotExist"] = "Book Category is not exist";
$Lang["libms"]["import_book"]["BookCirclationNotExist"] = "Book Circlation is not exist";
$Lang["libms"]["import_book"]["BookResourcesNotExist"] = "Book Resources is not exist";
$Lang["libms"]["import_book"]["BookLocationNotExist"] = "Book Location is not exist";
$Lang["libms"]["import_book"]["alreadyexist"] = "already exist.";
$Lang["libms"]["import_book"]["doubleused"] = "double used.";
$Lang["libms"]["import_book"]["CancelCheck"] = "Do you want to cancel check?";
$Lang["libms"]["import_book"]["AlreadyChecked"] = "Already checked: ";
$Lang["libms"]["import_book"]["StartToChecking"] = "Start to checking...";
$Lang["libms"]["import_book"]["CancelImport"] = "Do you want to cancel import?";
$Lang["libms"]["import_book"]["AlreadyImporttedBooks"] = "Already importted books: ";
$Lang["libms"]["import_book"]["AlreadyImporttedItems"] = "Already importted items: ";
$Lang["libms"]["import_book"]["StartToImporting"] = "Start to importting...";
$Lang["libms"]["import_book"]["CheckImport"] = "Check is completed, you can import now...";

$Lang["libms"]["import_book"]["acnoautofill"] = "<span class='tabletextrequire'>@</span> If ACNO is empty, server will generate it automatically.<br /> <br /> <span class='tabletextrequire'>+</span> For barcode generation, please fill in 'AUTO'. If ACNO is used as barcode, please fill in 'ACNO'.";
$Lang["libms"]["import_book"]["ImportCSVDataCol_New"] = array("<font color = red>*</font>Book Title","Sub-title","<font color = red>*</font>ACNO","Call Number","Call Number 2","ISBN","ISBN2","Language","Country","Introduction","Edition","Publish Year","Publisher","Publish Place","Series","Series Number","<font color = red>&</font>Responsibility Code 1","Responsibility By 1","<font color = red>&</font>Responsibility Code 2","Responsibility By 2","<font color = red>&</font>Responsibility Code 3","Responsibility By3","Number of Page","<font color = red>&</font>Book Category Code","<font color = red>&</font>Circulation Type Code","<font color = red>&</font>Resources Type Code","<font color = red>&</font>Location Code","Subject","Account Date","Remark Internal","Purchase Date","Purchase Price","List Price","Discount","Distributor","Purchase By Department","Purchase Note","Number Of Copy","Allow to Borrow","Allow to Reserve","Remark To User","Tag","Related URL","<font color = red>+</font>barcode1","<font color = red>+</font>barcode2","<font color = red>+</font>barcode3","<font color = red>+</font>barcode4","<font color = red>+</font>barcode5","<font color = red>+</font>barcode6","<font color = red>+</font>barcode7","<font color = red>+</font>barcode8","<font color = red>+</font>barcode9","<font color = red>+</font>barcode10","<font color = red>+</font>barcode11","<font color = red>+</font>barcode12","<font color = red>+</font>barcode13","<font color = red>+</font>barcode14","<font color = red>+</font>barcode15","<font color = red>+</font>barcode16","<font color = red>+</font>barcode17","<font color = red>+</font>barcode18","<font color = red>+</font>barcode19","<font color = red>+</font>barcode20");
$Lang["libms"]["import_book"]["ImportCSVDataCol_New2"] = array("<font color = red>*</font>Book Title",
"Sub-title",
"Call Number",
"Call Number 2",
"ISBN",
"ISBN2",
"Language",
"Country",
"Introduction",
"Edition",
"Publish Year",
"Publisher",
"Publish Place",
"Series",
"Series Number",
"Responsibility Code 1  <a href='javascript:viewCodes(1)' class='tablelink'>[Click here to check codes]</a>",
"Responsibility By 1",
"Responsibility Code 2  <a href='javascript:viewCodes(1)' class='tablelink'>[Click here to check codes]</a>",
"Responsibility By 2",
"Responsibility Code 3  <a href='javascript:viewCodes(1)' class='tablelink'>[Click here to check codes]</a>",
"Responsibility By3",
"Number of Page",
"Book Category Code  <a href='javascript:viewCodes(2)' class='tablelink'>[Click here to check codes]</a>",
"Circulation Type Code  <a href='javascript:viewCodes(3)' class='tablelink'>[Click here to check codes]</a>",
"Resources Type Code  <a href='javascript:viewCodes(4)' class='tablelink'>[Click here to check codes]</a>",
"Subject",
"Remark Internal",
"Allow to Borrow",
"Allow to Reserve",
"Remark To User",
"Tag",
"Related URL",
"<font color = red>*@</font>ACNO <span id='acno_type1'>(item information starts here, leave it blank if no item yet)</span> <span id='acno_type2' style='display:none;'><font color='red'>(Mandatory field)</font></span>",
"<font color = red>*+</font>barcode",
"Location Code  <a href='javascript:viewCodes(5)' class='tablelink'>[Click here to check codes]</a>",
"Account Date",
"Purchase Date",
"Purchase Price",
"List Price",
"Discount",
"Distributor",
"Purchase By Department",
"Purchase Note",
"Invoice Number",
"Item Status (Default: available) <a href='javascript:viewCodes(6)' class='tablelink'>[Click here to check codes]</a>");



$Lang["libms"]["import_book"]["Code"] = "Code";
$Lang["libms"]["import_book"]["TitleEng"] = "Title in English";
$Lang["libms"]["import_book"]["TitleChi"] = "Title in Chinese";


$Lang["libms"]["batch_edit"]["ByReturnDate"] = "By Due Date";
$Lang["libms"]["batch_edit"]["ByBorrowDate"] ="By Loan Date";

$Lang["libms"]["pre_label"]["print_label_border"] = " Including label border";
$Lang["libms"]["pre_label"]["bookcode"] = "ACNO";
$Lang["libms"]["pre_label"]["bookcode_number"] = "ACNO number";
$Lang["libms"]["pre_label"]["search"] = "Search";
$Lang["libms"]["pre_label"]["add"] = "Add";
$Lang["libms"]["pre_label"]["bookid"] = "BookID";
$Lang["libms"]["pre_label"]["barcode"] = "Barcode";
$Lang["libms"]["pre_label"]["spine_label"] = "Spin Label";
$Lang["libms"]["pre_label"]["callno"] = "Call number";
$Lang["libms"]["pre_label"]["callno2"] = "Call number 2";
$Lang["libms"]["pre_label"]["location"] = "Location";
$Lang["libms"]["pre_label"]["school_name"] = "School name";
$Lang["libms"]["pre_label"]["title"] = "Book title";
$Lang["libms"]["pre_label"]["toogle"] = "Toogle";
$Lang["libms"]["pre_label"]["label_format"] = "Label paper";
$Lang["libms"]["pre_label"]["label_new_format"] = "Add label paper";
$Lang["libms"]["pre_label"]["label_edit_format"] = "Edit label paper";
$Lang["libms"]["pre_label"]["no_match"] = "No record is found!";
$Lang["libms"]["pre_label"]["select_label"] = "Label type";
$Lang["libms"]["pre_label"]["print_value"] = "Contents to print";
$Lang["libms"]["pre_label"]["print_book"] = "Book(s) involved";
$Lang["libms"]["pre_label"]["action"] = "Action";
$Lang["libms"]["pre_label"]["prefix_not_match"] = "Prefix is not match!";
$Lang["libms"]["pre_label"]["search_comment"] = "* Attention: It's suggested to print at most 100 labels at a time. Otherwise, it may lead to browser crash.";
$Lang['libms']['pre_label']['ReaderBarcode'] = "Reader Label";
$Lang['libms']['pre_label']['PleaseSelectAGroup'] = "Please select a group";
$Lang['libms']['pre_label']['PleaseSelectReader'] = "Please select reader(s)";
$Lang['libms']['pre_label']['BarcodeNo'] = "Barcode Number";
$Lang['libms']['pre_label']['StartPosition'] = "Start Print Position";

$Lang["libms"]["gen_label"]["no_book_selected"] = "Error: no book has been selected!";
$Lang["libms"]["gen_label"]["no_label_format"] = "Error: no label paper has been selected!";
$Lang["libms"]["gen_label"]["no_print_fields"] = "Error: no content has been selected for printing!";
$Lang["libms"]["group_management"] = "Patron Group Management";
$Lang["libms"]["FormClassMapping"]["CtrlMultiSelectMessage"] = "Use CTRL to select multiple items";
$Lang["libms"]["FormClassMapping"]["Or"] = " or ";
$Lang["libms"]["GroupManagement"]["AccessRight"] = "Access right";
$Lang["libms"]["GroupManagement"]["AddUser"] = "Add user(s)";
$Lang["libms"]["GroupManagement"]["Alumni"] = "Alumni";
$Lang["libms"]["GroupManagement"]["BelongToSameForm"] = "... of the same Form";
$Lang["libms"]["GroupManagement"]["BelongToSameFormClass"] = "... of the same Class";
$Lang["libms"]["GroupManagement"]["BelongToSameGroup"] = "... of the same Group";
$Lang["libms"]["GroupManagement"]["BelongToSameSubject"] = "... of the same Subject";
$Lang["libms"]["GroupManagement"]["BelongToSameSubjectGroup"] = "... of the same Subject Group";
$Lang["libms"]["GroupManagement"]["CanSendTo"] = "Can send to:";
$Lang["libms"]["GroupManagement"]["ClassName"] = "Class";
$Lang["libms"]["GroupManagement"]["ClassNumber"] = "Class number";
$Lang["libms"]["GroupManagement"]["ClassLevel"] = "Class level";
$Lang["libms"]["GroupManagement"]["Delete"] = "Remove";
//$Lang["libms"]["GroupManagement"]["ExportGroupDetail"] = "匯出群組細節";
$Lang["libms"]["GroupManagement"]["Identity"] = "Identity";
$Lang["libms"]["GroupManagement"]["ModuleTitle"] = "Group Management";
$Lang["libms"]["GroupManagement"]["Name"] = "Name";
$Lang["libms"]["GroupManagement"]["New"] = "Add";
$Lang["libms"]["GroupManagement"]["NewGroup"] = "New group";
$Lang["libms"]["GroupManagement"]["Parent"] = "Parent";
//$Lang["libms"]["GroupManagement"]["PrintGroupDetail"] = "列印群組細節";
$Lang["libms"]["GroupManagement"]["RemoveThisGroup"] = "Delete group";
$Lang["libms"]["GroupManagement"]["GroupList"] = "Group list";
$Lang["libms"]["GroupManagement"]["GroupName"] = "Group name";
$Lang["libms"]["GroupManagement"]["GroupTitle"] = "Group name";
$Lang["libms"]["GroupManagement"]["SearchUser"] = "Search user";
$Lang["libms"]["GroupManagement"]["SelectAll"] = "Select all";
$Lang["libms"]["GroupManagement"]["SelectedUser"] = "Selected user(s)";
$Lang["libms"]["GroupManagement"]["SelectIdentity"] = "select identity";
$Lang["libms"]["GroupManagement"]["Student"] = "Student";
$Lang["libms"]["GroupManagement"]["SubmitAndEdit"] = "Submit and then New";
$Lang["libms"]["GroupManagement"]["SupportStaff"] = "Non-teaching Staff";
//$Lang["libms"]["GroupManagement"]["Targeting"] = "Targeting";
$Lang["libms"]["GroupManagement"]["TeachingStaff"] = "Teacher";
$Lang["libms"]["GroupManagement"]["ToAll"] = "All";
$Lang["libms"]["GroupManagement"]["ToAllUser"] = "All users";
$Lang["libms"]["GroupManagement"]["TopManagement"] = "Access Right";
$Lang["libms"]["GroupManagement"]["UncheckForMoreOptions"] = "Cancel selections";
$Lang["libms"]["GroupManagement"]["UserList"] = "Group Members";
$Lang["libms"]["GroupManagement"]["Users"] = "Members";
$Lang["libms"]["GroupManagement"]["UserType"] = "User identity";
$Lang["libms"]["SubjectClassMapping"]["SelectClass"] = "Select class";
$Lang["libms"]["GroupManagement"]["GroupNameDuplicateWarning"] = "Warning: same group name exists!";
$Lang["libms"]["GroupManagement"]["GroupCodeDuplicateWarning"] = "Warning: same group code exists!";
//sqlfeildname//$Lang["libms"]["SQL"]["UserNameFeild"] = "ChineseName";
$Lang["libms"]["SQL"]["CirDescription"] = "DescriptionEn";
//endofSQL$Lang["libms"]["GroupManagement"]["GroupTitle"] = "群組名稱";
$Lang["libms"]["GroupManagement"]["GroupCode"] = "Group code";
$Lang["libms"]["GroupManagement"]["IsDefault"] = "Default?";
$Lang["libms"]["GroupManagement"]["GroupSavedUnsuccess"] = "0|=|Record Add Failed.";
$Lang["libms"]["GroupManagement"]["GroupSavedSuccess"] = "1|=|Record Added.";
$Lang["libms"]["GroupManagement"]["GroupCodeUpdateUnsuccess"] = "0|=|Group Code Update Failed.";
$Lang["libms"]["GroupManagement"]["GroupCodeUpdateSuccess"] = "1|=|Group Code Updated.";
$Lang["libms"]["GroupManagement"]["GroupRemoveUnsuccess"] = "0|=|Record Delete Failed.";
$Lang["libms"]["GroupManagement"]["GroupRemoveSuccess"] = "1|=|Record Deleted.";
$Lang["libms"]["GroupManagement"]["DeleteWarning"] = "Are you sure you want to delete the record?";
$Lang["libms"]["GroupManagement"]["DefaultRule"] = "Default";
$Lang["libms"]["GroupManagement"]["GroupRule"] = "Circulation Policy";
$Lang["libms"]["GroupManagement"]["GroupPeriod"] = "Period (optional)";

########### Admin ################
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["name"] = "Admin Function";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["settings"] = "Settings";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["group management"] = "Patron Group Management";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["stock-take and write-off"] = "Stock-take & Write-off";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["book management"] = "Book Management (Book, Item, Periodical, Batch Edit, Label, and Current Reservation)";
########### Admin ################

$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["name"] = "Circulation";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["borrow"] = "Loan";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["reserve"] = "Reserve";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["return"] = "Return";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["renew"] = "Renew";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["overdue"] = "Overdue payment";

##### Report Start#####
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["name"] = "Reports";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["daily summary"] = "Summary";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["overdue"] = "Overdue";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["income"] = "Income";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["borrow records"] = "Loan";

$Lang["libms"]["GroupManagement"]["Right"]["reports"]["accession report"] = "Accession Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["book report"] = "Book Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["loan report"] = "Loan Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["overdue report"] = "Overdue Notice";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["lost report"] = "Lost Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["reader ranking"] = "Patron Ranking";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["inactive reader"] = "Inactive Patron";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["book ranking"] = "Book Ranking";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["stock-take report"] = "Stock-take Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"] = "Write-off Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["progress report"] = "Progress Report";

##### Report End #####
########## Statistic Start #########
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["name"] = "Statistics";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["summary"] = "Summary";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["general stats"] = "General Stats";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["category vs class or form"] = "Category vs Class/Form";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["frequency by form"] = "Frequency by Form";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["penalty stats"] = "Penalty Stats";
########## Statistic End  #########

$Lang["libms"]["GroupManagement"]["Right"]["service"]["name"] = "End-user Services";
$Lang["libms"]["GroupManagement"]["Right"]["service"]["reserve"] = "Reserve";
$Lang["libms"]["GroupManagement"]["Right"]["service"]["renew"] = "Renew";
$Lang["libms"]["GroupManagement"]["Rule"]["CirDescription"] = "Circulation Type";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitBorrow"] = "Loan (No. of books)";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitRenew"] = "Renew (No. of times)";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitReserve"] = "Reserve (No. of books)";
$Lang["libms"]["GroupManagement"]["Rule"]["ReturnDuration"] = "Duration (No. of days)";
$Lang["libms"]["GroupManagement"]["Rule"]["OverDueCharge"] = "Overdue (fine per day)";
$Lang["libms"]["GroupManagement"]["Rule"]["MaxFine"] = "Maxium Fine (per book)";
$Lang["libms"]["GroupManagement"]["Msg"]["chgrp_confirm"] = "The following user belongs to other groups. Please confirm group change.";
$Lang["libms"]["GroupManagement"]["Msg"]["no_group_right"] = "No loaning is permitted until this user belongs to a patron group and has granted circulation policy!";

$Lang["libms"]["GroupManagement"]["Rule"]["policy_remark"] = "<u>Remark</u>: The <b>Default Policy</b> refers to maximum number of borrowed item(s), which includes item(s) without circulation type.";
$Lang["libms"]["GroupManagement"]["index"]["remark"] = "Each user can belongs to one group only. If the user is not yet assgined to a group or his/her group has no circulation policy set, loaning will not be allowed.";

$Lang["libms"]["book_management"] = "Book management";
$Lang["libms"]["circulation_management"] = "Circulation management";
$Lang["libms"]["Circulation"]["detailrecord"] = "Detailed Records";
$Lang["libms"]["Circulation"]["borrowedBooks"] = "Loan Records";
$Lang["libms"]["Circulation"]["detailrecord"] = "Detailed Records";
$Lang["libms"]["Circulation"]["action_success"] = "succeed!";
$Lang["libms"]["Circulation"]["action_fail"] = "failed!";
$Lang["libms"]["Circulation"]["BookBorrowHistory"] = "Loan Records";
$Lang["libms"]["Circulation"]["ReaderBorrowHistory"] = "Patron Loan Records";

$Lang['libms']['CirculationManagement']['user_group_not_found'] = "This user does not belong to any patron group!";

$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'] = "Notification of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'] = <<<EOF
The book %s you reserved is now available for loan.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'] = <<<EOF
The book %s you reserved is now available for loan. Please borrowed it on or before %s.
EOF;

$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['subject'] = "Cancellation of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['mail_body'] = <<<EOF
Please be informed that your book ( %s ) reserved has been cancelled by the library administrator on %s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['subject'] = "Cancellation of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['mail_body'] = <<<EOF
Please be informed that your book ( %s ) reserved has been cancelled by you on %s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['subject'] = "Cancellation of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['mail_body'] = <<<EOF
Please be informed that your book ( %s ) reserved has been cancelled by system since the reservation gets expired on %s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['subject'] = "Cancellation of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['mail_body'] = <<<EOF
Please be informed that your book ( %s ) reserved has been cancelled by the library administrator and moved to waiting list on %s.
EOF;

$Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['subject'] = "Due Date Reminder";
$Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['mail_body'] = <<<EOF
Please be informed that the due date of your borrowed book(s) ( %s ) is/are on %s. Please return the book(s) on or before the due date.
EOF;

$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['CallNum']= "Call Number";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle']= "Book Title";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Publisher']= "Publisher";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author']= "Author";

$Lang['libms']['CirculationManagement']['barcode_user_not_found'] = "No user is found according to the barcode."; 
$Lang['libms']['CirculationManagement']['library_closed'] = "In closing";
$Lang['libms']['CirculationManagement']['batch_return'] = "Batch Book Returns";
$Lang['libms']['CirculationManagement']['batch_renew'] = "Batch Book Renews";
$Lang['libms']['CirculationManagement']['search_book_record'] = "Book Search";
$Lang['libms']['CirculationManagement']['batch_return_enter'] = "Enter";
$Lang['libms']['CirculationManagement']['normal_circulation'] = "Circulation for a Patron";
$Lang["libms"]["CirculationManagement"]["status"] = "Status";
$Lang["libms"]["CirculationManagement"]["add"] = "Add";
$Lang["libms"]["CirculationManagement"]["confirm"] = "Confirm";
$Lang["libms"]["CirculationManagement"]["cancel"] = "Cancel";
$Lang["libms"]["CirculationManagement"]["user_barcode"] = "User barcode";
$Lang["libms"]["CirculationManagement"]["student_only"] = "or student selection";
$Lang["libms"]["CirculationManagement"]["user_barcode_ByLin"] = "User barcode 用戶條碼";
$Lang["libms"]["CirculationManagement"]["barcode"] = "Barcode";
$Lang["libms"]["CirculationManagement"]["enter_code"] = "Input ACNO/Barcode/Call number/ISBN";
$Lang["libms"]["CirculationManagement"]["bkcode_acno"] = "Call number／ACNO";
$Lang["libms"]["CirculationManagement"]['reserve_page']["searchby"] = "Book title / Author / ACNO / Call number ";

$Lang["libms"]["CirculationManagement"]["booktitle"] = "Book title";
$Lang["libms"]["CirculationManagement"]["moredetail"] = "More details";
$Lang["libms"]["CirculationManagement"]["location"] = "Location";
$Lang["libms"]["CirculationManagement"]["borrowday"] = "Loan date";
$Lang["libms"]["CirculationManagement"]["returnday"] = "Return date";
$Lang["libms"]["CirculationManagement"]["renewday"] = "Renew date";
$Lang["libms"]["CirculationManagement"]["dueday"] = "Due date";
$Lang["libms"]["CirculationManagement"]["reserveday"] = "Reserve date";
$Lang["libms"]["CirculationManagement"]["overdueday"] = "NO. of days past due";
$Lang["libms"]["CirculationManagement"]["renew"] = "Renew";
$Lang["libms"]["CirculationManagement"]["renews"] = "Renews";
$Lang["libms"]["CirculationManagement"]["lost"] = "Report lost";
$Lang["libms"]["CirculationManagement"]["borrow"] = "Loan";
$Lang["libms"]["CirculationManagement"]["return"] = "Return";
$Lang["libms"]["CirculationManagement"]["reserve"] = "Reserve";
$Lang["libms"]["CirculationManagement"]["overdue"] = "Settle overdue fine";
$Lang["libms"]["CirculationManagement"]["statusnow"] = "Current records";
$Lang["libms"]["CirculationManagement"]["all_loan_records"] = "All loan records";
$Lang["libms"]["CirculationManagement"]["leave"] = "Quit";
$Lang["libms"]["CirculationManagement"]["no_borrow"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_renew"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_return"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_batch_renew"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_lost"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_reserve"] = "Total";
$Lang["libms"]["CirculationManagement"]["ppl_reserved"] = "Total";
$Lang["libms"]["CirculationManagement"]["n_renew"] = "Renew: disallowed";
$Lang["libms"]["CirculationManagement"]["y_renew"] = "Renew: allowed";
$Lang["libms"]["CirculationManagement"]["y_reserve"] = "Reserve: allowed";
$Lang["libms"]["CirculationManagement"]["n_reserve"] = "Reserve: disallowed";
$Lang["libms"]["CirculationManagement"]["y_borrow"] = "Loan: allowed";
$Lang["libms"]["CirculationManagement"]["n_borrow"] = "Loan: disallowed";
$Lang["libms"]["CirculationManagement"]["renew_left"] = "Renew left";
$Lang["libms"]["CirculationManagement"]["overdue_day"] = "Overdue (days)";
$Lang['libms']['CirculationManagement']['overdue_type'] ="Penalty Type";
$Lang["libms"]["CirculationManagement"]["overdue_with_day"] = "Overdue (%d days)";
$Lang["libms"]["CirculationManagement"]["payment"] = "Fine";
$Lang["libms"]["CirculationManagement"]["free_payment"] = "Waive";
$Lang["libms"]["CirculationManagement"]["paied"] = "Payment settled";
$Lang["libms"]["CirculationManagement"]["pay_now"] = "Amount to pay";
$Lang["libms"]["CirculationManagement"]["booked"] = "Reserved by other(s)";
$Lang["libms"]["CirculationManagement"]["booked_alert"] = "Reserved by other(s)!";
$Lang["libms"]["CirculationManagement"]["left"] = "Remained";
$Lang["libms"]["CirculationManagement"]["total_pay"] = "Total";
$Lang["libms"]["CirculationManagement"]["reserverperiod"] = "Reserve period";
$Lang["libms"]["CirculationManagement"]["trim"] = "(repairing)";
$Lang["libms"]["CirculationManagement"]["action"] = "Action";
$Lang["libms"]["CirculationManagement"]["borrowed"] = "Loaned";
$Lang["libms"]["CirculationManagement"]["reserved"] = "Reserved";
$Lang["libms"]["CirculationManagement"]["book_overdue"] = "Overdue";
$Lang["libms"]["CirculationManagement"]["borrow_limit"] = "Loan quota";
$Lang["libms"]["CirculationManagement"]["limit_left"] = "Quota left";
$Lang["libms"]["CirculationManagement"]["book_reserve"] = "Reserved";
$Lang["libms"]["CirculationManagement"]["left_pay"] = "Fine";
$Lang["libms"]["CirculationManagement"]["past_record"] = "Past records";
$Lang["libms"]["CirculationManagement"]["FineDate"] = "Fine Date (Return Date)";
$Lang["libms"]["CirculationManagement"]["msg"]["over_borrow_limit"] = "Loan limit is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_limit"] = "Reservation limit is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_renew_limit"] = "Renewal limit is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_borrow_cat_limit"] = "Loan limit on circulation type is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_cat_limit"] = "Reservation limit on circulation type is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_renew_cat_limit"] = "Renewal limit on circulation type is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["renew_but_overdue"] = "Over due! Must return it and settle payment.";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_borrow_limit"] = "Not allowed to loan by this user.";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_reserve_limit"] = "Not allowed to reserve by this user.";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_renew_limit"] = "Not allowed to renew by this user.";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_borrow"] = "cannot be loaned";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_borrow_book"] = "cannot be loaned";
$Lang["libms"]["CirculationManagement"]["msg"]["already_borrowed"] = "this book is borrowed by [user_name]";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve"] = "cannot be reserved";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_book"] = "cannot be reserved";
$Lang["libms"]["CirculationManagement"]["msg"]["scanned"] = "already in the list!";
$Lang["libms"]["CirculationManagement"]["msg"]["force_borrow"] = "forced to loan";
$Lang["libms"]["CirculationManagement"]["msg"]["force_reserve"] = "forced to reserve";
$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"] = "force to renew";
$Lang["libms"]["CirculationManagement"]["msg"]["f_reserve"] = "force to reserve";
$Lang["libms"]["CirculationManagement"]["msg"]["f_borrow"] = "force to loan";
$Lang["libms"]["CirculationManagement"]["msg"]["reserved"] = "on reserve";
$Lang["libms"]["CirculationManagement"]["msg"]["comfirm_leave"] = "[IMPORTANT] Record(s) have not been submitted yet! Are you sure you want to discard?";
$Lang["libms"]["CirculationManagement"]["msg"]["n_barcode"] = "No book is found!";
$Lang["libms"]["CirculationManagement"]["msg"]["n_callno_acno"] = "No record match your query";
$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"] = "Overdue!";
$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"] = "day(s)";
$Lang['libms']['CirculationManagement']['msg']['overdue_limit'] = "Overdue grace period is expired! Please return the overdring book.";
$Lang['libms']['CirculationManagement']['msg']['please_pick_a_book'] = "Please pick a book";
$Lang['libms']['CirculationManagement']['msg']['searching_string_too_short']= 'Searching string is too short!';
$Lang['libms']['CirculationManagement']['msg']['fix_error'] = "Please fix input error.";
$Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"] = "No loan record is found!";
$Lang['libms']['CirculationManagement']['msg']['batch_return'] = "You can handle book returns for different users here.";
$Lang['libms']['CirculationManagement']['msg']['batch_renew'] = "You can handle book renewals for different users here.";
$Lang['libms']['CirculationManagement']['msg']['circulation_index'] = "You can choose to handle batch book returns, circulation for an user or searching book.";
$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed'] = "Please input or select at least one book!";
$Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"] = "The book is reserved!";
$Lang['libms']['CirculationManagement']['msg']['user_reserved'] = "The book is reserved by other reader!";
$Lang['libms']['CirculationManagement']['msg']['reserved_by'] = "Reserved by";
$Lang['libms']['CirculationManagement']['msg']['expiry_date'] = "Expiry Date";
$Lang['libms']['CirculationManagement']['msg']['no_overdue_record'] = "There is no payment to be settled from returned record(s).";
$Lang["libms"]["CirculationManagement"]['msg']["AllOverduePaymentPaidWithePayment"] = "All overdue payments will be paid with ePayment.";

$Lang["libms"]["CirculationManagement"]["borrowed_by"] = "Loaned by";
$Lang["libms"]["CirculationManagement"]["valid_date"] = "Valid Date";
$Lang["libms"]["CirculationManagement"]['msg']["AllOverduePaymentPaidWithePayment"] = "All overdue payments will be paid with ePayment.";
$Lang["libms"]["CirculationHotKeys"]["Exit"] = "[Esc]";
$Lang["libms"]["CirculationHotKeys"]["Borrow"] = "[F2]";
$Lang["libms"]["CirculationHotKeys"]["Return"] = "[F3]";
$Lang["libms"]["CirculationHotKeys"]["Renew"] = "[F4]";
$Lang["libms"]["CirculationHotKeys"]["Lost"] = "[F6]";
$Lang["libms"]["CirculationHotKeys"]["Reserve"] = "[F7]";
$Lang["libms"]["CirculationHotKeys"]["Overdue"] = "[F8]";
$Lang["libms"]["CirculationHotKeys"]["Cancel"] = "[F5]";
$Lang["libms"]["CirculationHotKeys"]["Confirm"] = "[F9]";
$Lang["libms"]["CirculationHotKeys"]["Details"] = "[F10]";
$Lang["libms"]["CirculationHotKeys"]["BatchReturn"] = "[F1]";



//$Lang["libms"]["book_status"]["CANCEL"] = "取消";
//$Lang["libms"]["book_status"]["DELETE"] = "删除";
$Lang["libms"]["book_status"]["NORMAL"] = "Available";
$Lang["libms"]["book_status"]["SHELVING"] = "Processing";
$Lang["libms"]["book_status"]["BORROWED"] = "on Loan";
$Lang["libms"]["book_status"]["DAMAGED"] = "Damaged";
$Lang["libms"]["book_status"]["REPAIRING"] = "Repairing";
$Lang["libms"]["book_status"]["RESERVED"] = "on Reserve";
$Lang["libms"]["book_status"]["RESTRICTED"] = "on Display";
$Lang["libms"]["book_status"]["WRITEOFF"] = "Write-off";
$Lang["libms"]["book_status"]["SUSPECTEDMISSING"] = "Suspected missing";
$Lang["libms"]["book_status"]["LOST"] = "Lost";
$Lang["libms"]["book_status"]["ORDERING"] = "on Order";

$Lang["libms"]["book_reserved_status"]["WAITING"] = "Waiting";
$Lang["libms"]["book_reserved_status"]["READY"] = "Ready";

//reportingpart...................................................................................................................................................................................................................................................................................................
$Lang["libms"]["report"]["bookinfo"] = "Book Information";
$Lang["libms"]["report"]["country"] = "Country";
$Lang["libms"]["report"]["language"] = "Language";
$Lang["libms"]["report"]["time_period"] = "Period";
$Lang["libms"]["report"]["SchoolYear"] = "Academic year";
$Lang["libms"]["report"]["Semester"] = "Term";
$Lang["libms"]["report"]["period_date_from"] = "From";
$Lang["libms"]["report"]["period_date_to"] = " to";
$Lang["libms"]["report"]["display"] = "Display";
$Lang["libms"]["report"]["circulation_type"] = "Circulation type";
$Lang["libms"]["report"]["book_category"] = "Book category";
$Lang["libms"]["report"]["subject"] = "Subject";
$Lang["libms"]["report"]["book_status"] = "Book status";
$Lang["libms"]["report"]["book_status_all"] = "All books";
$Lang["libms"]["report"]["book_status_borrow"] = "Books on loan";
$Lang["libms"]["report"]["book_status_renew"] = "Books on renew";
$Lang["libms"]["report"]["target"] = "Target";
$Lang["libms"]["report"]["findByGroup"] = "Group";
$Lang["libms"]["report"]["findByStudent"] = "Student";
$Lang["libms"]["report"]["amount"] = "Amount";
$Lang["libms"]["report"]["due_time"] = "Due date";
$Lang["libms"]["report"]["show_penalty"] = "Show penalty";
$Lang["libms"]["report"]["due_time_ByLin"] = "Due date 到期日";
$Lang["libms"]["report"]["borrowdate"] = "Loan time";
$Lang["libms"]["report"]["returndate"] = "Return time";
$Lang["libms"]["report"]["listby"] = "List by";
$Lang["libms"]["report"]["sortby"] = "Sort by";
$Lang["libms"]["report"]["classno"] = "Class number";
$Lang["libms"]["report"]["ACNO"] = "ACNO";
$Lang["libms"]["report"]["ACNO_ByLin"] = "ACNO 登錄號碼";
$Lang["libms"]["report"]["frequency"] = "Number of times";
$Lang["libms"]["report"]["number"] = "Total";
$Lang["libms"]["report"]["name"] = "Title";
$Lang["libms"]["report"]["by_form_class"] = "form/class";
$Lang["libms"]["report"]["by_amount"] = "total";
$Lang["libms"]["report"]["ascending"] = "ascending";
$Lang["libms"]["report"]["descending"] = "descending";
$Lang["libms"]["report"]["cannot_define"] = " - unclassified - ";
$Lang["libms"]["report"]["print"] = "Print";
$Lang["libms"]["report"]["mailToStudent"] = "Email to students";
$Lang["libms"]["report"]["mailToParent"] = "Email to parents";
$Lang["libms"]["report"]["selectAll"] = "Select all";
$Lang["libms"]["report"]["PressCtrlKey"] = "(Use CTRL to select multiple items)";
$Lang["libms"]["report"]["Submit"] = "Submit";
$Lang["libms"]["report"]["Form"] = "Form";
$Lang["libms"]["report"]["Class"] = "Class";
$Lang["libms"]["report"]["Student"] = "User";
$Lang["libms"]["report"]["ListNumber"] = "List Number";
$Lang["libms"]["report"]["Resource"] = "Resource Category";
$Lang["libms"]["report"]["totalreserve"] = "Total Reserve";
$Lang["libms"]["report"]["totalborrow"] = "Total Borrow";
$Lang["libms"]["report"]["totalamount"] = "Total Amount Received";
$Lang["libms"]["report"]["totaloverdue"] = "Total Overdue";
$Lang["libms"]["report"]["barcode"] = "Barcode";
$Lang["libms"]["report"]["recordstatus"] = "Record Status";
$Lang["libms"]["report"]["statistical_category"] = "Statistical Category";
$Lang["libms"]["report"]["loans_less_than"] = "With record(s)";
$Lang["libms"]["report"]["loans_less_than_unit"] = " or less";


$Lang["libms"]["report"]["ClassLevel"] = "Level";
$Lang["libms"]["report"]["ClassName"] = "Class";
$Lang["libms"]["report"]["ClassName_ByLin"] = "Class 班別";
$Lang["libms"]["report"]["ClassNumber"] = "Class number";
$Lang["libms"]["report"]["ClassNumber_ByLin"] = "Class number 學號";
$Lang["libms"]["report"]["username"] = "User";
$Lang["libms"]["report"]["username_ByLin"] = "User 讀者姓名";
$Lang["libms"]["report"]["ChineseName"] = "Chinese name";
$Lang["libms"]["report"]["EnglishName"] = "English name";
$Lang["libms"]["report"]["booktitle"] = "Book";
$Lang["libms"]["report"]["booktitle_ByLin"] = "Book 書本";
$Lang["libms"]["report"]["LendingTotal"] = "Total";
$Lang["libms"]["report"]["CallNum"] = "Call No.";
$Lang["libms"]["report"]["ResponsibilityBy"] = "Responsibility By";
$Lang["libms"]["report"]["Publisher"] = "Publisher";
$Lang["libms"]["report"]["Edition"] = "Edition";
$Lang["libms"]["report"]["ISBN"]="ISBN";

$Lang["libms"]["report"]["search"]="Search (ACNO / Book Title / ISBN)";
$Lang["libms"]["report"]["pay"]="Penalty";
$Lang["libms"]["report"]["pay_ByLin"]="Penalty 罰款";
$Lang["libms"]["report"]["payment"]["title"] = "Sum";
$Lang["libms"]["report"]["payment"]["OVERDUE"] = "Outstanding";
$Lang["libms"]["report"]["payment"]["PAID"] = "Payment received";
$Lang["libms"]["report"]["total"] = "Total";
$Lang["libms"]["report"]["total_ByLin"] = "Total 總數";

$Lang["libms"]["report"]["bookrecord"]["title"]= "Book";
$Lang["libms"]["report"]["bookrecord"]["LOST"] = "Lost";
$Lang["libms"]["report"]["bookrecord"]["WRITEOFF"] = "Write-off";
$Lang["libms"]["report"]["bookrecord"]["NEW"] = "New";

$Lang["libms"]["report"]["overdue_email"]["Subject"] = "Due Date Reminder";
$Lang["libms"]["report"]["overdue_email"]["Subject_ByLin"] = "Due Date Reminder 到期通知";
$Lang["libms"]["report"]["overdue_email"]["introduction_ByLin"] = "The following library material(s) which you borrowed is/are already due for return. Please return it/them now. Please ignore this notice if you have already returned. <br />你所借的下列書本或物品現已到期，請立即交還。如你已交還，請無需理會此通知書。";

$Lang["libms"]["report"]["toolbar"]['Btn']['Export'] = "Export";
$Lang["libms"]["report"]["toolbar"]['Btn']['Print'] = "Print";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailuser'] = "Email to user";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailparent'] = "Email to parent";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailboth'] = "Email to user and parent";
$Lang["libms"]["report"]["toolbar"]['Btn']['Close'] = "Close";


$Lang["libms"]["report"]["printdate"] = "Date of printing";
$Lang["libms"]["report"]["printdate_ByLin"] = "Date of printing 列印日期";
$Lang["libms"]["report"]["noOfDayOverdue"] = "Overdue (days)";
$Lang["libms"]["report"]["noOfDayOverdue_ByLin"] = "Overdue (days) 過期日數";

$Lang["libms"]["report"]["showoption"] = "Display options";
$Lang["libms"]["report"]["hideoption"] = "Hide options";
$Lang["libms"]["report"]["norecord"] = "No record found!";

$Lang["libms"]["report"]["fine_date"] = "Period";
$Lang["libms"]["report"]["fine_status"] = "Payment Status";
$Lang["libms"]["report"]["fine_status_all"] = "All";
$Lang["libms"]["report"]["fine_outstanding"] = "Outstanding";
$Lang["libms"]["report"]["fine_waived"] = "Waived";
$Lang["libms"]["report"]["fine_handle_date"] = "Paid On";
$Lang["libms"]["report"]["fine_type"] = "Penalty Reason";
$Lang["libms"]["report"]["fine_type_overdue"] = "Overdue";
$Lang["libms"]["report"]["fine_type_lost"] = "Lost book";
$Lang["libms"]["report"]["handled_by"] = "Handled By";
$Lang["libms"]["report"]["library_stock"] = "Library Stock";
$Lang["libms"]["report"]["library_resources"] = "Use of Library Resources";
$Lang["libms"]["report"]['Others'] = "Others";
$Lang["libms"]["report"]['daily_averages_issue'] = "Daily Averages Issue (vol.)";
$Lang["libms"]["report"]['listTotalNumberOfPerson'] = "List the Total number of student by";
$Lang["libms"]["report"]['TotalNumberOfStudent'] = "Total number of student";

$Lang['libms']['report']['msg']['searching_string_too_short']= 'Searching string is too short!';
$Lang['libms']['report']['msg']['empty_search']= 'Please input search words';
$Lang['libms']['report']['msg']['please_pick_a_book'] = "Please pick a book";
$Lang['libms']['report']['msg']['please_select_category'] = "Please pick at least one category";
$Lang['libms']['report']['msg']['please_select_subject'] = "Please pick at least one subject";
$Lang['libms']['report']['msg']['please_select_language'] = "Please pick at least one language";
$Lang['libms']['report']['msg']['please_select_class'] = "Please pick at least one class";
$Lang['libms']['report']['msg']['please_select_form'] = "Please pick at least one form";
$Lang['libms']['report']['msg']['please_select_group'] = "Please pick at least one group";

$Lang["libms"]["import"]['BookCat']['title'] = "Book Category Import";
$Lang["libms"]["import"]['BookCat']['error_msg']['upload_fail'] = "Upload Fail";
$Lang["libms"]["import"]['BookCat']['error_msg']['contact_admin'] = "Please contact system administrator";


$Lang["libms"]["import"]['BookCat']['fields']['BookCategoryCode']['name']="Book Category Code";
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionEn']['name'] ="English Description";
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionChi']['name']="Chinese Description";
$Lang["libms"]["import"]['BookCat']['fields']['BookCategoryCode']['req']=true;
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionEn']['req'] =true;
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionChi']['req']=true;

$Lang["libms"]["import"]['BookLocation']['title'] = "Book Location Import";
$Lang["libms"]["import"]['BookLocation']['error_msg']['upload_fail'] = "Upload Fail";
$Lang["libms"]["import"]['BookLocation']['error_msg']['contact_admin'] = "Please contact system administrator";


$Lang["libms"]["import"]['BookLocation']['fields']['BookLocationCode']['name']="Book Location Code";
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionEn']['name'] ="English Description";
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionChi']['name']="Chinese Description";
$Lang["libms"]["import"]['BookLocation']['fields']['BookLocationCode']['req']=true;
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionEn']['req'] =true;
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionChi']['req']=true;


$Lang["libms"]["import"]['SpeicalOpenTime']['title'] = "Speical Openning Time Import";
$Lang["libms"]["import"]['SpeicalOpenTime']['error_msg']['upload_fail'] = "Upload Fail";
$Lang["libms"]["import"]['SpeicalOpenTime']['error_msg']['contact_admin'] = "Please contact system administrator";


$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateFrom']['name']="Date From (YYYY-MM-DD)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateEnd']['name'] ="Date End (YYYY-MM-DD)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['OpenTime']['name']="Open Time (HH:MM:SS)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['CloseTime']['name']="Close Time (HH:MM:SS)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['Open']['name']="Open 1/ Close 0";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateFrom']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateEnd']['req'] =true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['OpenTime']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['CloseTime']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['Open']['req']=true;

$Lang['libms']['import_book']['and'] = "and";
$Lang['libms']['import_book']['book_records'] = "book(s)";
$Lang['libms']['import_book']['item_records'] = "item(s)";
$Lang['libms']['import_book']['DateFormat'] = "<span class='tabletextrequire'>#</span> Date format must be YYYY-MM-DD or DD/MM/YYYY. If you use Excel to edit, please change the date format in Format Cells";

$Lang['General']['WrongDateLogic'] = "Invalid date. Start date must be earlier than End date.";

$Lang["libms"]["periodicalMgmt"]["Periodical"] = "Periodical";
$Lang["libms"]["periodicalMgmt"]["PeriodicalInfo"] = "Periodical Information";
$Lang["libms"]["periodicalMgmt"]["Basic"] = "Basic";
$Lang["libms"]["periodicalMgmt"]["Ordering"] = "Order";
$Lang["libms"]["periodicalMgmt"]["Registration"] = "Registration";
$Lang["libms"]["periodicalMgmt"]["PeriodicalCode"] = "Periodical Code";
$Lang["libms"]["periodicalMgmt"]["PeriodicalTitle"] = "Periodical Title";
$Lang["libms"]["periodicalMgmt"]["ISSN"] = "ISSN";
$Lang["libms"]["periodicalMgmt"]["Alert"] = "Alert";
$Lang["libms"]["periodicalMgmt"]["Status"] = "Status";
$Lang["libms"]["periodicalMgmt"]["DefaultStatus"] = "Default Status";
$Lang["libms"]["periodicalMgmt"]["OrderPeriod"] = "Order Period";
$Lang["libms"]["periodicalMgmt"]["StartDate"] = "Start Date";
$Lang["libms"]["periodicalMgmt"]["EndDate"] = "End Date";
$Lang["libms"]["periodicalMgmt"]["OrderStartDate"] = "Order Start Date";
$Lang["libms"]["periodicalMgmt"]["OrderEndDate"] = "Order End Date";
$Lang["libms"]["periodicalMgmt"]["OrderPrice"] = "Order Price";
$Lang["libms"]["periodicalMgmt"]["PublishFrequency"] = "Publish Frequency";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyPerUnit"] = "Publish every <!--frequencyNum--> <!--frequencyUnit--> per issue";
$Lang["libms"]["periodicalMgmt"]["PublishDate"] = "Publish Date";
$Lang["libms"]["periodicalMgmt"]["FirstIssue"] = "First Issue";
$Lang["libms"]["periodicalMgmt"]["LastIssue"] = "Last Issue";
$Lang["libms"]["periodicalMgmt"]["IssueOrCode"] = "Issue No. / Code";
$Lang["libms"]["periodicalMgmt"]["Remarks"] = "Remarks";
$Lang["libms"]["periodicalMgmt"]["RemarksAndAlert"] = "Remarks & Alert";
$Lang["libms"]["periodicalMgmt"]["RenewAlertDay"] = "Renew Alert Day(s)";
$Lang["libms"]["periodicalMgmt"]["RenewAlertDayRemarks"] = "0 means no alert";
$Lang["libms"]["periodicalMgmt"]["Generate"] = "Generate";
$Lang["libms"]["periodicalMgmt"]["Generated"] = "Generated";
$Lang["libms"]["periodicalMgmt"]["NotGeneratedYet"] = "Not Generated Yet";
$Lang["libms"]["periodicalMgmt"]["GenerateStatus"] = "Generate Status";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateRemarks"] = "System will generate the following periodical items.";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning1"] = "This order has generated item(s) already. System will delete the current periodical item(s) related to this order and re-generate new periodical item(s).";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning2"] = "Registered book(s) will not be affected. System will re-generate the periodical-related data only.";
$Lang["libms"]["periodicalMgmt"]["From"] = "From";
$Lang["libms"]["periodicalMgmt"]["To"] = "To";
$Lang["libms"]["periodicalMgmt"]["NoIssueCanBeGenerated"] = "No issues can be generated based on the order settings";
$Lang["libms"]["periodicalMgmt"]["PeriodicalRenewAlert"] = "Periodical Renew Alert";
$Lang["libms"]["periodicalMgmt"]["LastPublishDate"] = "Last Publish Date";
$Lang["libms"]["periodicalMgmt"]["Register"] = "Register";
$Lang["libms"]["periodicalMgmt"]["Registered"] = "Registered";
$Lang["libms"]["periodicalMgmt"]["NotRegistered"] = "Not Registered Yet";
$Lang["libms"]["periodicalMgmt"]["RegistrationStatus"] = "Registration Status";
$Lang["libms"]["periodicalMgmt"]["RegisteredItemNum"] = "Registered Item No.";
$Lang["libms"]["periodicalMgmt"]["RegistrationDate"] = "Registration Date";
$Lang["libms"]["periodicalMgmt"]["LastRegistrationDate"] = "Last Registration Date";
$Lang["libms"]["periodicalMgmt"]["Details"] = "Details";
$Lang["libms"]["periodicalMgmt"]["NoOfOrder"] = "No. of order";
$Lang["libms"]["periodicalMgmt"]["NoOfItem"] = "No. of item";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Day"] = "Day(s)";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Week"] = "Week(s)";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Month"] = "Month(s)";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Year"] = "Year(s)";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SelectItem"] = "Please select an item";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputValue"] = "Please input a value";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputPositiveNum"] = "Please input a positive value";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["CodeInUse"] = "The code is in-use already";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["PublishDateInUse"] = "The publish date is occupied already";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SaveBasicInfoFirst"] = "Please save basic info first";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"] = "The updated values will not be saved if you leave the page now. Are you sure you want to leave the page?";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["StartDateMustBeEarlierThanEndDate"] = "Start date must be earlier than end date";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["AddSuccess"] = "1|=|Record Added.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["AddUnsuccess"] = "0|=|Record Addition Failed.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["UpdateSuccess"] = "1|=|Record Updated.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["UpdateUnsuccess"] = "0|=|Record Update Failed.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["DeleteSuccess"] = "1|=|Record Deleted.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["DeleteUnsuccess"] = "0|=|Record Delete Failed.";

$Lang['libms']['NoRecordAtThisMoment'] = "There is no record at this moment.";

$Lang["libms"]["import_book_new"]["ImportCSVDataCol"] = array("<font color='red'>*</font>Book ID","<font color='red'>*</font>Book Title", "<font color='red'>*</font>Author", "Series Editor","Category","SubCategory","Chinese Library Classification", "Dewey Decimal Classification", "Level", "Publisher", "Language", "Published", "Edition", "ePublisher", "Preface", "ISBN");


$Lang['General']['ReturnMessage']['FailedToDelLoanAsPaymentSettled'] = '0|=|Record Delete Failed (because the over-due payment was settled)';
?>