<?php
# Editing by 

### Title
$eReportCard['Template']['SchoolNameEn'] = "Wesley Methodist School Kuala Lumpur";
$eReportCard['Template']['AcademicProgressReportEn'] = "Academic Progress Report";


### Header
$eReportCard['Template']['StudentNameEn'] = "Student's Name";
$eReportCard['Template']['ClassEn'] = "Class";
$eReportCard['Template']['FileNoEn'] = "File No.";
$eReportCard['Template']['FirstTermExaminationEn'] = "First-Term Examination";
$eReportCard['Template']['MidTermExaminationEn'] = "Mid-Term Examination";
$eReportCard['Template']['PMRTrialExaminationEn'] = "PMR Trial Examination";
$eReportCard['Template']['SPMTrialExaminationEn'] = "SPM Trial Examination";
$eReportCard['Template']['FinalTermExaminationEn'] = "Final-Term Examination";


### Mark Table
$eReportCard['Template']['GroupingEn'] = "Grouping";
$eReportCard['Template']['SubjectsEn'] = "Subjects";
$eReportCard['Template']['OverallMarksForFormEn'] = "Overall Mark for Form";
$eReportCard['Template']['StudentMarkEn'] = "Student's Mark";
$eReportCard['Template']['GradeEn'] = "Grade";
$eReportCard['Template']['HighestEn'] = "Highest";
$eReportCard['Template']['LowestEn'] = "Lowest";
$eReportCard['Template']['AverageEn'] = "Average";


### Footer
$eReportCard['Template']['TotalMarkEn'] = "Total Mark";
$eReportCard['Template']['AchievementEn'] = "Achievement";
$eReportCard['Template']['PercentageEn'] = "Percentage";
$eReportCard['Template']['GPMPEn'] = "GPMP";
$eReportCard['Template']['NoOfSubjectTakenEn'] = "No of subj taken";
$eReportCard['Template']['OverallResultsEn'] = "Overall results";
$eReportCard['Template']['PlacementClassEn'] = "Placement (Class)";
$eReportCard['Template']['AttendanceEn'] = "Attendance";
$eReportCard['Template']['DaysEn'] = "Days";
$eReportCard['Template']['ConductEn'] = "Conduct";


### Comment and Signature
$eReportCard['Template']['CommentsEn'] = "Comments";
$eReportCard['Template']['PrincipalCommentsEn'] = "Principal Comments";
$eReportCard['Template']['HomeroomTeacherEn'] = "Homeroom Teacher";
$eReportCard['Template']['DateEn'] = "Date";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['VicePrincipalEn'] = "Vice-Principal";
$eReportCard['Template']['UpperSecondarySupervisorEn'] = "Upper Secondary Supervisor";
$eReportCard['Template']['LowerSecondarySupervisorEn'] = "Lower Secondary Supervisor";
$eReportCard['Template']['FormSupervisorEn'] = "Form Supervisor";


### Overall Result
$eReportCard['Template']['OverallResultArr']['Excellent'] = "Excellent";
$eReportCard['Template']['OverallResultArr']['VeryGood'] = "Very Good";
$eReportCard['Template']['OverallResultArr']['Good'] = "Good";
$eReportCard['Template']['OverallResultArr']['Satisfactory'] = "Satisfactory";
$eReportCard['Template']['OverallResultArr']['Fair'] = "Fair";
$eReportCard['Template']['OverallResultArr']['Weak'] = "Weak";


### Subject Comment Page
$eReportCard['Template']['LowerSecondaryEn'] = "Lower Secondary";
$eReportCard['Template']['UpperSecondaryEn'] = "Upper Secondary";
$eReportCard['Template']['CommentSheetEn'] = "Comment Sheet";
$eReportCard['Template']['MarkEn'] = "Mark";
$eReportCard['Template']['SubjectTeacherEn'] = "Subject Teacher";


### Customized Wordings
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['AdditionalComment'] = "Principal Comment";
//2011-1115-1038-08067
$eReportCard['RemarkAbsentZeorMark'] = "0 (ABS)";
?>