<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name 姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "Class Name 班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender 性別";
$eReportCard['Template']['StudentInfo']['StudentNo'] = "Student No. 學生編號";
$eReportCard['Template']['StudentInfo']['DateOfIssue'] = "Date of Issue 派發日期";
$eReportCard['Template']['StudentInfo']['DateOfBirth'] = "Date of Birth 出生日期";

# MS Footer Row
$eReportCard['Template']['OverallResultEn'] = "Grand Total";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Pupils in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "全班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Pupils in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "全級人數";

$eReportCard['Template']['SubjectOverall'] = "總成績<br>Overall Result";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['DisplayOverallResult'] = "Display Overall Result";
$eReportCard['DisplayGrandAvg'] = "Display Average Mark";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";

$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

# Summary Info
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['NeatnessEn'] = "Neatness";
$eReportCard['Template']['NeatnessCh'] = "整潔";
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave";
$eReportCard['Template']['EarlyLeaveCh'] = "早退次數";
$eReportCard['Template']['Merit_and_DemeritEn'] = "Merit / Demerit";
$eReportCard['Template']['Merit_and_DemeritCh'] = "功 / 過";
$eReportCard['Template']['1stTermEn'] = "First Term";
$eReportCard['Template']['1stTermCh'] = "上學期";
$eReportCard['Template']['2ndTermEn'] = "Second Term";
$eReportCard['Template']['2ndTermCh'] = "下學期";
$eReportCard['Template']['WholeYearEn'] = "Whole Year";
$eReportCard['Template']['WholeYearCh'] = "全年";


# MISC Table
$eReportCard['Template']['Remark'] = "Remarks 備註";  
$eReportCard['Template']['ClassTeacherComment'] = "Comment 評語";
$eReportCard['Template']['eca'] = "ECA 課外活動";

# Signature Table
$eReportCard['Template']['PrincipalNameEn'] = "王麥潔麗";
$eReportCard['Template']['PrincipalNameCh'] = "Mrs. Alice Wong";
$eReportCard['Template']['ClassTeacherFemale'] = "Class Mistress<br>班主任";
$eReportCard['Template']['ClassTeacherMale'] = "Class Master<br>班主任";
$eReportCard['Template']['Principal'] = "Principal<br>校長";
$eReportCard['Template']['ParentGuardian'] = "Parent/Guardian<br>家長/監護人";
$eReportCard['Template']['SchoolChop'] = "School Chop<br>校印";

# Title
$eReportCard['Template']['SchoolNameEn'] = "St. Clare's Primary School";
$eReportCard['Template']['SchoolNameCh'] = "聖嘉勒小學";
$eReportCard['Template']['AddressEn'] = "3-6 Prospect Place, Bonham Road, HK";
$eReportCard['Template']['AddressCh'] = "香港般咸道光景台3-6號";
$eReportCard['Template']['Telephone'] = "Telephone 電話: 25472751";
$eReportCard['Template']['Fax'] = "Fax 傳真: 25594139";
$eReportCard['Template']['Email'] = "Email 電郵: stclareprimary@hotmail.com";
$eReportCard['Template']['Website'] = "Webpage URL 網址: http://scps.school.hk";
$eReportCard['Template']['TermReportTitleEn'] = "ACADEMIC REPORT";
$eReportCard['Template']['TermReportTitleCh'] = "學業成績表";
$eReportCard['Template']['ConsolidatedReportTitleEn'] = "ANNUAL ACADEMIC REPORT";
$eReportCard['Template']['ConsolidatedReportTitleCh'] = "全年學業成績表";

$eReportCard['Template']['MarkingSchemeMarks'] = "Marks";
$eReportCard['Template']['MarkingSchemeGrade'] = "Grade";

$eReportCard['Template']['Mr'] = "Mr.";
$eReportCard['Template']['Miss'] = "Miss";
$eReportCard['Template']['Mrs'] = "Mrs.";
$eReportCard['Template']['Ms'] = "Ms.";
$eReportCard['Template']['Dr'] = "Dr.";
$eReportCard['Template']['Prof'] = "Prof.";

?>