<?php
# Editing by 

### General language file ###

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "--";

$eReportCard['SchemesFullMark'] = "Full Mark";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

$eReportCard['Template']['StudentInfo']['MaleEn'] = "Male";
$eReportCard['Template']['StudentInfo']['MaleCh'] = "男";
$eReportCard['Template']['StudentInfo']['FemaleEn'] = "Female";
$eReportCard['Template']['StudentInfo']['FemaleCh'] = "女";
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "學生姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班 別";
$eReportCard['Template']['StudentInfo']['GenderEn'] = "Sex";
$eReportCard['Template']['StudentInfo']['GenderCh'] = "性別";
$eReportCard['Template']['StudentInfo']['StudentNoEn'] = "Student No.";
$eReportCard['Template']['StudentInfo']['StudentNoCh'] = "學生編號";
$eReportCard['Template']['StudentInfo']['WebSAMSEn'] = "DSEJ No.";
$eReportCard['Template']['StudentInfo']['WebSAMSCh'] = "教青局編號";
$eReportCard['Template']['StudentInfo']['HKIDEn'] = "I.D. No.";
$eReportCard['Template']['StudentInfo']['HKIDCh'] = "身份證編號";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "發出日期";


# Marks Table
$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['UnitEn'] = "Units";
$eReportCard['Template']['UnitCh'] = "單位";
$eReportCard['Template']['1stTermEn'] = "1st Term";
$eReportCard['Template']['1stTermCh'] = "第一段";
$eReportCard['Template']['2ndTermEn'] = "2nd Term";
$eReportCard['Template']['2ndTermCh'] = "第二段";
$eReportCard['Template']['DailyEn'] = "Daily";
$eReportCard['Template']['DailyCh'] = "平時";
$eReportCard['Template']['ExamEn'] = "Exam";
$eReportCard['Template']['ExamCh'] = "考試";
$eReportCard['Template']['FinalEn'] = "Final";
$eReportCard['Template']['FinalCh'] = "全學年";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['U_Prep_HistoryEn'] = "University Prep - History";
$eReportCard['Template']['U_Prep_HistoryCh'] = "升大歷史輔導";
$eReportCard['Template']['U_Prep_MathEn'] = "University Prep - Mathematics";
$eReportCard['Template']['U_Prep_MathCh'] = "升大數學複習";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Rank in the Class";
$eReportCard['Template']['ClassPositionCh'] = "全班名次";
$eReportCard['Template']['FormPositionEn'] = "Rank in the Form";
$eReportCard['Template']['FormPositionCh'] = "全級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "Number of Students in the Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "全班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "Number of Students in the Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "全級人數";

$eReportCard['Template']['AverageEn'] = "Average";
$eReportCard['Template']['AverageCh'] = "平均成績";
$eReportCard['Template']['RankEn'] = "Rank";
$eReportCard['Template']['RankCh'] = "考列名次";


# CSV Info
$eReportCard['Template']['UnexcusedDaysAbsentEn'] = "Unexcused Absence (Periods)";
$eReportCard['Template']['UnexcusedDaysAbsentCh'] = "曠課節數";
$eReportCard['Template']['DaysAbsentEn'] = "Excused Absence (Periods)";
$eReportCard['Template']['DaysAbsentCh'] = "缺席節數";
$eReportCard['Template']['TimesLateEn'] = "Late Attendance (Times)";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行等第"; 
$eReportCard['Template']['IncompleteHomeworkEn'] = "Incomplete homework";
$eReportCard['Template']['IncompleteHomeworkCh'] = "欠功課次數"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

$eReportCard['Template']['CommentEn'] = "Comments";
$eReportCard['Template']['CommentCh'] = "評語";

$eReportCard['Template']['ExtraActivityEn'] = "Extracurricular Activities";
$eReportCard['Template']['ExtraActivityCh'] = "課外活動";
$eReportCard['Template']['ServiceEn'] = "Services";
$eReportCard['Template']['ServiceCh'] = "服務";
$eReportCard['Template']['AwardPunishEn'] = "Awards and Punishment";
$eReportCard['Template']['AwardPunishCh'] = "獎懲";
$eReportCard['Template']['FinalCommentEn'] = "Final Comments";
$eReportCard['Template']['FinalCommentCh'] = "學年結語";

$eReportCard['Template']['NextYearClassCh'] = "下學年就讀";

$eReportCard['Template']['RemarkCh'] = "備註";
$eReportCard['Template']['RemarkArr']['A'] = "A—表現優異；";
$eReportCard['Template']['RemarkArr']['B'] = "B—表現良好；";
$eReportCard['Template']['RemarkArr']['C'] = "C—仍需努力。";

$eReportCard['Template']['DailyLifeReportCh'] = "生活報告";


# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

# Master Report
$eReportCard['MasterReport']['DataShortName']['ClassNumber'] = "班號";
$eReportCard['MasterReport']['DataShortName']['ChineseName'] = "中文名";
$eReportCard['MasterReport']['DataShortName']['EnglishName'] = "英文名";
$eReportCard['MasterReport']['DataShortName']['Gender'] = "性別";
$eReportCard['MasterReport']['DataShortName']['Mark'] = "分數";
$eReportCard['MasterReport']['DataShortName']['GrandTotal'] = "總分";
$eReportCard['MasterReport']['DataShortName']['GrandAverage'] = "平均分";
$eReportCard['MasterReport']['DataShortName']['OrderMeritForm'] = "全級名次";
$eReportCard['MasterReport']['DataShortName']['OrderMeritClass'] = "全班名次";
$eReportCard['MasterReport']['DataShortName']['FailSubjectUnit'] = "單位";
$eReportCard['MasterReport']['OtherInfo']['Conduct'] = "Conduct";
$eReportCard['MasterReport']['OtherInfo']['TimesLate'] = "遲到次數";
$eReportCard['MasterReport']['OtherInfo']['ExcusedAbsencePeriods'] = "缺席節數";
$eReportCard['MasterReport']['OtherInfo']['UnexcusedAbsencePeriods'] = "曠課節數";
$eReportCard['MasterReport']['OtherInfo']['FinalComments'] = "升留級";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

# Signature
$eReportCard['Template']['ClassTeacherSignStr'] ="班主任簽署";
$eReportCard['Template']['RemarkStr'] ="備註 :<br/>1. 只供校內查閱，不作文件影印及證明。<br/>2. 須與該生之入學註冊表一併歸入檔案。<br/>3. 須經負責部門審查及蓋章。<br/>4. 背面可填寫該生於該年的備忘事項，並註明日期及簽署。";


# Final Comment
$eReportCard['Template']['FinalCommentArr']['GrandAverageFailed'] = "學年平均成績不及格, 次學年仍留<!--FormName-->年級.";
$eReportCard['Template']['FinalCommentArr']['ExceededSubjectFailUnit'] = "不及格科目共<!--FailedSubjectWeight-->單位, 次學年仍留<!--FormName-->年級.";
$eReportCard['Template']['FinalCommentArr']['ReExamSubject'] = "應補考<!--SubjectNameList-->";
$eReportCard['Template']['FinalCommentArr']['AttendSummerClass'] = "應入夏令班補修";
$eReportCard['Template']['FinalCommentArr']['ReExamFailedAndRetain'] = "補考不及格, 次學年仍留<!--FormName-->年級";
$eReportCard['Template']['FinalCommentArr']['ReExamPassedAndGraduate'] = "各科補考及格, 准予畢業";
$eReportCard['Template']['FinalCommentArr']['PromoteToForm'] = "次學年准升<!--FormName-->年級";
$eReportCard['Template']['FinalCommentArr']['CanBeGraduate'] = "准予畢業";

$eReportCard['MasterReport']['OtherInfo']['FinalComments'] = "Promotion";
?>