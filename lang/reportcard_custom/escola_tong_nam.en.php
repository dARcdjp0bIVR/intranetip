<?php
# Editing by 

### Customization language file for Escola Tong Nam (Macau) ###

$eReportCard['SchemesFullMark'] = "Full Mark";

$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "學號";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";
$eReportCard['Template']['StudentInfo']['AcademicYear'] = "年度";
$eReportCard['Template']['StudentInfo']['DateOfIssue'] = "發出日期";

$eReportCard['Template']['ReportTitleEn'] = "STUDENT REPORT";
$eReportCard['Template']['ReportTitleCh'] = "學生成績報告表";

$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";

$eReportCard['Template']['1stTermCh'] = "上學期";
$eReportCard['Template']['1stTermEn'] = "1st Term";
$eReportCard['Template']['2ndTermCh'] = "下學期";
$eReportCard['Template']['2ndTermEn'] = "2nd Term";
$eReportCard['Template']['3rdTermEn'] = "3rd Term";

$eReportCard['Template']['1stHalfCh'] = "第一段";
$eReportCard['Template']['1stHalfEn'] = "1st Half";
$eReportCard['Template']['2ndHalfCh'] = "第二段";
$eReportCard['Template']['2ndHalfEn'] = "2nd Half";
$eReportCard['Template']['3rdHalfCh'] = "第三段";
$eReportCard['Template']['3rdHalfEn'] = "3rd Half";
$eReportCard['Template']['WholeTermCh'] = "總結";
$eReportCard['Template']['WholeTermEn'] = "Whole Term";
$eReportCard['Template']['WholeYearCh'] = "學年成績";
$eReportCard['Template']['WholeYearEn'] = "Whole Year";
$eReportCard['Template']['GraduateExamCh'] = "畢業試成績";
$eReportCard['Template']['GraduateExamEn'] = "Graduate Exam";

$eReportCard['Template']['1stSemesterCh'] = "上學期";
$eReportCard['Template']['1stSemesterEn'] = "1st Semester";
$eReportCard['Template']['2ndSemesterCh'] = "下學期";
$eReportCard['Template']['2ndSemesterEn'] = "2nd Semester";

$eReportCard['Template']['SemesterTestCh'] = "測驗";
$eReportCard['Template']['SemesterTestEn'] = "Test";
$eReportCard['Template']['SemesterExamCh'] = "考試";
$eReportCard['Template']['SemesterExamEn'] = "Exam";
$eReportCard['Template']['WholeSemesterCh'] = "總結";
$eReportCard['Template']['WholeSemesterEn'] = "Whole Semester";

$eReportCard['Template']['OverallResultEn'] = "Total Marks";
$eReportCard['Template']['OverallResultCh'] = "積分總計";
$eReportCard['Template']['AvgMarkEn'] = "Average Marks";
$eReportCard['Template']['AvgMarkCh'] = "平均分數";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班內名次";
$eReportCard['Template']['ClassNoStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNoStudentCh'] = "全班人數";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "全級名次";
$eReportCard['Template']['FormNoStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNoStudentCh'] = "全級人數";
$eReportCard['Template']['DaysAbsentEn'] = "No. of Periods Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課節數";
$eReportCard['Template']['TimesLateEn'] = "No. of Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct Grade";
$eReportCard['Template']['ConductCh'] = "操行等第";
$eReportCard['Template']['RemarkEn'] = "Remarks";   
$eReportCard['Template']['RemarkCh'] = "備註";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br />總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['ClassTeacher'] = "班主任<br />Class Teacher";
$eReportCard['Template']['Principal'] = "校長<br>Principal";
$eReportCard['Template']['ParentGuardian'] = "家長/監護人<br />Parent/Guardian";
$eReportCard['Template']['GeneralInstruction'] = "說明<br />General Instruction";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['MeritsEn'] = "Merits";
$eReportCard['Template']['MeritsCh'] = "優點";
$eReportCard['Template']['MinorCreditEn'] = "Minor Credit";
$eReportCard['Template']['MinorCreditCh'] = "小功";
$eReportCard['Template']['MajorCreditEn'] = "Major Credit";
$eReportCard['Template']['MajorCreditCh'] = "大功";
$eReportCard['Template']['DemeritsEn'] = "Demerits";
$eReportCard['Template']['DemeritsCh'] = "缺點";
$eReportCard['Template']['MinorFaultEn'] = "Minor Fault";
$eReportCard['Template']['MinorFaultCh'] = "小過";
$eReportCard['Template']['MajorFaultEn'] = "Major Fault";
$eReportCard['Template']['MajorFaultCh'] = "大過";
$eReportCard['Template']['RemarksEn'] = "Remarks";  
$eReportCard['Template']['RemarksCh'] = "備註";  
$eReportCard['Template']['ClassTeacherCommentEn'] = "Class Teacher Comment";
$eReportCard['Template']['ClassTeacherCommentCh'] = "班主任評語";
$eReportCard['Template']['AwardsEn'] = "Merits & Demerits";
$eReportCard['Template']['AwardsCh'] = "獎懲";
$eReportCard['Template']['eca'] = "課外活動 Extra-curricular Activities";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "----";
$eReportCard['DisplayOverallResult'] = "Display Overall Result";
$eReportCard['DisplayGrandAvg'] = "Display Average Mark";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";

$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

# Summary Info
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct Grade";
$eReportCard['Template']['ConductCh'] = "操行等第";
$eReportCard['Template']['PolitenessEn'] = "Politeness";
$eReportCard['Template']['PolitenessCh'] = "有禮";
$eReportCard['Template']['DisciplineEn'] = "Discipline";
$eReportCard['Template']['DisciplineCh'] = "紀律";
$eReportCard['Template']['ConcentrationEn'] = "Concentration";
$eReportCard['Template']['ConcentrationCh'] = "專心";
$eReportCard['Template']['TidinessEn'] = "Tidiness";
$eReportCard['Template']['TidinessCh'] = "整潔";

// Grand MS
$eReportCard['Template']['Merits'] = "Merits";
$eReportCard['Template']['Demerits'] = "Demerits";
$eReportCard['Template']['Minor Credit'] = "Minor Credit";
$eReportCard['Template']['Major Credit'] = "Major Credit";
$eReportCard['Template']['Minor Fault'] = "Minor Fault";
$eReportCard['Template']['Major Fault'] = "Major Fault";
$eReportCard['Template']['Days Absent'] = "Days Absent";
$eReportCard['Template']['Time Late'] = "Time Late";
$eReportCard['Template']['GRADE'] = "ECA Grade";

$eReportCard['DailyPerformanceUpload'] = "Daily Performance Record";

##### For Re-exam module #####
$eReportCard['ReExam'] = "Re-exam";
$eReportCard['ReExamStudentList'] = "Re-exam Student List";
$eReportCard['ReExamScore'] = "Re-exam Score";
$eReportCard['UploadReExamResult'] = "Upload Re-exam Score";
$eReportCard['LastUploadedDate'] = "Last Uploaded Date";
$eReportCard['LastUploadedBy'] = "Last Uploaded By";

$eReportCard['Download'] = "Download";

$eReportCard['Template']['RemarkGraduationExam'] = array();
$eReportCard['Template']['RemarkGraduationExam'][0] = "全科合格，准予參加畢業試。";
$eReportCard['Template']['RemarkGraduationExam'][1] = "符合學校規定，准予參加畢業試。";
$eReportCard['Template']['RemarkGraduationExam'][2] = "成績不合格，應予留級。";

$eReportCard['Template']['RemarkGraduation'] = array();
$eReportCard['Template']['RemarkGraduation'][0] = "全科合格，准予畢業。";
$eReportCard['Template']['RemarkGraduation'][1] = "成績不合格，不得畢業。";
$eReportCard['Template']['RemarkGraduation'][2] = "<!--PassSubjects-->補考合格，准予畢業。";
$eReportCard['Template']['RemarkGraduation'][3][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['RemarkGraduation'][3][1] = "<!--FailedSubjects-->補考不合格，不得畢業。";


$eReportCard['Template']['RemarkPromotion'] = array();
$eReportCard['Template']['RemarkPromotion'][0] = "全科合格，准予升級。";
$eReportCard['Template']['RemarkPromotion'][1] = "成績不合格，應予留級。";
$eReportCard['Template']['RemarkPromotion'][2] = "<!--PassSubjects-->補考合格，准予升級。";
$eReportCard['Template']['RemarkPromotion'][3][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['RemarkPromotion'][3][1] = "<!--FailedSubjects-->補考不合格，應予留級。";
$eReportCard['Template']['RemarkPromotion'][4][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['RemarkPromotion'][4][1] = "<!--FailedSubjects-->補考不合格，准予帶科升級。";

$eReportCard['Template']['SubjectSeparator'] = "、";

$eReportCard['Template']['NoReExamRequired'] = "There are no report templates which require re-examination.";
##### End of Re-exam module #####

?>