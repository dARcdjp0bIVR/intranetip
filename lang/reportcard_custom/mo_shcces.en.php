<?php 
# Editing by 

$eReportCard['RemarkExempted'] = "--";
$eReportCard['RemarkDropped'] = "--";
$eReportCard['RemarkAbsentZeorMark'] = "--";
// $eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkAbsentNotConsidered'] = "--";
$eReportCard['RemarkNotAssessed'] = "N.A.";

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";
$eReportCard['Template']['StudentInfo']['Class_ClassNo'] = '班別/班號';

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Student Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "學生姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['Class_ClassNoEn'] = 'Class/No.';
$eReportCard['Template']['StudentInfo']['Class_ClassNoCh'] = '班別/編號';
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "DSEJ No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "教青局編號";
$eReportCard['Template']['StudentInfo']['StudentNoEn'] = "Student No.";
$eReportCard['Template']['StudentInfo']['StudentNoCh'] = "學生編號";
$eReportCard['Template']['StudentInfo']['DateOfBirthEn'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['StudentInfo']['AcademicYearEn'] = "School Year";
$eReportCard['Template']['StudentInfo']['AcademicYearCh'] = "學年";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "發出日期";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";

$eReportCard['Template']['UnitEn'] = "Unit";
$eReportCard['Template']['UnitCh'] = "單位";
$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

// $eReportCard['RemarkExempted'] = "/";
// $eReportCard['RemarkDropped'] = "*";
// $eReportCard['RemarkAbsentNotConsidered'] = "ABS";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['eca'] = "Extra-curricular Activities"."&nbsp;"."課外活動";
$eReportCard['Template']['VoluntaryHoursEn'] = "Hours of Voluntary Service";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";
$eReportCard['Template']['CommentEn'] = "Comments";
$eReportCard['Template']['CommentCh'] = "評語";

# Signature Table
// $eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER"."&nbsp;"."班主任";
// $eReportCard['Template']['Principal'] = "PRINCIPAL"."&nbsp;"."校長";
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

$eReportCard['Template']['TermEn'][0] = "1st Term";
$eReportCard['Template']['TermEn'][1] = "2nd Term";
$eReportCard['Template']['TermEn'][2] = "3rd Term";
$eReportCard['Template']['TermCh'][0] = "第一學期";
$eReportCard['Template']['TermCh'][1] = "第二學期";
$eReportCard['Template']['TermCh'][2] = "第三學期";
$eReportCard['Template']['FinalAverageEn'] = 'Final Average';
$eReportCard['Template']['FinalAverageCh'] = '全年平均分';
$eReportCard['Template']['FinalResultEn'] = 'Final Result';
//$eReportCard['Template']['FinalResultCh'] = '全年總成績';
$eReportCard['Template']['FinalResultCh'] = '大考成績';
$eReportCard['Template']['TotalUnitFailedEn'] = 'Total Unit(s) Failed';
$eReportCard['Template']['TotalUnitFailedCh'] = '不合格單位';
$eReportCard['Template']['AverageEn'] = 'Average';
$eReportCard['Template']['AverageCh'] = '平均成績';
$eReportCard['Template']['NumberOfStudentsInClassEn'] = 'Number of Students in Class';
$eReportCard['Template']['NumberOfStudentsInClassCh'] = '全班人數';
$eReportCard['Template']['PositionInClassEn'] = 'Position in Class';
$eReportCard['Template']['PositionInClassCh'] = '班內名次';
$eReportCard['Template']['PeriodsAbsentEn'] = 'Periods Absent';
$eReportCard['Template']['PeriodsAbsentCh'] = '缺席節數';
$eReportCard['Template']['TimesTardyEn'] = 'Times Tardy';
$eReportCard['Template']['TimesTardyCh'] = '遲到次數';

$eReportCard['Template']['CreditsEn'] = 'Credit(s)';
$eReportCard['Template']['CreditsCh'] = '積點';
$eReportCard['Template']['MeritsEn'] = 'Merit(s)';
$eReportCard['Template']['MeritsCh'] = '優點';
$eReportCard['Template']['MisdemeanorsEn'] = 'Misdemeanor(s)';
$eReportCard['Template']['MisdemeanorsCh'] = '缺點';
$eReportCard['Template']['Minor MisdeedsEn'] = 'Minor Misdeed(s)';
$eReportCard['Template']['Minor MisdeedsCh'] = '小過';
$eReportCard['Template']['Major MisdeedsEn'] = 'Major Misdeed(s)';
$eReportCard['Template']['Major MisdeedsCh'] = '大過';
$eReportCard['Template']['ConductEn'] = 'Conduct';
$eReportCard['Template']['ConductCh'] = '操行';
$eReportCard['Template']['AppearanceEn'] = 'Appearance';
$eReportCard['Template']['AppearanceCh'] = '儀容';
$eReportCard['Template']['Learning AttitudeEn'] = 'Learning Attitude';
$eReportCard['Template']['Learning AttitudeCh'] = '學習態度';
$eReportCard['Template']['Sense of ResponsibilityEn'] = 'Sense of Responsibility';
$eReportCard['Template']['Sense of ResponsibilityCh'] = '責任感';

$eReportCard['Template']['PrincipalEn'] = 'Principal';
$eReportCard['Template']['PrincipalCh'] = '校長';
$eReportCard['Template']['ClassTeacherEn'] = 'Class Adviser';
$eReportCard['Template']['ClassTeacherCh'] = '班主任';
$eReportCard['Template']['ParentGuardianEn'] = 'Parent/Guardian';
$eReportCard['Template']['ParentGuardianCh'] = '家長/監護人';


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

$eReportCard['OtherInfoArr']['otherconduct'] = "Other Conduct Record";
$eReportCard['OtherInfoArr']['merit_demerit'] = "Merits & Demerits Record";

# Student Promotion Status
$eReportCard["PromotionStatus"][1] = "Promoted";
$eReportCard["PromotionStatus"][2] = "Retained";
$eReportCard["PromotionStatus"][4] = "Graduated";
$eReportCard["PromotionStatus"]["升級"] = "Promoted";
$eReportCard["PromotionStatus"]["Promoted"] = "Promoted";
$eReportCard["PromotionStatus"]["留級"] = "Retained";
$eReportCard["PromotionStatus"]["Retained"] = "Retained";
$eReportCard["PromotionStatus"]["畢業"] = "Graduated";
$eReportCard["PromotionStatus"]["Graduated"] = "Graduated";

$eReportCard["PromotionStatusDisplayArr"]["Promoted"]['P'] = "Promoted to Primary <!--Level-->";
$eReportCard["PromotionStatusDisplayArr"]["Promoted"]['S'] = "Promoted to Form <!--Level-->";
$eReportCard["PromotionStatusDisplayArr"]["Retained"]['P'] = "Retained in Primary <!--Level-->";
$eReportCard["PromotionStatusDisplayArr"]["Retained"]['S'] = "Retained in Form <!--Level-->";
$eReportCard["PromotionStatusDisplayArr"]["Graduated"] = "Graduated";

?>