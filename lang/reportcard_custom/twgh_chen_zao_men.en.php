<?php
# Editing by 

### General language file ###
$eReportCard['RemarkAbsentZeorMark'] = "ABS";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkNotAssessed'] = "N.A.";

$eReportCard['SchemesFullMark'] = "Full Mark";

$eReportCard['Template']['TermReportTitleEn'] = "Mid-Year Student Achievement Report";
$eReportCard['Template']['TermReportTitleCh'] = "學 生 中 期 成 績 報 告 表";
$eReportCard['Template']['YearReportTitleEn'] = "Yearly Student Achievement Report";
$eReportCard['Template']['YearReportTitleCh'] = "學 生 全 年 成 績 報 告 表";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name&nbsp; 姓 名";
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓 名";
$eReportCard['Template']['StudentInfo']['Class'] = "Class 班 級";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班 級";
$eReportCard['Template']['StudentInfo']['ClassNum'] = "Class Number 班 號";
$eReportCard['Template']['StudentInfo']['ClassNumEn'] = "Class Number";
$eReportCard['Template']['StudentInfo']['ClassNumCh'] = "班 號";
$eReportCard['Template']['StudentInfo']['DateOfBirth'] = "Date of Birth　出生日期";
$eReportCard['Template']['StudentInfo']['DateOfBirthEn'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['DateOfBirthCh'] = "出生日期";
$eReportCard['Template']['StudentInfo']['AdmissionNum'] = "Admission Number 學 號";
$eReportCard['Template']['StudentInfo']['AdmissionNumEn'] = "Admission Number";
$eReportCard['Template']['StudentInfo']['AdmissionNumCh'] = "學 號";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['SubjectEn'] = "SUBJECT";
$eReportCard['Template']['SubjectCh'] = "科　　　　目";
$eReportCard['Template']['WeightingEn'] = "Weighting";
$eReportCard['Template']['WeightingCh'] = "加權";
$eReportCard['Template']['MarkEn'] = "Mark";
$eReportCard['Template']['MarkCh'] = "分數";
$eReportCard['Template']['MarkPercentage'] = "(100)";
$eReportCard['Template']['PositionEn'] = "Position";
$eReportCard['Template']['PositionCh'] = "名次";
$eReportCard['Template']['YearPositionEn'] = "Position";
$eReportCard['Template']['YearPositionCh'] = "全年名次";
$eReportCard['Template']['Term1En'] = "1st Term";
$eReportCard['Template']['Term1Ch'] = "上學期";
$eReportCard['Template']['Term2En'] = "2nd Term";
$eReportCard['Template']['Term2Ch'] = "下學期";
$eReportCard['Template']['YearlyEn'] = "Yearly";
$eReportCard['Template']['YearlyCh'] = "全年";
$eReportCard['Template']['CmpPrefixEn'] = " ";
$eReportCard['Template']['CmpPrefixCh'] = "　";

$eReportCard['Template']['AverageEn'] = "AVERAGE";
$eReportCard['Template']['AverageCh'] = "平 均 分 數";
$eReportCard['Template']['PositionInFormEn'] = "POSITION IN CLASS LEVEL";
$eReportCard['Template']['PositionInFormCh'] = "全 級 名 次";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "No. of Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺 課 日 數";
$eReportCard['Template']['TimesLateEn'] = "No. of Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲 到 次 數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操 行";
$eReportCard['Template']['ClassTeacherComment'] = "REMARKS　班主任評語";
$eReportCard['Template']['ECAService'] = "EXTRA-CURRICULAR ACTIVITIES / SERVICES　課 外 活 動 / 服 務";
$eReportCard['Template']['AwardPunishment'] = "AWARD AND PUNISHMENT　獎 懲";
$eReportCard['Template']['PromotedToEn'] = "Promoted To";
$eReportCard['Template']['PromotedToCh'] = "准 予 升 讀";
$eReportCard['Template']['FailedRemark'] = "# failed 不合格";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['FormTeacherEn'] = "Form-Teacher";
$eReportCard['Template']['FormTeacherCh'] = "班 主 任";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校 長";
$eReportCard['Template']['ParentGuardianEn'] = "Parent/Guardian";
$eReportCard['Template']['ParentGuardianCh'] = "家 長 / 監 護 人";
$eReportCard['Template']['DateEn'] = "Date";
$eReportCard['Template']['DateCh'] = "日期";

$eReportCard['Template']['PrincipalName'] = "Ms. AU YEUNG Suk Lan";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

?>