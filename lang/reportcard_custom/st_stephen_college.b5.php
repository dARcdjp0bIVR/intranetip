<?php
# Editing by 

### Customization language file for St. Stephen's College ###

$eReportCard['SchoolNameEn'] = "St Stephen's College";
$eReportCard['SchoolNameCh'] = "聖士提反書院";

//$eReportCard['PrincipalName'] = "Dr Louise Y S Law";
$eReportCard['PrincipalName'] = "Ms Carol C Yang";

//Other Info
$eReportCard['AttendanceInfoUpload'] = "出席記錄";
$eReportCard['MeritRecordUpload'] = "優點記錄";
$eReportCard['AnnualDemeritRecordUpload'] = "過失記錄";
$eReportCard['AwardRecordUpload'] = "主要獎項及成就";
$eReportCard['ECAUpload'] = "課外活動";

// Settings > Curriculum Expectation
$eReportCard['CurriculumExpectation'] = "課程概要";

// Settings > Personal Characteristics
$eReportCard['PersonalCharacteristics'] = "個人表現";
$eReportCard['PersonalCharacteristicsPool'] = "Personal Characteristics Pool";
$eReportCard['PersonalCharacteristicsFormSettings'] = "Personal Characteristics Form Settings";
$eReportCard['NewPersonalCharacteristics'] = "New Personal Characteristics";
$eReportCard['EditPersonalCharacteristics'] = "Edit Personal Characteristics";
$eReportCard['Title_EN'] = "Title (English)";
$eReportCard['Title_CH'] = "Title (Chinese)";
$eReportCard['NoPersonalCharacteristicsSetting'] = "No personal characteristics is set.";
$eReportCard['ApplyOtherSubjects'] = "Apply to other subjects (same Class Level)";

//$eReportCard['PersonalCharacteristicsScalePointsAry'] = array("NA", "UN", "S", "G", "EX");
$eReportCard['Template']['PersonalParticularsEn'] = "Personal Particulars";
$eReportCard['Template']['PersonalParticularsCh'] = "學生資料";
$eReportCard['Template']['NameEn'] = "Name";
$eReportCard['Template']['NameCh'] = "姓名";
$eReportCard['Template']['SexEn'] = "SEX";
$eReportCard['Template']['SexCh'] = "性別";
$eReportCard['Template']['ClassEn'] = "Class";
$eReportCard['Template']['ClassCh'] = "班別";
$eReportCard['Template']['IDNOEn'] = "HKID No";
$eReportCard['Template']['IDNOCh'] = "身份証號碼";
$eReportCard['Template']['STRNEn'] = "STRN";
$eReportCard['Template']['STRNCh'] = "學生編號";
$eReportCard['Template']['DOBEn'] = "Date of Birth";
$eReportCard['Template']['DOBCh'] = "出生日期";
$eReportCard['Template']['Photo'] = "Photo";
$eReportCard['Template']['ClassTeacherEn'] = "Class Teacher";
$eReportCard['Template']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['ClassTeacherCommentEn'] = "Class Teacher Comment";
$eReportCard['Template']['ClassTeacherCommentCh'] = "評語";
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['SubjectCh'] = "科目";

$eReportCard['Template']['ExplanatoryNotesEn'] = "Explanatory Notes";
$eReportCard['Template']['ExplanatoryNotesCh'] = "註釋";
$eReportCard['Template']['AcademicResultsEn'] = "Academic Results";
$eReportCard['Template']['AcademicResultsCh'] = "學業成績";
$eReportCard['Template']['StudentAverageEn'] = "Student's Average";
$eReportCard['Template']['StudentAverageCh'] = "該生平均分";
$eReportCard['Template']['RankInFormEn'] = "Rank in Form";
$eReportCard['Template']['RankInFormCh'] = "該生於全級等第";

$eReportCard['Template']['AttendanceEn'] = "Attendance";
$eReportCard['Template']['AttendanceCh'] = "出席紀錄";
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['TotalNumberOfSchoolDaysEn'] = "Total Number of School Days";
$eReportCard['Template']['TotalNumberOfSchoolDaysCh'] = "上學日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['MeritEn'] = "Merits";
$eReportCard['Template']['MeritCh'] = "優點紀錄";
$eReportCard['Template']['MeritCountEn'] = "Merits";
$eReportCard['Template']['MeritCountCh'] = "優點次數";
$eReportCard['Template']['PrincipalMeritListEn'] = "Principal's Merit List";
$eReportCard['Template']['PrincipalMeritListCh'] = "小功次數";
$eReportCard['Template']['DemeritsEn'] = "Demerits";
$eReportCard['Template']['DemeritsCh'] = "過失紀錄";
$eReportCard['Template']['MinorBreachesEn'] = "Minor Breaches";
$eReportCard['Template']['MinorBreachesCh'] = "小過次數";
$eReportCard['Template']['MajorBreachesEn'] = "Major Breaches";
$eReportCard['Template']['MajorBreachesCh'] = "大過次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Awards&MajorAchievementsEn'] = "Awards & Major Achievements";
$eReportCard['Template']['Awards&MajorAchievementsCh'] = "獎項及主要成就";
$eReportCard['Template']['EcaEn'] = "Extra-curricular Activities";
$eReportCard['Template']['EcaCh'] = "課外活動";
$eReportCard['Template']['TeacherCommentsEn'] = "Teacher Comments";
$eReportCard['Template']['TeacherCommentsCh'] = "評語";
$eReportCard['Template']['AdditionalCommentsEn'] = "Additional Comments";
$eReportCard['Template']['AdditionalCommentsCh'] = "附加評語";

$eReportCard['Template']['CourseDescriptionEn'] = "Course Description";
$eReportCard['Template']['CourseDescriptionCh'] = "課程概要";
$eReportCard['Template']['SubjectTeacherEn'] = "Subject Teacher";
$eReportCard['Template']['SubjectTeacherCh'] = "科目老師";
$eReportCard['Template']['PersonalCharacteristicsEn'] = "Personal Characteristics";
$eReportCard['Template']['PersonalCharacteristicsCh'] = "個人表現";
$eReportCard['Template']['ResultEn'] = "Result";
$eReportCard['Template']['ResultCh'] = "成績";
$eReportCard['Template']['FullMarkEn'] = "Full Mark";
$eReportCard['Template']['FullMarkCh'] = "滿分";
$eReportCard['Template']['TermAverageEn'] = "Term Average";
$eReportCard['Template']['TermAverageCh'] = "平均分";
$eReportCard['Template']['NoOfCandidatesEn'] = "No. of Candidates";
$eReportCard['Template']['NoOfCandidatesCh'] = "與考人數";
$eReportCard['Template']['RankEn'] = "Rank";
$eReportCard['Template']['RankCh'] = "等第";


$eReportCard['Excellent_en'] = "Excellent";
$eReportCard['Good_en'] = "Good";
$eReportCard['Satisfactory_en'] = "Satisfactory";
$eReportCard['NeedsImprovement_en'] = "Needs Improvement";
$eReportCard['NotApplicable_en'] = "Not Applicable";
$eReportCard['Excellent_b5'] = "優";
$eReportCard['Good_b5'] = "良";
$eReportCard['Satisfactory_b5'] = "尚可";
$eReportCard['NeedsImprovement_b5'] = "有待改進";
$eReportCard['NotApplicable_b5'] = "不適用";

$eReportCard['Button']['back_to_PersonalCharacteristics'] = "回到".$eReportCard['PersonalCharacteristics'];
$eReportCard['Represents'] = "代表";

$eReportCard['AdditionalComment'] = "附加評語";
$eReportCard['RemarkExempted'] = "免修";
$eReportCard['RemarkAbsentZeorMark'] = "缺席 (0分)";
$eReportCard['RemarkAbsentNotConsidered'] = "缺席 (不獲考慮)";
$eReportCard['RemarkDropped'] = "退修";

### Extra Report
$eReportCard['ExtraReport']['SchoolNameEn'] = "ST. STEPHEN'S COLLEGE";
$eReportCard['ExtraReport']['SchoolNameCh'] = "聖士提反書院";
$eReportCard['ExtraReport']['SchoolNameExtra'] = "Stanley, Hong Kong";
$eReportCard['ExtraReport']['Remarks'] = "Note : Pass mark is 50% of the full mark.";


$eReportCard['AttendanceDays'] = "上課日數";
?>