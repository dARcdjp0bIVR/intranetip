<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "满分";

## School Info
$eReportCard['Template']['SchoolInfo']['NameEn'] = "CHUNG HUA HIGH SCHOOL";
$eReportCard['Template']['SchoolInfo']['NameCh'] = "芙 蓉 中 华 中 学";
$eReportCard['Template']['SchoolInfo']['Address'] = "Jalan Tun Dr. Ismail, 70200 Seremban, Negeri Sembilan, Malaysia";
$eReportCard['Template']['SchoolInfo']['Tel'] = "Tel: 06-7612782";
$eReportCard['Template']['SchoolInfo']['Fax'] = "Fax: 06-7621890";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班别";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班级";
$eReportCard['Template']['StudentInfo']['StudentAdmNoEn'] = "Adm. No";
$eReportCard['Template']['StudentInfo']['StudentAdmNoCh'] = "学号";
$eReportCard['Template']['StudentInfo']['AcademicYearEn'] = "Year";
$eReportCard['Template']['StudentInfo']['AcademicYearCh'] = "年份";
$eReportCard['Template']['StudentInfo']['Gender'] = "性别";
$eReportCard['Template']['StudentInfo']['STRN'] = "学生编号";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>总成绩";
$eReportCard['Template']['GrandTotal'] = "总分";

$eReportCard['Template']['OverallResultEn'] = "Total";
$eReportCard['Template']['OverallResultCh'] = "共计";
//2012-1024-1057-42156
//$eReportCard['Template']['AvgMarkEn'] = "Average %";
$eReportCard['Template']['AvgMarkEn'] = "Overall Average";
$eReportCard['Template']['AvgMarkCh'] = "总平均";
$eReportCard['Template']['ClassPositionEn'] = "Position In Class";
$eReportCard['Template']['ClassPositionCh'] = "名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "级名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "Class Enrolment";
$eReportCard['Template']['ClassNumOfStudentCh'] = "全班人数";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "级人数";
$eReportCard['Template']['MarksDeductedEn'] = "Marks Deducted";
$eReportCard['Template']['MarksDeductedCh'] = "扣分";
$eReportCard['Template']['ActualAverageEn'] = "Actual Average";
$eReportCard['Template']['ActualAverageCh'] = "实得平均";
//2013-0405-0845-35156
//$eReportCard['Template']['PositionOverallEn'] = "Position Overall";
$eReportCard['Template']['PositionOverallEn'] = "Overall Position";
$eReportCard['Template']['PositionOverallCh'] = "全年名次";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "NA";

$eReportCard['Template']['SubjectEng'] = "Subjects";
$eReportCard['Template']['SubjectChn'] = "科目";
$eReportCard['Template']['SemesterEn'] = 'Semester';
$eReportCard['Template']['SemesterCh'] = '学期';
$eReportCard['Template']['AverageEn'] = 'Average';
$eReportCard['Template']['AverageCh'] = '平均';
//2012-1024-1057-42156
//$eReportCard['Template']['PeriodEn'] = 'Period(s)';
$eReportCard['Template']['PeriodEn'] = 'Merit Point(s)';
$eReportCard['Template']['PeriodCh'] = '学点';
$eReportCard['Template']['TotalEn'] = 'Total';
$eReportCard['Template']['TotalCh'] = '积分';

# CSV Info
//2013-0405-0845-35156
//$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
//$eReportCard['Template']['DaysAbsentCh'] = "缺课日数";
$eReportCard['Template']['DaysAbsentEn'] = "Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "迟到次数";
$eReportCard['Template']['ConductEn'] = "Conduct Marks";
$eReportCard['Template']['ConductCh'] = "操行分数"; 
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 奖惩";
$eReportCard['Template']['Merits'] = "Merits 优点";
$eReportCard['Template']['Demerits'] = "Demerits 缺点";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小优";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大优";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小过";
$eReportCard['Template']['MajorFault'] = "Major Fault 大过";
$eReportCard['Template']['Remark'] = "Remark 备注";  
$eReportCard['Template']['eca'] = "ECA 课外活动";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任评语";
$eReportCard['Template']['ClassTeacherCommentEn'] = "Comment";
$eReportCard['Template']['ClassTeacherCommentCh'] = "评语";
$eReportCard['Template']['DayCh'] = "天";

$eReportCard['Template']['ActualAttendanceEn'] = "Actual Attendance";
$eReportCard['Template']['ActualAttendanceCh'] = "上课日数";
$eReportCard['Template']['TotalAttendanceEn'] = "Total Attendance";
$eReportCard['Template']['TotalAttendanceCh'] = "应上课日数";
$eReportCard['Template']['MedicalLeaveEn'] = "Medical Leave";
$eReportCard['Template']['MedicalLeaveCh'] = "病假";
$eReportCard['Template']['OfficialLeaveEn'] = "Official Leave";
$eReportCard['Template']['OfficialLeaveCh'] = "公假";
$eReportCard['Template']['PersonalLeaveEn'] = "Personal Leave";
$eReportCard['Template']['PersonalLeaveCh'] = "事假";
//2012-1024-1057-42156
//$eReportCard['Template']['LeaveWithoutPermissionEn'] = "Leave Without Permission";
$eReportCard['Template']['LeaveWithoutPermissionEn'] = "Truancy";
$eReportCard['Template']['LeaveWithoutPermissionCh'] = "旷课";
$eReportCard['Template']['TardinessEn'] = "Tardiness";
$eReportCard['Template']['TardinessCh'] = "迟到";
$eReportCard['Template']['LeavingEarlyEn'] = "Leaving Early";
$eReportCard['Template']['LeavingEarlyCh'] = "早退";
$eReportCard['Template']['AverageConductEn'] = "Average Conduct";
$eReportCard['Template']['AverageConductCh'] = "全年操行";
$eReportCard['Template']['ECAEn'] = "ECA";
$eReportCard['Template']['ECACh'] = "课外活动";
$eReportCard['Template']['Services&DutiesEn'] = "Services and Duties";
$eReportCard['Template']['Services&DutiesCh'] = "服务";
//2012-1024-1057-42156
//$eReportCard['Template']['AwardEn'] = "Award";
//$eReportCard['Template']['AwardCh'] = "奖励";
$eReportCard['Template']['AwardEn'] = "Award(s)";
$eReportCard['Template']['AwardCh'] = "得奖记录";
//2013-0405-0845-35156
$eReportCard['Template']['RemarkEn'] = "Remark";
$eReportCard['Template']['RemarkCh'] = "备注";
$eReportCard['Template']['PunishmentEn'] = "Punishment";
$eReportCard['Template']['PunishmentCh'] = "惩罚";
$eReportCard['Template']['ClassEnrolledNextYearEn'] = "Class to be enrolled next year";
$eReportCard['Template']['ClassEnrolledNextYearCh'] = "明年编入年级";
$eReportCard['Template']['CoCurricularActivitiesEn'] = "Co-curricular Activities";
$eReportCard['Template']['CoCurricularActivitiesCh'] = "联课活动记录";
$eReportCard['Template']['FormTeachersCommentsEn'] = "Form Teacher's Comments";
$eReportCard['Template']['FormTeachersCommentsCh'] = "班导师评语";
$eReportCard['Template']['SocietyEn'] = "Society";
$eReportCard['Template']['SocietyCh'] = "团体";
$eReportCard['Template']['PostEn'] = "Post";
$eReportCard['Template']['PostCh'] = "职务";
$eReportCard['Template']['MeritEn'] = "Merit";
$eReportCard['Template']['MeritCh'] = "功";
$eReportCard['Template']['Merit1Ch'] = "优点";
$eReportCard['Template']['Merit2Ch'] = "小功";
$eReportCard['Template']['Merit3Ch'] = "大功";
$eReportCard['Template']['DemeritEn'] = "Demerit";
$eReportCard['Template']['DemeritCh'] = "过";
$eReportCard['Template']['Demerit1Ch'] = "缺点";
$eReportCard['Template']['Demerit2Ch'] = "小过";
$eReportCard['Template']['Demerit3Ch'] = "大过";


# Signature Table
//2012-1024-1057-42156
//$eReportCard['Template']['ClassTeacher'] = "&nbsp;&nbsp;班导师签盖<br />&nbsp;&nbsp;Signature<br />&nbsp;&nbsp;of<br />&nbsp;&nbsp;Class Teacher";
//$eReportCard['Template']['Principal'] = "&nbsp;&nbsp;校长签盖<br />&nbsp;&nbsp;Signature<br />&nbsp;&nbsp;of<br />&nbsp;&nbsp;Principal";
//$eReportCard['Template']['ParentGuardian'] = "&nbsp;&nbsp;家长签盖<br />&nbsp;&nbsp;Signature<br />&nbsp;&nbsp;of<br />&nbsp;&nbsp;Parent / Guardian";
$eReportCard['Template']['ClassTeacher'] = "班导师签盖<br />Signature of<br />Form Teacher";
$eReportCard['Template']['Principal'] = "校长签盖<br />Signature of<br />Principal";
$eReportCard['Template']['ParentGuardian'] = "家长/监护人签盖<br />Signature of<br />Parent/Guardian";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派发日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班别总结", "全级名次");


?>