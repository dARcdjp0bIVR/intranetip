<?php
# Editing by 

$eReportCard['SchoolNameCh'] = "葵 涌 循 道 中 學";
$eReportCard['SchoolNameEn'] = "KWAI CHUNG METHODIST COLLEGE";
//$eReportCard['Template']['PrincipalName'] = "黃兆雄校長";
$eReportCard['Template']['PrincipalName'] = "林美儀校長";
$eReportCard['Template']['TeacherCh'] = "老師";
$eReportCard['Template']['NIL'] = "不適用 Nil ";
$eReportCard['Template']['PromotionRemarkCh'] = "學生予以<br>升級 /<br>試升 /<br>留級";
$eReportCard['Template']['PromotionRemarkEn'] = "Student is<br>promoted /<br>concessionally promoted /<br>to repeat";

$eReportCard['Template']['PassRemarksTitleCh'] = "備註 :";
$eReportCard['Template']['PassRemarksTitleEn'] = "Remarks:";
$eReportCard['Template']['PassRemarksCh'] = "	中一至中三級，學科成績合格分數為滿分的50％。中四至中六級，學科成績合格分數為滿分的40％。術科合格級別為E級。不合格成績以(&nbsp;&nbsp;&nbsp;)括弧表示。";
$eReportCard['Template']['PassRemarksEn'] = "For S.1-S.3, the passing mark is 50% of full mark. For S.4-S.6, the passing mark is 40% of full mark. For cultural subjects, <br>passing grade is E. Failed subjects are shown in brackets (&nbsp;&nbsp;&nbsp;). ";

$eReportCard['RemarkAbsent'] = "Absent";
$eReportCard['RemarkAbsentZeorMark'] = "+ : Absent (zero mark)";
$eReportCard['RemarkAbsentNotConsidered'] = "- : Absent (not considered)";
$eReportCard['RemarkDropped'] = "* : Dropped";
$eReportCard['RemarkExempted'] = "免修";
$eReportCard['RemarkNotAssessed'] = "免修";

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['GenderEn'] = "Sex";
$eReportCard['Template']['StudentInfo']['GenderCh'] = "性別";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班別";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "Class No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "班號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Registration No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "派發日期";
$eReportCard['Template']['StudentInfo']['GenderDisplay']['M'] = "男 (M)";
$eReportCard['Template']['StudentInfo']['GenderDisplay']['F'] = "女 (F)";


# Marks Table
$eReportCard['Template']['SubjectOverall'] = "成績<br>Results";
$eReportCard['Template']['WholeYear'] = "全年<br>Annual";
$eReportCard['Template']['GrandTotal'] = "總分";
$eReportCard['Template']['FullMark'] = "滿分<br>Full Mark";
$eReportCard['Template']['SubjectRank'] = "名次<br>Rank";
$eReportCard['Template']['Mark'] = "成績<br>Results";

# Extra Masks Table 
$eReportCard['ExtraTemplate']['MarkCh'] = "分　數";
$eReportCard['ExtraTemplate']['FullMarkCh'] = "滿 　分";
$eReportCard['ExtraTemplate']['Subject'] = "科　目 (Subject)";
$eReportCard['ExtraTemplate']['GrandTotalCh'] = "總分";
$eReportCard['ExtraTemplate']['GrandAverageCh'] = "總平均分";
$eReportCard['ExtraTemplate']['SignatureCh'] = "家長簽署";


$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";



$eReportCard['Template']['OverallResultEn'] = "Grand Total";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['LearningAttitude'] = "學習態度";
$eReportCard['Template']['LearningAttitudeEn'] = "Learning Attitude";
$eReportCard['Template']['LearningAttitudeCh'] = "學習態度";
$eReportCard['Template']['CommentEn'] = "Comment";
$eReportCard['Template']['CommentCh'] = "評語";
$eReportCard['Template']['Comment'] = "評語";
$eReportCard['Template']['OtherInfoEn'] = "Other Information";
$eReportCard['Template']['OtherInfoCh'] = "其他資料";
$eReportCard['Template']['OtherInfo'] = "其他資料";
$eReportCard['Template']['MeritsDemerits'] = "功過 Merit/Demerit";
$eReportCard['Template']['MeritsDemeritsEn'] = "Merit/Demerit";
$eReportCard['Template']['MeritsDemeritsCh'] = "功過";
$eReportCard['Template']['Merits'] = "優點 Merit";
$eReportCard['Template']['Demerits'] = "缺點 Demerit";
$eReportCard['Template']['MinorCredit'] = "小功 Minor Credit";
$eReportCard['Template']['MajorCredit'] = "大功 Major Credit";
$eReportCard['Template']['MinorFault'] = "小過 Minor Fault";
$eReportCard['Template']['MajorFault'] = "大過 Major Fault";
$eReportCard['Template']['WReportArr']['Merits'] = "優點&nbsp;%d&nbsp;個&nbsp;&nbsp;&nbsp;&nbsp;%d&nbsp;Merit(s)";
$eReportCard['Template']['WReportArr']['Demerits'] = "缺點&nbsp;%d&nbsp;個&nbsp;&nbsp;&nbsp;&nbsp;%d&nbsp;Demerit(s)";
$eReportCard['Template']['WReportArr']['MinorCredit'] = "小功&nbsp;%d&nbsp;個&nbsp;&nbsp;&nbsp;&nbsp;%d&nbsp;Minor Credit(s)";
$eReportCard['Template']['WReportArr']['MajorCredit'] = "大功&nbsp;%d&nbsp;個&nbsp;&nbsp;&nbsp;&nbsp;%d&nbsp;Major Credit(s)";
$eReportCard['Template']['WReportArr']['MinorFault'] = "小過&nbsp;%d&nbsp;個&nbsp;&nbsp;&nbsp;&nbsp;%d&nbsp;Minor Fault(s)";
$eReportCard['Template']['WReportArr']['MajorFault'] = "大過&nbsp;%d&nbsp;個&nbsp;&nbsp;&nbsp;&nbsp;%d&nbsp;Major Fault(s)";$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['eca'] = "課外活動/服務 ECA/Services and Duty";
$eReportCard['Template']['WReportArr']['eca'] = "課外活動／服務／獎項&nbsp;&nbsp;&nbsp;&nbsp;ECA／Services and Duties／Awards";

$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

$eReportCard['Template']['Times'] = "Time(s)";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "Class Teacher<br>班主任";
$eReportCard['Template']['ClassTeacherEn'] = "Class Teacher";
$eReportCard['Template']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['Principal'] = "Principal<br>校長";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['ParentGuardian'] = "Parent/Guardian<br>家長/監護人";
$eReportCard['Template']['ParentGuardianEn'] = "Parent/Guardian";
$eReportCard['Template']['ParentGuardianCh'] = "家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";
$eReportCard['Template']['SchoolChopEn'] = "School Chop";
$eReportCard['Template']['SchoolChopCh'] = "校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


?>