<?php
# Editing by 

$eReportCard['Template']['Nil'] = "Nil";

### Cover Page
$eReportCard['Template']['WholeYearEn'] = "Whole Year";
$eReportCard['Template']['PageNumFooter'] = "Page <!--CurPageNum--> of <!--TotalNumOfPage-->";
$eReportCard['Template']['CoverPageRemarksArr'][0] = "Lutheran Academy (L.A.) is a learner-oriented institution and is committed to providing quality education in a bright and international environment.";
$eReportCard['Template']['CoverPageRemarksArr'][1] = "L.A. seeks to maximize the all-around potentials of our next generations. Through a specially designed \"Glocalized\" curriculum that combines the essence and advantages of international (with IB as a reference) and local curricula, we aim to produce 21<sup>st</sup> century leaders with strong Christian faith, global mindedness and the desire to continuously search for excellence.";


### Student Info Page
$eReportCard['Template']['StudentRecordEn'] = "Student Record";
$eReportCard['Template']['NameEn'] = "Name";
$eReportCard['Template']['ClassEn'] = "Class";
$eReportCard['Template']['StudentNoEn'] = "Student No";
$eReportCard['Template']['I.D./PassportNoEn'] = "I.D. / Passport No";
$eReportCard['Template']['HouseEn'] = "House";
$eReportCard['Template']['DateOfBirthEn'] = "Date of Birth";
$eReportCard['Template']['SexEn'] = "Sex";
$eReportCard['Template']['AttendanceEn'] = "Attendance (days)";
$eReportCard['Template']['CocurricularActivitiesEn'] = "Co-curricular Activities";
$eReportCard['Template']['AwardsEn'] = "Awards";
$eReportCard['Template']['DisciplinaryRemindersEn'] = "Disciplinary Reminder(s)";
$eReportCard['Template']['ItemsEn'] = "Items";
$eReportCard['Template']['NoOfRemindersEn'] = "No. of reminders";
$eReportCard['Template']['RecordCalculationRemarks'] = "All the above records are calculated from <!--TermStartDate--> to <!--TermEndDate-->.";


### Signature Table
$eReportCard['Template']['HomeroomTeacherEn'] = "Homeroom Teacher";
$eReportCard['Template']['ParentGuardianEn'] = "Parent / Guardian";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['PrincipalNameEn'] = "Mr. Andy FUNG";


### Learner Profile
$eReportCard['Template']['LearnerProfileEn'] = "LEARNER PROFILE";
$eReportCard['Template']['ItIsOurGoalToDevelopChilderWhoAreEn'] = "It is our goal to develop childer who are";
$eReportCard['Template']['CommentOnPersonalAndSocialDevelopmentEn'] = "Comment on Personal and Social Development";
$eReportCard['Template']['InquiriesEn'] = "Inquiries";
$eReportCard['Template']['KnowledgeableEn'] = "Knowledgeable";
$eReportCard['Template']['ThinkersEn'] = "Thinkers";
$eReportCard['Template']['CommunicatorEn'] = "Communicator";
$eReportCard['Template']['PrincipledEn'] = "Principled";
$eReportCard['Template']['OpenMindedEn'] = "Open-minded";
$eReportCard['Template']['CaringEn'] = "Caring";
$eReportCard['Template']['RiskTakersEn'] = "Risk-takers";
$eReportCard['Template']['WellBalancedEn'] = "Well-Balanced";
$eReportCard['Template']['ReflectiveEn'] = "Reflective";


### Grading Scheme
$eReportCard['Template']['LevelEn'] = "Level";
$eReportCard['Template']['DescriptorsEn'] = "Descriptors";


### Theme For Investigation (TFI)
$eReportCard['Template']['ThemeForInvestigationEn'] = "Theme For Investigation (TFI)";
$eReportCard['Template']['TeachingBlockDisplayEn'] = "<!--Sequence--> teaching block";
$eReportCard['Template']['TransdisciplinaryThemeEn'] = "Transdisciplinary theme";
$eReportCard['Template']['UnitTitleEn'] = "Unit title";
$eReportCard['Template']['OverviewOfStudentPerformanceEn'] = "Overview of student's performance";
$eReportCard['Template']['EffortIndicatorEn'] = "Effort Indicator";


### Subject Page
$eReportCard['Template']['SubjectEn'] = "Subject";
$eReportCard['Template']['StrandsEn'] = "Strands";
$eReportCard['Template']['CriteriaEn'] = "Criteria";
$eReportCard['Template']['ProgressLevelEn'] = "Progress Level";
$eReportCard['Template']['CommentEn'] = "Comment";
$eReportCard['Template']['EffortIndicatorEn'] = "Effort Indicator";
$eReportCard['Template']['CriterionEn'] = "Criterion";
$eReportCard['Template']['DescriptorEn'] = "Descriptor";
$eReportCard['Template']['HighestPossibleLevelOfAchievementEn'] = "Highest Possible Level of Achievement";
$eReportCard['Template']['LevelAchievedByStudentEn'] = "Level Achieved by Student";

$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['StrandsCh'] = "學習範疇";
$eReportCard['Template']['CriteriaCh'] = "評估準則";
$eReportCard['Template']['ProgressLevelCh'] = "進度";
$eReportCard['Template']['CommentCh'] = "評語";
$eReportCard['Template']['EffortIndicatorCh'] = "勤奮指數";
$eReportCard['Template']['CriterionCh'] = "標準";
$eReportCard['Template']['DescriptorCh'] = "水平細則";
$eReportCard['Template']['HighestPossibleLevelOfAchievementCh'] = "最高成績水平";
$eReportCard['Template']['LevelAchievedByStudentCh'] = "成績水平";


### Personal Characteristics
$eReportCard['Template']['LearningAttitudes&ApproachesChecklistEn'] = "Learning Attitudes & Approaches Checklist (by subject)";
$eReportCard['Template']['OtherCommentsEn'] = "Other Comments";

$eReportCard['Excellent_en'] = "Excellent";
$eReportCard['VeryGood_en'] = "Very Good";
$eReportCard['Good_en'] = "Good";
$eReportCard['Satisfactory_en'] = "Satisfactory";
$eReportCard['Developing_en'] = "Developing";
$eReportCard['Unsatisfactory_en'] = "Unsatisfactory";
$eReportCard['Nil_en'] = "Nil";
$eReportCard['NA_en'] = "N/A";

$eReportCard['Excellent_b5'] = "Excellent";
$eReportCard['VeryGood_b5'] = "Very Good";
$eReportCard['Good_b5'] = "Good";
$eReportCard['Satisfactory_b5'] = "Satisfactory";
$eReportCard['Developing_b5'] = "Developing";
$eReportCard['Unsatisfactory_b5'] = "Unsatisfactory";
$eReportCard['Nil_en'] = "Nil";
$eReportCard['NA_b5'] = "N/A";
?>