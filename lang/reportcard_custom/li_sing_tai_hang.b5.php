<?php
# Editing by 

### General language file ###
$eReportCard['SchemesFullMark'] = "總分";
$eReportCard['Template']['SchemesFullMarkEn'] = "Full Marks";
$eReportCard['Template']['SchemesFullMarkCh'] = "總分";

$eReportCard['Template']['StudentInfo']['Name'] = "Name 姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "Class 班別";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "Class No. 學號";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN No. 學生編號";
$eReportCard['Template']['StudentInfo']['Year'] = "Year 學年";
$eReportCard['Template']['StudentInfo']['Term'] = "Term 學期";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Marks";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";
$eReportCard['Template']['DaysAbsentEn'] = "Absence (Day)";
$eReportCard['Template']['DaysAbsentCh'] = "缺席 (日)";
$eReportCard['Template']['TimesLateEn'] = "Lateness (Day)";
$eReportCard['Template']['TimesLateCh'] = "遲到 (日)";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave (Day)";
$eReportCard['Template']['EarlyLeaveCh'] = "早退 (日)";
$eReportCard['Template']['PromotionEn'] = "Promoted/Retained";
$eReportCard['Template']['PromotionCh'] = "升級 / 留級";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Remark'] = "Remarks 備註"; 

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";
$eReportCard['Template']['ClassTeacher'] = "Class Teacher<br>班主任";
$eReportCard['Template']['Principal'] = "Principal<br>校長";
$eReportCard['Template']['ParentGuardian'] = "Parent / Guardian<br>家長 / 監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['ClassTeacherComment'] = "Comments  評語";
$eReportCard['Template']['MeritDemerit'] = "Merit / Demerit  優點 / 缺點";
$eReportCard['Template']['Activity&Service'] = "Activity & Service  活動及服務";

$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkAbsent'] = "ABS";
$eReportCard['RemarkAbsentZeorMark'] = "ABS";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkDropped'] = "ABS";
$eReportCard['RemarkExempted'] = "ABS";
$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";

$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

# Summary Info
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";


$eReportCard['ExtraInfoLabel'] = "操行分";

$eReportCard['Template']['GradeRemarks'] = "Grade 等級";
$eReportCard['Template']['PassingMarkRemarks'] = "Passing Mark 及格分數: 60";

$eReportCard['Template']['Address'] = "Address 地址 : 73 Wun Sha Street, Tai Hang, Causeway Bay, Hone Kong. 香港銅鑼灣大坑浣紗街73號";
$eReportCard['Template']['Telephone'] = "Telephone 電話 : 2577-5188";
$eReportCard['Template']['Fax'] = "Fax 傳真 : 2882-4510";

$eReportCard['FinalGrade'] = "最終等級";

?>