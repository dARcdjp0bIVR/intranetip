<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

# School Info
$eReportCard['Template']['SchoolNameEn'] = "BUDDHIST HUI YUAN COLLEGE";
$eReportCard['Template']['SchoolNameCh'] = "佛&nbsp;&nbsp;&nbsp;教&nbsp;&nbsp;&nbsp;慧&nbsp;&nbsp;&nbsp;遠&nbsp;&nbsp;&nbsp;中&nbsp;&nbsp;&nbsp;學";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名 (Name)";
$eReportCard['Template']['StudentInfo']['Class'] = "班級 (Class)";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "學號 (Class No.)";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號 (STRN)";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['SchemesFullMarkEn'] = "Full Mark";
$eReportCard['Template']['SchemesFullMarkCh'] = "滿分";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

# CSV Info
$eReportCard['Template']['NumSchoolDayEn'] = "No. of School Days";
$eReportCard['Template']['NumSchoolDayCh'] = "上課日數";
$eReportCard['Template']['DaysAbsentEn'] = "Absent (days)";
$eReportCard['Template']['DaysAbsentCh'] = "缺課次數";
$eReportCard['Template']['TimesLateEn'] = "Late (times)";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['EarlyLeaveEn'] = "Early Leave (times)";
$eReportCard['Template']['EarlyLeaveCh'] = "早退次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";
$eReportCard['Template']['AttendanceAndConduct'] = "出席及操行 Attendance and Conduct";
$eReportCard['Template']['OLEEn'] = "Other Learning Experiences";
$eReportCard['Template']['OLECh'] = "其他學習經歷";
$eReportCard['Template']['AwardEn'] = "Awards";
$eReportCard['Template']['AwardCh'] = "獎勵";
$eReportCard['Template']['RemarkEn'] = "Remarks";
$eReportCard['Template']['RemarkCh'] = "評語/備註";
$eReportCard['Template']['PunishmentEn'] = "Punishments";
$eReportCard['Template']['PunishmentCh'] = "懲罰";
$eReportCard['Template']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['DateOfIssueCh'] = "簽發日期";
$eReportCard['Template']['PromotedToEn'] = "Promoted to";
$eReportCard['Template']['PromotedToCh'] = "升級";
$eReportCard['Template']['PromotedOnTrialEn'] = "Promoted on trial";
$eReportCard['Template']['PromotedOnTrialCh'] = "試升";
$eReportCard['Template']['ToRepeatEn'] = "To repeat";
$eReportCard['Template']['ToRepeatCh'] = "留級";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "班&nbsp;&nbsp;主&nbsp;&nbsp;任<br>Class Teacher";
$eReportCard['Template']['Principal'] = "校&nbsp;&nbsp;長<br>Principal";
$eReportCard['Template']['ParentGuardian'] = "家&nbsp;&nbsp;長 / 監&nbsp;&nbsp;護&nbsp;&nbsp;人<br>Parent / Guardian";
$eReportCard['Template']['PrefectOfStudies'] = "教&nbsp;&nbsp;務&nbsp;&nbsp;主&nbsp;&nbsp;任<br>Prefect of Studies";
$eReportCard['Template']['IssueDate'] = "派&nbsp;&nbsp;發&nbsp;&nbsp;日&nbsp;&nbsp;期<br>Date of Issue";
$eReportCard['Template']['SchoolChop'] = "校&nbsp;&nbsp;印<br>School Chop";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

$eReportCard['Template']['AcademicPerformance'] = "學業成績 Academic Performance";
$eReportCard['Template']['SubjectsEn'] = "Subjects";
$eReportCard['Template']['SubjectsCh'] = "科目";

$eReportCard['Template']['1stTerm'] = "上學期 1st Term";
$eReportCard['Template']['2ndTerm'] = "下學期 2nd Term";

?>