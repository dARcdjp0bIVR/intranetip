<?php
# Editing by 

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['SchoolName'] = "HKUGA COLLEGE";
//$eReportCard['Template']['PrincipalName'] = "Dennis K.W. Chan";
//$eReportCard['Template']['PrincipalName'] = "Tin Yau IP  (Mr.)";
$eReportCard['Template']['PrincipalName'] = "Ms. CHEN Hing Corina";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "Class Number";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['StudentNoEn'] = "Student Number";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";
$eReportCard['Template']['IssueDateEn'] = "Date Issued";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
//$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "N.A.";
//2018-0703-1155-26164
$eReportCard['RemarkAbsentZeorMark'] = "Absent";
$eReportCard['RemarkAbsentZeorGradeDisplay'] = "1";
$eReportCard['RemarkAbsentNotConsidered'] = "N.A.";
// [2020-0506-1150-50164]
$eReportCard['MarkRemindSet1'] = "備註: \"+\" 代表缺席已獲批準或已呈交相關醫生證明 (成績表中顯示 \"absent\"); \"0\" 代表缺席未獲批準或未呈交相關醫生證明 (成績表中顯示 \"1\"); \"-\" 於考試前退修 (成績表中顯示 \"N.A.\")。";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Number Of Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Number Of Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


# Achievement Attitude Remarks
$eReportCard['Template']['AchievementEn'] = 'Academic Achievement';
$eReportCard['Template']['AchievementCh'] = '學業成績';
$eReportCard['Template']['AchievementShortEn'] = 'Achievement';
$eReportCard['Template']['AchievementShortCh'] = '成績';
$eReportCard['Template']['ElectiveEn'] = 'Elective Subject';
$eReportCard['Template']['ElectiveCh'] = '選修科 ';
$eReportCard['Template']['AttitudeEn'] = 'Attributes and Learning Attitude';
$eReportCard['Template']['AttitudeCh'] = '個人特質及學習態度';
$eReportCard['Template']['GradeCriteriaEn'] = 'Grade Criteria';
$eReportCard['Template']['LevelDescriptionEn'] = 'Level Descriptors';
$eReportCard['Template']['NonAcademicAchievements'] = 'Non-academic Achievements:';
$eReportCard['Template']['CriteriaEn'] = 'Criteria';
$eReportCard['Template']['ExcellentEn'] = 'Excellent';
$eReportCard['Template']['UnsatisfactoryEn'] = 'Unsatisfactory';
$eReportCard['Template']['AchievementRemark']['Intro'] = 'This report provides detailed information on this student’s attitude and performance in individual subjects. It is essential that all grades are viewed as INDICATORS OF PROGRESS and as GRADES OF ACHIEVEMENT. They are MEASURES OF SUCCESS, not measures of failure. ';
// $eReportCard['Template']['AchievementGradeRemark']['A*'] = 'The student demonstrates a consistent and thorough understanding of the required knowledge and skills and has the ability to apply them in a wide variety of situations. There is consistent evidence of analysis, synthesis and evaluation, where appropriate. The student generally demonstrates originality and insight.';
// $eReportCard['Template']['AchievementGradeRemark']['A'] = 'The student demonstrates a consistent and thorough knowledge of the required knowledge and skills and has the ability to apply them in a variety of situations. The student generally shows evidence of analysis, synthesis and evaluation where appropriate and demonstrates originality and insight.';
// $eReportCard['Template']['AchievementGradeRemark']['B'] = 'The student had a good understanding of the required knowledge and skills and is able to apply them in a normal classroom or homework environment. There is more evidence of skills of analysis, synthesis and evaluation. There is adequate detail in all aspects of work and an occasional originality and insight demonstrated.';
// $eReportCard['Template']['AchievementGradeRemark']['C'] = 'Although the student has difficulties in some areas, there has been measurable achievement and the student is making effort to overcome the difficulties experienced. The student has some understanding of the required knowledge and skills and is able to apply them with support. There is occasional evidence of skills of analysis, synthesis and evaluation.';
// $eReportCard['Template']['AchievementGradeRemark']['D'] = 'The student has achieved some limited objectives. Although the student has experienced difficulty in understanding the required knowledge and skills necessary to access subject matter, s/he can do so, to a certain extent, with support.';
// $eReportCard['Template']['AchievementGradeRemark']['E'] = 'Certain minimal objectives have been achieved. Although the student has experienced much difficulty in understanding the required skills and knowledge and is unable to fully apply them at home and in the classroom without much support from the teacher, the student is making some attempt to overcome the difficulties experienced.';
$eReportCard['Template']['AchievementGradeRemark']['5'] = 'The student demonstrates strong knowledge and understanding of the subject and the ability to apply concepts and skills effectively in diverse and unfamiliar situations with insight, the ability to analyse, synthesise and evaluate information from a wide variety of sources, and the ability to communicate ideas and express views concisely and logically. Level 5 students with the best performance are annotated with the symbols ‘**’ and the next top group with the symbol ‘*’. ';
$eReportCard['Template']['AchievementGradeRemark']['4'] = 'The student demonstrates good knowledge and understanding of the subject and the ability to apply the concepts and skills effectively in unfamiliar situations, the ability to analyse, synthesise and interpret information from a variety of sources, and the ability to communicate ideas and express views logically.';
$eReportCard['Template']['AchievementGradeRemark']['3'] = 'The student demonstrates adequate knowledge and understanding of the subject and the ability to apply the concepts and skills appropriately in different familiar tasks, the ability to analyse and interpret information from a variety of sources, and the ability to communicate ideas and express views appropriately and sound. ';
$eReportCard['Template']['AchievementGradeRemark']['2'] = 'The student demonstrates basic knowledge and understanding of the subject and the ability to apply the concepts and skills in familiar situations, the ability to identify and interpret information from straightforward sources, and the ability to communicate simple ideas in a balanced way.';
$eReportCard['Template']['AchievementGradeRemark']['1'] = 'The student demonstrates elementary knowledge and understanding of the subject and the ability to apply the concepts and skills in simple familiar situations with support, the ability to identify and interpret information from simple sources with guidance, and the ability to communicate simple ideas generally.';

$eReportCard['Template']['AttitudeRemark'][6] = 'The student meets the pastoral and academic demands with the highest quality in terms of effort and enthusiasm all the time.';
$eReportCard['Template']['AttitudeRemark'][5] = 'The student meets the pastoral and academic demands with very high quality, most of the time. Although not quite the standard to merit excellence, the student has achieved well above expectation.';
$eReportCard['Template']['AttitudeRemark'][4] = 'The student has enthusiasm and a desire to meet the pastoral and academic demands of the school.';
$eReportCard['Template']['AttitudeRemark'][3] = 'The student meets the basic level of expectation, has some enthusiasm, contributes reasonably to group work and gets on well with others.';
$eReportCard['Template']['AttitudeRemark'][2] = 'The student shows inconsistent achievement of basic level of expectation. More effort is needed to become satisfactory.';
$eReportCard['Template']['AttitudeRemark'][1] = 'The student needs to be closely supervised to meet the very basic level of expectation, may have anti-social behaviour and whose conduct in class and around the school often causes trouble.';


$eReportCard['Template']['TutorCommentsEn'] = "Tutor's Comments";
$eReportCard['Template']['ClassTeacherCommentsEn'] = "Class Teachers' Comments";
$eReportCard['Template']['AttendanceEn'] = 'Attendance';
$eReportCard['Template']['PunctualityEn'] = 'Punctuality';
$eReportCard['Template']['ActivitiesEn'] = 'Extended Learning Activities (ELAs)';
$eReportCard['Template']['NumberOfDayAbsentEn'] = 'Number of Days Absent';
$eReportCard['Template']['NumberOfDayLateEn'] = 'Number of Days Late';
$eReportCard['Template']['CommunityServicesEn'] = 'Community Services';
$eReportCard['Template']['PositionsOfResponsibilityEn'] = 'Positions of Responsibility';
$eReportCard['Template']['ResponsibilityEn'] = 'Responsibilities';
$eReportCard['Template']['ServicesEn'] = 'Services';
$eReportCard['Template']['ResponsibilityServicesEn'] = 'Responsibilities /Services';
$eReportCard['Template']['AwardsEn'] = 'Awards';

$eReportCard['Template']['TutorNameEn'] = "Tutor's Name";
$eReportCard['Template']['NamesOfClassTeacher'] = "Class Teachers";
$eReportCard['Template']['PrincipalNameEn'] = "Principal";
$eReportCard['Template']['SignatureEn'] = "Signature";


### Subject Page
$eReportCard['Template']['TeacherNameEn'] = "Teacher";
$eReportCard['Template']['TeacherNameCh'] = "老師";
$eReportCard['Template']['CommentsEn'] = "Comments";
$eReportCard['Template']['SubjectTeacherCommentsEn'] = "Subject Teacher's Comments";
$eReportCard['Template']['SubjectTeacherCommentsCh'] = "科任老師評語 ";
$eReportCard['Template']['CourseDescriptionEn'] = "Course Description";
$eReportCard['Template']['CourseDescriptionCh'] = "課程簡介";
$eReportCard['Template']['TermGradeEn'] = "Term Grade";
$eReportCard['Template']['CAGradeEn'] = "Continuous Assessment";
$eReportCard['Template']['CAGradeCh'] = "持續性評估";
$eReportCard['Template']['ExaminationGradeEn'] = "Examination";
$eReportCard['Template']['ExaminationGradeCh'] = "考試";
$eReportCard['Template']['YearGradeEn'] = "Year Grade";
$eReportCard['Template']['YearGradeCh'] = "總成績";

### Personal Characteristics
$eReportCard['PersonalCharacteristics'] = "態度";
//$eReportCard['Template']['PersonalCharacteristicsRemarks'] = "Please refer to Principal for Attitude marked 1 and refer to Vice Principal for Attitude marked 2.";
$eReportCard['Template']['PersonalCharacteristicsRemarks'] = "Prior approval from your HoD is required for students getting ‘Rarely’ in Attitude.";

$eReportCard['6_en'] = '6';
$eReportCard['5_en'] = '5';
$eReportCard['4_en'] = '4';
$eReportCard['3_en'] = '3';
$eReportCard['2_en'] = '2';
$eReportCard['1_en'] = '1';

$eReportCard['6_b5'] = '6';
$eReportCard['5_b5'] = '5';
$eReportCard['4_b5'] = '4';
$eReportCard['3_b5'] = '3';
$eReportCard['2_b5'] = '2';
$eReportCard['1_b5'] = '1';

# Transcript
// $Lang['eReportCard']['ReportArr']['TranscriptArr']['ReportTitleEn'] = "TRANSCRIPT";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['ReportTitleEn'] = "RANSCRIPT";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['EnglishNameEn'] = "NAME IN ENGLISH";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['ChineseNameEn'] = "NAME IN CHINESE";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['HKIDEn'] = "HK IDENTITY NO.";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['GenderEn'] = "SEX";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfAdmissionEn'] = "DATE OF ADMISSION";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfBirthEn'] = "DATE OF BIRTH";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfGraduationEn'] = "DATE OF GRADUATION";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfLeaveEn'] = "DATE OF LEAVING";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfIssueEn'] = "DATE OF ISSUE";

$Lang['eReportCard']['ReportArr']['TranscriptArr']['SubjectsEn'] = "SUBJECTS";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['juniorFormsEn'] = "Junior Forms";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['seniorFormsEn'] = "Senior Forms";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['SecondaryEn'] = "Secondary";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['YearGradeEn'] = "Year Grade";

$Lang['eReportCard']['ReportArr']['TranscriptArr']['RemarksEn'] = "Remarks";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['SchoolChopEn'] = "School Chop";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['PrincipalEn'] = "Principal";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['EndOfTranscriptEn'] = "*** End of Transcript ***";


# Testimonial
// $Lang['eReportCard']['ReportArr']['TestimonialArr']['ReportTitleEn'] = "TESTIMONIAL";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['ReportTitleEn'] = "ESTIMONIAL";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['EnglishNameEn'] = "NAME IN ENGLISH";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['ChineseNameEn'] = "NAME IN CHINESE";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['UserLoginEn'] = "STUDENT NUMBER";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['HKIDEn'] = "HK IDENTITY NO.";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['GenderEn'] = "SEX";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfAdmissionEn'] = "DATE OF ADMISSION";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfBirthEn'] = "DATE OF BIRTH";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfGraduationEn'] = "DATE OF GRADUATION";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfLeaveEn'] = "DATE OF LEAVING";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfIssueEn'] = "DATE OF ISSUE";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['DateOfGraduation'] = "DATE OF GRADUATION";

$Lang['eReportCard']['ReportArr']['TestimonialArr']['SchoolChopEn'] = "School Chop";
$Lang['eReportCard']['ReportArr']['TestimonialArr']['PrincipalEn'] = "Principal";


# ReportCard Summary
//$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ReportTitleEn'] = "REPORT SUMMARY";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ReportTitle1En'] = "EPORT ";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ReportTitle2En'] = "UMMARY";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ClassEn'] = "CLASS";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ClassNumberEn'] = "CLASS NUMBER";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['EnglishNameEn'] = "NAME IN ENGLISH";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['ChineseNameEn'] = "NAME IN CHINESE";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['GenderEn'] = "SEX";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['RegNoEn'] = "REGNO";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['UserLoginEn'] = "STUDENT NUMBER";

$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['SubjectsEn'] = "SUBJECTS";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['juniorFormsEn'] = "Junior Forms";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['seniorFormsEn'] = "Senior Forms";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['SecondaryEn'] = "Secondary";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['Term1CAEn'] = "Term 1<br/>CA";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['Term2CAEn'] = "Term 2<br/>CA";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['Term2ExamEn'] = "Term 2<br/>Exam";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['YearGradeEn'] = "Year<br/>Grade";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['IncludeCurrentYearResult'] = "顯示本學年成績";
$Lang['eReportCard']['ReportArr']['ReportSummaryArr']['IncludeCurrentYearResultRemarks'] = "(只應用於 S1 - S2)";

?>