<?php
# Editing by 

### Cheung Chuk Shan College language file ###

$eReportCard['SchemesFullMark'] = "Full Mark";

$eReportCard['Template']['SchoolNameEn'] = "CHEUNG CHUK SHAN COLLEGE";
$eReportCard['Template']['SchoolNameCh'] = "張祝珊英文中學";
$eReportCard['Template']['SchoolAddressEn'] = "11 Cloud View Road, North Point, Hong Kong";
$eReportCard['Template']['SchoolAddressCh'] = "香港北角雲景道十一號";
$eReportCard['Template']['SchoolTelEn'] = "Tel: 25706665";
$eReportCard['Template']['SchoolTelCh'] = "電話：二五七零六六六五";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "<b>NAME</b> 姓名";
$eReportCard['Template']['StudentInfo']['Gender'] = "<b>SEX</b> 性別";
$eReportCard['Template']['StudentInfo']['Age'] = "<b>AGE</b> 年齡";
$eReportCard['Template']['StudentInfo']['RegNo'] = "<b>REG NO.</b> 註冊編號";
$eReportCard['Template']['StudentInfo']['Class'] = "<b>CLASS</b> 班別";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "<b>CLASS NO.</b> 班號";

# Marks Table
$eReportCard['Template']['SubjectEng'] = "SUBJECTS";
$eReportCard['Template']['SubjectChn'] = "科目";
$eReportCard['Template']['SchemesFullMarkEn'] = "MAX. MARKS";
$eReportCard['Template']['SchemesFullMarkCh'] = "滿分額";
$eReportCard['Template']['1stTermEn'] = "First Term";
$eReportCard['Template']['1stTermCh'] = "上學期";
$eReportCard['Template']['2ndTermEn'] = "Second Term";
$eReportCard['Template']['2ndTermCh'] = "下學期";
$eReportCard['Template']['AnnualEn'] = "Annual";
$eReportCard['Template']['AnnualCh'] = "全學年";
$eReportCard['Template']['AnnualTotalEn'] = "Total";
$eReportCard['Template']['AnnualTotalCh'] = "總分";
$eReportCard['Template']['TeachersCommentEn'] = "TEACHER REMARKS";
$eReportCard['Template']['TeachersCommentCh'] = "教師評語";

$eReportCard['Template']['SubjectOverall'] = "Total<br>總分";

$eReportCard['Template']['GrandTotal'] = "Grand Total";

$eReportCard['Template']['OverallResultEn'] = "TERM TOTAL";
$eReportCard['Template']['OverallResultCh'] = "本學期總分";
$eReportCard['Template']['AnnualTotalEn'] = "ANNUAL TOTAL";
$eReportCard['Template']['AnnualTotalCh'] = "全學年總分";
$eReportCard['Template']['AvgMarkEn'] = "GRAND AVERAGE(%)";
$eReportCard['Template']['AvgMarkCh'] = "總平均分(%)";
$eReportCard['Template']['ClassPositionEn'] = "<b>POSITION</b>";
$eReportCard['Template']['ClassPositionCh'] = "名次";
$eReportCard['Template']['FormPositionEn'] = "<b>POSITION</b>";
$eReportCard['Template']['FormPositionCh'] = "名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "<b>NO. OF STUDENTS IN CLASS</b>";
$eReportCard['Template']['ClassNumOfStudentCh'] = "全班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "<b>NO. OF STUDENTS IN FORM</b>";
$eReportCard['Template']['FormNumOfStudentCh'] = "全級人數";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "--";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "DAYS ABSENT";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['TimesLateEn'] = "TIMES LATE";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "CONDUCT";
$eReportCard['Template']['ConductCh'] = "操行"; 
$eReportCard['Template']['ResponsibilityEn'] = "RESPONSIBILITY";
$eReportCard['Template']['ResponsibilityCh'] = "責任感"; 
$eReportCard['Template']['ApplicationEn'] = "APPLICATION";
$eReportCard['Template']['ApplicationCh'] = "勤學"; 
$eReportCard['Template']['NeatnessEn'] = "NEATNESS";
$eReportCard['Template']['NeatnessCh'] = "整潔"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherCommentEn'] = "CLASS TEACHER REMARKS";
$eReportCard['Template']['ClassTeacherCommentCh'] = "班主任評語";
$eReportCard['Template']['ECA/ServicesAndAwardsEn'] = "ECA / Services and Awards";
$eReportCard['Template']['ECA/ServicesAndAwardsCh'] = "課外活動 / 服務及獎項";
$eReportCard['Template']['PromotionToEn'] = "PROMOTION TO";
$eReportCard['Template']['PromotionToCh'] = "升級";
$eReportCard['Template']['RetainedInEn'] = "RETAINED IN";
$eReportCard['Template']['RetainedInCh'] = "留級";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER 班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL 校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN 家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE 日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP 校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");


?>