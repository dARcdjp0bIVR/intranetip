<?php
# Editing by 

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['StudentInfo']['NameEn'] = "英文姓名";
$eReportCard['Template']['StudentInfo']['NameCh'] = "中文姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "班號";
$eReportCard['Template']['StudentInfo']['Gender'] = "姓別";
$eReportCard['Template']['StudentInfo']['STRN'] = "Student Reference No";
$eReportCard['Template']['StudentInfo']['DOB'] = "出生日期";
$eReportCard['Template']['StudentInfo']['DOI'] = "發出日期";

$eReportCard['Template']['AcademicResults'] = "學業成績";
$eReportCard['Template']['ConductAssessment'] = "操行評分";

$eReportCard['Template']['ClassTeacher'] = "班主任簽名";
$eReportCard['Template']['Principal'] = "校長簽名";
$eReportCard['Template']['ParentGuardian'] = "家長 / 監護人簽名";
$eReportCard['Template']['IssueDate'] = "派發日期";

$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkNotAssessed'] = "---";
$eReportCard['DisplayOverallResult'] = "顯示總分";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班排名";
$eReportCard['ShowFormPosition'] = "顯示級排名";

$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");
?>