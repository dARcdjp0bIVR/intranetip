<?php
# Editing by 

# Special Case
$eReportCard['RemarkAbsentZeorMark'] = "+";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkNotAssessed'] = "N.A.";

# Wordings
$eReportCard['SchemesFullMark'] = "Full Mark";
$eReportCard['ExtraInfoLabel'] = "Make-up Exams";
$eReportCard['Verification'] = "Parent & Student Verification and Preview";
$eReportCard['VerificationPeriod'] = "Parent & Student Verification and Preview Report Card Period";

# Header
$eReportCard['Template']['SchoolNameCh'] = "澳門培正中學";
$eReportCard['Template']['SchoolNameEn'] = "Escola Secundária Pui Ching, Macau";
$eReportCard['Template']['PSchoolNameCh'] = "澳門培正中學附屬小學";
$eReportCard['Template']['PSchoolNameEn'] = "Escola Secundária Pui Ching, Macau";
$eReportCard['Template']['ReportTypeCh'] = "學生成績報告表";
$eReportCard['Template']['ReportTypeEn'] = "Student Report";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "Class Number";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "班內號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Student Number";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "學生編號";
$eReportCard['Template']['StudentInfo']['SchoolYearEn'] = "School Year";
$eReportCard['Template']['StudentInfo']['SchoolYearCh'] = "年度";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "發出日期";

# Mark Table
$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['HalfCh'] = "全學年";
$eReportCard['Template']['Half1En'] = "1st Term";
$eReportCard['Template']['Half1Ch'] = "第一段";
$eReportCard['Template']['Half2En'] = "2nd Term";
$eReportCard['Template']['Half2Ch'] = "第二段";
$eReportCard['Template']['Half3En'] = "3rd Term";
$eReportCard['Template']['Half3Ch'] = "第三段";
$eReportCard['Template']['HalfGTCh'] = "畢業試";
$eReportCard['Template']['GeneralEn'] = "General";
$eReportCard['Template']['GeneralCh'] = "日常";
$eReportCard['Template']['ExamEn'] = "Exam";
$eReportCard['Template']['ExamCh'] = "考試";

$eReportCard['Template']['TotalEn'] = "Total";
$eReportCard['Template']['TotalCh'] = "學年成績";
$eReportCard['Template']['GradExamEn'] = "Graduation Exam";
$eReportCard['Template']['GradExamCh'] = "畢業考試<br>成績";
$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

# CSV Info
$eReportCard['Template']['AwardCol'] = "褒獎欄";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merit'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['PeriodsAbsentEn'] = "Periods Absent";
$eReportCard['Template']['PeriodsAbsentCh'] = "缺課節數";
$eReportCard['Template']['LightMeritEn'] = "Light Merit";
$eReportCard['Template']['LightMeritCh'] = "優點";
$eReportCard['Template']['MinorMeritEn'] = "Minor Merit";
$eReportCard['Template']['MinorMeritCh'] = "小功";
$eReportCard['Template']['MajorMeritEn'] = "Major Merit";
$eReportCard['Template']['MajorMeritCh'] = "大功";
$eReportCard['Template']['MisdemeanorEn'] = "Misdemeanor";
$eReportCard['Template']['MisdemeanorCh'] = "勸告";
$eReportCard['Template']['LightDemeritEn'] = "Light Demerit";
$eReportCard['Template']['LightDemeritCh'] = "缺點";
$eReportCard['Template']['MinorDemeritEn'] = "Minor Demerit";
$eReportCard['Template']['MinorDemeritCh'] = "小過";
$eReportCard['Template']['MajorDemeritEn'] = "Major Demerit";
$eReportCard['Template']['MajorDemeritCh'] = "大過";
$eReportCard['Template']['Half1Comment'] = "第一段評語";
$eReportCard['Template']['Half2Comment'] = "第二段評語";
$eReportCard['Template']['Half3Comment'] = "第三段評語";
$eReportCard['Template']['GoodAcademic'] = "勤學生";
$eReportCard['Template']['ExcellentAcademic'] = "優異生";
$eReportCard['Template']['ExcellentAcademic2'] = "學業優異生";
$eReportCard['Template']['ExcellentConduct'] = "品行優良生";
$eReportCard['Template']['ExcellentAcademicConduct'] = "品學優良生";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "班主任";
$eReportCard['Template']['TeachingHead'] = "教務主任";
$eReportCard['Template']['MoralHead'] = "德育主任";
$eReportCard['Template']['VicePrincipal'] = "副校長";
$eReportCard['Template']['VicePrincipal_AdminHead'] = "副校長兼總務主任";
$eReportCard['Template']['VicePrincipal_TeachingHead'] = "副校長兼教務主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['PrincipalCh'] = "校長";
$eReportCard['Template']['PrincipalEn'] = "Principal";
$eReportCard['Template']['SchoolCropCh'] = "校印";
$eReportCard['Template']['SchoolCropEn'] = "School<br>Chop";
$eReportCard['Template']['RemarksCh'] = "備註";
$eReportCard['Template']['RemarksEn'] = "Remarks";
$eReportCard['Template']['Remarks2Ch'] = "成績報告表加蓋校方印鑑方為有效。";
$eReportCard['Template']['Remarks2En'] = "Report cards without the school chop are invalid.";
$eReportCard['Template']['Remarks3Ch'] = "以下空白";
$eReportCard['Template']['Remarks3En'] = "This space is blank.";

$eReportCard['Template']['Promotion'] = "升級 Promotion";
$eReportCard['Template']['GraduationHighSch'] = "高中畢業 Graduation<br>(Senior Secondary Education)";
$eReportCard['Template']['GraduationSecondSch'] = "初中畢業 Graduation<br>(Junior Secondary Education)";
$eReportCard['Template']['GraduationPriSch'] = "小學畢業 Graduation<br>(Primary Education)";
$eReportCard['Template']['Retention'] = "留級 Grade Retention";
$eReportCard['Template']['SchImcomplete'] = "學年未完成<br>School Year Incomplete";

$eReportCard['Template']['TeachingHeadName'] = "楊珮欣";
$eReportCard['Template']['MoralHeadName'] = "梁永棠";
$eReportCard['Template']['VicePrincipalName'] = "陳敬濂";
$eReportCard['Template']['VicePrincipal_AdminHead_Name'] = "郭敬文";
$eReportCard['Template']['VicePrincipal_TeachingHead_Name'] = "";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

# Master Report - Promotion
$eReportCard['MasterReport']['PromotionArr'][1] = "Promoted";
$eReportCard['MasterReport']['PromotionArr'][2] = "Promoted with Subject Retention";
$eReportCard['MasterReport']['PromotionArr'][3] = "Graduate";
$eReportCard['MasterReport']['PromotionArr'][4] = "Retained";
$eReportCard['MasterReport']['PromotionArr'][5] = "Dropout";
$eReportCard['MasterReport']['PromotionArr'][6] = "Waiting Make-up Exam";

# Subject Stat Report
$eReportCard["SubjStatReport"]["Title"] = "各科成績統計表";
$eReportCard["SubjStatReport"]["TitleResult"] = "成績";
$eReportCard["SubjStatReport"]["WholeYear"] = "全學年";
$eReportCard["SubjStatReport"]["GradExamOpt"] = "Graduate Exam";
$eReportCard["SubjStatReport"]["Class"] = "班　別";
$eReportCard["SubjStatReport"]["ClassTeacher"] = "班主任";
$eReportCard["SubjStatReport"]["ClassNumber"] = "學<br>號";
$eReportCard["SubjStatReport"]["ChineseName"] = "學生姓名";
$eReportCard["SubjStatReport"]["GrandAverage"] = "平均分";
$eReportCard["SubjStatReport"]["OrderMeritClass"] = "全班<br>名次";
$eReportCard["SubjStatReport"]["NoOfFailSubject"] = "不合<br>格科<br>目數";
$eReportCard["SubjStatReport"]["FailSubjectUnit"] = "不合<br>格單<br>位數";
$eReportCard["SubjStatReport"]["FailingNumber"] = "不合格人數";
$eReportCard["SubjStatReport"]["FailingRate"] = "不合格%";
$eReportCard["SubjStatReport"]["ClassNumber"] = "學號";
$eReportCard["SubjStatReport"]["AwardType"] = "褒獎";
$eReportCard["SubjStatReport"]["PromotionType"] = "升留";
$eReportCard["SubjStatReport"]["PromotionType1"] = "升級";
$eReportCard["SubjStatReport"]["PromotionType1_S6"] = "畢業";
$eReportCard["SubjStatReport"]["PromotionType2"] = "留級";
$eReportCard["SubjStatReport"]["PromotionType3"] = "勸退";
$eReportCard["SubjStatReport"]["PromotionType4"] = "補考";
$eReportCard["SubjStatReport"]["PromotionType5"] = "夏令";
$eReportCard["SubjStatReport"]["GradExamInappropriate"] = "Level without Graduate Exam";

# Student Promotion Status
$eReportCard["PromotionStatus"][1] = "Promoted";
$eReportCard["PromotionStatus"][2] = "Promoted with Subject Retention";
$eReportCard["PromotionStatus"][3] = "Graduate";
$eReportCard["PromotionStatus"][4] = "Retained";
$eReportCard["PromotionStatus"][5] = "Dropout";
$eReportCard["PromotionStatus"][6] = "Waiting Make-up Exam";
$eReportCard["PromotionStatusType"]["MarkupExam"] = "補考";
$eReportCard["PromotionStatusType"]["BeforeMarkupExam"] = "before Make-up Exam";
$eReportCard["PromotionStatusType"]["AfterMarkupExam"] = "after Make-up Exam";
$eReportCard["PromotionStatusType"]["MarkupExamSubject"] = "補考科目";
$eReportCard["PromotionStatusType"]["MarkupExamPass"] = "補考合格";
$eReportCard["PromotionStatusType"]["MarkupExamFail"] = "補考不合格";
$eReportCard["PromotionStatusType"]["NoMarkupExamRequired"] = "沒有補考資料";
$eReportCard["PromotionStatusType"]["SubjectPass"] = "合格";
$eReportCard["PromotionStatusType"]["SubjectFail"] = "不合格";
$eReportCard["PromotionStatusType"]["SubjectAbsent"] = "缺考";
$eReportCard["PromotionStatusType"]["SubjectStatusAbsent"] = "Absent";
$eReportCard["PromotionStatusType"]["Status"] = "升級狀況";
$eReportCard["PromotionStatusType"]["PromotionStatus"] = "Promotion Status";
$eReportCard["PromotionStatusType"]["StatusType1"] = "升級";
$eReportCard["PromotionStatusType"]["StatusType2"] = "帶科升級";
$eReportCard["PromotionStatusType"]["StatusType3"] = "畢業";
$eReportCard["PromotionStatusType"]["StatusType4"] = "留級";
$eReportCard["PromotionStatusType"]["StatusType5"] = "勸退";
$eReportCard["PromotionStatusType"]["StatusType6"] = "等候補考";

?>