<?php
# Editing by 

$eReportCard['RemarkNotAssessed'] = "/";

#Standard Average Table
$eReportCard['Template']['StandardDeviation'] = "標準分";
$eReportCard['Template']['FormAverage'] = "級平均分";
$eReportCard['Template']['WeightedStandardScore'] = "加權平均標準分";

# School Info
//P108980
//$eReportCard['Template']['SchoolInfo']['Principal'] = "鄧少軒";
$eReportCard['Template']['SchoolInfo']['Principal'] = "何玉芬博士";

$eReportCard['Template']['SchoolInfo']['SchoolNameCh'] = "迦密愛禮信中學";
$eReportCard['Template']['SchoolInfo']['SchoolNameEn'] = "Carmel Alison Lam Foundation Secondary School";
$eReportCard['Template']['SchoolInfo']['SchoolAddrCh'] = "香港新界葵涌華景山路四號";
$eReportCard['Template']['SchoolInfo']['SchoolAddrEn'] = "4 Wah King Hill Road, Kwai Chung, N.T., Hong Kong";
$eReportCard['Template']['SchoolInfo']['SchoolTel'] = "27445117";
$eReportCard['Template']['SchoolInfo']['SchoolFax'] = "27854153";
$eReportCard['Template']['SchoolInfo']['SchoolEmail'] = "calfss.mail@calfss.edu.hk";

#Instruction
$eReportCard['Template']['Instruction']['Row'][] = "其他說明";
$eReportCard['Template']['Instruction']['Row'][] = "1.	各滿分為100分。中一至中三合格分數為50分；中四至中七合格分數為40分。";
$eReportCard['Template']['Instruction']['Row'][] = "2.	不合格科目之分數顯示於括弧內。";
$eReportCard['Template']['Instruction']['Row'][] = "3.	科目分數為最佳百份之十(以合格人數計算)，將以＊號表示。";
$eReportCard['Template']['Instruction']['Row'][] = "4.	等級標準";

$eReportCard['Template']['Instruction']['ExtraRow'][5] = "本校名次以「標準分」(standard score) 為計算基礎。在統計學上，「標準分」能有效反映同學間相對的表現。相對於簡單地以總平均分計算名次，「標準分」能有效地減少不同科目之間的差異對整體成績表現的影響。高年班學生因選修科目不同，以「標準分」作為計算名次的基礎，尤為有效。以下對照表能讓學生和家長更易明白標準分對相的百份比分段(例：標準分=1.04，則百份比分段=85，即同學的成績位列「首百份之15」)。";
$eReportCard['Template']['Instruction']['ExtraRow']['Reference'] = "參考資料：http://www.grahamtall.co.uk/percentiles_and_standard_scores.htm";

$eReportCard['Template']['Instruction']['Grade']['A'] = "優良";
$eReportCard['Template']['Instruction']['Grade']['B'] = "良好";
$eReportCard['Template']['Instruction']['Grade']['C'] = "普通";
$eReportCard['Template']['Instruction']['Grade']['D'] = "較差";
$eReportCard['Template']['Instruction']['Grade']['E'] = "劣";

$eReportCard['Template']['Instruction']['Pass'] = "合格";
$eReportCard['Template']['Instruction']['Fail'] = "不合格";

$eReportCard['Template']['Instruction']['PercentageDivisionCh'] = "百份比分段";
 
#SubTitle
$eReportCard['Template']['SubTitle']['StudentInfo'] = "學生資料";
$eReportCard['Template']['SubTitle']['MsTable'] = "成績表現";
$eReportCard['Template']['SubTitle']['OtherPerformance']  = "其他表現"; 
$eReportCard['Template']['SubTitle']['AwardPunishment']  = "獎懲記錄";
$eReportCard['Template']['SubTitle']['ECA']  = "課外活動";
$eReportCard['Template']['SubTitle']['Post']  = "職責";
$eReportCard['Template']['SubTitle']['Awrad']  = "獎項";
$eReportCard['Template']['SubTitle']['ClassTeacherComment']  = "班主任評語"; 
$eReportCard['Template']['SubTitle']['Reference']  = "<u>成績參考及說明</u>"; 
$eReportCard['Template']['SubTitle']['PromoteRetain']  = "升／留班";  

### General language file ###
$eReportCard['SchemesFullMark'] = "Full Mark";
$eReportCard['Template']['Reportcard'] = "學生成績表";
$eReportCard['Template']['WholeYear'] = "全年";
$eReportCard['Template']['SchoolInfo']['AddressCh'] = "地址"; 
$eReportCard['Template']['SchoolInfo']['AddressEn'] = "Address";
$eReportCard['Template']['SchoolInfo']['TelNoCh'] = "電話"; 
$eReportCard['Template']['SchoolInfo']['TelNoEn'] = "Tel";
$eReportCard['Template']['SchoolInfo']['FaxNoCh'] = "傳真"; 
$eReportCard['Template']['SchoolInfo']['FaxNoEn'] = "Fax";
$eReportCard['Template']['SchoolInfo']['EmailCh'] = "電郵"; 
$eReportCard['Template']['SchoolInfo']['EmailEn'] = "email";
$eReportCard['Template']['FirstTerm'] = "上學期";
$eReportCard['Template']['SecondTerm']= "下學期";
$eReportCard['Template']['Daily']= "平時";
$eReportCard['Template']['Exam']= "考試";
$eReportCard['Template']['MidtermTest']= "中期測驗";
$eReportCard['Template']['TestResult']= "測驗成績";
$eReportCard['Template']['Result']= "成績";

$eReportCard['Template']['SimpleReport']['PositionEn']= "總名次/人數";
$eReportCard['Template']['SimpleReport']['PositionCh']= "總名次/人數";


# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "學生姓名";
//$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";
$eReportCard['Template']['StudentInfo']['DateOfBirth'] = "出生日期";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "班別(班號)";
$eReportCard['Template']['StudentInfo']['DateOfBirth&Gender'] = "出生日期及性別";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "學生姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "總成績";
$eReportCard['Template']['GrandTotal'] = "總分";
$eReportCard['Template']['OverallPosition'] = "總名次";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "人數";
$eReportCard['Template']['Grade'] = "等級";
$eReportCard['Template']['SubjectTeachersCommentCh']= "評語 (備用)";
$eReportCard['Template']['CommentCh'] = "評語";



$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

# CSV Info
/*$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";*/
$eReportCard['Template']['ECA'] = "課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "班主任評語";

$eReportCard['Template']['Attendance'] = "考勤";
$eReportCard['Template']['Days Absent'] = "缺席日數";
$eReportCard['Template']['Days Present'] = "學校總上課日數";
$eReportCard['Template']['Days Skip School'] = "曠課日數";
$eReportCard['Template']['Time Late'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['Diligence'] = "勤學";
$eReportCard['Template']['Manner'] = "禮貌";
$eReportCard['Template']['Responsibility'] = "責任感";
$eReportCard['Template']['Service'] = "服務精神";
$eReportCard['Template']['ECA Performance'] = "課外活動表現";
$eReportCard['Template']['Reading Scheme'] = "閱讀獎勵計劃";
$eReportCard['Template']['Student Scheme'] = "學生獎勵計劃"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "優點";
$eReportCard['Template']['Demerits'] = "缺點";
$eReportCard['Template']['MinorCredit'] = "小功";
$eReportCard['Template']['MajorCredit'] = "大功";
$eReportCard['Template']['MinorFault'] = "小過";
$eReportCard['Template']['MajorFault'] = "大過";
$eReportCard['Template']['WholeYearTotal'] = "全年<br>總數";
$eReportCard['Template']['MeritType'] = "獎";
$eReportCard['Template']['DemeritType'] = "懲";
$eReportCard['Template']['Post'] = "職責";
$eReportCard['Template']['Award'] = "獎項";
$eReportCard['Template']['Remark'] = "班主任評語";
$eReportCard['Template']['ItemsDisplayEnd'] = "************<br>項目顯示至此<br>************";


# Signature Table
$eReportCard['Template']['ClassTeacher'] = "班主任簽署";
$eReportCard['Template']['Principal'] = "校長簽署";
$eReportCard['Template']['ParentGuardian'] = "家長簽署";
//$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['IssueDate'] = "學校印鑑及發出日期";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");


?>