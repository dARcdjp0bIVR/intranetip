<?php
# Editing by 

### Customization language file for Buddhist Fat Ho Memorial College###
#CSV
$eReportCard['SubjectWeight'] = "科目比重"; 

$eReportCard['SchemesFullMark'] = "滿分";

// 2015-0204-1547-05066 - change Student Info title - chinese english
//$eReportCard['Template']['StudentInfo']['Name'] = "Name 姓名";
//$eReportCard['Template']['StudentInfo']['Class'] = "Class 班別";
//$eReportCard['Template']['StudentInfo']['Gender'] = "Gender 性別";
//$eReportCard['Template']['StudentInfo']['STRN'] = "STRN 學生編號";
//$eReportCard['Template']['StudentInfo']['StudentID'] = "Student ID 學生編號";
//$eReportCard['Template']['StudentInfo']['AdmissionDate'] = "Admission Date 入學日期";
$eReportCard['Template']['StudentInfo']['Name'] = "姓名 Name";
$eReportCard['Template']['StudentInfo']['Class'] = "班別 Class";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別 Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號 STRN";
$eReportCard['Template']['StudentInfo']['StudentID'] = "學生編號 Student ID";
$eReportCard['Template']['StudentInfo']['AdmissionDate'] = "入學日期 Admission Date";

$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['AwardFromSchool'] = "校內獎項 Award(s) from the School";
$eReportCard['Template']['AwardName'] = "獎項名稱 Award(s)";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['Student'] = "STUDENT<br>學生";

$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Minor Credit'] = "Minor Credit 小功";
$eReportCard['Template']['Major Credit'] = "Major Credit 大功";
$eReportCard['Template']['Super Credit'] = "Super Credit 超功";
$eReportCard['Template']['Ultra Credit'] = "Ultra Credit 極功";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['Minor Fault'] = "Minor Fault 小過";
$eReportCard['Template']['Major Fault'] = "Major Fault 大過";
$eReportCard['Template']['Super Fault'] = "Super Fault 超過";
$eReportCard['Template']['Ultra Fault'] = "Ultra Fault 極過";
$eReportCard['Template']['Black Marks'] = "Warnings 警告";
$eReportCard['Template']['Remark'] = "Remarks 備註";  
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher's Comments 班主任評語";
$eReportCard['Template']['eca'] = "ECA 課外活動";

$eReportCard['RemarkExempted'] = "/";
//$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkDropped'] = "--";
$eReportCard['RemarkAbsentZeorMark'] = "ABS";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";
$eReportCard['DisplayOverallResult'] = "顯示總成績";
$eReportCard['DisplayGrandAvg'] = "顯示平均分";
$eReportCard['ShowClassPosition'] = "顯示班名次";
$eReportCard['ShowFormPosition'] = "顯示級名次";
// 2011-0627-0935-00071 Change "N.A." to "--"
//$eReportCard['RemarkNotAssessed'] = "N.A.";
$eReportCard['RemarkNotAssessed'] = "--";

$eReportCard['ExtraInfoLabel'] = "學習態度";

$eReportCard['Template']['DailyEn'] = "Daily";
$eReportCard['Template']['DailyCh'] = "平時分";
$eReportCard['Template']['ExaminationEn'] = "Examination";
$eReportCard['Template']['ExaminationCh'] = "考試分";
$eReportCard['Template']['ELearningEn'] = "e-learning";
$eReportCard['Template']['ELearningCh'] = "網上學習";
$eReportCard['Template']['TotalEn'] = "Total";
$eReportCard['Template']['TotalCh'] = "總成績";
$eReportCard['Template']['AnnualResultEn'] = "Annual";
$eReportCard['Template']['AnnualResultCh'] = "全年<br />總成績"; 
$eReportCard['Template']['LearningAttitudeEn'] = "Learning Attitude";
$eReportCard['Template']['LearningAttitudeCh'] = "學習態度";
//2013-0107-0951-11156
//$eReportCard['Template']['TimesNoSubmissionOfHomeworkEn'] = "H.W. not Submitted";
$eReportCard['Template']['TimesNoSubmissionOfHomeworkEn'] = "Missing Homework (No. of times)";
$eReportCard['Template']['TimesNoSubmissionOfHomeworkCh'] = "欠交功課次數";

$eReportCard['Template']['FirstTerm'] = "1st Term 上學期";
$eReportCard['Template']['SecondTerm'] = "2nd Term 下學期";
$eReportCard['Template']['Annual'] = "Annual";

$eReportCard['Template']['RemarkContent']['FullMarkPassingMark'] = "Full mark 滿分: 100, Passing Mark 合格分數: <!--PassingMark-->";
$eReportCard['Template']['RemarkContent']['PercentageCarried'] = "Percentage carried by each paper in language subjects  語文科各卷所佔比例:";
$eReportCard['Template']['RemarkContent']['EnglishLanguage'] = "English Language 英文科: ";
$eReportCard['Template']['RemarkContent']['ChineseLanguage'] = "Chinese Language 中文科: ";
$eReportCard['Template']['RemarkContent']['ChineseLanguageNCS'] = "Chinese Language (NCS) 中文科（非華語學生）: ";
$eReportCard['Template']['RemarkContent']['OnlineLearning'] = "Online Learning 英文網上學習: <br />PS=Pass 合格, NI=Needs Improvement 有待改善";
$eReportCard['Template']['RemarkContent']['OverallResultForm1&2En'] = "When calculating the Overall Result, Chinese Language, English Language and Mathematics carry <!--ChiEngMathMarks--> marks each, Liberal Studies and Integrated Science carry <!--ISMarks--> marks each and all other subjects carry <!--OthersMarks--> marks each.";
$eReportCard['Template']['RemarkContent']['OverallResultForm1&2Ch'] = "計算總分時，中英數三科各佔<!--ChiEngMathMarks-->分，通識及綜合科學科各佔<!--ISMarks-->分，其他科目則各佔<!--OthersMarks-->分。";
$eReportCard['Template']['RemarkContent']['OverallResultForm3En'] = "When calculating the Overall Result, Chinese Language, English Language and Mathematics carry <!--ChiEngMathMarks--> marks each, Liberal Studies carries <!--ISMarks--> marks and all other subjects carry <!--OthersMarks--> marks each.";
$eReportCard['Template']['RemarkContent']['OverallResultForm3Ch'] = "計算總分時，中英數三科各佔<!--ChiEngMathMarks-->分，通識科佔<!--ISMarks-->分，其他科目則各佔<!--OthersMarks-->分。";
$eReportCard['Template']['RemarkContent']['OverallResultForm4&5Ch'] = "計算總分時，中英數及通識科各佔<!--ChiEngMathMarks-->分，選修科(X1和X2)佔200分，其他科目則各佔<!--OthersMarks-->分。";
//$eReportCard['Template']['RemarkContent']['OverallResultEn'] = "When calculating the Overall Result, Chinese Language, English Language, Mathematics and Liberal Studies carry <!--ChiEngMathMarks--> marks each, elective subjects (X1 and X2) carry 200 marks each and all other subjects carry <!--OthersMarks--> marks each";
//$eReportCard['Template']['RemarkContent']['OverallResultCh'] = "計算總分時, 中英數及通識科各佔<!--ChiEngMathMarks-->分, 選修科(X1和X2)各佔200分, 其他科目則各佔<!--OthersMarks-->分";
$eReportCard['Template']['RemarkContent']['OverallResultEn'] = "When calculating the Overall Result, Chinese Language, English Language, Mathematics and Liberal Studies carry <!--ChiEngMathMarks--> marks each, elective subjects (X1 and X2), Mathematics Module 1 and Mathematics Module 2 carry 200 marks each and all other subjects carry <!--OthersMarks--> marks each.";
$eReportCard['Template']['RemarkContent']['OverallResultCh'] = "計算總分時，中英數及通識科各佔<!--ChiEngMathMarks-->分，選修科(X1和X2)、數學單元一和數學單元二各佔200分，其他科目則各佔<!--OthersMarks-->分。";
$eReportCard['Template']['RemarkContent']['OverallResultSame'] = "When calculating the Overall Result, each subject carries 100 marks 計算總分時, 每科各佔100分";
$eReportCard['Template']['RemarkContent']['AnnualResultEn'] = "Annual Result: 1st Term Total weighs 40%, 2nd Term Total weighs 60%";
$eReportCard['Template']['RemarkContent']['AnnualResultCh'] = "全年總成績: 上學期成績佔40%，下學期成績佔60%";
$eReportCard['Template']['RemarkContent']['ECAEn'] = "ECA 課外活動:  Refer to the Student Portfolio of Other Learning Experiences";
$eReportCard['Template']['RemarkContent']['ECACh'] = "請參考學生個人其他學習經歷檔案";

$eReportCard['Template']['ReportView']['Formal'] = "正式";
$eReportCard['Template']['ReportView']['Draft'] = "草稿";
?>