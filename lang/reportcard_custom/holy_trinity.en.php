<?php
# Editing by :	

### General language file ###  Modified by Josephine (2009/11/18)
$eReportCard['ProgressAward'] = "HTCPTA Best Academic Progress Award.";
$eReportCard['FirstIn'] = "First in ";
$eReportCard['Promoted'] = "Promotion";
$eReportCard['Retained'] = "Retention";
$eReportCard["PromotedonTrial"] = "Concession Pass";

$eReportCard['RemarkNotAssessed'] = "---";

$eReportCard['SchemesFullMark'] = "Full Mark";

$eReportCard['Template']['SchoolNameEn'] = "Holy Trinity College";
$eReportCard['Template']['SchoolNameCh'] = "上 智 書 院";
$eReportCard['Template']['ReportTitleCh'] = "學生成績表";

# Student Info Table
$eReportCard['Template']['StudentInfo']['AcademicYearEn'] = "School Year";
$eReportCard['Template']['StudentInfo']['AcademicYearCh'] = "學年";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "發出日期";
$eReportCard['Template']['ClassTeacherCommentRemark'] = "REMARKS 評語";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "Class No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "班號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Registration No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['CandidatesNoEn'] = "No. of Candidates";
$eReportCard['Template']['CandidatesNoCh'] = "考生人數";
$eReportCard['Template']['FullMarksEn'] = "Full Marks";
$eReportCard['Template']['FullMarksCh'] = "滿分額";

$eReportCard['Template']['TermOneEn'] = "1st Term";
$eReportCard['Template']['TermOneCh'] = "上學期";
$eReportCard['Template']['TermTwoEn'] = "2nd Term";
$eReportCard['Template']['TermTwoCh'] = "下學期";
$eReportCard['Template']['OverallResultEn'] = "Overall Results";
$eReportCard['Template']['OverallResultCh'] = "全年成績";

$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";


#$eReportCard['Template']['OverallResultEn'] = "Overall Results";
#$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

$eReportCard['RemarkExempted'] = "---";
$eReportCard['RemarkDropped'] = "---";
$eReportCard['RemarkAbsentNotConsidered'] = "---";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺席日數";
$eReportCard['Template']['NoOfSchoolDaysEn'] = "No. of School Days";
$eReportCard['Template']['NoOfSchoolDaysCh'] = "上課日數";
$eReportCard['Template']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行"; 
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";


# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");

?>