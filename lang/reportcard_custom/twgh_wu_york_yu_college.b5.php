<?php
# Editing by Bill

### General language file ###
//$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkExempted'] = "---";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "Abs";
$eReportCard['RemarkAbsentZeorMark'] = "Abs";
$eReportCard['RemarkNotAssessed'] = "N.A.";
$eReportCard['RemarkAbsent'] = "Abs";

# Report Types
$eReportCard['Template']['ReportView']['TermTestReport'] = "Term Test Report";
$eReportCard['Template']['ReportView']['TermReport'] = "Term Report";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['GenderEn'] = "Sex";
$eReportCard['Template']['StudentInfo']['GenderCh'] = "性別";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "Class No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['ClassnClassNoEn'] = "Class/Class Number";
$eReportCard['Template']['StudentInfo']['ClassnClassNoCh'] = "班別/班號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";
$eReportCard['Template']['StudentInfo']['AcademicYearEn'] = "School Year";
$eReportCard['Template']['StudentInfo']['AcademicYearCh'] = "學年";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "派發日期";

# Marks Table
$eReportCard['Template']['MSTable']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['MSTable']['OverallResultCh'] = "總分";
$eReportCard['Template']['MSTable']['FullMarksEn'] = "Full Mark";
$eReportCard['Template']['MSTable']['FullMarksCh'] = "滿分";
$eReportCard['Template']['MSTable']['ResultEn'] = "Results";
$eReportCard['Template']['MSTable']['ResultCh'] = "成績";
$eReportCard['Template']['MSTable']['OverallResultEn'] = "Results";
$eReportCard['Template']['MSTable']['OverallResultCh'] = "總成績";
$eReportCard['Template']['MSTable']['ClassRankEn'] = "Rank";
$eReportCard['Template']['MSTable']['ClassRankCh'] = "班名次";
$eReportCard['Template']['MSTable']['FormRankEn'] = "Rank";
$eReportCard['Template']['MSTable']['FormRankCh'] = "級名次";
$eReportCard['Template']['MSTable']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['MSTable']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['MSTable']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['MSTable']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['MSTable']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['MSTable']['FormPositionCh'] = "級名次";
$eReportCard['Template']['MSTable']['DaysAbsentEn'] = "Days Absent";
$eReportCard['Template']['MSTable']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['MSTable']['TimesLateEn'] = "Times Late";
$eReportCard['Template']['MSTable']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['MSTable']['EarlyLeaveEn'] = "Times Early Leave";
$eReportCard['Template']['MSTable']['EarlyLeaveCh'] = "早退次數";
$eReportCard['Template']['MSTable']['ConductEn'] = "Conduct";
$eReportCard['Template']['MSTable']['ConductCh'] = "操行"; 
$eReportCard['Template']['MSTable']['DemeritEn'] = "Demerits";
$eReportCard['Template']['MSTable']['DemeritCh'] = "缺點";
$eReportCard['Template']['MSTable']['MeritEn'] = "Merits";
$eReportCard['Template']['MSTable']['MeritCh'] = "優點";
$eReportCard['Template']['MSTable']['AttainIn1En'] = "Attained in 1st Term";
$eReportCard['Template']['MSTable']['AttainIn1Ch'] = "第一學期獲得";
$eReportCard['Template']['MSTable']['AttainIn2En'] = "Attained in 2nd Term";
$eReportCard['Template']['MSTable']['AttainIn2Ch'] = "第二學期獲得";
$eReportCard['Template']['MSTable']['AnnualEn'] = "Annual";
$eReportCard['Template']['MSTable']['AnnualCh'] = "全年";
$eReportCard['Template']['MSTable']['Term1En'] = "First Term";
$eReportCard['Template']['MSTable']['Term1Ch'] = "上學期";
$eReportCard['Template']['MSTable']['Term2En'] = "Second Term";
$eReportCard['Template']['MSTable']['Term2Ch'] = "下學期";
$eReportCard['Template']['MSTable']['TermTestEn'] = "Test";
$eReportCard['Template']['MSTable']['TermTestCh'] = "測驗";
$eReportCard['Template']['MSTable']['TestEn'] = "Exam";
$eReportCard['Template']['MSTable']['TestCh'] = "考試";
$eReportCard['Template']['MSTable']['ECAEn'] = "ECA/Services and Duty";
$eReportCard['Template']['MSTable']['ECACh'] = "課外活動/服務";
//$eReportCard['Template']['OverallResultEn'] = "Overall Result";
//$eReportCard['Template']['OverallResultCh'] = "總分";
//$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
//$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
//$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
//$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";
//$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
//$eReportCard['Template']['Conduct'] = "Conduct";
//$eReportCard['Template']['Merits'] = "Merits 優點";
//$eReportCard['Template']['Demerits'] = "Demerits 缺點";

# Merit Table
$eReportCard['Template']['MeritTable']['MPEn'] = "Merit for Punctuality";
$eReportCard['Template']['MeritTable']['MPCh'] = "守時優點";
$eReportCard['Template']['MeritTable']['MUEn'] = "Merit for Proper School Uniform";
$eReportCard['Template']['MeritTable']['MUCh'] = "儀容整潔優點";
$eReportCard['Template']['MeritTable']['MHEn'] = "Merit for Submitting Homework on Time";
$eReportCard['Template']['MeritTable']['MHCh'] = "交齊功課優點";
$eReportCard['Template']['MeritTable']['GCAEn'] = "Good Conduct Award";
$eReportCard['Template']['MeritTable']['GCACh'] = "操行獎";
$eReportCard['Template']['MeritTable']['MECAEn'] = "Merit on Extra Curricular Activities";
//$eReportCard['Template']['MeritTable']['MECACh'] = "課外活動優異獎";
$eReportCard['Template']['MeritTable']['MECACh'] = "課外活動優點";

# Signature Table
$eReportCard['Template']['SignatureTable']['SignatureEn'] = "Signature";
$eReportCard['Template']['SignatureTable']['SignatureCh'] = "簽　署";
$eReportCard['Template']['SignatureTable']['ParentGuardianEn'] = "Parent/Guardian";
$eReportCard['Template']['SignatureTable']['ParentGuardianCh'] = "家長/監護人";
$eReportCard['Template']['SignatureTable']['ClassTeacherEn'] = "Class Teacher";
$eReportCard['Template']['SignatureTable']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['SignatureTable']['PrincipalEn'] = "Principal";
$eReportCard['Template']['SignatureTable']['PrincipalCh'] = "校長";
//$eReportCard['Template']['SignatureTable']['PrincipalEn'] = "Acting Principal";
//$eReportCard['Template']['SignatureTable']['PrincipalCh'] = "署理校長";
$eReportCard['Template']['SignatureTable']['TeacherTitle'] = "老師";
$eReportCard['Template']['SignatureTable']['PrincipalName'] = "葉偉明博士";
$eReportCard['Template']['SignatureTable']['ClassTeacherRemakrsEn'] = "Remarks";
$eReportCard['Template']['SignatureTable']['ClassTeacherRemakrsCh'] = "班主任評語";

# Chinese Number
//$eReportCard['Template']['MSTable']['MeritNumChi'][1] = "One";
//$eReportCard['Template']['MSTable']['MeritNumChi'][2] = "Two";
//$eReportCard['Template']['MSTable']['MeritNumChi'][3] = "Three";
//$eReportCard['Template']['MSTable']['MeritNumChi'][4] = "Four";
//$eReportCard['Template']['MSTable']['MeritNumChi'][5] = "Five";
//$eReportCard['Template']['MSTable']['MeritNumChi'][6] = "Six";
//$eReportCard['Template']['MSTable']['MeritNumChi'][7] = "Seven";
//$eReportCard['Template']['MSTable']['MeritNumChi'][8] = "Eight";
//$eReportCard['Template']['MSTable']['MeritNumChi'][9] = "Nine";
//$eReportCard['Template']['MSTable']['MeritNumChi'][10] = "Ten";
//$eReportCard['Template']['MSTable']['MeritNumChi'][11] = "Eleven";
//$eReportCard['Template']['MSTable']['MeritNumChi'][12] = "Twelve";
//$eReportCard['Template']['MSTable']['MeritNumChi'][13] = "Thirteen";
//$eReportCard['Template']['MSTable']['MeritNumChi'][14] = "Fourteen";
//$eReportCard['Template']['MSTable']['MeritNumChi'][15] = "Fifteen";
//$eReportCard['Template']['MSTable']['MeritNumChi'][16] = "Sixteen";
//$eReportCard['Template']['MSTable']['MeritNumChi'][17] = "Seventeen";
//$eReportCard['Template']['MSTable']['MeritNumChi'][18] = "Eighteen";
//$eReportCard['Template']['MSTable']['MeritNumChi'][19] = "Nineteen";
//$eReportCard['Template']['MSTable']['MeritNumChi'][20] = "Twenty";
//$eReportCard['Template']['MSTable']['MeritNumChi'][30] = "Thirty";
//$eReportCard['Template']['MSTable']['MeritNumChi'][40] = "Fourty";
//$eReportCard['Template']['MSTable']['MeritNumChi'][50] = "Fifty";
//$eReportCard['Template']['MSTable']['MeritNumChi'][60] = "Sixty";
//$eReportCard['Template']['MSTable']['MeritNumChi'][70] = "Seventy";
//$eReportCard['Template']['MSTable']['MeritNumChi'][80] = "Eighty";
//$eReportCard['Template']['MSTable']['MeritNumChi'][90] = "Ninety";
?>