<?php
# Editing by 

# Customization
# Marksheet remarks
//2014-1014-1144-21164
//$eReportCard['MarkRemindSet1'] = "備註: \"-\" 代表缺席; \"/\" 代表免修。";
//$eReportCard['MarkRemindSet2'] = "備註: \"abs\" 代表缺席; \"/\" 代表免修。";
$eReportCard['MarkRemindSet1'] = "備註: \"+\" 代表不作評估; \"-\" 代表缺席; \"*\" 代表退修; \"/\" 代表免修。";
$eReportCard['MarkRemindSet2'] = "備註: \"abs\" 代表缺席; \"*\" 代表退修; \"/\" 代表免修。";

### General language file ###

$eReportCard['SchemesFullMark'] = "滿分";

$eReportCard['Template']['SchoolNameEn'] = "Sha Tin Methodist College";
//$eReportCard['Template']['SchoolNameCh'] = "沙田循道衛理中學";
$eReportCard['Template']['SchoolNameCh'] = "沙田循道衞理中學";


# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";
$eReportCard['Template']['StudentInfo']['RegNo'] = "Registration No.";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameEn'] = "Name";
$eReportCard['Template']['StudentInfo']['NameCh'] = "姓名";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "No.";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";

# Marks Table
$eReportCard['Template']['SesseionTitle'][1] = "Academic Performance";
$eReportCard['Template']['SesseionTitle'][2] = "Absence and Lateness";
$eReportCard['Template']['SesseionTitle'][3] = "Personal Qualities";
$eReportCard['Template']['SesseionTitle'][4] = "Merits and Demerits";
$eReportCard['Template']['SesseionTitle'][5] = "List of Awards and Major Achievements";
$eReportCard['Template']['SesseionTitle'][6] = "Other Learning Experiences";

$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['MaximumMarksEn'] = "Maximum Marks";
$eReportCard['Template']['MaximumMarksCh'] = "滿分額";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";

$eReportCard['Template']['WeightingEn'] = "Weighting";
$eReportCard['Template']['FirstTermEn'] = "First Term";
$eReportCard['Template']['FirstTermCh'] = "上學期";
$eReportCard['Template']['SecondTermEn'] = "Second Term";
$eReportCard['Template']['SecondTermCh'] = "下學期";
$eReportCard['Template']['GradeEn'] = "Grade";
$eReportCard['Template']['PositionEn'] = "Position";
$eReportCard['Template']['YearlyEn'] = "Yearly";


$eReportCard['Template']['OverallPositionEn'] = "Overall Position";
$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

//$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkExempted'] = "N.A.";
$eReportCard['RemarkDropped'] = "*";
//$eReportCard['RemarkAbsentNotConsidered'] = "*";
$eReportCard['RemarkAbsentNotConsidered'] = "N.A.";
$eReportCard['RemarkAbsentZeorMark'] = "Not Assessed";

# CSV Info
$eReportCard['Template']['DaysAbsentEn'] = "No. of days absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "No. of times late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "Merits & Demerits 獎懲";
$eReportCard['Template']['Merits'] = "Merits 優點";
$eReportCard['Template']['Demerits'] = "Demerits 缺點";
$eReportCard['Template']['MinorCredit'] = "Minor Credit 小優";
$eReportCard['Template']['MajorCredit'] = "Major Credit 大優";
$eReportCard['Template']['MinorFault'] = "Minor Fault 小過";
$eReportCard['Template']['MajorFault'] = "Major Fault 大過";
$eReportCard['Template']['Remark'] = "Remark 備註";  
$eReportCard['Template']['eca'] = "ECA 課外活動";
$eReportCard['Template']['ClassTeacherComment'] = "Class Teacher Comment 班主任評語";

$eReportCard['Template']['AbilityEn'] = "Ability";
$eReportCard['Template']['AttitudeEn'] = "Attitude";
$eReportCard['Template']['TrustworthinessEn'] = "Trustworthiness";
$eReportCard['Template']['SelfDisciplineEn'] = "Self-discipline";
$eReportCard['Template']['PolitenessEn'] = "Politeness";
$eReportCard['Template']['AnalyticalAndCriticalThinkingEn'] = "Analytical and Critical Thinking";
$eReportCard['Template']['InterpersonalSkillEn'] = "Interpersonal Skill";
$eReportCard['Template']['LeadershipEn'] = "Leadership";
$eReportCard['Template']['LearningAttitudeEn'] = "Learning Attitude";
$eReportCard['Template']['WillingnessToServeEn'] = "Willingness to Serve";
$eReportCard['Template']['InitiativeEn'] = "Initiative";

$eReportCard['Template']['NumberOf'] = "Number of";
$eReportCard['Template']['Merits&Demerits'] = "Merits & Demerits";
$eReportCard['Template']['Merit'] = "Merit";
$eReportCard['Template']['Contribution'] = "Contribution";
$eReportCard['Template']['MajorContribution'] = "Major Contribution";
$eReportCard['Template']['Demerit'] = "Demerit";
$eReportCard['Template']['Offense'] = "Offense";
$eReportCard['Template']['MajorOffense'] = "Major Offense";

$eReportCard['Template']['ProgrammesEn'] = "Programmes";
$eReportCard['Template']['RoleOrAwards'] = "Roles/Awards";
$eReportCard['Template']['Role'] = "Roles";
$eReportCard['Template']['Awards'] = "Awards";
$eReportCard['Template']['RemarksEn'] = "Remarks";

$eReportCard['Template']['CommentsEn'] = "Comments";


# Signature Table
$eReportCard['Template']['ClassTeacher'] = "Form Teacher's Signature";
$eReportCard['Template']['Principal'] = "Principal's Signature";
$eReportCard['Template']['ParentGuardian'] = "Parent's/Guardian's Signature";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "School Chop";

$eReportCard['Template']['PromotedTo'] = "Promoted to";
$eReportCard['Template']['PromotedToOnDiscretion'] = "Promoted to (on discretion)";
$eReportCard['Template']['RetainedIn'] = "Retained in";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

# personal characteristic
$eReportCard['Excellent_en'] = 'Excellent';
$eReportCard['Very Good_en'] = 'Very Good';
$eReportCard['Good_en'] = 'Good';
$eReportCard['Fair_en'] = 'Fair';
$eReportCard['Poor_en'] = 'Poor';

$eReportCard['Excellent_b5'] = 'Excellent';
$eReportCard['Very Good_b5'] = 'Very Good';
$eReportCard['Good_b5'] = 'Good';
$eReportCard['Fair_b5'] = 'Fair';
$eReportCard['Poor_b5'] = 'Poor';


$Lang['eReportCard']['templateChangeDescAry']['2012-11-15']['2012-1003-1209-17072'] = "更新成績表的等級備註。";
?>