<?php 
# Editing by 

### General language file ###
$eReportCard['RemarkExempted'] = "/";
$eReportCard['RemarkDropped'] = "*";
$eReportCard['RemarkNotAssessed'] = "N.A.";
$eReportCard['RemarkAbsentNotConsidered'] = "ABS";

$eReportCard['SchemesFullMark'] = "滿分";

# Report Header
$eReportCard['Template']['SchoolTitleCh'] = "香 港 培 道 中 學";
$eReportCard['Template']['SchoolTitleEn'] = "POOI TO MIDDLE SCHOOL";
$eReportCard['Template']['TestTitleCh'] = "統測成績單";
$eReportCard['Template']['TestTitleEn'] = "STANDARD TEST RESULTS";
$eReportCard['Template']['AcademicYearCh'] = "學年";
$eReportCard['Template']['AcademicYearEn'] = "Academic Year";
$eReportCard['Template']['Term1Ch'] = "上學期";
$eReportCard['Template']['Term1En'] = "FIRST TERM";
$eReportCard['Template']['Term2Ch'] = "下學期";
$eReportCard['Template']['Term2En'] = "SECOND TERM";
$eReportCard['Template']['FinalCh'] = "全學期";
$eReportCard['Template']['FinalEn'] = "SECOND TERM";

# Student Info Table
$eReportCard['Template']['StudentInfo']['Name'] = "姓名";
$eReportCard['Template']['StudentInfo']['Class'] = "班別";
$eReportCard['Template']['StudentInfo']['Gender'] = "性別";
$eReportCard['Template']['StudentInfo']['STRN'] = "學生編號";

# Student Info Table with lang
$eReportCard['Template']['StudentInfo']['NameCh'] = "學生姓名";
$eReportCard['Template']['StudentInfo']['NameEn'] = "Student Name";
$eReportCard['Template']['StudentInfo']['ClassCh'] = "班級";
$eReportCard['Template']['StudentInfo']['ClassEn'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassNoCh'] = "學號";
$eReportCard['Template']['StudentInfo']['ClassNoEn'] = "Class No.";
$eReportCard['Template']['StudentInfo']['ClassNo2Ch'] = "學號";
$eReportCard['Template']['StudentInfo']['ClassNo2En'] = "No.";
$eReportCard['Template']['StudentInfo']['STRNCh'] = "註冊編號";
$eReportCard['Template']['StudentInfo']['STRNEn'] = "Reg. No.";
$eReportCard['Template']['StudentInfo']['SchoolYearCh'] = "學年";
$eReportCard['Template']['StudentInfo']['SchoolYearEn'] = "School Year";
$eReportCard['Template']['StudentInfo']['TermCh'] = "學期";
$eReportCard['Template']['StudentInfo']['TermEn'] = "Term";
$eReportCard['Template']['StudentInfo']['DateOfIssueCh'] = "派發日期";
$eReportCard['Template']['StudentInfo']['DateOfIssueEn'] = "Date of Issue";

# Marks Table
$eReportCard['Template']['SubjectOverall'] = "Total Result<br>總成績";
$eReportCard['Template']['GrandTotal'] = "總分";

$eReportCard['Template']['SubjectEn'] = "Subjects";
$eReportCard['Template']['SubjectCh'] = "科目";
$eReportCard['Template']['FullMarksEn'] = "Full mark";
$eReportCard['Template']['FullMarksCh'] = "滿分";
$eReportCard['Template']['TotalMarksEn'] = "Total mark";
$eReportCard['Template']['TotalMarksCh'] = "總分";
$eReportCard['Template']['PassMarksEn'] = "Pass mark";
$eReportCard['Template']['PassMarksCh'] = "合格分";
$eReportCard['Template']['MarksAwardedEn'] = "Marks Awarded";
$eReportCard['Template']['MarksAwardedCh'] = "所得分數";

$eReportCard['Template']['AcademicResultEn'] = "Academic Results";
$eReportCard['Template']['AcademicResultCh'] = "學業成績";
$eReportCard['Template']['OverallResultEn'] = "Overall Result";
$eReportCard['Template']['OverallResultCh'] = "總分";
$eReportCard['Template']['AvgMarkEn'] = "Average Mark";
$eReportCard['Template']['AvgMarkCh'] = "平均分";
$eReportCard['Template']['WeightAvgEn'] = "Weighted Average";
$eReportCard['Template']['WeightAvgCh'] = "總平均";
$eReportCard['Template']['MarkEn'] = "Marks";
$eReportCard['Template']['MarkCh'] = "成績";
$eReportCard['Template']['TermMarkEn'] = "Term Marks";
$eReportCard['Template']['TermMarkCh'] = "日常成績";
$eReportCard['Template']['ExamMarkEn'] = "Exam Marks";
$eReportCard['Template']['ExamMarkCh'] = "期考成績";
$eReportCard['Template']['TermAvgEn'] = "Term Average";
$eReportCard['Template']['TermAvgCh'] = "學期成績";
$eReportCard['Template']['YearAvgEn'] = "Year Average";
$eReportCard['Template']['YearAvgCh'] = "學年成績";
$eReportCard['Template']['ClassPositionEn'] = "Position in Class";
$eReportCard['Template']['ClassPositionCh'] = "班名次";
$eReportCard['Template']['FormPositionEn'] = "Position in Form";
$eReportCard['Template']['FormPositionCh'] = "全級名次";
$eReportCard['Template']['ClassNumOfStudentEn'] = "No. of Students in Class";
$eReportCard['Template']['ClassNumOfStudentCh'] = "班人數";
$eReportCard['Template']['FormNumOfStudentEn'] = "No. of Students in Form";
$eReportCard['Template']['FormNumOfStudentCh'] = "級人數";

# MISC Table
$eReportCard['Template']['ESLEn'] = "English as a Study Language";
$eReportCard['Template']['ESLCh'] = "跨科英語學習能力";
$eReportCard['Template']['DistinctEn'] = "Distinction";
$eReportCard['Template']['DistinctCh'] = "優異";
$eReportCard['Template']['AverageEn'] = "Average";
$eReportCard['Template']['AverageCh'] = "及格";
$eReportCard['Template']['BelowAvgEn'] = "Below Average";
$eReportCard['Template']['BelowAvgCh'] = "不及格";
$eReportCard['Template']['Attendance'] = "考勤紀錄";
$eReportCard['Template']['AttendanceEn'] = "Attendance Records";
$eReportCard['Template']['AttendanceCh'] = "考勤紀錄";
$eReportCard['Template']['Absent'] = "缺席";
$eReportCard['Template']['AbsentEn'] = "Absent";
$eReportCard['Template']['AbsentCh'] = "缺席";
$eReportCard['Template']['Late'] = "遲到";
$eReportCard['Template']['LateEn'] = "Late";
$eReportCard['Template']['LateCh'] = "遲到";
$eReportCard['Template']['TermTotalEn'] = "Term Total";
$eReportCard['Template']['TermTotalCh'] = "學期總計";
$eReportCard['Template']['Days'] = "日數";
$eReportCard['Template']['DaysEn'] = "Days";
$eReportCard['Template']['DaysCh'] = "日數";
$eReportCard['Template']['Period'] = "節數";
$eReportCard['Template']['PeriodEn'] = "Period";
$eReportCard['Template']['PeriodCh'] = "節數";
$eReportCard['Template']['DaysAbsentEn'] = "Day(s) Absent";
$eReportCard['Template']['DaysAbsentCh'] = "缺課日數";
$eReportCard['Template']['TimesLateEn'] = "Time(s) Late";
$eReportCard['Template']['TimesLateCh'] = "遲到次數";
$eReportCard['Template']['ConductEn'] = "Conduct";
$eReportCard['Template']['ConductCh'] = "操行";
$eReportCard['Template']['Conduct'] = "操行";
$eReportCard['Template']['MeritsDemerits'] = "獎懲";
$eReportCard['Template']['MeritsDemeritsEn'] = "Merits and Demerits";
$eReportCard['Template']['MeritsDemeritsCh'] = "獎懲";
$eReportCard['Template']['Merits'] = "優點";
$eReportCard['Template']['MeritsEn'] = "Positive Mark";
$eReportCard['Template']['MeritsCh'] = "優點";
$eReportCard['Template']['Demerits'] = "缺點";
$eReportCard['Template']['DemeritsEn'] = "Negative Mark";
$eReportCard['Template']['DemeritsCh'] = "缺點";
$eReportCard['Template']['MinorMerit'] = "小功";
$eReportCard['Template']['MinorMeritEn'] = "Merit";
$eReportCard['Template']['MinorMeritCh'] = "小功";
$eReportCard['Template']['MajorMerit'] = "大功";
$eReportCard['Template']['MajorMeritEn'] = "Major Merit";
$eReportCard['Template']['MajorMeritCh'] = "大功";
$eReportCard['Template']['MinorDemerit'] = "小過";
$eReportCard['Template']['MinorDemeritEn'] = "Minor Demerit";
$eReportCard['Template']['MinorDemeritCh'] = "小過";
$eReportCard['Template']['MajorDemerit'] = "大過";
$eReportCard['Template']['MajorDemeritEn'] = "Major Demerit";
$eReportCard['Template']['MajorDemeritCh'] = "大過";
$eReportCard['Template']['AwardsEn'] = "Awards in Academic Performance and Conduct";
$eReportCard['Template']['AwardsCh'] = "學業及品行獎項";
$eReportCard['Template']['NotesEn'] = "Notes";
$eReportCard['Template']['NotesCh'] = "備註";
$eReportCard['Template']['ActivitynServiceEn'] = "Activities / Service";  
$eReportCard['Template']['ActivitynServiceCh'] = "課外活動 / 服務";
$eReportCard['Template']['ProgrammeEn'] = "Programmes";  
$eReportCard['Template']['ProgrammeCh'] = "活動名稱";
$eReportCard['Template']['PostEn'] = "Post";
$eReportCard['Template']['PostCh'] = "職位"; 
$eReportCard['Template']['PerformanceEn'] = "Performance";
$eReportCard['Template']['PerformanceCh'] = "表現";
$eReportCard['Template']['PQEn'] = "Personal Qualities";
$eReportCard['Template']['PQCh'] = "個人素質"; 
$eReportCard['Template']['AutonomyCh'] = "自治、自省";
$eReportCard['Template']['AutonomyEn'] = "Autonomy";  
$eReportCard['Template']['CompetenceCh'] = "自我肯定";
$eReportCard['Template']['CompetenceEn'] = "Competence";  
$eReportCard['Template']['RelatednessCh'] = "與人相屬的關係";
$eReportCard['Template']['RelatednessEn'] = "Relatedness";  
$eReportCard['Template']['SelfDisCh'] = "自律";
$eReportCard['Template']['SelfDisEn'] = "Self-discipline";  
$eReportCard['Template']['LearningAttCh'] = "學習態度";
$eReportCard['Template']['LearningAttEn'] = "Learning Attitude";  
$eReportCard['Template']['WillingnessCh'] = "服務精神";
$eReportCard['Template']['WillingnessEn'] = "Willingness to Serve";  
$eReportCard['Template']['SelfIntiCh'] = "自發性";
$eReportCard['Template']['SelfIntiEn'] = "Self-initiative";  
$eReportCard['Template']['InterpersonCh'] = "人際關係";
$eReportCard['Template']['InterpersonEn'] = "Interpersonal Skills";  
$eReportCard['Template']['CourtesyCh'] = "禮貌";
$eReportCard['Template']['CourtesyEn'] = "Courtesy";
$eReportCard['Template']['RemarkEn'] = "Remarks";
$eReportCard['Template']['RemarkCh'] = "評語";

$eReportCard['Template']['RRAEn'] = "Reader's Academy";
$eReportCard['Template']['RRACh'] = "校本閲讀計劃:林學士頁舉計畫";
$eReportCard['Template']['ChiEn'] = "Chinese Language";
$eReportCard['Template']['ChiCh'] = "中國語文";
$eReportCard['Template']['EngEn'] = "English Language";
$eReportCard['Template']['EngCh'] = "英國語文";
$eReportCard['Template']['MathEn'] = "Mathematics";
$eReportCard['Template']['MathCh'] = "數學";
$eReportCard['Template']['SciEn'] = "Science";
$eReportCard['Template']['SciCh'] = "科學";
$eReportCard['Template']['STEn'] = "Science	Technology";
$eReportCard['Template']['STCh'] = "科技";
$eReportCard['Template']['PSHEn'] = "Personal, Social & Humanities";
$eReportCard['Template']['PSHCh'] = "個人、社會及人文";
$eReportCard['Template']['ArtsEn'] = "Arts";
$eReportCard['Template']['ArtsCh'] = "藝術";
$eReportCard['Template']['SportEn'] = "Sports";
$eReportCard['Template']['SportCh'] = "體育";
$eReportCard['Template']['RAsEn'] = "Reading Areas";
$eReportCard['Template']['RAsCh'] = "閱讀範疇";
$eReportCard['Template']['BookNumEn'] = "No. of Books";
$eReportCard['Template']['BookNumCh'] = "冊數";
$eReportCard['Template']['PromoteEn'] = "Promoted";
$eReportCard['Template']['PromoteCh'] = "升級";
$eReportCard['Template']['CondPromoteEn'] = "Conditionally Promoted";
$eReportCard['Template']['CondPromoteCh'] = "試升";
$eReportCard['Template']['RepeatEn'] = "Repeat";
$eReportCard['Template']['RepeatCh'] = "留級";

# Signature Table
$eReportCard['Template']['ClassTeacher'] = "CLASS TEACHER<br>班主任";
$eReportCard['Template']['Principal'] = "PRINCIPAL<br>校長";
$eReportCard['Template']['ParentGuardian'] = "PARENT/GUARDIAN<br>家長/監護人";
$eReportCard['Template']['ParentSignCh'] = "家長簽署";
$eReportCard['Template']['ParentSignEn'] = "Parent’s Signature";
$eReportCard['Template']['IssueDate'] = "DATE OF ISSUE<br>派發日期";
$eReportCard['Template']['SchoolChop'] = "SCHOOL CHOP<br>校印";

$eReportCard['Template']['SignatureTable']['SignatureEn'] = "Signature";
$eReportCard['Template']['SignatureTable']['SignatureCh'] = "簽署";
$eReportCard['Template']['SignatureTable']['ParentGuardianEn'] = "Parent/Guardian";
$eReportCard['Template']['SignatureTable']['ParentGuardianCh'] = "家長/監護人";
$eReportCard['Template']['SignatureTable']['ClassTeacherEn'] = "Class Teacher";
$eReportCard['Template']['SignatureTable']['ClassTeacherCh'] = "班主任";
$eReportCard['Template']['SignatureTable']['PrincipalEn'] = "Principal";
$eReportCard['Template']['SignatureTable']['PrincipalCh'] = "校長";

$eReportCard['Template']['BLManager'] = "Manager";
$eReportCard['Template']['BLDirector'] = "Director";

# Chinese Number
$eReportCard['Template']['MSTable']['MeritNumChi'][1] = "一";
$eReportCard['Template']['MSTable']['MeritNumChi'][2] = "二";
$eReportCard['Template']['MSTable']['MeritNumChi'][3] = "三";
$eReportCard['Template']['MSTable']['MeritNumChi'][4] = "四";
$eReportCard['Template']['MSTable']['MeritNumChi'][5] = "五";
$eReportCard['Template']['MSTable']['MeritNumChi'][6] = "六";

# Grand Marksheet
$eReportCard['GrandMarksheetTypeOption'] = array("班別總結", "全級名次");

?>