<?php
// using: 
$slpLang['student_learning_profile'] = 
	array(
			'EN'=>'Student Learning Profile',
			'B5'=>'學生學習概覽'
			);
$slpLang['date_issue'] = 
	array(
			'EN'=>'Date of Issue',
			'B5'=>'發出日期'
			);
$slpLang['StudentName'] = 
	array(
			'EN'=>'Student Name',
			'B5'=>'學生姓名'
			);
$slpLang['DateOfBirth'] = 
	array(
			'EN'=>'DOB',
			'B5'=>'出生日期'
			);
$slpLang['AdmissionDate'] = 
	array(
			'EN'=>'Admission Date',
			'B5'=>'入學日期'
			);
$slpLang['SchoolAddress'] = 
	array(
			'EN'=>'School Address',
			'B5'=>'學校地址'
			);
$slpLang['SchoolPhone'] = 
	array(
			'EN'=>'School Phone',
			'B5'=>'學校電話'
			);
$slpLang['HKID'] = 
	array(
			'EN'=>'HKID',
			'B5'=>'身份証號碼'
			);
$slpLang['Gender'] = 
	array(
			'EN'=>'Gender',
			'B5'=>'性別'
			);
$slpLang['SchoolCode'] = 
	array(
			'EN'=>'School Code',
			'B5'=>'學校編號'
			);
$slpLang['StudentParticulars'] = 
	array(
			'EN'=>'Student Particulars',
			'B5'=>'學生資料'
			);
$slpLang['Subject'] = 
	array(
			'EN'=>'Subject',
			'B5'=>'科目'
			);
$slpLang['FullMark'] = 
	array(
			'EN'=>'Full Mark',
			'B5'=>'滿分'
			);
$slpLang['MarkPerformance'] = 
	array(
			'EN'=>'Mark/Performance in School',
			'B5'=>'校內表現'
			);
$slpLang['MarkPerformanceWithBR'] = 
	array(
			'EN'=>'Mark /<br/>Performance in School',
			'B5'=>'校內表現'
			);
$slpLang['NameOfPro/ExtAct/Prog'] = 
	array(
			'EN'=>'Name of Projects/Extension Activities/Programmes',
			'B5'=>'研習作品 / 活動項目'
			);
$slpLang['AcademicPerformanceInSchool'] = 
	array(
			'EN'=>'Academic Performance in School at Senior Secondary Level',
			'B5'=>'校內高中學科成績'
			);
$slpLang['AcademicPerformanceInSchool_LowerForm'] = 
	array(
			'EN'=>'Academic Performance in School at Junior Secondary Level',
			'B5'=>'校內初中學科成績'
			);
$slpLang['OtherLearningExperiences'] = 
	array(
			'EN'=>'Other Learning Experiences',
			'B5'=>'其他學習經歷'
			);
$slpLang['ProgrammesWithDescription'] = 
	array(
			'EN'=>'Programmes (with description)',
			'B5'=>'活動項目(及簡介)'
			);
$slpLang['Programmes'] =
	array(
			'EN'=>'Programmes',
			'B5'=>'活動項目'
	);
$slpLang['SelectedRecords'] = 
	array(
			'EN'=>' - Selected Records',
			'B5'=>' - 經選擇後的項目'
			);
$slpLang['SchoolYear'] = 
	array(
			'EN'=>'School Year',
			'B5'=>'學年'
			);
$slpLang['RoleOfParticipation'] = 
	array(
			'EN'=>'Role of Participation',
			'B5'=>'參與角色'
			);
$slpLang['PartnerOrganizationsIfAny'] = 
	array(
			'EN'=>'Partner Organizations (if any)',
			'B5'=>'合辦機構 (如有)'
			);
$slpLang['PartnerOrganizations'] = 
	array(
			'EN'=>'Partner Organizations',
			'B5'=>'合辦機構'
			);
$slpLang['EssentialLearningExperiences'] = 
	array(
			'EN'=>'Components of OLE',
			'B5'=>'其他學習經歷範疇'
			);
$slpLang['AchievementsIfAny'] = 
	array(
			'EN'=>'Awards / Certifications / Achievements* (if any)',
			'B5'=>'獎項 / 証書文憑 / 成就* (如有)'
			);
$slpLang['AchievementsStars'] = 
	array(
			'EN'=>'Awards / Certifications / Achievements**',
			'B5'=>'獎項 / 証書文憑 / 成就**'
			);
$slpLang['clubsProgrammeDescription'] = 
	array(
			'EN'=>'Clubs/Programmes (with description)*',
			'B5'=>'學會/活動項目(及簡介)*'
			);
$slpLang['OLE_TableDescription'] = 
	array(
			'EN'=>'Information about Other Learning Experiences must be validated by the school. Other Learning Experiences can be achieved through programmes organised by the school or co-organised by the school with outside organisations. They may include learning experiences implemented during time-tabled and/or non-time-tabled learning time. Apart from core and elective subjects, Other Learning Experiences that the student particpates in during his/her senior secondary education include Moral and Civic Education, Aesthetic Development, Physical Development, Community Service and Career-related Experiences.',
			'B5'=>'其他學習經歷的有關資料，須由學校確認。其他學習經歷可透過由學校舉辦或學校與校外機構合辦的學習活動獲得，包括在上課時間表以內及/或以外的學習時間進行的有關學習經歷。除核心及選修科目外，在高中學習階段的其他學習經歷，尚包括德育及公民教育、藝術發展、體育發展、社會服務及與工作有關的經驗。'
			);
$slpLang['ExternalOLE_TableDescription'] = 
	array(
			'EN'=>'For learning programmes not organized by the school during the senior secondary education, student could provide the information to the school. It is not necessary for the school to validate information below. Student should hold full responsibility to provide evidence to relevant people when requested.',
			'B5'=>'學生可向學校提供一些在高中階段曾參與過而並非由學校舉辦的學習活動資料。學校不須確認學生的參與資料。在有需要時，學生須負全責向相關人仕提供適當証明。'
			);
$slpLang['RequiredRemark'] = 
	array(
			'EN'=>'Evidence of awards/ certifications/ achievements listed is available for submission when required',
			'B5'=>'有需要時可提供 獎項 / 証書文憑 / 成就 作証明'
			);
$slpLang['AwardsSkill'] = 
	array(
			'EN'=>'Apart from explaining what the programme is about, the description also shows briefly what knowledge, generic skills, values and attitudes would be developed through the experience.',
			'B5'=>'本部分除介紹相關的活動項目外，也可概略地述說學生透過參與該活動項目所發展得來的知識、共通能力、價值觀及態度。'
			);
$slpLang['ExperienceRemark'] = 
	array(
			'EN'=>'Remarks: The above list, which does not mean to be exhaustive, merely illustrates the \'key\' learning experiences acquired by the student throughout the senior secondary years.',
			'B5'=>'備註: 上表只展示了學生在高中階段主要的學習經歷，並不需要徹底地列出所有曾參與過的經歷。'
			);
$slpLang['AwardsAndAchievements'] = 
	array(
			'EN'=>'Awards and Achievements',
			'B5'=>'獎項及成就'
			);
$slpLang['Remarks'] = 
	array(
			'EN'=>'Remarks',
			'B5'=>'備註'
			);
$slpLang['ListOfAwardsAndMABySchool'] = 
	array(
			'EN'=>'List of Awards and Major Achievements Issued by the School',
			'B5'=>'校內頒發的主要獎項及成就'
			);
$slpLang['Organization'] = 
	array(
			'EN'=>'Organization',
			'B5'=>'合辦機構'
			);
$slpLang['Performace/Awards'] = 
	array(
			'EN'=>'Performance / Awards and Key Participation Outside School',
			'B5'=>'校外表現 / 獎項及重要參與'
			);
$slpLang['StudentSelfAccount'] = 
	array(
			'EN'=>'Student\'s \'Self-Account\'',
			'B5'=>'學生自述'
			);
$slpLang['Optional'] = 
	array(
			'EN'=>'(Optional)',
			'B5'=>'(可選擇填寫)'
			);
$slpLang['SelfAccount_TableDescription'] = 
	array(
			'EN'=>'In this part, student could provide additional information to highlight any aspects of his/her learning life and personal development during
the senior secondary education for readers’ (e.g. tertiary institutions, future employers) references. In order to give a fuller picture to the
readers, student could consider to provide information about their key achievements before the period of senior secondary ',
			'B5'=>'學生可於此欄分享其高中學習生活及個人發展之得著或提供額外資料，好讓其他人仕(例如各大專院校及未來僱主等) 作參考之用。為提供
更全面的內容，學生可考慮提及於高中前所獲得的重要成就。'
			);

$slpLang['StudentSelfAccount_Optional'] = 
	array(
			'EN'=>$slpLang['StudentSelfAccount']['EN']." ".$slpLang['Optional']['EN'],
			'B5'=>$slpLang['StudentSelfAccount']['B5']." ".$slpLang['Optional']['B5'],
			);
$slpLang['NameOfEducationInstituation'] = 
	array(
			'EN'=>'Name of Educational Institution',
			'B5'=>'學校名稱'
			);
$slpLang['End'] = 
	array(
			'EN'=>'End',
			'B5'=>'完'
			);

$slpLang['MALE'] = 
	array(
			'EN'=>'M',
			'B5'=>'男'
			);

$slpLang['FEMALE'] = 
	array(
			'EN'=>'F',
			'B5'=>'女'
			);
$slpLang['ClassName_ClassNo'] = 
	array(
			'EN'=>'Class',
			'B5'=>'班別'
			);
$slpLang['StudentID'] = 
	array(
			'EN'=>'Student ID',
			'B5'=>'學生編號'
			);
$slpLang['ClassInfo'] = 
	array(
			'EN'=>'Class(Class No.)',
			'B5'=>'班別(學號)'
			);
$slpLang['RegistrationNo'] = 
	array(
			'EN'=>'Registration Number',
			'B5'=>'註冊編號'
			);
$slpLang['StudentsSelfAccount'] = 
	array(
			'EN'=>'Student\'s \'Self-Account\'',
			'B5'=>'學生的自述'
			);
$slpLang['OptionalPure'] = 
	array(
			'EN'=>'Optional',
			'B5'=>'可選擇填寫'
			);
$slpLang['SelfDesc2'] = 
	array(
			'EN'=>'(No more than 1000 words in English or 1600 words in Chinese)',
			'B5'=>'(以1000字內之英文 或 1600字內之中文撰寫本部分)'
			);
?>