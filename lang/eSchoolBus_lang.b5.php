<?php 
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/

// left menu
$Lang['eSchoolBus']['ManagementArr']['MenuTitle'] = '管理';
$Lang['eSchoolBus']['ManagementArr']['AttendanceArr']['MenuTitle'] = '點名';
$Lang['eSchoolBus']['ManagementArr']['SpecialArrangementArr']['MenuTitle'] = '更改當天安排';
$Lang['eSchoolBus']['ReportArr']['MenuTitle'] = '報告';
$Lang['eSchoolBus']['ReportArr']['ClassListArr']['MenuTitle'] = '各班乘車名單';
$Lang['eSchoolBus']['ReportArr']['RouteArrangementArr']['MenuTitle'] = '校車綫路安排';
$Lang['eSchoolBus']['ReportArr']['StudentListArr']['MenuTitle'] = '乘坐校車學生名單';
$Lang['eSchoolBus']['ReportArr']['AsaContactArr']['MenuTitle'] = 'ASA校車學生聯繫表';
$Lang['eSchoolBus']['ReportArr']['AsaRouteArr']['MenuTitle'] = 'ASA校車路綫';
$Lang['eSchoolBus']['ReportArr']['PickUpArr']['MenuTitle'] = '校車接送表';
$Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['MenuTitle'] = '指定日期各車(乘車)名單';
$Lang['eSchoolBus']['ReportArr']['StudentListByBusArr']['NotTakeBus'] = '指定日期各車(不乘車)名單';
$Lang['eSchoolBus']['ReportArr']['StudentListByClassArr']['MenuTitle'] = '指定日期各班(乘車)名單';
$Lang['eSchoolBus']['SettingsArr']['MenuTitle'] = '設定';
$Lang['eSchoolBus']['SettingsArr']['BusStopsArr']['MenuTitle'] = '地點';
$Lang['eSchoolBus']['SettingsArr']['RouteArr']['MenuTitle'] = '路綫';
$Lang['eSchoolBus']['SettingsArr']['VehicleArr']['MenuTitle'] = '校車';
$Lang['eSchoolBus']['SettingsArr']['StudentsArr']['MenuTitle'] = '乘車學生';
$Lang['eSchoolBus']['SettingsArr']['CutoffTimeArr']['MenuTitle'] = '乘車請假';
$Lang['eSchoolBus']['ViewArr']['MenuTitle'] = '校車管理';
$Lang['eSchoolBus']['ViewArr']['Student']['MenuTitle'] = '檢示學生乘車安排';


$Lang['eSchoolBus']['ActivityArr']['ASA'] = 'ASA';
$Lang['eSchoolBus']['ActivityArr']['FootballClass'] = '足球課';
$Lang['eSchoolBus']['ActivityArr']['SwimmingClass'] = '游泳課';
$Lang['eSchoolBus']['ActivityArr']['Orchestra'] = '樂隊';
$Lang['eSchoolBus']['ActivityArr']['MUN'] = 'MUN';
$Lang['eSchoolBus']['ActivityArr']['Volleyball'] = '排球';

$Lang['eSchoolBus']['AttendanceStatusArr']['OnTime']='準時';
$Lang['eSchoolBus']['AttendanceStatusArr']['Late']='遲到';
$Lang['eSchoolBus']['AttendanceStatusArr']['Absent']='缺席';
$Lang['eSchoolBus']['AttendanceStatusArr']['Activity']='外出活動';
$Lang['eSchoolBus']['AttendanceStatusArr']['Parent']='家長自接';

//General 
$Lang['eSchoolBus']['General']['From'] = '由';
$Lang['eSchoolBus']['General']['To'] = '至';

//Management
$Lang['eSchoolBus']['Management']['SpecialArrangement']['SpecialArrangementByDates'] = '指定特別日子安排';
$Lang['eSchoolBus']['Management']['SpecialArrangement']['FollowPreviousArrangementForNextRecord'] = '會以對上一個安排作新加。';
$Lang['eSchoolBus']['Management']['SpecialArrangement']['UnableToSetSpecialArrangements'] = '由於未設定平常乘車安排，不能為該學生設定特別乘車安排。';
$Lang['eSchoolBus']['Management']['Attendance']['NoStudents'] = '本站沒有相關學生';
$Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement'] = '行程已被更改';
$Lang['eSchoolBus']['Management']['Attendance']['SpecialArrangement2'] = '特別安排';
$Lang['eSchoolBus']['Management']['Attendance']['HaveASA'] = '本日乘坐ASA路線';

$Lang['eSchoolBus']['Management']['Attendance']['Date'] = '日期';
$Lang['eSchoolBus']['Management']['Attendance']['Route'] = '路綫';
$Lang['eSchoolBus']['Management']['Attendance']['AmPm'] = '上午/下午';
$Lang['eSchoolBus']['Management']['Attendance']['TimeAndStopName'] = '時間 / 站名';
$Lang['eSchoolBus']['Management']['Attendance']['StudentName'] = '學生姓名';
$Lang['eSchoolBus']['Management']['Attendance']['Status'] = '狀態';
$Lang['eSchoolBus']['Management']['Attendance']['Remarks'] = '備註';
$Lang['eSchoolBus']['Management']['Attendance']['ContactNumber'] = '聯系電話';

//Settings
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameEng'] = '地點 (英文)';
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopNameChi'] = '地點 (中文)';
$Lang['eSchoolBus']['Settings']['BusStops']['BusStopName'] = '地點';

$Lang['eSchoolBus']['Settings']['Vehicle']['CarNumber'] = '車號';
$Lang['eSchoolBus']['Settings']['Vehicle']['CarPlateNumber'] = '車牌號碼';
$Lang['eSchoolBus']['Settings']['Vehicle']['Driver'] = '校車司機';
$Lang['eSchoolBus']['Settings']['Vehicle']['DriverName'] = '姓名';
$Lang['eSchoolBus']['Settings']['Vehicle']['DriverPhoneNumber'] = '電話號碼';
$Lang['eSchoolBus']['Settings']['Vehicle']['NumberOfSeat'] = '座位數量';
$Lang['eSchoolBus']['Settings']['Vehicle']['Status'] = '狀態';
$Lang['eSchoolBus']['Settings']['Vehicle']['Active'] = '使用中';
$Lang['eSchoolBus']['Settings']['Vehicle']['Inactive'] = '已停用';

$Lang['eSchoolBus']['Settings']['Route']['RouteName']='路綫編號';
$Lang['eSchoolBus']['Settings']['Route']['Remarks']='備註';
$Lang['eSchoolBus']['Settings']['Route']['Type']='路綫分類';
$Lang['eSchoolBus']['Settings']['Route']['NormalRoute']='平常路綫';
$Lang['eSchoolBus']['Settings']['Route']['SpecialRoute']='ASA路綫';
$Lang['eSchoolBus']['Settings']['Route']['BusStop']='地點';
$Lang['eSchoolBus']['Settings']['Route']['AmTime']='上學';
$Lang['eSchoolBus']['Settings']['Route']['Am']='上學';
$Lang['eSchoolBus']['Settings']['Route']['PmTime']='放學';
$Lang['eSchoolBus']['Settings']['Route']['Pm']='放學';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][1]='星期一';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][2]='星期二';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][3]='星期三';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][4]='星期四';
$Lang['eSchoolBus']['Settings']['Route']['Weekday'][5]='星期五';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['MON']='星期一';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['TUE']='星期二';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['WED']='星期三';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['THU']='星期四';
$Lang['eSchoolBus']['Settings']['Route']['Weekday']['FRI']='星期五';
$Lang['eSchoolBus']['Settings']['Route']['Dates']='日期';
$Lang['eSchoolBus']['Settings']['Route']['StartDate']='開始日期';
$Lang['eSchoolBus']['Settings']['Route']['EndDates']='完結日期';
$Lang['eSchoolBus']['Settings']['Route']['IsActive']='狀態';
$Lang['eSchoolBus']['Settings']['Route']['Status']['Active']='使用中';
$Lang['eSchoolBus']['Settings']['Route']['Status']['Inactive']='已停用';
$Lang['eSchoolBus']['Settings']['Route']['NoOfStops']='站點數';
$Lang['eSchoolBus']['Settings']['Route']['TimeScheduled']='適用時段';
$Lang['eSchoolBus']['Settings']['Route']['ValidDates']='有效時間範圍';
$Lang['eSchoolBus']['Settings']['Route']['VehicleNum']='使用車號';
$Lang['eSchoolBus']['Settings']['Route']['LastModified']='最後修改';
$Lang['eSchoolBus']['Settings']['Route']['Teacher'] = '負責老師';
$Lang['eSchoolBus']['Settings']['Route']['ArriveOrLeaveTime']='到校/離校時間';
$Lang['eSchoolBus']['Settings']['Route']['LeaveTime']='離校時間';
$Lang['eSchoolBus']['Settings']['Route']['AddTeacher']='增加負責老師';
$Lang['eSchoolBus']['Settings']['Route']['SelectTeacher']='選擇負責老師';
$Lang['eSchoolBus']['Settings']['Route']['CtrlMultiSelectMessage'] = "按CTRL選擇更多";

$Lang['eSchoolBus']['Settings']['Student']['AM'] = '上學';
$Lang['eSchoolBus']['Settings']['Student']['PM'] = '放學';
$Lang['eSchoolBus']['Settings']['Student']['BusStop'] = '車站';
$Lang['eSchoolBus']['Settings']['Student']['AMBus'] = '上學校車';
$Lang['eSchoolBus']['Settings']['Student']['PMBus'] = '放學校車';
$Lang['eSchoolBus']['Settings']['Student']['ByParent'] = '家長自接';
$Lang['eSchoolBus']['Settings']['Student']['NotTake'] = '不乘坐';
$Lang['eSchoolBus']['Settings']['Student']['Activity'] = '活動';
$Lang['eSchoolBus']['Settings']['Student']['SelectActivity'] = '選擇活動';
$Lang['eSchoolBus']['Settings']['Student']['SelectRoute'] = '選擇路綫';
$Lang['eSchoolBus']['Settings']['Student']['SelectBusStop'] = '選擇車站';
$Lang['eSchoolBus']['Settings']['Student']['LeaveBlank'] = '請留空';
$Lang['eSchoolBus']['Settings']['Student']['RowNumber'] = '行數';
$Lang['eSchoolBus']['Settings']['Student']['Errors'] = '錯誤';
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'] = array();
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Login ID','學生登入編號 (請於學生用戶管理查找)。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('AM Normal Route','上學平常路綫 (如不需要乘坐請留空此欄)。','get_route_N_AM');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('AM Normal Route Bus Stop','上學平常路綫車站 。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('PM Normal Route','放學平常路綫 (如不需要乘坐請留空此欄)。','get_route_N_PM');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('PM Normal Route Bus Stop','放學平常路綫車站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route Activity','星期一的ASA活動 (如無活動請留空此欄)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route','星期一的ASA路綫。','get_route_S_MON');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Monday ASA Route Bus Stop','星期一的ASA路綫車站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route Activity','星期二的ASA活動 (如無活動請留空此欄)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route','星期二的ASA路綫。','get_route_S_TUE');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Tueday ASA Route Bus Stop','星期二的ASA路綫車站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route Activity','星期三的ASA活動 (如無活動請留空此欄)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route','星期三的ASA路綫。','get_route_S_WED');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Wednesday ASA Route Bus Stop','星期三的ASA路綫車站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route Activity','星期四的ASA活動 (如無活動請留空此欄)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route','星期四的ASA路綫。','get_route_S_THU');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Thursday ASA Route Bus Stop','星期四的ASA路綫車站。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route Activity','星期五的ASA活動 (如無活動請留空此欄)。','get_asa_activity');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route','星期五的ASA路綫。','get_route_S_FRI');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Friday ASA Route Bus Stop','星期五的ASA路綫車站 。','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('Start Date','開始日期 (日期格式為年月日 yyyy-mm-dd)','');
$Lang['eSchoolBus']['Settings']['Student']['ImportArr'][] = array('End Date','完結日期 (日期格式為年月日 yyyy-mm-dd)','');

$Lang['eSchoolBus']['Settings']['Student']['ImportError']['CannotBeFound'] = '找不到 <!--TARGET-->。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['RouteIsNotSet'] = '未設定 <!--TARGET--> 的路綫。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['BusStopIsNotSet'] = '未設定 <!--TARGET--> 的車站。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidStartDate'] = '開始日期不正確。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['InvalidEndDate'] = '完結日期不正確。';
$Lang['eSchoolBus']['Settings']['Student']['ImportError']['ExceedBusSeat'] = '該校巴路線已满座:';

$Lang['eSchoolBus']['Settings']['ApplyLeave']['ApplyLeaveCutOffTiming'] = '設定截止時間';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['GotoSchool'] = '上學';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['LeaveSchool'] = '放學';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['DaysBeforeApplyLeave'] = '需要提前多少天申請請假紀錄';
$Lang['eSchoolBus']['Settings']['ApplyLeave']['ClassGroup'] = "班別組別";

//eService
$Lang['eSchoolBus']['eService']['View']['Student'] = '本月乘車安排';
$Lang['eSchoolBus']['eService']['StudentView']['Time'] = '時間';
$Lang['eSchoolBus']['eService']['StudentView']['Class'] = '班別';
$Lang['eSchoolBus']['eService']['StudentView']['Date'] = '日期';
$Lang['eSchoolBus']['eService']['StudentView']['Year'] = '年';
$Lang['eSchoolBus']['eService']['StudentView']['Month'] = '月';

//Warning
$Lang['eSchoolBus']['Managment']['Attendance']['Warning']['WeekendNotAvailable'] = '週末不適用';
$Lang['eSchoolBus']['Managment']['Attendance']['Warning']['SundayNotAvailable'] = '週日不適用';

$Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseAtLeastOne']='請選擇至少一個地點';
$Lang['eSchoolBus']['Settings']['BusStops']['Warning']['ChooseOnlyOne']='請選擇一個地點';

$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarNum'] = '相同的車號已經存在。';
$Lang['eSchoolBus']['Settings']['Vehicle']['Warning']['DuplicatedCarPlateNum'] = '相同的車牌號碼已經被其他車輛使用了。';

$Lang['eSchoolBus']['Settings']['Student']['Warning']['ExceedBusSeat'] = '該校巴路線已满座:';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRoute'] = '請選擇路綫。';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectBusStop'] = '請選擇車站。';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['RequestSelectRouteAndBusStop'] = '請選擇路綫及車站。';
$Lang['eSchoolBus']['Settings']['Student']['Warning']['StudentHasBeenSet'] = '這位學生於本學年已經設定了行車表，請修改該紀錄。';

$Lang['eSchoolBus']['Settings']['Warning']['ConfirmDelete']='確定刪除?';
$Lang['eSchoolBus']['Settings']['Warning']['FieldCannotEmpty']='項目不能留空';
$Lang['eSchoolBus']['Settings']['Warning']['RequestInputData'] = '請輸入此數據。';
$Lang['eSchoolBus']['Settings']['Warning']['DuplicatedDate'] = '日期重覆了。';
$Lang['eSchoolBus']['Settings']['Warning']['Duplicate'] = '項目名稱已被使用';

//Report
$Lang['eSchoolBus']['Report']['ClassStudentList']['StudentNameList'] = 'Student name list  學生名單';
$Lang['eSchoolBus']['Report']['ClassStudentList']['RegularDays'] = '平常日子';
$Lang['eSchoolBus']['Report']['ClassStudentList']['People'] = '人';
$Lang['eSchoolBus']['Report']['ClassStudentList']['RequestSelectForms'] = '請選擇年級。';

$Lang['eSchoolBus']['Report']['RouteArrangement']['Period'] = '時段';
$Lang['eSchoolBus']['Report']['RouteArrangement']['RouteType'] = '路線類型';
$Lang['eSchoolBus']['Report']['RouteArrangement']['ASAWeekday'] = 'ASA 日子';
$Lang['eSchoolBus']['Report']['RouteArrangement']['SelectAll'] = '全選';
$Lang['eSchoolBus']['Report']['RouteArrangement']['NumberOfStudent'] = '人數';
$Lang['eSchoolBus']['Report']['RouteArrangement']['Time'] = '時間';

$Lang['eSchoolBus']['Report']['StudentList']['CarNum'] = '車號';
$Lang['eSchoolBus']['Report']['StudentList']['BusStop'] = '站名';
$Lang['eSchoolBus']['Report']['StudentList']['NumberOfStudent'] = '人數';
$Lang['eSchoolBus']['Report']['StudentList']['StudentName'] = '學生姓名';

$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Bus'] = '校巴';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['RequestSelectBus'] = '請選擇校巴。';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Teacher'] = '校車老師';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusRoute'] = '校車路綫';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['BusNumber'] = '原車號';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Name'] = '姓名';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['FormClass'] = '班級';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['PickupAddress'] = '接送地址';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['ContactPhone'] = '聯繫電話';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['EmailAddress'] = '郵箱';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['MON'] = '周一';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['TUE'] = '周二';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['WED'] = '周三';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['THU'] = '周四';
$Lang['eSchoolBus']['Report']['ASABusStudentContactList']['Weekday']['FRI'] = '周五';

$Lang['eSchoolBus']['Report']['Pickup']['SchoolBusList'] = 'BIBA School Bus List 海嘉学校校车接送表';
$Lang['eSchoolBus']['Report']['Pickup']['GeneralSchoolBusList'] = 'School Bus List 校車接送表';
$Lang['eSchoolBus']['Report']['Pickup']['BusNumber'] = 'Bus No. / 班車號';
$Lang['eSchoolBus']['Report']['Pickup']['Route'] = 'Route / 路綫';
$Lang['eSchoolBus']['Report']['Pickup']['BusDriver'] = 'Bus Driver / 校車司機';
$Lang['eSchoolBus']['Report']['Pickup']['BusTeacher'] = 'Bus Teacher / 校車老師';
$Lang['eSchoolBus']['Report']['Pickup']['Date'] = '日期 / Date';
$Lang['eSchoolBus']['Report']['Pickup']['Year'] = '年';
$Lang['eSchoolBus']['Report']['Pickup']['Month'] = '月';
$Lang['eSchoolBus']['Report']['Pickup']['Day'] = '日';
$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['en'] = 'No.';
$Lang['eSchoolBus']['Report']['Pickup']['Sequence']['ch'] = '序';
$Lang['eSchoolBus']['Report']['Pickup']['Name']['en'] = 'Name';
$Lang['eSchoolBus']['Report']['Pickup']['Name']['ch'] = '姓名';
$Lang['eSchoolBus']['Report']['Pickup']['Class']['en'] = 'Class';
$Lang['eSchoolBus']['Report']['Pickup']['Class']['ch'] = '班級';
$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['en'] = 'Sign-out Signature';
$Lang['eSchoolBus']['Report']['Pickup']['SignOutSignature']['ch'] = '接送者簽字';
$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['en'] = 'Sign-out Signature';
$Lang['eSchoolBus']['Report']['Pickup']['TeacherSignOutSignature']['ch'] = '接送教師簽字';
$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['en'] = 'Pick-up Person';
$Lang['eSchoolBus']['Report']['Pickup']['PickupPerson']['ch'] = '接送者簽字';
$Lang['eSchoolBus']['Report']['Pickup']['Remark']['en'] = 'Remark';
$Lang['eSchoolBus']['Report']['Pickup']['Remark']['ch'] = '備註';
$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['en'] = 'Arrived at home';
$Lang['eSchoolBus']['Report']['Pickup']['ArrivedAtHome']['ch'] = '到家時間';

$Lang['eSchoolBus']['Report']['Attendance']['TotalAbsent'] = '缺席總數';
$Lang['eSchoolBus']['Report']['Attendance']['TotalOnBus'] = '乘車總數';
$Lang['eSchoolBus']['Report']['Bus'] = '校車';
$Lang['eSchoolBus']['Report']['ByClass']['All'] = '全部';
$Lang['eSchoolBus']['Report']['ByClass']['HasRoute'] = '有路線';
$Lang['eSchoolBus']['Report']['ByClass']['HasRouteOrNot'] = '有路線/無路線';
$Lang['eSchoolBus']['Report']['ByClass']['NoRoute'] = '無路線';
$Lang['eSchoolBus']['Report']['ByClass']['NotTake'] = '不乘車';
$Lang['eSchoolBus']['Report']['ByClass']['RequestSelectClass'] = '請選擇班別。';
$Lang['eSchoolBus']['Report']['ByClass']['Take'] = '乘車';
$Lang['eSchoolBus']['Report']['ByClass']['TakeOption'] = '乘車/不乘車';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Name'] = '姓名';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['StudentNumber'] = '學號';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Route'] = '路線';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Class'] = '班別';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Campus'] = '位置';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['TimeSlot'] = '上學/放學';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['TakeOrNot'] = '乘車/不乘車';
$Lang['eSchoolBus']['Report']['ByClass']['Title']['Remark'] = '老師備註';
$Lang['eSchoolBus']['Report']['Campus'] = '位置';
$Lang['eSchoolBus']['Report']['IsHolidayWarning'] = '所選日期為假期,請重選';
$Lang['eSchoolBus']['Report']['SelectDate'] = '請選擇日期';

# App
$Lang['eSchoolBus']['App']['ApplyLeave']['AllStatus'] = '全部狀態';
$Lang['eSchoolBus']['App']['ApplyLeave']['Apply'] = '乘車請假申請';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplicaton'] = '是否確定取消此申請?';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelAllApplyLeave'] = '取消以上日子乘車請假申請';
$Lang['eSchoolBus']['App']['ApplyLeave']['CancelApplyLeave'] = '取消乘車請假申請';
$Lang['eSchoolBus']['App']['ApplyLeave']['ConfirmSubmit'] = '是否確定提交此申請?';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Absent'] = '缺席';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Cancelled'] = '已取消';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Confirmed'] = '已接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Deleted'] = '刪除';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Rejected'] = '不獲接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['FilterStatus']['Waiting'] = '待接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddSuccess'] = '紀錄已經新增';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['AddUnSuccess'] = '新增紀錄失敗';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveSuccess'] = '紀錄已經接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['ApproveUnsuccess'] = '紀錄未能接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelSuccess'] = '紀錄已經取消';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['CancelUnsuccess'] = '取消紀錄失敗';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['InvalidTimeSlot'] = '無效的時段';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingApplicationID'] = '遺失申請編號';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['MissingStudentID'] = '遺失學生編號';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['NotAllowedApplyLeave'] = '不允許申請乘車請假';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectSuccess'] = '紀錄已經更新為不獲接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['RejectUnsuccess'] = '紀錄未能更新為不獲接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateSuccess'] = '紀錄已經更新';
$Lang['eSchoolBus']['App']['ApplyLeave']['Msg']['UpdateUnsuccess'] = '更新紀錄失敗';
$Lang['eSchoolBus']['App']['ApplyLeave']['Reason'] = '原因';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Absent'] = '缺席(自動產生)';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Approved'] = '已接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Cancelled'] = '已取消';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Deleted'] = '刪除(自動產生)';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Pending'] = '待接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['Status']['Rejected'] = '不獲接納';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlot'] = '時段';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['AM'] = '上學';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['PM'] = '放學';
$Lang['eSchoolBus']['App']['ApplyLeave']['TimeSlotType']['WD'] = '全日';
$Lang['eSchoolBus']['App']['ApplyLeave']['Title'] = '乘車請假';
$Lang['eSchoolBus']['App']['Date'] = '日期';
$Lang['eSchoolBus']['App']['Error']['Ajax'] = '存取資料時遇到錯誤,請重試!';
$Lang['eSchoolBus']['App']['Month'][1] = '一月';
$Lang['eSchoolBus']['App']['Month'][2] = '二月';
$Lang['eSchoolBus']['App']['Month'][3] = '三月';
$Lang['eSchoolBus']['App']['Month'][4] = '四月';
$Lang['eSchoolBus']['App']['Month'][5] = '五月';
$Lang['eSchoolBus']['App']['Month'][6] = '六月';
$Lang['eSchoolBus']['App']['Month'][7] = '七月';
$Lang['eSchoolBus']['App']['Month'][8] = '八月';
$Lang['eSchoolBus']['App']['Month'][9] = '九月';
$Lang['eSchoolBus']['App']['Month'][10] = '十月';
$Lang['eSchoolBus']['App']['Month'][11] = '十一月';
$Lang['eSchoolBus']['App']['Month'][12] = '十二月';
$Lang['eSchoolBus']['App']['NewRecord'] = '新增紀錄';
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationContent'] = "校方已接納閣下於[ApplyDate]提交貴子弟[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been accepted.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['ApproveApplicationTitle'] = "接納乘車請假申請通知
Leave Application (not taking school bus) Accepted Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveContent'] = "現收到家長於[ApplyDate]提交學生[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for [StudentEnglishName] submitted by parent at [ApplyDate] has been received.";
$Lang['eSchoolBus']['App']['PushMessage']['ToTeacher']['ApplyLeaveTitle'] = "乘車請假申請通知
Leave Application (not taking school bus) Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationContent'] = "校方已取消閣下於[ApplyDate]提交貴子弟[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been cancelled.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['CancelApplicationTitle'] = "取消乘車請假申請通知
Leave Application (not taking school bus) Cancelled Alert";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationContent'] = "校方未能接納閣下於[ApplyDate]提交貴子弟[StudentChineseName]的乘車請假申請。
Please note that the leave application of not taking school bus for your child [StudentEnglishName] submitted at [ApplyDate] has been rejected.";
$Lang['eSchoolBus']['App']['PushMessage']['ToParent']['RejectApplicationTitle'] = "乘車請假申請不獲接納通知
Leave Application (not taking school bus) Rejected Alert";
$Lang['eSchoolBus']['App']['RequiredField'] = "附有<span class='alertText'> * </span>的項目必須填寫";
$Lang['eSchoolBus']['App']['Schedule']['Absent'] = '缺席(自動產生)';
$Lang['eSchoolBus']['App']['Schedule']['AppliedLeave'] = '已請假';
$Lang['eSchoolBus']['App']['Schedule']['NA'] = '不適用';
$Lang['eSchoolBus']['App']['Schedule']['Pending'] = '請假待接納';
$Lang['eSchoolBus']['App']['Schedule']['Rejected'] = '請假不獲接納';
$Lang['eSchoolBus']['App']['Tab']['ApplyLeave'] = '乘車請假';
$Lang['eSchoolBus']['App']['Tab']['Schedule'] = '乘車安排';
$Lang['eSchoolBus']['App']['Warning']['AllowFutureDatesOnly'] = '只可以申請今天或之後的日期';
$Lang['eSchoolBus']['App']['Warning']['DuplicateApplication'] = '不允許重複申請';
$Lang['eSchoolBus']['App']['Warning']['InputEndDate'] = '請輸入結束日期';
$Lang['eSchoolBus']['App']['Warning']['InputLeaveReason'] = '請輸入請假的原因';
$Lang['eSchoolBus']['App']['Warning']['InputStartDate'] = '請輸入開始日期';
$Lang['eSchoolBus']['App']['Warning']['PassedCutoffDate'] = '乘車請假必須在%s日前提交申請,請與學校聯絡。';
$Lang['eSchoolBus']['App']['Warning']['PassedCutoffTime'] = '乘車請假必須%s前提交申請,請與學校聯絡。';
$Lang['eSchoolBus']['App']['Warning']['SelectStudent'] = '請選擇學生';
$Lang['eSchoolBus']['App']['Warning']['StartDateIsLaterThanEndDate'] = '結束日期不能早於開始日期';
$Lang['eSchoolBus']['App']['Warning']['StartDateTimeSlotIsLaterThanEndDateTimeSlot'] = '結束日期時段不能早於開始日期時段';

$Lang['eSchoolBus']['TeacherApp']['AllStudent'] = '全部學生';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['Add'] = '新增乘車請假';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmApproval'] = '是否確定批準此申請?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmCancel'] = '是否確定取消此申請?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmReject'] = '是否確定拒絕此申請?';
$Lang['eSchoolBus']['TeacherApp']['ApplyLeave']['ConfirmSubmit'] = '是否確定新增此請假?';
$Lang['eSchoolBus']['TeacherApp']['Remark']='老師備註';
$Lang['eSchoolBus']['TeacherApp']['Tab']['Handled'] = '已處理';
$Lang['eSchoolBus']['TeacherApp']['Tab']['Pending'] = '待處理';
?>