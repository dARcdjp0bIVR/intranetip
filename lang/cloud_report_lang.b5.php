<?php 
/********************** Change Log ***********************/
/*
 * 2019-03-21 Isaac
 * Changed some of the wordings
 */
/***********************************************************/

### eclass on the cloud report page ###
$Lang['Cloud']['Report']['Title']['Greeting']='歡迎使用 eClass 雲端服務';
$Lang['Cloud']['Report']['Title']['UsedStorage']='儲存空間';
$Lang['Cloud']['Report']['Title']['Traffic']='';
$Lang['Cloud']['Report']['CurrentPackage']='現在儲存空間方案';
$Lang['Cloud']['Report']['Plan']='計劃';
$Lang['Cloud']['Report']['Used']='已使用';
$Lang['Cloud']['Report']['Usage'] = $Lang['Cloud']['Report']['Used'].' <!--usedStorage--> <!--capacityUnit--> (共 <!--availableStorage--> <!--capacityUnit-->)';
$Lang['Cloud']['Report']['ValidTill']='有效至';
$Lang['Cloud']['Report']['ChineseDay'] = '日';
$Lang['Cloud']['Report']['ChineseMonth'] = '月';
$Lang['Cloud']['Report']['ChineseYear'] = '年';

##alert messages##
$Lang['Cloud']['Report']['StorageWarningMessage']='已使用 <!--usedStoragePercentage-->% 的儲存空間. 建議考慮升級你的計劃.';

$Lang['Cloud']['Report']['LegendAry']['intranet']='內聯網';
$Lang['Cloud']['Report']['LegendAry']['classroom']='網上教室';
$Lang['Cloud']['Report']['LegendAry']['imail']='iMail';
$Lang['Cloud']['Report']['LegendAry']['ifolder']='我的資料夾';
$Lang['Cloud']['Report']['LegendAry']['mysql']='MySQL 資料庫';
$Lang['Cloud']['Report']['LegendAry']['other']='其他';
$Lang['Cloud']['Report']['Intro']='感謝使用及支持eClass on the Cloud雲端服務，管理員可透過此報告查看有關雲端服務及eClass平台的系統資訊。';
$Lang['Cloud']['Report']['RecommandationHeadingAry']['Recommendations']='如何釋出eClass on the Cloud儲存空間？';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Intranet']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Classroom']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Imail']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Ifolder']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Mysql']='';
// $Lang['Cloud']['Report']['RecommandationHeadingAry']['Others']='';
$Lang['Cloud']['Report']['RecommandationAry']['Recommendations']='-	管理員刪除 或 建議用戶刪除「網上教室」內舊有或不再使用的檔案。<br>
-	建議用戶刪除舊有或不要的電郵。<br>
-	管理員刪除 或 建議用戶刪除「網上社群」內舊有或不再使用的資料。<br>
-	管理員刪除 或 建議用戶刪除儲存於eClass on the Cloud內所有舊檔案或不再使用的資料。
';
// $Lang['Cloud']['Report']['RecommandationAry']['Intranet']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Classroom']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Imail']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Ifolder']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Mysql']='';
// $Lang['Cloud']['Report']['RecommandationAry']['Others']='';
?>