<?php
if ($sys_custom['PowerClass']) {
	// Editing by 
	$Lang['Header']['HomeTitle'] = "Power Class @ eClass";

	$Lang["PageTitle"] = "Power Class @ eClass";
	$Lang['Header']['Menu']['SystemSetting'] = "System Setup";
	$Lang['Header']['Menu']['PC_App'] = "App";
	$Lang['Header']['Menu']['PowerLesson2'] = "PowerLesson";
	$Lang['Header']['Menu']['StemXPl2'] = "STEM@eClass";
	$Lang['Header']['Menu']['PowerClassReport'] = "Analytics";
	$Lang['Header']['Menu']['iCalendar'] = "iCalendar";
	$Lang['Header']['Menu']['eBooking'] = "eBooking";
	$Lang['Header']['Menu']['eAttednance'] = "eAttendance";
	$Lang['Header']['Menu']['eAttendanceLesson'] = "eAttendance(Lesson)";
	$Lang['Header']['Menu']['ePayment'] = "ePayment";
	$Lang['Header']['Menu']['ChangePassword'] = "Change Password";
	$Lang['Header']['Menu']['iAccount'] = "My profile";
	
	$Lang['Footer']['PowerBy'] = "Powered by ";
	
	$Lang['PowerClass'] = array();
	$Lang['PowerClass']['Chinese'] = "繁體";
	$Lang['PowerClass']['ChineseSimplified'] = "简体";
	$Lang['PowerClass']['English'] = "ENG";
	$Lang["PowerClass"]["Settings"] = "Settings";
	$Lang["PowerClass"]["Add"] = "Add";
	$Lang["PowerClass"]["Import"] = "Import";
	$Lang["PowerClass"]["SendTo"] = "Send to ";
	$Lang["PowerClass"]["Parent"] = "Parent";
	$Lang["PowerClass"]["Student"] = "Student";
	$Lang["PowerClass"]["Teacher"] = "Teacher";
	$Lang["PowerClass"]["Staff"] = "Staff";
	$Lang["PowerClass"]["Payment"] = "Payment";
	
	$Lang["PowerClass"]["Login"]["Login"] = "Login";
	$Lang["PowerClass"]["Login"]["LoginTitle"] = "Employee Mobile App Platform";
	$Lang["PowerClass"]["Login"]["LoginID"] = "Login ID";
	$Lang["PowerClass"]["Login"]["LoginID_alert"] = "Please enter the user login.";
	$Lang["PowerClass"]["Login"]["LoginID_err"] = "Invalid LoginID/Password.";
	$Lang["PowerClass"]["Login"]["Password"] = "Password";
	$Lang["PowerClass"]["Login"]["Password_alert"] = "Please enter the password.";
	$Lang["PowerClass"]["Login"]["Password_err"]= "Invalid Password";
	$Lang["PowerClass"]['Login']['ForgotPassword'] = "Forgot password?";
	
	$Lang["PowerClass"]["Login"]["ForgotPasswordMsg"] = '1. Input UserLogin, then reset pwd mail will be sent to your preset address.\n2. If no mail received, kindly contact your organization.';
	$Lang['PowerClass']['Warnings']['DuplicatedName'] = "Duplicated name.";
	$Lang['PowerClass']['Warnings']['DuplicatedCode'] = "Duplicated code.";
	
	$Lang["PowerClass"]["cht_Today"] = "Today";
	$Lang["PowerClass"]["cht_Yesterday"] = "Yesterday";
	$Lang["PowerClass"]["cht_Week"] = "Week";
	$Lang["PowerClass"]["cht_Month"] = "Month";
	$Lang["PowerClass"]["cht_loginrecord"] = "Login Record";
	$Lang["PowerClass"]["cht_AccessLog"] = "Access Log";
	
	$Lang["PoserClass"]["BtnBack"] = "Back";
	
	$Lang["PowerClass"]["School"] = "School";
	$Lang["PowerClass"]["SchoolYearAndTerm"] = "School Year / Term";
	$Lang["PowerClass"]["Class"] = "Form & Class";
	$Lang["PowerClass"]["Subject"] = "Subject";
	$Lang["PowerClass"]["Group"] = "Group";
	$Lang["PowerClass"]["Role"] = "Role";
	$Lang["PowerClass"]["HolidayAndEvents"] = "Holiday / Events";
	$Lang["PowerClass"]["SchoolCalendar"] = "School Calendar";
	
	$Lang["PowerClass"]["Account"] = "Account";
	$Lang["PowerClass"]["eClass"] = "Class";
	$Lang["PowerClass"]["eClassroom"] = "eClassroom";
	
	$Lang["PowerClass"]["SchoolNews"] = "School News";
	$Lang["PowerClass"]["SchoolNews_BasicSettings"] = "Basic Settings";
	$Lang["PowerClass"]["eNotice"] = "eNotice";
	$Lang["PowerClass"]["eNotice_BasicSettings"] = "Basic Settings";
	$Lang["PowerClass"]["eNotice_PushMessageTemplate"] = "Push Message Template (App)";
    $Lang["PowerClass"]["eCircular_BasicSettings"] = "Basic Settings (eCircular)";
    $Lang["PowerClass"]["eCircular_AdminSettings"] = "Admin Settings (eCircular)";
	$Lang["PowerClass"]["eHomework"] = "eHomework";
	$Lang["PowerClass"]["eHomework_BasicSettings"] = "Basic Settings";
	$Lang["PowerClass"]["eHomework_ViewerGroup"] = "Viewer Group";
	$Lang["PowerClass"]["MessageCenter"] = "Message Center";
	$Lang["PowerClass"]["MessageCenter_MessageTemplate"] = "Message Templates";
	$Lang["PowerClass"]["ParentApp"] = "Parent App";
	$Lang["PowerClass"]["ParentApp_FunctionAccessRight"] = "Function Access Right";
	$Lang["PowerClass"]["ParentApp_SchoolImage"] = "School Image";
	$Lang["PowerClass"]["ParentApp_LoginStatus"] = "Login Status";
	$Lang["PowerClass"]["ParentApp_Blacklist"] = "Blacklist";
	$Lang["PowerClass"]["ParentApp_PushMessageRight"] = "Push Message Right";
	$Lang["PowerClass"]["StudentApp"] = "Student App";
	$Lang["PowerClass"]["StudentApp_FunctionAccessRight"] = "Function Access Right";
	$Lang["PowerClass"]["StudentApp_LoginStatus"] = "Login Status";
	$Lang["PowerClass"]["StudentApp_SchoolImage"] = "School Image";
	$Lang["PowerClass"]["StudentApp_Account"] = "Account";
	$Lang["PowerClass"]["StudentApp_OfficalPhoto"] = "Student Photos";
	$Lang["PowerClass"]["TeacherApp"] = "Teacher App";
	$Lang["PowerClass"]["TeacherApp_FunctionAccessRight"] = "Function Access Right";
	$Lang["PowerClass"]["TeacherApp_SchoolImage"] = "School Image";
	$Lang["PowerClass"]["TeacherApp_LoginStatus"] = "Login Status";
	$Lang["PowerClass"]["TeacherApp_Blacklist"] = "Blacklist";
	$Lang["PowerClass"]["TeacherApp_PushMessageRight"] = "Push Message Right";
	$Lang["PowerClass"]["TeacherApp_StaffList"] = "Staff List";
	$Lang["PowerClass"]["TeacherApp_StudentList"] = "Student List";
	
	$Lang["PowerClass"]["NumberOfAccess"] = "Number of Access";
	
	$Lang["PowerClass"]["App_GroupMessage"] = "Group Message";
	$Lang["PowerClass"]["App_SchoolInfo"] = "School Info";
	$Lang["PowerClass"]["SchoolSecurity"] = "School Security";
	
	$Lang['General']['SystemProperties'] = "System Properties";
	$Lang['eBooking']['Settings']['FieldTitle']['GeneralPermission'] = "Booking Properties";
	$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'] = "Approval Group";
	$Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'] = "Follow-up Group";
	$Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule'] = "User Permission";
	$Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod'] = "Booking Period";
	$Lang['eBooking']['Settings']['FieldTitle']['Category'] = "Resources Category";
	
	$Lang['PowerClass']['StudentAttendance_TimeSlotSettings'] = "Time Slot Settings";
	$Lang['PowerClass']['TakeAttendance'] = "Take Attendance";
	$Lang['PowerClass']['TakeAttendance_PresetAbsence_New'] = "Preset Student Absence - New";
	$Lang['PowerClass']['TakeAttendance_PresetAbsence_BrowseByStudent'] = "Preset Student Absence - Browse By Student";
	$Lang['PowerClass']['TakeAttendance_PresetAbsence_BrowseByDate'] = "Preset Student Absence - Browse By Date";
	
	$Lang['PowerClass']['GoogleApps_More'] = 'More';
	$Lang["PowerClass"]["TeacherNotice"] = "Teacher (eCircular)";
	
	$Lang['PowerClass']['eInventory']['Category'] = "Category";
	$Lang['PowerClass']['eInventory']['Caretaker'] = "Resource Management Group";
	$Lang['PowerClass']['eInventory']['FundingSource'] = "Funding Source";
	$Lang['PowerClass']['eInventory']['WriteOffReason'] = "Write-off Reason";
	$Lang['PowerClass']['eInventory']['General_Settings'] = "System Properties";
	
	$Lang['PowerClass']['ePayment']['Settings_TerminalIP'] = "Terminal Settings";
	$Lang['PowerClass']['ePayment']['Settings_TerminalAccount'] = "Login Account for Terminal";
	$Lang['PowerClass']['ePayment']['Settings_PaymentCategory'] = "Payment Item Category Settings";
	$Lang['PowerClass']['ePayment']['Settings_Letter'] = "Payment Letter Settings";
	$Lang['PowerClass']['ePayment']['PPS_Setting'] = "PPS Setting";
	
	$Lang['PowerClass']['GoWith']['Parent'] = "Parent user please continue with parent app";
	$Lang['PowerClass']['GoWith']['Student'] = "Student user please continue with student app";
}
if($sys_custom['Project_Use_Sub']){
	if(file_exists($intranet_root.'/lang/' . $sys_custom['Project_Sub_Label']. '/' . basename(__FILE__))){
		include($intranet_root.'/lang/' . $sys_custom['Project_Sub_Label']. '/' . basename(__FILE__));
	}
}
?>