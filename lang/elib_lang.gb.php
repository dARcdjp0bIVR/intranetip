<?php
$eLib["html"]["home"] = "主页";
$eLib["html"]["elibrary_settings"] = "网上图书馆设定";
$eLib["html"]["portal_display_settings"] = "主页显示设定";

$eLib["html"]["book_name"] = "书本名称";
$eLib["html"]["book_cover"] = "封面";

$eLib["html"]["records"] = "记录";
$eLib["html"]["display_top"] = "显示 : 置顶";
$eLib["html"]["catalogue"] = "目录";
$eLib["html"]["books"] = "书本";
$eLib["html"]["display"] = "显示 : ";
$eLib["html"]["display2"] = "显示";
$eLib["html"]["page"] = "页";
$eLib["html"]["view"] = "检视";

$eLib["html"]["most_active_reviewers"] = "最活跃的读者";
$eLib["html"]["most_useful_reviews"] = "最有帮助的读后感";
$eLib["html"]["list_all"] = "检视全部";
$eLib["html"]["last_week"] = "上星期最多阅读次数";
$eLib["html"]["accumulated"] = "累积阅读次数";

$eLib["html"]["recommended_books"] = "推荐书";
$eLib["html"]["bookS_with_highest_hit_rate"] = "最多阅读次数的书";
$eLib["html"]["recommended_reason"] = "推荐理由";

$eLib["html"]["save"] = "储存";
$eLib["html"]["submit"] = "提交";
$eLib["html"]["reset"] = "重设";
$eLib["html"]["cancel"] = "取消";
$eLib["html"]["confirm"] = "确定";

$eLib["html"]["settings"] = "设定";

$eLib["html"]["personal"] = "个人";
$eLib["html"]["my_reading_history"] = "我的阅读纪录";
$eLib["html"]["my_favourites"] = "我的最爱";
$eLib["html"]["my_reviews"] = "我的读后感";
$eLib["html"]["my_notes"] = "我的笔记";
$eLib["html"]["public"] = "公开";
$eLib["html"]["all_reviews"] = "全部读后感";
$eLib["html"]["student_summary"] = "学生总结";
$eLib["html"]["chinese"] = "中文";
$eLib["html"]["english"] = "英文";
$eLib["html"]["show_all"] = "全部显示";

$eLib["html"]["advance_search"] = "进阶搜寻";
$eLib["html"]["search_result"] = "搜寻结果";

$eLib["html"]["title"] = "书名";
$eLib["html"]["author"] = "作者";
$eLib["html"]["source"] = "来源";
$eLib["html"]["category"] = "类别";
$eLib["html"]["level"] = "级别";
$eLib["html"]["date"] = "日子";
$eLib["html"]["with_worksheets"] = "工作纸";
$eLib["html"]["sort_by"] = "排序";

$eLib["html"]["book_title"] = "书名";
$eLib["html"]["publisher"] = "发行商";

$eLib["html"]["all_category"] = "全部类别";
$eLib["html"]["all_level"] = "全部级别";

$eLib["html"]["report"] = "报告";
$eLib["html"]["record"] = "记录";
$eLib["html"]["total"] = "全部";

$eLib["html"]["keywords"] = "关键字";
$eLib["html"]["no_record"] = "没有记录 !";
$eLib["html"]["description"] = "描述";
$eLib["html"]["rating"] = "评分";

$eLib["html"]["click_to_read"] = "点击阅读";
$eLib["html"]["reviews"] = "读后感";
$eLib["html"]["add_to_my_favourite"] = "加入我的最爱";
$eLib["html"]["remove_my_favourite"] = "从我的最爱中移除";

$eLib["html"]["recommend_this_book"] = "推荐此书";
$eLib["html"]["add_new_review"] = "加入新的读后感";

$eLib["html"]["yes"] = "是";
$eLib["html"]["no"] = "否";
$eLib["html"]["was_this_review_helpful"] = "你觉得这读后感有帮助吗?";
$eLib["html"]["of"] = "/";
$eLib["html"]["people_found_this_review_helpful"] = "用户觉得此读后感有用";

$eLib["html"]["review_content"] = "读后感内容";

$eLib["html"]["personal_records"] = "个人记录";

$eLib["html"]["last_read"] = "上一次阅读";
$eLib["html"]["last_modified"] = "上一次更新";
$eLib["html"]["remove"] = "移除";

$eLib["html"]["confirm_remove_msg"] = "你确定要移除这纪录吗?";
$eLib["html"]["notes_content"] = "笔记内容";

$eLib["html"]["all_books"] = "全部书本";
$eLib["html"]["all_categories"] = "全部类别";

$eLib["html"]["num_of"] = "数字";

$eLib["html"]["num_of_review"] = "读后感数字";
$eLib["html"]["last_review_date"] = "上一次读后感日期";

$eLib["html"]["student_name"] = "学生姓名";
$eLib["html"]["num_of_book_read"] = "书本阅读数字";

$eLib["html"]["name"] = "名字";

$eLib["html"]["class"] = "班级";
$eLib["html"]["all_class"] = "全部班级";
$eLib["html"]["student"] = "学生";

$eLib["html"]["book_read"] = "书本阅读";
$eLib["html"]["review"] = "读后感";

$eLib["html"]["book_with_hit_rate_accumulated"] = "最多阅读次数的书 - 累积";
$eLib["html"]["book_with_hit_rate_last_week"] = "最多阅读次数的书 - 上星期";

$eLib["html"]["whole_school"] = "整间学校";

$eLib["html"]["recommend_to"] = "推荐给";
$eLib["html"]["reason_to_recommend"] = "推荐原因";

$eLib["html"]["language"] = "语言";
$eLib["html"]["subcategory"] = "子类别";
$eLib["html"]["english_books"] = "英文书";
$eLib["html"]["chinese_books"] = "中文书";

$eLib["html"]["all_english_books"] = "全部英文书";
$eLib["html"]["all_chinese_books"] = "全部中文书";

$eLib["html"]["all_english_categories"] = "全部英文类别";
$eLib["html"]["all_chinese_categories"] = "全部中文类别";
$eLib["html"]["search_input"] = "搜寻..";
$eLib["html"]["search"] = "搜寻";
$eLib["html"]["please_enter_keywords"] = "请输入搜寻关键字";
$eLib["html"]["publish"] = "公开";
$eLib["html"]["unpublish"] = "不公开";

$eLib["html"]["please_enter_title"] = "请输入书名";
$eLib["html"]["please_enter_author"] = "请输入作者";

$eLib["html"]["with_books_cover"] = "显示封面";
$eLib["html"]["table_list"] = "显示列表";

$eLib["html"]["no_recommend_book"] = "没有推荐书。";


$eLib['ManageBook']["ImportZIPDescribeHeading"] = "ZIP档案需备下档案";

$eLib['ManageBook']["ImportZIPDescribeChinese"] = "
				中文书:<br />
				<b>content.text</b> - text档,  将转换成XML档案 <br />
				<b>\"image\" 文件夹</b> - 包含相关图片案. <br />
				<b>control.text</b> - 控制转换过程. <br />
													";
$eLib['ManageBook']["ImportZIPDescribeEnglish"] = "
				英文书:<br />
				<b>content.xml</b> - XML档,  将转换成XML档案 <br />
				<b>\"image\" 文件夹</b> - 包含相关图片案. <br />
				";													
													

?>