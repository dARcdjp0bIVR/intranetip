<?php
//updating by :           
//please place the value in alphabetical order
$Lang['medical']['General']['All'] = '全部';

$Lang['medical']['menu']['message'] = '護理提示';
$Lang['medical']['menu']['writeMessage'] = '撰寫提示';
$Lang['medical']['menu']['readMessage'] = '接收提示';
$Lang['medical']['menu']['sendMessage'] = '送出提示';

$Lang['medical']['menu']['management'] = '管理';
$Lang['medical']['menu']['meal'] = '早/午/晚餐進食紀錄';
$Lang['medical']['menu']['bowel'] = '排便紀錄';
$Lang['medical']['menu']['studentLog'] = '學生日誌';
$Lang['medical']['menu']['studentSleep'] = '睡眠狀況紀錄';
$Lang['medical']['menu']['problemRecord'] = '有問題紀綠';
$Lang['medical']['menu']['input'] = '輸入';
$Lang['medical']['menu']['event'] = '行為問題及事故紀錄';
$Lang['medical']['menu']['case'] = '個案跟進';
$Lang['medical']['menu']['award'] = '獎勵計劃';
$Lang['medical']['menu']['revisit'] = '覆診紀錄';
if($sys_custom['medical']['MedicalReport'])
    $Lang['medical']['menu']['revisit'] = '醫療紀錄';
$Lang['medical']['menu']['staffEvent'] = '教職員護理紀錄';
$Lang['medical']['menu']['handover'] = '行政交更';

$Lang['medical']['menu']['settings'] = '設定';
$Lang['medical']['menu']['settings_accessRight'] = '用戶權限';
$Lang['medical']['menu']['settings_meal'] = '進食紀錄設定';
$Lang['medical']['menu']['settings_studentlog'] = '學生日誌分類及設定';
$Lang['medical']['menu']['settings_studentsleep'] = '睡眠狀況設定';
$Lang['medical']['menu']['settings_bowel'] = '排便紀錄設定';
$Lang['medical']['menu']['settings_defaultRemarks'] = '預設備註設定';
$Lang['medical']['menu']['settings_noticeRemarks'] = '護理預設提示設定';
$Lang['medical']['menu']['settings_groupFilter'] = '小組篩選設定';
$Lang['medical']['menu']['settings_event_type'] = '事故類別設定';
$Lang['medical']['menu']['settings_staff_event_type'] = '教職員事件類別設定';
$Lang['medical']['menu']['settings_system_setting'] = '系統設定';
$Lang['medical']['menu']['settings_allowed_ip'] = '指定可存取電腦 IP';
$Lang['medical']['menu']['settings_handover'] = '行政交更設定';

$Lang['medical']['menu']['report'] = '報告';
$Lang['medical']['menu']['report_meal'] = '進食報告';
$Lang['medical']['menu']['report_bowel'] = '排便報告';
$Lang['medical']['menu']['report_convulsion'] = '學生日誌報告';
$Lang['medical']['menu']['report_sleep'] = '睡眠狀況報告';
$Lang['medical']['menu']['report_student_event'] = '行為問題及事故報告';
$Lang['medical']['menu']['report_staff_event'] = '教職員護理報告';
$Lang['medical']['menu']['report_revisit'] = "醫療紀錄報告";
$Lang['medical']['menu']['report_handover'] = "行政交更報告";

$Lang['medical']['module'] = '護理系統';

// message option
$Lang['medical']['message']['sender'] = '寄件人';
$Lang['medical']['message']['receiver'] = '收件人';
$Lang['medical']['message']['sendContent'] = '提示內容';
$Lang['medical']['message']['content'] = '內容';
$Lang['medical']['message']['date'] = '日期';
$Lang['medical']['message']['defaultHint'] = ' -- 預設提示 -- ';
$Lang['medical']['message']['AlertMessage']['FillReceiver'] = '請選擇收件人';
$Lang['medical']['message']['AlertMessage']['FillMessage'] = '請填入內容';
$Lang['medical']['message']['ReturnMessage']['Delete']['Success'] = '1|=|信息刪除成功。';
$Lang['medical']['message']['ReturnMessage']['Delete']['Failed'] = '0|=|信息刪除失敗。';

$Lang['medical']['message']['setting']['title'] = '名稱';
$Lang['medical']['message']['setting']['message'] = '內容';
$Lang['medical']['message']['setting']['recordStatus'] = '狀況';
$Lang['medical']['message']['setting']['StatusOption']['InUse'] = '使用中';
$Lang['medical']['message']['setting']['StatusOption']['Suspend'] = '暫停使用';

$Lang['medical']['message_setting']['ReturnMessage']['Add']['Success'] = '1|=|預設提示設定新增成功。';
$Lang['medical']['message_setting']['ReturnMessage']['Add']['Failed'] = '0|=|預設提示設定新增失敗。';
$Lang['medical']['message_setting']['ReturnMessage']['Edit']['Success'] = '1|=|預設提示設定編輯成功。';
$Lang['medical']['message_setting']['ReturnMessage']['Edit']['Failed'] = '0|=|預設提示設定編輯失敗。';
$Lang['medical']['message_setting']['ReturnMessage']['Delete']['Success'] = '1|=|預設提示設定刪除成功。';
$Lang['medical']['message_setting']['ReturnMessage']['Delete']['Failed'] = '0|=|預設提示設定刪除失敗。';
$Lang['medical']['message_setting']['ReturnMessage']['Activate']['Success'] = '1|=|預設提示設定啓用成功。';
$Lang['medical']['message_setting']['ReturnMessage']['Activate']['Failed'] = '0|=|預設提示設定啓用失敗。';
$Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Success'] = '1|=|預設提示設定暫停成功。';
$Lang['medical']['message_setting']['ReturnMessage']['Suspend']['Failed'] = '0|=|預設提示設定暫停失敗。';

$Lang['medical']['message']['thickbox']['showOnlyCanReceiveUser'] = '<font color="red">*</font>此列表只會顯示可接收訊息的用戶。';

// meal option
$Lang['medical']['meal']['searchMenu']['date'] = '日期'; 
//$Lang['medical']['meal']['searchMenu']['timePeriod'] = '時段'; 
$Lang['medical']['meal']['searchMenu']['form'] = '班別'; 
$Lang['medical']['meal']['search']['ClassAll'] = '全部'; 
$Lang['medical']['meal']['searchMenu']['group'] = '小組';
$Lang['medical']['meal']['searchMenu']['sleep'] = '宿/走';
$Lang['medical']['meal']['search']['sleepOption'][] = $Lang['medical']['General']['All'];
$Lang['medical']['meal']['search']['sleepOption'][] = '宿生';
$Lang['medical']['meal']['search']['sleepOption'][] = '走讀生';
$Lang['medical']['meal']['searchMenu']['gender'] = '性別';
$Lang['medical']['meal']['searchMenu']['timePeriod'] = '時段'; 
$Lang['medical']['meal']['search']['timePeriodOption'][] = '早餐'; 
$Lang['medical']['meal']['search']['timePeriodOption'][] = '午餐'; 
$Lang['medical']['meal']['search']['timePeriodOption'][] = '晚餐';

$Lang['medical']['meal']['tableHeader']['Number'] = '#';
$Lang['medical']['meal']['tableHeader']['Add'] = '&nbsp;';
$Lang['medical']['meal']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['meal']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['meal']['tableHeader']['StayOrNot'] = '宿/走';
$Lang['medical']['meal']['tableHeader']['AttendanceStatus'] = '出席狀況';
$Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Present'] = '在校';
$Lang['medical']['meal']['tableHeaderOption']['AttendanceOption']['Absent'] = '缺席';
$Lang['medical']['meal']['tableHeader']['EatingCondition'] = '進食狀況';
$Lang['medical']['meal']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['meal']['tableHeader']['LastPersonConfirmed'] = '最後更新者';
$Lang['medical']['meal']['tableHeader']['LastUpdated'] = '最後更新時間';
$Lang['medical']['meal']['tableHeader']['Delete'] = '&nbsp;';

$Lang['medical']['meal']['AttendanceStatusExplain'] = '注意事項：<br />
	1) 早餐：學生昨天下午出席，本日早餐時段才可以輸入進食紀錄<br />
	2) 午餐：學生本日早上出席，本日午餐時段才可以輸入進食紀錄<br />
	3) 晚餐：學生本日下午出席，本日晚餐時段才可以輸入進食紀錄<br />
	<font color="red">**</font>) 學生出席紀錄己被修改為缺席';

$Lang['medical']['meal']['tableContent'][] = '編輯備註';

$Lang['medical']['meal']['button']['save'] = '儲存';
$Lang['medical']['meal']['button']['reset'] = '重設';
$Lang['medical']['meal']['button']['cancel'] = '取消';
$Lang['medical']['meal']['button']['search'] = '檢視';

// meal report
$Lang['medical']['report_meal']['report1'] = '進食報告';
$Lang['medical']['report_meal']['report2'] = '進食紀錄';

$Lang['medical']['report_meal']['searchMenu']['date'] = '日期';
$Lang['medical']['report_meal']['searchMenu']['form'] = '班別';
$Lang['medical']['report_meal']['searchMenu']['sleep'] = '宿/走';
$Lang['medical']['report_meal']['search']['sleepOption'][] = $Lang['medical']['General']['All'];
$Lang['medical']['report_meal']['search']['sleepOption'][] = '宿生';
$Lang['medical']['report_meal']['search']['sleepOption'][] = '走讀生';

$Lang['medical']['report_meal']['sleepOption']['StayIn'] = '宿生';
$Lang['medical']['report_meal']['sleepOption']['StayOut'] = '走讀生';

$Lang['medical']['report_meal']['searchMenu']['gender'] = '性別';
$Lang['medical']['report_meal']['search']['genderOption']['all'] = $Lang['medical']['General']['All'];
$Lang['medical']['report_meal']['search']['genderOption']['M'] = '男';
$Lang['medical']['report_meal']['search']['genderOption']['F'] = '女';
$Lang['medical']['report_meal']['searchMenu']['timePeriod'] = '時段';
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = '全部';
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = '早餐'; 
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = '午餐'; 
$Lang['medical']['report_meal']['search']['timePeriodOption'][] = '晚餐';
$Lang['medical']['report_meal']['searchMenu']['studentName'] = '學生姓名';
$Lang['medical']['report_meal']['searchMenu']['item'] = '項目';
$Lang['medical']['report_meal']['search']['itemCheckbox']['all'] = $Lang['medical']['General']['All'];
$Lang['medical']['report_meal']['search']['itemCheckbox']['remarks'] = '備註';
$Lang['medical']['report_meal']['searchMenu']['reportType'] = '報告類別';
$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealReport'] = '進食報告';
$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealStatistics'] = '進食統計';
$Lang['medical']['report_meal']['search']['reportTypeSelect']['mealReport2'] = '進食紀綠'; 


$Lang['medical']['report_meal']['tableHeader']['name'] = '姓名';
$Lang['medical']['report_meal']['tableHeader']['totalDayOfPresent'] = '該時段之出席日數';
$Lang['medical']['report_meal']['tableHeader']['remarks'] = '備註';
$Lang['medical']['report_meal']['tableSubHeader']['noOfMeal'] = '餐數';
$Lang['medical']['report_meal']['tableSubHeader']['ratio'] = '比率';
$Lang['medical']['report_meal']['remarks'] = '出席日數只計算已進食之日子。';

$Lang['medical']['report_meal2']['tableHeader']['class'] = '班別';
$Lang['medical']['report_meal2']['tableHeader']['name'] = '姓名';
$Lang['medical']['report_meal2']['tableHeader']['sleep'] = '宿/走';
$Lang['medical']['report_meal2']['tableHeader']['timePeroid'] = '時段';

$Lang['medical']['report_meal']['btn']['print'] = '列印';
$Lang['medical']['report_meal']['btn']['export'] = '匯出';

$Lang['medical']['report_meal']['searchMonthLimited'] = '搜尋日期不能多於三個月';

// access right
$Lang['medical']['accessRightView']['userType'] = '用戶類別';
$Lang['medical']['accessRightView']['noOfUsers'] = '用戶數量';

$Lang['medical']['accessRight']['selectTitle']['setUserGroup']='用戶類別';
$Lang['medical']['accessRight']['selectTitle']['setUsers']='自訂用戶';

$Lang['medical']['accessRight']['header']['option']='- 選項 -';

$Lang['medical']['accessRight']['selectTitle']['MEAL']='進食紀錄權限';
$Lang['medical']['accessRight']['selectTitle']['BOWEL']='排便紀錄權限';
$Lang['medical']['accessRight']['selectTitle']['STUDENTLOG']='學生日誌權限';
$Lang['medical']['accessRight']['selectTitle']['SLEEP']='睡眠狀況權限';
$Lang['medical']['accessRight']['selectTitle']['NOTICE']='護理提示權限';
$Lang['medical']['accessRight']['selectTitle']['EVENT']='行為問題及事故紀錄權限';
$Lang['medical']['accessRight']['selectTitle']['CASE']='個案跟進權限';
$Lang['medical']['accessRight']['selectTitle']['AWARDSCHEME']='獎勵計劃權限';
$Lang['medical']['accessRight']['selectTitle']['DEFAULTREMARK']='預設備註權限';
$Lang['medical']['accessRight']['selectTitle']['REVISIT']='覆診紀錄權限';
if($sys_custom['medical']['MedicalReport'])
    $Lang['medical']['accessRight']['selectTitle']['REVISIT'] = '醫療紀錄權限';
$Lang['medical']['accessRight']['selectTitle']['STAFFEVENT']='教職員護理紀錄權限';
$Lang['medical']['accessRight']['selectTitle']['HANDOVER']='行政交更權限';

$Lang['medical']['accessRight']['Option']['DeleteSelfRecord'] = '只可刪除自己的紀錄';
$Lang['medical']['accessRight']['Option']['Management'] = '管理';
$Lang['medical']['accessRight']['Option']['Report'] = '報告';
$Lang['medical']['accessRight']['Option']['Settings'] = '設定';
$Lang['medical']['accessRight']['Option']['NursingHint'] = '護理提示';
$Lang['medical']['accessRight']['Option']['Send'] = '撰寫';
$Lang['medical']['accessRight']['Option']['Receive'] = '接收';
$Lang['medical']['accessRight']['Option']['View'] = '檢視';
$Lang['medical']['accessRight']['Option']['Add'] = '新增';
$Lang['medical']['accessRight']['Option']['Edit'] = '編輯';
$Lang['medical']['accessRight']['Option']['Delete'] = '刪除';
$Lang['medical']['accessRight']['Option']['Self'] = '自己';
$Lang['medical']['accessRight']['Option']['All'] = '全部';
$Lang['medical']['accessRight']['Option']['ViewAll'] = "檢視全部紀錄";
$Lang['medical']['accessRight']['Option']['Print'] = '列印';
$Lang['medical']['accessRight']['Option']['Export'] = '匯出';

// meal setting
$Lang['medical']['meal_setting']['button']['new_category'] = '新增類別';

$Lang['medical']['meal_setting']['tableHeader']['Category'] = '類別名稱';
$Lang['medical']['meal_setting']['tableHeader']['Code'] = '代號';
$Lang['medical']['meal_setting']['tableHeader']['Status'] = '狀況';
$Lang['medical']['meal_setting']['tableHeader']['Default'] = '成為預設';

$Lang['medical']['meal_setting']['tableSorting'] = '註:排序是根據代號而定';

$Lang['medical']['meal_setting']['Name'] = '名稱';
$Lang['medical']['meal_setting']['Code'] = '代號';
$Lang['medical']['meal_setting']['Status'] = '狀況';
$Lang['medical']['meal_setting']['Default'] = '成為預設';
$Lang['medical']['meal_setting']['StatusOption']['InUse'] = '使用中';
$Lang['medical']['meal_setting']['StatusOption']['Suspend'] = '暫停使用';

$Lang['medical']['meal_setting']['AlertMessage']['FillText'] = '請填上這些欄位';
$Lang['medical']['meal_setting']['AlertMessage']['SelectOption'] = '請選擇選項';
$Lang['medical']['meal_setting']['AlertMessage']['Activate'] = '你是否確定啓用所選項目?';


$Lang['medical']['meal_setting']['RemindMessage']['DefaultHasSet'] = '已經有預設';

$Lang['medical']['meal_setting']['ReturnMessage']['Add']['Success'] = '1|=|進食紀錄設定新增成功。';
$Lang['medical']['meal_setting']['ReturnMessage']['Add']['Failed'] = '0|=|進食紀錄設定新增失敗。';
$Lang['medical']['meal_setting']['ReturnMessage']['Edit']['Success'] = '1|=|進食紀錄設定編輯成功。';
$Lang['medical']['meal_setting']['ReturnMessage']['Edit']['Failed'] = '0|=|進食紀錄設定編輯失敗。';
$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Success'] = '1|=|進食紀錄設定刪除成功。';
$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['Failed'] = '0|=|進食紀錄設定刪除失敗。';
$Lang['medical']['meal_setting']['ReturnMessage']['Delete']['FailedWithReason'] = '0|=|因該進食紀錄已用,設定刪除失敗。';
$Lang['medical']['meal_setting']['ReturnMessage']['Activate']['Success'] = '1|=|進食紀錄設定啓用成功。';
$Lang['medical']['meal_setting']['ReturnMessage']['Activate']['Failed'] = '0|=|進食紀錄設定啓用失敗。';
$Lang['medical']['meal_setting']['ReturnMessage']['Suspend']['Success'] = '1|=|進食紀錄設定暫停成功。';
$Lang['medical']['meal_setting']['ReturnMessage']['Suspend']['Failed'] = '0|=|進食紀錄設定暫停失敗。';

$Lang['medical']['general_setting']['GeneralSetting'] = '一般設定';

$Lang['medical']['general']['button']['new_category'] = '新增類別';
$Lang['medical']['general']['tableHeader']['Category'] = '類別名稱';
$Lang['medical']['general']['tableHeader']['Default'] = '成為預設';
$Lang['medical']['general']['tableHeader']['Subcategory'] = '子類別名稱';

$Lang['medical']['general']['tableSorting'] = '註:排序是根據代號而定';

$Lang['medical']['general']['Default'] = '成為預設';
$Lang['medical']['general']['StatusOption']['InUse'] = '使用中';
$Lang['medical']['general']['StatusOption']['Suspend'] = '暫停使用';

$Lang['medical']['general']['AlertMessage']['FillText'] = '請填上這些欄位';
$Lang['medical']['general']['AlertMessage']['SelectOption'] = '請選擇選項';
$Lang['medical']['general']['AlertMessage']['Activate'] = '你是否確定啓用所選項目?';
$Lang['medical']['general']['AlertMessage']['MaxAttachmentError'] ='請檢查檔案大小。(檔案容量限制: 6MB)';
$Lang['medical']['general']['AlertMessage']['noAccessRight'] = '你沒有此部分的權限。';
$Lang['medical']['general']['AlertMessage']['NoLevel4SelectedError'] = '請勾選最少一個部位項目。';
$Lang['medical']['general']['Hint']['MaxAttachment'] = '每件事件最多只能上載六個附件。';
$Lang['medical']['general']['RemindMessage']['DefaultHasSet'] = '已經有預設';

$Lang['medical']['general']['ReturnMessage']['Delete']['FailedWithReason'] = '0|=|因該紀錄已用,設定刪除失敗';
$Lang['medical']['general']['ReturnMessage']['Activate']['Success'] = '1|=|啓用成功';
$Lang['medical']['general']['ReturnMessage']['Activate']['Failed'] = '0|=|啓用失敗';
$Lang['medical']['general']['ReturnMessage']['Suspend']['Success'] = '1|=|暫停成功';
$Lang['medical']['general']['ReturnMessage']['Suspend']['Failed'] = '0|=|暫停失敗';

// bowel setting


$Lang['medical']['report_studentBowel']['report1'] = '折線圖報告';
$Lang['medical']['report_studentBowel']['report1_thickBox']['title'] = '自行排便報告';

$Lang['medical']['report_studentBowel']['report2'] = '排便報告';
$Lang['medical']['report_studentBowel']['reportOption'] = '報告人數';
$Lang['medical']['report_studentBowel']['option']['single'] = '個人';
$Lang['medical']['report_studentBowel']['option']['group'] = '多人';

$Lang['medical']['report_studentBowel']['searchMenu']['timePeriodHint'] = '若要在折線圖中顯示斜線, 請輸入多於<!--Time-->分鐘的間隔時間';
$Lang['medical']['report_studentBowel']['timePeriodWrong'] = '時段必需選擇最少一小時';

$Lang['medical']['bowel_setting']['BarCode'] = '條碼編號';
$Lang['medical']['bowel']['BowelStatusBarCode'] = '排便類別條碼';
$Lang['medical']['bowel']['duplicateRecord']= '重覆記錄，不能儲存';
$Lang['medical']['bowel']['enter'] = '輸入';
$Lang['medical']['bowel']['InvalidBarCode'] = '找不到該學生條碼';
$Lang['medical']['bowel']['StudentBarCode'] = '學生條碼';
$Lang['medical']['bowel']['tab']['Bowel'] = '排便紀錄';
$Lang['medical']['bowel']['tab']['Barcode'] = '以條碼輸入';

$Lang['medical']['bowel']['tableHeader']['Number'] = '#';
$Lang['medical']['bowel']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['bowel']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['bowel']['tableHeader']['StayOrNot'] = '宿/走';
$Lang['medical']['bowel']['tableHeader']['AttendanceStatus'] = '出席狀況';
$Lang['medical']['bowel']['tableHeaderOption']['AttendanceOption']['Present'] = '在校';
$Lang['medical']['bowel']['tableHeaderOption']['AttendanceOption']['Absent'] = '缺席';
$Lang['medical']['bowel']['tableHeader']['BowelCondition'] = '排便類別';
$Lang['medical']['bowel']['tableHeader']['BowelTime'] = '排便時間';
$Lang['medical']['bowel']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['bowel']['tableHeader']['LastPersonConfirmed'] = '最後更新者';
$Lang['medical']['bowel']['tableHeader']['LastUpdated'] = '最後更新時間';
$Lang['medical']['bowel']['tableHeader']['del'] = '';
$Lang['medical']['bowel']['tableHeader']['Add'] = '';
$Lang['medical']['bowel']['tableHeader']['Detail'] = '詳細資料';
$Lang['medical']['bowel']['AttendanceAbsentAlert'] = '學生本日有回校，才可以輸入排便紀錄';
$Lang['medical']['bowel']['AttendanceInputExplain'] = '*) 學生本日有回校，才可以輸入排便紀錄';
$Lang['medical']['bowel']['AttendanceStatusExplain'] = '注意事項：<br />
*) 學生上午或下午出席，出席狀況顯示為在校';
$Lang['medical']['bowel']['bowelStatusBarcodeNotFound'] = '排便類別條碼錯誤';
$Lang['medical']['bowel']['studentBarcodeNotFound'] = '學生條碼錯誤';
$Lang['medical']['bowel']['warning']['SelectBowelStatus'] = '請選擇排便類別';
$Lang['medical']['bowel']['warning']['InputBowelStatus'] = '請輸入排便類別條碼';
$Lang['medical']['bowel']['warning']['InputStudentBarcode'] = '請輸入學生條碼';
$Lang['medical']['bowel_setting']['AlertMessage']['Activate'] = '你是否確定啟用？';
$Lang['medical']['bowel_setting']['RemindMessage']['DefaultHasSet'] = '其它排便紀錄設定已設為預設';

$Lang['medical']['bowel_setting']['ReturnMessage']['Add']['Success'] = '1|=|排便紀錄設定新增成功。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Add']['Failed'] = '0|=|排便紀錄設定新增失敗。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Edit']['Success'] = '1|=|排便紀錄設定編輯成功。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Edit']['Failed'] = '0|=|排便紀錄設定編輯失敗。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Delete']['Success'] = '1|=|排便紀錄設定刪除成功。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Delete']['Failed'] = '0|=|排便紀錄設定刪除失敗。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Delete']['FailedWithReason'] = '0|=|因該排便紀錄已用,設定刪除失敗。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Activate']['Success'] = '1|=|排便紀錄設定啓用成功。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Activate']['Failed'] = '0|=|排便紀錄設定啓用失敗。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Suspend']['Success'] = '1|=|排便紀錄設定暫停成功。';
$Lang['medical']['bowel_setting']['ReturnMessage']['Suspend']['Failed'] = '0|=|排便紀錄設定暫停失敗。';

$Lang['medical']['bowel']['importRemarks']['barCode'] = '學生條碼 / 排便類別條碼';
$Lang['medical']['bowel']['importRemarks']['nullColumn'] = '(可留空白)';
$Lang['medical']['bowel']['importRemarks']['recordDateTime'] = '排便日期及時間 (YY/MM/DD HH:MM)';

$Lang['medical']['bowel']['import']['invalidBarCode'] = '學生編號錯誤';
$Lang['medical']['bowel']['import']['invalidAttendance'] = '學生沒有回校';
$Lang['medical']['bowel']['import']['invalidRecordBarCode'] = '排便類別錯誤';
$Lang['medical']['bowel']['import']['invalidDateTime'] = '排便日期及時間錯誤';
$Lang['medical']['bowel']['import']['duplicateRecord'] = '相同檔案內含有重覆記錄';
$Lang['medical']['bowel']['import']['duplicateRecordBelow'] = '以下資料重覆記錄，不會被輸入';

$Lang['medical']['studentBowel']['importRemarks']['StudentBarcode'] = '學生條碼';
$Lang['medical']['studentBowel']['importRemarks']['BowelBarcode'] = '排便類別條碼';
$Lang['medical']['studentBowel']['importRemarks']['RecordDate'] = '排便日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)';
$Lang['medical']['studentBowel']['importRemarks']['RecordTime'] = '排便時間 (HH:MM)';
$Lang['medical']['studentBowel']['importTableHeader']['RecordDate'] = '排便日期';
$Lang['medical']['studentBowel']['importTableHeader']['RecordTime'] = '排便時間';
$Lang['medical']['studentBowel']['importError']['barcodeNotExist'] = '學生條碼錯誤';
$Lang['medical']['studentBowel']['importError']['bowelBarcodeNotExist'] = '排便類別條碼錯誤';
$Lang['medical']['studentBowel']['importError']['duplicateRecord'] = '檔案內含記錄與現有記錄重覆';

$Lang['medical']['general']['BowelData'] = '排便記錄數據';

$Lang['medical']['bowel']['importRemarksTable']['studentID'] = '學生條碼';
$Lang['medical']['bowel']['importRemarksTable']['studentName'] = '姓名';
$Lang['medical']['bowel']['importRemarksTable']['class'] = '班別';
$Lang['medical']['bowel']['importRemarksTable']['classNo'] = '班號';
$Lang['medical']['bowel']['importRemarksTable']['bowelName'] = '排便類別';
$Lang['medical']['bowel']['importRemarksTable']['bowelDateTime'] = '系統儲存日期及時間';
$Lang['medical']['bowel']['importRemarksTable']['bowelDateTimeOriginal'] = '排便日期及時間';


$Lang['medical']['report_bowel']['tableHeader']['timeVerusPeople'] = '時段/人數';
$Lang['medical']['report_bowel']['noStudentSelected'] = '請選擇最少一位學生。';
$Lang['medical']['report_bowel']['noReasonSelected'] = '請選擇最少一個項目。';
$Lang['medical']['report_bowel']['wrongDatePeriod'] = '結束日期不能早於開始日期';
$Lang['medical']['report_bowel']['wrongTimePeriod'] = '結束時段不能早於開始時段';

// studentlog 
$Lang['medical']['studentLog']['button']['saveAll'] = '全部儲存';
$Lang['medical']['studentLog']['button']['back'] = '返回';
$Lang['medical']['studentlog_setting']['tableHeader']['ItemQty'] = '項目數量';
$Lang['medical']['studentlog_setting']['convulsionRemark'] = '抽搐部位及症狀';
$Lang['medical']['studentlog_setting']['convulsion']['syndrome'] = '症狀';

$Lang['medical']['studentLog']['className'] = '班別';
$Lang['medical']['studentLog']['classNumber'] = '班號';
$Lang['medical']['studentLog']['eventDate'] = '事件日期';
$Lang['medical']['studentLog']['time'] = '時間';
$Lang['medical']['studentLog']['studentLog'] = '日誌類別';
$Lang['medical']['studentLog']['case'] = '項目';
$Lang['medical']['studentLog']['bodyParts'] = '部位';
$Lang['medical']['studentLog']['bodyParts1'] = '部位1_代號';
$Lang['medical']['studentLog']['bodyParts2'] = '部位2_代號';
$Lang['medical']['studentLog']['pic'] = '負責人';

$Lang['medical']['studentLog']['button']['importBtn'] = '匯入數據';
$Lang['medical']['studentLog']['eventInfo'] = '- 事件資訊 -';

$Lang['medical']['studentLog']['behaviour'] = '行為';
$Lang['medical']['studentLog']['behaviour1'] = '日誌類別代號';
$Lang['medical']['studentLog']['behaviour2'] = '項目代號';
$Lang['medical']['studentLog']['timelasted'] = '持續時間';

$Lang['medical']['studentLog']['addEvent'] = '新增事件';
$Lang['medical']['studentLog']['addBodyParts'] = '新增部位';
$Lang['medical']['studentLog']['addAttachment'] = '新增附件';
$Lang['medical']['studentLog']['attachmentRemarks'] = '(每個檔案容量上限為6MB; 附件數量上限為6個)';

$Lang['medical']['studentLog']['deleteEvent'] = '刪除事件';

$Lang['medical']['studentLog']['hour'] = '小時';
$Lang['medical']['studentLog']['minute'] = '分';
$Lang['medical']['studentLog']['second'] = '秒';

$Lang['medical']['studentLog']['report_convulsion']['reportName'] = '學生抽搐紀錄表';
$Lang['medical']['studentLog']['report_convulsion']['reportName2'] = '學生日誌';
$Lang['medical']['studentLog']['LinkRelevantEvent'] = '聯繫相關行為問題及事故紀錄';
$Lang['medical']['studentLog']['newEventDesc']='新增項目';
$Lang['medical']['studentLog']['oldEventDesc']='已存項目';
$Lang['medical']['studentLog']['RelevantEvent'] = '相關行為問題及事故紀錄';
$Lang['medical']['studentLog']['RemoveStudentLog'] = '刪除相關學生日誌';

$Lang['medical']['studentLog']['importRemarks']['className'] = '班別';
$Lang['medical']['studentLog']['importRemarks']['classNum'] = '班號';
$Lang['medical']['studentLog']['importRemarks']['date'] = '日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)';
$Lang['medical']['studentLog']['importRemarks']['time'] = '時間 (HH:MM)';
$Lang['medical']['studentLog']['importRemarks']['behaviour1'] = '日誌類別代號';
$Lang['medical']['studentLog']['importRemarks']['behaviour2'] = '項目代號';
$Lang['medical']['studentLog']['importRemarks']['parts'] = '部位代號';
$Lang['medical']['studentLog']['importRemarks']['duration'] = '持續時間 (MM:SS)';
$Lang['medical']['studentLog']['importRemarks']['remarks'] = '備註';
$Lang['medical']['studentLog']['importRemarks']['pic'] = '負責人';

$Lang['medical']['studentLog']['importTableHeader']['date'] = '日期';
$Lang['medical']['studentLog']['importTableHeader']['time'] = '時間';
$Lang['medical']['studentLog']['importTableHeader']['duration'] = '持續時間';

$Lang['medical']['studentLog']['import']['dataChecking'] = '數據檢查';
$Lang['medical']['studentLog']['import']['reason'] = '錯誤備註';
$Lang['medical']['studentLog']['import']['invalidClassName'] = '班名錯誤';
$Lang['medical']['studentLog']['import']['invalidClassNum'] = '班號錯誤';
$Lang['medical']['studentLog']['import']['invalidDate'] = '日期錯誤';
$Lang['medical']['studentLog']['import']['invalidTime'] = '時間錯誤';
$Lang['medical']['studentLog']['import']['emptyBehaviour1'] = '日誌類別代號空白';
$Lang['medical']['studentLog']['import']['invalidBehaviour1'] = '日誌類別代號錯誤';
$Lang['medical']['studentLog']['import']['emptyBehaviour2'] = '項目代號空白';
$Lang['medical']['studentLog']['import']['invalidBehaviour2'] = '項目代號錯誤';
$Lang['medical']['studentLog']['import']['emptyParts'] = '部位代號空白';
$Lang['medical']['studentLog']['import']['invalidParts'] = '部位代號錯誤';
$Lang['medical']['studentLog']['import']['invalidDuration'] = '持續時間錯誤';
$Lang['medical']['studentLog']['import']['emptyPIC'] = '負責人空白';
$Lang['medical']['studentLog']['import']['invalidPIC'] = '負責人錯誤';

$Lang['medical']['studentLog']['addItemNoLev2'] = '項目沒有記錄';


$Lang['medical']['studentLog']['tableHeader']['Item'] = '項目名稱';

$Lang['medical']['studentLog']['AttendanceStatusExplain'] = '注意事項：<br />
*) 學生上午或下午出席，出席狀況顯示為在校';

$Lang['medical']['studentLog']['AttendanceInputExplain'] = '*) 學生本日有回校，才可以輸入學生日誌';

$Lang['medical']['studentLog_general']['deleteEvent'] = '你決定要刪除事件嗎?';
$Lang['medical']['studentLog']['email']['subject'] = '學生日誌更改通知';
$Lang['medical']['studentLog']['email2pic'] = '發送電郵給負責人';
$Lang['medical']['studentLog']['hasDuplicateRecord'] = '含有重覆記錄，不能儲存';
$Lang['medical']['studentLog']['duplicateRecord'] = '此記錄已重覆';
// student sleep
$Lang['medical']['sleep']['searchMenu']['date'] = '日期'; 
$Lang['medical']['sleep']['searchMenu']['form'] = '班別'; 
$Lang['medical']['sleep']['search']['ClassAll'] = '全部'; 
$Lang['medical']['sleep']['searchMenu']['group'] = '小組'; 
$Lang['medical']['sleep']['searchMenu']['sleep'] = '宿/走';
$Lang['medical']['sleep']['search']['sleepOption'][] = $Lang['medical']['General']['All'];
$Lang['medical']['sleep']['search']['sleepOption'][] = '宿生';
$Lang['medical']['sleep']['search']['sleepOption'][] = '走讀生';
$Lang['medical']['sleep']['searchMenu']['gender'] = '性別'; 
$Lang['medical']['sleep']['search']['genderOption'][] = '全部'; 
$Lang['medical']['sleep']['search']['genderOption'][] = '男'; 
$Lang['medical']['sleep']['search']['genderOption'][] = '女';
$Lang['medical']['sleep']['toNextDay'] = '&nbsp;至&nbsp;';

$Lang['medical']['sleep']['tableHeader']['Number'] = '#';
$Lang['medical']['sleep']['tableHeader']['NewItem'] = '';
$Lang['medical']['sleep']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['sleep']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['sleep']['tableHeader']['StayOrNot'] = '宿/走';
$Lang['medical']['sleep']['tableHeader']['AttendanceStatus'] = '出席狀況';
$Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Present'] = '在校';
$Lang['medical']['sleep']['tableHeaderOption']['AttendanceOption']['Absent'] = '缺席';
$Lang['medical']['sleep']['tableHeader']['SleepCondition'] = '睡眠狀況';
$Lang['medical']['sleep']['tableHeader']['Reason'] = '原因';
$Lang['medical']['sleep']['tableHeader']['Frequency'] = '次數';
$Lang['medical']['sleep']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['sleep']['tableHeader']['inputBy'] = '新增者';
$Lang['medical']['sleep']['tableHeader']['LastPersonConfirmed'] = '最後更新者';
$Lang['medical']['sleep']['tableHeader']['LastUpdated'] = '最後更新時間';
$Lang['medical']['sleep']['tableHeader']['DelItem'] = '';

$Lang['medical']['sleep']['AttendanceStatusExplain'] = '注意事項：<br />
*) 學生本日下午出席，才可以輸入睡眠紀錄<br />
<font color="red">**</font>) 學生出席紀錄己被修改為缺席';

$Lang['medical']['sleep_setting']['tableHeader']['DefaultLev2'] = '預設項目';

$Lang['medical']['report_sleep']['tableHeader']['stayForDays'] = '該時段之留宿日數';
$Lang['medical']['report_sleep']['tableHeader']['normal'] = '正常睡眠日數';
$Lang['medical']['report_sleep']['tableHeader']['abnormal'] = '不正常睡眠日數';
$Lang['medical']['report_sleep']['tableHeader']['sleepWell'] = '整晚安睡';
$Lang['medical']['report_sleep']['tableHeader']['healthProblem'] = '健康問題';
$Lang['medical']['report_sleep']['tableHeader']['emotionProblem'] = '情緒行為問題';
$Lang['medical']['report_sleep']['tableHeader']['otherProblem'] = '其他問題';
$Lang['medical']['report_sleep']['tableContent']['frequency'] = '次';

$Lang['medical']['report_sleep']['reminder1']= '註:睡眠報告不會計算補充字樣的項目';
$Lang['medical']['report_sleep']['reminder2']= '註:睡眠統計只會計算包含整晚安睡字樣的項目為正常睡眠，包含健康問題、情緒行為問題、其他問題的項目為不正常睡眠';
$Lang['medical']['report_sleep']['reminder3']= '註:睡眠統計只會計算包含整晚安睡字樣的項目為正常睡眠，包含健康問題、情緒行為問題、其他問題的項目為不正常睡眠';

$Lang['medical']['studentsleep_setting']['tableHeader']['ItemQty'] = '原因數量';

$Lang['medical']['studentSleep']['importRemarks']['className'] = '班別';
$Lang['medical']['studentSleep']['importRemarks']['classNum'] = '班號';
$Lang['medical']['studentSleep']['importRemarks']['date'] = '日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)';
$Lang['medical']['studentSleep']['importRemarks']['SleepCondition'] = '睡眠狀況代號';
$Lang['medical']['studentSleep']['importRemarks']['reason'] = '原因代號';
$Lang['medical']['studentSleep']['importRemarks']['frequency'] = '次數';
$Lang['medical']['studentSleep']['importRemarks']['remarks'] = '備註';

$Lang['medical']['studentSleep']['import']['invalidAttendance'] = '學生下午沒有回校';
$Lang['medical']['studentSleep']['import']['invalidStatus'] = '睡眠狀況代號錯誤';
$Lang['medical']['studentSleep']['import']['invalidReason'] = '原因代號錯誤';
$Lang['medical']['studentSleep']['import']['invalidFrequency'] = '次數錯誤';

$Lang['medical']['studentSleep']['tableHeader']['Reason'] = '原因名稱';

##### Problem Record START #####
$Lang['medical']['problemRecord']['searchMenu']['date'] = '日期';
$Lang['medical']['problemRecord']['searchMenu']['type'] = '類型';


$Lang['medical']['problemRecord']['sleep']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Date'] = '日期';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['SleepCondition'] = '睡眠狀況';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Reason'] = '原因';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Frequency'] = '次數';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['LastPersonConfirmed'] = '最後更新者';
$Lang['medical']['problemRecord']['sleep']['tableHeader']['LastUpdated'] = '最後更新時間';

$Lang['medical']['problemRecord']['meal']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['meal']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['problemRecord']['meal']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['problemRecord']['meal']['tableHeader']['Date'] = '日期';
$Lang['medical']['problemRecord']['meal']['tableHeader']['TimePeriod'] = '時段';
$Lang['medical']['problemRecord']['meal']['tableHeader']['EatingCondition'] = '進食狀況';
$Lang['medical']['problemRecord']['meal']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['problemRecord']['meal']['tableHeader']['LastPersonConfirmed'] = '最後更新者';
$Lang['medical']['problemRecord']['meal']['tableHeader']['LastUpdated'] = '最後更新時間';

$Lang['medical']['problemRecord']['bowel']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['Date'] = '排便日期';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['BowelCondition'] = '排便類別';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['Time'] = '排便時間';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['LastPersonConfirmed'] = '最後更新者';
$Lang['medical']['problemRecord']['bowel']['tableHeader']['LastUpdated'] = '最後更新時間';

$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Number'] = '#';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Date'] = '日期';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Time'] = '時間';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['StudentLogCondition'] = '日誌類別';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Item'] = '項目';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['LastPersonConfirmed'] = '最後更新者';
$Lang['medical']['problemRecord']['studentLog']['tableHeader']['LastUpdated'] = '最後更新時間';

// report
$Lang['medical']['report_general']['searchMenu']['date'] = '日期';
$Lang['medical']['report_general']['searchMenu']['form'] = '班別';
$Lang['medical']['report_general']['searchMenu']['sleep'] = '宿/走';
$Lang['medical']['report_general']['searchMenu']['gender'] = '性別';
$Lang['medical']['report_general']['searchMenu']['studentName'] = '學生姓名';
$Lang['medical']['report_general']['searchMenu']['item'] = '項目';

$Lang['medical']['report_general']['search']['itemCheckbox']['remarks'] = '備註';
$Lang['medical']['report_general']['search']['genderOption']['all'] = $Lang['medical']['General']['All'];
$Lang['medical']['report_general']['search']['genderOption']['M'] = '男';
$Lang['medical']['report_general']['search']['genderOption']['F'] = '女';

$Lang['medical']['report_general']['deleteEvent'] = '你決定要刪除事件嗎?';
$Lang['medical']['report_general']['deleteFile'] = '你決定要刪除附件嗎?';
$Lang['medical']['report_general']['attachmentTitle'] = '附件列表';

$Lang['medical']['report_studentlog']['tableHeader']['date'] = '日期';
$Lang['medical']['report_studentlog']['tableHeader']['time'] = '時間';
$Lang['medical']['report_studentlog']['tableHeader']['duration'] = '持續時間';
$Lang['medical']['report_studentlog']['tableHeader']['remarks'] = '備註';
$Lang['medical']['report_studentlog']['subTotal'] = '合計';
$Lang['medical']['report_studentlog']['grandTotal'] = '總計';

$Lang['medical']['report_studentlog2']['tableHeader']['type'] = '類別';

$Lang['medical']['report_general']['tableHeader']['studentName'] = '姓名';
$Lang['medical']['report_general']['tableHeader']['time'] = '時間';
$Lang['medical']['report_general']['tableHeader']['item'] = '項目';
$Lang['medical']['report_general']['tableHeader']['remarks'] = '備註';
$Lang['medical']['report_general']['tableHeader']['lastUpdatedBy'] = '最後更新者';
$Lang['medical']['report_general']['tableHeader']['lastUpdatedOn'] = '最後更新時間';

$Lang['medical']['report_general']['PeriodStatus'][1] = '早';
$Lang['medical']['report_general']['PeriodStatus'][2] = '午';
$Lang['medical']['report_general']['PeriodStatus'][3] = '晚';

$Lang['medical']['ReturnMessage']['ImportPartiallySuccess'] = '1|=|部份紀錄匯入成功';

$Lang['medical']['report_studentLog']['reportTitle'] = '學生抽搐紀錄表';
$Lang['medical']['report_studentLog']['address'][] = '香港新界將軍澳安達臣道三○一號';
$Lang['medical']['report_studentLog']['address'][] = '電話: (852) 2703 1722　　傳真: (852) 2703 6320';
$Lang['medical']['report_studentLog']['address'][] = '電郵: info@sunnyside.edu.hk　網址: www.sunnyside.edu.hk';
$Lang['medical']['report_studentLog']['printPerson'] = '紀錄表列印';
$Lang['medical']['report_studentLog']['printDate'] = '列印日期';
$Lang['medical']['report_studentLog']['printStudent'] = '學生姓名';
$Lang['medical']['report_studentLog']['printRecordDate'] = '日期';

$Lang['medical']['report_studentSleep']['report1'] = '睡眠報告';
$Lang['medical']['report_studentSleep']['report2'] = '睡眠統計';
$Lang['medical']['report_studentSleep']['report3'] = '睡眠紀錄';

$Lang['medical']['general']['dateModified'] = '最後更新時間';
$Lang['medical']['general']['deleteCategoryContainsItem'] = '類別含有項目';
$Lang['medical']['general']['duplicateBarCode'] = '條碼不能重覆';
$Lang['medical']['general']['duplicateCategory'] = $Lang['medical']['general']['tableHeader']['Category'] . '不能重覆';
$Lang['medical']['general']['duplicateCode'] = $Lang['General']['Code'] . '不能重覆';
$Lang['medical']['general']['edit'] = '編輯紀錄';
$Lang['medical']['general']['form'] = '班別';
$Lang['medical']['general']['ImportData'] = '匯入數據';
$Lang['medical']['general']['inputBy'] = '新增者';
$Lang['medical']['general']['lastModifiedBy'] = '最後更新者';
$Lang['medical']['general']['new'] = '新增紀錄';
$Lang['medical']['general']['parent'] = '家長';
$Lang['medical']['general']['pic'] = '負責人';
$Lang['medical']['general']['report']['total'] = '共';
$Lang['medical']['general']['sleepOption']['StayIn'] = '宿生';
$Lang['medical']['general']['sleepOption']['StayOut'] = '走讀生';
$Lang['medical']['general']['sleepType'] = '宿/走';
$Lang['medical']['general']['student'] = '學生';
$Lang['medical']['general']['StudentLogData'] = '學生日誌數據';
$Lang['medical']['general']['StudentSleepData'] = '睡眠狀況記錄數據';
$Lang['medical']['general']['update'] = '更新';

$Lang['medical']['bowel']['parent']['month'] = '月份';
$Lang['medical']['bowel']['parent']['bowelInfo'] = '排便資訊';
$Lang['medical']['bowel']['parent']['bowelDate'] = '排便日期';
$Lang['medical']['bowel']['parent']['new'] = '新增是日排便紀錄';
$Lang['medical']['bowel']['parent']['duplicateRecord'] = '紀錄不能重覆';

$Lang['medical']['bowel']['parent']['tableHeader']['Number'] = '#';
$Lang['medical']['bowel']['parent']['tableHeader']['Date'] = '日期';
$Lang['medical']['bowel']['parent']['tableHeader']['AttendanceStatus'] = '出席狀況';
$Lang['medical']['bowel']['parent']['tableHeader']['BowelTime'] = '排便時間';
$Lang['medical']['bowel']['parent']['tableHeader']['BowelCondition'] = '排便類別';
$Lang['medical']['bowel']['parent']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['bowel']['parent']['tableHeader']['LastUpdatedBy'] = '最後更新者';
$Lang['medical']['bowel']['parent']['tableHeader']['LastUpdatedOn'] = '最後更新時間';

$Lang['medical']['bowel']['parent']['AttendanceOption']['Present'] = '在校';
$Lang['medical']['bowel']['parent']['AttendanceOption']['StayAtHome'] = '缺席在家';
$Lang['medical']['bowel']['parent']['AttendanceOption']['Absent'] = '缺席';


$Lang['medical']['studentLog']['parent']['month'] = '月份';
$Lang['medical']['studentLog']['parent']['bowelInfo'] = '日誌資訊';
$Lang['medical']['studentLog']['parent']['bowelDate'] = '日誌日期';

$Lang['medical']['studentLog']['parent']['tableHeader']['Number'] = '#';
$Lang['medical']['studentLog']['parent']['tableHeader']['Date'] = '日期';
$Lang['medical']['studentLog']['parent']['tableHeader']['AttendanceStatus'] = '出席狀況';
$Lang['medical']['studentLog']['parent']['tableHeader']['Time'] = '日誌時間';
$Lang['medical']['studentLog']['parent']['tableHeader']['LogCondition'] = '日誌類別';
$Lang['medical']['studentLog']['parent']['tableHeader']['Item'] = '項目';
$Lang['medical']['studentLog']['parent']['tableHeader']['Remarks'] = '備註';
$Lang['medical']['studentLog']['parent']['tableHeader']['LastUpdatedBy'] = '最後更新者';
$Lang['medical']['studentLog']['parent']['tableHeader']['LastUpdatedOn'] = '最後更新時間';

$Lang['medical']['general']['allStudent'] = ' - 所有學生 - ';
$Lang['medical']['general']['datFileFormat'] = '(.dat 檔案)';
$Lang['medical']['general']['displayMethod']['title'] = '顯示方式';
$Lang['medical']['general']['displayMethod']['orderBy']['title'] = '按時間順序: ';
$Lang['medical']['general']['displayMethod']['orderBy']['desc'] = '由近至遠';
$Lang['medical']['general']['displayMethod']['orderBy']['asc'] = '由遠至近';
$Lang['medical']['general']['displayMethod']['displayEmptyDate'] = '顯示沒有資料的日子';
$Lang['medical']['general']['displayMethod']['displayEmptyStudent'] = '顯示沒有資料的學生';
$Lang['medical']['general']['duplicateRecordBelow'] = '以下資料重覆記錄';
$Lang['medical']['general']['error']['ajax'] = "AJAX 錯誤!";
$Lang['medical']['general']['group'] = '組別';
$Lang['medical']['general']['ImportDATArr']['ImportStepArr']['SelectDATFile'] = "選擇DAT檔案";
$Lang['medical']['general']['ImportDATArr']['ImportStepArr']['DATConfirmation'] = "確定";
$Lang['medical']['general']['ImportDATArr']['ImportStepArr']['ImportResult'] = "匯入結果";
$Lang['medical']['general']['importHint']['clickToCheck'] = '按此查詢';
$Lang['medical']['general']['linkRelevantStaffEvent'] = '聯繫相關教職員護理紀錄';
$Lang['medical']['general']['linkRelevantStudentLog'] = '聯繫相關學生日誌';
$Lang['medical']['general']['relevantStaffEvent'] = '相關教職員護理紀錄';
$Lang['medical']['general']['relevantStudentLog'] = '相關學生日誌';
$Lang['medical']['general']['ReturnMessage']['NotAllowDeletingNonSelfRecord'] = "0|=|不允許刪除其他人輸入的紀錄";
$Lang['medical']['general']['SaveAllConfirm'] = '是否儲存所有己修改的紀錄？';
$Lang['medical']['general']['viewPic'] = '檢視負責人';
$Lang['medical']['general']['Warning']['NotAllowToDeleteNonSelfRecord'] = '不允許刪除其他人的紀錄';
$Lang['medical']['general']['WarnMsg']['PleaseSelectDAT'] = "請選擇資料檔(.dat)";
$Lang['medical']['general']['Warning']['StartDateGtEndDate'] = '開始日期不能早於結束日期';

$Lang['medical']['default_remarks_setting']['module'] = '模組';
$Lang['medical']['default_remarks_setting']['defaultRemarks'] = '預設備註';
$Lang['medical']['default_remarks_setting']['defaultRemarksHint'] = '每一行為一項預設備註。';
$Lang['medical']['default_remarks_setting']['ReturnMessage']['Update']['Success'] = '1|=|預設備註設定成功。';
$Lang['medical']['default_remarks_setting']['ReturnMessage']['Update']['Failed'] = '0|=|預設備註設定失敗。';

$Lang['medical']['group_filter_setting']['category'] = '類別';
$Lang['medical']['group_filter_setting']['deselectedCategory'] = '小組類別';
$Lang['medical']['group_filter_setting']['selectedCategory'] = '已選類別';
$Lang['medical']['group_filter_setting']['hint'] = '注意事項：<br />
*) 類別已被重新命名';
$Lang['medical']['group_filter_setting']['ReturnMessage']['Update']['Success'] = '1|=|小組類別設定成功。';
$Lang['medical']['group_filter_setting']['ReturnMessage']['Update']['Failed'] = '0|=|小組類別設定失敗。';

$Lang['medical']['App']['ChooseStudent'] = '選擇學生';
$Lang['medical']['App']['CopyDefaultRemarks'] = '複製預設備註';
$Lang['medical']['App']['Misc'] = '其他資料';
$Lang['medical']['App']['NoRecordFound'] = "找不到所需紀錄";
$Lang['medical']['App']['PleaseSearch'] = '請搜尋';
$Lang['medical']['App']['PleaseSearchStudent'] = '請搜尋學生';
$Lang['medical']['App']['PleaseSelect'] = "請選擇";
$Lang['medical']['App']['PleaseSelectSyndrome'] = '請選擇症狀';
$Lang['medical']['App']['Remark'] = '備註';
$Lang['medical']['App']['SearchPICsUsingName'] = '以姓名搜尋負責人';
$Lang['medical']['App']['SearchStudentUsingClass'] = '以班別學號搜尋 (E.G. 1A15)';
$Lang['medical']['App']['SearchStudentUsingName'] = '以學生姓名搜尋';
$Lang['medical']['App']['SelectedPICs'] = '已選擇負責人';
$Lang['medical']['App']['SelectedStudent'] = '已選擇學生';

$Lang['medical']['award']['AddSuggestion'] = '增加班本目標建議';
$Lang['medical']['award']['AllGroupPIC'] = ' - 所有小組管理者 - ';
$Lang['medical']['award']['AllStatus'] = ' - 所有狀況 - ';
$Lang['medical']['award']['ClassOrientedSuggestion'] = '班本目標建議';
$Lang['medical']['award']['EditPerformance'] = '編輯表現';
$Lang['medical']['award']['Group'] = '班本組別';
$Lang['medical']['award']['GroupPIC'] = '小組管理者';
$Lang['medical']['award']['performance']['tableHeader']['StudentName'] = '學生姓名';
$Lang['medical']['award']['Performance'] = '班本目標表現';
$Lang['medical']['award']['RemoveTarget'] = '刪除目標';
$Lang['medical']['award']['RemoveTargetSuggestion'] = '刪除班本目標建議';
$Lang['medical']['award']['Scheme'] = '計劃';
$Lang['medical']['award']['SchemeName'] = '名稱';
$Lang['medical']['award']['Score'] = '表現';
$Lang['medical']['award']['ShowAllDate'] = '顯示所有日期';
$Lang['medical']['award']['ShowRecentDate'] = '只顯示近期表現';
$Lang['medical']['award']['status']['Completed'] = '已完結';
$Lang['medical']['award']['status']['Processing'] = '進行中';
$Lang['medical']['award']['tab']['Scheme'] = '獎勵計劃';
$Lang['medical']['award']['tab']['Performance'] = '學生表現';
$Lang['medical']['award']['Target'] = '目標';
$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedDelete'] = '此變更會刪除現有表現紀錄,你確定要變更？';
$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedFillBack'] = '此變更需要補回較早的日期紀錄,你確定要變更？';
$Lang['medical']['award']['Warning']['ConfirmChangeDateNeedFillBackDelete'] = '此變更會刪除現有表現紀錄及需要補回較早的日期紀錄,你確定要變更？';
$Lang['medical']['award']['Warning']['ConfirmDeleteGroup'] = '你確定要刪除此班本組別？';
$Lang['medical']['award']['Warning']['ConfirmDeleteSchemeTarget'] = '你確定要刪除此班本目標建議？';
$Lang['medical']['award']['Warning']['ConfirmDeleteStudentTarget'] = '你確定要刪除此目標及表現(如有)？';
$Lang['medical']['award']['Warning']['InputInteger'] = '請輸入0~100的整數';
$Lang['medical']['award']['Warning']['InputSuggestion'] = '請輸入目標';
$Lang['medical']['award']['Warning']['PerformanceRecordExist'] = '由於已輸入了學生的班本目標表現,不能刪除紀錄';
$Lang['medical']['award']['Warning']['SelectGroup'] = '請選擇班本組別';
$Lang['medical']['award']['Warning']['SelectGroupPIC'] = '請選擇小組管理者';
$Lang['medical']['case']['AddMeetingMinute'] = '增加會議紀錄';
$Lang['medical']['case']['AllReporter'] = ' - 所有報告者 - ';
$Lang['medical']['case']['AllStatus'] = ' - 所有狀況 - ';
$Lang['medical']['case']['AllPIC'] = ' - 所有管理者 - ';
$Lang['medical']['case']['ClosedRemark'] = '結案備註';
$Lang['medical']['case']['Detail'] = '個案細目';
$Lang['medical']['case']['EditMinutes'] = '編輯會議紀錄';
$Lang['medical']['case']['LinkRelevantEvent'] = '聯繫相關事件';
$Lang['medical']['case']['meeting']['Date'] = '會議日期';
$Lang['medical']['case']['meeting']['Record'] = '紀錄';
$Lang['medical']['case']['Minutes'] = '會議紀錄';
$Lang['medical']['case']['NewMinutes'] = '新增會議紀錄';
$Lang['medical']['case']['RelevantEvent'] = '相關事件';
$Lang['medical']['case']['RemoveEvent'] = '刪除相關事件';
$Lang['medical']['case']['RemoveMinutes'] = '刪除會議紀錄';
$Lang['medical']['case']['SelectEvent'] = '選擇相關事件';
$Lang['medical']['case']['status']['Closed'] = '已結案';
$Lang['medical']['case']['status']['Processing'] = '進行中';
$Lang['medical']['case']['StudentName'] = '學生姓名';
$Lang['medical']['case']['tableHeader']['CaseNo'] = '個案編號';
$Lang['medical']['case']['tableHeader']['CasePIC'] = '個案管理者';
$Lang['medical']['case']['tableHeader']['StartDate'] = '開始日期';
$Lang['medical']['case']['tableHeader']['Status'] = '狀況';
$Lang['medical']['case']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['case']['tableHeader']['Summary'] = '個案簡述';
$Lang['medical']['case']['ViewEvent'] = '檢視相關事件';
$Lang['medical']['case']['Warning']['ConfirmDeleteCaseEvent'] = '你確定要刪除此相關事件？';
$Lang['medical']['case']['Warning']['ConfirmDeleteMeetingMinutes'] = '你確定要刪除此會議紀錄？';
$Lang['medical']['case']['Warning']['DuplicateEvent'] = '最少有一相關事件重複,請重新選擇';
$Lang['medical']['case']['Warning']['InputMeetingDate'] = '請輸入會議日期';
$Lang['medical']['case']['Warning']['InvalidDateEarly'] = '會議日期不能早於開始日期。';
$Lang['medical']['case']['Warning']['SelectCasePIC'] = '請選擇個案管理者';
$Lang['medical']['case']['Warning']['SelectStudent'] = '請選擇學生';
$Lang['medical']['event']['AffectedPerson'] = '對象/受影響者';
$Lang['medical']['event']['AllAffectedPerson'] = ' - 所有受影響者 - ';
$Lang['medical']['event']['AllCategory'] = ' - 所有類別 - ';
$Lang['medical']['event']['AllClass'] = ' - 所有班別 - ';
$Lang['medical']['event']['AllGroup'] = ' - 所有組別 - ';
$Lang['medical']['event']['AllStudent'] = ' - 所有學生 - ';
$Lang['medical']['event']['AllTeacher'] = ' - 所有教職員 - ';
$Lang['medical']['event']['Attachment'] = '附件';
$Lang['medical']['event']['CaseNo'] = '個案編號';
$Lang['medical']['event']['Cause'] = '前因';
$Lang['medical']['event']['EditFollowupAction'] = '編輯事後跟進';
$Lang['medical']['event']['EndTime'] = '事件結束時間';
$Lang['medical']['event']['EventDate'] = '日期';
$Lang['medical']['event']['EventType'] = '事件類別';
$Lang['medical']['event']['FollowupAction'] = '事後跟進';
$Lang['medical']['event']['FollowupActionInfo'] = '%s於%s跟進事件';
$Lang['medical']['event']['Handling'] = '處理';
$Lang['medical']['event']['HideFollowupAction'] = '隱藏事後跟進';
$Lang['medical']['event']['NewCase'] = '新增個案';
$Lang['medical']['event']['NewFollowup'] = '新增跟進情況';
$Lang['medical']['event']['NonAccountHolder'] = '非帳戶持有者,請以逗號分隔';
$Lang['medical']['event']['Other'] = '其他';
$Lang['medical']['event']['Place'] = '發生地點';
$Lang['medical']['event']['RelevantCase'] = '相關個案';
$Lang['medical']['event']['RelevantCaseNo'] = '不需';
$Lang['medical']['event']['RelevantCaseYes'] = '需要';
$Lang['medical']['event']['RemoveCase'] = '刪除相關個案';
$Lang['medical']['event']['RemoveStaffEvent'] = '刪除相關教職員護理紀錄連結';
$Lang['medical']['event']['ReturnMessage']['EventTypeNotSetup'] = "0|=|事件類別未設定";
$Lang['medical']['event']['SelectCase'] = '選擇個案';
$Lang['medical']['event']['SelectFile'] = '選取文件';
$Lang['medical']['event']['SelectStaffEvent'] = '選擇相關教職員護理紀錄';
$Lang['medical']['event']['SelectStudentLog'] = '選擇相關學生日誌';
$Lang['medical']['event']['ShowFollowupAction'] = '顯示事後跟進';
$Lang['medical']['event']['StartTime'] = '事件開始時間';
$Lang['medical']['event']['StudentResponse'] = '學生反應';
$Lang['medical']['event']['Summary'] = '行為簡述';
$Lang['medical']['event']['tableHeader']['AffectedPerson'] = '受影響者';
$Lang['medical']['event']['tableHeader']['ClassName'] = '班別';
$Lang['medical']['event']['tableHeader']['EndTime'] = '結束時間';
$Lang['medical']['event']['tableHeader']['EventDate'] = '日期';
$Lang['medical']['event']['tableHeader']['EventType'] = '事件類別';
$Lang['medical']['event']['tableHeader']['Place'] = '地點';
$Lang['medical']['event']['tableHeader']['ReportPerson'] = '原報告者';
$Lang['medical']['event']['tableHeader']['StayOrNot'] = '宿/走';
$Lang['medical']['event']['tableHeader']['StartTime'] = '開始時間';
$Lang['medical']['event']['tableHeader']['StudentName'] = '姓名';
$Lang['medical']['event']['ViewCase'] = '檢視個案';
$Lang['medical']['event']['ViewRelevantStaffEvent'] = '檢視相關教職員護理紀錄';
$Lang['medical']['event']['ViewStaffEvent'] = '檢視教職員護理紀錄';
$Lang['medical']['event']['ViewStudentLog'] = '檢視學生日誌';
$Lang['medical']['event']['Warning']['ConfirmDeleteEventCase'] = '你確定要刪除此相關個案的連結？';
$Lang['medical']['event']['Warning']['ConfirmDeleteStaffEvent'] = '你確定要刪除此相關教職員護理紀錄的連結？';
$Lang['medical']['event']['Warning']['ConfirmDeleteStudentLLog'] = '你確定要刪除此相關學生日誌的連結？';
$Lang['medical']['event']['Warning']['InputFollowup'] = '請輸入最少一項事後跟進情況';
$Lang['medical']['event']['Warning']['InputSummary'] = '請填上行為簡述';
$Lang['medical']['event']['Warning']['SelectCase'] = '請選擇相關個案';
$Lang['medical']['event']['Warning']['SelectEventType'] = '請選擇事件類別';
$Lang['medical']['event']['Warning']['SelectStudent'] = '請選擇學生';
$Lang['medical']['event']['Warning']['StartTimeGtEndTime'] = '開始時間不能早於結束時間';
$Lang['medical']['filter']['FilterDateRange'] = '設定時段';
$Lang['medical']['revisit']['AddDrug'] = '新增藥物';
$Lang['medical']['revisit']['AllDivision'] = ' - 所有專科 - ';
$Lang['medical']['revisit']['AllGroup'] = ' - 所有組別 - ';
$Lang['medical']['revisit']['AllHospital'] = ' - 所有醫院 - ';
$Lang['medical']['revisit']['CuringStatus'] = '治療情況';
$Lang['medical']['revisit']['Division'] = '專科';
$Lang['medical']['revisit']['Dosage'] = '劑量';
$Lang['medical']['revisit']['DosageUnit']['mg'] = 'mg';
$Lang['medical']['revisit']['DosageUnit']['ml'] = 'ml';
$Lang['medical']['revisit']['DosageUnit']['prn'] = 'prn';
$Lang['medical']['revisit']['Drug'] = '藥物';
$Lang['medical']['revisit']['Hospital'] = '醫院';
$Lang['medical']['revisit']['IsChangedDrug'] = '藥物更改';
$Lang['medical']['revisit']['LastChangedDrug'] = '最後的藥物更改內容';
$Lang['medical']['revisit']['RemoveDrug'] = '刪除藥物';
$Lang['medical']['revisit']['RevisitDate'] = '下次覆診日期';
$Lang['medical']['revisit']['RevisitDate2'] = '覆診日期';
$Lang['medical']['revisit']['ShowLastChangedDrug'] = '顯示最後的藥物更改內容';
$Lang['medical']['revisit']['StudentName'] = '學生姓名';
$Lang['medical']['revisit']['ThisRevisitInfo'] = '是次覆診資料';
$Lang['medical']['revisit']['Unit'] = '單位';
$Lang['medical']['revisit']['Warning']['InputDosage'] = '請填上數字';
$Lang['medical']['revisit']['Warning']['InputDrugName'] = '請填上藥物名稱';
$Lang['medical']['revisit']['Warning']['SelectUnit'] = '請選擇劑量單位';
$Lang['medical']['revisit']['Warning']['CuringStatusEmpty'] = '治療情況不能留空';
$Lang['medical']['revisit']['CannotEarlier'] = "不能早於";
$Lang['medical']['revisit']['Diagnosis'] = "診斷";
$Lang['medical']['revisit']['VisitDate'] = "求診/護理日期";
$Lang['medical']['revisit']['AccidentInjure'] = "意外/受傷";
$Lang['medical']['revisit']['DischargedAdmittion'] = "出入院";
$Lang['medical']['revisit']['unstatedDate'] = "待定";
$Lang['medical']['revisit']['VisitHospital'] = "求診之醫院/診所";
$Lang['medical']['revisit']['StatusNormal'] = "一般";
$Lang['medical']['revisit']['SelectAtLeastOneItem'] = "請至少選擇一個項目";
$Lang['medical']['staffEvent']['AllSubcategory'] = ' - 所有子類別 - ';
$Lang['medical']['staffEvent']['DaysOfInjuryLeave'] = '工傷假日數';
$Lang['medical']['staffEvent']['DaysOfSickLeave'] = '病假日數';
$Lang['medical']['staffEvent']['EndTime'] = '事件結束時間';
$Lang['medical']['staffEvent']['EventDate'] = '日期';
$Lang['medical']['staffEvent']['EventType'] = '事件類別';
$Lang['medical']['staffEvent']['HasReportedInjury'] = '已報工傷';
$Lang['medical']['staffEvent']['LinkRelevantEvent'] = '聯繫相關行為問題及事故紀錄';
$Lang['medical']['staffEvent']['Place'] = '發生地點';
$Lang['medical']['staffEvent']['RelevantEvent'] = '相關行為問題及事故紀錄';
$Lang['medical']['staffEvent']['RemoveEvent'] = '刪除相關事件';
$Lang['medical']['staffEvent']['remark'] = '備註';
$Lang['medical']['staffEvent']['ReturnMessage']['EventTypeNotSetup'] = "0|=|教職員事件類別未設定";
$Lang['medical']['staffEvent']['SelectEvent'] = '選擇相關事件';
$Lang['medical']['staffEvent']['setting']['subcategoryQty'] = '子類別數量';
$Lang['medical']['staffEvent']['StartTime'] = '事件開始時間';
$Lang['medical']['staffEvent']['StudentName'] = '學生姓名';
$Lang['medical']['staffEvent']['tableHeader']['EndTime'] = '結束時間';
$Lang['medical']['staffEvent']['tableHeader']['EventDate'] = '日期';
$Lang['medical']['staffEvent']['tableHeader']['EventType'] = '事件類別';
$Lang['medical']['staffEvent']['tableHeader']['Place'] = '地點';
$Lang['medical']['staffEvent']['tableHeader']['ReportPerson'] = '原報告者';
$Lang['medical']['staffEvent']['tableHeader']['StartTime'] = '開始時間';
$Lang['medical']['staffEvent']['tableHeader']['StaffName'] = '姓名';
$Lang['medical']['staffEvent']['ViewEvent'] = '檢視相關事件';
$Lang['medical']['staffEvent']['Warning']['ConfirmDeleteStudentEventRelStaff'] = '你確定要刪除此相關事件的連結？';
$Lang['medical']['staffEvent']['warning']['duplicateStudentLog'] = '最少有一相關學生日誌,請重新選擇';
$Lang['medical']['staffEvent']['Warning']['PleaseInputNumeric'] = '請輸入數字';
$Lang['medical']['staffEvent']['Warning']['SelectEventType'] = '請選擇事件類別';
$Lang['medical']['staffEvent']['Warning']['SelectStaff'] = '請選擇教職員';
$Lang['medical']['upload']['jsWaitAllFilesUploaded'] = "請等待直至所有檔案完成上傳。";
$Lang['medical']['upload']['OrDragAndDropFilesHere'] = "或者在此拖放檔案。";
$Lang['medical']['reportStaffEvent']['noStaffSelected'] = '請選擇最少一位教職員。';
$Lang['medical']['reportStaffEvent']['noStaffTypeSelected'] = '請選擇最少一項教職員種類。';
$Lang['medical']['reportStaffEvent']['StaffType'] = '教職員種類';
$Lang['medical']['reportStaffEvent']['TotalLeave'] = '總日數';

$Lang['medical']['handover']['record'] = '行政交更紀錄';
$Lang['medical']['handover']['item'] = '行政項目';
$Lang['medical']['handover']['studentName'] = '宿生姓名';
$Lang['medical']['handover']['pic'] = '負責人';
$Lang['medical']['handover']['itemName'] = '項目名稱';
$Lang['medical']['handover']['itemCode'] = '代號';
$Lang['medical']['handover']['itemStatus'] = '狀況';
$Lang['medical']['handover']['attachment'] = '附件';
$Lang['medical']['handover']['attachmentCount'] = "附件數量";
$Lang['medical']['handover']['nameInUse'] = "此名稱已被其他項目使用";
$Lang['medical']['handover']['noPICSelected'] = '請選擇最少一位負責人。';

if (is_file("$intranet_root/lang/cust/medical_lang.$intranet_session_language.customized.php"))
{
	include("$intranet_root/lang/cust/medical_lang.$intranet_session_language.customized.php");
}

?>