<?php
// Using:anna

// ### Menu START ####
$Lang['SDAS']['menu']['academic'] = "學術統計";
$Lang['SDAS']['menu']['AcademicProgress'] = "成績進度";
$Lang['SDAS']['menu']['ClassSubjectComparison'] = "班別科目比較";
$Lang['SDAS']['menu']['ClassPerformance'] = "班別表現";
$Lang['SDAS']['menu']['DseAnalysis'] = "文憑試成績分析";
$Lang['SDAS']['menu']['DsePrediction'] = "文憑試成績預測";
$Lang['SDAS']['menu']['DsePredictionEvaluation'] = "文憑試成績預測檢討";
$Lang['SDAS']['menu']['DsePredictionDegreeDiploma'] = "重點培育名單";
$Lang['SDAS']['menu']['PassingRateStats'] = "任教合格統計";
$Lang['SDAS']['menu']['StudentPerformanceTracking'] = "學生表現追蹤";
$Lang['SDAS']['menu']['SubjectImprovementStats'] = "學科進步統計";
$Lang['SDAS']['menu']['SubjectStats'] = "學科統計";
$Lang['SDAS']['menu']['FormStudentPerformance'] = "級別學生表現";
$Lang['SDAS']['menu']['StudentCrossYearPerformance'] = "學生跨年表現";
$Lang['SDAS']['menu']['TeachingImprovementStats'] = "任教進步統計";
$Lang['SDAS']['menu']['SubjectClassDistribution'] = "班別科目成績分佈";
$Lang['SDAS']['menu']['LearnAndTeach'] = "學與教";
$Lang['SDAS']['menu']['ValueAddedData'] = "	增值數據報告";
$Lang['SDAS']['menu']['ReportComparsion'] = "報告比較";
$Lang['SDAS']['menu']['customReport'] = "校本報告";
$Lang['SDAS']['menu']['ClassPromotionAssessment'] = "升班評估";
$Lang['SDAS']['menu']['FormPassingRate'] = "各級合格率";
$Lang['SDAS']['menu']['management'] = "管理";
$Lang['SDAS']['menu']['examMgmt'] = "資料匯入";
$Lang['SDAS']['menu']['CEES'] = "呈分至中央平台";
$Lang['SDAS']['menu']['settings'] = "設定";
$Lang['SDAS']['menu']['accessRight'] = "權限設定";
$Lang['SDAS']['menu']['percentile'] = "百分排名";
$Lang['SDAS']['menu']['SchBaseReportConfig'] = "校本報告";
$Lang['SDAS']['menu']['MonitoringGroupSetting'] = "觀察小組設定";
$Lang['SDAS']['menu']['PreS1Analysis'] = "Pre-S1 分析";

$Lang['SDAS']['toCEES']['RecordType'] = "紀錄類型";
$Lang['SDAS']['toCEES']['NotSubmit'] = "未呈交";
$Lang['SDAS']['toCEES']['NeedResubmit'] = "需重新呈交";
$Lang['SDAS']['toCEES']['Resubmit'] = "重新呈交";
$Lang['SDAS']['toCEES']['Endorsed'] = "校長已確認";
$Lang['SDAS']['toCEES']['VerifiedByCEES'] = "教育部已驗證";
$Lang['SDAS']['toCEES']['PleaseEndorseDSEfirst'] = "請先確認文憑試紀錄";
$Lang['SDAS']['toCEES']['confirmAppealNoData'] = "確認沒有覆核紀錄";
$Lang['SDAS']['toCEES']['PleaseConfirmAppealNoData'] = "請確認是否沒有覆核紀錄";
$Lang['SDAS']['toCEES']['PleaseSyncOnceMore'] = "與中央平台的資料未同步，<br>請在確認前再呈交一次。";
$Lang['SDAS']['toCEES']['Duration'] = "呈交時段";
$Lang['SDAS']['toCEES']['DataFile'] = "資料檔案";
$Lang['SDAS']['toCEES']['Status'] = "狀態";
$Lang['SDAS']['toCEES']['LastUpdated'] = "最後呈交";
$Lang['SDAS']['toCEES']['SchoolYear'] = "學年";
$Lang['SDAS']['toCEES']['Confirm'] = "確認呈交嗎 ?";
$Lang['SDAS']['toCEES']['Confirmed'] = "已確認 ";
$Lang['SDAS']['toCEES']['PrincipalConfirm'] = "文憑試資料確認";
$Lang['SDAS']['toCEES']['NoNeedConfirm'] = "沒有紀錄需要確認";
$Lang['SDAS']['toCEES']['LastConfirm'] = "最後確認日期";
$Lang['SDAS']['toCEES']['principalInstruction'] = "
請校長檢閱以下五個香港中學文憑試報告：<br>
<br>
一. <a href=\"index.php?t=academic.dse_stats.dse_distribution\">文憑試成績分佈</a><br>
二. <a href=\"index.php?t=academic.dse_stats.attendance\">文憑試應考人數統計</a><br>
三. <a href=\"index.php?t=academic.dse_stats.best_five_score\">最佳5科積分</a><br>
四. <a href=\"index.php?t=academic.dse_stats.university\">聯招學士課程最低入學要求數據</a><br>
五. <a href=\"index.php?t=academic.dse_stats.best_student\">最佳學生</a><br>
<br>
如報告無誤，請在此向東華三院教育部確認。如有發現任何問題，請聯絡你的同工。 感謝你的合作！ <br>
";
$Lang['SDAS']['CEES']['MonthlyReport'] = "每月報表";
$Lang['SDAS']['CEES']['MonthlyReportNotification']['Title'] = "每月報表呈交提示";
$Lang['SDAS']['CEES']['MonthlyReportNotification']['Content'] = "請呈交上月之每月報表至中央平台。<br> 你可登入__WEBSITE__，然後前往「中央電子教育系統 (學校) > 管理 > 每月報表」處理。<br>";

$Lang['SDAS']['DSE']['exportAll'] = "匯出全部紀錄 (格式2)";
$Lang['SDAS']['DSE']['ImportError']['grade'] = "錯誤的等級";
// # DSE analysis START ####
$Lang['SDAS']['DSEstat']['ClassName'] = "班別";
$Lang['SDAS']['DSEstat']['ClassNumber'] = "學號";
$Lang['SDAS']['DSEstat']['DSELevel']['Title'] = "DSE等級";
$Lang['SDAS']['DSEstat']['DSELevel']['X'] = "缺席";
$Lang['SDAS']['DSEstat']['DSELevel']['U'] = "U等";
$Lang['SDAS']['DSEstat']['DSELevel']['1'] = "1等";
$Lang['SDAS']['DSEstat']['DSELevel']['2'] = "2等";
$Lang['SDAS']['DSEstat']['DSELevel']['3'] = "3等";
$Lang['SDAS']['DSEstat']['DSELevel']['4'] = "4等";
$Lang['SDAS']['DSEstat']['DSELevel']['5'] = "5等";
$Lang['SDAS']['DSEstat']['DSELevel']['5*'] = "5*等";
$Lang['SDAS']['DSEstat']['DSELevel']['5**'] = "5**等";
$Lang['SDAS']['DSEstat']['orAbove'] = "或以上";
$Lang['SDAS']['DSEstat']['NumApplied'] = "報考人數";
$Lang['SDAS']['DSEstat']['NumSat'] = "應考人數";
$Lang['SDAS']['DSEstat']['form']['4'] = "中四";
$Lang['SDAS']['DSEstat']['form']['5'] = "中五";
$Lang['SDAS']['DSEstat']['form']['6'] = "中六";
$Lang['SDAS']['DSEstat']['Mock'] = "模擬試成績";
$Lang['SDAS']['DSEstat']['Int'] = "校內成績";
$Lang['SDAS']['DSEstat']['Alert']['NoData'] = "沒有資料可以進行分析";
$Lang['SDAS']['DSEstat']['barChartTitle'] = "中學文憑試成績分佈";
$Lang['SDAS']['DSEstat']['box1'] = "中學文憑試  vs 模擬試成績";
$Lang['SDAS']['DSEstat']['box2'] = "中學文憑試  vs \"FormName\"校內成績";
$Lang['SDAS']['DSEstat']['boxplot_remarks'] = "*以上數字已四捨五入至最接近的整數";
$Lang['SDAS']['DSEstat']['box_y'] = "分數";
$Lang['SDAS']['DSEstat']['bar_y'] = "學生人數";
$Lang['SDAS']['DSEstat']['percent'] = "百份比 (%)";
$Lang['SDAS']['DSEstat']['number'] = "人數";
$Lang['SDAS']['DSEstat']['number2'] = "人次";
$Lang['SDAS']['DSEstat']['Totalnumber'] = "總人數";
$Lang['SDAS']['DSETopStudent']['Top5']['Title'][0] = "4個主修及2個選修成績為<!--score-->分或以上";
$Lang['SDAS']['DSETopStudent']['Top5']['Title'][1] = "沒有4個主修及2個選修成績為<!--score-->分或以上的學生";
$Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][0] = "最佳<!--subject-->科成績為<!--score-->分或以上";
$Lang['SDAS']['DSETopStudent']['PointOver24']['Title'][1] = "沒有<!--subject-->科成績為<!--score-->分或以上的學生";
$Lang['SDAS']['DSETopStudent']['Table'] = "列表";
$Lang['SDAS']['DSEUniversity']['Title'] = "聯招學士課程最低入學要求數據";
$Lang['SDAS']['DSEBestFive']['Title'] = "最佳5科積分";
$Lang['SDAS']['TitleNumber'] = "(人數)";
$Lang['SDAS']['TitleNumber2'] = "(人次)";
$Lang['SDAS']['TitlePercent'] = "(百份比)";
$Lang['SDAS']['ColumnNumber'] = "學生人數";
$Lang['SDAS']['ColumnPercent'] = "百份比 (%)";
$Lang['SDAS']['ColumnNumberShort2'] = "人次";
$Lang['SDAS']['ColumnNumberShort'] = "人數";
$Lang['SDAS']['ColumnPercentShort'] = "%";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['55'] = "主修科目在3322，並至少兩科選修科目為等級5或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['5'] = "主修科目在3322，並至少一科選修科目為等級5或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['44'] = "主修科目在3322，並至少兩科選修科目為等級4或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['4'] = "主修科目在3322，並至少一科選修科目為等級4或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['33'] = "主修科目在3322，並至少兩科選修科目為等級3或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['3'] = "主修科目在3322，並至少一科選修科目為等級3或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['coreCount'] = "主修科目在3322或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['22'] = "中國語文(3)英國語文(3)數學(2)通識(2)加兩科選修科目等級2等或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['2'] = "中國語文(3)英國語文(3)數學(2)通識(2)加一科選修科目等級2或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['core22222'] = "五科在等級2或以上，包括中國語文、英國語文、數學及通識";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['CE22222'] = "五科在等級2或以上，包括中國語文及英國語文";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['22222'] = "任何五科在等級2或以上";
$Lang['SDAS']['DSEUniversity']['HighChart']['Xaxis']['totalNum'] = "總數";
$Lang['SDAS']['DseDropEnterSatReport']['column'][0] = "應考人數 (模擬試)";
$Lang['SDAS']['DseDropEnterSatReport']['column'][1] = "報考人數 (文憑試)";
$Lang['SDAS']['DseDropEnterSatReport']['column'][2] = "應考人數 (文憑試)";
$Lang['SDAS']['DsePerformanceTrend']['Title'] = "表現趨勢";
$Lang['SDAS']['DseDistribution']['Title'] = "文憑試成績分佈 ";
$Lang['SDAS']['DseDistributionSubjectGroup']['Title'] = "文憑試成績分佈 (科組)";
$Lang['SDAS']['DseBestStudent']['Title'] = "最佳學生";
$Lang['SDAS']['DseAttendance']['Title'] = "文憑試應考人數統計";
$Lang['SDAS']['DseVsMock']['Title'] = "文憑試 vs 模擬試";
$Lang['SDAS']['DseVsGpa']['Title'] = "文憑試 vs 成績平均績點";
$Lang['SDAS']['DisplayBy'] = "顯示";
$Lang['SDAS']['SubjectNameDisplay'] = "科目名稱顯示";
$Lang['SDAS']['SubjectNameShortForm'] = "簡短名稱";
$Lang['SDAS']['SubjectNameFullName'] = "完整名稱";
$Lang['SDAS']['DisplayItem'] = "顯示項目";
$Lang['SDAS']['TotalScore'] = "總分";
// # DSE analysis END ####

// # DSE prediction START ####
$Lang['SDAS']['DSEprediction']['DseLevel'][1] = "1 級";
$Lang['SDAS']['DSEprediction']['DseLevel'][2] = "2 級";
$Lang['SDAS']['DSEprediction']['DseLevel'][3] = "3 級";
$Lang['SDAS']['DSEprediction']['DseLevel'][4] = "4 級";
$Lang['SDAS']['DSEprediction']['DseLevel'][5] = "5 級";
$Lang['SDAS']['DSEprediction']['DseLevel'][6] = "5* 級";
$Lang['SDAS']['DSEprediction']['DseLevel'][7] = "5** 級";
$Lang['SDAS']['DSEprediction']['DseLevelOrAbove'] = "或以上";
$Lang['SDAS']['DSEprediction']['FilterType']['UniversityRequire'] = "聯招學士課程入學要求";
$Lang['SDAS']['DSEprediction']['FilterType']['BestFive'] = "最佳五科總分";
$Lang['SDAS']['DSEprediction']['FilterType']['Subject'] = "個別科目";
$Lang['SDAS']['DSEprediction']['PredictMethod'] = "預測方法";
$Lang['SDAS']['DSEprediction']['Method']['Percentile'] = "百份排名";
$Lang['SDAS']['DSEprediction']['Method']['ConfidenceInterval'] = "置信區間 (95%)";
$Lang['SDAS']['DSEprediction']['Display'] = "顯示";
$Lang['SDAS']['DSEprediction']['Filter'] = "篩選條件";
$Lang['SDAS']['DSEprediction']['Meet'] = "達到";
$Lang['SDAS']['DSEprediction']['NotMeet'] = "無法達到";
$Lang['SDAS']['DSEprediction']['compulsory'] = "必修科要求";
$Lang['SDAS']['DSEprediction']['elective'] = "選修科要求";
$Lang['SDAS']['DSEprediction']['remarks'] = "請用逗號(,)分隔等級";
$Lang['SDAS']['DSEprediction']['SummaryTable'] = "預測結果總結";
$Lang['SDAS']['DSEprediction']['IndividualStudent'] = "個別學生";
$Lang['SDAS']['DSEprediction']['PercentileTable'] = "等級與百份排名關係";
$Lang['SDAS']['DSEprediction']['ConfidenceIntervalTable'] = "等級與置信區間關係";
$Lang['SDAS']['DSEprediction']['BestFiveTotal'] = "最佳五科總分";
$Lang['SDAS']['DSEprediction']['FirstLine'] = "第一行是預測DSE取得之等級";
$Lang['SDAS']['DSEprediction']['SecondLine'] = "第二行是<!--type-->";
$Lang['SDAS']['DSEprediction']['ThirdLine'] = "第三行是在<!--exam-->之百分排名";
$Lang['SDAS']['DSEprediction']['percentileRemark'] = "在<!--exam-->之百分排名";
$Lang['SDAS']['DSEprediction']['scoreRemark'] = "在<!--exam-->之分數";
$Lang['SDAS']['DSEprediction']['assessment'] = "校內考試";
$Lang['SDAS']['DSEprediction']['mock'] = "模擬試";
$Lang['SDAS']['DSEprediction']['remarks2'] = "等級與百份排名關係是由<!--Year-->學生之DSE表現所產生";
$Lang['SDAS']['DSEprediction']['remarksConfidenceInterval'] = "等級與置信區間關係是由<!--Year-->學生之DSE表現所產生";
$Lang['SDAS']['DSEprediction']['to'] = "至";
$Lang['SDAS']['DSEprediction']['customWeighting'] = "自訂成績比重";
$Lang['SDAS']['DSEprediction']['refYear'] = "參考學年";
$Lang['SDAS']['DSEprediction']['saveEvaluationReport'] = "儲存至成績預測檢討報告";
$Lang['SDAS']['DSEprediction']['saveEvaluationReportSuccess'] = "資料儲存成功 。";
$Lang['SDAS']['DSEprediction']['saveEvaluationReportUnsuccess'] = "資料儲存失敗 。";
$Lang['SDAS']['DSEprediction']['ConfirmOverwrite'] = "系統已存在資料，是否更新資料？";
$Lang['SDAS']['DSEprediction']['SelectRefYearWarning'] = "請選擇參考學年。";
// # DSE prediction END ####

// # DSE prediction evaluation START ####
$Lang['SDAS']['DSEpredictionEvaluation']['EvaluationReport'] = "成績預測檢討報告";
$Lang['SDAS']['DSEpredictionEvaluation']['AllSubject'] = "全部科目";
$Lang['SDAS']['DSEpredictionEvaluation']['PredictGrade'] = "成績預測";
$Lang['SDAS']['DSEpredictionEvaluation']['DseResult'] = "DSE結果";
$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiff'] = "結果預測相差";
$Lang['SDAS']['DSEpredictionEvaluation']['DsePredictDiffAbs'] = "結果預測相差絕對值";
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfPass'] = "合格人數";
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfPassSubject'] = "合格科目數";
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfCandidate'] = "應考人數";
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfSubject'] = "應考科目數";
$Lang['SDAS']['DSEpredictionEvaluation']['PassPercent'] = "合格比例";
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfLevel'] = "等級<!--Level--> 的人數";
$Lang['SDAS']['DSEpredictionEvaluation']['NumOfLevelStudent'] = "等級<!--Level--> 的數量";
$Lang['SDAS']['DSEpredictionEvaluation']['MeanError'] = "平均偏差";
$Lang['SDAS']['DSEpredictionEvaluation']['MeanAbsoluteError'] = "平均絕對偏差";
// # DSE prediction evaluation END ####

$Lang['SDAS']['Exam']['HKAT'] = "學科測驗";
$Lang['SDAS']['Exam']['HKDSE'] = "香港中學文憑試";
$Lang['SDAS']['Exam']['HKDSE_APPEAL'] = "香港中學文憑試(成績覆核)";
$Lang['SDAS']['Exam']['Mock'] = "模擬試成績";
$Lang['SDAS']['Exam']['FinalExam'] = "年終試成績";
$Lang['SDAS']['Exam']['SchoolExam'] = "校內考試紀錄";
$Lang['SDAS']['Exam']['OtherExam'] = "其他考試紀錄";
$Lang['SDAS']['Exam']['archivedStudent'] = "已離校學生";
$Lang['SDAS']['Exam']['ImportOtherExam'] = "匯入其他考試紀錄";
$Lang['SDAS']['Exam']['ExportOtherExam'] = "匯出其他考試紀錄";
$Lang['SDAS']['Exam']['EraseOtherExam'] = "刪除其他考試紀錄";
$Lang['SDAS']['Exam']['NoRecordToErase'] = "沒有紀錄可以刪除";
$Lang['SDAS']['Exam']['ContinueToErase'] = "繼續刪除其他紀錄";
$Lang['SDAS']['Exam']['ImportFileFormat']['Title'] = "檔案格式";
$Lang['SDAS']['Exam']['ImportFileFormat']['Default'] = "eClass 格式";
$Lang['SDAS']['Exam']['ImportFileFormat']['WebSAMS'] = "WebSAMS 格式";
$Lang['SDAS']['Exam']['ImportFileFormat']['HKEAA'] = "考評局格式";
$Lang['SDAS']['Exam']['Template'] = "範本";
$Lang['SDAS']['Exam']['PleaseSelectType'] = "請選擇紀錄類型。";
$Lang['SDAS']['Exam']['FormatRemarks']['HKEAA'] = "備註: 使用考評局格式需要學生的 STRN 或 身份証號碼資料";
$Lang['SDAS']['Exam']['FormatRemarks']['Regno'] = "備註: RegNo 編號應加上#（如 \"#0025136\")，以確保編號數字的完整";
$Lang['SDAS']['Exam']['Import']['InvalidRecords'] = "錯誤的紀錄";
$Lang['SDAS']['Exam']['Import']['MoreThanOneTerm'] = "檔案內存有多過一個學期/考試的資料";
$Lang['SDAS']['Exam']['Import']['Error'] = "發生錯誤 !";
$Lang['SDAS']['Exam']['Import']['MisMatchTerm'] = "檔案內標示了<b>T<!--termNoFromFile--></b>，但閣下選擇了<b><!--displayChoseTerm--></b>，學期應該為<b><!--displaySuggestionTermName--></b>嗎?";
$Lang['SDAS']['Exam']['Import']['ConfirmToContinue'] = "我的選擇正確，請繼續匯入。";
$Lang['SDAS']['Exam']['Import']['NoDseRecord'] = "目標學年未有文憑試資料 ，無法匯入成績覆核資料。";
$Lang['SDAS']['Exam']['Import']['NoRecordToUpdate'] = "沒有紀錄需要更新。";
$Lang['SDAS']['Exam']['Import']['AlreadyAppealNotAllowToImport'] = "已有成績覆核資料，不能再修改文憑試成績。";
$Lang['SDAS']['Exam']['Import']['EndorsedNotAllowToImport'] = "目標學年文憑試成績已確認 ，並呈送到中央電子教育系統，無法再更改資料。";
$Lang['SDAS']['Exam']['Import']['EndorsedNotAllowToImport_Appeal'] = "目標學年文憑試(覆核成績)已確認 ，並呈送到中央電子教育系統，無法再更改資料。";
$Lang['SDAS']['Exam']['Import']['NotAllowRemoveAsEndorse'] = "紀錄已確認並呈送到中央電子教育系統，不可以刪除有關紀錄。";
// ### Menu END ####

// ### Normal START ####
$Lang['SDAS']['Warning']['PleaseSelectSubject'] = "請選擇科目";
$Lang['SDAS']['Warning']['PleaseSelectForm'] = "請選擇級別";
$Lang['SDAS']['Warning']['PleaseSelectAcademicYear'] = "請選擇學年";
$Lang['SDAS']['Warning']['PleaseSelectTeacher'] = "請選擇教師";
// ### Normal END ####

// ### Class Performance START ####
$Lang['SDAS']['ClassPerformance']['IndividualYear'] = "按年度";
$Lang['SDAS']['ClassPerformance']['CrossYear'] = "跨年";
$Lang['SDAS']['ClassPerformance']['CrossTerm'] = "按學期";
// ### Class Performance END ####

// ### Student Performance Tracking START ####
$Lang['SDAS']['StudentPerformanceTracking']['Marksheet'] = "分表";
$Lang['SDAS']['StudentPerformanceTracking']['Percentile'] = "百分排名";
$Lang['SDAS']['StudentPerformanceTracking']['FormPercentile'] = "級排名百分位數";
// ### Student Performance Tracking END ####

// ### Teaching Improvement Stats START ####
$Lang['SDAS']['TeachingImprovementStats']['SelectDiffCompare'] = "請選擇不同的比較項目";
// ### Teaching Improvement Stats END ####

// ### AcademicProgress START ####
$Lang['SDAS']['AcademicProgress']['Selection'] = "顯示";
$Lang['SDAS']['AcademicProgress']['Sort'] = "排序";
$Lang['SDAS']['AcademicProgress']['Order'] = "次序";
$Lang['SDAS']['AcademicProgress']['Ordering']['Class'] = "班別學號";
$Lang['SDAS']['AcademicProgress']['Ordering']['ScoreDiff'] = "分數差別";
$Lang['SDAS']['AcademicProgress']['Ordering']['ZScoreDiff'] = "標準分差別";
$Lang['SDAS']['AcademicProgress']['Ordering']['Asc'] = "升序";
$Lang['SDAS']['AcademicProgress']['Ordering']['Dec'] = "降序";
$Lang['SDAS']['AcademicProgress']['Color'] = "分數差別高亮";
$Lang['SDAS']['AcademicProgress']['highlight']['red'] = "或以下用紅色顯示";
$Lang['SDAS']['AcademicProgress']['highlight']['blue'] = "或以上用藍色顯示";
$Lang['SDAS']['AcademicProgress']['HideShow'] = "點擊班別 顯示/隱藏";
$Lang['SDAS']['AcademicProgress']['PleaseEnterInt'] = "請輸入正整數";
$Lang['SDAS']['AcademicProgress']['PleaseSelectSubject'] = "請選擇科目";
$Lang['SDAS']['AcademicProgress']['PleaseSelect'] = "--請選擇--";
$Lang['SDAS']['AcademicProgress']['MonitoringGroup'] = "觀察小組";
// ### AcademicProgress END ####

// ### Subject Stat START ####
$Lang['SDAS']['SubjectStat']['SingleYear'] = "單學年";
$Lang['SDAS']['SubjectStat']['MultipleYear'] = "跨學年";
// ### Subject Stat END ####

// ### JUPAS START ####
$Lang['SDAS']['JUPASRecord'] = "大學聯招取錄結果";
$Lang['SDAS']['ImportJUPASRecord'] = "匯入大學聯招取錄結果";
$Lang['SDAS']['ExportJUPASRecord'] = "匯出大學聯招取錄結果";
$Lang['SDAS']['Error']['institution'] = "學院錯誤";
$Lang['SDAS']['Error']['funding'] = "資助類別錯誤";
$Lang['SDAS']['Error']['round'] = "階段類別錯誤";
$Lang['SDAS']['Error']['status'] = "狀態錯誤";
$Lang['SDAS']['Error']['noSubject'] = "沒有對應科目";
$Lang['SDAS']['Error']['JUPASempty'] = "聯招申請編號及聯招課程編號不能留空";
$Lang['SDAS']['Error']['JUPASinvalid'] = "聯招課程編號不正確";
// ### JUPAS END ####

#### Exit to START ###########
$Lang['SDAS']['ExitTo']['LeftYear'] = "離校學年";
$Lang['SDAS']['ExitToRecord'] = "畢業生出路";
$Lang['SDAS']['ExitTo']['FinalChoice'] = "出路選擇"; 
$Lang['SDAS']['ExitTo']['JUPAS'] = "JUPAS";
$Lang['SDAS']['ExitTo']['OtherInstitution'] = "其他學校";
$Lang['SDAS']['ExitTo']['Employment'] = "僱傭";
#### Exit to END ###########

$Lang['SDAS']['StackedPercentageColumn'] = "堆疊百分比列圖";
$Lang['SDAS']['StudentDistributionChart'] = "學生分布圖";
$Lang['SDAS']['SplineDiagram'] = "折線圖";
$Lang['SDAS']['Reverse'] = "倒轉";
$Lang['SDAS']['WholeForm'] = "全級";
$Lang['SDAS']['PositionInForm'] = "全級排名";
$Lang['SDAS']['PositionInFormDiff'] = "全級排名增減";
$Lang['SDAS']['Mode'] = "顯示模式";
$Lang['SDAS']['NoTeachingClass'] = "沒有任教班別";
$Lang['SDAS']['Option'] = "選項";
$Lang['SDAS']['ConvertTo100'] = "轉換至100";
$Lang['SDAS']['XAsixOrdering'] = "X軸排序";
$Lang['SDAS']['Descending'] = "降序";
$Lang['SDAS']['Ascending'] = "升序";
$Lang['SDAS']['SEN'] = "特殊學習需要";
$Lang['SDAS']['totalStudents'] = "學生總數";
$Lang['SDAS']['averageValue'] = "平均值";
$Lang['SDAS']['averageScore'] = "平均分";
$Lang['SDAS']['passPercent'] = "及格百分比";
$Lang['SDAS']['StudentName'] = "學生姓名";
$Lang['SDAS']['Class'] = "班別";
$Lang['SDAS']['ClassNumber'] = "班號";
$Lang['SDAS']['Subject'] = "科目";
$Lang['SDAS']['SubjectGroup'] = "科組";
$Lang['SDAS']['Amount'] = "人數";
$Lang['SDAS']['Score'] = "分數";
$Lang['SDAS']['Error']['noSubject'] = "沒有對應科目";
$Lang['SDAS']['SchoolYear'] = "學年";
$Lang['SDAS']['Term'] = "學期";
$Lang['SDAS']['JupasAnalysis'] = "大學聯招分析";
$Lang['SDAS']['JupasOfferStat'] = "大學聯招取錄結果統計";
$Lang['SDAS']['JupasVsDse'] = "大學聯招取錄結果 vs 文憑試 ";
$Lang['SDAS']['JupasVsDseAvg'] = "大學聯招取錄結果 vs 文憑試平均分";
$Lang['SDAS']['Jupas']['Result'] = "大學聯招取錄結果"; 
$Lang['SDAS']['Jupas']['University'] = "大學";
$Lang['SDAS']['Jupas']['Institution'] = "學院";
$Lang['SDAS']['Jupas']['ProgrammeName'] = "課程名稱";
$Lang['SDAS']['Jupas']['NumOffer'] = "取錄人數";
$Lang['SDAS']['Jupas']['Band'] = "Band";
$Lang['SDAS']['Jupas']['OfferRound'] = "取錄階段";
$Lang['SDAS']['Jupas']['JUPASNo'] = "大學聯招申請編號";
$Lang['SDAS']['Jupas']['Degree'] = "學士學位課程";
$Lang['SDAS']['Jupas']['SubDegree'] = "副學位課程";
$Lang['SDAS']['Jupas']['NoOffer'] = "沒有取錄";
$Lang['SDAS']['Jupas']['No'] = "人數";
$Lang['SDAS']['Jupas']['%'] = "百份比";
$Lang['SDAS']['Jupas']['InstitutionName']['BU'] = "浸會大學";
$Lang['SDAS']['Jupas']['InstitutionName']['CITY'] = "城市大學";
$Lang['SDAS']['Jupas']['InstitutionName']['CU'] = "中文大學";
$Lang['SDAS']['Jupas']['InstitutionName']['HKU'] = "香港大學";
$Lang['SDAS']['Jupas']['InstitutionName']['IED'] = "教育大學";
$Lang['SDAS']['Jupas']['InstitutionName']['LU'] = "嶺南大學";
$Lang['SDAS']['Jupas']['InstitutionName']['OU'] = "公開大學";
$Lang['SDAS']['Jupas']['InstitutionName']['PU'] = "理工大學";
$Lang['SDAS']['Jupas']['InstitutionName']['UST'] = "科技大學";
$Lang['SDAS']['Jupas']['InstitutionName']['SSSDP'] = "指定專業/界別課程資助計劃";
$Lang['SDAS']['Jupas']['RoundName']['SR'] = "Subsequent Round";
$Lang['SDAS']['Jupas']['RoundName']['MR'] = "Main Round";
$Lang['SDAS']['Jupas']['RoundName']['CR'] = "Clearing Round";
$Lang['SDAS']['Jupas']['JupasVsDseAvg']['Min'] = "最低";
$Lang['SDAS']['Jupas']['JupasVsDseAvg']['Avg'] = "平均";
$Lang['SDAS']['Jupas']['JupasVsDseAvg']['Max'] = "最高";
$Lang['SDAS']['Jupas']['AllInstitution'] = "全部學院";
$Lang['SDAS']['Jupas']['All'] = "總報名人數";
$Lang['SDAS']['Form_student_Performance']['PecentRankingInFormComparsion'] = "全級百分排名增減";
$Lang['SDAS']['Form_student_Performance']['Compare'] = "比較";
$Lang['SDAS']['Form_student_Performance']['Remarks'] = "<span class='red'>(&nbsp;)</span> 代表退步";
$Lang['SDAS']['Student_CrossYear_Result']['PreS1'] = "學科測驗  ";
$Lang['SDAS']['Student_CrossYear_Result']['MOCK_EXAM'] = "模擬試成績 ";
$Lang['SDAS']['Student_CrossYear_Result']['DSE'] = "香港中學文憑試";
$Lang['SDAS']['Student_CrossYear_Result']['overall_score_diff'] = "整體平均分差";
$Lang['SDAS']['DeletedUserLegend'] = "<font style='color:red;'>*</font> 表示該用戶已被刪除或已離校。";

$Lang['SDAS']['Promotion']['ClassPromotionAssessment'] = "升班評估";
$Lang['SDAS']['Promotion']['ClassPromotionAssessmentConfig'] = "升班評估設定";
$Lang['SDAS']['Promotion']['JuniorAndSeniorFormConfigration'] = "初中高中設定";
$Lang['SDAS']['Promotion']['Criteria'] = "標準";
$Lang['SDAS']['Promotion']['JuniorFormPromotionCriteria'] = "初中升班規則設定";
$Lang['SDAS']['Promotion']['SeniorFormPromotionCriteria'] = "高中升班規則設定";
$Lang['SDAS']['Promotion']['JuniorForm'] = "初中";
$Lang['SDAS']['Promotion']['SeniorForm'] = "高中";
$Lang['SDAS']['Promotion']['Repeat'] = "留班";
$Lang['SDAS']['Promotion']['TryPromote'] = "試升";
$Lang['SDAS']['Promotion']['Rule'] = "規則";
$Lang['SDAS']['Promotion']['Step1'] = "步驟 1";
$Lang['SDAS']['Promotion']['Step2'] = "步驟 2";
$Lang['SDAS']['Promotion']['Mode'] = "模式";
$Lang['SDAS']['Promotion']['SimpleScoreMode'] = "簡易模式";
$Lang['SDAS']['Promotion']['WeightedAverage'] = "加權平均分";
$Lang['SDAS']['Promotion']['CoreAndElectiveSubjectMode'] = "核心與選修科目模式";
$Lang['SDAS']['Promotion']['CoreSubject'] = "核心科目";
$Lang['SDAS']['Promotion']['ElectiveSubject'] = "選修科目";
$Lang['SDAS']['Promotion']['SubjectsThatIsNotCoreAreElectives'] = "核心科目之外的科目";
$Lang['SDAS']['Promotion']['WeightedAverageCEML'] = "加權平均數 (CEML)";
$Lang['SDAS']['Promotion']['Weighting'] = "比重";
$Lang['SDAS']['Promotion']['Chinese'] = "中文";
$Lang['SDAS']['Promotion']['English'] = "英文";
$Lang['SDAS']['Promotion']['Maths'] = "數學";
$Lang['SDAS']['Promotion']['LS'] = "通識";
$Lang['SDAS']['Promotion']['WarningEmpty'] = "不能為空白";
$Lang['SDAS']['Promotion']['FormOverLap'] = "初中高中不能重疊";
$Lang['SDAS']['Promotion']['Result']['Repeat'] = "留班";
$Lang['SDAS']['Promotion']['Result']['TryPromote'] = "試升";
$Lang['SDAS']['Promotion']['Result']['Pass'] = "合格";
$Lang['SDAS']['Promotion']['Stats'] = "統計";
$Lang['SDAS']['Promotion']['SubjectPerformance'] = "整體學科表現";
$Lang['SDAS']['Promotion']['SubjectStatsType']['average'] = "平均分";
$Lang['SDAS']['Promotion']['SubjectStatsType']['min'] = "最低分";
$Lang['SDAS']['Promotion']['SubjectStatsType']['percentile5'] = "最低5%";
$Lang['SDAS']['Promotion']['SubjectStatsType']['percentile10'] = "最低10%";
$Lang['SDAS']['Promotion']['SubjectStatsType']['percentile60'] = "最高40%";
$Lang['SDAS']['NoRecord']['A'] = "暫時沒有紀錄";
$Lang['SDAS']['FullMarkSetting']['RecalculateSDMEAN'] = "重新計算 標準分/ 合格率";
$Lang['SDAS']['FullMarkSetting']['FullMarkNoSet'] = "未有正確地設定學科滿分. 請前往: 設定>學科滿分, 正確地設定學科滿分";
$Lang['SDAS']['Import']['HeaderFormatError'] = "格式不正確";

$Lang['SDAS']['Form_Passing_Rate']['YearTerm']['YearTerm'] = "學期";
$Lang['SDAS']['Form_Passing_Rate']['Form'] = "級別";
$Lang['SDAS']['Form_Passing_Rate']['AcademicYear'] = "學年";
$Lang['SDAS']['Form_Passing_Rate']['GroupBy'] = "顯示模式";
$Lang['SDAS']['Form_Passing_Rate']['Subject'] = "科目";
$Lang['SDAS']['Form_Passing_Rate']['Class'] = "班別";
$Lang['SDAS']['Form_Passing_Rate']['WholeForm'] = "全級";
$Lang['SDAS']['Form_Passing_Rate']['WholeYear'] = "全年";
$Lang['SDAS']['Form_Passing_Rate']['Subject_Class'] = "科目\班別";
$Lang['SDAS']['Form_Passing_Rate']['AcademicYear_Form'] = "年度\年級";
$Lang['SDAS']['Form_Passing_Rate']['OverallResult'] = "整體成績";
$Lang['SDAS']['Form_Passing_Rate']['Warning']['PleaseSelectForm'] = "請選擇級別";
$Lang['SDAS']['Form_Passing_Rate']['Warning']['PleaseSelectAcademicYear'] = "請選擇學年";
$Lang['SDAS']['Form_Passing_Rate']['YearTerm']['YearTermNo'] = "學期 ##";
$Lang['SDAS']['Form_Passing_Rate']['Remark']['1'] = "註: 括號內之數字為合格人數";

// ### Learn and Teach START ####
$Lang['SDAS']['LearnAndTeach']['TeacherReport'] = "教師報告";
$Lang['SDAS']['LearnAndTeach']['SubjectReport'] = "科目報告";
$Lang['SDAS']['LearnAndTeach']['ScoreRemark'] = "非常同意(5)，同意(4)，中立(3)，不同意(2)，極不同意(1)";
$Lang['SDAS']['LearnAndTeach']['TotalAverage'] = "總平均值";
$Lang['SDAS']['LearnAndTeach']['OverallTeacherMin'] = "全體老師中的最小值";
$Lang['SDAS']['LearnAndTeach']['OverallTeacherAvg'] = "全體老師中的平均值";
$Lang['SDAS']['LearnAndTeach']['OverallTeacherMax'] = "全體老師中的最大值";
$Lang['SDAS']['LearnAndTeach']['OverallTeacherSD'] = "全體老師中的標準差";
$Lang['SDAS']['LearnAndTeach']['OverallSubjectMin'] = "本科中的最小值";
$Lang['SDAS']['LearnAndTeach']['OverallSubjectAvg'] = "本科中的平均值";
$Lang['SDAS']['LearnAndTeach']['OverallSubjectMax'] = "本科中的最大值";
$Lang['SDAS']['LearnAndTeach']['OverallSubjectSD'] = "本科中的標準差";
$Lang['SDAS']['LearnAndTeach']['ListByClass'] = "按班平均分";
$Lang['SDAS']['LearnAndTeach']['ListByStdAns'] = "按回答人數平均分";
// ### Learn and Teach END ####
$Lang['SDAS']['class_subject_performance_summary']['Subject'] = "科目";
$Lang['SDAS']['class_subject_performance_summary']['Score'] = "分數";
$Lang['SDAS']['class_subject_performance_summary']['FullMark'] = "滿分";
$Lang['SDAS']['class_subject_performance_summary']['ScoreStatistic'] = "<!-- Name -->的分數統計";
$Lang['SDAS']['class_subject_performance_summary']['convertTo100'] = "轉換成百分分數";
$Lang['SDAS']['class_subject_performance_summary']['convertTo100_Remarks'] = "如滿分並非100, 必須設定科目滿分";
$Lang['SDAS']['class_subject_performance_summary']['DataTable'] = "資料表格";
$Lang['SDAS']['class_subject_performance_summary']['DataChart'] = "資料圖表";
$Lang['SDAS']['class_subject_performance_summary']['DataTableInfo'] = "資料表格顯示內容";
$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['HighChartMode'] = "圖表";
$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['column'] = "棒形圖";
$Lang['SDAS']['class_subject_performance_summary']['HighChartMode']['box'] = "箱形圖";
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Min'] = "最低分";
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Q1'] = "下四分位數";
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Median'] = "中位數";
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Q3'] = "上四分位數";
$Lang['SDAS']['class_subject_performance_summary']['HighChartBox']['Max'] = "最高分";

$Lang['SDAS']['MonitoringGroupSetting']['MonitoringGroupSetting'] = "觀察小組設定";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameCH'] = "組名(中文)";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['GroupNameEN'] = "組名(英文)";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['DateModified'] = "最後修改時間";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['PIC'] = "負責人";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['AcademicYear'] = "所屬學年";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['Member'] = "成員";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['ChineseName'] = "中文姓名";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['EnglishName'] = "英文姓名";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['ClassName'] = "班別";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['ClassNumber'] = "班號";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['Copy'] = "複製";
$Lang['SDAS']['MonitoringGroupSetting']['TableHead']['CopyMember'] = "複製組員";
$Lang['SDAS']['MonitoringGroupSetting']['NoResult'] = "沒有紀錄";
$Lang['SDAS']['MonitoringGroupSetting']['NoMember'] = "沒有成員";
$Lang['SDAS']['MonitoringGroupSetting']['UpdateSuccess'] = "1|=|紀錄已經更新";
$Lang['SDAS']['MonitoringGroupSetting']['UpdateUnsuccess'] = "0|=|更新失敗";
$Lang['SDAS']['MonitoringGroupSetting']['EditGroup'] = "編輯小組";
$Lang['SDAS']['MonitoringGroupSetting']['NewGroup'] = "新增小組";
$Lang['SDAS']['MonitoringGroupSetting']['CopyGroup'] = "複製小組";
$Lang['SDAS']['MonitoringGroupSetting']['AddMember'] = "新增成員";
$Lang['SDAS']['MonitoringGroupSetting']['AllYear'] = "--所有學年--";
$Lang['SDAS']['MonitoringGroupSetting']['PleaseSelect'] = "--請選擇-";
$Lang['SDAS']['MonitoringGroupSetting']['GroupList'] = "小組列表";
$Lang['SDAS']['MonitoringGroupSetting']['CopyGroupToOtherYear'] = "複製小組至其他學年";
$Lang['SDAS']['MonitoringGroupSetting']['DeleteConfirm'] = "是否確定刪除?";
$Lang['SDAS']['MonitoringGroupSetting']['PleaseSelectToDelete'] = "請選擇要刪除的項目";
$Lang['SDAS']['MonitoringGroupSetting']['PleaseFillInGroupNameCH'] = "請輸入中文組名";
$Lang['SDAS']['MonitoringGroupSetting']['PleaseFillInGroupNameEN'] = "請輸入英文組名";
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['HaveSameGroup'] = "目標年度已有此組別";
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopySuccess'] = "複製成功";
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopySuccessWRecord'] = "成功複製<--Count-->紀錄";
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['CopyFail'] = "複製失敗";
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['Yes'] = "是";
$Lang['SDAS']['MonitoringGroupSetting']['CopyToOtherYear']['No'] = "否";

$Lang['SDAS']['FormStudentPerformance']['Remark']['PercentileLabel'] = "百分排名標記";
$Lang['SDAS']['FormStudentPerformance']['Remark']['RankingLabel'] = "排名增減標記";
$Lang['SDAS']['FormStudentPerformance']['Remark']['LightBlue'] = "<span style='background-color:#D2F6F8;'>&nbsp;&nbsp;&nbsp;&nbsp;</span>代表退步30名或以上";
$Lang['SDAS']['FormStudentPerformance']['Remark']['LightYellow'] = "<span style='background-color:#FEEE80;'>&nbsp;&nbsp;&nbsp;&nbsp;</span>代表進步20名或以上";
$Lang['SDAS']['FormStudentPerformance']['Remark']['Yellow'] = "<span style='background-color:yellow;'>&nbsp;&nbsp;&nbsp;&nbsp;</span>代表第一組百分排名";
$Lang['SDAS']['FormStudentPerformance']['Remark']['Orange'] = "<span style='background-color:orange;'>&nbsp;&nbsp;&nbsp;&nbsp;</span>代表最後一組百分排名";

// ### Value-added Data Report START ####
$Lang['SDAS']['ValueAddedData']['Tab']['ClassValueAdded'] = "各班增值指數";
$Lang['SDAS']['ValueAddedData']['Tab']['TeacherValueAdded'] = "各老師增值指數";
$Lang['SDAS']['ValueAddedData']['ValueAdded'] = "增值指數";
// ### Value-added Data Report END ####

// ### DSE vs GPA Report START ####
$Lang['SDAS']['DseVsGpa']['graphTitle'] = "文憑試 vs 成績平均績點";
$Lang['SDAS']['DseVsGpa']['gpaResult'] = "成績平均績點";
$Lang['SDAS']['DseVsGpa']['highChanceJupasOffer'] = "較高機會進入大學聯招";
$Lang['SDAS']['DseVsGpa']['dseBest5'] = "文憑試最佳五科 / 4C+2X";
$Lang['SDAS']['DseVsGpa']['jupasBaseLine'] = "大學聯招基線";
$Lang['SDAS']['DseVsGpa']['universityBaseLink'] = "HKU/UST/CUHK 基線";
$Lang['SDAS']['DseVsGpa']['unexpectedResult'] = "意外收獲";
$Lang['SDAS']['DseVsGpa']['slipped'] = "失手";
$Lang['SDAS']['DseVsGpa']['studentResult'] = "學生成績";
$Lang['SDAS']['DseVsGpa']['schoolResult'] = "學校成績";
$Lang['SDAS']['DseVsGpa']['dseResult'] = "文憑試成績";
// ### DSE vs GPA Report END ####

// ### Predict Degree-Diploma START ####
$Lang['SDAS']['PredictDegreeDiploma']['DseScoreAfterAdj'] = "調整後估計評級";
$Lang['SDAS']['PredictDegreeDiploma']['MeetRequirement'] = "符合資格";
$Lang['SDAS']['PredictDegreeDiploma']['ChiFocus'] = "中文焦點";
$Lang['SDAS']['PredictDegreeDiploma']['EngFocus'] = "英文焦點";
$Lang['SDAS']['PredictDegreeDiploma']['MathFocus'] = "數學焦點";
$Lang['SDAS']['PredictDegreeDiploma']['LsFocus'] = "通識焦點";
$Lang['SDAS']['PredictDegreeDiploma']['ChiEngFail'] = "中及英斷";
$Lang['SDAS']['PredictDegreeDiploma']['ElectiveFail'] = "選斷";
$Lang['SDAS']['PredictDegreeDiploma']['Degree'] = "學位";
$Lang['SDAS']['PredictDegreeDiploma']['HigherDiploma'] = "高級文憑";

$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Chi'] = "中";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Eng'] = "英";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Math'] = "數";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Ls'] = "通";
$Lang['SDAS']['PredictDegreeDiploma']['Subject']['Elective'] = "選";
// #### Predict Degree-Diploma END ####

##### pre s1 analysis report ###
$Lang['SDAS']['PreS1Analysis']['SubjectAverage'] = "科目平均分";



// ### Report Comparsion START ####
$Lang['SDAS']['ReportComparison']['Assessment'] = "評估";
$Lang['SDAS']['ReportComparison']['AssessmentAry']['CA'] = "持續性評估";
$Lang['SDAS']['ReportComparison']['AssessmentAry']['Exam'] = "考試";
$Lang['SDAS']['ReportComparison']['AssessmentAry']['YearGrade'] = "總成績";
$Lang['SDAS']['ReportComparison']['Compare'] = "比較";
$Lang['SDAS']['ReportComparison']['Percentile'] = "百份排名";
$Lang['SDAS']['ReportComparison']['Term1'] = "Term 1";
$Lang['SDAS']['ReportComparison']['Term2'] = "Term 2";

$Lang['SDAS']['ReportComparison']['TableHeader']['Class'] = "Class";
$Lang['SDAS']['ReportComparison']['TableHeader']['ClassNo'] = "Class No.";
$Lang['SDAS']['ReportComparison']['TableHeader']['EnglishName'] = "English Name";
$Lang['SDAS']['ReportComparison']['TableHeader']['ChineseName'] = "Chinses Name";
$Lang['SDAS']['ReportComparison']['TableHeader']['NickName'] = "Nickname";
$Lang['SDAS']['ReportComparison']['TableHeader']['Subject'] = "Subjects";
$Lang['SDAS']['ReportComparison']['TableHeader']['DataInYear'] = "<!--DataType--> of <!--AssessmentType--> in <!--Year-->";
$Lang['SDAS']['ReportComparison']['TableHeader']['ChangeInYear'] = "Change in <!--DataType--> of <!--AssessmentType-->";

$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectSubject'] = "請選擇科目";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectSubjectGroup'] = "請選擇科組";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelect2Years'] = "請選擇最少兩個學年";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectAssessment'] = "請選擇評估";
$Lang['SDAS']['ReportComparsion']['JSWarning']['PleaseSelectCompareTarget'] = "請選擇比較目標";
// #### Report Comparsion END ####

$Lang['SDAS']['Settings']['AccessRight']['PIC'] = "校長/負責人 (能檢視全部分析)";
$Lang['SDAS']['Settings']['AccessRight']['SubjectPanel'] = "科主任";
$Lang['SDAS']['Settings']['AccessRight']['MonthlyReport'] = "每月報表負責人";
$Lang['SDAS']['Settings']['AccessRight']['MonthlyReportSection'] = "每月報表細項負責人";

$Lang['SDAS']['Settings']['AccessGroup']['Title'] = "權限小組";
$Lang['SDAS']['Settings']['AccessGroup']['Name_ch'] = "小組名稱(中文)";
$Lang['SDAS']['Settings']['AccessGroup']['Name_en'] = "小組名稱(英文)";
$Lang['SDAS']['Settings']['AccessGroup']['Description'] = "簡介";
$Lang['SDAS']['Settings']['AccessGroup']['MemberCount'] = "成員數";
$Lang['SDAS']['Settings']['AccessGroup']['Member'] = "小組成員";

### Class Subject Comparison START ###

$Lang['SDAS']['ClassSubjectComparsion']['ClassWarning'] = "請選擇至少一個班別";
$Lang['SDAS']['ClassSubjectComparsion']['SubjectWarning'] = "請選擇至少一個科目";

### Class Subject Comparison END ###

### SSPA START ###
// $Lang['SDAS']['SSPA']['Title'] = "升中派位";
// $Lang['SDAS']['SSPA']['ParticipateAllocation'] = "參加升中派位";
// $Lang['SDAS']['SSPA']['PlacesEMI'] = "派往英文中學";
// $Lang['SDAS']['SSPA']['PlacesFirstChoice'] = "派獲第一志願";
// $Lang['SDAS']['SSPA']['PlacesFirsttoThirdChoice'] = "派獲第一至第三志願";
// $Lang['SDAS']['SSPA']['Net'] = "網內";
// $Lang['SDAS']['SSPA']['Territory'] = "區內";
// $Lang['SDAS']['SSPA']['TotalCount'] = "總人數";
// $Lang['SDAS']['SSPA']['Count'] = "人數";

### SSPA END ###