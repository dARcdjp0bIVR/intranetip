/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Need save as utf-8!!!!!!
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// global messages
var globalAlertMsg1 = "請選擇一個項目。";
var globalAlertMsg2 = "請最少選擇一個項目。";
var globalAlertMsg3 = "你是否確定要刪除項目?";
var globalAlertMsg4 = "你是否確定要批准?";
var globalAlertMsg5 = "你是否確定要暫停?";
var globalAlertMsg6 = "請選擇兩個項目。";
var globalAlertMsg7 = "你是否確定要轉換?";
var globalAlertMsg8 = "輸入新的資料夾名稱：";
var globalAlertMsg9 = "請輸入整數。";
var globalAlertMsg10 = "請輸入新名稱：";
var globalAlertMsg11 = "檔案名稱已存在。";
var globalAlertMsg12 = "你是否確定要解壓?";
var globalAlertMsg13 = "檔案並非 zip 檔案";
var globalAlertMsg14 = "請最少選擇一個項目。";
var globalAlertMsg15 = "請最少選擇一個項目。";
var globalAlertMsg16 = "你是否確定要登出?";
var globalAlertMsg17 = "請選擇種類組別。";
var globalAlertMsg18 = "請選擇一個項目。";
var globalAlertMsg19 = "輸入新的上課節數名稱：";
var globalAlertMsg20 = "結束時間與開始時間不正確。";
var globalAlertMsg21 = "你是否確定存檔?";
var globalAlertMsg25 = "檔案名稱無效。";
var globalAlertMsg26 = "你是否確定要公開?";
var globalAlertMsg27 = "你是否確定不公開?";
var globalAlertMsg28 = "你是否確定要將首頁設定為空白頁?";
var globalAlertMsg29 = "你是否確定要將首頁設定為普通頁?";
var globalAlertMsg30 = "你是否確定要將已選擇項目設定為直排?";
var globalAlertMsg31 = "你是否確定要將已選擇項目取消直排?";
var globalAlertMsg32 = "你是否確定要傳送?";
var globalAlertActivate = "你是否確定恢復使用?";
var NonIntegerWarning = "你必須輸入一個正整數。";
var globalAlertMsgRemoveAll = "你是否確定刪除所有項目(包括不顯示之項目)?";
var globalAlertMsgSendPassword = "以電郵把密碼傳送給使用者?";
var globalAlertMsgNewBatch = "請輸入新的名稱。";
var globalAlertMsgSetBatch = "你是否確定以此為資源預訂時間表?\n(所有使用原有資源預訂的資源將改為使用此時間表, 原有預訂紀錄將不受影響,\n但你需要檢查它們是否仍然有效。)";

var globalAlertMsgReject = "你是否確定要拒絕？拒絕項目會被刪除。";
var globalAlertMsgRejectNoDel = "你是否確定要拒絕?";

var globalAlertMsgEnable = "你是否確認要啟用所選項目?";
var globalAlertMsgDisable = "你是否確認要停用所選項目?";

var globalAlertMsgTransferToWebSAMS = "你是否確認要將紀錄傳送至網上校管系統?";
var globalClearMsgForUpdateStatus = "清除";

//var ForgotPasswordMsg = "1. 請輸入登入名稱，重啟密碼程序將寄送至閣下備用電郵。\n2. 如無法取得密碼，請聯絡學校的技術支援主任。";
var ForgotPasswordMsg = "1. 請輸入登入名稱，重啟密碼程序將寄送至閣下備用電郵。\n2. 如無法取得密碼，請聯絡學校或所屬機構查詢。";
var JSLang = Array();
JSLang['WarningFirefoxNotAllowPaste'] = '只有 IE 及 Firefox 15 或以前的版本支援貼上功能。如你使用 Firefox 15 或以前的版本而瀏覽器不允許使用貼上功能，請於網址欄輸人 "about:config"，然後將 "signed.applets.codebase_principal_support" 設置為 "true"。';
JSLang['Loading'] = '載入中...';
JSLang['Close'] = '關閉';	// used in thickbox.js
JSLang['OK'] = '確定';		// used in jquery.alerts.js
JSLang['Cancel'] = '取消';
JSLang['Alert'] = '通知';
JSLang['Confirm'] = '確定';
JSLang['Prompt'] = '提示';
JSLang['RefreshOverload'] = '惡意刷新行為已被記錄。請不要再次刷新同一頁面。';