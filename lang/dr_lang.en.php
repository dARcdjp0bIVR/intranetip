<?php 
// using:  
/*
 * Remarks:
 * 1) Arrange the lang variables in alphabetical order
 * 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
 * 3) Use single quote for the array keys and double quote for the lang value
 * 
 * Modification Log:
 * 
 * Date:	2019-11-15 (Sam)
 * 			Added labels for Delete Existing Routing
 */

// A
$Lang['DocRouting']['Action'] = "Actions";
$Lang['DocRouting']['Actions']['SIGN'] = "request to sign / confirm";
$Lang['DocRouting']['Actions']['COMMENT'] = "allow comments / remarks"; 
$Lang['DocRouting']['Actions']['ATTACHMENT'] = "allow attachments";
$Lang['DocRouting']['Actions']['ONLINEFORM'] = "provide online reply slip (user has to fill in if set)";
$Lang['DocRouting']['AddAdHocRoute'] = "Add ad hoc route";
$Lang['DocRouting']['AddMoreFollowUp'] = "Add next route";
$Lang['DocRouting']['ArchiveRoutings'] = "Archive Routings";
$Lang['DocRouting']['AddMoreFile'] = "Add more file";
$Lang['DocRouting']['AllowAdHocRoute'] = "Allow ad hoc route";
$Lang['DocRouting']['Active'] = "Active";
$Lang['DocRouting']['Attachment'] = "Attachment";
$Lang['DocRouting']['AddAttachment'] = "Add Attachment";
$Lang['DocRouting']['AddVoice'] = "Add Voice";
$Lang['DocRouting']['AddAdHocRouting'] = "Add ad hoc routing";
$Lang['DocRouting']['ActivateDocument'] = "Activate document";
$Lang['DocRouting']['ArchiveFilesToDigitalArchive'] = "Archive files to [ ".$Lang['Header']['Menu']['DigitalArchive']." ]";
$Lang['DocRouting']['Advanced'] = "Advanced";
$Lang['DocRouting']['AttachmentRemark'] = ' * Remark: Attachment cannot be changed once there is a follow-up done.';
$Lang['DocRouting']['ActionAndTime'] = "Action / Time";
$Lang['DocRouting']['Attached'] = "Attached";
$Lang['DocRouting']['ArchiveDocument'] = "Archive Document";
$Lang['DocRouting']['AuthenticationSetting'] = "Authenticate when first time access";
$Lang['DocRouting']['AllowAccessIP'] = "Access Right Computer IP";


// B
$Lang['DocRouting']['Button']['Back'] = "Back";
$Lang['DocRouting']['Button']['CopyExistingRounting'] = "Copy Existing Routing";
$Lang['DocRouting']['Button']['CopyFromExistingRounting'] = "Copy From Existing Routing";
$Lang['DocRouting']['Button']['DeleteExistingRounting'] = "Delete Existing Routing";

// C
$Lang['DocRouting']['CompletedRoutings'] = "Completed Routings";
$Lang['DocRouting']['CurrentRoutings'] = "Current Routings";
$Lang['DocRouting']['CompleteRouting'] = "Complete document";
$Lang['DocRouting']['Creator'] = "Creator";
$Lang['DocRouting']['CopyStatus']['Yes'] = "Yes";
$Lang['DocRouting']['CopyStatus']['No'] = "No";
$Lang['DocRouting']['CheckingRoutingStatus'] = "Checking routing status...";
$Lang['DocRouting']['ChooseFile'] = "Choose File";
$Lang['DocRouting']['CommentAndDocument'] = "Comment / Document";
$Lang['DocRouting']['Commented'] = "Commented";
$Lang['DocRouting']['CommentedAndAttached'] = "Commented and attached";
$Lang['DocRouting']['CanViewAllCommentsAndAttachments'] = "Can view all comments and attachments";

// D
$Lang['DocRouting']['Documents'] = "Document(s)";
$Lang['DocRouting']['DocumentNA'] = "N/A";
$Lang['DocRouting']['DocumentPhysical'] = "from a physical location";
$Lang['DocRouting']['DocumentUpload'] = "from upload";
$Lang['DocRouting']['DocumentDA'] = "from digital archive";
$Lang['DocRouting']['DraftRoutings'] = "Draft Routings";
$Lang['DocRouting']['Draft'] = "Draft";
$Lang['DocRouting']['DownloadAll'] = "Download all";
$Lang['DocRouting']['DraftCommentRemark'] = "Comment last drafted on <!--DATETIME-->.";
$Lang['DocRouting']['DeleteDocRoutings'] = "Delete routing(s)";
$Lang['DocRouting']['Deleted'] = "Deleted";
$Lang['DocRouting']['DeletedRoutings'] = "Deleted Routings";
$Lang['DocRouting']['DeletedBy'] = "Deleted by <!--USERNAME--> on <!--DATETIME-->";

// E
$Lang['DocRouting']['EditDocRouting'] = "Edit routing";
$Lang['DocRouting']['EmailNotification'] = "Email Notification";
$Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailTitle'] = "[".$Lang['Header']['Menu']['DocRouting']."] New feedback is received for \"<!--docTitle-->\"";
$Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailContentAry'][] = "New feedback is received for \"<!--docTitle-->\" in route <!--routeNum--> from <!--userName-->.";
$Lang['DocRouting']['EmailNotification_newFeedbackInRoute']['EmailContentAry'][] = "You may go to \"".$Lang['Header']['Menu']['eAdmin']." > ".$Lang['Header']['Menu']['ResourcesManagement']." > ".$Lang['Header']['Menu']['DocRouting']."\" to view the feedback.";
$Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailTitle'] = "[".$Lang['Header']['Menu']['DocRouting']."] New Routing \"<!--docTitle-->\"";
$Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailContentAry'][] = "You need to follow new routing \"<!--docTitle-->\".";
$Lang['DocRouting']['EmailNotification_newRouteReceived']['EmailContentAry'][] = "You may go to \"".$Lang['Header']['Menu']['eAdmin']." > ".$Lang['Header']['Menu']['ResourcesManagement']." > ".$Lang['Header']['Menu']['DocRouting']."\" to view the routing.";
$Lang['DocRouting']['EmailTarget'] = "Email Target";
$Lang['DocRouting']['ExportCSV'] = "Export to CSV";
$Lang['DocRouting']['EditRouting'] = "Edit routing";
$Lang['DocRouting']['ExistingRoutings'] = "Existing Routing(s)";
$Lang['DocRouting']['EndDate'] = "End Date Range";
$Lang['DocRouting']['EditRoutingPeriodAndSendEmail'] = "Edit Routing Period and Send Email";
$Lang['DocRouting']['EmailContent'] = "Email Content";
$Lang['DocRouting']['Email'] = "Email";
$Lang['DocRouting']['EmailNotification_RouteSigned']['EmailTitle'] = "Digital Routing Signed Alert [<!--DocTitle-->]";
$Lang['DocRouting']['EmailNotification_RouteSigned']['EmailContent'] = "Please noted that the Digital Routing titled \"<!--DocTitle-->\" signed at <!--SignedTime--> has been received.";
$Lang['DocRouting']['EmailSendArchivedDocument']['EmailTitle'] =  $Lang['Header']['Menu']['DocRouting']." - completed document [<!--DocTitle-->]";
$Lang['DocRouting']['EmailSendArchivedDocument']['EmailContent'] = "Please note that [<!--DocTitle-->] has been completed and the document is attached for your reference.";
$Lang['DocRouting']['EmailSendArchivedDocument']['EmailRemark'] = "Remark: Due to the large file size of the document, it is splitted into <!--NUMBER_OF_FILE--> parts and sent out separatedly. You can restore the document file with any zip program.";

// F
$Lang['DocRouting']['Files'] = "files";
$Lang['DocRouting']['File'] = "file";
$Lang['DocRouting']['FollowedRoutings'] = "Followed Routings";

// G


// H


// I
$Lang['DocRouting']['InProgress'] = "In progress";
$Lang['DocRouting']['Instruction'] = "Instruction";

// J


// K


// L
$Lang['DocRouting']['LockRoutingWhenGivingFeedback'] = "Lock routing when giving feedback";
$Lang['DocRouting']['LockFor'] = "Maximum lock for ";
$Lang['DocRouting']['LastSentTime'] = "Last sent time";
$Lang['DocRouting']['LastFollowedTime'] = "Last followed time";

// M
$Lang['DocRouting']['Management']['BeforeSigningRemind'] ="Before signing, you may";
//$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'] = "Please check for the document and work which you are get involved. Follow up them according to the instructions given by the senders on or before the due dates if set. The item(s) you should follow now are listed in red row(s).";
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'] = "Please check for the document and work which you are get involved. Follow up them according to the instructions given by the senders on or before the due dates if set. <br />";
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#F7D8D8;padding:2px;line-height:18px;">Red—your reply required item(s)</span><br />';
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#FFDBC1;padding:2px;line-height:18px;">Orange—overdue item(s)</span><br />';
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#FFFBCE;padding:2px;line-height:18px;">Yellow—pending for the colleague(s) reply unexpired item(s)</span><br />';
$Lang['DocRouting']['Management']['CurrentRoutings']['InstructionContent'].= '<span style="background-color:#E9F6E0;padding:2px;line-height:18px;">Green—already signed or replied item(s)</span>';
$Lang['DocRouting']['Management']['FillInReplySlip'] = "Please fill in below reply slip.";
$Lang['DocRouting']['Management']['Publish'] = "Publish";
$Lang['DocRouting']['Management']['PublishDate'] = "Published Date:";
$Lang['DocRouting']['Management']['DraftDate'] = "Drafted Date:";
//$Lang['DocRouting']['Management']['Status']['CreatedByYourself'] = "Created By Yourself";
//$Lang['DocRouting']['Management']['Status']['FollowedByYouNow'] = "Followed By You Now";
//$Lang['DocRouting']['Management']['Status']['RoutedByPeriod'] = "Routed By Period";
//$Lang['DocRouting']['Management']['Status']['StarRemark'] = "Already Remarked";

$Lang['DocRouting']['Management']['WriteYourComment'] = "write your comment";
$Lang['DocRouting']['Management']['EditYourComment'] = "edit your comment";


$Lang['DocRouting']['Management']['FilterStatus']['AllStarStatus'] ="All Star Status";
$Lang['DocRouting']['Management']['FilterStatus']['StarRemarked'] ="Star Remarked";
$Lang['DocRouting']['Management']['FilterStatus']['NotStarRemarked'] ="Not Star Remarked";
$Lang['DocRouting']['Management']['FilterStatus']['AllCreateStatus'] ="All Created Status";
$Lang['DocRouting']['Management']['FilterStatus']['CreatedByYou'] ="Created By You";
$Lang['DocRouting']['Management']['FilterStatus']['NotCreatedByYou'] ="Not Created By You";
$Lang['DocRouting']['Management']['FilterStatus']['AllFollowUpStatus'] ="All Follow Up Status";
//$Lang['DocRouting']['Management']['FilterStatus']['FollowedByYouNow'] ="Followed By You Now";
$Lang['DocRouting']['Management']['FilterStatus']['FollowedByYouNow'] ="Follow-up required item(s) (inc. those created by yourself)";
$Lang['DocRouting']['Management']['FilterStatus']['NotFollowedByYouNow'] ="Not Followed By You Now";
$Lang['DocRouting']['Management']['FilterStatus']['AllExpireStatus'] = "All Expire Status";
$Lang['DocRouting']['Management']['FilterStatus']['ExpiredButNotYetFinished'] = "Expired But Not Yet Finished";

$Lang['DocRouting']['Minutes'] = "minutes";

// N
$Lang['DocRouting']['NewDocRouting'] = "New routing";
$Lang['DocRouting']['NewDocRoutings'] = "New routing(s)";
$Lang['DocRouting']['NewDocRoutingStep1'] = "Input document(s)";
$Lang['DocRouting']['NewDocRoutingStep2'] = "Define routing(s)";
$Lang['DocRouting']['NewFeedbackReceivedInOwnRouting'] = "New feedback recevied in own created routing";
$Lang['DocRouting']['NewRouteReceived'] = "New routing received by users";
$Lang['DocRouting']['NotApplicableToFirstRoute'] = "not applicable to the first route";
$Lang['DocRouting']['Note'] = "Note";
$Lang['DocRouting']['NotViewedYet']= "Not viewed yet";
$Lang['DocRouting']['NotYetSigned'] = "Not yet signed";
$Lang['DocRouting']['NextRouting'] = "Next routing";

// O
//$Lang['DocRouting']['OptionalAction'] = "Optional Actions";
$Lang['DocRouting']['ONLINEFORM']['ReadByOthers'] = "Read by Others:";
$Lang['DocRouting']['ONLINEFORM']['ReleaseResultSettings'] = "Release result:";
$Lang['DocRouting']['ONLINEFORM']['ReleaseAnytime'] = "Anytime";
$Lang['DocRouting']['ONLINEFORM']['ReleaseNever'] = "Never";
$Lang['DocRouting']['ONLINEFORM']['ReleaseResultAfterSubmission'] = "After submission";
$Lang['DocRouting']['ONLINEFORM']['ReleaseResultAfterRouteCompeleted'] = "After route completed";
$Lang['DocRouting']['ONLINEFORM']['UserNameDisplaySettings'] = "User name display:";
$Lang['DocRouting']['ONLINEFORM']['DisplayUserName'] = "Display user name";
$Lang['DocRouting']['ONLINEFORM']['HideUserName'] = "Hide user name";
$Lang['DocRouting']['ONLINEFORM']['ReplySlipType'] = "Reply slip type";
$Lang['DocRouting']['ONLINEFORM']['ReplySlipTypeList'] = "List";
$Lang['DocRouting']['ONLINEFORM']['ReplySlipTypeTable'] = "Table";
$Lang['DocRouting']['ONLINEFORM']['SubmitAfterDeadline'] = "Allow to submit after deadline:";
$Lang['DocRouting']['ONLINEFORM']['Yes'] = 'Yes';
$Lang['DocRouting']['ONLINEFORM']['No'] = 'No';

// P
//$Lang['DocRouting']['PassRouting'] = "Pass Routings";
$Lang['DocRouting']['Period'] = "according to period (set below)";
$Lang['DocRouting']['Personal'] = "Personal";
$Lang['DocRouting']['PhysicalLocation'] = "Physical Location";
$Lang['DocRouting']['Public'] = "Public";
$Lang['DocRouting']['Password'] = "Password";
$Lang['DocRouting']['PreviousRouting'] = "Previous routing";

# Preset Doc Instruction
$Lang['DocRouting']['PresetDocInstruction'] = "Preset Document Instruction";
$Lang['DocRouting']['PresetDocInstructionContent'] = "Content";
$Lang['DocRouting']['PresetInstruction'] = "Preset Instruction";
$Lang['DocRouting']['PresetNotes'][] = array("1", "I would like to have your comments or recommendations");
$Lang['DocRouting']['PresetNotes'][] =  array("2", "Please make arrangement or return the reply slip");
$Lang['DocRouting']['PresetNotes'][] =  array("3", "Please discuss with me (on or before: yyyy-mm-dd )");
$Lang['DocRouting']['PresetNotes'][] =  array("4", "Please encourage teachers or students to participate");
$Lang['DocRouting']['PresetNotes'][] =  array("5", "For your action (on or before: yyyy-mm-dd )");
$Lang['DocRouting']['PresetNotes'][] =  array("6", "For your filing");
$Lang['DocRouting']['PresetNotesSelection'] = "--- select from preset options ---";
$Lang['DocRouting']['PresetRoutingInstruction'] = "Preset Routing Instruction";
$Lang['DocRouting']['PresetType'] = "Preset Type";
$Lang['DocRouting']['PreviewRouteDone'] = "only if previous route is finished";
$Lang['DocRouting']['StartRouteNow'] = "Start route now";
$Lang['DocRouting']['PreviewReplySlip'] = "Preview Reply Slip";
$Lang['DocRouting']['PreviewCurrentReplySlip'] = "Preview Current Reply Slip";

// Q


// R
$Lang['DocRouting']['ReadOnlyUser'] = "Read-only user";
$Lang['DocRouting']['Route'] = "Route(s)";
$Lang['DocRouting']['Routing'] = "Routing";
$Lang['DocRouting']['RoutingDetail'] = "Routing detail"; 
$Lang['DocRouting']['ReturnMessage']['EmailSentUnsuccess'] = "0|=|Failed to send email.";
$Lang['DocRouting']['ReturnMessage']['EmailSentSuccess'] = "1|=|Success to send email.";
$Lang['DocRouting']['ReplySlipStatistics'] = "Reply Slip Statistics";
$Lang['DocRouting']['RequestNumber'] = "Request Number";

// S
$Lang['DocRouting']['SaveAsDraft'] = "Save as Draft";
$Lang['DocRouting']['Selected'] = "selected";
$Lang['DocRouting']['SelectUser'] = "Select user(s)";
$Lang['DocRouting']['StepAccess'] = "Also accessed by";
$Lang['DocRouting']['StepAccessAll'] = "All staff involved in this document";
$Lang['DocRouting']['StepAccessRelated'] = "Only the staff involved in this route";
$Lang['DocRouting']['StepAccessRelatedAndMore'] = "The staff involved in this route and selected user(s)";
$Lang['DocRouting']['SignAndConfirm'] = "Sign/Confirm";
$Lang['DocRouting']['Status'] = "Status";
$Lang['DocRouting']['StatusDisplay']['Waiting'] = "To be Followed";
$Lang['DocRouting']['StatusDisplay']['Processing'] = "In Progress";
$Lang['DocRouting']['StatusDisplay']['Completed'] = "Completed";
$Lang['DocRouting']['StatusDisplay']['NotYetStarted'] = "Not Yet Started";
$Lang['DocRouting']['StatusDisplay']['ViewOnly'] = "Only Can View";

$Lang['DocRouting']['SendReminder'] = "Send Reminder";
$Lang['DocRouting']['SubmitComment'] = "Submit Comment";
$Lang['DocRouting']['SaveAsDraft'] = "Save as Draft";
$Lang['DocRouting']['StartDate'] = "Start Date Range";
$Lang['DocRouting']['StarredFile'] = "Starred file";
$Lang['DocRouting']['SendEmail'] = "Send email";
$Lang['DocRouting']['SendSignEmailInstruction'] = "You can invite users to follow the routings with direct hyper-links. ";
$Lang['DocRouting']['SendSignEmailRemark'] = 'Remark: Once email is sent, user can sign all routings independent of routing date.';
$Lang['DocRouting']['SendSignEmailTitle'] = "New routing titled <!--TITLE--> from <!--SCHOOL_NAME-->";
$Lang['DocRouting']['SendSignEmailContent'] = '<p>You are invited to follow/sign a document issued by <!--SCHOOL_NAME-->.</p><p>Click the link below to start: </p><p><a href="<!--LINK-->" target="_blank"><!--LINK--></a></p><p>Thank you for your attention.</p>';
$Lang['DcoRouting']['SignedOn'] = "Signed on <!--TIME-->";
$Lang['DcoRouting']['SignRoutings'] = "Sign Routings";
$Lang['DocRouting']['ReturnMessage']['SignSuccess'] = "1|=|Routing has been signed successfully。";
$Lang['DocRouting']['ReturnMessage']['SignUnsuccess'] = "0|=|Routing failed to be signed。";
$Lang['DocRouting']['SignConfirmMessage'] = "Are you sure want to sign this routing?";
$Lang['DocRouting']['SignedAllRoutingMessage'] = "You have signed all the routings that related to the email you have received. Thank you.";
$Lang['DocRouting']['SignedConfirmed'] = "Signed/Confirmed";
$Lang['DocRouting']['SendArchivedDocumentEmailInstruction'] = "You can send the archived document to users via email.";
$Lang['DocRouting']['Security'] = "Security";

// T
$Lang['DocRouting']['Tag']['label'] = "Tag:";
$Lang['DocRouting']['Tags'] = "Tags";
$Lang['DocRouting']['TagsInputRemarks'] = "Use comma (,) as separator. Maximun <!--maxNumOfTags--> tags are allowed.";

# Target Type 
$Lang['DocRouting']['TargetType']['ForAllUsers'] = "for all users";
$Lang['DocRouting']['TargetType']['ForMySelf'] = "for myself";
$Lang['DocRouting']['TargetType']['Customized'] = "for customized userd";
$Lang['DocRouting']['TargetType']['TargetType'] = "Target Type";
$Lang['DocRouting']['TargetType']['Title'] = "Title";

$Lang['DocRouting']['Title'] = "Title";
$Lang['DocRouting']['To'] = "TO";

// U
$Lang['DocRouting']['Users'] = "Users";
$Lang['DocRouting']['UploadAttachments'] = "upload attachments";
$Lang['DocRouting']['UploadAttachment'] = "Upload attachment";
$Lang['DocRouting']['UserName'] = "User name";

// V
$Lang['DocRouting']['Visible'] = "Effective";
$Lang['DocRouting']['ViewBy'] = "View by";
$Lang['DocRouting']['ViewReplySlipStatistics'] = "View reply slip statistics";
$Lang['DocRouting']['Version'] = "Version";

// W
$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisFile'] = "Are you sure want to delete this file?";
$Lang['DocRouting']['WarningMsg']['AreYouSureWantToDeleteThisRouting'] = "Are you sure want to delete this this routing?";
$Lang['DocRouting']['WarningMsg']['AreYouSureWantToSign'] = "Are you sure want to sign?";
$Lang['DocRouting']['WarningMsg']['MoreThanAllowedTags'] = "More than number of allowed tags are input. Please remove some of the tags.";
$Lang['DocRouting']['WarningMsg']['PleaseFillInBelowReplySlip'] = "Please fill in below reply slip.";
$Lang['DocRouting']['WarningMsg']['PleaseFillInComment'] = "Please fill in comment.";
$Lang['DocRouting']['WarningMsg']['PleaseFillInValidEffectiveDateRange'] = "Please fill in a valid effective date range.";
$Lang['DocRouting']['WarningMsg']['PleaseSelectAllowedActions'] = "Please select allowed actions.";
$Lang['DocRouting']['WarningMsg']['PleaseSelectEffectiveTypeByPeriod'] = "Please select effective type according to period for the first routing.";
$Lang['DocRouting']['WarningMsg']['PleaseSelectUsers'] = "Please select users.";
$Lang['DocRouting']['WarningMsg']['PleaseSetStartDateAfterDateTime'] = "Please set start date after <!--DATETIME-->.";
$Lang['DocRouting']['WarningMsg']['PleaseUploadAtLeastOneFile'] = "Please upload at least one file.";
$Lang['DocRouting']['WarningMsg']['PleaseUploadReplySlipFile'] = "Please upload reply slip file.";
$Lang['DocRouting']['WarningMsg']['ReplySlipCsvNotCorrect'] = "The selected csv format or data is not correct. Please click \"".$Lang['DocRouting']['PreviewReplySlip']."\" to view the details.";
$Lang['DocRouting']['WarningMsg']['ReplySlipHasAnswerAlready'] = "There are users replied the reply slip already. So, the reply slip cannot be edited now.";
$Lang['DocRouting']['WarningMsg']['ThereShouldBeAtLeastOneRouting'] = 'There should be at least one routing.\n Please add more routings before removing this route.';
$Lang['DocRouting']['WarningMsg']['AnotherPersonGivingFeedback'] = "Another person is giving feedback to this routing. Please try again <!--TIMEOUT--> minutes later.";
$Lang['DocRouting']['WarningMsg']['RequestInputPositiveInteger'] = "Please input a positive integer.";
$Lang['DocRouting']['WarningMsg']['RouteLockTiemout'] = "The locking of this routing had timeouted. Please cancel and try again.";
$Lang['DocRouting']['WarningMsg']['SignIgnoreDraftedComment'] = "You have drafted comment that has not been submitted yet. Continue to sign?";
$Lang['DocRouting']['WarningMsg']['UserNameWillBeDisplayInReplySlipResult'] = "User name will be displayed in reply slip result.";
$Lang['DocRouting']['WarningMsg']['UserNameWillNotBeDisplayInReplySlipResult'] = "User name will not be displayed in reply slip result.";
$Lang['DocRouting']['WarningMsg']['PleaseSelectEmailTarget'] = "Please select Email Target.";
$Lang['DocRouting']['WarningMsg']['PleaseEnterEmailContent'] = "Please enter Email Content.";
$Lang['DocRouting']['WarningMsg']['PleaseSelectEmailType'] = "Please select email type.";
$Lang['DocRouting']['WarningMsg']['PleaseSelectRoutes'] = "Please select routes.";
$Lang['DocRouting']['WarningMsg']['RequestEnterPasswordToSign'] = "Please enter your eClass password to access the routing and sign.";
$Lang['DocRouting']['WarningMsg']['InvalidToken'] = "Invalid token.";
$Lang['DocRouting']['WarningMsg']['UnauthorizedAccess'] = "Unauthorized access.";
$Lang['DocRouting']['WarningMsg']['ConfirmDeleteFeedback'] = "Are you sure want to delete this feedback?";

// X


// Y


// Z

// Print Instruction Customization
$Lang['DocRouting']['PrintInstruction']['PageTitle'] = "Print Instruction";
$Lang['DocRouting']['PrintInstruction']['SchoolName'] = "LOK SIN TONG YOUNG KO HSIAO LIN SECONDARY SCHOOL";
$Lang['DocRouting']['PrintInstruction']['Memo'] = "MEMORANDUM";
$Lang['DocRouting']['PrintInstruction']['FromThePrincipal'] = "FROM THE PRINCIPAL";
$Lang['DocRouting']['PrintInstruction']['Date'] = "DATE";
$Lang['DocRouting']['PrintInstruction']['TO'] = "TO";
$Lang['DocRouting']['PrintInstruction']['Signature'] = "Signature";
$Lang['DocRouting']['PrintInstruction']['CommentsAndRemarks'] = "Comments/ Remarks by teacher with signature (Use back of paper if necessary)";


?>
