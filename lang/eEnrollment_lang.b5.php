<?php
//using: 

$Lang['eEnrolment']['Hour'] = "小時";
$Lang['eEnrolment']['Unit'] = "個";
$Lang['eEnrolment']['AllDay'] = "所有日子";
$Lang['eEnrolment']['WeekDay'][1] = "星期一";
$Lang['eEnrolment']['WeekDay'][2] = "星期二";
$Lang['eEnrolment']['WeekDay'][3] = "星期三";
$Lang['eEnrolment']['WeekDay'][4] = "星期四";
$Lang['eEnrolment']['WeekDay'][5] = "星期五";
$Lang['eEnrolment']['WeekDay'][6] = "星期六";
$Lang['eEnrolment']['WeekDay'][7] = "星期日";

$Lang['eEnrolment']['Number'][1] = "One";
$Lang['eEnrolment']['Number'][2] = "Two";
$Lang['eEnrolment']['Number'][3] = "Three";
$Lang['eEnrolment']['Number'][4] = "Four";
$Lang['eEnrolment']['Number'][5] = "Five";
$Lang['eEnrolment']['Number'][6] = "Six";
$Lang['eEnrolment']['Number'][7] = "Seven";
$Lang['eEnrolment']['Number'][8] = "Eight";
$Lang['eEnrolment']['Number'][9] = "Nine";
$Lang['eEnrolment']['Number'][10] = "Ten";
$Lang['eEnrolment']['Number'][11] = "Eleven";
$Lang['eEnrolment']['Number'][12] = "Twelve";

$Lang['eEnrolment']['ClubTypeAry'] = array ("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T");

$Lang['eEnrolment']['ShortWeekday'][1] = "一";
$Lang['eEnrolment']['ShortWeekday'][2] = "二";
$Lang['eEnrolment']['ShortWeekday'][3] = "三";
$Lang['eEnrolment']['ShortWeekday'][4] = "四";
$Lang['eEnrolment']['ShortWeekday'][5] = "五";
$Lang['eEnrolment']['ShortWeekday'][6] = "六";
$Lang['eEnrolment']['ShortWeekday'][7] = "日";

$Lang['eEnrolment']['AfterDayDisplay'][0] = "今天";
$Lang['eEnrolment']['AfterDayDisplay'][1] = "明天";
$Lang['eEnrolment']['AfterDayDisplay'][2] = "後天";

$Lang['eEnrolment']['Total'] = "共";
$Lang['eEnrolment']['Times'] = "次";
$Lang['eEnrolment']['CountClub'] = "個";
$Lang['eEnrolment']['AfterDay'] = "天後";
$Lang['eEnrolment']['NextMeeting'] = "下一次活動";
$Lang['eEnrolment']['MeetingCount'] = "第[__count__]次活動";
$Lang['eEnrolment']['Finished'] = "完成";
$Lang['eEnrolment']['FinishedClub'] = "已完結";
$Lang['eEnrolment']['FinishedMeeting'] = "活動完結";
$Lang['eEnrolment']['InParticipating'] = "參與中";
$Lang['eEnrolment']['Header']['ClubInfo'] = "學會資料";
$Lang['eEnrolment']['Header']['ActivityInfo'] = "活動資料";
$Lang['eEnrolment']['PerformanceandComment']  = "表現及評語";
$Lang['eEnrolment']['TeacherComment']  = "老師評語";
$Lang['eEnrolment']['Performance'] = "表現";
$Lang['eEnrolment']['ShowMore'] = "顯示更多";
$Lang['eEnrolment']['Gender'] = "對象性別";
$Lang['eEnrolment']['Quato'] = "名額";
$Lang['eEnrolment']['TargetAge'] = "對象年齡範圍";
$Lang['eEnrolment']['Tentative'] = "暫定";
$Lang['eEnrolment']['SelectMaxClub1'] = "可選最多參加的學會數目為";
$Lang['eEnrolment']['SelectMaxClub2'] = "至";
$Lang['eEnrolment']['ErrorMaxClubTitle'] = "注意";

$Lang['eEnrolment']['TargetForm'] = "對象級別";
$Lang['eEnrolment']['ClubContent'] = "學會內容";
$Lang['eEnrolment']['Details'] = "詳細資料";
$Lang['eEnrolment']['OLECategory'] = "其他學習經歷種類";
$Lang['eEnrolment']['Fee'] = "費用";

$Lang['eEnrolment']['AttendanceRecord'] = "出席紀錄";
$Lang['eEnrolment']['Deadline'] = "截止";
$Lang['eEnrolment']['FromTodayTo'] = "即日至";
$Lang['eEnrolment']['InApplication'] = "申請中";

$Lang['eEnrolment']['ViewApplicationInstructionsAndStartApply'] ="查看報名須知及開始申請";
$Lang['eEnrolment']['StartApply'] = "開始申請";
$Lang['eEnrolment']['EditClubApply'] = "編輯學會申請";
$Lang['eEnrolment']['OnlyApplyOneClub'] = "你只能申請一個學會。";
$Lang['eEnrolment']['ConsiderApplyClashClub'] = "請考慮是否同時提出申請。";
$Lang['eEnrolment']['ClubHaveTimeClash'] = "之間出現時間衝突，";
$Lang['eEnrolment']['YouHaveChoose'] = "你已選擇";
$Lang['eEnrolment']['SuretoApplyClashClub'] = "你確定同時提出申請？";
$Lang['eEnrolment']['OnlyCanApplyOneClub'] = "你只能申請其中一個學會。";
$Lang['eEnrolment']['YouNeedAppliedClubMin'] = "你最少需要申請 [__count__]";
$Lang['eEnrolment']['YouMustAppliedClub'] = "你必需要申請 [__count__]";
$Lang['eEnrolment']['YouNeedChoose'] = "你需要申請 ";
$Lang['eEnrolment']['NowYouOnlyChoose']= "目前你只選擇了";
$Lang['eEnrolment']['NoOfClubMustBeApply'] = "必須要選擇學會數目";

$Lang['eEnrolment']['AlreadyAchieveMaxClubCount']= "已達學會數目上限";
$Lang['eEnrolment']['AlreadyAchieveMaxClubCountTitle']= "已達學會數目上限";
$Lang['eEnrolment']['AlreadyExceededMaxClubCount'] = "已超過學會數目上限";
$Lang['eEnrolment']['ExceededMaxClubCount'] = "已超過學會數目上限";
$Lang['eEnrolment']['NotAchieveMinClubCount'] = "未達學會數目下限";
$Lang['eEnrolment']['AreYouSureToSubmitApplication'] = "你要呈送申請嗎？";

$Lang['eEnrolment']['EditMaxClubNumber'] = "編輯最多學會數目";
$Lang['eEnrolment']['YouHaveFinishClubApply'] = "你已完成學會申請。";
$Lang['eEnrolment']['EditClubOrder'] = "編輯優先次序";
$Lang['eEnrolment']['CheckandHandleTimeClash']="檢查及處理時間衝突";
$Lang['eEnrolment']['FindandHandleTimeClash']="查詢及處理時間衝突";
$Lang['eEnrolment']['YouHaveSelected'] = "你已選擇";
$Lang['eEnrolment']['ShowApplyInstruction']= "顯示報名須知";
$Lang['eEnrolment']['EachClubTimeClashTimes'] = "各學會出現衝突的次數︰";
$Lang['eEnrolment']['AreYouSureToRemoveFromList'] = "你確定要將「[__ClubTitle__]」從申請名單中移除？";
$Lang['eEnrolment']['ClubApplyResultWillShow'] = "批核結果將於[__date__]公佈";
$Lang['eEnrolment']['YouHaveTotalAppliedClubCount'] = "你共提出了 [__ClubCount__] 項學會申請。";
$Lang['eEnrolment']['ExitClubApplicationWrningMessage'] = "你的申請尚未送出，
離開後申請資料將會失去，
    
你確定要放棄申請，並離開？";
$Lang['eEnrolment']['ClubApplicationResult'] = "批核結果";
$Lang['eEnrolment']['NoResultFound'] = "未有合乎的結果";
$Lang['eEnrolment']['ReturntoEdit'] = "返回修改";

$Lang['eEnrolment']['FullStop'] = "。";
$Lang['eEnrolment']['Comma'] = "，";
$Lang['eEnrolment']['AlertArchivedMaxCanEnrolledClub'] = "已加入學會的數目已達上限，不需要申請更多學會。";
$Lang['eEnrolment']['CannotEnrolledClub'] = "不能進行申請";
?>