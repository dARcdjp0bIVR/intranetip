<?php
if ($sys_custom['LivingHomeopathy']) {
	// Editing by 
	$Lang['Header']['HomeTitle'] = "Living Homeopathy Platform @ eClass";

	$Lang["PageTitle"] = "Living Homeopathy Platform @ eClass";
	$Lang['Header']['Menu']['SchoolCalendar'] = "Calendar";
	$Lang['SysMgr']['SchoolCalendar']['ModuleTitle'] = "Calendar";
	$Lang['Header']['Menu']['SchoolSettings'] = "Management";
	$Lang['Header']['Menu']['StaffAccount'] = "User";
	$Lang['Header']['Menu']['eEnrolment'] = "Course";
	
	$Lang['Header']['Menu']['schoolNews'] = "School News";
	$Lang['SysMgr']['SchoolNews']['SchoolNews'] = "School News";
	
	$Lang['Footer']['PowerBy'] = "Powered by ";
	
	$Lang['LivingHomeopathy'] = array();
	$Lang['LivingHomeopathy']['Organization'] = "Organization";
	$Lang['LivingHomeopathy']['Company'] = "Company";
	$Lang['LivingHomeopathy']['Division'] = "Division";
	$Lang['LivingHomeopathy']['Department'] = "Department";
	$Lang['LivingHomeopathy']['Chinese'] = "繁";
	$Lang['LivingHomeopathy']['English'] = "English";
	$Lang['LivingHomeopathy']['Code'] = "Code";
	$Lang['LivingHomeopathy']['Description'] = "Description";
	$Lang['LivingHomeopathy']['CompanyName'] = "Company Name";
	$Lang['LivingHomeopathy']['DivisionName'] = "Division Name";
	$Lang['LivingHomeopathy']['DepartmentName'] = "Department Name";
	$Lang['LivingHomeopathy']['RelatedCompany'] = "Related Company";
	$Lang['LivingHomeopathy']['RelatedDivision'] = "Related Division";
	$Lang['LivingHomeopathy']['DepartmentStaff'] = "Department Staff";
	$Lang['LivingHomeopathy']['NoOfStaff'] = "No. of Staff";
	$Lang['LivingHomeopathy']['StaffName'] = "Staff Name";
	$Lang['LivingHomeopathy']['ViewStaffList'] = "View Staff List";
	
	$Lang['LivingHomeopathy']['JoinDate'] = "Join Date";
	$Lang['LivingHomeopathy']['TerminatedDate'] = "Terminated Date";
	$Lang['LivingHomeopathy']['EffectiveDate'] = "Effective Date";
	
	$Lang["LivingHomeopathy"]["Login"]["Login"] = "Login";
	$Lang["LivingHomeopathy"]["Login"]["LoginTitle"] = "Employee Mobile App Platform";
	$Lang["LivingHomeopathy"]["Login"]["LoginID"] = "Login ID";
	$Lang["LivingHomeopathy"]["Login"]["LoginID_alert"] = "Please enter the user login.";
	$Lang["LivingHomeopathy"]["Login"]["LoginID_err"] = "Invalid LoginID/Password.";
	$Lang["LivingHomeopathy"]["Login"]["Password"] = "Password";
	$Lang["LivingHomeopathy"]["Login"]["Password_alert"] = "Please enter the password.";
	$Lang["LivingHomeopathy"]["Login"]["Password_err"]= "Invaid Password";
	$Lang["LivingHomeopathy"]['Login']['ForgotPassword'] = "Forgot password?";
	
	$Lang["LivingHomeopathy"]["Login"]["ForgotPasswordMsg"] = '1. Input UserLogin, then reset pwd mail will be sent to your preset address.\n2. If no mail received, kindly contact your organization.';
	$Lang['LivingHomeopathy']['Warnings']['DuplicatedName'] = "Duplicated name.";
	$Lang['LivingHomeopathy']['Warnings']['DuplicatedCode'] = "Duplicated code.";
	$Lang['LivingHomeopathy']['Warnings']['norecord'] = "There is no record at the moment.";
	
	$Lang['Portal']['Tag']['eHomework'] = "eHomework";
	$Lang['Portal']['AlleHomework'] = "All eHomework";
	$Lang['Portal']["DueOn"] = "Due on";
	$Lang['Portal']["ToMark"] = "to mark";
	$Lang['Portal']['Tag']['eNotice'] = "eNotice";
	$Lang['Portal']['AlleNotice'] = "All eNotice";
	$Lang['Portal']['AllSchoolNews'] = "All School News";
	$Lang['Portal']['AllCourses'] = "All Courses";
}
?>