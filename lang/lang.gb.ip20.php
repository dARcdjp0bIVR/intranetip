<?php
# using: 

// Please do NOT add/ update/ delete any of the entry in this file (By Kenneth Chung 20090813)
// if you wish to add a language, please add it in lang.b5.php

$i_title = "eClass 综合平台";
$i_home_title  = "eClass 综合平台";
$i_admin_title = "综合平台行政管理中心";
$i_no_record_exists_msg = "暂时仍未有任何纪录";
$i_no_record_exists_msg2 = "暂时仍未有任何纪录";
$i_no_record_searched_msg = "找不到所需记录";
$i_no_search_result_msg = "很抱歉，找不到和你的查询相符的资料。请修订你的查询句并再尝试。";
$i_alert_or = "或";
$i_alert_pleasefillin = "请填上 ";
$i_alert_pleaseselect = "请选择 ";
$i_select_file = "选择档案";
$i_general_startdate = "开始日期";
$i_general_enddate = "结束日期";
$i_general_title = "题目";
$i_general_description = "说明";
$i_general_status = "状况";
$i_general_clickheredownloadsample = "按此下载范例";
$i_general_sysadmin = "<B>系统管理员</B>";
$i_general_WholeSchool = "全校";
$i_general_TargetGroup = "目标小组";
$i_general_Poster = "发布人";
$i_general_BasicSettings = "基本设定";
$i_general_NotSet = "不设定";
$i_general_EachDisplay = "每页显示";
$i_general_PerPage = "项";
$i_general_DisplayOrder = "显示次序";
$i_general_Teacher = "老师";
$i_general_Day = "日";
$i_general_Days = "日";
$i_general_Month = "月";
$i_general_Months = "月";
$i_general_Year = "年";
$i_general_Years = "年";
$i_general_Format = "格式";
$i_general_Format_web = "Web";
$i_general_Format_word = "MSWord";
$i_general_Type = "种类";
$i_general_SelectTarget = "选择目标";
$i_general_SelectOption = "选项";
$i_general_ViewDetailRecords = "查看详细纪录";
$i_general_MonthShortForm = array(
"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"
);
$i_general_more = "更多";
$i_general_subject_teacher = "科目教师";
$i_general_ImportFailed = "以下的纪录不能被汇入";
$i_general_targetParent = "的家长";
$i_general_select_parent = "选择家长";
$i_general_targetGuardian = " 的监护人";
$i_general_select_guardian_from_student = "从学生名字选择监护人";

$i_general_record_date = "纪录日期";
$i_general_record_time = "纪录时间";
$i_general_show_child = "显示他的子女";
$i_general_BackToTop = "页首";
$i_general_students_selected = "已选择学生";
$i_general_selected_students = "已选择学生";
$i_general_selected_teachers = "已选择老师";
$i_general_search_again = "重新搜寻";
$i_general_view_past = "检视昔日纪录";
$i_general_english = "英文";
$i_general_chinese = "中文";
$i_general_yes = "是";
$i_general_yes2 = "有";
$i_general_no = "否";
$i_general_active = "可用";
$i_general_inactive = "停用";
$i_general_main_menu = "返回清单";
$i_general_logout = "登出";
$i_general_steps = "步骤";
$i_general_required_field = "附有「<span class='tabletextrequire'>*</span>」的项目必须填写";
$i_general_required_field2 = "附有「<span class='tabletextrequire'>*</span>」的项目必须填写";
$i_general_display_format = "显示形式";

$i_general_alternative = "其他方式";
$i_general_expand_all = "全部展开";
$i_general_collapse_all = "全部折叠";
$i_general_report_creation_time="报表建立时间";
$i_general_last_modified_by="最后修改人";
$i_general_no_privileges = "你没有检视特权。";
$i_general_icon_list = "图示表";
## Added on 18-7-2007 by Andy
$i_general_all_classes = "全部班别";
$i_general_all_houses = "全部社";
$i_general_refresh = "重新整理";
$i_general_refreshed = "重新整理完成";

$i_general_choose_student = "选择学生";
$i_general_from_class_group = "自 <b>班别</b> / <b>组别</b>";
$i_general_from_class = "自 <b>班别</b>";
$i_general_or = "或";
$i_general_search_by_inputformat = "输入\"[班名] [学号] [姓名]\"<br>(e.g. 1a15 Chan) ";
$i_general_search_by_loginid = "以登入名称搜寻";

$i_general_select_csv_file = "选择CSV档案";
$i_general_imported_result = "汇入结果";
$i_general_confirm_import_data = "确认要汇入纪录";
$i_general_complete = "完成";

$i_general_enabled = "可用";
$i_general_disabled = "停用";
$i_general_orderby = "排序";
$i_general_target = "对象";
$i_general_class = "班别";
$i_general_name = "姓名";
$i_general_orderby_class = "班别, 班号";
$i_general_count = "次数";
$i_general_display_details = "显示详细纪录";
$i_general_all = "全部";
$i_general_no_access_right = "你没有权限观看此页。";
$i_general_highest = "最高";
$i_general_most = "最多";
$i_general_every = "每";
$i_general_first = "首";
$i_general_another = "再";
$i_general_instance_of = "次的";
$i_general_will_be_count_as = "将转换为一次";
$i_general_receive = "奖惩数量";
$i_general_show = "显示";
$i_general_loading = "载入中...";
$i_general_timeout_relogin = "逾时操作, 请重新登入系统。";

# 20090324 yatwoon
$i_general_level = "等级";
$i_general_lowest = "最低";

$i_general_na = "不适用";
$i_general_print_date = "列印日期";

$i_email_to = "致";
$i_email_subject = "主旨";
$i_email_message = "内容";
$i_email_sendemail = "传送电邮";
$i_From = "由";
$i_To = "至";
$i_LastModified = "最后修改日期";
$i_status_approve = "批准";
$i_status_approved = "已批准";
$i_status_suspend = "暂停";
$i_status_suspended = "已暂停";
$i_status_pending = "未发布";
$i_status_pendinguser = "未批准";
$i_status_graduate = "已离校";
$i_status_publish = "已发布";
$i_status_published = "已发布";
$i_status_checkin = "已登记";
$i_status_checkout = "已退回";
$i_status_cancel = "已取消";
$i_status_reserved = "预订的";
$i_status_rejected = "已被拒绝";
$i_status_all = "全部";
$i_status_activated = "使用中";
$i_status_template = "范本";
$i_status_waiting = "等待批核";
$i_invalid_email = "电邮地址不符";
$i_invalid_date = "日期不符";
$i_gender_male = "男性";
$i_gender_female = "女性";
$i_title_mr = "先生";
$i_title_miss = "小姐";
$i_title_mrs = "太太";
$i_title_ms = "女士";
$i_title_dr = "博士";
$i_title_prof = "教授";
$i_export_msg1 = "分隔符号";
$i_export_msg2 = "汇出指定项目";
$i_import_msg1 = "1. 选择汇入用户的格式";
$i_import_msg2 = "2. 选择需要汇入的档案";
$i_import_msg3 = "按汇入键来上载及汇入档案";
$i_import_msg4 = "甚么是eClass .CSV档案？";
$i_import_msg5 = "eClass .CSV 档案是一个已被编排格式的 .CSV 档案：";
$i_import_msg6 = "包含所有名单上的项目";
$i_import_msg7 = "例如：登入账号、密码、用户电邮、英文名称、中文名称";
$i_import_msg8 = "可以在 Microsoft Excel 中编辑";
$i_import_msg9 = "下载 eClass.csv 档案范本";
$i_import_msg10 = "3. 最少选择一个身份组别项目";
$i_import_msg_note = "备注:<br>
<ul>
<li>档案中的 'UserLogin' 须由 'a-z'(小楷), '0-9' 及 '_' 组成, 而且<b>必须</b>使用 'a-z' 为开始字元
<li>如果档案中的 'UserLogin' 已经存在, 该帐户所有资料会被更新(包括电邮及密码)
<li>如欲不更新电邮及密码, 可在档案中留空该栏</ul>";
$i_import_msg_note2 = "备注二:<br>
<ul><li>为免遗失号码前端的数字 \"0\"，请在每个 WebSAMS 注册号码前加上 \"#\" 号 (如: <b>#</b>0012345 )</ul>";

$i_import_invalid_format = "你上载之档案格式并不符合. 资料顺序应为: ";
$i_groupimport_msg = "选择需要汇入的档案";
$i_groupimport_type = "新增的组别种类";
$i_groupimport_fileformat = "档案格式";
$i_groupimport_fileformat2 = "档案中的第一栏须为内联网帐号.<br>
第二栏则为该组别的名称, 如该组别不存在, 系统将会自动新增.";
$i_groupimport_sample = "按此下载范本档案 (CSV 格式)";

$i_import_big5 = "以Big-5格式汇入";
$i_import_gb = "以GB格式汇入";
$i_import_utf = "以UTF格式汇入";
$i_import_utf_type = "请上载UTF格式之档案。";

$i_identity = "身份";
$i_identity_teachstaff = "教职员";
$i_identity_student = "学生";
$i_identity_parent = "家长";
$i_identity_alumni = "校友";
$i_identity_array = array("",$i_identity_teachstaff,$i_identity_student,$i_identity_parent,$i_identity_alumni);

$i_import_teacher_data = "汇入$i_identity_teachstaff"."资料";
$i_import_student_data = "汇入$i_identity_student"."资料";
$i_import_parent_data = "汇入$i_identity_parent"."资料";
$i_import_alumni_data = "汇入$i_identity_alumni"."资料";
$i_import_identity_array = array("",$i_import_teacher_data,$i_import_student_data,$i_import_parent_data,$i_import_alumni_data);

$i_teachingStaff = "教学职务员工";
$i_nonteachingStaff = "非教学职务员工";
$i_teachingDifference = "(教学职员会被自动加入 Teacher 小组; 非教学职员会加入 Admin Staff 小组)";

$i_ifapplicable = "如适用";
$i_notapplicable = "不适用";

$i_ClassLevel = "级别";
$i_ClassLevelName = "级别名称";
$i_ClassName = "班别";
$i_GroupName = "小组";
$i_ClassNumber = "学号";
$i_Class_ImportInstruction = "<b><u>档案说明:</u></b><br>
第一栏 : 班级名称 (如 1A ).<br>
第二栏 : 级别 (如 F.1).<br>";
$i_Class_DownloadSample = "按此下载范例";

$i_SettingsSchoolName = "学校名称";
$i_SettingsOrganization = "办学团体";
$i_SettingsSchoolPhone = "学校电话";
$i_SettingsSchoolAddress = "学校地址";
$i_SettingsCurrentAcademicYear = "本年度";
$i_SettingsSemester = "学期";
$i_SettingsSchoolDayType = "日制";
$i_SettingsCurrentSemester = "现时";
$i_SettingsSemesterList = "学期";
$i_SettingsSchool_Class = "班级设定";
$i_SettingsSchool_ClassTeacher = "老师职务调配";
$i_SettingsSchool_Forms = "学校自订表格";

### added by ronald 20081118 ###
$i_SettingSemesterMode = "学期更新模式";
$i_SettingSemesterManualUpdate = "用户自行更新";
$i_SettingSemesterAutoUpdate = "系统自行更新";
$i_SettingSemesterSetting_Alert1 = "请填上开始日期";
$i_SettingSemesterSetting_Alert2 = "请填上结束日期";
$i_SettingSemesterSetting_Alert3 = "输入的日期无效";
$i_SettingSemesterSetting_Alert4 = "输入的日期排列次序无效，请重新输入。";
######

$i_DayTypeWholeDay = "全日";
$i_DayTypeAM = "上午";
$i_DayTypePM = "下午";
$i_DayTypeArray = array(
"",
$i_DayTypeWholeDay,
$i_DayTypeAM,
$i_DayTypePM);

$i_wordtemplates_select = "选择类型";
$i_wordtemplates_wordings = "预设字句 (以每行为一字句)";
$i_wordtemplates_attendance = "迟到/缺席/早退原因";
$i_wordtemplates_merit = "奖励原因";
$i_wordtemplates_demerit = "惩罚原因";
$i_wordtemplates_activity = "活动名称";
$i_wordtemplates_performance = "表现";
$i_wordtemplates_service = "服务名称";
$i_wordtemplates_service_role = "服务职位";
$i_wordtemplates_activity_role = "活动职位";
$i_wordtemplates_award_name = "奖项名称";
$i_wordtemplates_award_role = "奖项职位";

$i_wordtemplates_type_array = array(
$i_wordtemplates_attendance,
$i_wordtemplates_merit,
$i_wordtemplates_demerit
);

$i_wordtemplates_instruction = "你可以更改预设字句, 以每一行为一字句. <br>更改后请按更新储存.";

$i_menu_setting = "内联网设定";
$i_menu_intranet = "内联网管理";
$i_menu_eclass = "网上教室管理";
$i_menu_network = "网络系统管理";

# menu name
$i_adminmenu_group = "组别";
$i_adminmenu_user = "用户";
$i_adminmenu_announcement = "宣布";
$i_adminmenu_event = "事项";
$i_adminmenu_polling = "投票";
$i_adminmenu_timetable = "小组时间表";
$i_adminmenu_resource = "资源清单";
$i_adminmenu_booking = "预订";
$i_adminmenu_staffdirectory = "职员名册";
$i_adminmenu_filemgmt = "档案管理";
$i_adminmenu_eclass = "网上教室";
$i_adminmenu_role = "职位";
$i_adminmenu_usermgmt = "用户管理";
$i_adminmenu_infomgmt = "资讯管理";
$i_adminmenu_resourcemgmt = "资源管理";
$i_adminmenu_eclassmgmt = "网上教室管理";
$i_adminmenu_referencefiles = "参考档案";
$i_adminmenu_motd = "流动资讯";
$i_adminmenu_sysmgmt = "系统管理";
$i_adminmenu_email_setting = "电子邮件设定";
//$i_adminmenu_admin_account = "管理人户口";
$i_adminmenu_rbps = "资源预订时期设定";

$i_admintitle_group = "组别";
$i_admintitle_user = "用户";
$i_admintitle_announcement = "宣布";
$i_admintitle_event = "事项";
$i_admintitle_polling = "投票";
$i_admintitle_timetable = "时间表";
$i_admintitle_resource = "资源";
$i_admintitle_tmpfiles = "暂存档案";
$i_admintitle_booking = "预订";
$i_admintitle_staffdirectory = "职员名册";
$i_admintitle_filemgmt = "档案管理";
$i_admintitle_eclass = "网上教室";
$i_admintitle_role = "职位";
$i_admintitle_usermgmt = "用户管理";
$i_admintitle_infomgmt = "资料管理";
$i_admintitle_resourcemgmt = "资源管理";
$i_admintitle_eclassmgmt = "网上教室管理";
$i_admintitle_referencefiles = "参考档案";
$i_admintitle_motd = "流动资讯";
$i_admintitle_sysmgmt = "系统管理";
$i_admintitle_email_setting = "电子邮件设定";
$i_admintitle_admin_account = "管理人户口";
$i_admintitle_rbps = "资源预订时期设定";
$i_admintitle_subjects = "科目";

$i_adminmenu_sa = "系统总管";
$i_adminmenu_sa_email = "电邮设定";
$i_adminmenu_sa_password = "更改密码";
$i_adminmenu_sa_helper = "系统助手";
$i_adminmenu_sa_pw_leave_blank = "沿用旧密码请留空";
$i_adminmenu_sc = "系统设定";
$i_adminmenu_sc_globalsetting = "总体设定";
$i_adminmenu_sc_resourcebooking = "资源预订";
$i_adminmenu_sc_campuslink = "自订连结";
$i_adminmenu_sc_basic_settings = "基本设定";
$i_adminmenu_sc_school_settings = "学校设定";
$i_adminmenu_sc_group_settings = "小组设定";
$i_adminmenu_sc_user_info_settings = "用户资料设定";
$i_adminmenu_sc_clubs_enrollment_settings = "学会报名设定";
$i_adminmenu_sc_campustv = "校园电视台";
$i_adminmenu_sc_url = "联网网址设定";
$i_adminmenu_sc_cycle = "循环日设定";
$i_adminmenu_sc_period = "上课节数设定";
$i_adminmenu_sc_language = "语言设定";
$i_adminmenu_sc_webmail = "网邮设定";
$i_adminmenu_sc_campusmail = "校园通告设定";
$i_adminmenu_sc_homework = "家课纪录表";
$i_adminmenu_sc_campusquota = "校园信箱储存量";
$i_adminmenu_sc_wordtemplates = "预设字句";
$i_adminmenu_fs = "功能设定";
$i_adminmenu_fs_homework = "家课纪录表设定";
$i_adminmenu_fs_campusmail = "校园信箱设定";
$i_adminmenu_fs_resource_set = "资源预订设定";
$i_adminmenu_sf = "系统档案";
$i_adminmenu_plugin = "系统整合设定";
$i_adminmenu_plugin_sls_library = "SLS 图书馆系统";
$i_adminmenu_plugin_qb = "中央题目库";
$i_adminmenu_am = "用户管理";
$i_adminmenu_am_user = "用户管理中心";
$i_adminmenu_am_usage = "登入纪录";
//$i_adminmenu_am_group = "组别";
$i_adminmenu_am_role = "职位";
$i_adminmenu_adm = "行政管理";
$i_adminmenu_adm_academic_record = "学生档案";
$i_adminmenu_adm_teaching = "教师职务设定";
$i_adminmenu_adm_adminjob = "行政职务设定";
$i_adminmenu_adm_enrollment_approval = "学会报名管理";
$i_adminmenu_adm_enrollment_group = "小组报名情况";
//$i_adminmenu_adm_enrollment_student = "学生报名情况";
$i_adminmenu_im = "日常资讯管理";
$i_adminmenu_im_motd = "提示讯息";
$i_adminmenu_im_announcement = "校园最新消息";
$i_adminmenu_im_event = "校历表";
$i_adminmenu_im_timetable = "小组时间表";
$i_adminmenu_im_campusmail = "学校通告";
$i_adminmenu_im_polling = "投票";
$i_adminmenu_im_group_bulletin = "小组讨论栏";
$i_adminmenu_im_group_files = "小组文件";
$i_adminmenu_im_group_links = "小组连结";
$i_adminmenu_gm = "小组管理";
$i_adminmenu_gm_group = "小组管理中心";
$i_adminmenu_gm_groupfunction = "小组功能管理";
$i_adminmenu_rm = "资源管理";
$i_adminmenu_rm_item = "资源清单";
$i_adminmenu_rm_record = "资源预订纪录";
$i_adminmenu_rm_record_periodic = "定期资源预订纪录";
$i_adminmenu_us = "使用统计";
$i_adminmenu_us_user = "用户";
$i_adminmenu_us_group = "组别";
$i_adminmenu_basic_settings_misc = "其他设定";
$i_adminmenu_basic_settings_email = "用户不能更改电邮地址.";
$i_adminmenu_basic_settings_title = "不显示'$i_general_Teacher'字眼或称谓 ($i_identity_teachstaff 及 $i_identity_parent)";
$i_adminmenu_basic_settings_home_tel = "用户不能更改住宅电话.";
$i_adminmenu_basic_settings_badge = "校徽";
$i_adminmenu_basic_settings_badge_instruction = "图像只能以'.JPG'、'.GIF' 或'.PNG'的格式上载，大小也规定为 120 X 60 pixel (阔 x 高)。";
$i_adminmenu_basic_settings_badge_use = "显示校徽";

$i_adminmenu_user_info_settings_disable_1 = "教师不能更改:";
$i_adminmenu_user_info_settings_disable_2 = "学生不能更改:";
$i_adminmenu_user_info_settings_disable_3 = "家长不能更改:";

$i_adminmenu_user_info_settings_display_disable_1 = "老师不显示";
$i_adminmenu_user_info_settings_display_disable_2 = "学生不显示";
$i_adminmenu_user_info_settings_display_disable_3 = "家长不显示";

$i_adminmenu_user_info_settings_display = "显示";

$i_admintitle_sa = "系统总管";
$i_admintitle_sa_email = "电邮设定";
$i_admintitle_sa_password = "更改密码";
$i_admintitle_sa_helper = "系统助手";
$i_admintitle_sc = "系统设定";
$i_admintitle_sc_globalsetting = "总体设定";
$i_admintitle_sc_resourcebooking = "资源预订";
$i_admintitle_sc_campuslink = "自订连结";
$i_admintitle_sc_url = "联网网址设定";
$i_admintitle_sc_homework = "家课";
$i_admintitle_sc_subject = "科目清单";
$i_admintitle_sc_cycle = "循环日设定";
$i_admintitle_sc_period = "上课节数设定";
$i_admintitle_sc_language = "语言设定";
$i_admintitle_sc_webmail = "网邮设定";
$i_admintitle_sc_campusmail = "校园通告设定";
$i_admintitle_sc_campusquota = "校园信箱容量";
$i_admintitle_fs = "功能设定";
$i_admintitle_fs_homework = "家课纪录表设定";
$i_admintitle_fs_homework_subject = "科目设定";
$i_admintitle_fs_official_subject = "增值模组科目设定";
$i_admintitle_fs_campusmail = "校园信箱设定";
$i_admintitle_fs_resource_set = "资源预订设定";
$i_admintitle_sf = "系统档案";
$i_admintitle_am = "用户管理";
$i_admintitle_am_user = "基本用户";
$i_admintitle_am_group = "组别";
$i_admintitle_am_role = "职位";
$i_admintitle_im = "日常资讯管理";
$i_admintitle_im_motd = "提示讯息";
$i_admintitle_im_announcement = "校园最新消息";
$i_admintitle_im_event = "校历表";
$i_admintitle_im_timetable = "小组时间表";
$i_admintitle_im_campusmail = "学校通告";
$i_admintitle_im_campusmail_outbox = "通告纪录";
$i_admintitle_im_campusmail_compose = "发通告";
$i_admintitle_im_campusmail_template = "通告草稿";
$i_admintitle_im_campusmail_trash = "垃圾箱";
$i_admintitle_im_polling = "投票";
$i_admintitle_im_group_bulletin = "小组讨论栏";
$i_admintitle_im_group_files = "小组文件";
$i_admintitle_im_group_links = "小组连结";
$i_admintitle_rm = "资源管理";
$i_admintitle_rm_item = "资源清单";
$i_admintitle_rm_record = "资源预订纪录";
$i_admintitle_rm_per_record = "定期资源预订纪录";
$i_admintitle_us = "使用统计";
$i_admintitle_us_user = "用户";
$i_admintitle_us_group = "组别";

//$i_admin_status_1 = "处理进行中...";
//$i_admin_status_2 = "请稍候!";

# List Value
$list_total = "总数";
$list_prev = "前一页";
$list_next = "后一页";
$list_page = "页";
$list_sortby = "排序";
$list_desc = "递减";
$list_asc = "递增";

# Button Name
$button_add = "增加";
$button_attach = "附件";
$button_approve = "批准";
$button_archive = "存档";
$button_archive_rb = "保存过去纪录";
$button_archive1 = "整存";
$button_activate = "恢复使用";
$button_back = "返回";
$button_cancel = "取消";
$button_change = "更改";
$button_check_all = "全部选择";
$button_checkin = "取用";
$button_checkout = "退还";
$button_clear = "清除";
$button_clear_all = "全部清除";
$button_close = "关闭视窗";
$button_continue = "继续";
$button_copy = "复制";
$button_deactivate = "停用";
$button_edit = "编辑";
$button_email = "电邮";
$button_erase = "清除";
$button_export = "汇出";
$button_export_all = "全部汇出";
$button_export_pps = "汇出缴费聆之手续费";
$button_export_xml = "汇出 XML";
$button_find = "寻找";
$button_finish = "完成";
$button_image = "影像";
$button_import = "汇入";
$button_import_xml = "汇入 XML";
$button_move = "移动";
$button_moveto = "移至";
$button_new = "新增";
$button_newfolder = "新增资料夹";
$button_next = "下一步";
$button_next_page = "下一页";
$button_pay = "缴款";
$button_pending = "未发布";
$button_preview = "预览";
$button_save_preview = "储存 / 储存及预览";
$button_previous_page = "上一页";
$button_quickadd = "特快新增";
$button_quicksearch = "寻找";
$button_delete = "删除";
$button_remove = "删除";
$button_remove_selected = "移除已选";
$button_remove_selected_student = "移除已选择学生";
$button_skip = "略过";
$button_add_attachment = "附加文档";
$button_remove_all = "全部删除";
$button_rename = "重新命名";
$button_reject = "拒绝";
$button_unlock = "解锁";
$button_lock = "封锁";
$button_release = "开放";
$button_unrelease = "取消开放";
$button_waive = "豁免";
$button_unwaive = "取消「豁免」";
$button_reset = "重设";
$button_reserve = "预订";
$button_restore = "复原";
$button_save = "储存";
$button_save_as = "另存";
$button_save_as_new = "另存";
$button_save_draft = "储存草稿";
$button_save_as_draft = "储存草稿";
$button_save_continue = "储存及继续";
$button_submit_continue = "呈送及继续";
$button_submit_preview = "呈送及预览";
$button_search = "寻找";
$button_select = "选择";
$button_select_situation = "选择发放原因";
$button_select_template = "选择模板";
$button_send = "传送";
$button_submit = "呈送";
$button_suspend = "暂停";
$button_swap = "转换";
$button_use_default = "使用预设值";
$button_undo = "还原至未缴";
$button_unzip = "解压";
$button_update = "更新";
$button_upload = "上载";
$button_view = "检视";
$button_emailpassword = "传送密码给使用者";
$button_select_all = "全选";
$button_updateperformance = "更新表现";
$button_print = "列印";
# 20090227 yat woon
$button_print_all = "列印所有资料";
$button_print_selected = "列印已选资料";

$button_quit = "离开";
$button_assignsubjectleader = "设定科长";
$button_confirm = "确认";
$button_emptytrash = "清理垃圾箱";
$button_set = "设定";
$button_more_file = "添加更多档案";
$button_discard = "舍弃";
$button_hide = "隐藏";
$button_notify = "提醒";
$button_view_template = "检视范本";
$button_view_statistics = "检视统计";
$button_hide_statistics = "隐藏统计";
$button_print_statistics = "列印统计";


# General Confirmation Message
$i_con_gen_msg_add = "已增加纪录";
$i_con_gen_msg_update = "已更新纪录";
$i_con_gen_msg_delete = "已删除纪录";
$i_con_gen_msg_email = "已寄出电邮";
$i_con_gen_msg_email_remove = "已删除电邮";
$i_con_gen_msg_email_restore = "已还原电邮";
$i_con_gen_msg_email_save = "已储存电邮";
$i_con_gen_msg_password1 = "已更改密码";
$i_con_gen_msg_password2 = "密码不符";
$i_con_gen_msg_password3 = "新旧密码不可相同";
$i_con_gen_msg_email1 = "已更改电邮地址";
$i_con_gen_msg_email2 = "不能更改电邮地址，原因：电邮地址已被另一用户登记。";
$i_con_gen_msg_photo_delete = "已删除相片";

# Confirmation Message
$i_con_msg_add = "<font color=green>已增加纪录。</font>\n";
$i_con_msg_add_failed = "<font color=red>无法新增纪录。</font>";
$i_con_msg_update = "<font color=green>已更新纪录。</font>\n";
$i_con_msg_update_failed = "<font color=red>更新纪录失败。</font>\n";
$i_con_msg_delete = "<font color=green>已删除纪录。</font>\n";
$i_con_msg_delete_failed = "<font color=red>删除纪录失败。</font>\n";
$i_con_msg_email = "<font color=green>已寄出电邮。</font>\n";
$i_con_msg_password1 = "<font color=green>已更改密码。</font>";
$i_con_msg_password2 = "<font color=red>密码不符。</font>";
$i_con_msg_password3 =" <font color=red>新旧密码不可相同。</font>";
$i_con_msg_email1 = "<font color=green>已更改电邮地址。</font>";
$i_con_msg_email2 = "<font color=red>不能更改电邮地址，原因：电邮地址已被另一用户登记。</font>";
$i_con_msg_archive = "<font color=green>已成功存档。</font>";
$i_con_msg_import_success = "<font color=green>已成功汇入。</font>";
$i_con_msg_date_wrong = "<font color=red>日期输入错误。</font>";
$i_con_msg_admin_modified = "<font color=green>已设定管理员。</font>";
$i_con_msg_suspend = "<font color=green>已更新纪录。</font>\n";
$i_con_msg_cannot_edit_poll = "<font color=red>不能更改已开始的投票。</font>\n";
$i_con_msg_photo_delete = "<font color=green>已删除相片。</font>";
$i_con_msg_import_failed = "<font color=red>档案格式无效。</font>";
$i_con_msg_import_failed2 = "<font color=red>汇入档案失败。</font>";
$i_con_msg_import_header_failed = "<font color=red>标题格式不正确。</font>";
$i_con_msg_grade_generated = "<font color=green>操行等级计算完毕。</font>";
$i_con_msg_import_invalid_login="<font color=red>内联网帐号不符。</font>";
$i_con_msg_user_add_failed = "<font color=red>新增用户失败。 (内联网帐号及电子邮件不符规格或已存在)</font>";
$i_con_msg_date_startend_wrong_alert = "日期错误. 结束日期不能早于开始日期。";
$i_con_msg_date_startend_wrong = "<font color=red>$i_con_msg_date_startend_wrong_alert</font>";
$i_con_msg_date_start_wrong_alert = "日期错误. 开始日期必须是今天或以后。";
$i_con_msg_date_start_wrong = "<font color=red>$i_con_msg_date_start_wrong_alert</font>";
$i_con_msg_add_form_failed = "<font color=red>失败. 表格名称必须是唯一。</font>";
$i_con_msg_delete_part = "<font color=red>部份已删除。 (只能删除非预设及没有小组的类别)</font>";
$i_con_msg_enrollment_next = "<font color=green>学生可以进行下一轮选择 (你可能需要更改报名日期配合)。</font>";
$i_con_msg_enrollment_confirm = "<font color=green>已批核的报名名单已加进小组。</font>";
$i_con_msg_enrollment_lottery_completed = "<font color=green>已完成抽签程序。</font>";
$i_con_msg_subject_leader_updated = "<font color=green>科长设定已更新。</font>";
$i_con_msg_sports_house_group_existed = "<font color=red>内联网小组已被选用。</font>";
$i_con_msg_voted = "<font color=green>投票完成。</font>";
$i_con_msg_delete_following_failed = "<font color=red>以下纪录未能被删除：</font>\n";
$i_con_msg_copy = "<font color=green>复制完成。</font>";
$i_con_msg_copy_failed_dup_name = "<font color=red>复制失败，原因：样式名称已被使用。</font>";
$i_con_msg_copy_failed = "<font color=red>复制失败。</font>";
$i_con_msg_activate = "<font color=green>已启用纪录。</font>";
$i_con_msg_quota_exceeded = "<font color=red>超出限额。</font>";
$i_con_msg_notification_sent = "<font color=green>已传送学生通知信。</font>";
$i_con_msg_wrong_header = "<font color=red>标题格式不正确</font>";
$i_con_msg_approve = "<font color=green>已批核纪录。</font>\n";
$i_con_msg_reject = "<font color=green>纪录已被拒绝。</font>\n";
$i_con_msg_release = "<font color=green>已开放纪录。</font>\n";
$i_con_msg_unrelease = "<font color=green>纪录已被取消「开放」状态。</font>\n";
$i_con_msg_waive = "<font color=green>纪录已被豁免。</font>\n";
$i_con_msg_unwaive = "<font color=green>纪录已被取消「豁免」。</font>\n";
$i_con_msg_no_delete_acc_record = "<font color=green>部份累积纪录删除失败。</font>\n";
$i_con_msg_no_waive_acc_record = "<font color=green>部份累积纪录豁免失败。</font>\n";
$i_con_msg_no_unwaive_acc_record = "<font color=green>累积纪录取消豁免失败。</font>\n";
$i_con_msg_no_student_select = "<font color=red>没有选择学生。</font>\n";
$i_con_msg_duplicate_students = "<font color=red>选择学生重复。</font>\n";
$i_con_msg_not_all_approve = "<font color=red>部份纪录已批核。</font>\n";
$i_con_msg_no_record_approve = "<font color=red>没有已批核的纪录。</font>\n";
$i_con_msg_not_all_reject = "<font color=red>已拒绝部份纪录。</font>\n";
$i_con_msg_not_all_release = "<font color=red>已开放部份纪录。</font>\n";
$i_con_msg_no_record_release = "<font color=red>没有开放纪录。</font>\n";
$i_con_msg_not_all_unrelease = "<font color=red>已设定部份纪录为未开放。</font>\n";
$i_con_msg_no_record_unrelease = "<font color=red>没有未开放纪录。</font>\n";
$i_con_msg_import_success_with_update = "<font color='green'>成功汇入新纪录，并更新了现有纪录。</font>";
$i_con_msg_sports_ranking_generated = "<font color=green>已更新纪录及赛项排名。</font>";
$i_con_msg_import_no_record = "<font color=red>档案没有资料。</font>";
$i_con_msg_duplicate_item = "<font color=red>该类别名称已经被使用过，请重新输入。</font>";
$i_con_msg_duplicate_category = "<font color=red>该项目名称已经被使用过，请重新输入。</font>";
$i_con_msg_add_overlapped = "<font color=red>时段重复，增加纪录失败。</font>\n";
$i_con_msg_update_overlapped = "<font color=red>时段重迭，更新纪录失败。</font>\n";
$i_con_msg_add_past = "<font color=red>时段已过去，增加纪录失败。</font>\n";
$i_con_msg_update_past = "<font color=red>时段已过去，更新纪录失败。</font>\n";
$i_con_msg_add_past_overlapped = "<font color=red>未能增加某些时段已过去或重迭的纪录。</font>\n";
$i_con_msg_session_not_available = "<font color=red>没有此时段</font>\n";
$i_con_msg_session_not_available2 = "<font color=red>没有合适时段</font>\n";
$i_con_msg_session_past = "<font color=red>时段已过去</font>\n";
$i_con_msg_session_full = "<font color=red>时段额满</font>\n";
$i_con_msg_session_assigned_before = "<font color=red>学生已编定到此时段</font>\n";
$i_con_msg_update_inapplicable_form = "<font color=red>级别不适用，更新纪录失败。</font>\n";
$i_con_msg_update_already_in_session = "<font color=red>学生已编定到此时段，更新纪录失败。</font>\n";
$i_con_msg_update_not_enough_session = "<font color=red>时段不足够，更新纪录失败。</font>\n";
$i_con_msg_update_incompatible_form = "<font color=red>级别不适用，更新纪录失败。</font>\n";
$i_con_msg_update_incompatible_vacancy = "<font color=red>余额不适用，更新纪录失败。</font>\n";
$i_con_msg_cancel = "<font color=green>留堂安排已取消。</font>\n";
$i_con_msg_cancel_failed = "<font color=red>取消留堂安排失败。</font>\n";
$i_con_msg_cancel_past = "<font color=red>时段已过去，取消留堂安排失败。</font>\n";


## added on 15 Jan 2009 by Ivan
$i_con_msg_redeem_success = "<font color=green>功过相抵完成</font>\n";
$i_con_msg_redeem_failed = "<font color=red>功过相抵失败。</font>\n";
$i_con_msg_cancel_redeem_success = "<font color=green>取消功过相抵完成</font>\n";
$i_con_msg_cancel_redeem_failed = "<font color=red>取消功过相抵失败</font>\n";

# for club enrolment (old version) 20080924 yat woon
$i_con_msg_enrollment_club_quota_exceeded = "<font color=red>学会超出限额。</font>";
$i_con_msg_enrollment_no_student = "<font color=red>没有学生需要批核。</font>";
$i_con_msg_enrollment_some_already_exists_club = "<font color=red>部份学生已加入次序够高的小组。</font>";
$i_con_msg_enrollment_club_reject_success = "<font color=green>学生已被拒绝。</font>";

# For class history promotion February 2009 By Sandy
$i_con_msg_record_not_found[0] = "<font color=red>Record not found</font>";
$i_con_msg_record_not_found[1] = "<font color=red>Duplicate/Problematic record not found. <BR />Record may have been corrected in another session</font>";
$i_con_msg_promo_not_deleted = "<font color=red>No records were deleted.</font>";


# Functions
$i_AlbumName = "相簿名称";
$i_AlbumDescription = "内容";
$i_AlbumNumberOfItems = "项目数目";
$i_AlbumDateModified = "上次更新";
$i_AlbumDisplayOrder = "显示次序";
$i_AlbumAccessType = "使用者类别";
$i_AlbumIntranet = "所有内联网用户";
$i_AlbumInternet = "所有互联网用户";
$i_AlbumSelectedGroupsUsers = "选择组别及使用者";
$i_AlbumInternalGroup = "内部小组";
$i_AlbumUserGroup = "使用者";
$i_AlbumQuota = "储存量 (MB)";
$i_AlbumPhotoFile = "档案";
$i_AlbumPhotoDescription = "内容";
$i_AnnouncementTitle = "题目";
$i_AnnouncementDescription = "内容";
$i_AnnouncementDate = "开始日期";
$i_AnnouncementEndDate = "结束日期";
$i_AnnouncementRecordType = "种类";
$i_AnnouncementRecordStatus = "状况";
$i_AnnouncementDateInput = "输入日期";
$i_AnnouncementDateModified = "最近修改日期";
$i_AnnouncementAlert = "发送电子邮件给用户";
$i_AnnouncementAttachment = "附件";
$i_AnnouncementCurrAttachment = "现有附件";
$i_AnnouncementNewAttachment = "更多附件";
$i_AnnouncementNoAttachment = "没有附件";
$i_AnnouncementDelete = "删除?";
$i_AnnouncementSchool = "学校宣布";
$i_AnnouncementAlert_SMS = "传送 SMS 短讯至用户";
$i_AnnouncementAdminRight = "不能编辑或删除由系统管理员建立的项目.";
$i_AnnouncementPublic = "显示在首页";
$i_AnnouncementOwner = "发布人";
$i_AnnouncementByGroup = "负责小组";
$i_AnnouncementSystemAdmin = $i_general_sysadmin;
$i_AnnouncementNoAnnouncer = "Unknown (Old Post)";
$i_AnnouncementPublicInstruction = "如果此宣布为公众宣布,请把组别一栏留空.";
$i_AnnouncementPublic = "公众宣布";
$i_AnnouncementGroup = "我的小组宣布";
$i_AnnouncementDisplayDate = "显示日期";
$i_AnnouncementWholeSchool = "全校";
$i_AnnouncementTargetGroup = "目标小组";
$i_AnnouncementReadCount = "已阅 ";
$i_AnnouncementUnreadCount = "未阅 ";
$i_AnnouncementReadList = "已阅名单";
$i_AnnouncementUnreadList = "未阅名单";
$i_AnnouncementViewReadStatus = "检视观看情况";
$i_AnnouncementViewReadList = "检视已阅名单";
$i_AnnouncementViewUnreadList = "检视未阅名单";
$i_All_MyGroup = "全部小组";

$i_BookingDateStart = "日期";
$i_BookingDateEnd = "时间";
$i_BookingRemark = "备注";
$i_BookingRecordType = "种类";
$i_BookingRecordStatus = "状况";
$i_BookingDateInput = "输入日期";
$i_BookingDateModified = "最近修改日期";
$i_BookingArterisk = "<font color=red>*</font>";
$i_BookingLegend = " - 该申请仍在处理中";
$i_BookingListAll = "检视所有已预订之纪录";
$i_BookingMyRecords = "我的纪录";
$i_BookingNew = "新增预订";
$i_BookingSingle = "单次预订";
$i_BookingPeriodic = "定期重复预订";
$i_BookingAddStep = array("");
$i_BookingAddStep[1] = "先按入所需预订项目的种类.";
$i_BookingAddStep[2] = "选择预订的频率: 单次或定期重复预订.";
$i_BookingAddStep[3] = "填写其他相关的细节.";
$i_BookingAddRule = "如需要预订，请按以下步骤:";
$i_BookingAddRule .= "<br>\n1. ".$i_BookingAddStep[1];
$i_BookingAddRule .= "<br>\n2. ".$i_BookingAddStep[2];
$i_BookingAddRule .= "<br>\n3. ".$i_BookingAddStep[3];
$i_BookingNewSingle = "新增单次预订纪录";
$i_BookingEditSingle = "编辑单次预订纪录";
$i_BookingNewPeriodic = "新增定期重复预订纪录";
$i_BookingEditPeriodic = "编辑定期重复预订纪录";
$i_BookingItem = "资源";
$i_BookingDate = "日期";
$i_BookingTimeSlots = "时段";
$i_BookingApplied = "申请时间";
$i_BookingRetrieveTimeSlots = "检视时段";
$i_BookingStatus = "状况";
$i_BookingStartDate = "开始日期";
$i_BookingEndDate = "结束日期";
$i_BookingPeriodicType = "周期";
$i_BookingNotes = "备忘";
$i_BookingReserved = "不可借用 - 已被预约";
$i_BookingAvailable = "可借用";
$i_BookingUnavailable = "不可借用";
$i_BookingAppliedByU = "你原来的申请.";
$i_BookingPending = "审批中";
$i_BookingSingleBooking = "单一预订纪录";
$i_BookingPeriodicBooking = "定期预订纪录";
$i_BookingPeriodicDates = "预订的日期";
$i_BookingStatusArray = array(
"审批中",
"已取消",
"已取",
"已退还",
"已批准",
"已拒绝"
);
$i_BookingStatusImageArray = array(
"审批中",
"已取消",
"已取",
"已退还",
"已批准",
"已拒绝"
);
$i_BookingType_EveryDay = "每天";
$i_BookingType_EverySeparate = "每";
$i_BookingType_Days = "天";
$i_BookingType_Every = "逢";
$i_BookingType_Weekdays = "周日";
$i_BookingType_Cycleday = "循环日";
$i_BookingType_PleaseSelect = "请选择";
$i_BookingPeriodicConfirm = "确定定期重复预订纪录申请";
$i_BookingPeriodic_confirmMsg1 = "预订一览 :";
$i_BookingPeriodic_confirmMsg2 = "您希望预订的项目的状态如下:";
$i_BookingPeriodic_confirmMsg3 = "你所申请的项目未必会全部获批准. 确定提交申请?";
$i_BookingPeriodic_NoMatch = "没有日期符合你所选的条件.";
$i_Booking_TooLate1 = "你需要在 ";
$i_Booking_TooLate2 = "  日前预订此项目.";
$i_Booking_TodayOrLater = "你只能作今天或以后预订.";
$i_Booking_EndDateWrong = "结束日期必须是开始日期或以后.";
$i_Booking_sepdayswrong = "日数必须为正整数";
$i_Booking_PlsWeekDays = "请选择周日.";
$i_Booking_PlsCycleDays = "请选择循环日.";
$i_BookingWrongPeriodicDateEdit = "开始日期必须为 ";
$i_BookingWrongPeriodicDateEdit2 = "或以后.";
$i_BookingWrongPeriodicEndDate = "结束日期必须为 ";
$i_BookingWrongPeriodicEndDate2 = "或以前.";
$i_Booking_indexTitle = "<img border=0 src=/images/index/resources_glass_b5.gif>";
$i_Booking_Period = "期间";
$i_Booking_filter = "只显示";
$i_Booking_ViewSshool = "正常时段";
$i_Booking_ViewOther = "特别时段";
$i_Booking_Weekday = array (
"<img src=/images/resourcesbooking/week_sun_b5.gif>",
"<img src=/images/resourcesbooking/week_mon_b5.gif>",
"<img src=/images/resourcesbooking/week_tue_b5.gif>",
"<img src=/images/resourcesbooking/week_wed_b5.gif>",
"<img src=/images/resourcesbooking/week_thu_b5.gif>",
"<img src=/images/resourcesbooking/week_fri_b5.gif>",
"<img src=/images/resourcesbooking/week_sat_b5.gif>"
);
$i_Booking_Year = "年";
$i_Booking_BookingRecords = "预订纪录";
$i_Booking_Time = "时间";
$i_Booking_ReservedBy = "预订者";
$i_Booking_ClearReject = "清除被拒借用项目纪录";
$i_Booking_Signal = array("",
"<font color=green>已新增纪录</font>",
"<font color=red>所选之时间已被预订</font>",
"<font color=green>已更新纪录</font>",
"<font color=red>你没有权限预订本项目</font>",
"<font color=green>已取消纪录</font>",
"<font color=red>没有日期符合你所选的条件</font>",
"<font color=red>日期有误</font>",
"<font color=green>已清除纪录</font>"
);
$i_Booking_ArchiveConfirm = "你确定要保存?";
$i_Booking_ViewArchive = "检视已保存纪录";
$i_Booking_ArchiveUser = "申请人";
$i_Booking_ArchiveLastAction = "最后更新时间";
$i_Booking_ArchiveBooking = "保存资源预订纪录";
$i_Booking_DateSelectOr = "或使用 ";
$i_BookingLegendFull = "所有时段已被预留";
$i_BookingLegendHalf = "部份时段可供借用";
$i_BookingLegendFree = "所有时段可供借用";
$i_BookingItemList = "/images/resourcesbooking/itemlist_b5.gif";
$i_BookingBanList = "限制用户名单";
$i_BookingBanList_Instruction = "请输入限制用户的登入名称. <br>每一行输入一个登入名称.<br>这些用户不能再新增预订纪录, 但已申请的纪录则不受影响.<br>";

$i_EmailWebsite = "内联网网址";
$i_EmailWebmaster = "管理人电子邮件";

$i_AdminLogin = "管理人名称";
$i_AdminPassword = "管理人密码";
$i_AdminRights = "功能权限设定";

$i_RbpsTitle = "上课节数名称";
$i_RbpsTimeStart = "开始时间";
$i_RbpsTimeEnd = "结束时间";

$i_EventTitle = "题目";
$i_EventDescription = "内容";
$i_EventDate = "事项日期";
$i_EventVenue = "地点";
$i_EventNature = "性质";
$i_EventRecordType = "种类";
$i_EventRecordStatus = "状况";
$i_EventDateInput = "输入日期";
$i_EventDateModified = "最近修改日期";
$i_EventTypeSchool = "学校";
$i_EventTypeAcademic = "教学";
$i_EventTypeHoliday = "假期";
$i_EventTypeGroup = "小组";
$i_EventSkipCycle = "不计算循环日";
$i_EventImport_msg = "选择需要汇入的档案";
$i_EventImport_type = "事项的种类";
$i_EventImport_fileformat = "档案格式";
$i_EventImport_fileformat2 = "";
$i_EventImport_sample = "按此下载范本档案 (CSV 格式)";
$i_EventImport_notes = "注意日期必须为 YYYY-MM-DD 或 YYYY/MM/DD 格式. 请在储存CSV 档前, 在 Microsoft Excel 中设定日期格式.";
$i_Events = "事项";
$i_EventTypeString = array (
"学校事项",
"教学事项",
"假期",
"小组事项"
);
$i_EventAdminRight = "您不能编辑或删除由系统管理员建立的项目.";
$i_EventList = "<img src=$image_path/index/btn_eventlist.gif border=0>";
$i_EventShowAll = "<img border=0 src=$image_path/index/el_btn_allevents.gif width=76 height=22>";
$i_EventCloseImage = "<img border=0 src=$image_path/index/el_btn_close.gif width=14 height=14>";
$i_EventAll = "所有$i_Events";
$i_EventUpcoming = "即将来临的$i_Events";
$i_EventPast = "昔日$i_Events";
$i_EventPoster = "发起人";
$i_EventPublicInstruction = "如果你欲把此$i_Events"."显示给所有用户, 请把小组一栏留空.";
$i_EventAcademicYear = "学年";

$i_RecordDate = "纪录日期";

$i_GroupTitle = "小组名称";
$i_GroupDescription = "小组简介";
$i_GroupRecordType = "种类";
$i_GroupRecordStatus = "状况";
$i_GroupDateInput = "输入日期";
$i_GroupDateModified = "最近修改日期";
$i_GroupURL = "网址";
$i_GroupSetAdmin = "设定为管理员";
$i_GroupUnsetAdmin = "取消管理员";
$i_GroupAssignAdminRight = "设定小组管理员权限";
$i_GroupAssignAdminRight_SetAdminRightFor = "设定此用户为小组管理员";
$i_GroupAssignAdminRight_AvailableAdminRights = "可选之权限";
$i_GroupAssignAdminRight_AllRights = "所有权限";
$i_GroupKey = "为本小组管理员.";
$i_GroupQuota = "储存量 (MBytes)";
$i_GroupQuotaIsInt = "储存量必须为正整数.";
$i_GroupStorageUsage1 = "使用中";
$i_GroupStorageUsage2 = "(KB)";
$i_GroupSettingsBasicInfo = "基本资料";
$i_GroupSettingsMemberList = "组员名单";
$i_GroupSettingsAnnouncement = "小组宣布";
$i_GroupSettingsEvent = "小组活动";
$i_GroupSettingsSurvey = "小组问卷调查";

$i_GroupSettingTabs = array(
$i_GroupSettingsBasicInfo,
$i_GroupSettingsMemberList,
$i_GroupSettingsAnnouncement,
$i_GroupSettingsEvent);

$i_GroupAdminRightInternalAnnouncement = "内部宣布";
$i_GroupAdminRightAllAnnouncement = "所有宣布 (包括公开及内部)";
$i_GroupAdminRightInternalEvent = "小组内部事项";
$i_GroupAdminRightAllEvent = "所有事项 (包括公开及内部)";
$i_GroupAdminRightTimetable = "时间表";
$i_GroupAdminRightBulletin = "讨论栏";
$i_GroupAdminRightLinks = "共享网址";
$i_GroupAdminRightFiles = "共享档案";
$i_GroupAdminRightQB = "题目库管理";
$i_GroupAdminRightInternalSurvey = "内部问卷调查";
$i_GroupAdminRightAllSurvey = "所有问卷调查 (包括公开及内部)";

$i_GroupPageDescription = "下列为全部与您相关的小组. 如需要与小组成员交换讯息及档案等, 请按相关的功能键.";
$i_GroupMemberImport = "按$button_import"."以增添新成员";
$i_GroupMemberNewRole = "按$button_new$i_admintitle_role"."以增添新$i_admintitle_role.";
$i_GroupMemberFunction = "如需要转换成员的$i_admintitle_role".", 请选择新$i_admintitle_role,并按下$button_update$i_admintitle_role"."键";
$i_GroupSettingDescription = "这里提供的资料将于学校组织版面展示.";
$i_GroupAnnounceRight = "公众宣布权限";
$i_GroupPublicToSchool = "可作公众宣布";
$i_GroupUseAllTools = "容许使用所有小组工具";
$i_GroupToolsAllowed = "小组工具使用权限";
$i_ClubToolsAllowed = "学会工具使用权限";
$i_GroupOnlyAcademicAllowed = "学术组别专用";
$i_GroupAdminRights = array(
"基本资料",
"管理会员名单",
"管理组内宣布",
"管理所有宣布 (包括对外)",
"管理组内活动",
"管理所有活动 (包括对外)",
"管理题目库");

$i_GroupCategorySettings = "小组类别";
$i_GroupCategoryName = "类别名称";
$i_GroupCategoryLastModified = "最近修改日期";
$i_GroupCategoryNewDefaultRole = "此类别的预设职位";

$i_Polling_Settings = "投票设定";
$i_PollingQuestion = "问题";
$i_PollingAnswerA = "选择A";
$i_PollingAnswerB = "选择B";
$i_PollingAnswerC = "选择C";
$i_PollingAnswerD = "选择D";
$i_PollingAnswerE = "选择E";
$i_PollingAnswerF = "选择F";
$i_PollingAnswerG = "选择G";
$i_PollingAnswerH = "选择H";
$i_PollingAnswerI = "选择I";
$i_PollingAnswerJ = "选择J";
$i_PollingReference = "相关网址";
$i_PollingDateStart = "开始";
$i_PollingDateEnd = "结束";
$i_PollingDateRelease = "结果公布日期";
$i_PollingDateRelease_remark = "结果将会在投票后<b>及</b>结果公布日期后公开";
$i_PollingRecordType = "种类";
$i_PollingRecordStatus = "状况";
$i_PollingDateInput = "输入日期";
$i_PollingDateModified = "最近修改日期";
$i_PollingWrontStart = "开始日期必须是今天或以后";
$i_PollingWrongEnd = "结束日期必须是开始日期或以后";
$i_PollingTopic = "最近题目";
$i_PollingPeriod = "投票期限";
$i_PollingResult = "投票结果";
$i_Polling_PastDescription = "请选择您有兴趣的题目观看投票结果.";
$i_Polling_Vote = "投票";

$i_eNews = "校园最新消息";
$i_eNews_Settings = "校园最新消息设定";
$i_eNews_AddTo = "增加";
$i_eNews_DeleteTo = "删除";
$i_eNews_Approval = "批核";
$i_eNews_Approval2 = "校园最新消息批核";

$i_ResourceCode = "资源编号";
$i_ResourceCategory = "资源种类";
$i_ResourceTitle = "项目";
$i_ResourceDescription = "内容";
$i_ResourceRemark = "备注";
$i_ResourceAttachment = "附件";
$i_ResourceRecordType = "种类";
$i_ResourceRecordStatus = "状况";
$i_ResourceDateInput = "输入日期";
$i_ResourceDateModified = "最近修改日期";
$i_ResourceDateStart = "开始日期";
$i_ResourceDateEnd = "结束日期";
$i_ResourcePeriodType = "每逢";
$i_ResourceDateStartWrong = "开始日期必须是今天或以后";
$i_ResourceDateEndWrong = "结束日期必须是开始日期或以后";
$i_ResourceDaysBefore = "提早预订期限(日)";
$i_ResourceTimeSlot = "时间表";
$i_ResourceDaysBeforeWarning = "必须为正整数或 0.";
$i_ResourceImportFile = "请选择档案";
$i_ResourceImportNote = "备注";
$i_ResourceImportNote1 = "<ul><li>时间表名称必须完全正确, 否则资料不能汇入
<li><a href=javascript:newWindow('tscheme.php',0)>检视时间表内容</a>
<li><a href=itemsample.csv> 下载范本档案 (CSV 格式)</a></ul>";
$i_ResourceViewScheme = "<a href=javascript:newWindow('tscheme.php',0)>检视时间表内容</a>";
$i_ResourcePopScheme = "学校时间表";
$i_ResourceAutoApprove = "自动批准?";
$i_ResourceSelectCategory = "种类";
$i_ResourceSelectItem = "资源";
$i_ResourceSlotChangeAlert = "更改时间表或会令已申请之预订纪录有误, 是否继续?";
$i_ResourceSlotChangeAlert2 = "(预订者将不会收到电邮通知)";
$i_ResourceResetBooking = "重设资源预订纪录";
$i_ResourceResetBooking2 = "取消所有此资源的预订纪录 (今天或以后的预订纪录会被删除,过去的纪录会被保存).<br> $i_ResourceSlotChangeAlert2";
$i_ResourceSelectAllCat = "全部种类";
$i_ResourceSelectAllSituations = "所有发放原因";
$i_ResourceSelectAllItem = "全部资源";
$i_ResourceSelectAllResourceCode = "全部资源编号";
$i_ResourceSelectAllUser = "全部皇请人";
$i_ResourceSelectAllStatus = "全部状况";

$i_RoleTitle = "职位名称";
$i_RoleDescription = "简介";
$i_RoleRecordType = "种类";
$i_RoleRecordStatus = "状况";
$i_RoleDateInput = "输入日期";
$i_RoleDateModified = "最近修改日期";
$i_RoleDefault = "预设值";
$i_RoleNewPrompt = "请输入新职位名称";

$i_TimetableTitle = "题目";
$i_TimetableDescription = "内容";
$i_TimetableURL = "附件";
$i_TimetableRecordType = "种类";
$i_TimetableRecordStatus = "状况";
$i_TimetableDateInput = "输入日期";
$i_TimetableDateModified = "最近修改日期";
$i_Timetable_type0 = "使用学校时间表 <a href=\"/home/school/timetable/school_timetable.csv\">[范本 (CSV 格式)]</a>";
$i_Timetable_type1 = "使用自订时间表 <a href=\"/home/school/timetable/self_timetable.csv\">[范本 (CSV 格式)]</a>";
$i_Timetable_IndexInstruction = "按编辑键, 然后填写时间表的内容. 您可以自行定义时间表, 按汇入键上载文件.";

$i_UserLogin = "内联网帐号";
$i_UserPassword = "密码";
$i_UserEmail = "电子邮件";
$i_UserFirstName = "名字";
$i_UserLastName = "姓氏";
$i_UserChineseName = "中文名称";
$i_UserEnglishName = "英文名称";
$i_UserNickName = "别名";
$i_UserDisplayName = "显示名称";
$i_UserTitle = "称谓";
$i_UserGender = "性别";
$i_UserDateOfBirth = "出生日期";
$i_UserHomeTelNo = "住宅电话";
$i_UserOfficeTelNo = "公司";
$i_UserMobileTelNo = "手提电话";
$i_UserMobileSMSNotes = "<br>(此流动电话号码会用作接收SMS 短讯之用. 如果此用户不会接收任何SMS 短讯, 请留空此栏.)";
$i_UserFaxNo = "传真号码";
$i_UserICQNo = "ICQ";
$i_UserURL = "个人网站网址";
$i_UserAddress = "地址";
$i_HKID = "身份证号码";
$i_STRN = "学生编号(STRN)";
$i_UserCountry = "国家";
$i_UserInfo = "资料";
$i_UserRemark = "备注";
$i_UserClassNumber = "班号";
$i_UserClassName = "班别";
$i_UserClassLevel = "级别";
$i_UserLastUsed = "最后使用";
$i_UserLastModifiedPwd = "最后更改密码";
$i_UserRecordType = "种类";
$i_UserRecordStatus = "状况";
$i_UserDateInput = "输入日期";
$i_UserDateModified = "最近修改日期";
$i_UserPhoto = "相片";
$i_UserPhotoDelete = "删除现时相片";
$i_UserPhotoGuide = "相片只能以'.JPG'、'.GIF' 或'.PNG'的格式上载，相片大小也规定为100 X 130 pixel (阔 x 高)。";
$i_ClassNameNumber = "班别 (班号)";
$i_UserResetPassword = "重设密码";
$i_UserNotMustChangePasswd = "(如果你不需要更改密码, 请留空以上三项.)";
$i_UserName = "姓名";
$i_UserChineseName = "中文姓名";
$i_UserEnglishName = "英文姓名";
$i_UserOpenWebmail = "替新用户开设网上电邮户口";
$i_UserProfileDescription = "您的个人资料, 联络方法及其他留言将于您所有的 eClass 教室中出现. 所有内容都可以选择是否填写.";
$i_UserProfilePersonal = "个人资料";
$i_UserProfileContact = "联络资料";
$i_UserProfileMessage = "留言";
$i_UserProfileLogin = "登入密码";
$i_UserProfileEmailUse = "用于接收系统邮件, 所以只可输入一个有效的电子邮件，错误的输入有可能导致你使用时出现问题！";
$i_UserProfilePwdChangeRule = "如果您不想更改您的密码, 请勿填写以下内容.";
$i_UserProfilePwdUse = "用作确认您的身份";
$i_UserStudentName = "学生姓名";
$i_UserProfile_Change_Notice="如需更改此项资料，请联络系统管理员。";

$i_UserImportGraduate = "$button_import"."离校名单";
$i_UserGraduate_ImportInstruction = "<b><u>档案说明:</u></b><br>
第一栏 : 用户的$i_UserLogin.<br>
";
$i_UserParentLink = "连结子女";
$i_UserParentLink_SelectClass = "选择班别";
$i_UserParentLink_SelectStudent = "选择子女";
$i_UserParentWithLink = "有连结";
$i_UserParentWithNoLink = "没有连结";
$i_UserParent_LinkedChildren = "的子女名单";
$i_UserAccountOpenForNewOnly = "新用户: 系统会自动开启 Linux 系统户口(用作FTP/Email). <br>现有用户: 权限会被更新, 但基于系统保安理由, 不会再开启Linux 户口.";
$i_UserRemoveConfirm = "注意有关以下户口会一并删除";
$i_UserLinuxAccount = "Linux 户口 (用作外来电邮及个人文件柜)";
$i_UserLDAPAccount = "LDAP 户口 (用作登入检查)";
$i_UserRemoveConfirmAsk = "你确定要删除刚才选择的用户?";
$i_UserDefaultExport = "汇出预设格式(修改资料后可再汇入至系统)";
$i_UserDefaultExportWarning = "如要汇出预设格式(修改资料后可再汇入至系统)，请选择一个身份";
$i_UserDisplayTitle_English = "显示称谓 (英)";
$i_UserDisplayTitle_Chinese = "显示称谓 (中)";
$i_UserWarningiPortfolio = "<font color=red>请注意: 学生档案及iPortfolio的资料将会在删除学生户口时被整存, 整存后的户口资料将不能复元及更改</font>";
//$i_UserRemoveStop_PaymentBalanceNonZero = "此动作已停止, 因有部份用户的户口仍有结存. 请先作退款动作.";
$i_UserRemoveStop_PaymentBalanceNonZero = "以下用户户口仍有结余或尚有未缴款项。操作终止。";

$i_FileSystemFiles = "档案";
$i_FileSystemName = "名称";
$i_FileSystemSize = "大小";
$i_FileSystemModified = "最近修改日期";
$i_FileSystemUp = "上移一层";

$i_eClassCourseID = "课程ID";
$i_eClassCourseCode = "课程编号";
$i_eClassCourseCodePrefix = "课程编号称谓";
$i_eClassCourseCodePrefixGuide = "如课程编号\"C003\"的\"C\"";
$i_eClassCourseSettings = "课程设定";
$i_eClassCourseProposal = "根据现有班别及科目，组成以下可开设的教室";
$i_eClassCourseProposalNotes = "注意:<br> 系统会根据教师职务设定，自动将老师加入适当教室。请确定已完成教师职务设定。";
$i_eClassCourseConfirm = "你是否确定要加入已选课程?";
$i_eClassCourseProcessing = "正在开设教室: ";
$i_eClassCourseWait = "请等候 ... ";
$i_eClassCourseDone = "完成!";
$i_eClassCourseImportStudent = "加入学生";
$i_eClassLicenseExceed = "你没有足够教室许可证去开设已选教室。";
$i_eClassCourseNoneWarn = "请最少选择一个课程。";
$i_eClassCourseProposalNone = "因为没有班别或科目纪录，所以不能建议任何教室。你应该先在[学校设定]输入班别及科目。";
$i_eClassCourseName = "课程名称";
$i_eClassCourseDesc = "课程概括";
$i_eClassIsGuest = "访客进入";
$i_eClassNumUsers = "用户数目";
$i_eClassMaxUser = "用户限额";
$i_eClassMaxStorage = "容量限额 (MB)";
$i_eClassStorage = "容量 (MB)";
$i_eClassFileNo = "档案数目";
$i_eClassFileSize = "档案大小 (MB)";
$i_eClassFileSpace = "尚余限额 (MB)";
$i_eClassHitsTotal = "总点击次数";
$i_eClassHitsTeacher = "老师点击次数";
$i_eClassHitsStudent = "学生点击次数";
$i_eClassHitsAssistant = "助教点击次数";
$i_eClassInputdate = "开设日期";
$i_eClassCreatorBy = "开设人";
$i_eClassLicenseDisplayN1 = "你的一般教室许可证有 ";
$i_eClassLicenseDisplayR1 = "你的阅读室许可证有 ";
$i_eClassLicenseDisplay2 = " 个，尚有 ";
$i_eClassLicenseDisplay3 = " 个可供使用。";
$i_eClassLicenseDisplay4 = "你已使用的许可证为 ";
$i_eClassLicenseDisplay5 = " 个。";
$i_eClassLicenseMsg1 = "教室许可证";
$i_eClassLicenseMsg2 = "总数";
$i_eClassLicenseMsg3 = "已开设";
$i_eClassLicenseMsg4 = "尚余";
$i_eClassLicenseFull = "你已使用所有教室许可证, 你不可再新增任何教室.";
$i_eClassUserQuotaMsg1 = "用户限额为";
$i_eClassUserQuotaMsg2 = "，尚有";
$i_eClassUserQuotaMsg3 = "个空位。";
$i_eClassUserQuotaMsg4 = "这教室已使用的用户数目为";
$i_eClassUserImport1 = "第一步：用户分组";
$i_eClassUserImport2 = "第二步：选择用户";
$i_eClassStorageDisplay1 = "储存限额为";
$i_eClassStorageDisplay2 = " MB ﹐尚有";
$i_eClassStorageDisplay3 = "MB 可供使用。";
$i_eClassStorageDisplay4 = "你已使用的储存量为";
$i_eClassStorageDisplay5 = " MB.";
$i_eClassStorageDisplay6 = " 个档案及 ";
$i_eClassStorageDisplay7 = "份文件。";
$i_eClassAdminFileRight = "开放所有档案权限";
//$i_eClass_Admin_MgmtCenter = "eClass 管理中心";
$i_eClass_Admin_MgmtCenter = "特别室";
$i_eClass_Admin_Settings = "eClass 设定";
$i_eClass_Admin_Stats = "eClass 统计";
$i_eClass_Admin_AssessmentReport = "评估报告";
$i_eClass_Admin_AllAssessments = "全部评估";
$i_eClass_Admin_NoStudentsinAnyeClass = "没有学生在任何网上教室中";
$i_eClass_Admin_OnlyAssessmentsPassedDeadline = "只包括到期评估";
$i_eClass_Admin_Shared_Files = "教师共享地带";
$i_eClass_Admin_stats_login = "登入纪录";
$i_eClass_Admin_stats_participation = "参与纪录";
$i_eClass_Admin_stats_files = "文档使用纪录";
$i_eClass_Identity = "身份";
$i_eClass_ClassLink = "所属班别";
$i_eClass_SubjectLink = "所属科目";
# eclass homework list $i_eClass_ClassSubjectNote = "(如需连结家课纪录表, 以上两项必须填写)";
$i_eClass_normal_course = "一般教室";
$i_eClass_reading_room = "阅读室";
$i_eClass_elp = "eClass 学习资源室";
$i_eClass_elp_courseware = "课件";
$i_eClass_ssr = "网上评估室";
$i_eClass_ssr_type = "评估范本";
$i_eClass_course_type = "类别";
$i_eClass_group_teacher = "Teacher 老师";
$i_eClass_group_assistant = "Assistant 助教";
$i_eClass_group_student = "Student 学生";
$i_eClass_group_default = "Default Group 预设小组";
$i_eClass_set_teacher_rights = "设定老师权能";
$i_eClass_eclass_group = "设定eClass小组";
$i_eClass_teacher_rights_1 = "管理教室";
$i_eClass_teacher_rights_2 = "批核及批改读书报告";
$i_eClass_teacher_rights_3 = "Student Profile Team";
$i_eClass_Tools = "内置工具";
$i_eClass_Tool_LicenseLeft = "尚余许可证";
$i_eClass_Tool_EquationEditor = "方程式编辑器";
$i_eClass_Tool_EquationEditorUsing = "正使用方程式编辑器";
$i_eClass_management_enable = "让老师使用eClass 管理";
$i_eClass_teacher_open_course = "让老师在eClass目录页自行开eClass";
$i_eClass_email_function = "电邮功能";
$i_eClass_email_function_enable = "可使用eClass寄出电邮";
$i_eClass_identity_helper = "助手";
$i_eClass_quota_exceeded = "汇入失败。用户限额不足。请增加此网上教室的用户限额。";
$i_eClass_no_user_selected = "请最少选择一个用户!";
$i_eClass_batch_new = "档案新增";
$i_eClass_batch_import = "汇入班名单";
$i_eClass_ImportInstruction_batch_new = "
第一栏 : $i_eClassCourseCode<br>
第二栏 : $i_eClassCourseName<br>
第三栏 : $i_eClassMaxUser<br>
第四栏 : $i_eClassMaxStorage<br>
第五栏 : KLA 编号
";
$i_eClass_CategoryList = "Key Learning Areas Code Mapping";
$i_eClass_ImportInstruction_batch_member = "
第一栏 : $i_UserLogin<br>
第二栏 : $i_eClassCourseID<br>
第三栏 : 小组名称
";

$i_eClass_planner_function = "教学计划";
$i_eClass_planner_type_color = "设定事项分类颜色（留空当不使用）";
$i_eClass_planner_type = "类别";
$i_eClass_planner_color = "颜色";
$i_eClass_survey_deadline = "问卷限期";

$i_eClass_function_access = "使用功能";
$i_eClass_parent_access = "家长参与";
$i_eClass_portfolio_lang_c = "（中）";
$i_eClass_portfolio_lang_e = "（英）";

$i_eClass_Bulletin_BadWords_Instruction_top = "你可以输入系统限制的字句. 在此出现的字句在讨论栏将会被 *** 取代.";
$i_eClass_MyeClass_Content = "学习内容";
$i_eClass_MyeClass_Bulletin = "讨论栏";
$i_eClass_MyeClass_Announcement = "宣布";
$i_eClass_MyeClass_SharedLinks  = "共享网址";

$i_ssr_anonymous_warning = "「不记名」设定于问卷被作答后将不能取消，除非先清除所有已收回的问卷！";
$i_ssr_anonymous_guide = "若要取消「不记名」设定，请先清除所有已收回的问卷。";


$i_GroupRole = array("基本组别","行政","学术","班别","社级","课外活动","其他");
$i_GroupRoleOrganization = array("identity","admin","academic","class","house","eca","misc");

# $i_frontpage_separator = "<img src=/images/frontpage/triangleicon.gif border=0 align=absmiddle hspace=5>";
$i_frontpage_separator = "<img src=/images/arrow.gif border=0 align=absmiddle hspace=5>";
$i_frontpage_menu_schoolinfo = "<img src=/images/header/icon_schinfo.gif border=0>";
$i_frontpage_menu_schoolinfo_organization = "组织";
$i_frontpage_menu_schoolinfo_groupinfo = "小组资讯";
$i_frontpage_menu_schoolinfo_campus_gallery = "校园相簿"; #add by kelvin ho 2008-12-23
$i_frontpage_menu_schoolinfo_homework = "家课纪录表";
$i_frontpage_menu_campusmail = "<img src=/images/header/icon_campusmail.gif border=0>";
$i_frontpage_menu_campusmail_inbox = "收件箱";
$i_frontpage_menu_campusmail_compose = "撰写邮件";
$i_frontpage_menu_campusmail_title = "校园信箱";
$i_frontpage_menu_myinfo = "<img src=/images/header/icon_myinfo.gif border=0>";
$i_frontpage_menu_myinfo_profile = "更新个人资料";
$i_frontpage_menu_myinfo_email = "电邮设定";
$i_frontpage_menu_myinfo_password = "密码设定";
$i_frontpage_menu_myinfo_bookmark = "我的书签";
$i_frontpage_menu_myinfo_cabinet = "私人档案柜";
$i_frontpage_menu_classes = "<img src=/images/header/icon_classes.gif border=0>";
$i_frontpage_menu_resourcebooking = "<img src=/images/header/icon_resourcesbking.gif border=0>";
$i_frontpage_menu_resourcebooking_myrecords = "我的预订纪录";
$i_frontpage_menu_resourcebooking_reserved = "预订纪录";
$i_frontpage_menu_eclass = "<img src=/images/header/icon_eclass.gif border=0>";
$i_frontpage_menu_campustv_title = "校园电视";
$i_frontpage_menu_octopus = "校园通";
$i_frontpage_menu_library = "<img src=/images/header/icon_library.gif border=0>";
$i_frontpage_menu_library_title = "图书馆";
$i_frontpage_menu_logo = "<a href=/home/><img src=/images/frontpage/logo_big5.gif border=0 width=112 height=37></a>";
$i_frontpage_menu_home = "首页";
$i_frontpage_menu_exit = "<img src=/images/header/icon_logout_b5.gif border=0>";
$i_frontpage_menu_eclass_mgt = "eClass 管理";

$i_frontpage_button_result = "<img src=/images/frontpage/button_result_big5.jpg border=0 width=51 height=22>";
$i_frontpage_button_vote = "<img src=/images/frontpage/button_vote_big5.jpg border=0 width=50 height=22>";
$i_frontpage_button_cancel = "<img src=/images/frontpage/button_cancel_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_reset = "<img src=/images/frontpage/button_reset_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_save = "<img src=/images/frontpage/button_save_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_submit = "<img src=/images/frontpage/button_submit_big5.gif border=0 width=75 height=28>";
$i_frontpage_button_update = "<img src=/images/frontpage/button_update_big5.gif border=0 width=75 height=28>";
$i_frontpage_image_result = "/images/frontpage/button_result_big5.jpg";
$i_frontpage_image_vote = "/images/frontpage/button_vote_big5.jpg";
$i_frontpage_image_cancel = "/images/frontpage/button_cancel_big5.gif";
$i_frontpage_image_reset = "/images/frontpage/button_reset_big5.gif";
$i_frontpage_image_save = "/images/frontpage/button_save_big5.gif";
$i_frontpage_image_submit = "/images/frontpage/button_submit_big5.gif";
$i_frontpage_image_update = "/images/frontpage/button_update_big5.gif";
$i_frontpage_day = array("日","一","二","三","四","五","六");
$i_frontpage_classes = "教室";
$i_frontpage_eclass = "网上教室";
$i_frontpage_eclass_courses = "教室名称";
$i_frontpage_eclass_lastlogin = "最后登入";
$i_frontpage_eclass_whatsnews = "新消息";
$i_frontpage_myinfo = "我的资讯";
$i_frontpage_myinfo_profile = "更新个人资料";
$i_frontpage_myinfo_email = "电邮设定";
$i_frontpage_myinfo_password = "密码设定";
$i_frontpage_myinfo_password_old = "旧密码";
$i_frontpage_myinfo_password_new = "新密码";
$i_frontpage_myinfo_password_retype = "确认新密码";
$i_frontpage_myinfo_password_mismatch = "密码不符";
$i_frontpage_myinfo_bookmark = "我的书签";
$i_frontpage_myinfo_cabinet = "私人档案柜";
$i_frontpage_myinfo_title_profile = "<img src=/images/myaccount_personalinfo/title_personalinfo_b5.gif>";
$i_frontpage_myinfo_title_email = "<img src=/images/frontpage/title_myinfo_email_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_password = "<img src=/images/frontpage/title_myinfo_password_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_bookmark = "<img src=/images/frontpage/title_myinfo_bookmark_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_myinfo_title_cabinet = "<img src=/images/frontpage/title_myinfo_cabinet_big5.gif border=0 width=286 height=38 hspace=5 vspace=5>";
$i_frontpage_rb = "资源预订";
$i_frontpage_rb_msg = "资源预订纪录";
$i_frontpage_rb_title_add = "<img src=/images/frontpage/title_resourcebooking_add_big5.gif border=0 width=313 height=42 hspace=5 vspace=5>";
$i_frontpage_rb_title_edit = "<img src=/images/frontpage/title_resourcebooking_edit_big5.gif border=0 width=313 height=42 hspace=5 vspace=5>";
$i_frontpage_rb_title_form = "<img src=/images/frontpage/resourcebooking_form_title_big5.gif border=0 width=275 height=35>";
$i_frontpage_rb_title = "<img src=/images/resourcesbooking/title_b5.gif>";
$i_frontpage_rb_title_reserved = "<img src=/images/resourcesbooking/title_reservedrecords_b5.gif>";
$i_frontpage_rb_title_mybooking = "<img src=/images/frontpage/mybooking_title_big5.gif border=0 width=170 height=30>";
$i_frontpage_rb_title_mybookeditems = "<img src=/images/frontpage/mybookeditems_title_big5.gif border=0 width=414 height=50>";
$i_frontpage_rb_title_allbookeditems = "<img src=/images/frontpage/allbookeditems_title_big5.gif border=0 width=552 height=50>";
$i_frontpage_rb_title_allavailableitems = "<img src=/images/frontpage/allavailableitems_title_big5.gif border=0 width=552 height=50>";
$i_frontpage_rb_date = "预订日期";
$i_frontpage_rb_period = "时期";
$i_frontpage_rb_note = "备忘录";
$i_frontpage_rb_lastmodified = "最近修改日期";
$i_frontpage_home_bookings_title = "<img src=/images/frontpage/home_bookings_big5.gif border=0 width=163 height=37>";
$i_frontpage_home_eclass_title = "<img src=/images/frontpage/home_eclass_big5.gif border=0 width=163 height=37>";
$i_frontpage_home_mygroup_title = "<img src=/images/frontpage/home_mygroup_big5.gif border=0 width=163 height=37>";
$i_frontpage_home_schoolannouncement_title = "<img src=/images/index/announcement_glass_b5.gif>";
$i_frontpage_home_cal_legend = "<img src=/images/frontpage/cal_legend_big5.gif border=0 width=158 height=97 vspace=5>";
$i_frontpage_home_cal_legend_academicevents = "<img src=/images/frontpage/cal_legend_academicevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_schoolevents = "<img src=/images/frontpage/cal_legend_schoolevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_holidays = "<img src=/images/frontpage/cal_legend_holidays_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_legend_groupevents = "<img src=/images/frontpage/cal_legend_groupevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_academicevents = "<img src=/images/frontpage/cal_white_legend_academicevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_schoolevents = "<img src=/images/frontpage/cal_white_legend_schoolevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_holidays = "<img src=/images/frontpage/cal_white_legend_holidays_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_cal_white_legend_groupevents = "<img src=/images/frontpage/cal_white_legend_groupevents_big5.gif border=0 width=158 height=22>";
$i_frontpage_home_title_vote = "<img src=/images/index/pollingtab_b5.gif width=69 height=21>";
$i_frontpage_home_title_vote2 = "<img src=/images/index/pollingtab2_b5.gif width=203 height=6>";
$i_frontpage_home_title_pastvote = "<img border=0 src=/images/index/pollingicon.gif width=13 height=19>";
$i_frontpage_schoolinfo = "校园资讯";
$i_frontpage_schoolinfo_organization = "组织";
$i_frontpage_schoolinfo_groupinfo = "小组资讯";
$i_frontpage_schoolinfo_groupinfo_event = "事项";
$i_frontpage_schoolinfo_groupinfo_announcement = "宣布";
$i_frontpage_schoolinfo_groupinfo_period = "日期范围";
$i_frontpage_schoolinfo_groupinfo_infotype = "资讯类型";
$i_frontpage_schoolinfo_groupinfo_group = "小组";
$i_frontpage_schoolinfo_groupinfo_egroup = "小组";
$i_frontpage_schoolinfo_groupinfo_group_timetable = "时间表";
$i_frontpage_schoolinfo_groupinfo_group_chat = "交谈室";
$i_frontpage_schoolinfo_groupinfo_group_bulletin = "讨论栏";
$i_frontpage_schoolinfo_groupinfo_group_links = "共享网址";
$i_frontpage_schoolinfo_groupinfo_group_files = "共享档案";
$i_frontpage_schoolinfo_groupinfo_group_settings = "小组管理";
$i_frontpage_schoolinfo_groupinfo_group_members = "组员名单";
$i_frontpage_schoolinfo_groupinfo_group_photoalbum = "相簿";
$i_frontpage_schoolinfo_groupinfo_thisweek = "这个星期";
$i_frontpage_schoolinfo_groupinfo_thismonth = "这个月";
$i_frontpage_schoolinfo_groupinfo_today = "今天";
$i_frontpage_schoolinfo_groupinfo_today_onward = "由今天起";
$i_frontpage_schoolinfo_popup_organization = "<img src=/images/frontpage/popup_schoolInfo_organization_big5.gif border=0 width=216 height=45>";
$i_frontpage_schoolinfo_title_organization = "<img src=/images/organization/title_b5.gif border=0>";
$i_frontpage_schoolinfo_title_group = "<img src=/images/frontpage/title_schoolInfo_group_big5.gif border=0 width=255 height=42 hspace=5 vspace=5>";
$i_frontpage_schoolinfo_title_homework = "<img src=/images/frontpage/title_schoolinfo_homework_big5.gif width=289 height=45 border=0 hspace=5 vspace=5>";
$i_frontpage_schoolinfo_organization_misc_t = "<img src=/images/frontpage/schoolInfo_category_misc_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_misc_m = "<img src=/images/frontpage/schoolInfo_category_misc_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_misc_b = "<img src=/images/frontpage/schoolInfo_category_misc_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_eca_t = "<img src=/images/frontpage/schoolInfo_category_eca_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_eca_m = "<img src=/images/frontpage/schoolInfo_category_eca_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_eca_b = "<img src=/images/frontpage/schoolInfo_category_eca_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_house_t = "<img src=/images/frontpage/schoolInfo_category_house_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_house_m = "<img src=/images/frontpage/schoolInfo_category_house_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_house_b = "<img src=/images/frontpage/schoolInfo_category_house_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_class_t = "<img src=/images/frontpage/schoolInfo_category_class_title_big5.gif border=0 width=692 height=50>";
$i_frontpage_schoolinfo_organization_class_m = "<img src=/images/frontpage/schoolInfo_category_class_bg_m.gif border=0 width=692 height=5>";
$i_frontpage_schoolinfo_organization_class_b = "<img src=/images/frontpage/schoolInfo_category_class_bg_b.gif border=0 width=692 height=18>";
$i_frontpage_schoolinfo_organization_admin_t = "<img src=/images/frontpage/schoolInfo_category_admin_title_big5.gif border=0 width=383 height=50>";
$i_frontpage_schoolinfo_organization_admin_m = "<img src=/images/frontpage/schoolInfo_category_admin_bg_m.gif border=0 width=383 height=5>";
$i_frontpage_schoolinfo_organization_admin_b = "<img src=/images/frontpage/schoolInfo_category_admin_bg_b.gif border=0 width=383 height=18>";
$i_frontpage_schoolinfo_organization_academic_t = "<img src=/images/frontpage/schoolInfo_category_academic_title_big5.gif border=0 width=383 height=50>";
$i_frontpage_schoolinfo_organization_academic_m = "<img src=/images/frontpage/schoolInfo_category_academic_bg_m.gif border=0 width=383 height=5>";
$i_frontpage_schoolinfo_organization_academic_b = "<img src=/images/frontpage/schoolInfo_category_academic_bg_b.gif border=0 width=383 height=18>";
$i_frontpage_schoolinfo_homework = "家课纪录表";
$i_frontpage_schoolinfo_popup_misc_t = "<img src=/images/frontpage/schoolinfo_popup_misc_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_eca_t = "<img src=/images/frontpage/schoolinfo_popup_eca_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_house_t = "<img src=/images/frontpage/schoolinfo_popup_house_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_class_t = "<img src=/images/frontpage/schoolinfo_popup_class_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_admin_t = "<img src=/images/frontpage/schoolinfo_popup_admin_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_schoolinfo_popup_academic_t = "<img src=/images/frontpage/schoolinfo_popup_academic_tl_big5.gif border=0 width=95 height=44>";
$i_frontpage_welcome = "<img src=/images/index/welcometxt_b5.gif>";
$i_frontpage_currentpoll = "现有投票";
$i_frontpage_pastpoll = "昔日投票";
$i_frontpage_my_eclass_t = "<img src=/images/frontpage/eclass_t_big5.gif border=0 width=770 height=45>";
$i_frontpage_dir_eclass_t = "<img src=/images/frontpage/eclass_directory_top_big5.gif border=0 width=172 height=27>";
$i_frontpage_eclass_eclass_dir = "eClass目录";
$i_frontpage_eclass_eclass_my = "我的eClass";
$i_frontpage_campusmail_campusmail = "校园信箱";
$i_frontpage_campusmail_title_compose = "<img src=/images/campusmail/title_compose_b5.gif border=0>";
$i_frontpage_campusmail_title_inbox = "<img src=/images/campusmail/title_inbox_b5.gif border=0>";
$i_frontpage_campusmail_title_outbox = "<img src=/images/campusmail/title_outbox_b5.gif border=0>";
$i_frontpage_campusmail_title_draft = "<img src=/images/campusmail/title_draft_b5.gif border=0>";
$i_frontpage_campusmail_title_trash = "<img src=/images/campusmail/title_trash_b5.gif border=0>";
$i_frontpage_campusmail_icon_compose = "<img src=/images/frontpage/campusmail/campusmail_compose_big5.gif border=0>";
$i_frontpage_campusmail_icon_inbox = "<img src=/images/frontpage/campusmail/campusmail_inbox_big5.gif border=0>";
$i_frontpage_campusmail_icon_outbox = "<img src=/images/frontpage/campusmail/campusmail_outbox_big5.gif border=0>";
$i_frontpage_campusmail_icon_draft = "<img src=/images/frontpage/campusmail/campusmail_draft_big5.gif border=0>";
$i_frontpage_campusmail_icon_trash = "<img src=/images/frontpage/campusmail/campusmail_trash_big5.gif border=0>";
$i_frontpage_campusmail_compose = "撰写邮件";
$i_frontpage_campusmail_inbox = "收件箱";
$i_frontpage_campusmail_outbox = "寄件箱";
$i_frontpage_campusmail_draft = "草稿箱";
$i_frontpage_campusmail_trash = "垃圾箱";
$i_frontpage_campusmail_recipients = "收件人";
$i_frontpage_campusmail_subject = "标题";
$i_frontpage_campusmail_message = "内容";
$i_frontpage_campusmail_attachment = "附件";
$i_frontpage_campusmail_important = "列为重要信件";
$i_frontpage_campusmail_notification = "要求回复";
$i_frontpage_campusmail_size = "所占空间 (Kb)";
$i_frontpage_campusmail_size2 = "所占空间";
$i_frontpage_campusmail_read = "已阅读";
$i_frontpage_campusmail_unread = "未阅读";
$i_frontpage_campusmail_total = "总数";
$i_frontpage_campusmail_reply = "你的回复";
$i_frontpage_campusmail_i = "I";
$i_frontpage_campusmail_a = "A";
$i_frontpage_campusmail_date = "日期";
$i_frontpage_campusmail_subject = "标题";
$i_frontpage_campusmail_sender = "寄件人";
$i_frontpage_campusmail_status = "状况";
$i_frontpage_campusmail_folder = "收集箱";
$i_frontpage_campusmail_choose = "选择收件人";
$i_frontpage_campusmail_remove = "删除";
$i_frontpage_campusmail_attach = "加入附件";
$i_frontpage_campusmail_send = "传送";
$i_frontpage_campusmail_save = "储存草稿";
$i_frontpage_campusmail_reset = "重设";
$i_frontpage_campusmail_delete = "删除";
$i_frontpage_campusmail_close = "关闭";
$i_frontpage_campusmail_add = "加入";
$i_frontpage_campusmail_expand = "扩张";
$i_frontpage_campusmail_retore = "恢复";
$i_frontpage_campusmail_icon_important = " <img src=/images/campusmail/btn_important.gif border=0>";
$i_frontpage_campusmail_icon_notification = "<img src=/images/frontpage/campusmail/icon_notification.gif border=0>";
$i_frontpage_campusmail_icon_attachment = "<img src=/images/frontpage/campusmail/icon_attachment.gif border=0>";
$i_frontpage_campusmail_icon_read = "<img src=/images/icon_envelope_read.gif border=0>";
$i_frontpage_campusmail_icon_unread = "<img src=/images/icon_envelope_unread.gif border=0>";
$i_frontpage_campusmail_icon_status_new = "<img src=/images/frontpage/campusmail/status_new_big5.gif border=0>";
$i_frontpage_campusmail_icon_status_read = "<img src=/images/frontpage/campusmail/status_read_big5.gif border=0>";
$i_frontpage_campusmail_icon_status_reply = "<img src=/images/frontpage/campusmail/status_replied_big5.gif border=0>";
$i_frontpage_campusmail_attachment_t = "<img src=/images/frontpage/campusmail/attachment_top_big5.gif width=450 height=40 border=0>";
$i_frontpage_campusmail_attachment_m = "<img src=/images/frontpage/campusmail/attachment_middle.gif width=450 height=16 border=0>";
$i_frontpage_campusmail_attachment_b = "<img src=/images/frontpage/campusmail/attachment_bottom.gif width=450 height=32 border=0>";
$i_frontpage_campusmail_namelist_t = "<img src=/images/frontpage/campusmail/namelist_top_big5.gif width=450 height=43 border=0>";
$i_frontpage_campusmail_namelist_m = "<img src=/images/frontpage/campusmail/namelist_middle.gif width=450 height=14 border=0>";
$i_frontpage_campusmail_namelist_b = "<img src=/images/frontpage/campusmail/namelist_bottom.gif width=450 height=21 border=0>";
$i_frontpage_campusmail_select_category = "选择小组种类";
$i_frontpage_campusmail_select_group = "选择小组";
$i_frontpage_campusmail_select_user = "选择用户";
$i_frontpage_campusmail_notification_instruction = "寄件人要求你回复此讯息, 请输入回复信息或选取空格, 然后按传送.";
$i_frontpage_campusmail_notification_instruction_message_only = "寄件人要求你回复此讯息, 请输入回复信息, 然后按传送.";
$i_frontpage_campusmail_alert_not_reply = "你是否决定不回复此讯息?";
$i_frontpage_campusmail_new_mail = " 封新邮件";
$i_frontpage_survey = "问卷";
$i_frontpage_schedule = "日程表";
$i_frontpage_announcement = "宣布";
$i_frontpage_notes = "学习内容";
$i_frontpage_links = "共享网址";
$i_frontpage_bulletin = "讨论栏";
// Group Bulletin
$i_frontpage_bulletin_group = "组别";
$i_frontpage_bulletin_date = "时间";
$i_frontpage_bulletin_subject = "标题";
$i_frontpage_bulletin_author = "作者";
$i_frontpage_bulletin_message = "留言";
$i_frontpage_bulletin_no_record = "没有留言于此讨论栏";
$i_frontpage_bulletin_no_result = "对不起, 系统找不到您寻找的项目内容. 请尝试修改您用的关键字后再试一次.";
$i_frontpage_bulletin_email_alert = "以电邮回复作者";
$i_frontpage_bulletin_result = "结果";
$i_frontpage_bulletin_postnew = "新增标题";
$i_frontpage_bulletin_thread = "留言";
$i_frontpage_bulletin_replies = "回复";
$i_frontpage_bulletin_reference = "回复";
$i_frontpage_bulletin_postreply = "回复";
$i_frontpage_bulletin_latest = "最新留言时间";
$i_frontpage_bulletin_markallread = "全部标示成已阅读";
$i_frontpage_bulletin_alert_msg1 = "请填上文章标题";
$i_frontpage_bulletin_alert_msg2 = "请填上文章内容";
$i_frontpage_bulletin_alert_msg3 = "请填上重要字句";
$i_frontpage_bulletin_quote = "引用原文";
$i_frontpage_bulletin_instruction = "按新增标题键设立新的讨论题目. 如需检视及回复其他作者的留言, 请键入相关的标题.";

// Group Link
$i_LinkGroup = "组别";
$i_LinkUserName = "作者";
$i_LinkUserEmail = "电子邮件";
$i_LinkCategory = "种类";
$i_LinkTitle = "题目";
$i_LinkURL = "超链接";
$i_LinkKeyword = "重要字句";
$i_LinkDescription = "描述";
$i_LinkVoteNum = "投票次数";
$i_LinkRating = "平均分数";
$i_LinkDateInput = "输入日期";
$i_LinkDateModified = "最近修改日期";
$i_LinkSortBy = "排序方式";
$i_LinkNew = "新网址";
$i_LinkInstruction = "请按下CTRL键，用鼠标选择共享此链接的小组.";

// Group File
$i_FileGroup = "组别";
$i_FileUserName = "作者";
$i_FileUserEmail = "电子邮件";
$i_FileCategory = "种类";
$i_FileTitle = "档案名称";
$i_FileLocation = "档案位置";
$i_FileKeyword = "重要字句";
$i_FileDescription = "描述";
$i_FileDateInput = "输入日期";
$i_FileDateModified = "最近修改日期";
$i_FileSize = "档案大小(KB)";
$i_FileNew = "新档案";

// Campus Link
$i_CampusLinkTitle = "名称";
$i_CampusLinkURL = "超连结";
$i_CampusLinkDescription = "校园连结会在校园资讯中显示.";
$i_CampusLink_title = "校园连结";
$i_CampusLink_title_image = "<img src=/images/campus_links/title_b5.gif>";
// New CampusLink
$i_CampusLink_LinkName = "名称";
$i_CampusLink_LinkAddress = "地址";
$i_CampusLink_Description = "叙述";
$i_CampusLink_DisplayOrder = "显示次序";
$i_CampusLink_DisplayOrder_Desc = "1=最先, 10=最后";
$i_CampusLink_Identity = "身份";
$i_CampusLink_Identity_All = "全部";
$i_CampusLink_RecordType = "种类";
$i_CampusLink_RecordType_Normal = "一般";
$i_CampusLink_RecordType_Secure = "安全";

// Cycle
$i_CycleDay = "日子";
$i_CycleCycle = "循环";
$i_CycleOptions = "选项";
$i_CycleType = "显示类别";
$i_CycleStart = "开始日期";
$i_CycleMsg = "日子循环功能会显示于学校的行事历中。";
$i_CycleSelect = "请选择";
$i_CycleWeek = "日子";
$i_Cycle5Day = "五日";
$i_Cycle6Day = "六日";
$i_Cycle7Day = "七日";
$i_Cycle8Day = "八日";
$i_CycleSpecialArrangment = "特别安排";
$i_CycleSpecialForSatCounting = "以下日期, 星期六会计算循环日";
$i_CycleSpecialStart = "开始日期";
$i_CycleSpecialEnd = "结束日期";
$i_CycleSpecialNotes = "更新循环日设定后, 所有按循环日的定期资源预订申请将会随之更改, 请通知用户更新其资源预订申请。";

$i_DayType0 = array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
$i_DayType1 = array("循环日 1","循环日 2","循环日 3","循环日 4","循环日 5","循环日 6","循环日 7","循环日 8");
$i_DayType2 = array("循环日 A","循环日 B","循环日 C","循环日 D","循环日 E","循环日 F","循环日 G","循环日 H");
$i_DayType3 = array("循环日 I","循环日 II","循环日 III","循环日 IV","循环日 V","循环日 VI","循环日 VII","循环日 VIII");
$i_DayType4 = array("日","一","二","三","四","五","六");

$i_SimpleDayType0 = array ();
$i_SimpleDayType1 = array ("1","2","3","4","5","6","7","8");
$i_SimpleDayType2 = array ("A","B","C","D","E","F","G","H");
$i_SimpleDayType3 = array ("I","II","III","IV","V","VI","VII","VIII");

// Homework
$i_Homework_WholeSchool = "<font color=#FF6600>一般班别</font>";
$i_Homework_New = "新增";
$i_Homework_Import = "汇入";
$i_Homework_SearchTeacher = "按发出人名字搜寻";
$i_Homework_SearchSubject = "按科目搜寻";
$i_Homework_SearchTeaching = "按老师任教科目搜寻";
$i_Homework_class = "班别";
$i_Homework_subject = "科目";
$i_Homework_title = "标题";
$i_Homework_description = "内容";
$i_Homework_loading = "工作量";
$i_Homework_loading_csv_header = "需时";
$i_Homework_startdate = "开始日期";
$i_Homework_duedate = "限期";
$i_Homework_hours = "小时";
$i_Homework_morethan = "多于";
$i_Homework_lastModified = "最近修改时间";
$i_Homework_view = "检视";
$i_Homework_unauthorized = "你没有权限查看这个纪录";
$i_Homework_404 = "这纪录并不存在";
$i_Homework_TeacherName = "教师";
$i_Homework_new_failed ="输入失败, 请检查输入的资料是否正确";
$i_Homework_new_succuess = "纪录已新增";
$i_Homework_edit_failed = "请检查输入的资料是否正确.";
$i_Homework_new_startdate_wrong ="开始日期必须是今天或以后";
$i_Homework_new_duedate_wrong ="限期必须是开始日期或以后";
$i_Homework_new_title_missing = "课题必须输入";
$i_Homework_no_subject_alert = "你确定要提交没有科目的家课纪录表?";
$i_Homework_new_subject_missing = "请选择科目";
$i_Homework_new_homework_type_missing="请选择家课类型";
$i_Homework_import_errors = "部份资料错误, 请检查档案再试";
$i_Homework_import_success = "已成功汇入";
$i_Homework_new_failed = "资料错误, 请检查再试";
$i_Homework_new_success = "已新增纪录";
$i_Homework_admin_startdate = "只可设定今天家课";
$i_Homework_admin_parent = "家长可检视各班家课";
$i_Homework_admin_disable = "不使用家课纪录表";
$i_Homework_admin_disable_search_subject = "不使用科目搜寻";
$i_Homework_admin_disable_search_teacher = "不使用发出人名字搜寻";
$i_Homework_admin_disable_search_taught = "不使用以教师任教职务搜寻";
$i_Homework_admin_nonteaching_access = "非教学职务员工可以新增家课 (所有科目及班别)";
$i_Homework_admin_restrict_access = "教学职务员工只可新增任教科目及班别";
$i_Homework_admin_enable_subjectleader = "容许科长输入家课";
$i_Homework_admin_enable_export = "容许所有职员汇出家课";
$i_Homework_admin_clear_all_records = "清除所有家课纪录";
$i_Homework_admin_clear_all_records_alert = "你确定要清除家课纪录?";
$i_Homework_admin_past_day_allowed = "容许输入旧日纪录";
$i_Homework_admin_use_homework_type = "使用家课类型";
$i_Homework_admin_use_homework_handin = "使用家课点收功能";
$i_Homework_admin_use_teacher_collect_homework = "显示班主任是否收回有关功课";
$i_Homework_list = "家课纪录表";
$i_Homework_overdue_list="欠交家课清单";
$i_Homework_handin_list = "家课缴交清单";
$i_Homework_handin_current_status ="现在状况";
$i_Homework_Handin_Required="须缴交";
$i_Homework_Collect_Required="班主任是否收回有关功课";
$i_Homework_Collected_By_Class_Teacher = "班主任收回功课";
$i_Homework_handin_records_will_be_deleted="这份家课的缴交清单将会被删除";
$i_Homework_handin_continue="继续";
$i_Homework_handin_not_handin="未缴交";
$i_Homework_handin_handin="已缴交";
$i_Homework_handin_no_need_to_handin="不需缴交";
$i_Homework_handin_change_status="更改状况";
$i_Homework_handin_set_NotHandin_to_NoNeedHandin="把未缴交转为不需缴交";
$i_Homework_handin_set_All_to_Handin="全部转为已缴交";
$i_Homework_handin_set_All_to_NotHandin="全部转为未缴交";


$i_Homework_detail = "详情";
$i_Homework_import_sample = "按此下载范本档案 (CSV 格式)";
$i_Homework_ToDoList = "功课清单";
$i_Homework_AllRecords = "所有纪录";
$i_Homework_teacher_name = "教师";
$i_Homework_edit = "编辑";
$i_Homework_myrecords = "<font color=#FF6600>我的纪录</font>";
$i_Homework_myrecords_icon = "<img src=/images/homeworklist/icon_myrecord.gif>";
$i_Homework_today = "<img src=/images/homework/b5/hw/icon_todayhw.gif border=0 width=29 height=15>";
$i_Homework_con_add = "<font color=green>已新增纪录</font>";
$i_Homework_con_add_failed = "<font color=red>资料错误, 请检查再试</font>";
$i_Homework_import_instruction = "请选择档案";
$i_Homework_import_points = "请注意:";
$i_Homework_import_instruction1 = "班名和科目名称须要完全正确";
//$i_Homework_import_instruction2 = "需时必须为整数, 每半小时为一个单位. 例: 5 代表 2.5 小时. (0 为少于0.5 小时, 17 为多于8 小时)";
$i_Homework_import_instruction2 = "需时必须为整数, 每半小时为一个单位. 例: 2.5 代表 1.25 小时. (0 为少于0.25 小时, 17 为多于8 小时)";
$i_Homework_import_instruction3 = "日期的格式必须为 YYYY-MM-DD 或 YYYY/MM/DD. 如你使用 Excel 编辑, 请在日期的资料上更改储存格格式.";
$i_Homework_import_instruction4 = "除负责老师及".$i_Homework_description."以外, 所有资料必须填写.";
$i_Homework_import_instruction5 = "不能$button_import"."特别组别家课.";
$i_Homework_import_instruction6 = "家课类型名称须要完全正确";
$i_Homework_import_reference = "请参考";
$i_Homework_import_sample = "范本档案 (CSV 格式)";
$i_Homework_import_class = "各班名称";
$i_Homework_import_subject = "各科目名称";
$i_Homework_import_homeworktype = "各家课类型名称";
$i_Homework_minimum = "--";
$i_Homework_WholeImage = "<img border=0 src=/images/btn_wholeschhomework_b5.gif>";
$i_Homework_WholeIcon = "<img src=/images/homeworklist/icon_wholeschhomework.gif>";
$i_Homework_MyRecordsImage = "<img border=0 src=/images/btn_myrecord_b5.gif>";
$i_Homework_PageTitle = "<img src=/images/homeworklist/title_b5.gif>";
$i_Homework_PleaseSelectType = "请选择家课类别";
$i_Homework_ClassType = "班别家课";
$i_Homework_GroupType = "特别班别家课";
$i_Homework_Type = "家课类别";
$i_Homework_Group = "特别班别";
$i_Homework_IncludingClass = "包括班别";
$i_Homework_MultipleClassInstruction = "(可按下 CTRL 键，用滑鼠选择多于一班)";
$i_Homework_Error_NoAccessRight = "此用户没有制作家课之权限";
$i_Homework_Error_HandIn = "请填上'yes'或'no'选择是否需要家课点收";
$i_Homework_Error_Collect= "请填上'yes'或'no'选择是否需要家课收集";
$i_Homework_Error_Empty = "部份栏位没有填上";
$i_Homework_Error_NoSubject = "没有此科目";
$i_Homework_Error_NoClass = "没有此班别";
$i_Homework_Error_Date = "日期错误";
$i_Homework_Error_StartDate = "$i_Homework_startdate"."早于今天";
$i_Homework_Error_EndDate = "$i_Homework_duedate"."早于"."$i_Homework_startdate";
$i_Homework_Error_NoHomeworkType = "没有此家课类型";
$i_Homework_SpecialGroup = "特别班别";
$i_Homework_NormalClass = "普通班别";
$i_Homework_WholeSchoolGroup = "<font color=#FF6600>$i_Homework_SpecialGroup</font>";
$i_Homework_ListView = "清单模式";
$i_Homework_MonthView = "月份模式";
$i_Homework_WeekView = "星期模式";
$i_Homework_ChildrenList = "子女名单";
$i_Homework_Only = "之家课";
$i_Homework_eClassAssignment = "此家课属于课程: ";
$i_Homework_SubjectLeaderKey = "为此班科长";
$i_Homework_CreatedHomework = "所制作的家课";
$i_Homework_ByTeaching = "所任教科目的家课";
$i_Homework_Poster = "发出人";
$i_Homework_Export_Instruction = "请输入日期范围以搜寻需汇出的家课纪录";
$i_Homework_HomeworkType = "家课类型";

$i_Subject_name = "科目名称";
$i_SAMS_code = "代码";
$i_SAMS_cmp_code = "分卷代码";
$i_SAMS_description = "说明 (英/中)";
$i_SAMS_abbr_name = "名称缩写 (英/中)";
$i_SAMS_short_name = "简称 (英/中)";
$i_SAMS_CSV_file = "WEBSAMS CSV 档";
$i_SAMS_download_subject_csv  = "下载科目 CSV 档案范本";
$i_SAMS_download_cmp_subject_csv  = "下载科目分卷 CSV 档案范本";
$i_SAMS_import_error_wrong_format = "错误的汇入格式";
$i_SAMS_import_update_no = "成功更新资料项数";
$i_SAMS_import_add_no = "成功加入资料项数";
$i_SAMS_import_fail_no = "不能加入资料项数";
$i_SAMS_import_error_row = "行数";
$i_SAMS_import_error_data = "错误项目";
$i_SAMS_import_error_reason = "错误原因";
$i_SAMS_import_error_detail= "档案资料";
$i_SAMS_short_name_duplicate = "简称不可重复";
$i_SAMS_import_error_unknown = "不明";
$i_SAMS_notes_after = "之后";

$i_import_error = "部份资料有误. 下列显示的纪录未能新增.";
$i_image_home = "<img src=/images/home.gif border=0>";

$i_Campusquota_basic_identity = array("教师","学生","职员","家长");
$i_Campusquota_identity = "身份";
$i_Campusquota_quota = "储存量 (以MBytes 为单位)";
$i_Campusquota_noLimit = "无限制";
$i_Campusquota_max = "储存量";
$i_Campusquota_used = "已用";
$i_Campusquota_left = "尚余";
$i_Campusquota_uploadSuccess = "附件上载成功 ";
$i_Campusquota_noQuota = "储存量不足";
$i_Campusquota_using1 = "你使用了 ";
$i_Campusquota_using2 = " 的储存量.";
$i_Set_enabled = "允许 ";

$i_Tempfiles_display1 = "个档案并占用了 ";
$i_Tempfiles_display2 = " MBytes.";
$i_Tempfiles_confirm = "如欲移除这些档案, 请按删除.";

$i_Batch_Default = "<font color=red> *</font>";
$i_Batch_SetDefault = "设定为学校时间表";
$i_Batch_Description = "此时间表之说明";
$i_Batch_DefaultDescription = "* - 学校时间表";
$i_Batch_TitleNotNull = "时间表名称必须输入.";
$i_Batch_TitleDuplicated = "不能使用相同的时间表名称.";
$i_Slot_TimeRange = "时间";
$i_Slot_Title = "时段名称";
$i_Slot_ChangeArchiveRecords = "取消所有使用此时间表的预订纪录 (今天或以后的预订纪录会被删除,过去的纪录会被保存).<br> $i_ResourceSlotChangeAlert2";
$i_Slot_NotArchiveAlert = "你选择不取消预订纪录, 可能会引起资料错误, 是否继续?";

$i_schoolinfo_organization_iconleft_admin = "<img src=/images/organization/icon_admin.gif>";
$i_schoolinfo_organization_iconright_admin = "<img src=/images/organization/adminheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_admin = "<img src=/images/organization/adminfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_admin_popup = "<img src=/images/organization/adminfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_academic = "<img src=/images/organization/icon_academic.gif>";
$i_schoolinfo_organization_iconright_academic = "<img src=/images/organization/academicheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_academic = "<img src=/images/organization/academicfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_academic_popup = "<img src=/images/organization/academicfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_class = "<img src=/images/organization/icon_class.gif>";
$i_schoolinfo_organization_iconright_class = "<img src=/images/organization/classheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_class = "<img src=/images/organization/classfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_class_popup = "<img src=/images/organization/classfrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_house = "<img src=/images/organization/icon_house.gif>";
$i_schoolinfo_organization_iconright_house = "<img src=/images/organization/househeading_b5.gif>";
$i_schoolinfo_organization_iconbottom_house = "<img src=/images/organization/housefrbottom.gif>";
$i_schoolinfo_organization_iconbottom_house_popup = "<img src=/images/organization/housefrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_eca = "<img src=/images/organization/icon_eca.gif>";
$i_schoolinfo_organization_iconright_eca = "<img src=/images/organization/ecaheading_b5.gif>";
$i_schoolinfo_organization_iconbottom_eca = "<img src=/images/organization/ecafrbottom.gif>";
$i_schoolinfo_organization_iconbottom_eca_popup = "<img src=/images/organization/ecafrbottom_popup.gif>";
$i_schoolinfo_organization_iconleft_misc = "<img src=/images/organization/icon_misc.gif>";
$i_schoolinfo_organization_iconright_misc = "<img src=/images/organization/mischeading_b5.gif>";
$i_schoolinfo_organization_iconbottom_misc = "<img src=/images/organization/miscfrbottom.gif>";
$i_schoolinfo_organization_iconbottom_misc_popup = "<img src=/images/organization/miscfrbottom_popup.gif>";

$image_back = "/images/btn_back_b5.gif";
$image_cancel = "/images/btn_cancel_b5.gif";
$image_chooserecipient = "/images/btn_chooserecipients_b5.gif";
$image_delete = "/images/btn_delete_b5.gif";
$image_next = "/images/btn_next.gif";
$image_ok = "/images/btn_ok_b5.gif";
$image_pre = "/images/btn_pre.gif";
$image_removeselected = "/images/btn_removeselected_b5.gif";
$image_reset = "/images/btn_reset_b5.gif";
$image_save = "/images/btn_save_b5.gif";
$image_send = "/images/btn_send_b5.gif";
$image_submit = "/images/btn_submit_b5.gif";
$image_update = "/images/btn_update_b5.gif";
$image_periodic_a = "btn_periodic_a_b5.gif";
$image_periodic_b = "btn_periodic_b_b5.gif";
$image_single_a = "btn_single_a_b5.gif";
$image_single_b = "btn_single_b_b5.gif";
$image_moreAttach = "/images/btn_moreattachment_b5.gif";
$image_add = "/images/btn_add_b5.gif";
$image_closewindow = "/images/btn_closewindow_b5.gif";
$image_expand = "/images/btn_expand_b5.gif";
$image_selectall = "/images/btn_selectall_b5.gif";
$image_iconlist = "/images/groupinfo/iconlist_b5.gif";
$image_nextweek = "<img border=0 src=/images/btn_nextweek_b5.gif width=131 height=24>";
$image_prevweek = "<img border=0 src=/images/btn_preweek_b5.gif width=124 height=24>";
$image_prevday = "<img border=0 src=/images/btn_preday_b5.gif width=124 height=24>";
$image_nextday = "<img border=0 src=/images/btn_nextday_b5.gif width=131 height=24>";
$image_import = "/images/btn_import_b5.gif";
$image_search = "/images/btn_search_b5.gif";
$image_include_quote = "/images/btn_includequote_b5.gif";
$image_edit = "/images/btn_edit_b5.gif";
$image_clear = "/images/btn_clear_b5.gif";
$image_restore = "/images/btn_restore_b5.gif";
$image_viewSlots = "/images/btn_viewslots_b5.gif";
$image_updaterole = "/images/btn_updaterole_b5.gif";
$image_delete_member = "/images/btn_deletemember_b5.gif";
$image_attachfiles = "/images/btn_attachfiles_b5.gif";
$image_authorize = "/images/btn_authorize.gif";
$image_forbid = "/images/btn_fornid.gif";
$image_reject = "/images/btn_reject.gif";
$image_go = "/images/btn_go.gif";
$image_cs = "btn_customersupport.gif";
$image_cs_blink = "btn_customersupport_blink.gif";
$image_cs_admin = "btn_customersupport_admin.gif";
$image_cs_admin_blink = "btn_customersupport_admin_blink.gif";

$i_grouphead_timetable = "<img src=/images/groupinfo/heading_timetable_b5.gif>";
$i_grouphead_chatroom = "<img src=/images/groupinfo/heading_chatroom_b5.gif>";
$i_grouphead_bulletin = "<img src=/images/groupinfo/heading_bulletin_b5.gif>";
$i_grouphead_sharedlinks = "<img src=/images/groupinfo/heading_sharedlinks_b5.gif>";
$i_grouphead_sharedfiles = "<img src=/images/groupinfo/heading_sharedfiles_b5.gif>";
$i_grouphead_groupsetting = "<img src=/images/groupinfo/heading_groupsetting_b5.gif>";
$i_grouphead_questionbank = "<img src=/images/groupinfo/heading_schoolbasedqb.gif>";

$i_campusmail_stamp = "<img border=0 src=/images/campusmail/stamp.gif>";
$i_campusmail_compose = "<img border=0 src=/images/campusmail/icon_compose_b5.gif>";
$i_campusmail_inbox = "<img border=0 src=/images/campusmail/icon_inbox_b5.gif>";
$i_campusmail_outbox = "<img border=0 src=/images/campusmail/icon_outbox_b5.gif>";
$i_campusmail_draft = "<img border=0 src=/images/campusmail/icon_draft_b5.gif>";
$i_campusmail_trash = "<img border=0 src=/images/campusmail/icon_trash_b5.gif>";
$i_campusmail_lettertext = "<img border=0 src=/images/campusmail/lettertext.gif>";
$i_campusmail_novaliduser = "你所选择的收件人并没有权限使用校园信箱";
$i_campusmail_policy = "要求收信人回复的政策";
$i_campusmail_policy_original = "收件人必须回复, 寄件人才能得知收件人已查阅该信件.";
$i_campusmail_policy_auto = "收件人只要打开信件, 寄件人便能得知收件人已查阅该信件.";
$i_campusmail_nondelivery = "以下用户未能接收此讯息, 因为他们的邮箱已满. 他们需要清理邮箱, 或你可以将没有附件之讯息发送给他们.";
$i_campusmail_sent_successful = "讯息已传送";
$i_campusmail_save_successful = "讯息已储存";
$i_campusmail_processing = "处理中 ...";
$i_campusmail_processing_note = "如果收件人数多, 所需时间将会较长, 请耐心等候.";
$i_campusmail_accessright = "使用权";
$i_campusmail_ban = "禁止用户发送邮件";
$i_campusmail_ban_instruction = "请输入限制用户的登入名称. <br>每一行输入一个登入名称.<br>这些用户不能发送邮件, 但能接收邮件及回应讯息.<br>";
$i_campusmail_alert_trashall = "你确定要把所有邮件放进垃圾箱?";
$i_campusmail_alert_emptytrash = "你确定要把垃圾箱的邮件永久删除?";
$i_campusmail_alert_export = "只有讯息内容会被汇出(即不包括附件档案). 你确定要继续?";
$i_campusmail_forceRemoval = "强制移除";
$i_campusmail_forceRemoval_instruction1 = "请输入开始及结束日期, 所有于此段期间的校园信箱邮件会被移除";
$i_campusmail_forceRemoval_Start = "开始日期";
$i_campusmail_forceRemoval_End = "结束日期";
$i_campusmail_next = "下一封";
$i_campusmail_prev = "上一封";

$i_campusmail_recipient_selection_control="收件者管理";

$i_campusmail_no_users_available="没有收件人可供选择";
$i_campusmail_usage_setting = "使用设定";



$i_campusmail_select_interface="选择介面";
$i_campusmail_use_old_interface= "使用旧介面";
$i_campusmail_use_new_interface= "使用新介面";
$i_campusmail_staff_recipients_selection ="职员收件者";
$i_campusmail_student_recipients_selection="学生收件者";
$i_campusmail_parent_recipients_selection ="家长收件者";
$i_campusmail_blocked_groups="禁止选择的小组类别";
$i_campusmail_blocked_usertypes="禁止选择的用户类别";
$i_campusmail_group_categories="小组类别";
$i_campusmail_usertypes ="用户类别";
$i_campusmail_select_parents_from_students="从学生名字选择家长";
$i_campusmail_teaching_staff="教学职务员工";
$i_campusmail_non_teaching_staff="非教学职务员工";

$i_campusmail_all_teacher_staff="所有教学及非教学职员";
$i_campusmail_no_teacher_staff="不可寄给职员";
$i_campusmail_teaching_sender_children_teacher="现正任教寄信者子女的教师";
$i_campusmail_teaching_sender_teacher="现正任教寄信者的教师";

$i_campusmail_all_students="所有学生";
$i_campusmail_only_students_in_same_class_level="只可寄给相同级别的学生";
$i_campusmail_only_students_in_same_class="只可寄给同班学生";
$i_campusmail_no_students="不可寄给学生";
$i_campusmail_only_students_in_taught_class="只可寄给任教班别的学生";
$i_campusmail_only_sender_own_children="只可寄给自己的子女";

$i_campusmail_all_parents="所有家长";
$i_campusmail_only_parents_with_children_in_taught_class="只可寄给任教班别的学生的家长";
$i_campusmail_only_parents_with_children_in_same_class_level="只可寄给相同级别的学生的家长";
$i_campusmail_only_parents_with_children_in_same_class="只可寄给同班学生的家长";
$i_campusmail_no_parents="不可寄给家长";
$i_campusmail_only_own_parents="只可寄给学生自己的家长";

$i_header_myaccount = "<img border=0 src=/images/header/icon_myaccount_b5.gif>";
$i_header_myaccount_teacher = "<img border=0 src=/images/header/icon_myaccount_t_b5.gif>";
$i_header_newmail = "<img border=0 src=/images/header/icon_newmail.gif>";
$i_header_newmail_icon_path = "/images/header/icon_newmail.gif";
$i_header_studentprofile_icon = "$image_path/header/icon_studentprofile_b5.gif";

$i_myac_title = "我的户口";
$i_myac_myinfo = "个人资料";

$i_Webmail_title = "网上电邮";

$i_CampusTV_top = "<img src=/images/campus_tv/tv_top_b5.gif width=640 height=110>";
$i_CampusTV_Title = "标题";
$i_CampusTV_56k = "窄频 (56K)";
$i_CampusTV_200k = "宽频 (200K)";
$i_CampusTV_Select = "请选择播放片段及频宽";
$i_CampusTV_Failed = "如无法显示片段画面，";
$i_CampusTV_alt = "请按此开启。";

$i_SMS_System_Message = "系统讯息";
$i_SMS_Balance_Lass_Than = "户口结存少于";
$i_SMS_Notification = "短讯通知";
$i_SMS_NoChangeMobile = "(此流动电话号码只用作接收 SMS 短讯之用, 如果你需要更改此项资料, 请联络系统管理员.)";
$i_SMS_Cautions = "请注意:";
$i_SMS_Limitation1 = "全英文短讯, 最多可输入160字元.";
$i_SMS_Limitation2 = "如短讯包含BIG5编码之繁体中文字, 则最多可输入70字元(包括中英文字元及标点符号).";
$i_SMS_Limitation3 = "超出字数上限的短讯将被删除过长部分.";
$i_SMS_Limitation4 = "部份手机或未能显示中文标点符号(如\"，\"或\"。\"), 建议使用英文标点符号(如\",\"或\".\").";
$i_SMS_Limitation5 = "不能显示之特殊字元: ~^`{}\|[]";
$i_SMS_Limitation6 = "Unicode编码之字元可能不能显示.";
$i_SMS_SMS = "短讯服务";
$i_SMS_Send = "传送短讯";
$i_SMS_Send_Confirmation = "传送短讯确认";
$i_SMS_Send_Broadcast = "传送讯息至所有用户";
$i_SMS_Send_SelectUser = "传送短讯至内联网用户";
$i_SMS_Send_SelectParent = "传送短讯至家长(以学生选择)";
$i_SMS_Send_Raw = "传送短讯(输入电话号码)";
$i_SMS_UsageReport = "使用报告";
$i_SMS_MessageTemplate = "讯息范本";
$i_SMS_MobileNumber = "流动电话号码";
$i_SMS_Recipient = "接收人";
$i_SMS_Recipient2 = "接收人";
$i_SMS_RelatedUser = "相关用户";
$i_SMS_SeparateRecipient = "电话号码须以分号(;) 分隔";
$i_SMS_MessageContent = "短讯内容";
$i_SMS_SendDate = "传送日期";
$i_SMS_SendTime = "传送时间";
$i_SMS_Status = "状态";
$i_SMS_TargetSendTime = "实际传送时间";
$i_SMS_SubmissionTime = "请求传送时间";
$i_SMS_NoteSendTime = "如即时传送, 请留空传送日期及时间";
$i_SMS_MessageSent = "<font color=green>短讯已传送</font>";
$i_SMS_AlertSelectUserType = "请最少选择一个身份";
$i_SMS_AlertMessageContent = "请输入讯息内容";
$i_SMS_AlertRecipient = "请输入接收人";
$i_SMS_TargetStudent = "目标学生";
$i_SMS_ClearRecords = "清除所有纪录";
$i_SMS_RetrieveRecords = "由服务中心查询纪录";
$i_SMS_MessageSentSuccessfully = "已传送的讯息 (包括未由服务中心更新的讯息)";
$i_SMS_MessageQuotaLeft = "尚余讯息数目";
$i_SMS_con_NoUser = "<font color=red>所选用户并没有输入流动电话号码</font>";
$i_SMS_con_DataRetrieved = "<font color=green>已由服务中心取得资料</font>";
$i_SMS_NotExamined = "未更新";
$i_SMS_InTransit = "传送中";
$i_SMS_Delivered = "已传送";
$i_SMS_Expired = "已过期";
$i_SMS_Deleted = "已被删除";
$i_SMS_Undeliverable = "不能传送";
$i_SMS_Accepted = "已接纳";
$i_SMS_Invalid = "不合规格";
$i_SMS_Pending = "等待中";
$i_SMS_InvalidPhone = "号码错误";
$i_SMS_Status0 = $i_SMS_NotExamined;
$i_SMS_Status1 = $i_SMS_InTransit;
$i_SMS_Status2 = $i_SMS_Delivered;
$i_SMS_Status3 = $i_SMS_Expired;
$i_SMS_Status4 = $i_SMS_Deleted;
$i_SMS_Status5 = $i_SMS_Undeliverable;
$i_SMS_Status6 = $i_SMS_Accepted;
$i_SMS_Status7 = $i_SMS_Invalid;
$i_SMS_Status8 = $i_SMS_Pending;
$i_SMS_Status9 = $i_SMS_InvalidPhone;
$i_SMS_24Hr = "两位数之二十四小时格式";
$i_SMS_InvalidTime = "时间格式错误";
$i_SMS_TemplateName = "范本名称";
$i_SMS_TemplateType = "范本类别";
$i_SMS_TemplateContent = "内容";
$i_SMS_File_Send = "档案输入";
$i_SMS_ColData = "第一栏资料";
$i_SMS_Type_Mobile = "流动电话号码";
$i_SMS_Type_Login = "$i_UserLogin";
$i_SMS_UploadFile = "选择档案";
$i_SMS_FileDescription = "<u><b>档案说明:</b></u><br>
Column 1: 流动电话号码<br>
Column 2: SMS 讯息.";
$i_SMS_FileConfirm = "请确认所上载的讯息正确, 然后按传送.";
$i_SMS_SendTo_Student_Guardian ="传送短讯至学生监护人";
$i_SMS_Notice_Send_To_Users ="将会传送讯息至以下接收人:";
$i_SMS_Warning_Cannot_Send_To_Users="无法传送讯息至以下接收人:";
$i_SMS_Error_No_Guardian ="没有监护人";
$i_SMS_Error_NovalidPhone = "没有合适的号码";
$i_SMS_Notice_Show_Student_With_MobileNo_Only="不显示没有流动电话号码的学生";
$i_SMS_Notice_Show_Student_With_Guardian_Only="不显示没有监护人的学生";

$i_SMS_SystemTemplate = "特别情况范本";
$i_SMS_NormalTemplate = "通用范本";
$i_SMS_Personalized_Msg_Tags = "自动插入项目";

/*
$i_SMS_Personalized_Tag['user_name_eng'] = "Target user's English name";
$i_SMS_Personalized_Tag['user_name_chi'] = "Target user's Chinese name";
$i_SMS_Personalized_Tag['user_class_name'] = "Target student's class name";
$i_SMS_Personalized_Tag['user_class_number'] = "Target student's class number";
$i_SMS_Personalized_Tag['user_login'] = "Target user's login name";
$i_SMS_Personalized_Tag['attend_card_time'] = "Arrival time or departure time when tapping card";
$i_SMS_Personalized_Tag['payment_balance'] = "Student's balance left";
$i_SMS_Personalized_Tag['payment_outstanding_amount'] = "Student's total outstanding amount";
*/


$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ABSENCE'] 		= "缺席";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LATE'] 		= "迟到";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_EARLYLEAVE'] 	= "早退";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_ARRIVAL'] 		= "已回校";
$i_SMS_Personalized_Msg_Template['STUDENT_ATTEND_LEAVE'] 		= "离开学校";
$i_SMS_Personalized_Msg_Template['STUDENT_MAKE_PAYMENT'] 		= "缴款";
$i_SMS_Personalized_Msg_Template['BALANCE_REMINDER'] 			= "户口金额提示";

$i_SMS_Personalized_Template_Instruction="你可在自订范本讯息中，加入如学生姓名或离校拍卡时间等资料。<BR>
<BR>
要加入该等资料，请从以下选单中，选取适用的项目，然后按\"增加\"，系统将以如(\$user_class_name)等的符号代表有关资料。<BR><BR>";
$i_SMS_Personalized_Template_Type="范本类型";
$i_SMS_Personalized_Template_SendCondition="讯息发出情况";


$i_SMS_no_student_select = "请最少选择一个学生!";
$i_SMS_RecipientCount = "接收人数目";
$i_SMS_PIC = "PIC";
$i_SMS_Reply_Message="可回复讯息";
$i_SMS_Reply_Message_View="检视讯息";
$i_SMS_Reply_Message_View_Sent_Message="检视已发送讯息";
$i_SMS_Reply_Message_View_Replied_Message="检视已回复讯息";
$i_SMS_Reply_Message_Message="讯息";
$i_SMS_Reply_Message_Replied_MessageCount="已回复人数";
$i_SMS_Reply_Message_Replied_Message_DeliveryTime="发出时间";
$i_SMS_Reply_Message_Replied_Message="回复讯息";
$i_SMS_Reply_Message_Replied_PhoneNumber="回复号码";
$i_SMS_Reply_Message_Replied_Time="回复时间";
$i_SMS_Reply_Message_Status_Not_Yet_Replied="未回复";
$i_SMS_Reply_Message_Status_Replied="已回复";
$i_SMS_Reply_Message_Replied_Message_Stat="回复讯息统计";
$i_SMS_Reply_Message_Retrieve="撷取回复信息";
$i_SMS_Reply_Message_DateRange_Msg="显示以下日子期间所有的纪录";

$i_SMS_Select_DateRange="下载以下日期范围内的$i_SMS_UsageReport";
$i_SMS_MultipleMessage="多项讯息";

$i_SMS['NoPluginWarning'] = "你可以透过eClass综合平台发送即时短讯予学生或家长。请即联络销售人员启动有关服务!";
$i_SMS['jsWarning']['NoPluginWarning'] = "短讯服务尚未启动, 请即联络销售人员启动有关服务!";


$i_iconAttachment = "<img src=/images/icon_attachment.gif border=0>";

$i_Alert_Reject = "你是否确定要拒绝?";
$i_Alert_Checkin = "你是否确定要取用?";
$i_Alert_Checkout = "你是否确定要退还?";

$i_label_SelectAll = "全选";

$i_Step_Name = "<img src=/images/step/step_b5.gif border=0>";

$i_organization_description = "此为本校的组织架构, 当中环节包含了本校不同的功能及活动组别. 要进入个别小组的网页, 请按 $i_image_home (如有网页). 要检视小组成员名单, 请按该小组名称.";

$i_SelectMultipleInstruction = "请按下CTRL键，用滑鼠选择多于一个";
$i_SelectMemberInstruction = "请先选择类别. 您可以于第 2 步选择整个小组, 或于第 3 步选择个别成员.";
$i_SelectMemberSteps = array (
"",
"选择类别",
"选择整个小组或检视个别成员",
"选择个别成员"
);
$i_SelectMemberNoGroupInstruction = "请先选择新用户身份, 然后选择类别. 接着便可以检视小组成员，及选择个别用户.";
$i_SelectMemberNoGroupSteps = array (
"",
"选择新用户身份",
"选择类别",
"检视个别成员",
"选择个别成员"
);

$i_toggleBy = "只显示";

$i_sls_library_mapping_file = "用户连结档案";
$i_sls_library_mapping_file_select = "选择上载档案";
$i_sls_library_mapping_file_instruction = "请注意, 使用中之连结档案将会被取代. 如果只是新增用户连结, 请先下载使用中之连结档案再把新连结加到档案中, 然后再进行上载.";
$i_sls_library_mapping_file_use = "此用户连结档案是作为连结 eClass 内联网及 SLS 图书馆系统之用. 此连结档案由另一个独立程式产生. 如果你不懂得如何产生此档案, 请与博文教育技术支援部联络.";
$i_sls_library_mapping_file_download = "下载现在使用的连结档案.";
$i_sls_library_mapping_file_upload_success = "<font color=green>已成功上载.</font>";
$i_sls_library_search = "检索";
$i_sls_library_info = "一般资料";
$i_sls_library_newadditions = "新书推介";
$i_sls_library_patron_top = "借书龙虎榜";
$i_sls_library_item_top = "热门书籍龙虎榜";
$i_sls_library_patron_info = "Patron Information";
$i_sls_library_checkout_list = "借出书目";
$i_sls_library_hold_list = "预留书目";
$i_sls_library_outstanding_fine = "过期图书罚款";
$i_sls_library_norecords = "&#26283;&#26178;&#20173;&#26410;&#26377;&#20219;&#20309;&#35352;&#37636;";          // 暂时仍未有任何纪录
$i_sls_library_image_newadditions = "<img src='images/sls_info_btn_new_add.gif' border='0'>";
$i_sls_library_image_patron_top = "<img src='images/sls_info_btn_top_patron.gif' border='0'>";
$i_sls_library_image_item_top = "<img src='images/sls_info_btn_top_item.gif' border='0'>";
$i_sls_library_image_checkout_list = "<img src='images/sls_info_btn_checkout.gif' border='0'>";
$i_sls_library_image_hold_list = "<img src='images/sls_info_btn_hold.gif' border='0'>";
$i_sls_library_title_image_newadditions = "<img src='images/sls_info_table_new_add.gif' border='0'>";
$i_sls_library_title_image_patron_top = "<img src='images/sls_info_table_top_patron.gif' border='0'>";
$i_sls_library_title_image_item_top = "<img src='images/sls_info_table_top_item.gif' border='0'>";
$i_sls_library_title_image_checkout_list = "<img src='images/sls_info_table_checkout.gif' border='0'>";
$i_sls_library_title_image_hold_list = "<img src='images/sls_info_table_hold.gif' border='0'>";
$i_sls_library_book_title = "&#26360;&#30446;&#21517;&#31281;"; // 书目名称
$i_sls_library_call_number = "&#32034;&#26360;&#34399;";        // 索书号
$i_sls_library_from = "&#30001;&#65306;";
$i_sls_library_to = "&#33267;&#65306;";
$i_sls_library_reader_name = "&#35712;&#32773;&#22995;&#21517;";              // 读者姓名
$i_sls_library_reader_class = "&#29677;&#21029;&#21450;&#23416;&#34399;";     // 班别及学号
$i_sls_library_read_count = "&#20511;&#38321;&#25976;&#37327;";               // 借阅数量
$i_sls_library_borrow_count = "&#20511;&#20986;&#27425;&#25976;";             // 借出次数
$i_sls_library_return_date = "&#36996;&#26360;&#26085;&#26399;";              // 还书日期
$i_sls_library_current_status = "&#29694;&#26178;&#29376;&#24907;";           // 现时状态

$i_motd_description = "此讯息将会在用户登入后的首页以流动形式显示.";

$i_QB_AccessGroup = "使用中央题目库之组别";
$i_QB_ModifySetting = "按下你欲更改设定 (程度, 种类及难易度) 的组别名称.";
$i_QB_NoGroupUsing = "现时没有设定任何可使用中央题目库之组别.";
$i_QB_Category = "种类";
$i_QB_Level = "程度";
$i_QB_Difficulty = "难易度";
$i_QB_EngName = "英文名称";
$i_QB_ChiName = "中文名称";
$i_QB_DisplayOrder = "显示次序";
$i_QB_admin_titlegif = "/images/admin/system_admin/sls_b5.gif";
$i_QB_NameCantEmpty = "你必须填上英文或中文名称";
$i_QB_SelectFile = "选择档案:";
$i_QB_DownloadSettingSample = "按此处下载范本(CSV) 档案.";
$i_QB_Pending = "待批";
$i_QB_Public = "已批核";
$i_QB_Rejected = "已拒绝";
$i_QB_Question = "问题";
$i_QB_Status = "状况";
$i_QB_LangVer = "语言版本";
$i_QB_EngOnly = "只有英文版";
$i_QB_ChiOnly = "只有中文版";
$i_QB_BothLang = "中英文版";
$i_QB_LastModified = "最后修改日期";
$i_QB_QuestionCode = "问题编号";
$i_QB_DateSubmission = "递交日期";
$i_QB_Owner = "拥有人";
$i_QB_EngVer = "英文版";
$i_QB_ChiVer = "中文版";
$i_QB_Answer = "答案";
$i_QB_MC = "多项选择题";
$i_QB_ShortQ = "短题目";
$i_QB_FillIn = "填充";
$i_QB_Matching = "配对";
$i_QB_TandF = "是非题";
$i_QB_ImportNotes = "Description of Import";
$i_QB_QuestionType = "问题类别";
$i_QB_SelectFile = "选择档案";
$i_QB_MyQuestion = "我的题目";
$i_QB_SharingArea = "分享地带";
$i_QB_FileList = "档案清单";
$i_QB_DownloadEnglish = "下载英文版";
$i_QB_DownloadChinese = "下载中文版";
$i_QB_All = "全部";
$i_QB_ToBulletin = "[Bulletin]";
$i_QB_EditNotes = "编辑后请按储存";
$i_QB_SubjectAdminLink = "问题库";
$i_QB_LangSelect = "语言";
$i_QB_LangSelectEnglish = "英文";
$i_QB_LangSelectChinese = "中文";
$i_QB_LangSelectBoth = "双语";
$i_QB_QuestionAdmin = "Admin";
$i_QB_Alert_Authorize = "Are you sure to authorize the checked questions?\\n(The questions will be shown in Sharing Area)";
$i_QB_Alert_Forbid = "Are you sure to forbid the checked questions?\\n(The questions will not be shown in Sharing Area)";
$i_QB_Alert_Reject = "Are you sure to reject the checked questions?";
$i_QB_Prompt_Reject = "请输入拒绝接纳之原因, 或取消拒绝.";
$i_QB_ViewOriginalEnglish = "检视原档案 [英文版]";
$i_QB_ViewOriginalChinese = "检视原档案 [中文版]";
$i_QB_MarkAllRead = ExportIcon()."标示全部为已阅 ";
$i_QB_MarkAllReadDownload = ExportIcon()."标示全部为已阅及下载";
$i_QB_ConfirmAllRead = "你确定要标示所有题目为已阅?";
$i_QB_ConfirmAllDownload = "你确定要标示所有题目为已阅及下载?";
$i_QB_ConfirmAddToBucket = "你确定要把已选之题目加进 pack?";
$i_QB_ConfirmAddAllToBucket = "你确定要把所有题目加进 pack?";
$i_QB_ConfirmClearBucket = "你确定要清除 pack 中所有题目?";
$i_QB_AddToBucket = "加进 pack";
$i_QB_AddAllToBucket = "所有题加进 pack";
$i_QB_ClearBucket = "清除 pack";
$i_QB_QuestionBucket1 = "已加进 pack 中的题目:";
$i_QB_QuestionBucket2 = " 条题目在pack 中, 你可以:";
$i_QB_DownloadAsWord = "下载 Microsoft Word 格式, 并以 zip 储存.";
$i_QB_DownloadToEclass = "下载到以下其中一个eClass 课室:";
$i_QB_SameServer = "本伺服器";
$i_QB_AnswerOption = "答案选择";
$i_QB_ChangeGroupReset = "转换科目会清除已加进 pack 之题目. 是否确定继续?";
$i_QB_Settings = "设定";
$i_QB_NoGroupUsing = "目前没有小组使用题目库";
$i_QB_AwardPts = "题目价值";
$i_QB_Pts = "分";
$i_QB_DownloadedCount = "已被下载次数";
$i_QB_Pts_Available = "现有点数";
$i_QB_Pts_InBucket = "在pack 的点数";
$i_QB_Pts_LeftAfterDownload = "下载后剩余点数";
$i_QB_Pts_NotEnough = "你没有足够的点数下载题目";
$i_QB_SelectRemovalTransfer1 = "请选择";
$i_QB_SelectRemovalTransfer2 = ", 系统会把原来题目转至此.";
$i_QB_TypeRemovalDelete = "直接移除题目";
$i_QB_AllQuestions = "所有题目";
$i_QB_ViewAll = "检视所有题目";

$i_CustomerSupport = "用户支援";

$i_Profile_Student_Profile = "学生纪录";
$i_Profile_settings = "学生档案设定";
$i_Profile_settings_display = "显示设定";
$i_Profile_settings_acl = "教师存取设定";
$i_Profile_settings_merit_type_select = "选择使用的奖惩项目";
$i_Profile_settings_attend_stat_method = "考勤纪录 - 缺席之统计方法设定";
$i_Profile_settings_select_fields_to_disable = "请选择系统不使用的种类";
$i_Profile_settings_AttednanceReason = "考勤纪录 - 可被班主任编辑";
$i_Profile_settings_AttednanceReasonNotEditable = "不可编辑";
$i_Profile_settings_AttednanceReasonEditable = "可编辑";
# added on 9 Sept
$i_Profile_settings_merit_type_edit = "编辑奖惩项目名称";

$i_Profile_Attendance = "考勤纪录";
$i_Profile_Merit = "奖惩纪录";
$i_Profile_Service = "服务纪录";
$i_Profile_Activity = "活动纪录";
$i_Profile_Award = "得奖纪录";
$i_Profile_Assessment = "学生评估";
$i_Profile_Files = "参考档案";
$i_Profile_Summary = "总结";
$i_Profile_Summary_Has = "共 有";
$i_Profile_Summary_Conversion = "换算";
$i_Profile_Summary_Conversion_To = "进";
$i_Profile_AdminUserMgmt = "学生档案";
$i_Profile_AdminLevel_Description_Normal = "只可以新增纪录及更改自己输入的纪录";
$i_Profile_AdminLevel_Description_Full = "可以更改所有纪录";
$i_Profile_AdminACL = "可存取纪录";
$i_Profile_Admin_NotAllowed = "不允许 ";

$i_Profile_Attendance_Analysis = "考勤分析";
$i_Profile_Merit_Analysis = "奖惩分析";

$i_Profile_SelectUser = "请选择一位学生";

$i_Profile_Absent = "缺席";
$i_Profile_Late = "迟到";
$i_Profile_EarlyLeave = "早退";
$i_Profile_Days = "日";

$i_Profile_From = "由";
$i_Profile_To = "到";
$i_Profile_Today = "今天";
$i_Profile_ThisWeek = "本星期";
$i_Profile_ThisMonth = "本月";
$i_Profile_ThisAcademicYear = "本年度";

$i_Profile_SelectStudent = "选择学生";
$i_Profile_NotClassTeacher = "非班主任教师不能检视学生资料.";
$i_Profile_NoFamilyRelation = "尚未建立子女连结.";
$i_Profile_OverallStudentView = "检视现时学生纪录";
$i_Profile_DataLeftStudent = "检视已离校学生的资料";
$i_Profile_OverallStudentViewLeft = "检视暂存已离校学生纪录";
$i_Profile_DataRemovedStudent = "已被删除学生的资料";
$i_Profile_SelectClass = "选择班级:";

$i_Profile_Year = "年度";
$i_Profile_Semester = "学期";
$i_Profile_SelectSemester = "选择日期";
$i_Profile_SelectReason = "选择原因";
$i_Profile_ByReason = "以原因检视";
$i_Profile_StudentRecord = "学生纪录";
$i_Profile_DataLeftYear = "离校年度";
$i_Profile_ClassOfFinalYear = "离校班别";

$i_Profile_StudentNotFound = "找不到这名学生";

$i_Profile_StudentName = "学生名称";

$i_Profile_ConfirmRemoveAllArchiveRecord = "你是否确定删除所有学生的资料？";
$i_Profile_ConfirmRemoveLink = "按此删除所有资料";
$i_Profile_SucceedRemoveMsg = "已成功删除所有学生的资料";

$i_Profile_SearchMethod = "寻找方法";
$i_Profile_ClassHistory = "班级纪录";

$i_Profile_Hide_Frontend = "不显示在 eClass 校园网";
$i_Profile_Hide_PrintPage = "不显示在列印格式";
$i_Profile_PersonInCharge = "负责人";

$i_Profile_RecordDate = "纪录日期";
$i_Profile_SearchByDate = "搜寻选项";
$i_Profile_SearchByPeriod = "以时段搜寻";
$i_Profile_DateType = "日期类别";
$i_Profile_NotSearchByDate = "不以日期搜寻";
$i_Profile_ByRecordDate = "以纪录日期";
$i_Profile_ByLastModified = "以最后修改日期";
$i_Profile_SearchText = "搜寻项目";

$i_Profile_AttendanceStatistic_Method1 = "$i_DayTypeWholeDay/$i_DayTypeAM/$i_DayTypePM 皆计算 1 次";
$i_Profile_AttendanceStatistic_Method2 = "$i_DayTypeWholeDay 计 1 次, $i_DayTypeAM/$i_DayTypePM 计 0.5 次";
$i_Profile_AttendanceStatistic_Description = "这个选项会影响所有计算的部份 (班别之";

$i_Profile_Import_NoMatch_Entry[2]="班别及班号错误(请用英文填写)";
$i_Profile_Import_NoMatch_Entry[3]="内联网帐号错误";
$i_Profile_Import_NoMatch_Entry[1]="部份栏不正确";
$i_Profile_Import_NoMatch_Entry[4]="部份栏位没有填上";
$i_Profile_Import_NoMatch_Entry[5]="学校年份或学期不存在(请用英文填写)";

$i_AttendanceTypeArray = array("",$i_Profile_Absent, $i_Profile_Late,$i_Profile_EarlyLeave);
$i_Attendance="考勤";
$i_Attendance_Others="其它";
$i_Attendance_Standard="标准";

$i_Attendance_Remark ="备注";
$i_Attendance_Num_Of_Day = "星期";
$i_Attendance_Date = "日期";
$i_Attendance_Type = "类别";
$i_Attendance_attendance_type="考勤类别";
$i_Attendance_Year = "年度";
$i_Attendance_AllYear = "全部年度";
$i_Attendance_Semester = "学期";
$i_Attendance_DayType = "时段";
$i_Attendance_Modified = "最后修改时间";
$i_Attendance_Reason = "原因";
$i_Attendance_ImportSelect = "选择档案";
$i_Attendance_DownloadSample = "按此下载范例";

$i_Attendance_ImportInstruction1 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四栏 : 学生班别 (注意大小写).<br>
第五栏 : 学生班号<br>
第六栏 : 类别 (A - 缺席, L - 迟到, E - 早退)<br>
第七栏 : 时段 (WD - 全日, AM - 上午, PM - 下午).<br>
第八栏 : 原因 (可选择留空).
";

$i_Attendance_ImportInstruction2 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四栏 : WebSAMS 注册号码 (帐号由 # 开始 如  #123456).<br>
第五栏 : 类别 (A - 缺席, L - 迟到, E - 早退)<br>
第六栏 : 时段 (WD - 全日, AM - 上午, PM - 下午).<br>
第七栏 : 原因 (可选择留空).
";

$i_Attendance_DetailedAttendanceRecord = "详细考勤纪录";

/*----- Added On 18 DEC 2007 */
$i_StaffAttendance_Sort = "排序";
$i_StaffAttendance_Sort_Type = "排序方向";
$i_StaffAttendance_Sort_Name = "职员名称";
$i_StaffAttendance_Sort_Status = "状态";
$i_StaffAttendance_Sort_Time = "时间";
$i_StaffAttendance_Sort_Type_Asc = "向上";
$i_StaffAttendance_Sort_Type_Desc = "向下";
/*---------------------------*/

/*----- Added On 21 DEC 2007 */
$i_StaffAttendance_Field_Date_From = "由";
$i_StaffAttendance_Field_Date_To = "到";
/*---------------------------*/

/*----- Added On 24 DEC 2007 */
$i_StaffAttendance_Total_Day_Of_Abs = "总日数";
/*---------------------------*/

/*----- Added On 27 DEC 2007 */
$i_StaffAttendance_Report_Staff_Waived_Flag = "包括豁免记录";
/*---------------------------*/

/*----- Added On 29 Feb 2008 */
$i_StaffAttendance_Report_Staff_Outgoing_Monthy = "职员出勤记录 (每月)";
/*---------------------------*/

/*----- Added On 03 Mar 2008 */
$i_StaffAttendance_Report_Date_Range = "日期范围";
$i_StaffAttendance_Report_Days_Abs = "日数";
/*---------------------------*/

/*----- Added On 14 Mar 2008 */
$i_StaffAttendance_ReasonType_Message = "以下原因类型将作为「教职员请假缺席统计」报告之栏位名称，若<br><br>

																				 1.有关原因类型已停用，及<br>
																				 2.有关原因类型于缺席统计之统计日期范围期间未曾选用<br><br>

																				 则该原因类型将不予显示于该统计报告中。
																				 ";
/*---------------------------*/

/*----- Added On 27 Mar 2008 */
$i_StaffAttendance_SelfAttendance = "个人考勤记录";
$i_StaffAttendance_Month_Selector_SelectMonth = "选择月份";
/*---------------------------*/

$i_Activity_ViewStudent = "检视学生纪录";
$i_Activity_ArchiveRecord = "汇入本年度活动纪录 (应只在学期或学年完结时执行)";
$i_Activity_Count = "数量";
$i_ActivityYear = "年度";
$i_ActivitySemester = "学期";
$i_ActivityName = "活动名称";
$i_ActivityRole = "职位";
$i_ActivityOrganization = "举办机构";
$i_ActivityPerformance = "表现";
$i_ActivityLastModified = "最后更新时间";
$i_ActivityRemark = "备注";
$i_ActivityNoRecord = "暂时没有任何课外活动纪录.";
$i_ActivityArchive = "保存纪录";
$i_ActivityArchiveDescription = "将学生活动资料从课外活动小组汇入至活动纪录，汇入的资料包括活动名称、职位和表现，并以下列年度及学期作为纪录:";
$i_ActivityArchiveWrongData = "如以上资料有任何错误，请到内联网设定 > 系统设定 >基本设定，更改年度及学期的资料.";
$i_ActivityArchiveProceed = "按此进行汇入程序";

$i_Activity_ImportInstruction1 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : 学生班别 (注意大小写).<br>
第四栏 : 学生班号<br>
第五栏 : $i_ActivityName.<br>
第六栏 : $i_ActivityRole (可选择留空).<br>
第七栏 : $i_ActivityPerformance (可选择留空).<br>
第八栏 : $i_ActivityRemark (可选择留空).<br>
第九栏 : $i_ActivityOrganization (可选择留空).
";

$i_Activity_ImportInstruction2 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : WebSAMS 注册号码 (帐号由 # 开始 如  #123456).<br>
第四栏 : $i_ActivityName.<br>
第五栏 : $i_ActivityRole (可选择留空).<br>
第六栏 : $i_ActivityPerformance (可选择留空).<br>
第七栏 : $i_ActivityRemark (可选择留空).<br>
第八栏 : $i_ActivityOrganization (可选择留空).
";

$i_Merit_Date = "奖惩日期";
$i_Merit_Type = "类型";
$i_Merit_Qty = "数量";
$i_Merit_Reason = "原因";
$i_Merit_Remark = "备注";
$i_Merit_DateModified = "最后更新日期";
$i_Merit_Award = "奖励";
$i_Merit_Punishment = "惩罚";
$i_Merit_NoAwardPunishment = "不适用";
$i_Merit_Merit = "优点";
$i_Merit_MinorCredit = "小功 ";
$i_Merit_MajorCredit = "大功 ";
$i_Merit_SuperCredit = "超功 ";
$i_Merit_UltraCredit = "极功 ";
$i_Merit_Warning = "警告";
$i_Merit_BlackMark = "缺点 ";
$i_Merit_MinorDemerit = "小过 ";
$i_Merit_MajorDemerit = "大过 ";
$i_Merit_SuperDemerit = "超过 ";
$i_Merit_UltraDemerit = "极过 ";
$i_Merit_MinorCredit_unicode = "小&#21151; ";
$i_Merit_MajorCredit_unicode = "大&#21151; ";
$i_Merit_SuperCredit_unicode = "超&#21151; ";
$i_Merit_UltraCredit_unicode = "极&#21151; ";

$i_Merit_TypeArray = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_Warning,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_Short_Merit = "优点";
$i_Merit_Short_MinorCredit = "小功 ";
$i_Merit_Short_MajorCredit = "大功 ";
$i_Merit_Short_SuperCredit = "超功 ";
$i_Merit_Short_UltraCredit = "极功 ";
$i_Merit_Short_BlackMark = "缺点";
$i_Merit_Short_MinorDemerit = "小过";
$i_Merit_Short_MajorDemerit = "大过";
$i_Merit_Short_SuperDemerit = "超过";
$i_Merit_Short_UltraDemerit = "极过";
$i_Merit_Short_TypeArray = array(
$i_Merit_Short_Merit,
$i_Merit_Short_MinorCredit,
$i_Merit_Short_MajorCredit,
$i_Merit_Short_SuperCredit,
$i_Merit_Short_UltraCredit,
$i_Merit_Short_BlackMark,
$i_Merit_Short_MinorDemerit,
$i_Merit_Short_MajorDemerit,
$i_Merit_Short_SuperDemerit,
$i_Merit_Short_UltraDemerit
);

$i_Merit_QtyMustBeInteger = "$i_Merit_Qty 必须为正整数";
$i_Merit_Unit = "个";
$i_Merit_CountByReason = "以原因分类";
$i_Merit_Count = "次数";
$i_Merit_DetailedMeritRecord = "详细奖惩纪录";

$i_Merit_ImportInstruction1 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设)<span style=\"color:red;\">*请用英文填写</span>.<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设)<span style=\"color:red;\">*请用英文填写</span.<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四栏 : 学生班别 (注意大小写)<span style=\"color:red;\">*请用英文填写</span>.<br>
第五栏 : 学生班号<br>
第六栏 : 类别 (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
第七栏 : $i_Merit_Qty.<br>
第八栏 : $i_Merit_Reason (可选择留空).<br>
第九栏 : $i_Merit_Remark (可选择留空).<br>
第十栏 : 负责人的登入账号 (可选择留空).
";

$i_Merit_ImportInstruction2 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设)<span style=\"color:red;\">*请用英文填写</span>.<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设)<span style=\"color:red;\">*请用英文填写</span>.<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (如留空即代表今天).<br>
第四栏 : WebSAMS 注册号码 (帐号由 # 开始 如  #123456).<br>
第五栏 : 类别 (C1 - $i_Merit_Merit, C2 - $i_Merit_MinorCredit, C3 - $i_Merit_MajorCredit,
C4 - $i_Merit_SuperCredit, C5 - $i_Merit_UltraCredit,
D1 - $i_Merit_BlackMark, D2 - $i_Merit_MinorDemerit, D3 - $i_Merit_MajorDemerit, D4 - $i_Merit_SuperDemerit,
D5 - $i_Merit_UltraDemerit)<br>
第六栏 : $i_Merit_Qty.<br>
第七栏 : $i_Merit_Reason (可选择留空).<br>
第八栏 : $i_Merit_Remark (可选择留空).<br>
第九栏 : 负责人的登入账号 (可选择留空).
";

$i_Service_Count = "数量";
$i_ServiceYear = "年度";
$i_ServiceSemester = "学期";
$i_ServiceDate = "日期";
$i_ServiceName = "服务名称";
$i_ServiceRole = "职位";
$i_ServiceOrganization = "服务机构";
$i_ServicePerformance = "表现";
$i_ServiceLastModified = "最后更新时间";
$i_ServiceRemark = "备注";
$i_ServiceNoRecord = "暂时没有任何服务纪录.";
$i_ServiceDateCanBeIgnored = "(如此纪录为一学期之服务,可留空此格)";

$i_Service_ImportInstruction1 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (可选择留空).<br>
第四栏 : 学生班别 (注意大小写).<br>
第五栏 : 学生班号<br>
第六栏 : $i_ServiceName<br>
第七栏 : $i_ServiceRole (可选择留空).<br>
第八栏 : $i_ServicePerformance (可选择留空).<br>
第九栏 : $i_ServiceRemark (可选择留空).<br>
第十栏 : $i_ServiceOrganization (可选择留空).
";

$i_Service_ImportInstruction2 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (可选择留空).<br>
第四栏 : WebSAMS 注册号码 (帐号由 # 开始 如  #123456).<br>
第五栏 : $i_ServiceName<br>
第六栏 : $i_ServiceRole (可选择留空).<br>
第七栏 : $i_ServicePerformance (可选择留空).<br>
第八栏 : $i_ServiceRemark (可选择留空).<br>
第九栏 : $i_ServiceOrganization (可选择留空).
";

$i_Award_Count = "数量";
$i_AwardYear = "年度";
$i_AwardSemester = "学期";
$i_AwardDate = "日期";
$i_AwardName = "奖项";
$i_AwardRole = "职位";
$i_AwardOrganization = "颁发机构";
$i_AwardSubjectArea = "项目名称";
$i_AwardPerformance = "表现";
$i_AwardLastModified = "最后更新时间";
$i_AwardRemark = "备注";
$i_AwardNoRecord = "暂时没有任何奖项纪录.";
$i_AwardDateCanBeIgnored = "(如此纪录为一学期之服务,可留空此格)";

$i_Award_ImportInstruction1 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (可选择留空).<br>
第四栏 : 学生班别 (注意大小写).<br>
第五栏 : 学生班号<br>
第六栏 : $i_AwardName<br>
第七栏 : $i_AwardRemark (可选择留空).<br>
第八栏 : $i_AwardOrganization (可选择留空).<br>
第九栏 : $i_AwardSubjectArea (可选择留空).
";

$i_Award_ImportInstruction2 = "<b><u>档案说明:</u></b><br>
第一栏 : 纪录年度 (如留空即为本年度. 可在基本设定预设).<br>
第二栏 : 纪录学期 (如留空即为本学期. 可在基本设定预设).<br>
第三栏 : 纪录日期 (YYYY-MM-DD) (可选择留空).<br>
第四栏 : WebSAMS 注册号码 (帐号由 # 开始 如  #123456).<br>
第五栏 : $i_AwardName<br>
第六栏 : $i_AwardRemark (可选择留空).<br>
第七栏 : $i_AwardOrganization (可选择留空).<br>
第八栏 : $i_AwardSubjectArea (可选择留空).
";

$i_MyAccount_StudentID = "学生证";
$i_MyAccount_SchoolRecord = "学校纪录";
$i_MyAccount_Library = "图书纪录";
$i_MyAccount_Usage = "用户登入纪录";
$i_MyAccount_CampusMail = "校园信箱";
$i_MyAccount_WebMail = "网上电邮";
$i_MyAccount_FileCabinet = "个人文件柜";

$i_Teaching_TeacherName = "教师姓名";
$i_Teaching_ClassTeacher = "班主任";
$i_Teaching_SubjectTeacher = "科目老师";
$i_Teaching_SubjectTaught = "任教科目数目";
$i_Teaching_NotClassTeacher = "不是班主任";
$i_Teaching_TeachingArrangement = "教职调配";
$i_Teaching_ClassSubjects = "任教科目/班别";
$i_Teaching_NotApplicable = "不适用";
$i_Teaching_Class = "班别";
$i_Teaching_Subject = "科目";
$i_Teaching_ImportInstruction = "<b><u>档案说明:</u></b><br>
第一栏 : 教师内联网帐号 (如 tmchan ).<br>
第二栏 : 班主任的班别 (如 1A. 如非班主任, 请留空此栏).<br>
第三栏 : 任教的班别 (如 1A)<br>
第四栏 : 任教的科目 (如 English, 与第三栏为一对) <br>
第五栏及以后的栏位 : 任教的班别及科目, 必须以一对出现<br>
班别及科目名称必须完全正确 (分大小写)<br><br>
<b>注意现时设定将会被取代.</b>";
$i_Teaching_ClassList = "班别列表";
$i_Teaching_SubjectList = "科目列表";
$i_Teaching_ImportFailed = "以下的纪录不能被汇入";

$i_Form_Name = "表格名称";
$i_Form_Description = "备注";
$i_Form_Type = "种类";
$i_Form_LastModified = "最后更新时间";
$i_Form_Templates = "范本";
$i_Form_Form = "表格";
$i_Form_Suspended = "停用";
$i_Form_Approved = "可用";
$i_Form_Assessment = "学生评估";
$i_Form_Activity = "活动报名";
$i_Form_ConstructForm = "制作表格";
$i_Form_AssessmentDone = "已完成评估表";
$i_Form_FormEditNotAllow = "此表格已被使用, 所以不能更改表格内容. 更改表格内容会引致资料错误. 你可以选择更改表格属性或停用此表格, 然后新增另一张表格代替(不能同名).";
$i_Form_FormNameMustBeUnique = "(必须是唯一)";
$i_Form_RemovalAlert = "删除这些表格会同时删除使用这些表格的资料, 你是否确定要删除? <BR> (如果你希望保留资料, 可以停用这些表格.)";
$i_Form_answer_sheet="网上表格制作";
$i_Form_answersheet_template="范本";
$i_Form_answersheet_header="题目 / 标题";
$i_Form_answersheet_no="题数";
$i_Form_answersheet_type="填写方式";
$i_Form_answersheet_maxno="问题数目已超越已选择指数极限: 英文字母为 'a - z';  罗马数字为 'i - x'!";
$i_Form_no_options_for = "不用为此填写方式选择数目!";
$i_Form_pls_specify_type = "请选定填写方式!";
$i_Form_pls_fill_in = "请输入内容!";
$i_Form_chg_title = "更改题目/标题:";
$i_Form_chg_template = "确定变换应用范本?";
$i_Form_answersheet_tf="是非项";
$i_Form_answersheet_mc="多项选择";
$i_Form_answersheet_mo="可选多项";
$i_Form_answersheet_sq1="填写(短)";
$i_Form_answersheet_sq2="填写(长)";
$i_Form_answersheet_option="选项数目";
$i_Form_answersheet_not_applicable="不用填写";
$i_Form_ShowHide = "显示／隐藏";
$i_Form_ShowDetails = "显示详情";
$i_Form_HideDetails = "隐藏详情";
$i_Form_TotalNo = "总共人数";

$i_Assessment_Year = "年份";
$i_Assessment_Semester = "学期";
$i_Assessment_Date = "评估日期";
$i_Assessment_By = "评估者";
$i_Assessment_LastModified = "最后更新日期";
$i_Assessment_SelectForm = "选择表格";
$i_Assessment_PleaseSelectAForm = "请选择一张表格";
$i_Assessment_FillForm = "填写表格";
$i_Assessment_PleaseFillAForm = "请填上相关资料及按[显示表格]显示评估表";
$i_Assessment_Open = "显示表格";
$i_Assessment_UserSelectAdmin = "如不选择教职员, 评估者将会显示为 $i_general_sysadmin.";
$i_Assessment_OriginalAssessBy = "原来评估者";

$i_ReferenceFiles_Qty = "档案数目";
$i_ReferenceFiles_Title = "标题";
$i_ReferenceFiles_FileType = "档案类型";
$i_ReferenceFiles_NewFile = "新增档案";
$i_ReferenceFiles_SelectFile = "选择档案";
$i_ReferenceFiles_Description = "档案说明";
$i_ReferenceFiles_FileName = "档案名称";

$i_PrinterFriendlyPage = "可列印格式";
$i_PrinterFriendly_StudentName = "学生姓名";

$i_UsageStartTime = "登入时间";
$i_UsageEndTime = "登出时间";
$i_UsageDuration = "逗留时间";
$i_UsageHost = "用户地址";
$i_UsageToday = "今天";
$i_UsageLastWeek = "最近一星期";
$i_UsageLast2Week = "最近两星期";
$i_UsageLastMonth = "最近一个月";
$i_UsageAll = "全部";
$i_Usage_Hour = "小时";
$i_Usage_Min = "分";
$i_Usage_Sec = "秒";
$i_Usage_RemoveRecords = "删除纪录";
$i_Usage_Remove1 = "删除在 ";
$i_Usage_Remove2 = " 及以前的纪录.";
$i_Usage_RemoveConfirm = "确定要删除纪录?";

$i_ClubsEnrollment = "学会报名";
$i_ClubsEnrollment_BasicSettings = "基本设定";
$i_ClubsEnrollment_StudentRequirement = "学生要求设定";
$i_ClubsEnrollment_QuotaSettings = "小组使用及限额设定";
$i_ClubsEnrollment_Mode = "模式";
$i_ClubsEnrollment_AppStart = "报名开始日期";
$i_ClubsEnrollment_AppEnd = "报名结束日期";
$i_ClubsEnrollment_GpStart = "小组批核开始日期";
$i_ClubsEnrollment_GpEnd = "小组批核结束日期";
$i_ClubsEnrollment_ApplicationDescription = "申请规则";
$i_ClubsEnrollment_DefaultMin = "预设学生最少须选";
$i_ClubsEnrollment_DefaultMax = "预设学生最多可选";
$i_ClubsEnrollment_TieBreakRule = "申请过多时的优先考虑次序";
$i_ClubsEnrollment_ClearRecord = "清除所有报名纪录(用于新学年开始报名时)";
$i_ClubsEnrollment_Disable = "不使用";
$i_ClubsEnrollment_Simple = "简单";
$i_ClubsEnrollment_Advanced = "进阶";
$i_ClubsEnrollment_NoLimit = "无限制";
$i_ClubsEnrollment_Random = "随机";
$i_ClubsEnrollment_AppTime = "先到先得";
$i_ClubsEnrollment_DefaultMaxMinWrong = "预设最少数目不能大于预设最多数目";
$i_ClubsEnrollment_EnrollMaxWrong = "最多可参加数目不能大于预设最多数目和不能小于预设最少数目";
$i_ClubsEnrollment_AppEndGpStart = "报名时段和小组批核时段不能重迭。";
$i_ClubsEnrollment_AppStartAppEnd = "报名时段不正确。";
$i_ClubsEnrollment_GpStartGpEnd = "小组批核时段不正确。";
$i_ClubsEnrollment_UseDefault = "使用预设值?";
$i_ClubsEnrollment_Min = "最少";
$i_ClubsEnrollment_Max = "最多";
$i_ClubsEnrollment_GroupName = "学会名称";
$i_ClubsEnrollment_AllowEnrol = "使用报名系统?";
# added on 10 Sept 2008
$i_ClubsEnrollment_OnlineRegistration = "网上登记";
$i_ClubsEnrollment_ClubName = "学会名称";

$i_ClubsEnrollment_Quota = "组员限额";
$i_ClubsEnrollment_ApplicationPeriod = "报名期";
$i_ClubsEnrollment_From = "由";
$i_ClubsEnrollment_To = "至";
$i_ClubsEnrollment_MinForStudent = "最少须要选择学会数目";
$i_ClubsEnrollment_MaxForOwnWill = "你希望最多参加的学会数目";
$i_ClubsEnrollment_Priority = "次序";
$i_ClubsEnrollment_ClubsForSelection = "可选择学会";
$i_ClubsEnrollment_Status = "状况";
$i_ClubsEnrollment_StatusWaiting = "候批";
$i_ClubsEnrollment_StatusRejected = "已拒绝";
$i_ClubsEnrollment_StatusApproved = "已批核";
$i_ClubsEnrollment_Alert_NotEnough = "你并未选择足够的学会";
$i_ClubsEnrollment_Alert_Reject = "你确定要拒绝申请?";
$i_ClubsEnrollment_LastSubmissionTime = "最后提交日期";
$i_ClubsEnrollment_EnrollmentList = "报名名单";
$i_ClubsEnrollment_NotSubmitted = "尚未报名";
$i_ClubsEnrollment_GroupQuota1 = "此学会人数上限为";
$i_ClubsEnrollment_ActivityQuota1 = "此活动人数上限为";
$i_ClubsEnrollment_GroupQuotaNo = "此学会没有人数上限";
$i_ClubsEnrollment_GroupApprovedCount = "已经批准加入的人数";
$i_ClubsEnrollment_SpaceLeft = "余额";
$i_ClubsEnrollment_NoQuota = "无限额";
$i_ClubsEnrollment_NumOfStudent = "学生人数";
$i_ClubsEnrollment_NumOfReceived = "已提交报名表的学生";
$i_ClubsEnrollment_NumOfNotFulfilSchool = "未符合学校要求的学生";
$i_ClubsEnrollment_NumOfNotFulfilOwn = "符合学校要求, 但未符合个人要求的学生";
$i_ClubsEnrollment_NumOfSatisfied = "符合学校要求及个人要求的学生";
$i_ClubsEnrollment_NotHandinList = "欠交的学生名单";
$i_ClubsEnrollment_HeadingNotHandinList = "以下学生并未提交学会报名表";
$i_ClubsEnrollment_HeadingNotFulfilSchool = "以下学生并未符合学校要求";
$i_ClubsEnrollment_HeadingNotFulfilOwn = "以下学生已符合学校要求, 但并未符合学生个人意愿";
$i_ClubsEnrollment_HeadingSatisfied = "以下学生已符合学校及个人要求";
$i_ClubsEnrollment_NameList = "[学生名单]";
$i_ClubsEnrollment_AllMemberList = "各组名单";
$i_ClubsEnrollment_NoMember = "未有已批核组员";
$i_ClubsEnrollment_CurrentSize = "会员人数";
$i_ClubsEnrollment_GoLottery = "进行抽签";
$i_ClubsEnrollment_Proceed2NextRound = "进行下一轮报名程序";
$i_ClubsEnrollment_FinalConfirm = "确认整个报名程序完成";
$i_ClubsEnrollment_Confirm_Clear2Next = "你确定要进行下一轮? (请确定你已在设定中更改报名日期.)";
$i_ClubsEnrollment_Confirm_GoLottery = "你是否确认进行随机抽签？";
$i_ClubsEnrollment_Confirm_FinalConfirm = "你确定要完成整个报名程序?";
$i_ClubsEnrollment_Form_Updated = "你的表格已经更新及递交";
$i_ClubsEnrollment_LotteryInProcess = "抽签进行中，请等候片刻，并且不要关闭此视窗。";
$i_ClubsEnrollment_LotteryFinished = "抽签已完成。请关闭此视窗。";
$i_ClubsEnrollment_Mail1 = "学校的学会报名程序已进入下一轮. 你的报名结果如下:";
$i_ClubsEnrollment_MailChoice = "选择";
$i_ClubsEnrollment_Mail2 = "如你的部份申请未被批核, 你可选择其他学会, 而报名期由 ";
$i_ClubsEnrollment_Mail3 = "至 ";
$i_ClubsEnrollment_Mail4 = "请到首页按课外活动登记检视结果及输入此轮的选择(如需要).";
$i_ClubsEnrollment_MailSubject = "学会报名进入下一轮";
$i_ClubsEnrollment_MailConfirm1 = "学会报名程序已完成. 你的报名结果如下:";
$i_ClubsEnrollment_MailConfirm2 = "你应该可在小组资讯中, 寻找你已加入的学会资讯.";
$i_ClubsEnrollment_MailConfirm3 = "如有问题, 请联络系统管理员或负责课外活动的导师.";
$i_ClubsEnrollment_MailConfirmSubject = "学会报名结果";
//$i_ClubsEnrollment_Warning_NotUse = "如果你选取任何级别为不使用, 则该级别已递交之申请将会被取消";
$i_ClubsEnrollment_Warning_NotUse = "若把某级别设定为「不使用」，则该级别的申请人纪录会被删除。";

$i_Survey = "问卷调查";
$i_Survey_Poster = "制作人";
$i_Survey_Progress = "进行中";
$i_Survey_Finished = "已完成";
$i_Survey_PublicInstruction = "如果你期望所有用户都能看到此问卷调查, 请把小组一栏留空.";
$i_Survey_PleaseFill = "请填写以下调查问卷, 然后按提交. 问卷一经提交, 将不能更改.";
$i_Survey_Result = "你的问题已经被提交. 以下是你的答案纪录. 你可以列印本页, 或关闭本视窗.";
$i_Survey_Overall = "总括调查结果";
$i_Survey_Detail = "检视详细结果";
$i_Survey_perGroup = "检视单一小组结果";
$i_Survey_perIdentity = "检视单一身份结果(教职员,学生,家长)";
$i_Survey_perClass = "检视单一班别结果";
$i_Survey_perUser = "检视单一问卷结果";
$i_Survey_NoSurveyForCriteria = "此选择并没有已提交的问卷";
$i_Survey_RemoveConfirm = "删除这些问卷会同时删除问卷的答案及统计, 你是否确定要删除?";
$i_Survey_NewSurvey = "份未填问卷调查";
$i_Survey_NoSurveyAvailable = "目前未有问卷调查";
$i_Survey_SurveyConstruction = "问卷制作";
$i_Survey_FillSurvey = "填写问卷";
$i_Survey_FilledSurvey = "已填妥问卷";
$i_Survey_ClassList = "选择班别";
$i_Survey_GroupList = "选择组别";
$i_Survey_IdentityList = "选择身份";
$i_Survey_UserList = "选择用户";
$i_Survey_PleaseSelectType = "请选择其中一项";
$i_Survey_UnfillList = "未填问卷名单";
$i_Survey_AllFilled = "没有未填用户";
$i_Survey_Anonymous = "不记名";
$i_Survey_Anonymous_Description = "不能浏览单一问卷结果及名单";
$i_Survey_AllRequire2Fill = "所有题目必须回答";
$i_Survey_alert_PleaseFillAllAnswer = "必须回答此问卷的所有问题";

$i_Notice_ElectronicNotice2 = "电子通告";
$i_Notice_ElectronicNotice = "电子通告系统";
$i_Notice_ElectronicNoticeSettings = "电子通告设定";
$i_Notice_ElectronicNotice_History = "昔日通告";
$i_Notice_ElectronicNotice_Current = "现时通告";
$i_Notice_ElectronicNotice_All = "所有学校通告";
$i_Notice_Disable = "不使用电子通告";
$i_Notice_Setting_FullControlGroup = "进阶管理小组 (可删除通告及代家长更改回条).";
$i_Notice_Setting_NormalControlGroup = "一般管理小组 (可发通告及检视回条).";
# 20090306 yat
$i_Notice_Setting_DisciplineGroup = "整批列印操行纪录系统通告";

$i_Notice_Setting_DisableClassTeacher = "不让班主任代家长更改回条.";
$i_Notice_Setting_AllHaveRight = "所有教职员可发通告.";
$i_Notice_Setting_DefaultNumDays = "预设签署期限日数.";
$i_Notice_Setting_AdminGroupOnly = "必须为行政小组";
$i_Notice_Setting_ParentStudentCanViewAll = "让所有家长及学生检视所有通告.";
$i_Notice_New = "新增通告";
$i_Notice_Edit = "编辑通告";
$i_Notice_NoticeNumber = "通告编号";
$i_Notice_Title = "通告标题";
$i_Notice_Description = "通告内容";
$i_Notice_DateStart = "发出日期";
$i_Notice_DateEnd = "签署限期";
$i_Notice_Issuer = "发出人";
$i_Notice_RecipientType = "适用对象";
$i_Notice_RecipientTypeAllStudents = "全校";
$i_Notice_RecipientTypeLevel = "部份级别";
$i_Notice_RecipientTypeClass = "部份班别";
$i_Notice_RecipientTypeIndividual = "相关同学";
$i_Notice_RecipientLevel = "级别";
$i_Notice_RecipientClass = "班别";
$i_Notice_RecipientIndividual = "个别学生/组别";
$i_Notice_ReplyContent = "设定回条内容";
$i_Notice_Attachment = "通告附件";
$i_Notice_FromTemplate = "载入范本";
$i_Notice_NotUseTemplate = "不使用范本";
$i_Notice_Type = "类别";
$i_Notice_StatusPublished = "已派发";
$i_Notice_StatusSuspended = "未派发";
$i_Notice_StatusTemplate = "范本";
$i_Notice_ViewOwnClass = "检视班别";
$i_Notice_Signed = "已签";
$i_Notice_Total = "总数";
$i_Notice_ViewResult = "检视结果";
$i_Notice_ReplySlip = "回条";
$i_Notice_ReplySlip_Signed = "以下为已签回之回条内容. 你可以列印此页或关闭视窗.";
$i_Notice_StudentName = "学生姓名";
$i_Notice_OpenSign = "开启";
$i_Notice_Sign = "签署";
$i_Notice_SignStatus = "签署状况";
$i_Notice_Unsigned = "未签";
$i_Notice_Signer = "签署人";
$i_Notice_Editor = "代签署人:";
$i_Notice_SignerNoColon = "签署人";
$i_Notice_SignedAt = "签署时间";
$i_Notice_At = "在";
$i_Notice_SignInstruction = "请填妥以上回条，再按签署。";
$i_Notice_SignInstruction2 = "签署后如有修改，请修改后再签署。";
$i_Notice_NoRecord = "暂时没有通告";
$i_Notice_MyNotice = "我发出的通告";
$i_Notice_AllNotice = "所有通告";
$i_Notice_RemovalWarning = "注意: 移除此通告会一并移除家长交回的回条. 你确定要继续?";
$i_Notice_TemplateNotes = "(只有 <font color=green>$i_Notice_Title</font>, <font color=green>$i_Notice_Description</font>, 和 <font color=green>$i_Notice_ReplySlip</font> 会成为范本的内容.)";
$i_Notice_Alert_DateInvalid = "签署日期必须是或迟于发出日期";
$i_Notice_ResultForEachClass = "各班情况";
$i_Notice_TableViewReply = "表列回条内容";
$i_Notice_ViewStat = "统计资料";
$i_Notice_NoReplyAtThisMoment = "暂时没有已签回条";
$i_Notice_NumberOfSigned = "已签回人数";
$i_Notice_Option = "选项";
$i_Notice_NotForThisClass = "此通告并没有派发给此班的任何学生";
$i_Notice_NoStudent = "此通告并没有派发给任何学生.";
$i_Notice_AllStudents = "所有同学";
$i_Notice_Alert_Sign = "你所填写的回条将被呈送. 确定签署通告并递交回条?";
$i_Notice_CurrentList = "现时通告清单";
$i_Notice_ListIncludingSuspend = "包括已派发及未派发";
$i_Notice_SendEmailToParent = "向家长发出电邮通知 (如类别为$i_Notice_StatusPublished)";

# 20090227 - eNotice with Discipline Notice enhancement
$eNotice['SchoolNotice'] = "学校通告";
$eNotice['DisciplineNotice'] = "操行纪录系统通告";
$eNotice['Period_Start'] = "日期由";
$eNotice['Period_End'] = "至";

#20090611 eDisciplinev12 template setting
$eNotice['ReplySlipType'] = "回条类别";
$eNotice['QuestionBase'] = "问题模式";
$eNotice['ContentBase'] = "便条模式";

$i_LinuxAccountQuotaSetting = "iFolder 储存量设定";
$i_LinuxAccount_SetDefaultQuota = "设定预设储存量";
$i_LinuxAccount_SetUserQuota = "设定个别用户储存量";
$i_LinuxAccount_Quota_Description = "此储存量包括: ";
$i_LinuxAccount_Webmail = "网上电邮";
$i_LinuxAccount_Campusmail = "外来电邮暂存 (当用户检视收件箱时, 邮件会被下载而不再占储存空间.)";
$i_LinuxAccount_PersonalFile = "个人文件柜";
$i_LinuxAccount_Quota = "储存量";
$i_LinuxAccount_NotExist = "户口并不存在, 请再检查一次. 大小写须完全符合.";
$i_LinuxAccount_AccountName = "户口名称";
$i_LinuxAccount_Alert_NameMissing = "请输入户口名称";
$i_LinuxAccount_Alert_QuotaMissing = "请输入限额";
$i_LinuxAccount_DisplayQuota = "检视用户储存量";
$i_LinuxAccount_SelectUserType = "选择用户类别";
$i_LinuxAccount_SelectGroup = "选择组别";
$i_LinuxAccount_UsedQuota = "已用储存量";
$i_LinuxAccount_CurrentQuota = "现时储存量";
$i_LinuxAccount_SetGroupQuota = "设定全组/身分用户的储存量";
$i_LinuxAccount_IncreaseOnly = "只增加不减少";
$i_LinuxAccount_Reset = "重设所有此身份/组员用户";
$i_LinuxAccount_UserSetNote = "要注意设定限额为 0 即等于没有限制.";
$i_LinuxAccount_GroupSetNote = "要注意设定限额为 0 即等于没有限制.<br>如用户人数较多, 请耐心等候.";
$i_LinuxAccount_NoAccount = "没有系统户口";
$i_LinuxAccount_NoLimit = "没有限制";
$i_LinuxAccount_Webmail_QuotaSetting = "电邮暂存设定";
$i_LinuxAccount_Folder_QuotaSetting = "iFolder 储存量";
$i_LinuxAccount_PersonalFile_Description = "此设定会修改文件伺服器的储存量, 请小心使用.";
$i_LinuxAccount_iMail_Description = "此储存量只用于暂时存放外来的互联网邮件";
$i_Linux_Description_ImportUser = "系统只会为新 eClass 用户, 在档案/邮件伺服器开启户口. 现有用户需要在 <b>功能设定 &gt; $i_LinuxAccount_Folder_QuotaSetting</b> 设定";
$i_LinuxAccount_BatchProcess = "整批处理";
$i_LinuxAccount_NewCreation = "新增系统户口";
$i_LinuxAccount_prompt_TypeRestricted = "此户口所属身份不能使用此项服务.";

$i_Files_ConnectionFailed_Remote = "不能连结档案伺服器. 请确定密码是否正确.";
$i_Files_ConnectionFailed_Local = "不能连结档案伺服器. 请再更改一次密码.";
$i_Files_ConnectionFailed = "不能连结档案伺服器. 请再更改一次密码. 如此错误仍然发生, 请联络系统管理员.";
$i_Files_CurrentDirectory = "现时位置";
$i_Files_Type = "种类";
$i_Files_Size = "大小";
$i_Files_Date = "最后更新日期";
$i_Files_Name = "档案名称";
$i_Files_NewFolder = "新增资料夹";
$i_Files_Upload = "上载";
$i_Files_Files = "档案";
$i_Files_Move = "移动";
$i_Files_Delete = "移除";
$i_Files_Rename = "重新命名";
$i_Files_ClickToDownload = "按此下载档案";
$i_Files_ThisIsPublic = "(此资料夹的档案是能被其他人存取)";
$i_Files_UploadTarget = "上载档案至 : ";
$i_Files_To = "至";
$i_Files_OpenAccount = "允许使用 iFolder";
$i_Files_LinkFTPAccount = "连结档案伺服器及 iFolder";
$i_Files_LinkToAero = "允许使用个人文件柜, 并连接至 AeroDrive.";
$i_Files_UseIFolder = "允许使用 iFolder";
$i_Files_Description_Unlink = "如果没有选择, 用户将不能在 eClass 中使用 iFolder. 但档案伺服器的户口仍然保留.";
$i_Files_Description_Link = "如果选择此项, 系统会在档案伺服器新增此户口.";
#$i_Files_AllowUserUseFTPAccount = "允许用户使用个人文件柜. (如作业系统(linux) 户口已开启)";
$i_Files_DefaultQuotaSameServer = "如果邮件伺服器及档案伺服器是同一电脑, 请在 iMail 设定中更改. (此处资料会被略过)";
$i_Files_Linked = "已连结 iFolder";
$i_Files_LinkIfolder = "连结 iFolder";
$i_Files_BatchOption_OpenAccount = "新增档案伺服器户口及连结 iFolder. 储存量为: ";
$i_Files_BatchOption_UnlinkAccount = "不连结 iFolder <font color=red>(档案伺服器上的户口<b><u>不会</u></b> 被删除)</font>";
$i_Files_BatchOption_RemoveAccount = "删除档案伺服器上的户口 <font color=red>(如邮件伺服器为同一电脑, 则 iMail 亦会受影响)</font>";
$i_Files_alert_RemoveAccount = "你确定要移除这个档案伺服器的户口? (如果邮件伺服器是同一电脑, 删除会一并影响邮件接收)";
$i_Files_CheckQuota = "检视储存量";
$i_Files_msg_FailedToWrite = "档案上载失败. 可能之原因:<br>
1. 超出储存量. 请移除部份档案.<br>
2. 其中一些档案名称系统不能接受. 请更改部份档案名称 (至一般英文或UTF-8 中文字).<br>
如果问题仍然存在, 请联络系统管理员.<br>
";
$i_Files_ClickHereToBrowse = "按此返回档案列表.";


$i_CampusTV_BasicSettings = "基本设定";
$i_CampusTV_LiveBroadcastManagement = "网上直播管理";
$i_CampusTV_ChannelManagement = "频道管理";
$i_CampusTV_VideoManagement = "影片管理";
$i_CampusTV_BulletinManagement = "讨论区管理";
$i_CampusTV_PollingManagement = "投票管理";
$i_CampusTV_AccessByPublic = "开放给公众 (无须登入)";
$i_CampusTV_DisableBulletin = "不使用讨论区";
$i_CampusTV_DisablePolling = "不使用投票";
$i_CampusTV_LiveBroadcast = "网上直播";
$i_CampusTV_LiveURL = "直播连结";
$i_CampusTV_ChannelName = "频道名称";
$i_CampusTV_ClipName = "影片名称";
$i_CampusTV_Recommended = "推介";
$i_CampusTV_RecommendedStatus = "是";
$i_CampusTV_Type_Link = "连结形式";
$i_CampusTV_Type_File = "档案形式";
$i_CampusTV_MovieURL = "影片连结";
$i_CampusTV_InputType = "输入方式";
$i_CampusTV_SelectMovieFile = "选择影片档案";
$i_CampusTV_MovieProvider = "提供者";
$i_CampusTV_Play = "播放";
$i_CampusTV_Alert_Recommend = "你确定把已选的影片定为推介?";
$i_CampusTV_Alert_CancelRecommend = "你确定把已选的影片由推介中剔除?";
$i_CampusTV_LiveProgrammeList = "节目表";
$i_CampusTV_Time = "时段";
$i_CampusTV_Programme = "节目名称";
$i_CampusTV_SelectClip = "选择片段";
$i_CampusTV_SelectChannel = "选择频道";
$i_CampusTV_PollingName = "投票名称";
$i_CampusTV_Reference = "参考";
$i_CampusTV_NumClips = "影片数目";
$i_CampusTV_PollingSelectClips = "选择影片";
$i_CampusTV_PollingCandidate = "候选";
$i_CampusTV_ViewProgrammer = "收看节目";
$i_CampusTV_RecommendedList = "最新推介节目";
$i_CampusTV_MostChannel = "最受欢迎频道";
$i_CampusTV_MostClip = "最受欢迎节目";
$i_CampusTV_Bulletin = "节目讨论区";
$i_CampusTV_Upload = "节目上载";
$i_CampusTV_Polling = "节目投票区";
$i_CampusTV_UploadSuccessful =  "<font color=#FFFFFF>影片档案已上载.</font>";
$i_CampusTV_NoPolling = "你现时并没有投票未投.";
$i_CampusTV_NoVote = "并没有用户曾经投票";
$i_CampusTV_ChannelList = "频道一览表";
$i_CampusTV_PastPoll = "昔日投票";
$i_CampusTV_CurrentPoll = "今日投票";
$i_CampusTV_SecureNeeded = "需要登入 eClass IP 才能观看";
$i_CampusTV_URL = "网址";
$i_CampusTV_Guide_Please_Select_Channel="请选择频道";
$i_CampusTV_Guide_Please_Select_Movie="请选择影片";
$i_CampusTV_Alert_No_Movies_Available="没有影片可供选择";
$i_CampusTV_Alert_No_Channels_Available="没有频道可供选择";
$i_CampusTV_Live_Portal_Config = "在 Portal 主页的设定";
$i_CampusTV_Live_Portal_display = "显示";
$i_CampusTV_Live_Portal_size = "尺寸";
$i_CampusTV_Live_Portal_pixel = "像素";
$i_CampusTV_Live_Portal_auto = "自动播放";
$i_CampusTV_Live_Portal_size_width_warn = "影片的宽度必需是1至320之间！";
$i_CampusTV_Live_Portal_size_height_warn = "影片的高度必需大过 0 ！";


$i_Mail_OpenWebmailAccount = "开设网上电邮户口";
$i_Mail_LinkWebmail = "连结网上电邮";
$i_Mail_AllowSendReceiveExternalMail = "允许用户收发外来电邮";
$i_Mail_AllowUserUseWebmail = "允许用户使用网上电邮. (如此用户已有电邮/FTP/Linux, 方为有效)";
$i_Mail_AllowSendReceiveExternalMailEdit = "允许用户收发外来电邮. (如此用户已有电邮/FTP/Linux, 方为有效)";

$i_Help_Show = "eClass 启动讯息";
$i_Help_Description = "你可以在此设定启动讯息，提示所有用户有关开始使用eClass系统时的注意事项。请输入启动讯息的开始日期及结束日期。如任何一栏留空, 即表示该讯息不会在用户登入时显示出来。";
$i_Help_DateStart = "开始日期";
$i_Help_DateEnd = "结束日期";

$i_ec_file_assign = "移交文档拥有权";
$i_ec_file_remove_teacher = "持有档案的老师";
$i_ec_file_exist_teacher = "在此教室的老师";
$i_ec_file_not_exist_teacher = "不在此教室的老师";
$i_ec_file_target_teacher = "对象老师";
$i_ec_file_assign_to = "档案移交给";
$i_ec_file_confirm = "档案移交是不能逆转的!\\n你是否确定要移交?";
$i_ec_file_confirm2 = "对象老师没有在此教室任教，必须把这老师加入此教室才可移交文档拥有权!\\n你是否确定要将这老师加进此教室?";
$i_ec_file_warning = "请最少选择一位持有档案的老师";
$i_ec_file_warning2 = "请选择一位老师来接收文档拥有权";
$i_ec_file_user_delete = "将被删除的eClass用户：";
$i_ec_file_msg_transfer = "删除老师会使该老师拥有的文档失去管理人，可移交拥有权给其他老师：";
$i_ec_file_no_transfer = "不移交";
$i_ec_file_user_delete_confirm = "你是否确定要删除这些eClass用户?";

$i_OrganizationPage_Settings = "组织页面设定";
$i_OrganizationPage_NotDisplayMemberList = "不显示用户名单.";
$i_OrganizationPage_NotDisplayEmailAddress = "不显示用户电邮地址";
$i_OrganizationPage_HideInOrganization = "不显示于组织页面";
$i_OrganizationPage_DisplayOption = "在组织页面的显示";
$i_OrganizationPage_NotDisplay = "全部不显示";
$i_OrganizationPage_Unchange = "不变";
$i_OrganizationPage_DisplayAll = "全部显示";
$i_OrganizationPage_DefaultCat = "预设小组类别";

$i_SpecialRoom = "特别室";
$i_SpecialRoom_ReadingRoom = "阅读室";
$i_SpecialRoom_ReadingRoom_Name = "阅读室名称";
$i_SpecialRoom_ReadingRoom_Description = "描述";
$i_SpecialRoom_ELP = "ELP";
$i_SpecialRoom_iPortfolio = "iPortfolio";

//$i_AdminJob_AdminCenter = "行政中心";

$i_AdminJob_StaffName = "职员姓名";
$i_AdminJob_AdminLevel = "管理权限";
$i_AdminJob_AdminLevel_Normal = "一般权限";
$i_AdminJob_AdminLevel_Full = "完全控制";
$i_AdminJob_AdminLevel_Detail_Normal = "一般权限 (可发出职员通告及观看自己所发通告的结果)";
$i_AdminJob_AdminLevel_Detail_Full = "完全控制 (可发出职员通告及观看所有通告的结果)";
//$i_AdminJob_Description_SetAdmin = "请选择管理人员及选取管理权限.";
//$i_AdminJob_AccessClass = "班别";

$i_AdminJob_Announcement = "宣布批核";

//$i_AdminJob_Announcement_SelectApproval = "需要批核, 批核者:";
$i_AdminJob_Announcement_NoEdit = "<font color=red>(此项宣布已被批核. 因此不能作更改.)</font>";
$i_AdminJob_Announcement_NewEdit = "<font color=red>(此项宣布已被拒绝. 你可以更改纪录而系统会视为新增的纪录.)</font>";
$i_AdminJob_Announcement_ApprovalUser = "批核者";
$i_AdminJob_Announcement_Remark = "备注";
$i_AdminJob_Announcement_HandleAnnouncement = "处理宣布";
$i_AdminJob_Announcement_Handle_Reason = "原因: <br> (给发布人)";
$i_AdminJob_Announcement_Handle_Remark = "私人<!-- 备注 -->:";
$i_AdminJob_Announcement_ApproveSubject = "宣布已被批核";
$i_AdminJob_Announcement_RejectSubject = "宣布已被拒绝";
$i_AdminJob_Announcement_ApproveMessage1 = "你所提交的宣布申请 (";
$i_AdminJob_Announcement_ApproveMessage2 = ") 已得到批准并已发布, 即已显示在有关用户的首页宣布栏内";
$i_AdminJob_Announcement_RejectMessage1 = "你所提交的宣布申请 (";
$i_AdminJob_Announcement_RejectMessage2 = ") 已被拒绝. ";
//$i_AdminJob_Announcement_Reason = "原因为:";
$i_AdminJob_Announcement_AlertWaiting = "项等待纪录";
$i_AdminJob_Announcement_Being = "已被";
$i_AdminJob_Announcement_At = "在";
$i_AdminJob_Announcement_Approved = "批准.";
$i_AdminJob_Announcement_Rejected = "拒绝.";
$i_AdminJob_Announcement_Handle = "处理";
$i_AdminJob_Announcement_Action_Approve = "批准";
$i_AdminJob_Announcement_Action_Waiting = "等待";
$i_AdminJob_Announcement_Action_Reject = "拒绝";
$i_AdminJob_Announcement_LastAction = "上次处理时间";
$i_AdminJob_Announcement_NeedApproval = "经批核";
$i_AdminJob_Announcement_NoApproval = "不经批核";
$i_AdminJob_Announcement_RemoveAdmin = "你确定要取消这个管理员?";

$i_CampusMail_Internal_Recipient_Group ="内在收件组别";
$i_CampusMail_External_Recipient= "外在收件人";
$i_CampusMail_External_Recipient_Group="外在收件组别";
$i_CampusMail_New_iMail = "iMail";
$i_CampusMail_New_FolderManagement = "管理邮件夹";
$i_CampusMail_New_Alert_RemoveFolder = "删除此邮件夹会一并删除内里邮件. 你确定要继续?";
$i_CampusMail_New_FolderName = "邮件夹名称";
$i_CampusMail_New_Internal_Recipient_Group ="新增内在收件组别";
$i_CampusMail_New_Prompt_FolderExist = "此名称的邮件夹已存在";
$i_CampusMail_New_ToFolder = "浏览邮件夹";
$i_CampusMail_New_AddressBook = "通讯录";
$i_CampusMail_New_AddressBook_Name = "名称";
$i_CampusMail_New_AddressBook_Type = "个人/小组";
$i_CampusMail_New_AddressBook_EmailAddress = "电邮地址";
$i_CampusMail_New_AddressBook_Category = "类别";
$i_CampusMail_New_AddressBook_Internal = "Int";
$i_CampusMail_New_AddressBook_External = "Ext";
$i_CampusMail_New_AddressBook_Prompt_Num = "你需要新增多少个纪录?";
$i_CampusMail_New_AddressBook_Alert_NeedInteger = "请输入一个正整数.";
$i_CampusMail_New_AddressBook_ByUser = "用户";
$i_CampusMail_New_AddressBook_ByGroup = "小组";
$i_CampusMail_New_AddressBook_TypeSelect = "选择";
$i_CampusMail_New_InternalRecipients = "内联网收件人";
$i_CampusMail_New_ExternalRecipients = "外在收件人";
$i_CampusMail_New_Settings_PersonalSetting ="基本设定";
$i_CampusMail_New_Settings_Signature="署名栏";
$i_CampusMail_New_Settings_EmailForwarding="转寄邮件";
$i_CampusMail_New_Settings_ReplyEmail ="回复电邮地址(外在邮件)";
$i_CampusMail_New_Settings_DisplayName ="显示名称(外在邮件)";
$i_CampusMail_New_Settings_DaysInSpam ="滥发邮件保留天数";
$i_CampusMail_New_Settings_DaysInTrash ="垃圾箱邮件保留天数";
$i_CampusMail_New_Settings_PleaseFillInYourSignature ="请填上你的署名";
$i_CampusMail_New_Settings_PleaseFillInForwardEmail ="请输入要转寄的电邮地址:<font color=red>(每一行输入一个电邮地址)</font>";
$i_CampusMail_New_Settings_PleaseFillInForwardKeepCopy="保留备份";
$i_CampusMail_New_Settings_Forever="永久保留";
$i_CampusMail_New_Settings_NoticeDefaultDisplayName ="<font color=red>(若不输入，将会使用系统预设的使用者名称。)</font>";
$i_CampusMail_New_Settings_NoticeDefaultReplyEmail  ="<font color=red>(若不输入，将会使用系统预设的回邮地址。)</font>";
$i_CampusMail_New_Settings_DisableEmailForwarding="停用转寄";
$i_CampusMail_New_Settings_POP3 = "POP3";
$i_CampusMail_New_Settings_DisableCheckEmail = "停止使用 iMail 下载互联网邮件";
$i_CampusMail_New_Settings_AutoReply = "自动回复";
$i_CampusMail_New_Settings_EnableAutoReply = "使用自动回复";
$i_CampusMail_New_Settings_PleaseFillInAutoReply = "请输入自动回复之讯息 (只适用于互联网邮件)";
$i_CampusMail_New_Settings_POP3_guideline = "检视电邮软件设定指引";

$i_CampusMail_New_Recipient_Status = "收件人状况";
$i_CampusMail_Warning_External_Recipient_No_Semicolon ="收件人名称不能含有分号 ( ; )";
$i_CampusMail_New_No_Subject = "(没有标题)";
$i_CampusMail_New_No_Sendmail = "你没有此权限";

$i_CampusMail_New_To = "To";
$i_CampusMail_New_CC = "CC";
$i_CampusMail_New_BCC = "BCC";
$i_CampusMail_New_MailFolder = "邮件夹";
$i_CampusMail_New_Settings = "偏好设定";
$i_CampusMail_New_Quota1 = "已使用";
$i_CampusMail_New_Quota2 = "的储存空间";
$i_CampusMail_New_MailSource = "邮件来源";
$i_CampusMail_New_MailSource_Internal = "内联网邮件";
$i_CampusMail_New_MailSource_External = "互联网邮件";
$i_CampusMail_New_Icon_MailSource = "<img src=\"$image_path/frontpage/imail/icon_mailfrom.gif\" border=0>";
$i_CampusMail_New_Icon_IntMail = "<img src=\"$image_path/frontpage/imail/icon_intmail.gif\" alt=\"$i_CampusMail_New_MailSource_Internal\" border=0>";
$i_CampusMail_New_Icon_ExtMail = "<img src=\"$image_path/frontpage/imail/icon_extmail.gif\" alt=\"$i_CampusMail_New_MailSource_External\" border=0>";
$i_CampusMail_New_Icon_NewMail = "<img src=\"$image_path/frontpage/imail/icon_newmail.gif\" border=0>";
$i_CampusMail_New_Icon_ReadMail = "<img src=\"$image_path/frontpage/imail/icon_mailread_$intranet_session_language.gif\" border=0>";
$i_CampusMail_New_Icon_RepliedMail = "<img src=\"$image_path/frontpage/imail/icon_mailreplied_$intranet_session_language.gif\" border=0>";
$i_CampusMail_New_EmailAddressSeparationNote = "请以 ; 或 , 分隔每个电邮地址";
$i_CampusMail_New_NotificationInternalOnly = "只适用于 $i_CampusMail_New_InternalRecipients";
$i_CampusMail_New_ExternalSentSuccess = "$i_CampusMail_New_MailSource_External 传送成功.";
$i_CampusMail_New_ExternalSentFailed = "$i_CampusMail_New_MailSource_External 传送失败.";
$i_CampusMail_New_MoveTo = "移至";
$i_CampusMail_New_alert_moveto = "你确定要移动已选的邮件?";
$i_CampusMail_New_alert_norecipients = "没有任何收件人";
$i_CampusMail_New_NumOfMail = "邮件数量";
$i_CampusMail_New_NoFolder = "请按新增以设定新邮件夹.";
$i_CampusMail_New_MailAliasGroup = "自订收件组别";
$i_CampusMail_New_AliasGroup = "组别名称";
$i_CampusMail_New_AliasGroupRemark = "备注";
$i_CampusMail_New_NumberOfEntry = "数目";
$i_CampusMail_New_AddressBook_TargetGroup = "目标小组";
$i_CampusMail_New_SelectFromAlias = "由收件组别选取";
$i_CampusMail_New_Reply = "回复";
$i_CampusMail_New_ReplyAll = "回复全部";
$i_CampusMail_New_Forward = "转寄";
$i_CampusMail_New_Wrote = "写着...";
$i_CampusMail_New_iMail_Settings = "iMail 设定";
$i_CampusMail_New_QuotaSettings = "邮箱储存量设定";
$i_CampusMail_New_ViewQuota_Identity = "以身份检视";
$i_CampusMail_New_ViewQuota_Group = "以组别检视";
$i_CampusMail_New_Quota = "储存量";
$i_CampusMail_New_BatchUpdate = "整批更新";
$i_CampusMail_New_SingleEdit = "修改单一用户储存量";
$i_CampusMail_New_TargetUser = "欲修改的用户";
$i_CampusMail_New_BatchUpdateTarget = "整批用户更新";
$i_CampusMail_New_DefaultQuota = "预设储存量 (以MBytes 为单位)";
$i_CampusMail_New_ExternalAllow = "允许接收/传送外来互联网邮件";
$i_CampusMail_New_alert_RemoveAccount = "你确定要移除这个邮件伺服器的户口? (如果档案伺服器是同一电脑, 删除会一并影响)";
$i_CampusMail_New_BatchOption_OpenAccount = "连结 iMail, 允许接收/传送互联网邮件. 储存量为: ";
$i_CampusMail_New_BatchOption_UnlinkAccount = "不连结 iMail <font color=red>(邮件伺服器上的户口<b><u>不会</u></b> 被删除)</font>";
$i_CampusMail_New_BatchOption_RemoveAccount = "删除邮件伺服器上的户口 <font color=red>(如档案伺服器为同一电脑, 则 iFolder (如有)亦会受影响)</font>";
$i_CampusMail_New_QuotaNotEnough = "储存量不足以传送所有附件";
$i_CampusMail_New_ViewFormat_PlainText = "观看纯文字格式";
$i_CampusMail_New_ViewFormat_HTML = "观看 HTML 格式";
$i_CampusMail_New_ViewFormat_MessageSource = "观看邮件原文";
$i_CampusMail_New_PlsWait = "请等候 ...";
$i_CampusMail_New_ConnectingMailServer = "正在连接邮件伺服器 ... ";
$i_CampusMail_New_Receiving1 = "已接收";
$i_CampusMail_New_Receiving2 = "封邮件 (共";
$i_CampusMail_New_Receiving3 = "封)";
$i_CampusMail_New_WarningQuotaNotEnough = "请清理阁下的邮箱以接收余下的电邮.";
$i_CampusMail_New_BackMailbox = "返回收件箱";
$i_CampusMail_New_CheckMail = "检查邮件";
$i_CampusMail_New_MailServerNotReachable = "注意: 不能连接邮件伺服器. 请尝试更改密码. 如果此讯息仍然显示, 请联络系统管理员.";
$i_CampusMail_New_MailSearch_Description = "请输入搜寻条件";
$i_CampusMail_New_MailSearch_hasAttachment = "包含附件档案";
$i_CampusMail_New_MailSearch_SearchFolder = "搜寻邮件夹";
$i_CampusMail_New_MailSearch_SearchResult = "搜寻结果";
$i_CampusMail_New_MailSearch_BackToResult = "返回搜寻结果";
$i_CampusMail_New_Confirm_remove_mail = "你确定要移除这封邮件?";
$i_CampusMail_New_BadWords_Settings = "不当字词列表";
$i_CampusMail_New_BadWords_Instruction_top = "你可以输入系统限制的字句. 在此出现的字句在邮件将会被 *** 取代.";
$i_CampusMail_New_BadWords_Instruction_bottom = "请每个字词输入一行";
$i_CampusMail_New_ViewMail = "检视邮件";
$i_CampusMail_New_Inbox = "收件箱";
$i_CampusMail_New_Outbox = "寄件箱";
$i_CampusMail_New_Draft = "草稿箱";
$i_CampusMail_New_Trash = "垃圾箱";
$i_CampusMail_New_Action_Clean = "清理";
$i_CampusMail_New_FolderRename_NewName = "新邮件夹名称";
$i_CampusMail_New_InternalAlias = "自订内部组别";
$i_CampusMail_New_ExternalContact = "对外通讯录";
$i_CampusMail_New_ListAlias = "名单";
$i_CampusMail_New_RemoveBySearch = "特别条件搜寻";
$i_CampusMail_New_RemoveBySearch_SearchPhrase = "搜寻字句";
$i_CampusMail_New_AddToAliasGroup = "加入收件组别";
$i_CampusMail_New_Mail_ExternalContact = "对外连络人";
$i_CampusMail_New_Add_CC = "副本";
$i_CampusMail_New_Add_BCC = "密件副本";
$i_CampusMail_New_IntranetNameList = "内联网名单";
$i_CampusMail_New_SearchRecipient = "搜寻";
$i_CampusMail_Encoding_Warning="此邮件之附件名称含有一些系统无法辨别的字元，收件人可能无法看到正确之附件名称。 请先储存附件后再新增至邮件中。";
$i_CampusMail_New_FolderManager = "档案管理";
$i_CampusMail_New_ComposeMail = "撰写邮件";
$i_CampusMail_New_Mail_Search = "搜寻";
$i_CampusMail_New_Show_All_Recipient = "显示详细资料";
$i_CampusMail_New_Hide_All_Recipient = "隐藏详细资料";
$i_CampusMail_OutOfQuota_Warning="由于你的收件箱储存量不足，你需要清除收件箱以接收新的邮件。";
### added by Ronald on 20090211 ##
$i_CampusMail_ClickHereToDownloadAttachment = "或按此下载";
### added by Ronald on 20090401 ##
$i_CampusMail_Admin_DayInTrashSettings = "垃圾/杂件保留期限";
$i_CampusMail_Admin_DayInTrashSettings_Notice = "* 如用户已设定保留日数，系统将会以该用户的设定作准。";
### added by Ronald on 20090402 ##
$i_CampusMail_New_Settings_EmailRules = "邮件规则";
### added by Ronald on 20090417 ###
$i_CampusMail_Condition = "条件";
$i_CampusMail_Condition_From = "由";
$i_CampusMail_Condition_To = "致";
$i_CampusMail_Condition_Subject = "主旨";
$i_CampusMail_Condition_Message = "内容";
$i_CampusMail_Condition_HasAttachment = "包含附件";
$i_CampusMail_Action = "动作";
$i_CampusMail_Action_MarkAsRead = "标记成已读取";
$i_CampusMail_Action_DeleteIt = "删除";
$i_CampusMail_Action_MoveToFolder = "移至资料夹";
$i_CampusMail_Action_ForwardTo = "转寄致";
$i_CampusMail_MailRule_DeleteActionJSWaring = "如选择\"删除\"，将不能同时选择\"标记成已读取\"或\"移至资料夹\"。";
$i_CampusMail_MailRule_ActionEmptyJSWaring = "请选择最少一项动作。";

$i_Calendar_Admin_Setting = "iCalendar 设定";

$i_SmartCard = "智能卡";
$i_SmartCard_CardID = "智能卡 ID";
$i_SmartCard_DateRecorded = "纪录日期";
$i_SmartCard_TimeRecorded = "纪录时间";
$i_SmartCard_Site = "纪录地点";
$i_SmartCard_StudentOutingDate = "活动/外出日期";
$i_SmartCard_StudentOutingOutTime = "活动/外出时间";
$i_SmartCard_StudentOutingBackTime = "结束时间";
$i_SmartCard_StudentOutingPIC = "负责老师";
$i_SmartCard_StudentOutingFromWhere = "出发地点";
$i_SmartCard_StudentOutingLocation = "活动/外出地点";
$i_SmartCard_StudentOutingReason = "活动/外出目的";
$i_SmartCard_StudentOutingNoOut = "不是由学校出发";
$i_SmartCard_StudentOutingNoBack = "未/没有返回";
$i_SmartCard_ClassName = "班别";
$i_SmartCard_GroupName = "小组";
$i_SmartCard_Remark = "备注";
$i_SmartCard_DetentionDate = "留堂日期";
$i_SmartCard_DetentionArrivalTime = "到达时间";
$i_SmartCard_DetentionDepartureTime = "离开时间";
$i_SmartCard_DetentionReason = "留堂原因";
$i_SmartCard_DetentionLocation = "地点";
$i_SmartCard_DetentionNotArrived = "未到";
$i_SmartCard_DetentionNotLeft = "未离开";
$i_SmartCard_ReportMonth = "月份";
$i_SmartCard_Description_Terminal_IP_Settings = "
在 IP 位址的格上, 可以使用:
<ol><li>固定 IP 位址 (e.g. 192.168.0.101)</li>
<li>IP 位址一个范围 (e.g. 192.168.0.[10-100])</li>
<li>CISCO Style 范围(e.g. 192.168.0.0/24)</li>
<li>容许所有位址 (输入 0.0.0.0)</li></ol>
<font color=red>连接读卡器的电脑, 建议该电脑设定固定 IP 地址 (即不采用 DHCP), 以确保资料不会被其他电脑更改.</font><br>
本系统设有安全措施防范假冒的IP 地址 (Faked IP Address).
";
$i_SmartCard_Instruction_RemoveLog = "请输入日期";
$i_SmartCard_DownloadOnly = "下载";
$i_SmartCard_RemoveOnly = "移除";
$i_SmartCard_DownloadRemove = "下载后移除";
$i_SmartCard_FromToIncluded = "资料将包括所填写的两个日期";
$i_SmartCard_DownloadLogFormat = "下载的档案是班别及班号的格式 (可上载回伺服器)";
$i_SmartCard_SystemSettings = "系统设定";

$i_StudentAttendance_System = "学生智能卡考勤";
$i_StudentAttendance_SystemAdminUserSetting = "设定管理用户";
$i_StudentAttendance_AllStudentsWithRecords = "所有出入的学生";
$i_StudentAttendance_Menu_ResponsibleAdmin = "可负责点名人员设定";
$i_StudentAttendance_Menu_DailyOperation = "每日工作";
$i_StudentAttendance_Menu_DataManagement = "出席资料管理";
$i_StudentAttendance_Menu_OtherFeatures = "其他功能";
$i_StudentAttendance_Menu_DataExport = "资料汇出";
$i_StudentAttendance_Menu_DataImport = "资料汇入";
$i_StudentAttendance_Menu_Report = "报告";
$i_StudentAttendance_Menu_OtherSettings = "其他设定";

$i_StudentAttendance_AttendanceMode = "点名模式";
$i_StudentAttendance_AttendanceMode_AM_Only = "只有上午";
$i_StudentAttendance_AttendanceMode_PM_Only = "只有下午";
$i_StudentAttendance_AttendanceMode_WD_Lunch = "全日, 需要计算午饭时间";
$i_StudentAttendance_AttendanceMode_WD_NoLunch = "全日, 不需计算午饭时间";

$i_StudentAttendance_Menu_Slot_School = "学校时间设定";
$i_StudentAttendance_Menu_Slot_Class = "班级特别设定";
$i_StudentAttendance_Menu_Slot_Group = "小组特别设定";

$i_StudentAttendance_NormalDays = "一般日子";
$i_StudentAttendance_Weekday_Specific = "周日特别设定";
$i_StudentAttendance_Cycleday_Specific = "循环日特别设定";
$i_StudentAttendance_NoSpecialSettings = "没有特别设定";
$i_StudentAttendance_Warn_Please_Select_Weekday="请选择周日";
$i_StudentAttendance_Warn_Please_Select_CycleDay="请选择循环日";
$i_StudentAttendance_Warn_Please_Select_A_Day="请选择日子";
$i_StudentAttendance_ShowALlGroup_Options = "所有小组";
$i_StudentAttendance_HiddenGroup_Options = "按时间表点名";
$i_StudentAttendance_Diplay_Mode = "小组点名模式";


### Added On 3 Sep 2007 ###
$i_StudentAttendance_NonSchoolDaySetting_Warning = "一般日子不能设定为非上课日子";
###########################

$i_StudentAttendance_SpecialDay = "特别日期设定";

$i_StudentAttendance_SetTime_AMStart = "上午上课时间";
$i_StudentAttendance_SetTime_LunchStart = "午饭时间";
$i_StudentAttendance_SetTime_PMStart = "下午上课时间";
$i_StudentAttendance_SetTime_SchoolEnd = "放学时间";

###############################
$i_StudentAttendance_NonSchoolDay="非上课日子";
$i_StudentAttendance_TodayOnward="由今天起";
$i_StudentAttendance_Past = "昔日";
$i_StudentAttendance_ViewPastRecords="检视过往纪录";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_LunchStart="$i_StudentAttendance_SetTime_AMStart 不可大于 / 等于 $i_StudentAttendance_SetTime_LunchStart";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_PMStart="$i_StudentAttendance_SetTime_AMStart 不可大于 / 等于 $i_StudentAttendance_SetTime_PMStart";
$i_StudentAttendance_Warn_AM_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_AMStart 不可大于 / 等于 $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_PMStart="$i_StudentAttendance_SetTime_LunchStart 不可大于 / 等于 $i_StudentAttendance_SetTime_PMStart";
$i_StudentAttendance_Warn_LunchStart_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_LunchStart 不可大于 / 等于 $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_PMStart_Must_Smaller_Than_SchoolEnd="$i_StudentAttendance_SetTime_PMStart 不可大于 / 等于 $i_StudentAttendance_SetTime_SchoolEnd";
$i_StudentAttendance_Warn_Cannot_Modify_Past_Records="不能编辑旧纪录";
$i_StudentAttendance_Previous_Week="上星期";
$i_StudentAttendance_Next_Week="下星期";

###############################
$i_StudentAttendance_WeekDay = "周日";
$i_StudentAttendance_CycleDay = "循环日";
$i_StudentAttendance_TimeSlot_SpecialDay="特别日子";
$i_StudentAttendance_Warn_Please_Select_WeekDay="请选择周日";
$i_StudentAttendance_Warn_Please_Select_A_Day="请选择日期";
$i_StudentAttendance_Warn_Invalid_Time_Format="时间格式错误";
$i_StudentAttendance_Warn_Invalid_Date_Format="日期格式错误";

$i_EditGroupHints = "请到「内联网管理」，「小组管理」，「小组管理中心」新增小组。";
$i_StudentAttendance_ClassMode = "点名模式";
$i_StudentAttendance_GroupMode = "点名模式";
$i_StudentAttendance_ClassMode_UseSchoolTimetable = "使用学校时间表";
$i_StudentAttendance_ClassMode_UseClassTimetable = "使用班别指定时间表";
$i_StudentAttendance_GroupMode_UseSchoolTimetable = "无须按时间表点名";
$i_StudentAttendance_GroupMode_UseGroupTimetable = "须按时间表点名";
$i_StudentAttendance_ClassMode_NoNeedToTakeAttendance = "无须点名";
$i_StudentAttendance_GroupMode_NoNeedToTakeAttendance = "无须点名";
$i_StudentAttendance_ClassMode_Edit = "编辑时间";

$i_StudentAttendance_CardLog = "拍卡纪录";
$i_StudentAttendance_ViewTodayRecord = "查看今天纪录";
$i_StudentAttendance_ArchiveTodayRecord = "整存今天纪录";
$i_StudentAttendance_Outing = "外出纪录";
$i_StudentAttendance_Detention = "留堂纪录";
$i_StudentAttendance_TimeSlotSettings = "时间设定";
$i_StudentAttendance_Report = "报告";
$i_StudentAttendance_WordTemplates = "预设字句";
$i_StudentAttendance_DataExport = "资料汇出 (离线电脑之用)";
$i_StudentAttendance_Reminder = "老师召见提示";
$i_StudentAttendance_DataImport = "资料汇入";
$i_StudentAttendance_ParentLetters = "家长信";
$i_StudentAttendance_By = "自";
$i_StudentAttendance_Slot = "时段";
$i_StudentAttendance_Type = "类型";
$i_StudentAttendance_Slot_Start = "开始时间";
$i_StudentAttendance_Slot_End = "结束时间";
$i_StudentAttendance_Slot_Boundary = "返校时间/放学时间";
$i_StudentAttendance_Slot_InUse  = "使用";
$i_StudentAttendance_Slot_Special = "特别日子";
$i_StudentAttendance_Slot_Special_Today1 = "今天为";
$i_StudentAttendance_Slot_Special_Today2 = " ,所以时间有所不同";
$i_StudentAttendance_Type_GoToSchool = "返校";
$i_StudentAttendance_Type_LeaveSchool = "离校";
$i_StudentAttendance_Slot_AM = "上午";
$i_StudentAttendance_Slot_PM = "下午";
$i_StudentAttendance_Slot_AfterSchool = "放学";
$i_StudentAttendance_Slot_SettingsDescription = "&nbsp; * 请输入二十四小时制时间 (HH:mm) (e.g. 07:30, 16:30).";
$i_StudentAttendance_View_Date = "检视日期";
$i_StudentAttendance_ToSchoolTime = "返校时间";
$i_StudentAttendance_LeaveSchoolTime = "放学时间";
$i_StudentAttendance_Time_Arrival = "到达时间";
$i_StudentAttendance_Time_Departure = "离开时间";
$i_StudentAttendance_Status = "状态";
$i_StudentAttendance_MinLate = "迟到时间(分钟)";
$i_StudentAttendance_MinEarly = "早退时间(分钟)";
$i_StudentAttendance_Status_Present = "出席";
$i_StudentAttendance_Status_OnTime = "准时";
$i_StudentAttendance_Status_Late = "迟到";
$i_StudentAttendance_Status_Absent = "缺席";
$i_StudentAttendance_Status_PreAbsent = "无出席";
$i_StudentAttendance_Status_EarlyLeave = "早退";
$i_StudentAttendance_Status_Outing = "外出活动";
$i_StudentAttendance_Status_SL = "病假";
$i_StudentAttendance_Status_AR = "事假";
$i_StudentAttendance_Status_LE = "迟到及早退";
$i_StudentAttendance_Status_Truancy = "逃学";
$i_StudentAttendance_Status_Waived = "豁免";
$i_StudentAttendance_Archive_Date = "整存资料日期";
$i_StudentAttendance_con_RecordArchived = "资料已整存";
$i_StudentAttendance_Report_Daily = "检视每天纪录";
$i_StudentAttendance_Report_Student = "检视学生每月纪录";
$i_StudentAttendance_Report_Class = "全班列表";
$i_StudentAttendance_Report_SelectSlot = "检视时段";
$i_StudentAttendance_Report_PlsSelectSlot = "请选择最少一个时段";
$i_StudentAttendance_Report_NoRecord = "没有纪录";
$i_StudentAttendance_Report_Search="搜寻";
$i_StudentAttendance_Report_NoCardTab = "没有拍卡报告";
$i_StudentAttendance_InSchool = "回校";
$i_StudentAttendance_LeaveSchool = "离校";
$i_StudentAttendance_Today_lastRecord = "今天最后拍卡时间";
$i_StudentAttendance_PresetWord_CardSite = "拍卡地点";
$i_StudentAttendance_PresetWord_OutingFromWhere = "[外出/活动] 出发地点";
$i_StudentAttendance_PresetWord_OutingLocation = "[外出/活动] 地点";
$i_StudentAttendance_PresetWord_OutingObjective = "[外出/活动] 目的";
$i_StudentAttendance_PresetWord_DetentionLocation = "[留堂] 地点";
$i_StudentAttendance_PresetWord_DetentionReason = "[留堂] 原因";

$i_StudentAttendance_NotDisplayOntime = "不显示准时纪录";
$i_StudentAttendance_ImportFormat_CardID = "时间, $i_SmartCard_Site, 智能卡 ID";
$i_StudentAttendance_ImportFormat_ClassNumber = "时间, $i_SmartCard_Site, 班别, 班号";
$i_StudentAttendance_ImportFormat_From_OfflineReader = "由离线拍卡机收集的纪录";
$i_StudentAttendance_ImportTimeFormat = "请把时间一项的格式设定为 YYYY-MM-DD HH:mm:ss";
$i_StudentAttendance_Import_Instruction_OneDayOnly = "档案内的所有纪录需为同一天的纪录";
$i_StudentAttendance_Import_Warning_OneDayOnly = "并非所有纪录为同一天的纪录, 请分开处理.";
$i_StudentAttendance_ImportConfirm = "如确定上例的纪录正确, 请按汇入以完成汇入.";
$i_StudentAttendance_ImportCancel = "如资料有误, 请按取消清除这些纪录.";
$i_StudentAttendance_ArchiveWait = "此动作需时较长, 请耐心等候...";
$i_StudentAttendance_StudentInformation = "学生资料";
$i_StudentAttendance_ParentLetters_Late = "迟到家长信";
$i_StudentAttendance_Reminder_Date = "提示日期";
$i_StudentAttendance_Reminder_Teacher = "接见老师";
$i_StudentAttendance_Reminder_Reason = "原因";
$i_StudentAttendance_Reminder_Status_Past = "已过";
$i_StudentAttendance_Reminder_Status_Coming = "未到";
$i_StudentAttendance_Reminder_ImportFileDescription = "
第一栏 : 班别 <br>
第二栏 : 班号 <br>
第三栏 : 提示日期 (如留空即为<b>明天</b>) <br>
第四栏 : 召见老师之内联网帐号 <br>
第五栏 : 内容
";
$i_StudentAttendance_Reminder_StartDate = "提示开始日期";
$i_StudentAttendance_Reminder_FinishDate = "提示完结日期";
$i_StudentAttendance_Reminder_RepeatSelection[0] = "不重复";
$i_StudentAttendance_Reminder_RepeatSelection[1] = "每天";
$i_StudentAttendance_Reminder_RepeatSelection[2] = "周日";
$i_StudentAttendance_Reminder_RepeatSelection[3] = "循环日";
$i_StudentAttendance_Reminder_WeekdaySelection = "选择周日";
$i_StudentAttendance_Reminder_CycledaySelection = "选择循环日";
$i_StudentAttendance_Reminder_RepeatFrequency = "重复频率";
$i_StudentAttendance_Reminder_StartDateEmptyWarning = "请输入提示开始日期";
$i_StudentAttendance_Reminder_FinishDateEmptyWarning = "请输入提示完结日期";
$i_StudentAttendance_Reminder_WrongDateWarning = "请输入正确日期";
$i_StudentAttendance_Reminder_WeekdaySelectionWarning = "请选择周日";
$i_StudentAttendance_Reminder_CycleDaySelectionWarning = "请选择循环日";
$i_StudentAttendance_Reminder_TeacherSelectionWarning = "请选择接见老师";
$i_StudentAttendance_Reminder_InputReasonWarning = "请输入原因";
$i_StudentAttendance_Outing_ImportFileDescription = "
第一栏 : 班别 <br>
第二栏 : 班号 <br>
第三栏 : 负责老师姓名 <br>
第四栏 : 日期 (如留空即为<b>今天</b>) <br>
第五栏 : 外出时间 (HH:mm:ss) <br>
第六栏 : 结束时间 (HH:mm:ss) <br>
第七栏 : 原因
";
$i_StudentAttendance_Detention_ImportFileDescription = "
第一栏* : 班别 <br>
第二栏* : 班号 <br>
第三栏 : $i_SmartCard_DetentionDate (如留空即为<b>今天</b>) <br>
第四栏 : $i_SmartCard_DetentionArrivalTime (HH:mm:ss) <br>
第五栏 : $i_SmartCard_DetentionDepartureTime (HH:mm:ss) <br>
第六栏 : $i_SmartCard_DetentionLocation<br>
第七栏 : $i_SmartCard_DetentionReason<br>
第八栏 : $i_SmartCard_Remark<br>
<br>
* - 必须填上
";

$i_StudentAttendance_SimCard = "摸拟拍卡";
$i_StudentAttendance_Action_SpecialArchive = "特别点名时间";
$i_StudentAttendance_NeedTakeAttendance = "需要点名";
$i_StudentAttendance_NoCardRecord = "没有拍卡纪录";

$i_StudentAttendance_RemoveByDateRange = "以日期移除";
$i_StudentAttendance_ImportCardID = "汇入智能卡 ID";
$i_StudentAttendnace_ImportRawRecord = "汇入拍卡纪录";
$i_StudentAttendance_OR_ExportStudentInfo = "汇出离线拍卡机使用的学生资料";

$i_StudentAttendance_LunchSettings = "中午出入设定";
$i_StudentAttendance_NonTargetSettings = "不使用智能卡点名的名单";
$i_StudentAttendance_BackSchoolTime = "上课时间";
$i_StudentAttendance_LunchStartTime = "午饭时间开始";
$i_StudentAttendance_LunchEndTime = "午饭时间结束";
$i_StudentAttendance_SchoolEndTime = "放学时间";

$i_StudentAttendance_Frontend_menu_TakeAttendance = "点名";
$i_StudentAttendance_Frontend_menu_CheckStatus = "检查纪录";
$i_StudentAttendance_Frontend_menu_Report = "报告";
$i_StudentAttendance_Frontend_menu_CheckMyRecord = "我的纪录";

$i_StudentAttendance_SelectAnotherClass = "选择另一班别";
$i_StudentAttendance_Field_Date = "日期";
$i_StudentAttendance_Field_Date_From = "由";
$i_StudentAttendance_Field_Date_To = "到";
$i_StudentAttendance_Field_ConfirmedBy = "已作确认的用户";
$i_StudentAttendance_Field_LastConfirmedTime = "确认时间";
$i_StudentAttendance_LeftStatus_Type_InSchool = "返校情况";
$i_StudentAttendance_LeftStatus_Type_Lunch = "午饭情况";
$i_StudentAttendance_LeftStatus_Type_AfterSchool = "离校情况";
$i_StudentAttendance_NoLateStudents = "没有学生迟到";
$i_StudentAttendance_NoAbsentStudents = "没有学生缺席";
$i_StudentAttendance_NoEarlyStudents = "没有学生早退";
$i_StudentAttendance_NoNeedTakeAttendance = "不使用智能卡点名";
$i_StudentAttendance_InSchool_BackAlready = "曾回校";
$i_StudentAttendance_InSchool_HaveBeenToSchool= "曾回校";
$i_StudentAttendance_InSchool_NotBackYet = "未回校";
$i_StudentAttendance_Lunch_NotOutYet = "未外出";
$i_StudentAttendance_Lunch_GoneOut = "已外出";
$i_StudentAttendance_Lunch_Back = "已回来";
$i_StudentAttendance_AfterSchool_Left = "已离校";
$i_StudentAttendance_AfterSchool_Stay = "未离校";
$i_StudentAttendance_ViewStudentList = "检视学生名单";
$i_StudentAttendance_Menu_DataMgmt_ResetTime = "更改旧日上课时间以重设该日资料";
$i_StudentAttendance_Menu_DataMgmt_UndoPastProfileRecord = "还原已确认的缺席/迟到学生档案纪录";
$i_StudentAttendance_Menu_DataMgmt_DataClear = "清除过时资料";
$i_StudentAttendance_Menu_DataMgmt_BadLogs = "错误行为纪录";
$i_StudentAttendance_Menu_OtherFeatures_Outing = "外出纪录";
$i_StudentAttendance_Menu_OtherFeatures_Detention = "留堂纪录";
$i_StudentAttendance_Menu_OtherFeatures_Reminder = "老师召见提示纪录";
$i_StudentAttendance_Offline_Import_DataType = "资料种类";
$i_StudentAttendance_Offline_Import_DataType_InSchool = "回校上课";
$i_StudentAttendance_Offline_Import_DataType_LunchOut = "外出午膳";
$i_StudentAttendance_Offline_Import_DataType_LunchIn = "午膳后回校";
$i_StudentAttendance_Offline_Import_DataType_AfterSchool = "放学离校";
$i_StudentAttendance_BadLogs_Type_NotInLunchList = "无权的学生尝试外出午膳";
$i_StudentAttendance_BadLogs_Type_GoLunchAgain = "尝试再次外出午膳";
$i_StudentAttendance_BadLogs_Type_FakedCardAM = "有卡纪录但缺席 (上午)";
$i_StudentAttendance_BadLogs_Type_FakedCardPM = "有卡纪录但缺席 (下午)";
$i_StudentAttendance_BadLogs_Type_NoCardRecord = "忘记带卡 / 拍卡";
$i_StudentAttendance_BadLogs_Type = $i_general_Type;
$i_StudentAttendance_UndoProfile_Warning = "以上的考勤纪录(不论是由智能卡系统还是自行输入的纪录), 将会被删除. 请按继续还原.";
$i_StudentAttendance_ResetTime_Warning = "学生档案迟到纪录亦会同时被更新";
$i_StudentAttendance_DataRemoval_Warning = "所有拍卡纪录 (包括时间, 每月纪录) 会被移除, 而<u><b>学生档案中的资料则会保留</b></u>.";
$i_StudentAttendance_Field_CardStation = "拍卡地点";
$i_StudentAttendance_Report_ClassMonth = "班别每月出席表";
$i_StudentAttendance_Report_ClassDaily = "班别每日出席表";
$i_StudentAttendance_Report_ClassBadRecords = "各班的错误行为的统计";
$i_StudentAttendance_Report_StudentBadRecords = "学生错误行为的统计";
$i_StudentAttendance_Field_InSchoolTime = "回校时间";
$i_StudentAttendance_Field_LunchOutTime = "外出午饭时间";
$i_StudentAttendance_Field_LunchBackTime = "返回时间";
$i_StudentAttendance_Field_LeaveTime = "离校时间";
$i_StudentAttendance_ReportHeader_NumStudents = "学生人数";
$i_StudentAttendance_ReportHeader_NumRecords = "纪录数目";
$i_StudentAttendance_DisplayTop = "显示最高的";
$i_StudentAttendance_Top = "最高";
$i_StudentAttendance_Top_student_suffix = "位的学生";
$i_StudentAttendance_Message_ThisClassNoNeedToTake = "这个班级无须点名";
$i_StudentAttendance_Message_NoStudentInClass = "本班没有学生";
$i_StudentAttendance_New_InSchoolTime = "重设的上课时间";
$i_StudentAttendance_Field_NumLateOriginal = "原来的迟到人数";
$i_StudentAttendance_Field_NumLateNew = "更新后的迟到人数";
$i_StudentAttendance_CalendarLegend_NotConfirmed = "未确认";
$i_StudentAttendance_CalendarLegend_Confirmed = "已确认";
$i_StudentAttendance_Action_SetAbsentToPresent = "把".$i_StudentAttendance_Status_PreAbsent."转为".$i_StudentAttendance_Status_Present."";
$i_StudentAttendance_Action_SetAbsentToOntime = "把".$i_StudentAttendance_Status_PreAbsent."转为".$i_StudentAttendance_Status_OnTime."";

$i_StudentAttendance_Symbol_Present = "<span style='font-family:新细明体'>／</span>";
$i_StudentAttendance_Symbol_Absent = "<span style='font-family:新细明体'>○</span>";
$i_StudentAttendance_Symbol_Late = "<span lang=EN-US style='font-family:Symbol;mso-ascii-font-family:新细明体;mso-hansi-font-family:新细明体;mso-char-type:symbol;mso-symbol-font-family:Symbol'><span style='mso-char-type:symbol;mso-symbol-font-family:Symbol'>&AElig;</span></span>";
$i_StudentAttendance_Symbol_Outing = "z";
$i_StudentAttendance_Symbol_EarlyLeave = "<span class=GramE><span lang=EN-US style='font-family:新细明体'>#</span></span>";
$i_StudentAttendance_Symbol_SL = "<span style='font-family:新细明体'>⊕</span>";
$i_StudentAttendance_Symbol_AR = "<span lang=EN-US style='font-family:Webdings;mso-ascii-font-family:新细明体;mso-hansi-font-family:新细明体;mso-char-type:symbol;mso-symbol-font-family:Webdings'><span style='mso-char-type:symbol;mso-symbol-font-family:Webdings'>y</span></span>";
$i_StudentAttendance_Symbol_LE = "<span lang=EN-US style='font-family:\"Wingdings 2\";mso-ascii-font-family:新细明体;mso-hansi-font-family:新细明体;mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'><span style='mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'>U</span></span>";
$i_StudentAttendance_Symbol_Truancy = "<span lang=EN-US style='font-family:新细明体'>●</span>";
$i_StudentAttendance_Symbol_Waived = "<span class=GramE><span lang=EN-US style='font-family:新细明体'>*</span></span>";

$i_StudentAttendance_Stat_Present = "出席数";
$i_StudentAttendance_Stat_Absent = "缺席数";
$i_StudentAttendance_Stat_Late = "迟到数";
$i_StudentAttendance_Stat_EarlyLeave = "早退数";
$i_StudentAttendance_Stat_Truancy = "逃学数";
$i_StudentAttendance_Daily_Stat = "每日统计";
$i_StudentAttendance_Monthly_Level_Stat = "本月统计";
$i_StudentAttendance_Attendance_Number = "在席人数";
$i_StudentAttendance_Average_Present = "平均出席人数";
$i_StudentAttendance_Average_Absent = "平均缺席人数";
$i_StudentAttendance_Symbol_Reference = "考勤符号";
$i_StudentAttendance_FullReport_Days1 = "本月上课共:";
$i_StudentAttendance_FullReport_Days2  = "日";
$i_StudentAttendance_exactly_matched="须完全符合输入字串";
$i_StudentAttendance_Export_Notice="备注：如使用离线模式(Offline Mode)而非离线拍卡器，请按\"学生资料\"汇出考勤纪录。";
### Added By Ronald On 12 Apr 2007
$i_StudentAttendance_LeftStatus_Type_LeavingTime = "检视学生离校时间";

### Added By Ronald On 13 Apr 2007
$i_StudentAttendance_Leave_Status_Early_Leave_AM = "早退 (上午)";
$i_StudentAttendance_Leave_Status_Early_Leave_PM = "早退 (下午)";
$i_StudentAttendance_Leave_Status_Normal = "正常";
### Added On 19 Apr 2007
$i_StudentAttendance_Early_Leave_Insert_Record = "新增早退记录";

### Added On 20 Apr 2007
$i_StudentAttendance_Class_Select_Instruction = "请选择班别";
$i_StudentAttendance_Student_Select_Instruction = "请选择学生";
$i_StudentAttendance_Input_Correct_Time = "请输入正确时间";
$i_StudentAttendance_Input_Time = "请输入时间";
$i_StudentAttendance_Early_Leave_Warning = "<font color=red>请注意下列学生的离校记录将会被更改:</font>";
$i_StudentAttendance_Menu_Slot_Session_Setting = "预定时间设定";
$i_StudentAttendance_Menu_Slot_Session_School = "学校时间设定";
$i_StudentAttendance_Menu_Slot_Session_Class = "班级特别设定";
$i_StudentAttendance_Menu_Slot_Session_Group = "小组特别设定";

$i_StudentAttendance_TimeSessionSettings = "时间设定";
$i_StudentAttendance_Menu_Slot_Session_Name = "名称";
$i_StudentAttendance_Menu_Time_Mode = "时间表模式";
$i_StudentAttendance_Menu_Time_Input_Mode = "时间输入";
$i_StudentAttendance_Menu_Time_Session_Mode = "预定时间";
$i_StudentAttendance_Menu_Time_Session_MorningTime = "上午上课时间";
$i_StudentAttendance_Menu_Time_Session_LunchStartTime = "午饭时间";
$i_StudentAttendance_Menu_Time_Session_LunchEndTime = "下午上课时间";
$i_StudentAttendance_Menu_Time_Session_LeaveSchoolTime = "放学时间";
$i_StudentAttendance_Menu_Time_Session_NonSchoolDay = "非上课日子";
$i_StudentAttendance_Menu_Time_Session_Using = "使用中";
$i_StudentAttendance_Menu_Time_Session_Activated = "使用中";
$i_StudentAttendance_Menu_Time_Session_School_Use = "使用";
$i_StudentAttendance_ViewToSchoolTime = "检视返校时间";
$i_StudentAttendance_CalendarLegend_RecordWithData = "有记录";
$i_StudentAttendance_TimeSessionSettings_SessionName_Warning = "请输入名称";
$i_StudentAttendance_TimeSessionSettings_WeekdaySelection_Warning = "请选择周日";
$i_StudentAttendance_TimeSessionSettings_SessionSelection_Warning = "请选择时间";
$i_StudentAttendance_TimeSessionSettings_CycledaySelection_Warning = "请选择循环日";

$i_StudentAttendance_TimeSessionSettings_NowUsing1 = "正在使用";
$i_StudentAttendance_TimeSessionSettings_NowUsing2 = "的班别";
$i_StudentAttendance_TimeSessionSettings_AlreadyHaveSpecialDaySession = "已有特别日期设定的日子";
$i_StudentAttendance_TimeSession_GetSpecialdayInfo_Warning = "注意: 以上日子将不会被更改";
$i_StudentAttendance_TimeSession_UsingInactiveSession_Warning = "* - 该时间设定已经停用";
$i_StudentAttendance_TimeSession_UsingAsNormalSetting = "正在使用为<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_NormalDays</span>]</span>的班别";
$i_StudentAttendance_TimeSession_UsingAsWeeklySetting = "正在使用为<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_Weekday_Specific</span>]</span>的班别";
$i_StudentAttendance_TimeSession_UsingAsCycleSetting = "正在使用为<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_Cycleday_Specific</span>]</span>的班别";
$i_StudentAttendance_TimeSession_UsingAsSpecialSetting = "正在使用为<span class='extraInfo'>[<span class=subTitle>$i_StudentAttendance_SpecialDay</span>]</span>的班别";
$i_StudentAttendance_TimeSession_SessionNowUsing = "* - 正在使用中";
$i_StudentAttendance_TimeSession_OtherClassUseSameSession = "其他班别同时使用";
$i_StudentAttendance_TimeSession_TypeSelection = "选择类别";
$i_StudentAttendance_TimeSession_ClassSelection = "选择班别";
$i_StudentAttendance_TimeSession_TypeSelectionWarning = "请选择类别";
$i_StudentAttendance_TimeSession_ClassSelectionWarning = "请选择班别";
### Added By Ronald On 2 Aug 2007 ###
$i_StudentAttendance_StaffAndStudentInformation = "教职员及学生资料";
$i_StudentAttendance_Setting_DisallowStudentProfileInput="不允许在学生档案中新增/删除考勤纪录";

### Added On 24 Aug 2007 ###
$i_StudentAttendance_Reminder_Content = "内容";
############################

### Added on 22 Oct 2008 ###
$i_StudentAttendance_ShortCut = "快捷方式";
############################

### Added on 23 Jan 2009 ###
$i_StudentAttendance_default_attend_status="默认出席状态";

$i_StudentAttendance_Warning_Data_Outdated="资料已过时，请重新呈送一次";

// added on 20090608 by kenneth chung
$Lang['StudentAttendance']['TakeAttendanceFor'] = "点名";
$Lang['StudentAttendance']['School'] = "回校";
$Lang['StudentAttendance']['Lesson'] = "上课";
$Lang['StudentAttendance']['Session'] = "课节";

// added on 20090710 by kenneth chung
$Lang['StudentAttendance']['RecordTime'] = "记录时间";

$i_SmartCard_TerminalSettings = "智能卡终端机设定";
$i_SmartCard_Terminal_IPList = "终端机的 IP 位址";
$i_SmartCard_Terminal_IPInput = "请把终端机的 IP 地址每行一个输入";
$i_SmartCard_Terminal_ExpiryTime = "终端机登记后可使用时间(分钟)";
$i_SmartCard_Terminal_YourAddress = "你现时的 IP 地址";
$i_SmartCard_Terminal_IgnorePeriod = "不接受重复拍卡的时间";
$i_SmartCard_Terminal_IgnorePeriod_unit = "分钟";

$i_SmartCard_Confirm_Update_Attend = "确定更新记录";

$i_SmartCard_Responsible_Admin_Class = "班代表";

$i_SmartCard_Responsible_Admin_Settings_List = "检视负责点名学生";
$i_SmartCard_Responsible_Admin_Settings_Manage = "管理负责点名学生";

$i_SmartCard_Settings_Lunch_List_Title = "检视可外出午膳学生名单";
$i_SmartCard_Settings_Lunch_Misc_Title = "其他设定";

$i_SmartCard_Settings_Lunch_New_Type = "模式";
$i_SmartCard_Settings_Lunch_New_Type_Class = "新增整个班别";
$i_SmartCard_Settings_Lunch_New_Type_Student = "新增个别学生";
$i_SmartCard_Settings_NoStudentsAvailableForSelection = "没有可选择学生. 所有学生已经编入名单";
$i_SmartCard_Settings_Lunch_Instruction_Class = "请选择班别. 该班别的所有学生将会编入可外出午膳名单.";
$i_SmartCard_Settings_Lunch_Instruction_Student = "请选择要加入可外出午膳名单的学生.";
$i_SmartCard_Settings_Admin_Number_Students = "可负责点名的学生人数";


$i_SmartCard_Settings_Lunch_Misc_No_Record = "中午外出不需拍卡";
$i_SmartCard_Settings_Lunch_Misc_Out_Once = "中午只准外出一次";
$i_SmartCard_Settings_Lunch_Misc_All_Allow = "全部准许外出";

$i_SmartCard_DailyOperation_viewClassStatus_PrintOption = "$i_PrinterFriendlyPage 选项";
$i_SmartCard_DailyOperation_ViewClassStatus = "检视各班别情况";
$i_SmartCard_DailyOperation_ViewGroupStatus = "检视各小组情况";
$i_SmartCard_DailyOperation_ViewLateStatus = "检视迟到名单";
$i_SmartCard_DailyOperation_ViewAbsenceStatus = "检视缺席名单";
$i_SmartCard_DailyOperation_ViewEarlyLeaveStatus = "检视早退名单";
$i_SmartCard_DailyOperation_ViewLeftStudents = "检视在校情况";
$i_SmartCard_DailyOperation_ImportOfflineRecords = "汇入离线纪录";
$i_SmartCard_DailyOperation_ViewEntryLog = "检视出入纪录";
$i_SmartCard_DailyOperation_BlindConfirmation="核实所有班别";
$i_SmartCard_DailyOperation_Number_Of_Absent_Student="未确认的缺席学生人数";
$i_SmartCard_DailyOperation_Number_Of_Outing_Student="未确认的外出学生人数";
$i_SmartCard_DailyOperation_OrderBy_InSchoolTime="以拍卡时间排序";
$i_SmartCard_DailyOperation_OrderBy_ClassNameNumber="以班别和班号排序";
$i_SmartCard_DailyOperation_Preset_Absence="学生请假纪录";
$i_SmartCard_DailyOperation_Preset_Absence_BrowseByStudent="检视指定学生";
$i_SmartCard_DailyOperation_Preset_Absence_BrowseByDate="检视指定日期";
$i_SmartCard_DailyOperation_Preset_Absence_Has_Leave_Record="有请假纪录";

$i_SmartCard_DailyOperation_Check_Time = "检视学生返学时间";
$i_SmartCard_DailyOperation_Check_Time_School_Time_Table="学校 / 班级 时间设定";
$i_SmartCard_DailyOperation_Check_Time_Group_Time_Table="小组时间设定";
$i_SmartCard_DailyOperation_Check_Time_Final_Time_Table="总结返学时间";

$i_SmartCard_DailyOperation_BlindConfirmation_Notes="<b><u>注意:</u></b>
<br>
<ul><li>呈送前请先核实当日出席资料准确无误</li></ul>
";
$i_SmartCard_DailyOperation_BlindConfirmation_Notes1="注意";
$i_SmartCard_DailyOperation_BlindConfirmation_Notes2="呈送前请先核实当日出席资料准确无误";

$i_SmartCard_DailyOperation_ViewClassStatus_PrintAll = "显示所有学生";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbs = "显示缺席学生";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintLate = "显示迟到学生";
$i_SmartCard_DailyOperation_ViewClassStatus_PrintAbsAndLate = "显示缺席或迟到学生";
### Added By Ronald On 16 Apr 2007
$i_SmartCard_DailyOperation_OrderBy_LeaveSchoolTime = "以离开时间排序";

$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record = "记录未被确认";
$i_SmartCard_Frontend_Take_Attendance_In_School_Time = "拍卡时间";
$i_SmartCard_Frontend_Take_Attendance_In_School_Station = "拍卡地点";
$i_SmartCard_Frontend_Take_Attendance_Status = "出席?";
$i_SmartCard_Frontend_Take_Attendance_PreStatus = "状况";
$i_SmartCard_Frontend_Take_Attendance_Waived = "豁免";
$i_SmartCard_Frontend_Take_Attendance_No_Confirm_Record_Update = "记录未被确认 ( 储存途中,记录曾被更新 )．请检查记录及再次呈送";


$i_StudentPromotion = "学生升班系统";
$i_StudentPromotion_Menu_Reset = "重设所有资料 (升班前的准备程序)";
$i_StudentPromotion_Menu_ViewEditList = "编配下学年班别 (升班程序 1)";
$i_StudentPromotion_Menu_EditClassNumber = "编排下学年班号 (升班程序 2)";
$i_StudentPromotion_Menu_NewClassEffective = "更新所有学生资料 (完成程序 1)";
$i_StudentPromotion_Menu_ArchiveLeftStudents = "整存所有离校生的资料 (完成程序 2)";
$i_StudentPromotion_StudentAssigned = "已编配至新班别的学生";
$i_StudentPromotion_Unset = "未被编配的学生 (将被视作离校生)";
$i_StudentPromotion_OldClass = "现时班别";
$i_StudentPromotion_OldClassNumber = "现时班号";
$i_StudentPromotion_NewClass = "新班别";
$i_StudentPromotion_NewClassNumber = "新班号";
$i_StudentPromotion_Type_ClassName = "$i_StudentPromotion_OldClass, $i_StudentPromotion_OldClassNumber, $i_StudentPromotion_NewClass, $i_StudentPromotion_NewClassNumber";
$i_StudentPromotion_Type_Login = "$i_UserLogin, $i_StudentPromotion_NewClass, $i_StudentPromotion_NewClassNumber";
$i_StudentPromotion_Prompt_PreparationDone = "<font color=green>资料已重设</font>";
$i_StudentPromotion_Prompt_NewClassEffectiveDone = "<font color=green>新班别资料已经更新</font>";
$i_StudentPromotion_Prompt_DataArchived = "<font color=green>资料已整存</font>";
$i_StudentPromotion_Summary = "总结";
$i_StudentPromotion_ConfirmFinalize = "请按呈送以完成整个程序";
$i_StudentPromotion_ClickContinueToProceed = "请按继续以启动此程序. 程序进行需时, 请耐心等候.";
$i_StudentPromotion_Sorting_Name = "英文名称";
$i_StudentPromotion_Sorting_BoysName = "男生, 英文名称";
$i_StudentPromotion_Sorting_GirlsName = "女生, 英文名称";
$i_StudentPromotion_GenerateClassNumber = "自动排列班号";
$i_StudentPromotion_ClassSize = "本班总人数";
$i_StudentPromotion_Unset_List = "现时未分配新班别名单";
$i_StudentPromotion_RestoreLogin = "新$i_UserLogin";
$i_StudentPromotion_LoginDuplicated = "因$i_UserLogin"."已被其他使用者使用.";
$i_StudentPromotion_Description_ClassAssignment = "请选择现时班别以编配该班学生至下学年的新班别.<br>或以档案汇入方式编配新班别及新班号.";
$i_StudentPromotion_Notes_NewClassEffective = "<b><u>注意:</u></b><br>
<ul><li>此程序<font color=red><b><u>不能还原</u></b></font>, 请预先作资料备份</li>
<li>此程序启动后:<br>
   <ul>
     <li>学生将更新至新班别及新班号.</li>
     <li><font color=red><u><b>所有未被编配新班别班号的学生，将被编为「已离校」</b></u></font></li>
     <li>学生将自动加入升班后的新班别小组; 旧班别小组将被存档.</li>
     <li>已离校学生将不能登入系统.</li>
     <li>如有需要, 仍可于用户管理中心将已离校学生重新编配至新班别 (特别升班)</li>
   </ul></li>
<li><u>刚完结之年度</u>为系统设定 &gt; 基本设定中的本年度. 请先进行此程序, 才更新本年度至新学年.</li>
</ul>
";
$i_StudentPromotion_NumLeftStudents = "状态为已离校之学生人数";
$i_StudentPromotion_ArchivalYear = "这些学生的离校年份为";
$i_StudentPromotion_Notes_ArchiveLeftStudents = "<b><u>注意:</u></b><br>
<ul><li>此程序<font color=red><b><u>不能还原</font></u></b>, 请预先作资料备份.</li>
<li>此程序启动后:<br>
    <ul>
      <li>离校生的学生纪录可于 <u>学生档案 &gt; 检视已离校学生纪录</u> 中查看.</li>
      <li>状态为已离校的学生不能再重新编配至新班别. 这些学生户口会从用户管理中心删除.</li></ul>
";
$i_StudentPromotion_Notes_SpecialPromote = "<b><u>注意:</u></b><br>
<ul><li>此功能只适用于特别情况.</li>
<li>此程序启动后, 现时为已离校的学生, 不能再变回已批准.</li>
<li>请确定此学生已入学, 因为会影响此学生的班级纪录.</li>
</ul>";
$i_StudentPromotion_Notes_MarkLeft = "<b><u>注意:</u></b><br>
<ul><li>此功能只适用于特别情况 (学期中途离校).</li>
<li>此程序启动后, 现时为已离校的学生, 不能再变回已批准.</li>
<li>请确定此学生已退学, 因为会影响此学生的班级纪录.</li>
</ul>";

$i_StudentPromotion_NoAutoAlumniAdd = "<font color=green>此程序启动后, 将<b>不会</b>为已离校学生自动开启本系统的校友户口. 校友户口需要自行开启.</font>";
$i_StudentPromotion_AutoAlumni = "<font color=green>此程序启动后, 将为已离校学生自动开启校友户口 (使用原来的登入账号及密码)</font>";
$i_StudentPromotion_AutoAlumni_NoGroupSet = "<font color=red>系统不能自动开启校友户口 (因为校友会的组别并没有设置, 请联络博文教育技术支援人员.)</font>";
$i_StudentPromotion_AutoAlumni_NoGroupExist = "<font color=red>系统不能自动开启校友户口 (因为校友会的组别不存在/已被删除, 请联络博文教育技术支援人员.)</font>";
$i_StudentPromotion_SpecialPromote = "特别升级";
$i_StudentPromotion_SetToLeft = "编为已离校";
$i_StudentPromotion_PromoteTo = "升班至";
$i_StudentPromotion_NewClassNumber = "新班号";

$i_StudentPromotion_Alert_Reset = "你确定要重设所有学生分班资料?";
$i_StudentPromotion_Alert_PromoteClass = "请选择新班别";
$i_StudentPromotion_Alert_NoClassNumber = "你没有输入班号. 你确定要继续?";
$i_StudentPromotion_Alert_RemoveRecord = "你确定从新班别中删除这学生?";
$i_StudentPromotion_AcademicYear = "刚完结之年度";
$i_StudentPromotion_PlsChangeAfterPromotion = "请完成此程序后, 再更新$i_SettingsCurrentAcademicYear.";
$i_StudentPromotion_ImportFailed = "以下的纪录不能被汇入";

// for iPortfolio Groups Promote
$i_StudentPromotion_Menu_iPortfolioGroup = "进行 iPortfolio 学生升班程序";
$i_StudentPromotion_iPortfolioGroup_Summary = "iPortfolio 学生升班报告";
$i_StudentPromotion_Alert_iPortfolioGroup = "你是否确定要进行 iPortfolio 学生升班程序？";
$i_StudentPromotion_iPortfolioGroup_UserNum = "升班学生数目";
$i_StudentPromotion_Current_AcademicYear = "新学年";
$i_StudentPromotion_Confirm_iPortfolioGroup = "iPortfolio 学生升班程序已完成。";
$i_StudentPromotion_iPortfolioRemind = "<b><u>注意:</u></b><br><ul><li>在 iPortfolio 中, 学生会按年度及所属班别被分组。本程序会将学生加入到升班后所属的年度及班别组别，而旧有的组别将一同保留。例如某同学在新学年前的所属组别是 \"2005-2006 3A\"，在进行升班程序后，她将会被加入到 \"2006-2007 4A\"（但你仍然可从 \"2005-2006 3A\" 组别中找到该名学生）。</ul></li>";

//Class History Management 2009-01-15
$i_StudentPromotion_mgt['ACADEMICYEAR'] = "年度";
$i_StudentPromotion_mgt['NUMSTUDENT']	= "学生人数";
$i_StudentPromotion_mgt['CLASS']		= "班别";
$i_StudentPromotion_mgt['CLASSNUM']		= "班号";
$i_StudentPromotion_mgt['STUDENT']		= "学生";
$i_StudentPromotion_mgt['CLASSHISTORY']	= "班级纪录";
$i_StudentPromotion_mgt['CONFIRMDELETE'][0]= "你是否确定要删除这个学生的记录？";
$i_StudentPromotion_mgt['CONFIRMDELETE'][1]= "你是否确定要删除这班别所有学生的记录？";
$i_StudentPromotion_mgt['CONFIRMDELETE'][2]= "你是否确定要删除这年所有学生的记录？";
$i_StudentPromotion_mgt['CONFIRMDELETE'][3]= "你是否确定要删除全部学生的记录？";
$i_StudentPromotion_mgt['BACKTOTOP']	= "返回顶";
$i_StudentPromotion_mgt['IMPORTDESC'][0]	= "User Login, 英文名称, 年度, 班别, 班号";
$i_StudentPromotion_mgt['IMPORTDESC'][1]	= "WebSAMSRegNo, 英文名称, 年度, 班别, 班号";
$i_StudentPromotion_mgt['YEARDUPLICATE']	= "年度重复. 请输入其他年度.";
$i_StudentPromotion_mgt['EDITERROR']		= "请输入有关资料";
$i_StudentPromotion_mgt['TOTAL'] = "总人数";
$i_StudentPromotion_mgt['PREVIOUS']	= "前一页";
$i_StudentPromotion_mgt['NEXT']	= "后一页";
$i_StudentPromotion_mgt['PAGE']	= "页";
$i_StudentPromotion_mgt['DISPLAY']	= "每页显示";
$i_StudentPromotion_mgt['PERPAGE']	= "项";
$i_StudentPromotion_mgt['USER']	= "用户";
$i_StudentPromotion_mgt['NAV']['YEAR']		= "班别纪录管理";
$i_StudentPromotion_mgt['NAV']['CLASS']		= "班别名纪录管理";
$i_StudentPromotion_mgt['NAV']['STUDENT']	= "学生纪录管理";
$i_StudentPromotion_mgt['NAV']['DUPLICATE']	= "班别重复纪录";
$i_StudentPromotion_mgt['NAV']['EDIT']		= "纪录编辑";
$i_StudentPromotion_mgt['NAV']['PROBLEM']	= "管理问题纪录";
$i_StudentPromotion_mgt['BTNDUPLICATE']		= "管理重复纪录";
$i_StudentPromotion_mgt['BTNPROBLEMDATA']	= "管理问题纪录";
$i_StudentPromotion_mgt['BTNSEARCH']		= "搜寻";
$i_StudentPromotion_mgt['NORECORD'] 		= "没有记录";
$i_StudentPromotion_mgt['UNCHECKTOREMOVE']	= "取消勾选以移除项目";
$i_StudentPromotion_mgt['DUPSEARCH']		= "搜寻";
$i_StudentPromotion_mgt['EDIT_NOTE']		= "* 重复或有问题之纪录";
$i_StudentPromotion_mgt['RECORD']			= "纪录";
$i_StudentPromotion_mgt['OCCURRENCE']		= "次数";
$i_StudentPromotion_mgt['DUPLICATE']		= "重复次数";


$i_WebSAMS_Notice_Export="如要汇出 WebSAMS 格式的考勤资料，请完成以下各项的设定：<br><Br>";
$i_WebSAMS_Notice_Export1="一 、 设定每个级别相对应的 WebSAMS 级别名称<br><br>";
$i_WebSAMS_Notice_Export2="二 、 设定学校的基本资料 (如：WebSAMS 学校编号，现时 WebSAMS 学年等)<br><br>";
$i_WebSAMS_Notice_Export3="三 、 在原因代码管理中输入各类 WebSAMS 原因代码<br><Br>";
$i_WebSAMS_Notice_AttendanceCode = "汇出 WebSAMS 格式的考勤资料用";
$i_WebSAMSRegNo_Format_Notice="(\"<b>#</b>\" 号是WebSAMS 注册号码格式的一部份)";
$i_WebSAMS_AttendanceCode_No_RegNo_Students="未有WebSAMS 注册号码的学生人数";
$i_WebSAMS_AttendanceCode_Export="汇出考勤纪录";
$i_WebSAMS_Registration_No ="WebSAMS 注册号码";
$i_WebSAMS_ClassLevelName ="WebSAMS 级别名称";
$i_WebSAMS_ClassLevel ="WebSAMS 级别";
$i_WebSAMS_Integration = "WebSAMS 整合";
$i_WebSAMS_Menu_Attendance = "考勤资料更新";
$i_WebSAMS_Menu_AttendanceCode = "汇出 WebSAMS 格式的考勤资料";
$i_WebSAMS_Attendance_StartDate = "资料开始日期";
$i_WebSAMS_Attendance_EndDate = "资料结束日期";
$i_WebSAMS_con_RecordSynchronized = "资料更新成&#21151;";
$i_WebSAMS_AttendanceCode_Basic = "基本资料";
$i_WebSAMS_AttendanceCode_ReasonCode = "原因代码管理";
$i_WebSAMS_AttendanceCode_SchoolID = "WebSAMS 学校编号";
$i_WebSAMS_AttendanceCode_SchoolYear = "现时 WebSAMS 学年 (YYYY)";
$i_WebSAMS_AttendanceCode_SchoolLevel = "WebSAMS 学校级别";
$i_WebSAMS_AttendanceCode_SchoolSession = "WebSAMS 学校时段";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Kindergarten = "幼稚园";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Primary = "小学";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Secondary = "中学";
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_Others = "其他";

# Added On 13 Aug 2007
$i_WebSAMS_AttendanceCode_Option_SchoolLevel_DependOnClassLevel = "以学生班级决定";
####################

$i_WebSAMS_AttendanceCode_Option_SchoolSession_AM = "上午";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_PM = "下午";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_WD = "全日";
$i_WebSAMS_AttendanceCode_Option_SchoolSession_Evening = "夜校";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K1 = "K1 - Nursery Class";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K2 = "K2 - Lower Kindergarten";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_K3 = "K3 - Upper Kindergarten";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_LP = "LP - Lower Preparatory";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P1 = "P1 - 小一";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P2 = "P2 - 小二";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P3 = "P3 - 小三";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P4 = "P4 - 小四";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P5 = "P5 - 小五";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_P6 = "P6 - 小六";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_PP = "PP - Pre-Primary - Spec";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_PR = "PR - Primary - Spec";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S1 = "S1 - 中一";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S2 = "S2 - 中二";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S3 = "S3 - 中三";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S4 = "S4 - 中四";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S5 = "S5 - 中五";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S6 = "S6 - 中六";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_S7 = "S7 - 中七";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_SJ = "SJ - 初中";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_SS = "SS - 高中";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_UP = "UP - Upper Preparatory";
$i_WebSAMS_Attendance_Reason_ReasonCode = "原因代码";
$i_WebSAMS_Attendance_Reason_ReasonText = "原因名称";
$i_WebSAMS_Attendance_Reason_ReasonType="原因类别";
$i_WebSAMS_Attendance_Reason_OtherReason="其他原因";
$i_WebSAMS_Attendance_Reason_OtherReason_Notice="当所输入的原因没有相应代码时，系统将会以此代码代替";
$i_WebSAMS_Attendance_Reason_Notice1="使用WEBSAMS考勤原因.";
$i_WebSAMS_ClassCode="WebSAMS 班别代码";
$i_WebSAMS_Class_ImportInstruction="<b><u>档案说明:</u></b><br>
第一栏 : 班级名称 (如 1A ).<br>
第二栏 : 级别 (如 F.1 ).<br>
第三栏 : WebSAMS 班别代码 (如 1B ).<br>
第四栏 : WebSAMS 级别 (如 S1 ).<br>";


$i_WebSAMS_ReasonCode_ImportInstruction="<table border=0 cellspacing=1 cellpadding=1><tr><td align=right><b><u>档案说明:</u></b><br></td></tr>
<tr><td align=right>第一栏 : </td><td>原因代码 (如 01 )</td></tr>
<tr><td align=right>第二栏 : </td><td>原因名称 (如 Sick Leave )</td></tr>
<tr><td align=right>第三栏 : </td><td>原因类别 (如 1) :</td></tr>
<tr><td></td><td>1 - 缺席</td></tr>
<tr><td></td><td>2 - 迟到</td></tr>
<tr><td></td><td>3 - 早退</td></tr>
</table>";
$i_WebSAMS_Attendance_Export_Warning1="请先输入以下基本资料 :<br><li>$i_WebSAMS_AttendanceCode_SchoolID</li><li>$i_WebSAMS_AttendanceCode_SchoolYear</li><li>$i_WebSAMS_AttendanceCode_SchoolLevel</li><li>$i_WebSAMS_AttendanceCode_SchoolSession</li>";
$i_WebSAMS_Attendance_Export_Warning2="以下级别没有连结至WebSAMS级别:";
$i_WebSAMS_Attendance_Export_Warning3="搜寻的结果超出限额10000,请收窄日期范围";
$i_WebSAMS_Attendance_Reason_CurrenlyMapTo="现正使用";

$i_Payment_UserType="用户类别";
$i_Payment_All ="全部";
## Added on 18-7-2007
$i_Payment_All_Users ="全部用户";
$i_Payment_System = "智能卡缴费系统";
$i_Payment_System_Admin_User_Setting = "设定管理用户";
$i_Payment_Menu_Settings = "基本设定";
$i_Payment_Menu_DataImport = "户口纪录资料汇入";
$i_Payment_Menu_DataBrowsing = "各项纪录浏览";
$i_Payment_Menu_StatisticsReport = "统计及报表";
$i_Payment_Menu_PrintPage = "可列印页";
$i_Payment_Menu_Settings_TerminalIP = "智能卡终端机设定";
$i_Payment_Menu_Settings_TerminalAccount = "终端机登入户口";
$i_Payment_Menu_Settings_PaymentCategory = "缴费类别设定";
$i_Payment_Menu_Settings_PaymentItem = "缴费项目设定";
$i_Payment_Menu_Settings_PaymentItem_SelectDateRange="显示以下日子期间所有的缴费项目";
$i_Payment_Menu_Settings_PurchaseCategory = "货品类别设定";
$i_Payment_Menu_Settings_PurchaseItem = "货品项目设定";
$i_Payment_Menu_Settings_ClearTransaction = "清除所有交易纪录";
$i_Payment_Menu_Settings_ResetAccount = "重设所有户口金额";
$i_Payment_Menu_Settings_Letter = "缴费通知书设定";
$i_Payment_Menu_Settings_Letter_Header = "通知书页首";
$i_Payment_Menu_Settings_Letter_Footer = "通知书页尾";
$i_Payment_Menu_Settings_PPS_Setting = "缴费聆设定";
$i_Payment_Menu_Settings_Letter_Disable_Signature = "不显示家长签名栏";


$i_Payment_Menu_Import_PPSLink = "学生 PPS 户口连结";
$i_Payment_Menu_Import_PPSLog = "PPS 每日纪录";
$i_Payment_Menu_Import_CashDeposit = "现金存入纪录";
$i_Payment_Menu_Browse_TerminalLog = "终端机登入纪录";
$i_Payment_Menu_Browse_CreditTransaction = "户口增值纪录";
$i_Payment_Menu_Browse_CreditTransaction_DateRange = "选择时间范围";
$i_Payment_Menu_Browse_StudentBalance = "检视用户的结存及收支纪录";
$i_Payment_Menu_Browse_TransactionRecord = "详细纪录";
$i_Payment_Menu_Browse_PurchasingRecords = "单一销售纪录";
$i_Payment_Menu_Browse_MissedPPSBill = "不明的缴费聆纪录";
$i_Payment_Menu_PrintPage_Outstanding = "追数表";
$i_Payment_Menu_PrintPage_AddValueReport = "增值报告";
$i_Payment_Menu_PrintPage_AddValueReport_Date= "日期";
$i_Payment_Menu_Report_SchoolAccount = "学校户口报告";
$i_Payment_Menu_Report_PresetItem = "预设缴费项目报告";
$i_Payment_Menu_Report_TodayTransaction = "今日交易纪录";

$i_Payment_Menu_PrintPage_AddValueReport_PaymentTotal = "款项(总数)";
$i_Payment_Menu_PrintPage_AddValueReport_TransactionCount = "交易纪录(项数)";

$i_Payment_Menu_PrintPage_Receipt = "收据";
$i_Payment_Menu_PrintPage_Receipt_PaymentItemList = "学生缴费收据";

$i_Payment_Import_Format_PPSLink2 = "<b><u>档案说明:</u></b><br>
第一栏 : $i_UserLogin<br>
第二栏 : PPS 户口号码
";
$i_Payment_Import_Format_PPSLink = "<b><u>档案说明:</u></b><br>
第一栏 : 班别<br>
第二栏 : 班号<br>
第三栏 : PPS 户口号码
";
$i_Payment_Import_Format_PPSLog = "请上载由 PPS 下载的纪录档案";
$i_Payment_Import_Format_PPSLog_Charge_Deduction = "缴费聆手续费($".LIBPAYMENT_PPS_CHARGE.", Counter Bill 则为 $".LIBPAYMENT_PPS_CHARGE_COUNTER_BILL.")将自动从所有交易项目中扣除。";
$i_Payment_Import_PPSLink_Result = "汇入结果";
$i_Payment_Import_PPSLink_Result_Summary_TotalRows = "总数";
$i_Payment_Import_PPSLink_Result_Summary_Successful = "已更新";
$i_Payment_Import_PPSLink_Result_Summary_NoMatch = "没有这个学生";
$i_Payment_Import_PPSLink_Result_Summary_Occupied = "PPS 帐号已被使用";
$i_Payment_Import_PPSLink_Result_Summary_Missing = "没有 PPS 帐号";
$i_Payment_Import_PPSLink_Result_Summary_ExistAlready = "学生已有 PPS 帐号";
$i_Payment_Import_PPSLink_Result_ErrorMsg_NotSuccessful = "未能汇入的纪录";
$i_Payment_Import_PPSLink_Result_ErrorMsg_NoMatch = "没有这个学生.";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Occupied = "此 PPS 帐号已被使用.";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Missing = "没有 PPS 帐号";
$i_Payment_Import_PPSLink_Result_ErrorMsg_ExistAlready = "此学生已有 PPS 帐号. 如更改请先移除原有号码.";
$i_Payment_Import_PPSLink_Result_GoToIndex = "返回学生列表";
$i_Payment_Import_PPSLink_Result_ErrorMsg_Reason = "原因";
$i_Payment_Import_PPS_FileType = "档案类型";
$i_Payment_Import_PPS_FileType_Normal = "IEPS档案 (利用电话或互联网)";
$i_Payment_Import_PPS_FileType_CounterBill = "Counter Bill 档案 (例如: OK 便利店)";
$i_Payment_PPS_Charge = "缴费聆手续费";
$i_Payment_Import_PPS_ChargeSeparation = "手续费会分开作另一个交易纪录显示";

$i_Payment_Field_PPSAccountNo = "PPS 帐户号码";
$i_Payment_Field_CreditAmount = "增值金额";
$i_Payment_Field_CreditAmount_AfterDeduction = "扣除手续费后增值金额";
$i_Payment_Field_TransactionTime = "交易时间";
$i_Payment_Field_PostTime = "进志时间";
$i_Payment_Field_Username = "用户名称";
$i_Payment_Field_PaymentAllowed = "缴费";
$i_Payment_Field_PurchaseAllowed = "售货/增值";
$i_Payment_Field_LastLogin = "最后登入";
$i_Payment_Field_AccountCreation = "户口开设时间";
$i_Payment_Field_RefCode = "参考编号";
$i_Payment_Field_Balance = "结存";
$i_Payment_Field_LastUpdated = "最后更新";
$i_Payment_Field_LastUpdatedBy = "最后更新的管理员";
$i_Payment_Field_AdminHelper = "经管理中心";
$i_Payment_Field_TerminalUser = "经终端机";
$i_Payment_Field_TransactionType = "交易类别";
$i_Payment_Field_TransactionDetail = "交易内容";
$i_Payment_Field_PaymentCategory = "缴费类别";
$i_Payment_Field_DisplayOrder = "排列次序";
$i_Payment_Field_PaymentItem = "缴费项目";
$i_Payment_Field_PaymentDefaultAmount = "正常金额";
$i_Payment_Field_Amount = "金额";
$i_Payment_Field_Function = "功能";
$i_Payment_Field_ChangePassword = "更改密码";
$i_Payment_Field_LoginTime = "登入时间";
$i_Payment_Field_LogoutTime = "登出时间";
$i_Payment_Field_IP = "终端机 IP 位址";
$i_Payment_Field_CurrentBalance = "现时结余";
$i_Payment_Field_BalanceAfterPay = "缴款后结余";
$i_Payment_Field_BalanceAfterUndo = "还原后结余";
$i_Payment_Field_PaidTime = "缴费时间";
$i_Payment_Field_PaymentDeadline = "缴费最后限期";
$i_Payment_Field_BalanceAfterTransaction = "帐后结余";
$i_Payment_Field_PayPriority = "付费优先次序";
$i_Payment_Field_NewDefaultAmount = "新$i_Payment_Field_PaymentDefaultAmount";
$i_Payment_Field_SinglePurchase_Unit = "收费单位";
$i_Payment_Field_SinglePurchase_TotalAmount = "共收金额";
$i_Payment_Field_SinglePurchase_Count = "交易次数";
$i_Payment_Field_AdminInCharge = "负责人";
$i_Payment_Field_RecordedAccountNo = "纪录中的 $i_Payment_Field_PPSAccountNo";
$i_Payment_Field_MappedStudent = "已转到学生";
$i_Payment_Field_PaidCount = "已缴费";
$i_Payment_Field_TotalPaidCount = "总人数";

$i_Payment_Field_AllPaid = "已收妥";
$i_Payment_Field_NotAllPaid = "尚未收妥";

$i_Payment_Field_PaidCount = "已缴费人数";
$i_Payment_Field_TotalPaidCount = "总人数";
$i_Payment_Field_TransactionFileTime = "增值纪录时间";

### Added On 3 Aug 2007 ###
$i_Payment_Menu_ManualCashDeposit = "手动现金存入纪录";
$i_Payment_CashDepositDate = "现金存入日期";
$i_Payment_CashDepositTime = "现金存入时间";
###########################

### Added On 7 Aug 2007 ###
$i_Payment_Field_SetAllUse = "设定为全部使用";
$i_Payment_AmountWarning = "请输入金额";
$i_Payment_RefCodeWarning = "请输入参考编号";
$i_Payment_DepositDateWarning = "请输入现金存入日期";
###########################

### Added On 8 Aug 2007 ###
$i_Payment_DetailTransactionLog = "详细交易记录";
$i_Payment_DepositWrongDateWarning = "请输入正确现金存入日期";
$i_Payment_DepositAmountWarning = "请输入正确金额";
###########################

### Sentence is too long for using linterface->GET_SYS_MSG()
$i_Payment_Import_ConfirmRemoval = "<table class='systemmsg' width='50%' border='0' cellpadding='3' cellspacing='0'><tr><td>系统发现有未经确认之纪录. 这可能是由于上次的输入并未确认成功或另一管理员正在输入.<br />";
$i_Payment_Import_ConfirmRemoval2 = "你需要等待 (重新整理此页) 或强行删除那些未确认纪录, 才可以汇入新的档案.</td><tr></table>";

$i_Payment_Import_Confirm = "请按确认以继续或按取消以清除.";
$i_Payment_AccountNoMatch = "帐户号码尚未登记<font color=red>*</font>";
$i_Payment_AccountNoMatch_Remark="系统会将此交易纪录视为不明的缴费聆纪录处理。<br>请前往 <b>$i_Payment_Menu_DataBrowsing</b> &gt; <b>$i_Payment_Menu_Browse_MissedPPSBill</b> 匹配此帐户号码予相关学生。";
$i_Payment_Import_Error="是次纪录未能汇入，请检查档案中的资料正确无误后再次上载";
$i_Payment_Import_NoMatch_Entry = "班别及班号错误";
$i_Payment_Import_NoMatch_Entry2 = "内联网帐号错误";


$i_Payment_Import_InvalidAmount="金额无效";
$i_Payment_Import_DuplicatedStudent="资料重复";
$i_Payment_Import_PayAlready="此学生已缴费";

$i_Payment_Import_CashDeposit_Instruction = "
<span class=extraInfo>只适用于学生</span><Br><Br>
第一栏 : 班别<Br>
第二栏 : 班号<br>
第三栏 : 存入金额<br>
第四栏 : 存入之日期时间 (YYYY-MM-DD HH:mm:ss, 留空会被视为此汇入之时间)<br>
第五栏 : $i_Payment_Field_RefCode (留空会由系统自动产生)
";
$i_Payment_Import_CashDeposit_Instruction2 = "
<span class=extraInfo>适用于学生或老师</span><Br><br>
第一栏 : $i_UserLogin<br>
第二栏 : 存入金额<br>
第三栏 : 存入之日期时间 (YYYY-MM-DD HH:mm:ss, 留空会被视为此汇入之时间)<br>
第四栏 : $i_Payment_Field_RefCode (留空会由系统自动产生)
";

$i_Payment_Auth_PaymentRequired = "缴费终端机不需登入";
$i_Payment_Auth_PurchaseRequired = "货品售卖/增值终端机不需登入";

$i_Payment_Credit_Method = "增值方法";
$i_Payment_Credit_TypePPS = "PPS";
$i_Payment_Credit_TypeCashDeposit = "现金";
$i_Payment_Credit_TypeAddvalueMachine = "增值机";
$i_Payment_Credit_TypeUnknown = "没有纪录";

$i_Payment_TransactionType_Credit = "增值";
$i_Payment_TransactionType_Payment = "缴费";
$i_Payment_TransactionType_Purchase = "销售";
$i_Payment_TransactionType_TransferTo = "转账至";
$i_Payment_TransactionType_TransferFrom = "由转账";
$i_Payment_TransactionType_CancelPayment = "取消缴费";
$i_Payment_TransactionType_Refund = "退款";
$i_Payment_TransactionType_Debit = "支出";
$i_Payment_TransactionType_Other = "其他";

$i_Payment_TransactionDetailPPS = "PPS 增值 / PPS Credit";
$i_Payment_TransactionDetailCashDeposit = "现金增值 / Cash Deposit";
$i_Payment_ClassInvolved = "需要缴费的班别";
$i_Payment_NoClass = "如不是全班需要缴费, 请新增后才使用汇入";

$i_Payment_PaymentStatus_NotStarted="未开始";
$i_Payment_PaymentStatus_Paid = "已缴交";
$i_Payment_PaymentStatus_Unpaid = "未缴交";
$i_Payment_Payment_PaidCount = "已缴交的学生";
$i_Payment_Payment_UnpaidCount = "未缴交的学生";
/*
$i_Payment_Payment_StudentImportFileDescription1 = "
第一栏 : 班别 <br>
第二栏 : 班号 <br>
第三栏 : 需缴付金额 <br>";
$i_Payment_Payment_StudentImportFileDescription2 = "
第一栏 : 内联网帐号 <br>
第二栏 : 需缴付金额 <br>";
*/
$i_Payment_Payment_StudentImportFileDescription1 = "班别,班号,需缴付金额";
$i_Payment_Payment_StudentImportFileDescription2 = "内联网帐号,需缴付金额";

$i_Payment_Payment_StudentImportNotice = "
请注意:<br>
- 已缴交之学生不会被更改 <br>
- 如金额空白, 则表示此学生无需缴交此项费用
";
$i_Payment_con_PaidNoEdit = "<font color=red>已缴费的纪录不能更改</font>";
$i_Payment_con_PaymentProcessed = "<font color=green>缴费已成功处理.</font>";
$i_Payment_con_PaymentUndo = "<font color=green>款项已还原.</font>";
$i_Payment_con_PaymentProcessFailed = "<font color=red>款项未能成功缴交.</font>";
$i_Payment_con_RefundFailed = "<font color=red>不能退款或结存为零</font>";
$i_Payment_con_RecordMapped = "<font color=red>这项纪录已经被处理</font>";
$i_Payment_con_FileImportedAlready = "<font color=red>这个档案已被汇入过一次</font>";

$i_Payment_alert_selectOneFunction = "你必须选择最少一项功能";
$i_Payment_alert_forcepay = "你确定要替已选之学生进行缴款程序?";
$i_Payment_alert_undopay = "你确定要把已选学生之缴款纪录还原?";
$i_Payment_alert_refund = "你确定要把已选学生退款? (此程序不能还原)";

// add by kenneth chung on 20090112
$i_Payment_alert_forcepayall = "你确定要替学生进行缴款程序?";

$i_Payment_action_batchpay = "缴款";
$i_Payment_action_undo = "还原至未缴";
$i_Payment_action_proceed_payment = "进行缴款";
$i_Payment_action_proceed_undo = "进行还原";
$i_Payment_action_cancel_payment = "取消";
$i_Payment_action_refund = "退款";
$i_Payment_action_handle = "处理";

$i_Payment_AccountBalance = "户口结存及纪录";
$i_Payment_PaymentRecord = "缴费纪录";
$i_Payment_ValueAddedRecord = "增值纪录";
$i_Payment_Transfer = "转账至另一子女";
$i_Payment_SelectChildToTransfer = "转账至";
$i_Payment_TransferAmount = "转账金额";
$i_Payment_ItemSummary = "概览";
$i_Payment_ItemPaid = "已缴金额";
$i_Payment_ItemUnpaid = "未缴金额";
$i_Payment_ItemTotal = "总金额";
# Added on 17-7-2007
$i_Payment_Item_Select_Class_Level = "全级";
$i_Payment_Students = "学生";
$i_Payment_Refunded = "已退款";
$i_Payment_ChangeDefaultAmount = "更改$i_Payment_Field_PaymentDefaultAmount";

### Added on 6 Sep 2007
$i_Payment_UpdateRecordConfirm = "你确定要更新记录?";

$i_Payment_Note_Priority = "数值最细为优先";

$i_Payment_Title_Receipt_Payment = "缴费收据";
$i_Payment_Title_Overall_SinglePurchasing = "单一销售纪录";
$i_Payment_Title_SinglePurchasing_Detail = "单一销售纪录";

$i_Payment_SearchResult = "搜寻结果";
$i_Payment_IncreaseSensitivity = "缩窄搜寻 (较少结果会被显示)";
$i_Payment_DecreaseSensitivity = "扩大搜寻 (更多结果会被显示)";
$i_Payment_ManualSelectStudent = "自行选择";
$i_Payment_ShowAll = "显示全部";
$i_Payment_ShowUnlink = "未处理";
$i_Payment_ShowHandled = "已处理";
$i_Payment_Exclude = "不显示无需追收的班别 <font color=red>(不适用于用缴费通知书)</font>";
$i_Payment_IncludeNonOverDue = "包括所有未到期的缴费项目";

$i_Payment_TotalAmount = "总金额";
$i_Payment_TotalCountTransaction = "交易次数";

// Add on 20-2-2009
$i_Payment_Receipt_Person_In_Charge = "负责人";

$i_Payment_Receipt_Principal = "校长";
$i_Payment_Receipt_SchoolChop = "校印";
$i_Payment_Receipt_Date = "日期";
$i_Payment_Receipt_ThisIsComputerReceipt = "此乃电脑收据";
$i_Payment_Receipt_Remark = "备注";
$i_Payment_Receipt_UpTo = "截至";
$i_Payment_Receipt_AccountBalance = "智能卡户口结余";
$i_Payment_Receipt_Official_Receipt = "正式收据";
$i_Payment_Receipt_Computer_GeneratedReceipt = "电脑收据";
$i_Payment_Receipt_ReceivedFrom = "兹收到";
$i_Payment_Receipt_PaidAmount = "交来 HKD";
$i_Payment_Receipt_ForItems = ", 所缴费用以支付下列项目:";
$i_Payment_Receipt_Payment_Category = "收费类别";
$i_Payment_Receipt_Payment_Item = "收费项目";
$i_Payment_Receipt_Payment_Description = "说明";
$i_Payment_Receipt_Payment_Amount = "金额";
$i_Payment_Receipt_Payment_Total = "总额";
$i_Payment_Receipt_Payment_ReceiptCode = "收据编号";
$i_Payment_Receipt_Payment_StaffInCharge_Name = "负责职员姓名";
$i_Payment_Receipt_Payment_StaffInCharge_Signature = "负责职员签署";
$i_Payment_Receipt_Payment_Date = "日期";
$i_Payment_PrintPage_Outstanding_Total = "总结欠金额";
$i_Payment_PrintPage_Outstanding_DueDate = "到期日";
$i_Payment_PrintPage_PaymentLetter = "缴费通知书";
$i_Payment_PrintPage_PaymentLetter_ParentSign = "家长签署";
$i_Payment_PrintPage_PaymentLetter_Date = "日期";
$i_Payment_PrintPage_PaymentLetter_UnpaidItem = "未缴费项目";
$i_Payment_PrintPage_Browse_UserIdentityWarning = "请选择身份";
$i_Payment_PrintPage_Browse_UserStatusWarning = "请选择状况";
$i_Payment_PrintPage_StudentOnly = "学生专用";

$i_Payment_PrintPage_Outstanding_AccountBalance = "户口结余";
$i_Payment_PrintPage_Outstanding_LeftAmount = "户口结存不足金额";
$i_Payment_Note_StudentRemoved = "<font color=red>*</font> - 表示该用户已被移除.";
$i_Payment_Note_PPSCostExport = "日期计算 PPS 每日纪录内的交易时间";

### added on 11 Oct 2007
$i_Payment_PhotocopierQuotaNotValid_Warning = "请输入有效配额";
$i_Payment_PhotocopierPackageQtyNotValid_Warning = "请输入有效数量";
$i_Payment_PhotocopierQuotaSelectPackage_Warning = "请选择影印套票";
$i_Payment_PhotocopierQuotaPackageName_Warning = "请输入影印套票名称";
$i_Payment_PhotocopierQuotaPriceNotValid_Warning = "请输入有效金额";

$i_Payment_SchoolAccount_Detailed_Transaction="进支详情";
$i_Payment_SchoolAccount_Deposit="存入";
$i_Payment_SchoolAccount_Expenses="支出";
$i_Payment_SchoolAccount_PresetItem="预设缴费项目";
$i_Payment_SchoolAccount_Total="总数";
$i_Payment_SchoolAccount_SinglePurchase="单一销售";
$i_Payment_SchoolAccount_TotalIncome="总收入";
$i_Payment_SchoolAccount_TotalExpense="总支出";
$i_Payment_SchoolAccount_NetIncomeExpense="现金净流入(流出)";
$i_Payment_SchoolAccount_Other="其它";
$i_Payment_PresetPaymentItem_PaidCount ="已缴交";
$i_Payment_PresetPaymentItem_UnpaidCount ="未缴交";
$i_Payment_PresetPaymentItem_PaidAmount ="已缴金额";
$i_Payment_PresetPaymentItem_UnpaidAmount ="未缴金额";
$i_Payment_PresetPaymentItem_ClassStat ="班别统计";
$i_Payment_PresetPaymentItem_Personal ="个别学生纪录";
$i_Payment_PresetPaymentItem_Progress ="进行中";
$i_Payment_PresetPaymentItem_PaymentPeriod="收款日期";
$i_Payment_PaymentStatus_Ended="已结束";
$i_Payment_PresetPaymentItem_PaidStudentCount="已缴人数";
$i_Payment_PresetPaymentItem_UnpaidStudentCount="未缴人数";
$i_Payment_Menu_StatisticsReport_CreateTime="报表建立时间";
$i_Payment_Warning_Enter_PaymentItemName="请输入项目名称";
$i_Payment_Warning_Invalid_DisplayOrder ="排列次序无效";
$i_Payment_Warning_Invalid_PayPriority ="付费优先次序无效";
$i_Payment_Warning_Enter_NewDefaultAmount="请输入新正常金额";
$i_Payment_Warning_Enter_DefaultAmount="请输入正常金额";
$i_Payment_Warning_Item_Has_Paid_Student = "已有学生就所选缴费项目进行缴款, 请还原有关缴款后再行删除。";
$i_Payment_Warning_Item_No_Has_Paid_Student = "未有学生就所选缴费项目进行缴款, 你可以放心删除。";
$i_Payment_Class_Payment_Report="班别缴费报告";
$i_Payment_Class_Payment_Report_Warning="此日期范围内的缴费项目多于256个，请尝试收窄日期范围。";
$i_Payment_PresetPaymentItem_NoNeedToPay ="--";
$i_Payment_Menu_Settings_Letter_Header2 = "通知书页首二";
$i_Payment_Warning_InvalidAmount ="输入金额无效";
$i_Payment_Total_Balance ="总结存";
$i_Payment_Total_Users ="总用户";

### Added On 30 Apr 2007
$i_Payment_Menu_PhotoCopierQuotaSetting = "影印配额设定";
$i_Payment_Menu_PhotoCopier_Package_Setting = "影印套票设定";
$i_Payment_Menu_PhotoCopier_Quota_Management = "影印配额管理";
$i_Payment_Menu_PhotoCopier_Quota_Record = "影印配额记录";
$i_Payment_Menu_PhotoCopier_Quota_Details = "影印使用记录";
$i_Payment_Menu_PhotoCopier_Quota_Purchase = "购买影印套票";
$i_Payment_Menu_PhotoCopier_View_Quota_Record = "检视影印配额记录";
$i_Payment_Menu_PhotoCopier_TitleName = "影印套票名称";
### Added On 2 May 2007
$i_Payment_Field_Quota = "配额";
### Added On 25 Apr 2007
$i_SmartCard_Payment_alert_RecordAddSuccessful = "<font color=green>已新增纪录</font>";
$i_SmartCard_Payment_Student_Select_Instruction = "请选择学生";
$i_SmartCard_Payment_Input_Amount_Instruction = "请输入金额";
$i_SmartCard_Payment_Photocopier_Quota_Purchase_Quantity = "数量";
$i_SmartCard_Payment_Input_PaymentItem_Instruction = "请输入缴费项目";
$i_SmartCard_Payment_Input_Quota_Instruction = "请输入配额";
### Added On 3 May 2007
$i_SmartCard_Payment_PhotoCopier_TotalQuota = "总配额";
$i_SmartCard_Payment_PhotoCopier_UsedQuota = "已使用配额";
$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction = "请选择班别";
$i_SmartCard_Payment_PhotoCopier_LastestTransactionTime = "最后交易时间";
### Added On 4 May 2007
$i_SmartCard_Payment_PhotoCopier_SelectUser = "选择用户";
### Added On 7 May 2007
$i_SmartCard_Payment_PhotoCopier_ResetQuota = "你是否确定要重设所选学生的配额?";
### Added On 8 May 2007
$i_SmartCard_Payment_PhotoCopier_ClassLevel_Select = "选择级别";
$i_SmartCard_Payment_PhotoCopier_Class_Select = "选择班别";
### Added On 9 May 2007
$i_SmartCard_Payment_PhotoCopier_TotalQuota_Instruction = "请输入总配额";
$i_SmartCard_Payment_PhotoCopier_ClassLevel_Select_Instruction = "请选择级别";
$i_SmartCard_Payment_PhotoCopier_Class_Select_Instruction = "请选择班别";
$i_SmartCard_Payment_PhotoCopier_SelectUser_Instruction = "请选择用户";
$i_SmartCard_Payment_photoCopier_UserType = "用户类别";
$i_SmartCard_Payment_PhotoCopier_TotalQuota_Invalid = "输入总配额无效";

$i_SmartCard_Payment_PhotoCopier_ChangeQuota = "新增 / 修改配额";
$i_SmartCard_Payment_PhotoCopier_FinalQuota = "最后配额";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType = "修改类别";

$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Reset_By_Admin = "管理员重设";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Add_By_Admin = "管理员新增";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Purchasing = "购买";
$i_SmartCard_Payment_PhotoCopier_ChangeRecordType_Copied_Deduction = "影印扣除";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Quota_Purchase_Quantity = "数量";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type = "类型";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_bw = "黑白 A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_bw = "黑白 非A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_a4_color = "彩色 A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_Type_non_a4_color = "彩色 非A4";
$i_SmartCard_Payment_PhotoCopier_Details_Transaction_No_Record= "没有纪录";

$i_Payment_Menu_Settings_PaymentItem_Import_Action_New="新增学生";
$i_Payment_Menu_Settings_PaymentItem_Import_Action_Update="更改金额";
$i_Payment_Menu_Settings_PaymentItem_Import_Action_NoChange="不更改(已缴交)";
$i_Payment_Menu_Settings_PaymentItem_Import_Action="动作";
$i_Payment_Menu_Settings_PaymentItem_ArchivedRecord="已整存纪录";
$i_Payment_Menu_Settings_PaymentItem_ActiveRecord="现行纪录";
$i_Payment_Menu_Settings_PaymentItem_Archive_Warning="你是否确定要整存项目?";
$i_Payment_Menu_Settings_PaymentItem_UndoArchive_Warning="你是否确定要将项目还原至未整存?";
$i_Payment_Menu_Settings_PaymentItem_Archive_Warning2="以下项目已整存，不能再进行缴款 :";

### Addded On 23 Aug 2007 ###
$i_Payment_Teacher_PaymentRecord = "检视教师个人缴费纪录";
#############################

$i_Payment_Subsidy_Option_No="没有资助";
$i_Payment_Subsidy_Current_Amount="现时金额";
$i_Payment_Subsidy_New_Amount="更新金额";
$i_Payment_Subsidy_Current_Subsidy_Amount="现时资助金额";
$i_Payment_Subsidy_New_Subsidy="更新资助金额";
$i_Payment_Subsidy_Amount="资助金额";
$i_Payment_Subsidy_Unit="资助来源";
$i_Payment_Subsidy_UnitName=$i_Payment_Subsidy_Unit;
$i_Payment_Subsidy_Edit_Confirm="你是否确定要呈送?";
$i_Payment_Subsidy_Total_Subsidy_Amount ="总资助金额";
$i_Payment_Subsidy_Total_Amount ="总额";
$i_Payment_Subsidy_Setting="资助来源设定";
$i_Payment_Subsidy_Warning_Enter_UnitName="请输入".$i_Payment_Subsidy_UnitName;
$i_Payment_Subsidy_Used_Amount="已使用";
$i_Payment_Subsidy_Left_Amount="余额";
$i_Payment_Subsidy_Search_Amount="以下学生合共使用资助额";
$i_Payment_Subsidy_Unit_Admin ="最后更新";
$i_Payment_Subsidy_Unit_UpdateTime="更新时间";
$i_Payment_StudentStatus_Removed="已移除的学生";
$i_Payment_StudentStatus_NonRemoved="未移除的学生";
$i_Payment_Subsidy_Warning_Select_SubsidyUnit="请选择".$i_Payment_Subsidy_Unit;
$i_Payment_Subsidy_Warning_Enter_New_SubsidyAmount=$i_Payment_Subsidy_New_Subsidy."必须大于0";
$i_Payment_Subsidy_Warning_NotEnough_Amount=$i_Payment_Subsidy_Unit.$i_Payment_Subsidy_Left_Amount."不足";

$i_Payment_PPS_Export_Choices_1=$i_status_suspended.", ".$i_status_approved.", ".$i_status_pendinguser."的学生";
$i_Payment_PPS_Export_Choices_2=$i_status_graduate."的学生";
$i_Payment_PPS_Export_Choices_3=$i_Payment_StudentStatus_Removed." <span class='extraInfo'>(以 </span><b>*</b><span class='extraInfo'> 标记)</span>";
$i_Payment_PPS_Export_Select_Student="学生类别";
$i_Payment_PPS_Export_Warning="请选择".$i_Payment_PPS_Export_Select_Student;
$i_Payment_Note_StudentRemoved2 = "* - 表示该用户已被移除.";
$i_Payment_OutstandingAmount="未缴金额";
$i_Payment_OutstandingAmountLeft="尚欠";
$i_Payment_PaymentItem_Pay_Warning="系统将不处理户口结余不足以缴付当前款项之学生.";
$i_Payment_Refund_Warning="系统将不处理户口结余为 $0 之用户";
$i_Payment_Subsidy_Warning_TotalAmount_MustGreaterThan_ConsumedAmount=$i_Payment_Subsidy_Total_Amount."不能少于".$i_Payment_Subsidy_Used_Amount.$i_Payment_Field_Amount;
$i_Payment_Class_Payment_Report_Display_Mode="显示模式";
$i_Payment_Class_Payment_Report_Display_Mode_1="于列显视学生, 于栏显视缴费项目";
$i_Payment_Class_Payment_Report_Display_Mode_2="于列显视缴费项目, 于栏显视学生";
$i_Payment_PresetPaymentItem_ClassStat_Subsidy_Plus_Paid="总金额(已付+资助)";
$i_Payment_Class_Payment_Report_NoNeedToPay="不需缴交";
$i_Payment_Class_Item_Type="纪录类型";
$i_Payment_Report_Print_Date="编印日期";
$i_Payment_Class_Payment_Report_Notice="每页最多可显示45位学生之纪录";
$i_Payment_Class_Payment_Report_No_of_Items="每版显示项目";
$i_Payment_Search_By_TransactionTime="以".$i_Payment_Field_TransactionTime;
$i_Payment_Search_By_PostTime="以".$i_Payment_Field_PostTime;
$i_Payment_Field_Total_Chargeable_Amount="总应缴金额";
$i_Payment_Field_Chargeable_Amount="应缴金额";
$i_Payment_Field_Current_Chargeable_Amount="现时应缴金额";
$i_Payment_Field_New_Chargeable_Amount="更新应缴金额";
$i_Payment_NoTransactions_Processed="未有处理任何增值纪录。";

### added on 5 jun 2008 ###
$i_Payment_ViewStudentAccount_InvalidStartDate = "开始日期无效。";
$i_Payment_ViewStudentAccount_InvalidEndDate = "结束日期无效。";
$i_Payment_ViewStudentAccount_InvalidDateRange = "日期范围无效。";
###########################

### Added on 20080813 ###
$i_Payment_Menu_Report_Export_StudentBalance = "汇出用户结存及收支纪录";
$i_Payment_Menu_Report_StudentBalance = "用户的结存及收支纪录";
$i_Payment_Menu_Report_Export_StudentBalance_Alert1 = "请选择级别";
$i_Payment_Menu_Report_Export_StudentBalance_Alert2 = "请选择班别";
$i_Payment_Menu_Report_Export_StudentBalance_Alert3 = "请选择学生";
$i_Payment_Menu_Report_Export_StudentBalance_MultiSelect = "可按住CTRL键，点击多个项目。";
$i_Payment_Menu_Browse_StudentBalance_Status_Left_Temp = "已离校 (暂存)";
$i_Payment_Menu_Browse_StudentBalance_Status_Left_Archived = "已离校 (整存)";
#########################


// added on 20090407
$Lang['Payment']['CancelDeposit'] = "取消纪录";
$Lang['Payment']['CSVImportDeposit'] = "CSV 汇入";
$Lang['Payment']['ManualDeposit'] = "手动输入";

// added on 20090415
$Lang['Payment']['CancelDepositDescription'] = "取消现金存入纪录";
$Lang['Payment']['CashDeposit'] = "现金存入";
$Lang['Payment']['CancelDepositWarning'] = "你确定要取消选取的纪录? (此程序不能还原)";
$Lang['Payment']['DonateBalanceWarning'] = "你确定要把已选取之纪录捐给学校? (此程序不能还原)";
$Lang['Payment']['DonateBalance'] = "捐款给学校";
$Lang['Payment']['DonateBalanceDescription'] = "结余捐款给学校";
$Lang['Payment']['Donated'] = "已捐款";
$Lang['Payment']['DonateFailed'] = "<font color=red>不能捐款或结存为零</font>";
$Lang['Payment']['DepositCancelSuccess'] = "<font color=green>纪录已取消</font>\n";
$Lang['Payment']['CancelDepositInvalidWarning'] = "某些记录的结余未能支付取消动作";

// added on 20090708
$Lang['Payment']['CancelDepositReport'] = "取消增值记录报告";
$Lang['Payment']['DonationReport'] = "捐款报告";
$Lang['Payment']['Keyword'] = "关键字";

// added on 20090803
$Lang['Payment']['ArchiveUserLegend'] = "<font color=red>*</font> - 已整存用户";

$i_StaffAttendance_InputTime_Warning1="进入时间不可大过下班时间";
$i_StaffAttendance_InputTime_Warning2="这个职员于所选日期不需要拍卡";
$i_StaffAttendance_InputTime_Warning3="系统已有进入时间纪录";
$i_StaffAttendance_InputTime_Warning4="离开时间不可小于上班时间";
$i_StaffAttendance_InputTime_Warning5="系统已有离开时间纪录";
$i_StaffAttendance_InputTime_Confirmation="请确保所输入之时间没有错误. 确定要新增时间?";


$i_StaffAttendance_ManualTimeAdjustment ="手动更改时间";
$i_StaffAttendance_Outing_warning="以下职员在该日已有假期纪录";
$i_StaffAttendance_Holiday_warning="以下职员在该日已有外出纪录";
$i_StaffAttendance_msg_time1 = "放工时间不可早或等于返工时间";
$i_StaffAttendance_import_invalid_entries="汇入资料错误";
$i_StaffAttendance_msg_password1 = "密码已更改";
$i_StaffAttendance_msg_invalid_number="输入数字无效";
$i_StaffAttendance_System = "eClass 职员智能卡考勤系统";
$i_StaffAttendance_SystemSettings = "系统设定";
$i_StaffAttendance_SystemSettings_Terminal = "终端机设定";
$i_StaffAttendance_SystemSettings_ChangePassword = "更改密码";
$i_StaffAttendance_StaffSettings = "职员资料";
$i_StaffAttendance_StaffSettings_Group = "分组设定";
$i_StaffAttendance_StaffSettings_Staff = "职员资料";
$i_StaffAttendance_DataManagement = "资料管理";
$i_StaffAttendance_DataManagement_RawLog = "拍卡纪录";
$i_StaffAttendance_DataManagement_ViewArchive = "检视/整存拍卡纪录";
$i_StaffAttendance_DataManagement_Outing = "外出纪录";
$i_StaffAttendance_DataManagement_Leave = "假期纪录";
$i_StaffAttendance_Report = "报表";
$i_StaffAttendance_Report_Day = "每日";
$i_StaffAttendance_Report_Week = "每星期";
$i_StaffAttendance_Report_Staff = "以职员检视";
$i_StaffAttendance_Report_Summary = "概览";
$i_StaffAttendance_Report_Day_Range_Abs = "教职员缺席统计表";
$i_StaffAttendance_Report_Day_Range_Abs_By_Name = "教职员请假缺席统计";
$i_StaffAttendance_PageTitle_Day = "职员考勤纪录详情";
$i_StaffAttendance_Report_Staff_Waived = "撤回";
$i_StaffAttendance_Report_Date = "以日期检视";
$i_StaffAttendance_Report_Date_Title = "日期";
$i_StaffAttendance_Report_Number_Of_Date = "日数";

$i_StaffAttendance_Report_Monthly = "每月";

$i_StaffAttendance_LogOut="登出";
$i_StaffAttendance_Find="寻找";

$i_StaffAttendance_Greeting = "请从左边清单选择";

$i_StaffAttendance_Group = "上班时间组别";
$i_StaffAttendance_Group_ID = "组别号码";
$i_StaffAttendance_Group_Title = "组别名称";
$i_StaffAttendance_Group_Start = "返工时间";
$i_StaffAttendance_Group_End = "放工时间";
$i_StaffAttendance_Group_Special = "特别日子";
$i_StaffAttendance_Group_Special_DutyTime = "特别日子上班时间";

$i_StaffAttendance_Group_SpecialDayType = "特别日子";
$i_StaffAttendance_Group_WorkingHrs = "工作时间";
$i_StaffAttendance_NoNeedMark = "不用点名";
$i_StaffAttendance_NeedMark = "需要点名";

$i_StaffAttendance_Group_OnDuty = "需上班";
$i_StaffAttendance_Group_InTime = "进入时间";
$i_StaffAttendance_Group_InStatus = "状况(进入)";
$i_StaffAttendance_Group_InWaived = "撤回进入纪录";
$i_StaffAttendance_Group_OutTime = "离开时间";
$i_StaffAttendance_Group_OutStatus = "状况(离开)";
$i_StaffAttendance_Group_OutWaived = "撤回离开纪录";
$i_StaffAttendance_Status = "状况";
$i_StaffAttendance_Status_in = "进入";
$i_StaffAttendance_Status_out = "离开";

$i_StaffAttendance_Result_Waived = "撤回";
$i_StaffAttendance_Result_NotWaived = "未撤回";

$i_StaffAttendance_Change_Status = "更改状况";
$i_StaffAttendance_Status_Normal = "正常";
$i_StaffAttendance_Status_Absent = "缺席";
$i_StaffAttendance_Status_Late = "迟到";
$i_StaffAttendance_Status_Holiday = "放假";
$i_StaffAttendance_Status_Outgoing = "外出";
$i_StaffAttendance_Status_EarlyLeave = "早退";

$i_StaffAttendance_Mode = "显示模式";
$i_StaffAttendance_Mode_List = "列出名单";
$i_StaffAttendance_Mode_Group = "分组显示";

$i_StaffAttendance_StaffName = "职员姓名";
$i_StaffAttendance_Field_RecordDate = "纪录日期";
$i_StaffAttendance_Field_ArrivalTime = "抵达时间";
$i_StaffAttendance_Field_DepartureTime = "离开时间";
$i_StaffAttendance_Field_Signature = "签名";
$i_StaffAttendance_Field_InSite = "进入站";
$i_StaffAttendance_Field_OutSite = "离开站";
$i_StaffAttendance_Status_NoRecord = "没有纪录";
$i_StaffAttendance_Field_ArrivalAndDepartureTime = "抵达及离开时间";

$i_StaffAttendance_Sim_BackSchool = "模拟拍卡 (返校)";
$i_StaffAttendance_Sim_LeaveSchool = "模拟拍卡 (离校)";

$i_StaffAttendance_Outing_Date = "外出日期";
$i_StaffAttendance_Outing_OutTime = "出发时间";
$i_StaffAttendance_Outing_BackTime = "返回时间";
$i_StaffAttendance_Outing_FromWhere = "出发地点";
$i_StaffAttendance_Outing_Location = "目的地";
$i_StaffAttendance_Outing_Objective = "原因";
$i_StaffAttendance_Outing_Remark = "备注";
$i_StaffAttendance_Outing_ImportFormat = "
<b><u>档案说明 : </u></b><br>
第一栏 : $i_UserLogin<br>
第二栏 : $i_StaffAttendance_Outing_Date <br>
第三栏 : $i_StaffAttendance_Outing_OutTime<br>
第四栏 : $i_StaffAttendance_Outing_BackTime<br>
第五栏 : $i_StaffAttendance_Outing_FromWhere<br>
第六栏 : $i_StaffAttendance_Outing_Location<br>
第七栏 : $i_StaffAttendance_Outing_Objective<br>
第八栏 : $i_StaffAttendance_Sim_BackSchool<br>
第九栏 : $i_StaffAttendance_Sim_LeaveSchool<br>
";

$i_StaffAttendance_DayShort="日";
$i_StaffAttendance_MonthShort="月";
$i_StaffAttendance_YearShort="年";

$i_StaffAttendance_Slot_AM = "上午";
$i_StaffAttendance_Slot_PM = "下午";
$i_StaffAttendance_Slot_WD = "整日";

$i_StaffAttendance_Leave_Date = "放假日期";
$i_StaffAttendance_Leave_Deduct_Salary = "扣薪";
$i_StaffAttendnace_Leave_TimeSlot = "时节";
$i_StaffAttendance_Leave_Type = "类别";
$i_StaffAttendance_Leave_Reason = "原因";
$i_StaffAttendance_Leave_Remark = "备注";
$i_StaffAttendance_LeaveType_AL = "年假";
$i_StaffAttendance_LeaveType_SL = "病假";
$i_StaffAttendance_LeaveType_CL = "补假";
$i_StaffAttendance_LeaveType_OL = "其他";
$i_StaffAttendance_LeaveType_AL_Short = "AL";
$i_StaffAttendance_LeaveType_SL_Short = "SL";
$i_StaffAttendance_LeaveType_CL_Short = "CL";
$i_StaffAttendance_LeaveType_OL_Short = "OL";
$i_StaffAttendance_Leave_ImportFormat = "
<b><u>档案说明 : </u></b><br>
第一栏 : $i_UserLogin<br>
第二栏 : $i_StaffAttendance_Leave_Date <br>
第三栏 : $i_StaffAttendnace_Leave_TimeSlot (AM, PM or WD)<br>
第四栏 : $i_StaffAttendance_Leave_Type (
$i_StaffAttendance_LeaveType_AL_Short - $i_StaffAttendance_LeaveType_AL
,$i_StaffAttendance_LeaveType_SL_Short - $i_StaffAttendance_LeaveType_SL
,$i_StaffAttendance_LeaveType_CL_Short - $i_StaffAttendance_LeaveType_CL
,$i_StaffAttendance_LeaveType_OL_Short - $i_StaffAttendance_LeaveType_OL)
<br>
第五栏 : $i_StaffAttendance_Leave_Reason<br>
第六栏 : $i_StaffAttendance_Sim_BackSchool<br>
第七栏 : $i_StaffAttendance_Sim_LeaveSchool<br>
";
$i_StaffAttendance_Result_ArchivedSummary = "已整存概览";
$i_StaffAttendance_Result_Total = "总数";
$i_StaffAttendance_Result_Ontime = "准时";
$i_StaffAttendance_Result_Absent = "缺席";
$i_StaffAttendance_Result_Late = "迟到";
$i_StaffAttendance_Result_EarlyLeave = "早退";

#########################
$i_StaffAttendance_Report_Staff_Duty = "当值";
$i_StaffAttendance_Report_Staff_Time="时间";
$i_StaffAttendance_Report_Staff_WaivedOnly="已撤回";
$i_StaffAttendance_Report_Staff_ValidOnly ="有效的";
$i_StaffAttendance_Report_Staff_Off = "休息/不需上班";

#########################

$i_StaffAttendance_ReportTitle_Weekly = "每星期出席资料表";
$i_StaffAttendance_ReportTitle_Daily = "每天出席资料表";
$i_StaffAttendance_ReportTitle_StaffMonthly = "职员每月报告";
$i_StaffAttendance_ReportTitle_Summary = "$i_StaffAttendance_Report_Summary";
$i_StaffAttendance_Report_Week = "一星期";
$i_StaffAttendance_Report_Month = "一个月";
$i_StaffAttendance_Report_Year = "一年";
$i_StaffAttendance_Report_SelfInput = "自行输入日期";

$i_StaffAttendance_ImportFormat_CardID = "时间, $i_SmartCard_Site, 智能卡 ID";
$i_StaffAttendance_ImportFormat_UserLogin = "时间, $i_SmartCard_Site, $i_UserLogin";
$i_StaffAttendance_ImportFormat_OfflineReader = "由离线拍卡机的资料档";
$i_StaffAttendance_ImportTimeFormat = "请把时间一项的格式设定为 YYYY-MM-DD HH:mm:ss";
$i_StaffAttendance_ImportConfirm = "如确定上例的纪录正确, 请按汇入以完成汇入.";
$i_StaffAttendance_ImportCancel = "如资料有误, 请按取消清除这些纪录.";
$i_StaffAttendance_ImportCardFormat = "
<b><u>档案说明 : </u></b><br>
第一栏 : $i_UserLogin<br>
第二栏 : $i_SmartCard_CardID <br>
第三栏 : $i_StaffAttendance_Group_ID <br>
";

$i_StaffAttendance_SelectAnother = "选择另一位职员";
#### New version
$i_StaffAttendance_SystemSettings_OtherSettings = "选项设定";

$i_StaffAttendance_DataManagement_DailyStatus = "每日状况";
$i_StaffAttendance_Group_InStation = "进入地点";
$i_StaffAttendance_Group_OutStation = "离开地点";

$i_StaffAttendance_DataManagement_LateList = "每日迟到名单";
$i_StaffAttendance_DataManagement_AbsentList = "每日缺席名单";
$i_StaffAttendance_DataManagement_EarlyLeaveList = "每日早退名单";
$i_StaffAttendance_DataManagement_SpecialWorkingPreset = "特别日工作设定";
$i_StaffAttendance_DataManagement_HolidayPreset = "假期设定";
$i_StaffAttendance_DataManagement_OutgoingPreset = "外出工作纪录";
$i_StaffAttendance_DataManagement_ReasonTypeSettings = "原因类型";
$i_StaffAttendance_DataManagmenet_IntermediateRecord = "工作中出入纪录";
$i_StaffAttendance_DataManagmenet_OTRecord="超时工作纪录";

$i_StaffAttendance_WordTemplate_Reason = "预设原因";

$i_StaffAttendance_GroupID = "组别号码";

##########################################
$i_StaffAttendance_Big5="繁";
$i_StaffAttendance_GB="简";
$i_StaffAttendance_IntermediateRecord_SelectView = "选择模式";
$i_StaffAttendance_IntermediateRecord_Date ="日期";
$i_StaffAttendance_IntermediateRecord_Staff ="职员";
$i_StaffAttendance_IntermediateRecord_Warn_Please_Select_Staff="请选择一位职员";
$i_StaffAttendance_IntermediateRecord_Warn_Please_Select_Day="请选择一个日期";
$i_StaffAttendance_IntermediateRecord_StaffName="职员名称";
$i_StaffAttendance_IntermediateRecord_RecordTime="纪录时间";
$i_StaffAttendance_OTRecord_WorkingRecord="工作纪录";
$i_StaffAttendance_OTRecord_Redeem="扣除";
$i_StaffAttendance_OTRecord_ConfirmRedeem="确定扣除?";
$i_StaffAttendance_OTRecord_RedeemRecord="扣除纪录";
$i_StaffAttendance_OTRecord_Outstanding="未扣除";
$i_StaffAttendance_OTRecord_RecordDate="记录日期";
$i_StaffAttendance_OTRecord_StartTime="开始时间";
$i_StaffAttendance_OTRecord_EndTime="结束时间";
$i_StaffAttendance_OTRecord_OTmins="超时工作分钟";
$i_StaffAttendance_OTRecord_RedeemDate="扣除日期";
$i_StaffAttendance_OTRecord_MinsRedeemed="扣除的分钟";
$i_StaffAttendance_OTRecord_Remark="备注";
$i_StaffAttendance_OTRecord_OutstandingOTmins="未扣除的分钟";
$i_StaffAttendance_OTRecord_DatePeriod="日期";
$i_StaffAttendance_OTRecord_DateFrom="由";
$i_StaffAttendance_OTRecord_DateTo="至";
$i_StaffAttendance_OTRecord_Warn_Please_Select_Range="请选择日期范围";
$i_StaffAttendance_OTRecord_Warn_Invalid_Date_Format="日期格式错误";
$i_StaffAttendance_OTRecord_TotalOTmins="总超时分钟";
$i_StaffAttendance_OTRecord_TotalMinsRedeemed="总扣除分钟";
$i_StaffAttendance_OTRecord_Warn_RedeemedMins_Out_Of_Range="扣除的分钟超出了范围";
$i_StaffAttendance_Special_Working_ViewALL="检视全部";
$i_StaffAttendance_Special_Working_ViewByDate="检视指定日期";
$i_StaffAttendance_Report_Roster="职员更表";

$i_StaffAttendance_OT_Mins_Ignored ="一般日子不当作超时工作的分钟";

###########################################
$i_StaffAttendance_ReasonTypeName = "原因类型名称";
$i_StaffAttendance_ReasonType = "原因类型";
$i_StaffAttendance_ReasonText="原因";
$i_StaffAttendance_Outgoing_Type = "外出类别";
$i_StaffAttendance_Warning_Select_OutgoingType="请选择外出类别";

$i_StaffAttendance_OutgoingType1 = "不计算迟到及早退";
$i_StaffAttendance_OutgoingType2 = "不计算迟到";
$i_StaffAttendance_OutgoingType3 = "不计算早退";

$i_StaffAttendance_OutgoingType[1] = "不计算迟到及早退";
$i_StaffAttendance_OutgoingType[2] = "不计算迟到";
$i_StaffAttendance_OutgoingType[3] = "不计算早退";

$i_StaffAttendance_WordTemplate_Reason = "预设原因";
$i_StaffAttendance_GroupID = "组别号码";

$i_StaffAttendance_Month = "月份";
$i_StaffAttendance_StaffType ="类别";

$i_staffAttendance_Date = "日期";
$i_staffAttendance_ShowBy = "显示类别";
$i_staffAttendance_ExportFileType = "报表格式";
$i_staffAttendance_DetailEntryLogRecord = "完整出入记录";
$i_staffAttendance_DailyEntryLogRecord = "每日出席记录表";
$i_staffAttendance_GroupSelection = "选择组别";
$i_staffAttendance_SelectAllGroup = "所有组别";
$i_staffAttendance_SelectNotInGroup = "不属任何组别";
$i_staffAttendance_PleaseSpecify = "请注明";
$i_staffAttendance_OtherReason = "其他原因";
$i_staffAttendance_GroupSelectionWarning = "请选择组别";

### Added On 16 Aug 2007 ###
$i_staffAttendance_SpecialWorking_ImportFormat = "$i_UserLogin, $i_StaffAttendance_Field_RecordDate, $i_StaffAttendance_Group_OnDuty, $i_StaffAttendance_Group_Start, $i_StaffAttendance_Group_End";
$i_staffAttendance_Holiday_ImportFormat = "$i_UserLogin, $i_StaffAttendance_Field_RecordDate, $i_StaffAttendance_ReasonTypeName, $i_StaffAttendance_ReasonText";
$i_staffAttendance_Outgoing_ImportFormat = "$i_UserLogin, $i_StaffAttendance_ReasonTypeName, $i_StaffAttendance_ReasonText, $i_StaffAttendance_Field_RecordDate, *$i_StaffAttendance_Outgoing_Type";
############################

### Added On 22 Aug 2007 ###
$i_staffAttendance_DateFormat_Warning = "请输入正确日期";
$i_staffAttendance_SelectType_Warning = "请选择类别";
$i_staffAttendance_TimeFormat_Warning = "请输入正确时间";
$i_staffAttendance_ImportDutyRecordDateFormat = "请把纪录日期一项的格式设定为 YYYY-MM-DD";
$i_staffAttendance_ImportDutyFormat = "请把需上班一项的格式设定为 Y/N";
$i_staffAttendance_IMportDutyStartFormat = "请把返工时间一项的格式设定为 HH:mm:ss";
$i_staffAttendance_ImportDutyEndFormat = "请把放工时间一项的格式设定为 HH:mm:ss";
$i_staffAttendance_OutgoingTypeFormat = "外出类别: <br><ol><li>不计算迟到及早退</ul><li>不计算迟到</ul><li>不计算早退</ul></ol>";
############################

### Added On 23 Aug 2007 ###
$i_staffAttendance_InvalidImportFormatWarning = "资料错误, 请检查档案再试";
$i_staffAttendance_Import_Row = "行数";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[1] = "内联网帐号 错误";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[2] = $i_StaffAttendance_Group_OnDuty." 错误";
$i_staffAttendance_SpecialWorking_Import_ErrorMsg[3] = $i_general_record_date."/".$i_StaffAttendance_Group_Start."/".$i_StaffAttendance_Group_End." 错误";
$i_staffAttendance_Holiday_Import_ErrorMsg[1] = "内联网帐号 错误";
$i_staffAttendance_Holiday_Import_ErrorMsg[2] = $i_general_record_date." 错误";
$i_staffAttendance_Holiday_Import_ErrorMsg[3] = $i_StaffAttendance_ReasonTypeName." 错误";
$i_staffAttendance_Holiday_Import_ErrorMsg[4] = $i_StaffAttendance_Holiday_warning;
$i_staffAttendance_Outgoing_Import_ErrorMsg[1] = "内联网帐号 错误";
$i_staffAttendance_Outgoing_Import_ErrorMsg[2] = $i_StaffAttendance_ReasonTypeName." 错误";
$i_staffAttendance_Outgoing_Import_ErrorMsg[3] = $i_general_record_date." 错误";
$i_staffAttendance_Outgoing_Import_ErrorMsg[4] = $i_StaffAttendance_Outgoing_Type." 错误";
$i_staffAttendance_Outgoing_Import_ErrorMsg[5] = $i_StaffAttendance_Outing_warning;
############################

### added on 17 Jun 2008 ###
$i_StaffAttendnace_ShowReportMethod = "检视模式";
$i_StaffAttendnace_ShowReportByHoldPage = "整页显示";
$i_StaffAttendnace_ShowReportByseparatePage = "分页显示";
###

### added on 29 Dec 2008 ###
$i_staffAttendance_all_group = "所有组别";
$i_staffAttendance_no_group = "不属任何组别";
###

$i_Circular_System = "职员通告系统";
$i_Circular_Circular = "职员通告";
$i_Circular_Settings = "职员通告设定";
$i_Circular_Admin = "职员通告管理人员";
$i_Circular_Settings_Disable = "不使用$i_Circular_System.";
$i_Circular_Settings_AllowHelpSigning = "容许管理人员替收件人更改回条.";
$i_Circular_Settings_AllowLateSign = "容许迟交.";
$i_Circular_Settings_AllowResign = "容许收件者更改已签回的回条.";
$i_Circular_Settings_DefaultDays = "预设签署期限日数.";
$i_Circular_Setting_TeacherCanViewAll = "让所有老师检视所有通告.";
$i_Circular_AdminLevel = "管理权限";
$i_Circular_AdminLevel_Normal = "一般权限";
$i_Circular_AdminLevel_Full = "完全控制";
$i_Circular_AdminLevel_Detail_Normal = "一般权限 (可发出职员通告及观看自己所发通告的结果)";
$i_Circular_AdminLevel_Detail_Full = "完全控制 (可发出职员通告及观看所有通告的结果)";
$i_Circular_Description_SetAdmin = "请选择管理人员及选取管理权限.";
$i_Circular_Management = "职员通告管理";
$i_Circular_MyIssueCircular = "我发出的职员通告";
$i_Circular_AllCircular = "所有职员通告";
$i_Circular_MyCircular = "我的职员通告";
$i_Circular_DateStart = "开始日期";
$i_Circular_DateEnd = "结束日期";
$i_Circular_CircularNumber = "通告编号";
$i_Circular_Title = "标题";
$i_Circular_Description = "内容";
$i_Circular_Issuer = "发出人";
$i_Circular_RecipientType = "适用对象类型";
$i_Circular_RecipientTypeAllStaff = "所有员工";
$i_Circular_RecipientTypeAllTeaching = "所有教学职务员工";
$i_Circular_RecipientTypeAllNonTeaching = "所有非教学职务员工";
$i_Circular_RecipientTypeIndividual = "个别有关员工/小组";
$i_Circular_Signed = "已签回";
$i_Circular_Total = "总数";
$i_Circular_ViewResult = "查看结果";
$i_Circular_NoRecord = "暂时没有通告";
$i_Circular_NotUseTemplate = "不使用范本";
$i_Circular_FromTemplate = "载入范本";
$i_Circular_RecipientIndividual = "个别员工";
$i_Circular_CurrentList = "现时通告清单";
$i_Circular_Attachment = "附加档案";
$i_Circular_SetReplySlipContent = "设定回条内容";
$i_Circular_ReplySlip = "回条";
$i_Circular_Type = "类别";
$i_Circular_TemplateNotes = "(只有 <font color=green>$i_Circular_Title</font>, <font color=green>$i_Circular_Description</font>, 和 <font color=green>$i_Circular_ReplySlip</font> 会成为范本的内容.)";
$i_Circular_ListIncludingSuspend = "包括已派发及未派发";
$i_Circular_CurrentCircular = "现时通告";
$i_Circular_PastCircular = "通告纪录";
$i_Circular_Alert_Sign = "你所填写的回条将被呈送. 确定签署通告并递交回条?";
# 20090326 yatwoon eService>eCircular
$i_Circular_Alert_Sign_AddStar = "你所填写的回条将被呈送及加上星号标记. 确定签署通告并递交回条?";
$i_Circular_Alert_UnStar = "确定取消这职员通告的星号标记?";
$i_Circular_Alert_AddStar = "确定加上这职员通告的星号标记?";

$i_Circular_Sign = "签署";
$i_Circular_SignStatus = "签署状况";
$i_Circular_Unsigned = "未签";
$i_Circular_Signer = "签署人:";
$i_Circular_Editor = "代签署人:";
$i_Circular_SignerNoColon = "签署人";
$i_Circular_SignedAt = "签署时间";
$i_Circular_Recipient = "收件人";
$i_Circular_SignInstruction = "请填妥以上回条, 再按签署.";
$i_Circular_SignInstruction2 = "你可以更改以上回条的内容. 请于更改后按签署.";
$i_Circular_Sign = "签署";
$i_Circular_Option = "选项";
$i_Circular_NoTarget = "此通告并没有派发给任何职员.";
$i_Circular_ViewResult = "检视结果";
$i_Circular_TableViewReply = "表列回条内容";
$i_Circular_NoReplyAtThisMoment = "暂时没有已签回条";
$i_Circular_ViewStat = "统计资料";
$i_Circular_NumberOfSigned = "已签回人数";
$i_Circular_RemovalWarning = "注意: 移除此通告会一并移除已交回的回条. 你确定要继续?";
$i_Circular_Alert_DateInvalid = "签署日期必须是或迟于发出日期";
$i_Circular_NotExpired = "未过期";
$i_Circular_Expired = "已过期";
$i_Circular_AllSigned = "全部已签回";
$i_Circular_NotAllSigned = "未全部签回";
############ New wordings for eClass IP ###############
$i_Circular_Select_Template = "选择范本";
$i_Circular_Select_Recipient_Type = "选择适用对象类型";
$i_Circular_Add_Circular = "增加通告";
$i_Circular_New = "新增通告";
$i_Circular_Edit = "编辑通告";

# 20090326 yatwoon eCircular Stared function
//职员通告
$eCircular["StarredCirculars"] = "有星号标记";
$eCircular["SignClose"] = "签署及关闭视窗";
$eCircular["SignAddStar"] = "签署及加上星号标记";
$eCircular["RemoveStar"] = "移除星号标记";
$eCircular["AddStar"] = "加上星号标记";
#######################################################

$i_CycleNew_Menu_DefinePeriods = "定义日期范围";
$i_CycleNew_Menu_GeneratePreview = "产生预览";
$i_CycleNew_Menu_CheckPreview = "检查预览";
$i_CycleNew_Menu_MakePreviewToProduction = "把预览转至使用";
$i_CycleNew_Alert_GeneratePreview = "你是否确定要把现时预览清除, 再产生新预览?";
$i_CycleNew_Alert_RemoveConfirm = "你确定要移除这个纪录?";
$i_CycleNew_Alert_RemoveAllConfirm = "你确定要移除所有纪录?";
$i_CycleNew_Alert_MakeToProduction = "你是否确定要把现时预览转至使用?";
$i_CycleNew_Prompt_DateRangeOverlapped = "各日期不能重迭.";
$i_CycleNew_Prompt_PreviewGenerated = "已产生预览. 请检查预览后才转至使用.";
$i_CycleNew_Prompt_ProductionMade = "已转成使用.";
$i_CycleNew_Field_PeriodStart = "开始日期";
$i_CycleNew_Field_PeriodEnd = "结束日期";
$i_CycleNew_Field_PeriodType = "类型";
$i_CycleNew_Field_CycleType = "循环日类型";
$i_CycleNew_Field_PeriodDays = "循环日数";
$i_CycleNew_Field_SaturdayCounted = "计算星期六";
$i_CycleNew_Field_FirstDay = "初始日";
$i_CycleNew_Field_Date = "日期";
$i_CycleNew_Field_TxtChi = "中文显示";
$i_CycleNew_Field_TxtEng = "英文显示";
$i_CycleNew_field_TxtShort = "日历显示";
$i_CycleNew_PeriodType_NoCycle = "不计算循环日";
$i_CycleNew_PeriodType_Generation = "一般计法";
$i_CycleNew_PeriodType_FileImport = "档案汇入";
$i_CycleNew_CycleType_Numeric = "数字 (1, 2, 3 ...)";
$i_CycleNew_CycleType_Alphabetic = "字母 (A, B, C ...)";
$i_CycleNew_CycleType_Roman = "罗马数字 (I, II, III ...)";
$i_CycleNew_Description_FileImport = "<u><b>档案说明:</b></u><br>
第一栏: 日期 (YYYY-MM-DD)<br>
第二栏: 中文介面的显示 (如: 循环日 A)<br>
第三栏: 英文介面的显示 (如: Day A)<br>
第四栏: 日历介面的显示及定期资源预订等功能使用 (如: A)<br>
";
$i_CycleNew_Warning_FileImport = "在此日期内的汇入纪录会被清除.";
$i_CycleNew_View_ImportRecords = "检视上次汇入的纪录";
$i_CycleNew_Export_FileImport = "下载可汇入格式";
$i_CycleNew_Prefix_Chi = "循环日 ";
$i_CycleNew_Prefix_Eng = "Day ";
$i_CycleNew_NumOfMonth = "显示月份数目";

$i_Discipline_System = "操行纪录系统";
$i_Discipline_System_Approval = "批核";
$i_Discipline_System_PunishCounselling = "留堂及操分调整";
$i_Discipline_System_Report = "报告";
$i_Discipline_System_Stat = "统计";
$i_Discipline_System_SchoolRules = "校规";
$i_Discipline_System_Settings = "基本设定";
$i_Discipline_System_Accumulative = "累积违规";
$i_Discipline_System_Settings_SchoolRule = "设定校规";
$i_Discipline_System_Settings_Merit = "奖励项目及类别";
$i_Discipline_System_Settings_Punishment = "违规项目及类别";
$i_Discipline_System_Settings_MildPunishment = "轻微违规纪录";
$i_Discipline_System_Settings_MeritType = "优点类型及批核方式";
$i_Discipline_System_Settings_DemeritType = "缺点类型及批核方式";
$i_Discipline_System_Settings_UpgradeRule = "累积升级规则";
$i_Discipline_System_Settings_SpecialPunishment = "特别处罚项目";
$i_Discipline_System_Settings_Conduct = "操行分使用规则";
$i_Discipline_System_Settings_Subscore1 =  "勤学分使用规则";
$i_Discipline_System_Settings_Calculation = "操行分计算";
$i_Discipline_System_Settings_Calculation_Conduct_Semester = "学期操行等级";
$i_Discipline_System_Settings_Calculation_Conduct_Annual = "全年操行分及等级";
$i_Discipline_System_Approval = "操行纪录批核";

$i_Discipline_System_RuleCode = "校规编号";
$i_Discipline_System_RuleName = "校规";
$i_Discipline_System_UpgradeRule = "升级条件";
$i_Discipline_System_NeedApproval = "需要批核数目";
$i_Discipline_System_ApprovalLevel = "批核的级别";
$i_Discipline_System_WaivedBy_Prefix = "由";
$i_Discipline_System_WaivedBy_Subfix = "豁免";
$i_Discipline_System_UnwaivedBy_Prefix = "由";
$i_Discipline_System_UnwaivedBy_Subfix = "取消豁免";
$i_Discipline_System_Admin_Level1 = "训导老师";
$i_Discipline_System_Admin_Level2 = "系统管理员";
$i_Discipline_System_Category_Merit = "奖励类型";
$i_Discipline_System_Category_Punish = "违规类型";
$i_Discipline_System_CategoryName = "类别名称";
$i_Discipline_System_Item_Merit = "奖励项目";
$i_Discipline_System_Item_Punish = "违规项目";
$i_Discipline_System_ItemName = "项目名称";
$i_Discipline_System_ItemCode = "项目编号";
$i_Discipline_System_Item_Code_Name = "项目编号/名称";
$i_Discipline_System_Item_Code_Item_Name = "项目编号/项目名称";
$i_Discipline_System_ConductScore = "操行分";
$i_Discipline_System_NumOfMerit = "优点 数目/类型";
$i_Discipline_System_NumOfPunish = "缺点 数目/类型";
$i_Discipline_System_SpecialPunishment = "特别处罚";
$i_Discipline_System_DetentionMinutes = "留堂时间 (分钟)";
$i_Discipline_System_InputAward = "输入奖励纪录";
$i_Discipline_System_InputPunishment = "输入违规纪录";
$i_Discipline_System_Add_Merit = "记功 ";
$i_Discipline_System_No_Of_Award_Punishment = "奖惩数量";
$i_Discipline_System_Quantity = "数量";
$i_Discipline_System_Add_Demerit = "记过";
$i_Discipline_System_Add_Merit_Demerit = "奖励/罚则";
$i_Discipline_System_Event_Merit = "奖励事项";
$i_Discipline_System_Event_Demerit = "违规事项";
$i_Discipline_System_ContactReq_Parent = "需会见家长";
$i_Discipline_System_ContactReq_Social = "需会见社工";
$i_Discipline_System_Contact_Date = "会面日期";
$i_Discipline_System_Contact_Time = "会面时间";
$i_Discipline_System_Contact_Venue = "地点";
$i_Discipline_System_Contact_PIC = "负责人";
$i_Discipline_System_Illegal_Character = "包含系统不接受的符号: /";



$i_Discipline_System_Description_Approval = "奖励/惩罚相等或大于此数目将需要批核";
$i_Discipline_System_Message_SaveRequired = "由于其他设定已变更, 请再存储一次";
$i_Discipline_System_general_increment = "增加";
$i_Discipline_System_general_decrement = "扣减";
$i_Discipline_System_general_date_merit = "事件日期"; # "奖励日期";
$i_Discipline_System_general_date_demerit = "事件日期"; # "惩处日期";
$i_Discipline_System_general_record = "纪录项目";
$i_Discipline_System_general_remark = "备注";
$i_Discipline_System_default = "通用时段";
$i_Discipline_System_customize = "类别专用时段";
$i_Discipline_System_period_setting = "使用时段";
$i_Discipline_System_new_period_setting = "新增晋段";
$i_Discipline_System_Field_Times = "超过次数";
$i_Discipline_Ssytem_Category_Item_Not_Selected = "尚未选择纪录类别或项目.";


$i_Discipline_System_Control_LevelSetting = "设定用户分级名称";
$i_Discipline_System_Control_UserACL = "指定用户的使用级别";
$i_Discipline_System_Control_ItemLevel = "可用功能级别设定";
$i_Discipline_System_Control_AttendanceSettings = "与学生智能卡考勤的连结";
$i_Discipline_System_Control_Attend_YearSemester = "设定累计学年/学期";
$i_Discipline_System_Control_Attend_CalculationRule = "计算规则";

$i_Discipline_System_Control_Warning_LevelSetting = "第一步：设定用户级别名称";
$i_Discipline_System_Control_Warning_ItemLevel = "第二步：设定可用功能级别";
$i_Discipline_System_Control_Warning_UserACL = "第三步：设定指定用户的使用级别";


$i_Discipline_System_Field_Type = "类别";
$i_Discipline_System_Field_AdminLevel = "级别";
$i_Discipline_System_Field_Personal = "个人";

$i_Discipline_System_NumOfLevel = "级别数目";
$i_Discipline_System_alert_NumLevelLess = "你选择的数目少于现时的级别数目, 较高的级别将被移除. 是否确定要更改?";
$i_Discipline_System_alert_NeedToSave = "请注意: 新设定尚未储存!";
$i_Discipline_System_alert_ChangeAdminLevel = "你是否确定要更改已选择用户的权限?";
$i_Discipline_System_alert_RecordAddSuccessful = "已新增纪录";
$i_Discipline_System_alert_RecordAddFail = "新增纪录失败";
$i_Discipline_System_alert_RecordModifySuccessful = "已更新纪录";
$i_Discipline_System_alert_RecordModifyFail = "杆新纪录失败";
$i_Discipline_System_alert_RecordWaitForApproval = "已新增纪录, 并等待批核.";
$i_Discipline_System_alert_PleaseSelectStudent = "请选择学生";
$i_Discipline_System_alert_PleaseSelectClass = "请选择班别";
$i_Discipline_System_alert_PleaseSelectClassLevel = "请选择级别";
$i_Discipline_System_alert_PleaseSelectDemeritRecord = "请选择违规纪录";
$i_Discipline_System_alert_PleaseSelectDetentionRecord = "请选择原因";
$i_Discipline_System_alert_PleaseSelectEvent = "请选择项目";
$i_Discipline_System_alert_PleaseSelectMeritRecord = "请选择奖励纪录";
$i_Discipline_System_alert_PleaseInsertNewConductScore = "请填写新操行分";
$i_Discipline_System_alert_PleaseInsertNewSubscore1 = "请填写新勤学分";
$i_Discipline_System_alert_remove_warning_level = "你是否确定要移除此纪录?";
$i_Discipline_System_alert_grading_scheme_already_exists = "<font color=red>此操行等级规则已存在</font>";
$i_Discipline_System_alert_warning_level_already_exists = "<font color=red>此提示已存在</font>";
$i_Discipline_System_alert_warning_level_not_exists = "<font color=red>此提示并不存在</font>";
$i_Discipline_System_alert_reset_conduct = "你是否确定要重设所有学生的操行分?";
$i_Discipline_System_alert_ApprovedSome = "<font color=red>部份纪录已批核, 但仍有部份你没有足够权限批核</font>";
$i_Discipline_System_alert_Approved = "<font color=green>纪录已批核</font>";
$i_Discipline_System_alert_calculate = "你是否确定要进行计算?";
$i_Discipline_System_alert_SchoolYear = "请填写年度";
$i_Discipline_System_alert_remove_category = "此类型所包含的项目将同时被删除, 你是否确定要删除项目？";
$i_Discipline_System_alert_remove_record = "所有相关纪录将一同被删除。你是否确定要删除？";
$i_Discipline_System_normal_teacher = "一般老师/职员";
$i_Discipline_System_alert_require_group_title = "请输入小组名称";
$i_Discipline_System_alert_exist_group_title = "已有该小组名称";
$i_Discipline_System_alert_Award_Punishment_Missing = "你选择了不对所选学生进行奖励或惩罚。你是否确定要继续?";

$i_Discipline_System_msg_GoBackPersonalRecord = "回到个人纪录";
$i_Discipline_System_msg_AddAnotherRecord = "新增另一项纪录";
$i_Discipline_System_msg_ModifyAnotherRecord = "更新更多纪录";
$i_Discipline_System_msg_ModifyOtherRecord = "更新其他纪录";
$i_Discipline_System_msg_GoBackApproval = "返回批核目录";
$i_Discipline_System_field_discipline_record = "操行纪录";
$i_Discipline_System_Report_Type_Personal = "个人报告";
$i_Discipline_System_Report_Type_AwardPunish = "奖励/违规纪录";
$i_Discipline_System_Report_Type_Class = "班别摘要";
$i_Discipline_System_Report_Type_PunishCounselling = "处罚及辅导报告";
$i_Discipline_System_Report_Type_Assessment = "评估项目";
$i_Discipline_System_Report_Type_Detail = "详细报告";
$i_Discipline_System_Report_Personal_PersonalRecord = "个人纪录";
$i_Discipline_System_Report_Personal_ConductAdjustment = "操行纪录调整";
$i_Discipline_System_Report_AwardPunish_ClassLevel = "级别纪录";
$i_Discipline_System_Report_AwardPunish_ClassRecord = "班别纪录";
$i_Discipline_System_Report_AwardPunish_TodayRecord = "每天奖励/违规纪录";
$i_Discipline_System_Report_PunishCounselling_ParentMeeting = "家长会议";
$i_Discipline_System_Report_PunishCounselling_SocialMeeting = "辅导工作";
$i_Discipline_System_Report_PunishCounselling_PunishReport = "处罚报告";
$i_Discipline_System_Report_PunishCounselling_DetentionReport = "留堂报告";
$i_Discipline_System_Report_Assessment_Assessment = "评估项目";
$i_Discipline_System_Report_Detail_Student = "班别学生详细报告";
$i_Discipline_System_Report_Detail_CountReport = "违规次数报告";
$i_Discipline_System_Report_Class_Total = "全班合计";
$i_Discipline_System_Field_Total = "总数";
$i_Discipline_System_Stat_Type_General = "班别操行统计";
$i_Discipline_System_Stat_Type_Award = "奖励统计";
$i_Discipline_System_Stat_Type_Detention = "留堂统计";
$i_Discipline_System_Stat_Type_Discipline = "违规统计";
$i_Discipline_System_Stat_Type_Punishment = "处罚统计";
$i_Discipline_System_Stat_Type_Others = "其他统计";
$i_Discipline_System_Stat_General_ClassLevel = "级别统计";
$i_Discipline_System_Stat_General_Class = "班别统计";
$i_Discipline_System_Stat_General_Detail = "学生个别操行统计";
$i_Discipline_System_Stat_Award_ClassLevel = "级别统计";
$i_Discipline_System_Stat_Award_Class = "班别统计";
$i_Discipline_System_Stat_Detention_Reason_ClassLevel = "级别 - 原因统计";
$i_Discipline_System_Stat_Detention_Reason_Class = "班别 - 原因统计";
$i_Discipline_System_Stat_Detention_Subject_ClassLevel = "级别 - 科目统计";
$i_Discipline_System_Stat_Detention_Subject_Class = "班别科目统计";
$i_Discipline_System_Stat_Discipline_ClassLevel = "级别统计";
$i_Discipline_System_Stat_Discipline_Class = "班别统计";
$i_Discipline_System_Stat_Punishment_ClassLevel = "级别统计";
$i_Discipline_System_Stat_Punishment_Class = "班别统计";
$i_Discipline_System_Stat_Others_Top_Punish_Student = "违规最多的学生";
$i_Discipline_System_Stat_Others_Top_Punish_Class = "违规最多的班别";
$i_Discipline_System_Stat_Others_Top_Award_Student = "奖励最多的学生";
$i_Discipline_System_Stat_Others_Top_Award_Class = "奖励最多的班别";
$i_Discipline_System_PunishCouncelling_Type_PunishmentFollowup = "跟进今天的处罚事宜";
$i_Discipline_System_PunishCouncelling_Type_DetentionFollowup = "跟进今天的留堂事宜";
$i_Discipline_System_PunishCouncelling_Type_MeetingFollowup = "跟进今天的会面事宜";
$i_Discipline_System_PunishCouncelling_Type_ExportNotice = "汇出通告";
$i_Discipline_System_PunishCouncelling_Punishment_NotExecuted = "未执行处罚";
$i_Discipline_System_PunishCouncelling_Detention_TodayDetentionRecord = "今天须留堂纪录";
$i_Discipline_System_PunishCouncelling_Detention_Attendance = "留堂出席点名";
$i_Discipline_System_PunishCouncqlling_Detention_Outstanding_Minutes = "未留堂时间";
$i_Discipline_System_PunishCouncelling_Detention_This_Minutes = "本次留堂时间";
$i_Discipline_System_PunishCouncelling_Detention_Past_Minutes = "已留堂时间";

$i_Discipline_System_Detention_Calentar_Instruction_Msg = "行事历显示提供本月留堂课节的概榄。你可在此为尚未编排留堂的学生编排留堂课节。";
$i_Discipline_System_Student_Detention_Calentar_Instruction_Msg = "行事历显示你本月的留堂安排情况。";
$i_Discipline_System_Detention_List_Instruction_Msg = "留堂课节列表显示留堂课节之详细资讯。你可在此为尚未编排留堂的学生编排留堂时段。";
$i_Discipline_System_Detention_Student_Instruction_Msg = "学生列表显示学生之留堂安排情况。你可在此编排学生之留堂时段和地点。";


$i_Discipline_System_PunishCouncelling_Detention_Attendance_Manual = "留堂出席点名(人手)";
$i_Discipline_System_PunishCouncelling_Detention_New = "新增留堂纪录";
$i_Discipline_System_PunishCouncelling_Meeting_Parent = "今天的家长会议";
$i_Discipline_System_PunishCouncelling_Meeting_Social = "今天的辅导工作";
$i_Discipline_System_PunishCouncelling_ExportNotice_Punishment = "尚未列印违规通告";
$i_Discipline_System_PunishCouncelling_ExportNotice_Award = "尚未列印奖励通告";
$i_Discipline_System_PunishCouncelling_ExportNotice_Meeting = "尚未列印会面通告";
$i_Discipline_System_PunishCouncelling_ExportNotice_Detention = "尚未列印留堂通告";
$i_Discipline_System_PunishCouncelling_Conduct = "更改操行分";
$i_Discipline_System_PunishCouncelling_Conduct_ManualAdjustment = "手动调整操行分";

$i_Discipline_System_PunishCouncelling_Detention_SetMinutes = "设定为出席全部时间";
$i_Discipline_System_PunishCouncelling_Detention_SetAllMinutes = "设定为所有学生出席全部时间";

$i_Discipline_System_Count_Option = "迟到计算选项";
$i_Discipline_System_Count_Semester = "只计算同一学年及同一学期";
$i_Discipline_System_Count_Year = "只计算同一学年";
$i_Discipline_System_Count_All = "计算所有纪录";
$i_Discipline_System_LateUpgrade_Rule_AddNew = "增加下一项";
$i_Discipline_System_LateUpgrade_Accumulated = "累计迟到次数";
$i_Discipline_System_LateUpgrade_Next = "若再迟到次数";
$i_Discipline_System_LateUpgrade_Detention_Minutes = "留堂(分钟)";

$i_Discipline_System_Report_DateSelected = "已选择日期";
$i_Discipline_System_Report_thisYear = "本年度";
$i_Discipline_System_Report_thisSem = "本学期";
$i_Discipline_System_Report_DetailType_Demerit = "违规纪录";
$i_Discipline_System_Report_DetailType_Merit = "奖励纪录";
$i_Discipline_System_Report_DetailType_Merit_Demerit = "奖励/违规纪录";
$i_Discipline_System_Report_DetailType_SpecialPunishment = "特别处罚纪录";
$i_Discipline_System_Report_DetailType_Detention = "留堂出席纪录";
$i_Discipline_System_Report_DetailType_Punishment_Detention = "留堂处罚纪录";
$i_Discipline_System_Report_DetailType_Meeting = "会面纪录";
$i_Discipline_System_Report_ClassReport_Grade = "操行等级";
$i_Discipline_System_general_SortField = "排列次序";

$i_Discipline_System_Approval_Demerit_Waiting = "等待批核的违规纪录";
$i_Discipline_System_Approval_Merit_Waiting = "等待批核的奖励纪录";
$i_Discipline_System_Approval_Amount = "数量";
$i_Discipline_System_Approval_View = "检视";
$i_Discipline_System_Waiting_Approval_Event = "等待批核事项";


$i_Discipline_System_Conduct_Initial_Score = "底分";
$i_Discipline_System_Conduct_Warning_Point = "提示分数";
$i_Discipline_System_Conduct_List_ConductScore_Dropped = "以下学生的操行分已跌至警告水平";
$i_Discipline_System_Conduct_DroppedPassWarningPoint = "跌至警告水平";
$i_Discipline_System_Conduct_UpdatedScore = "更旸后操行分";
$i_Discipline_System_Conduct_ShowNotice = "显示家长信";
$i_Discipline_System_Conduct_Print_Remark = "列印时连同备注";
$i_Discipline_System_Conduct_ResetAllStudents = "重设所有学生的操行分(本学期)";
$i_Discipline_System_Conduct_ManualAdjustment = "手动设定";
$i_Discipline_System_Conduct_CurrentScore = "现时操行分";
$i_Discipline_System_Subscore1_CurrentScore = "现时勤学分";
$i_Discipline_System_Conduct_ScoreDifference = "已加/减操行分";
$i_Discipline_System_Conduct_UpdatedScore = "已更新操行分";
$i_Discipline_System_Subscore1_ScoreDifference = "已加/减勤学分";
$i_Discipline_System_Conduct_AdjustTo = "调整至";
$i_Discipline_System_Conduct_ConductGradeRule = "操行等级规则";
$i_Discipline_System_Conduct_ConductGradeRule_MinScore = "操行分下限";
$i_Discipline_System_Conduct_ConductGradeRule_CalculatedGrade = "对应等级";
$i_Discipline_System_Calculation_ScoreRatio = "计算比例";

$i_Discipline_System_Subscore1 = "勤学分";
$i_Discipline_System_Subscore1_ResetAllStudents = "重设所有学生的勤学分 (本学期)";
$i_Discipline_System_alert_reset_subscore1 = "你是否确定要重设所有学生的勤学分?";
$i_Discipline_System_Settings_Calculation_SubScore1_Annual = "全年勤学分";
$i_Discipline_System_Subscore1_UpdatedScore = "更新后的勤学分";
$i_Discipline_System_Subscore1_List_SubScore_Dropped = "以下学生的勤学分已跌至警告水平";
$i_Discipline_System_PunishCouncelling_Subscore1_ManualAdjustment = "手动调整勤学分";

# added on 31 Jan 2008
$i_Discipline_System_Notice_RecordType = "种类";
$i_Discipline_System_Notice_RecordType1 = "家长信";
$i_Discipline_System_Notice_RecordType2 = "警告信";
#################

# added on 4 Feb 2008
$i_Discipline_System_Notice_Date = "日期";
$i_Discipline_System_Notice_Pending = "未发布家长信";
$i_Discipline_System_Notice_Show = "显示";
$i_Discipline_System_Notice_Remove = "移除";
$i_Discipline_System_Notice_Warning_Please_Select = "请选择最少一项纪录";
$i_Discipline_System_Notice_Alert_Remove_Notice = "请列印已选取家长信内容，此家长信纪录将会被移除";
####################

$i_Discipline_System_Notice_AttachItem = "附加扣分事项";
$i_Discipline_System_Notice_Following = "以下是";
$i_Discipline_System_Notice_Demerit_Receord = "的违规纪录";
$i_Discipline_System_Option_SameEvent = "同一事件";
$i_Discipline_System_Option_DifferentEvent = "不同事件";

$i_Discipline_System_Select_Student = "选择学生";
$i_Discipline_System_Add_Record = "为学生增加纪录";
$i_Discipline_System_Over = "超过";
$i_Discipline_System_Times = "次";
$i_Discipline_System_Conduct_Change_Log ="操行分更改纪录";
$i_Discipline_System_SubScore1_Change_Log ="勤学分更改纪录";

$i_Discipline_System_FromScore="分数(由)";
$i_Discipline_System_ToScore ="分数(至)";

$i_Discipline_System_New_MeritDemerit="新增纪录";
$i_Discipline_System_Edit_MeritDemerit="更新纪录";
$i_Discipline_System_Remove_MeritDemerit="移除纪录";
$i_Discipline_System_Record_Already_Removed="纪录已被移除";
$i_Discipline_System_Manual_Adjustment="手动调整";
$i_Discipline_System_System_Revised="系统修正";
$i_Discipline_System_Previous_Student="上一个学生";
$i_Discipline_System_Next_Student="下一个学生";
$i_Discipline_System_Print_WarningLetter="列印警告信";
$i_Discipline_System_WaiveStatus ="豁免状况";
$i_Discipline_System_WaiveStatus_Waived ="豁免";
$i_Discipline_System_WaiveStatus_Waived_Prefix = "其中";
$i_Discipline_System_WaiveStatus_Waived_Subfix = "个纪录已豁免";
$i_Discipline_System_WaiveStatus_NoWaived="没有参加计划";
$i_Discipline_System_WaiveStatus_InProgress="进行中";
$i_Discipline_System_WaiveStatus_Success="完成";
$i_Discipline_System_WaiveStatus_Fail="失败";
$i_Discipline_System_WaiveStatus_ChangeStatus_Warning="重新挑战豁免计划，上一次的失败纪录将被覆盖.继续?";
$i_Discipline_System_RecordNotYet_Arppoved ="无法显示家长信.纪录尚待批核中.";
$i_Discipline_System_Waive_NoUpdate_Warning="你已选择豁免此纪录，已更改的资料(除备注及豁免日期外)将不会被储存";
$i_Discipline_System_Set_Status_Warning="请选择$i_Discipline_System_WaiveStatus";
$i_Discipline_System_Show_WaiveStatus="显示豁免纪录";
$i_Discipline_System_No_WaiveStatus="隐藏豁免纪录";
$i_Discipline_System_WaiveStartDate="豁免开始日期";
$i_Discipline_System_WaiveEndDate="豁免结束日期";
$i_Discipline_System_Waive_LostDateInfo_Warning="选择".$i_Discipline_System_WaiveStatus_NoWaived."将失去全部旧有的豁免日期资料";
$i_Discipline_System_Waive_LostEndDateInfo_Warning="选择".$i_Discipline_System_WaiveStatus_InProgress."将失去旧有的豁免结束日期资料";
$i_Discipline_System_Please_Select_Record = "请选择要显示的纪录。";

# added on 17 Jan 2008 #
$i_Discipline_System_Frontend_menu_eDiscipline = "操行纪录";

# added on 29 Jan 2008 #
$i_Discipline_System_Conduct_KeepNotice = "储存家长信";
########################

$iDiscipline['By'] = "由";
$iDiscipline['Period_Start'] = "由";
$iDiscipline['Period_End'] = "至";
$iDiscipline['Period_SchoolYear'] = "对应".$i_Profile_Year;
$iDiscipline['Period_Semester'] = "对应".$i_Profile_Semester;
$iDiscipline['Accumulative_Category'] = "类别";
$iDiscipline['Accumulative_Category_EditItem'] = "编辑项目";
$iDiscipline['Accumulative_Category_EditPeriod'] = "编辑时段设定";
$iDiscipline['Accumulative_Category_Name'] = "类别名称";
$iDiscipline['Accumulative_DefaultCat_Late'] = "迟到";
$iDiscipline['Accumulative_DefaultCat_Homework'] = "欠交功课";
$iDiscipline['Accumulative_DefaultCat_Uniform'] = "校服仪容违规";
$iDiscipline['Accumulative_Category_Item_Name'] = "项目名称";
$iDiscipline['Accumulative_Category_Homework_Intranet_Homework_List']="Intranet 科目";
$iDiscipline['Accumulative_Period_Overlapped_Warning']="各时段不能重迭";
$iDiscipline['Accumulative_Period_InUse_Warning']="无法删除纪录。所选之时段中，某些时段已被使用。";
$iDiscipline['Accumulative_Homework_ChangeList_Warning']="新增".$i_Homework_subject."将停止使用".$iDiscipline['Accumulative_Category_Homework_Intranet_Homework_List'];
$iDiscipline['Accumulative_Demerit_Times']="累积违规次数";
# added on 01 Sept 2008
$iDiscipline['Accumulative_Merit_Times']="累积奖励次数";

$iDiscipline['Accumulative_Punishment']="惩罚";
$iDiscipline['Period']="时段";
$iDiscipline['Accumulative_Category_Period']="计算".$iDiscipline['Period'];
$iDiscipline['Accumulative_Category_Select_Period']="选择时段";
$iDiscipline['Accumulative_Category_Warning_Select_Period']="请选择时段";
$iDiscipline['Accumulative_Category_Warning_Invalid_Accumulated_Demerit']=$iDiscipline['Accumulative_Demerit_Times']."无效";
$iDiscipline['Accumulative_Select_Category']="选择类别";
$iDiscipline['Accumulative_Please_Select_Category']="请选择".$iDiscipline['Accumulative_Category'];
$iDiscipline['SetAll']="设定给全部";
$iDiscipline['GeneralSettings']="一般设定";
$iDiscipline['Accumulative_Punishment_Warning_Select_Category']="请选择".$iDiscipline['Accumulative_Category'];
$iDiscipline['Accumulative_Punishment_Warning_Select_Item']="请选择".$iDiscipline['Accumulative_Category_Item'];
$iDiscipline['Accumulative_Punishment_Warning_Select_Subject']="请选择".$i_Homework_subject;
$iDiscipline['Warning_Enter_Integer'] ="分数必须为整数";
$iDiscipline['Accumulative_Punishment_Report']="累积违规纪录";
# added on 3 Sept 2008
$iDiscipline['Accumulative_Reward_Report']="累积奖励纪录";

$iDiscipline['Accumulative_Punishment_No_Category_Period_Setting']="no linked period";
$iDiscipline['Accumulative_Period_In_Use_Warning']= "该时段已有违规纪录，新设定将只对往后新增之纪录有效。";
$iDiscipline['Accumulative_Category_EditPeriod_UseRule'] ="编辑时段设定 (使用升级条件)";
$iDiscipline['Accumulative_Category_NextNumDemerit']="再违规次数";
$iDiscipline['Outdated_Page_Waring']="此版面已停用, 请前往 学校行政管理工具>训导管理>设定 进行有关设定。";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme']="计算方法";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_Details']="计算方法细节";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1']="固定方式";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_2']="累加方式";
$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_Undefined']="未设定";
$iDiscipline['Accumulative_Category_Period_Select_Calculation_Scheme']="选择".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme'];
$iDiscipline['Accumulative_Category_Selected_Period']="已选择时段";
$iDiscipline['Accumulative_Category_Period_Set_Details']="设定计算细节";
$iDiscipline['Accumulative_Category_Period_Set_Details_Warning']="必需填写至少一种处罚方式.";
$iDiscipline['Accumulative_Category_Period_Set_Details_Warning_2']="必需填写至少一种处罚方式.";
$iDiscipline['Management_Child_AccumulativePunishmentAlert']="累积违规通知";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_NoNeed_Alert']="不需提示";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_NA']="不适用";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_AlertCount']="累积违规次数到达以下数目便需提示";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_DateRange_Part1']="检视自";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Notice']="通知书";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendEmailCampusmail']="传送电子邮件/校园电邮";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendEmail']="传送电子邮件给";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_SendCampusmail']="传送「校园电邮」给";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Show_TeacherNotice']="班主任通知书";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Show_ParentNotice']="显示家长通知书";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Select_Recipient']="请选择收件人";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_Mail_Sent']="邮件已传送";
$iDiscipline['Reports_Child_AccumulativePunishmentReport']="累积违规报告";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_PrintAll']="列印所有班别";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Total']="共";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Item']="次";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_BlackMarkCount']="缺点次数";
$iDiscipline['Reports_Child_AccumulativePunishmentReport_Sum']="总计";
$i_Discipline_System_Merit = "记功 ";
$i_Discipline_System_Demerit = "记过";
$iDiscipline['Reports_Child_AccumulativePunishmentClassReport']="班级累积违规(".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1'].")总表";
$iDiscipline['Reports_Child_AccumulativePunishmentCategoryReport']="每天班级累积违规报告";
$iDiscipline['AccumulativePunishment_Preset_Category']="附有「<span class='tabletextrequire'>*</span>」的项目为系统预设类别，不可删除。";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning1']="注意：更改预设类别之显示名称并不影响其功能。系统仍会将此类别下的违规事项视为「迟到」处理。";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning2']="注意：更改预设类别之显示名称并不影响其功能。系统仍会将此类别下的违规事项视为「欠交功课」处理。";
$iDiscipline['AccumulativePunishment_Preset_Category_Edit_Warning3']="注意：更改预设类别之显示名称并不影响其功能。系统仍会将此类别下的违规事项视为「校服仪容违规」处理。";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning']="要增加操行纪录到任何日期，该日期必须位于一\"累计时段\"内，且该时段之\"累计转换方式\"必须已设定妥当。详情请参阅用户手册。";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning2']="要增加操行纪录到任何日期，该日期必须位于一\"累计时段\"内，且该时段之\"累计转换方式\"必须已设定妥当。详情请参阅用户手册。";
$iDiscipline['AccumulativePunishment_Preset_Category_Delete_Warning']="所选项目中包含不能被删除之类别.";
$iDiscipline['AccumulativePunishment_No_CatPeriod_Setting_Warning']="你所输入的事项日期尚未设定进行累绩违规计算，或事项日期所属时段尚未设定计算方法细节。请前往 设定>累成积违规设定 进行设定。";
$iDiscipline['AccumulativePunishment_Period_FromDate_MustSmallerThan_ToDate']="时段之 开始日期 须早过 结束日期";
$iDiscipline['Management_Child_AccumulativePunishmentAlert_DateRange_Part2']="起之纪录";
$iDiscipline['AccumulativePunishment_Setting_EditCategoryProperty']="编辑类别属性";
$iDiscipline['AccumulativePunishment_Setting_DeleteCategory']="删除类别";
$iDiscipline['AccumulativePunishment_Import']="累积违规项目汇入";
$iDiscipline['AccumulativePunishment_Parent']="所属违规类别/项目";
$iDiscipline['AccumulativePunishment_Report_Remark']="X/Y: 现时违规次数 / 引致处罚的违规次数。";
$iDiscipline['AccumulativePunishment_Import_FileDescription1']="$i_UserClassName, $i_ClassNumber, $i_RecordDate, ".$iDiscipline['Accumulative_Category'].", ".$iDiscipline['Accumulative_Category_Item_Name'].", ".$i_Discipline_System_general_remark;
$iDiscipline['AccumulativePunishment_Import_FileDescription2']="$i_UserLogin, $i_RecordDate, ".$iDiscipline['Accumulative_Category'].", ".$iDiscipline['Accumulative_Category_Item_Name'].", ".$i_Discipline_System_general_remark;
$iDiscipline['AccumulativePunishment_Import_Failed_Reason']="错误";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason1']="没有对应的学生";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason2']="日期格式错误";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason3']="没有对应的".$iDiscipline['Accumulative_Category']." / ".$iDiscipline['Accumulative_Category_Item'];
$iDiscipline['AccumulativePunishment_Import_Failed_Reason4']="没有累积违规设定";
$iDiscipline['AccumulativePunishment_Import_Failed_Reason5']="不可汇入迟到纪录";
$iDiscipline['AccumulativePunishment_Input_Confirm']="你是否确定要呈送?";
$iDiscipline['DuplicatdItemCode'] = "项目编号重复";
$iDiscipline['item_code_not_found'] = "项目编号无效";
$iDiscipline['invalid_date'] = "事项日期无效";
$iDiscipline['student_not_found'] = "找不到学生资料";
$iDiscipline['MeritType'] = "奖励项目";
$iDiscipline['Award_Punishment_Type'] = "奖惩类型";
$iDiscipline['DemeritType'] = "违规项目";
$iDiscipline['Award_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore, $i_Discipline_System_Subscore1, $i_Discipline_System_Add_Merit, ". $iDiscipline['MeritType'].", $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withSubject'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Subscore1 ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_Subject_name, $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withoutSubject'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Subscore1 ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_UserRemark";

# added on 01 Sept 2008
$iDiscipline['AccumulativeReward_Import']="累积奖励项目汇入";

# added on 04 Sept 2008
$iDiscipline['Reports_Child_AccumulativeRewardReport']="累积奖励报告";
$iDiscipline['Reports_Child_AccumulativeRewardClassReport']="班级累积奖励 (".$iDiscipline['Accumulative_Category_Upgrade_Calculation_Scheme_1'].")总表";
$iDiscipline['Reports_Child_AccumulativeRewardCategoryReport']="每天班级累积奖励报告";
$iDiscipline['Reports_Child_AccumulativeRewardReport_RewardCount']="优点次数";
$iDiscipline['AccumulativeReward_Report_Remark']="X/Y: 现时奖励次数 / 引致优点的奖励次数。";

# add on 08 Sept 2008
$i_Discipline_System_Conduct_Marks_Reset = "重设学生操行分";




# added by Kelvin Ho on 1 Dec 2008
$i_Discipline_System_Discipline_Conduct_JS_alert = "请输入分数";
###########################################
## cases (added by YatWoon 20080211)
###########################################
$iDiscipline['CaseReport'] = "个案报告";
$iDiscipline['CreateReport'] = "建立报告";
$iDiscipline['SelectCaseReport'] = "选择报告类型";
$iDiscipline['SelectedCaseReport'] = "已选择报告类型";
$iDiscipline['ReportInfo'] = "报告设定";
$iDiscipline['CaseChoice'] = array(
									array(1, "Violation of Mobile Phone Regulations"),
									array(2, "Violation of School Regulations"),
									array(3, "Case Record (form1)"),
									array(4, "Case Record (form4)"),
									array(5, "Warning Letter"),
									array(6, "Improper School Uniform"),
);

$iDiscipline['Venue'] = "地点";
$iDiscipline['VenueChoice'] = array(
									array(1, "Classroom ([#Room#])"),
									array(2, "Corridor"),
									array(3, "Hall"),
									array(4, "New Annex"),
									array(5, "Tuck Shop"),
									array(6, "Playground"),
);

$iDiscipline['Others'] = "其他";
$iDiscipline['StudentInvolved'] = "牵涉学生";
$iDiscipline['TearchersInvolved'] = "牵涉老师";
$iDiscipline['Description'] = "详情";
$iDiscipline['CourseOfAction'] = "处罚/跟进行动";
$iDiscipline['CourseOfActionChoice'] = array(
									array(1, "Detention"),
									array(2, "Compensation"),
									array(3, "Suspension"),
									array(4, "Verbal Warning"),
									array(5, "Record in 'Black Book'"),
									array(6, "School Services"),
									array(7, "Social Services"),
									array(8, "Inform Parents"),
									array(9, "Refer to Counseling Committee / Social Worker"),
);
$iDiscipline['UniformAction'] = array(
									array(1, "See [#ActionTeacherName#] on [#ActionDate#] at [#ActionTime#] [#ActionAPM#]"),
									array(2, "Send student home to change"),
									array(3, "Detention on [#ActionDate#]"),
);
$iDiscipline['Remarks'] = "备注";
$iDiscipline['ReferenceNo'] = "参考编号";
$iDiscipline['Reason_s'] = "选项";
$iDiscipline['MobileReasons'] = array(
									array(1, "The phone was switched on during school hours;"),
									array(2, "He brought the mobile phone without prior application;"),
									array(3, "The phone rang during the lesson or during school hours;"),
									array(4, "The mobile phone had no \"Approved Label\" issued by the Discipline Committee;"),
);
$iDiscipline['NoReasonSelected'] = "请最少选择一个选项。";
$iDiscipline['NoProblemSelected'] = "请最少选择一个选项。";
$iDiscipline['DatesTimes'] = "日期, 时间";
$iDiscipline['Details'] = "详情";
$iDiscipline['Punishment'] = "处罚";
$iDiscipline['RecordInBlackBook'] = "加入黑名单";
//$iDiscipline['DetailsOffence'] = "Details of offence";
$iDiscipline['DetailsDescription'] = "详情";
$iDiscipline['RecordDate'] = "纪录日期";
$iDiscipline['RecordTime'] = "纪录时间";
$iDiscipline['UniformProblem'] = array(
									array(1, "Belt"),
									array(2, "School Badge / Tie"),
									array(3, "Shoes / Socks"),
									array(4, "Coloured Singlet / Vest"),
									array(5, "Ring / Accessories"),
									array(6, "Sweater"),
									array(7, "Hair Style"),
									array(8, "School Blazer"),
									array(9, "Trousers"),
									array(10, "Jacket"),
									array(11, "Shirt"),
);
$iDiscipline['UniformProblems'] = "选项";
$iDiscipline['fromStaffList'] = "自 <b>教职员名单</b>";
$iDiscipline['CaseHandledby'] = "负责人";
$iDiscipline['FollowUp'] = "跟进";
$iDiscipline['PleaseSelectOption'] = "请最少选择一个选项。";
$iDiscipline['PreviewReport'] = "预览报告";
$iDiscipline['RemoveRecord'] = "你确定要删除此纪录？";

### added on 20080828 ###
$i_Discipline_System_Settings_Email_Notification = "电邮通知设定";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Teachers = "电邮通知 (教师)";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Parents = "电邮通知 (家长)";
$i_Discipline_System_Settings_Email_Notification_SubTitle_Students = "电邮通知 (学生)";
$i_Discipline_System_Settings_Email_Notification_Email_ClassTeacher = "通知班主任每当学生有新违规 / 奖励纪录";
$i_Discipline_System_Settings_Email_Notification_Email_PIC = "通知负责教师每当有关记录已被批核 / 拒绝 / 修改";
$i_Discipline_System_Settings_Email_Notification_Email_Parents = "发送家长信给有关家长";
$i_Discipline_System_Settings_Email_Notification_Email_Student_Record = "通知学生每当有新违规 / 奖励纪录";
$i_Discipline_System_Settings_Email_Notification_Email_Student_Detention = "通知学生余下未留堂时间";
$i_Discipline_System_Record_Status_Updated = "已更新";

### added on 20080918 ###
$i_Discipline_System_ParentLetter_Title = "违规纪录通知";
$i_Discipline_System_ParentLetter_Content1 = "敬启者︰";
$i_Discipline_System_ParentLetter_Content2 = "贵子弟";
$i_Discipline_System_ParentLetter_Content3 = "于";
$i_Discipline_System_ParentLetter_Content4 = "违反校规";
$i_Discipline_System_ParentLetter_Content5 = "现给予惩罚";
$i_Discipline_System_ParentLetter_Content6 = "希　台端督促　贵子弟遵守校规，用心向学。";
$i_Discipline_System_ParentLetter_Content7 = "此致";
$i_Discipline_System_ParentLetter_Content8 = "贵家长";

$eDiscipline['RankingReport'] = "排名报告";
$eDiscipline['AwardPunishmentRanking'] = "奖励及惩罚排名";
$eDiscipline['GoodconductMisconductRanking'] = "良好及违规行为排名";
$eDiscipline['StudentReport'] = "学生报告";
$eDiscipline['ClassSummary'] = "班别摘要";
$eDiscipline['MonthlyReport'] = "每月违规报告";
$eDiscipline['UniformRecords'] = "Uniform Records";
$eDiscipline['Top10'] = $eDiscipline['RankingReport'];
$eDiscipline['MisconductReport'] = "违规行为报告";
$eDiscipline['GoodConductMisconductReport'] = "良好及违规行为报告";
$eDiscipline['AwardPunishmentReport'] = "奖励及惩罚报告";
$eDiscipline['Conduct_Grade_Report'] = "操行评级报告";

# added by marcus 3/7
$eDiscipline['Viewing'] = "训导纪录";
$eDiscipline['ClassRank'] = "全班排名";
$eDiscipline['ClassLevelRank'] = "全级排名";
$eDiscipline['AwardRanking'] = "奖励排名";
$eDiscipline['GoodconductRanking'] = "良好行为排名";
$eDiscipline['Class'] = "班";
$eDiscipline['Form'] = "级";
$eDiscipline['Detention_Reason'] = "留堂原因";
$i_Discipline_List_View = "列表";
$i_Discipline_System_Student_Detention_List_Instruction_Msg= "列表显示你的留堂安排情况";
$i_Discipline_All_Arrangement = "所有留堂安排";

$eDiscipline['no_ConductScore_MeritNum_selected'] = "没有选择奖惩内容及操行分, 继续吗?";

$eDiscipline['ReportGeneratedDate'] = "报告产生日期 : ";

###########################################

$i_eSports = "运动会系统";
$i_Sports_Select_Menu = "请从左方功能清单选出所需服务。";
$i_Sports_System = "运动会系统";
$i_Sports_menu_Settings = "设定";
$i_Sports_menu_General_Settings = "基本设定";
$i_Sports_menu_Annual_Settings = "年度事项设定";
$i_Sports_menu_Arrangement = "比赛安排";
$i_Sports_menu_Item_Settings = "项目及赛事设定";
$i_Sports_menu_Report = "浏览报告";
$i_Sports_menu_Participation = "参赛纪录";
$i_Sports_menu_Settings_House = "社际设定";
$i_Sports_menu_Settings_Lane = "线道数量";
$i_Sports_menu_Settings_Score = "得分设定";
$i_Sports_menu_Settings_LaneSet = "线道设定";
$i_Sports_menu_Settings_AgeGroup = "年龄分组";
$i_Sports_menu_Settings_GroupNumber = "分组编号";
$i_Sports_menu_Settings_Participant = "运动员号码";
$i_Sports_menu_Settings_ParticipantFormat = "运动员号码格式设定";
$i_Sports_menu_Settings_ParticipantGenerate = "产生运动员号码";
$i_Sports_menu_Settings_ParticipantRecord = "参赛纪录历";
$i_Sports_menu_Settings_Enrolment = "报名设定";
$i_Sports_menu_Settings_TrackFieldName = "项目";
$i_Sports_menu_Settings_CommonTrackFieldEvent = "一般田径比赛";
$i_Sports_menu_Settings_TrackFieldEvent = "赛事";
$i_Sports_menu_Settings_EnrolmentLimit = "报名限额";
$i_Sports_menu_Settings_Detail = "比赛详情";
$AllGroups = "全部组别";

$i_Sports_export_reuslt = "汇出赛事成绩";

#########################################################
$i_Sports_menu_Settings_YearEndClearing = "删除年度纪录";
#########################################################


$i_Sports_menu_Arrangement_EnrolmentUpdate = "报名更新";
$i_Sports_menu_Arrangement_Schedule = "赛程自动安排";
$i_Sports_menu_Arrangement_Relay = "接力报名名单";
$i_Sports_menu_Report_WholeSchool = "全校统计";
$i_Sports_menu_Report_Class = "班别统计";
$i_Sports_menu_Report_House = "社际统计";
$i_Sports_menu_Report_Event = "赛项统计";
$i_Sports_menu_Report_EventRanking = "赛项排名";
$i_Sports_menu_Report_HouseGroupScore = "社际及组别分数";
$i_Sports_menu_Report_GroupChampion = "分组个人成绩";
$i_Sports_menu_Report_ClassEnrolment = "班别参赛表";
$i_Sports_menu_Report_HouseEnrolment = "社际参赛表";
$i_Sports_menu_Report_ExportRecord = "汇出比赛档案";
$i_Sports_menu_Report_ExportRaceArrangement = "汇出比赛安排";
$i_Sports_menu_Report_ExportRaceResult = "列印比赛成绩纸";
$i_Sports_menu_Report_ExportAthleticNumber = "列印号码纸";
$i_Sports_menu_Report_RaceRecord = "比赛纪录";
$i_Sports_menu_Report_RaceTopRecord = "历年比赛最佳纪录";
$i_Sports_menu_Report_RaceResult = "比赛成绩";
$i_Sports_menu_Participation_TrackField = "田径成绩";
$i_Sports_menu_Participation_Relay = "接力纪录";

#added by marcus 20090713
$i_Sports_menu_Report_Best_Athlete_By_Group = "各组别最佳运动员";
$i_Sports_menu_Report_Best_Athlete_By_Class = "各班最佳运动员";

$i_Sports_field_Total_Score = "总分";
$i_Sports_field_Score = "分数";
$i_Sports_field_NumberOfLane = "线道数目";
$i_Sports_field_ScoreStandard = "分数标准";
$i_Sports_field_Rank_1 = "第一名";
$i_Sports_field_Rank_2 = "第二名";
$i_Sports_field_Rank_3 = "第三名";
$i_Sports_field_Rank_4 = "第四名";
$i_Sports_field_Rank_5 = "第五名";
$i_Sports_field_Rank_6 = "第六名";
$i_Sports_field_Rank_7 = "第七名";
$i_Sports_field_Rank_8 = "第八名";
$i_Sports_field_Lane = "线道";
$i_Sports_field_Rank = "名次";
$i_Sports_field_Grade = "级别";
$i_Sports_field_GradeName = "级别名称";
$i_Sports_field_DOBUpLimit = "最早出生日期";
$i_Sports_field_DOBLowLimit = "最迟出生日期";
$i_Sports_field_GradeCode = "分组编号";
$i_Sports_field_Enrolment_Date_start = "开始报名日期";
$i_Sports_field_Enrolment_Date_end = "截止报名日期";
$i_Sports_field_Event_Name = "项目";
$i_Sports_field_Event_Type = "比赛类别";
$i_Sports_field_Group = "组别";
$i_Sports_field_Heat = "场次";
$i_Sports_field_Remark = "备注";
$i_Sports_field_Gender = "姓别";
$i_Sports_field_Result = "成绩";
$i_Sports_Class = "班别";
$i_Sports_Time = "时间";
$i_Sports_Detail = "详细资料";
$i_Sports_The = "第";
$i_Sports_TheLine1 = "第";
$i_Sports_TheLine2 = "线";
$i_Sports_TheGroup1 = "第";
$i_Sports_TheGroup2 = "组";
$i_Sports_Line = "线";
$i_Sports_Order = "次序";
$i_Sports_Pos = "位";
$i_Sports_Group = "组";
$i_Sports_Rank = "名";
$i_Sports_From = "从";
$i_Sports_To = "到";
$i_Sports_All = "所有项目";
$i_Sports_All_Grade = "所有级别";
$i_Sports_Color_Palette = "调色板";
$i_Sports_Color_Preview = "预览颜色";
$i_Sports_Metres = "米";

$i_Sports_Enrolment_Warn_MaxEvent = "你最多可参与";
$i_Sports_Enrolment_MaxEvent = "最多参与项目";
$i_Sports_Enrolment_Detail = "报名详情";
$i_Sports_Warn_Please_Select = "请选择或输入其中一项";
$i_Sports_Warn_OpenGroup_Error = "公开组项目不能同时设定为非公开组项目, 请重选组别.";
$i_Sports_Warn_Select_Group = "请选择组别";
$i_Sports_Warn_Please_Fill_EventName = "请输入比赛名称";
$i_Sports_Record_Holder = "纪录保持者";
$i_Sports_New_Record = "新纪录";
$i_Sports_New_Record_Delete = "新纪录已删除";
$i_Sports_Record = "纪录";
$i_Sports_Record_Year = "纪录年份";
$i_Sports_House = "社";
$i_Sports_Standard_Record = "标准纪录";
$i_Sports_First_Round = "初赛";
$i_Sports_Second_Round = "复赛";
$i_Sports_Final_Round = "决赛";
$i_Sports_Person_Per_Group = "每组人数";
$i_Sports_Num_Of_Group = "组别数目";
$i_Sports_Random_Order = "随机排列";
$i_Sports_Race_Day = "比赛日期";
$i_Sports_Item = "项目";
$i_Sports_Event_Jump = "跳高";
$i_Sports_Enroled_Student_Count = "报名人数";
$i_Sports_Enrolled = "已报名";
$i_Sports_Arranged_Student_Count = "已安排人数";
$i_Sports_Participation_Count = "出席人数";
$i_Sports_Explain = "注解";
$i_Sports_Item_Without_Setting_Meaning = "表示此项目尚未设定";
$i_Sports_Not_Complete_Events_Meaning = "表示此项目尚未完结";
$i_Sports_Auto_Lane_Arrange_Reminder = "<font color=blue>*按呈送进行赛道自动安排</font>";

$i_Sports_Re_Auto_Arrange_Lane = "重新进行赛道自动安排";
$i_Sports_Arrange_Lane = "赛道安排";
$i_Sprots_Proceed_Arrange = "请按确定继续";
$i_Sports_Stu_Num_Exceed_Lane_Num ="每组参与人数将超越赛道数目，请更改项目设定";


############################################################################
$i_Sports_Warn_Please_Select_Rank=" 请选择同分者的名次 ";
$i_Sports_Warn_Rank_Assigned_To_Others=" 已分配给其他运动员";
$i_Sports_YearEndClearing_Msg1 = "这将会删除旧年度纪录";
$i_Sports_YearEndClearing_Msg2 = "<font color=red>纪录已删除</font>";
$i_Sports_YearEndClearing_Msg3 = "这将会:<li>删除报名纪录<li>更新年龄组别年份<li>更新比赛纪录";

$i_Sports_YearEndClearing_Clear ="删除";
$i_Sports_YearEndClearing_Continue="继续";
############################################################################

$i_Sports_RecordBroken = "破纪录";
$i_Sports_Qualified = "达到标准";

####################################
$i_Sports_Foul = "违规";
$i_Sports_NotAttend="缺席";
####################################

$i_Sports_Day_Enrolment = "运动会报名";
$i_Sports_Enrolment = "报名";
$i_Sports_Present = "出席";
$i_Sports_Absent = "缺席";
$i_Sports_Other = "其他";
#############################################
$i_Sports_Tie_Breaker= "选择同分者名次";
$i_Sports_Tie_Breaker_Rank = "名次";
#####################

$i_Sports_Count = "人次";
$i_Sports_Decrement = "扣分";
$i_Sports_total = "总数";
$i_Sports_already_set = "已设定";
$i_Sports_Unsuitable= "不适用";
$i_Sports_Event_Set_OnlineEnrol = "网上报名?";
$i_Sports_Event_Set_RestrictQuota = "计算报名限额?";
$i_Sports_Event_Set_CountHouseScore = "计算社分数";
$i_Sports_Event_Set_CountClassScore = "计算班分数";
$i_Sports_Event_Set_CountIndividualScore = "计算个人分数";
$i_Sports_Event_Set_ScoreStandard = "分数标准";
$i_Sports_Event_Open_Group_Event = "公开组赛事";
$i_Sports_Event_UnrestrictQuota_Event = "不计算报名限额赛事";
$i_Sports_Event_Open = "公开组";
$i_Sports_Event_Boys_Open = "男子公开组";
$i_Sports_Event_Girls_Open = "女子公开组";
$i_Sports_Event_Mixed_Open = "混合公开组";
$i_Sports_Event_Boys = "男子组";
$i_Sports_Event_Girls = "女子组";
$i_Sports_Event_Mixed = "男女混合";
$i_Sports_Event_All = "所有";

$i_Sports_Participant = "运动员";
$i_Sports_Participant_Number = "运动员号码";
$i_Sports_Participant_Number_Type = "类别";
$i_Sports_Participant_Number_1stRow = "第一行";
$i_Sports_Participant_Number_2ndRow = "第二行";
$i_Sports_Participant_Number_3rdRow = "第三行";
$i_Sports_Participant_Number_AutoNumLength = "顺序号码长度";
$i_Sports_Participant_Number_Generation_cond_HouseOrder = "社次序";
$i_Sports_Participant_Number_Generate = "产生运动员号码";
$i_Sports_Participant_Number_alert_generate = "你确定要产生运动员号码?";
$i_Sports_Participant_Number_Summary_NumberOfStudents = "学生数目";
$i_Sports_Participant_Number_Summary_NoClass  = "没有班别";
$i_Sports_Participant_Number_Summary_NoClassNum = "没有班号";
$i_Sports_Participant_Number_Summary_NoHouse = "没有社";
$i_Sports_Participant_Number_Summary_MultipleHouse = "多于一个社";
$i_Sports_Participant_Number_Summary_NoGender = "没有性别";
$i_Sports_Participant_Number_Summary_NoDOB = "没有出生日期";
$i_Sports_Participant_Number_Summary_NoAthleticNumber = "没有运动员号码";
$i_Sports_AdminConsole_AccountAllowedToUse = "可以设定使用运动会系统的用户";
$i_Sports_AdminConsole_UserTypes = "用户类别";
$i_Sports_AdminConsole_UserType['ADMIN'] = "管理员";
$i_Sports_AdminConsole_UserType['HELPER'] = "工作人员";
$i_Sports_AdminConsole_Alert_ChangeAdminLevel = "你是否确定要更改已选择用户的权限?";

$i_Sports_Report_Total_Participant_Count = "参赛人数";
$i_Sports_Report_Total_Student_Count = "学生总数";
$i_Sports_Report_Enrolment_Count = "报名人次";
$i_Sports_Report_Average_Enrolment_Count = "平均报名人次";
$i_Sports_Report_Item_Name = "项目名称";
$i_Sports_Arrange_Change_Postion = "对调位置";
$i_Sports_Record_Generate_Second_Round = "产生复赛参赛名单";
$i_Sports_Record_Generate_Final_Round = "产生决赛参赛名单";
$i_Sports_Record_Alert_Duplicate_RankNumber = "名次重复, 请重新选择";
$i_Sports_Record_Msg_Generate_Success = "<font color=red>出赛名单已成功产生</font>";
$i_Sports_Record_Msg_Generate_Unsuccess = "<font color=red>没有足够参赛纪录, 未能成功产生下一轮赛事的出赛名单</font>";
$i_Sports_Setting_Alert_FinalRound_Req = "若此项目需设定复赛, 必须同时设定决赛.";
$i_Sports_No_DOB = "出生日期尚未设定";

$i_Sports_Participation_Score = "参赛分数";
$i_Sports_Event_Detail = "赛事项目资料";
$i_Sports_Invalid_Data = "资料不正确";
$i_Sports_Edit_House_Alert = "更改社设定可能影响其他管理工具, 继续?";

####### Add for lunch box
$i_SmartCard_Lunchbox_System = "饭盒领取管理系统";
$i_SmartCard_Lunchbox_Menu_Settings  = "系统设定";
$i_SmartCard_Lunchbox_Menu_DataInput = "资料输入";
$i_SmartCard_Lunchbox_Menu_StatusChecking = "检视每日状况";
$i_SmartCard_Lunchbox_Menu_Report = "报告";
$i_SmartCard_Lunchbox_Settings_Terminal = "终端机设定";
$i_SmartCard_Lunchbox_Settings_Calendar = "每月饭盒分发日期设定";
$i_SmartCard_Lunchbox_Settings_DataClearance = "旧资料清除 (以提升效能)";
$i_SmartCard_Lunchbox_DataInput_Day = "每日名单";
$i_SmartCard_Lunchbox_DataInput_Month = "每月名单";
$i_SmartCard_Lunchbox_Sync_From_Attendance = "由今天的考勤纪录汇入名单";
$i_SmartCard_Lunchbox_Total_LunchBox = "名单总数";
$i_SmartCard_Lunchbox_Total_Present = "出席学生总数(包括迟到)";
$i_SmartCard_Lunchbox_Sync_From_Attendance_Confirm = "你是否确定由今天的考勤纪录汇入名单?";
$i_SmartCard_Lunchbox_Report_Generation = "报表产生";
$i_SmartCard_Lunchbox_Report_View = "报表检视";
$i_SmartCard_Lunchbox_Warning_CannotEditPrevious = "不能更改以往的纪录";
$i_SmartCard_Lunchbox_Warning_AlreadyHasLunchTicket = "此月份已有学生饭盒纪录. 更改月份纪录并<font color=red><b>不会</b></font>自动更改学生饭盒纪录.";
$i_SmartCard_Lunchbox_Warning_Save = "你确定要储存?";
$i_SmartCard_Lunchbox_Warning_Change = "你确定要转变?";
$i_SmartCard_Lunchbox_Warning_MonthlyCalendarNotExist = "此月份并未设定. 因此不能新增任何学生纪录.";
$i_SmartCard_Lunchbox_DataField_NumTickets = "已买饭日数";
$i_SmartCard_Lunchbox_DateField_Date = "日期";
$i_SmartCard_Lunchbox_Action_AddStudent = "更改名单";
$i_SmartCard_Lunchbox_Action_SetTo = "转为";
$i_SmartCard_Lunchbox_Status_NotTaken = "未取";
$i_SmartCard_Lunchbox_Status_Taken = "已取";
$i_SmartCard_Lunchbox_Status_NoTicket = "没有买饭";
$i_SmartCard_Lunchbox_Report_Daily = "每日报告";
$i_SmartCard_Lunchbox_Report_Month = "每月报告";
$i_SmartCard_Lunchbox_Report_BadAction = "错误行为";
$i_SmartCard_Lunchbox_Report_Type_Summary = "总结";
$i_SmartCard_Lunchbox_Report_Type_UntakenList = "未取饭盒名单";
$i_SmartCard_Lunchbox_Report_Type_TakenList = "已取饭盒名单";
$i_SmartCard_Lunchbox_Report_Type_StudentList = "所有学生名单";
$i_SmartCard_Lunchbox_Report_Type_DayBreakdown = "每日分析";
$i_SmartCard_Lunchbox_Report_Type_TopUntaken = "最多次未取饭盒名单";
$i_SmartCard_Lunchbox_Report_Type_BadAction_NoTicket = "没有买饭, 但尝试取饭";
$i_SmartCard_Lunchbox_Report_Type_BadAction_TakeAgain = "尝试再次取饭";
$i_SmartCard_Lunchbox_Report_Param_Type = "报告类型";
$i_SmartCard_Lunchbox_Report_Param_SplitClass = "分隔班别";
$i_SmartCard_Lunchbox_Report_Param_SelectClass = "选择班别";
$i_SmartCard_Lunchbox_Report_Lang_AllClasses = "所有班别";
$i_SmartCard_Lunchbox_Report_Field_Untaken_Times = "未取次数";
$i_SmartCard_Lunchbox_Report_Field_Untaken_Days = "未取日期";
$i_SmartCard_Lunchbox_Report_Words_NoUntaken = "没有学生未取饭盒";
$i_SmartCard_Lunchbox_Report_Words_NoTaken = "没有学生已取饭盒";
$i_SmartCard_Lunchbox_Report_Words_NoTicket = "没有学生已买饭盒";
$i_SmartCard_Lunchbox_Import_Format_Login = "以内联网帐号";
$i_SmartCard_Lunchbox_Import_Format_ClassNumber = "以班别班号";
$i_SmartCard_Lunchbox_Import_From_LastMonth = "从上月汇入";
$i_SmartCard_Lunchbox_MonthTarget = "从何月份汇入";
$i_SmartCard_Lunchbox_TargetMonthNumStudents = "上述月份已买饭盒的学生";
$i_SmartCard_Lunchbox_NewMonth_Days = "新月份饭盒日数";
$i_SmartCard_Lunchbox_Import_From_LastMonth_Description = "";


# yat 20090625
$i_eSwimmingGala = "水运会系统";
$i_swimming_gala = "水运会";
$i_Swimming_Gala_Enrolment = "水运会报名";


$i_House = "社";
$i_House_name = "社名";
$i_House_GroupLink = "内联网小组";
$i_House_ColorCode = "社颜色";
$i_House_HouseCode = "社代号";


$i_Community_Communities = "群组";
$i_Community_MyCommunities = "我的群组";
$i_Community_CommunityDirectory = "群组组织";
$i_Community_CommunityFunction_Settings = "设定";
$i_Community_Community_GoTo = "转至";
$i_Community_EventAll = "所有";
$i_Community_EventUpcoming = "即将来临";
$i_Community_EventPast = "昔日";


$i_ServiceMgmt_System = "时数计划管理"; #"服务时数管理系统";
$i_ServiceMgmt_System_Settings = $i_ServiceMgmt_System."设定";
$i_ServiceMgmt_System_Admin = "系统使用者及使用权设定";
$i_ServiceMgmt_System_Description_SetAdmin = "请选择可以使用本系统的用户及权限.";
$i_ServiceMgmt_System_AccessLevel = "权限";
$i_ServiceMgmt_System_AccessLevel_Normal = "一般用户";
$i_ServiceMgmt_System_AccessLevel_High = "管理员";
$i_ServiceMgmt_System_AccessLevel_Detail_Normal = "$i_ServiceMgmt_System_AccessLevel_Normal (允许资料输入及检视报告)";
$i_ServiceMgmt_System_AccessLevel_Detail_High = "$i_ServiceMgmt_System_AccessLevel_High (允许更改设定及学生要求)";

$i_ServiceMgmt_System_Menu_Report = "服务时数报表";
$i_ServiceMgmt_System_Menu_ClassReport = "服务时数一览表";
$i_ServiceMgmt_System_Menu_InputNew = "新增活动工作时数";
$i_ServiceMgmt_System_Menu_PresetActivity_Attend = "输入预设活动出席情况";
$i_ServiceMgmt_System_Menu_PresetActivity_Report = "预设活动报表";
$i_ServiceMgmt_System_Menu_PresetActivity_Input = "预设活动设定";
$i_ServiceMgmt_System_Menu_Settings_Type = "活动类型设定";
$i_ServiceMgmt_System_Menu_Settings_Student = "学生要求设定";

$i_ServiceMgmt_System_NotSet = "未设定";
$i_ServiceMgmt_System_Field_HoursRequired = "工作时数";
$i_ServiceMgmt_System_Field_ActivityCategory = "活动类型";
$i_ServiceMgmt_System_TargetClass = "欲更改要求的班别";
$i_ServiceMgmt_System_Warning_Numeric = "请输入数字";

$i_ServiceMgmt_System_Hours_Type1_Name = "校内";
$i_ServiceMgmt_System_Hours_Type2_Name = "校外";
$i_ServiceMgmt_System_Hours_Type3_Name = "其他";

$i_ServiceMgmt_System_ActivityCategory = "活动类型";

$i_PresetActivity_ActivityName = "活动名称";
$i_PresetActivity_ActivityCode = "活动编号";
$i_PresetActivity_ActivityCategory = "活动类型";
$i_PresetActivity_ActivityNature = "性质";
$i_PresetActivity_ActivityDate = "活动日期";
$i_PresetActivity_ActivityTime = "活动时间";
$i_PresetActivity_LevelName = "摘星计划种类";
$i_PresetActivity_PIC = "负责人";
$i_PresetActivity_StudentSelect = "已选学生";
$i_PresetActivity_HoursReq = "所需时数";
$i_PresetActivity_HoursComplete = "完成时数";
$i_PresetActivity_HoursLeft = "未完成时数";
$i_PresetActivity_DateStart = "开始日期";
$i_PresetActivity_DateEnd = "完成日期";

$i_ServiceMgmt_System_Class_Settings = "班级设定";

$i_Extra_SocialNumber = "社署编号";
$i_Extra_Manage_SocialNumber = "管理$i_Extra_SocialNumber";


################################################################################################
$iDiscipline['Management'] = "管理";
$iDiscipline['Management_Child_AwardInput'] = "输入奖励纪录";
$iDiscipline['Management_Child_AwardImport'] = "汇入奖励纪录";
$iDiscipline['Management_Child_AwardApproval'] = "批核奖励纪录";
$iDiscipline['Management_Child_PunishmentInput'] = "输入违规纪录";
$iDiscipline['Management_Child_PunishmentImport'] = "汇入违规纪录";
$iDiscipline['Management_Child_PunishmentApproval'] = "批核违规纪录";
$iDiscipline['Management_Child_PunishmentCounselling'] = "留堂及操行分纪录";
$iDiscipline['Management_Child_AccumulativePunishment'] = "输入累积违规纪录";
# added on 01 Sept 2008
$iDiscipline['Management_Child_AccumulativeReward'] = "输入累积奖励纪录";
# added on 31 Jan 2008
$iDiscipline['Management_Child_PendingNoticeRecord'] = "未发布家长信纪录";
# added on 8 Sept 2008
$iDiscipline['Management_Child_Personal'] = "个人纪录";
# added on 9 Sept 2008
$iDiscipline['Detention_Attendance_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber,$i_Discipline_System_PunishCouncelling_Detention_This_Minutes";
#####################

$iDiscipline['Reports'] = "报告";
$iDiscipline['Reports_Child_PersonalReport'] = $i_Discipline_System_Report_Type_Personal;
$iDiscipline['Reports_Child_AwardPunishmentRecord'] = "奖励/违规报告";
$iDiscipline['Reports_Child_DetailReport'] = $i_Discipline_System_Report_Type_Detail;
$iDiscipline['Reports_Child_ClassBasedReport'] = $i_Discipline_System_Report_Type_Class;

$iDiscipline['Statistics'] = "统计";
$iDiscipline['Statistics_Child_GeneralStats'] = $i_Discipline_System_Stat_Type_General;
$iDiscipline['Statistics_Child_DisciplineStats'] = $i_Discipline_System_Stat_Type_Discipline;
$iDiscipline['Statistics_Child_AwardStats'] = $i_Discipline_System_Stat_Type_Award;
$iDiscipline['Statistics_Child_DetentionStats'] = $i_Discipline_System_Stat_Type_Detention;
$iDiscipline['Statistics_Child_OtherStats'] = $i_Discipline_System_Stat_Type_Others;

$iDiscipline['Settings'] = "设定";
$iDiscipline['Settings_Child_Settings'] = $i_Discipline_System_Settings;
$iDiscipline['Settings_Child_Calculation'] = $i_Discipline_System_Settings_Calculation;
$iDiscipline['Settings_Child_Accumulative'] = "累积违规";
$iDiscipline['Settings_Child_Accumulative_Reward'] = "累积奖励";

$button_addtoview = "加入检视";
$iDiscipline['select_students'] = "选择学生";
$iDiscipline['class'] = "班别";
$iDiscipline['students'] = "学生";
$iDiscipline['warning_format'] = "学生";

$iDiscipline['compare_different_periods'] = "不同时期的比较";
$iDiscipline['Accumulative_Setting_Period'] = "设定计算时段";
$iDiscipline['Accumulative_NO_Setting_Period'] = "今天尚未设定时段";
$iDiscipline['Accumulative_Setting_Category'] = "类别及项目";

##################################################################################################################
# eDiscipline v1.2 [REVISED - START]

$eDiscipline['Overview'] = "概览";
$eDiscipline['Management'] = "管理";
$eDiscipline['Award_and_Punishment'] = "奖励及惩罚";
$eDiscipline['Good_Conduct_and_Misconduct'] = "良好及违规行为";
$eDiscipline['Case_Record'] = "个案纪录";
$eDiscipline['Conduct_Mark'] = "操行分";
$eDiscipline['Conduct_Grade'] = "操行评级";
$eDiscipline['Detention'] = "留堂";
$eDiscipline['Statistics'] = "统计";
$eDiscipline['Reports'] = "报告";
$eDiscipline['Settings'] = "设定";
$eDiscipline['eNoticeTemplate'] = "电子通告模板";
$eDiscipline['Access_Right'] = "权限";
$eDiscipline['General_Settings'] = "系统属性";
$iDiscipline['teacher'] = "老师";
$i_general_name = "姓名";
$i_general_class = "班别";
$i_general_are_you_sure = "确定?";
$i_general_please_select = "请选择";
$i_Discipline_System_CategoryList = "类别列表";
$i_Discipline_System_NewCategory = "新增类别";
$i_Discipline_System_Not_Yet_Assigned = "未被编排";

# Award & Punishment
$i_Discipline_Last_Updated = "最近更新";
$i_Discipline_Last_Updated_Str = "最近由 %NAME% 于 %DATE% 更新";
$i_Discipline_System_Conduct_School_Year = "年度";
$i_Discipline_System_Conduct_Semester = "学期";
$eDiscipline["BasicInfo"] = "基本资讯";
$eDiscipline["RecordList"] = "纪录列表";
$eDiscipline["RecordDetails"] = "纪录明细";
$eDiscipline["Record"] = "纪录";
$eDiscipline["AwardPunishmentQty"] = "奖惩数量";
$eDiscipline["Type"] = "类型";
$eDiscipline["Award_Punishment_Type"] = "奖励/惩罚项目";
$eDiscipline["Award_Punishment_Type2"] = "奖罚类别　";
$eDiscipline["Award_Punishment"] = "奖励/惩罚内容";
$eDiscipline["EventDate"] = $i_RecordDate;
$i_Discipline_System_Award_Punishment_Reference = "参考资讯";
$eDiscipline["Actions"] = "跟进行动";
$iDiscipline['Accumulative_Category_Item'] = "项目";
$eDiscipline["PreviewNotice"] = "预览通告";
$eDiscipline["Status"] = "状况";
$eDiscipline["ApprovedBy"] = "由";
$eDiscipline["ApprovedBy2"] = "于";
$eDiscipline["ApprovedBy3"] = "批核";
$eDiscipline["ReleasedBy"] = "由";
$eDiscipline["ReleasedBy2"] = "于";
$eDiscipline["ReleasedBy3"] = "开放";
$eDiscipline["RejectedBy"] = "由";
$eDiscipline["RejectedBy2"] = "于";
$eDiscipline["RejectedBy3"] = "拒绝";
$eDiscipline["WaivedBy"] = "由";
$eDiscipline["WaivedBy2"] = "于";
$eDiscipline["WaivedBy3"] = "豁免";
$eDiscipline["EditRecordInfo"] = "编辑此纪录";
$i_Discipline_System_Discipline_Case_Record_Case = "个案";
$i_Discipline_System_Discipline_Case_Record_Delete_Case_Delete = "你是否确定要删除?";
$eDiscipline["SelectStudents"] = "选择学生";
$eDiscipline["AddRecordToStudents"] = "新增纪录";
$eDiscipline["SelectActions"] = "选择跟进行动";
$eDiscipline["FinishNotification"] = "完成并发送通知";
$eDiscipline["DisciplineDetails"] = "个案资讯";
$eDiscipline["DisciplineDetails2"] = "个案明细";
$eDiscipline["RecordItem"] = "原因";
$eDiscipline["Award_Punishment_RecordItem"] = "奖惩项目";
$eDiscipline["SelectRecordCategory"] = "选择良好/违规行为类别";
$eDiscipline["SelectRecordCategory2"] = "选择奖惩类别";
$eDiscipline["SelectRecordCategory3"] = "选择奖惩类别";
$eDiscipline["SelectRecordItem"] = "选择奖惩项目";
$eDiscipline["SelectRecordItem2"] = "选择奖惩项目";
$eDiscipline["MeritDemeritContent"] = "奖惩内容";
$eDiscipline["Action"] = "跟进行动";
$eDiscipline["SendNoticeWhenReleased"] = "在开放予学生时，发送电子通告";
$eDiscipline["GlobalSettingsToAllStudent"] = "通用设定(套用至以下所有学生)";
$i_Discipline_Apply_To_All = "套用至所有纪录";
$i_Discipline_Detention_Times = "留堂次数";
$i_Discipline_Session = "时段";
$i_Discipline_Session2 = "留堂课节";
$i_Discipline_Auto_Assign_Session = "自动编排时段";
$i_Discipline_Template = "模板";
$i_Discipline_Usage_Template = "原因/模板";
$i_Discipline_Additional_Info = "附加资讯";
$eDiscipline["SelectTemplate"] = "选择模板";
$i_Discipline_PIC = "负责人";
$eDiscipline["Notification"] = "通知";
$eDiscipline["EmailTo"] = "发送电邮给";
$eDiscipline["ApproveRelease"] = "批准并开放";
$i_Discipline_System_Access_Right_Release = "开放";
$eDiscipline["SendNotice"] = "发放通告";
$i_Discipline_Not_Yet = "尚未点名";
$i_Discipline_Arranged_Session = "已编订时段";
$i_Discipline_Attendance = "点名";
$eDiscipline["WaiveRecord"] = "豁免记录";
$eDiscipline["NewLeafRecord"] = "更新计划记录";
$eDiscipline["NewLeaf_Rehabilitation_Record"] = "「更新计划」建议豁免记录";
$eDiscipline["SelectedRecord"] = "已选取纪录";
$eDiscipline["WaiveReason"] = "豁免原因";
$eDiscipline["HISTORY"] = "经过";
$i_Discipline_System_Award_Punishment_Submenu_Category_Item = "类别及项目";
$i_Discipline_System_Award_Punishment_Submenu_Approval = "批核";
$i_Discipline_System_Award_Punishment_Submenu_Promotion = "转换";
$i_Discipline_System_Award_Punishment_Move_Up = "上移";
$i_Discipline_System_Award_Punishment_Move_Down = "下移";
$i_Discipline_System_Award_Punishment_Move_Top = "移至最高";
$i_Discipline_System_Award_Punishment_Move_Bottom = "移至最低";
$i_Discipline_System_Award_Punishment_No_of_Item = "项目数目";
$i_Discipline_System_Award_Punishment_Award_Category = "奖励类别";
$i_Discipline_System_Award_Punishment_Punish_Category = "惩罚类别";
$i_Discipline_System_Award_Punishment_Setting_Category = "使用类别独立设定";
$i_Discipline_System_Award_Punishment_Setting_Global = "使用通用设定";
$i_Discipline_System_Award_Punishment_Common_Setting = "通用设定";
$i_Discipline_System_Award_Punishment_Conversion_Method_List = "转换方式列表";
$i_Discipline_System_Award_Punishment_Conversion_Method = "转换方式";
$i_Discipline_System_Award_Punishment_Equivalent_Point_Msg = "*对应分数显示要获得奖励所需的Good Point";
$i_Discipline_System_Award_Punishment_Search_Alert = "请输入内容";
$i_Discipline_System_Award_Punishment_Equivalent_Point = "对应分数 *";
$i_Discipline_System_Award_Punishment_All_School_Year = "所有年度";
$i_Discipline_System_Award_Punishment_Whole_Year = "全年";
$i_Discipline_System_Award_Punishment_All_Classes = "所有班别";
$i_Discipline_System_Award_Punishment_Warning = "你可以从\"选择状况\"下拉选单，选择检视特定状况的纪录。";
$i_Discipline_System_Award_Punishment_All_Records = "所有纪录";
$i_Discipline_System_Award_Punishment_Awards = "奖励纪录";
$i_Discipline_System_Award_Punishment_Punishments = "惩罚纪录";
$i_Discipline_System_Award_Punishment_Change_Status = "更改状况";
$i_Discipline_System_Award_Punishment_Select_Status = "选择状况";
$i_Discipline_System_Award_Punishment_Reference = "参考资讯";
$i_Discipline_System_Award_Punishment_Pending = "等待批核";
$i_Discipline_System_Award_Punishment_Wait_For_Waive = "等待豁免";
$i_Discipline_System_Award_Punishment_Wait_For_Release = "等待开放予学生";
$i_Discipline_System_Award_Punishment_Approved = "已批核";
$i_Discipline_System_Award_Punishment_Rejected = "已拒绝";
$i_Discipline_System_Award_Punishment_Released = "已开放";
$i_Discipline_System_Award_Punishment_UnReleased = "未开放";
$i_Discipline_System_Award_Punishment_ProcessedBy = "已由";
$i_Discipline_System_Award_Punishment_Processed = "处理完毕。";
$i_Discipline_System_Award_Punishment_Change_To_Processed = "转换状况为'已处理'";
$i_Discipline_System_Award_Punishment_Change_To_Processing = "转换状况为'处理中'";
$i_Discipline_System_Award_Punishment_Change_To_Release = "转换状况为'开放'";
$i_Discipline_System_Award_Punishment_Change_To_Unrelease = "转换状况为'未开放'";
$i_Discipline_System_Award_Punishment_Waived = "已豁免";
$i_Discipline_System_Award_Punishment_Locked = "已锁定";
$i_Discipline_System_Award_Punishment_Redeemed = "功过相抵";
$i_Discipline_System_Award_Punishment_Approve = "批核";
$i_Discipline_System_Award_Punishment_Reject = "拒绝";
$i_Discipline_System_Award_Punishment_Release = "开放";
$i_Discipline_System_Award_Punishment_UnRelease = "取消开放";
$i_Discipline_System_Award_Punishment_Waive = "豁免";
$i_Discipline_System_Award_Punishment_Redeem = "功过相抵";
$i_Discipline_System_Award_Punishment_Detention = "留堂";
$i_Discipline_System_Award_Punishment_Send_Notice = "发放通告";
$i_Discipline_System_Award_Punishment_Convert_To = "转换成";
$i_Discipline_System_Award_Punishment_Event_Date = "发生日期";

$i_Discipline_System_GoodConduct_Misconduct_Waive_Date_Count = "逾豁免期日数";
$i_Discipline_System_GoodConduct_Misconduct_Every = "每";
$i_Discipline_System_GoodConduct_Misconduct_Instance_Of = "次的";
$i_Discipline_System_GoodConduct_Misconduct_Will_Be_Count_As = "计算为";
$i_Discipline_System_GoodConduct_Misconduct_Category_To_Subcategory = "类别下的";
$i_Discipline_System_GoodConduct_Misconduct_Wait_For_NewLeaf = "等待透过「更新计划」进行豁免";

$i_Discipline_System_Insert_From_PPC = "从电子手帐输入";

$i_Discipline_GoodConduct = "良好行为";
$i_Discipline_Misconduct = "违规行为";
$i_Discipline_Conduct_Grade = "操行等级";
$iDiscipline['Confirmation'] = "确定";
$iDiscipline['ConductCategoryItem'] = "行为类别/项目";
$iDiscipline['SelectBehaviourItem'] = "-- 选择行为项目 --";
$eDiscipline["CompareShow"] = "比较/显示";


# Case Record
$i_Discipline_System_Discipline_Case_Record_Case_List = "个案列表 ";
$i_Discipline_System_Discipline_Case_Record_Case_Details = "个案明细";
$i_Discipline_System_Access_Right_Case_Record = "个案纪录";
$i_Discipline_System_Case_Record_Case_Info = "个案资讯";
$i_Discipline_System_Case_Record_Student_Involved = "相关学生";
$i_Discipline_System_Discipline_Case_Record_Case_Number = "个案编号";
$i_Discipline_System_Discipline_Case_Record_Case_Title = "个案名称";
$i_Discipline_System_Discipline_Case_Record_Category = "类别";
$i_Discipline_System_Discipline_Case_Record_Case_School_Year = "年度";
$i_Discipline_System_Discipline_Case_Record_Case_Semester = "学期";
$i_Discipline_System_Discipline_Case_Record_Case_Event_Date = "发生日期";
$i_Discipline_System_Discipline_Case_Record_Case_Location = "地点";
$i_Discipline_System_Discipline_Case_Record_Case_PIC = "负责人";
$i_Discipline_System_Discipline_Case_Record_Attachment = "附件";
$i_Discipline_System_Discipline_Case_Record_Processing = "处理中";
$i_Discipline_System_Discipline_Case_Record_Processed = "已处理";
$i_Discipline_System_Discipline_Case_Record_Case_Number_JS_alert = "请输入个案编号";
$i_Discipline_System_Discipline_Case_Record_Case_Number_Exists_JS_alert = "个案编号已存在";
$i_Discipline_System_Discipline_Case_Record_Case_Title_JS_alert = "请输入个案名称";
$i_Discipline_System_Discipline_Case_Record_Category_JS_alert = "请输入类别";
$i_Discipline_System_Discipline_Case_Record_Year_JS_alert = "请输入年度";
$i_Discipline_System_Discipline_Case_Record_Event_Date_JS_alert = "请输入记录日期";
$i_Discipline_System_Discipline_Case_Record_Location_JS_alert = "请输入地点";
$i_Discipline_System_Discipline_Case_Record_PIC_JS_alert = "请输入负责人";
$i_Discipline_System_Discipline_Case_Record_PIC = "负责人";
$i_Discipline_System_Case_Record_New_Student = "新增学生";

$i_Discipline_System_Ranking_Report_Msg = "排名报告显示获奖励或惩罚最多的级别或班别。请设定报告时段、对象、及排名方式。";
$i_Discipline_System_Ranking_Goodconduct_Misconduct_Report_Msg = "排名报告显示良好或违规行为最多的级别、班别或学生。请设定报告时段、对象、及排名方式。";
$i_Discipline_System_Case_Record_Delete_Msg = "删除个案纪录时，所有相关学生的奖惩纪录将会被一并删除。你是否确定要删除所选的个案纪录?";

$i_Discipline_All_Classes = "所有班别";
$i_Discipline_System_Award_Punishment_Approved = "已批核";
$i_Discipline_System_Award_Punishment_Rejected = "已拒绝";
$i_Discipline_System_Award_Punishment_Waived = "已豁免";
$i_Discipline_System_Award_Punishment_Released = "已开放";
$i_Discipline_System_Discipline_Case_Record_CaseNumber = "个案编号";
$i_Discipline_System_Discipline_Case_Record_days_ago = "日前";
$i_Discipline_System_Discipline_Case_Record_New_Case = "新增个案";
$eDiscipline["CaseIsFinished"] = "<font color=red>个案已完成</font>";
$eDiscipline["RecordIsWavied"] = "<font color=red>纪录已被豁免</font>";
$eDiscipline["RecordIsRejected"] = "<font color=red>纪录已被拒绝</font>";
$eDiscipline["RecordisGoodConduct"] = "<font color=red>纪录是".$i_Discipline_GoodConduct.".</font>";
$eDiscipline["RecordNotPassWaiveDate"] = "<font color=red>纪录未过豁免期</font>";
$eDiscipline["RecordHappenAgain"] = "<font color=red>豁免期内重犯</font>";
$eDiscipline["RecordNotEnoughNewLeaf"] = "<font color=red>未有足够改过迁善的限额.</font>";
$eDiscipline["RecordNoNewLeafSetting"] = "<font color=red>未有改过迁善的设定</font>";
$eDiscipline["RecordIsPending"] = "<font color=red>纪录正等待批核</font>";
$eDiscipline["RecordIsCase"] = "<font color=red>纪录属于个案纪录。</font>";
$eDiscipline["RecordIsRedeemed"] = "<font color=red>纪录已功过相抵。</font>";
$eDiscipline["RecordIsAcc"] = "<font color=red>纪录由良好/违规行为纪录组成。</font>";
$eDiscipline["noDeleteRight"] = "<font color=red>没有删除权限。</font>";
$eDiscipline["RejectRecord"] = "拒绝纪录";
$eDiscipline["ApproveRecord"] = "批核纪录";
$eDiscipline["RecordIsApproved"] = "<font color=red>纪录已被批核</font>";

# Good Conduct & Misconduct
$eDiscipline_Settings_GoodConduct = "良好行为";
$eDiscipline_Settings_Misconduct = "违规行为";
$eDiscipline["SelectConductCategory"] = "选择行为类别";
$eDiscipline["SelectConductItem"] = "选择行为";
$eDiscipline["RecordCategoryItem"] = "奖惩类别/项目";
$eDiscipline["Category_Item"] = "类别/项目";
$eDiscipline["Category_Item2"] = "类别/项目";

# 20090401 yatwoon
$eDiscipline["MissingSemesterPromptMsg_WithRight"] = "系统管理员之前曾更改学期更新模式至\"用户自行更新\"，导致系统无跟依据日期资料判断各累计时段所属之学期。你需要重新为所有累计时段指定所属学期。在此之前，用户将无法存取 管理>良好及违规 页面。";
$eDiscipline["MissingSemesterPromptMsg_WithoutRight"] = "由于学期更新模式已由\"系统自行更新\"更改为\"用户自行更新\"，系统无法跟据日期资料判断各累计时段所属之学期。你需要重新为所有累计时段指定所属学期。在此之前，你将无法存取 管理>良好及违规 页面。你可以联络你的系统管理员，了解学期更新模式的变更。";

# 20090513 Henry
$eDiscipline["PPC_Input_Award_Record"] = "输入奖励纪录";
$eDiscipline["PPC_Input_Punishment_Record"] = "输入惩罚纪录";
$eDiscipline["PPC_Input_Goodconduct_Record"] = "输入良好行为纪录";
$eDiscipline["PPC_Input_Misconduct_Record"] = "输入违规行为纪录";


# Conduct Mark Management
$i_Discipline_System_Conduct_Mark_Current_Conduct_Mark = "现时操行分";
$i_Discipline_System_Conduct_Mark_Pre_Adjustment = "调整前";
$i_Discipline_System_Conduct_Mark_Acc_Adjustment = "累积调整";
$i_Discipline_System_Conduct_Mark_Current = "现时操行分";
$i_Discipline_System_Discipline_Conduct_Mark_Adjustment = "调整";
$i_Discipline_System_Discipline_Conduct_Mark_Adjusted_Conduct_Mark = "调整后的操行分";
$i_Discipline_System_Discipline_Conduct_Last_Updated = "最近更新";
$i_Discipline_System_Discipline_Conduct_Mark_Generate_Conduct_Grade = "计算操行评级";
$i_Discipline_System_Access_Right_Adjust = "调整";
$i_Discipline_Reason = "原因";
$i_Discipline_Reason2 = "奖惩内容";
$i_Discipline_System_Discipline_Conduct_This_Adjustment = "本次调整";
$i_Discipline_System_Discipline_Conduct_Last_Generated = "上一次计算";
$i_Discipline_System_Discipline_Conduct_Last_Adjustment = "最近一次调整";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction1 = "使用指引";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction2 = "请选择要计算的年度及学期。";
$i_Discipline_System_Discipline_Conduct_Mark_Generation_Instruction3 = "一旦进行计算，所有先前的计算结果将被覆写。";
$i_Discipline_System_Discipline_Conduct_Mark_Weighting = "操行分权重";
$i_Discipline_System_Discipline_Conduct_Mark_Weight = "比重";
$i_Discipline_System_Discipline_Conduct_Mark_Weight_Total = "总共";
$i_Discipline_System_Discipline_Conduct_Mark_Weight_Semester = "学期";

# Conduct Mark Setting
$i_Discipline_System_Conduct_Warning_Rules = "警告规则";
$i_Discipline_System_Conduct_Base_Mark = "操行分底分";
$i_Discipline_System_Conduct_Grading_Scheme = "评级准则";
$i_Discipline_System_Conduct_Semester_Ratio = "学期计算比例";
$i_Discipline_System_Conduct_alert_min_conduct_mark = "请输入操行分下限";
$i_Discipline_System_Conduct_Ratio2 = "比例";
$i_Discipline_System_Conduct_Warning_Reminder_Point = "操行分底分";
$i_Discipline_System_Conduct_Instruction_Msg = "使用指引";
$i_Discipline_System_Conduct_Conduct_Balance_Auto_Update = "不论结余, 所有操行分数将会统一更新成所输入数目。你是否确定要继续?";
$i_Discipline_System_Conduct_Instruction_Recalculate_Msg = "更改评级准则后，请前往<b>管理 > 操行分</b>，重新计算操行评级。";
//$i_Discipline_System_Conduct_Instruction = "";
$i_Discipline_System_Conduct_Mark_Tips = "* 当操行分到达该警戒分数时,系统将发出警告.";
$i_Discipline_System_Conduct_Total = "总共";
$i_Discipline_System_Discipline_Conduct_Mark_Grade_JS_alert = "对应等级含有不允许的字元";
$i_Discipline_System_Discipline_Conduct_Mark_Score_Integer_JS_alert = "请输入数目";

$i_Discipline_System_Conduct_Instruction = "使用指引";
$i_Discipline_System_Conduct_Warning_Rules_List = "警告规则列表";
$i_Discipline_System_Conduct_Warning_Reminder_Point = "警戒分数";
$i_Discipline_System_Conduct_School_Year = "年度";
$i_Discipline_System_Conduct_Semester = "学期";
$i_Discipline_System_Conduct_Ratio = "比重";

$i_Discipline_System_Reports_Instruction_Msg = "个人报告显示所选学生之奖惩纪录及良好/违规行为纪录。请设定报告时段与及要显示的训导纪录类型。";
$i_Discipline_System_Reports_Report_Option = "- 选项 -";
$i_Discipline_System_Reports_First_Section = "- 奖惩纪录 -";
$i_Discipline_System_Reports_Second_Section = "- 良好/违规行为纪录 -";
$i_Discipline_System_Reports_Award_Punishment = "奖励 / 惩罚";
$i_Discipline_System_Reports_All_Awards = "所有奖励";
$i_Discipline_System_Reports_All_Punishment = "所有惩罚";
$i_Discipline_System_Reports_Include_Waived_Record = "包含豁免记录";
$i_Discipline_System_Reports_Report_Period = "报告时段";
$i_Discipline_System_Reports_Conduct_Behavior = "操行 / 行为";
$i_Discipline_System_Reports_All_Good_Conducts = "所有良好行为";
$i_Discipline_System_Reports_All_Misconduct = "所有违规行为";
$i_Discipline_System_Reports_Discipline_report = "操行报告";
$i_Discipline_System_Reports_Show_Reports_Option = "显示报告选项";
$i_Discipline_System_Reports_Misconduct_Behavior = "违规项目";
$i_Discipline_System_Reports_Invalid_Date_Compare = "起始日期需早于结束日期";
$iDiscipline['RankingTarget'] = "对象";
$iDiscipline['RecordType'] = "纪录种类";
$iDiscipline['RecordType2'] = "纪录类型";
$iDiscipline['Awarded'] = "奖励";
$iDiscipline['Punished'] = "惩罚";
$iDiscipline['Rank'] = "名次";
$iDiscipline['RankingRange'] = "范围";
$iDiscipline['ReceiveType'] = "类型";
$iDiscipline['byCategory'] = "行为类别";
$iDiscipline['byItem'] = "行为项目";
$iDiscipline['HorizontalDisplay'] = "并排显示";
$iDiscipline['StackDisplay'] = "层迭显示";

# Conduct Grade
$iDiscipline['ConductGradeAssessment'] = "操行评级评估";
$iDiscipline['ConductGradeMeeting'] = "操行评级会议";
$iDiscipline['CompleteRatio'] = "完成比例";
$iDiscipline['Assessment'] = "评级";
$iDiscipline['Percentage'] = "百分比";
$iDiscipline['TotalAward'] = "奖励次数";
$iDiscipline['TotalPunishment'] = "奖励总次数";
$iDiscipline['ViewConductGradeOf1stSemester'] = "检视上学期操行资料";
$iDiscipline['ConductGradeOverview'] = "操行等级概览";
$iDiscipline['ConductGradeFinal'] = "综合操行等级";
$iDiscipline['Grade'] = "操行评级";
$iDiscipline['Grade Range'] = "相差";
$iDiscipline['TeacherInvolved'] = "参与评级老师";
$iDiscipline['ReAssessmentOfConductGrade'] = "调整操行等级";
$iDiscipline['OriginalResult'] = "原有结果";
$iDiscipline['IntegratedConductGrade'] = "综合操行评级";
$iDiscipline['Overall'] = "综合";
$iDiscipline['Revised'] = "已调整　";
$iDiscipline['WaitingForReview'] = "等待调整";
$iDiscipline['Class_Report'] = "班别报告";
$iDiscipline['Teacher_Report'] = "老师报告";
$iDiscipline['NoAssessmentIsNeeded'] = "没有班别需要评级。";
$iDiscipline['PrintedBy'] = "列印老师";
$iDiscipline['NeedToReassessInConductMeeting'] = "若第一栏\"注\"显示「<font color='red'>*</font>」即表示该生某些项目之评级须于操行会议作讨论。";
$iDiscipline['ClassNo'] = "学号";
$iDiscipline['ChiName'] = "中文姓名";
$iDiscipline['Grade2'] = "等级";
$iDiscipline['Remark'] = "注";
$iDiscipline['IntegratedConduct'] = "综合操行";
$iDiscipline['TableTopText1'] = "各项目之等级(";
$iDiscipline['TableTopText2'] = "给予学生之等级) / 等级范围 / 参与评级老师之百分比";

# conduct report
$eDiscipline['Student_Conduct_Report'] = "学生操行报告";
$eDiscipline['Item'] = "事项";
$eDiscipline['PICTeacher'] = "负责老师";
$eDiscipline['ConductReportPeriod'] = " 期数";
$eDiscipline['AccumulateHomeworkNotSubmitted'] = "累积欠交功课";
$BaptistWingLungSecondarySchool = "浸信会永隆中学<br>BAPTIST WING LUNG SECONDARY SCHOOL";
$eDiscipline['Remark1ForWaiveRecord'] = "注一：\"R\"已抵消之记录";
$eDiscipline['Remark2ForPrefectReport'] = "注二：由领袖生报告之记录已交训导老师查证并核对。";
$eDiscipline['PrincipalSignature'] = "校长签署";
$eDiscipline['DisciplineGroupSignature'] = "训导组签署";
$eDiscipline['GuardianSignature'] = "家长/监护人签署";
$eDiscipline['Date'] = "签发日期";


# Modified by henry on 30 Dec 2008
$eDiscipline['NewLeaf_Notes'] = "注意：「更新计划」记录豁免功能不具追溯性。因此，如于记录被豁免后始追加更新时段内的违规行为记录，系统并不会取消已豁免记录的\"豁免\"状态。此时，请自行取消豁免有关记录。";

# eNotice Template Setting
$i_Discipline_System_Discipline_Template_Name = "范本名称";
$i_Discipline_System_Discipline_Category = "类别";
$i_Discipline_System_Discipline_Situation = "发放原因";
$i_Discipline_System_Discipline_Reply_Slip = "附有回条";
$i_Discipline_System_Discipline_Status = "状况";
$i_Discipline_System_Discipline_Reason_For_Issue = "发放原因";
$i_Discipline_System_Discipline_Template_All_Status = "所有状况";
$i_Discipline_System_Discipline_Template_Published = "使用中";
$i_Discipline_System_Discipline_Template_Draft = "暂不使用";
$i_Discipline_System_Discipline_Template_Title = "模板名称";
$i_Discipline_System_Discipline_Template_Title_JS_warning= "请输入范本名称";
$i_Discipline_System_Discipline_Template_Subject_JS_warning= "请输入范本标题";$i_Discipline_System_Discipline_Template_Auto_Fillin = "自动填充项目";
$i_Discipline_System_Discipline_Template_Topic_Title = "题目 / 标题";

# Access Right Setting
$i_Discipline_System_Discipline_Group_Access_Rights = "小组权限";
$i_Discipline_System_Discipline_Members_Rights = "训导成员";
$i_Discipline_System_Teacher_Rights = "老师";
$i_Discipline_System_Student_Rights = "家长 / 学生";
$i_Discipline_System_Student_Right_Navigation2 = "学生权限";
$i_Discipline_System_Access_Right_Award_Punish = "奖励<br>与惩罚";
$i_Discipline_System_Access_Right_Case_Record = "个案纪录";
$i_Discipline_System_Access_Right_Case = "个案";
$i_Discipline_System_Access_Right_Good_Conduct_Misconduct = "良好行为<br>及违规行为";
$i_Discipline_System_Access_Right_Conduct_Mark = "操行分";
$i_Discipline_System_Access_Right_Detention = "留堂";
$i_Discipline_System_Access_Right_Detention_Management = "留堂 -<br>安排";
$i_Discipline_System_Access_Right_Detention_Session_Arrangement = "留堂 -<br>时段管理";
$i_Discipline_System_Access_Right_Top10 = "排名报告";
$i_Discipline_System_Access_Right_Email = "电子通告模板";
$i_Discipline_System_Access_Right_Letter = "权限";
$i_Discipline_System_Access_Right_eNotice_Template = "电子通告模板";
$i_Discipline_System_Access_Right_View = "检视";
$i_Discipline_System_Access_Right_New = "新增";
$i_Discipline_System_Access_Right_Edit = "编辑";
$i_Discipline_System_Access_Right_Own = "自己";
$i_Discipline_System_Access_Right_All = "全部";
$i_Discipline_System_Access_Right_Delete = "删除";
$i_Discipline_System_Access_Right_Waive = "豁免";
$i_Discipline_System_Access_Right_NewLeaf = "更新计划";
$i_Discipline_System_Access_Right_Release = "开放";
$i_Discipline_System_Access_Right_Approval = "批核";
$i_Discipline_System_Access_Right_Finish = "完成";
$i_Discipline_System_Access_Right_Lock = "锁定";
$i_Discipline_System_Access_Right_Adjust = "调整操行分";
$i_Discipline_System_Access_Right_Take_Attendance = "点名";
$i_Discipline_System_Access_Right_Rearrange_Student = "重新编排时段";
$i_Discipline_System_Access_Right_Access = "存取";
$i_Discipline_System_Access_Right_Member_Name = "成员姓名";
$i_Discipline_System_Access_Right_Added_Date = "加入日期";
$button_Add_Members_Now = "立即新增组员";
$button_Back_To_Group_List = "返回小组列表";
$i_Discipline_System_Teacher_Right_Navigation2 = "老师权限";
$i_Discipline_System_Teacher_Right_Management = "- 管理 -";
$i_Discipline_System_Teacher_Right_Statistics = "- 统计 -";
$i_Discipline_System_Teacher_Right_Reports = "- 报告 -";
$i_Discipline_System_Teacher_Right_Settings = "- 设定 -";
$i_Discipline_System_Group_Right_Navigation_Group_List = "小组列表";
$i_Discipline_System_Group_Right_Navigation_New_Group = "新增小组";
$i_Discipline_System_Group_Right_Navigation_New_Member = "新增";
$i_Discipline_System_Group_Right_Navigation_Choose_Member = "选择成员";
$i_Discipline_System_Group_Right_Navigation_Selected_Member = "选择学生";
$i_Discipline_System_Group_Right_Navigation_Selected_Member2 = "已选择成员";
$i_Discipline_System_Group_Right_Navigation_Group_Info = "小组资讯";
$i_Discipline_System_Group_Right_Navigation_Group_Title = "小组名称";
$i_Discipline_System_Group_Right_Navigation_Group_Description = "描述";
$i_Discipline_System_Group_Right_Navigation_Group_Access_Right = "小组权限";
$i_Discipline_System_Group_Right_Group_Name = "小组名称";
$i_Discipline_System_Group_Right_Description = "描述";
$i_Discipline_System_Group_Right_No_Of_Members = "组员数目";

### Settings - Good Conduct & Misconduct
$eDiscipline['GoodConduct_and_Misconduct'] = "良好及违规行为";
$eDiscipline['Setting_GoodConduct'] = "良好行为";
$eDiscipline['Setting_Misconduct'] = "违规行为";
$eDiscipline['Setting_Cancel_PPC'] = "取消电子手帐状况";
$eDiscipline['Setting_NewLeaf'] = "更新计划";
$eDiscipline['Waive_By_New_Leaf_Scheme'] = "豁免(更新计划)";
$eDiscipline['Setting_NewLeaf_Remark'] = "因更新计划而被豁免.";
$eDiscipline['Setting_DefaultPeriodSettings'] = "累计时段";
$eDiscipline['Setting_CategorySettings'] = "类别及项目";
$eDiscipline['Setting_HeldEach'] = "Held each";
$eDiscipline['Setting_Rehabilitation_Period'] = "时段";
$eDiscipline['Setting_Days'] = "日";
$eDiscipline['Setting_WaiveFirst'] = "豁免数量上限";
$eDiscipline['Setting_Category'] = "类别";
$eDiscipline['Setting_Period'] = "累计时段";
$eDiscipline['Setting_Status'] = "状况";
$eDiscipline['Setting_Status_All'] = "所有状况";
$eDiscipline['Setting_Status_Using'] = "使用中";
$eDiscipline['Setting_Status_NonUsing'] = "非使用中";
$eDiscipline['Setting_CategoryList'] = "类别列表";
$eDiscipline['Setting_Status_Published'] = "使用中";
$eDiscipline['Setting_Status_Drafted'] = "暂不使用";
$eDiscipline['Setting_ItemName'] = "项目名称";
$eDiscipline['Setting_Reorder'] = "重新排序";
$eDiscipline['Setting_NAV_CategoryList'] = "类别列表";
$eDiscipline['Setting_NAV_NewCategoryItem'] = "新增项目";
$eDiscipline['Setting_NAV_EditCategoryItem'] = "修改项目";
$eDiscipline['Setting_NAV_NewCategory'] = "新增类别";
$eDiscipline['Setting_NAV_EditCategory'] = "编辑类别";
$eDiscipline['Setting_NAV_NewPeriod'] = "新增时段";
$eDiscipline['Setting_NAV_EditPeriod'] = "编辑时段";
$eDiscipline['Setting_CategoryName'] = "类别名称";
$eDiscipline['Setting_CalculationSchema'] = "计算方式";
$eDiscipline['Setting_CalculationSchema_Static'] = "固定递增方式";
$eDiscipline['Setting_CalculationSchema_Static_Abb'] = "固定";
$eDiscipline['Setting_CalculationSchema_Floating'] = "浮动递增方式";
$eDiscipline['Setting_CalculationSchema_Floating_Abb'] = "浮动";
$eDiscipline['Setting_CalculationSchema_Undefined'] = "未设定";
$eDiscipline['Setting_AccumulatedTimes'] = "累积次数";
$eDiscipline['Setting_DetentionTimes'] = "留堂次数";
$eDiscipline['Setting_SendNotice'] = "发出提示";
$eDiscipline['Setting_NewPeriod_Step1'] = "设定时段";
$eDiscipline['Setting_NewPeriod_Step2'] = "选择累计转换方式";
$eDiscipline['Setting_NewPeriod_Step3'] = "设定累计步骤";
$eDiscipline['Setting_CalculationDetails'] = "- 首个步骤 -";
$eDiscipline['Setting_FirstCalculationSchema']  = "- 首轮计算设定 -";
$eDiscipline['Setting_FollowUpAction'] = "- 跟进行动 -";
$eDiscipline['Setting_Reminder'] = "- 提示 -";
$eDiscipline['Setting_Condition'] = "条件";
$eDiscipline['Setting_Duplicate'] = "复制";
$eDiscipline['Setting_DefaultFollowUpAction'] = "预设跟进行动";
$eDiscipline['Setting_Detention'] = "留堂";
$eDiscipline['Setting_Semester'] = "对应年度";
$eDiscipline['Setting_BasicInformation'] = "基本资讯";
$eDiscipline['Setting_NumOfItem'] = "项目数量";
$eDiscipline['Setting_ConductMark_Increase']  = "操行分 (增加)";
$eDiscipline['Setting_ConductMark_Decrease']  = "操行分 (减少)";
$eDiscipline['Setting_Warning_PleaseSelectAtLeaseOnePunlishment'] = "请最少选择一项惩罚";
$eDiscipline['Setting_CalculationInfo'] = "- 计算设定 -";
$eDiscipline['Content'] = "内容";
$eDiscipline['Subject'] = "标题";
$eDiscipline['MeritDemerit'] = "奖惩";

$i_Discipline['Warning_Integer_Input'] = "必须为正整数或 0。";
$i_Discipline['Warning_Input_WaiveFirst'] = "当时段内之Waive First有数值时, 请同时设定Waive Day的数值。";
$i_Discipline['Warning_Need_Setting'] = "请先设定".$eDiscipline['Setting_Period'];
$i_Discipline['Waive_Day_Header_Detail'] = "几多日之内冇再犯可Waive* (days)";
$i_Discipline['Waive_First_Header_Detail']  = "Waive first* (times)";

### added on 20090113 by Ivan ###
$eDiscipline["Redeem"]["Step1"] = "选择要抵销的惩罚";
$eDiscipline["Redeem"]["Step2"] = "确认纪录";
$eDiscipline["RedeemRecord"] = "功过相抵纪录";
$eDiscipline["Redemption"] = "功过相抵";
$eDiscipline["jsWarning"]["MeritOnly"] = "要进行功过相抵，请选择一个奖励纪录。";
$eDiscipline["jsWarning"]["OneStudentOnly"] = "只可选择同一位学生之纪录。";
$eDiscipline["jsWarning"]["ApprovedRecordOnly"] = "只有已批核的奖励纪录可进行功过相抵。";
$eDiscipline["SelectPunishmentToRedeem"] = "请选择需要功过相抵的惩罚。";
$eDiscipline["AwardRedemption"] = "用作抵销惩罚的奖励";
$eDiscipline["PunishmentRedemption"] = "要抵销的惩罚";
$eDiscipline["CorrespondingWaivedRecords"] = "- 相关豁免记录 -";
$eDiscipline["CancelRedemption"] = "取消功过相抵";
$eDiscipline["Processing"] = "处理中...";
$eDiscipline["CancelRedemptionSuccess"] = "取消功过相抵完成";
$eDiscipline["CancelRedemptionFailed"] = "取消功过相抵失败";
### added on 2 Mar 2009 by Ivan ###
$eDiscipline["Websams_Transition"] = "资料传送(至网上校管系统)";
$eDiscipline["Websams_Transition_Step1"] = "上载网上校管系统CSV档";
$eDiscipline["Websams_Transition_Step2"] = "选择要传送之纪录类型";
$eDiscipline["Websams_Transition_Step3"] = "传送纪录";
$eDiscipline["Websams_Transition_Step1_url_example"] = "例子: http://websams.xxx.edu.hk/";
$eDiscipline["File"] = "档案";
$eDiscipline["WebSAMS_URL"] = "网上校管系统网址";
### added on 3 Mar 2009 by Ivan ###
$eDiscipline["Websams_Transition_CSV_File_Remarks"] = "从网上校管系统汇出包含STUID及REGNO之CSV档";
$eDiscipline["jsWarning"]["SelectFileFirst"] = "尚未选择CSV档。";
$eDiscipline["Websams_Transition_Step1_Instruction1"] = "你只能传送纪录到网上校管系统一次。进行传送前，请确认所有奖惩纪录均已经更新并正确无误。";
$eDiscipline["Websams_Transition_Step1_Instruction2"] = "要汇出所需CSV档, 请使用此查询语句:";
$eDiscipline["Websams_Transition_Step1_Instruction3"] = "<i>SELECT STUID, REGNO FROM TB_STU_STUDENT</i>";
$eDiscipline["Websams_Transition_Step1_Instruction4"] = "要传送奖惩纪录到网上校管系统，请先登入网上校管系统并汇出含有STUID及REGNO的CSV档，然后进入本页。传送过程必须在登入及汇出CSV档后的五分钟内开始。";
$eDiscipline["WebSAMS_Warning_CsvNoData"] = "CSV档案没有任何资料";
$eDiscipline["WebSAMS_Warning_wrongExtension"] = "<font color=\"red\">档案不是CSV格式</font>";
$eDiscipline["WebSAMS_Warning_uploadFailed"] = "<font color=\"red\">上载失败</font>";
$eDiscipline["WebSAMS_Warning_wrongHeader"] = "<font color=\"red\">标题错误</font>";
$eDiscipline["Instruction"] = "使用指引";
$eDiscipline["NumOfWebSAMSRegNoInCSV"] = "已上传REGNO数量";
$eDiscipline["ReportCardPrintIndicate"] = "于网上校管系统成绩表显示所传送之奖惩纪录";
$eDiscipline["Caution"] = "注意";
$eDiscipline["Websams_Transition_Step2_Instruction1"] = "系统只会传送 <i>已经批核</i> 的奖惩纪录。";
$eDiscipline["TargetSchoolYear"] = "目标学年";
$eDiscipline["CurrentSchoolYear"] = "现时学年";
$eDiscipline["RecordsInDiscipline"] = "纪录概览";
$eDiscipline["RecordType"] = "纪录类型";
$eDiscipline["TransferredRecords"] = "已传送纪录数量";
$eDiscipline["NotYetTransferredRecords"] = "未传送纪录数量";
$eDiscipline["LastTransferringDate"] = "最近传送";
$eDiscipline["Websams_Transition_Step3_Instruction1"] = "资料传送期间，请勿浏览其他页面，或关闭任何浏览器视窗或弹出式视窗。";
$eDiscipline["Websams_Transition_Step3_Instruction2"] = "按\"开始传送\"按钮，传送纪录往网上校管系统。";
$eDiscipline["DataTransferComplete"] = "纪录传送完成。";
$eDiscipline["RecordsTransferred1"] = "已传送";
$eDiscipline["RecordsTransferred2"] = "纪录";
$eDiscipline["jsWarning"]["InvalidURL"] = "网址不正确。";
$eDiscipline["jsWarning"]["NothingSelected"] = "你尚未选择要传送之纪录类型。";
$eDiscipline["jsWarning"]["AllAwardsTransferrred"] = "已传送所有奖励纪录。";
$eDiscipline["jsWarning"]["AllPunishmentsTransferrred"] = "已传送所有惩罚纪录。";
$eDiscipline["Transferring"] = "转送中...";
$eDiscipline["CheckConnection"] = "检查连线";
$eDiscipline["Connecting"] = "连线中";
$eDiscipline["Successful"] = "成功连线";
$eDiscipline["Failed"] = "连线失败";
$eDiscipline["StartDataTransfer"] = "开始传送";
$eDiscipline["Websams_Transition_Step3_PopUpWarning"] = "<b>注意：</b><br />
资料传送时，<b><font color=\"red\">请勿</font></b>关闭此视窗。<br />
完成传送后，此视窗会自动关闭。";
$eDiscipline["Websams_Transition_Step1_Instruction5"] = "请注意，系统目前只支援传送纪录至WebSAMS1.5.1版本。";
$eDiscipline["Re-generate_CM_Warning"] = "由于评级准则已被修改，请前往<b>管理 > 操行分</b>，重新计算操行评级。";
# added on 30-07-2009 by Ivan - to WebSAMS
$eDiscipline["StudentMappingCSVFile"] = "学生编码CSV档案";
$eDiscipline["TeacherMappingCSVFile"] = "老师编码CSV档案";
$eDiscipline["Websams_Transition_Teacher_CSV_File_Remarks"] = "包含WebSAMS及eClass的老师编码之CSV档";
$eDiscipline["Download_Teacher_CSV"] = "按此下载eClass的老师编码";


# without SubScore
//$iDiscipline['Award_Import_FileDescription_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_SettingsSemester, $i_Discipline_System_ItemCode, ".$eDiscipline['Conduct_Mark'].", $i_Discipline_System_Discipline_Case_Record_PIC, $i_email_subject, $i_Discipline_System_No_Of_Award_Punishment, ".$iDiscipline['RecordType'].", ".$iDiscipline['Award_Punishment_Type'].", $i_UserRemark";
/*$iDiscipline['Award_Import_FileDescription_withoutSubScore'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td>
<td>Class Number</td><td>Record Date</td><td>Semester</td><td>Item Code</td><td>Conduct Score</td><td>PIC</td><td>Subject</td><td>Add Merit Record</td><td>Category</td><td>Merit Type</td><td>Remark</tr>
<tr><td>1A</td><td>12</td><td>2009-03-20</td><td>Fall semester</td><td>LATE002F</td><td>4</td><td>Teacher01</td><td>English</td><td>1</td><td>Punishment</td><td>-3</td><td>Remark</td></tr>
</table>";	*/
$iDiscipline['Demerit_Import_FileDescription_withSubject_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_Subject_name, $i_UserRemark";
$iDiscipline['Demerit_Import_FileDescription_withoutSubject_withoutSubScore'] = "$i_UserClassName, $i_ClassNumber, $i_RecordDate, $i_Discipline_System_ItemCode, $i_Discipline_System_ConductScore ($i_Discipline_System_general_decrement), $i_Discipline_System_Add_Demerit, ". $iDiscipline['DemeritType'].", $i_Discipline_System_DetentionMinutes, $i_UserRemark";

$iDiscipline['Award_Import_Remarks'] = "如果你使用Excel编辑CSV档，请紧记更改所有储存格成文字格式。选取有关储存格，然后按滑鼠右键并选择<b>储存格格式</b>。在<b>储存格格式</b>对话框之<b>数值</b>分页，选择<b>文字</b>。";
$iDiscipline['ImportOtherRecords'] = "汇入其他纪录";
$iDiscipline['DemeritCount'] = "违规次数";

$iDiscipline['TotalTime'] = "总次数";
$iDiscipline['PastRecord'] = "过往纪绿";
$iDiscipline['TotalRecord'] = "纪绿总数";


### added on 9 Feb 2009 by Kelvin ###
$eDiscipline["jsWarning"]["EnableEnotice"] = "此功能已被系统管理员停用.";

### added on 11 Feb 2009 by Kelvin ###

$eDiscipline["EnoticeDisabledMsg"] = "电子通告并未启用 或 你没有使用电子通告的权限。";

$eDiscipline["Every"] = "每";
$eDiscipline["InstanceOf"] = "次";
$eDiscipline["WillBeCounted"] = "将转换为一次";

$eDiscipline["SelectCategory"] = "-- 选择一分类 --";
$eDiscipline["SelectItem"] = "-- 选择一项目 --";

$eDiscipline['RecordWillChangeToWaiting_alert'] = "纪录将会自动转为「等待批准」状态。继续?";

$eDiscipline['SettingName'] = "系统属性";
$eDiscipline['Value'] = "设定值";
$eDiscipline['SemesterRatio_MAX'] = "学期计算比例最大值";
$eDiscipline['ConductMarkIncrement_MAX'] = "操行分调整上限";
$eDiscipline['AwardPunish_MAX'] = "优缺功过数量上限";
$eDiscipline['AccumulativeTimes_MAX'] = "累积良好及违规行为纪录数量上限";
$eDiscipline['ApprovalLevel_MAX'] = "需要批核的奖惩内容数量上限";
$eDiscipline['UseSubject'] = "需要指定奖励及惩罚纪录之相关科目";
$eDiscipline['AP_Conversion_MAX'] = "奖励及惩罚转换数量上限";
$eDiscipline['Activate_NewLeaf'] = "启动「更新计划」功能";
$eDiscipline['Detention_Sat'] = "留堂时段包括星期六";
$eDiscipline['Hidden_ConductMark'] = "隐藏操行分功能";

# eDiscipline v1.2 [REVISED - END]
##################################################################################################################



# eDiscipline v1.2
$eDiscipline["UnReleasedBy"] = "未开放设定者";
$i_Discipline_Date = "日期";
$i_Discipline_Time = "时间";
$i_Discipline_Applicable_Form = "适用级别";
$i_Discipline_Form = "级别";
$i_Discipline_Location = "地点";
$i_Discipline_Duty_Teacher = "当值老师";
$i_Discipline_Vacancy = "余额";
$i_Discipline_Assigned = "已编定人数";
$i_Discipline_Attended = "已出席";
$i_Discipline_StartEnd_Time_Alert = "时间不正确。开始时间必须早于结束时间。";
$i_Discipline_Invalid_Vacancy = "余额不正确";
$i_Discipline_Date_Selected = "已选择日期";
$i_Discipline_Assigned_Session_Cannot_Be_Deleted = "不能删除已分配时段。";
$i_Discipline_Delete_Session_Alert = "你是否确定删除此时段？";
$i_Discipline_Delete = "删除";
$i_Discipline_All_Forms = "所有级别";
$i_Discipline_Select = "选择";
$i_Discipline_Remove = "移除已选用户";
$i_Discipline_Select_Duty_Teachers = "选择当值老师";
$i_Discipline_All_Date = "所有日期";
$i_Discipline_All_Week = "所有周次";
$i_Discipline_This_Month = "当月";
$i_Discipline_This_Month2 = "本月";
$i_Discipline_This_Week = "当周";
$i_Discipline_This_Week2 = "本周";
$i_Discipline_Next_Week = "下星期";
$i_Discipline_All_Status = "所有状况";
$i_Discipline_All_Classroom = "所有课室";
$i_Discipline_Available = "未满";
$i_Discipline_Full = "已满";
$i_Discipline_Finished = "留堂完毕";
$i_Discipline_All_Time = "所有时间";
$i_Discipline_All_Duty = "所有当值老师";
$i_Discipline_Weekday = "平日";
$i_Discipline_In_The_Past_Alert = " is in the past";
$i_Discipline_Records = "纪录";
$i_Discipline_Session_List = "时段列表";
$i_Discipline_New_Sessions = "新增时段";
$i_Discipline_Edit_Session = "编辑时段";
$i_Discipline_Detention_List = "留堂纪录列表";
$i_Discipline_Detention_All_Status = "所有状况";
$i_Discipline_Detention_All_Time_Slot = "所有时间";
$i_Discipline_Detention_All_Teachers = "所有当值老师";
$i_Discipline_New_Student_Records = "新增学生";
$i_Discipline_Select_Student = "选择学生";
$i_Discipline_Add_Record = "增加纪录";
$i_Discipline_Enotice_Setting = "设定电子通告";
$i_Discipline_Previous = "Previous";
$i_Discipline_Next = "Next";
$i_Discipline_Remark = "备注";
$i_Discipline_Calendar = "行事历";
$i_Discipline_List = "详细纪录";
$i_Discipline_New_Session = "新增时段";
$i_Discipline_Student_Record = "学生纪录";
$i_Discipline_Take_Attendance = "点名";
$i_Discipline_Assign_To_Here = "分配于此";
$i_Discipline_Assign_To_Here_Alert = "你是否确定要分配于此？";
$i_Discipline_Unassign_Student_List = "未编排学生列表";
$i_Discipline_Unassign_Student = "未编排学生";
$i_Discipline_Student = "学生";
$i_Discipline_Times_Remain = "剩余次数";
$i_Discipline_Times = "次数";
$i_Discipline_View_Student_Detention_Arrangement = "检视留堂安排";
$i_Discipline_Vacancy_Is_Not_Enough = "余额不足。";
$i_Discipline_Auto_Arrange = "自动编排";
$i_Discipline_Arrange = "手动编排";
$i_Discipline_Arrange_Alert = "你是否确定要自行为所选学生分配留堂时间？";
$i_Discipline_Auto_Arrange_Alert = "你是否确定要自动为所选学生分配留堂时间？";
$i_Discipline_Detention_Details = "留堂明细";
$i_Discipline_Detention_Info = "留堂资料";
$i_Discipline_Students_Attending = "出席学生数目";
$i_Discipline_Attending_Student_List = "出席学生清单";
$i_Discipline_Student_List = "学生列表";
$i_Discipline_Cancel_Arrangement = "取消安排";
$i_Discipline_Class = "班别";
$i_Discipline_Detention_Reason = "留堂原因";
$i_Discipline_Request_Date = "惩罚日期";
$i_Discipline_Status = "状况";
$i_Discipline_Cancel_Arrange_Alert = "你是否确定取消已选择的安排？";
$i_Discipline_Display_Photos = "显示相片";
$i_Discipline_Hide_Photos = "隐藏相片";
$i_Discipline_Set_Absent_To_Present = "设定全部学生为\"出席\"";
$i_Discipline_Take_Attendance_Alert = "你是否确定更新纪录？";
$i_Discipline_Present = "出席";
$i_Discipline_Absent = "缺席";
$i_Discipline_Details = "详情";
$i_Discipline_Take = "立即点名";
$i_Discipline_Waiting_For_Arrangement = "等候编排时段";
$i_Discipline_Arranged = "已编排";
$i_Discipline_All_Students = "所有学生";
$i_Discipline_Not_Assigned = "未编定";
$i_Discipline_Detented = "已留堂";
$i_Discipline_Send_Notice_Via_ENotice = "发放电子通告";
$i_Discipline_Select_Category = "选择类别";
$i_Discipline_Session_Arranged_Before = "时段已被分配。";
$i_Discipline_Session_Not_Arranged_Before = "时段未被分配。";
$i_Discipline_Event_Type = "事项类别";
$i_Discipline_No_of_Records = "纪录数目";
$i_Discipline_No_of_Records2 = "纪录数量";
$i_Discipline_Pending_Event = "待办事项";
$i_Discipline_Released_To_Student = "已开放予学生";
$i_Discipline_No_Action = "没有跟进行动";
$i_Discipline_Attendance_Sheet = "点名纸";
$i_Discipline_Session_Info = "时段资讯";
$i_Discipline_Statistics_Options = "选项";
$i_Discipline_Show_Statistics_Option = "显示统计选项";
$i_Discipline_Hide_Statistics_Option = "隐藏统计选项";
$i_Discipline_School_Year = "年度";
$i_Discipline_Semester = "学期";
$i_Discipline_By = "By";
$i_Discipline_Target = "对象";
$i_Discipline_Press_Ctrl_Key = "可按Ctrl键选择多项";
$i_Discipline_No_Of_Detentions = "总留堂次数";
$i_Discipline_Reasons = "特定留堂原因数目";
$i_Discipline_Detention_Reasons = "特定留堂原因纪录数量";
$i_Discipline_Subjects = "科目";
$i_Discipline_Subject = "科目";
$i_Discipline_Generate = "输出统计";
$i_Discipline_Generate_Update_Statistic = "产生/更新统计";
$i_Discipline_Generate_Update_Report = "产生/更新报告";
$i_Discipline_Put_Into_Unassign_List = "加到未编排列表";
$i_Discipline_Quantity = "数量";
$i_Discipline_No_Of_Merits_Demerits = "奖惩纪录总数";
$i_Discipline_Award_Punishment_Titles = "特定类型纪录数量";
$i_Discipline_Show_Only = "统计范围";
$i_Discipline_Top = "最高";
$i_Discipline_No_Of_GoodConductsMisconducts = "良好及违规行为总数";
$i_Discipline_GoodConductMisconduct_Titles = "特定类型纪录数量";
$i_Discipline_Award_Punishment_Miss_Titles = "你尚未选择任何奖惩类型/项目。";

$eDiscipline["DisciplineAdmin"] = "操行管理员";
$eDiscipline["DisciplineAdmin2"] = "模组管理员";
$i_Discipline_System_Discipline_Case_Record_Finished = "已完成";
$i_Discipline_System_Case_Record_Notes = "Notes";
$i_Discipline_System_Award_Punishment_Pending = "等待批核";
$i_Discipline_System_Award_Punishment_Wait_For_Waive = "等待豁免";
$i_Discipline_System_Award_Punishment_Wait_For_Release = "等待开放予学生";
$i_Discipline_System_Award_Punishment_Locked = "已锁定";
$eDisicpline["GoodConduct_Misconduct_View"] = "检视";

$eDiscipline["EditDetention"] = "留堂编辑";
$eDiscipline["LockedBy"] = "Locked By";
$eDiscipline['Detention_Settings'] = "留堂设定";
$eDiscipline['Detention_Statistics'] = "留堂统计";
$eDiscipline['Award_and_Punishment_Statistics'] = "奖励及惩罚统计";
$eDiscipline['Good_Conduct_and_Misconduct_Statistics'] = "良好及违规行为统计";
$eDiscipline['Conduct_Mark_Statistics'] = "操行分统计";

$eDiscipline['Personal_Report_Goodconduct_Record']="良好行为";
$eDiscipline['Personal_Report_Misconduct_Record']="违规行为";
$eDiscipline['Personal_Report_Good_Misconduct_Record']="良好/违规行为";
$eDiscipline["Personal_Report_AwardPunishmentRecord"] = "奖励/惩罚纪录";


$eDiscipline['WarningLetter'] = "警告";
$eDiscipline['DetentionNoticeLetter'] = "留堂";
$eDiscipline['AwardNoticeLetter'] = "奖励";
$eDiscipline['PunishmentNoticeLetter'] = "惩罚";
$eDiscipline['ConductMarkNoticeLetter'] = "操行分";
$eDiscipline['GoodConductNoticeLetter'] = "良好行为";
$eDiscipline['MisConductNoticeLetter'] = "违规行为";


$eDiscipline['AdjustmentDate'] = "调整日期";

$eDiscipline_System_Admin_User_Setting = "设定管理用户";
$eDiscipline["AdminUser"] = "管理用户";
$eDiscipline["NewRecord"] = "新增纪录";
$eDiscipline["EditRecord"] = "编辑纪录";
$eDiscipline["AwardRecord"] = "奖励纪录";
$eDiscipline["PunishmentRecord"] = "惩罚纪录";
$eDiscipline["SelectSemester"] = "选择学期";
$eDiscipline["AwardPunishmentRecord"] = "奖励/惩罚纪录";
$eDiscipline["AwardPunishmentRecord2"] = "奖惩纪录";
$eDiscipline["TeacherStaffList"] = "教学及非教学职员名单";
$eDiscipline["SelectTeacherStaff"] = "请选择教学及非教学职员";
$eDiscipline["RecordRejectAlert"] = "如拒绝此纪录，相关的留堂纪录将会被移除，及相关通告将被暂停。\\n你确定要拒绝此纪录？";
$eDiscipline["RecordRemoveAlert"] = "如删除此纪录，相关的留堂纪录及通告将会被删除。\\n你确定要删除此纪录？";
$eDiscipline["EditActions"] = "编辑跟进行动";
$eDiscipline["AddDetention"] = "新增留堂纪录";
$eDiscipline["NoSemesterSettings"] = "没有学期设定。\\n请联络系统管理员跟进相关学期资料设定。";
$eDiscipline["CorrespondingConductRecords"] = "相对操行纪录";
$eDiscipline["DisciplineName"] = "操行";	# for eNotice usage
$eDiscipline["WaivedReason"] = "豁免原因";
$eDiscipline["SemesterSettingError"] = "学期设定错误";
$eDiscipline["AddStudents"] = "新增学生";
$eDiscipline["FinishCaseConfirm"] = "你是否确定要此个案已处理完毕？";
$eDiscipline["ReleaseCaseConfirm"] = "所有相关的奖励及惩罚纪录将自动开放。\\n\\n你是否确定开放此个案？";
$eDiscipline["UnReleaseCaseConfirm"] = "所有相关的奖励及惩罚纪录将自动设定为「未开放」。\\n\\n你是否确定不开放此个案？";

### Settings - Good Conduct & Misconduct (Need to translate)
$eDiscipline['Setting_NAV_PeriodList'] = "时段清单";
$eDiscipline['Setting_NewPeriod_Step4'] = "计算方案总结";
$eDiscipline['Setting_FutherCalculationInfo'] = "- 额外步骤 -";
$eDiscipline['Setting_DetentionSession'] = "时段";
$eDiscipline['Setting_StudyMark_Increase']  = "勤学分 (增加)";
$eDiscipline['Setting_StudyMark_Decrease']  = "勤学分 (减少)";
$eDiscipline['Setting_Period_Default'] = "预设时段";
$eDiscipline['Setting_Period_Default_Setting'] = "使用预设累计时段";
$eDiscipline['Setting_Period_Use_Default'] = "使用预设时段";
$eDiscipline['Setting_Period_Use_Default_Periods'] = "预设累计时段使用中";
$eDiscipline['Setting_Period_Specify'] = "指定时段";
$eDiscipline['Setting_Period_Specify_Setting'] = "使用类别累计时段";
$eDiscipline['Setting_Period_Use_Specify'] = "使用类别时段";
$eDiscipline['Setting_Period_Use_Specify_Periods'] = "类别累计时段使用中";
$eDiscipline['Accumulative_Period_InUse_Warning'] = "不能删除纪录。有已选择的纪录时段已在使用中。";
$eDiscipline['Accumulative_Period_InUse_Warning_Update'] = "不能更新纪录。有已选择的纪录时段已在使用中。";
$eDiscipline['Setting_Warning_InputCategoryName'] = "请输入类别名称。";
$eDiscipline['Setting_CalculationScheme'] = "计算方案";
$eDiscipline['Setting_ConversionScheme'] = "累计转换方式";
$eDiscipline['Setting_AddMeritRecord'] = "增加优点纪录";
$eDiscipline['Setting_AddDemeritRecord'] = "增加缺点纪录";
$eDiscipline['Setting_DefaultPeriodOverlapWarning'] = "时段不能重迭.";
$eDiscipline['Setting_JSWarning_PeriodStartDateEmpty'] = "请输入开始日期。";
$eDiscipline['Setting_JSWarning_PeriodEndDateEmpty'] = "请输入结束日期。";
$eDiscipline['Setting_JSWarning_InvalidStartDate'] = "开始日期不正确。";
$eDiscipline['Setting_JSWarning_InvalidEndDate'] = "结束日期不正确。";
$eDiscipline['Setting_JSWarning_StartDateLargerThanEndDate'] = "开始日期不能迟于结束日期。";
$eDiscipline['Setting_JSWarning_RemoveCategory'] = "不能删除此类别。";
$eDiscipline['Setting_JSWarning_CategoryItemNameEmpty'] = "请输入项目名称。";
$eDiscipline['Setting_JSWarning_CategoryItemCodeEmpty'] = "请输入项目编号。";
$eDiscipline['AwardPunishmentRecord'] = "奖惩内容";

$iPowerVoice['bit_rate'] = "位元率 (影响音质)";
$iPowerVoice['bit_rate_unit'] = "kbps";
$iPowerVoice['sampling_rate'] = "采样率 (影响清晰度)";
$iPowerVoice['sampling_rate_unit'] = "Hz";
$iPowerVoice['sound_length'] = "最大录音长度";
$iPowerVoice['sound_length_unit'] = "mins";
$iPowerVoice['sound_unlimited'] = "没有限制";
$iPowerVoice['power_voice_setting'] = "PowerVoice 设定";
$iPowerVoice['sound_recording'] = "录音";
$iPowerVoice['powervoice'] = "PowerVoice";

$i_frontpage_campusmail_icon_important_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_important_title.gif' align='absmiddle' border='0'  >";
$i_frontpage_campusmail_icon_important2_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_important.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_important}' >";
$i_frontpage_campusmail_icon_notification_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_notification_title.gif' align='absmiddle' border='0' >";
$i_frontpage_campusmail_icon_notification2_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_notification.gif' align='absmiddle' border='0' alt='{$i_frontpage_campusmail_notification}' >";
$i_frontpage_campusmail_icon_status_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_unread_mail_title.gif' align='absmiddle' border='0' >";
$i_CampusMail_New_Icon_ReadMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_open_mail.gif' align='absmiddle'  border='0' alt='{$i_frontpage_campusmail_read}' >";
$i_CampusMail_New_Icon_RepliedMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_reply_mail.gif' align='absmiddle'  border='0' alt='{$i_CampusMail_New_Reply}' >";
$i_CampusMail_New_Icon_NewMail_2007 =  "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_unread_mail.gif' align='absmiddle'  border='0' alt='{$i_frontpage_campusmail_unread}' >";
$i_frontpage_campusmail_icon_attachment_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/icon_attachment.gif' border='0' align='absmiddle' alt='{$i_frontpage_campusmail_attachment}' >";
$i_frontpage_campusmail_icon_restore_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash_restore.gif' border='0' align='absmiddle' alt='{$button_restore}' >";
$i_frontpage_campusmail_icon_trash_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash.gif' border='0' align='absmiddle' alt='{$button_remove}' >";
$i_frontpage_campusmail_icon_empty_trash_2007 = "<img src='{$image_path}/{$LAYOUT_SKIN}/iMail/btn_trash_clear.gif' border='0' align='absmiddle' alt='{$button_emptytrash}' >";

$i_frontpage_campusmail_ago['days'] = " 天以前";
$i_frontpage_campusmail_ago['hours'] = " 小时以前";
$i_frontpage_campusmail_ago['minutes'] = " 分钟以前";
$i_frontpage_campusmail_ago['seconds'] = " 秒以前";

$i_frontpage_campusmail_after['days'] = " 小时以后";
$i_frontpage_campusmail_after['hours'] = " 天以后";
$i_frontpage_campusmail_after['minutes'] = "  分钟以后";
$i_frontpage_campusmail_after['seconds'] = " 秒以后";

$ip20_lang_eng = "ENG";
$ip20_lang_b5 = "繁";
$ip20_lang_gb = "简";
$ip20_powerby = "<span class=\"footertext\">Powered by</span> <a href=\"http://www.eclass.com.hk\" target=\"_blank\"><img src=\"$image_path/2007a/logo_eclass_footer.gif\" border=\"0\" align=\"absmiddle\"></a>";
$ip20_welcome = "欢迎";
$ip20_event_today = "今日事项";
$ip20_event_month = "本月事项";
$ip20_esurvey = "问卷";
$ip20_enotice = "通告";
$ip20_reminder = "提醒";
$ip20_what_is_new = "校园最新消息";
$ip20_loading = "载入中";
$ip20_public = "公众";
$ip20_my_group = "我的小组";


################################################################################################
$i_ReportCard = "成绩表";
$i_ReportCard_System = "成绩表系统";
$i_ReportCard_System_Setting = "成绩表系统设定";
$i_ReportCard_System_Admin_User_Setting = "设定管理用户";
$i_ReportCard_All_Year = "全部年度";
$eReportCard["AdminUser"] = "管理用户";
$eReportCard["TeachingAppointmentSettings"] = "教师职务设定";
$eReportCard['AssignSubjectClassWarning'] = "请选择班别！";
$eReportCard['Code'] = "科目代码";
$eReportCard['SubjectName'] = "科目名称";
$i_ReportCard_Teaching_ImportInstruction = "<b><u>档案说明:</u></b><br>
第一栏 : 教师内联网帐号 (如 tmchan )。<br>
第二栏 : 任教的科目代码 (如 ENG)。 <br>
第三栏 : 任教的班别 (如 1A;1B, 班别名称以';'分隔)。<br>
班别及科目代码必须完全正确 (分大小写)<br><br>
<b>注意现时设定将会被取代。</b>";


#####
$i_StudentGuardian['MenuInfo'] = "监护人资料";
$i_StudentGuardian_all_students="所有学生";
$i_StudentGuardian_MainContent="主监护人";
$i_StudentGuardian_SMSPhone="SMS 号码";
$i_StudentGuardian_Phone="联络号码";
$i_StudentGuardian_EMPhone="紧急联络号码";
$i_StudentGuardian_GuardianName="监护人名称";
$i_StudentGuardian_SMS="SMS";
$i_StudentGuardian_warning_enter_chinese_english_name="请输入监护人的英文或中文姓名";
$i_StudentGuardian_warning_enter_phone_emphone ="请输入$i_StudentGuardian_Phone 或 $i_StudentGuardian_EMPhone";
$i_StudentGuardian_warning_Delete_Main_Guardian="你选择了删除主监护人，请设定主监护人。";
$i_StudentGuardian_warning_Delete_SMS="你选择了删除SMS联络号码，请选择其他号码。";
$i_StudentGuardian_Additional_Searching_Criteria="附加搜寻条件";
$i_StudentGuardian_warning_PleaseSelectClass="请选择最少一个班别";
$i_StudentGuardian_Back_To_Search_Result="返回搜寻结果";
$ec_guardian['01'] = "父亲";
$ec_guardian['02'] = "母亲";
$ec_guardian['03'] = "祖父";
$ec_guardian['04'] = "祖母";
$ec_guardian['05'] = "兄/弟";
$ec_guardian['06'] = "姊/妹";
$ec_guardian['07'] = "亲戚";
$ec_guardian['08'] = "其他";
$ec_iPortfolio['relation'] = "关系";
$ec_iPortfolio['guardian_import_remind'] = "注意: <Br> - 第一栏为RegNo 编号, 应加上#（如 \"#0025136\")，以确保编号数字的完整！ <Br> - 第二栏为监护人的英文姓名 <Br> - 第三栏为监护人的中文姓名 <Br> - 第四栏为监护人的联络号码 <Br> - 第五栏为监护人的紧急联络号码 <Br> - *第六栏为监护人与学生关系 <Br> - 第七栏为设定为学生的主监护人 (1 - 是, 0 - 否) <Br> - 第八栏为设定为SMS的接收者 (1 - 是, 0 - 否) <br> * (01 - 父亲, 02 - 母亲, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 亲戚, 08 - 其他) <Br> - <font color=red>新汇入的监护人资料将会覆盖现有的监护人资料。</font><br>";

$ec_iPortfolio['guardian_import_remind_sms'] = "注意: <Br> - 第一栏为RegNo 编号, 应加上#（如 \"#0025136\")，以确保编号数字的完整！ <Br> - 第二栏为监护人的英文姓名 <Br> - 第三栏为监护人的中文姓名 <Br> - 第四栏为监护人的联络号码 <Br> - 第五栏为监护人的紧急联络号码 <Br> - * 第六栏为监护人与学生关系 <Br> - 第七栏为设定为学生的主监护人 (1 - 是, 0 - 否) <Br> - 第八栏为设定为SMS的接收者 (1 - 是, 0 - 否) <br> - 第九栏为设定监护人做紧急联络 (1 - 是, 0 - 否) <br> * (01 - 父亲, 02 - 母亲, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 亲戚, 08 - 其他) <Br> - <font color=red>新汇入的监护人资料将会覆盖现有的监护人资料。</font><br>";

$ec_iPortfolio['guardian_import_remind_studentregistry'] = "注意: <Br> - 第一栏为RegNo 编号, 应加上#（如 \"#0025136\")，以确保编号数字的完整！ <Br> - 第二栏为监护人的英文姓名 <Br> - 第三栏为监护人的中文姓名 <Br> - 第四栏为监护人的联络号码 <Br> - 第五栏为监护人的紧急联络号码 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六栏为监护人与学生关系 <Br> - 第七栏为设定为学生的主监护人 (1 - 是, 0 - 否) <Br> - 第八栏为设定监护人做紧急联络 (1 - 是, 0 - 否) <Br> - 第九栏为监护人职业 <Br> - 第十栏为监护人是否与该学生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> 第十一栏为地址区码<Br> - 第十二栏为住址街道 <Br> - 第十三栏为住址其他资料，如大厦，层数及单位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父亲, 02 - 亲, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 亲戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> (O - 其他, M - 澳门, T - &#27705;仔, C - 路环) <Br> - <font color=red>新汇入的监护人资料将会覆盖现有的监护人资料。</font><br>";



$ec_iPortfolio['guardian_import_remind_sms_studentregistry'] = "注意: <Br> - 第一栏为RegNo 编号, 应加上#（如 \"#0025136\")，以确保编号数字的完整！ <Br> - 第二栏为监护人的英文姓名 <Br> - 第三栏为监护人的中文姓名 <Br> - 第四栏为监护人的联络号码 <Br> - 第五栏为监护人的紧急联络号码 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六栏为监护人与学生关系 <Br> - 第七栏为设定为学生的主监护人 (1 - 是, 0 - 否) <Br> - 第八栏为设定为SMS的接收者 (1 - 是, 0 - 否) <br> - 第九栏为设定监护人做紧急联络 (1 - 是, 0 - 否)  <Br> - 第十栏为监护人职业 <Br> - 第十一栏为监护人是否与该学生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> 第十二栏为地址区码<Br> - 第十三栏为住址街道 <Br> - 第十四栏为住址其他资料，如大厦，层数及单位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父亲, 02 - 母亲, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 亲戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> (O - 其他, M - 澳门, T - &#27705;仔, C - 路环) <Br> - <font color=red>新汇入的监护人资料将会覆盖现有的监护人资料。</font><br>";


$ec_yyc['yyc_export_usage'] = "汇出给offline system";

$i_StudentGuardian_Searching_Area = "搜寻范围";


### new smartcard
$StatusRecord = "考勤纪录";
$MonthlyRecord = "每月考勤纪录";
$DisplayAllPhoto = "显示所有相片";
$HiddenAllPhoto = "隐藏所有相片";
$OverallRecord = "纪录一览";

### PPC
$i_PPC_SmartCard_Take_Attendance_In_School_TimeLocation="时间 / 地点";
$i_PPC_SmartCard_Take_Attendance_Show_LateAbsentOuting="只显示迟到 / $i_StudentAttendance_Status_PreAbsent";
$i_PPC_SmartCard_Take_Attendnace_Show_AllStatus ="显示所有状况";
$i_ppc_home_title ="Mobile eClass";
$i_ppc_menu_home ="首页";
### eFilling
$eFilling = "eFilling";

### iAccount
$iAccount = "iAccount";
$iAccount_Account = "户口";
$AllRecords = "全部纪录";

###############################################################################################################
##### eEnrollment Begin #######################################################################################
###############################################################################################################

$eEnrollmentMenu['act_attendance_mgt'] = "活动出席管理";
$eEnrollmentMenu['act_date_time'] = "活动日期";
$eEnrollmentMenu['act_event_further_mgt'] = "活动进阶管理";
$eEnrollmentMenu['act_event_status_stat'] = "活动报名情况一览";
$eEnrollmentMenu['act_event_status_stat_overall'] = "学会报名情况统计";
$eEnrollmentMenu['act_event_status_average_attendance'] = "平均出席总括";
$eEnrollmentMenu['act_status_stat'] = "学会报名情况一览";
$eEnrollmentMenu['add_act_event'] = "新增活动";
$eEnrollmentMenu['attendance_mgt'] = "学生出席管理及统计";
$eEnrollmentMenu['attendance_mgt_stu_attend'] = "学生出席　";
$eEnrollmentMenu['attendance_mgt_stu_stat'] = "学生出席统计";
$eEnrollmentMenu['cat_setting'] = "类别";
$eEnrollmentMenu['cat_title'] = "类别名称";
$eEnrollmentMenu['class_lvl_setting'] = "级别设定";
$eEnrollmentMenu['club_attendance_mgt'] = "学会出席管理";
$eEnrollmentMenu['club_enroll'] = "学会报名";
$eEnrollmentMenu['club_enroll_status'] = "学会报名情况";
$eEnrollmentMenu['data_batch_process'] = "资料同步处理";
$eEnrollmentMenu['enroll_title'] = "我的报名概况";
$eEnrollmentMenu['enrollment_sys_setting'] = "设定";
$eEnrollmentMenu['enrollment_basic_setting'] = "报名基本设定";
$eEnrollmentMenu['event_enroll'] = "活动报名";
$eEnrollmentMenu['gro_setting'] = "管理学会";
$eEnrollmentMenu['head_count'] = "人数";
$eEnrollmentMenu['mgt_act_event'] = "管理";
$eEnrollmentMenu['mgt_cat'] = "管理类别";
$eEnrollmentMenu['mgt_event'] = "管理活动";
$eEnrollmentMenu['percentage'] = "百分比";
$eEnrollmentMenu['title'] = "课外活动系统";
$eEnrollmentMenu['user_permission_mgt'] = "使用者权限管理";
$eEnrollmentMenu['user_set'] = "使用者身份设定";
## added on 13 Aug 2008
$eEnrollmentMenu['reports'] = "报告";
$eEnrollmentMenu['overall_perform_report'] = "表现总括报告";
## added on 01/09/2008 Yat Woon
$eEnrollmentMenu['transition'] = "传送";
$eEnrollmentMenu['transfer_to_ole'] = "传送至其他学习经历";
## added on 5 Sept 2008
$eEnrollmentMenu['club'] = "学会";
$eEnrollmentMenu['activity'] = "活动";
$eEnrollment['tab']['management'] = "管理";
$eEnrollment['tab']['process'] = "进度";
$eEnrollment['tab']['attendance'] = "出席率";
## added on 10 Sept 2008
$eEnrollment['tab']['record'] = "纪录";
## added on 11 Sept 2008
$eEnrollmentMenu['overall_perform'] = "表现总括";
$eEnrollmentMenu['class_level_access'] = "班级权限";
$eEnrollmentMenu['attendance_taking'] = "更新出席率";
## added on 02 Dec 2008
$eEnrollmentMenu['data_handling_to_SP'] = "资料处理(转移至学生档案)";
$eEnrollmentMenu['data_handling_to_OLE'] = "资料处理(转移至其他学习经历)";
$eEnrollmentMenu['data_handling_to_WebSAMS'] = "资料传送(至网上校管系统)";
## added on 23 Mar 2009
$eEnrollmentMenu['enrolment_summary'] = "报名总括";
$eEnrollment['enrolment_summary_report'] = "报名总括报告";

$eEnrollment['absent'] = "不出席";
$eEnrollment['act_attendence'] = "活动出席率概要";
$eEnrollment['act_most_update_avg_attendance'] = "最新平均出席总括";
$eEnrollment['act_name'] = "活动名称";
$eEnrollment['accept_apply'] = "接受申请";
$eEnrollment['activity'] = "活动";
$eEnrollment['add_student_conflict_warning'] = "<font color=\"#ff0000\">以下学生因为时间冲突问题而未能成为活动成员。</font>";
$eEnrollment['add_payment']['step_1'] = "缴费细则";
$eEnrollment['add_payment']['step_2'] = "津贴";
$eEnrollment['all'] = "全部";
$eEnrollment['app_period'] = "报名时段";
$eEnrollment['app_stage'] = "报名阶段";
$eEnrollment['app_stu_list'] = "报名学生名单";
$eEnrollment['approve_period'] = "小组批核时段";
$eEnrollment['approve_stage'] = "小组批核阶段";
$eEnrollment['approved'] = "已批核";
$eEnrollment['approved_stu_list'] = "参加学生名单";
$eEnrollment['attendence'] = "出席率";
$eEnrollment['amount_paid_by_student'] = "学生支付金额";
$eEnrollment['amount_paid_by_school'] = "校方津贴余额";
$eEnrollment['back_to_event_index'] = "回到管理活动";
$eEnrollment['back_to_group_index'] = "回到管理学会";
$eEnrollment['backup_activity_btn'] = "把活动资料加入至学生档案";
$eEnrollment['backup_activity_confirm'] = "你确定要把资料加入学生档案？";
$eEnrollment['belong_to_group'] = "所属学会";
$eEnrollment['club_name'] = "学会名称";
$eEnrollment['club_quota'] = "会员名额";
$eEnrollment['club_setting'] = "活动设定";
$eEnrollment['conflict_warning'] = "<font color=\"#ff0000\">时间冲突</font>";
$eEnrollment['debit_directly'] = "直接缴费";
$eEnrollment['debit_later'] = "拍卡收费";
$eEnrollment['debit_method'] = "缴费方式";
$eEnrollment['decline_apply'] = "拒绝申请";
$eEnrollment['default_enroll_max_club'] = "学生可参加学会数目上限";
$eEnrollment['default_max_club'] = "学生可申请学会数目上限";
$eEnrollment['default_min_club'] = "学生可申请学会数目下限";
$eEnrollment['denied'] = "已拒绝";
$eEnrollment['del_warning'] = "<font color=\"#ff0000\">如果你储存此页，所有已递交之申请将会被取消。</font>";
$eEnrollment['DoNoUseEnrolment'] = "不使用 eEnrolment";
$eEnrollment['js_del_warning'] = "如果你早前已输入此步骤内的资料，而现在修改并储存，所有已递交之申请将被删除。确定储存？";
$eEnrollment['draw'] = "抽签";
$eEnrollment['draw_and_end'] = "抽签并完成报名程序";
$eEnrollment['event_member'] = "活动参加者";
# added on 10 Sept 2008
$eEnrollment['member'] = "会员";

$eEnrollment['enroll_persontype'] = "申请者对象";
$eEnrollment['female'] = "女";
$eEnrollment['groupmates_list'] = "会员名单";
$eEnrollment['lucky_draw'] = "进行抽签";
$eEnrollment['mail_enroll_event_mail1'] = "活动报名程序已完成。";
$eEnrollment['mail_enroll_event_mail2'] = "你应该可在活动资讯中，寻找你已成功申请的活动资讯。";
$eEnrollment['mail_enroll_event_mail3'] = "如有问题，请联络系统管理员或负责课外活动的导师。";
$eEnrollment['mail_enroll_event_mail_fail'] = "你<STRONG>未能</STRONG>成功申请此活动。";
$eEnrollment['mail_enroll_event_mail_success'] = "你已<STRONG>成功</STRONG>申请此活动。";
$eEnrollment['mail_enroll_event_subject'] = "活动报名结果";
$eEnrollment['mail_enroll_club_mail1'] = "学会报名程序已完成。";
$eEnrollment['mail_enroll_club_mail2'] = "你可在学会资讯中，寻找已成功申请的学会资讯。";
$eEnrollment['mail_enroll_club_mail3'] = "如有问题，请联络系统管理员或负责课外活动的导师。";
$eEnrollment['mail_enroll_club_mail_fail'] = "你<STRONG>未能</STRONG>成功申请此学会。";
$eEnrollment['mail_enroll_club_mail_success'] = "你已<STRONG>成功</STRONG>申请此学会。";
$eEnrollment['mail_enroll_club_subject'] = "学会报名结果";
$eEnrollment['male'] = "男";
$eEnrollment['manage_app_stu_list'] = "已报名学生";
$eEnrollment['manage_apply_namelist'] = "已报名学生";
$eEnrollment['manage_groupmates_list'] = "组员";
$eEnrollment['manage_namelist'] = "管理名单";
$eEnrollment['meeting'] = "聚会";
$eEnrollment['next_app_stage'] = "进行下一轮报名程序";
$eEnrollment['no_limit'] = "无限制";
$eEnrollment['no_name'] = "无名称";
$eEnrollment['no_record'] = "没有纪录";
$eEnrollment['parent'] = "家长";
$eEnrollment['payment'] = "缴费";
$eEnrollment['PaymentAmount'] = "暂定费用 HK($)";
$eEnrollment['payment_item'] = "缴费项目";
$eEnrollment['position'] = "排位";
$eEnrollment['present'] = "出席";
$eEnrollment['process_end'] = "程序完成";
$eEnrollment['quota_left'] = "余额";
$eEnrollment['set_admin'] = "设定为管理员";
$eEnrollment['status'] = "状态";
$eEnrollment['stu_club_rule'] = "预设学生只能选 <font color=\"#ff0000\"><!--MinClub--></font> 至 <font color=\"#ff0000\"><!--MaxChoice--></font> 个活动，学生最多可参加 <font color=\"#ff0000\"><!--MaxClub--></font> 个活动";
$eEnrollment['studnet_attendence'] = "学生出席率";
$eEnrollment['student_name'] = "学生姓名";
$eEnrollment['student'] = "学生";
$eEnrollment['system_user_list'] = "能使用系统之教职员名单";
$eEnrollment['system_user_name'] = "教职员姓名";
$eEnrollment['teachers_comment'] = "老师评语";
$eEnrollment['to'] = "至";
$eEnrollment['too_many_apply_process'] = "报名人数过多时的安排";
$eEnrollment['unset_admin'] = "取消管理员";
$eEnrollment['up_to'] = "截至";
$eEnrollment['use_system'] = "能够使用此系统";
$eEnrollment['waiting'] = "待批";
$eEnrollment['whole_stage_end'] = "传送通知予成功申请者";
$eEnrollment['year'] = "年度";

$eEnrollment['add_club_activity']['step_1'] = "设定资料";
$eEnrollment['add_club_activity']['step_1b'] = "委任职员及助手";
$eEnrollment['add_club_activity']['step_2'] = "设定活动日期及时间";

$eEnrollment['add_activity']['act_age'] = "对象年龄范围";
$eEnrollment['add_activity']['act_assistant'] = "活动点名助手";
$eEnrollment['add_activity']['act_category'] = "活动类型";
$eEnrollment['add_activity']['act_content'] = "活动内容";
$eEnrollment['add_activity']['act_date'] = "活动日期";
$eEnrollment['add_activity']['act_gender'] = "对象性别";
$eEnrollment['add_activity']['act_name'] = "活动名称";
$eEnrollment['add_activity']['act_pic'] = "活动负责人";
$eEnrollment['add_activity']['act_quota'] = "名额";
$eEnrollment['add_activity']['act_target'] = "对象级别";
$eEnrollment['add_activity']['act_time'] = "活动时段";
$eEnrollment['add_activity']['app_method'] = "报名方式";
$eEnrollment['add_activity']['app_period'] = "报名时段";
$eEnrollment['add_activity']['attachment'] = "附件";
$eEnrollment['add_activity']['age'] = "岁";
$eEnrollment['add_activity']['apply_role_type'] = "委任角色";
$eEnrollment['add_activity']['apply_ppl_type'] = "报名权限";
$eEnrollment['add_activity']['more_attachment'] = "更多附件";
$eEnrollment['add_activity']['parent_enroll'] = "由家长报名";
$eEnrollment['add_activity']['pickup_ppl'] = "选择参加者";
$eEnrollment['add_activity']['select'] = "选择";
$eEnrollment['add_activity']['set_select_period'] = "设定已选择时段";
$eEnrollment['add_activity']['set_all'] = "设定所有时段";
$eEnrollment['add_activity']['step_1'] = "定义活动";
$eEnrollment['add_activity']['step_1b'] = "委任职员及助手";
$eEnrollment['add_activity']['step_2'] = "设定活动日期及时间";
$eEnrollment['add_activity']['step_3'] = "设定报名权限";
$eEnrollment['add_activity']['student_enroll'] = "由学生报名";

$eEnrollment['front']['app_period'] = "报名时段：<!--AppStart--> 至 <!--AppEnd-->";
$eEnrollment['front']['applied'] = "已申请";
$eEnrollment['front']['approve_period'] = "小组批核时段：<!--AppStart--> 至 <!--AppEnd-->";
$eEnrollment['front']['enroll'] = "报名参加";
$eEnrollment['front']['enrolled'] = "成功参加";
$eEnrollment['front']['event_time_conflict'] = "<font color=\"#ff0000\">若你选取之活动有时间冲突，你只能参加其中一项。</font>";
$eEnrollment['front']['information'] = "资料";
$eEnrollment['front']['quota_warning'] = "<font color=\"#ff0000\">满额</font>";
$eEnrollment['front']['time_conflict_warning'] = "<font color=\"#ff0000\">若你选取之学会其聚会有时间冲突，你只能参加其中一个。</font>";
$eEnrollment['front']['wish_enroll_min'] = "你最少需要参加 <!--NoOfClub--> 个学会";
$eEnrollment['front']['wish_enroll_min_event'] = "你最少需要参加 <!--NoOfActivity--> 个活动";
$eEnrollment['front']['wish_enroll_front'] = "，希望最多参加&nbsp;";
$eEnrollment['front']['wish_enroll_end'] = "&nbsp;个活动。";
$eEnrollment['front']['wish_enroll_end_event'] = "&nbsp;个学会。";
$eEnrollment['approve_quota_exceeded'] = "人数超出限额，有学生未能加入此活动。";

$eEnrollment['js_confirm_del'] = "你是否确认删除所选项目？";
$eEnrollment['js_confirm_del_single'] = "你是否确认删除所选项目？";
$eEnrollment['js_payment_alert'] = "每项活动只能缴费一次，缴费实行之后不能修改，确定缴费？";
$eEnrollment['js_prepayment_alert'] = "每项活动只能进行缴费一次，并请先确定资料无误。你是否确认进行缴费？";
$eEnrollment['js_sel_pic'] = "请选择负责人。";
$eEnrollment['js_sel_student'] = "请选择参与学生。";
$eEnrollment['js_quota_exists'] = "人数超出限额，你选择了<!-- NoOfSelectedStudent-->位学生，请选择 <!-- NoOfStudent--> 位学生。";
$eEnrollment['js_sel_date'] = "请选择活动日期。";
$eEnrollment['js_time_invalid'] = "时间范围不适当。";
$eEnrollment['js_confirm_cancel'] = "你确定要取消申请？";
$eEnrollment['js_fee_positive'] = "暂定费用只能输入正数数字";
## added on 26 Aug 2008
//add_member.php
$eEnrollment['js_enter_role'] = "请输入学生角色";
## added on 03 Sept 2008
//add_member.php
$eEnrollment['js_zero_means_no_limit'] = "设定限额为 0 即等于没有限制";
# added on 24 Sept 2008
//active_member.php
$eEnrollment['js_update_active_member_per'] = "系统会因应新的出席率计算学生是否活跃会员，要继续？";
## added on 29 Sept 2008
//payment.php
$eEnrollment['js_amount_cannot_be_zero'] = "项目收费必须多于零";
## added on 02 Oct 2008
$eEnrollment['js_enrol_one_only'] = "学会之间有时间冲突，你只能申请其中一个学会。";
## added on 09 Oct 2008
$eEnrollment['js_percentage_alert'] = "输入之百分比必须为 0.00-100.00% ";
$eEnrollment['js_hours_alert'] = "输入之时数必须是正数。";
## added on 11 Oct 2008
$eEnrollment['js_target_group_alert'] = "请至少选择一组活动对象。";
$eEnrollment['js_no_active_member_club'] = "以上学会皆没有活跃会员";
$eEnrollment['js_no_active_member_activity'] = "以上活动皆没有活跃会员";
$eEnrollment['js_close_compose_email_window'] = "如关闭此视窗，你将失去所有已修改的内容。你是否确定要关闭此视窗？";


$eEnrollment['month'][0] = "十二月";
$eEnrollment['month'][1] = "一月";
$eEnrollment['month'][2] = "二月";
$eEnrollment['month'][3] = "三月";
$eEnrollment['month'][4] = "四月";
$eEnrollment['month'][5] = "五月";
$eEnrollment['month'][6] = "六月";
$eEnrollment['month'][7] = "七月";
$eEnrollment['month'][8] = "八月";
$eEnrollment['month'][9] = "九月";
$eEnrollment['month'][10] = "十月";
$eEnrollment['month'][11] = "十一月";
$eEnrollment['month'][12] = "十二月";
$eEnrollment['month'][13] = "一月";

$eEnrollment['pay']['amount'] = "项目收费";
$eEnrollment['pay']['item_name'] = "项目名称";

$eEnrollment['select']['helper'] = "活动点名助手";
$eEnrollment['select']['pic'] = "活动负责人";

$eEnrollment['setting']['club_lower_limit'] = "能申请之学会数目上限";
$eEnrollment['setting']['club_upper_limit'] = "能申请之学会数目下限";

$eEnrollment['user_role']['normal_user'] = "Normal User 一般使用者";
$eEnrollment['user_role']['enrollment_master'] = "Enrollment Master 学会报名主任";
$eEnrollment['user_role']['enrollment_admin'] = "Enrollment Admin 学会报名管理员";

$eEnrollment['weekday']['sun'] = "日";
$eEnrollment['weekday']['mon'] = "一";
$eEnrollment['weekday']['tue'] = "二";
$eEnrollment['weekday']['wed'] = "三";
$eEnrollment['weekday']['thu'] = "四";
$eEnrollment['weekday']['fri'] = "五";
$eEnrollment['weekday']['sat'] = "六";

$eEnrollment['skip'] = "跳过此步骤";

### added on 19 Jun 2008
$eEnrollment['payment_success_list'] = "已成功处理以下缴费项目：";
$eEnrollment['payment_fail_list'] = "未能处理以下缴费项目：";
$eEnrollment['payment_status_success'] = "缴费成功。";
$eEnrollment['payment_status_fail'] = "缴费失败，户口结余不足。";
$eEnrollment['payment_summary'] = "缴费总结";

$eEnrollment['attendance']['present'] = "出席";
$eEnrollment['attendance']['absent'] = "缺席";

$eEnrollment['role'] = "角色";
$eEnrollment['class_name'] = "班别";
$eEnrollment['class_number'] = "学号";
$eEnrollment['student_name'] = "姓名";
$eEnrollment['performance'] = "表现";
$eEnrollment['comment'] = "意见";
$eEnrollment['member_perform_comment_list'] = "组员表现及老师评语";
$eEnrollment['student_perform_comment_list'] = "学生表现及老师评语";

### added on 04 Aug 2008
$eEnrollment['club'] = "学会";
$eEnrollment['selection_mode'] = "选择模式";

$eEnrollment['assigning_member'] = "加入会员";
$eEnrollment['assigning_student'] = "加入参加者";
$eEnrollment['assigning_enrolment'] = "加入报名学生";

### added on 05 Aug 2008
// import.php
$eEnrolment['DownloadCSVFile'] = "下载CSV范本";
$eEnrolment['AlertSelectFile'] = "请选择档案";
$eEnrolment['attendance_title'] = "出席资料";

### added on 07 Aug 2008
// import_result.php
$eEnrollment['invalid_date'] = "事项日期时间无效";
$eEnrollment['student_not_found'] = "找不到学生资料";
$eEnrollment['student_not_member'] = "学生不是会员";
$eEnrollment['record_existed'] = "已有该记录";
$eEnrollment['empty_row'] = "没有输入资料";
$eEnrollment['student_not_event_student'] = "学生没有参加此活动";
$eEnrollment['club_not_found'] = "没有此学会";
$eEnrollment['activity_not_found'] = "没有此活动";
$eEnrollment['clubs_with_same_name'] = "学会名称重复";
$eEnrollment['activity_with_same_name'] = "活动名称重复";
$eEnrollment['invalid_active_member_status'] = "活跃状况不正确";
$eEnrollment['member_added'] = "已加入会员资料";
$eEnrollment['member_updated'] = "已更新会员资料";
$eEnrollment['participant_added'] = "已加入参加者资料";
$eEnrollment['participant_updated'] = "已更新参加者资料";

$eEnrollment['import_status'] = "汇入状况";

$eEnrollment['back_to_enrolment'] = "回到报名情况";
$eEnrollment['back_to_attendance'] = "回到出席管理";

//member_index and event_member_index
$eEnrollment['import_performance_comment'] = "汇入表现资料及老师评语";

// import.php
$eEnrollment['Attendance_Import_FileDescription'] = "\"班别\",\"学号\",\"活动日期(YYYY-MM-DD)\",\"活动开始时间(HH:MM)\"";
$eEnrollment['Performance_Import_FileDescription'] = "\"班别\",\"学号\",\"表现\",\"老师评语\"";
$eEnrollment['Member_Import_FileDescription'] = "\"班别\",\"学号\",\"角色\",\"表现\",\"老师评语\",\"活跃/非活跃\"";
$eEnrollment['All_Member_Import_FileDescription'] = "\"学会名称\", \"班别\",\"学号\",\"角色\",\"表现\",\"老师评语\",\"活跃/非活跃\"";
$eEnrollment['All_Participant_Import_FileDescription'] = "\"活动名称\", \"学号\",\"学号\",\"角色\",\"表现\",\"老师评语\",\"活跃/非活跃\"";
$eEnrollment['activity_start_time'] = "活动开始时间";
$eEnrollment['activity_title'] = "活动名称";
$eEnrollment['performance_title'] = "表现及老师评语";
$eEnrollment['csv_header']['club_name'] = "学会名称";
$eEnrollment['csv_header']['activity_title'] = "活动名称";

### added on 11 Aug 2008
//add_member.php
$eEnrollment['add_student_result'] = "加入学生结果";
$eEnrollment['select_student'] = "选择学生";

//add_member_update.php
$eEnrollment['student_is_member'] = "学生已经是会员";
$eEnrollment['student_is_joined_event'] = "学生已参加此活动";
$eEnrollment['crash_group'] = "与其他学会发生时间冲突";
$eEnrollment['crash_activity'] = "与其他活动发生时间冲突";
$eEnrollment['crash_group_activity'] = "与其他学会及活动发生时间冲突";

//add_member_result.php
$eEnrollment['back_to_member_list'] = "回到会员名单";
$eEnrollment['back_to_student_list'] = "回到学生名单";
$eEnrollment['back_to_applicant_list'] = "回到报名名单";
$eEnrollment['add_others_student'] = "加入其他学生";
$eEnrollment['back_to_club_management'] = "回到学会管理";
$eEnrollment['back_to_activity_management'] = "回到活动管理";

### added on 12 Aug 2008
//get_sample_csv.php
$eEnrollment['sample_csv']['ClassName'] = "班别";
$eEnrollment['sample_csv']['ClassNumber'] = "学号";

//basic.php
$eEnrollment['disable_status'] = "缴费后不允许更改申请者状态";
$eEnrollment['active_member_per'] = "活跃组员之最低出席率要求";
$eEnrollment['active_member_per_short'] = "成为活跃会员的最低出席率要求";

$eEnrollment['active_member'] = "活跃会员";
$eEnrollment['in_active_member'] = "非活跃会员";

## added on 01/09/2008 Yat Woon
$eEnrollment['Record_Type'] = "纪录类别";
$eEnrollment['All_Records'] = "所有纪录";
$eEnrollment['Club_Records'] = "学会纪录";
$eEnrollment['Activity_Records'] = "活动纪录";
$eEnrollment['Transfer'] = "传送";
$eEnrollment['Not_Transfer'] = "不传送";
$eEnrollment['Date'] = " 日期";
$eEnrollment['Period'] = "时段";
$eEnrollment['No_of_Student'] = "学生人数";
$eEnrollment['Component_of_OLE '] = "其他学习经历种类";
$eEnrollment['transfer_finish']['step1'] = "选择纪录类别";
$eEnrollment['transfer_finish']['step2'] = "设定细则";
$eEnrollment['transfer_finish']['step3'] = "覆写纪录";
$eEnrollment['transfer_finish']['step4'] = "完成";
$eEnrollment['duplicate_ole_records']  = "如学会或活动之其他学习经历重复，取替其纪录？";
$eEnrollment['duplicate_ole_participant_record']  = "如参与学生之纪录重复，取替其纪录？";
$eEnrollment['overwrite']  = "取替";
$eEnrollment['skip2']  = "略过";
$eEnrollment['Already_Transfered']  = "已传送";
$eEnrollment['Transfer_Another_OLE']  = "传送其他纪录";
$eEnrollment['transfer_student_setting']  = "纪录传送对象";
$eEnrollment['all_member']  = "全部会员";
$eEnrollment['active_member_only']  = "只限活跃会员";
$eEnrollment['OLE_Category'] = "纪录类别";

## added on 13 Aug 2008
//basic.php
$eEnrollment['active_member_alert'] = "活跃学生之最低出席率必须为 0.00-100.00%";

//overall_performance_report.php
$eEnrollment['total_performance'] = "整体表现";

## added on 14 Aug 2008
//overall_performance_report.php
$eEnrollment['total_club_performance'] = "学会表现总分";
$eEnrollment['total_act_performance'] = "活动表现总分";

## added on 21 Aug 2008
$eEnrollment['notification_letter'] = "通知信";

## added on 25 Aug 2008
//overall_performance_report.php
$eEnrollment['print_class_enrolment_result'] = "列印整班课外活动报名结果";
$eEnrollment['print_club_enrolment_result'] = "列印整个学会报名结果";

//add_member_result.php
$eEnrollment['club_quota_exceeded'] = "超出学会限额";
$eEnrollment['student_quota_exceeded'] = "超出学生可加入学会之限额";
$eEnrollment['act_quota_exceeded'] = "超出活动限额";
$eEnrollment['rejected'] = "已拒绝";

//basic.php
$eEnrollment['one_club_when_time_crash'] = "如出现时间冲突，学生只允许参加其中一个学会";
$eEnrollment['hide_club_if_quota_full'] = "如报名人数达到该学会之限额，所有学生将不能报名参加该学会";

## added on 26 Aug 2008
//add_member_result.php
$eEnrollment['time_crash'] = "时间冲突";
$eEnrollment['club_or_act_title'] = "学会/活动及其时间";

## added on 28 Aug 2008
//payment_summary.php
$eEnrollment['back_to_club_enrolment'] = "回到学会报名情况一览";
$eEnrollment['back_to_act_enrolment'] = "回到活动报名情况一览";

//search box initial text
$eEnrollment['enter_name'] = "输入姓名";
$eEnrollment['enter_student_name'] = "输入学生姓名";

## added on 10 Sept 2008
$eEnrollment['enable_online_registration'] = "启用网上登记";
$eEnrollment['disable_online_registration'] = "停用网上登记";
$eEnrollment['current_title'] = "现有名称";
$eEnrollment['new_title'] = "新名称";

## added on 11 Sept 2008
$eEnrollment['waiting_list'] = "候批";
$eEnrollment['number_of_applicants'] = "报名人数";
$eEnrollment['process'] = "处理";
$eEnrollment['participant'] = "参加人数";
$eEnrollment['participant2'] = "参加者";

## added on 12 Sept 2008
$eEnrollment['processed'] = "已处理";

## added on 17 Sept 2008
$eEnrollment['quota_exceeded'] = "超出限额";
$eEnrollment['default_school_quota_exceeded'] = "超出学校预设限额";
//basic_event.php
$eEnrollment['default_max_act'] = "学生可申请活动数目上限";
$eEnrollment['default_min_act'] = "学生可申请活动数目下限";
$eEnrollment['default_enroll_max_act'] = "学生可参加活动数目上限";
$eEnrollment['one_act_when_time_crash'] = "如出现时间冲突，学生只允许参加其中一个活动";

## added on 26 Sept 2008
$eEnrollment['Edit_Club_Info'] = "编辑学会资料";
$eEnrollment['crash_with'] = "时间冲突：";

## added on 29 Sept 2008
$eEnrollment['app_stage_event'] = "以下设定有效期为";
$eEnrollment['event_and'] = "至";

## added on 02 Oct 2008
$eEnrollment['warn_no_more_enrolment'] = "<font color=\"red\">你参予的学会数目已超出学校限额，你不能申请其他学会。</font>";

## added on 09 Oct 2008
$eEnrollment['total_hours_of_student'] = "传送实际参与时数";
$eEnrollment['total_hours_if_above_percentage'] = "如出席率等于或高于 ";
$eEnrollment['total_hours_if_above_percentage2'] = "，传送整个活动的所有时数";
$eEnrollment['manual_input_hours'] = "自订传送时数值 - ";
$eEnrollment['unit_hour'] = "小时";
$eEnrollment['hours_setting'] = "时数传送设定";
$eEnrollment['transfer_if_zero_hour'] = "允许传送时数为0小时的纪录";
$eEnrollment['num_transferred_record'] = "已传送纪录数量";

## added on 10 Oct 2008
$eEnrollment['overall_performance_instruction'] = "表现总括报告只适用于表现<font color=\"red\"><strong>分数</strong></font>。您仍可于表现处输入文字，但所有文字将不会显示在表现总括报告中。";

## added on 20 Oct 2008
$eEnrollment['OLE_Components'] = "其他学习经历种类";

## added on 21 Oct 2008
$eEnrollment['club_act_in_OLE'] = "学会/活动于其他学习经历纪录";
$eEnrollment['student_OLE_records'] = "学生于其他学习经历纪录";
$eEnrollment['replace_all'] = "更新全部";
$eEnrollment['replace'] = "更新";

## added on 22 Oct 2008
$eEnrollment['global_settings'] = "整体设定";
$eEnrollment['Record_exists'] = "纪录己存在";	// for OLE program record
$eEnrollment['record_exists'] = "个纪录己存在";	// for OLE student record
$eEnrollment['record_in_OLE'] = "其他学习经历纪录数量";

## added on 3 Nov 2008
$eEnrollment['alert_all_enrolment_records_deleted_club'] = "警告：除学会资料外，所有纪录将被删除。";
$eEnrollment['alert_all_enrolment_records_deleted_activity'] = "警告：除活动资料外，所有纪录将被删除。";

## added on 14 Nov 2008
$eEnrollment['all_categories'] = "全部类别";

## added on 2 Dec 2008
$eEnrollment['enrolment_settings'] = "课外活动设定";
$eEnrollment['schedule_settings']['instruction'] = "请于日历内选择学会聚会/活动的举行日期。当日期被选取后，系统便会自动显示相应时槽以供设定。";
$eEnrollment['schedule_settings']['scheduling_dates'] = "设定活动日期";
$eEnrollment['schedule_settings']['scheduling_times'] = "设定活动时间";
$eEnrollment['date_chosen'] = "已选择日期";
$eEnrollment['button']['add&assign'] = "新增及委任";
$eEnrollment['button']['active_member'] = "检视及编辑".$eEnrollment['active_member'];
$eEnrollment['button']['notification_letter'] = "列印".$eEnrollment['notification_letter'];
$eEnrollment['button']['update_role'] = $button_update.$eEnrollment['role'];
$eEnrollment['select_role'] = "- 选择角色 -";
$eEnrollment['add_member_and_assign_role'] = "新增会员及委任角色";
$eEnrollment['add_participant_and_assign_role'] = "新增参加者及委任角色";
$eEnrollment['add_member'] = "新增会员";
$eEnrollment['add_participant'] = "新增参加者";
$eEnrollment['class_no'] = "学号";
$eEnrollment['time_conflict'] = "时间冲突";
$eEnrollment['lookup_for_clashes'] = "检查冲突";
$eEnrollment['succeeded'] = "成功";
$eEnrollment['failed'] = "失败";
$eEnrollment['active'] = "活跃";
$eEnrollment['inactive'] = "非活跃";
$eEnrollment['category'] = "类别";
$eEnrollment['view_statistics'] = "检视统计";
$eEnrollment['take_attendance'] = "记录出席情况";
$eEnrollment['unhandled'] = "候批";
$eEnrollment['drawing'] = "抽签";
$eEnrollment['new_activity'] = "新增活动";
$eEnrollment['edit_activity'] = "编辑活动";
$eEnrollment['participant_quota'] = "参加名额";
$eEnrollment['participant_list'] = "参加者名单";
$eEnrollment['enter_role'] = "输入角色";
$eEnrollment['attendance_rate'] = "出席率";
$eEnrollment['member_selection_and_drawing'] = "申请批核及抽签";
$eEnrollment['participant_selection_and_drawing'] = "Participant Selection & Drawing";
$eEnrollment['random_drawing'] = "随机抽签";
$eEnrollment['last_modified'] = "上一次更新";
$eEnrollment['confirm'] = "确定";
$eEnrollment['proceed_to_step2'] = "下一步";

$eEnrollment['next_round_instruction'] = "过渡至下一轮报名程序前，请按此按钮：";
$eEnrollment['button']['next_round'] = "清除未批核申请";
$eEnrollment['drawing_instruction'] = "要启动系统的<font color='red'><!--DrawingMethod--></font>派位，请按此按钮：";
$eEnrollment['button']['perform_drawing'] = "进行抽签";
$eEnrollment['student_enrolment_rule'] = "学生可选择<font color='red'><!--MinNumApp--></font>至<font color='red'><!--MaxNumApp--></font>个学会并最多可参加<font color='red'><!--MaxEnrol--></font>学会";
$eEnrollment['enrolment_period'] = "本轮报名程序时段：";
$eEnrollment['student_enrolment_rule_activity'] = "学生可选择<font color='red'><!--MinNumApp--></font>至<font color='red'><!--MaxNumApp--></font>个活动并最多可参加<font color='red'><!--MaxEnrol--></font>学会";
$eEnrollment['enrolment_period_activity'] = "报名时段：";

$eEnrollment['archive_instruction'] = "你可把课外活动小组的资料输出至学生档案，这些资料包括<b>活动名称，角色及表现</b>，并根据所属年度及学期输出";
$eEnrollment['archive_footer1'] = "如要更改上述年度及学期资料，请前往综合平台行政管理中心的内联网设定>系统设定>基本设定或联络系统管理员。";
$eEnrollment['archive_footer2'] = "你是否确认把课外活动资料加入到学生档案？";

$eEnrollment['attendence_record'] = "出席纪录";
$eEnrollment['import_member'] = "汇入会员";
$eEnrollment['import_participant'] = "汇入参加者";

$eEnrollment['Specify'] = "指定";
$eEnrollment['generate_or_update'] = "产生 / 更新报告";

$eEnrollment['send_club_enrolment_notification'] = "传送学会报名通知";
$eEnrollment['notify_by_email'] = "电邮提醒";
$eEnrollment['default_student_notification_title'] = "Reminder: Club Enrolment (deadline: <--deadline-->)";
$eEnrollment['default_student_notification_content'] = "Please be reminded to register clubs in eEnrolment by <--deadline-->!";
$eEnrollment['default_class_teacher_notification_title'] = "Reminder: Student Club Enrolment (deadline: <--deadline-->)";
$eEnrollment['default_class_teacher_notification_content'] = "Some of your students have not registerred any clubs. They are listed below. Please kindly remind them to register clubs in eEnrolment by <--deadline-->! \n";
$eEnrollment['to_student'] = "致学生";
$eEnrollment['to_class_teacher'] = "致班主任";
$eEnrollment['no_class_teacher_warning'] = "<--ClassList-->班没有班主任";
$eEnrollment['class'] = "班";
$eEnrollment['and'] = "及";
$eEnrollment['system_append_student_list_atuomatically'] = "系统会于电邮内容结尾自动加上该班的学生列表。";

$eEnrollment['success'] = "完成";
$eEnrollment['ImportOtherRecords'] = "汇入其他纪录";

$eEnrollment['Content'] = "内容";

$eEnrollment['Target'] = "对象";
$eEnrollment['AverageAttendance'] = "平均出席";

###############################################################################################################
##### eEnrollment End #########################################################################################
###############################################################################################################



## IP20 Menu language
$ip20TopMenu['iTools'] = "个人通讯管理工具";
$ip20TopMenu['iMail'] = "我的电邮";
$ip20TopMenu['iFolder'] = "我的文件夹";
$ip20TopMenu['iCalendar'] = "我的行事历";
$ip20TopMenu['iSmartCard'] = "我的智能卡纪录";
$ip20TopMenu['iPortfolio'] = "学习档案";
$ip20TopMenu['iAccount'] = "我的户口";

$ip20TopMenu['eAdmin'] = "学校行政管理工具";
$ip20TopMenu['eAttendance'] = "考勤管理";
$ip20TopMenu['eDiscipline'] = "训导管理";
$ip20TopMenu['eDisciplinev12'] = "训导管理";
$ip20TopMenu['eReportCard'] = "成绩表管理";
$ip20TopMenu['eMarking'] = "网上试卷批改";
$ip20TopMenu['eHomework'] = "网上家课表";
$ip20TopMenu['eEnrollment'] = "课外活动管理";
$ip20TopMenu['eSports'] = "运动会管理";
$ip20TopMenu['SwimmingGala'] = "水运会管理";
$ip20TopMenu['eOffice'] = "网上校务处";
$ip20TopMenu['ePayment'] = "收费管理";
$ip20TopMenu['eBooking'] = "预订管理";
$ip20TopMenu['eInventory'] = "资产管理行政系统";
$ip20TopMenu['eSurvey'] = "问卷管理";
$ip20TopMenu['eTimeTabling'] = "编课管理";

$ip20TopMenu['eLearning'] = "学与教管理工具";
$ip20TopMenu['eClass'] = "网上教室";
$ip20TopMenu['eLC'] = "网上课件中心";
$ip20TopMenu['eMeeting'] = "网上视像会议";
$ip20TopMenu['LSLP'] = "通识学习平台";
$ip20TopMenu['eLibrary'] = "电子图书";
if (isset($plugin['eLib_trial']) && $plugin['eLib_trial'])
{
	$ip20TopMenu['eLibrary'] .= " (试用版)";
}
$ip20TopMenu['eTV'] = "网上电视台";

$ip20TopMenu['eCommunity'] = "社群管理工具";
$ip20TopMenu['eGroups'] = "校内社群";
$ip20TopMenu['eParents'] = "家长会";
$ip20TopMenu['eAlumni'] = "校友会";

$ip20TopMenu['eService'] = "资讯服务";
$ip20TopMenu['SLSLibrary'] = "SLS图书馆系统";

$eOffice = $ip20TopMenu['eOffice'];

$i_SMS_Personalized_Tag['user_name_eng'] = $i_UserEnglishName;
$i_SMS_Personalized_Tag['user_name_chi'] = $i_UserChineseName;
$i_SMS_Personalized_Tag['user_class_name'] = $i_ClassName;
$i_SMS_Personalized_Tag['user_class_number'] = $i_ClassNumber;
$i_SMS_Personalized_Tag['user_login'] = $i_UserLogin;
$i_SMS_Personalized_Tag['attend_card_time'] = "进入/离开之拍卡时间";
$i_SMS_Personalized_Tag['payment_balance'] = $i_Payment_PrintPage_Outstanding_AccountBalance;
$i_SMS_Personalized_Tag['payment_outstanding_amount'] = $i_Payment_PrintPage_Outstanding_LeftAmount;

$i_SMS_SentRecords = "检视已发送讯息";
$i_SMS_MessageCount = "短讯数目";


$i_license_sys_msg = "你的学生用户许可证有<!--license_total-->个，尚有<!--license_left-->个可供使用。你已使用的许可证为<!--license_used-->个。";
$i_license_sys_msg_none = "没有学生用户许可证可用!";

###### iMail Spam Control ######
$i_spam['FolderSpam'] =  "杂件箱";
$i_spam['Setting'] = "过滤电邮防护服务设定";
$i_spam['WhiteList'] = "白名单";
$i_spam['BlackList'] = "黑名单";
$i_spam['BypassSetting'] = "使用设定";
$i_spam['SpamControl'] = "垃圾邮件控制";
$i_spam['VirusScan'] = "扫毒程式";
$i_spam['Option_Activated'] = "使用";
$i_spam['Option_Bypass'] = "绕过";
$i_spam['EmailAddress'] = "电邮地址";
$i_spam['AlertRemove'] = "你确定要移除这项纪录?";
# added by Ronald on 20080807 #
$i_spam['SpamControlLevel'] = "垃圾邮件控制程度";
$i_spam['SpamControlLevel_DefaultLevel'] = "使用预设程度";
$i_spam['SpamControlLevel_CustomLevel'] = "使用自设程度";
$i_spam['js_Alert_SpamControlLevelEmpty'] = "请输入垃圾邮件控制程度";
$i_spam['js_Alert_SpamControlLevelNotVaild'] = "请输入有效的垃圾邮件控制程度";
# added by Ronald on 20080812 #
$i_spam['CustoSpamControlLevel_Description'] = "* - 数值越少代表垃圾邮件控制程度越高，反之亦然。范围：0 - 6.0";
$i_Campusmail_MailQuotaSettting_SelectGroupWarning = "请选择组别";

####################################
### Student Leave School Option ####
### Added On 29 Aug 2007 ###
$i_StudentAttendance_Menu_OtherFeatures_LeaveSchoolOption = "学生离校选项";
$i_StudentAttendance_LeaveOption_Weekday = array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
$i_StudentAttendance_LeaveOption_Selection = array("无","家长接送","校车","自行离开");
$i_StudentAttendance_LeaveOption_ImportFileDescription = "
第一栏 : 班别 <br>
第二栏 : 班号 <br>
第三栏 : * <b>星期一</b> 离校选项 <br>
第四栏 : * <b>星期二</b> 离校选项 <br>
第五栏 : * <b>星期三</b> 离校选项 <br>
第六栏 : * <b>星期四</b> 离校选项 <br>
第七栏 : * <b>星期五</b> 离校选项 <br>
第八栏 : * <b>星期六</b> 离校选项 <br>
<br>
*&nbsp;0 - 无选项 <br>
&nbsp;&nbsp;&nbsp;1 - 家长接送 <br>
&nbsp;&nbsp;&nbsp;2 - 校车 <br>
&nbsp;&nbsp;&nbsp;3 - 自行离开
";
############################

### Added On 31 Aug 2007 ###
$i_studentAttendance_LeaveOption_ClassSelection_Warning = "请选择班别";

$i_studentAttendance_LeaveOption_Import_ErrorMsg[1] = "<b>星期一</b> 离校选项 错误";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[2] = "<b>星期二</b> 离校选项 错误";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[3] = "<b>星期三</b> 离校选项 错误";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[4] = "<b>星期四</b> 离校选项 错误";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[5] = "<b>星期五</b> 离校选项 错误";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[6] = "<b>星期六</b> 离校选项 错误";
$i_studentAttendance_LeaveOption_Import_ErrorMsg[7] = "班别/学号 错误";
############################

### Added On 1 Sep 2007 ###
$i_studentAttendance_ImportFailed_Reason = "错误原因";
###########################

### Added On 3 Sep 2007 ###
$attendst_leave_option = array(
array(0,"无"),
array(1,"家长接送"),
array(2,"校车"),
array(3,"自行离开"),
);

$i_studentAttendance_ConfirmMsg = "<font color=red>请确认以下记录正确, 然后按呈送储存</font>";
### End Of Student Leave School Option ###
##########################################


### Added On 4 Sep 2007 ###
$i_Staff_Patrol = "教师巡查记录系统";


# eInventory words
$i_InventorySystem['eInventory'] = "资产管理行政系统";
$i_InventorySystem['InventorySystemSetting'] = "资产管理行政系统设定";
$i_InventorySystem['AdminUserSetting'] = "设定管理用户";
$i_InventorySystem['AdminUser'] = "管理用户";
$i_InventorySystem['Inventory'] = "物品清单";
$i_InventorySystem['Category'] = "类别";
$i_InventorySystem['SubCategory'] = "子类别";
$i_InventorySystem['Location'] = "位置";
$i_InventorySystem['Caretaker'] = "物品管理小组";
$i_InventorySystem['FundingSource'] = "资金来源";
$i_InventorySystem['WriteOffReason'] = "报销原因";
$i_InventorySystem['StockTake'] = "盘点";
$i_InventorySystem['Report'] = "报表";
$i_InventorySystem['Settings'] = "设定";
$i_InventorySystem['HidePictures'] = "隐藏图片";
$i_InventorySystem['Others'] = "其他";
$i_InventorySystem['PageManagement'] = "管理";

### Added On 28 Sep 2007 ###
$i_InventorySystem_Select_Menu = "请从左方功能清单选出所需服务。";
$i_InventorySystem_Category_Code = "类别编号";
$i_InventorySystem_Category_Name = "类别名称";
$i_InventorySystem_Category_ChineseName = "中文名称";
$i_InventorySystem_Category_EnglishName = "英文名称";
$i_InventorySystem_Category_DisplayOrder = "显示次序";
$i_InventorySystem_Category_Photo = "相片";
$i_InventorySystem_Category2_License = "许可证";
$i_InventorySystem_Category2_Warranty = "保用证";
$i_InventorySystem_Category2_Barcode = "条码";

### Added on 13 Nov 2007 ###
$i_InventorySystem_ReportType_Array = array(
array(1,"物品资料"),
array(2,"数量改变"),
array(3,"保用证到期日"),
array(4,"现时数量"));
$i_InventorySystem_ReportType = "类型";
$i_InventorySystem_Report_Warranty = "保用证";
$i_InventorySystem_Report_QuantityLevel = "现时数量";
$i_InventorySystem_Report_ItemName = "名称";
$i_InventorySystem_Report_CurrentQuantity = "现时数量";
$i_InventorySystem_Report_InitialQuantity = "开始数量";
$i_InventorySystem_Report_QtyDifference = "数量差别";
$i_InventorySystem_Report_WarrantyExpiryDate = "保用证到期日";
$i_InventorySystem_Report_WarrantyExpied = "已过期";
$i_InventorySystem_Report_WarrantyNotExpied = "未过期";
$i_InventorySystem_Report_QtyMoreThan = "多于 $eInventory_StockAlertLevel";
$i_InventorySystem_Report_QtyLessEqualTo = "少于/等于 $eInventory_StockAlertLevel";
$i_InventorySystem_Report_ItemDetails = "物品详细资料";

$i_InventorySystem_Action_Purchase = "购买";
$i_InventorySystem_Action_StockCheck = "盘点";
$i_InventorySystem_Action_UpdateStatus = "更新状况";
$i_InventorySystem_Action_ChangeLocation = "更改位置";
$i_InventorySystem_Action_ChangeLocationQty = "更改数量";
$i_InventorySystem_Action_MoveBackToOriginalLocation = "搬回原位";
$i_InventorySystem_Action_Stocktake_Original_Location = "盘点 - 在原位找到";
$i_InventorySystem_Action_Stocktake_Other_Location = "盘点 - 在其他位置找到";
$i_InventorySystem_Action_Stocktake_Not_Found = "盘点 - 找不到";
$i_InventorySystem_Action_Surplus_Ignore = "忽略多出数量";
$i_InventorySystem_Action_Edit_Item = "修改物品资料";
$i_InventorySystem_Action_WriteOff = "报销";

$i_InventorySystem_ItemStatus_Normal = "正常";
$i_InventorySystem_ItemStatus_Repair = "维修中";
$i_InventorySystem_ItemStatus_WriteOff = "报销";
$i_InventorySystem_ItemStatus_Damaged = "损坏";

$i_InventorySystem_Action_Array = array(
array(1,$i_InventorySystem_Action_Purchase),
array(2,$i_InventorySystem_Action_Stocktake_Original_Location),
array(3,$i_InventorySystem_Action_Stocktake_Other_Location),
array(4,$i_InventorySystem_Action_Stocktake_Not_Found),
array(5,$i_InventorySystem_Action_WriteOff),
array(6,$i_InventorySystem_Action_UpdateStatus),
array(7,$i_InventorySystem_Action_ChangeLocation),
array(8,$i_InventorySystem_Action_ChangeLocationQty),
array(9,$i_InventorySystem_Action_MoveBackToOriginalLocation),
array(10,$i_InventorySystem_Action_Surplus_Ignore)
);

$i_InventorySystem_ItemType_Single = "单一物品";
$i_InventorySystem_ItemType_Bulk = "大量物品";

$i_InventorySystem_ItemType_Array = array(
array(1,$i_InventorySystem_ItemType_Single),
array(2,$i_InventorySystem_ItemType_Bulk));

$i_InventorySystem_Assigned = "被编配";
$i_InventorySystem_Avanced_Search = "进阶搜寻";
$i_InventorySystem_Simple_Search = "简单搜寻";
$i_InventorySystem_Found = "找到";
$i_InventorySystem_Group_Member = "管理成员";
$i_InventorySystem_Import_Member_From = "由内联网小组汇入名单";
$i_InventorySystem_Loading = "处理中";
$i_InventorySystem_Location_Level = "位置";
$i_InventorySystem_Location = "子位置";

$i_InventorySystem_Keyword = "关键字";
$i_InventorySystem_Optional = "可不填";
$i_InventorySystem_Item_NotFound = "物品位置错误";
$i_InventorySystem_Item_NotFound_Display = "检视位置错误的物品";
$i_InventorySystem_Item_Name = "物品名称";
$i_InventorySystem_Item_Qty = "数量";
$i_InventorySystem_Item_Code = "物品编号";
$i_InventorySystem_Item_Barcode = "条码";
$i_InventorySystem_Item_Description = "详细资料";
$i_InventorySystem_Item_Ownership = "拥有权";
$i_InventorySystem_Item_Price = "购买金额 (单据总额)";
$i_InventorySystem_Item_Remark = "备注";
$i_InventorySystem_Item_Detail = "详细资料";
$i_InventorySystem_Item_History = "纪录";
$i_InventorySystem_Item_Invoice = "发票";
$i_InventorySystem_Item_Supplier_Name = "供应商名称";
$i_InventorySystem_Item_Supplier_Contact = "供应商联络";
$i_InventorySystem_Item_Supplier_Description = "供应商资料";
$i_InventorySystem_Item_Purchase_Date = "购入日期";
$i_InventorySystem_Item_Purchase_Date2 = "购入日期";
$i_InventorySystem_Item_Quot_Num = "报价单编号";
$i_InventorySystem_Item_Tender_Num = "标书编号";
$i_InventorySystem_Item_Invoice_Num = "发票编号";
$i_InventorySystem_Item_Voucher_Num = "收据编号";
$i_InventorySystem_Item_Attachment = "附件";
$i_InventorySystem_Item_Brand_Name = "牌子";
$i_InventorySystem_Item_Warrany_Expiry = "保用日期";
$i_InventorySystem_Item_Record_Date = "纪录日期";
$i_InventorySystem_Item_License_Type = "许可证";
$i_InventorySystem_Item_Action = "行动";
$i_InventorySystem_Item_Past_Status = "改动前状态";
$i_InventorySystem_Item_New_Status = "改动后状态";
$i_InventorySystem_Item_Remark = "备注";
$i_InventorySystem_Item_PIC = "负责人";
$i_InventorySystem_Item_AssignTo = "编配至";
$i_InventorySystem_Item_Stock_Qty = $i_InventorySystem['StockCheck'].$i_InventorySystem_Item_Qty;
$i_InventorySystem_Item_ChineseName= "物品名称 (中文)";
$i_InventorySystem_Item_EnglishName= "物品名称 (英文)";
$i_InventorySystem_Item_ChineseDescription = "详细资料 (中文)";
$i_InventorySystem_Item_EnglishDescription = "详细资料 (英文)";
$i_InventorySystem_Item_Photo = "相片";
$i_InventorySystem_Item_Location = "位置";
$i_InventorySystem_Item_Funding = "资金来源";
$i_InventorySystem_Item_License = "许可证";
$i_InventorySystem_Item_WarrantyExpiryDate = "保用证到期日";
$i_InventorySystem_Item_Serial_Num = "序号";
$i_InventorySystem_Item_BulkItemAdmin = "误差总管";

$i_InventorySystem_Ownership_School = "学校";
$i_InventorySystem_Ownership_Government = "政府";
$i_InventorySystem_Ownership_Donor = "办学团体";

$i_InventorySystem_Location_Level_Name = "位置名称";
$i_InventorySystem_Location_Name = "子位置名称";
$i_InventorySystem_Group_Name = "物品管理组别名称";
$i_InventorySystem_Group_MemberName = "组员名称";

$i_InventorySystem_ItemStatus_Array = array(
array(1,$i_InventorySystem_ItemStatus_Normal),
array(2,$i_InventorySystem_ItemStatus_Damaged),
array(3,$i_InventorySystem_ItemStatus_Repair)
);

$i_InventorySystem_ItemDetails = "物品详细资料";
$i_InventorySystem_PurchaseDetails = "购买资料";

# added on 30 Nov 2007

$i_InventorySystem_Item_CategoryCode = "类别编号";
$i_InventorySystem_Item_CategoryName = "类别名称";
$i_InventorySystem_Item_Category2Code = "子类别编号";
$i_InventorySystem_Item_LocationCode = "位置编号";
$i_InventorySystem_Item_Location2Code = "子位置编号";
$i_InventorySystem_Item_GroupCode = "管理组别编号";
$i_InventorySystem_Item_FundingSourceCode = "资金来源编号";
$i_InventorySystem_Index = "编号";

$i_InventorySystem_Item_New = "新物品";
$i_InventorySystem_Item_Existing = "现存物品";

# added on 5 Dec 2007 #
$i_InventorySystem_ImportInstruction_Title = "CSV 档案使用方法";
$i_InventorySystem_ImportInstruction_Download = "按此下载";
$i_InventorySystem_ImportInstruction_InstallFontType = "及安装条码字型 - IDAutomationHC39M (true type)";
$i_InventorySystem_ImportInstruction_ReportTemplate = "报表样式(MS Word 格式)";
$i_InventorySystem_ImportInstruction_ImportCSVFile = "设定报表中的资料来源为 CSV 档案";
$i_InventorySystem_ImportInstruction_PrintReport = "列印报表";
$i_InventorySystem_Category_CustomPhoto = "自订";

# addded on 6 Dec 2007 #
$i_InventorySystem_Setting_StockCheckPeriod = "盘点日期";
$i_InventorySystem_Setting_StockCheckStart = "开始日期";
$i_InventorySystem_Setting_StockCheckEnd = "完结日期";
$i_InventorySystem_Setting_WarrantyExpiryWarning = "保用证到期提示";
$i_InventorySystem_Setting_WarrantyExpiryWarningDayPeriod = "到期前日数";
$i_InventorySystem_Report_WarrantyExpiry = "到期保用证";
$i_InventorySystem_Report_ItemNotStockCheck = "未点算物品";

# added on 10 Dec 2007 #
$i_InventorySystem_StockAssigned = "数量(已编配)";
$i_InventorySystem_StockFound = "数量(已找到)";
$i_InventorySystem_Input_Category_Code_Warning = "请输入编号";
$i_InventorySystem_Input_Category_Item_ChineseName_Warning = "请输入中文名称";
$i_InventorySystem_Input_Category_Item_EnglishName_Warning = "请输入英文名称";

# added on 11 Dec 2007 #
$i_InventorySystem_Import_CheckItemCode = "按此查询".$i_InventorySystem_Item_Code;
$i_InventorySystem_Import_CheckCategory = "按此查询".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_Import_CheckCategory2 = "按此查询".$i_InventorySystem_Item_Category2Code;
$i_InventorySystem_Import_CheckGroup = "按此查询".$i_InventorySystem_Item_GroupCode;
$i_InventorySystem_Import_CheckLocation = "按此查询".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_Import_CheckLocation2 = "按此查询".$i_InventorySystem_Item_Location2Code;
$i_InventorySystem_Import_CheckFunding = "按此查询".$i_InventorySystem_Item_FundingSourceCode;
$i_InventorySystem_Import_CorrectSingleItemCodeReminder = " * 以上之物品编号已在使用，请使用新的物品编号进行汇入。";
$i_InventorySystem_Import_CorrectBulkItemCodeReminder = " * 以上之物品编号已在使用，汇入已有的物品编号，将更改所对应物品之数量。";
$i_InventorySystem_Import_CorrectFundingIndexReminder = " * 请小心核对csv汇入档之资金来源编号是否如上相同。";
$i_InventorySystem_Import_CorrectGroupIndexReminder = " * 请小心核对csv汇入档之管理组别编号是否如上相同。";
$i_InventorySystem_Import_CorrecrCategory2IndexReminder = " * 请小心核对csv汇入档之子类别编号是否如上相同。";
$i_InventorySystem_Import_CorrectLocationIndexReminder = " * 请小心核对csv汇入档之子位置编号是否如上相同。";
$i_InventorySystem_Import_CorrectLocation2IndexReminder = " * 汇入档案时 请使用正确的".$i_InventorySystem_Item_Location2Code;
$i_InventorySystem_StockCheck_QtyDifferent = "相差数量";
$i_InventorySystem_StockCheck_ValidQuantityWarning = "请输入有效数量";
$i_InventorySystem_StockCheck_EmptyQuantityWarning = "请输入数量";
$i_InventorySystem_StockCheck_Found = "已找到";
$i_InventorySystem_StockCheck_LastStatus = "最后状况";
$i_InventorySystem_StockCheck_NewStatus = "最新状况";
$i_InventorySystem_Import_CorrectCategoryCodeWarning = " * 以上之类别编号已在使用，请使用新的类别编号进行汇入。";
$i_InventorySystem_Import_CorrecrCategory2CodeWarning = " * 以上之子类别编号已在使用，请使用新的子类别编号进行汇入。";
$i_InventorySystem_Import_CorrectLocationCodeWarning = " * 以上之位置编号已在使用，请使用新的位置编号进行汇入。";
$i_InventorySystem_Import_CorrectLocation2CodeWarning = " * 以上之子位置编号已在使用，请使用新的子位置编号进行汇入。";
$i_InventorySystem_Import_CorrectFundingCodeWarnging = " * 以上之资金来源编号已在使用，请使用新的资金来源编号进行汇入。";

# added on 12 Dec 2007 #
$i_InventorySystem_Item_Registration = "新增物品";
$i_InventorySystem_Input_Item_Code_Warning = "请输入物品编号";
$i_InventorySystem_Input_Item_Code_Length_Warning = "物品编号不能多于10个字元";
$i_InventorySystem_Input_Item_GroupSelection_Warning = "请编配组别";
$i_InventorySystem_Input_Item_LocationSelection_Warning = "请选择位置";
$i_InventorySystem_Input_Item_FundingSelection_Warning = "请选择资金来源";
$i_InventorySystem_Input_Item_PurchaseDate_Warning = "请输入正确购入日期";
$i_InventorySystem_Input_Item_WarrantyExpiryDate_Warning = "请输入正确保用证到期日";
$i_InventorySystem_Input_Item_Barcode_Warning = "请输入或产生条码";

# added on 14 Dec 2007 #
$i_InventorySystem_StockCheck_Step1 = "选择盘点地点";
$i_InventorySystem_StockCheck_Step2 = "开始盘点";
$i_InventorySystem_StockCheck_Step3 = "确认盘点结果";
$i_InventorySystem_Setting_WarrantyGiveWarning1 = "到期";
$i_InventorySystem_Setting_WarrantyGiveWarning2 = "日前提示";
$i_InventorySystem['BasicSettings'] = "基本设定";
$i_InventorySystem['FullList'] = "所有物品";
$i_InventorySystem['View'] = "检视";
$i_InventorySystem_Quantity_StockChecked = "数量(已点算)";
$i_InventorySystem_PhotoGuide = "相片只能以'.JPG'、'.GIF' 或'.PNG'的格式上载，相片大小也规定为100 X 100 pixel (阔 x 高)。";
$i_InventorySystem_PurchasePriceGuide = "代表整张单据售价总数。";
$i_InventorySystem_ImportCaretaker_Warning = "系统只会汇入所选组别内的教职员";

# added on 27 Dec 2007 #
$i_InventorySystem_Setting_BarcodeMaxLength1 = "条码长度";
$i_InventorySystem_Setting_BarcodeMaxLength2 = "字元";
$i_InventorySystem_Setting_BarcodeFormat = "条码格式";
$i_InventorySystem_Setting_BarcodeFormat_NumOnly = "只使用数字，特定字元('-','$','|','.','/')及空白键";
$i_InventorySystem_Setting_BarcodeFormat_BothNumAndChar = "使用数字(0-9)，大楷字母(A-Z)特定字元('-','$','|','.','/')及空白键";
$i_InventorySystem_ItemWarrantyExpiryWarning = "以下物品的保用证即将到期";

# added on 28 Dec 2007 #
$i_InventorySystem_WriteOffItem = "报销物品";
$i_InventorySystem_WriteOffQty = "数量 (报销)";

# added on 2 Jan 2008 #
$i_InventorySystem_Input_Item_PurchasePrice_Warning = "请输入有效购入价钱";

# added on 3 Jan 2008 #
$i_InventorySystem_Input_Item_Generate_Item_Code = "产生物品编号";
$i_InventorySystem_Input_Item_Generate_Item_Barcode = "产生条码";

# added on 4 Jan 2008 #
$i_InventorySystem_Report_ItemFinishStockCheck = "物品点算";

# added on 8 Jan 2008 #
$i_InventorySystem_Search_Result = "搜寻结果";
$i_InventorySystem_ViewItem_All_Location_Level = "所有位置";
$i_InventorySystem_ViewItem_All_Resource_Management_Group = "所有管理小组";
$i_InventorySystem_ViewItem_All_Finding_Source = "所有资金来源";
$i_InventorySystem_Item_Update_Status = "更新状况";

# added on 9 Jan 2008 #
$i_InventorySystem_Location_Code = "子位置编号";
$i_InventorySystem_Location_Level_Code = "位置编号";
$i_InventorySystem_Caretaker_Code = "小组编号";
$i_InventorySystem_Funding_Code = "资金编号";

# added on 10 Jan 2008 #
$i_InventorySystem_RequestWriteOff = "申请报销";

# added on 10 Jan 2008 #
$i_InventorySystem_WriteOffReason_Name = "报销原因";

# added on 15 Jan 2008 #
$i_InventorySystem_StockTake_SelectItemStatus_Warning = "请选择物品状况";

# added on 16 Jan 2008 #
$i_InventorySystem_ItemStatus_Lost = "遗失";
$i_InventorySystem_ItemStatus_Surplus = "过剩";
$i_InventorySystem_Btn_Generate = "产生";
$i_InventorySystem_Unit_Price = "单位价格";
$i_InventorySystem_Group_MemberPosition = "职位";
$i_InventorySystem_Group_MemberPosition_Leader = "组长";
$i_InventorySystem_Group_MemberPosition_Member = "组员";
$i_InventorySystem_Group_Member_LeaderRemark = "组长";
$i_InventorySystemItemStatus = "物品状况";
$i_InventorySystemItemStatusWriteOff = "已报销";
$i_InventorySystemItemStatusNonWriteOff = "未报销";
$i_InventorySystemItemStatusAll = "全部";
$i_InventorySystemItemMaintainInfo = "维修资料";
$i_InventorySystem_Approval = "资产管理批核";
$i_InventorySystem_WriteOffItemApproval = "物品报销批核";

$i_InventorySystem_Error = "错误";


# added on 17 Jan 2008 #
$i_InventorySystem_Input_Item_ExistingItemCode = "现有物品编号";

# added on 18 Jan 2008 #
$i_InventorySystem_Input_Item_Barcode_Exist_Warning = "刚输入的条码已被使用。请输入另一个。";
$i_InventorySystem_Input_Category_Item_Name_Exist_Warning = "刚输入的名称已被使用。请输入另一个。";
$i_InventorySystem_Input_Category_Code_Exist_Warning = "刚输入的类别编号已被使用。请输入另一个。";
$i_InventorySystem_Last_Stock_Take_Time = "最后盘点时间";
$i_InventorySystem_Write_Off_RequestTime = "申请日期";
$i_InventorySystem_Write_Off_RequestQty = "申请数量";
$i_InventorySystem_Write_Off_Reason = "报销原因";

# added on 21 Jan 2008 #
$i_InventorySystem_Export_Select_Column = "请选择所需的物品资料：";
$i_InventorySystem_Item_TagCode = "条码";

# added on 22 Jan 2008 #
$i_InventorySystem_Report_ItemWrittenOff = "已报销物品";

# added on 31 Jan 2008 #
$i_InventorySystem_StocktakeVarianceHandling = "盘点误差处理";

# added on 4 Feb 2008 #
$i_InventorySystem_Resource_Management_GroupLeader = "组长";
$i_InventorySystem_SetAsGroupLeader = "设定为组长";
$i_InventorySystem_SetAsGroupMember = "设定为组员";
$i_InventorySystem_Report_FixedAssetsRegister = "固定资产登记册";
$i_InventorySystem_Report_ItemStatus = "物品状况";
$i_InventorySystem_Input_Category_Code_RegExp_Warning = "请输入有效的类别编号";
$i_InventorySystem_Input_SubCategory_Code_RegExp_Warning = "请输入有效的类别编号";
$i_InventorySystem_Input_SubCategory_Item_Name_Exist_Warning = "刚输入的名称已被使用。请输入另一个。";
$i_InventorySystem_Input_SubCategory_Code_Exist_Warning = "刚输入的类别编号已被使用。请输入另一个。";
$i_InventorySystem_Input_LocationLevel_Code_RegExp_Warning = "请输入有效的位置编号";
$i_InventorySystem_Input_LocationLevel_Name_Exist_Warning = "刚输入的名称已被使用。请输入另一个。";
$i_InventorySystem_Input_LocationLevel_Code_Exist_Warning = "刚输入的位置编号已被使用。请输入另一个。";
$i_InventorySystem_Input_Location_Code_RegExp_Warning = "请输入有效的子位置编号";
$i_InventorySystem_Input_Location_Name_Exist_Warning = "刚输入的名称已被使用。请输入另一个。";
$i_InventorySystem_Input_Location_Code_Exist_Warning = "刚输入的子位置编号已被使用。请输入另一个。";
$i_InventorySystem_Input_Group_Code_RegExp_Warning = "请输入有效的小组编号";
$i_InventorySystem_Input_Group_Name_Exist_Warning = "刚输入的名称已被使用。请输入另一个。";
$i_InventorySystem_Input_Group_Code_Exist_Warning = "刚输入的小组编号已被使用。请输入另一个。";
$i_InventorySystem_Input_Funding_Code_RegExp_Warning = "请输入有效的资金来源编号";
$i_InventorySystem_Input_Funding_Name_Exist_Warning = "刚输入的名称已被使用。请输入另一个。";
$i_InventorySystem_Input_Funding_Code_Exist_Warning = "刚输入的资金来源编号已被使用。请输入另一个。";

# added on 6 Feb 2008 #
$i_InventorySystem_EditItem_NoEditRightWarning = "您不能编辑该物品";
$i_InventorySystem_Report_Col_Item = "物品";
$i_InventorySystem_Report_Col_Cost = "成本";
$i_InventorySystem_Report_Col_Description = "详情";
$i_InventorySystem_Report_Col_Purchase_Date = "购买日期";
$i_InventorySystem_Report_Col_Govt_Fund = "政府经费";
$i_InventorySystem_Report_Col_Sch_Fund = "学校经费";
$i_InventorySystem_Report_Col_Sch_Unit_Price = "单位价格";
$i_InventorySystem_Report_Col_Sch_Purchase_Price = "价值";
$i_InventorySystem_Report_Col_Quantity = "数量";
$i_InventorySystem_Report_Col_Location = "放置地点";
$i_InventorySystem_Report_Col_Date_Reason_of_Write_Off = "报销日期及理由";
$i_InventorySystem_Report_Col_Signature = "校监/校长签署批准日期";
$i_InventorySystem_Report_Col_Remarks = "备注(发票编号及公司名称)";

# added on 11 Feb 2008 #
$i_InventorySystem_Input_Item_ItemPrice_Warning = "请输入有效单位价格";

# added on 12 Feb 2008 #
$i_InventorySystem_StockTakeResult = "盘点结果";

# added on 13 Feb 2008 #
$i_InventorySystem_FileFormat = "档案类型";
$i_InventorySystem_ViewItem_All_Category = "所有类别";
$i_InventorySystem_Setting_BarcodeSetting = "条码格式";
$i_InventorySystem_Input_ItemCode_Exist_Warning = "刚输入的物品编号已被使用。请输入另一个。";

# added on 14 Feb 2008 #
$i_InventorySystem_MissingQty = "遗失数量";
$i_InventorySystem_SurplusQty = "多出数量";
$i_InventorySystem_TotalMissingQty = "总遗失数量";
$i_InventorySystem_TotalSurplusQty = "总多出数量";
$i_InventorySystem_RecordDate = "记录日期";
$i_InventorySystem_RequestPerson = "申请人";
$i_InventorySystem_ApprovePerson = "批准人";

# added on 15 Feb 2008 #
$i_InventorySystem_ApproveDate = "批准日期";
$i_InventorySystem_Write_Off_Attachment = "报销附件";
$i_InventorySystem_Item_UpdateStatus = "更新状况";
$i_InventorySystem_Item_UpdateLocation = "更改位置";
$i_InventorySystem_Item_RequestWriteOff = "申请报销";

# added on 19 Feb 2008 #
$i_InventorySystem_Search_Purchase_Price_Range = "总购入金额范围";
$i_InventorySystem_Search_Unit_Price_Range = "单位价格范围";
$i_InventorySystem_From = "由";
$i_InventorySystem_To = "至";
$i_InventorySystem_BarcodeLabels = "条码标签";
$i_InventorySystem_Report_Stocktake = "盘点";
$i_InventorySystem_NumOfItemAdd = "数量";
$i_InventorySystem_AddNewItem_Step1 = "输入基本物品资料";
$i_InventorySystem_AddNewItem_Step2 = "输入详细物品资料";
$i_InventorySystem_Report_Stocktake_Progress = "盘点进度";
$i_InventorySystem_Stocktake_ProgressFinished = "已完成";
$i_InventorySystem_Stocktake_ProgressNotFinished = "未完成";

# added on 20 Feb 2008 #
$i_InventorySystem_ViewItem_All_Category = "所有类别";
$i_InventorySystem_DisplayAllImage = "显示图片";
$i_InventorySystem_Setting_StockCheckDateRange = "选择日期范围";
$i_InventorySystem_Funding_Type = "资金类别";
$i_InventorySystem_Funding_Type_School = "学校";
$i_InventorySystem_Funding_Type_Government = "政府";
$i_InventorySystem_Funding_Type_Array = array(
array(1,$i_InventorySystem_Funding_Type_School),
array(2,$i_InventorySystem_Funding_Type_Government));
$i_InventorySystem_ItemType_All = "所有种类";
$i_InventorySystem_HideAllImage = "隐藏图片";
$i_InventorySystem_Stocktake_List = "盘点清单";
$i_InventorySystem_Stocktake_ReadBarcode = "输入或扫瞄条码";
$i_InventorySystem_Stocktake_LastStocktakeBy= "最后盘点职员";
$i_InventorySystem_Status = "状况";
$i_InventorySystem_Stocktake_TotalQty = "总数";
$i_InventorySystem_Stocktake_FoundQty = "找到";
$i_InventorySystem_Stocktake_UnlistedQty = "不明";
$i_InventorySystem_Stocktake_ExpectedQty = "预期数量";
$i_InventorySystem_Stocktake_StocktakeQty = "盘点数量";
$i_InventorySystem_Stocktake_NormalQty = "正常";
$i_InventorySystem_Stocktake_RequestWriteOffQty = "申请报销";
$i_InventorySystem_Stocktake_VarianceQty = "误差";
$i_InventorySystem_Stocktake_MissingRemark = "未能找到";
$i_InventorySystem_Stocktake_UnlistedItem = "不明物品";
$i_InventorySystem_Stocktake_ExpectedLocation = "预期位置";
$i_InventorySystem_Stocktake_Dislocated_Item = "位置错误";
$i_InventorySystem_Stocktake_ItemNotExist = "没有物品记录";
$i_InventorySystem_Stocktake_ReasonAndAttachment = "原因及附件";
$i_InventorySystem_VarianceHandling_ChangeLocation = "更改位置";
$i_InventorySystem_VarianceHandling_Qty = "数量";
$i_InventorySystem_Report_StocktakeProgress = "盘点进度";

$i_InventorySystem_CategoryImport_Format1_Row1 = "第一栏 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_CategoryImport_Format1_Row2 = "第二栏 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_CategoryImport_Format1_Row3 = "第三栏 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_CategoryImport_Format1_Row4 = "第四栏 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_Category2Import_Format1_Row1 = "第一栏 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_Category2Import_Format1_Row2 = "第二栏  : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_Category2Import_Format1_Row3 = "第三栏 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_Category2Import_Format1_Row4 = "第四栏 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_Category2Import_Format1_Row8 = "第八栏 : $i_InventorySystem_Category_DisplayOrder";
$i_InventorySystem_Category2Import_Format1_Row5 = "第五栏 : $i_InventorySystem_Category2_License (1 - 适用, 0 - 不适用)";
$i_InventorySystem_Category2Import_Format1_Row6 = "第六栏 : $i_InventorySystem_Category2_Warranty (1 - 适用, 0 - 不适用)";

$i_InventorySystem_LocationImport_Format1_Row1 = "第一栏 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_LocationImport_Format1_Row2 = "第二栏 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_LocationImport_Format1_Row3 = "第三栏 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_LocationImport_Format1_Row4 = "第四栏 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_Location2Import_Format1_Row1 = "第一栏 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_Location2Import_Format1_Row2 = "第二栏 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_Location2Import_Format1_Row3 = "第三栏 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_Location2Import_Format1_Row4 = "第四栏 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_Location2Import_Format1_Row5 = "第五栏 : $i_InventorySystem_Category_DisplayOrder";

$i_InventorySystem_FundingImport_Format1_Row1 = "第一栏 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_FundingImport_Format1_Row2 = "第二栏 : $i_InventorySystem_Category_ChineseName";
$i_InventorySystem_FundingImport_Format1_Row3 = "第三栏 : $i_InventorySystem_Category_EnglishName";
$i_InventorySystem_FundingImport_Format1_Row4 = "第四栏 : $i_InventorySystem_Funding_Type (e.g School, Government)";
$i_InventorySystem_FundingImport_Format1_Row5 = "第五栏 : $i_InventorySystem_Category_DisplayOrder";


$i_InventorySystem_CategoryImportError[1] = $i_InventorySystem_Item_CategoryCode."已存在";
$i_InventorySystem_CategoryImportError[2] = "没有填上".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_CategoryImportError[3] = "没有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_CategoryImportError[4] = "没有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_CategoryImportError[5] = "没有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_CategoryImportError[19] = $i_InventorySystem_Item_CategoryCode."已存在";
$i_InventorySystem_CategoryImportError[20] = $i_InventorySystem_Item_CategoryCode."长度必须在5个字以内";

$i_InventorySystem_Category2ImportError[1] = $i_InventorySystem_Item_CategoryCode."无效";
$i_InventorySystem_Category2ImportError[2] = "没有填上".$i_InventorySystem_Item_CategoryCode;
$i_InventorySystem_Category2ImportError[3] = "没有填上".$i_InventorySystem_Item_Category2Code;
$i_InventorySystem_Category2ImportError[4] = "没有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_Category2ImportError[5] = "没有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_Category2ImportError[6] = "没有填上".$i_InventorySystem_Category2_License;
$i_InventorySystem_Category2ImportError[7] = "没有填上".$i_InventorySystem_Category2_Warranty;
$i_InventorySystem_Category2ImportError[8] = "没有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_Category2ImportError[9] =  $i_InventorySystem_Item_Category2Code."已存在";
$i_InventorySystem_Category2ImportError[19] = $i_InventorySystem_Item_Category2Code."已存在";
$i_InventorySystem_Category2ImportError[22] = $i_InventorySystem_Item_Category2Code."长度必须在5个字以内";

$i_InventorySystem_LocationLevelImportError[1] = $i_InventorySystem_Item_LocationCode."已存在";
$i_InventorySystem_LocationLevelImportError[2] = "没有填上".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_LocationLevelImportError[3] = "没有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_LocationLevelImportError[4] = "没有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_LocationLevelImportError[5] = "没有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_LocationLevelImportError[19] = $i_InventorySystem_Item_LocationCode."已存在";

$i_InventorySystem_LocationImportError[1] = $i_InventorySystem_Item_LocationCode."无效";
$i_InventorySystem_LocationImportError[2] = $i_InventorySystem_Item_Location2Code."已存在";
$i_InventorySystem_LocationImportError[3] = "没有填上".$i_InventorySystem_Item_LocationCode;
$i_InventorySystem_LocationImportError[4] = "没有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_LocationImportError[5] = "没有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_LocationImportError[6] = "没有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_LocationImportError[19] = $i_InventorySystem_Item_LocationCode."已存在";
$i_InventorySystem_LocationImportError[9] = $i_InventorySystem_Item_Location2Code."已存在";

$i_InventorySystem_FundingImportError[1] = $i_InventorySystem_Item_FundingSourceCode."已存在";
$i_InventorySystem_FundingImportError[2] = "没有填上".$i_InventorySystem_Item_FundingSourceCode;
$i_InventorySystem_FundingImportError[3] = "没有填上".$i_InventorySystem_Item_ChineseName;
$i_InventorySystem_FundingImportError[4] = "没有填上".$i_InventorySystem_Item_EnglishName;
$i_InventorySystem_FundingImportError[5] = "没有填上".$i_InventorySystem_Funding_Type;
$i_InventorySystem_FundingImportError[6] = "没有填上".$i_InventorySystem_Category_DisplayOrder;
$i_InventorySystem_FundingImportError[7] = $i_InventorySystem_Funding_Type."无效";
$i_InventorySystem_FundingImportError[8] = $i_InventorySystem_Category_DisplayOrder."无效";
$i_InventorySystem_FundingImportError[19] = $i_InventorySystem_Funding_Type."已存在";

# Added on 4 Mar 2008 #
$i_InventorySystem_VarianceHandling_Action[1] = "搬回原来位置";
$i_InventorySystem_VarianceHandling_Action[2] = "重新设定位置";
$i_InventorySystem_VarianceHandling_Action[3] = "要求报销";
$i_InventorySystem_VarianceHandling_Action[4] = "新增物品";
$i_InventorySystem_VarianceHandling_Action[5] = "忽略";

# Added on 6 Mar 2008 #
$i_InventorySystem_Report_Stocktake_groupby = "显示次序";

# Added on 7 Mar 2008 #
$i_InventorySystem['item_type'] = "类型";

# added on 10 Mar 2008 #
$i_InventorySystem_WriteOff_Reason_LostItem = "找不到"; #"盘点时已遗失";


$i_InventorySystem_VarianceHandling_AddAsNewItemQty = "新增物品数量";
$i_InventorySystem_VarianceHandling_IgnoreQty = "忽略数量";

# added on 12 Mar 2008 #
$i_InventorySystem['jsLocationCheckBox'] = "请选择位置";
$i_InventorySystem['jsGroupCheckBox'] = "请选择组别";
$i_InventorySystem['jsLocationGroupCheckBox'] = "请选择位置/组别";

# added on 13 Mar 2008 #
$i_InventorySystem['VarianceHandlingNotice'] = "误差处理通知";
$i_InventorySystem_VarianceHandlingNotice_CurrentLocation = "现时位置";
$i_InventorySystem_VarianceHandlingNotice_NewLocation = "新位置";
$i_InventorySystem_VarianceHandlingNotice_Sender = "发送者";
$i_InventorySystem_VarianceHandlingNotice_Handler = "处理者";
$i_InventorySystem_VarianceHandlingNotice_SendDate = "发送日期";

# added on 14 Mar 2008 #
$i_InventorySystem_BackToFullList = "返回物品清单";

# added on 17 Mar 2008 #
$i_InventorySystem_SubCategory_Code = "子类别编号";
$i_InventorySystem_SubCategory_Name = "子类别名称";
$i_InventorySystem_Setting_Location_ChineseName = "位置名称 (中文)";
$i_InventorySystem_Setting_Location_EnglishName = "位置名称 (英文)";
$i_InventorySystem_Setting_SubLocation_ChineseName = "子位置名称 (中文)";
$i_InventorySystem_Setting_SubLocation_EnglishName = "子位置名称 (英文)";
$i_InventorySystem_Setting_NewLocation = "新增位置";
$i_InventorySystem_Setting_NewSubLocation  = "新增子位置";
$i_InventorySystem_Setting_EditLocation = "编辑位置";
$i_InventorySystem_Setting_EditSubLocation  = "编辑子位置";
$i_InventorySystem_Setting_NewManagementGroup = "新增物品管理小组";
$i_InventorySystem_Setting_EditManagementGroup = "编辑物品管理小组";
$i_InventorySystem_Setting_ManagementGroup_ChineseName = "小组名称 (中文)";
$i_InventorySystem_Setting_ManagementGroup_EnglishName = "小组名称 (英文)";
$i_InventorySystem_Setting_ManagementGroup_NewMember = "新增组员";
$i_InventorySystem_Setting_ManagementGroup_SelectUser = "选择用户";
$i_InventorySystem_Setting_Funding_Name = "资金名称";
$i_InventorySystem_Setting_NewFunding = "新增资金来源";
$i_InventorySystem_Setting_EditFunding = "编辑资金来源";
$i_InventorySystem_Setting_Funding_ChineseName = "资金名称 (中文)";
$i_InventorySystem_Setting_Funding_EnglishName = "资金名称 (英文)";
$i_InventorySystem_Setting_NewWriteOffReason = "新增报销原因";
$i_InventorySystem_Setting_EditWriteOffReason = "编辑报销原因";
$i_InventorySystem_Setting_WriteOffReason_ChineseName = "原因 (中文)";
$i_InventorySystem_Setting_WriteOffReason_EnglishName = "原因 (英文)";
$i_InventorySystem_Search_Warrany_Expiry_Between = "保用日期范围";
$i_InventorySystem_Search_And = "至";
$i_InventorySystem_Search_And2 = "及";
$i_InventorySystem_Search_Purchase_Date_Between = "购买日期范围";

# added on 20 Mar 2008 #
$i_InventorySystem_NewItem_PurchasePrice_Help = "<p>若交易涉及折扣，「总购入金额」未必等同「单位价格」乘以「数量」。</p><p>「总购入金额」一般为发票上的总金额，亦是你为整项交易所缴付的金额。</p><p>此金额将会列于「固定资产登记册」的「总购入金额」栏上。</p>";
$i_InventorySystem_NewItem_UnitPrice_Help = "「单位价格」是此物品的原价，并会乘以「数量」以计算「固定资产登记册」上「成本」栏的金额。";
$i_InventorySystem_MultiSelect_Help = "可按住CTRL键，点击多个项目。";

# added on 28 Mar 2008 #
$i_InventorySystem_VarianceHandling_New_Quantity = "新数量";
$i_InventorySystem_VarianceHandling_Original_Quantity = "原有数量";

# added on 8 Apr 2008 #
$i_InventorySystem_Send_Reminder = "发送提示";

# added on 15 Apr 2008 #
$i_InventorySystem_ImportItem_Format1_Title = "<span class=extraInfo>汇入$i_InventorySystem_ItemType_Single</span><Br><Br>";
$i_InventorySystem_ImportItem_Format1_Row1 = "第一栏 : $i_InventorySystem_Item_Code";
$i_InventorySystem_ImportItem_Format1_Row2 = "第二栏 : $i_InventorySystem_Item_Barcode";
$i_InventorySystem_ImportItem_Format1_Row3 = "第三栏 : $i_InventorySystem_Item_ChineseName";
$i_InventorySystem_ImportItem_Format1_Row4 = "第四栏 : $i_InventorySystem_Item_EnglishName";
$i_InventorySystem_ImportItem_Format1_Row5 = "第五栏 : $i_InventorySystem_Item_ChineseDescription";
$i_InventorySystem_ImportItem_Format1_Row6 = "第六栏 : $i_InventorySystem_Item_EnglishDescription";
$i_InventorySystem_ImportItem_Format1_Row7 = "第七栏 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_ImportItem_Format1_Row8 = "第八栏 : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_ImportItem_Format1_Row9 = "第九栏 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_ImportItem_Format1_Row10 = "第十栏 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_ImportItem_Format1_Row11 = "第十一栏 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_ImportItem_Format1_Row12 = "第十二栏 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_ImportItem_Format1_Row13 = "第十三栏 : $i_InventorySystem_Item_Ownership (1 - 学校, 2 - 政府, 3 - 办学团体)";
$i_InventorySystem_ImportItem_Format1_Row14 = "第十四栏 : $i_InventorySystem_Item_Warrany_Expiry (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format1_Row15 = "第十五栏 : $i_InventorySystem_Item_License";
$i_InventorySystem_ImportItem_Format1_Row16 = "第十六栏 : $i_InventorySystem_Item_Serial_Num";
$i_InventorySystem_ImportItem_Format1_Row17 = "第十七栏 : $i_InventorySystem_Item_Brand_Name";
$i_InventorySystem_ImportItem_Format1_Row18 = "第十八栏 : $i_InventorySystem_Item_Supplier_Name";
$i_InventorySystem_ImportItem_Format1_Row19 = "第十九栏 : $i_InventorySystem_Item_Supplier_Contact";
$i_InventorySystem_ImportItem_Format1_Row20 = "第二十栏 : $i_InventorySystem_Item_Supplier_Description";
$i_InventorySystem_ImportItem_Format1_Row21 = "第二十一栏 : $i_InventorySystem_Item_Quot_Num";
$i_InventorySystem_ImportItem_Format1_Row22 = "第二十二栏 : $i_InventorySystem_Item_Tender_Num";
$i_InventorySystem_ImportItem_Format1_Row23 = "第二十三栏 : $i_InventorySystem_Item_Invoice_Num";
$i_InventorySystem_ImportItem_Format1_Row24 = "第二十四栏 : $i_InventorySystem_Item_Purchase_Date (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format1_Row25 = "第二十五栏 : $i_InventorySystem_Item_Price";
$i_InventorySystem_ImportItem_Format1_Row26 = "第二十六栏 : $i_InventorySystem_Unit_Price";
$i_InventorySystem_ImportItem_Format1_Row27 = "第二十七栏 : $i_InventorySystemItemMaintainInfo";
$i_InventorySystem_ImportItem_Format1_Row28 = "第二十八栏 : $i_InventorySystem_Item_Remark";

$i_InventorySystem_ImportItem_Format2_Title = "<span class=extraInfo>汇入$i_InventorySystem_ItemType_Bulk</span><Br><Br>";
$i_InventorySystem_ImportItem_Format2_Row1 = "第一栏 : $i_InventorySystem_Item_Code";
$i_InventorySystem_ImportItem_Format2_Row2 = "第二栏 : $i_InventorySystem_Item_ChineseName";
$i_InventorySystem_ImportItem_Format2_Row3 = "第三栏 : $i_InventorySystem_Item_EnglishName";
$i_InventorySystem_ImportItem_Format2_Row4 = "第四栏 : $i_InventorySystem_Item_ChineseDescription";
$i_InventorySystem_ImportItem_Format2_Row5 = "第五栏 : $i_InventorySystem_Item_EnglishDescription";
$i_InventorySystem_ImportItem_Format2_Row6 = "第六栏 : $i_InventorySystem_Item_CategoryCode";
$i_InventorySystem_ImportItem_Format2_Row7 = "第七栏 : $i_InventorySystem_Item_Category2Code";
$i_InventorySystem_ImportItem_Format2_Row8 = "第八栏 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_ImportItem_Format2_Row9 = "第九栏 : $i_InventorySystem_Item_LocationCode";
$i_InventorySystem_ImportItem_Format2_Row10 = "第十栏 : $i_InventorySystem_Item_Location2Code";
$i_InventorySystem_ImportItem_Format2_Row11 = "第十一栏 : $i_InventorySystem_Item_FundingSourceCode";
$i_InventorySystem_ImportItem_Format2_Row12 = "第十二栏 : $i_InventorySystem_Item_Ownership (1 - 学校, 2 - 政府, 3 - 办学团体)";
$i_InventorySystem_ImportItem_Format2_Row13 = "第十三栏 : $i_InventorySystem_Item_BulkItemAdmin";
$i_InventorySystem_ImportItem_Format2_Row14 = "第十四栏 : $i_InventorySystem_Item_Qty";
$i_InventorySystem_ImportItem_Format2_Row15 = "第十五栏 : $i_InventorySystem_Item_Supplier_Name";
$i_InventorySystem_ImportItem_Format2_Row16 = "第十六栏 : $i_InventorySystem_Item_Supplier_Contact";
$i_InventorySystem_ImportItem_Format2_Row17 = "第十七栏 : $i_InventorySystem_Item_Supplier_Description";
$i_InventorySystem_ImportItem_Format2_Row18 = "第十八栏 : $i_InventorySystem_Item_Quot_Num";
$i_InventorySystem_ImportItem_Format2_Row19 = "第十九栏 : $i_InventorySystem_Item_Tender_Num";
$i_InventorySystem_ImportItem_Format2_Row20 = "第二十栏 : $i_InventorySystem_Item_Invoice_Num";
$i_InventorySystem_ImportItem_Format2_Row21 = "第二十一栏 : $i_InventorySystem_Item_Purchase_Date (YYYY-mm-dd)";
$i_InventorySystem_ImportItem_Format2_Row22 = "第二十二栏 : $i_InventorySystem_Item_Price";
$i_InventorySystem_ImportItem_Format2_Row23 = "第二十三栏 : $i_InventorySystem_Unit_Price";
$i_InventorySystem_ImportItem_Format2_Row24 = "第二十四栏 : $i_InventorySystemItemMaintainInfo";
$i_InventorySystem_ImportItem_Format2_Row25 = "第二十五栏 : $i_InventorySystem_Item_Remark";

# added on 16 Apr 2008 #
$i_InventorySystem_ImportItem_RowWithRecord = "有记录行数";
$i_InventorySystem_ImportItem_TotalRow = "总行数";
$i_InventorySystem_ImportItem_EmptyRowWarning = "CSV档案不能有空白行";
$i_InventorySystem_ImportItem_EmptyRowRecord = "空白行数";

# added on 17 Apr 2008 #
$i_InventorySystem_UpdateSingleItemStatusWarning = "请选择状况";
$i_InventorySystem_UpdateBulkItemStatusWarning_Normal = "请输入有效的正常数量";
$i_InventorySystem_UpdateBulkItemStatusWarning_Damaged = "请输入有效的损坏数量";
$i_InventorySystem_UpdateBulkItemStatusWarning_Reparing = "请输入有效的维修数量";
$i_InventorySystem_UpdateItemRequestWriteOffQty_Warning = "请输入有效的报销数量";
$i_InventorySystem_UpdateItemRequestWriteOffReason_Warning = "请选择报销原因";

# added on 18 Apr 2008 #
$i_InventorySystem_GroupImport_Format1_Row1 = "第一栏 : $i_InventorySystem_Item_GroupCode";
$i_InventorySystem_GroupImport_Format1_Row2 = "第二栏 : $i_InventorySystem_Setting_ManagementGroup_ChineseName";
$i_InventorySystem_GroupImport_Format1_Row3 = "第三栏 : $i_InventorySystem_Setting_ManagementGroup_EnglishName";
$i_InventorySystem_GroupImport_Format1_Row4 = "第四栏 : $i_InventorySystem_Category_DisplayOrder";


# added on 21 Apr 2008 #
$i_InventorySystem_Import_CorrectLocationCodeWarning2 = " * 请小心核对csv汇入档之位置编号是否如上相同。";
$i_InventorySystem_Import_CorrectCategoryCodeWarning2 = " * 请小心核对csv汇入档之类别编号是否如上相同。";
$i_InventorySystem_Import_CorrectGroupCodeWarning = " * 以上之管理组别编号已在使用，请使用新的管理组别编号进行汇入。";

# added on 24 Apr 2008 #
$i_InventorySystem_Category2_Serial = "序号";
$i_InventorySystem_Category2Import_Format1_Row7 = "第七栏 : $i_InventorySystem_Category2_Serial (1 - 适用, 0 - 不适用)";
$i_InventorySystem_Category2ImportError[10] = "没有填上".$i_InventorySystem_Category2_Serial;

# added on 28 Apr 2008 #
$i_InventorySystem_Export_Search_Result_Column = "请选择所需的资料：";

# added on 2 May 2008 #
$i_InventorySystem_Settings_BulkItemAdmin = "误差总管";
$i_InventorySystem_GroupMemberImport_Format1_Row1 = "第一栏 : 管理组别编号";
$i_InventorySystem_GroupMemberImport_Format1_Row2 = "第二栏 : 内联网帐号";
$i_InventorySystem_GroupMemberImport_Format1_Row3 = "第三栏 : 身分 (1 - 组长, 2 - 组员)";
$i_InventorySystem_Import_CorrectGroupCodeWarning2 = " * 请小心核对csv汇入档之管理组别编号是否如上相同。";
$i_InventorySystem_GroupMemberImportError[1] = $i_InventorySystem_Item_GroupCode."无效";
$i_InventorySystem_GroupMemberImportError[2] = "没有填上".$i_InventorySystem_Item_GroupCode;
$i_InventorySystem_GroupMemberImportError[3] = "没有填上".$i_UserLogin;
$i_InventorySystem_GroupMemberImportError[4] = $i_UserLogin."无效";
$i_InventorySystem_GroupMemberImportError[5] = "没有填上".$i_identity;
$i_InventorySystem_GroupMemberImportError[6] = $i_identity."无效";
$i_InventorySystem_GroupMemberImportError[7] = $i_UserLogin."已存在";

# added on 5 May 2008 #
$i_InvertorySystem_SelectOriginalLocation_Warning = "请选择原有位置";
$i_InvertorySystem_SelectOriginalGroup_Warning = "请选择原有管理组别";
$i_InvertorySystem_SelectNewLocation_Warning = "请选择新位置";
$i_InvertorySystem_SelectNewGroup_Warning = "请选择新管理组别";
$i_InventorySystem_Notice = "资产管理通告";
$i_InventorySystem_Input_ItemName_Warning = "请输入物品名称";

# added on 7 May 2008 #
$i_InventorySystem_VarianceHandle_ReAssignLocationQuantity = "重新分配各位置的数量";
$i_InventorySystem_VarianceHandle_TotalAvaliableQuantity = "可分配数量";
$i_InventorySystem_VarianceHandle_TotalAssignedQuantity = "已分配数量";
$i_InventorySystem_VarianceHandle_TotalRemainingQuantity = "余下数量";
$i_InventorySystem_VarianceHandle_TotalIgnoreQuantity = "忽略数量";
$i_InventorySystem_VarianceHandle_TotalWriteOffQuantity = "报销数量";
$i_InventorySystem_VarianceHandle_InputWriteOffQuantity = "请输入各位置的报销数量";
$i_InventorySystem_VarianceHandle_InputWriteOffQty_Warning = "输入的报销数量无效";
$i_InventorySystem_VarianceHandle_InputReAssignQty_Warning = "重新分配各的位置数量无效";
$i_InventorySystem_VarianceHandle_InputReAssignQty_Warning = "重新分配各位置的数量与可分配数量不符";
$i_InventorySystem_VarianceHandle_QuantityRemaining_Warning = "余下数量";
$i_InventorySystem_VarianceHandle_SelectHandlerWarning = "请选择处理者";
$i_InventorySystem_VarianceHandle_SelectActionWarning = "请选择动作";
$i_InventorySystem_VarianceHandle_InputReAssignQtyWarning = "请输入各位置的数量";
$i_InventorySystem_VarianceHandle_ButtonCalculate = "计算";
$i_InventorySystem_VarianceHandle_ClickCalculateWarning = "请先按$i_InventorySystem_VarianceHandle_ButtonCalculate"."以便核对数量";
$i_InventorySystem_NewItem_BulkItemAdmin_Warning = "请选择误差总管";
$i_InventorySystem_UpdateItemLocation_OriginalLocation = "原有位置";
$i_InventorySystem_UpdateItemLocation_OriginalGroup = "原有管理组别";
$i_InventorySystem_UpdateItemLocation_OriginalQuantity = "原有数量";
$i_InventorySystem_UpdateItemLocation_NewLocation = "新位置";
$i_InventorySystem_UpdateItemLocation_NewGroup = "新管理组别";
$i_InventorySystem_UpdateItemLocation_NewQuantity = "新数量";
$i_InventorySystem_SettingOthers_StocktakePeriod_Warning = "盘点日期无效";
$i_InventorySystem_SettingOthers_ReminderDay_Warning = "请输入保用证到期提示日数";
$i_InventorySystem_SettingOthers_BarcodeLength_Warning = "请输入条码长度";
$i_InventorySystem_Setting_Catgeory_DeleteFail = "删除纪录失败，该类别已被使用。";
$i_InventorySystem_Setting_SubCatgeory_DeleteFail = "删除纪录失败，该子类别已被使用。";
$i_InventorySystem_Setting_Location_DeleteFail = "删除纪录失败，该位置已被使用。";
$i_InventorySystem_Setting_SubLocation_DeleteFail = "删除纪录失败，该子位置已被使用。";
$i_InventorySystem_Setting_ResourceManagementGroup_DeleteFail = "删除纪录失败，该管理小组已被使用。";
$i_InventorySystem_Setting_FundingSource_DeleteFail = "删除纪录失败，该资金来源已被使用。";
$i_InventorySystem_ItemFullList_DeleteItemFail = "删除纪录失败，该物品仍在使用中。";
$i_InventorySystem_ItemFullList_Cannot_New_Item = "无法新增物品。请前往<b>设定</b>，设置好<b>物品类别</b>、<b>位置/子位置</b>、<b>物品管理小组</b>及<b>资金来源 </b>后，重新新增物品。";

### added on 14 May 2008 ###
$i_InventorySystem_UpdateBulkItemStatus_Warning = "你所输入的物品数量大于/小于表列数量。确定继续?";
$i_InventorySystem_Import_CheckVarianceManager = "按此查询".$i_InventorySystem_Item_BulkItemAdmin;

### added on 15 May 2008 ###
$i_InventorySystem_VarianceHandling_CurrentLocation = "当前位置";
$i_InventorySystem_Report_Stocktake_NotDone = "还未进行盘点";
$i_InventorySystem_FullList_DeleteItemAlert = "该物品记录将被永久移除，你将再不能存取该物品之相关资讯。如欲于物品清单上移除该物品，但保留物品之过往记录，请注销物品。";
$i_InventorySystem_NewItem_PurchaseType_Help = "如选择\"现存物品\"，新增之物品必须与之后所选择的现存物品属于同一个物品管理小组及资金来源。如要新增之物品和已有物品名称相同，但属于不同的物品管理小组及资金来源，你仍应选择\"新物品\"。";
$i_InventorySystem_NewItem_SelectLocationFisrt_MSG = "先选择位置";
$i_InventorySystem_LabelPerPage = "每页列印数量";
$i_InventorySystem_Report_PCS = "件";
$i_InventorySystem_Stocktake_NotIncludeWriteOffQty = "不包括申请报销数量";

# added on 29 May 2008 #
$i_InventorySystem_SetAsResourceBookingItem = "设定为资源预订顶目";
$i_InventorySystem_Warning_SetBulkItemAsResourceItem = "大量物品不能用于资源预订";
$i_InventorySystem_ResourceItemsSetting = "资源顶目设定";

# added on 2 Jun 2008 #
$i_InventorySystem_RequestWriteOff_ResourceItemStatusSelection = "资源项目设定";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus1 = "删除资源项目";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus2 = "将项目状态转为审批中";
$i_InventorySystem_RequestWriteOff_ResourceItemStatus3 = "保留资源项目";
$i_InventorySystem_RequestWriteOff_ResourceItemConditionWarning = "请选择资源顶目设定";
$i_InventorySystem_UpdateStatus_ResourceItemStatus = "资源状况";

# added on 12 Jan 2009
$i_InventorySystem_Setting_ItemCodeFormat_Title = "物品编号格式";
$i_InventorySystem_Setting_ItemCodeFormat_Year = "购买年份";

# added on 22 Jan 2009
$i_InventorySystem_Setting_ItemCodeFormatSetting_Warning = "请先完成物品编号格式设定。";
$i_InventorySystem_JSWarning_NewItem_SelectResourceMgmtGroup = "请选择物品管理小组。";
$i_InventorySystem_JSWarning_NewItem_SelectLocation = "请选择位置。";
$i_InventorySystem_JSWarning_NewItem_SelectSubLocation = "请选择位置。";
$i_InventorySystem_JSWarning_NewItem_SelectFunding = "请选择资金来源。";
$i_InventorySystem_JSWarning_NewItem_InvalidItemBarcode = "条码无效，请重新输入。";
$i_InventorySystem_JSWarning_NewItem_ItemCodeEmpty = "请输入物品编号。";

# added on 4 Feb 2009
$i_InventorySystem_NewItem_ItemBarcodeRemark = "请参考 \"设定> 其他 > 条码格式\" 中的条码格式。";

# added on 20090213
$i_InventorySystem_ExportItemBarcode = "汇出条码";

# added on 20090324
$i_InventorySystem_ImportFileFormatWarning = "请确保汇入的csv档案为最新格式";
$i_InventorySystem_Setting_GroupLeaderRight = "组长权限";
$i_InventorySystem_Setting_GroupLeader_NotAllowAddItem = "不允许组长新增物品";
$i_InventorySystem_Setting_GroupLeader_AllowAddItem = "允许组长新增物品";
# added on 20090325
$i_InventorySystem_Setting_Photo_Use = "使用";
$i_InventorySystem_Setting_Photo_DoNotUse = "不使用";
# added on 20090331
$i_InventorySystem_Setting_Barcode_ChangedWarning = "注意：条码格式已被更改。";

# added on 20090409
$i_InventorySystem_ItemType_Single_ExportTitle_ExportTitle = "Single Items";
$i_InventorySystem_ItemType_Bulk_ExportTitle = "Bulk Items";
$i_InventorySystem_Item_Code_ExportTitle = "Item Code";
$i_InventorySystem_Item_Barcode_ExportTitle = "Barcode";
$i_InventorySystem_Item_ChineseName_ExportTitle = "Item Chinese Name";
$i_InventorySystem_Item_EnglishName_ExportTitle = "Item English Name";
$i_InventorySystem_Item_ChineseDescription_ExportTitle = "Chinese Description";
$i_InventorySystem_Item_EnglishDescription_ExportTitle = "English Description";
$i_InventorySystem_Item_CategoryCode_ExportTitle = "Category Code";
$i_InventorySystem_Item_Category2Code_ExportTitle = "Sub-category Code";
$i_InventorySystem_Item_GroupCode_ExportTitle = "Group Code";
$i_InventorySystem_Item_LocationCode_ExportTitle = "Location Code";
$i_InventorySystem_Item_Location2Code_ExportTitle = "Sub-location Code";
$i_InventorySystem_Item_FundingSourceCode_ExportTitle = "Funding Source Code";
$i_InventorySystem_Item_Ownership_ExportTitle = "Ownership";
$i_InventorySystem_Item_Warrany_Expiry_ExportTitle = "Warranty Expiry Date";
$i_InventorySystem_Item_License_ExportTitle = "License";
$i_InventorySystem_Item_Serial_Num_ExportTitle = "Serial No";
$i_InventorySystem_Item_Brand_Name_ExportTitle = "Brand";
$i_InventorySystem_Item_Supplier_Name_ExportTitle = "Supplier";
$i_InventorySystem_Item_Supplier_Contact_ExportTitle = "Supplier Contact";
$i_InventorySystem_Item_Supplier_Description_ExportTitle = "Supplier Description";
$i_InventorySystem_Item_Quot_Num_ExportTitle = "Quotation No";
$i_InventorySystem_Item_Tender_Num_ExportTitle = "Tender No";
$i_InventorySystem_Item_Invoice_Num_ExportTitle = "Invoice No";
$i_InventorySystem_Item_Purchase_Date_ExportTitle = "Purchase Date";
$i_InventorySystem_Item_Price_ExportTitle = "Total Purchase Amount";
$i_InventorySystem_Unit_Price_ExportTitle = "Unit Price";
$i_InventorySystemItemMaintainInfo_ExportTitle = "Maintanence Details";
$i_InventorySystem_Item_Remark_ExportTitle = "Remarks";
$i_InventorySystem_Item_BulkItemAdmin_ExportTitle = "Variance Manager";
$i_InventorySystem_Item_Qty_ExportTitle = "Quantity";
$i_InventorySystem_Item_CategoryName_ExportTitle = "Category";
$i_InventorySystem_Item_Category2Name_ExportTitle = "Sub-category";
$i_InventorySystem_Item_GroupName_ExportTitle = "Resource Management Group";
$i_InventorySystem_Item_LocationName_ExportTitle = "Location";
$i_InventorySystem_Item_Location2Name_ExportTitle = "Sub-location";
$i_InventorySystem_Item_FundingSourceName_ExportTitle = "Funding Source";

## added on 20090416
$i_InventorySystem_FixedAssetsRegister_Warning = "当使用「位置」作为显示次序时, 系统会依据个别位置中大量物品的数目，计算物品的单位价格、价值及成本，该等计算结果*可能*与实际数值有所偏差。如需准确了解单位价格、价值及成本, 请选用\"类型\"或\"物品管理小组\"作为显示次序。";
$i_InventorySystem_ExportItemDetails1 = "汇出 (编号格式)";
$i_InventorySystem_ExportItemDetails2 = "汇出 (名称格式)";
$i_InventorySystem_ExportWarning = "如没有选择任何物品, 系统将会汇出全部物品资料。确定继续？";
$i_InventorySystem_CategorySetting_PriceCeiling = "价格上限";
$i_InventorySystem_CategorySetting_PriceCeiling_ApplyToBulk = "此设定适用于大量物品";
$i_InventorySystem_CategorySetting_PriceCeiling_JSWarning1 = "请输入有效的价格上限";

## added on 20090527
$i_InventorySystem_DeleteItemPhoto = "删除现时物品相片";

## added on 20090812
$i_InventorySystem_StocktakeType_AllItem = "显示所有物品";
$i_InventorySystem_StocktakeType_StocktakeNotDone = "只显示盘点未完成的";

### End Of eInventory Words ###

$i_StudentRegistry['System'] = "学籍纪录系统";
$i_StudentRegistry_ModifiedSince = "上次更新日期";
$i_StudentRegistry_AuthCodeMain = "教青局学生编号";
$i_StudentRegistry_AuthCodeCheck = "校验位";
$i_StudentRegistry_PlaceOfBirth = "出生地点";
$i_StudentRegistry_ID_Type = "身份证明文件种类";
$i_StudentRegistry_ID_No = "身份证明文件编号";
$i_StudentRegistry_ID_IssuePlace = "身份证明文件发出地点";
$i_StudentRegistry_ID_IssueDate = "身份证明文件本次发出日期";
$i_StudentRegistry_ID_ValidDate = "身份证明文件有效日期";
$i_StudentRegistry_SP_Type = "逗留许可";
$i_StudentRegistry_SP_No = "逗留许可编号";
$i_StudentRegistry_SP_IssueDate = "逗留许可本次发出日期";
$i_StudentRegistry_SP_ValidDate = "逗留许可有效日期";
$i_StudentRegistry_Province = "籍贯";
$i_StudentRegistry_ResidentialArea_Night = "夜间住宿地区";
$i_StudentRegistry_ResidentialArea = "居住地区";
$i_StudentRegistry_ResidentialRoad = "住址街名";
$i_StudentRegistry_ResidentialAddress = "住址门牌、大厦、层数、座";

$i_StudentRegistry_ExportPurpose = "汇出用途";
$i_StudentRegistry_ExportFormat_XML_1 = "用于批量查询学生编号";
$i_StudentRegistry_ExportFormat_XML_2 = "用于学生资料升级(方案二)";
$i_StudentRegistry_ExportFormat_XML_3 = "用于学生资料升级(方案三/五)";

$i_StudentRegistry_EnglishName_Alert = "英名姓必需为大写。现正被自动转换。请储存此变更。";

$i_UserAddress_Area = "居住地区";
$i_UserAddress_Road = "住址街名";
$i_UserAddress_Address = "住址门牌、大厦、层数、座";

$i_StudentGuardian_EmergencyContact = "紧急联络人";
$i_StudentGuardian_LiveTogether = "同住";
$i_StudentGuardian_Occupation = "职业";

$i_StudentRegistry_AreaCode = array();//see cust. template
$i_StudentRegistry_PlaceOfBirth_Code = array();//see cust. template
$i_StudentRegistry_ID_Type_Code = array();//see cust. template
$i_StudentRegistry_ID_IssuePlace_Code = array();//see cust. template
$i_StudentRegistry_Country_Code = array();//see cust. template
$i_StudentRegistry_ResidentialAreaNight_Code = array();//see cust. template


$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE1 = "EE1 - 特殊教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE2 = "EE2 - 特殊教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EE3 = "EE3 - 特殊教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EEP = "EEP - 特殊教育";

$i_WebSAMS_AttendanceCode_Option_ClassLevel_1 = "1 - 葡文小学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_2 = "2 - 葡文小学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_3 = "3 - 葡文小学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_4 = "4 - 葡文小学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_5 = "5 - 葡文小学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_6 = "6 - 葡文小学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_EP6 = "EP6 - 英文中学预备班";

$i_WebSAMS_AttendanceCode_Option_ClassLevel_F1 = "F1 - 英文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F2 = "F2 - 英文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F3 = "F3 - 英文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F4 = "F4 - 英文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F5 = "F5 - 英文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_F6 = "F6 - 英文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_7 = "7 - 葡文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_8 = "8 - 葡文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_9 = "9 - 葡文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_10 = "10 - 葡文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_11 = "11 - 葡文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_12 = "12 - 葡文中学";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERP = "ERP - 小学回归教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERG = "ERG - 初中回归教育";
$i_WebSAMS_AttendanceCode_Option_ClassLevel_ERC = "ERC - 高中回归教育";

$ec_iPortfolio['guardian_import_remind_sms'] = "注意: <Br> - 第一栏为RegNo 编号, 应加上#（如 \"#0025136\")，以确保编号数字的完整！ <Br> - 第二栏为监护人的英文姓名 <Br> - 第三栏为监护人的中文姓名 <Br> - 第四栏为监护人的联络号码 <Br> - 第五栏为监护人的紧急联络号码 <Br> - * 第六栏为监护人与学生关系 <Br> - 第七栏为设定为学生的主监护人 (1 - 是, 0 - 否) <Br> - 第八栏为设定为SMS的接收者 (1 - 是, 0 - 否) <br> - 第九栏为设定监护人做紧急联络 (1 - 是, 0 - 否) <br> * (01 - 父亲, 02 - 母亲, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 亲戚, 08 - 其他) <Br> - <font color=red>新汇入的监护人资料将会覆盖现有的监护人资料。</font><br>";

$ec_iPortfolio['guardian_import_remind_studentregistry'] = "注意: <Br> - 第一栏为RegNo 编号, 应加上#（如 \"#0025136\")，以确保编号数字的完整！ <Br> - 第二栏为监护人的英文姓名 <Br> - 第三栏为监护人的中文姓名 <Br> - 第四栏为监护人的联络号码 <Br> - 第五栏为监护人的紧急联络号码 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六栏为监护人与学生关系 <Br> - 第七栏为设定为学生的主监护人 (1 - 是, 0 - 否) <Br> - 第八栏为设定监护人做紧急联络 (1 - 是, 0 - 否) <Br> - 第九栏为监护人职业 <Br> - 第十栏为监护人是否与该学生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> 第十一栏为地址区码<Br> - 第十二栏为住址街道 <Br> - 第十三栏为住址其他资料，如大厦，层数及单位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父亲, 02 - 亲, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 亲戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>11</span></span> (O - 其他, M - 澳门, T - &#27705;仔, C - 路环) <Br> - <font color=red>新汇入的监护人资料将会覆盖现有的监护人资料。</font><br>";

$ec_iPortfolio['guardian_import_remind_sms_studentregistry'] = "注意: <Br> - 第一栏为RegNo 编号, 应加上#（如 \"#0025136\")，以确保编号数字的完整！ <Br> - 第二栏为监护人的英文姓名 <Br> - 第三栏为监护人的中文姓名 <Br> - 第四栏为监护人的联络号码 <Br> - 第五栏为监护人的紧急联络号码 <Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> 第六栏为监护人与学生关系 <Br> - 第七栏为设定为学生的主监护人 (1 - 是, 0 - 否) <Br> - 第八栏为设定为SMS的接收者 (1 - 是, 0 - 否) <br> - 第九栏为设定监护人做紧急联络 (1 - 是, 0 - 否)  <Br> - 第十栏为监护人职业 <Br> - 第十一栏为监护人是否与该学生同住 (1 - 是, 0 - 否)<Br> - <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> 第十二栏为地址区码<Br> - 第十三栏为住址街道 <Br> - 第十四栏为住址其他资料，如大厦，层数及单位 <br> <span style='color:#0000ff'>*<span style='font-size:8px'>6</span></span> (01 - 父亲, 02 - 母亲, 03 - 祖父, 04 - 祖母, 05 - 兄/弟, 06 - 姊/妹, 07 - 亲戚, 08 - 其他) <br> <span style='color:#0000ff'>*<span style='font-size:8px'>12</span></span> (O - 其他, M - 澳门, T - &#27705;仔, C - 路环) <Br> - <font color=red>新汇入的监护人资料将会覆盖现有的监护人资料。</font><br>";

$i_StudentRegistry_Import_XML_Format = "XML 汇入格式:<br />
&lt;?xml version=&quot;1.0&quot; encoding=&quot;ISO-8859-1&quot;?&gt;<br />
&lt;!DOCTYPE SRA SYSTEM &quot;http://app.dsej.gov.mo/prog/edu/sra/sra3.dtd&quot;&gt;<br />

&lt;SRA&gt;<br />
&lt;STUDENT&gt;<br />
&lt;STUD_ID&gt;12345&lt;/STUD_ID&gt;<br />
&lt;CODE&gt;9400999-6&lt;/CODE&gt;<br />
&lt;NAME_C&gt;&#37758;&#22283;&#20161;&lt;/NAME_C&gt;<br />
&hellip;<br />
&lt;/STUDENT&gt;<br />
&lt;STUDENT&gt;<br />
&lt;STUD_ID&gt;12346&lt;/STUD_ID&gt;<br />
&lt;CODE&gt;9400998-X&lt;/CODE&gt;<br />
&lt;NAME_C&gt;&#37758;&#22283;&#26862;&lt;/NAME_C&gt;<br />
&hellip;<br />
&lt;/STUDENT&gt;<br />
&hellip;<br />
&lt;/SRA&gt;";

$i_StudentRegistry_Import_Invalid_Record = "档案含有无效纪录";
$i_StudentRegistry_Import_No_Record = "档案未包含任何纪录";
$i_StudentRegistry_Import_ValidInvalid_Record = "档案含有有效纪录及错误纪录。 如继续汇入，只有有效纪录会被汇入";
$i_StudentRegistry_Import_Valid_Record = "档案含有有效纪录";

$i_StudentRegistry_Export_Invalid_Record = "汇出的资料包含不完整的纪录。";
$i_StudentRegistry_Export_Valid_Record = "汇出的资料包括 <--NoOfValid--> 个完整纪录。";

$i_general_clickheredownloaddtd = '按此下载 DTD 档';
$i_general_invalid = '资料无效';
$i_general_incomplete = '资料不完整';

$i_StudentRegistry_Import_Invalid_Reason = "此问题可能由于：<br>1. <span style='color:red;'>-</span> 没有资料<br>2. <span style='color:red;'>[已输入资料]</span> 错误资料。";
$i_StudentRegistry_Export_Invalid_Reason = "此问题可能由于：<br><span style='color:red;'>-</span> 没有资料";


##########################################
# eCommunity
##########################################
$eComm['eCommunity'] = "eCommunity 网上社群";
$eComm['MyGroups'] = "我的社群";
$eComm['OtherGroups'] = "其他社群";
$eComm['MyPage'] = "我的主页";
$eComm['Settings'] = "设定";
$eComm['LatestShare'] = "最新分享";
$eComm['Photo'] = "相片";
$eComm['Video'] = "视讯";
$eComm['Website'] = "网站";
$eComm['File'] = "档案";
$eComm['P'] = $eComm['Photo'];
$eComm['V'] = $eComm['Video'];
$eComm['W'] = $eComm['Website'];
$eComm['F'] = $eComm['File'];
$eComm['Add_'] = "新增";
$eComm['New_'] = "新增";
$eComm['Edit_'] = "编辑";

$eComm['View'] = "检视";
$eComm['Add'] = "新增";

$eComm_Field_Topic = "主题";
$eComm_Field_Createdby = "发表者";
$eComm_Field_ReplyNo = "回复数目";
$eComm_Field_LastUpdated = "最近更新";
$eComm['Forum'] = "论坛";
$eComm['Title'] = "标题";
$eComm['Content'] = "内容";
$eComm['Group'] = "社群";
$eComm['YourReply'] = "你的回复";
$eComm['Search'] = "搜寻";
$eComm['Close'] = "关闭";
$eComm['Attachment'] = "附件";

$eComm['Share'] = "资源分享";
$eComm['By'] = "自";
$eComm['Comment'] = "意见";
$eComm['Album'] = "相簿";
$eComm['Folder'] = "资料夹";
$eComm['Category'] = "类别";
$eComm['NewAlbum'] = "新增" . $eComm['Album'];
$eComm['NewFolder'] = "新增" . $eComm['Folder'];
$eComm['NewCateogry'] = "新增" . $eComm['Cateogry'];
$eComm['EditAlbum'] = "编辑" . $eComm['Album'];
$eComm['EditFolder'] = "编辑" . $eComm['Folder'];
$eComm['EditCateogry'] = "编辑" . $eComm['Cateogry'];
$eComm['LastUpload'] = "最新上载";
$eComm['Status'] = "状态";
$eComm['Public'] = "公开";
$eComm['Private'] = "非公开";
$eComm['Top'] = "最高";
$eComm['jsDeleteRecord'] = "你是否确定要删除记录？";
$eComm['jsDeleteFolder'] = "你是否确定要删除记录？所有相关档案及意见将一并删除。";
$eComm['CreatedBy'] = "上载者";
$eComm['CreatedDate'] = "分享日期";
$eComm['WriteYourcomment'] = "发表意见";
$eComm['Yourcomment'] = "你的意见";
$eComm['NoRecord'] = "尚未有纪录";


$eComm['Steps'] = "步骤";
$eComm['select_file'] = "选择/上载档案";
$eComm['select_photo'] = "选择/上载相片";
$eComm['input_website'] = "输入网址 ";
$eComm['select_video'] = "选择你的影片 ";
$eComm['input_detail_info'] = "输入详细资料";
$eComm['add_file_finished'] = "档案上载完毕";
$eComm['add_photo_finished'] = "相片上载完毕";
$eComm['add_website_finished'] = "已加入网页";
$eComm['add_video_finished'] = "已加入影片";
$eComm['add_to_folder'] = "加入到资料夹";
$eComm['add_to_album'] = "加入到相簿";
$eComm['add_to_category'] = "加入到类别";
$eComm['move_to_folder'] = "移进资料夹";
$eComm['move_to_album'] = "移进相簿";
$eComm['move_to_category'] = "移至类别";
$eComm['Upload'] = "上载";
$eComm['URL'] = "网址";
$eComm['js_please_select_file'] = "你必须选择最少一个档案。";
$eComm['js_please_select_photo'] = "你必须选择最少一张相片。";
$eComm['js_please_input_url'] = "你必须选择最少一个网址。";
$eComm['info_str']['P'] = "相片上载完毕。你可以按一下图示，填写详细资料。";
$eComm['info_str']['F'] = "档案上载完毕。你可以按一下图示，填写详细资料。";
$eComm['info_str']['W'] = "已加入网页。你可以按一下图示，填写详细资料。";
$eComm['info_str']['V'] = "影片上载完毕。你可以按一下图示，填写详细资料。";
$eComm['Description'] = "描述";
$eComm['remove_this_file'] = "移除此档案";
$eComm['remove_this_video'] = "移除此影片";
$eComm['set_private'] = "设定为非公开";
$eComm['set_all_to_folder'] = "将所有档案加入到资料夹";
$eComm['set_all_to_album'] = "将所有相片加入到相簿";
$eComm['set_all_to_category'] = "将所有网页加入到类别";
$eComm['set_all_to_video_album'] = "将所有影片加入到资料夹";
$eComm['select_all'] = "全选";

$eComm['Member'] = "组员";
$eComm['NewTopic'] = "新主题";
$eComm['NewMember'] = "新增组员";
$eComm['Storage'] = "储存量";
$eComm['NoFolder']['P'] = "尚未有相簿，请联络你的系统管理员建立相簿，然后再行上载。";
$eComm['NoFolder']['F'] = "尚未有资料夹，请联络你的系统管理员建立资料夹，然后再行上载。";
$eComm['NoFolder']['W'] = "尚未有类别，请联络你的系统管理员建立资料夹，然后再行上载。";
$eComm['NoFolder']['V'] = "尚未有影片资料夹，请联络你的系统管理员建立资料夹，然后再行上载。";
$eComm['Performance'] = "表现";
$eComm['MemberList'] = "组员名单";
$eComm['Warning'] = "警告";
$eComm['no_space'] = "没有足够储存空间";
$eComm['no_file'] = "没有上载任何档案";
$eComm['no_website'] = "没有加入任何网页";
$eComm['chatroom'] = "聊天室";
$eComm['GroupSettings'] = "小组设定";
$eComm['MemberSettings'] = "成员设定";
$eComm['AnnouncementSettings'] = "宣布设定";
$eComm['SharingSettings'] = "分享设定";
$eComm['ForumSettings'] = "讨论区设定";
$eComm['GroupLogo'] = "群组图示";
$eComm['GroupLogo_PhotoGuide'] = "群组图示只接受'.JPG'、'.GIF'或'.PNG'等格式，而且尺寸必须为225 x 170像素(阔x高)。";
$eComm['GroupLogoDelete'] = "删除现有群组图示";
$eComm['PublicStatus'] = "Public Status";
$eComm['Management'] = "管理";
$eComm['IndexAnnounce'] = "Announcement# at index page";
$eComm['FolderType'] = "资料夹种类";
$eComm['FileType'] = "档案类型";
$eComm['FileName'] = "档案名称";
$eComm['NoFolderSelected'] = "没有选择资料夹";
$eComm['WaitingRejectList'] = "等待批核 / 不获批核列表";
$eComm['WaitingApproval'] = "等待批核";
$eComm['Rejected'] = "不获批核";
$eComm['ApprovalStatus'] = "批核状况";
$eComm['DisplayOnTop'] = "置顶";
$eComm['GrantAdmin'] = "授予管理权限";
$eComm['LatestNumber'] = "最新纪录编号";
$eComm['NeedApproval'] = "需要批核";
$eComm['SetAsCoverImage'] = "设为封面图像";
$eComm['DefaultViewModeIndexPage'] = "首页预设检视模式";
$eComm['ViewMode_List'] = "列表";
$eComm['ViewMode_4t'] = "4 图示";
$eComm['ViewMode_2t'] = "2 图示";
$eComm['ViewMode_1t'] = "1 图示";
$eComm['AllowedImageTypes'] = "接受相片档案格式";
$eComm['AllowedFileTypes'] = "接受档案格式";
$eComm['AllowedVideoTypes'] = "接受影片档案格式";
$eComm['AllowedTypesRemarks'] = "\"ALL\" 代表接受任何格式，若你希望接受多种格式，请用 / 将档名分隔 e.g. jpg/bmp/tif";
$eComm['file_type_not_allowed'] = "档案上载失败，不接受此档案格式";
$eComm['CalendarSettings'] = "月历";
$eComm['NoCalendar'] = "暂时未有月历，现在加入?";
$eComm['CreateCalendar'] = "设定月历";
$eComm['DisplayAtGroupIndex'] = "在群组首页显示?";
$eComm['AddTo'] = "<< 增加";
$eComm['DeleteTo'] = "删除 >>";
$eComm['Message'] = "讯息";
$eComm['TopicSettings'] = "相关主题设定";

# HKU Medicine research
$i_MedicalReasonTitle = "健康分类";
$i_MedicalReasonName[] = array(0,"未能定义");
$i_MedicalReasonName[] = array(1,"疑似流感/呼吸道疾病");
$i_MedicalReasonName[] = array(2,"肠胃科疾病");
$i_MedicalReasonName[] = array(3,"手足口病");
$i_MedicalReasonName[] = array(99,"其他疾病");
$i_MedicalReasonName[] = array(999,"非因疾缺席");
$i_MedicalReason_Export = "汇出健康报表";
$i_MedicalDataTransferSuccess = "资料已传送。";
$i_MedicalDataTransferFailed = "资料传送失败。";
$i_MedicalReason_Export2="汇出健康报表";
$i_MedicalExportError ="无法汇出报告，因为WebSAMS级别名称尚未设定妥当。";

# eLibrary
$i_eLibrary_System = "电子图书";
$i_eLibrary_System_Admin_User_Setting = "设定管理用户";
$i_eLibrary_System_Current_Batch = "现有批次";
$i_eLibrary_System_Update_Batch = "批次更新";
$i_eLibrary_System_Updated_Batch = "已更新批次";
$i_eLibrary_System_Reset_Fail = "更新批次失败";
$i_eLibrary_System_Updated_Records = "已更新记录";
$eLib["AdminUser"] = "管理用户";

$eLib['ManageBook']["Title"] = "管理书籍";
$eLib['ManageBook']["ConvertBook"] = "转换图书";
$eLib['ManageBook']["ContentManage"] = "管理内容";
$eLib['ManageBook']["ContentView"] = "检视内容";
$eLib['ManageBook']["ImportBook"] = "汇入图书";
$eLib['ManageBook']["ImportCSV"] = "汇入CSV档案";
$eLib['ManageBook']["ImportXML"] = "汇入XML档案";
$eLib['ManageBook']["ImportZIP"] = "汇入ZIP档案";
$eLib['ManageBook']["ImportZIPDescribe"] = "
				ZIP档案需备下档案<br />
				content.text - text档,  将转换成XML档案 <br />
				 \"image\" 文件夹 - 包含相关图片案. <br />
				control.text - 控制转换过程. <br />
													";

$eLib['Book']["Author"] = "作者";
$eLib['Book']["SeriesEditor"] = "系列编辑";
$eLib['Book']["Category"] = "类型";
$eLib['Book']["Subcategory"] = "子类型";
$eLib['Book']["DateModified"] = "最近修改日期";
$eLib['Book']["Description"] = "简介";
$eLib['Book']["Language"] = "语言";
$eLib['Book']["Level"] = "等级";
$eLib['Book']["AdultContent"] = "敏感内容";
$eLib['Book']["Title"] = "书名";
$eLib['Book']["Publisher"] = "出版社";
$eLib['Book']["TableOfContent"] = "目录";
$eLib['Book']["Copyright"] = "版权";

$eLib["SourceFrom"] = "资料来源";
$eLib['Source']["green"] = "Green Apple";
$eLib['Source']["cup"] = "Cambridge University Press";

$eLib['EditBook']["IsChapterStart"] = "章节开始?";
$eLib['EditBook']["ChapterID"] = "章节索引";

# HTML editor
$ec_html_editor['bold'] = "粗体";
$ec_html_editor['italic'] = "斜体";
$ec_html_editor['underline'] = "加底线";
$ec_html_editor['strikethr'] = "删除线";
$ec_html_editor['subscript'] = "下标";
$ec_html_editor['superscript'] = "上标";
$ec_html_editor['justifyleft'] = "靠左对齐";
$ec_html_editor['justifyright'] = "靠右对齐";
$ec_html_editor['justifycent'] = "置中";
$ec_html_editor['justifyfull'] = "左右对齐";
$ec_html_editor['orderlist'] = "编号";
$ec_html_editor['bulletlist'] = "项目符号";
$ec_html_editor['decindent'] = "减少缩排";
$ec_html_editor['incindent'] = "增加缩排";
$ec_html_editor['fontcolor'] = "字型色彩";
$ec_html_editor['bgcolor'] = "醒目提示";
$ec_html_editor['horrule'] = "插入分隔线";
$ec_html_editor['weblink'] = "插入或编辑超连结";
$ec_html_editor['insertimage'] = "插入或编辑图片";
$ec_html_editor['table'] = "表格功能";
$ec_html_editor['htmlsource'] = "浏览纲页原始码";
$ec_html_editor['topicsent'] = "中心句子";
$ec_html_editor['wordcount'] = "字数总计";
$ec_html_editor['highlight'] = "强调";
$ec_html_editor['missing'] = "错漏";
$ec_html_editor['comment'] = "提议";
$ec_html_editor['correct'] = "正确";
$ec_html_editor['pcorrect'] = "部分正确";
$ec_html_editor['incorrect'] = "不正确";
$ec_html_editor['appreciate'] = "赞赏";
$ec_html_editor['markcode'] = $ec_wordtemplates['marking_code'];
$ec_html_editor['errsummy'] = "错误概括";
$ec_html_editor['markscheme'] = $ec_wordtemplates['marking_scheme'];
$ec_html_editor['sticker'] = "贴纸";
$ec_html_editor['undo'] = "复原";
$ec_html_editor['redo'] = "取消复原";
$ec_html_editor['hvmode'] = "直书/横书";
$ec_html_editor['mgcomment'] = "注解";
$ec_html_editor['linespace'] = "设定行距";
$ec_html_editor['copy'] = "复制";
$ec_html_editor['cut'] = "剪下";
$ec_html_editor['paste'] = "贴上";
$ec_html_editor['font'] = "字型";
$ec_html_editor['font_size'] = "字型大小";
$ec_html_editor['clear_text_style'] = "消除字型格式";
$ec_html_editor['createanchor'] = "建立或编辑锚点";
$ec_html_editor['anchorname'] = "锚点名称";
$ec_html_editor['anchor'] = "锚点";
$ec_html_editor['selectcolor'] = "选择颜色";
$ec_html_editor['targetwindow'] = "目标视窗";
$ec_html_editor['power_voice'] = "Power Voice";

$ec_html_editor['headingNor'] = "预设标题";
$ec_html_editor['heading1'] = "标题一";
$ec_html_editor['heading2'] = "标题二";
$ec_html_editor['heading3'] = "标题三";
$ec_html_editor['heading4'] = "标题四";
$ec_html_editor['heading5'] = "标题五";
$ec_html_editor['heading6'] = "标题六";
$ec_html_editor['headingAdd'] = "位址";

$ec_html_editor['inserttable'] = "插入表格...";
$ec_html_editor['edittable'] = "编辑表格格式...";
$ec_html_editor['editcell'] = "编辑储存格格式...";
$ec_html_editor['insertcoll'] = "插入左方栏";
$ec_html_editor['insertcolr'] = "插入右方栏";
$ec_html_editor['insertrowa'] = "插入上方列";
$ec_html_editor['insertrowb'] = "插入下方列";
$ec_html_editor['increasecols'] = "增加栏合并";
$ec_html_editor['decreasecols'] = "减少栏合并";
$ec_html_editor['increaserows'] = "增加列合并";
$ec_html_editor['decreaserows'] = "减少列合并";
$ec_html_editor['deleterow'] = "删除列";
$ec_html_editor['deletecol'] = "删除栏";

$ec_html_editor['image_url'] = "图片 URL";
$ec_html_editor['alternate_text'] = "提示文字";
$ec_html_editor['layout'] = "外观";
$ec_html_editor['alignment'] = "对齐方式";
$ec_html_editor['border_thickness'] = "框线宽度";
$ec_html_editor['spacing'] = "间隔";
$ec_html_editor['horizontal'] = "水平间距";
$ec_html_editor['vertical'] = "垂直间距";

$ec_html_editor['al_none'] = "无";
$ec_html_editor['al_left'] = "向左对齐";
$ec_html_editor['al_center'] = "置中";
$ec_html_editor['al_right'] = "向右对齐";
$ec_html_editor['al_texttop'] = "向文字顶部对齐";
$ec_html_editor['al_absmiddle'] = "绝对置中";
$ec_html_editor['al_baseline'] = "向文字底部对齐";
$ec_html_editor['al_absbottom'] = "绝对置下";
$ec_html_editor['al_bottom'] = "向下对齐";
$ec_html_editor['al_middle'] = "向中对齐";
$ec_html_editor['al_top'] = "向上对齐";

$ec_html_editor['tb_size'] = "大小";
$ec_html_editor['tb_row'] = "列数";
$ec_html_editor['tb_col'] = "栏数";
$ec_html_editor['tb_height'] = "高度";
$ec_html_editor['tb_width'] = "宽度";
$ec_html_editor['tb_pixel'] = "像素";
$ec_html_editor['tb_percent'] = "百分比";
$ec_html_editor['tb_cellspacing'] = "储存格间距";
$ec_html_editor['tb_cellpadding'] = "储存格填塞";
$ec_html_editor['tb_color'] = "色彩";
$ec_html_editor['tb_bgcolor'] = "网底色彩";
$ec_html_editor['tb_bordercolor'] = "框线色彩";
$ec_html_editor['tb_wrap'] = "换行";
$ec_html_editor['tb_nowrap'] = "换行";
$ec_html_editor['tb_nowrap_yes'] = "不换行";
$ec_html_editor['tb_nowrap_no'] = "自动换行";
$ec_html_editor['tb_hor_alignment'] = "水平对齐";
$ec_html_editor['tb_ver_alignment'] = "垂直对齐";

$ec_html_editor['warn_image_url'] = "必须填上图片URL";
$ec_html_editor['warn_hor_space'] = "水平间距必须是 0 至 999";
$ec_html_editor['warn_border_thick'] = "框线宽度必须是 0 至 999";
$ec_html_editor['warn_ver_space'] = "垂直间距必须是 0 至 999";
$ec_html_editor['warn_specify_a'] = "必须输入";
$ec_html_editor['warn_specify_b'] = "的数值";
$ec_html_editor['warn_specify_num_a'] = "";
$ec_html_editor['warn_specify_num_b'] = "的数值必须是 0 至 999";
$ec_html_editor['warn_overwrite'] = "你是否确定要取替已选的内容？";

$ec_html_editor['total_word'] = "字数";
$ec_html_editor['total_symbol'] = "标点符号";

$ec_html_editor['save_content'] = "储存内容";
$button_ok = "确定";

$file_kb = "KB";
$file_mb = "MB";

###############################################################################
# iPortfolio 2.5
###############################################################################
$ec_iPortfolio['above_mean'] = "高于全级总平均分";
$ec_iPortfolio['academic_performance'] = "校内高中学科成绩";
$ec_iPortfolio['account_quota_free'] = "剩余可使用的户口额";
$ec_iPortfolio['achievement'] = "奖项 / 证书文凭 / 成就 (如有)";
$ec_iPortfolio['action_type'] = "动作";
$ec_iPortfolio['activate'] = "启用";
$ec_iPortfolio['activation_quota_afterward'] = "启用后将剩余户口额";
$ec_iPortfolio['activation_result_activated'] = "此户口在之前已被启用";
$ec_iPortfolio['activation_result_no_regno'] = "欠缺学生编号";
$ec_iPortfolio['activation_student_reactivate'] = "再启用的学生数目";
$ec_iPortfolio['activation_student_activate'] = "启用的学生数目";
$ec_iPortfolio['activation_total_fail'] = "启用失败的学生户口数目";
$ec_iPortfolio['activation_total_success'] = "启用成功的学生户口数目";
$ec_iPortfolio['activity'] = "活动纪录";
$ec_iPortfolio['activity_name'] = "活动名称";
$ec_iPortfolio['address'] = "地址";
$ec_iPortfolio['admission_date'] = "入学日期";
$ec_iPortfolio['adv_analyze'] = "搜寻式报告";
$ec_iPortfolio['all_class'] = "全部班级";
$ec_iPortfolio['all_classlevel'] = "全部年级";
$ec_iPortfolio['all_ele'] = "全部其他学习经历种类";
$ec_iPortfolio['all_school_year'] = "全部";
$ec_iPortfolio['all_statistic'] = "全级统计";
$ec_iPortfolio['amount'] = "数量";
$ec_iPortfolio['analysis_report'] = "分析报告";
$ec_iPortfolio['assessment_report'] = "学业报告";
$ec_iPortfolio['attendance'] = "考勤纪录";
$ec_iPortfolio['attendance_detail'] = "考勤纪录";
$ec_iPortfolio['available_portfolio_acc'] = "剩余可使用的户口额";

$ec_iPortfolio['award'] = "奖项";
$ec_iPortfolio['award'] = "得奖纪录";

$ec_iPortfolio['award_achievement'] = "奖项及成就";
$ec_iPortfolio['award_date'] = "得奖日期";
$ec_iPortfolio['award_name'] = "奖项";
$ec_iPortfolio['based_on'] = "按";
$ec_iPortfolio['basic_info'] = "基本资料";
$ec_iPortfolio['below_mean'] = "低于全级总平均分";
$ec_iPortfolio['blue'] = "蓝";
$ec_iPortfolio['bold'] = "粗体";
$ec_iPortfolio['by_semester'] = '学期';
$ec_iPortfolio['by_year'] = '年度';
$ec_iPortfolio['category'] = "分类";
$ec_iPortfolio['choose_group'] = "请选择以下小组";

$ec_iPortfolio['class'] = "班别";
$ec_iPortfolio['class'] = "班级";

$ec_iPortfolio['class_and_number'] = "班别 - 学号";
$ec_iPortfolio['class_list'] = "班别名单";
$ec_iPortfolio['class_number'] = "班别 -<br>学号";
$ec_iPortfolio['class_perform_stat'] = "学期/年度表现统计";
$ec_iPortfolio['class_photo_number'] = "相片数目/<span class='guide'>*</span>学生总数";
$ec_iPortfolio['class_review'] = "班别内互评";
$ec_iPortfolio['color'] = "颜色";
$ec_iPortfolio['comments'] = "评语/意见";
$ec_iPortfolio['comment_chi'] = "中文评语";
$ec_iPortfolio['comment_eng'] = "英文评语";
$ec_iPortfolio['composite_performance_report'] = "综合表现报告";
$ec_iPortfolio['conduct_grade'] = "操行";
$ec_iPortfolio['copied_portfolio_reomved'] = "早前预备了的 XXX 班同学的学习档案已被删除。";
$ec_iPortfolio['copied_portfolio_reomved_fail'] = "早前预备了的 XXX 班同学的学习档案未能被删除。";
$ec_iPortfolio['copied_template_title'] = "复制模板名称";
$ec_iPortfolio['copy_web_portfolio_files'] = "预备学习档案";
$ec_iPortfolio['csv_report_config'] = "学生学习纪录 CSV 档案上载";
$ec_iPortfolio['date'] = "日期";
$ec_iPortfolio['date_issue'] = "发出日期";
$ec_iPortfolio['deactive'] = "暂停启用";
$ec_iPortfolio['delete'] = "删除";
$ec_iPortfolio['detail'] = "详细资料";
$ec_iPortfolio['details'] = "详情";
$ec_iPortfolio['disable_review'] = "否";
$ec_iPortfolio['display'] = "显示";
$ec_iPortfolio['display_by_grade'] = "以等级显示";
$ec_iPortfolio['display_by_rank'] = "以级名次显示";
$ec_iPortfolio['display_by_score'] = "以分数显示";
$ec_iPortfolio['display_by_stand_score'] = "以标准分数显示";
$ec_iPortfolio['display_class_distribution'] = "显示班别统计";
$ec_iPortfolio['display_class_stat'] = "显示班别统计";
$ec_iPortfolio['draft'] = "草稿";
$ec_iPortfolio['edit'] = "编辑";
$ec_iPortfolio['edit_content'] = "编辑内容";
$ec_iPortfolio['edit_portfolios'] = "编辑";
$ec_iPortfolio['edit_template_alert'] = "注意：此页面正在使用模板 \'XXX\'，对此页面所作的任何改动，将反映到其他采用相同模板的页面。如只想修改此页面, 请建立一个新模板。";
$ec_iPortfolio['edit_this_page'] = "编辑此页";
$ec_iPortfolio['ele'] = "其他学习经历种类";
$ec_iPortfolio['em_phone'] = "紧急联络电话";
$ec_iPortfolio['enable_review'] = "是";
$ec_iPortfolio['end_report'] = "完";
$ec_iPortfolio['enter_student_name'] = "输入学生姓名";
$ec_iPortfolio['failed_uploaded_photo_count'] = "未能上载的相片数目";
$ec_iPortfolio['finished_cd_burning_preparation'] = "XXX 班同学的学习档案已预备至以下文件夹：";

$ec_iPortfolio['form'] = "班别";
$ec_iPortfolio['form'] = "年级";

$ec_iPortfolio['friends_review'] = "朋友互评";
$ec_iPortfolio['full_mark'] = "满分";
$ec_iPortfolio['full_report'] = "整体报告";
$ec_iPortfolio['full_report_config'] = "整体报告设定";
$ec_iPortfolio['generate_score_list'] = "制作分数表";
$ec_iPortfolio['get_prepared_portfolio'] = "学生的学习档案放置在以学生编号命名的文件夹内，老师可根据同学的学生编号去烧录 CD-ROM。";
$ec_iPortfolio['grade'] = "等级";
$ec_iPortfolio['green'] = "绿";
$ec_iPortfolio['green_frame'] = " (以 <span style=\"color:#3d7001; font-weight:bold\">绿色相框</span> 标示)";
$ec_iPortfolio['group_by'] = "请选择";
$ec_iPortfolio['growth_description'] = "简介";
$ec_iPortfolio['growth_group'] = "参予小组";
$ec_iPortfolio['growth_phase_period'] = "时段";
$ec_iPortfolio['growth_status'] = "状况";
$ec_iPortfolio['growth_title'] = "名称";
$ec_iPortfolio['guardian_info'] = "监护人资料";
$ec_iPortfolio['heading']['learning_portfolio'] = "学习档案";
$ec_iPortfolio['heading']['lp_updated'] = "学习档案更新数目";
$ec_iPortfolio['heading']['no'] = "编号";
$ec_iPortfolio['heading']['no_lp_active_stu'] = "已启用学习档案的学生数目";
$ec_iPortfolio['heading']['no_stu'] = "学生数目";
$ec_iPortfolio['heading']['photo'] = "照片";
$ec_iPortfolio['heading']['sb_scheme_updated'] = "校本计划更新数目";
$ec_iPortfolio['heading']['school_record_updated'] = "学校纪录更新数目";
$ec_iPortfolio['heading']['student_info'] = "个人资料";
$ec_iPortfolio['heading']['student_name'] = "学生名称";
$ec_iPortfolio['heading']['student_record'] = "学校纪录";
$ec_iPortfolio['heading']['student_regno'] = "学生编号";
$ec_iPortfolio['highest_score'] = "最高分";
$ec_iPortfolio['highlight_result'] = "强调方式";
$ec_iPortfolio['house'] = "社级";
$ec_iPortfolio['hours'] = "时数";
$ec_iPortfolio['hours_above'] = "小时以上";
$ec_iPortfolio['hours_below'] = "小时以下";
$ec_iPortfolio['hours_range'] = "时数范围";
$ec_iPortfolio['hours_to'] = "至";
$ec_iPortfolio['import_activation'] = "启用 iPortfolio 户口";
$ec_iPortfolio['import_regno'] = "汇入学生编号";
$ec_iPortfolio['iportfolio_folder_size'] = "档案储存容量";
$ec_iPortfolio['iPortfolio_published'][1] = "<font color=green>你 的 iPortfolio 已 成 功 <u>出 版</u> 了 ！</font>";
$ec_iPortfolio['iPortfolio_published'][0] = "<font color=green>你 的 iPortfolio 已 成 功 <u>收 起</u> 了 ！</font>";
$ec_iPortfolio['italic'] = "斜体";
$ec_iPortfolio['last_update'] = "更新日期";
$ec_iPortfolio['last_update_time'] = "最后更新时间";
$ec_iPortfolio['learning_portfolio_content'] = "学习档案内容管理";
$ec_iPortfolio['level_review'] = "级别内互评";
$ec_iPortfolio['list'] = "清单";
$ec_iPortfolio['list_award'] = "校内颁发的主要奖项及成就";
$ec_iPortfolio['lowest_score'] = "最低分";
$ec_iPortfolio['main_guardian'] = "主监护人";
$ec_iPortfolio['mandatory_field_description']= "附有<span class='tabletextrequire'>*</span>的项目必须填写。";
$ec_iPortfolio['mark_performance'] = "校内表现";
$ec_iPortfolio['master_score_list'] = "班别科目表现总览";
$ec_iPortfolio['mean'] = "平均分";
$ec_iPortfolio['merit'] = "奖惩纪录";
$ec_iPortfolio['merit_detail'] = "奖惩纪录";
$ec_iPortfolio['mtype'] = "种类";
$ec_iPortfolio['new_comments'] = " 个新评语/意见";
$ec_iPortfolio['new_portfolios'] = "新增";
$ec_iPortfolio['no'] = "否";
$ec_iPortfolio['notes_guide'] = "指引";
$ec_iPortfolio['notes_method'] = "输入方式";
$ec_iPortfolio['notes_method_eClass'] = "eClass 汇入";
$ec_iPortfolio['notes_method_form'] = "表格式";
$ec_iPortfolio['notes_method_html'] = "网页式";
$ec_iPortfolio['notes_method_weblog'] = "网上日志";
$ec_iPortfolio['notes_relocate_DL'] = "移出一层至&quot;XXX&quot;之后";
$ec_iPortfolio['notes_relocate_Down'] = "下移至&quot;XXX&quot;之后";
$ec_iPortfolio['notes_relocate_DR'] = "移入一层至&quot;XXX&quot;之内";
$ec_iPortfolio['notes_relocate_UL'] = "移出一层至&quot;XXX&quot;之前";
$ec_iPortfolio['notes_relocate_Up'] = "上移至&quot;XXX&quot;之前";
$ec_iPortfolio['notes_relocate_UR'] = "移入一层至&quot;XXX&quot;之内";
$ec_iPortfolio['notes_template'] = "预设模板";
$ec_iPortfolio['no_student'] = "没有学生";
$ec_iPortfolio['number'] = "学号";
$ec_iPortfolio['number_using_template'] = "使用此范本的学习档案数目";
$ec_iPortfolio['ole'] = "其他学习经历";
$ec_iPortfolio['ole_report_stat'] = "其他学习经历报告";
$ec_iPortfolio['ole_role'] = "参与角色";
$ec_iPortfolio['optional'] = "(可选择填写)";
$ec_iPortfolio['overall'] = "总成绩";
$ec_iPortfolio['overall_comment'] = "总评语";
$ec_iPortfolio['overall_grade'] = "总等级";
$ec_iPortfolio['overall_perform_stat'] = "科目表现统计";
$ec_iPortfolio['overall_rank'] = "总级名次";
$ec_iPortfolio['overall_result'] = "总成绩";
$ec_iPortfolio['overall_score'] = "总平均分";
$ec_iPortfolio['overall_stand_score'] = "总标准分数";
$ec_iPortfolio['overall_summary'] = "总结";
$ec_iPortfolio['peer_review_setting'] = "同辈互评设定";
$ec_iPortfolio['peer_review_type'] = "同辈互评类型";
$ec_iPortfolio['performance'] = "表现";
$ec_iPortfolio['period'] = "时段";
$ec_iPortfolio['phone'] = "电话号码";
$ec_iPortfolio['photo_num_guide'] = "* 指已启动iPortfolio户口的学生。";
$ec_iPortfolio['photo_removed_msg'] = "相片已成功被删除";
$ec_iPortfolio['pic'] = "负责人";
$ec_iPortfolio['place_of_birth'] = "出生地点";
$ec_iPortfolio['portfolios'] = "学习档案内容管理";
$ec_iPortfolio['portfolios_mgt'] = "学习档案";
$ec_iPortfolio['portfolio_status'] = "共有 <!--TotalStudentNo--> 个学生，已启用 <!--PortfolioNo--> 个学习档案<!--GreenFrame-->";
$ec_iPortfolio['prepare_other_class_cd_burning'] = "预备其他班的 CD-ROM 烧录";
$ec_iPortfolio['programme'] = "活动名称";
$ec_iPortfolio['proj_ext_act_prog'] = "研习作品 / 活动项目";
$ec_iPortfolio['publish_date'] = "发布日期";
$ec_iPortfolio["rank"] = "级名次";
$ec_iPortfolio['reason'] = "原因";
$ec_iPortfolio['record'] = "纪录";
$ec_iPortfolio['red'] = "红";
$ec_iPortfolio['remark'] = "备注";
$ec_iPortfolio['remove_copied_files'] = "删除早前预备了的学习档案";
$ec_iPortfolio['report_title_class'] = "Class (班别)";
$ec_iPortfolio['report_title_classno'] = "Class No. (学号)";
$ec_iPortfolio['report_title_name'] = "Name (姓名)";
$ec_iPortfolio['report_title_regno'] = "Reg. No. (编号)";
$ec_iPortfolio['report_title_year'] = "Year (学年)";
$ec_iPortfolio['report_type'] = "报告类型";
$ec_iPortfolio['review_author'] = "发表：";
$ec_iPortfolio['review_type'] = "互评";
$ec_iPortfolio['role'] = "职位";
$ec_iPortfolio['role_participate'] = "角色";
$ec_iPortfolio['SAMS_CSV_file'] = "WEBSAMS CSV 档";
$ec_iPortfolio['SAMS_last_update'] = "最后更新日期";
$ec_iPortfolio['SAMS_subject'] = "科目";
$ec_iPortfolio['school_code'] = "学校编号";
$ec_iPortfolio['school_phone'] = "学校电话";
$ec_iPortfolio['school_remark'] = "学校备注";
$ec_iPortfolio['school_review'] = "全校互评";
$ec_iPortfolio['school_year'] = "学年";
$ec_iPortfolio['school_yr'] = "全部年份";
$ec_iPortfolio["score"] = "分数";
$ec_iPortfolio['select_group'] = "已选择了以下小组";
$ec_iPortfolio['self_account'] = "个人自述";
$ec_iPortfolio['semester'] = "学期";
$ec_iPortfolio['service_name'] = "服务名称";
$ec_iPortfolio['show_by'] = "显示";
$ec_iPortfolio['size_unit'] = '(MB)';
$ec_iPortfolio['slp_config'] = "学生学习概览设定";
$ec_iPortfolio['standard_deviation'] = "标准差";
$ec_iPortfolio["stand_score"] = "标准分数";
$ec_iPortfolio['statistic'] = "个人时数统计";
$ec_iPortfolio['student_account'] = "学生户口";
$ec_iPortfolio['student_learning_profile'] = "学生学习概览";
$ec_iPortfolio['student_particulars'] = "学生资料";
$ec_iPortfolio['student_perform_stat'] = "个人表现统计";
$ec_iPortfolio['student_photo'] = "学生相片";
$ec_iPortfolio['student_photo_no'] = "没有相片";
$ec_iPortfolio['student_report_print'] = "学生报告列印";
$ec_iPortfolio['student_self_account'] = "学生自述";
$ec_iPortfolio['student_self_account_desc'] = "学生可于此栏分享其高中学习生活及个人发展之得着或提供额外资料，好让其他人仕(例如各大专院校及未来雇主等) 作参考之用。为提供
更全面的内容，学生可考虑提及于高中前所获得的重要成就。";
$ec_iPortfolio['stype'] = "类别";
$ec_iPortfolio['subject_chart'] = "学科统计图表";
$ec_iPortfolio['success_uploaded_photo_count'] = "成功上载的相片数目";
$ec_iPortfolio['suspend_result_inactive'] = "此户口尚未启用";
$ec_iPortfolio['suspend_result_suspended'] = "此户口在之前已被停用";
$ec_iPortfolio['suspend_total_fail'] = "失败暂停户口的学生数目";
$ec_iPortfolio['suspend_total_success'] = "成功暂停户口的学生数目";
$ec_iPortfolio['teacher_comment'] = "老师评语";
$ec_iPortfolio['templates'] = "模板管理";
$ec_iPortfolio['templates_mgt'] = "范本";
$ec_iPortfolio['template_title'] = "模板名称";
$ec_iPortfolio['term'] = "学期";
$ec_iPortfolio['thumbnail'] = "缩图";
$ec_iPortfolio['title'] = "项目";
$ec_iPortfolio['title_academic_report'] = "学业报告";
$ec_iPortfolio['title_activity'] = "活动纪录";
$ec_iPortfolio['title_attendance'] = "考勤纪录";
$ec_iPortfolio['title_award'] = "得奖纪录";
$ec_iPortfolio['title_award_punishment'] = "Awards & Punishment 奖惩资料";
$ec_iPortfolio['title_eca'] = "Extra-Curricular Activities 课外活动";
$ec_iPortfolio['title_form_teacher_chi'] = "班主任";
$ec_iPortfolio['title_form_teacher_eng'] = "FORM TEACHER";
$ec_iPortfolio['title_guardian_chi'] = "家长/监护人";
$ec_iPortfolio['title_guardian_eng'] = "PARENT/GUARDIAN";
$ec_iPortfolio['title_ole'] = "Other Learning Experiences 其他学习经历";
$ec_iPortfolio['title_principal_chi'] = "校长";
$ec_iPortfolio['title_principal_eng'] = "PRINCIPAL";
$ec_iPortfolio['title_merit'] = "奖惩纪录";
$ec_iPortfolio['title_service'] = "Services in School 校内服务";
$ec_iPortfolio['title_teacher_comments'] = "老师评语";
$ec_iPortfolio['total'] = "总数";
$ec_iPortfolio['total_absent_count'] = "总缺席次数";
$ec_iPortfolio['total_earlyleave_count'] = "总早退次数";
$ec_iPortfolio['total_hours'] = "总时数";
$ec_iPortfolio['total_late_count'] = "总迟到次数";
$ec_iPortfolio['total_record'] = "共有 <!--NoRecord--> 项记录";
$ec_iPortfolio['total_records'] = "记录数目";
$ec_iPortfolio['transcript_config'] = "学业报告设定";
$ec_iPortfolio['type'] = "建立";
$ec_iPortfolio['type_category'] = "文件夹";
$ec_iPortfolio['type_document'] = "文件";
$ec_iPortfolio['underline'] = "加底线";
$ec_iPortfolio['unknown_photo'] = "不明相片";
$ec_iPortfolio['update_msg'] = "已更新纪录";
$ec_iPortfolio['upload_cert'] = "奖项/证明";
$ec_iPortfolio['upload_photo'] = "上载相片";
$ec_iPortfolio['view_analysis'] = "检视";
$ec_iPortfolio['view_comment'] = "检视评语/意见";
$ec_iPortfolio['view_my_record'] = "检视我的纪录";
$ec_iPortfolio['view_stat'] = "检视统计";
$ec_iPortfolio['web_view'] = "学习档案";
$ec_iPortfolio['whole_school'] = "全级";
$ec_iPortfolio['whole_year'] = "全年";
$ec_iPortfolio['within_word_limit'] = "(以 <!--EngLimit--> 字内之英文 或 <!--ChiLimit--> 字内之中文撰写本部分)";
$ec_iPortfolio['with_comment'] = "包括留言";
$ec_iPortfolio['year'] = "年度";
$ec_iPortfolio['year_period'] = "时期";
$ec_iPortfolio['yes'] = "是";
$ec_iPortfolio['your_comment'] = "你的意见";

$ec_iPortfolio_guide['group_growth'] = "如没有选择小组，则会发放予全部学生。";
$ec_iPortfolio_guide['manage_student_portfolio_msg'] = "你将会以学生身份管理学生的学习档案！";

$ec_iPortfolio_Report['form'] = "年级";
$ec_iPortfolio_Report['subject'] = "科目";
$ec_iPortfolio_Report['term'] = "学期";
$ec_iPortfolio_Report['year'] = "年度";

$ec_guide['fail_reason'] = "失败的原因";
$ec_guide['import_back'] = "再汇入";
$ec_guide['import_error_activated_regno'] = "此学生户口已启用，不能更改 WebSAMS RegNo";
$ec_guide['import_error_detail'] = "档案资料";
$ec_guide['import_error_duplicate_classnum'] = "此学生的班别及学号出现重复";
$ec_guide['import_error_duplicate_regno'] = "WebSAMS RegNo 已存在";
$ec_guide['import_error_incorrect_regno'] = "汇入的RegNo 编号应加上#";
$ec_guide['import_error_incorrect_regno_2'] = "汇入的RegNo 编号应为数字";
$ec_guide['import_error_no_user'] = "没有此用户";
$ec_guide['import_error_reason'] = "错误原因";
$ec_guide['import_error_row'] = "行数";
$ec_guide['import_error_unknown'] = "不明";
$ec_guide['import_error_wrong_format'] = "错误的汇入格式";
$ec_guide['import_update_no'] = "成功更新资料项数";
$ec_guide['learning_portfolio_time_start'] = "如果设定了开始时间，这个学习档案将会于开始时间后出现给学生使用。";
$ec_guide['learning_portfolio_time_end'] = "如果设定了终结时间，学生将不能于终结时间后更改这个学习档案的内容。";

$ec_student_word['name_chinese'] = "中文姓名";
$ec_student_word['name_english'] = "英文姓名";
$ec_student_word['registration_no'] = "学生编号";

# Warning message
$ec_iPortfolio['suspend_account_confirm'] = "你是否确定要暂停所选用户？";
$ec_iPortfolio['suspend_template_confirm'] = "你是否确定要停用所选的模板？";
$ec_iPortfolio['active_template_confirm'] = "你是否确定要恢复所选的模板？";
$ec_iPortfolio['delete_attachment_confirm'] = "你是否确定要删除所选的附件？";
$ec_iPortfolio['delete_template_confirm'] = "你是否确定要删除所选的模板？";
$ec_iPortfolio['photo_remove_confirm'] = "你是否确定要删除已上载的相片？";
$ec_iPortfolio['student_photo_upload_remind'] = "上载前请先以有大小写之分的学生编号命名相片并压缩相片至'.ZIP'档案格式！<br />相片只能以'.JPG'的格式上载，相片大小也规定为100 X 130 像素 (阔 x 高)。";
$ec_iPortfolio['student_photo_upload_warning'] = "相片必须要以'.ZIP'档案格式上载。";
$ec_iPortfolio['used_template_cant_delete'] = "*注意: 使用中的范本不能被删除";
$ec_warning['comment_content'] = "请先填上你的意见。";
$ec_warning['comment_submit_confirm'] = "你是否确定要提交意见？";
$ec_warning['growth_title'] = "请输入计划名称。";
$ec_warning['notes_title'] = "请输入题目。";
$ec_warning['please_select_class'] = "请选择班别！";
$ec_warning['please_select_subject'] = "请选择科目！";
$ec_warning['please_enter_pos_integer'] = "请输入正数数字";
$ec_warning['start_end_year'] = "终结年份不能早于开始年份。";
$ec_warning['start_end_hours'] = "最多时数不能少于最少时数。";
$ec_warning['start_end_scheme_start_time'] = "开始时间不能早于学习档案开始时间。";
$ec_warning['start_end_scheme_end_time'] = "终结时间不能迟于学习档案终结时间。";

# for semester sort name
$ec_iPortfolio['semester_name_array_1'] = array(1, '一', '上', 'first', '1st Semester');
$ec_iPortfolio['semester_name_array_2'] = array(2, '二', '中', 'second', '2nd Semester');
$ec_iPortfolio['semester_name_array_3'] = array(3, '三', '下', 'third', '3rd Semester');

//$ec_iPortfolio['cutomized_report_remark_chi'] = "有关报告内之详情，可参阅学生综合表现纪录小册子";
//$ec_iPortfolio['cutomized_report_remark_eng'] = "For details, please refer to the handbook of Student Portfolio";

#for Kei Wai Report
$ec_iPortfolio['keiwai_part_A'] = "【甲部】";
$ec_iPortfolio['keiwai_part_B'] = "【乙部】";
$ec_iPortfolio['keiwai_part_C'] = "【丙部】";
$ec_iPortfolio['keiwai_part_D'] = "【丁部】";
$ec_iPortfolio['keiwai_part_E'] = "【戊部】";
$ec_iPortfolio['keiwai_part_F'] = "【己部】";
$ec_iPortfolio['keiwai_part_G'] = "【庚部】";
$ec_iPortfolio['keiwai_personal_info'] = "个人资料";
$ec_iPortfolio['keiwai_education'] = "教育程度";
$ec_iPortfolio['keiwai_academic_results'] = "学业成绩";
$ec_iPortfolio['keiwai_professional_profile'] = "考获资格";
$ec_iPortfolio['keiwai_extra_data_manage'] = "基慧补充资料管理";
$ec_iPortfolio['keiwai_report'] ="学生个人成就纪录表";
$ec_iPortfolio['keiwai_report_2'] ="学生个人成就证明书";
$ec_iPortfolio['keiwai_HKID'] = "香港身份证号码";
$ec_iPortfolio['keiwai_religion'] = "宗教信仰";
$ec_iPortfolio['pob'] = "出生地点";
$ec_iPortfolio['nationality'] = "国籍";
$ec_iPortfolio['telno'] = "联络电话";
$ec_iPortfolio['keiwai_subject_area'] = "项目名称";
$ec_iPortfolio['keiwai_organization_a'] = "颁发机构";
$ec_iPortfolio['keiwai_organization_b'] = "举办机构";
$ec_iPortfolio['keiwai_organization_c'] = "服务机构";
$ec_iPortfolio['service'] = "服务纪录";
$ec_iPortfolio['female'] = "女";
$ec_iPortfolio['male'] = "男";
$ec_iPortfolio['institute'] = "学校名称";
$ec_iPortfolio['doc'] = "修读年级";
$ec_iPortfolio['keiwai_period'] = "年份";
$ec_iPortfolio['keiwai_classlevel'] = "年级";
$ec_iPortfolio['keiwai_position_c'] = "全班名次";
$ec_iPortfolio['keiwai_totalnumber_c'] = "全班人数";
$ec_iPortfolio['keiwai_position_l'] = "全级名次";
$ec_iPortfolio['keiwai_totalnumber_l'] = "全级人数";
$ec_iPortfolio['keiwai_certificate'] = "考获证书";
$ec_iPortfolio['keiwai_qualification'] = "项目成绩";
$ec_iPortfolio['keiwai_organization'] = "签发机构";

$assignments_alert_msg6 = "HTML 预览";
$assignments_alert_msg9 = "日期不正确";
$assignments_alert_msg13 = " 档案储存容量必须是正数";
$button_activate_iPortfolio = "启用";
$button_apply = "应用";
$button_new_template = "新增模板";
$button_edit_template = "编辑模板";
$button_organize = "整理";
$button_private = "保密";
$button_public = "公开";
$button_publish_iPortfolio = "发布";
$button_save_and_publish_iPortfolio = "储存及发布";
$con_msg_add = "已增加纪录";
$con_msg_del = "已删除纪录";
$con_msg_save = "已储存纪录";
$con_msg_update = "已更新纪录";
$EndTime = "终结时间";
$i_general_others = "其他";
$i_general_settings = "设定";
$i_list_all = "全部";
$i_status_shared = "已分享";
$i_status_usable = "可使用";
$list_display = "每页显示";
$list_item_no = "项";
$msg_check_at_least_one = "请最少选择一个选项";
$msg_check_at_least_one_item = "请最少选择一个项目";
$msg_check_at_most_one = "请选择其中一项";
$MyInfo = "我的个人资料";
$namelist_class_number = "学号";
$notes_after = "之后";
$notes_position = "位置";
$notes_title = "题目";
$notes_under = "之下";
$no_record_msg = "暂时仍未有任何纪录。";
$profiles_to = "至";
$profile_dob = "出生日期";
$profile_gender = "性别";
$profile_modified = "最近修改日期";
$profile_nationality = "国籍";
$profile_pob = "出生地点";
$qbank_status = "状况";
$qbank_public = "公开";
$quizbank_desc = "简介";
$range_all = "全部";
$StartTime = "开始时间";
$usertype_s = "学生";
$valueTitle = "分数";

$ec_iPortfolio['not_display'] = "不显示";
$ec_iPortfolio['not_display_student_photo'] = "学生相片";
$ec_iPortfolio['no_student_details'] = "学生详细资料";
$ec_iPortfolio['no_school_name'] = "校名于页首";
$ec_iPortfolio['other_learning_record'] = "其他学习纪录";
$ec_iPortfolio['show_component_sub'] = "显示科目分卷";
$ec_iPortfolio['preview'] = "预览";
$ec_words['school_badge'] = "校徽";
$ec_words['school_address'] = "学校地址";
$ec_iPortfolio['transcript_description'] = "学生成绩报告单简介";
$ec_iPortfolio['transcript_CSV_file'] = "CSV 补充资料";
$ec_iPortfolio['reading_record'] = "阅读纪录";
$ec_iPortfolio['ole_record'] = "其他学习经历纪录";
$ec_iPortfolio['file'] = "档案";
$import_csv['download'] = "下载 CSV 档案范本";
$ec_iPortfolio['customized_file_upload_remind'] = "注意：上载之档案必须以年份命名。(例：2006.csv)";

$w_alert['start_end_time2'] = "终结日期不能早于开始日期。";
$wording['contents_notice1'] = "<font color=#AAAAAA>灰色</font>代表保密。";


	$ec_iPortfolio['eClass_update_assessment'] = "从 eClass 更新学业资料";
	$ec_iPortfolio['eClass_update'] = "从 eClass 更新";
	$ec_iPortfolio['eClass_update_merit'] = "从 eClass 更新奖惩纪录";
	$ec_iPortfolio['eClass_update_activity'] = "从 eClass 更新活动纪录";
	$ec_iPortfolio['eClass_update_attendance'] = "从 eClass 更新考勤纪录";
	$ec_iPortfolio['eClass_update_award'] = "从 eClass 更新得奖纪录";
	$ec_iPortfolio['eClass_update_comment'] = "从 eClass 更新老师评语";
	$ec_iPortfolio['update_more_assessment'] = "再更新学业纪录";
	$ec_iPortfolio['update_more_award'] = "再更新得奖纪录";
	$ec_iPortfolio['update_more_activity'] = "再更新活动纪录";
	$ec_iPortfolio['update_more_attendance'] = "再更新考勤纪录";
	$ec_iPortfolio['update_more_merit'] = "再更新奖惩纪录";
	$ec_iPortfolio['update_more_comment'] = "再更新老师评语";
	$ec_iPortfolio['last_update'] = "更新日期";
	$ec_iPortfolio['SAMS_last_update'] = "最后更新日期";
	$ec_iPortfolio['activation_no_quota'] = "很抱歉，没有足够的户口额！";
	$ec_iPortfolio['SAMS_code'] = "代码";
	$ec_iPortfolio['SAMS_cmp_code'] = "分卷代码";
	$ec_iPortfolio['SAMS_cmp_code'] = "分卷代码";
	$ec_iPortfolio['SAMS_description'] = "说明 (英/中)";
	$ec_iPortfolio['SAMS_abbr_name'] = "名称缩写 (英/中)";
	$ec_iPortfolio['SAMS_short_name'] = "简称 (英/中)";
	$ec_iPortfolio['SAMS_display_order'] = "显示次序";
	$ec_iPortfolio['SAMS_subject'] = "科目";
	$ec_iPortfolio['SAMS_short_name_duplicate'] = "简称不可重复";
	$ec_iPortfolio['WebSAMSRegNo'] = "WebSAMS RegNo";

	$ec_iPortfolio['record_submitted'] = "已申报奖项";
	$ec_iPortfolio['record_approved'] = "已批准奖项";
	$ec_iPortfolio['house'] = "社级";
	$ec_iPortfolio['pending'] = "审批中";
	$ec_iPortfolio['approved'] = "已批准";
	$ec_iPortfolio['rejected'] = "已拒绝";
	$ec_iPortfolio['approve'] = "批准";
	$ec_iPortfolio['reject'] = "拒绝";
	$ec_iPortfolio['status'] = "状态";
	$ec_iPortfolio['source'] = "来源";
	$ec_iPortfolio['student_apply'] = "学生申报";
	$ec_iPortfolio['teacher_submit_record'] = "老师提交纪录";
	$ec_iPortfolio['school_record'] = "学校纪录";
	$ec_iPortfolio['process_date'] = "审批日期";
	$ec_iPortfolio['submit_external_award_record'] = "提交校外得奖纪录";
	$ec_iPortfolio['total'] = "总数";
	$ec_iPortfolio['grade'] = "等级";
	$ec_iPortfolio['score'] = "分数";
	$ec_iPortfolio['stand_score'] = "标准分数";
	$ec_iPortfolio['rank'] = "级名次";
	$ec_iPortfolio['conduct_grade'] = "操行";
	$ec_iPortfolio['comment_chi'] = "中文评语";
	$ec_iPortfolio['comment_eng'] = "英文评语";
	$ec_iPortfolio['detail'] = "详细资料";

	$ec_iPortfolio['all_status'] = "全部状况";
	$ec_iPortfolio['all_category'] = "全部类别";
	$ec_iPortfolio['title'] = "名称";
	$ec_iPortfolio['category'] = "类别";
	$ec_iPortfolio['category_code'] = "类别代号";
	$ec_iPortfolio['achievement'] = "奖项 / 证书文凭 / 成就";
	$ec_iPortfolio['organization'] = "合办机构";
	$ec_iPortfolio['details'] = "详情";
	$ec_iPortfolio['approved_by'] = "审批老师";
	$ec_iPortfolio['attachment'] = "附件 / 证明";
	$ec_iPortfolio['attachment_only'] = "附件";
	$ec_iPortfolio['startdate'] = "开始日期";
	$ec_iPortfolio['enddate']= "结束日期";
	$ec_iPortfolio['or'] = "或";
	$ec_iPortfolio['add_enddate'] = "加入结束日期";
	$ec_iPortfolio['unit_hour'] = "小时";
	$ec_iPortfolio['school_remarks'] = "学校备注";

	$ec_warning['date'] = "请输入日期。";
	$w_alert['Invalid_Date'] = "日子错误";
	$w_alert['Invalid_Time'] = "时间错误";
	$ec_warning['title'] = "请输入名称。";
	$ec_iPortfolio['upload_error'] = "请输入正确的档案路径";


	$ec_iPortfolio_ele_array = array("[ID]"=>"智育发展",
									"[MCE]"=>"德育及公民教育",
									"[CS]"=>"社会服务",
									"[PD]"=>"体育发展",
									"[AD]"=>"艺术发展",
									"[CE]"=>"与工作有关经验",
									"[OTHERS]"=>"其他");
	$ec_iPortfolio['type_competition'] = "比赛";
	$ec_iPortfolio['type_activity'] = "活动";
	$ec_iPortfolio['type_course'] = "课程";
	$ec_iPortfolio['type_service'] = "服务";
	$ec_iPortfolio['type_award'] = "奖项";
	$ec_iPortfolio['type_others'] = "其他";
	$ec_iPortfolio_category_array = array("1"=>$ec_iPortfolio['type_competition'],
										"2"=>$ec_iPortfolio['type_activity'],
										"3"=>$ec_iPortfolio['type_course'],
										"4"=>$ec_iPortfolio['type_service'],
										"5"=>$ec_iPortfolio['type_award'],
										"6"=>$ec_iPortfolio['type_others']);
	$ec_iPortfolio['waiting_approval']= "等待批核";
	$ec_iPortfolio['program']= "活动";
	$ec_iPortfolio['program_list']= "活动一览";
	$ec_warning['olr_from_teacher'] = "此乃老师呈交之纪录，无需审批！";
	$ec_iPortfolio['student_list']= "学生名单";
	$ec_iPortfolio['assign_more_student']= "加入更多学生";
	$ec_iPortfolio['no_record_found'] = "此档案不存在";
	$ec_iPortfolio['no_privilege_to_read_file'] = "你没有权限观看此档案";
	$ec_iPortfolio['new_ole_activity'] = "新增其他学习纪录活动";
	$ec_iPortfolio['assign_student_now'] = "加入学生";
	$ec_iPortfolio['comment'] = "评语";
	$ec_iPortfolio['no_of_records'] = "纪录数目";


$ip_lang['class_history_management'] = "班级纪录管理";

$iDiscipline['Conduct_Category'] = "行为类别";
$iDiscipline['Conduct_Item'] = "行为项目";
//$iDiscipline['Good_Conduct_Import_FileDescription'] = "$i_Discipline_System_Discipline_Case_Record_Case_Semester, $i_UserClassName, $i_ClassNumber, $i_Discipline_System_Discipline_Category , ".$iDiscipline['Conduct_Category'].", ".$iDiscipline['Conduct_Item'].", $i_Discipline_System_Discipline_Case_Record_PIC, ".$iDiscipline['RecordDate'].",$i_UserRemark";
$iDiscipline['Good_Conduct_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Record Date</td><td>Semester</td><td>Item Code</td><td>PIC</td><td>Remark</tr>
<tr><td>1A</td><td>11</td><td>2009-03-20</td><td>Fall Semester</td><td>Item01</td><td>Teacher01</td><td>Remark</td></tr></table>";
//$iDiscipline['Conduct_Mark_Import_FileDescription'] = "$i_general_Year, $i_SettingsSemester, $i_UserClassName, $i_ClassNumber, $i_Discipline_System_Discipline_Case_Record_PIC, $i_Discipline_System_Discipline_Conduct_Mark_Adjustment,$i_Attendance_Reason";
$iDiscipline['Conduct_Mark_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Year</td><td>Semester</td><td>Adjustment</td><td>PIC</td><td>Reason</td></tr>
<tr><td>1A</td><td>12</td><td>2008-2009</td><td>Fall semester</td><td>20</td><td>Teacher01</td><td>Helping Teacher</td></tr>
</table>";
//$iDiscipline['Detention_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_Attendance_Reason, $i_UserRemark";
$iDiscipline['Detention_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Reason</td><td>Remark</td></tr>
<tr><td>1A</td><td>12</td><td>Late for school</td><td>Remark</td></tr>
</table>";
$iDiscipline['Good_Conduct_Missing_Category'] = "没有Category";
$iDiscipline['Good_Conduct_Wrong_Category'] = "Wrong Category";
$iDiscipline['Good_Conduct_Missing_Conduct_Category'] = "没有行为类别";
$iDiscipline['Good_Conduct_Missing_Conduct_Item'] = "没有行为项目";
$iDiscipline['Good_Conduct_Missing_Conduct_Score'] = "没有操分";
$iDiscipline['Good_Conduct_No_Conduct_Item'] = "没有此行为项目";
$iDiscipline['Good_Conduct_No_Conduct_Category'] = "没有此行为类别";
$iDiscipline['Good_Conduct_No_Staff'] = "没有此职员";
$iDiscipline['Good_Conduct_No_Student'] = "没有此学生";
$iDiscipline['Award_Punishment_Wrong_Merit_Type'] = "没有此优点类型";
$iDiscipline['Award_Punishment_ConductScore_Numeric'] = "操分必须为数字";
$iDiscipline['Award_Punishment_No_Subject'] = "没有此科目";
$iDiscipline['Award_Punishment_No_Merit_Num'] = "没有奖惩数目";
$iDiscipline['Award_Punishment_Missing_Merit_Num'] = "没有优点类型";
$iDiscipline['Award_Punishment_Merit_Numeric'] = "奖惩必须为数字";
$iDiscipline['Award_Punishment_Merit_Negative'] = "奖惩必须为整数";
$iDiscipline['Award_Punishment_Wrong_MeritType'] = "没有此类别";
$iDiscipline['Award_Punishment_No_ItemCode'] = "没有此项目编号";
$iDiscipline['Award_Punishment_Missin_ItemCode'] = "没有项目编号";
$iDiscipline['Award_Punishment_Missing_Year'] = "没有年份";
$iDiscipline['Award_Punishment_No_Year'] = "没有此年份";
$iDiscipline['Award_Punishment_Missing_Semester'] = "没有学期";
$iDiscipline['Award_Punishment_RecordDate_In_Future'] = "纪录日期必须是今天或以后";
$iDiscipline['Award_Punishment_RecordDate_Error'] = "纪录日期必须是日期";
$iDiscipline['Award_Punishment_Missing_RecordDate'] = "没有纪录日期";
$iDiscipline['Good_Conduct_Cat_Item_Mismatch'] = "行为类别及行为项目不符";
$iDiscipline['Good_Conduct_No_Category'] = "没有此类别";
$iDiscipline['Good_Conduct_No_Period_Setting'] = "未设定累计时段";
$iDiscipline['Good_Conduct_Category_Mismatch'] = "行为类别及行为项目并不符类别";
$iDiscipline['Detention_Missing_Reason'] = "没有留堂原因";
$iDiscipline['Assign_Duty_Teacher'] = "请最少选择一名负责人。";

$iDiscipline['Form_Templates'] = "模板";
$iDiscipline['Form_answersheet_option'] = '选项数目';
$iDiscipline['Form_answersheet_header'] = '题目 / 标题';
$iDiscipline['Form_answersheet_content'] = '内容';
$iDiscipline['Form_answersheet_type_selection'] = "-- 选择格式 --";
$iDiscipline['Form_answersheet_selection'] = "-- 选择一数字 --";
$i_Discipline_System_Discipline_Conduct_Mark_No_Ratio_Setting = '此年度没有此重设定';
//$iDiscipline['Case_Import_FileDescription'] = "$i_Discipline_System_Discipline_Case_Record_Case_Number,$i_Discipline_System_Discipline_Case_Record_Case_Title,$i_Discipline_System_Discipline_Case_Record_Case_Event_Date,$i_Discipline_System_Discipline_Case_Record_Case_Semester,$i_Discipline_System_Discipline_Case_Record_Case_Location,$i_Discipline_System_Discipline_Case_Record_Case_PIC";
$iDiscipline['Example'] = "例子";
$iDiscipline['Case_Import_FileDescription'] = "例子:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Case Number</td><td>Case Name</td><td>Event Date</td><td>Semester</td><td>Location</td><td>PIC</td><td>Remark</td></tr>
<tr><td>001</td><td>Case 1</td><td>2009-03-20</td><td>Fall Semester</td><td>Classroom</td><td>Teacher01</td><td>Remark</td></tr>
</table>";
$iDiscipline['Case_Missing_CaseNumber'] = "没有个案编号";
$iDiscipline['Case_Missing_CaseName'] = "没有个案名称";
$iDiscipline['Case_Missing_Category'] = "没有类别";
$iDiscipline['Case_Missing_EventDate'] = "没有发生日期";
$iDiscipline['Case_Missing_Location'] = "没有地点";
$iDiscipline['Case_Missing_PIC'] = "没有负责人";
//$iDiscipline['Case_Student_Import_FileDescription'] = "$i_UserClassName, $i_ClassNumber, $i_general_record_date,$i_SettingsSemester,$i_Discipline_System_ItemCode,$i_Discipline_System_ConductScore,$i_Discipline_System_Discipline_Case_Record_PIC, ".$eDiscipline['Subject'].",$i_Discipline_System_Add_Merit,$i_ResourceCategory,".$iDiscipline['MeritType'].",$i_UserRemark";
/*$iDiscipline['Case_Student_Import_FileDescription'] = "Example:<br/>
<table width=\"100%\" border=\"1\">
<tr><td>Class Name</td><td>Class Number</td><td>Record Date</td><td>Semester</td><td>Item Code</td><td>Conduct Score</td><td>PIC</td><td>Subject</td><td>Add Merit Record</td><td>Category</td><td>Merit Type</td><td>Remark</td></tr>
<tr><td>1A</td><td>12</td><td>2009-03-20</td><td>Fall Semester</td><td>LATE002F</td><td>3</td><td>Teacher01</td><td>English</td><td>1</td><td>Punishment</td><td>-2</td><td>Remark</td></tr>
</table>";*/
$iDiscipline['Case_Student_Import_No_Semester'] = "没有此学期";
$iDiscipline['Case_Student_Import_Conduct_Score_OutRange'] = "操行分超出了范围";
$iDiscipline['Case_Student_Import_Quantity_OutRange'] = "数量超出了范围";
$iDiscipline['Case_Student_Import_Conduct_Score_Negative'] = "操行分应少于0";
$iDiscipline['Case_Student_Import_Conduct_Score_Positive'] = "操行分应大于0";
$eDiscipline['Detention_Arrangement'] = "安排";
$eDiscipline['Session_Management'] = "时段管理";
$eDiscipline['Detention_Session_Default_String'] =  "$i_Discipline_Date | $i_Discipline_Time | $i_Discipline_Location | $i_Discipline_Vacancy | $i_Discipline_PIC";
$iDiscipline['Category_Item_Code_Warning'] = "项目编号已存在或曾被使用/删除";
$iDiscipline['Reord_Import_Successfully'] = "纪录已成功汇入";
$iDiscipline['Invalid_Date_Format'] = "日期格式错误";
$iDiscipline['Import_Instruct_Main'] = "要进行汇入，请：";
//$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_1'] = "Press ";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_2'] = "按此";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_a_3'] = "下载CSV资料档模板。";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_b_1'] = "根据以下范例，于CSV资料档模板中输入纪录。";
$iDiscipline['Import_Instruct_Note'] = "注:";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_1'] = "你需于CSV资料档中以项目编号形式，指定每个纪录所属之良好及违规行为项目。你可以";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2'] = "按此";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_2_1'] = "检视良好及违规行为项目名称及 对应之项目编号。";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1'] = "请使用YYYY-MM-DD格式，于\"Record Date\"栏中输入纪录日期。";
$iDiscipline['Good_Conduct_and_Misconduct_Import_Instruct_c_1'] = "储存CSV资料档。输入CSV资料档的路径，或按\"浏览\"，从你的电脑中选取资料档，然后按<strong>继续</strong>。";

$iDiscipline['Award_Punishment_Import_Instruct_Note_1_1'] = "你需于CSV资料档中以项目编号形式，指定每个纪录所属之奖惩项目。你可以";
$iDiscipline['Award_Punishment_Import_Instruct_Note_1_2'] = "按此";
$iDiscipline['Award_Punishment_Import_Instruct_Note_1_2_1'] = "检视奖惩项目名称及 对应之项目编号。";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_1'] = "<b>只适用于从1.0版升级之用户：</b>你需于CSV资料档中以科目编号形式，指定每个纪录所属之科目。你可以";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_2'] = "按此";
$iDiscipline['Award_Punishment_Import_Instruct_Note_2_3'] = "检视科目名称及对应之科目编号。如不适用，此栏可留空。 ";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_1'] = "请参阅以下代号，于\"Type\"栏及\"Quantity\"栏填写纪录的奖惩内容。";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_2'] = "检视代号";
$iDiscipline['Award_Punishment_Import_Instruct_Note_3_3'] = "如记三个缺点，请于\"Quantity\"栏及\"Type\"栏分别输入\"{代 表缺点的代号}\"及\"3\"。 ";
$iDiscipline['Award_Punishment_Import_Instruct_Note_4_1'] = "请使用YYYY-MM-DD格式，于\"Record Date\"栏中输入纪录日期。";
$iDiscipline['Award_Punishment_Import_Instruct_c_1'] = "储存CSV资料档。输入CSV资料档的路径，或按\"浏览\"，从你的电脑中选取资料档，然后按<strong>继续</strong>。";

$iDiscipline['Case_Record_Import_Instruct_Note_1_1'] = "请使用YYYY-MM-DD格式，于\"Record Date\"栏中输入纪录日期。";
$iDiscipline['Case_Record_Import_Instruct_c_1'] = "储存CSV资料档。输入CSV资料档的路径，或按\"浏览\"，从你的电脑中选取资料档，然后按<strong>继续</strong>。";
$iDiscipline['Import_Source_File'] = "CSV资料档";
$iDiscipline['Import_Source_File_Note'] = "你可于\"资料档案\"栏输入CSV资料档的路径，或按\"浏览\"，从你的电脑中选取资料档。";

###############################################################################
# scrabble fun corner
###############################################################################
$word_sfc['title'] = "拼字乐";
$word_sfc['description'] = "「Scrabble拼字乐」让你透过不同的有趣小游戏，轻轻松松地掌握到Scrabble拼字游戏的规则和技巧。";

# 20090403 yatwoon (semester related)
$semester_change_warning = "警告：切勿于学年结束前修改\"学期更新模式\"，否则将破坏系统中所储存纪录的完整性，引起资料损失或其他严重后果。";

# 20090627 yat woon - eRC reports
$i_alert_PleaseSelectClassForm = "请选择班别或级别";

###############################################################################

###############################################################################
# LSLP
###############################################################################
$i_LSLP['LSLP'] = "通识学习平台";
$i_LSLP['admin_user_setting'] = "设定管理用户";
$i_LSLP['user_license_setting'] = "许可证设定";
$i_LSLP['license_display_1'] = "你的通识学习平台许可证有";
$i_LSLP['license_display_2'] = " 个，尚有 ";
$i_LSLP['license_display_3'] = " 个可供使用。";
$i_LSLP['license_display_4'] = "你已使用的许可证为 ";
$i_LSLP['license_display_5'] = " 个。";
$i_LSLP['used_up_all_license_msg'] = "你已使用所有许可证, 你不可再新增任何教室.";
$i_LSLP['license_period_expired'] = "许可证已过期";
$i_LSLP['license_exceed_msg1'] = "你只有";
$i_LSLP['license_exceed_msg2'] = "个许可证可供使用。";
$i_LSLP['license_exceed_msg3'] = "请返回";
$i_LSLP['school_license'] = "学校许可证";
$i_LSLP['student_license'] = "学生许可证";
$i_LSLP['license_type'] = "许可证类型";
$i_LSLP['license_period'] = "许可证有效期";
###############################################################################

if (is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
  include_once("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}

# if setting of merit/demerit title are set
# load the setting
$merit_demerit_customize_file = "$intranet_root/file/merit.b5.customized.txt";
if(file_exists($merit_demerit_customize_file) && is_file($merit_demerit_customize_file) && filesize($merit_demerit_customize_file)!=0){
	if ($file_content_merit_demeirt_wordings=="")
		$file_content_merit_demeirt_wordings = get_file_content($merit_demerit_customize_file);
	$lines = explode("\n",$file_content_merit_demeirt_wordings);

	$i_Merit_Merit = $lines[0];
	$i_Merit_MinorCredit = $lines[1];
	$i_Merit_MajorCredit = $lines[2];
	$i_Merit_SuperCredit = $lines[3];
	$i_Merit_UltraCredit = $lines[4];

	$i_Merit_BlackMark = $lines[5];
	$i_Merit_MinorDemerit = $lines[6];
	$i_Merit_MajorDemerit = $lines[7];
	$i_Merit_SuperDemerit = $lines[8];
	$i_Merit_UltraDemerit = $lines[9];
}

$i_Merit_TypeArray = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_Warning,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);


?>