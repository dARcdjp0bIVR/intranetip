<?
# modifying by : 


// the schema update from 2010-03-xx to 2010-04-xx had all changed to 2010-04-29 

############################################################################################
############################################################################################
############################################################################################
############################################################################################
############################################################################################
######																	 ###################
######					******************* 							 ###################
######					***  IMPORTANT  ***								 ###################
######					******************* 							 ###################
######																	 ###################
###### For eClass DB update, please assign to array "$sql_eClass_update" ###################
######																	 ###################
######																	 ###################
######																	 ###################
###### For IP25 DB update, please assign to array "$sql_eClassIP_update" ###################
######																	 ###################
######																	 ###################
######																	 ###################
######																	 ###################
############################################################################################
############################################################################################
############################################################################################
############################################################################################
############################################################################################



############################################################################################
############################# IntranetIP 2.5 schema update #################################
############################################################################################

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Add Building Level",
	"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION_BUILDING  (
		BuildingID int(11) NOT NULL auto_increment,
		Code varchar(10),
		NameChi varchar(255),
		NameEng varchar(255),
		DisplayOrder int(11),
		RecordType int(11),
		RecordStatus int(11),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (BuildingID),
		INDEX BuildingID (BuildingID),
		INDEX InventoryBuildingNameChi (NameChi),
		INDEX InventoryBuildingNameEng (NameEng),
		UNIQUE ChineseName (NameChi),
		UNIQUE EnglishName (NameEng),
		UNIQUE BuildingIDWithCode (BuildingID, Code)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-05-12",
	"School Settings -> Location - Add building-floor mapping in the floor table",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL ADD BuildingID int(11), ADD INDEX InventoryBuildingID (BuildingID);"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Record Last Modified UserID for Building",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING Add LastModifiedBy int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Record Last Modified UserID for Floor",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL Add LastModifiedBy int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Record Last Modified UserID for Room",
	"ALTER TABLE INVENTORY_LOCATION Add LastModifiedBy int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Add Description Field to Room",
	"ALTER TABLE INVENTORY_LOCATION Add Description text default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Change Engine of Floor Table",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL ENGINE = InnoDB;"
);

$sql_eClassIP_update[] = array(
	"2009-05-14",
	"School Settings -> Location - Change Engine of Room Table",
	"ALTER TABLE INVENTORY_LOCATION ENGINE = InnoDB;"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create academic year table",
	"CREATE TABLE IF NOT EXISTS ACADEMIC_YEAR (
	  AcademicYearID int(8) NOT NULL auto_increment,
	  YearNameEN varchar(50) default NULL,
	  YearNameB5 varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (AcademicYearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create academic year term table",
	"CREATE TABLE IF NOT EXISTS ACADEMIC_YEAR_TERM (
	  YearTermID int(8) NOT NULL auto_increment,
	  AcademicYearID int(8) default NULL,
	  TermID int(8) default NULL,
	  YearTermNameEN varchar(255) default NULL,
	  YearTermNameB5 varchar(255) default NULL,
	  TermStart datetime default NULL,
	  TermEnd datetime default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (YearTermID),
	  UNIQUE KEY YearTerm (AcademicYearID,TermID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

/*$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Name of Class table",
	"CREATE TABLE CLASS (
	  ClassID int(8) NOT NULL auto_increment,
	  DepartmentID int(8) default NULL,
	  ClassCode varchar(50) default NULL,
	  ClassNameB5 varchar(255) default NULL,
	  ClassNameEN varchar(255) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  Sequence int(2) default NULL,
	  PRIMARY KEY  (ClassID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject table",
	"CREATE TABLE SUBJECT (
	  SubjectID int(8) NOT NULL auto_increment,
	  SubjectShortFormB5 varchar(100) NOT NULL,
	  SubjectShortFormEN varchar(100) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  SubjectDescB5 text,
	  SubjectDescEN text,
	  Sequence int(2) default NULL,
	  WEBSAMSCode varchar(10) NOT NULL,
	  SubjectAbbrEN varchar(100) NOT NULL,
	  SubjectAbbrB5 varchar(100) NOT NULL,
	  PRIMARY KEY  (SubjectID),
	  UNIQUE KEY WEBSAMSCode (WEBSAMSCode)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject component table",
	"CREATE TABLE SUBJECT_COMPONENT (
	  SubjectComponentID int(8) NOT NULL auto_increment,
	  SubjectComponentNameB5 varchar(255) default NULL,
	  SubjectComponentNameEN varchar(255) default NULL,
	  SubjectComponentDescB5 text,
	  SubjectComponentDescEN text,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  SubjectCode varchar(5) default NULL,
	  SubjectComponentCode varchar(5) default NULL,
	  PRIMARY KEY  (SubjectComponentID),
	  UNIQUE KEY SubjectID (SubjectComponentID),
	  UNIQUE KEY SubjectCode (SubjectCode,ComponentSubjectCode)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
*/

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject term table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM (
	  SubjectTermID int(8) NOT NULL auto_increment,
	  SubjectID int(8) default NULL,
	  YearTermID int(8) default NULL,
	  SubjectGroupID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectTermID),
	  UNIQUE KEY SubjectTerm (SubjectID,YearTermID,SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject term class table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS (
	  SubjectGroupID int(8) NOT NULL,
	  ClassCode varchar(50) default NULL,
	  ClassTitleEN varchar(255) default NULL,
	  ClassTitleB5 varchar(255) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject class teacher table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS_TEACHER (
	  SubjectClassTeacherID int(8) NOT NULL auto_increment,
	  SubjectGroupID int(8) NOT NULL,
	  UserID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (SubjectClassTeacherID),
	  UNIQUE KEY YearTerm (SubjectGroupID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject class student table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS_USER (
	  SubjectClassUserID int(8) NOT NULL auto_increment,
	  SubjectGroupID int(8) NOT NULL,
	  UserID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (SubjectClassUserID),
	  UNIQUE KEY YearTerm (SubjectGroupID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Subject class form relation table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS_YEAR_RELATION (
	  SubjectGroupID int(8) NOT NULL,
	  YearID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectGroupID,YearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Term table",
	"CREATE TABLE IF NOT EXISTS TERM (
	  TermID int(8) NOT NULL auto_increment,
	  TermNameEN varchar(255) default NULL,
	  TermNameB5 varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (TermID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form table",
	"CREATE TABLE IF NOT EXISTS YEAR (
	  YearID int(8) NOT NULL auto_increment,
	  YearName varchar(50) default NULL,
	  Sequence int(2) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  WEBSAMSCode varchar(255) default NULL,
	  PRIMARY KEY  (YearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form Class table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS (
	  YearClassID int(8) NOT NULL auto_increment,
	  AcademicYearID int(8) default NULL,
	  YearID int(8) default NULL,
	  ClassID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  ClassTitleEN varchar(255) default NULL,
	  ClassTitleB5 varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  PRIMARY KEY  (YearClassID),
	  UNIQUE KEY YearClass (AcademicYearID,YearID,ClassID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form Class Subject table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_SUBJECT (
	  YearClassSubjectID int(8) NOT NULL auto_increment,
	  YearClassID int(8) default NULL,
	  SubjectGroupID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (YearClassSubjectID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create Form Class Teacher table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_TEACHER (
	  YearClassTeacherID int(8) NOT NULL auto_increment,
	  YearClassID int(8) default NULL,
	  UserID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (YearClassTeacherID),
	  UNIQUE KEY YearClassUser (YearClassID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-15",
	"School Settings -> Class - Create form class student table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_USER (
	  YearClassUserID int(8) NOT NULL auto_increment,
	  YearClassID int(8) default NULL,
	  UserID int(8) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  ClassNumber int(8) default NULL,
	  PRIMARY KEY  (YearClassUserID),
	  UNIQUE KEY YearClassUser (YearClassID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Subject - Add Subject-LearningCategory Mapping in the Subject Table",
	"ALTER Table SUBJECT add LearningCategoryID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Subject - RecordStatus of the Subject: 0 - inactive; 1 - active",
	"ALTER Table SUBJECT add RecordStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Subject - RecordStatus of the Subject Component: 0 - inactive; 1 - active",
	"ALTER Table SUBJECT_COMPONENT add RecordStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2009-05-25",
	"School Settings -> Class - Create form class student table",
	"CREATE TABLE IF NOT EXISTS LEARNING_CATEGORY (
		LearningCategoryID int(11) auto_increment,
		Code varchar(10) default NULL,
		NameChi varchar(255) default NULL,
		NameEng varchar(255) default NULL,
		DisplayOrder int(11) default NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		LastModifiedBy int(11) default NULL,
		PRIMARY KEY (LearningCategoryID),
		KEY LearningCategoryCode (Code),
		KEY LearningCategoryNameChi (NameChi),
		KEY LearningCategoryNameEng (NameEng)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

/*
$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Rename ComponentSubjectCode to SubjectComponentCode for consistency",
	"ALTER TABLE SUBJECT_COMPONENT CHANGE ComponentSubjectCode SubjectComponentCode varchar(5) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Add Sequence field for Subject Component",
	"ALTER TABLE SUBJECT_COMPONENT ADD Sequence int(5) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT drop SubjectComponentNameEN;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT drop SubjectComponentNameB5;"
);

$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentAbbrB5 varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentAbbrEN varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentShortFormB5 varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-27",
	"School Settings -> Subject - Change Subject Component Schema for consistency with SUBJECT table",
	"alter table SUBJECT_COMPONENT add SubjectComponentShortFormEN varchar(100) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Unify the data type of SubjectCode of Subject and SubjectComponent",
	"ALTER TABLE SUBJECT_COMPONENT CHANGE SubjectCode SubjectCode varchar(10) default NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Unify the data type of SubjectCode of Subject and SubjectComponent",
	"ALTER TABLE SUBJECT_COMPONENT CHANGE SubjectComponentCode SubjectComponentCode varchar(10) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT Add UniqueSubjectID int(8) NOT NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT Add INDEX UniqueSubjectID (UniqueSubjectID);"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT_COMPONENT Add UniqueSubjectID int(8) NOT NULL;"
);
$sql_eClassIP_update[] = array(
	"2009-05-29",
	"School Settings -> Subject - Set an unique code among Subject and Subject Component",
	"ALTER TABLE SUBJECT_COMPONENT Add INDEX UniqueSubjectID (UniqueSubjectID);"
);

*/


$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Location - Building English Name need not unique",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP Index EnglishName;"
);
$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Location - Building Chinese Name need not unique",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP Index ChineseName;"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Create subject table",
	"CREATE TABLE IF NOT EXISTS ASSESSMENT_SUBJECT (
		RecordID int(11) NOT NULL auto_increment,
		CODEID varchar(20) default NULL,
		EN_SNAME varchar(20) default NULL,
		CH_SNAME varchar(20) default NULL,
		EN_DES varchar(255) default NULL,
		CH_DES varchar(255) default NULL,
		EN_ABBR varchar(100) default NULL,
		CH_ABBR varchar(100) default NULL,
		DisplayOrder int(11) default NULL,
		AllLevelSubject int(11) default NULL,
		Level1Subject int(11) default NULL,
		Level2Subject int(11) default NULL,
		Level3Subject int(11) default NULL,
		Level4Subject int(11) default NULL,
		Level5Subject int(11) default NULL,
		Level6Subject int(11) default NULL,
		Level7Subject int(11) default NULL,
		Level8Subject int(11) default NULL,
		Level9Subject int(11) default NULL,
		InputDate datetime default NULL,
		ModifiedDate datetime default NULL,
		CMP_CODEID varchar(20) default NULL,
		LearningCategoryID varchar(8) default NULL,
		LastModifiedBy int(8) default NULL,
		RecordStatus tinyint(1) default '1',
		PRIMARY KEY (RecordID),
		UNIQUE KEY SBJ_CMP_CODEID (CODEID,CMP_CODEID),
		KEY DisplayOrder (DisplayOrder),
		KEY LearningCategoryID (LearningCategoryID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Add LearningCategory-Subject mapping in the subject table",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD LearningCategoryID varchar(8) default null;"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Set LearningCategoryID as an index",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD INDEX LearningCategoryID (LearningCategoryID);"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Add LastModifiedBy field to record the use who modified the record",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD LastModifiedBy int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-06-04",
	"School Settings -> Subject - Add RecordStatus field to record active or inactive subject",
	"ALTER TABLE ASSESSMENT_SUBJECT ADD RecordStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE",
	"CREATE TABLE IF NOT EXISTS ROLE (
	  RoleID int(8) NOT NULL auto_increment,
	  RoleName varchar(100) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (RoleID),
	  UNIQUE KEY RoleName (RoleName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE_MEMBER",
	"CREATE TABLE IF NOT EXISTS ROLE_MEMBER (
	  RoleID int(8) NOT NULL,
	  UserID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  IdentityType varchar(20) default NULL,
	  PRIMARY KEY  (RoleID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE_RIGHT table",
	"CREATE TABLE IF NOT EXISTS ROLE_RIGHT (
	  RoleID int(8) NOT NULL,
	  FunctionName varchar(200) NOT NULL,
	  RightFlag int(1) default '0',
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (RoleID,FunctionName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-06-05",
	"create ROLE_RIGHT table",
	"CREATE TABLE IF NOT EXISTS ROLE_TARGET (
	  RoleID int(8) NOT NULL,
	  TargetName varchar(200) NOT NULL,
	  RightFlag int(1) default '0',
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (RoleID,TargetName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - Create Temp cycle period setting table > INTRANET_CYCLE_GENERATION_PERIOD",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_GENERATION_PERIOD (
	  PeriodID int(11) NOT NULL auto_increment,
	  PeriodStart date NOT NULL default '0000-00-00',
	  PeriodEnd date NOT NULL default '0000-00-00',
	  PeriodType int(11) default NULL,
	  CycleType int(11) default NULL,
	  PeriodDays int(11) default NULL,
	  FirstDay int(11) default NULL,
	  SaturdayCounted int(11) default NULL,
	  Sequence int(2) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY (PeriodID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create tmp cycle period preview table > INTRANET_CYCLE_TEMP_DAYS_VIEW",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_TEMP_DAYS_VIEW (
	  RecordDate date NOT NULL default '0000-00-00',
	  TextEng varchar(50) default NULL,
	  TextChi varchar(50) default NULL,
	  TextShort varchar(50) default NULL,
	  PRIMARY KEY (RecordDate)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create cycle period production view table > INTRANET_CYCLE_DAYS",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_DAYS (
	  RecordDate date NOT NULL default '0000-00-00',
	  TextEng varchar(50) default NULL,
	  TextChi varchar(50) default NULL,
	  TextShort varchar(50) default NULL,
	  PRIMARY KEY (RecordDate)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create cycle period import table > INTRANET_CYCLE_IMPORT_RECORD",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_IMPORT_RECORD (
	  RecordID int(11) NOT NULL auto_increment,
	  RecordDate date NOT NULL default '0000-00-00',
	  TextEng varchar(50) default NULL,
	  TextChi varchar(50) default NULL,
	  TextShort varchar(50) default NULL,
	  PRIMARY KEY  (RecordID),
	  UNIQUE KEY RecordDate (RecordDate)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create cycle period gen production view log table > INTRANET_CYCLE_DAYS_PRODUCTION_LOG",
	"CREATE TABLE IF NOT EXISTS INTRANET_CYCLE_DAYS_PRODUCTION_LOG (
	  LogID int(11) NOT NULL auto_increment,
	  DateInput datetime default NULL,
	  PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - add column to store cycle period bgcolor in INTRANET_CYCLE_GENERATION_PERIOD",
	"ALTER TABLE INTRANET_CYCLE_GENERATION_PERIOD ADD COLUMN ColorCode varchar(10) AFTER SaturdayCounted"
);

$sql_eClassIP_update[] = array(
	"2009-06-12",
	"School Settings -> School Calendar - create school event / holiday table > INTRANET_EVENT",
	"CREATE TABLE IF NOT EXISTS INTRANET_EVENT (
	  EventID int(8) NOT NULL auto_increment,
	  Title varchar(255) default NULL,
	  Description text,
	  EventDate datetime default NULL,
	  EventVenue varchar(100) default NULL,
	  EventNature varchar(100) default NULL,
	  ReadFlag text,
	  UserID int(8) default NULL,
	  isSkipCycle tinyint(4) default NULL,
	  Internal int(11) default NULL,
	  RecordType char(2) default NULL,
	  RecordStatus char(2) default NULL,
	  OwnerGroupID int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (EventID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-06-24",
	"add InternalClassCode for batch update subject group in Subject group mapping",
	"alter table SUBJECT_TERM_CLASS add InternalClassCode varchar(50) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing timetable information",
	"CREATE TABLE IF NOT EXISTS INTRANET_SCHOOL_TIMETABLE (
		TimetableID int(11) NOT NULL auto_increment,
		AcademicYearID int(11) default NULL,
		YearTermID int(11) default NULL,
		TimetableName varchar(255) default NULL,
		CycleDays tinyint(2) default '1',
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (TimetableID),
		KEY AcademicYearID (AcademicYearID),
		KEY YearTermID (YearTermID),
		KEY CycleDays (CycleDays)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing timetable time slot",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_TIMESLOT (
		TimeSlotID int(11) NOT NULL auto_increment,
		TimeSlotName varchar(255) NOT NULL,
		StartTime time NOT NULL,
		EndTime time NOT NULL,
		DisplayOrder tinyint(2) default NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (TimeSlotID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing timetable time slot relationship",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_TIMESLOT_RELATION (
		TimetableID int(11) NOT NULL,
		TimeSlotID int(11) NOT NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-08",
	"School Settings -> Timetable - create table storing room allocation information",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_ROOM_ALLOCATION (
		RoomAllocationID int(11) NOT NULL auto_increment,
		TimetableID int(11) NOT NULL,
		TimeSlotID int(11) NOT NULL,
		Day tinyint(2) NOT NULL,
		LocationID int(11) default NULL,
		OthersLocation varchar(255) default NULL,
		SubjectGroupID int(8) NOT NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		LastModifiedBy int(11) default NULL,
		PRIMARY KEY (RoomAllocationID),
		KEY TimetableID (TimetableID),
		KEY TimeSlotID (TimeSlotID),
		KEY Day (Day),
		KEY LocationID (LocationID),
		KEY SubjectGroupID (SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

/*
$sql_eClassIP_update[] = array(
	"2009-07-10",
	"Role setting -> role target -> add Identity field",
	"alter table ROLE_TARGET add Identity varchar(15) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Role Target Restructure",
	"alter table ROLE_TARGET DROP PRIMARY KEY"
);

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Role Target Restructure",
	"alter table ROLE_TARGET ADD PRIMARY KEY (RoleID,TargetName,Identity)"
);
*/

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Table for General Settings",
	"CREATE TABLE IF NOT EXISTS GENERAL_SETTING (
	  Module varchar(20) NOT NULL default '',
	  SettingName varchar(100) NOT NULL default '',
	  SettingValue text,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (Module,SettingName)
	) ENGINE=innoDB DEFAULT CHARSET=utf8");

$sql_eClassIP_update[] = array(
	"2009-07-13",
	"Change Class Number format in INTRANET_USER to integer",
	"alter table INTRANET_USER change ClassNumber ClassNumber int(8) default NULL;");

$sql_eClassIP_update[] = array(
	"2009-07-20",
	"Table for Campus Link",
	"CREATE TABLE IF NOT EXISTS INTRANET_CAMPUS_LINK (
	LinkID int(8) NOT NULL auto_increment,
	Title varchar(255) default NULL,
	URL varchar(255) default NULL,
	DisplayOrder int(11) default NULL,
	Description text,
	DateInput datetime default NULL,
	InputBy int(11) default NULL,
	DateModified datetime default NULL,
	ModifiedBy int(11) default NULL,
	PRIMARY KEY  (LinkID)
	)  ENGINE=innoDB DEFAULT CHARSET=utf8
");

$sql_eClassIP_update[] = array(
	"2009-07-21",
	"Change table PAYMENT_PAYMENT_ITEMSTUDENT field Amount to Double, to prevent overflow problem",
	"alter table PAYMENT_PAYMENT_ITEMSTUDENT change Amount Amount Double(20,2) default NULL");

$sql_eClassIP_update[] = array(
	"2009-07-21",
	"Change table PAYMENT_PAYMENT_ITEMSTUDENT field SubsidyAmount to Double, to prevent overflow problem",
	"alter table PAYMENT_PAYMENT_ITEMSTUDENT change SubsidyAmount SubsidyAmount Double(20,2) default NULL");

$sql_eClassIP_update[] = array(
	"2009-07-21",
	"Change table PAYMENT_SUBSIDY_UNIT field TotalAmount to Double, to prevent overflow problem",
	"alter table PAYMENT_SUBSIDY_UNIT change TotalAmount TotalAmount double(20,2) default NULL");

$sql_eClassIP_update[] = array(
"2009-07-23",
"create a LSLP user table for license control",
"CREATE TABLE IF NOT EXISTS LS_USER (
  UserID int(11) NOT NULL,
  LsUserID int(11),
  DateInput datetime NOT NULL,
  LastUsed datetime,
  PRIMARY KEY (UserID)
)ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-07-24",
	"Add column 'Module' to table ACCESS_RIGHT_GROUP",
	"ALTER TABLE ACCESS_RIGHT_GROUP ADD COLUMN Module varchar(50) DEFAULT 'DISCIPLINE';");

$sql_eClassIP_update[] = array(
	"2009-07-27",
	"Add column 'AcademicYearID' to INTRANET_GROUP",
	"alter table INTRANET_GROUP add AcademicYearID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-07-29",
	"Add column 'YearTermID' to DISCIPLINE_MERIT_RECORD",
	"alter table DISCIPLINE_MERIT_RECORD add YearTermID int(8) default NULL AFTER Semester;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'AcademicYearID' to DISCIPLINE_MERIT_RECORD",
	"alter table DISCIPLINE_MERIT_RECORD add AcademicYearID int(8) default NULL AFTER Year;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'YearTermID' to DISCIPLINE_ACCU_RECORD",
	"alter table DISCIPLINE_ACCU_RECORD add YearTermID int(8) default NULL AFTER Semester;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'AcademicYearID' to DISCIPLINE_ACCU_RECORD",
	"alter table DISCIPLINE_ACCU_RECORD add AcademicYearID int(8) default NULL AFTER Year;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'GroupID' to YEAR_CLASS",
	"alter table YEAR_CLASS add GroupID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'YearTermID' to DISCIPLINE_CASE",
	"alter table DISCIPLINE_CASE add YearTermID int(8) default NULL AFTER Semester;"
);

$sql_eClassIP_update[] = array(
	"2009-07-30",
	"Add column 'AcademicYearID' to DISCIPLINE_CASE",
	"alter table DISCIPLINE_CASE add AcademicYearID int(8) default NULL AFTER Year;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"Make INTRANET_USER Class Number field from varchar(20) to varchar(255)",
	"alter table INTRANET_USER change ClassName ClassName varchar(255) DEFAULT NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column InputBy int(8)",
	"ALTER TABLE INTRANET_EVENT ADD COLUMN InputBy int(8) after DateInput;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column ModifyBy int(8)",
	"ALTER TABLE INTRANET_EVENT ADD COLUMN ModifyBy int(8) after DateModified;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Add column InputBy int(8)",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column InputBy int(8) after DateInput;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Add column ModifyBy int(8)",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column ModifyBy int(8) after DateModified;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column RelatedTo int(8)",
	"alter table INTRANET_EVENT add column RelatedTo int(8) NULL AFTER Internal;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column EventLocationID int(8)",
	"alter table INTRANET_EVENT add column EventLocationID int(8) after EventDate;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Holiday and Event - Add column EventLocationID int(8)",
	"alter table INTRANET_EVENT add column EventLocationID int(8) after EventDate;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Add column Sequence int(2)",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column Sequence int(2) after ColorCode;"
);

$sql_eClassIP_update[] = array(
	"2009-08-10",
	"School Settings -> School Calendar -> Time Zone - Create table INTRANET_PERIOD_TIMETABLE_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_PERIOD_TIMETABLE_RELATION (
  	PeriodID int(11) NOT NULL,
  	TimetableID int(11) NOT NULL,
  	RecordType int(11) default NULL,
  	RecordStatus int(11) default NULL,
  	DateInput datetime default NULL,
  	DateModified datetime default NULL,
  	PRIMARY KEY (PeriodID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
"2009-08-10",
"SmartCard Lunch-box",
"CREATE TABLE IF NOT EXISTS CARD_STUDENT_LUNCH_CALENDAR (
 RecordID int NOT NULL auto_increment,
 Year int,
 Month int,
 DaysString text,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE YearMonth (Year, Month)
) ENGINE=InnoDB Charset=utf8"
);


$sql_eClassIP_update[] = array(
"2009-08-10",
"SmartCard Lunch-box",
"CREATE TABLE IF NOT EXISTS CARD_STUDENT_LUNCH_BAD_LOG (
 RecordID int NOT NULL auto_increment,
 RecordDate date,
 StudentID int,
 ActionTime datetime,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX StudentID (StudentID),
 INDEX RecordType (RecordType),
 INDEX RecordStatus (RecordStatus)
) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-08-11",
	"School Settings -> Class - Add WEBSAMSCode field",
	"alter table YEAR_CLASS add column WEBSAMSCode varchar(5)"
);

$sql_eClassIP_update[] = array(
	"2009-08-13",
	"Create table for deletion log",
	"CREATE TABLE IF NOT EXISTS MODULE_RECORD_DELETE_LOG
	(
		LogID int(11) NOT NULL auto_increment,
		Module varchar(20) NOT NULL,
		Section varchar(50) default NULL,
		RecordDetail text default NULL,
		DelTableName varchar(50) default NULL,
		DelRecordID int(11) default NULL,
		LogDate datetime default NULL,
		LogBy int(11) default NULL,
		PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_CALENDAR",
	"Alter Table CALENDAR_CALENDAR
		Add Column CalSharedType char(1),
		Add Column SyncURL varchar(200)
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_CALENDAR_VIEWER",
	"Alter Table CALENDAR_CALENDAR_VIEWER
		Add Column GroupPath varchar(255)
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_ENTRY",
	"Alter Table CALENDAR_EVENT_ENTRY
		Add Column UID varchar(100),
		Add Column ExtraIcalInfo mediumtext
	"
);
$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_ENTRY_REPEAT",
	"Alter Table CALENDAR_EVENT_ENTRY_REPEAT
		Add Column ICalIsInfinite char(1) Default 0
	"
);

$sql_eClassIP_update[] = array(
	"2009-08-14",
	"iCalendar - Alter table CALENDAR_EVENT_PERSONAL_NOTE",
	"Alter Table CALENDAR_EVENT_PERSONAL_NOTE
		Add Column CalType tinyint(4)
	"
);

$sql_eClassIP_update[] = array(
	"2009-08-20",
	"Lesson Attendance Default plan table",
	"CREATE TABLE IF NOT EXISTS SUBJECT_GROUP_ATTEND_DEFAULT_PLAN (
	  PlanID int(8) NOT NULL auto_increment,
	  SubjectGroupID int(8) NOT NULL default '0',
	  PlanName varchar(255) default NULL,
	  StudentPositionAMF text,
	  PlanLayoutAMF text,
	  LocationID int(8) default NULL,
	  CreateBy int(8) default NULL,
	  CreateDate datetime default NULL,
	  LastModifiedBy int(8) default NULL,
	  LastModifiedDate datetime default NULL,
	  PRIMARY KEY  (PlanID,SubjectGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
"2009-08-20",
"Change GroupCategoryType from char(2) to int(11)",
"alter table INTRANET_ROLE change RecordType RecordType int(11);"
);

$sql_eClassIP_update[] = array(
	"2009-08-21",
	"eClass update history table",
	"CREATE TABLE IF NOT EXISTS ECLASS_UPDATE_HISTORY (
	  HistoryID int(8) NOT NULL auto_increment,
	  EventTime datetime default NULL,
	  Events text,
	  Status varchar(16),
	  ReferenceID int(11),
	  PRIMARY KEY (HistoryID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-08-21",
	"Lesson attendance Default Plan - add plan id generate by Flash",
	"alter table SUBJECT_GROUP_ATTEND_DEFAULT_PLAN add FlashPlanID varchar(20) after SubjectGroupID;"
);

$sql_eClassIP_update[] = array(
	"2009-08-25",
	"[Sport Day] Add code to event",
	"alter table SPORTS_EVENTGROUP add EventCode varchar(20);"
);

$sql_eClassIP_update[] = array(
	"2009-08-25",
	"[Swimming Gala] Add code to event",
	"alter table SWIMMINGGALA_EVENTGROUP add EventCode varchar(20);"
);

$sql_eClassIP_update[] = array(
	"2009-08-26",
	"Add academic Year ID to Homework record",
	"alter table INTRANET_HOMEWORK add AcademicYearID int(8)"
);

$sql_eClassIP_update[] = array(
	"2009-08-28",
	"enlarge the size of UID in Calendar event entry",
	"alter table CALENDAR_EVENT_ENTRY modify COLUMN UID varchar(200)"
);

$sql_eClassIP_update[] = array(
	"2009-08-31",
	"Add academic Year Term ID to Homework record",
	"alter table INTRANET_HOMEWORK add YearTermID int(8)"
);

$sql_eClassIP_update[] = array(
	"2009-09-03",
	"Drop UNIQUE KEY on Title to INTRANET_GROUP",
	"alter table INTRANET_GROUP drop KEY title;"
);

$sql_eClassIP_update[] = array(
	"2009-09-03",
	"Add UNIQUE KEY to column 'Title,AcademicYearID' to INTRANET_GROUP",
	"alter table INTRANET_GROUP ADD KEY AcademicYearTitle (Title,AcademicYearID);"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add description to CALENDAR_CALENDAR",
	"alter table CALENDAR_CALENDAR ADD column Description mediumtext;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add groupType, groupID to CALENDAR_CALENDAR",
	"alter table CALENDAR_CALENDAR_VIEWER ADD column GroupID int(8), ADD column GroupType char(1)"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add CalType to CALENDAR_REMINDER",
	"alter table CALENDAR_REMINDER Add Column CalType tinyint(1)"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"Add groupType, groupID to CALENDAR_CALENDAR",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key EventID, Drop Key UserID, Add Column PersonalNoteID int auto_increment primary key"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"RENAME TABLE SUBJECT_GROUP_ATTEND_DEFAULT_PLAN TO ATTEND_DEFAULT_PLAN;",
	"RENAME TABLE SUBJECT_GROUP_ATTEND_DEFAULT_PLAN TO ATTEND_DEFAULT_PLAN;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) default NULL;",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN drop PRIMARY KEY;",
	"alter table ATTEND_DEFAULT_PLAN drop PRIMARY KEY;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN ADD PRIMARY KEY (PlanID);",
	"alter table ATTEND_DEFAULT_PLAN ADD PRIMARY KEY (PlanID);"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) NOT NULL auto_increment;",
	"alter table ATTEND_DEFAULT_PLAN change PlanID PlanID int(8) NOT NULL auto_increment;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN add YearClassID int (8) after SubjectGroupID;",
	"alter table ATTEND_DEFAULT_PLAN add YearClassID int (8) after SubjectGroupID;"
);

$sql_eClassIP_update[] = array(
	"2009-09-04",
	"alter table ATTEND_DEFAULT_PLAN change LocationID LocationID int (8) after YearClassID;",
	"alter table ATTEND_DEFAULT_PLAN change LocationID LocationID int (8) after YearClassID;"
);

$sql_eClassIP_update[] = array(
	"2009-09-07",
	"create general log file to log down crond job working status",
	"CREATE TABLE IF NOT EXISTS GENERAL_LOG (
	  RecordID int(8) NOT NULL auto_increment,
	  Process varchar(50) NOT NULL default '',
	  Result varchar(1),
	  Remarks text,
	  RunDate TIMESTAMP not NULL default CURRENT_TIMESTAMP,
	  PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Alter SUBJECT_TERM_CLASS table for eclass mapping",
	"alter table SUBJECT_TERM_CLASS add course_id int(8)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index TimetableID in INTRANET_TIMETABLE_TIMESLOT_RELATION",
	"alter table INTRANET_TIMETABLE_TIMESLOT_RELATION add index TimetableID (TimetableID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index TimeSlotID in INTRANET_TIMETABLE_TIMESLOT_RELATION",
	"alter table INTRANET_TIMETABLE_TIMESLOT_RELATION add index TimeSlotID (TimeSlotID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS",
	"alter table SUBJECT_TERM_CLASS add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearID in SUBJECT_TERM_CLASS_YEAR_RELATION",
	"alter table SUBJECT_TERM_CLASS_YEAR_RELATION add index YearID (YearID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS_YEAR_RELATION",
	"alter table SUBJECT_TERM_CLASS_YEAR_RELATION add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS_TEACHER",
	"alter table SUBJECT_TERM_CLASS_TEACHER add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in SUBJECT_TERM_CLASS_TEACHER",
	"alter table SUBJECT_TERM_CLASS_TEACHER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM_CLASS_USER",
	"alter table SUBJECT_TERM_CLASS_USER add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in SUBJECT_TERM_CLASS_USER",
	"alter table SUBJECT_TERM_CLASS_USER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectID in SUBJECT_TERM",
	"alter table SUBJECT_TERM add index SubjectID (SubjectID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearTermID in SUBJECT_TERM",
	"alter table SUBJECT_TERM add index YearTermID (YearTermID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index SubjectGroupID in SUBJECT_TERM",
	"alter table SUBJECT_TERM add index SubjectGroupID (SubjectGroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index AcademicYearID in YEAR_CLASS",
	"alter table YEAR_CLASS add index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearID in YEAR_CLASS",
	"alter table YEAR_CLASS add index YearID (YearID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index GroupID in YEAR_CLASS",
	"alter table YEAR_CLASS add index GroupID (GroupID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearClassID in YEAR_CLASS_TEACHER",
	"alter table YEAR_CLASS_TEACHER add index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in YEAR_CLASS_TEACHER",
	"alter table YEAR_CLASS_TEACHER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index YearClassID in YEAR_CLASS_USER",
	"alter table YEAR_CLASS_USER add index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index UserID in YEAR_CLASS_USER",
	"alter table YEAR_CLASS_USER add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2009-09-09",
	"Timetable - Add index AcademicYearID in ACADEMIC_YEAR_TERM",
	"alter table ACADEMIC_YEAR_TERM add index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Staff Smart Card Attendance System - Add Table to store user customized attendance status symbols",
"CREATE TABLE IF NOT EXISTS CARD_STAFF_ATTENDANCE_STATUS_SYMBOL(
	StatusID int(11) NOT NULL,
	StatusSymbol varchar(20),
	PRIMARY KEY (StatusID)
)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Staff Smart Card Attendance System - Add init symbols for users to customize attendance status symbols",
"INSERT INTO CARD_STAFF_ATTENDANCE_STATUS_SYMBOL (StatusID) VALUES (0),(1),(2),(3),(4),(5),(6)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Student Smart Card Attendance - Add Table to store user customized attendance status symbols",
"CREATE TABLE IF NOT EXISTS CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL(
	StatusID int(11) NOT NULL,
	StatusSymbol varchar(20),
	PRIMARY KEY (StatusID)
)"
);

$sql_eClassIP_update[] = array(
"2009-09-10",
"Student Smart Card Attendance - Add init symbols for users to customize attendance status symbols",
"INSERT INTO CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL (StatusID) VALUES (0),(1),(2),(3),(4)"
);

$sql_eClassIP_update[] = array(
"2009-09-23",
"iMail - add Column DateInFolder ",
"alter TABLE INTRANET_CAMPUSMAIL add column DateInFolder datetime after DateModified"
);

$sql_eClassIP_update[] = array(
"2009-09-28",
"iCalendar add GroupType ",
"alter TABLE CALENDAR_CALENDAR_VIEWER add column `GroupType` char(1)"
);

$sql_eClassIP_update[] = array(
"2009-09-28",
"iCalendar add GroupID ",
"alter TABLE CALENDAR_CALENDAR_VIEWER add column `GroupID` int(8)"
);

$sql_eClassIP_update[] = array(
"2009-09-28",
"iCalendar add GroupPath ",
"alter TABLE CALENDAR_CALENDAR_VIEWER add column `GroupPath` varchar(255)"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Staff Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STAFF_ATTENDANCE_STATUS_SYMBOL TYPE=innoDB"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Staff Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STAFF_ATTENDANCE_STATUS_SYMBOL CONVERT TO CHARACTER SET utf8"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Student Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL TYPE=innoDB"
);

$sql_eClassIP_update[] = array(
"2009-10-05",
"Student Smart Card Attendance System - change Table of store user customized attendance status symbols to IP25's Engine and Charset",
"alter table CARD_STUDENT_ATTENDANCE_STATUS_SYMBOL CONVERT TO CHARACTER SET utf8"
);

$sql_eClassIP_update[] = array(
	"2009-10-05",
	"eDiscipline - set tag for AP items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_ITEM_TAG_SETTING (
		TagID int(11) NOT NULL auto_increment,
		TagName varchar(255),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (TagID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-10-05",
	"eDiscipline - store the tag of AP items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_AP_ITEM_TAG (
		APTagID int(11) NOT NULL auto_increment,
		APItemID int(11),
		TagID int(11),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (APTagID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-10-06",
	"StudentAttendance - add table for customize attend reason symbols",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_ATTENDANCE_REASON_SYMBOL (
	  ReasonType int(8) NOT NULL,
	  Reason varchar(255) NOT NULL,
	  StatusSymbol varchar(20),
	  CreateDate datetime,
	  LastModified datetime,
	  PRIMARY KEY  (ReasonType,Reason)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-10-09",
"eEnrolment - Add isAmountTBC in INTRANET_ENROL_GROUPINFO to cater club fee not confirmed improvement",
"Alter Table INTRANET_ENROL_GROUPINFO Add isAmountTBC tinyint(2) default 0"
);

$sql_eClassIP_update[] = array(
"2009-10-09",
"eEnrolment - Add isAmountTBC in INTRANET_ENROL_EVENTINFO to cater activity fee not confirmed improvement",
"Alter Table INTRANET_ENROL_EVENTINFO Add isAmountTBC tinyint(2) default 0"
);

$sql_eClassIP_update[] = array(
	"2009-10-13",
	"Payment - Add Invoice number for POS detail",
	"alter table PAYMENT_PURCHASE_DETAIL_RECORD add InvoiceNumber varchar(100) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct adjustment - Add AcademicYearID ",
	"alter table DISCIPLINE_CONDUCT_ADJUSTMENT add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct adjustment - Add YearTermID ",
	"alter table DISCIPLINE_CONDUCT_ADJUSTMENT add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct balance  - Add AcademicYearID ",
	"alter table DISCIPLINE_STUDENT_CONDUCT_BALANCE add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Conduct balance - Add YearTermID ",
	"alter table DISCIPLINE_STUDENT_CONDUCT_BALANCE add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Subscore balance  - Add AcademicYearID ",
	"alter table DISCIPLINE_STUDENT_SUBSCORE_BALANCE add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > Subscore balance - Add YearTermID ",
	"alter table DISCIPLINE_STUDENT_SUBSCORE_BALANCE add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > conduct change log  - Add AcademicYearID ",
	"alter table DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > conduct change log - Add YearTermID ",
	"alter table DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > subscore change log  - Add AcademicYearID ",
	"alter table DISCIPLINE_SUB_SCORE_CHANGE_LOG add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > subscore change log - Add YearTermID ",
	"alter table DISCIPLINE_SUB_SCORE_CHANGE_LOG add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > DISCIPLINE_SEMESTER_RATIO  - Add AcademicYearID ",
	"alter table DISCIPLINE_SEMESTER_RATIO add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-17",
	"eDiscipline > DISCIPLINE_SEMESTER_RATIO - Add YearTermID ",
	"alter table DISCIPLINE_SEMESTER_RATIO add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-20",
	"SMS - Add index to RecordStatus in INTRANET_SMS2_SOURCE_MESSAGE to speed up the sms status retrieval",
	"alter table INTRANET_SMS2_SOURCE_MESSAGE add index RecordStatus (RecordStatus);"
);

$sql_eClassIP_update[] = array(
	"2009-10-20",
	"SMS - Add index to ReferenceID in INTRANET_SMS2_MESSAGE_RECORD to speed up the sms status retrieval",
	"alter table INTRANET_SMS2_MESSAGE_RECORD add index ReferenceID (ReferenceID);"
);

$sql_eClassIP_update[] = array(
	"2009-10-20",
	"SMS - Add index to RelatedUserID in INTRANET_SMS2_MESSAGE_RECORD to speed up the sms status retrieval",
	"alter table INTRANET_SMS2_MESSAGE_RECORD add index RelatedUserID (RelatedUserID);"
);

$sql_eClassIP_update[] = array(
	"2009-10-29",
	"eDiscipline > AP record add SubjectID ",
	"alter table DISCIPLINE_MERIT_RECORD add SubjectID int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-10-30",
	"StudentAttendance - Add new column AbsentSession to record the number of absent sessions for each student",
	"ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD COLUMN AbsentSession int(11);"
);

$sql_eClassIP_update[] = array(
	"2009-11-09",
	"eNotice - add print date for reference",
	"ALTER TABLE INTRANET_NOTICE_REPLY ADD COLUMN PrintDate datetime default NULL;"
);

$sql_eClassIP_update[] = array(
	"2009-11-10",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_MERIT_RECORD change ProfileMeritCount ProfileMeritCount float(8,2)"
);

$sql_eClassIP_update[] = array(
	"2009-11-16",
	"eDiscipline - create table for UCCKE Conduct Grade [CRM Ref No.: 2009-0923-0926]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_STUDENT_CONDUCT_GRADE (
		StudentID int(11) NOT NULL,
		Year varchar(100),
		AcademicYearID int(8),
		Semester varchar(100),
		YearTermID int(8),
		IsAnnual int(11),
		GradeChar varchar(100),
		INDEX StudentID (StudentID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

/*
$sql_eClassIP_update[] = array(
	"2009-11-16",
	"eDiscipline - create table for control the category can access by which access group",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_CATEGORY_GROUP_ACCESSRIGHT
	(
		RecordID int(11) NOT NULL auto_increment,
		CategoryType char(2) NOT NULL,
		CategoryID int(11),
		GroupID int(11),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
*/

$sql_eClassIP_update[] = array(
	"2009-11-18",
	"Add RankPattern Setting for eSports",
	"ALTER TABLE SPORTS_SYSTEM_SETTING ADD COLUMN RankPattern VARCHAR(4) Default '1223' AFTER DefaultLaneArrangement"
);

$sql_eClassIP_update[] = array(
	"2009-11-18",
	"Add HiddenAutoArrangeButton Setting for eSports > SportDay",
	"ALTER TABLE SPORTS_SYSTEM_SETTING ADD COLUMN HiddenAutoArrangeButton tinyint(1) Default 0 AFTER RankPattern"
);

$sql_eClassIP_update[] = array(
	"2009-11-18",
	"Add HiddenAutoArrangeButton Setting for eSports > SwimmingGala",
	"ALTER TABLE SWIMMINGGALA_SYSTEM_SETTING ADD COLUMN HiddenAutoArrangeButton tinyint(1) Default 0 AFTER DefaultLaneArrangement"
);

/*
CREATE TABLE IF NOT EXISTS DISCIPLINE_GROUP_ACCESS_GM_ITEM
(
	RecordID int(11) NOT NULL auto_increment,
	GroupID int(11),
	CategoryID int(11),
	ItemID int(11),
	PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
*/

$sql_eClassIP_update[] = array(
	"2009-11-26",
	"School Settings -> Class - Add Class Group Table",
	"CREATE TABLE IF NOT EXISTS YEAR_CLASS_GROUP (
		ClassGroupID int(11) NOT NULL auto_increment,
		Code varchar(10),
		TitleEn varchar(255),
		TitleCh varchar(255),
		DisplayOrder int(11),
		RecordStatus int(11),
		DateInput datetime,
		DateModified datetime,
		LastModifiedBy int(11),
		PRIMARY KEY (ClassGroupID),
		INDEX Code (Code),
		INDEX DisplayOrder (DisplayOrder)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-11-26",
	"School Settings -> Class - Add Class Group Mapping in Class Table",
	"ALTER TABLE YEAR_CLASS ADD COLUMN ClassGroupID int(11) Default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-11-30",
	"iMail - RawMessage Table",
	"CREATE TABLE IF NOT EXISTS INTRANET_RAWCAMPUSMAIL (
	CampusMailID int(8) NOT NULL,
	UserID int(8) default NULL,
	HeaderFilePath mediumtext,
	MessageFilePath mediumtext,
	MessageEncoding varchar(60) default NULL,
	DateInput datetime default NULL,
	PRIMARY KEY (CampusMailID),
	INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-12-02",
	"iCalendar - Missing column 1 ",
	"ALTER TABLE CALENDAR_EVENT_USER ADD COLUMN `GroupType` char(1) default NULL "
);

$sql_eClassIP_update[] = array(
	"2009-12-02",
	"iCalendar - Missing column 2 ",
	"ALTER TABLE CALENDAR_REMINDER ADD COLUMN `ReminderBefore` int(8) default '0' "
);

$sql_eClassIP_update[] = array(
	"2009-12-03",
	"Discipline - Different Access Group can access different GM items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_GROUP_ACCESS_GM
	(
		RecordID int(11) NOT NULL auto_increment,
		GroupID int(11),
		CategoryID int(11),
		ItemID int(11),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-12-07",
	"Discipline - Different Access Group can access different AP items",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_GROUP_ACCESS_AP
	(
		RecordID int(11) NOT NULL auto_increment,
		GroupID int(11),
		CategoryID int(11),
		ItemID int(11),
		ConductScore int(11),
		Subscore int(11),
		MeritCount float(8,2),
		MeritType int(11),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2009-12-10",
	"alter table SPORTS_EVENT change SpecialLaneArrangment SpecialLaneArrangement mediumtext;",
	"alter table SPORTS_EVENT change SpecialLaneArrangment SpecialLaneArrangement mediumtext;"
);

$sql_eClassIP_update[] = array(
	"2009-12-14",
	"School Settings -> Subject Group - Record the Source of the Subject Group for the copy subject group function",
	"ALTER TABLE SUBJECT_TERM_CLASS ADD COLUMN CopyFromSubjectGroupID int(8) Default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-12-14",
	"School Settings -> Timetable - Record the Source of the Timetable for the copy timetable function from copy subject group",
	"ALTER TABLE INTRANET_SCHOOL_TIMETABLE ADD COLUMN CopyFromTimetableID int(11) Default NULL After CycleDays"
);

$sql_eClassIP_update[] = array(
	"2009-12-14",
	"School Settings -> Timetable - Record the Source of the TimeSlot for the copy timetable function from copy subject group",
	"ALTER TABLE INTRANET_TIMETABLE_TIMESLOT ADD COLUMN CopyFromTimeSlotID int(11) Default NULL After EndTime"
);

$sql_eClassIP_update[] = array(
	"2009-12-17",
	"Munsang Conduct Grade - add columns AcademicYearID and YearTermID to table DISCIPLINE_MS_STUDENT_CONDUCT",
	"Alter Table DISCIPLINE_MS_STUDENT_CONDUCT
		Add Column AcademicYearID int(8) default NULL AFTER Year,
		Add Column YearTermID int(8) default NULL AFTER Semester;
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-17",
	"Munsang Conduct Grade - add columns AcademicYearID and YearTermID to table DISCIPLINE_MS_STUDENT_CONDUCT_SEMI",
	"Alter Table DISCIPLINE_MS_STUDENT_CONDUCT_SEMI
		Add Column AcademicYearID int(8) default NULL AFTER Year,
		Add Column YearTermID int(8) default NULL AFTER Semester;
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-17",
	"Munsang Conduct Grade - add columns AcademicYearID and YearTermID to table DISCIPLINE_MS_STUDENT_CONDUCT_FINAL",
	"Alter Table DISCIPLINE_MS_STUDENT_CONDUCT_FINAL
		Add Column AcademicYearID int(8) default NULL AFTER Year,
		Add Column YearTermID int(8) default NULL AFTER Semester;
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Conduct Mark - create new table DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD to store the conduct mark calculation method",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD 
	(
	 	MethodType int(11) default 0,
		RecordType int(11),
		RecordStatus int(11),
		InputBy int(11),
		DateInput datetime,
		ModifiedBy int(11),
		DateModified datetime
	)ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column RecordType to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"ALTER TABLE DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN RecordType int(11) after AttendanceTakenDate"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column RecordStatus to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"ALTER TABLE DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN RecordStatus int(11) DEFAULT 1 after RecordType"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column ModifiedBy to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"Alter Table DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN ModifiedBy int(11) after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Detention - add column RelatedTo to DISCIPLINE_DETENTION_STUDENT_SESSION ",
	"Alter Table DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN RelatedTo int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Conduct Mark - create new table DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD to store the conduct mark calculation method",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_CONDUCT_MARK_CALCULATION_METHOD 
	(
	 	MethodType int(11) default 0,
		RecordType int(11),
		RecordStatus int(11),
		InputBy int(11),
		DateInput datetime,
		ModifiedBy int(11),
		DateModified datetime
	)ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2009-12-28",
	"eDis Misconduct- add a new column LateMinutes to DISCIPLINE_ACCU_CATEGORY ",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY ADD COLUMN LateMinutes int(4) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-01-04",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_MERIT_ITEM change NumOfMerit NumOfMerit float(8,2)"
);

$sql_eClassIP_update[] = array(
	"2010-01-05",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING change UpgradeDemeritCount UpgradeDemeritCount float(8,2)"
);

$sql_eClassIP_update[] = array
(
	"2010-01-05",
	"add Allow website setting to eCommunity (sharing)",
	"alter table INTRANET_GROUP add AllowedWebsite tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array(
	"2010-01-05",
	"Add Receipt Number counting for payment > print pages > receipt with flag sys_custom['PaymentReceiptNumber']",
	"CREATE TABLE IF NOT EXISTS PAYMENT_RECEIPT_COUNT (
	  AcademicYear varchar(15) NOT NULL,
	  ReceiptNumber int(11) default NULL,
	  PRIMARY KEY (AcademicYear)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-01-06",
	"add ActionDueDate in DISCIPLINE_MERIT_RECORD",
	"alter table DISCIPLINE_MERIT_RECORD add ActionDueDate date"
);

$sql_eClassIP_update[] = array
(
	"2010-01-07",
	"Copy Subject Group - Add a field to store the old course id before unlink the subject group and the eClass",
	"alter table SUBJECT_TERM_CLASS add old_course_id int(8) default NULL After course_id"
);

$sql_eClassIP_update[] = array
(
	"2010-01-14",
	" Add DisplayInCommunity to INTRANET_GROUP ",
	"alter table INTRANET_GROUP add DisplayInCommunity int(8) default 1 "
);


//----- CALENDAR_CALENDAR -----//
$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `Owner` int(8) NOT NULL default '0' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `Name` varchar(255) NOT NULL default '' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `Description` mediumtext "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `ShareToAll` char(1) NOT NULL default '0' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `CalType` tinyint(1) NOT NULL default '0' "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `DefaultEventAccess` char(1) default NULL "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `CalSharedType` char(1) default NULL "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `SyncURL` varchar(200) default NULL "
);


$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `SyncURL` varchar(200) default NULL "
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR add `CalID` int(8) NOT NULL auto_increment primary key"
);

//---- CALENDAR_CALENDAR_VIEWER ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `CalID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `GroupID` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `GroupType` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `Access` char(3) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `Color` char(6) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `Visible` char(1) default '1'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CALENDAR_VIEWER add `GroupPath` varchar(255) default NULL"
);

//---- CALENDAR_EVENT_ENTRY ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar remove dummy field  ",
	"alter table CALENDAR_EVENT_ENTRY drop column `ParentID`"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `EventID` int(8) NOT NULL auto_increment primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `RepeatID` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `CalID` int(10) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `EventDate` datetime NOT NULL default '0000-00-00 00:00:00'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `InputDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `ModifiedDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Duration` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `IsImportant` char(1) default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `IsAllDay` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Access` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Title` varchar(255) NOT NULL default ''"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Description` mediumtext"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Location` varchar(255) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `Url` mediumtext"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `UID` varchar(200) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY add `ExtraIcalInfo` mediumtext"
);

//---- CALENDAR_CONFIG ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CONFIG add `Setting` varchar(50) NOT NULL default '' primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_CONFIG add `Value` varchar(100) default NULL"
);

//---- CALENDAR_EVENT_ENTRY_REPEAT ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar remove dummy field  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT drop column `EventID`"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `RepeatID` int(8) NOT NULL auto_increment primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `RepeatType` varchar(20) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `EndDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `Frequency` int(2) default '1'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `Detail` varchar(7) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_ENTRY_REPEAT add `ICalIsInfinite` char(1) default '0'"
);

//---- CALENDAR_EVENT_PERSONAL_NOTE ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `EventID` int(10) NOT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `UserID` int(8) NOT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `PersonalNote` mediumtext"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `CreateDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `LastUpdateDate` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `CalType` tinyint(4) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_PERSONAL_NOTE add `PersonalNoteID` int(11) NOT NULL auto_increment primary key"
);

//---- CALENDAR_EVENT_USER ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `EventID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `GroupID` int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `GroupType` char(1) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `Status` char(1) default 'A'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `Access` char(1) default 'R'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_EVENT_USER add `InviteStatus` tinyint(4) default NULL"
);

//---- CALENDAR_REMINDER ------//

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderID` int(10) NOT NULL auto_increment primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `EventID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderType` char(1) NOT NULL default ''"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderDate` datetime NOT NULL default '0000-00-00 00:00:00'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `ReminderBefore` int(8) default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `LastSent` datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `TimesSent` int(10) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_REMINDER add `CalType` tinyint(4) default NULL"
);

//---- CALENDAR_USER_PREF ------//


$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar remove dummy field  ",
	"alter table CALENDAR_USER_PREF drop primary key"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_USER_PREF add `UserID` int(8) NOT NULL default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_USER_PREF add `Setting` varchar(50) NOT NULL default ''"
);

$sql_eClassIP_update[] = array
(
	"2010-01-19",
	" iCalendar add all fields  ",
	"alter table CALENDAR_USER_PREF add `Value` varchar(100) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-01-20",
	" eLibrary - add field ContentTLF in INTRANET_ELIB_BOOK to store whole book content in Text Layout Framework xml format ",
	"alter table INTRANET_ELIB_BOOK add `ContentTLF` mediumtext default NULL"
);

$sql_eClassIP_update[] = array(
	"2009-01-20",
	"School Settings -> Timetable - Record the Source of the TimeSlot for the copy timetable function from copy subject group (alter table again to make sure the field is here)",
	"ALTER TABLE INTRANET_TIMETABLE_TIMESLOT ADD COLUMN CopyFromTimeSlotID int(11) Default NULL After EndTime"
);

$sql_eClassIP_update[] = array(
	"2009-01-22",
	"Disciplinev12 - Munsang College 'Conduct Grade' meeting absent list",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST (
		RecordID int(11) NOT NULL auto_increment,
		UserID int(8) NOT NULL DEFAULT 0,
		RecordType int(11),
		RecordStatus int(11),
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-01-22",
	"eDiscipline - Change the Merit count from integer to float",
	"alter table DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE change ProfileMeritNum ProfileMeritNum float(8,2)"
);

$sql_eClassIP_update[] = array(
	"2010-01-25",
	"eNotice - add column DebitMethod in INTRANET_NOTICE for \"Payment Notice\"",
	"alter table INTRANET_NOTICE add column DebitMethod int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2010-01-25",
	"eDiscipline - add column ConversionPeriod in DISCIPLINE_MERIT_ITEM_CATEGORY for AP conversion improvement",
	"alter table DISCIPLINE_MERIT_ITEM_CATEGORY ADD COLUMN ConversionPeriod int(2) Default 0 after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2010-01-28",
	"eEnrolment - add column SchoolActivity to indicate the activity is 'inSchool' if value = 1 (by YatWoon)",
	"alter table INTRANET_ENROL_EVENTINFO ADD COLUMN SchoolActivity tinyint(1) Default 1"
);

$sql_eClassIP_update[] = array(
	"2010-02-01",
	"for eNotice-Payment enhancement, add NoticeID to PAYMENT_PAYMENT_ITEM",
	"alter table PAYMENT_PAYMENT_ITEM add NoticeID int(11) after DefaultAmount"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"for eNotice-Payment enhancement, add Unique key for ItemID and StudentID to PAYMENT_PAYMENT_ITEM",
	"alter table PAYMENT_PAYMENT_ITEMSTUDENT add unique key ItemIDStudentID (ItemID,StudentID)"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_ATTENDANCE",
	"alter table PROFILE_STUDENT_ATTENDANCE add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_ATTENDANCE",
	"alter table PROFILE_STUDENT_ATTENDANCE add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_MERIT",
	"alter table PROFILE_STUDENT_MERIT add AcademicYearID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'AcademicYearID' to PROFILE_STUDENT_MERIT",
	"alter table PROFILE_STUDENT_MERIT add YearTermID int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-02-02",
	"Add column 'isDeleted' to INTRANET_NOTICE - for payment notice enhancement",
	"ALTER TABLE INTRANET_NOTICE ADD Column isDeleted int(11) default 0"
);


$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_USERGROUP for Term-based Club enhancement",
	"ALTER TABLE INTRANET_USERGROUP ADD COLUMN EnrolGroupID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Drop old unique key (GroupID, UserID) in INTRANET_USERGROUP for Term-based Club enhancement",
	"Alter Table INTRANET_USERGROUP Drop Index GroupID;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add new unique key (GroupID, UserID, EnrolGroupID) in INTRANET_USERGROUP for Term-based Club enhancement",
	"Alter Table INTRANET_USERGROUP Add Unique (GroupID, UserID, EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_USERGROUP for Term-based Club enhancement",
	"ALTER TABLE INTRANET_USERGROUP Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_ENROL_GROUP_ATTENDANCE for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE ADD COLUMN EnrolGroupID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_ENROL_GROUP_ATTENDANCE for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add Semester in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO ADD COLUMN Semester int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add MinQuota in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO ADD COLUMN MinQuota int(11) default 0;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index Semester in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO Add INDEX Semester (Semester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Drop old unique key GroupID in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter Table INTRANET_ENROL_GROUPINFO Drop Index GroupID;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add new unique key (GroupID, Semester) in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter Table INTRANET_ENROL_GROUPINFO Add Unique (GroupID, Semester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Change from EnrolGroupID to EnrolGroupStudentID as the primary key in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT CHANGE EnrolGroupID EnrolGroupStudentID INT(11) auto_increment;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT ADD COLUMN EnrolGroupID INT(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT ADD COLUMN EnrolSemester int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolSemester (EnrolSemester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add Table INTRANET_ENROL_GROUPINFO_MAIN to record the Club main info for Term-based Club enhancement",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPINFO_MAIN (
		GroupMainInfoID int(11) NOT NULL auto_increment,
		GroupID int(11) NOT NULL default '0',
		ClubType char(2) default NULL,
		ApplyOnceOnly tinyint(1) default NULL,
		FirstSemEnrolOnly tinyint(1) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (GroupMainInfoID),
		UNIQUE KEY GroupID (GroupID)
	 )"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add OLE_ProgramID in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter table INTRANET_ENROL_GROUPINFO Add OLE_ProgramID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolGroupID in INTRANET_ENROL_EVENTINFO for Term-based Club enhancement",
	"Alter table INTRANET_ENROL_EVENTINFO Add EnrolGroupID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolGroupID in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolGroupID (EnrolGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT ADD COLUMN EnrolSemester int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add index EnrolSemester in INTRANET_ENROL_GROUPSTUDENT for Term-based Club enhancement",
	"ALTER TABLE INTRANET_ENROL_GROUPSTUDENT Add INDEX EnrolSemester (EnrolSemester);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add Table INTRANET_ENROL_GROUPINFO_MAIN to record the Club main info for Term-based Club enhancement",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPINFO_MAIN (
		GroupMainInfoID int(11) NOT NULL auto_increment,
		GroupID int(11) NOT NULL default '0',
		ClubType char(2) default NULL,
		ApplyOnceOnly tinyint(1) default NULL,
		FirstSemEnrolOnly tinyint(1) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (GroupMainInfoID),
		UNIQUE KEY GroupID (GroupID)
	 )"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"eEnrolment - Add OLE_ProgramID in INTRANET_ENROL_GROUPINFO for Term-based Club enhancement",
	"Alter table INTRANET_ENROL_GROUPINFO Add OLE_ProgramID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"Campus Location - Add Barcode in INVENTORY_LOCATION_BUILDING for Inventory Offline Stocktake use",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING ADD COLUMN Barcode varchar(10) after Code, Add UNIQUE KEY BuildingIDWithBarcode (BuildingID,Barcode);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"Campus Location - Add Barcode in INVENTORY_LOCATION_LEVEL for Inventory Offline Stocktake use",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL ADD COLUMN Barcode varchar(10) after Code, Add UNIQUE KEY LocationLevelIDWithBarcode (LocationLevelID,Barcode);"
);

$sql_eClassIP_update[] = array(
	"2010-02-18",
	"Campus Location - Add Barcode in INVENTORY_LOCATION for Inventory Offline Stocktake use",
	"ALTER TABLE INVENTORY_LOCATION ADD COLUMN Barcode varchar(10) after Code, Add UNIQUE KEY LocationIDWithBarcode (LocationLevelID,Barcode);"
);

$sql_eClassIP_update[] = array(
	"2010-02-25",
	"For Gamma Mail - for temporary mapping between mail and attachment",
	"CREATE TABLE IF NOT EXISTS MAIL_ATTACH_FILE_MAP (
	  FileID int(8) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  UploadTime datetime NOT NULL,
	  DraftMailUID int(8) default NULL,
	  OrgDraftMailUID int(8) default NULL,
	  OriginalFileName varchar(200) default NULL,
	  EncodeFileName int(8) default NULL,
	  PRIMARY KEY  (FileID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-02-25",
	"For Gamma Mail - trigger to synchronize FileID and EncodeFileName ",
	"CREATE TRIGGER UPDATE_ENCODE_FILE_NAME BEFORE INSERT ON MAIL_ATTACH_FILE_MAP
	  FOR EACH ROW 
	  BEGIN
		SET NEW.EncodeFileName = (SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE
		TABLE_SCHEMA = DATABASE() AND TABLE_NAME = 'MAIL_ATTACH_FILE_MAP');
	  END;"
);

$sql_eClassIP_update[] = array(
	"2010-02-26",
	"eDisciplinev12 - add LOCK column in table DISCIPLINE_MS_STUDENT_CONDUCT_FINAL for Munsang Conduct Grade Meeting",
	"ALTER TABLE DISCIPLINE_MS_STUDENT_CONDUCT_FINAL ADD COLUMN LockStatus tinyint(1) DEFAULT 0 NOT NULL;"
);

$sql_eClassIP_update[] = array(
	"2010-02-26",
	"eDisciplinev12 - add YearID (form) column in table DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST for Munsang Conduct Grade Absent List",
	"ALTER TABLE DISCIPLINE_MS_STUDENT_CONDUCT_ABSENTLIST ADD COLUMN YearID int(8) DEFAULT 0 NOT NULL AFTER RecordID;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Gamma Mail  - Add ImapUserEmail to store email address for gamma mail",
	"ALTER TABLE INTRANET_USER ADD ImapUserEmail varchar(100) ;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eLibrary - change field ContentTLF in INTRANET_ELIB_BOOK from mediumtext to LONGBLOB ",
	"ALTER TABLE INTRANET_ELIB_BOOK CHANGE ContentTLF ContentTLF LONGBLOB DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eLibrary - add field styleTLF in INTRANET_ELIB_BOOK to store the style sheet for Book content's TLF ",
	"alter table INTRANET_ELIB_BOOK add `StyleTLF` text default NULL"
);
/*	Disabled By Ronald - 20100419
$sql_eClassIP_update[] = array
(
	"2010-05-09",
	" iMail - Set Message Data type to mediumtext to store more characters",
	"ALTER TABLE INTRANET_CAMPUSMAIL MODIFY Message MEDIUMTEXT"
);
*/
$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eEnrolment - Add field to store different status of attendance record",
	"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE ADD COLUMN RecordStatus int(11) DEFAULT 1"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" eEnrolment - Add field to store different status of attendance record",
	"ALTER TABLE INTRANET_ENROL_EVENT_ATTENDANCE ADD COLUMN RecordStatus int(11) DEFAULT 1"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" iMail Gamma - Add field AutoReplyTxt to store Auto Reply Text",
	"ALTER TABLE INTRANET_IMAIL_PREFERENCE ADD COLUMN AutoReplyTxt TEXT DEFAULT NULL AFTER SkipCheckEmail"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"For Gamma Mail - create table MAIL_SHARED_MAILBOX to store shared mail",
	"CREATE TABLE IF NOT EXISTS MAIL_SHARED_MAILBOX (
	  MailBoxID int(11) NOT NULL auto_increment,
	  MailBoxName varchar(255) NOT NULL,
	  MailBoxPassword varchar(255) NOT NULL,
	  MailBoxType varchar(8) default NULL,
	  CreateDate datetime default NULL,
	  CreateBy int(8) default NULL,
	  LastModified datetime default NULL,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (MailBoxID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-04-29",
	"For Gamma Mail - create table MAIL_SHARED_MAILBOX_MEMBER to store shared mail member",
	"CREATE TABLE IF NOT EXISTS MAIL_SHARED_MAILBOX_MEMBER (
	  MailBoxMemberID int(11) NOT NULL auto_increment,
	  MailBoxID varchar(255) NOT NULL,
	  UserID varchar(255) NOT NULL,
	  CreateDate datetime default NULL,
	  CreateBy int(8) default NULL,
	  LastModified datetime default NULL,
	  ModifiedBy int(8) default NULL,
	  PRIMARY KEY  (MailBoxMemberID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REQ20100102 - Student eAttendance enhancement to support Entry/ Leave date for student",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_ENTRY_LEAVE_PERIOD (
	  RecordID int(11) NOT NULL auto_increment,
	  UserID int(11) default NULL,
	  PeriodStart date NOT NULL default '1970-01-01',
	  PeriodEnd date NOT NULL default '2099-12-31',
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModify datetime default NULL,
	  ModifyBy int(11) default NULL,
	  PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"eEnrolment - set club default attendance record as present",
	"Alter Table INTRANET_ENROL_GROUP_ATTENDANCE Change RecordStatus RecordStatus int(11) default 1"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"eEnrolment - set activity default attendance record as present",
	"Alter Table INTRANET_ENROL_EVENT_ATTENDANCE Change RecordStatus RecordStatus int(11) default 1"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add option 'all fields are required' for eCircular",
	"ALTER Table INTRANET_CIRCULAR add AllFieldsReq tinyint(1);"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add option 'all fields are required' for eNotice",
	"ALTER Table INTRANET_NOTICE add AllFieldsReq tinyint(1);"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add 'Remark' in CARD_STUDENT_PROFILE_RECORD_REASON",
	"ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD Remark mediumtext AFTER Reason;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add 'HandinLetter' in CARD_STUDENT_PROFILE_RECORD_REASON",
	"ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD HandinLetter int(11) AFTER AbsentSession;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"Add 'HandinLetter' in CARD_STUDENT_PROFILE_RECORD_REASON",
	"Alter Table CARD_STUDENT_PROFILE_RECORD_REASON
		Add Column Last_Discipline_RecordDate datetime,
		Add Column GM_RecordID_Str TEXT;
	"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" iMail Gamma - Add field AutoSaveInterval to save auto save draft interval time",
	"ALTER TABLE INTRANET_IMAIL_PREFERENCE ADD COLUMN AutoSaveInterval int(3) DEFAULT 0 AFTER AutoReplyTxt"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"update field 'NumberOfUnit' in PROFILE_STUDENT_MERIT to float(8,2)",
	"ALTER TABLE PROFILE_STUDENT_MERIT change NumberOfUnit NumberOfUnit float(8,2)"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	" iMail Gamma - Add field GmailMode to save gmail view preference",
	"ALTER TABLE INTRANET_IMAIL_PREFERENCE ADD COLUMN GmailMode int(3) DEFAULT 1 AFTER AutoSaveInterval"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"Add UNIQUE KEY for UserID and RecordDate",
	"alter IGNORE table CARD_STUDENT_OUTING add UNIQUE KEY UserIDAndRecordDate (UserID,RecordDate)"
);




$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - book license ",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_LICENSE (
	  BookLicenseID int(11) NOT NULL auto_increment,
	  BookID int(11) default NULL,
	  NumberOfCopy int(5) default 0,
	  CheckKey varchar(128) default NULL,
	  InputDate datetime default NULL,	  
	  PRIMARY KEY  (BookLicenseID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - book license enable/disable",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_ENABLED (	  
	  BookID int(11) default NULL,
	  UserID int(11) default NULL,	  
	  InputDate datetime default NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);


$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - book license for student",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_STUDENT (
	  BookStudentID int(11) NOT NULL auto_increment,
	  BookID int(11) default NULL,
	  BookLicenseID int(11) default NULL,
	  StudentID int(11) default NULL,
	  InputDate datetime default NULL,	  
	  PRIMARY KEY  (BookStudentID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Management Group",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_GROUP (
	  GroupID int(11) NOT NULL auto_increment,
	  GroupTitle varchar(128),
	  GroupDescription mediumtext,
	  RecordStatus tinyint(1) default 1,
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Management Group Member",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_GROUP_MEMBER (
	  GroupMemberID int(11) NOT NULL auto_increment,
	  GroupID int(11) default 0,
	  UserID int(11) default 0,
	  DateInput datetime,
	  PRIMARY KEY (GroupMemberID),
	  INDEX GroupID (GroupID),
	  INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"Drop UNIQUE KEY SBJ_CMP_CODEID (CODEID, CMP_CODEID) in ASSESSMENT_SUBJECT",
	"Alter Table ASSESSMENT_SUBJECT Drop Index SBJ_CMP_CODEID;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-29",
	"Add UNIQUE KEY SBJ_CMP_CODEID (CODEID, CMP_CODEID, RecordStatus) in ASSESSMENT_SUBJECT",
	"Alter Table ASSESSMENT_SUBJECT Add Unique Index SBJ_CMP_CODEID (CODEID, CMP_CODEID, RecordStatus);"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Location",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_LOCATION (
	  LocationID int(11) NOT NULL auto_increment,
	  LocationName varchar(255),
	  RecordStatus tinyint(1),
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (LocationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Category",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_CATEGORY (
	  CategoryID int(11) NOT NULL auto_increment,
	  Name varchar(255),
	  GroupID int(11) default 0,
	  RecordStatus tinyint(1),
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (CategoryID),
	  INDEX GroupID (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REPAIR SYSTEM - Records",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_RECORDS (
	  RecordID int(11) NOT NULL auto_increment,
	  CategoryID int(11),
	  LocationID int(11),
	  DetailsLocation text,
	  UserID int(11),
	  Title varchar(255),
	  Content text,
	  RecordStatus tinyint(1),
	  Remark text,
	  DateInput datetime,
	  DateModified datetime,
	  LastModifiedBy int(11),
	  PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"REQ20100205 - Forgot to bring card bad action should appears one and only one per day",
	"alter ignore table CARD_STUDENT_BAD_ACTION add UNIQUE KEY UniqueKey (StudentID,RecordDate,RecordType)"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eDisciplinev12 - add column PICID in table DISCIPLINE_DETENTION_STUDENT_SESSION",
	"ALTER TABLE DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN PICID varchar(255) AFTER Reason"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - new chapter table structure for Text Layout Framework",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_CHAPTER_TLF (
	  BookChapterID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  ChapterIndex int(11) default -1,
	  ShowNumber int(11) default -1,
	  StartPageIndex int(11) default -1,
	  EndPageIndex int(11) default -1,
	  Title varchar(255) default NULL,
	  ContentTLF longblob default NULL,
	  StyleTLF text default NULL,
	  DateModified datetime default NULL,	  
	  PRIMARY KEY  (BookChapterID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - add a new column AllowBooking in table INVENTORY_ITEM",
	"ALTER TABLE INVENTORY_ITEM ADD COLUMN AllowBooking int(11) default 1 AFTER PhotoLink"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - add a new column AllowBooking in table INVENTORY_LOCATION",
	"ALTER TABLE INVENTORY_LOCATION ADD COLUMN AllowBooking int(11) default 1 after Description"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD (
	  PeriodID int(11) NOT NULL auto_increment,
	  RecordDate date default NULL,
	  TimeSlotID int(11) default NULL,
	  StartTime time default NULL,
	  EndTime time default NULL,
	  RepeatType tinyint(4) default NULL,
	  RepeatValue text,
	  RelatedPeriodID int(11) default NULL,
	  RelatedItemID int(11) default NULL,
	  RelatedSubLocationID int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (PeriodID)
	) ENGINE=InnoDB AUTO_INCREMENT=426 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS (
	  RecordID int(11) NOT NULL auto_increment,
	  BookingID int(11) default NULL,
	  ItemID int(11) default NULL,
	  PIC int(8) default NULL,
	  ProcessDate date default NULL,
	  BookingStatus int(8) default 0,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB AUTO_INCREMENT=198 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FACILITIES_BOOKING_RULE (
	  RuleID int(11) NOT NULL auto_increment,
	  DaysBeforeUse int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RuleID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FOLLOWUP_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FOLLOWUP_GROUP (
	  GroupID int(11) NOT NULL auto_increment,
	  GroupName varchar(255) default NULL,
	  Description text,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_FOLLOWUP_GROUP_MEMBER (
	  UserID int(11) default NULL,
	  GroupID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  UNIQUE KEY UserIDWithGroupID (UserID,GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM (
	  ItemID int(11) NOT NULL auto_increment,
	  ItemType int(11) default NULL,
	  CategoryID int(11) default NULL,
	  Category2ID int(11) default NULL,
	  NameChi varchar(255) default NULL,
	  NameEng varchar(255) default NULL,
	  DescriptionChi mediumtext,
	  DescriptionEng mediumtext,
	  ItemCode varchar(100) default NULL,
	  Barcode varchar(100) default NULL,
	  LocationID int(11) default NULL,
	  BookingDayBeforehand tinyint(3) default NULL,
	  AllowBooking tinyint(3) default 1,
	  IncludedWhenBooking int(11) default 0,
	  ShowForReference int(11) default 0,
	  eInventoryItemID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemID),
	  UNIQUE KEY ItemCode (ItemCode),
	  KEY ItemType (ItemType),
	  KEY CategoryID (CategoryID),
	  KEY Category2ID (Category2ID),
	  KEY NameChi (NameChi),
	  KEY NameEng (NameEng),
	  KEY LocationID (LocationID)
	) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM_FOLLOWUP_GROUP (
	  ItemFollowUpGroupID int(11) NOT NULL auto_increment,
	  ItemID int(11) default NULL,
	  GroupID int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemFollowUpGroupID),
	  UNIQUE KEY ItemGroupMapping (ItemID,GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM_MANAGEMENT_GROUP (
	  ItemManagementGroupID int(11) NOT NULL auto_increment,
	  ItemID int(11) default NULL,
	  GroupID int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemManagementGroupID),
	  UNIQUE KEY ItemGroupMapping (ItemID,GroupID)
	) ENGINE=InnoDB AUTO_INCREMENT=240 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ITEM_USER_BOOKING_RULE (
	  ItemUserBookingRuleID int(11) NOT NULL auto_increment,
	  ItemID int(11) default NULL,
	  RuleID int(11) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (ItemUserBookingRuleID),
	  UNIQUE KEY ItemUserBookingRuleMapping (ItemID,RuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_BOOKING_RULE (
	  RuleID int(11) NOT NULL auto_increment,
	  DaysBeforeUse int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION (
	  LocationBookingRuleID int(11) NOT NULL auto_increment,
	  LocationID int(11) default NULL,
	  BookingRuleID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (LocationBookingRuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_BULK_ITEM_RELATION (
	  LocationID int(11) default NULL,
	  BulkItemID int(11) default NULL,
	  IncludedWhenBooking int(11) default 0,
	  ShowForReference int(11) default 0,
	  UNIQUE KEY LocationWithBulkItem (LocationID,BulkItemID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION (
	  LocationGroupID int(11) NOT NULL auto_increment,
	  LocationID int(11) default NULL,
	  GroupID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (LocationGroupID)
	) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION (
	  LocationUserBookingRuleID int(11) NOT NULL auto_increment,
	  LocationID int(11) default NULL,
	  UserBookingRuleID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (LocationUserBookingRuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_MANAGEMENT_GROUP",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_MANAGEMENT_GROUP (
	  GroupID int(11) NOT NULL auto_increment,
	  GroupName varchar(255) default NULL,
	  Description text,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (GroupID)
	) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_MANAGEMENT_GROUP_MEMBER (
	  UserID int(11) default NULL,
	  GroupID int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  UNIQUE KEY UserIDWithGroupID (UserID,GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_RECORD",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_RECORD (
	  BookingID int(11) NOT NULL auto_increment,
	  BookingRef text,
	  PeriodID int(11) default NULL,
	  Date date default NULL,
	  StartTime time default NULL,
	  EndTime time default NULL,
	  RelatedTo int(11) default NULL,
	  Remark text,
	  RequestedBy int(8) default NULL,
	  RequestDate date default NULL,
	  ResponsibleUID int(8) default NULL,
	  PIC int(8) default NULL,
	  ProcessDate date default NULL,
	  BookingStatus int(8) default 0,
	  InputBy int(8) default NULL,
	  ModifiedBy int(8) default NULL,
	  RecordStatus int(11) default NULL,
	  RecordType int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (BookingID)
	) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ROOM_BOOKING_DETAILS (
	  RecordID int(11) NOT NULL auto_increment,
	  BookingID int(11) default NULL,
	  RoomID int(11) default NULL,
	  PIC int(8) default NULL,
	  ProcessDate date default NULL,
	  BookingStatus int(8) default 0,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RecordID)
	) ENGINE=InnoDB AUTO_INCREMENT=323 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_USER_BOOKING_RULE",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_USER_BOOKING_RULE (
	  RuleID int(11) NOT NULL auto_increment,
	  RuleName varchar(255) default NULL,
	  NeedApproval tinyint(4) default NULL,
	  DaysBeforeUse int(11) default NULL,
	  CanBookForOthers tinyint(4) default NULL,
	  InputBy int(11) default NULL,
	  ModifiedBy int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY  (RuleID)
	) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER (
	  RuleID int(11) default NULL,
	  YearID int(11) default NULL,
	  UserType int(11) default NULL,
	  IsTeaching int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - add colume IsShown in table INTRANET_ELIB_BOOK_CHAPTER_TLF",
	"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER_TLF ADD COLUMN IsShown char(1) DEFAULT '1' AFTER ShowNumber;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - new table structure for book style",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_STYLE (
	  BookStyleID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  PageNum int(11) default 0,
	  BookX float(8,2) default 0,
	  BookY float(8,2) default 0,
	  BookWidth float(8,2) default 0,
	  BookHeight float(8,2) default 0,
	  PageX float(8,2) default 0,
	  PageY float(8,2) default 0,
	  PageWidth float(8,2) default 0,
	  PageHeight float(8,2) default 0,
	  DateModified datetime default NULL,	  
	  PRIMARY KEY (BookStyleID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-04-29",
	"eLibrary - new table structure for page media such as image and movie",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_PAGEMEDIA (
	  BookPageMediaID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  Page int(11) default -1,
	  MediaX float(8,2) default 0,
	  MediaY float(8,2) default 0,
	  MediaWidth float(8,2) default 0,
	  MediaHeight float(8,2) default 0,
	  FilePath text default NULL,	
	  DateModified datetime default NULL,	  
	  PRIMARY KEY (BookPageMediaID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-30",
	"update field 'Title' in INTRANET_ELIB_BOOK to TEXT to support longer title",
	"ALTER TABLE INTRANET_ELIB_BOOK change Title Title text Default NULL;"
);

$sql_eClassIP_update[] = array
(
	"2010-04-30",
	"eBooking - enlarge the BookingDayBeforehand value",
	"Alter Table INTRANET_EBOOKING_ITEM Change BookingDayBeforehand BookingDayBeforehand int(4);"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add approved date to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add ApprovedDate datetime;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add approved by to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add ApprovedBy int(11);"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add rejected date to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add RejectedDate datetime;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-10",
	"Discipline - add rejected by to conduct records",
	"alter table DISCIPLINE_ACCU_RECORD add RejectedBy int(11);"
);

$sql_eClassIP_update[] = array
(
	"2010-05-11",
	"eBooking - add Column - DeleteOtherRelatedIfReject",
	"ALTER TABLE INTRANET_EBOOKING_RECORD ADD COLUMN DeleteOtherRelatedIfReject int(11) DEFAULT 0 AFTER BookingStatus;"
);


$sql_eClassIP_update[] = array
(
	"2010-05-14",
	" eLibrary - add field IsChapterTLF in INTRANET_ELIB_BOOK to flag any content in Text Layout Framework xml format ",
	"alter table INTRANET_ELIB_BOOK add `IsTLF` char(2) default '0'"
);

$sql_eClassIP_update[] = array
(
	"2010-05-17",
	" eLibrary - add field ChapterID in INTRANET_ELIB_USER_BOOKMARK to point every bookmark to corresponding chapter (ID, not index). ",
	"alter table INTRANET_ELIB_USER_BOOKMARK add `ChapterID` int(11) default -1 AFTER USERID, Add INDEX ChapterID (ChapterID)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-17",
	" eLibrary - add field PageIndex in INTRANET_ELIB_USER_BOOKMARK to point every bookmark to corresponding page reference to index (not id). ",
	"alter table INTRANET_ELIB_USER_BOOKMARK add `PageIndex` int(11) default -1 AFTER PageID, ADD INDEX PageIndex (PageIndex)"
);

$sql_eClassIP_update[] = array(
	"2010-05-17",
	"eLibrary - new table structure for user's free drawings on page",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_USER_DRAWING (
	  DrawingID int(11) NOT NULL auto_increment,
	  BookID int(11) default -1,
	  UserID int(11) default -1,
	  ChapterID int(11) default -1,
 	  PageIndex int(11) default -1,
	  FileName varchar(255) default NULL,		  
	  DateModified datetime default NULL,	  
	  PRIMARY KEY (DrawingID),
	  INDEX BookID (BookID),
	  INDEX UserID (UserID),
	  INDEX ChapterID (ChapterID),
	  INDEX PageIndex (PageIndex)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-17",
	" eLibrary - add KEY in INTRANET_ELIB_BOOK_CHAPTER_TLF. ",
	"alter table INTRANET_ELIB_BOOK_CHAPTER_TLF ADD INDEX BookID (BookID)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-18",
	"Alter INTRANET_ARCHIVE_USER -> ClassNumber field to integer",
	"alter table INTRANET_ARCHIVE_USER change ClassNumber ClassNumber int(8) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-05-20",
	"CREATE TABLE INVENTORY_BARCODE_SETTING_LOG For log down any inventory barcode change",
	"CREATE TABLE IF NOT EXISTS INVENTORY_BARCODE_SETTING_LOG (
	  LogID int(11) NOT NULL auto_increment,
	  MaxLengthChanged int(11) default NULL,
	  FormatChanged int(11) default NULL,
	  RecordType int(11) default NULL,
	  RecordStatus int(11) default NULL,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column InternalUserID to INTRANET_USER",
	"alter table INTRANET_USER add column InternalUserID varchar(255)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column IDNumber to INTRANET_USER",
	"alter table INTRANET_USER add column IDNumber varchar(255)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column PlaceOfBirth to INTRANET_USER",
	"alter table INTRANET_USER add column PlaceOfBirth varchar(255)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column InputBy to INTRANET_USER",
	"alter table INTRANET_USER add column InputBy int(8)"
);

$sql_eClassIP_update[] = array
(
	"2010-05-24",
	"Add column ModifyBy to INTRANET_USER",
	"alter table INTRANET_USER add column ModifyBy int(8)"
	
);

$sql_eClassIP_update[] = array
(
	"2010-05-26",
	"Add column CompleteDate to REPAIR_SYSTEM_RECORDS",
	"alter table REPAIR_SYSTEM_RECORDS add column CompleteDate datetime"
	
);

$sql_eClassIP_update[] = array(
"2010-06-02",
"Add RFID field to INTRANET_USER for RFID attendance taking",
"alter table INTRANET_USER add RFID varchar(20) default NULL after CardID"
);


$sql_eClassIP_update[] = array(
	"2010-06-04",
	"MODULE LICENSE `INTRANET_MODULE`, store all the module",
	"CREATE TABLE IF NOT EXISTS `INTRANET_MODULE` (
		`ModuleID` int(11) NOT NULL auto_increment,
		`Code` varchar(255) default NULL,
		`Description` text,
		`DateInput` timestamp NULL default NULL,
		`DateModified` datetime NOT NULL default '0000-00-00 00:00:00',
		PRIMARY KEY  (`ModuleID`),
		KEY `ModuleID` (`ModuleID`),
		UNIQUE KEY `Code` (`Code`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2010-06-04",
	"MODULE LICENSE `INTRANET_MODULE_LICENSE`, store all the module license",
	"CREATE TABLE IF NOT EXISTS `INTRANET_MODULE_LICENSE` (
		`ModuleLicenseID` int(11) NOT NULL auto_increment,
		`ModuleID` int(11) default NULL,
		`NumberOfLicense` int(5) default '0',
		`CheckKey` varchar(128) default NULL,
		`InputDate` datetime default NULL,
		PRIMARY KEY  (`ModuleLicenseID`),
		KEY `ModuleID` (`ModuleID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2010-06-04",
	"MODULE LICENSE `INTRANET_MODULE_USER`, store all the module license user",
	"CREATE TABLE IF NOT EXISTS `INTRANET_MODULE_USER` (
		`ModuleStudentID` int(11) NOT NULL auto_increment,
		`ModuleID` int(11) default NULL,
		`UserID` int(8) default NULL,
		`InputDate` datetime default NULL,
		PRIMARY KEY  (`ModuleStudentID`),
		KEY `ModuleID` (`ModuleID`),
		KEY `UserID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-06-04",
	"CREATE A NEW TABLE - INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS For eBooking",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS (
		EventID int(10) NOT NULL auto_increment,
		BookingID int(11) NOT NULL,
		RepeatID int(8) default NULL,
		UserID int(8) NOT NULL default '0',
		CalID int(10) NOT NULL default '0',
		EventDate datetime NOT NULL default '0000-00-00 00:00:00',
		InputDate datetime default NULL,
		ModifiedDate datetime default NULL,
		Duration int(8) default NULL,
		IsImportant char(1) default '0',
		IsAllDay char(1) default NULL,
		Access char(1) default NULL,
		Title varchar(255) NOT NULL default '',
		Description mediumtext,
		Location varchar(255) default NULL,
		Url mediumtext,
		UID varchar(200) default NULL,
		ExtraIcalInfo mediumtext,
		PRIMARY KEY  (EventID),
		UNIQUE KEY UserID_2 (UserID,EventDate,InputDate,Title,CalID),
		KEY RepeatID (RepeatID),
		KEY UserID (UserID),
		KEY CalID (CalID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-06-04",
	"CREATE A NEW TABLE - INTRANET_EBOOKING_CALENDAR_EVENT_RELATION For eBooking",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_CALENDAR_EVENT_RELATION (
		BookingID int(11) default NULL,
		EventID int(10) default NULL,
		UNIQUE KEY BookingID_EventID (BookingID,EventID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
"2010-06-07",
"Add Attachment field to DISCIPLINE_ACCU_RECORD",
"alter table DISCIPLINE_ACCU_RECORD add Attachment varchar(255) AFTER YearTermID"
);

$sql_eClassIP_update[] = array(
"2010-06-07",
"Add Attachment field to DISCIPLINE_MERIT_RECORD",
"alter table DISCIPLINE_MERIT_RECORD add Attachment varchar(255) AFTER PICID"
);

$sql_eClassIP_update[] = array(
"2010-06-08",
"Add TemplateID field to DISCIPLINE_ACCU_CATEGORY",
"alter table DISCIPLINE_ACCU_CATEGORY add TemplateID int(11)"
);

$sql_eClassIP_update[] = array(
"2010-06-09",
"Add TemplateID field to DISCIPLINE_MERIT_ITEM_CATEGORY",
"alter table DISCIPLINE_MERIT_ITEM_CATEGORY add TemplateID int(11) AFTER ConversionPeriod"
);

$sql_eClassIP_update[] = array(
"2010-06-11",
"Add unique key DateStudentDayType to table CARD_STUDENT_PROFILE_RECORD_REASON (key should be exist since the table created, this script is to make sure all client consist have this key on that table - CRM Ref No.: 2010-0611-0856)",
"alter ignore table CARD_STUDENT_PROFILE_RECORD_REASON ADD UNIQUE KEY DateStudentDayType (RecordDate,StudentID,DayType,RecordType)"
);

$sql_eClassIP_update[] = array(
"2010-06-11",
"Add Gamma Quota to INTRANET_CAMPUSMAIL_USERQUOTA to store mail quota for gamma mail",
"ALTER TABLE INTRANET_CAMPUSMAIL_USERQUOTA ADD COLUMN GammaQuota int(11)"
);

$sql_ip_update[] = array(
	"2010-06-29",
	"Avoid duplicate record with same module code",
	"ALTER TABLE INTRANET_MODULE ADD UNIQUE (Code)"
);

	/*
if($plugin['ischoolbag'])
{
	
	$sql_ip_update[] = array(
		"2010-06-29",
		"INSERT INTRANET MODULE iSchoolBag",
		"INSERT INTO INTRANET_MODULE (Code,Description,DateInput,DateModified) VALUES ('isb','iSCHOOLBAG MODULE',now(),now())"
	);
}
*/
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_FAQ, store IES FAQ",
	"CREATE TABLE IF NOT EXISTS  `IES_FAQ` (
	  `QuestionID` int(11) NOT NULL auto_increment,
	  `Question` mediumtext,
	  `Answer` text,
	  `FromSchemeID` int(8) NOT NULL,
	  `AskedBy` int(11) NOT NULL,
	  `UserType` int(8) NOT NULL,
	  `AssignType` int(8) NOT NULL default '0',
	  `AssignToStudent` int(11) default NULL,
	  `RecordStatus` int(8) default NULL,
	  `AnswerDate` datetime default NULL,
	  `DateInput` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(11) default NULL,
	  PRIMARY KEY  (`QuestionID`),
	  KEY `QuestionID` (`QuestionID`),
	  KEY `AskedID` (`AskedBy`),
	  KEY `SchemeID` (`FromSchemeID`),
	  KEY `FromSchemeID` (`FromSchemeID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_FAQ_RELATIONSHIP, store IES FAQ RELATIONSHIP",
	"CREATE TABLE IF NOT EXISTS  `IES_FAQ_RELATIONSHIP` (
	  `RelationID` int(11) NOT NULL auto_increment,
	  `QuestionID` int(11) NOT NULL,
	  `SchemeID` int(8) NOT NULL,
	  PRIMARY KEY  (`RelationID`),
	  KEY `QuestionID` (`QuestionID`),
	  KEY `SchemeID` (`SchemeID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
		
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_FILE, store IES FILE",
	"CREATE TABLE IF NOT EXISTS  `IES_FILE` (
	  `FileID` int(11) NOT NULL auto_increment,
	  `FileName` varchar(255) default NULL,
	  `FolderPath` varchar(255) default NULL,
	  `FileHashName` varchar(255) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`FileID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_MARKING, store IES MARKING",
	"CREATE TABLE IF NOT EXISTS  `IES_MARKING` (
	  `MarkingID` int(11) NOT NULL auto_increment,
	  `AssignmentID` int(11) default NULL,
	  `AssignmentType` tinyint(1) default NULL,
	  `MarkCriteria` tinyint(1) default NULL,
	  `MarkerID` int(8) default NULL,
	  `Score` float default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`MarkingID`),
	  UNIQUE KEY `MarkerMark` (`AssignmentID`,`AssignmentType`,`MarkCriteria`,`MarkerID`),
	  KEY `AssignmentID` (`AssignmentID`),
	  KEY `MarkerID` (`MarkerID`),
	  KEY `TargetAssignment` (`AssignmentID`,`AssignmentType`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_QUESTION_HANDIN, store IES QUESTION HANDIN ",
	"CREATE TABLE IF NOT EXISTS  `IES_QUESTION_HANDIN` (
  `AnswerID` int(11) NOT NULL auto_increment,
  `StepID` int(11) NOT NULL,
  `UserID` int(8) NOT NULL,
  `QuestionType` varchar(255) default NULL,
  `QuestionCode` varchar(255) default NULL,
  `Answer` longtext,
  `AnswerActualValue` longtext COMMENT 'store the actual text (HTML) for checkbox , radio button (optional)',
  `AnswerExtra` longtext COMMENT 'store the extra input value for checkbox , radio button (optional)',
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`AnswerID`),
  UNIQUE KEY `StepUserQuestion` (`StepID`,`UserID`,`QuestionCode`),
  KEY `StepID` (`StepID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_REFLECT_NOTE, store IES REFLECT NOTE ",
	"CREATE TABLE IF NOT EXISTS  `IES_REFLECT_NOTE` (
  `NoteID` int(11) NOT NULL auto_increment,
  `SchemeStudentID` int(11) NOT NULL,
  `Content` mediumtext,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`NoteID`),
  KEY `SchemeStudentID` (`SchemeStudentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_SCHEME, store IES SCHEME ",
	"CREATE TABLE IF NOT EXISTS  `IES_SCHEME` (
  `SchemeID` int(8) NOT NULL auto_increment,
  `Title` varchar(255) default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`SchemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_SCHEME_STUDENT, store IES SCHEME STUDENT ",
	"CREATE TABLE IF NOT EXISTS  `IES_SCHEME_STUDENT` (
  `SchemeStudentID` int(11) NOT NULL auto_increment,
  `SchemeID` int(8) NOT NULL,
  `UserID` int(8) NOT NULL,
  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `InputBy` int(8) default NULL,
  PRIMARY KEY  (`SchemeStudentID`),
  UNIQUE KEY `SchemeStudent` (`SchemeID`,`UserID`),
  KEY `SchemeID` (`SchemeID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);	
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_SCHEME_TEACHER, store IES SCHEME TEACHER ",
	"CREATE TABLE IF NOT EXISTS  `IES_SCHEME_TEACHER` (
  `SchemeTeacherID` int(11) NOT NULL auto_increment,
  `SchemeID` int(8) NOT NULL,
  `UserID` int(8) NOT NULL,
  `TeacherType` int(8) default NULL,
  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `InputBy` int(8) default NULL,
  PRIMARY KEY  (`SchemeTeacherID`),
  UNIQUE KEY `SchemeTeacher` (`SchemeID`,`UserID`,`TeacherType`),
  KEY `SchemeID` (`SchemeID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE, store IES STAGE ",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE` (
  `StageID` int(11) NOT NULL auto_increment,
  `SchemeID` int(8) NOT NULL,
  `Title` varchar(255) default NULL,
  `Description` text,
  `Sequence` int(8) default NULL,
  `Deadline` date default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`StageID`),
  KEY `SchemeID` (`SchemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_HANDIN_BATCH, store IES STAGE HANDIN BATCH ",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_HANDIN_BATCH` (
  `BatchID` int(11) NOT NULL auto_increment,
  `StageID` int(11) NOT NULL,
  `UserID` int(8) NOT NULL,
  `BatchStatus` tinyint(1) default NULL,
  `SubmissionDate` timestamp NULL default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`BatchID`),
  KEY `StageID` (`StageID`),
  KEY `UserID` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_HANDIN_COMMENT, store IES STAGE HANDIN COMMENT ",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_HANDIN_COMMENT` (
  `BatchCommentID` int(11) NOT NULL auto_increment,
  `BatchID` int(11) NOT NULL,
  `Comment` longtext,
  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `InputBy` int(8) default NULL,
  PRIMARY KEY  (`BatchCommentID`),
  KEY `BatchID` (`BatchID`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8"
	);
	
		
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_HANDIN_FILE, store IES STAGE HANDIN FILET",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_HANDIN_FILE` (
  `FileID` int(11) NOT NULL auto_increment,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `BatchID` int(11) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STAGE_STUDENT, store IES STAGE STUDENT",
	"CREATE TABLE IF NOT EXISTS  `IES_STAGE_STUDENT` (
  `StageID` int(11) NOT NULL,
  `UserID` int(8) NOT NULL default '0',
  `Approval` tinyint(1) default NULL,
  `ApprovedBy` int(8) default NULL,
  `BatchHandinStatus` tinyint(1) default NULL,
  `Score` float default NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`StageID`,`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_STEP, store IES STEP",
	"CREATE TABLE IF NOT EXISTS  `IES_STEP` (
	  `StepID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `Title` varchar(255) default NULL,
	  `StepNo` int(8) default NULL,
	  `Status` tinyint(1) default '1',
	  `Sequence` int(8) default NULL,
	  `SaveToTask` tinyint(1) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`StepID`),
	  KEY `StepNo` (`StepNo`),
	  KEY `TaskID` (`TaskID`)
	  )ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK, store IES TASK",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK` (
	  `TaskID` int(11) NOT NULL auto_increment,
	  `StageID` int(11) NOT NULL,
	  `Title` varchar(255) default NULL,
	  `Code` varchar(255) default NULL,
	  `Sequence` int(8) default NULL,
	  `Approval` tinyint(1) default '0',
	  `Description` text,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`TaskID`),
	  UNIQUE KEY `StageTask` (`StageID`,`Code`),
	  KEY `StageID` (`StageID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_HANDIN, store IES_TASK_HANDIN",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_HANDIN` (
	  `AnswerID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Answer` longtext,
	  `QuestionType` varchar(255) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`AnswerID`),
	  UNIQUE KEY `TaskUser` (`TaskID`,`UserID`),
	  KEY `TaskID` (`TaskID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_HANDIN_COMMENT, store IES TASK HANDIN COMMENT",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_HANDIN_COMMENT` (
	  `SnapshotCommentID` int(11) NOT NULL auto_increment,
	  `SnapshotAnswerID` int(11) NOT NULL,
	  `Comment` longtext,
	  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
	  `InputBy` int(8) default NULL,
	  PRIMARY KEY  (`SnapshotCommentID`)
	) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_HANDIN_SNAPSHOT, store IES_TASK_HANDIN_SNAPSHOT",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_HANDIN_SNAPSHOT` (
	  `SnapshotAnswerID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Answer` longtext,
	  `AnswerTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `DateInput` timestamp NOT NULL default '0000-00-00 00:00:00',
	  `InputBy` int(8) default NULL,
	  PRIMARY KEY  (`SnapshotAnswerID`),
	  UNIQUE KEY `TaskUserSnapshot` (`TaskID`,`UserID`,`AnswerTime`),
	  KEY `TaskID` (`TaskID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_STUDENT, store IES TASK STUDENT",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_STUDENT` (
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Score` float default NULL,
	  `Approved` tinyint(1) default NULL,
	  `ApprovedBy` int(8) default NULL,
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`TaskID`,`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_TASK_STUDENT_DETAILS, store IES TASK STUDENT DETAILS",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_STUDENT_DETAILS` (
	  `RecordID` int(11) NOT NULL auto_increment,
	  `TaskID` int(11) NOT NULL,
	  `UserID` int(8) NOT NULL,
	  `Status` tinyint(1) default '0',
	  `DateInput` timestamp NULL default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`RecordID`),
	  UNIQUE KEY `TaskUser` (`TaskID`,`UserID`),
	  KEY `TaskID` (`TaskID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
		$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET, store IES WORKSHEET",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET` (
	  `WorksheetID` int(11) NOT NULL auto_increment,
	  `Title` varchar(255) default NULL,
	  `StartDate` datetime default NULL,
	  `EndDate` datetime default NULL,
	  `SchemeID` int(11) default NULL,
	  `RecordStatus` int(11) NOT NULL,
	  `DateInput` datetime default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`WorksheetID`),
	  KEY `SchemeID` (`SchemeID`),
	  KEY `RecordStatus` (`RecordStatus`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
			$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET_HANDIN_COMMENT, store IES WORKSHEET HANDIN COMMENT",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET_HANDIN_COMMENT` (
	  `CommentID` int(11) NOT NULL auto_increment,
	  `WorksheetID` int(11) NOT NULL,
	  `StudentID` int(8) default NULL,
	  `Comment` text,
	  `DateInput` datetime default NULL,
	  `InputBy` int(8) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`CommentID`),
	  KEY `WorksheetID` (`WorksheetID`),
	  KEY `StudentID` (`StudentID`),
	  CONSTRAINT `IES_WORKSHEET_HANDIN_COMMENT_ibfk_1` FOREIGN KEY (`WorksheetID`) REFERENCES `IES_WORKSHEET` (`WorksheetID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET_HANDIN_FILE, store IES_WORKSHEET_HANDIN_FILE",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET_HANDIN_FILE` (
	  `WorksheetFileID` int(11) NOT NULL auto_increment,
	  `WorksheetID` int(11) NOT NULL,
	  `FileName` varchar(255) default NULL,
	  `FolderPath` varchar(255) default NULL,
	  `FileHashName` varchar(255) default NULL,
	  `DateInput` datetime default NULL,
	  `StudentID` int(8) default NULL,
	  `DateModified` datetime default NULL,
	  `ModifyBy` int(8) default NULL,
	  PRIMARY KEY  (`WorksheetFileID`),
	  KEY `WorksheetID` (`WorksheetID`),
	  CONSTRAINT `IES_WORKSHEET_HANDIN_FILE_ibfk_1` FOREIGN KEY (`WorksheetID`) REFERENCES `IES_WORKSHEET` (`WorksheetID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
	"2010-07-05",
	"IES IES_WORKSHEET_TEACHER_FILE, store IES_WORKSHEET_TEACHER_FILE",
	"CREATE TABLE IF NOT EXISTS  `IES_WORKSHEET_TEACHER_FILE` (
  `WorksheetFileID` int(11) NOT NULL auto_increment,
  `WorksheetID` int(11) NOT NULL,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `DateInput` datetime default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` datetime default NULL,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`WorksheetFileID`),
  KEY `WorksheetID` (`WorksheetID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
		$sql_eClassIP_update[] = array(
	"2010-07-07",
	"IP file management",
	"CREATE TABLE IF NOT EXISTS  `IPORTAL_FILE` (
  `FileID` int(11) NOT NULL auto_increment,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `UserID` int(8) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  `Size` int(11) default NULL,
  `SizeMax` int(64) default NULL,
  PRIMARY KEY  (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);

$sql_eClassIP_update[] = array(
	"2010-07-07",
	"iMail Gamma - Table for tracking internal mail receipt status",
	"CREATE TABLE IF NOT EXISTS INTRANET_IMAIL_RECEIPT_STATUS(
	 RecordID int(11) NOT NULL auto_increment,
	 MailID varchar(255) NOT NULL,
	 UserID int(11) NOT NULL,
	 Status int(11),
	 TrashDate datetime,
	 RecordType int(11),
	 RecordStatus int(11),
	 DateInput datetime,
	 DateModified datetime,
	 PRIMARY KEY (RecordID),
	 UNIQUE (MailID,UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-07-07",
	"Class History - Add YearClassID to PROFILE_CLASS_HISTORY",
	"ALTER TABLE PROFILE_CLASS_HISTORY ADD COLUMN YearClassID int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2010-07-07",
	"Class History - Add AcademicYearID to PROFILE_CLASS_HISTORY",
	"ALTER TABLE PROFILE_CLASS_HISTORY ADD COLUMN AcademicYearID int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Change the ClassName field from varchar(20) to varchar(255) to match with table YEAR_CLASS",
	"ALTER TABLE PROFILE_CLASS_HISTORY Change ClassName ClassName varchar(255) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Change the AcademicYear field from varchar(20) to varchar(50) to match with table ACADEMIC_YEAR",
	"ALTER TABLE PROFILE_CLASS_HISTORY Change AcademicYear AcademicYear varchar(50) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Change the ClassNumber field from varchar(20) to int(8) to match with table INTRANET_USER",
	"ALTER TABLE PROFILE_CLASS_HISTORY Change ClassNumber ClassNumber int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-08",
	"Class History - Set Unique Key of Student, Academic Year, Class and ClassNumber",
	"Alter Table PROFILE_CLASS_HISTORY Add Unique Key StudentYearClass (UserID, AcademicYearID, YearClassID, ClassNumber)"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Class History - Add Index to YearClassID of PROFILE_CLASS_HISTORY",
	"Alter Table PROFILE_CLASS_HISTORY Add Index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Class History - Add Index to AcademicYearID of PROFILE_CLASS_HISTORY",
	"Alter Table PROFILE_CLASS_HISTORY Add Index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Add Day Type for CARD_STUDENT_OUTING",
	"alter table CARD_STUDENT_OUTING add DayType int(2) default NULL after RecordDate"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Drop CARD_STUDENT_OUTING unique",
	"alter table CARD_STUDENT_OUTING drop key UserIDAndRecordDate"
);

$sql_eClassIP_update[] = array(
	"2010-07-12",
	"Add Unique Key to CARD_STUDENT_OUTING (UserID,RecordDate,DayType)",
	"alter table CARD_STUDENT_OUTING add UNIQUE KEY UserIDRecordDateDayType (UserID,RecordDate,DayType)"
);

$sql_eClassIP_update[] = array(
	"2010-07-13",
	"eEnrol - Add table to log the copy clubs history",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_COPY_CLUB_LOG (
		RecordID int(11) NOT NULL auto_increment,
		UserID int(11),
		FromAcademicYearID int(11),
		ToAcadedmicYearID int(11),
		FromTermID text,
		ToTermID text,
		FromEnrolGroupID int(11),
		ToEnrolGroupID int(11),
		CopyMember tinyint(3),
		CopyActivity tinyint(3),
		CopyDateTime datetime,
		PRIMARY KEY (RecordID),
		KEY UserID (UserID),
		KEY FromEnrolGroupID (FromEnrolGroupID),
		KEY ToEnrolGroupID (ToEnrolGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Academic Year field for activities",
	"Alter table INTRANET_ENROL_EVENTINFO Add AcademicYearID int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Index AcademicYearID To INTRANET_ENROL_EVENTINFO",
	"Alter table INTRANET_ENROL_EVENTINFO Add Index AcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Index EnrolGroupID To INTRANET_ENROL_EVENTINFO",
	"Alter table INTRANET_ENROL_EVENTINFO Add Index EnrolGroupID (EnrolGroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Add Index OLE_ProgramID To INTRANET_ENROL_EVENTINFO",
	"Alter table INTRANET_ENROL_EVENTINFO Add Index OLE_ProgramID (OLE_ProgramID)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"eEnrol - Change the Semester field from varchar(255) to int(11) in INTRANET_ENROL_GROUPINFO",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO Change Semester Semester int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"iPortal File - Remove field 'SizeMax' in IPORTAL_FILE",
	"ALTER TABLE IPORTAL_FILE DROP COLUMN SIZEMAX"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"iPortal File - Add field 'Code' in IPORTAL_FILE",
	"ALTER TABLE IPORTAL_FILE ADD Code VARCHAR(255)"
);

$sql_eClassIP_update[] = array(
	"2010-07-14",
	"iPortal File - Add a table IPORTAL_FILE_MODULE",
	"CREATE TABLE IF NOT EXISTS `IPORTAL_FILE_MODULE` (
  `ModuleID` int(11) NOT NULL auto_increment,
  `Code` varchar(255) default NULL,
  `ModuleMaxSize` int(65) default NULL,
  `DateInput` timestamp NULL default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `Description` text,
  PRIMARY KEY  (`ModuleID`),
  UNIQUE KEY `Code` (`Code`),
  KEY `ModuleID` (`ModuleID`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-07-15",
	"iPortal File - Add a new module",
	"INSERT INTO IPORTAL_FILE_MODULE SET CODE = 'ies', DATEINPUT = NOW(),DATEMODIFIED = NOW()"
);

$sql_eClassIP_update[] = array(
"2010-07-15",
"improve icalevent performance",
"alter table INTRANET_EVENT add INDEX RecordType (RecordType);"
);

$sql_eClassIP_update[] = array(
"2010-07-15",
"improve icalevent performance",
"alter table INTRANET_EVENT add INDEX RecordStatus (RecordStatus);"
);

$sql_eClassIP_update[] = array(
"2010-07-15",
"improve icalevent performance",
"alter table INTRANET_GROUPEVENT add INDEX EventID (EventID);"
);

$sql_eClassIP_update[] = array(
	"2010-07-16",
	"IES IES_COMMENT, store IES COMMENT BANK",
	"CREATE TABLE  IF NOT EXISTS `IES_COMMENT` (
	  `CommentID` int(11) NOT NULL auto_increment,
	  `Comment` mediumtext,
	  `DateInput` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  `ModifyBy` int(11) default NULL,
	  PRIMARY KEY  (`CommentID`),
	  KEY `InputBy` (`InputBy`),
	  KEY `ModifyBy` (`ModifyBy`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2010-07-18",
"Student Registry System - create table for student info",
"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_STUDENT
(
	SRS_ID int(8) NOT NULL auto_increment,
	UserID int(8) NOT NULL,
	STUD_ID varchar(20) default NULL,
	CODE varchar(9) default NULL,
	NAME_P varchar(255) default NULL,
	ADMISSION_DATE date default NULL,
	S_CODE int(8) default NULL,
	B_PLACE int(8) default NULL,
	BIRTH_CERT_NO varchar(10) default NULL,
	ID_NO varchar(20) default NULL,
	ID_TYPE int(8) default NULL,
	I_PLACE int(8) default NULL,
	I_DATE date default NULL,
	V_DATE date default NULL,
	S6_TYPE tinyint(1) default NULL,
	S6_IDATE date default NULL,
	S6_VDATE date default NULL,
	RACE varchar(20) default NULL,
	ANCESTRAL_HOME varchar(20) default NULL,
	NATION int(8) default NULL,
	ORIGIN varchar(20) default NULL,
	RELIGION int(8) default NULL,
	PAYMENT_TYPE int(8) default NULL,
	FAMILY_LANG int(8) default NULL,
	LODGING int(8) default NULL,
	ADDRESS1 varchar(255) default NULL,
	ADDRESS2 varchar(255) default NULL,
	ADDRESS3 varchar(255) default NULL,
	ADDRESS4 varchar(255) default NULL,
	ADDRESS5 varchar(255) default NULL,
	ADDRESS6 varchar(255) default NULL,
	PRI_SCHOOL varchar(255) default NULL,
	R_AREA char(1) default NULL,
	RA_DESC varchar(255) default NULL,
	AREA char(1) default NULL,
	ROAD varchar(255) default NULL,
	ADDRESS varchar(255) default NULL,
	EMAIL varchar(255) default NULL,
	DateInput datetime default NULL,
	InputBy int(8) default NULL,
	DateModified date default NULL,
	ModifyBy int(8) default NULL,
	PRIMARY KEY  (SRS_ID),
	KEY UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2010-07-18",
"Student Registry System - create table for PG info",
"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_PG
(
	SRPG_ID int(8) NOT NULL auto_increment,
	StudentID int(8) NOT NULL,
	PG_TYPE char(1) NOT NULL,
	NAME_C varchar(255),
	NAME_E varchar(255),
	NAME_P varchar(255),
	G_GENDER char(1) NOT NULL,
	GUARD char(1) NOT NULL,
	G_RELATION varchar(255),
	PROF varchar(255),
	JOB_NATURE varchar(255),
	JOB_TITLE varchar(255),
	MARITAL_STATUS int(8) NOT NULL,
	TEL varchar(40),
	MOBILE varchar(40),
	OFFICE_TEL varchar(40),
	LIVE_SAME tinyint(1),
	G_AREA char(1),
	G_ROAD varchar(255),
	G_ADDRESS varchar(255),
	POSTAL_ADDR1 varchar(255),
	POSTAL_ADDR2 varchar(255),
	POSTAL_ADDR3 varchar(255),
	POSTAL_ADDR4 varchar(255),
	POSTAL_ADDR5 varchar(255),
	POSTAL_ADDR6 varchar(255),
	POST_CODE varchar(255),
	EMAIL varchar(255),
	EDU_LEVEL varchar(255),
	DateInput datetime default NULL,
	InputBy int(8) default NULL,
	DateModified date default NULL,
	ModifyBy int(8) default NULL,
	PRIMARY KEY  (SRPG_ID),
	KEY StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2010-07-29",
"Student Registry System - Record UserID of the user who modify the access right settings",
"ALTER TABLE ACCESS_RIGHT_GROUP_SETTING add LastModifiedBy int(11);"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Student Profile - Add index for UserID",
	"Alter Table PROFILE_STUDENT_ACTIVITY Add INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Add Field to store Subject Component Mapping to Subject Group",
	"Alter Table SUBJECT_TERM Add SubjectComponentID int(11) Default Null After SubjectID;"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Add Index to Subject Component Mapping to Subject Group",
	"Alter Table SUBJECT_TERM Add Index SubjectComponentID (SubjectComponentID);"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Drop Unique Key SubjectTerm (SubjectID, YearTermID, SubjectGroupID)",
	"Alter Table SUBJECT_TERM Drop Index SubjectTerm;"
);

$sql_eClassIP_update[] = array(
	"2010-08-02",
	"Subject Group - Add Unique Key SubjectTerm (SubjectID, SubjectComponentID, YearTermID, SubjectGroupID)",
	"Alter Table SUBJECT_TERM Add Unique Index SubjectTerm (SubjectID, SubjectComponentID, YearTermID, SubjectGroupID);"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"Subject Group - Temp Import Table",
	"CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_GROUP_IMPORT
	 (
			TempID int(11) NOT NULL auto_increment,
			UserID int(11),
			RowNumber int(11),
			SubjectCode varchar(20),
			SubjectGroupCode varchar(50),
			SubjectGroupTitleEn varchar(255),
			SubjectGroupTitleCh varchar(255),
			ApplicableForm varchar(255),
			CreateEclass varchar(10),
			DateInput datetime,
			PRIMARY KEY (TempID)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"Subject Group - Add Subject Component Code in the Temp Import Table",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add SubjectComponentCode varchar(20) Default Null After SubjectCode;"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"Intranet Module - Add a field UsageStatus to the table INTRANET_MODULE_USER",
	"ALTER TABLE INTRANET_MODULE_USER ADD UsageStatus int(1) default 0;"
);

$sql_eClassIP_update[] = array(
	"2010-08-03",
	"INSERT INTRANET MODULE IES",
	"INSERT IGNORE INTO INTRANET_MODULE (Code,Description,DateInput,DateModified) VALUES ('ies','IES MODULE',now(),now())"
);


$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Add resize setting to eCommunity photo folder",
	"alter table INTRANET_FILE_FOLDER add PhotoResize tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Add resize setting (x-demension) to eCommunity photo folder",
	"alter table INTRANET_FILE_FOLDER add DemensionX int(1);"
);

$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Add resize setting (y-demension) to eCommunity photo folder",
	"alter table INTRANET_FILE_FOLDER add DemensionY int(1);"
);

$sql_eClassIP_update[] = array(
	"2010-08-04",
	"Student Registry Status (Malaysia)",
	"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_STATUS
	 (
		RecordID int(8) NOT NULL auto_increment,
		UserID int(8) NOT NULL,
		ApplyDate date,
		Reason int(4) NOT NULL,
		ReasonOthers varchar(100),
		NoticeID int(11),
		RecordStatus int(1) NOT NULL,
		IsCurrent int(1) NOT NULL default 0,
		DateInput datetime,
		InputBy int(11),
		DateModified datetime,
		ModifyBy int(11),
		PRIMARY KEY (RecordID),
		INDEX UserID (UserID),
		INDEX RecordStatus (RecordStatus),
		INDEX IsCurrent (IsCurrent)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-09",
	"eEnrol - Add CopyFromEnrolGroupID in INTRANET_ENROL_GROUPINFO",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO Add CopyFromEnrolGroupID int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-09",
	"Group - Add CopyFromGroupID in INTRANET_GROUP",
	"ALTER TABLE INTRANET_GROUP Add CopyFromGroupID int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-10",
	"IES - Add versioning for scheme",
	"ALTER TABLE IES_SCHEME ADD Version BIGINT(13) DEFAULT NULL;"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field HeaderLeft in INTRANET_ELIB_BOOK_STYLE to store left page's header in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `HeaderLeft` text default NULL after PageHeight"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field HeaderRight in INTRANET_ELIB_BOOK_STYLE to store right page's header in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `HeaderRight` text default NULL after HeaderLeft"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field FooterLeft in INTRANET_ELIB_BOOK_STYLE to store left page's footer in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `FooterLeft` text default NULL after HeaderRight"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	" eLibrary - add field FooterRight in INTRANET_ELIB_BOOK_STYLE to store right page's footer in TLF format",
	"alter table INTRANET_ELIB_BOOK_STYLE add `FooterRight` text default NULL after FooterLeft"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	"student reg - update field from code table",
	"alter table STUDENT_REGISTRY_STUDENT change ID_TYPE ID_TYPE varchar(5) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	"student reg - update field from code table",
	"alter table STUDENT_REGISTRY_STUDENT change NATION NATION varchar(2) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-08-10",
	"student reg - update field from code table",
	"alter table STUDENT_REGISTRY_STUDENT change S_CODE S_CODE varchar(3) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-08-11",
	" eLibrary - add field PageOneIndex in INTRANET_ELIB_BOOK_STYLE to store the page index of real Page No.1",
	"alter table INTRANET_ELIB_BOOK_STYLE add `PageOneIndex` int(6) default 1 after FooterRight"
);

$sql_eClassIP_update[] = array(
	"2010-08-13",
	"School Settings > Timetable - Import Lesson Temp Table",
	"CREATE TABLE IF NOT EXISTS TEMP_TIMETABLE_LESSON_IMPORT
	 (
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		TimetableID int(11),
		RowNumber int(11),
		Day tinyint(3),
		TimeSlot tinyint(3),
		TimeSlotID int(11),
		SubjectGroupCode varchar(50),
		SubjectGroupID int(11),
		BuildingCode varchar(10),
		FloorCode varchar(10),
		RoomCode varchar(10),
		LocationID int(11),
		OthersLocation varchar(255),
		DateInput datetime,
		PRIMARY KEY (TempID)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-13",
	"School Settings > Timetable - Import Lesson Temp Table",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP (
	     RoomAllocationID int(11) NOT NULL auto_increment,
	     TimetableID int(11) NOT NULL,
	     TimeSlotID int(11) NOT NULL,
	     Day tinyint(2) NOT NULL,
	     LocationID int(11) default NULL,
	     OthersLocation varchar(255) default NULL,
	     SubjectGroupID int(8) NOT NULL,
		 ImportUserID int(11) NOT NULL,
	     DateInput datetime default NULL,
	     DateModified datetime default NULL,
	     LastModifiedBy int(11) default NULL,
	     PRIMARY KEY (RoomAllocationID),
	     KEY TimetableID (TimetableID),
	     KEY TimeSlotID (TimeSlotID),
	     KEY Day (Day),
	     KEY LocationID (LocationID),
	     KEY SubjectGroupID (SubjectGroupID),
		 KEY ImportUserID (ImportUserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-08-17",
	"Student Registry System - add 1 more column to store 'Brother & Sister in school'",
	"alter table STUDENT_REGISTRY_STUDENT add column BRO_SIS_IN_SCHOOL varchar(255) AFTER ADDRESS6"
);

$sql_eClassIP_update[] = array
(
	"2010-08-18",
	" eLibrary - add field ChapterID in INTRANET_ELIB_USER_FORMAT to store the Chapter ID",
	"alter table INTRANET_ELIB_USER_FORMAT add `ChapterID` int(11) default -1 after UserID, Add INDEX ChapterID (ChapterID)"
);

$sql_eClassIP_update[] = array
(
	"2010-08-19",
	"iMail Gamma - add table for storing temporary mail inline images when composing mail",
	"CREATE TABLE IF NOT EXISTS MAIL_INLINE_IMAGE_MAP(
	 	FileID int(8) NOT NULL auto_increment,
	 	UserID int(11) NOT NULL,
	 	UploadTime datetime NOT NULL,
	 	OriginalImageFileName varchar(255) DEFAULT NULL,
	 	ContentID varchar(100) DEFAULT NULL,
	 	PRIMARY KEY (FileID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-08-19",
	"eEnrol - add field to store Active Member Percentage in Club",
	"alter table INTRANET_ENROL_GROUPINFO add ActiveMemberPercentage float(5,2) default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-19",
	"eDiscipline - FollowUp Action [customization]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_FOLLOWUP_ACTION (
	  ActionID int(8) NOT NULL auto_increment,
	  Title varchar(255) default NULL,
	  Sequence int(2) default NULL,
	  RecordStatus int(1) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (ActionID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-20",
	"eDisciplinev12 - add followup action to conduct settings",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_ITEM ADD ActionID int(11);"
);

$sql_eClassIP_update[] = array(
	"2010-08-20",
	"eDisciplinev12 - add followup action data to GM record",
	"alter table DISCIPLINE_ACCU_RECORD add ActionID int(11), add ActionTimes int(11), add ActionCompleted tinyint(1);"
);

$sql_eClassIP_update[] = array(
	"2010-08-23",
	"eDisciplinev12 - add last followup date to GM record",
	"alter table DISCIPLINE_ACCU_RECORD add LastFollowUpDate datetime, add LastFollowUpBy int(11);"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eBooking - add column GroupColor to INTRANET_EBOOKING_FOLLOWUP_GROUP",
	"ALTER TABLE INTRANET_EBOOKING_FOLLOWUP_GROUP ADD Column GroupColor text AFTER Description;"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eBooking - add column IsReserve to INTRANET_EBOOKING_RECORD",
	"ALTER TABLE INTRANET_EBOOKING_RECORD ADD Column IsReserve int(8) AFTER DeleteOtherRelatedIfReject;"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eBooking - create table INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_LOCATION_FOLLOWUP_GROUP_RELATION (
		LocationFollowUpGroupID int(11) NOT NULL auto_increment,
  		LocationID int(11) default NULL,
  		GroupID int(11) default NULL,
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY  (LocationFollowUpGroupID),
		UNIQUE KEY LocationGroupMapping (LocationID,GroupID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eSports (Sport Day) - Add AbsentReason for Absent - Waived ",
	"ALTER TABLE SPORTS_LANE_ARRANGEMENT ADD COLUMN AbsentReason VARCHAR(128) DEFAULT NULL AFTER Rank"
);

$sql_eClassIP_update[] = array(
	"2010-08-24",
	"eSports (Swimminggala)- Add AbsentReason for Absent - Waived ",
	"ALTER TABLE SWIMMINGGALA_LANE_ARRANGEMENT ADD COLUMN AbsentReason VARCHAR(128) DEFAULT NULL AFTER Rank"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"eDisciplinev12 - Munsang Generate reference number when creating AP/GM records",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_REFERENCE_NUMBER (
	ReferenceID INT(11) NOT NULL auto_increment,
	AcademicYearID INT(11) not null,
	YearTermID INT(11) not null,
	ReferenceToTable varchar(200),
	RecordID INT(11),
	DateInput DATETIME,
	InputBy int(11),
	PRIMARY KEY (ReferenceID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Batch Edit Subject Group Info Temp Table",
	"CREATE TABLE IF NOT EXISTS TEMP_SUBJECT_GROUP_QUICK_EDIT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		SubjectGroupCode varchar(50),
		NewSubjectGroupCode varchar(50),
		NewTitleEn varchar(255),
		NewTitleCh varchar(255),
		SubjectGroupID int(11),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Form Info in the Batch Edit Subject Group Info Temp Table",
	"ALTER TABLE TEMP_SUBJECT_GROUP_QUICK_EDIT ADD COLUMN ApplicableFormList varchar(255) Default Null After SubjectGroupID"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Form Info in the Batch Edit Subject Group Info Temp Table",
	"ALTER TABLE TEMP_SUBJECT_GROUP_QUICK_EDIT ADD COLUMN ApplicableFormIDList varchar(255) Default Null After ApplicableFormList"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Subject Group Teacher Info in the Temp Import Table",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add TeacherList varchar(255) Default Null After CreateEclass;"
);

$sql_eClassIP_update[] = array(
	"2010-08-26",
	"Subject Group - Add Subject Group Teacher Info in the Temp Import Table",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add TeacherIDList varchar(255) Default Null After TeacherList;"
);

$sql_eClassIP_update[] = array(
	"2010-08-31",
	"eDisciplinev12 - Munsang Generate reference number when creating AP/GM records (Add column)",
	"ALTER TABLE DISCIPLINE_REFERENCE_NUMBER ADD COLUMN ReferenceNo int(11) not null AFTER YearTermID"
);

$sql_eClassIP_update[] = array(
	"2010-08-31",
	"Staff attendance v2 - add session time field to admin table",
	"ALTER TABLE CARD_STAFF_ATTENDANCE_ADMIN ADD COLUMN Type tinyint default NULL"
);

$sql_eClassIP_update[] = array(
	"2010-08-31",
	"Staff attendance v2 - add session time field to admin table",
	"UPDATE CARD_STAFF_ATTENDANCE_ADMIN SET Type=0 WHERE Username = 'admin'"
);

$sql_eClassIP_update[] = array(
	"2010-09-03",
	"add a field to store user's latest lang selection for IP",
	"ALTER TABLE INTRANET_USER ADD LastLang varchar(2)"
);


$sql_eClassIP_update[] = array(
	"2010-09-06",
	"Add DISCIPLINE_AP_APPROVAL_GROUP for AP Approval Group",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_AP_APPROVAL_GROUP
	(
		GroupID INT(11) not null auto_increment,
		Name varchar(200),
		Description text,
		RecordStatus int(11) default 1,
		DateInput datetime, 
		InputBy int(11),
		DateModified datetime,
		ModifyBy int(11),
		PRIMARY KEY (GroupID),
		INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-09-06",
	"Add DISCIPLINE_AP_APPROVAL_GROUP_MEMBER for AP Approval Group Member",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_AP_APPROVAL_GROUP_MEMBER
	(
		GroupMemberID INT(11) not null auto_increment,
		GroupID int(11) not null,
		UserID int(11) not null,
		DateInput datetime,
		PRIMARY KEY (GroupMemberID),
		INDEX GroupID (GroupID),
		INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY` (
	  `SurveyID` int(11) NOT NULL auto_increment,
	  `Title` varchar(255) NOT NULL,
	  `Description` mediumtext,
	  `Question` mediumtext,
	  `DateStart` datetime default NULL,
	  `DateEnd` datetime default NULL,
	  `SurveyType` tinyint(4) default NULL,
	  `SurveyStatus` tinyint(4) default NULL,
	  `InputBy` int(11) NOT NULL,
	  `DateInput` timestamp NULL default NULL,
	  `ModifiedBy` int(11) NOT NULL,
	  `DateModified` timestamp NULL default NULL,
	  PRIMARY KEY  (`SurveyID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_ANSWER",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY_ANSWER` (
	  `AnswerID` int(11) NOT NULL auto_increment,
	  `SurveyID` int(11) NOT NULL,
	  `Respondent` varchar(255) default NULL,
	  `Answer` mediumtext,
	  `DateInput` datetime default NULL,
	  `DateModified` datetime default NULL,
	  `Type` int(11) NOT NULL,
	  `status` int(11) default NULL,
	  PRIMARY KEY  (`AnswerID`),
	  UNIQUE KEY `AnswerUser` (`SurveyID`,`Respondent`),
	  KEY `SurveyID` (`SurveyID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_EMAIL",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY_EMAIL` (
		  `SurveyEmailID` int(11) NOT NULL auto_increment,
		  `SurveyID` int(11) NOT NULL,
		  `DateInput` timestamp NULL default NULL,
		  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		  `Email` varchar(255) default NULL,
		  `Status` int(11) default NULL,
		  `EmailDisplayName` mediumtext,
		  PRIMARY KEY  (`SurveyEmailID`),
		  UNIQUE KEY `EmailUser` (`SurveyID`,`Email`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_EMAILCONTENT",
	"CREATE TABLE IF NOT EXISTS `IES_SURVEY_EMAILCONTENT` (
	  `SurveyEmailCONTENTID` int(11) NOT NULL auto_increment,
	  `SurveyID` int(11) NOT NULL,
	  `Title` mediumtext,
	  `Content` longblob,
	  `DateInput` timestamp NULL default NULL,
	  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  PRIMARY KEY  (`SurveyEmailCONTENTID`),
	  UNIQUE KEY `SurveyID` (`SurveyID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);


$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : ADD FOR IES_SURVEY_MAPPING",
			"CREATE TABLE IF NOT EXISTS `IES_SURVEY_MAPPING` (
		  `SurveyMappingID` int(11) NOT NULL auto_increment,
		  `SurveyID` int(11) NOT NULL,
		  `UserID` int(8) NOT NULL,
		  `XMAPPING` char(11) default NULL,
		  `YMAPPING` char(11) default NULL,
		  `XTitle` varchar(255) default NULL,
		  `YTitle` varchar(255) default NULL,
		  `DateInput` timestamp NULL default NULL,
		  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		  `MappingTitle` varchar(255) NOT NULL,
		  PRIMARY KEY  (`SurveyMappingID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : Add field InstantEdit to IES_TASK",
	"ALTER TABLE IES_TASK ADD COLUMN `InstantEdit` tinyint(1)  default 1 AFTER Description;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : Add field Status to IES_TASK_HANDIN_COMMENT",
	"ALTER TABLE IES_TASK_HANDIN_COMMENT ADD COLUMN `Status` int(8) default '0' AFTER InputBy;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-09",
	"IES : Add field QuestionType to IES_TASK_HANDIN_SNAPSHOT",
	"ALTER TABLE IES_TASK_HANDIN_SNAPSHOT ADD COLUMN `QuestionType` varchar(255) default NULL AFTER Answer;"	
);
/*
$sql_eClassIP_update[] = array(
	"2010-09-09",
	"eDisciplinev12 - Add ApprovalGroupID in DISCIPLINE_MERIT_ITEM",
	"ALTER TABLE DISCIPLINE_MERIT_ITEM ADD COLUMN ApprovalGroupID INT(11) NOT NULL AFTER DetentionMinutes, ADD INDEX ApprovalGroupID (ApprovalGroupID)"	
);
*/
$sql_eClassIP_update[] = array(
	"2010-09-10",
	"Student Attendance : Change session data type from int(11) to float",
	"ALTER TABLE CARD_STUDENT_STATUS_SESSION_COUNT MODIFY LateSession FLOAT DEFAULT NULL,MODIFY RequestLeaveSession FLOAT DEFAULT NULL,MODIFY PlayTruantSession FLOAT DEFAULT NULL,MODIFY OfficalLeaveSession FLOAT DEFAULT NULL "
);

$sql_eClassIP_update[] = array(
	"2010-09-10",
	"eDisciplinev12 - add ApprovalGroupID in DISCIPLINE_MERIT_TYPE_SETTING",
	"ALTER TABLE DISCIPLINE_MERIT_TYPE_SETTING ADD COLUMN ApprovalGroupID INT(11) NOT NULL, ADD INDEX ApprovalGroupID (ApprovalGroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-09-10",
	"INTRANET_USER Personal Photo Link",
	"ALTER TABLE INTRANET_USER ADD PersonalPhotoLink varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-09-14",
	"IES : Change of default approval status",
	"ALTER TABLE IES_TASK MODIFY Approval tinyint(1) DEFAULT 1"	
);

$sql_eClassIP_update[] = array(
	"2010-09-15",
	"Import Class Student - Temp csv info Table",
	"CREATE TABLE IF NOT EXISTS TEMP_CLASS_STUDENT_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		UserLogin varchar(100),
		ClassNameEn varchar(100),
		ClassNumber varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-15",
	"Import Class Student - Add ID fields to the temp table",
	"ALTER TABLE TEMP_CLASS_STUDENT_IMPORT Add StudentID int(11) After ClassNumber, Add YearClassID int(11) After StudentID"
);

$sql_eClassIP_update[] = array(
	"2010-09-16",
	"Import Class Student - Temp import class result Table",
	"CREATE TABLE IF NOT EXISTS TEMP_CLASS_STUDENT_RESULT_IMPORT
	(
		TempID int(11) NOT NULL auto_increment,
		UserID int(11),
		RowNumber int(11),
		StudentID int(11),
		YearClassID int(11),
		ClassNumber varchar(100),
		DateInput datetime,
		PRIMARY KEY (TempID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"	
);

$sql_eClassIP_update[] = array(
	"2010-09-16",
	"Import Class Student - Add OldYearClassID fields to the temp table",
	"ALTER TABLE TEMP_CLASS_STUDENT_IMPORT Add OldYearClassID int(11) After YearClassID"
);

$sql_eClassIP_update[] = array(
	"2010-09-17",
	"IES : Add task enable status",
	"ALTER TABLE IES_TASK ADD Enable tinyint(1) DEFAULT 1 AFTER Approval"	
);

$sql_eClassIP_update[] = array(
	"2010-09-21",
	"Form - Add RecordStatus field to Form Table",
	"ALTER TABLE YEAR Add RecordStatus tinyint(3) Default 1"
);

############## 2010-09-28 IES: IES_SURVEY_EMAIL START ##############
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add UserID field to IES_SURVEY_EMAIL",
"ALTER TABLE IES_SURVEY_EMAIL ADD UserID INT(8) DEFAULT 0" 
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add RespondentNature to IES_SURVEY_EMAIL",
"ALTER TABLE IES_SURVEY_EMAIL ADD RespondentNature tinyint(4) COMMENT '1. Email is sent through internal email; 2. Email is sent through external email' default 0"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Set the IES_SURVEY_EMAIL Email with default '' to let ON DUPLICATE KEY can function",
"ALTER TABLE IES_SURVEY_EMAIL CHANGE Email `Email` varchar(255) default ''"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : IES_SURVEY_EMAIL redefine Unique Key",
"ALTER TABLE IES_SURVEY_EMAIL DROP KEY EmailUser, ADD UNIQUE KEY EmailUser (SurveyID,UserID,Email,RespondentNature)"
);
############## 2010-09-28 IES: IES_SURVEY_EMAIL END ##############
############## 2010-09-28 IES: IES_SURVEY_ANSWER START ##############
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add RespondentNature field to IES_SURVEY_ANSWER",
"ALTER TABLE IES_SURVEY_ANSWER ADD RespondentNature tinyint(4) COMMENT '1. Respondent is save as UserID; 2. Respondent is save as email; 3. Respondent is manually input' default 0"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Add UserID field to IES_SURVEY_ANSWER",
"ALTER TABLE IES_SURVEY_ANSWER ADD UserID int(8) default 0"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES : Set the IES_SURVEY_ANSWER Respondent with default '' to let ON DUPLICATE KEY can function",
"ALTER TABLE IES_SURVEY_ANSWER CHANGE Respondent `Respondent` varchar(255) default ''"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"IES :IES_SURVEY_ANSWER redefine Unique Key",
"ALTER TABLE IES_SURVEY_ANSWER DROP KEY AnswerUser, ADD UNIQUE KEY `AnswerUser` (`SurveyID`,`Respondent`,`UserID`,`RespondentNature`);"
);
############## 2010-09-28 IES: IES_SURVEY_ANSWER END ##############
$sql_eClassIP_update[] = array(
"2010-09-28",
"Add table IES_DEFAULT_SURVEY_QUESTION",
"CREATE TABLE IF NOT EXISTS `IES_DEFAULT_SURVEY_QUESTION` (
  `QuestionID` int(11) NOT NULL auto_increment,
  `QuestionStr` varchar(255) default NULL,
  `DateInput` datetime default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`QuestionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
"2010-09-28",
"Add table IES_BIBLIOGRAPHY",
"CREATE TABLE IF NOT EXISTS `IES_BIBLIOGRAPHY` (
  `bibliographyID` int(11) NOT NULL auto_increment,
  `UserID` int(11) NOT NULL,
  `Content` mediumtext,
  `Type` varchar(255) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `SchemeID` int(8) default NULL,
  PRIMARY KEY  (`bibliographyID`),
  KEY `UserID` (`UserID`),
  KEY `SchemeID` (`SchemeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
"2010-09-29",
"add Todisplay field to IES_BIBLIOGRAPHY",
"ALTER TABLE IES_BIBLIOGRAPHY ADD Todisplay int(8) default '1'"
);

$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eSports(Sport day) - add InputBy to record the UserID of user who make enrolment.",
	"ALTER TABLE SPORTS_STUDENT_ENROL_EVENT ADD InputBy int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eSports(Swimming Gala) - add InputBy to record the UserID of user who make enrolment.",
	"ALTER TABLE SWIMMINGGALA_STUDENT_ENROL_EVENT ADD InputBy int(11) DEFAULT NULL"
);


$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eDisciplinev12 - add TemplateID in DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING, auto send eNotice when GM->AP (Statis)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING ADD TemplateID int(11) not null default 0 AFTER UpgradeDeductSubScore1"
);


$sql_eClassIP_update[] = array
(
	"2010-10-04",
	"eDisciplinev12 - add TemplateID in DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE, auto send eNotice when GM->AP (Floating)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE ADD TemplateID int(11) not null default 0 AFTER DetentionReason"
);

$sql_eClassIP_update[] = array
(
	"2010-10-06",
	"eDisciplinev12 - add sendNoticeAction in DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING, flag of send notice action(Statis)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING ADD sendNoticeAction tinyint(1) not null default 0 AFTER TemplateID"
);


$sql_eClassIP_update[] = array
(
	"2010-10-06",
	"eDisciplinev12 - add sendNoticeAction in DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE, flag of send notice action (Floating)",
	"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE ADD sendNoticeAction tinyint(1) not null default 0 AFTER TemplateID"
);

$sql_eClassIP_update[] = array
(
	"2010-10-08",
	"eSports - ADD ResultTrial1,ResultTrial2,ResultTrial3 to record trial record for field event.",
	"ALTER TABLE SPORTS_LANE_ARRANGEMENT ADD ResultTrial1 float default NULL AFTER ResultMetre , ADD ResultTrial2 float default NULL AFTER ResultTrial1 , ADD ResultTrial3 float default NULL AFTER ResultTrial2"
);

$sql_eClassIP_update[] = array
(
	"2010-10-11",
	"Staff Attendance V3 - add ReasonActive to CARD_STAFF_ATTENDANCE3_REASON for changing status of a reason.",
	"ALTER TABLE CARD_STAFF_ATTENDANCE3_REASON ADD COLUMN ReasonActive int(2) default 1 AFTER ReasonDescription"
);


$sql_eClassIP_update[] = array
(
	"2010-10-13",
	"Student Registry - add option \"Others\" on some fields in table STUDENT_REGISTRY_STUDENT",
	"ALTER TABLE STUDENT_REGISTRY_STUDENT 
		ADD COLUMN B_PLACE_OTHERS varchar(255) AFTER B_PLACE,
		ADD COLUMN RACE_OTHERS varchar(255) AFTER RACE,
		ADD COLUMN NATION_OTHERS varchar(255) AFTER NATION,
		ADD COLUMN RELIGION_OTHERS varchar(255) AFTER RELIGION,
		ADD COLUMN FAMILY_LANG_OTHERS varchar(255) AFTER FAMILY_LANG,
		ADD COLUMN LODGING_OTHERS varchar(255) AFTER LODGING
		;"
);

$sql_eClassIP_update[] = array
(
	"2010-10-13",
	"Student Registry - add option \"Others\" on some fields in table STUDENT_REGISTRY_PG",
	"ALTER TABLE STUDENT_REGISTRY_PG 
		ADD COLUMN JOB_NATURE_OTHERS varchar(255) AFTER JOB_NATURE,
		ADD COLUMN JOB_TITLE_OTHERS varchar(255) AFTER JOB_TITLE,
		ADD COLUMN EDU_LEVEL_OTHERS varchar(255) AFTER EDU_LEVEL
		;"
);

$sql_eClassIP_update[] = array(
	"2010-10-14",
	"Add Language to IES_SCHEME",
	"ALTER TABLE IES_SCHEME ADD Language varchar(5) default 'b5'"
	);	
$sql_eClassIP_update[] = array(
	"2010-10-14",
	"Add SchemeType to IES_SCHEME",
	"ALTER TABLE IES_SCHEME ADD SchemeType varchar(20) default 'ies'"
	);	
	
$sql_eClassIP_update[] = array
(
	"2010-10-15",
	"iMail Gamma - add DisplayName to MAIL_SHARED_MAILBOX as SharedMailBox sender name.",
	"ALTER TABLE MAIL_SHARED_MAILBOX add column DisplayName varchar(255) default NULL after MailBoxType"
);

$sql_eClassIP_update[] = array
(
	"2010-10-19",
	"IES - new table for marking criteria",
	"CREATE TABLE IF NOT EXISTS IES_MARKING_CRITERIA (
		MarkCriteriaID int(8) NOT NULL auto_increment,
		TitleChi varchar(255) default NULL,
		TitleEng varchar(255) default NULL,
		isDefault tinyint(1) default NULL,
		defaultWeight float default NULL,
		DateInput timestamp NULL default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (MarkCriteriaID)
	) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-10-19",
	"IES - new table for mapping marking criteria with stage",
	"CREATE TABLE IF NOT EXISTS IES_STAGE_MARKING_CRITERIA (
		StageMarkCriteriaID int(8) NOT NULL auto_increment,
		StageID int(8) default NULL,
		MarkCriteriaID int(8) default NULL,
		task_rubric_id int(8) default NULL,
		Weight float default NULL,
		DateInput timestamp NULL default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (StageMarkCriteriaID),
		UNIQUE KEY StageCriteria (StageID,MarkCriteriaID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2010-10-19",
	"IES - new field to store stage-criteria map ID",
	"ALTER TABLE IES_MARKING ADD COLUMN StageMarkCriteriaID int(8)"
);

$sql_eClassIP_update[] = array
(
	"2010-10-25",
	"iCalendar - new table for mapping School Calendar & iCal Event (Group Event Only))",
	" CREATE TABLE IF NOT EXISTS CALENDAR_EVENT_SCHOOL_EVENT_RELATION (
		  CalendarEventID int(11) default NULL,
		  SchoolEventID int(11) default NULL,
		  UNIQUE KEY CalendarSchoolEventMapID (CalendarEventID,SchoolEventID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array
(
	"2010-10-28",
	"Repair System, add default details content",
	"alter table REPAIR_SYSTEM_CATEGORY add DetailsContent text"
);

$sql_eClassIP_update[] = array
(
	"2010-10-28",
	"eHomework - Add creator user id",
	"alter table INTRANET_HOMEWORK add CreatedBy int(11)"
);

$sql_eClassIP_update[] = array
(
	"2010-10-29",
	"eNotice - Add notice type (parent / student)",
	"alter table INTRANET_NOTICE add TargetType char(1) default 'P'"
);

$sql_eClassIP_update[] = array
(
	"2010-11-02",
	"Student Profile - Add AcademicYearID, YearTermID, eEnrol-related fields and indices",
	"Alter Table PROFILE_STUDENT_ACTIVITY 	Add Column AcademicYearID int(8) Default Null, 
											Add Column YearTermID int(8) Default Null,
											Add Column YearClassID int(8) Default Null,
											Add Column FromModule varchar(64) Default Null, 
											Add Column eEnrolRecordType varchar(64) Default Null, 
											Add Column eEnrolRecordID int(11) Default Null, 
											Add Index AcademicYearID (AcademicYearID), 
											Add Index YearTermID (YearTermID), 
											Add Index YearClassID (YearClassID), 
											Add Index eEnrolRecordID (eEnrolRecordID)
	"
);

$sql_eClassIP_update[] = array
(
	"2010-11-02",
	"eNotice - Add Footer choice",
	"alter table INTRANET_NOTICE add column Footer text AFTER ReplySlipContent"
);

$sql_eClassIP_update[] = array(
	"2010-11-02",
	"Create table for data transfer log",
	"CREATE TABLE IF NOT EXISTS MODULE_RECORD_TRANSFER_LOG
	(
		LogID int(11) NOT NULL auto_increment,
		FromModule varchar(20) NOT NULL,
		ToModule varchar(20) NOT NULL,
		RecordDetail text default NULL,
		LogDate datetime default NULL,
		LogBy int(11) default NULL,
		PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student activity profile",
	"ALTER TABLE PROFILE_STUDENT_ACTIVITY MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student attendance profile",
	"ALTER TABLE PROFILE_STUDENT_ATTENDANCE MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student award profile",
	"ALTER TABLE PROFILE_STUDENT_AWARD MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student merit profile",
	"ALTER TABLE PROFILE_STUDENT_MERIT MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-03",
	"Lengthen semester field in student service profile",
	"ALTER TABLE PROFILE_STUDENT_SERVICE MODIFY Semester varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2010-11-04",
	"Subject Group - Add field to record the locked Class for the Subject Group",
	"ALTER TABLE SUBJECT_TERM_CLASS Add Column LockedYearClassID int(8) default Null, Add Index LockedYearClassID (LockedYearClassID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-10",
	"IES : ADD Comment FOR IES_SURVEY_MAPPING",
	"ALTER TABLE IES_SURVEY_MAPPING ADD Comment longtext"
);

$sql_eClassIP_update[] = array(
	"2010-11-11",
	"Staff Attendance V3 : Create table for storing user settings (e.g. report option settings) for Staff Attendance",
	"CREATE TABLE IF NOT EXISTS CARD_STAFF_ATTENDANCE3_USER_SETTING(
		SettingID int(11) NOT NULL auto_increment, 
		UserID int(11) NOT NULL,
		SettingName varchar(100) NOT NULL,
		SettingValue text default NULL,
		DisplayName varchar(255) NOT NULL,
		DateModified datetime default NULL,
		PRIMARY KEY(SettingID),
		UNIQUE UniqueName(UserID, SettingName, DisplayName),
		INDEX UserID (UserID)
	) ENGINE=InnoDB CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Use to Categorized users, Customization for eRC Rubrics (Shek Wu).",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_EXTRA_INFO_CATEGORY (
		CategoryID int(11) NOT NULL auto_increment,
		CategoryCode varchar(10),
		CategoryNameCh varchar(255),
		CategoryNameEn varchar(255),
		DisplayOrder int(11),
		RecordType int(2),
		RecordStatus int(2) DEFAULT 1,
		DateInput datetime,
		DateModified datetime,
		InputBy int(11) ,
		LastModifiedBy int(11) ,
		PRIMARY KEY (CategoryID),
		INDEX CategoryCode (CategoryCode),
		INDEX CategoryNameCh (CategoryNameCh),
		INDEX CategoryNameEn (CategoryNameEn)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Use to Categorized users, Customization for eRC Rubrics (Shek Wu).",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_EXTRA_INFO_ITEM (
		ItemID int(11) NOT NULL auto_increment,
		CategoryID int(11) NOT NULL,
		ItemCode varchar(10),
		ItemNameCh varchar(255),
		ItemNameEn varchar(255),
		DisplayOrder int(11),
		RecordStatus int(2) DEFAULT 1,
		DateInput datetime,
		DateModified datetime,
		InputBy int(11) ,
		LastModifiedBy int(11) ,
		PRIMARY KEY (ItemID),
		INDEX CategoryID (CategoryID),
		INDEX ItemCode (ItemCode),
		INDEX ItemNameCh (ItemNameCh),
		INDEX ItemNameEn (ItemNameEn)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Use to Categorized users, Customization for eRC Rubrics (Shek Wu).",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_EXTRA_INFO_MAPPING (
		UserItemID int(11) NOT NULL auto_increment,
		UserID int(11) NOT NULL,
		ItemID int(11) NOT NULL,
		DateModified datetime,
		LastModifiedBy int(11) ,
		PRIMARY KEY (UserItemID),
		UNIQUE INDEX UserItemMapping (UserID,ItemID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add Sequence to  IES_SURVEY_MAPPING",
	"ALTER TABLE IES_SURVEY_MAPPING ADD Sequence int(8) default NULL"
);


$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Create Table IES_FAQ_FILE",
	"CREATE TABLE IF NOT EXISTS `IES_FAQ_FILE` (
  `FileID` int(11) NOT NULL auto_increment,
  `FileName` varchar(255) default NULL,
  `FolderPath` varchar(255) default NULL,
  `FileHashName` varchar(255) default NULL,
  `QuestionID` int(11) NOT NULL,
  `DateInput` timestamp NULL default NULL,
  `InputBy` int(8) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(8) default NULL,
  PRIMARY KEY  (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add column for storing scheme score",
	"ALTER TABLE IES_SCHEME_STUDENT 
	ADD COLUMN Score float AFTER UserID,
	MODIFY DateInput timestamp NULL DEFAULT NULL,
	ADD COLUMN DateModified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	ADD COLUMN ModifyBy int(8)"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add column for storing stage weight (for calculating scheme score)",
	"ALTER TABLE IES_STAGE ADD COLUMN Weight float AFTER Deadline"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Add column for storing criteria display sequence",
	"ALTER TABLE IES_MARKING_CRITERIA ADD COLUMN defaultSequence int(8) AFTER defaultWeight"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Create table for storing document section of stages",
	"CREATE TABLE IF NOT EXISTS IES_STAGE_DOC_SECTION(
	SectionID int(8) NOT NULL auto_increment,
	StageID int(8),
	Sequence int(8),
	Title varchar(255),
	DateInput timestamp NULL DEFAULT NULL,
	InputBy int(8),
	DateModified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	ModifyBy int(8),
	PRIMARY KEY (SectionID)
) ENGINE = InnoDB DEFAULT CHARSET = utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-16",
	"Create table for storing tasks of document sections",
	"CREATE TABLE IF NOT EXISTS IES_STAGE_DOC_SECTION_TASK(
	SectionID int(8),
	TaskID int(8),
	Sequence int(8),
	DateInput timestamp NULL DEFAULT NULL,
	InputBy int(8),
	DateModified timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	ModifyBy int(8),
	PRIMARY KEY (SectionID, TaskID)
) ENGINE = InnoDB DEFAULT CHARSET = utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-17",
	"Create table for storing IES comment category",
	"CREATE TABLE IF NOT EXISTS `IES_COMMENT_CATEGORY` (
  `CategoryID` int(11) NOT NULL auto_increment,
  `Title` mediumtext,
  `DateInput` datetime default NULL,
  `InputBy` int(11) default NULL,
  `DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `ModifyBy` int(11) default NULL,
  PRIMARY KEY  (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
/*
 * Rerun on 2011-02-07
$sql_eClassIP_update[] = array(
	"2010-11-17",
	"Add categoryID to IES_COMMENT",
	"ALTER TABLE IES_COMMENT ADD `CategoryID` int(11) default NULL"
);
*/
$sql_eClassIP_update[] = array(
	"2010-11-18",
	"Add TitleEN to IES_COMMENT_CATEGORY",
	"ALTER TABLE IES_COMMENT_CATEGORY ADD `TitleEN` mediumtext"
);
$sql_eClassIP_update[] = array(
	"2010-11-18",
	"Add CommentEN to IES_COMMENT",
	"ALTER TABLE IES_COMMENT ADD `CommentEN` mediumtext"
);
$sql_eClassIP_update[] = array(
	"2010-11-18",
	"Add Cache table on class/ group confirm status",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_CACHED_CONFIRM_STATUS (
	  TargetDate Date NOT NULL,
	  DayType int(2) NOT NULL,
	  Type varchar(20) NOT NULL,
	  ConfirmStatus varchar(2) default NULL,
	  PRIMARY KEY  (TargetDate,DayType,Type)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-22",
	"Create Table to Store User Signature Image",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_SIGNATURE (
		SignatureID int(11) NOT NULL auto_increment,
		UserLogin varchar(20) NOT NULL,
		SignatureLink text NOT NULL,
		DateInput datetime,
		InputBy int(11) ,
		PRIMARY KEY (SignatureID),
		INDEX UserLogin(UserLogin)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-11-22",
	"Subject Group - Change SubjectGroupID to auto increment",
	"ALTER TABLE SUBJECT_TERM_CLASS CHANGE SubjectGroupID SubjectGroupID int(8) NOT NULL auto_increment"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Added DefaultFlag Field to CARD_STAFF_ATTENDANCE3_REASON for default reason",
	"alter table CARD_STAFF_ATTENDANCE3_REASON add DefaultFlag int(2) default NULL after ReasonActive"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Add TAG table (can apply to all modules)",
	"CREATE TABLE IF NOT EXISTS TAG (
		TagID int(11) NOT NULL auto_increment,
		TagName varchar(200) NOT NULL,
		DateInput datetime,
		InputBy int(11),
		PRIMARY KEY (TagID)
	)  ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Add TAG_RELATIONSHIP table (can apply to all modules)",
	"CREATE TABLE IF NOT EXISTS TAG_RELATIONSHIP (
		TagRelationID int(11) not null auto_increment,
		TagID int(11) not null,
		Module varchar(20) not null,
		DateInput datetime,
		InputBy int(11),
		DateModified datetime,
		ModifyBy int(11),
		PRIMARY KEY (TagRelationID),
		INDEX TagID (TagID),
		INDEX Module (Module)
	)  ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-23",
	"Add column TAGID in STUDENT_REGISTRY_STUDENT",
	"alter table STUDENT_REGISTRY_STUDENT add column TAGID varchar(200) AFTER EMAIL"
);

$sql_eClassIP_update[] = array(
	"2010-11-24",
	"Add column CompulsoryInReport in table DISCIPLINE_ACCU_CATEGORY_ITEM",
	"alter table DISCIPLINE_ACCU_CATEGORY_ITEM add column CompulsoryInReport tinyint(1) default 0 not null, add index CompulsoryInReport(CompulsoryInReport)"
);

$sql_eClassIP_update[] = array(
	"2010-11-24",
	"Add column CompulsoryInReport in table DISCIPLINE_MERIT_ITEM",
	"alter table DISCIPLINE_MERIT_ITEM add column CompulsoryInReport tinyint(1) default 0 not null after RecordStatus, add index CompulsoryInReport(CompulsoryInReport)"
);

$sql_eClassIP_update[] = array(
	"2010-11-24",
	"Repair System - Add Request Summary settings",
	"CREATE TABLE IF NOT EXISTS REPAIR_SYSTEM_REQUEST_SUMMARY (
		RecordID int(8) NOT NULL auto_increment,
		CategoryID int(11) default NULL,
		Title varchar(255) default NULL,
		Sequence int(2) default NULL,
		RecordStatus int(1) default NULL,
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (RecordID),
		INDEX CategoryID (CategoryID),
		INDEX Title (Title)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Subject Group - Add Index UserID, RowNumber to TEMP_SUBJECT_GROUP_IMPORT",
	"Alter Table TEMP_SUBJECT_GROUP_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber)"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Class - Add Index UserID, RowNumber to TEMP_CLASS_STUDENT_IMPORT",
	"Alter Table TEMP_CLASS_STUDENT_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber)"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Class - Add Index UserID, RowNumber, YearClassID to TEMP_CLASS_STUDENT_RESULT_IMPORT",
	"Alter Table TEMP_CLASS_STUDENT_RESULT_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber), Add Index YearClassID (YearClassID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-25",
	"Timetable - Add Index UserID, RowNumber, TimetableID to TEMP_TIMETABLE_LESSON_IMPORT",
	"Alter Table TEMP_TIMETABLE_LESSON_IMPORT Add Index UserID (UserID), Add Index RowNumber (RowNumber), Add Index TimetableID (TimetableID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWSubjectID (SubjectID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWClassGroupID (ClassGroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWYearTermID (YearTermID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-29",
	"INTRANET_HOMEWORK",
	"alter table INTRANET_HOMEWORK add index HWAcademicYearID (AcademicYearID)"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add column CurrentStatus int(11) default 0 after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add column CheckInCheckOutRemark text after CurrentStatus"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add column CurrentStatus int(11) default 0 after RecordStatus"
);

$sql_eClassIP_update[] = array(
	"2010-11-30",
	"INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add column CheckInCheckOutRemark text after CurrentStatus"
);

$sql_eClassIP_update[] = array(
	"2010-12-06",
	"add table DISCIPLINE_SKSS_CONDUCT_POTENTIALS_REPORT [CRM Ref No.: 2010-0811-1619]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_SKSS_CONDUCT_POTENTIALS_REPORT (
		RecordID int(8) NOT NULL auto_increment,
		AcademicYearID int(8) NOT NULL,
		StudentID int(8) NOT NULL default 0,
		GradeString varchar(200),
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (RecordID),
		INDEX AcademicYearID (AcademicYearID),
		INDEX StudentID (StudentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2010-12-06",
	"eEnrolment - Performance adjustment [Customization]",
	"CREATE TABLE IF NOT EXISTS ENROLMENT_ADJUSTMENT_PERFORMNAMCE_CUST (
	  RecordID int(8) NOT NULL auto_increment,
	  UserID int(8),
	  AcademicYearID int(8),
	  Adjustment int(8),
	  TotalPerformance float(8,2),
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY (RecordID),
	  INDEX UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"iMail - Performance issue [Add back a Index CampusMailFromID - Special Case only]",
	"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX CampusMailFromID (CampusMailFromID)"
);

/* disabled on 20110523
$sql_eClassIP_update[] = array(
	"2010-12-07",
	"iMail - Performance issue [Drop Duplicate Index]",
	"ALTER TABLE INTRANET_CAMPUSMAIL drop index CampusMailFromID_2, drop index CampusMailFromID_3, drop index CampusMailFromID_4, drop index CampusMailFromID_5, drop index UserFolderID_2, drop index MailType_2"
);
*/

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - ResourceID]",
	"alter table INTRANET_BOOKING_RECORD add index ResourceID (ResourceID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - UserID]",
	"alter table INTRANET_BOOKING_RECORD add index UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - PeriodicBookingID]",
	"alter table INTRANET_BOOKING_RECORD add index PeriodicBookingID (PeriodicBookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - RecordStatus]",
	"alter table INTRANET_BOOKING_RECORD add index RecordStatus (RecordStatus)"
);

$sql_eClassIP_update[] = array(
	"2010-12-07",
	"Resource Booking - Performance issue [Add Index - BookingDate]",
	"alter table INTRANET_BOOKING_RECORD add index BookingDate (BookingDate)"
);

$sql_eClassIP_update[] = array(
	"2010-12-08",
	"School Calendar - add Column SkipOnWeekday",
	"alter table INTRANET_CYCLE_GENERATION_PERIOD add column SkipOnWeekday text after SaturdayCounted"
);

$sql_eClassIP_update[] = array(
	"2010-12-09",
	"iMail Gamma - change MailBoxID type from varchar to int",
	"alter table MAIL_SHARED_MAILBOX_MEMBER modify MailBoxID int(11) NOT NULL"
);

$sql_eClassIP_update[] = array(
	"2010-12-13",
	"Add TitleChinese for Group",
	"alter table INTRANET_GROUP add TitleChinese varchar(100) default NULL after Title"
);

$sql_eClassIP_update[] = array(
	"2010-12-13",
	"create table for student attendance outing reason",
	"CREATE TABLE CARD_STUDENT_OUTING_REASON (
	  RecordID int(11) NOT NULL auto_increment,
	  ReasonText varchar(255) default NULL,
	  DateInput datetime default NULL,
	  InputBy int(11),
	  DateModify datetime default NULL,
	  ModifyBy int(11),
	  PRIMARY KEY  (RecordID),
	  UNIQUE KEY OutingReasonText (ReasonText)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-12-13",
	"Subject Settings -> Relationship between Form & Subject",
	"CREATE TABLE IF NOT EXISTS SUBJECT_YEAR_RELATION (
	  SubjectID int(11) NOT NULL,
	  YearID int(8) NOT NULL,
	  DateInput datetime default NULL,
	  InputBy int(8) default NULL,
	  DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	  ModifyBy int(8) default NULL,
	  PRIMARY KEY  (SubjectID, YearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-12-15",
	"iMail Gamma - Add ComposeTime to MAIL_ATTACH_FILE_MAP prevent opening multiple Compose Mail page delete uploaded attchment",
	"alter table MAIL_ATTACH_FILE_MAP add column ComposeTime datetime default '1970-01-01 00:00:00'"
);

$sql_eClassIP_update[] = array(
	"2010-12-15",
	"iMail Gamma - Add ComposeTime to MAIL_INLINE_IMAGE_MAP prevent opening multiple Compose Mail page delete inline images",
	"alter table MAIL_INLINE_IMAGE_MAP add column ComposeTime datetime default '1970-01-01 00:00:00'"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RelatedSubLocationID to TABLE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD",
	"alter table INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD add index RelatedSubLocationID (RelatedSubLocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RelatedItemID to TABLE INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD",
	"alter table INTRANET_EBOOKING_AVAILABLE_BOOKING_PERIOD add index RelatedItemID (RelatedItemID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RuleID to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index RuleID (RuleID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index YearID to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index YearID (YearID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index UserType to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index UserType (UserType)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index IsTeaching to TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER",
	"alter table INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER add index IsTeaching (IsTeaching)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingRuleID to TABLE INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_BOOKING_RULE_RELATION add index BookingRuleID (BookingRuleID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index UserBookingRuleID to TABLE INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_USER_BOOKING_RULE_RELATION add index UserBookingRuleID (UserBookingRuleID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingID to TABLE INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS",
	"alter table INTRANET_EBOOKING_CALENDAR_EVENT_DETAILS add index BookingID (BookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingID to TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add index BookingID (BookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index ItemID to TABLE INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS add index ItemID (ItemID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index LocationID to TABLE INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION add index LocationID (LocationID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index GroupID to TABLE INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION",
	"alter table INTRANET_EBOOKING_LOCATION_MANAGEMENT_GROUP_RELATION add index GroupID (GroupID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RelatedTo to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index RelatedTo (RelatedTo)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RequestedBy to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index RequestedBy (RequestedBy)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index ResponsibleUID to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index ResponsibleUID (ResponsibleUID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index PIC to TABLE INTRANET_EBOOKING_RECORD",
	"alter table INTRANET_EBOOKING_RECORD add index PIC (PIC)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index BookingID to TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add index BookingID (BookingID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index RoomID to TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add index RoomID (RoomID)"
);

$sql_eClassIP_update[] = array(
	"2010-12-16",
	"eBooking - Add Index PIC to TABLE INTRANET_EBOOKING_ROOM_BOOKING_DETAILS",
	"alter table INTRANET_EBOOKING_ROOM_BOOKING_DETAILS add index PIC (PIC)"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add Default MAX MARK for IES_MARKING_CRITERIA ",
	"ALTER TABLE IES_MARKING_CRITERIA ADD `defaultMaxScore`  float default NULL after `isDefault`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add setting MaxScore for IES_SCHEME ",
	"ALTER TABLE IES_SCHEME ADD `MaxScore`  float default NULL after `Title`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add setting MaxScore for IES_STAGE ",
	"ALTER TABLE IES_STAGE ADD `MaxScore`  float default NULL after `Deadline`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-17",
	"IES - Add setting MaxScore for IES_STAGE_MARKING_CRITERIA",
	"ALTER TABLE IES_STAGE_MARKING_CRITERIA ADD `MaxScore`  float default NULL after `task_rubric_id`;"
);

$sql_eClassIP_update[] = array(
	"2010-12-20",
	"Add Preset waive field for preset absence record",
	"alter table CARD_STUDENT_PRESET_LEAVE add Waive int(1) default NULL after Reason"
);

$sql_eClassIP_update[] = array(
	"2010-12-21",
	"eComm - add annoumcnent settings - allow delete others records",
	"alter table INTRANET_GROUP add AllowDeleteOthersAnnouncement tinyint(1) default 1"
);

$sql_eClassIP_update[] = array(
	"2010-12-28",
	"Polling - add attachment",
	"alter table INTRANET_POLLING add Attachment varchar(255)"
);

$sql_eClassIP_update[] = array(
	"2011-01-06",
	"Super system admin",
	"alter table INTRANET_USER add isSystemAdmin tinyint(1) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-01-11",
	"Create Mysql Function MINIMUM() - simulate php function min()",
	"
		CREATE FUNCTION MINIMUM(n1 double, n2 double) 
		RETURNS double DETERMINISTIC 
		RETURN IF(n1<=n2,n1,n2)
	"
);

$sql_eClassIP_update[] = array(
	"2011-01-11",
	"Create Mysql Function MAXIMUM() - simulate php function max()",
	"
		CREATE FUNCTION MAXIMUM(n1 double, n2 double) 
		RETURNS double DETERMINISTIC 
		RETURN IF(n1>=n2,n1,n2)
	"
);

$sql_eClassIP_update[] = array(
	"2011-01-17",
	"Notice Payment - add column TempItemAmount for \"Save Draft\" ",
	"alter table INTRANET_NOTICE_REPLY add TempItemAmount int(10) default 0 AFTER Answer"
);

$sql_eClassIP_update[] = array(
	"2011-01-24",
	"Resource Booking - add an index UserIDBookDateRecordStatusPeriodBookingID to improve the performance",
	"alter table INTRANET_BOOKING_RECORD add KEY UserIDBookDateRecordStatusPeriodBookingID (UserID,BookingDate,RecordStatus,PeriodicBookingID)"
);

$sql_eClassIP_update[] = array(
	"2011-01-31",
	"Create table ECLASS_UPDATE_LOG - for \"eClass\" update checking",
	"CREATE TABLE IF NOT EXISTS ECLASS_UPDATE_LOG (
		RecordID INT(8) NOT NULL AUTO_INCREMENT,
		Module varchar(100) NOT NULL,
		Version float(5,2) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (RecordID),
		INDEX Module (Module),
		INDEX Version (Version)
	) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-01-31",
	"Create table ECLASS_MODULE_VERSION_LOG - store most update module version number",
	"CREATE TABLE IF NOT EXISTS ECLASS_MODULE_VERSION_LOG (
		RecordID INT(8) NOT NULL AUTO_INCREMENT,
		Module varchar(100) NOT NULL,
		VersionNo float(5,2) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (RecordID),
		INDEX Module (Module),
		INDEX VersionNo (VersionNo)
	) ENGINE=InnoDB Charset=utf8"
);


$sql_eClassIP_update[] = array(
	"2011-02-01",
	"eLibrary - add colume ExtensionTLF in table INTRANET_ELIB_BOOK_CHAPTER_TLF",
	"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER_TLF ADD COLUMN ExtensionTLF varchar(9) DEFAULT '.tlf' AFTER StyleTLF;"
);

$sql_eClassIP_update[] = array(
	"2011-02-02",
	"Create table ECLASS_BUBBLE_VERSION - bubble version",
	"CREATE TABLE IF NOT EXISTS ECLASS_BUBBLE_VERSION (
		VersionID INT(8) NOT NULL AUTO_INCREMENT,
		Version varchar(100) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (VersionID),
		INDEX Version (Version)
	) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-02-02",
	"Create table ECLASS_BUBBLE_READ - bubble message read (don't remind again on same version)",
	"CREATE TABLE IF NOT EXISTS ECLASS_BUBBLE_READ (
		VersionID INT(8) NOT NULL AUTO_INCREMENT,
		UserID INT(8) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (VersionID, UserID),
		INDEX VersionID (VersionID),
		INDEX UserID (UserID)
	) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-02-07",
	"Discover some client can't create `CategoryID` in 2010-11-17, need to re-run Add categoryID to IES_COMMENT",
	"ALTER TABLE IES_COMMENT ADD `CategoryID` int(11) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-02-08",
	"eLibrary - add colume TLFcharIndex in table INTRANET_ELIB_USER_BOOKMARK",
	"ALTER TABLE INTRANET_ELIB_USER_BOOKMARK ADD COLUMN TLFcharIndex int(11) DEFAULT -1 AFTER PageIndex;"
);

$sql_eClassIP_update[] = array(
	"2011-02-08",
	"eLibrary - add colume TLFcharIndex in table INTRANET_ELIB_USER_DRAWING",
	"ALTER TABLE INTRANET_ELIB_USER_DRAWING ADD COLUMN TLFcharIndex int(11) DEFAULT -1 AFTER PageIndex;"
);

$sql_eClassIP_update[] = array(
	"2011-02-08",
	"eLibrary - add colume TLFcharIndex in table INTRANET_ELIB_USER_MYNOTES",
	"ALTER TABLE INTRANET_ELIB_USER_MYNOTES ADD COLUMN TLFcharIndex int(11) DEFAULT -1 AFTER PageID;"
);

$sql_eClassIP_update[] = array(
	"2011-02-08",
	"eLibrary - add colume TLFcharIndex in table INTRANET_ELIB_USER_PROGRESS",
	"ALTER TABLE INTRANET_ELIB_USER_PROGRESS ADD COLUMN TLFcharIndex int(11) DEFAULT -1 AFTER PageID;"
);

$sql_eClassIP_update[] = array(
	"2011-02-08",
	"eLibrary - add colume ChapterID in table INTRANET_ELIB_USER_MYNOTES",
	"ALTER TABLE INTRANET_ELIB_USER_MYNOTES ADD COLUMN ChapterID int(11) DEFAULT -1 AFTER PageID;"
);

$sql_eClassIP_update[] = array(
	"2011-02-08",
	"eLibrary - add colume ChapterID in table INTRANET_ELIB_USER_PROGRESS",
	"ALTER TABLE INTRANET_ELIB_USER_PROGRESS ADD COLUMN ChapterID int(11) DEFAULT -1 AFTER PageID;"
);


$sql_eClassIP_update[] = array(
	"2011-02-11",
	"Create table STUDENT_OFFICAL_PHOTO_MGMT_GROUP - group member of teachers who can upload offical photo of student",
	"CREATE TABLE IF NOT EXISTS STUDENT_OFFICAL_PHOTO_MGMT_GROUP (
		RecordID INT(8) NOT NULL AUTO_INCREMENT,
		UserID INT(8) NOT NULL,
		DateInput datetime,
		PRIMARY KEY (RecordID),
		INDEX UserID (UserID)
	) ENGINE=InnoDB Charset=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-02-11",
	"School Settings -> Form - Add Form Stage Table",
	"CREATE TABLE IF NOT EXISTS FORM_STAGE (
		FormStageID int(11) NOT NULL auto_increment,
		Code varchar(10) Default Null,
		TitleEn varchar(255) Default Null,
		TitleCh varchar(255) Default Null,
		DisplayOrder int(11) Default Null,
		RecordStatus int(11) Default 1,
		DateInput datetime Default Null,
		DateModified datetime Default Null,
		LastModifiedBy int(11) Default Null,
		PRIMARY KEY (FormStageID),
		INDEX Code (Code),
		INDEX DisplayOrder (DisplayOrder)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2011-02-11",
	"School Settings -> Form - Add Form Stage Mapping in Form Table",
	"ALTER TABLE YEAR ADD COLUMN FormStageID int(11) Default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-02-16",
	"eNotice - add DisplayQuestionNumber field",
	"alter table INTRANET_NOTICE add DisplayQuestionNumber tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2011-02-16",
	"eBooking - add Attachment field to eBooking Item",
	"ALTER TABLE INTRANET_EBOOKING_ITEM ADD COLUMN Attachment varchar(255) AFTER eInventoryItemID"
);

$sql_eClassIP_update[] = array(
	"2011-02-18",
	"eCircular - add DisplayQuestionNumber field",
	"alter table INTRANET_CIRCULAR add DisplayQuestionNumber tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2011-02-18",
	"eBooking - add Attachment field to eInventory INVENTORY_LOCATION",
	"ALTER TABLE INVENTORY_LOCATION ADD COLUMN Attachment varchar(255) AFTER NameEng"
);

$sql_eClassIP_update[] = array(
	"2011-02-22",
	"SMS - Change ReferenceID field from varchar(20) to varchar(50)",
	"ALTER TABLE INTRANET_SMS2_MESSAGE_RECORD CHANGE ReferenceID ReferenceID varchar(50);"
);

$sql_eClassIP_update[] = array(
	"2011-02-23",
	"eSurvey - add DisplayQuestionNumber field",
	"alter table INTRANET_SURVEY add DisplayQuestionNumber tinyint(1) default 0"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-03",
	"iPortfolio (OEA) - OEA Student Record",
	"CREATE TABLE IF NOT EXISTS OEA_STUDENT (
	  RecordID int(11) NOT NULL auto_increment,
	  StudentID int(11) NOT NULL,
	  Title varchar(255) default NULL,
	  StartYear int(4) NOT NULL,
	  EndYear int(4) NOT NULL,
	  OEA_AwardBearing varchar(10) default NULL,
	  Participation varchar(10) default NULL,
	  Role varchar(10) default NULL,
	  ParticipationNature varchar(10) default NULL,
	  Achievement varchar(10) default NULL,
	  OEA_ProgramCode varchar(10) NOT NULL,
	  OLE_STUDENT_RecordID int(11) NOT NULL COMMENT 'Reference to OLE_SUDENT>RecordID, OEA checkbox checking',
	  OLE_PROGRAM_ProgramID int(11) NOT NULL COMMENT 'Reference to OLE_PROGRAM>ProgramID, quick reference to OLE program',
	  RecordStatus int(2) NOT NULL default '0' COMMENT '1:approved / 0:pending',
	  InputDate datetime default NULL,
	  InputBy int(11) default NULL,
	  ApprovalDate datetime default NULL,
	  ApprovedBy int(11) default NULL,
	  ModifyDate datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (RecordID),
	  INDEX StudentID (StudentID),
	  INDEX StartYear (StartYear),
	  INDEX EndYear (EndYear),
	  INDEX OEA_ProgramCode (OEA_ProgramCode),
	  INDEX OLE_STUDENT_RecordID (OLE_STUDENT_RecordID),
	  INDEX OLE_PROGRAM_ProgramID (OLE_PROGRAM_ProgramID),
	  INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='STORE OEA Studetn Record'"
);
# end of eClass DB update
# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-03",
	"iPortfolio (OEA) - OEA & OLE Mapping",
	"CREATE TABLE IF NOT EXISTS OEA_OLE_MAPPING (
	  RecordID int(11) NOT NULL auto_increment,
	  OEA_ProgramCode varchar(10) NOT NULL,
	  OLE_ProgramID int(11) NOT NULL COMMENT 'Reference to OLE_PROGRAM>ProgramID',
	  DefaultAwardBearing varchar(10) NOT NULL,
	  DefaultParticipation varchar(10) NOT NULL,
	  PRIMARY KEY  (RecordID),
	  INDEX OEA_ProgramCode (OEA_ProgramCode),
	  INDEX OLE_ProgramID (OLE_ProgramID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='SLP and OEA program mapping'"
);
# end of eClass DB update
# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-03",
	"iPortfolio (OEA) - OEA Additional Information",
	"CREATE TABLE IF NOT EXISTS OEA_ADDITIONAL_INFO (
	  AdditionalInfoID int(11) NOT NULL auto_increment,
	  StudentID int(11) NOT NULL,
	  FromSelfAccountID int(11) default NULL,
	  Title varchar(255) default NULL,
	  Details text,
	  RecordStatus int(2) NOT NULL default '0' COMMENT '1:approved / 0:pending',
	  InputDate datetime default NULL,
	  InputBy int(11) default NULL,
	  ModifyDate datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  ApprovedBy int(11) default NULL,
	  ApprovalDate datetime default NULL,
	  PRIMARY KEY  (AdditionalInfoID),
	  INDEX StudentID (StudentID),
	  INDEX FromSelfAccountID (FromSelfAccountID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
# end of eClass DB update
# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-04",
	"iPortfolio (OEA) - OEA Supplementary Information from Principal",
	"CREATE TABLE IF NOT EXISTS OEA_SUPPLEMENTARY_INFO (
	  SupplementaryInfoID int(11) NOT NULL auto_increment,
	  StudentID int(11) NOT NULL,
	  FromSelfAccountID int(11) default NULL,
	  Title varchar(255) default NULL,
	  Details text,
	  RecordStatus int(2) NOT NULL default '0' COMMENT '1:approved / 0:pending',
	  InputDate datetime default NULL,
	  InputBy int(11) default NULL,
	  ModifyDate datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  ApprovedBy int(11) default NULL,
	  ApprovalDate datetime default NULL,
	  PRIMARY KEY (SupplementaryInfoID),
	  INDEX StudentID (StudentID),
	  INDEX FromSelfAccountID (FromSelfAccountID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-03-04",
	"Resource Booking - add index for table INTRANET_SLOT",
	"alter table INTRANET_SLOT ADD KEY SlotSeq (SlotSeq)"
);

$sql_eClassIP_update[] = array(
	"2011-03-04",
	"Resource Booking - add index for table INTRANET_SLOT",
	"alter table INTRANET_SLOT ADD KEY BatchID (BatchID)"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-10",
	"iPortfolio (OEA) - OEA Student Ability",
	"CREATE TABLE OEA_STUDENT_ABILITY (
	     StudentAbilityID int(11) NOT NULL auto_increment,
	     StudentID int(11) NOT NULL,
	     AbilityValue text,
	     RecordStatus int(2) NOT NULL default '0',
	     InputDate datetime default NULL,
	     InputBy int(11) default NULL,
	     ApprovalDate datetime default NULL,
	     ApprovedBy int(11) default NULL,
	     ModifyDate datetime default NULL,
	     ModifiedBy int(11) default NULL,
	     PRIMARY KEY (StudentAbilityID),
	     KEY StudentID (StudentID),
	     KEY RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='STORE OEA Studetn ABILITY'"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-16",
	"iPortfolio - OEA Setting table",
	"CREATE TABLE IF NOT EXISTS OEA_SETTING (
		SettingID INT(11) NOT NULL AUTO_INCREMENT,
		SettingName varchar(255) NOT NULL default '',
		SettingValue text,
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		DateModified datetime default NULL,
		ModifyBy int(11) default NULL,
		PRIMARY KEY (SettingID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store the OEA setting values'"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_BOOK",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_BOOK (
	    BookID int(11) NOT NULL auto_increment,
	    CallNumber varchar(20),
	    BookName varchar(255),
	    Author varchar(255),
	    Publisher varchar(255),
	    Description text,
	    BookCoverImage varchar(255),
	    eBookID int(11),
	    Language int(1),
	    DefaultAnswerSheet mediumtext,
	    CategoryID int(11),
	    ByStudent int(1),
	    RecordStatus int(1) DEFAULT 1,
	    DisplayOrder int(11),
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (BookID),
	    INDEX CallNumber (CallNumber),
	    INDEX CategoryID (CategoryID),
	    INDEX ByStudent (ByStudent),
	    INDEX Language (Language),
	    INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_BOOK_COMMENT",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_BOOK_COMMENT (
	    BookCommentID int(11) NOT NULL auto_increment,
	    BookID int(11) NOT NULL,
	    UserID int(11) NOT NULL,
	    Comment mediumtext,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (BookCommentID),
	    INDEX BookUserID (BookID,UserID),
	    INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_BOOK_CATEGORY",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_BOOK_CATEGORY (
	    CategoryID int(11) NOT NULL auto_increment,
	    CategoryCode varchar(20) NOT NULL,
	    CategoryName varchar(255) NOT NULL,
	    Language int(1),
	    RecordStatus int(1) DEFAULT 1,
	    DisplayOrder int(11),
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (CategoryID),
	    INDEX CategoryCode (CategoryCode),
	    INDEX RecordStatus (RecordStatus),
	    INDEX Language (Language)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_FORM_RECOMMEND_BOOK_MAPPING (
	    RecommendBookID int(11) NOT NULL auto_increment,
	    BookID int(11) NOT NULL,
	    YearID int(11) NOT NULL,
	    AcademicYearID int(11) NOT NULL,
	    DateInput datetime,
	    InputBy int(11) ,
	    PRIMARY KEY (RecommendBookID),
	    INDEX AcademicYearYearID (YearID ,AcademicYearID),
	    INDEX AcademicYearBookID (BookID, AcademicYearID),
	    INDEX AcademicYearID (AcademicYearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_STUDENT_READING_TARGET",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_STUDENT_READING_TARGET (
	    ReadingTargetID int(11) NOT NULL auto_increment,
	    TargetName varchar(255),
	    StartDate date,
	    EndDate date,
	    ReadingRequiredCh int(2),
	    BookReportRequiredCh int(2),
	    ReadingRequiredEn int(2),
	    BookReportRequiredEn int(2),
	    ReadingRequiredOthers int(2),
	    AcademicYearID int(11) NOT NULL,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (ReadingTargetID),
	    INDEX RecordStatus (RecordStatus),
	    INDEX AcademicYearID (AcademicYearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_STUDENT_READING_TARGET_CLASS_MAPPING (
	    TargetClassMappingID int(11) NOT NULL auto_increment,
	    ReadingTargetID int(11) NOT NULL,
	    ClassID int(11) NOT NULL,
	    DateInput datetime,
	    InputBy int(11) ,
	    PRIMARY KEY (TargetClassMappingID),
	   INDEX ReadingTargetID (ReadingTargetID),
	   INDEX ClassID (ClassID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_READING_RECORD",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_READING_RECORD (
	    ReadingRecordID int(11) NOT NULL auto_increment,
	    UserID int(11) NOT NULL,
	    BookID int(11) NOT NULL,
	    StartDate date,
	    FinishedDate date,
	    NumberOfHours float,
	    Progress float,
	    AcademicYearID int(11) NOT NULL,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (ReadingRecordID),
	    INDEX UserBookID (AcademicYearID ,UserID, BookID),
	    INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_STUDENT_ASSIGNED_READING",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_STUDENT_ASSIGNED_READING (
	    AssignedReadingID int(11) NOT NULL auto_increment,
	    BookID int(11) NOT NULL,
	    AssignedReadingName varchar(255),
	    Description text,
	    Attachment varchar(255),
	    AnswerSheet mediumtext,
	    BookReportRequired int(1),
	    AcademicYearID int(11) NOT NULL,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (AssignedReadingID),
	    INDEX RecordStatus (RecordStatus),
	    INDEX AcademicYearID (AcademicYearID),
	    INDEX BookID (BookID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_STUDENT_ASSIGNED_READING_TARGET (
	    AssignedReadingTargetID int(11) NOT NULL auto_increment,
	    AssignedReadingID int(11) NOT NULL,
	    ClassID int(11) NOT NULL,
	    DisplayOrder int(11),
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (AssignedReadingTargetID),
	    INDEX ClassID (ClassID),
	    INDEX AssignedReadingID (AssignedReadingID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_BOOK_REPORT",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_BOOK_REPORT (
	    BookReportID int(11) NOT NULL auto_increment,
	    ReadingRecordID int(11) NOT NULL,
	    Attachment varchar(255),
	    AnswerSheet mediumtext,
	    Grade varchar(5),
	    Mark float,
	    TeacherComment varchar(255),
	    Recommend int(1),
	    IsEmbed int(1),
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (BookReportID),
	    INDEX ReadingRecordID (ReadingRecordID),
	    INDEX RecordStatus (RecordStatus),
	    INDEX Recommend (Recommend)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_BOOK_REPORT_COMMENT",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_BOOK_REPORT_COMMENT (
	    BookReportCommentID int(11) NOT NULL auto_increment,
	    BookReportID int(11) NOT NULL,
	    UserID int(11) NOT NULL,
	    Comment mediumtext,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (BookReportCommentID),
	    INDEX ReportUserID (BookReportID,UserID),
	    INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_AWARD_SCHEME",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_AWARD_SCHEME (
	    AwardSchemeID int(11) NOT NULL auto_increment,
	    AwardName varchar(255),
	    AwardType int(1),
	    NumberOfWinners int(5),
	    Language int(1),
	    Target varchar(255),
	    MinBookRead int(5),
	    MinReportSubmit int(5),
	    StartDate date,
	    EndDate date,
	    ResultPublishDate date,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (AwardSchemeID),
	    INDEX RecordStatus (RecordStatus),
	    INDEX Language (Language)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_AWARD_SCHEME_TARGET_CLASS",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_AWARD_SCHEME_TARGET_CLASS (
	    AwardTargetID int(11) NOT NULL auto_increment,
	    AwardSchemeID int(11) NOT NULL,
	    ClassID int(11) NOT NULL,
	    AcademicYearID int(11) NOT NULL,
	    DateInput datetime,
	    InputBy int(11) ,
	    PRIMARY KEY (AwardTargetID),
	    INDEX AcademicYearSchemeID (AcademicYearID, AwardSchemeID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_AWARD_SCHEME_REQUIREMENT",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_AWARD_SCHEME_REQUIREMENT (
	    RequirementID int(11) NOT NULL auto_increment,
	    AwardSchemeID int(11) NOT NULL,
	    CategoryID int(11) NOT NULL,
	    MinBookRead int(5),
	    DateInput datetime,
	    InputBy int(11) ,
	    PRIMARY KEY (RequirementID),
	    INDEX SchemeCategoryID (AwardSchemeID, CategoryID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_AWARD_SCHEME_WINNER",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_AWARD_SCHEME_WINNER (
	   AwardSchemeWinnerID int(11) NOT NULL auto_increment,
	   AwardSchemeID int(11) NOT NULL,
	   WinnerID int(11),
	   AcademicYearID int(11) NOT NULL,
	   DateInput datetime,
	   InputBy int(11) ,
	   PRIMARY KEY (AwardSchemeWinnerID),
	   INDEX AcademicYearSchemeID(AcademicYearID,AwardSchemeID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_FORUM",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_FORUM (
	    ForumID int(11) NOT NULL auto_increment,
	    ForumName varchar(255) NOT NULL,
	    Description text,
	    DisplayOrder int(11),
	    TopicNeedApprove int(1) DEFAULT 1,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (ForumID),
	    INDEX  RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_FORUM_TARGET_CLASS",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_FORUM_TARGET_CLASS (
	    ForumTargetID int(11) NOT NULL auto_increment,
	    ForumID int(11) NOT NULL,
	    ClassID int(11) NOT NULL,
	    DateInput datetime,
	    InputBy int(11) ,
	    PRIMARY KEY (ForumTargetID),
	    INDEX ClassID (ClassID),
	    INDEX ForumID (ForumID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_FORUM_TOPIC",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_FORUM_TOPIC (
	    ForumTopicID int(11) NOT NULL auto_increment,
	    ForumID int(11) NOT NULL,
	    UserID int(11) NOT NULL,
	    TopicSubject varchar(255),
	    Message mediumtext,
	    OnTop int(1) DEFAULT 0,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (ForumTopicID),
	    INDEX ForumUserID (ForumID, UserID),
	    INDEX RecordStatus (RecordStatus),
	    INDEX OnTop (OnTop)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_FORUM_POST",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_FORUM_POST (
	    ForumPostID int(11) NOT NULL auto_increment,
	    ForumTopicID int(11) NOT NULL,
	    UserID int(11) NOT NULL,
	    PostSubject varchar(255),
	    Message mediumtext,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (ForumPostID),
	    INDEX ForumTopicUserID (ForumTopicID, UserID),
	    INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_ANNOUNCEMENT",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_ANNOUNCEMENT (
	    AnnouncementID int(11) NOT NULL auto_increment,
	    Title varchar(255) NOT NULL,
	    Message mediumtext,
	    StartDate datetime,
	    EndDate datetime,
	    Attachment varchar(255),
	    AwardSchemeID int(11) DEFAULT NULL,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11),
	    PRIMARY KEY (AnnouncementID),
	    INDEX RecordStatus (RecordStatus),
	    INDEX AwardSchemeID (AwardSchemeID )
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_ANNOUNCEMENT_TARGET_CLASS",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_ANNOUNCEMENT_TARGET_CLASS (
	    AnnouncementTargetID int(11) NOT NULL auto_increment,
	    AnnouncementID int(11) NOT NULL,
	    ClassID int(11) NOT NULL,
	    DateInput datetime,
	    InputBy int(11),
	    PRIMARY KEY (AnnouncementTargetID),
	    INDEX AnnouncementID (AnnouncementID),
	    INDEX ClassID (ClassID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_STATE",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_STATE (
	    StateID int(11) NOT NULL auto_increment,
	    StateName varchar(255) NOT NULL,
	    Description text,
	    ScoreRequired int(11),
	    ImagePath varchar(255),
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (StateID),
	    INDEX RecordStatus (RecordStatus),
	    INDEX ScoreRequired (ScoreRequired)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_SETTING",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_SETTING (
	    SettingID int(11) NOT NULL auto_increment,
	    SettingName varchar(255) NOT NULL,
	    SettingValue varchar(255),
	    DateModified datetime,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (SettingID),
	    INDEX SettingName (SettingName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_LIKE_RECORD",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_LIKE_RECORD (
	   LikeID int(11) NOT NULL auto_increment,
	   FunctionName varchar(255) NOT NULL,
	   UserID int(11) NOT NULL,
	   FunctionID int(11) NOT NULL,
	   AcademicYearID int(11) NOT NULL,
	   DateInput datetime,
	   PRIMARY KEY (LikeID),
	   INDEX AcademicYearFunctionNameID (FunctionName, FunctionID, AcademicYearID ),
	   INDEX AcademicYearUserID (UserID, AcademicYearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_ANSWER_SHEET_TEMPLATE",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_ANSWER_SHEET_TEMPLATE(
	    TemplateID int(11) NOT NULL auto_increment,
	    TemplateName varchar(255) ,
	    AnswerSheet mediumtext,
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11) ,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (TemplateID),
	    INDEX RecordStatus(RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_CLASS_BOOK_SOURCE_SETTING",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_CLASS_BOOK_SOURCE_SETTING (
	    ClassBookSourceID int(11) NOT NULL auto_increment,
	    ClassID int(11) NOT NULL,
	    BookSource int(1) NOT NULL,
	    AcademicYearID int(11) NOT NULL,
	    DateModified datetime,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (ClassBookSourceID),
	    UNIQUE KEY AcademicYearClassID (AcademicYearID, ClassID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_LOGIN_SESSION_RECORD",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_LOGIN_SESSION_RECORD (
	 	LoginSessionID int(11) NOT NULL auto_increment,
	 	UserID int(11) NOT NULL,
	 	StartTime datetime default NULL,
	 	DateModified datetime default NULL,
	 	PRIMARY KEY (LoginSessionID),
	 	KEY UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_STUDENT_ACTION_HISTORY",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_STUDENT_ACTION_HISTORY (
	   StudentActionID int(11) NOT NULL auto_increment,
	   StudentID int(11) NOT NULL,
	   ActionName varchar(255)  NOT NULL,
	   Count int(2) NOT NULL,
	   DateInput datetime NOT NULL,
	   AcademicYearID int(11) NOT NULL,
	   PRIMARY KEY (StudentActionID),
	   INDEX AcademicYearStudentID (StudentID, AcademicYearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-03-16",
	"Reading Scheme - Create table READING_GARDEN_DELETE_RECORD_LOG",
	"CREATE TABLE READING_GARDEN_DELETE_RECORD_LOG (
	     LogID int(11) NOT NULL auto_increment,
	     CallerPage text default NULL,
	     FunctionName varchar(255) default NULL,
	     DeleteSql text default NULL,
	     DateDeleted datetime default NULL,
	     DeletedBy int(11) default NULL,
	     PRIMARY KEY (LogID )
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-17",
	"iPortfolio - OEA Import OLE Mapping Temp Table",
	"CREATE TABLE IF NOT EXISTS OEA_OLE_MAPPING_IMPORT_TEMP (
		TempID int(11) NOT NULL auto_increment,
		ImportUserID int(11),
		RowNumber int(11),
		OLE_ProgramID int(11),
		OLE_AcademicYear varchar(255),
		OLE_ProgramName text,
		OLE_ProgramNature varchar(4),
		OEA_ProgramCode varchar(128),
		OEA_ProgramName text,
		OEA_AwardBearing varchar(4),
		DateInput datetime,
		PRIMARY KEY (TempID),
		KEY ImportUserIDAndRowNumber (ImportUserID, RowNumber)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store the temp csv data when importing OLE OEA mapping'"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-03-18",
	"eDisciplinev12 - add index to table DISCIPLINE_GENERAL_SETTING",
	"ALTER TABLE DISCIPLINE_GENERAL_SETTING ADD INDEX SettingName (SettingName)"
);

$sql_eClassIP_update[] = array(
	"2011-03-18",
	"School Setting > Group - add index to table INTRANET_GROUP",
	"ALTER TABLE INTRANET_GROUP ADD INDEX RecordType (RecordType)"
);

$sql_eClassIP_update[] = array(
	"2011-03-21",
	"Reading Scheme > alter table index to optimize performance",
	"ALTER TABLE READING_GARDEN_LIKE_RECORD DROP INDEX AcademicYearFunctionNameID , DROP INDEX AcademicYearUserID , ADD INDEX AcademicYearID (AcademicYearID) , ADD INDEX UserID (UserID ) , ADD INDEX FunctionID (FunctionID) , ADD INDEX FunctionName (FunctionName )"
);

$sql_eClassIP_update[] = array(
	"2011-03-22",
	"add index to table DISCIPLINE_NEW_LEAF_SCHEME",
	"alter table DISCIPLINE_NEW_LEAF_SCHEME add index CategoryID (CategoryID)"
);

$sql_eClassIP_update[] = array(
	"2011-03-22",
	"add index to table DISCIPLINE_ACCU_RECORD",
	"alter table DISCIPLINE_ACCU_RECORD ADD INDEX UpgradedRecordID (UpgradedRecordID), ADD INDEX RecordType (RecordType), ADD INDEX RecordStatus (RecordStatus), ADD INDEX AcademicYearID (AcademicYearID), ADD INDEX YearTermID (YearTermID), ADD INDEX fromPPC (fromPPC)"
);

$sql_eClassIP_update[] = array(
	"2011-03-22",
	"add index to table DISCIPLINE_MERIT_RECORD",
	"alter table DISCIPLINE_MERIT_RECORD ADD INDEX AcademicYearID (AcademicYearID), ADD INDEX YearTermID (YearTermID), ADD INDEX StudentID (StudentID), ADD INDEX ProfileMeritType (ProfileMeritType), ADD INDEX RecordType (RecordType), ADD INDEX RecordStatus (RecordStatus), ADD INDEX ReleaseStatus (ReleaseStatus), ADD INDEX fromPPC (fromPPC)"
);

$sql_eClassIP_update[] = array(
	"2011-03-23",
	"add index to table INTRANET_GROUPANNOUNCEMENT",
	"ALTER TABLE INTRANET_GROUPANNOUNCEMENT Add INDEX AnnouncementID (AnnouncementID)"
);

$sql_eClassIP_update[] = array(
	"2011-03-23",
	"add index to table DISCIPLINE_NEW_LEAF_SCHEME",
	"ALTER TABLE DISCIPLINE_NEW_LEAF_SCHEME Add INDEX PeriodID (PeriodID)"
);

$sql_eClassIP_update[] = array(
	"2011-03-23",
	"add index to table CALENDAR_CALENDAR_VIEWER",
	"ALTER TABLE CALENDAR_CALENDAR_VIEWER ADD INDEX CalID (CalID)"
);

$sql_eClassIP_update[] = array(
	"2011-03-23",
	"add index to table CALENDAR_CALENDAR_VIEWER",
	"ALTER TABLE CALENDAR_CALENDAR_VIEWER ADD INDEX UserID (UserID)"
);

$sql_eClassIP_update[] = array(
	"2011-03-23",
	"add index to table CALENDAR_CALENDAR_VIEWER",
	"ALTER TABLE CALENDAR_CALENDAR_VIEWER ADD INDEX GroupID (GroupID)"
);

$sql_eClassIP_update[] = array(
	"2011-03-23",
	"add index to table CALENDAR_CALENDAR_VIEWER",
	"ALTER TABLE CALENDAR_CALENDAR_VIEWER ADD INDEX CalIDUserID (CalID,UserID)"
);

$sql_eClassIP_update[] = array(
	"2011-03-24",
	"add columns to table REPAIR_SYSTEM_RECORD, use 'Setting > Campus' as the location info in 'Repair System'",
	"ALTER TABLE REPAIR_SYSTEM_RECORDS ADD COLUMN LocationBuildingID int(11) NOT NULL default 0 AFTER CategoryID, ADD COLUMN LocationLevelID int(11) NOT NULL default 0 AFTER LocationBuildingID, ADD INDEX LocationBuildingID (LocationBuildingID), ADD INDEX LocationLevelID (LocationLevelID)"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-03-29",
	"iPortfolio - Subject Full Mark Table",
	"CREATE TABLE IF NOT EXISTS ASSESSMENT_SUBJECT_FULLMARK (
		FullMarkID int(11) NOT NULL auto_increment,
		SubjectID int(11) Default Null,
		YearID int(8) Default Null,
		AcademicYearID int(8) Default Null,
		FullMarkInt int(4) Default Null,
		FullMarkGrade varchar(4) Default Null,
		DateInput datetime Default Null,
		InputBy int(8) Default Null,
		DateModified datetime Default Null,
		LastModifiedBy int(8) Default Null,
		PRIMARY KEY (FullMarkID),
		UNIQUE KEY YearFormSubject (AcademicYearID, YearID, SubjectID),
		KEY FormSubject (YearID, SubjectID),
		KEY Subject (SubjectID),
		KEY LastModifiedBy (LastModifiedBy)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Store the Subject Full mark of each Form of each Academic Year'"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-03-30",
	"eDiscipline - Log the agreement to use Data Transfer to WebSAMS for eDiscipline",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_WEBSAMS_AGREE_LOG (
		LogID int(11) NOT NULL auto_increment,
		UserID int(11) Default Null,
		DateInput datetime Default Null,
		PRIMARY KEY (LogID),
		KEY AgreedUserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Log the agreement to use Data Transfer to WebSAMS for eDiscipline'"
);

$sql_eClassIP_update[] = array(
	"2011-03-30",
	"iMail Gamma/Plus - Create table MAIL_REMOVAL_LOG for batch removal function",
	"CREATE TABLE IF NOT EXISTS MAIL_REMOVAL_LOG(
		RecordID int(11) NOT NULL auto_increment,
		UserEmail varchar(255) NOT NULL,
		MailUID int(11) NOT NULL,
		MailBox varchar(255) NOT NULL,
		ArrivalDate datetime NOT NULL,
		FromEmail varchar(255),
		Subject varchar(255),
		RawMessageSource mediumtext,
		RecordStatus int(1),
		RecordType int(1) default 0,
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY(RecordID),
		UNIQUE KEY (UserEmail,MailBox,MailUID),
		INDEX IUserEmail(UserEmail),
		INDEX IMailBox(MailBox),
		INDEX IMailUID(MailUID),
		INDEX IArrivalDate(ArrivalDate),
		INDEX IFromEmail(FromEmail),
		INDEX ISubject(Subject),
		INDEX IRecordStatus(RecordStatus),
		INDEX IRecordType(RecordType)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-04-13",
	"POLL - add choice K to Z",
	"ALTER TABLE INTRANET_POLLING 
	ADD COLUMN AnswerK varchar(255) default NULL,
	ADD COLUMN AnswerL varchar(255) default NULL,
	ADD COLUMN AnswerM varchar(255) default NULL,
	ADD COLUMN AnswerN varchar(255) default NULL,
	ADD COLUMN AnswerO varchar(255) default NULL,
	ADD COLUMN AnswerP varchar(255) default NULL,
	ADD COLUMN AnswerQ varchar(255) default NULL,
	ADD COLUMN AnswerR varchar(255) default NULL,
	ADD COLUMN AnswerS varchar(255) default NULL,
	ADD COLUMN AnswerT varchar(255) default NULL,
	ADD COLUMN AnswerU varchar(255) default NULL,
	ADD COLUMN AnswerV varchar(255) default NULL,
	ADD COLUMN AnswerW varchar(255) default NULL,
	ADD COLUMN AnswerX varchar(255) default NULL,
	ADD COLUMN AnswerY varchar(255) default NULL,
	ADD COLUMN AnswerZ varchar(255) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-04-13",
	"eHomework - View Group",
	"CREATE TABLE IF NOT EXISTS INTRANET_HOMEWORK_VIEWER_GROUP_MEMBER (
		GroupMemberID int(11) NOT NULL auto_increment,
		UserID int(11) Default 0 NOT NULL,
		DateInput datetime Default Null,
		PRIMARY KEY (GroupMemberID),
		KEY UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-04-20",
	"eEnrolment - Drop unique key (GroupID, UserID)",
	"ALTER TABLE INTRANET_USERGROUP DROP INDEX UserGroupID"
);

$sql_eClassIP_update[] = array(
	"2011-04-21",
	"iMail Gamma/Plus - ALTER TABLE MAIL_REMOVAL_LOG DROP COLUMN RawMessageSource",
	"ALTER TABLE MAIL_REMOVAL_LOG DROP COLUMN RawMessageSource"
);

$sql_eClassIP_update[] = array(
	"2011-04-21",
	"iMail Gamma/Plus - ALTER TABLE MAIL_REMOVAL_LOG ADD COLUMN MessageID varchar(255) AFTER Subject",
	"ALTER TABLE MAIL_REMOVAL_LOG ADD COLUMN MessageID varchar(255) AFTER Subject"
);

$sql_eClassIP_update[] = array(
	"2011-04-21",
	"iMail Gamma/Plus - ALTER TABLE MAIL_REMOVAL_LOG ADD COLUMN RuleID int(11) NOT NULL AFTER RecordID",
	"ALTER TABLE MAIL_REMOVAL_LOG ADD COLUMN RuleID int(11) NOT NULL AFTER RecordID"
);

$sql_eClassIP_update[] = array(
	"2011-04-21",
	"iMail Gamma/Plus - ALTER TABLE MAIL_REMOVAL_LOG ADD INDEX IRuleID(RuleID)",
	"ALTER TABLE MAIL_REMOVAL_LOG ADD INDEX IRuleID(RuleID)"
);

$sql_eClassIP_update[] = array(
	"2011-04-21",
	"iMail Gamma/Plus - CREATE TABLE MAIL_REMOVAL_RULES",
	"CREATE TABLE IF NOT EXISTS MAIL_REMOVAL_RULES(
	  RecordID int(11) NOT NULL auto_increment,
	  StartDate date NOT NULL,
	  EndDate date NOT NULL,
	  SubjectKeyword varchar(255) NOT NULL,
	  Sender varchar(255) NOT NULL,
	  RecordStatus int(1),
	  DateInput datetime,
	  DateModified datetime,
	  PRIMARY KEY (RecordID),
	  INDEX IStartDate(StartDate),
	  INDEX IEndDate(EndDate),
	  INDEX ISubject(SubjectKeyword),
	  INDEX ISender(Sender),
	  INDEX IRecordStatus(RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2011-04-21",
	"iMail Gamma/Plus - CREATE TABLE MAIL_REMOVAL_MESSAGE_LOG",
	"CREATE TABLE IF NOT EXISTS MAIL_REMOVAL_MESSAGE_LOG(
	  MessageID varchar(255) NOT NULL,
	  RawMessageSource mediumtext,
	  DateInput datetime,
	  PRIMARY KEY (MessageID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);

$sql_eClassIP_update[] = array(
	"2011-04-26",
	"Archive User Account - Store who do the \"archive\" action",
	"ALTER TABLE INTRANET_ARCHIVE_USER ADD COLUMN DateArchive datetime, ADD COLUMN ArchivedBy int(11) not null default 0"
);

$sql_eClassIP_update[] = array(
	"2011-05-11",
	"eDiscipline - Study Score wanrning rule Setting",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_SUBSCORE_GRADE_RULE (
		MinConductScore int(11) default NULL,
		GradeChar varchar(100) default NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default 1,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		UNIQUE KEY MinConductScore (MinConductScore),
		INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-11",
	"add column \"GradeChar\" to DISCIPLINE_STUDENT_SUBSCORE_BALANCE",
	"ALTER TABLE DISCIPLINE_STUDENT_SUBSCORE_BALANCE ADD COLUMN GradeChar varchar(100) AFTER SubScore"
);

$sql_eClassIP_update[] = array(
	"2011-05-13",
	"drop index RecordType in table DISCIPLINE_SUBSCORE_SETTING",
	"ALTER TABLE DISCIPLINE_SUBSCORE_SETTING drop index RecordType"
);

$sql_eClassIP_update[] = array(
	"2011-05-19",
	"create table IF NOT EXISTS DISCIPLINE_SUBSCORE_GRADE_RULE - for SubScore",
	"CREATE TABLE DISCIPLINE_SUBSCORE_GRADE_RULE (
		MinConductScore int(11) default NULL,
		GradeChar varchar(100) default NULL,
		RecordType int(11) default NULL,
		RecordStatus int(11) default '1',
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		UNIQUE KEY MinConductScore (MinConductScore),
		INDEX RecordStatus (RecordStatus)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-19",
	"add column \"Remark\" to for Vaiance Handling Notice",
	"ALTER TABLE INVENTORY_VARIANCE_HANDLING_REMINDER ADD COLUMN Remark text"
);

$sql_eClassIP_update[] = array(
	"2011-05-23",
	"Reading Scheme - add table to award scheme level",
	"CREATE TABLE READING_GARDEN_AWARD_SCHEME_LEVEL (
	    AwardLevelID int(11) NOT NULL auto_increment,
	    AwardSchemeID int(11) NOT NULL,
	    Level int(2),
	    LevelName varchar(255) ,
	    MinBookRead int(5),
	    MinReportSubmit int(5),
	    PRIMARY KEY (AwardLevelID),
	    KEY AwardSchemeID (AwardSchemeID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-23",
	"Reading Scheme - improve award scheme to support multi award in 1 scheme",
	"ALTER TABLE READING_GARDEN_AWARD_SCHEME_REQUIREMENT CHANGE AwardSchemeID AwardLevelID int(11)"
);

$sql_eClassIP_update[] = array(
	"2011-05-23",
	"Reading Scheme - improve award scheme to support multi award in 1 scheme",
	"ALTER TABLE READING_GARDEN_AWARD_SCHEME_WINNER ADD  AwardLevelID int(11) AFTER AwardSchemeID "
);

$sql_eClassIP_update[] = array(
	"2011-05-23",
	"Reading Scheme - improve award scheme to support multi award in 1 scheme",
	"ALTER TABLE READING_GARDEN_STUDENT_ASSIGNED_READING ADD  WritingLimit int(11) DEFAULT -1 AFTER BookReportRequired  "
);

$sql_eClassIP_update[] = array(
	"2011-05-24",
	"Conduct Mark Balance table - Set new Unique Index for DISCIPLINE_STUDENT_CONDUCT_BALANCE",
	"ALTER TABLE DISCIPLINE_STUDENT_CONDUCT_BALANCE ADD UNIQUE INDEX StudentIDYearAndTerm (StudentID, AcademicYearID, YearTermID, IsAnnual), drop index StudentIDYear"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Access Right Group ",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP (
		GroupID int(11) NOT NULL auto_increment,
		GroupTitle varchar(200) default NULL,
		Description text,
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (GroupID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Access Right Group Member",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_MEMBER (
		GroupID int(11) NOT NULL default '0',
		UserID int(11) NOT NULL default '0',
		IsAdmin int(1) NOT NULL default '0',
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		PRIMARY KEY  (GroupID,UserID),
		INDEX IsAdmin (IsAdmin),
		INDEX GroupID (GroupID),
		INDEX UserID (UserID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Access Right Group Setting",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_SETTING (
		GroupSettingID int(11) NOT NULL auto_increment,
		GroupID int(11) NOT NULL default '0',
		Role varchar(50) NOT NULL COMMENT 'value : Admin / Member',
		FileType varchar(10) NOT NULL,
		Action varchar(50) NOT NULL,
		DateInput datetime default NULL,
		PRIMARY KEY  (GroupSettingID),
		INDEX GroupID (GroupID),
		INDEX Role (Role),
		INDEX Action (Action),
		INDEX FileType (FileType)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Admin Document",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD (
		DocumentID int(11) NOT NULL auto_increment,
		Title varchar(255) NOT NULL,
		FileFolderName varchar(200) NOT NULL,
		FileHashName varchar(200) NOT NULL,
		FileExtension varchar(10) NOT NULL,
		Description text,
		VersionNo int(11) NOT NULL default '1',
		SizeInBytes int(30) NOT NULL default '0',
		IsLatestFile int(1) NOT NULL default '1',
		ParentFolderID int(11) NOT NULL default '0',
		IsFolder int(1) NOT NULL default '0',
		GroupID int(11) NOT NULL default '0',
		Thread int(11) NOT NULL default '0',
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		DateModified datetime NOT NULL,
		ModifyBy int(11) NOT NULL default '0',
		RecordStatus int(1) NOT NULL default '1',
		PRIMARY KEY  (DocumentID),
		INDEX ParentFolderID (ParentFolderID),
		INDEX IsFolder (IsFolder),
		INDEX VersionNo (VersionNo),
		INDEX IsLatestFile (IsLatestFile),
		INDEX GroupID (GroupID),
		INDEX RecordStatus (RecordStatus),
		INDEX SizeInBytes (SizeInBytes),
		INDEX Thread (Thread),
		INDEX FileExtension (FileExtension)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Admin Document Like Record",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ADMIN_DOCUMENT_LIKE_RECORD (
		LikeID int(11) NOT NULL auto_increment,
		UserID int(11) NOT NULL,
		DocumentID int(11) NOT NULL,
		DateInput datetime default NULL,
		PRIMARY KEY  (LikeID),
		INDEX UserID (UserID),
		INDEX DocumentID (DocumentID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Admin Document Tag Record",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ADMIN_DOCUMENT_TAG_USAGE (
		TagUsageID int(11) NOT NULL auto_increment,
		DocumentID int(11) NOT NULL,
		TagID int(11) NOT NULL,
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		PRIMARY KEY  (TagUsageID),
		INDEX DocumentID (DocumentID),
		INDEX TagID (TagID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Subject Resources Record",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RECORD (
		SubjectResourceID int(11) NOT NULL auto_increment,
		Title varchar(255) NOT NULL,
		Description text,
		Url varchar(255) default NULL,
		FileName varchar(200) default NULL,
		FileHashName varchar(200) default NULL COMMENT 'IF file, then encrypt',
		SourceExtension varchar(10) default NULL,
		SizeInBytes int(30) NOT NULL default '0',
		SubjectID int(11) NOT NULL,
		LikeCount int(11) NOT NULL default '0',
		RecordStatus int(1) NOT NULL default '1',
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		DateModified datetime default NULL,
		ModifyBy int(11) default NULL,
		PRIMARY KEY  (SubjectResourceID),
		INDEX SubjectID (SubjectID),
		INDEX LikeCount (LikeCount),
		INDEX SizeInBytes (SizeInBytes),
		INDEX RecordStatus (RecordStatus),
		INDEX SourceExtension (SourceExtension)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Subject Resources Like Record",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_LIKE_RECORD (
		LikeID int(11) NOT NULL auto_increment,
		UserID int(11) NOT NULL,
		SubjectResourceID int(11) NOT NULL,
		DateInput datetime default NULL,
		PRIMARY KEY  (LikeID),
		INDEX UserID (UserID),
		INDEX SubjectResourceID (SubjectResourceID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Subject Resources Tag Record",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_TAG_USAGE (
		TagUsageID int(11) NOT NULL auto_increment,
		SubjectResourceID int(11) NOT NULL,
		TagID int(11) NOT NULL,
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		PRIMARY KEY  (TagUsageID),
		INDEX SubjectResourceID (SubjectResourceID),
		INDEX TagID (TagID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Digital Archive - Subject Resources Class Level Relation",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_AND_CLASSLEVEL_RELATION (
		SubjectResourceID int(11) NOT NULL default '0',
		ClassLevelID int(11) NOT NULL default '0',
		DateInput datetime default NULL,
		InputBy int(11) NOT NULL default '0',
		PRIMARY KEY  (SubjectResourceID,ClassLevelID),
		INDEX SubjectResourceID (SubjectResourceID),
		INDEX ClassLevelID (ClassLevelID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Add column NumberOfUnit, NumberOfStudents, DateModified in Table INTRANET_MODULE_LICENSE",
	"ALTER TABLE INTRANET_MODULE_LICENSE
		ADD COLUMN NumberOfStudents int(11) DEFAULT 0 AFTER NumberOfLicense,
		ADD COLUMN NumberOfUnit int(11) DEFAULT 0 AFTER NumberOfLicense,
		ADD COLUMN DateModified datetime DEFAULT NULL AFTER InputDate"
);

$sql_eClassIP_update[] = array(
	"2011-05-25",
	"Add column Unit, DateModified in Table INTRANET_MODULE_USER",
	"ALTER TABLE INTRANET_MODULE_USER
		ADD COLUMN Unit int(11) DEFAULT 0 AFTER UserID,
		ADD COLUMN DateModified datetime DEFAULT NULL AFTER InputDate"
);

$sql_eClassIP_update[] = array(
	"2011-05-26",
	"Reading Scheme , Add OnlineWriting to READING_GARDEN_BOOK_REPORT",
	"ALTER TABLE READING_GARDEN_BOOK_REPORT ADD OnlineWriting MEDIUMTEXT AFTER AnswerSheet"
);

$sql_eClassIP_update[] = array(
	"2011-05-27",
	"Reading Scheme - Create table READING_GARDEN_REPORT_STICKER",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_REPORT_STICKER (
	    StickerID int(11) NOT NULL auto_increment,
	    ImagePath varchar(255),
	    RecordStatus int(1) DEFAULT 1,
	    DateInput datetime,
	    DateModified datetime,
	    InputBy int(11),
	    LastModifiedBy int(11),
	    PRIMARY KEY (StickerID),
	    INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-27",
	"Reading Scheme - Create table READING_GARDEN_REPORT_STICKER_MAPPING",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_REPORT_STICKER_MAPPING (
	  StickerMappingID int(11) NOT NULL auto_increment,
	  BookReportID int(11) NOT NULL,
	  StickerID int(11) NOT NULL,
	  DateInput datetime,
	  InputBy int(11), 
	  PRIMARY KEY(StickerMappingID),
	  INDEX BookReportIDIndex(BookReportID),
	  INDEX StickerIDIndex(StickerID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-05-27",
	"Reading Scheme - Create table READING_GARDEN_COMMENT_BANK",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_COMMENT_BANK(
	  CommentID int(11) NOT NULL auto_increment,
	  Comment varchar(255),
	  RecordStatus int(1) DEFAULT 1,
	  DateInput datetime,
	  DateModified datetime,
	  InputBy int(11),
	  LastModifiedBy int(11),
	  PRIMARY KEY (CommentID),
	  INDEX RecordStatusIndex(RecordStatus) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-06-03",
	"iPortfolio (OEA) - OEA Student Academic Performance",
	"CREATE TABLE IF NOT EXISTS OEA_STUDENT_ACADEMIC (
	     StudentAcademicID int(11) NOT NULL auto_increment,
	     StudentID int(11) NOT NULL,
	     Percentile text,
		 OverallRating text,
	     RecordStatus int(2) NOT NULL default '0',
	     InputDate datetime default NULL,
	     InputBy int(11) default NULL,
	     ApprovalDate datetime default NULL,
	     ApprovedBy int(11) default NULL,
	     ModifyDate datetime default NULL,
	     ModifiedBy int(11) default NULL,
	     PRIMARY KEY (StudentAcademicID),
	     KEY StudentID (StudentID),
	     KEY RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='STORE OEA Student ACADEMIC'"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-06-04",
	"eInventory > item add creator field",
	"ALTER TABLE INVENTORY_ITEM ADD CreatedBy int(11);"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-06-09",
	"iPortfolio (OEA) - OEA Subject Info",
	"CREATE TABLE IF NOT EXISTS OEA_SUBJECT_INFO (
	     SubjectInfoID int(11) NOT NULL auto_increment,
	     SubjectID int(11) NOT NULL,
	     JupasSubjectCode varchar(64),
		 RecordStatus int(2) NOT NULL default 0,
	     InputDate datetime default NULL,
	     InputBy int(11) default NULL,
	     ModifyDate datetime default NULL,
	     ModifiedBy int(11) default NULL,
	     PRIMARY KEY (SubjectInfoID),
	     KEY SubjectID (SubjectID),
	     KEY JupasSubjectCode (JupasSubjectCode)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='STORE OEA Subject Info'"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-06-13",
	"Repair System - add column 'ArchiveStatus' in REPAIR_SYSTEM_RECORDS",
	"ALTER TABLE REPAIR_SYSTEM_RECORDS ADD ArchivedBy int(11), ADD DateArchived datetime"
);

$sql_eClassIP_update[] = array(
	"2011-06-14",
	"Digital Archive - create table for storing last VIEW time of group",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ADMIN_DOC_GROUP_SESSION (
		GroupSessionID int(11) NOT NULL AUTO_INCREMENT,
		GroupID int(11) NOT NULL,
		UserID int(11) NOT NULL,
		StartTime datetime,
		PRIMARY KEY (GroupSessionID),
		KEY GroupID (GroupID),
		KEY UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-06-13",
	"Digital Archive - add column Group Code in DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP",
	"ALTER TABLE DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP ADD Code varchar(50) NOT NULL AFTER Description, ADD INDEX Code (Code)"
);

$sql_eClassIP_update[] = array(
	"2011-06-20",
	"Subject Group - Remove UNIQUE Index to Cater Using same subject group code in different term",
	"ALTER TABLE SUBJECT_TERM_CLASS DROP INDEX ClassCode"
);

$sql_eClassIP_update[] = array(
	"2011-06-22",
	"Subject Group - add index ClassCode for better performance",
	"ALTER TABLE SUBJECT_TERM_CLASS ADD INDEX ClassCode1 (ClassCode)"
);

$sql_eClassIP_update[] = array(
	"2011-06-23",
	"INTRANET_USER APPLICANT NUMBER OF JUPAS",
	"ALTER TABLE INTRANET_USER ADD `HKJApplNo` int(10) default NULL"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-06-23",
	"iPortfolio - Add index to Academic Table to improve performance",
	"Alter Table ASSESSMENT_STUDENT_SUBJECT_RECORD
		Drop Index UserID,
		Drop Index SubjectCode,
		Add Index StudentSubjectAcademicYearScore (UserID, SubjectID, AcademicYearID, YearTermID),
		Add Index SubjectAcademicYearScore (SubjectID, AcademicYearID, YearTermID),
		Add Index AcademicYearScore (AcademicYearID, YearTermID),
		Add Index SubjectCode (SubjectCode)"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-06-23",
	"For new function - Message Center -> Mass Mailing",
	"CREATE TABLE IF NOT EXISTS INTRANET_MASS_MAILING (
	     MassMailingID int(11) NOT NULL auto_increment,	     
	     EmailTitle varchar(128),
		 ShowContentIn int(2) NOT NULL default 1,
		 EmailBody LONGBLOB,
		 CSVFile LONGBLOB,
		 HTMLFile LONGBLOB,
		 RecipientStudent boolean default 1,
		 RecipientParent boolean default 0,
		 SendAsAdmin boolean default 0,
		 LastSenderEmail varchar(255),
		 NoOfCopySent int(8),
		 RecordStatus int(2) NOT NULL default 0,
	     InputBy int(11) default NULL,
	     InputDate datetime default NULL,
	     ModifyDate datetime default NULL,
	     ModifiedBy int(11) default NULL,
	     PRIMARY KEY (MassMailingID),
	     INDEX InputBy (InputBy)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);



$sql_eClassIP_update[] = array(
	"2011-06-23",
	"For new function - Message Center -> Mass Mailing",
	"CREATE TABLE IF NOT EXISTS INTRANET_MASS_MAILING_LOG (
	     MassMailingLogID int(11) NOT NULL auto_increment,	     
	     MassMailingID int(11),
		 RecipientName varchar(255),
		 RecipientEmail varchar(255),
		 Class varchar(64),
		 ClassNumber int(8),
		 SenderEmail varchar(255),
		 EmailBody LONGBLOB,
		 Attachment LONGBLOB,
	     InputBy int(11) default NULL,
	     InputDate datetime default NULL,
	     PRIMARY KEY (MassMailingLogID),
	     INDEX MassMailingID (MassMailingID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-06-30",
	"Message Center -> Mass Mailing",
	"ALTER TABLE INTRANET_MASS_MAILING_LOG CHANGE ClassNumber ClassNumber int(8)"
);

$sql_eClassIP_update[] = array(
	"2011-06-30",
	"Message Center -> Mass Mailing",
	"ALTER TABLE INTRANET_MASS_MAILING_LOG ADD InputBy int(11) default NULL after Attachment"
);


$sql_eClassIP_update[] = array(
	"2011-06-30",
	"Message Center -> Mass Mailing",
	"ALTER TABLE INTRANET_MASS_MAILING_LOG ADD IsParent boolean default false after RecipientEmail"
);

$sql_eClassIP_update[] = array(
	"2011-07-06",
	"School Calendar -> add skip SAT setting",
	"alter table INTRANET_EVENT add isSkipSAT tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2011-07-06",
	"School Calendar -> add skip SUN setting",
	"alter table INTRANET_EVENT add isSkipSUN tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2011-07-07",
	"Add index for INTRANET_GROUPANNOUNCEMENT",
	"ALTER TABLE INTRANET_GROUPANNOUNCEMENT Add INDEX AnnouncementID (AnnouncementID)"
);

$sql_eClassIP_update[] = array(
	"2011-07-07",
	"Special Timetable Settings > Basic Settings Info",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS (
	     SpecialTimetableSettingsID int(11) NOT NULL auto_increment,	     
	     TimezoneID int(11) default Null,
		 TimetableID int(11) default Null,
		 BgColor varchar(64) default Null,
		 ReportType varchar(64) default Null,
		 InputBy int(11) default NULL,
	     InputDate datetime default NULL,
	     ModifiedBy int(11) default NULL,
		 ModifyDate datetime default NULL,
	     PRIMARY KEY (SpecialTimetableSettingsID),
		 Index TimezoneIDTimetableID (TimezoneID, TimetableID),
		 Index TimetableID (TimetableID),
	     INDEX InputBy (InputBy),
		 INDEX ModifiedBy (ModifiedBy)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-07-07",
	"Special Timetable Settings > Repeat Day Settings Info",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMEZONE_SPECIAL_TIMETABLE_REPEAT_DAY (
	     SpecialTimetableRepeatDayID int(11) NOT NULL auto_increment,
	     SpecialTimetableSettingsID int(11) default Null,
		 RepeatDay tinyint(2) default Null,
		 InputBy int(11) default NULL,
	     InputDate datetime default NULL,
	     ModifiedBy int(11) default NULL,
		 ModifyDate datetime default NULL,
	     PRIMARY KEY (SpecialTimetableRepeatDayID),
		 Index SpecialTimetableSettingsID (SpecialTimetableSettingsID),
		 Index RepeatDay (RepeatDay),
	     INDEX InputBy (InputBy),
		 INDEX ModifiedBy (ModifiedBy)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-07-07",
	"Special Timetable Settings > Store the Special Timetable Settings in the Date's Cycle Day Info Table",
	"ALTER TABLE INTRANET_CYCLE_DAYS Add SpecialTimetableSettingsID int(11) default Null AFTER TextShort, Add Index SpecialTimetableSettingsID (SpecialTimetableSettingsID)"
);

$sql_eClassIP_update[] = array(
	"2011-07-11",
	"TimeZone > Add fields to store PeriodID(TimeZoneID) and CycleDay",
	"ALTER TABLE INTRANET_CYCLE_DAYS Add PeriodID int(11) default Null, Add Index PeriodID (PeriodID), Add CycleDay int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2011-07-11",
	"TimeZone > Add fields to store PeriodID(TimeZoneID) and CycleDay",
	"ALTER TABLE INTRANET_CYCLE_TEMP_DAYS_VIEW Add PeriodID int(11) default Null, Add Index PeriodID (PeriodID), Add CycleDay int(11) default Null"
);

$sql_eClassIP_update[] = array(
	"2011-07-14",
	"Special Timetable Settings > Add Table to Store Special Timetable Dates",
	"CREATE TABLE IF NOT EXISTS INTRANET_TIMEZONE_SPECIAL_TIMETABLE_DATE (
	     SpecialTimetableDateID int(11) NOT NULL auto_increment,
	     SpecialDate date Default Null,
		 SpecialTimetableSettingsID int(11) Default Null,
		 InputBy int(11) default NULL,
	     InputDate datetime default NULL,
	     PRIMARY KEY (SpecialTimetableDateID),
		 Index SpecialTimetableSettingsID (SpecialTimetableSettingsID),
		 Index SpecialDate (SpecialDate)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-07-14",
	"TimeZone > Drop field SpecialTimetableSettingsID from INTRANET_CYCLE_DAYS",
	"ALTER TABLE INTRANET_CYCLE_DAYS Drop Column SpecialTimetableSettingsID"
);

$sql_eClassIP_update[] = array(
	"2011-07-21",
	"Add Semester in table INTRANET_ENROL_EVENTINFO (defualt existing activity is Whole Year event)",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO add column YearTermID int(11) default 0 NOT NULL, add index YearTermID (YearTermID)"
);

$sql_eClassIP_update[] = array(
	"2011-07-25",
	"TimeZone > Change INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS Column From ReportType to RepeatType",
	"ALTER TABLE INTRANET_TIMEZONE_SPECIAL_TIMETABLE_SETTINGS CHANGE ReportType RepeatType varchar(64) default Null;"
);

$sql_eClassIP_update[] = array(
	"2011-07-29",
	"eBooking - Add Table to Log delete action",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_DELETE_LOG (
		LogID int(11) NOT NULL auto_increment,
		DeleteItemName VARCHAR(255) default NULL,
		DeleteBy int(11) default NULL,
		DeleteDate datetime default NULL,
		PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-08-02",
	"iTextbook - Add Table to store book details",
	"CREATE TABLE IF NOT EXISTS ITEXTBOOK_BOOK (
		BookID int(11) NOT NULL auto_increment,
		BookType varchar(255) default NULL,
		BookName varchar(255) default NULL,
		Chapters mediumtext default NULL,
		Status tinyint(1) default 0,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (BookID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-08-02",
	"iTextbook - Add Table to store distribution details",
	"CREATE TABLE IF NOT EXISTS ITEXTBOOK_DISTRIBUTION (
		DistributionID int(11) NOT NULL auto_increment,
		ModuleLicenseID int(11) default NULL,
		BookID int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (DistributionID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-08-02",
	"iTextbook - Add Table to store user distribution details",
	"CREATE TABLE IF NOT EXISTS ITEXTBOOK_DISTRIBUTION_USER (
		DistributionUserID int(11) NOT NULL auto_increment,
		DistributionID int(11) default NULL,
		UserID int(11) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (DistributionUserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2010-08-09",
	"eBooking - CREATE TABLE INTRANET_EBOOKING_BOOKING_DETAILS",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_BOOKING_DETAILS (
	  RecordID int(11) NOT NULL auto_increment,
      BookingID int(11) default NULL,
	  FacilityType int(11) default NULL,
      FacilityID int(11) default NULL,
      PIC int(8) default NULL,
      ProcessDate date default NULL,
      BookingStatus int(8) default '0',
      InputBy int(11) default NULL,
      ModifiedBy int(11) default NULL,
      RecordType int(11) default NULL,
      RecordStatus int(11) default NULL,
      CurrentStatus int(11) default '0',
      CheckInCheckOutRemark text,
      DateInput datetime default NULL,
      DateModified datetime default NULL,
	  PRIMARY KEY  (RecordID),
	  KEY BookingID (BookingID),
	  KEY FacilityTypeID (FacilityType, FacilityID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2011-08-09",
	"Alter table INTRANET_EBOOKING_RECORD - Add field to store attachment folder path",
	"ALTER TABLE INTRANET_EBOOKING_RECORD ADD Attachment varchar(255) DEFAULT NULL AFTER Remark ;"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-08-16",
	"iPortfolio - OEA Import OLE Mapping Temp Table",
	"Alter Table OEA_OLE_MAPPING_IMPORT_TEMP Add Column OLE_NumOfStudentJoined int(8) Default Null After OLE_AcademicYear"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-08-17",
	"eBooking - Add table INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING ",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING (
	 TimezoneSettingID int(11) NOT NULL auto_increment,
	 TimezoneID int(11) default Null,
	 FacilityType tinyint(2) default Null,
	 FacilityID int(11) default Null,
	 SpecialTimetableSettingsID int(11) default Null,
	 ModifiedBy int(11) default NULL,
	 DateModified datetime default NULL,
	 PRIMARY KEY (TimezoneSettingID),
	 INDEX TimezoneID (TimezoneID),
	 INDEX FacilityTypeID (FacilityType,FacilityID),
	 INDEX SpecialTimetableSettingID (SpecialTimetableSettingsID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-08-17",
	"eBooking - Add table INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT ",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_AVAILABLE_BOOKING_TIMEZONE_SETTING_TIMESLOT (
	 TimezoneTimeslotID int(11) NOT NULL auto_increment,
	 TimezoneSettingID int(11) default Null,
	 Day tinyint(2) default Null,
	 TimeslotID int(11) default Null,
	 PRIMARY KEY (TimezoneTimeslotID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);	



$sql_eClassIP_update[] = array(
	"2011-08-18",
	"improved Email Merge ",
	"ALTER TABLE INTRANET_MASS_MAILING add RecipientAddress boolean default 0 after RecipientParent"
);	


$sql_eClassIP_update[] = array(
	"2011-08-23",
	"Create table INTRANET_ENROL_STUDENT_CATEGORY_ENROL for categorized enrolment ",
	"CREATE TABLE INTRANET_ENROL_STUDENT_CATEGORY_ENROL (
     EnrolID int(11) NOT NULL auto_increment,
     StudentID int(11) NOT NULL,
     CategoryID int(11) NOT NULL,
     Max int(11) default NULL,
     Approved int(11) default NULL,
     DateInput datetime default NULL,
     DateModified datetime default NULL,
     MaxEvent int(11) default NULL,
     PRIMARY KEY (EnrolID),
     UNIQUE KEY StudentCategoryID (StudentID, CategoryID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8"
);	

$sql_eClassIP_update[] = array(
	"2011-08-26",
	"Create table INTRANET_ENROL_GRADE_RULE for category grading scheme [CRM : 2011-0616-1516-53126]",
	"CREATE TABLE INTRANET_ENROL_GRADE_RULE (
	 RuleID INT(11) NOT NULL AUTO_INCREMENT,
	 CategoryID INT(11) NOT NULL DEFAULT 0,
	 MinScore INT(11) DEFAULT 0,
	 MaxScore INT(11) DEFAULT 0,
	 GradeChar VARCHAR(50),
	 DateInput DATETIME,
	 DateModified DATETIME,
     PRIMARY KEY (RuleID),
	 INDEX CategoryID (CategoryID)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8"
);	

$sql_eClassIP_update[] = array(
	"2011-08-30",
	"Reading Scheme - Add LowerLimit to READING_GARDEN_STUDENT_ASSIGNED_READING ",
	"ALTER TABLE READING_GARDEN_STUDENT_ASSIGNED_READING ADD LowerLimit int(11) DEFAULT -1 AFTER WritingLimit  "
);

$sql_eClassIP_update[] = array(
	"2011-08-30",
	"eDiscipline - Add default notice template for conduct reminder warning ",
	"alter table DISCIPLINE_CONDUCT_SETTING add TemplateID int(11) default NULL after RecordStatus"
);


$sql_eClassIP_update[] = array(
	"2011-08-30",
	"Create table INTRANET_API_REQUEST_LOG for eClass Parent Apps log",
	"CREATE TABLE `INTRANET_API_REQUEST_LOG` (
`UserID` int(11) NOT NULL,
`LastLoginTime` datetime default NULL,
`LastRequestTime` datetime default NULL,
`LastRequestXML` text,
`APIKey` varchar(32) default NULL,
PRIMARY KEY (`UserID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1"
);


$sql_eClassIP_update[] = array(
	"2011-09-01",
	"Create table INTRANET_APP_NOTIFY_MESSAGE for ASL Parent App",
	"CREATE TABLE IF NOT EXISTS INTRANET_APP_NOTIFY_MESSAGE (
	  NotifyMessageID int(11) NOT NULL auto_increment,
	  NotifyDateTime datetime,
	  MessageTitle varchar(255),
	  MessageContent text,
	  NotifyFor varchar(255),
	  IsPublic char DEFAULT 'N' COMMENT 'Y:Yes;N:No',
	  NotifyUser varchar(255),
	  NotifyUserID int(11),
	  RecordStatus char,
	  DateInput datetime,
	  DateModified datetime,
	  PRIMARY KEY (NotifyMessageID),
	  INDEX INotifyDateTime(NotifyDateTime),
	  INDEX IMessageTitle(MessageTitle),
	  INDEX IMessageContent(MessageContent(255)),
	  INDEX INotifyFor(NotifyFor),
	  INDEX IIsPublic(IsPublic),
	  INDEX INotifyUser(NotifyUser)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-01",
	"Create table INTRANET_APP_NOTIFY_MESSAGE_TARGET for ASL Parent App",
	"CREATE TABLE IF NOT EXISTS INTRANET_APP_NOTIFY_MESSAGE_TARGET (
	  NotifyMessageTargetID int(11) NOT NULL auto_increment,
	  NotifyMessageID int(11) NOT NULL,
	  TargetType char NOT NULL COMMENT 'U for User, G for Group',
	  TargetID int(11) NOT NULL COMMENT 'UserID',
	  DateInput datetime,
	  PRIMARY KEY (NotifyMessageTargetID),
	  INDEX ITargetType(TargetType),
	  INDEX ITargetID(TargetID)
	)"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-09-05",
	"iPortfolio - Volunteer Event Table",
	"CREATE TABLE IF NOT EXISTS VOLUNTEER_EVENT (
	     EventID int(11) NOT NULL auto_increment,
	     Code varchar(255) default NULL,
	     Title varchar(255) default NULL,
	     StartDate date default NULL,
	     Organization varchar(200) default NULL,
	     Details mediumtext,
	     Remark mediumtext,
	     InputDate datetime default NULL,
	     ModifiedDate datetime default NULL,
	     ModifyBy int(11) default NULL,
	     CreatorID int(11) NOT NULL,
	     CreatorType char(2) default NULL,
	     AcademicYearID int(8) default NULL,
	     YearTermID int(8) default NULL,
	     ComeFrom smallint(6) default NULL,
	     RemoveStatus char(2) default 'N',
	     RemoveDate datetime default NULL,
	     RemovedBy int(11) default NULL,
	     PRIMARY KEY (EventID),
	     KEY RemoveStatusAndStartDateAndTitleAndCode (RemoveStatus,StartDate,Title,Code),
	     KEY RemoveStatusAndCode (RemoveStatus,Code),
	     KEY CreatorID (CreatorID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-09-05",
	"iPortfolio - Volunteer Event Participant Table",
	"CREATE TABLE IF NOT EXISTS VOLUNTEER_EVENT_USER (
	     RecordID int(11) NOT NULL auto_increment,
	     UserID int(11) NOT NULL,
	     EventID int(11) default NULL,
	     Hours float default NULL,
	     Role varchar(64) default NULL,
	     Achievement text,
	     TeacherPIC int(11) default NULL,
	     InputDate datetime default NULL,
	     InputBy int(11) default NULL,
	     ComeFrom smallint(6) default NULL,
	     ModifiedDate datetime default NULL,
	     ModifiedBy int(11) default NULL,
	     ApproveStatus tinyint(2) default '1' COMMENT '1:pending / 2:approved / 3:rejected',
	     ApprovedBy int(11) default NULL,
	     ProcessDate datetime default NULL,
	     RemoveStatus char(2) default NULL,
	     RemoveDate datetime default NULL,
	     RemovedBy int(11) default NULL,
	     PRIMARY KEY (RecordID),
	     KEY ActiveUserEvent (RemoveStatus,UserID),
	     KEY ActiveEventUser (RemoveStatus,EventID),
	     KEY UserID (UserID),
	     KEY EventID (EventID),
	     KEY TeacherPIC (TeacherPIC),
	     KEY ModifiedBy (ModifiedBy),
	     KEY ApprovedBy (ApprovedBy)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-09-07",
	"Invoice [customization] -> Group Budget",
	"CREATE TABLE IF NOT EXISTS INVOICEMGMT_GROUP_BUDGET (
		RecordID int(11) NOT NULL auto_increment,
		AcademicYearID int(8) default NULL,
		AdminGroupID int(11) default NULL,
		CategoryID int(11) default NULL,
		Budget float(8,2) default NULL,
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified datetime default NULL,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (RecordID),
		Index AcademicYearID (AcademicYearID),
		Index AdminGroupID (AdminGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-07",
	"Invoice [customization] -> Invoice Item Category",
	"CREATE TABLE IF NOT EXISTS INVOICEMGMT_ITEM_CATEGORY (
		CategoryID int(11) NOT NULL auto_increment,
		NameChi varchar(255) default NULL,
		NameEng varchar(255) default NULL,
		RecordStatus INT(1) NOT NULL DEFAULT 1,
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified datetime default NULL,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (CategoryID),
		INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-07",
	"Invoice [customization] -> Invoice main data",
	"CREATE TABLE IF NOT EXISTS INVOICEMGMT_INVOICE (
		RecordID int(11) NOT NULL auto_increment,
		AcademicYearID int(8) default NULL,
		InvoiceDate date default NULL,
		Company varchar(255) default NULL,
		InvoiceNo varchar(255) default NULL,
		InvoiceDescription text,
		DiscountAmount float(8,2) default NULL,
		TotalAmount float(8,2) default NULL,
		AccountDate date default NULL,
		PIC varchar(255) default NULL,
		FundingSource int(11) default NULL,
		Remarks text,
		RecordStatus INT(1) default 1 NOT NULL,
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified datetime default NULL,
		ModifyBy int(8) default NULL,
		PRIMARY KEY  (RecordID),
		INDEX InvoiceNo (InvoiceNo),
		INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-07",
	"Invoice [customization] -> Invoice Item",
	"CREATE TABLE IF NOT EXISTS INVOICEMGMT_ITEM (
		ItemID int(1) NOT NULL auto_increment,
		InvoiceRecordID int(1) NOT NULL,
		NameChi varchar(255) default NULL,
		NameEng varchar(255) default NULL,
		Price float(8,2) default NULL,
		Quantity int(11) default NULL,
		IsAssetItem tinyint(1) default NULL,
		CategoryID int(11) default NULL,
		ResourceMgmtGroup int(11) NOT NULL default '1',
		RecordStatus int(1) NOT NULL default '1',
		DateInput datetime default NULL,
		InputBy int(8) default NULL,
		DateModified datetime default NULL,
		ModifyBy int(8) default NULL,
		PRIMARY KEY (ItemID),
		KEY RecordStatus (RecordStatus),
		KEY CategoryID (CategoryID),
		KEY InvoiceRecordID (InvoiceRecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-07",
	"Invoice [customization] -> Group Budget - Used",
	"CREATE TABLE IF NOT EXISTS INVOICEMGMT_GROUP_BUDGET_USED (
		RecordID int(11) NOT NULL auto_increment,
		AcademicYearID int(8) default NULL,
		AdminGroupID int(11) default NULL,
		CategoryID int(11) default NULL,
		BudgetUsed float(8,2) default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY  (RecordID),
		Index AcademicYearID (AcademicYearID),
		Index AdminGroupID (AdminGroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-07",
	"Invoice [customization] -> InvoiceItemID vs eInventorItemID",
	"CREATE TABLE IF NOT EXISTS INVOICEMGMT_ITEMID_RELATIONSHIP (
		InvoiceItemID int(11) NOT NULL,
		eInventoryItemID int(11) NOT NULL,
		UNIQUE KEY InvoiceInventoryItemID (InvoiceItemID, eInventoryItemID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-07",
	"Invoice Mgmt System - View Group",
	"CREATE TABLE IF NOT EXISTS INVOICEMGMT_VIEWER_GROUP_MEMBER (
		GroupMemberID int(11) NOT NULL auto_increment,
		UserID int(11) Default 0 NOT NULL,
		DateInput datetime Default Null,
		PRIMARY KEY (GroupMemberID),
		KEY UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-09",
	"eInventory - add field to store whether the inventory item already transfer to eBooking or not",
	"ALTER TABLE INVENTORY_ITEM ADD COLUMN TransferredToEbooking tinyint(1) NOT NULL default 0 AFTER AllowBooking, ADD INDEX TransferredToEbooking (TransferredToEbooking)"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"Subject Group - add INDEX to table SUBJECT_TERM_CLASS",
	"ALTER TABLE SUBJECT_TERM_CLASS ADD INDEX course_id (course_id)"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"Reading Scheme - Add field ISBN for book",
	"ALTER TABLE READING_GARDEN_BOOK ADD COLUMN ISBN VARCHAR(20) DEFAULT NULL AFTER CallNumber, ADD INDEX ISBN (ISBN)"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Support Score in TASK HANDIN",
	"CREATE TABLE IF NOT EXISTS  `IES_TASK_HANDIN_SCORE` (
	  `SnapshotScoreID` int(11) NOT NULL auto_increment,
	  `SnapshotAnswerID` int(11) NOT NULL,
	  `Score` float default NULL,
	  `DateInput` timestamp NOT NULL default CURRENT_TIMESTAMP,
	  `InputBy` int(8) default NULL,
	  `Status` int(8) default '0',
	  PRIMARY KEY  (`SnapshotScoreID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add inputby to table IES_WORKSHEET_HANDIN_FILE",
	"ALTER TABLE IES_WORKSHEET_HANDIN_FILE ADD `InputBy` int(8) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add Remark1 field to IES_SURVEY",
	"alter table IES_SURVEY add `Remark1` mediumtext default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add Remark2 field to IES_SURVEY",
	"alter table IES_SURVEY add `Remark2` mediumtext default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add Remark3 field to IES_SURVEY",
	"alter table IES_SURVEY add `Remark3` mediumtext default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add Remark4 field to IES_SURVEY",
	"alter table IES_SURVEY add `Remark4` mediumtext default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add DefaultQuestion field to IES_DEFAULT_SURVEY_QUESTION",
	"ALTER TABLE IES_DEFAULT_SURVEY_QUESTION ADD `DefaultQuestion` tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add DefaultQuestionCode field to IES_DEFAULT_SURVEY_QUESTION",
	"ALTER TABLE IES_DEFAULT_SURVEY_QUESTION ADD `DefaultQuestionCode` varchar(50) default NULL"
);


$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add SurveyType field to IES_DEFAULT_SURVEY_QUESTION",
	"ALTER TABLE IES_DEFAULT_SURVEY_QUESTION ADD `SurveyType` tinyint(1) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add DisplayOrder field to IES_DEFAULT_SURVEY_QUESTION",
	"ALTER TABLE IES_DEFAULT_SURVEY_QUESTION ADD `DisplayOrder` SMALLINT(1) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-14",
	"IES - Add index handinComment table IES_WORKSHEET_HANDIN_COMMENT",
	"alter table IES_WORKSHEET_HANDIN_COMMENT add UNIQUE KEY `handinComment` (`WorksheetID`, `StudentID`)"
);

$sql_eClassIP_update[] = array(
	"2011-09-15",
	"Reading Scheme - Add AllowAttachment to READING_GARDEN_FORUM",
	"ALTER TABLE READING_GARDEN_FORUM ADD COLUMN AllowAttachment INT(1) DEFAULT 0 AFTER TopicNeedApprove"
);

$sql_eClassIP_update[] = array(
	"2011-09-15",
	"Reading Scheme - Add Attachment to READING_GARDEN_FORUM_TOPIC",
	"ALTER TABLE READING_GARDEN_FORUM_TOPIC ADD COLUMN Attachment VARCHAR(255) DEFAULT NULL AFTER Message"
);

$sql_eClassIP_update[] = array(
	"2011-09-15",
	"Reading Scheme - Add Attachment to READING_GARDEN_FORUM_POST",
	"ALTER TABLE READING_GARDEN_FORUM_POST ADD COLUMN Attachment VARCHAR(255) DEFAULT NULL AFTER Message"
);

$sql_eClassIP_update[] = array(
	"2011-09-15",
	"User Personal Settings data",
	"CREATE TABLE IF NOT EXISTS INTRANET_USER_PERSONAL_SETTINGS (
		UserID int(11) NOT NULL,
		ePayment_NoNeedReceipt tinyint(1),
		ePayment_NoNeedReceipt_DateModified datetime Default Null,
		PRIMARY KEY (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
	"2011-09-15",
	"IES - Add QuestionLang field to IES_DEFAULT_SURVEY_QUESTION",
	"ALTER TABLE IES_DEFAULT_SURVEY_QUESTION ADD `LangType` varchar(10) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-09-19",
	"eBooking - Add Setting Table INTRANET_EBOOKING_SETTING to save general setting for ebooking",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_SETTING (
	    SettingID int(11) NOT NULL auto_increment,
	    SettingName varchar(255) NOT NULL,
	    SettingValue varchar(255),
	    DateModified datetime,
	    LastModifiedBy int(11) ,
	    PRIMARY KEY (SettingID),
	    UNIQUE INDEX SettingName (SettingName)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-20",
	"Group Setting - Alter table INTRANET_USERGROUP set default value of EnrolmentID to 0",
	"ALTER TABLE INTRANET_USERGROUP ALTER EnrolGroupID SET DEFAULT 0"
);

$sql_eClassIP_update[] = array(
	"2011-09-21",
	"eNotice - add column \"SendReplySlip\" to INTRANET_NOTICE_MODULE_TEMPLATE",
	"ALTER TABLE INTRANET_NOTICE_MODULE_TEMPLATE ADD COLUMN SendReplySlip tinyint(1) default 0 not null, ADD INDEX SendReplySlip (SendReplySlip)"
);


$sql_eClassIP_update[] = array(
	"2011-09-22",
	"eBooking - Add Table INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_BOOKING_PERIOD_DAY_SUSPENSION (
     PeriodID int(11) NOT NULL auto_increment,
     RecordDate date default NULL,
     RepeatType tinyint(4) default NULL,
     RepeatValue text,
     RelatedPeriodID int(11) default NULL,
     RelatedItemID int(11) default NULL,
     RelatedSubLocationID int(11) default NULL,
     InputBy int(11) default NULL,
     ModifiedBy int(11) default NULL,
     DateInput datetime default NULL,
     DateModified datetime default NULL,
     PRIMARY KEY (PeriodID),
     KEY RelatedItemID (RelatedItemID),
     KEY RelatedSubLocationID (RelatedSubLocationID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-09-23",
	"iPortfolio (JUPAS) - Change OEA Title from varchar(255) to text",
	"Alter Table OEA_STUDENT Modify COLUMN Title text"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-09-23",
	"iPortfolio (JUPAS) - Add description field to OEA",
	"Alter Table OEA_STUDENT Add COLUMN Description text default Null After Achievement"
);

$sql_eClass_update[] = array(
	"2011-12-08",
	"iPortfolio - Optimzed loading of self-account page in teacher view by adding index to UserID",
	"alter table SELF_ACCOUNT_STUDENT add index UserID (UserID)"
);


$sql_eClass_update[] = array(
	"2011-12-08",
	"iPortfolio - Optimzed loading of self-account page in teacher view by adding index to ApprovedBy",
	"alter table SELF_ACCOUNT_STUDENT add index ApprovedBy (ApprovedBy)"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-09-26",
	"eBooking - Add RejectReason to INTRANET_EBOOKING_BOOKING_DETAILS",
	"Alter Table INTRANET_EBOOKING_BOOKING_DETAILS Add COLUMN RejectReason text default Null After CheckInCheckOutRemark"
);

$sql_eClassIP_update[] = array(
	"2011-09-27",
	"eEnrolment - default setting of OLE setting (Mgmt > Data Handling to OLE)",
	"CREATE TABLE IF NOT EXISTS ENROLMENT_TO_OLE_SETTING (
	SettingID int(11) NOT NULL auto_increment,
	RecordType varchar(10) NOT NULL COMMENT 'club / activity',
	RelatedEnrolID int(11) NOT NULL default 0 COMMENT 'equals to EnrolGroupID or EnrolEventID',
	CategoryID int(11) default 0 NOT NULL,
	OLE_Component varchar(100),
	Organization varchar(200),
	Details mediumtext,
	SchoolRemark mediumtext,
    InputBy int(11) default NULL,
    ModifiedBy int(11) default NULL,
    DateInput datetime default NULL,
    DateModified datetime default NULL,
    PRIMARY KEY (SettingID),
    KEY RecordType (RecordType),
    KEY RelatedEnrolID (RelatedEnrolID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2011-09-28",
	"eBooking - Add CheckOutTime to INTRANET_EBOOKING_BOOKING_DETAILS",
	"Alter Table INTRANET_EBOOKING_BOOKING_DETAILS Add COLUMN CheckOutTime DATETIME default NULL After CheckInCheckOutRemark"
);

$sql_eClassIP_update[] = array(
	"2011-09-30",
	"IP25 - Add two-way hashed, reversable, encrypted EncPassword to INTRANET_USER_PERSONAL_SETTINGS",
	"alter table INTRANET_USER_PERSONAL_SETTINGS add EncPassword varchar(80), add EncPassword_DateModified datetime"
);

$sql_eClassIP_update[] = array(
	"2011-10-06",
	"Digital Archive - Store last view history of user",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_VIEW_HISTORY (
	HistoryID int(11) not null auto_increment,
    DocumentID int(11) default 0 NOT NULL,
	UserID int(11) default 0 NOT NULL,
    DateInput datetime default NULL,
    PRIMARY KEY (HistoryID),
    KEY DocumentID (DocumentID),
    KEY UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-10-12",
	"iPortfolio JUPAS - Create table for ROLE MAPPING",
	"CREATE TABLE IF NOT EXISTS `OEA_OLE_ROLE_MAPPING` (
	  `OLE_ROLE` varchar(64) NOT NULL,
	  `OEA_ROLE` char(2) NOT NULL,
	  KEY `OLE_ROLE` (`OLE_ROLE`),
	  KEY `OEA_ROLE` (`OEA_ROLE`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-10-12",
	"Reading Scheme - Add field AwardSchemeID to READING_GARDEN_READING_RECORD ",
	"ALTER TABLE READING_GARDEN_READING_RECORD add AwardSchemeID INT(11) DEFAULT NULL AFTER Progress "
);

$sql_eClassIP_update[] = array(
	"2011-10-13",
	"Reading Scheme - Add Index AwardSchemeID to READING_GARDEN_READING_RECORD ",
	"ALTER TABLE READING_GARDEN_READING_RECORD ADD INDEX AwardSchemeID (AwardSchemeID) "
);

$sql_eClassIP_update[] = array(
	"2011-10-13",
	"Reading Scheme - Alter table READING_GARDEN_AWARD_SCHEME - Add field to store attachment folder path",
	"ALTER TABLE READING_GARDEN_AWARD_SCHEME ADD Attachment varchar(255) DEFAULT NULL AFTER ResultPublishDate, ADD Description varchar(255) DEFAULT NULL AFTER ResultPublishDate"
);

$sql_eClassIP_update[] = array(
	"2011-10-14",
	"Reading Scheme - Alter table READING_GARDEN_AWARD_SCHEME_LEVEL - Add field AwardType to cater AddtionalAward",
	"ALTER TABLE READING_GARDEN_AWARD_SCHEME_LEVEL ADD AwardType INT(1) DEFAULT 1 AFTER MinReportSubmit,  ADD INDEX AwardType (AwardType) "
);


$sql_eClassIP_update[] = array(
	"2011-10-14",
	"Reading Scheme - Alter table READING_GARDEN_AWARD_SCHEME_LEVEL - Add INDEX AwardType ",
	"ALTER TABLE READING_GARDEN_AWARD_SCHEME_LEVEL ADD INDEX AwardType (AwardType) "
);

$sql_eClassIP_update[] = array(
	"2011-10-14",
	"Reading Scheme - Create table READING_GARDEN_CATEGORY_ANSWERSHEET_MAPPING to cater assigning default answer sheet to category",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_CATEGORY_ANSWERSHEET_MAPPING (
     CategoryAnswerSheetID int(11) NOT NULL auto_increment,
     CategoryID int(11) NOT NULL,
     AnswerSheetID int(11) NOT NULL,
     DateInput datetime default NULL,
     InputBy int(11) default NULL,
     PRIMARY KEY (CategoryAnswerSheetID),
	 INDEX CategoryID (CategoryID),
	 INDEX AnswerSheetID (AnswerSheetID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);


$sql_eClassIP_update[] = array(
	"2011-10-17",
	"Message Center - Email merge",
	"ALTER TABLE INTRANET_MASS_MAILING ADD CSVFormat int (2) default 1 after EmailBody "
);


$sql_eClassIP_update[] = array(
	"2011-10-18",
	"Reading Scheme - Alter table READING_GARDEN_BOOK_REPORT - Add StudentSelectedAnswerSheetID ",
	"ALTER TABLE READING_GARDEN_BOOK_REPORT ADD StudentSelectedAnswerSheetID INT(11) DEFAULT NULL AFTER AnswerSheet "
);

$sql_eClassIP_update[] = array(
	"2011-10-19",
	"Reading Scheme - Alter table READING_GARDEN_BOOK - Add DefaultAnswerSheetID  ",
	"ALTER TABLE READING_GARDEN_BOOK ADD DefaultAnswerSheetID INT(11) DEFAULT NULL AFTER DefaultAnswerSheet "
);


# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-10-24",
	"iPortfolio OLE add default approver",
	"ALTER TABLE OLE_PROGRAM add DefaultApprover int(8) default NULL"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2011-10-31",
	"Add group code in eEnrolment",
	"ALTER TABLE INTRANET_GROUP ADD GroupCode varchar(50) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2011-11-01",
	"add column \"StaffCode\" to INTRANET_USER [CRM : 2011-0816-1638-52071]",
	"ALTER TABLE INTRANET_USER ADD StaffCode varchar(100) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2011-11-02",
	"add column \"Subject\" to KSK_USER [FOR KSK project]",
	"Alter table KSK_USER add column Subject tinyint(1)"
);

$sql_eClassIP_update[] = array(
	"2011-11-02",
	"eBooking - Add table INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER to cater custom group for User Booking Rule",
	"CREATE TABLE INTRANET_EBOOKING_USER_BOOKING_RULE_CUSTOM_GROUP_USER (
	 RuleCustomGroupUserID int(11) NOT NULL auto_increment,
	 RuleID int(11) default NULL,
     UserID int(11) default NULL,
     DateInput datetime default NULL,
	 PRIMARY KEY (RuleCustomGroupUserID),
     KEY RuleID (RuleID),
     KEY UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
	"2011-11-10",
	"eBooking - Alter table INTRANET_EBOOKING_RECORD change RequestDate from date to datetime",
	"ALTER TABLE INTRANET_EBOOKING_RECORD MODIFY COLUMN RequestDate DATETIME"
);

$sql_eClassIP_update[] = array(
	"2011-11-14",
	"eBooking - Customized remark fields",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_RECORD_CUST_REMARK (
	 BookingID int(11) NOT NULL,
     ActivityType varchar(2) NOT NULL,
     ActivityName varchar(255) NOT NULL,
     Attendance varchar(255) NOT NULL,
     PIC varchar(255) NOT NULL,
     IT_Teachnician int(11) default 0,
     Photographer int(11) default 0,
     Clerical int(11) default 0,
     Janitor int(11) default 0,
     PhotoTaking tinyint(1) default 0,
     VideoShooting tinyint(1) default 0,
     Projector tinyint(1) default 0,
     Visualizer tinyint(1) default 0,
     Microphone tinyint(1) default 0,
     MobilePASystem tinyint(1) default 0,
     Laptop tinyint(1) default 0,
     Signage tinyint(1) default 0,
     Souvenir tinyint(1) default 0,
     Stationery tinyint(1) default 0,
     Flower tinyint(1) default 0,
     CateringServicer tinyint(1) default 0,
     Others tinyint(1) default 0,
     PRIMARY KEY (BookingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
	"2011-11-16",
	"SMS - Add index to table INTRANET_SMS2_SOURCE_MESSAGE field UserPIC to improve performance",
	"Alter table INTRANET_SMS2_SOURCE_MESSAGE add index UserPIC (UserPIC)"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-11-17",
	"iPortfolio JUPAS - Add OEA_Suggestions column to Import Temp Table",
	"Alter Table OEA_OLE_MAPPING_IMPORT_TEMP Add Column OEA_Suggestions text Default Null After OLE_ProgramNature"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-11-21",
	"iPortfolio - ADD a setting table to store the iPortfolio client system config",
	"CREATE TABLE IF NOT EXISTS `IPORTFOLIO_SETTING` (
	`SETTING_ID` int(11) NOT NULL auto_increment,
	`SETTING_NAME` varchar(255) NOT NULL default '',
	`SETTING_VALUE` text,
	`DATE_INPUT` datetime default NULL,
	`INPUT_BY` int(11) default NULL,
	`DATE_MODIFIED` datetime default NULL,
	`MODIFY_BY` int(11) default NULL,
	PRIMARY KEY  (`SETTING_ID`),
	UNIQUE KEY `SETTING_NAME` (`SETTING_NAME`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='STORE THE IPORTFOLIO SETTING VALUES'"
);
# end of eClass DB update

//if($plugin['W2']){
	//add on for writing 2.0
	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_WRITING_TEACHER (
			     WRITING_TEACHER_ID int(11) NOT NULL auto_increment,
			     WRITING_ID int(8) NOT NULL,
			     USER_ID int(8) NOT NULL,
			     INPUT_BY int(8) default NULL,
			     DATE_INPUT timestamp NULL default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
			     DELETE_STATUS tinyint(3) default '1',
			     DELETED_BY int(8) default NULL,
			     DATE_DELETE timestamp NULL default NULL,
			     PRIMARY KEY (WRITING_TEACHER_ID),
			     UNIQUE KEY WritingTeacher (WRITING_ID,USER_ID),
			     KEY USER_ID (USER_ID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);

	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_WRITING_STUDENT (
			     WRITING_STUDENT_ID int(11) NOT NULL auto_increment,
			     WRITING_ID int(8) NOT NULL,
			     USER_ID int(8) NOT NULL,
			     INPUT_BY int(8) default NULL,
			     HANDIN_SUBMIT_STATUS tinyint(3) default NULL,
			     HANDIN_SUBMIT_STATUS_DATE timestamp NULL default NULL,
			     OTHER_INFO text COMMENT 'store Vocab for ENG',
			     DATE_INPUT timestamp NULL default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
			     DELETE_STATUS tinyint(3) default '1',
			     DELETED_BY int(8) default NULL,
			     DATE_DELETE timestamp NULL default NULL,
			     PRIMARY KEY (WRITING_STUDENT_ID),
			     UNIQUE KEY WritingStudent (WRITING_ID,USER_ID),
			     KEY USER_ID (USER_ID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);

	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_WRITING (
			     WRITING_ID int(8) NOT NULL auto_increment,
			     TITLE varchar(255) default NULL,
			     INTRODUCTION text,
			     CONTENT_CODE varchar(255) default NULL COMMENT 'THIS WRITING USING WHICH CONTENT (CONTENT_CODE)',
			     SCHEME_CODE varchar(255) default NULL COMMENT 'THIS WRITING USING WHICH SCHEME (SCHEME_CODE)',
			     START_DATE_TIME datetime default NULL,
			     END_DATE_TIME datetime default NULL,
			     RECORD_STATUS tinyint(3) default NULL,
			     WITH_DEFAULT_CONCEPTMAP tinyint(1) default '0',
			     INPUT_BY int(8) default NULL,
			     DATE_INPUT timestamp NULL default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
			     DELETE_STATUS tinyint(3) default '1',
			     PRIMARY KEY (WRITING_ID),
			     KEY INPUT_BY (INPUT_BY)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);

	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_STEP_STUDENT_FILE (
			     RECORD_ID int(11) NOT NULL auto_increment,
			     USER_ID int(11) NOT NULL,
			     STEP_ID int(11) NOT NULL,
			     FILE_ORI_NAME text,
			     FILE_HASH_NAME text,
			     FILE_SUPPLEMENTARY_NAME text,
			     FILE_PATH varchar(255) default NULL,
			     DATE_INPUT timestamp NULL default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
			     PRIMARY KEY (RECORD_ID),
			     KEY STEP_ID (STEP_ID),
			     KEY USER_ID (USER_ID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);

	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_STEP_STUDENT_COMMENT (
			     COMMENT_ID int(11) NOT NULL auto_increment,
			     CONTENT text NOT NULL,
			     DATE_INPUT timestamp NULL default NULL,
			     INPUT_BY int(8) default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
			     MODIFIED_BY int(8) default NULL,
			     PRIMARY KEY (COMMENT_ID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);

	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_STEP_STUDENT (
			     STEP_ID int(11) NOT NULL,
			     USER_ID int(11) NOT NULL COMMENT ' UNIQUE KEY STEP_USER--> Student should have only one STEP ID W2_STEP_STUDENT',
			     STEP_STATUS tinyint(3) default NULL,
			     COMMENT_ID int(11) default NULL,
			     DATE_INPUT timestamp NULL default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
			     UNIQUE KEY STEP_USER (STEP_ID,USER_ID),
			     KEY STEP_ID (STEP_ID),
			     KEY USER_ID (USER_ID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);
	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_STEP_HANDIN (
			     STEP_ID int(11) NOT NULL,
			     USER_ID int(11) NOT NULL,
			     STEP_HANDIN_CODE varchar(255) default NULL COMMENT ' UNIQUE KEY STEP_USER_CODE--> Student should have only one handin in equal STEP_ID and STEP_HANDIN_CODE',
			     ANSWER text,
			     DATE_INPUT timestamp NULL default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
			     UNIQUE KEY STEP_USER_CODE (STEP_ID,USER_ID,STEP_HANDIN_CODE),
			     KEY STEP_ID (STEP_ID),
			     KEY USER_ID (USER_ID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);

	$sql_eClassIP_update[] = array(
		"2011-12-01",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_STEP (
			     STEP_ID int(11) NOT NULL auto_increment,
			     WRITING_ID int(11) NOT NULL,
			     TITLE_ENG varchar(255) default NULL,
			     TITLE_CHI varchar(255) default NULL,
			     CODE varchar(255) default NULL,
			     SEQUENCE int(8) default NULL,
			     DATE_INPUT timestamp NULL default NULL,
			     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
			     ECLASS_TASK_ID int(8) default NULL,
			     PRIMARY KEY (STEP_ID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		"
	);


	# this is for eClass DB update (not for IP25 table)
	$sql_eClass_update[] = array(
		"2011-12-08",
		"Writing 2.0 - ADD Table",
		"CREATE TABLE IF NOT EXISTS W2_DEFAULT_CONTENT (
			     SETTING_ID int(11) NOT NULL auto_increment,
			     SETTING_NAME varchar(255) NOT NULL default '',
			     SETTING_VALUE longblob,
			     CONTENT_CODE varchar(255) NOT NULL default '' COMMENT 'Content code, suppose is eng , chi, ls',
			     SCHEME_CODE varchar(255) NOT NULL default '' COMMENT 'system unique code to identify the scheme, may be scheme1 , scheme2',
			     DATE_INPUT datetime default NULL,
			     DATE_MODIFIED datetime default NULL,
			     PRIMARY KEY (SETTING_ID),
			     UNIQUE KEY SETTING_CONTENT_SCHEME (SETTING_NAME,CONTENT_CODE,SCHEME_CODE)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='STORE WRITING 2.0 DEFAULT CONTENT'
		"
	);
	# end of eClass DB update
	
	
	$sql_eClassIP_update[] = array(
		"2011-12-08",
		"Writing 2.0 - Add field COMMENT_STATUS to table W2_STEP_STUDENT_COMMENT",
		"ALTER TABLE W2_STEP_STUDENT_COMMENT ADD COLUMN COMMENT_STATUS tinyint(3) DEFAULT 15 AFTER CONTENT"
	);
//}

$sql_eClassIP_update[] = array(
	"2011-12-08",
	"Add activity code in eEnrolment",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD ActivityCode varchar(50) DEFAULT NULL"
);


$sql_eClassIP_update[] = array(
	"2011-12-09",
	"optimized performance for eNotice",
	"alter table INTRANET_NOTICE_REPLY add index IDandStatus (NoticeID, RecordStatus)"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-12-15",
	"iPortfolio - Add default template related db fields to student_report_template",
	"Alter Table student_report_template 	Add Column is_default tinyint(1) default 0,
											Add Column template_code varchar(64) default null"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-12-15",
	"iPortfolio - Add template_code as unique key for dynamic report",
	"Alter Table student_report_template Add Unique Key template_code (template_code)"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2011-12-16",
	"iPortfolio - OLE PROGRAM CODE support with more than 99",
	"ALTER TABLE OLE_PROGRAM MODIFY `Category` char(3) default NULL"
);

$sql_eClassIP_update[] = array(
	"2011-12-19",
	"Add UserID for TEMP_INVENTORY_STOCKTAKE_RESULT",
	"ALTER TABLE TEMP_INVENTORY_STOCKTAKE_RESULT ADD UserID int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2011-12-29",
	"ADD Performance Mark in INTRANET_ENROL_EVENTSTUDENT [CRM : 2011-0616-1516-53126]",
	"ALTER TABLE INTRANET_ENROL_EVENTSTUDENT ADD ECA int(10) default NULL, ADD SS int(10) default NULL, ADD CS int(10) default NULL"
);


$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX LocationIDWithCode"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX LocationIDWithBarcode"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX InventoryLocationID"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX InventorySubLocationID"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX InventorySubLocationCode"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX InventorySubLocationNameChi"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX InventorySubLocationNameEng"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION drop unnecessary key ",
	"ALTER TABLE INVENTORY_LOCATION DROP INDEX InventoryLocationCode"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION add unique index Code ",
	"ALTER TABLE INVENTORY_LOCATION ADD INDEX Code (Code)"
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_LEVEL drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY LocationLevelIDWithBarcode "
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_LEVEL drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY LocationLevelIDWithCode  "
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_LEVEL drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY InventoryLocationID "
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_LEVEL drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY InventoryLocationCode "
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_LEVEL drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY InventoryBuildingID  "
);

//$sql_eClassIP_update[] = array(
//	"2012-01-04",
//	"Location - Alter table INVENTORY_LOCATION_LEVEL add unique index ",
//	"ALTER IGNORE TABLE INVENTORY_LOCATION_LEVEL ADD UNIQUE KEY UniqueCodeForEachBuilding (BuildingID, Code) "
//);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_BUILDING drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP KEY BuildingIDWithCode "
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_BUILDING drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP KEY BuildingIDWithBarcode "
);

$sql_eClassIP_update[] = array(
	"2012-01-04",
	"Location - Alter table INVENTORY_LOCATION_BUILDING drop unnecessary index ",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP KEY BuildingID "
);

//$sql_eClassIP_update[] = array(
//	"2012-01-04",
//	"Location - Alter table INVENTORY_LOCATION_BUILDING add unique index ",
//	"ALTER IGNORE TABLE INVENTORY_LOCATION_BUILDING ADD UNIQUE KEY Code (Code)  "
//);

$sql_eClassIP_update[] = array(
	"2012-01-05",
	"eDiscipline - alter table DISCIPLINE_ACCU_RECORD add amount (times) of GM record ",
	"ALTER IGNORE TABLE DISCIPLINE_ACCU_RECORD ADD column GMCount float(8,2) default 1 NOT NULL AFTER StudentID "
);

$sql_eClassIP_update[] = array(
	"2012-02-09",
	"IP25 - keep track the last access eCommunity Group",
	"alter table INTRANET_USER_PERSONAL_SETTINGS add eComm_LastAccessGroupID int(11), add eComm_LastAccessGroupID_DateModified datetime"
);

$sql_eClassIP_update[] = array(
	"2012-02-09",
	"Location - Alter table INVENTORY_LOCATION_BUILDING drop unique index ",
	"ALTER TABLE INVENTORY_LOCATION_BUILDING DROP KEY Code "
);

$sql_eClassIP_update[] = array(
	"2012-02-09",
	"Location - Alter table INVENTORY_LOCATION_LEVEL drop unique index ",
	"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY UniqueCodeForEachBuilding  "
);

$sql_eClassIP_update[] = array(
	"2012-02-13",
	"Writing 2.0 - Add Reference Category Table",
	"CREATE TABLE IF NOT EXISTS W2_REF_CATEGORY (
		REF_CATEGORY_ID int(11) NOT NULL auto_increment,
		REF_CATEGORY_CODE varchar(64) default NULL,
		WRITING_STUDENT_ID int(11) default NULL,
		INFOBOX_CODE varchar(255) default NULL,
		TITLE varchar(255) default NULL,
		CSS_SET int(3) default 0,
		DATE_INPUT timestamp NULL default NULL,
		INPUT_BY int(8) default NULL,
		DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
		MODIFIED_BY int(8) default NULL,
		DELETE_STATUS tinyint(3) default '1',
		DELETED_BY int(8) default NULL,
		PRIMARY KEY (REF_CATEGORY_ID),
		KEY REF_CATEGORY_CODE (REF_CATEGORY_CODE),
		KEY WRITING_STUDENT_ID (WRITING_STUDENT_ID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-02-13",
	"Writing 2.0 - Add Reference Item Table",
	"CREATE TABLE IF NOT EXISTS W2_REF_ITEM (
		REF_ITEM_ID int(11) NOT NULL auto_increment,
		REF_CATEGORY_TYPE varchar(32) default NULL,
		REF_CATEGORY_CODE varchar(64) default NULL,
		WRITING_STUDENT_ID int(11) default NULL,
		TITLE varchar(255) default NULL,
		DATE_INPUT timestamp NULL default NULL,
		INPUT_BY int(8) default NULL,
		DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
		MODIFIED_BY int(8) default NULL,
		DELETE_STATUS tinyint(3) default '1',
		DELETED_BY int(8) default NULL,
		PRIMARY KEY (REF_ITEM_ID),
		KEY REF_CATEGORY_CODE (REF_CATEGORY_CODE),
		KEY WRITING_STUDENT_ID (WRITING_STUDENT_ID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-02-13",
	"eNotice - Add index to TargetType for table INTRANET_NOTICE to improve login portal performance",
	"Alter Table INTRANET_NOTICE Add Index TargetType (TargetType)"
);

$sql_eClassIP_update[] = array(
	"2012-02-13",
	"eEnrolment - add \"Location\" to Group Info",
	"Alter Table INTRANET_ENROL_GROUPINFO Add Location varchar(200)"
);

$sql_eClassIP_update[] = array(
	"2012-02-13",
	"Writing 2.0 - Add index ActiveCategory to Reference Category Table",
	"Alter Table W2_REF_CATEGORY Add Index ActiveCategory (INFOBOX_CODE, DELETE_STATUS)"
);

$sql_eClassIP_update[] = array(
	"2012-02-14",
	"Writing 2.0 - Add column INFOBOX_CODE to Reference Item Table",
	"Alter Table W2_REF_ITEM Add Column INFOBOX_CODE varchar(255) default NULL after REF_CATEGORY_CODE"
);

$sql_eClassIP_update[] = array
(
	"2012-02-27",
	" eLibrary - add field BookFormat in INTRANET_ELIB_BOOK to store book format ",
	"alter table INTRANET_ELIB_BOOK add `BookFormat` varchar (16)"
);

$sql_eClassIP_update[] = array
(
	"2012-02-29",
	" Account Mgmt - store extra info for student account [CRM : 2012-0201-1440-47098] (store in INTRANET_USER_PERSONAL_SETTINGS)",
	"alter table INTRANET_USER_PERSONAL_SETTINGS add Occupation varchar(200), add Occupation_DateModified datetime, add Company varchar(200), add Company_DateModified datetime, add PassportNo varchar(100), add PassportNo_DateModified datetime, add Passport_ValidDate date, add Passport_ValidDate_DateModified datetime, add HowToKnowBlissWisdom varchar(100), add HowToKnowBlissWisdom_DateModified datetime"
);

$sql_eClassIP_update[] = array
(
	"2012-03-07",
	"Account Mgmt - Alter table INTRANET_USER_EXTRA_INFO_CATEGORY, change CategoryCode to varchar(40)",
	"alter table INTRANET_USER_EXTRA_INFO_CATEGORY change CategoryCode CategoryCode varchar(40)"
);

$sql_eClassIP_update[] = array
(
	"2012-03-07",
	"Account Mgmt - Alter table INTRANET_USER_EXTRA_INFO_MAPPING, add column OtherInfo",
	"alter table INTRANET_USER_EXTRA_INFO_MAPPING add OtherInfo varchar(255) AFTER ItemID"
);


$sql_eClassIP_update[] = array
(
	"2012-03-20",
	"Email Merge - support online edit",
	"ALTER TABLE INTRANET_MASS_MAILING add BodyFrom int (2) default '2' after CSVFile"
);

$sql_eClassIP_update[] = array (
	"2012-03-21",
	"Writing 2.0 - Add peer-marking related columns to W2_WRITING",
	"ALTER TABLE W2_WRITING ADD ALLOW_PEER_MARKING tinyint(3) default 1 AFTER WITH_DEFAULT_CONCEPTMAP,
							ADD PEER_MARKING_START_DATE datetime default NULL AFTER ALLOW_PEER_MARKING,
							ADD PEER_MARKING_END_DATE datetime default NULL AFTER PEER_MARKING_START_DATE,
							ADD PEER_MARKING_TARGET_NUM tinyint(3) default NULL AFTER PEER_MARKING_END_DATE,
							ADD SHOW_PEER_NAME tinyint(3) default 0 AFTER PEER_MARKING_TARGET_NUM
	"
);

$sql_eClassIP_update[] = array (
	"2012-03-21",
	"iCalendar - save original visible calendars which were disabled in eCommunity",
	"alter table INTRANET_USER_PERSONAL_SETTINGS add column iCalendarVisibleCals text default NULL"
);

$sql_eClassIP_update[] = array (
	"2012-03-21",
	"iCalendar - save original visible calendars which were disabled in eCommunity",
	"alter table INTRANET_USER_PERSONAL_SETTINGS add column iCalendarVisibleCals_DateModified datetime default NULL"
);


$sql_eClassIP_update[] = array (
	"2012-03-28",
	"CampusLink - visible to particular identities",
	"alter table INTRANET_CAMPUS_LINK add column VisibleTo varchar(128) default 'ALL'"
);

$sql_eClassIP_update[] = array(
	"2012-03-28",
	"Add KEY to INVENTORY_ITEM_BULK_LOCATION",
	"alter table INVENTORY_ITEM_BULK_LOCATION ADD KEY BulkItemFunding (FundingSourceID);"
); 

$sql_eClassIP_update[] = array(
	"2012-03-28",
	"Writing 2.0 - Add Peer Marking Group Table",
	"CREATE TABLE IF NOT EXISTS W2_PEER_MARKING_GROUPING (
		GROUP_ID int(8) NOT NULL auto_increment,
		GROUP_NUM tinyint(3) default NULL,
		WRITING_ID int(8) default NULL,
		USER_ID int(8) default NULL,
		DATE_INPUT timestamp NULL default NULL,
		INPUT_BY int(8) default NULL,
		DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
		MODIFIED_BY int(8) default NULL,
		DELETE_STATUS tinyint(3) default '1',
		DELETED_BY int(8) default NULL,
		PRIMARY KEY (GROUP_ID),
		KEY WRITING_GROUP_USER (WRITING_ID, GROUP_NUM, USER_ID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-03-30",
	"eEnrolment - Add Achievement field for Club Member",
	"Alter Table INTRANET_USERGROUP Add Column Achievement text Default Null"
);

$sql_eClassIP_update[] = array(
	"2012-03-30",
	"eEnrolment - Add Achievement field for Activity Participant",
	"Alter Table INTRANET_ENROL_EVENTSTUDENT Add Column Achievement text Default Null"
);

$sql_eClassIP_update[] = array(
	"2012-04-02",
	"Writing 2.0 - Add Peer Marking Data Table",
	"CREATE TABLE IF NOT EXISTS W2_PEER_MARKING_DATA (
		RECORD_ID int(11) NOT NULL auto_increment,
		WRITING_ID int(8) default NULL,
		MARKER_USER_ID int(8) default NULL,
		TARGET_USER_ID int(8) default NULL,
		COMMENT text default NULL,
		DATE_INPUT timestamp NULL default NULL,
		INPUT_BY int(8) default NULL,
		DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
		MODIFIED_BY int(8) default NULL,
		DELETE_STATUS tinyint(3) default '1',
		DELETED_BY int(8) default NULL,
		PRIMARY KEY (RECORD_ID),
		KEY WRITING_MARKER_TARGET_USER (WRITING_ID, MARKER_USER_ID, TARGET_USER_ID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-04-03",
	"Writing 2.0 - Add Peer Marking Data Table Index",
	"Alter Table W2_PEER_MARKING_DATA Add Index MARKER_USER_ID (MARKER_USER_ID)"
);

$sql_eClassIP_update[] = array(
	"2012-04-03",
	"eLibrary - Add Relevant Subject field to table INTRANET_ELIB_BOOK",
	"ALTER TABLE INTRANET_ELIB_BOOK ADD COLUMN RelevantSubject varchar(100) default NULL"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2012-04-11",
	"iPortfolio - OLE PROGRAM support Teacher Incharge",
	"ALTER TABLE OLE_PROGRAM ADD `TeacherInCharge` int(11) default 0"
);


$sql_eClass_update[] = array(
	"2012-04-11",
	"iPortfolio - A TABLE To Temporary Store OLE IMPORT DATA",
	"CREATE TABLE IF NOT EXISTS OLE_IMPORT_RECORD (  
		  IMPORT_LINE_NO int(11) NOT NULL default 0,
		  OLE_PROGRAM_ID int(11) NOT NULL default 0,
		  OLE_STUDENT_RECORD_ID int(11) NOT NULL default 0,  
		  OLE_STUDENT_ID int (11) NOT NULL default 0,  
		  IMPORT_BATCH_NO varchar(255) NOT NULL default '',
		  DATE_INPUT TIMESTAMP(14) not null DEFAULT NOW(),      
		  INDEX IMPORT_LINE_NO (IMPORT_LINE_NO),
		  INDEX OLE_PROGRAM_ID (OLE_PROGRAM_ID),  
		  INDEX OLE_STUDENT_RECORD_ID (OLE_STUDENT_RECORD_ID),  
		  INDEX OLE_STUDENT_ID (OLE_STUDENT_ID)
	) TYPE=MyISAM"
);

$sql_eClass_update[] = array(
	"2012-04-11",
	"iPortfolio - OEA store some pre map student OEA record",
	"CREATE TABLE IF NOT EXISTS `OEA_STUDENT_PRE_SELECT` (
	  `RecordID` int(11) NOT NULL auto_increment,
	  `StudentID` int(11) NOT NULL,
	  `Title` text,
	  `StartYear` int(4) NOT NULL,
	  `EndYear` int(4) NOT NULL,
	  `OEA_AwardBearing` varchar(10) default NULL,
	  `Participation` varchar(10) default NULL,
	  `Role` varchar(10) default NULL,
	  `ParticipationNature` varchar(10) default NULL,
	  `Achievement` varchar(10) default NULL,
	  `Description` text,
	  `OEA_ProgramCode` varchar(10) NOT NULL,
	  `OLE_STUDENT_RecordID` int(11) NOT NULL COMMENT 'Reference to OLE_SUDENT>RecordID, OEA checkbox checking',
	  `OLE_PROGRAM_ProgramID` int(11) NOT NULL COMMENT 'Reference to OLE_PROGRAM>ProgramID, quick reference to OLE program',
	  `RecordStatus` int(2) NOT NULL default '0' COMMENT '1:approved / 0:pending',
	  `InputDate` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `ApprovalDate` datetime default NULL,
	  `ApprovedBy` int(11) default NULL,
	  `ModifyDate` datetime default NULL,
	  `ModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`RecordID`),
	  KEY `StudentID` (`StudentID`),
	  KEY `StartYear` (`StartYear`),
	  KEY `EndYear` (`EndYear`),
	  KEY `OEA_ProgramCode` (`OEA_ProgramCode`),
	  KEY `OLE_STUDENT_RecordID` (`OLE_STUDENT_RecordID`),
	  KEY `OLE_PROGRAM_ProgramID` (`OLE_PROGRAM_ProgramID`),
	  KEY `RecordStatus` (`RecordStatus`)
	) ENGINE=InnoDB AUTO_INCREMENT=4850 DEFAULT CHARSET=utf8 COMMENT='STORE PRE MAP OEA Studetn Record'
  "
);

$sql_eClassIP_update[] = array(
	"2012-04-11",
	"Writing 2.0 - Add ViewStatus Field into Peer Marking Data Table",
	"Alter Table W2_PEER_MARKING_DATA Add Column VIEW_STATUS tinyint(3) default 17 After COMMENT"
);


$sql_eClassIP_update[] = array(
	"2012-04-16",
	"eDiscipline - add AP Approval Group Right table",
	"CREATE TABLE DISCIPLINE_AP_APPROVAL_GROUP_RIGHT (
     GroupRightID int(11) NOT NULL auto_increment,
     GroupID int(11) NOT NULL,
     APCategoryID int(11) NOT NULL,
     DateInput datetime default NULL,
	 InputBy int(11) default NULL,
     PRIMARY KEY (GroupRightID),
     KEY GroupID (GroupID),
     KEY APCategoryID (APCategoryID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-04-17",
	"eBook Reader - create table INTRANET_EBOOK_BOOK",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOK_BOOK (
	  BookID int(11) NOT NULL auto_increment,
	  BookTitle varchar(255) default NULL,
	  BookPath varchar(255) default NULL,
	  BookSubFolder varchar(255) default NULL,
	  ImageBook varchar(1) default NULL,
	  ImageWidth int(11) default NULL,
	  RecordStatus int(1) default 1,
	  DateInput datetime default NULL,
	  InputBy int(11),
	  PRIMARY KEY (BookID),
	  INDEX RecordStatus(RecordStatus)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-04-17",
	"eBook Reader - create table INTRANET_EBOOK_PAGE_INFO",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOK_PAGE_INFO (
	  RecordID int(11) NOT NULL auto_increment,
	  BookID int(11) NOT NULL,
	  RefID varchar(512) NOT NULL,
	  FileName varchar(512) NOT NULL,
	  Pages1PageMode int(11),
	  Pages2PageMode int(11),
	  PRIMARY KEY (RecordID),
	  INDEX BookID(BookID),
	  INDEX RefID(RefID(20))  
	)ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-04-17",
	"eBook Reader - create table INTRANET_EBOOK_HIGHLIGHT_NOTES",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOK_HIGHLIGHT_NOTES (
	  RecordID int(11) NOT NULL auto_increment,
	  BookID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  RefID varchar(512) NOT NULL,
	  UniqueID varchar(50) NOT NULL,
	  Highlight text,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY (RecordID),
	  INDEX BookID(BookID),
	  INDEX UserID(UserID),
	  INDEX RefID(RefID(20)),
	  INDEX UniqueID(UniqueID),
	  UNIQUE KEY UniqueKey(BookID,UserID,RefID(20),UniqueID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-04-17",
	"eBook Reader - create table INTRANET_EBOOK_BOOKMARKS",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOK_BOOKMARKS (
	  BookMarkID int(11) NOT NULL auto_increment,
	  BookID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  PageNum1PageMode int(11),
	  PageNum2PageMode int(11),
	  DateInput datetime default NULL,
	  PRIMARY KEY (BookMarkID),
	  UNIQUE KEY UniqueKey(BookID,UserID,PageNum1PageMode,PageNum2PageMode),
	  INDEX BookID(BookID),
	  INDEX UserID(UserID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);	

$sql_eClassIP_update[] = array(
	"2012-04-17",
	"eBook Reader - create table INTRANET_EBOOK_PAGE_NOTES",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOK_PAGE_NOTES (
	  RecordID int(11) NOT NULL auto_increment,
	  BookID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  PageNum1PageMode int(11),
	  PageNum2PageMode int(11),
	  Notes text,
	  DateInput datetime default NULL,
	  DateModified datetime default NULL,
	  PRIMARY KEY (RecordID),
	  UNIQUE KEY UniqueKey(BookID,UserID,PageNum1PageMode,PageNum2PageMode),
	  INDEX BookID(BookID),
	  INDEX UserID(UserID),
	  INDEX PageNum1PageMode(PageNum1PageMode),
	  INDEx PageNum2PageMode(PageNum2PageMode)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-04-17",
	"eBook Reader - create table INTRANET_EBOOK_USER_PROGRESS",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOK_USER_PROGRESS (
	  RecordID int(11) NOT NULL auto_increment,
	  BookID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  ProgressStatus tinyint,
	  Percentage tinyint,
	  DateModified datetime,
	  PRIMARY KEY (RecordID),
	  UNIQUE KEY UniqueKey(BookID,UserID),
	  INDEX BookID (BookID),
	  INDEX UserID (UserID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
	"2012-04-24",
	"Add TAG table to eLibrary",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_TAG (
		TagID int(11) NOT NULL auto_increment,
		TagName varchar(200) NOT NULL,
		DateInput datetime,
		InputBy int(11),
		PRIMARY KEY (TagID)
	)  ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);



$sql_eClassIP_update[] = array(
	"2012-04-24",
	"Add TAG table to eLibrary",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_TAG (
		BookTagID int(11) NOT NULL auto_increment,
		BookID int(11) NOT NULL,
		TagID int(11) NOT NULL,
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		PRIMARY KEY  (BookTagID),
		INDEX BookID (BookID),
		INDEX TagID (TagID),
		UNIQUE KEY TagBook (TagID, BookID)
	)  ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);


$sql_eClassIP_update[] = array(
	"2012-04-26",
	"Add OnlineWritingDesc to READING_GARDEN_ASSIGNED_READING",
	"ALTER TABLE READING_GARDEN_STUDENT_ASSIGNED_READING ADD COLUMN OnlineWritingDesc text AFTER LowerLimit "
);

$sql_eClassIP_update[] = array(
	"2012-05-02",
	"change STRN from 8 digits to 9 digits",
	"ALTER TABLE INTRANET_USER MODIFY STRN varchar(9)"
);


$sql_eClassIP_update[] = array(
	"2012-05-08",
	"Reading Scheme Add Table READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM for Populat Award Scheme ",
	"CREATE TABLE IF NOT EXISTS `READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM` (
  `ItemID` int(11) NOT NULL auto_increment,
  `ItemCode` varchar(20) DEFAULT NULL,
  `ItemDescEn` text DEFAULT NULL,
  `ItemDescCh` text DEFAULT NULL,
  `Score` int(5) NOT NULL DEFAULT 0,
  `CanSubmitByStudent` int(1) DEFAULT 0,
  `CategoryID` int(11) DEFAULT NULL,
  `DisplayOrder` int(5) DEFAULT NULL,
  `RecordStatus` int(1) DEFAULT '1',
  `DateInput` datetime DEFAULT NULL,
  `DateModified` datetime DEFAULT NULL,
  `InputBy` int(11) DEFAULT NULL,
  `LastModifiedBy` int(11) DEFAULT NULL,
  PRIMARY KEY  (`ItemID`),
  KEY `ItemCode` (`ItemCode`),
  KEY `RecordStatus` (`RecordStatus`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8"
);

$sql_eClassIP_update[] = array(
	"2012-05-08",
	"Reading Scheme Add Table READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY for Populat Award Scheme ",
	"CREATE TABLE IF NOT EXISTS `READING_GARDEN_POPULAR_AWARD_SCHEME_ITEM_CATEGORY` (
  `CategoryID` int(11) NOT NULL auto_increment,
  `CategoryCode` varchar(20) NOT NULL,
  `CategoryNameEn` varchar(255) DEFAULT NULL,
  `CategoryNameCh` varchar(255) DEFAULT NULL,
  `DisplayOrder` int(11) DEFAULT NULL,
  `RecordStatus` int(1) DEFAULT '1',
  `DateInput` datetime DEFAULT NULL,
  `DateModified` datetime DEFAULT NULL,
  `InputBy` int(11) DEFAULT NULL,
  `LastModifiedBy` int(11) DEFAULT NULL,
  PRIMARY KEY  (`CategoryID`),
  KEY `CategoryCode` (`CategoryCode`),
  KEY `RecordStatus` (`RecordStatus`)
) ENGINE=INNODB DEFAULT CHARSET=UTF8"
);

$sql_eClassIP_update[] = array(
	"2012-05-09",
	"Add unique key to BookTAG table",
	"ALTER TABLE INTRANET_ELIB_BOOK_TAG ADD UNIQUE KEY TagBook (TagID, BookID)"
);

$sql_eClassIP_update[] = array(
	"2012-05-10",
	"Reading Scheme Add Table READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING for Popular Award Scheme ",
	"CREATE TABLE IF NOT EXISTS `READING_GARDEN_POPULAR_AWARD_SCHEME_STUDENT_ITEM_MAPPING` (
  `StudentItemID` int(11) NOT NULL auto_increment,
  `StudentID` int(11) NOT NULL,
  `ItemID` int(11) NOT NULL,
  `AcademicYearID` int(11) NOT NULL,
  `DateInput` datetime default NULL,
  `InputBy` int(11) default NULL,
  PRIMARY KEY  (`StudentItemID`),
  KEY `StudentItemID` (`StudentID`,`ItemID`),
  KEY `AcademicYearID` (`AcademicYearID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-05-16",
	"Reading Scheme Add Table READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD for Popular Award Scheme ",
	"CREATE TABLE IF NOT EXISTS `READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD` (
  `AwardID` int(11) NOT NULL auto_increment,
  `AwardNameEn` varchar(255) default NULL,
  `AwardNameCh` varchar(255) default NULL,
  `Score` int(5) NOT NULL, 
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  `InputBy` int(11) default NULL,
  `LastModifiedBy` int(11) default NULL,
  PRIMARY KEY  (`AwardID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-05-16",
	"Reading Scheme Add Table READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT for Popular Award Scheme ",
	"CREATE TABLE IF NOT EXISTS `READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT` (
  `RequirementID` int(11) NOT NULL auto_increment,
  `AwardID` int(11) NOT NULL,
  `Score` int(5) NOT NULL, 
  `DateInput` datetime default NULL,
  `InputBy` int(11) default NULL,
  PRIMARY KEY  (`RequirementID`),
  KEY `AwardID` (`AwardID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-05-16",
	"Reading Scheme Add Table READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT_CATEGORY for Popular Award Scheme ",
	"CREATE TABLE IF NOT EXISTS `READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_REQUIREMENT_CATEGORY` (
  `RequirementCategoryID` int(11) NOT NULL auto_increment,
  `RequirementID` int(11) NOT NULL,
  `CategoryID` int(11) NOT NULL,
  `DateInput` datetime default NULL,
  `InputBy` int(11) default NULL,
  PRIMARY KEY  (`RequirementCategoryID`),
  KEY `RequirementID` (`RequirementID`),
  KEY `CategoryID` (`CategoryID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-05-17",
	"Add Column TitleLang in table ENROLMENT_TO_OLE_SETTING to store the target title language for data transfer",
	"ALTER TABLE ENROLMENT_TO_OLE_SETTING ADD Column TitleLang varchar(8) default 'E' After SchoolRemark"
);


$sql_eClassIP_update[] = array(
	"2012-05-21",
	"Reading Scheme - Create table READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_TARGET_FORM",
	"CREATE TABLE IF NOT EXISTS READING_GARDEN_POPULAR_AWARD_SCHEME_AWARD_TARGET_FORM (
	   TargetFormID int(11) NOT NULL auto_increment,
	   AwardID int(11) NOT NULL,
	   YearID int(11) NOT NULL,
	   DateInput datetime,
	   InputBy int(11) ,
	   PRIMARY KEY (TargetFormID),
	   INDEX AwardID (AwardID),
	   INDEX YearID (YearID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
	"2012-05-23",
	"Add KEY to INVENTORY_ITEM_BULK_LOCATION",
	"alter table INVENTORY_ITEM_BULK_LOCATION ADD KEY BulkItemFunding (FundingSourceID);"
); 

$sql_eClassIP_update[] = array(
	"2012-05-23",
	"Add FundingSourceID to INVENTORY_ITEM_WRITE_OFF_RECORD",
	"alter table INVENTORY_ITEM_WRITE_OFF_RECORD add FundingSourceID int(11)"
); 

$sql_eClassIP_update[] = array(
	"2012-05-23",
	"Add FundingSourceID to INVENTORY_ITEM_MISSING_RECORD",
	"alter table INVENTORY_ITEM_MISSING_RECORD add FundingSourceID int(11)"
); 

$sql_eClassIP_update[] = array(
	"2012-05-23",
	"Add FundingSourceID to INVENTORY_ITEM_SURPLUS_RECORD",
	"alter table INVENTORY_ITEM_SURPLUS_RECORD add FundingSourceID int(11)"
); 

$sql_eClassIP_update[] = array(
	"2012-05-23",
	"Add GroupInCharge to INVENTORY_VARIANCE_HANDLING_REMINDER",
	"alter table INVENTORY_VARIANCE_HANDLING_REMINDER add GroupInCharge int(11)"
); 

$sql_eClassIP_update[] = array(
	"2012-05-23",
	"Add FundingSourceID to INVENTORY_VARIANCE_HANDLING_REMINDER",
	"alter table INVENTORY_VARIANCE_HANDLING_REMINDER add FundingSourceID int(11)"
); 

$sql_eClassIP_update[] = array(
	"2012-05-25",
	"add column \"ByStocktake\" to for write-off record",
	"ALTER TABLE INVENTORY_ITEM_WRITE_OFF_RECORD ADD COLUMN ByStocktake tinyint(1) default 0"
);

$sql_eClassIP_update[] = array(
	"2012-06-06",
	"eInventory",
	"ALTER TABLE INVENTORY_ITEM add StockTakeOption varchar(16) default 'FOLLOW_SETTING' "
);




$sql_eClassIP_update[] = array(
	"2012-06-08",
	"eSports > sports day - add event quota feature",
	"alter table SPORTS_EVENTGROUP add EventQuota int(11) default 0"
);

$sql_eClassIP_update[] = array(
	"2012-06-08",
	"eSports > swimming gala - add event quota feature",
	"alter table SWIMMINGGALA_EVENTGROUP add EventQuota int(11) default 0"
);

$sql_eClassIP_update[] = array(
	"2012-06-25",
	"eDiscipline - remove foreign key which cause failed to edit",
	"alter table DISCIPLINE_DETENTION_SESSION_CLASSLEVEL drop FOREIGN key DISCIPLINE_DETENTION_SESSION_CLASSLEVEL_ibfk_1"
);

$sql_eClassIP_update[] = array(
	"2012-06-25",
	"eDiscipline - remove foreign key which cause failed to edit",
	"alter table DISCIPLINE_DETENTION_SESSION_CLASSLEVEL drop FOREIGN key DISCIPLINE_DETENTION_SESSION_CLASSLEVEL_ibfk_2"
);

$sql_eClassIP_update[] = array(
	"2012-06-26",
	"eDiscipline ",
	"alter table DISCIPLINE_ACCU_CATEGORY_ITEM drop key ItemCode"
);


$sql_eClassIP_update[] = array(
	"2012-06-26",
	"eDiscipline ",
	"alter table DISCIPLINE_ACCU_CATEGORY_ITEM drop key CatName"
);


$sql_eClassIP_update[] = array(
	"2012-06-26",
	"Mail Merge ",
	"alter table INTRANET_MASS_MAILING add RecipientLoginID tinyint(1) after RecipientAddress"
);

$sql_eClassIP_update[] = array(
	"2012-06-28",
	"eBooking - Add field Event in table INTRANET_EBOOKING_RECORD",
	"ALTER TABLE INTRANET_EBOOKING_RECORD ADD Event varchar(255) DEFAULT NULL AFTER Attachment"
);

$sql_eClassIP_update[] = array(
	"2012-07-04",
	"eDiscipline - Conduct Grade Setting [CRM : 2012-0221-1557-57098]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_YEAR_GRADE_SETTING (
		YearGradeID int(11) NOT NULL AUTO_INCREMENT,
		YearID int(11) NOT NULL,
		GradeChar varchar(10) NOT NULL,
		Sequence int(5) NOT NULL default 1,
		DateInput datetime,
		InputBy int(11) ,
		DateModified datetime,
		ModifiedBy int(11),
		PRIMARY KEY (YearGradeID),
		INDEX YearID (YearID),
		INDEX Sequence (Sequence)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-07-04",
	"eDiscipline - Conduct Grade Detail Setting for each FORM [CRM : 2012-0221-1557-57098]",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_YEAR_GRADE_SETTING_DETAILS (
		SettingID int(11) NOT NULL AUTO_INCREMENT,
		YearGradeID int(11) NOT NULL,
		CriteriaValue mediumtext NOT NULL,
		RecordStatus int(1) NOT NULL default 1 COMMENT '1:in use / 0:not in use',
		Sequence int(5) NOT NULL default 1,
		DateInput datetime,
		InputBy int(11) ,
		DateModified datetime,
		ModifiedBy int(11),
		PRIMARY KEY (SettingID),
		INDEX YearGradeID (YearGradeID),
		INDEX RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-07-04",
	"eEnrol - add TimeDescription to INTRANET_ENROL_GROUPINFO",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO ADD TimeDescription text default NULL"
);

$sql_eClassIP_update[] = array(
	"2012-07-04",
	"eEnrol - add TimeDescription to INTRANET_ENROL_EVENTINFO",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD TimeDescription text default NULL"
);

$sql_eClassIP_update[] = array(
	"2012-07-09",
	"eEnrol - add DateInput. InputBy, DateModified and ModifiedBy field to INTRANET_ENROL_GROUP_DATE",
	"ALTER TABLE INTRANET_ENROL_GROUP_DATE 	ADD COLUMN DateInput datetime default Null,
											ADD COLUMN InputBy int(8) default Null,
											ADD COLUMN DateModified datetime default Null,
											ADD COLUMN ModifiedBy int(8) default Null
	"
);

$sql_eClassIP_update[] = array(
	"2012-07-09",
	"eEnrol - add DateInput. InputBy, DateModified and ModifiedBy field to INTRANET_ENROL_EVENT_DATE",
	"ALTER TABLE INTRANET_ENROL_EVENT_DATE 	ADD COLUMN DateInput datetime default Null,
											ADD COLUMN InputBy int(8) default Null,
											ADD COLUMN DateModified datetime default Null,
											ADD COLUMN ModifiedBy int(8) default Null
	"
);

$sql_eClassIP_update[] = array(
	"2012-07-09",
	"Timetable - add RelatedRoomAllocationID to INTRANET_TIMETABLE_ROOM_ALLOCATION",
	"ALTER TABLE INTRANET_TIMETABLE_ROOM_ALLOCATION ADD RelatedRoomAllocationID int(11) default NULL After SubjectGroupID,
													ADD Index RelatedRoomAllocationID (RelatedRoomAllocationID)
	"
);

$sql_eClassIP_update[] = array(
	"2012-07-09",
	"IP25 - Store Student Extra Info (Nationality, Place of birth, Admission Date)",
	"alter table INTRANET_USER_PERSONAL_SETTINGS add Nationality varchar(32), add Nationality_DateModified datetime, add PlaceOfBirth varchar(32), add PlaceOfBirth_DateModified datetime, add AdmissionDate date, add AdmissionDate_DateModified datetime"
);

$sql_eClassIP_update[] = array(
	"2012-07-10",
	"eInventory - allow 2 funding sources for single item",
	"alter table INVENTORY_ITEM_SINGLE_EXT add UnitPrice1 double(20,2), add FundingSource2 int(11), add UnitPrice2 double(20,2)"
);


$sql_eClassIP_update[] = array
(
	"2012-07-12",
	"login failue log",
	"CREATE TABLE IF NOT EXISTS  `INTRANET_LOGIN_FAILURE_LOG` (
	  `FailureID` int(11) NOT NULL auto_increment,
	  `UserLogin` varchar(20) NOT NULL,
	  `UserPassword` varchar(50) NOT NULL,
	  `SessionKey` varchar(128),
	  `RemoteAddr` varchar(128),
	  `AttemptTotal` int(4) default 0,
	  `EmailAlert` varchar(128),
	  `AttemptTimeFirst` datetime,
	  `AttemptTimeLast` datetime,
	  PRIMARY KEY (FailureID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-07-17",
	"INSERT REQUIRE_APPROVAL INTO WRITING 2.0 W2_STEP TO SUPPORT STEP NEED APPROVAL",
	"ALTER TABLE W2_STEP ADD `REQUIRE_APPROVAL` tinyint(3) default '0'"
);
$sql_eClassIP_update[] = array(
	"2012-07-19",
	"INSERT APPROVAL_STATUS INTO WRITING 2.0 W2_STEP_STUDENT TO SUPPORT STEP NEED TEACHER APPROVAL",
	"ALTER TABLE W2_STEP_STUDENT ADD `APPROVAL_STATUS` tinyint(3) default '0'"
);

$sql_eClassIP_update[] = array(
	"2012-07-25",
	"iPortal File - Add a new module",
	"INSERT INTO IPORTAL_FILE_MODULE SET CODE = 'SBA_IES', DATEINPUT = NOW(),DATEMODIFIED = NOW()"
);

$sql_eClassIP_update[] = array
(
	"2012-07-27",
	"log for house-keeping including refresh SMS",
	"CREATE TABLE IF NOT EXISTS INTRANET_HOUSEKEEPING_LOG (
	  `HouseKeepingID` int(11) NOT NULL auto_increment,
	  `UserID` int(8) NOT NULL,
	  `InputTime` datetime,	  
	  PRIMARY KEY (HouseKeepingID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add Introduction field in table IES_SCHEME",
	"ALTER TABLE IES_SCHEME ADD `Introduction` Text default NULL after `Title`"
);


$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add Introduction field in table IES_TASK",
	"ALTER TABLE IES_TASK ADD `Introduction` Text default NULL after `Title`"
);


$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add Description field in table IES_STEP",
	"ALTER TABLE IES_STEP ADD `Description` Text default NULL after `Title`"
);

$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add QuestionType field in table IES_STEP",
	"ALTER TABLE IES_STEP ADD `QuestionType` varchar(255) default NULL after `Description`"
);

$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add Question field in table IES_STEP",
	"ALTER TABLE IES_STEP ADD `Question` longtext default NULL after `QuestionType`"
);

$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add UserID field in table IES_SURVEY",
	"ALTER TABLE IES_SURVEY ADD `UserID` int(11) default NULL COMMENT 'this column used in IES COMBO only , get the survey belong to who in this table directly'"
);

$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add Index UserID in table IES_SURVEY",
	"ALTER TABLE `IES_SURVEY` ADD INDEX UserID (`UserID`)"
);

$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add SchemeID field in table IES_SURVEY",
	"ALTER TABLE IES_SURVEY ADD `SchemeID` int(8) default NULL COMMENT 'this column used in IES COMBO only,survey belongs to a scheme (not task as ies)'"
);

$sql_eClassIP_update[] = array
(
	"2012-07-30",
	"Add SchemeID field in table IES_SURVEY",
	"ALTER TABLE `IES_SURVEY` ADD INDEX SchemeID (`SchemeID`)"
);

$sql_eClassIP_update[] = array(
	"2012-07-30",
	"INSERT INTRANET MODULE SBA_IES",
	"INSERT IGNORE INTO INTRANET_MODULE (Code,Description,DateInput,DateModified) VALUES ('SBA_IES','SBA MODULE FOR IES ',now(),now())"
);

$sql_eClassIP_update[] = array(
	"2012-07-30",
	"ALTER TABLE IES_SCHEME, add RecordStatus",
	"ALTER TABLE IES_SCHEME ADD Column RecordStatus int(11) default 1 not null, ADD INDEX RecordStatus (RecordStatus)"
);

$sql_eClassIP_update[] = array(
	"2012-07-30",
	"ALTER TABLE IES_STAGE, add RecordStatus",
	"ALTER TABLE IES_STAGE ADD Column RecordStatus int(11) default 1 not null AFTER Weight, ADD INDEX RecordStatus (RecordStatus)"
);

$sql_eClassIP_update[] = array(
	"2012-07-30",
	"ALTER TABLE IES_STAGE_MARKING_CRITERIA, add RecordStatus",
	"ALTER TABLE IES_STAGE_MARKING_CRITERIA ADD Column RecordStatus int(11) default 1 not null AFTER Weight, ADD INDEX RecordStatus (RecordStatus)"
);

$sql_eClassIP_update[] = array(
	"2012-07-30",
	"CREATE TABLE `IES_COMBO_MARKING_CRITERIA`",
	"CREATE TABLE IF NOT EXISTS  `IES_COMBO_MARKING_CRITERIA` (
  		`MarkCriteriaID` int(8) NOT NULL auto_increment,
  		`TitleChi` varchar(255) default NULL,
  		`TitleEng` varchar(255) default NULL,
  		`isDefault` tinyint(1) default NULL,
  		`defaultMaxScore` float default NULL,
  		`defaultWeight` float default NULL,
  		`defaultSequence` int(8) default NULL,
  		`DateInput` timestamp NULL default NULL,
  		`InputBy` int(8) default NULL,
  		`DateModified` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  		`ModifyBy` int(8) default NULL,
  		PRIMARY KEY  (`MarkCriteriaID`)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-07-30",
	"CREATE TABLE `TOOLS_POWERCONCEPT`",
	"CREATE TABLE IF NOT EXISTS `TOOLS_POWERCONCEPT` (
		`PowerConceptID` int(8) NOT NULL auto_increment,
		`DataContent` longblob,
		`UserID` int(8) default NULL,
		`ModuleCode` varchar(255) default NULL,
		`InputDate` datetime default NULL,
		`ModifiedDate` datetime default NULL,
		PRIMARY KEY  (`PowerConceptID`)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-08-01",
	"eDis Study Score - create new table DISCIPLINE_STUDY_SCORE_CALCULATION_METHOD to store the study score calculation method",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_STUDY_SCORE_CALCULATION_METHOD 
	(
	 	MethodType int(11) default 0,
		RecordType int(11),
		RecordStatus int(11),
		InputBy int(11),
		DateInput datetime,
		ModifiedBy int(11),
		DateModified datetime
	)ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2012-08-02",
	"iPortfolio - Add field ComeFrom to table CONDUCT_STUDENT to record the data source",
	"ALTER TABLE CONDUCT_STUDENT ADD ComeFrom int(2) default null"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2012-08-02",
	"iPortfolio - Add field ComeFrom to table ASSESSMENT_STUDENT_MAIN_RECORD to record the data source",
	"ALTER TABLE ASSESSMENT_STUDENT_MAIN_RECORD ADD ComeFrom int(2) default null"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2012-08-02",
	"iPortfolio - Add field ComeFrom to table ASSESSMENT_STUDENT_SUBJECT_RECORD to record the data source",
	"ALTER TABLE ASSESSMENT_STUDENT_SUBJECT_RECORD ADD ComeFrom int(2) default null"
);

if($plugin['ClassDiary'])
{
	$sql_eClassIP_update[] = array(
		"2012-08-14",
		"Class Diary - Create table INTRANET_CLASS_DIARY",
		"CREATE TABLE IF NOT EXISTS INTRANET_CLASS_DIARY (
			DiaryID int(11) NOT NULL auto_increment,
			ClassID int(11) NOT NULL,
			DiaryDate date NOT NULL,
			Cycle varchar(20) DEFAULT NULL, 
			DateInput datetime,
			DateModified datetime,
			InputBy int(11),
			ModifiedBy int(11),
			PRIMARY KEY(DiaryID),
			UNIQUE KEY CompositeKey(ClassID,DiaryDate)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
		"2012-08-14",
		"Class Diary - Create table INTRANET_CLASS_DIARY_CLASS_LESSON",
		"CREATE TABLE IF NOT EXISTS INTRANET_CLASS_DIARY_CLASS_LESSON (
			LessonID int(11) NOT NULL auto_increment,
			DiaryID int(11) NOT NULL,
			LessonNumber int(11) NOT NULL,
			SubjectID int(11) NOT NULL,
			InstructionMediumP varchar(1),
			InstructionMediumE varchar(1),
			InstructionMediumC varchar(1),
			InstructionMediumB varchar(1),
			Assignment text,
			SubmissionDate date default NULL,
			Others text default NULL,
			DateInput datetime,
			DateModified datetime,
			InputBy int(11),
			ModifiedBy int(11),
			PRIMARY KEY (LessonID),
			INDEX IDiaryID(DiaryID),
			INDEX ISubjectID(SubjectID) 
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
		"2012-08-14",
		"Class Diary - Create table INTRANET_CLASS_DIARY_LESSON_STUDENT",
		"CREATE TABLE IF NOT EXISTS INTRANET_CLASS_DIARY_LESSON_STUDENT (
			LessonID int(11) NOT NULL,
			UserID int(11) NOT NULL,
			RecordType varchar(1) NOT NULL COMMENT 'F:forgot bring books or stationery; E:excused between lessons; M:not adopt medium of instruction',
			UNIQUE KEY CompositeKey(LessonID,UserID,RecordType) 
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
	
	$sql_eClassIP_update[] = array(
		"2012-08-14",
		"Class Diary - Create table INTRANET_CLASS_DIARY_PROFILE_STUDENT",
		"CREATE TABLE IF NOT EXISTS INTRANET_CLASS_DIARY_PROFILE_STUDENT (
			DiaryID int(11) NOT NULL,
			UserID int(11) NOT NULL,
			RecordType varchar(1) NOT NULL COMMENT '1:AM Absent,2:PM Absent,3:Late,4:Earlyleave',
			UNIQUE KEY CompositeKey(DiaryID,UserID,RecordType)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
	);
}

$sql_eClassIP_update[] = array(
	"2012-08-14",
	"eBooking (Door Access) - Create table INVENTORY_LOCATION_CARD_READER to store card reader information",
	"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION_CARD_READER (
		ReaderID		int(11)			NOT NULL auto_increment,
		LocationID 		int(11) 		NOT NULL,
		Code			varchar(64)		NOT NULL,
		NameEng			varchar(255)	NOT NULL,
		NameChi			varchar(255)	NOT NULL,
		Remarks			text			DEFAULT NULL,
		RecordStatus	tinyint(2)		NOT NULL,
		IsDeleted		tinyint(2)		DEFAULT 0,
		InputDate		datetime		NOT NULL,
		InputBy			int(11)			NOT NULL,
		ModifiedDate	datetime		NOT NULL,
		ModifiedBy		int(11)			NOT NULL,
		PRIMARY KEY (ReaderID),
		INDEX RecordStatusLocationID (RecordStatus, LocationID),
		INDEX LocationID (LocationID),
		INDEX RecordStatusCode (RecordStatus, Code) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-08-21",
	"iTextbook - Add Book Language Column",
	"ALTER TABLE ITEXTBOOK_BOOK ADD BookLang varchar(2) default 'en' after BookName"
);

$sql_eClassIP_update[] = array(
	"2012-08-22",
	"eBooking (Door Access) - Add ApplyDoorAccess field in table INTRANET_EBOOKING_RECORD",
	"ALTER TABLE INTRANET_EBOOKING_RECORD ADD ApplyDoorAccess tinyint(2) default 0 after IsReserve"
);

$sql_eClassIP_update[] = array(
	"2012-08-22",
	"eBooking (Door Access) - Create table INTRANET_EBOOKING_DOOR_ACCESS_RECORD to store the door access records for each booking",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_DOOR_ACCESS_RECORD (
		RecordID int(11) NOT NULL auto_increment,
	    BookingID int(11) NOT NULL,
		BookingDetailsID int(11) NOT NULL,
	    UserID int(11) NOT NULL,
		CardID varchar(255) NOT NULL,
		CardReaderID int(11) NOT NULL,
		DoorAccessRecordID int(11) NOT NULL,
		StartTime datetime NOT NULL,
		EndTime datetime NOT NULL,
	    InputBy int(11) NOT NULL,
		DateInput datetime NOT NULL,
	    ModifiedBy int(11) NOT NULL,
	    DateModified datetime NOT NULL,
	    PRIMARY KEY (RecordID),
	    UNIQUE KEY BookingIDBookingDetailsIDUserID (BookingID, BookingDetailsID, UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-08-23",
	"eBooking (Door Access) - allow null user input",
	"ALTER TABLE INVENTORY_LOCATION_CARD_READER CHANGE InputBy InputBy int(11) DEFAULT NULL,
												CHANGE ModifiedBy ModifiedBy int(11) DEFAULT NULL
	"
);

$sql_eClassIP_update[] = array(
	"2012-08-27",
	"Staff Attendance V3 - Customization table for storing doctor certificate files",
	"CREATE TABLE IF NOT EXISTS CARD_STAFF_ATTENDANCE3_DOCTOR_CERT_FILE (
		FileID int(11) NOT NULL auto_increment,
		DailyLogID int(11) NOT NULL,
		FileName varchar(256) NOT NULL,
		FilePath varchar(300) NOT NULL,
		UniqueID varchar(100) NOT NULL,
		FileSize float COMMENT 'Unit KB',
		Year varchar(4),
		Month varchar(2),
		DateInput datetime,
		InputBy int(11),
		PRIMARY KEY(FileID),
		INDEX IDailyLogID(DailyLogID), 
		INDEX IUniqueID(UniqueID),
		INDEX IFileName(FileName),
		INDEX IYear(Year),
		INDEX IMonth(Month) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-08-29",
	"eBooking (Door Access) - drop unique key",
	"ALTER TABLE INTRANET_EBOOKING_DOOR_ACCESS_RECORD Drop Index BookingIDBookingDetailsIDUserID"
);

$sql_eClassIP_update[] = array
(
	"2012-08-30",
	"Add Barcode field for INTRANET_USER",
	"ALTER TABLE INTRANET_USER ADD COLUMN Barcode varchar(30)"
);

$sql_eClassIP_update[] = array
(
	"2012-09-05",
	"eEnrol - Add ModifiedBy field for INTRANET_USERGROUP",
	"ALTER TABLE INTRANET_USERGROUP ADD COLUMN ModifiedBy int(11)"
);

$sql_eClassIP_update[] = array
(
	"2012-09-05",
	"eEnrol - Add ModifiedBy field for INTRANET_ENROL_EVENTSTUDENT",
	"ALTER TABLE INTRANET_ENROL_EVENTSTUDENT ADD COLUMN ModifiedBy int(11)"
);

$sql_eClassIP_update[] = array
(
	"2012-09-12",
	"Digital Archive - Add FromModule field to DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD for recognizing files from which module",
	"ALTER TABLE DIGITAL_ARCHIVE_ADMIN_DOCUMENT_RECORD ADD COLUMN FromModule varchar(100) DEFAULT NULL AFTER Thread"
);

$sql_eClassIP_update[] = array
(
	"2012-09-14",
	"SBA - add a field to distinguish IES and SBA for Comments",
	"ALTER TABLE IES_COMMENT ADD `SchemeType` varchar(20) default 'ies'"
);

$sql_eClassIP_update[] = array
(
	"2012-09-14",
	"SBA - add a field to distinguish IES and SBA for FAQ",
	"ALTER TABLE IES_FAQ ADD `SchemeType` varchar(20) default 'ies'"
);

$sql_eClassIP_update[] = array
(
	"2012-09-14",
	"SBA - add a field to distinguish IES and SBA for Comment Category",
	"ALTER TABLE IES_COMMENT_CATEGORY ADD `SchemeType` varchar(20) default 'ies'"
);

$sql_eClassIP_update[] = array
(
	"2012-09-14",
	"SBA - add a field to distinguish IES and SBA for default survey question",
	"ALTER TABLE IES_DEFAULT_SURVEY_QUESTION ADD `SchemeType` varchar(20) default 'ies'"
);

$sql_eClassIP_update[] = array
(
	"2012-09-14",
	"SBA - add fields for identifying the code and the version of a default SBA scheme",
	"ALTER TABLE IES_SCHEME ADD (`DefaultSchemeCode` int NOT NULL default 0,`DefaultSchemeVersion` int NOT NULL default 0, `Description` text NOT NULL)"
);




###### Library Management System ############

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_ACNO_LOG` (
	  `ACNO_ID` int(10) unsigned NOT NULL auto_increment,
	  `Key` char(8) NOT NULL,
	  `Next_Number` int(8) unsigned zerofill NOT NULL,
	  PRIMARY KEY  (`ACNO_ID`),
	  UNIQUE KEY `Key` (`Key`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='For autocomplete like function for ACNO (BookCode) in book r'  ;"
);


$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BOOK` (
	  `BookID` int(11) NOT NULL auto_increment,
	  `BookCode` varchar(16) NOT NULL COMMENT 'ACNO',
	  `ACNO_Prefix` varchar(16) NOT NULL,
	  `ACNO_Num` int(10) unsigned zerofill NOT NULL,
	  `CallNum` varchar(64) default NULL COMMENT 'as it can be null or unique, uniqueness should be checked using PHP if it is filled',
	  `CallNum2` varchar(64) default NULL COMMENT 'as it can be null or unique, uniqueness should be checked using PHP if it is filled',
	  `ISBN` varchar(64) default NULL COMMENT 'as it can be null or unique, uniqueness should be checked using PHP if it is filled',
	  `ISBN2` varchar(64) default NULL COMMENT 'as it can be null or unique, uniqueness should be checked using PHP if it is filled',
	  `BarCode` varchar(128) default NULL COMMENT 'as it can be null or unique, uniqueness should be checked using PHP if it is filled',
	  `NoOfCopy` int(2) default '1',
	  `NoOfCopyAvailable` int(2) default '1' COMMENT 'the number of copies allow to be borrowed',
	  `BookTitle` varchar(128) default NULL,
	  `BookSubTitle` varchar(128) default NULL,
	  `Introduction` varchar(255) default NULL,
	  `Language` varchar(32) default NULL,
	  `Country` varchar(128) default NULL,
	  `Edition` varchar(32) default NULL,
	  `PublishPlace` varchar(64) default NULL,
	  `Publisher` varchar(64) default NULL,
	  `PublishYear` varchar(32) default NULL,
	  `NoOfPage` int(4) default '1',
	  `RemarkInternal` varchar(32) default NULL COMMENT 'remark display in admin pages only',
	  `RemarkToUser` varchar(32) default NULL COMMENT 'remark to show to end user and also in the borrow book process for librarian''s reference',
	  `PurchaseDate` date default NULL,
	  `Distributor` varchar(64) default NULL,
	  `PurchaseNote` varchar(32) default NULL,
	  `PurchaseByDepartment` varchar(32) default NULL,
	  `ListPrice` decimal(10,2) default NULL,
	  `Discount` decimal(10,2) default NULL,
	  `PurchasePrice` decimal(10,2) default NULL,
	  `AccountDate` date default NULL,
	  `CirculationTypeCode` varchar(8) default NULL,
	  `ResourcesTypeCode` varchar(8) default NULL,
	  `BookCategoryCode` varchar(8) default NULL,
	  `eClassBookCode` varchar(8) default NULL,
	  `LocationCode` varchar(8) default NULL,
	  `Subject` varchar(32) default NULL,
	  `Series` varchar(64) default NULL,
	  `SeriesNum` varchar(32) default NULL,
	  `URL` varchar(255) default NULL,
	  `ResponsibilityCode1` varchar(8) default NULL,
	  `ResponsibilityBy1` varchar(64) default NULL,
	  `ResponsibilityCode2` varchar(8) default NULL,
	  `ResponsibilityBy2` varchar(64) default NULL,
	  `ResponsibilityCode3` varchar(8) default NULL,
	  `ResponsibilityBy3` varchar(64) default NULL,
	  `CoverImage` varchar(128) default NULL,
	  `OpenBorrow` tinyint(1) default '0',
	  `OpenReservation` tinyint(1) default '0',
	  `RecordStatus` varchar(16) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`BookID`),
	  UNIQUE KEY `BookCode` (`BookCode`),
	  KEY `ACNO_Prefix` (`ACNO_Prefix`,`ACNO_Num`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;"
);


$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BOOK_CATEGORY` (
	  `BookCategoryCode` varchar(8) NOT NULL,
	  `DescriptionEn` varchar(64) default NULL,
	  `DescriptionChi` varchar(64) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  UNIQUE KEY `BookCategoryCode` (`BookCategoryCode`),
	  UNIQUE KEY `both_en_chi` (`DescriptionEn`,`DescriptionChi`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BOOK_OPTION` (
	  `BookOptionID` int(11) NOT NULL auto_increment,
	  `BookID` int(11) default NULL,
	  `LinkedToBookID` int(11) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`BookOptionID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BOOK_TAG` (
	  `BookTagID` int(11) NOT NULL auto_increment,
	  `BookID` int(11) NOT NULL,
	  `TagID` int(11) NOT NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`BookTagID`),
	  UNIQUE KEY `TagBook` (`TagID`,`BookID`),
	  KEY `BookID` (`BookID`),
	  KEY `TagID` (`TagID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BOOK_UNIQUE` (
	  `UniqueID` int(11) NOT NULL auto_increment,
	  `BarCode` varchar(128) default NULL,
	  `BookID` int(11) NOT NULL,
	  `BorrowLogID` int(11) NOT NULL default '0' COMMENT 'current burrow log that this book binds to ',
	  `RecordStatus` varchar(16) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`UniqueID`),
	  UNIQUE KEY `BarCode` (`BarCode`),
	  KEY `BookID` (`BookID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BOOK_WRITE_OFF` (
	  `WriteOffID` int(11) NOT NULL auto_increment,
	  `BookID` int(11) NOT NULL,
	  `UserID` int(11) NOT NULL,
	  `Remark` varchar(32) default NULL,
	  `NoOfCopy` int(2) default '1',
	  `WriteOffTime` datetime default NULL,
	  `RecordStatus` varchar(16) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`WriteOffID`),
	  KEY `BookID` (`BookID`),
	  KEY `TagID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BORROW_LOG` (
	  `BorrowLogID` int(11) NOT NULL auto_increment,
	  `BookID` int(11) NOT NULL,
	  `UniqueID` int(11) NOT NULL,
	  `UserID` int(11) NOT NULL,
	  `BorrowTime` datetime NOT NULL,
	  `DueDate` date NOT NULL COMMENT 'it will be calculated according user group, circulation type, ReturnDuration and Holidays ',
	  `RenewalTime` int(2) default '0' COMMENT '0 for the first borrow; 1 for the first renewal; it should not exceed LimitRenew',
	  `ReturnedTime` datetime NOT NULL,
	  `RecordStatus` varchar(16) default NULL COMMENT '1 for active; 0 for returned;',
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`BorrowLogID`),
	  KEY `BookID` (`BookID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_CIRCULATION_TYPE` (
	  `CirculationTypeCode` varchar(8) NOT NULL,
	  `DescriptionEn` varchar(64) default NULL,
	  `DescriptionChi` varchar(64) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  UNIQUE KEY `CirculationTypeCode` (`CirculationTypeCode`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_CLOSE_TIME` (
	  `CloseID` int(11) NOT NULL,
	  `Description` varchar(128) NOT NULL,
	  `DateForm` datetime NOT NULL,
	  `DateEnd` datetime NOT NULL,
	  `DateModified` datetime NOT NULL,
	  `LastModifiedBy` int(11) NOT NULL,
	  PRIMARY KEY  (`CloseID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_CSV_TMP` (
	  `id` int(15) NOT NULL auto_increment,
	  `BookTitle` varchar(255) collate utf8_unicode_ci NOT NULL default '',
	  `BookSubTitle` varchar(255) collate utf8_unicode_ci default NULL,
	  `BookCode` varchar(255) collate utf8_unicode_ci default NULL,
	  `CallNum` varchar(255) collate utf8_unicode_ci default NULL,
	  `CallNum2` varchar(255) collate utf8_unicode_ci default NULL,
	  `ISBN` varchar(255) collate utf8_unicode_ci default NULL,
	  `ISBN2` varchar(255) collate utf8_unicode_ci default NULL,
	  `Language` varchar(255) collate utf8_unicode_ci default NULL,
	  `Country` varchar(255) collate utf8_unicode_ci default NULL,
	  `RemarkInternal` varchar(255) collate utf8_unicode_ci default NULL,
	  `Edition` varchar(255) collate utf8_unicode_ci default NULL,
	  `PublishYear` varchar(255) collate utf8_unicode_ci default NULL,
	  `Publisher` varchar(255) collate utf8_unicode_ci default NULL,
	  `PublishPlace` varchar(255) collate utf8_unicode_ci default NULL,
	  `Series` varchar(255) collate utf8_unicode_ci default NULL,
	  `SeriesNum` varchar(255) collate utf8_unicode_ci default NULL,
	  `ResponsibilityCode1` varchar(255) collate utf8_unicode_ci default NULL,
	  `ResponsibilityBy1` varchar(255) collate utf8_unicode_ci default NULL,
	  `ResponsibilityCode2` varchar(255) collate utf8_unicode_ci default NULL,
	  `ResponsibilityBy2` varchar(255) collate utf8_unicode_ci default NULL,
	  `ResponsibilityCode3` varchar(255) collate utf8_unicode_ci default NULL,
	  `ResponsibilityBy3` varchar(255) collate utf8_unicode_ci default NULL,
	  `NoOfPage` varchar(255) collate utf8_unicode_ci default NULL,
	  `BookCategory` varchar(255) collate utf8_unicode_ci default NULL,
	  `BookCirclation` varchar(255) collate utf8_unicode_ci default NULL,
	  `BookResources` varchar(255) collate utf8_unicode_ci default NULL,
	  `BookLocation` varchar(255) collate utf8_unicode_ci default NULL,
	  `Subject` varchar(255) collate utf8_unicode_ci default NULL,
	  `AccountDate` varchar(255) collate utf8_unicode_ci default NULL,
	  `BookInternalremark` varchar(255) collate utf8_unicode_ci default NULL,
	  `PurchaseDate` varchar(255) collate utf8_unicode_ci default NULL,
	  `PurchasePrice` varchar(255) collate utf8_unicode_ci default NULL,
	  `ListPrice` varchar(255) collate utf8_unicode_ci default NULL,
	  `Discount` varchar(255) collate utf8_unicode_ci default NULL,
	  `Distributor` varchar(255) collate utf8_unicode_ci default NULL,
	  `PurchaseByDepartment` varchar(255) collate utf8_unicode_ci default NULL,
	  `PurchaseNote` varchar(255) collate utf8_unicode_ci default NULL,
	  `NoOfCopy` varchar(255) collate utf8_unicode_ci default NULL,
	  `OpenBorrow` varchar(255) collate utf8_unicode_ci default NULL,
	  `OpenReservation` varchar(255) collate utf8_unicode_ci default NULL,
	  `RemarkToUser` varchar(255) collate utf8_unicode_ci default NULL,
	  `Tags` varchar(255) collate utf8_unicode_ci default NULL,
	  `URL` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode1` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode2` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode3` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode4` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode5` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode6` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode7` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode8` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode9` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode10` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode11` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode12` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode13` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode14` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode15` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode16` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode18` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode19` varchar(255) collate utf8_unicode_ci default NULL,
	  `barcode20` varchar(255) collate utf8_unicode_ci default NULL,
	  `DateModified` datetime NOT NULL,
	  `LastModifiedBy` int(11) NOT NULL,
	  `barcode17` varchar(255) collate utf8_unicode_ci default NULL,
	  PRIMARY KEY  (`BookTitle`),
	  UNIQUE KEY `id` (`id`)
	) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_GROUP` (
	  `GroupID` int(11) NOT NULL auto_increment,
	  `GroupTitle` varchar(128) default NULL,
	  `GroupCode` varchar(8) NOT NULL,
	  `IsDefault` tinyint(1) default '0' COMMENT '1 for all admin rights; if 0, need to check group rights set for each section and functions.',
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`GroupID`),
	  UNIQUE KEY `GroupCode` (`GroupCode`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_GROUP_CIRCULATION` (
	  `GroupCirculationID` int(11) NOT NULL auto_increment,
	  `GroupID` int(11) NOT NULL,
	  `CirculationTypeCode` varchar(8) default NULL,
	  `LimitBorrow` int(4) NOT NULL COMMENT 'max. number of books being borrowed (currently)',
	  `LimitRenew` int(4) NOT NULL COMMENT 'max. time of renewing the borrowed book',
	  `LimitReserve` int(4) NOT NULL COMMENT 'max. number of (current) reservation allowed',
	  `ReturnDuration` int(4) NOT NULL COMMENT 'once the book is borrow, how many days the book the user can keep the book.',
	  `OverDueCharge` float NOT NULL COMMENT 'charge per book per day',
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`GroupCirculationID`),
	  UNIQUE KEY `GroupCirculation` (`GroupID`,`CirculationTypeCode`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_GROUP_RIGHT` (
	  `GroupRightID` int(11) NOT NULL auto_increment,
	  `GroupID` int(11) NOT NULL,
	  `Section` varchar(64) default NULL,
	  `Function` varchar(64) default NULL,
	  `IsAllow` tinyint(1) default '0',
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`GroupRightID`),
	  KEY `GroupID` (`GroupID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_GROUP_USER` (
	  `GroupUserID` int(11) NOT NULL auto_increment,
	  `GroupID` int(11) NOT NULL,
	  `UserID` int(11) NOT NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`GroupUserID`),
	  UNIQUE KEY `GroupUser` (`GroupID`,`UserID`),
	  KEY `GroupID` (`GroupID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_HOLIDAY` (
	  `HolidayID` int(11) NOT NULL auto_increment,
	  `Event` varchar(128) NOT NULL,
	  `DateFrom` datetime NOT NULL,
	  `DateEnd` datetime NOT NULL,
	  `IntranetEventID` int(11) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`HolidayID`),
	  KEY `IntranetEventID` (`IntranetEventID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_LABEL_FORMAT` (
	  `id` int(10) unsigned NOT NULL auto_increment,
	  `name` varchar(64) NOT NULL,
	  `paper-size-width` double unsigned NOT NULL,
	  `paper-size-height` double unsigned NOT NULL,
	  `metric` char(3) NOT NULL default 'mm',
	  `lMargin` double NOT NULL COMMENT 'left page margin',
	  `tMargin` double NOT NULL COMMENT 'top page margin',
	  `NX` int(3) NOT NULL COMMENT 'number of label x',
	  `NY` int(3) NOT NULL COMMENT 'number of label y',
	  `SpaceX` double unsigned NOT NULL,
	  `SpaceY` double unsigned NOT NULL,
	  `width` double unsigned NOT NULL,
	  `height` double unsigned NOT NULL,
	  `font-size` int(11) unsigned NOT NULL,
	  `lineHeight` double unsigned NOT NULL,
	  `printing_margin_h` double NOT NULL,
	  `printing_margin_v` double NOT NULL,
	  `max_barcode_width` double unsigned NOT NULL,
	  `max_barcode_height` double unsigned NOT NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`id`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_LOCATION` (
	  `LocationCode` varchar(8) NOT NULL,
	  `DescriptionEn` varchar(64) default NULL,
	  `DescriptionChi` varchar(64) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  UNIQUE KEY `LocationCode` (`LocationCode`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_MARC_TMP` (
	  `id` int(15) unsigned NOT NULL auto_increment,
	  `f000` varchar(100) collate utf8_unicode_ci default NULL,
	  `f001` varchar(100) collate utf8_unicode_ci default NULL,
	  `f005` varchar(100) collate utf8_unicode_ci default NULL,
	  `f008` varchar(100) collate utf8_unicode_ci default NULL,
	  `f010` varchar(100) collate utf8_unicode_ci default NULL,
	  `f015` varchar(100) collate utf8_unicode_ci default NULL,
	  `f020` varchar(100) collate utf8_unicode_ci default NULL,
	  `f022` varchar(100) collate utf8_unicode_ci default NULL,
	  `f025` varchar(100) collate utf8_unicode_ci default NULL,
	  `f035` varchar(100) collate utf8_unicode_ci default NULL,
	  `f040` varchar(100) collate utf8_unicode_ci default NULL,
	  `f041` varchar(100) collate utf8_unicode_ci default NULL,
	  `f042` varchar(100) collate utf8_unicode_ci default NULL,
	  `f043` varchar(100) collate utf8_unicode_ci default NULL,
	  `f050` varchar(100) collate utf8_unicode_ci default NULL,
	  `f060` varchar(100) collate utf8_unicode_ci default NULL,
	  `f082` varchar(100) collate utf8_unicode_ci default NULL,
	  `f090` varchar(100) collate utf8_unicode_ci default NULL,
	  `f095` varchar(100) collate utf8_unicode_ci default NULL,
	  `f097` varchar(100) collate utf8_unicode_ci default NULL,
	  `f099` varchar(100) collate utf8_unicode_ci default NULL,
	  `f100` varchar(100) collate utf8_unicode_ci default NULL,
	  `f110` varchar(100) collate utf8_unicode_ci default NULL,
	  `f111` varchar(100) collate utf8_unicode_ci default NULL,
	  `f240` varchar(100) collate utf8_unicode_ci default NULL,
	  `f245` varchar(100) collate utf8_unicode_ci default NULL,
	  `f246` varchar(100) collate utf8_unicode_ci default NULL,
	  `f250` varchar(100) collate utf8_unicode_ci default NULL,
	  `f260` varchar(100) collate utf8_unicode_ci default NULL,
	  `f264` varchar(100) collate utf8_unicode_ci default NULL,
	  `f300` varchar(100) collate utf8_unicode_ci default NULL,
	  `f336` varchar(100) collate utf8_unicode_ci default NULL,
	  `f337` varchar(100) collate utf8_unicode_ci default NULL,
	  `f338` varchar(100) collate utf8_unicode_ci default NULL,
	  `f440` varchar(100) collate utf8_unicode_ci default NULL,
	  `f490` varchar(100) collate utf8_unicode_ci default NULL,
	  `f500` varchar(100) collate utf8_unicode_ci default NULL,
	  `f504` varchar(100) collate utf8_unicode_ci default NULL,
	  `f505` varchar(100) collate utf8_unicode_ci default NULL,
	  `f510` varchar(100) collate utf8_unicode_ci default NULL,
	  `f520` varchar(100) collate utf8_unicode_ci default NULL,
	  `f521` varchar(100) collate utf8_unicode_ci default NULL,
	  `f546` varchar(100) collate utf8_unicode_ci default NULL,
	  `f561` varchar(100) collate utf8_unicode_ci default NULL,
	  `f586` varchar(100) collate utf8_unicode_ci default NULL,
	  `f590` varchar(100) collate utf8_unicode_ci default NULL,
	  `f600` varchar(100) collate utf8_unicode_ci default NULL,
	  `f610` varchar(100) collate utf8_unicode_ci default NULL,
	  `f630` varchar(100) collate utf8_unicode_ci default NULL,
	  `f650` varchar(100) collate utf8_unicode_ci default NULL,
	  `f651` varchar(100) collate utf8_unicode_ci default NULL,
	  `f653` varchar(100) collate utf8_unicode_ci default NULL,
	  `f655` varchar(100) collate utf8_unicode_ci default NULL,
	  `f690` varchar(100) collate utf8_unicode_ci default NULL,
	  `f700` varchar(100) collate utf8_unicode_ci default NULL,
	  `f710` varchar(100) collate utf8_unicode_ci default NULL,
	  `f730` varchar(100) collate utf8_unicode_ci default NULL,
	  `f740` varchar(100) collate utf8_unicode_ci default NULL,
	  `f800` varchar(100) collate utf8_unicode_ci default NULL,
	  `f830` varchar(100) collate utf8_unicode_ci default NULL,
	  `f856` varchar(100) collate utf8_unicode_ci default NULL,
	  `f886` varchar(100) collate utf8_unicode_ci default NULL,
	  `f906` varchar(100) collate utf8_unicode_ci default NULL,
	  `f920` varchar(100) collate utf8_unicode_ci default NULL,
	  `f922` varchar(100) collate utf8_unicode_ci default NULL,
	  `f923` varchar(100) collate utf8_unicode_ci default NULL,
	  `f925` varchar(100) collate utf8_unicode_ci default NULL,
	  `f952` varchar(100) collate utf8_unicode_ci default NULL,
	  `f955` varchar(100) collate utf8_unicode_ci default NULL,
	  `f984` varchar(100) collate utf8_unicode_ci default NULL,
	  `f985` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991a` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991b` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991c` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991d` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991e` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991f` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991g` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991h` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991i` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991j` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991k` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991l` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991m` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991n` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991o` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991p` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991q` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991r` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991s` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991t` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991u` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991v` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991w` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991x` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991y` varchar(100) collate utf8_unicode_ci default NULL,
	  `f991z` varchar(100) collate utf8_unicode_ci default NULL,
	  `f260a` varchar(100) collate utf8_unicode_ci default NULL,
	  `f260b` varchar(100) collate utf8_unicode_ci default NULL,
	  `f260c` varchar(100) collate utf8_unicode_ci default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`id`)
	) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_OPEN_TIME` (
	  `OpenTimeID` int(11) NOT NULL auto_increment,
	  `Weekday` enum('Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday') NOT NULL,
	  `OpenTime` time default NULL,
	  `CloseTime` time default NULL,
	  `Open` tinyint(1) NOT NULL default '0',
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`OpenTimeID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_OPEN_TIME_SPECIAL` (
	  `OpenTimeSpecialID` int(11) NOT NULL auto_increment,
	  `DateFrom` date default NULL,
	  `DateEnd` date default NULL,
	  `OpenTime` time default NULL,
	  `CloseTime` time default NULL,
	  `Open` tinyint(1) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`OpenTimeSpecialID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_OVERDUE_LOG` (
	  `OverDueLogID` int(11) NOT NULL auto_increment,
	  `BorrowLogID` int(11) NOT NULL COMMENT 'refer to LIBMS_BORROW_LOG for the borrow details like bookID',
	  `DaysCount` int(4) default NULL COMMENT 'number of days counted for late returning of the book.',
	  `Payment` decimal(10,2) default NULL COMMENT 'payment = overdue charge per day * DaysCount',
	  `IsWaived` tinyint(1) default '0' COMMENT 'mark it as waived if admin or librarian sets it to be waived',
	  `PaymentReceived` decimal(10,2) default NULL COMMENT 'actual amount of money received',
	  `PaymentMethod` varchar(16) default NULL COMMENT 'payment method (CASH, ePayment)',
	  `PaymentTime` datetime default NULL,
	  `RecordStatus` varchar(16) default NULL COMMENT '1 for active; 0 for settled ',
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`OverDueLogID`),
	  KEY `BorrowLogID` (`BorrowLogID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_RESERVATION_LOG` (
	  `ReservationID` int(11) NOT NULL auto_increment,
	  `BookID` int(11) NOT NULL,
	  `UserID` int(11) NOT NULL,
	  `ReserveTime` datetime NOT NULL,
	  `ExpiryDate` date NOT NULL COMMENT 'user can set this date and this reservation will get expired if no book can be borrowed on or before this date',
	  `RecordStatus` varchar(16) default NULL COMMENT '2 for assigned; 1 for active; 0 for cancelled; -1 for expired.',
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`ReservationID`),
	  KEY `BookID` (`BookID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_RESOURCES_TYPE` (
	  `ResourcesTypeCode` varchar(8) NOT NULL,
	  `DescriptionEn` varchar(64) default NULL,
	  `DescriptionChi` varchar(64) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  UNIQUE KEY `ResourcesTypeCode` (`ResourcesTypeCode`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_RESPONSIBILITY` (
	  `ResponsibilityCode` varchar(8) NOT NULL,
	  `DescriptionEn` varchar(64) default NULL,
	  `DescriptionChi` varchar(64) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  UNIQUE KEY `ResponsibilityCode` (`ResponsibilityCode`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_TAG` (
	  `TagID` int(11) NOT NULL auto_increment,
	  `TagName` varchar(200) NOT NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY  (`TagID`),
	  UNIQUE KEY `TagName` (`TagName`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_TRANSACTION` (
	  `TransactionID` int(11) NOT NULL,
	  `OverDueLogID` int(11) NOT NULL,
	  `UserID` int(11) NOT NULL,
	  `Amount` decimal(10,2) NOT NULL,
	  `DateModified` datetime NOT NULL,
	  `LastModifiedBy` int(11) NOT NULL,
	  KEY `OverDueLogID` (`OverDueLogID`),
	  KEY `UserID` (`UserID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_USER` (
	  `UserID` int(8) NOT NULL auto_increment,
	  `UserLogin` varchar(20) default NULL,
	  `UserEmail` varchar(128) default NULL,
	  `EnglishName` varchar(128) default NULL,
	  `ChineseName` varchar(128) default NULL,
	  `UserType` varchar(8) default NULL,
	  `ClassNumber` int(8) default NULL,
	  `ClassName` varchar(255) default NULL,
	  `ClassLevel` varchar(20) default NULL,
	  `RecordStatus` char(2) default NULL,
	  `HomeTelNo` varchar(20) default NULL,
	  `MobileTelNo` varchar(20) default NULL,
	  `BarCode` varchar(64) default NULL,
	  `UserPhoto` varchar(255) default NULL,
	  `LastUsed` datetime default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  `Balance` decimal(10,2) NOT NULL COMMENT 'Overdue Payment Balance',
	  PRIMARY KEY  (`UserID`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2012-09-18",
	"LIBMS",
	"INSERT INTO `LIBMS_OPEN_TIME` (`OpenTimeID`, `Weekday`, `OpenTime`, `CloseTime`, `Open`, `DateModified`, `LastModifiedBy`) VALUES
	(1, 'Monday', null, null, 0, null, null),
	(2, 'Tuesday', null, null, 0, null, null),
	(3, 'Wednesday', null, null, 0, null, null),
	(4, 'Thursday', null, null, 0, null, null),
	(5, 'Friday', null, null, 0, null, null),
	(6, 'Saturday', null, null, 0, null, null),
	(7, 'Sunday', null, null, 0, null, null);"
);


###### Library Management System ############


$sql_eClassIP_update[] = array(
	"2012-09-20",
	"eInventory - allow 2 funding sources for single item",
	"alter table INTRANET_ELIB_BOOK add CoverImage varchar(128)"
);



##### eLibrary - Reading Scheme - Booklist - for import book temp table #####
$sql_eClassIP_update[] = array(
	"2012-09-21",
	"Reading Scheme > Booklist - Import Book Temp Table",
	"CREATE TABLE IF NOT EXISTS TEMP_READING_SCHEME_BOOK_IMPORT
	 (
		TempID 					int(11) 		NOT NULL auto_increment,
		UserID 					int(11)			default NULL,
		RowNumber 				int(11)			default NULL,
		BookName 				varchar(255)	default NULL,
		CallNumber 				varchar(20)		default NULL,
		ISBN 					varchar(20)		default NULL,
		Author 					varchar(255)	default NULL,
		Publisher 				varchar(255)	default NULL,
		Language 				varchar(255)	default NULL,
		CategoryName			varchar(255)	default NULL,
		CategoryID				int(11)			default NULL,
		Description 			text			default NULL,
		YearName 				varchar(255)	default NULL,
		YearID					int(11)			default NULL,
		DefaultAnswerSheet		mediumtext		default NULL,
		DefaultAnswerSheetID 	int(11)			default NULL,
		DateInput 				datetime 		default NULL,
		PRIMARY KEY (TempID),
		KEY UserIDRowNumber (UserID, RowNumber)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


##### eLibrary - Reading Scheme - Booklist - for import book temp table #####
$sql_eClassIP_update[] = array(
	"2012-09-24",
	"Reading Scheme > Booklist - Import Book Temp Table",
	"ALTER TABLE TEMP_READING_SCHEME_BOOK_IMPORT 	ADD COLUMN YearIDList varchar(255) After YearName,
													DROP COLUMN YearID
	"
);

##### eLibrary - Reading Scheme - Booklist - revise temp table #####
$sql_eClassIP_update[] = array(
	"2012-09-25",
	"Reading Scheme > Booklist - Import Book Temp Table",
	"ALTER TABLE TEMP_READING_SCHEME_BOOK_IMPORT 	ADD COLUMN CategoryCode varchar(20) DEFAULT NULL AFTER Language,
													DROP COLUMN CategoryName
																									
	"
);


$sql_eClassIP_update[] = array(
	"2012-09-25",
	"Reading Scheme > Popular Reading Award Scheme - Import Award Temp Table",
	"CREATE TABLE IF NOT EXISTS TEMP_READING_SCHEME_AWARD_IMPORT
	 (
		TempID 					int(11) 		NOT NULL auto_increment,
		UserID 					int(11)			default NULL,
		RowNumber 				int(11)			default NULL,
		ClassName 				varchar(255)		default NULL,
		ClassNumber				varchar(100)		default NULL,
		AcademicYearID				int(11)			default NULL,
		UserLogin 				varchar(100)		default NULL,
		StudentID				int(11)			default NULL,
		ItemCode 				varchar(20)		default NULL,
		ItemID					int(11)			default NULL,
		Times 					int(11)			default NULL,
		DateInput 				datetime 		default NULL,
		PRIMARY KEY (TempID),
		KEY UserIDRowNumber (UserID, RowNumber)
	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


# Document Routing
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ENTRY
//	 (
//		DocumentID				int(11) 		NOT NULL auto_increment,
//		UserID 					int(11)			NOT NULL,
//		Title	 				varchar(128)	NOT NULL,
//		Instruction 			text			default NULL,
//		DocumentType			varchar(32)		default NULL,
//		AllowAdHocRoute			tinyint(1) 		default 0,
//		RecordStatus 			int(1)			default 1,
//		DateInput 				datetime 		NOT NULL,
//		DateModified 			datetime 		default NULL,
//		PRIMARY KEY (DocumentID),
//		INDEX UserID (UserID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);
//
//
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTES
//	 (
//		RouteID					int(11) 		NOT NULL auto_increment,
//		DocumentID				int(11) 		NOT NULL,
//		UserID 					int(11)			NOT NULL,
//		Note 					text			default NULL,
//		ActionType				varchar(32)		default NULL,
//		EffectiveType			varchar(32)		default NULL,
//		EffectiveStartDate 		datetime 		default NULL,
//		EffectiveEndDate		datetime 		default NULL,
//		ViewRight				varchar(32)		default NULL,
//		ReplySlipContent 		LONGTEXT		default NULL,
//		DateInput 				datetime 		NOT NULL,
//		DateModified 			datetime 		default NULL,
//		PRIMARY KEY (RouteID),
//		INDEX DocumentID (DocumentID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);
//
//
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_PRESET
//	 (
//		PresetID				int(11) 		NOT NULL auto_increment,
//		UserID 					int(11)			NOT NULL,
//		PresetType				varchar(32)		NOT NULL,
//		PresentContents 		text			default NULL,
//		IsForAll				tinyint(1) 		default 0,
//		DateInput 				datetime 		NOT NULL,
//		DateModified 			datetime 		default NULL,
//		PRIMARY KEY (PresetID),
//		INDEX PresetType (PresetType),
//		INDEX UserID (UserID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);
//
//
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_FILE
//	 (
//		FileID					int(11) 		NOT NULL auto_increment,
//		FilePath 				varchar(255)	NOT NULL,
//		LinkToType				varchar(32)		NOT NULL,
//		LinkToID 				int(11)			NOT NULL,
//		DateInput 				datetime 		NOT NULL,
//		DateModified 			datetime 		default NULL,
//		PRIMARY KEY (FileID),
//		INDEX LinkIndex (LinkToType, LinkToID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);
//
//
//
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTE_TARGER
//	 (
//		RouteID 				int(11)			NOT NULL,
//		UserID 					int(11)			NOT NULL,
//		AccessRight				varchar(32)		NOT NULL,
//		DateInput 				datetime 		NOT NULL,
//		INDEX TargetIndex (RouteID, UserID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);
//
//
//
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_TAG
//	 (
//		DocumentID 				int(11)			NOT NULL,
//		TagID 					int(11)			NOT NULL,
//		DateInput 				datetime 		NOT NULL,
//		INDEX TagIndex (DocumentID, TagID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);
//
//
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTE_FEEDBACK
//	 (
//		FeedbackID				int(11) 		NOT NULL auto_increment,
//		DocumentID 				int(11)			NOT NULL,
//		RouteID					int(11)			NOT NULL,
//		UserID 					int(11)			NOT NULL,
//		ActionType 				varchar(32)		NOT NULL,
//		Comment 				text			NOT NULL,
//		ReplySlipAnswer			LONGTEXT		NOT NULL,
//		DateInput 				datetime 		NOT NULL,
//		DateModified 			datetime 		default NULL,
//		PRIMARY KEY (FeedbackID),
//		INDEX FeedbackIndex (DocumentID, RouteID, UserID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);
//
//
//$sql_eClassIP_update[] = array(
//	"2012-10-03",
//	"Document Routing",
//	"CREATE TABLE IF NOT EXISTS INTRANET_DR_STAR
//	 (
//		DocumentID 				int(11)			NOT NULL,
//		UserID 					int(11)			NOT NULL,
//		DateInput 				datetime 		NOT NULL,
//		INDEX StarIndex (DocumentID, UserID)
//	 ) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//);



$sql_LIBMS_update[] = array(
	"2012-10-06",
	"LIBMS",
	"ALTER TABLE `LIBMS_BOOK_UNIQUE` change `RecordStatus` `RecordStatus` varchar(16) character set utf8 collate utf8_unicode_ci default NULL"
);

$sql_LIBMS_update[] = array(
	"2012-10-06",
	"LIBMS",
	"ALTER TABLE `LIBMS_BOOK_UNIQUE` add `InvoiceNumber` varchar(64) default NULL,
	  add `Distributor` varchar(64) default NULL,
	  add `Discount` varchar(64) default NULL,
	  add `PurchaseDate` varchar(64) default NULL,
	  add `PurchasePrice` decimal(10,2) default NULL,
	  add `PurchaseNote` varchar(64) default NULL,
	  add `CreationDate` datetime default NULL"
);


$sql_LIBMS_update[] = array(
	"2012-10-06",
	"LIBMS",
	"ALTER TABLE `LIBMS_MARC_TMP` 
		add `f100a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f110g` varchar(100) collate utf8_unicode_ci default NULL,
		add `f700a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f245a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f245b` varchar(100) collate utf8_unicode_ci default NULL,
		add `f090a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f090b` varchar(100) collate utf8_unicode_ci default NULL,
		add `f250a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f300a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f300b` varchar(100) collate utf8_unicode_ci default NULL,
		add `f300c` varchar(100) collate utf8_unicode_ci default NULL,
		add `f300e` varchar(100) collate utf8_unicode_ci default NULL,
		add `f690a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f440a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f440v` varchar(100) collate utf8_unicode_ci default NULL,
		add `f650a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f020a` varchar(100) collate utf8_unicode_ci default NULL,
		add `f500a` varchar(100) collate utf8_unicode_ci default NULL"
);

$sql_eClassIP_update[] = array(
	"2012-10-18",
	"iMail - Added new field RequestUpdate to INTRANET_CAMPUSMAIL_USED_STORAGE for notifying which users required to recalculate total used quota",
	"ALTER TABLE INTRANET_CAMPUSMAIL_USED_STORAGE ADD COLUMN RequestUpdate varchar(1) default '1' COMMENT '1:need to update total used quota;otherwise do nothing'"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_ENTRY",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ENTRY (
	  DocumentID int(11) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  Title varchar(128) NOT NULL,
	  Instruction text,
	  DocumentType varchar(32) default NULL,
	  AllowAdHocRoute tinyint(1) default '0',
	  RecordStatus int(1) default '1' COMMENT '1:active,2:inactive',
	  DateInput datetime NOT NULL,
	  DateModified datetime default NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (DocumentID),
	  KEY UserID (UserID),
	  KEY RecordStatus (RecordStatus),
	  KEY Title (Title)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_FILE",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_FILE (
	  FileID int(11) NOT NULL auto_increment,
	  FilePath varchar(255) NOT NULL,
	  FileName varchar(255) NOT NULL,
	  LinkToType varchar(32) NOT NULL,
	  LinkToID int(11) NOT NULL,
	  RecordType tinyint(4) default '1' COMMENT '1:Attachment,2:PowerVoice',
	  DateInput datetime NOT NULL,
	  DateModified datetime default NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (FileID),
	  KEY LinkIndex (LinkToType,LinkToID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_PRESET",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_PRESET (
	  PresetID int(11) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  PresetType varchar(32) NOT NULL,
	  Title varchar(255) NOT NULL,
	  Contents text,
	  TargetType tinyint(1) default '1' COMMENT '1:All,2:Self,3:Custom',
	  DateInput datetime NOT NULL,
	  DateModified datetime default NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (PresetID),
	  KEY PresetType (PresetType),
	  KEY UserID (UserID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_ROUTES",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTES (
	  RouteID int(11) NOT NULL auto_increment,
	  DocumentID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  Note text,
	  EffectiveType varchar(32) default NULL,
	  EffectiveStartDate datetime default NULL,
	  EffectiveEndDate datetime default NULL,
	  ViewRight varchar(32) default NULL,
	  ReplySlipID int(11) default NULL,
	  RecordStatus tinyint(2) default '1',
	  DisplayOrder int(11) default NULL,
	  DateInput datetime NOT NULL,
	  DateModified datetime default NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (RouteID),
	  KEY DocumentID (DocumentID),
	  KEY UserID (UserID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_ROUTE_ACTION",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTE_ACTION (
	  ActionID int(11) NOT NULL auto_increment,
	  RouteID int(11) NOT NULL,
	  Action tinyint(4) NOT NULL COMMENT '1:SignConfirm,2:ReplySlip,3:Comment,4:Attachment',
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  PRIMARY KEY  (ActionID),
	  KEY RouteID (RouteID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_ROUTE_FEEDBACK",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTE_FEEDBACK (
	  FeedbackID int(11) NOT NULL auto_increment,
	  DocumentID int(11) NOT NULL,
	  RouteID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  Comment text NOT NULL,
	  DateInput datetime NOT NULL,
	  DateModified datetime default NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (FeedbackID),
	  KEY FeedbackIndex (DocumentID,RouteID,UserID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_ROUTE_TARGET",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTE_TARGET (
	  RouteID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  AccessRight varchar(32) NOT NULL,
	  RecordStatus tinyint(4) NOT NULL COMMENT '1:Not viewed yet, 2:Inprogress, 3:Completed',
	  DateInput datetime NOT NULL,
	  KEY TargetIndex (RouteID,UserID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_SETTING",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_SETTING (
	  SettingID int(11) NOT NULL auto_increment,
	  SettingName varchar(255) NOT NULL,
	  SettingValue text,
	  DateInput datetime default NULL,
	  InputBy int(11) default NULL,
	  DateModified datetime default NULL,
	  ModifiedBy int(11) default NULL,
	  PRIMARY KEY  (SettingID),
	  UNIQUE KEY SettingName (SettingName)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_STAR",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_STAR (
	  DocumentID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  DateInput datetime NOT NULL,
	  KEY StarIndex (UserID,DocumentID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Document Routing - Create Table INTRANET_DR_TAG",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_TAG (
	  DocumentID int(11) NOT NULL,
	  TagID int(11) NOT NULL,
	  DateInput datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  KEY TagIndex (DocumentID,TagID),
	  KEY TagID (TagID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Reply Slip - Create Table INTRANET_REPLY_SLIP",
	"CREATE TABLE IF NOT EXISTS INTRANET_REPLY_SLIP (
	  ReplySlipID int(11) NOT NULL auto_increment,
	  LinkToModule varchar(128) default NULL,
	  LinkToType varchar(128) default NULL,
	  LinkToID int(11) default NULL,
	  Title varchar(128) default NULL,
	  Description text,
	  ShowQuestionNum tinyint(1) NOT NULL,
	  AnsAllQuestion tinyint(1) NOT NULL,
	  RecordType tinyint(2) NOT NULL,
	  RecordStatus tinyint(2) NOT NULL,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (ReplySlipID),
	  KEY ModuleForm (LinkToModule,LinkToType,LinkToID),
	  KEY RecordType (RecordType)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Reply Slip - Create Table INTRANET_REPLY_SLIP_QUESTION",
	"CREATE TABLE IF NOT EXISTS INTRANET_REPLY_SLIP_QUESTION (
	  QuestionID int(11) NOT NULL auto_increment,
	  ReplySlipID int(11) NOT NULL,
	  RecordType tinyint(2) NOT NULL,
	  Content text NOT NULL,
	  DisplayOrder int(4) NOT NULL,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (QuestionID),
	  KEY ReplySlipID (ReplySlipID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Reply Slip - Create Table INTRANET_REPLY_SLIP_QUESTION_OPTION",
	"CREATE TABLE IF NOT EXISTS INTRANET_REPLY_SLIP_QUESTION_OPTION (
	  OptionID int(11) NOT NULL auto_increment,
	  QuestionID int(11) NOT NULL,
	  Content text NOT NULL,
	  DisplayOrder int(4) NOT NULL,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (OptionID),
	  KEY QuestionID (QuestionID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-22",
	"Reply Slip - Create Table INTRANET_REPLY_SLIP_USER_ANSWER",
	"CREATE TABLE IF NOT EXISTS INTRANET_REPLY_SLIP_USER_ANSWER (
	  UserAnswerID int(11) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  ReplySlipID int(11) NOT NULL,
	  QuestionID int(11) NOT NULL,
	  OptionID int(11) default NULL,
	  Content text,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (UserAnswerID),
	  KEY UserReplySlipQuestion (UserID,ReplySlipID,QuestionID),
	  KEY ReplySlipQuestion (ReplySlipID,QuestionID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-10-24",
	"ePost - Add Column DisplayAuthor to table EPOST_NEWSPAPER_PAGE_ARTICLE",
	"ALTER TABLE EPOST_NEWSPAPER_PAGE_ARTICLE ADD COLUMN DisplayAuthor tinyint(1) DEFAULT 1 AFTER POSITION"
);

$sql_LIBMS_update[] = array(
	"2012-10-25",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_NOTIFY_SETTING` (
	  `name` varchar(64) NOT NULL,
	  `enable` tinyint(1) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2012-10-31",
	"Document Routing - Add column PhysicalLocation to table INTRANET_DR_ENTRY",
	"ALTER TABLE INTRANET_DR_ENTRY ADD COLUMN PhysicalLocation varchar(255) DEFAULT NULL AFTER DocumentType"
);



$sql_LIBMS_update[] = array(
	"2012-11-03",
	"eLib + ",
	"alter table `LIBMS_NOTIFY_SETTING` add unique setting_name (name)"
);


$sql_LIBMS_update[] = array(
	"2012-11-03",
	"eLib + ",
	"INSERT INTO `LIBMS_NOTIFY_SETTING` (`name`, `enable`) VALUES
		('overdue_email', 1),
		('reserve_email', 1);"
);

$sql_eClassIP_update[] = array(
	"2012-11-09",
	"eDiscipline - add semester ratio type ",
	"alter table DISCIPLINE_SEMESTER_RATIO add RecordType char(1) default 'C'"
);


$sql_LIBMS_update[] = array(
	"2012-11-11",
	"eLib + ",
	"alter table LIBMS_CSV_TMP DROP PRIMARY KEY"
);


$sql_eClassIP_update[] = array(
	"2012-11-22",
	"Add column ExpiryDate in Table INTRANET_MODULE_LICENSE",
	"ALTER TABLE INTRANET_MODULE_LICENSE
		ADD COLUMN ExpiryDate datetime DEFAULT NULL AFTER NumberOfStudents"
);

$sql_eClassIP_update[] = array(
	"2012-12-04",
	"eInventory - Create Table INVENTORY_TAG",
	"CREATE TABLE IF NOT EXISTS INVENTORY_TAG (
	  RecordID int(11) NOT NULL,
	  TagID int(11) NOT NULL,
	  DateInput datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  KEY TagIndex (RecordID,TagID),
	  KEY TagID (TagID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

/*
$sql_eClassIP_update[] = array(
	"2012-12-07",
	"Add 2 column (api) for project client",
	"alter table INTRANET_USER add ApiDateInput date, add ApiInputBy int(8) default NULL"
);
*/

$sql_eClassIP_update[] = array(
	"2012-12-11",
	"ePost - Add Column SchoolLogo to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN SchoolLogo text AFTER Skin"
);

$sql_eClassIP_update[] = array(
	"2012-12-12",
	"Add DisplayQuestionNumber and Question for Activity Survey Form",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO 	ADD Question mediumtext DEFAULT NULL,
					      					ADD DisplayQuestionNumber tinyint(1) DEFAULT 0"
);


$sql_eClassIP_update[] = array(
	"2012-12-12",
	"Enrolment Survey Form - Answer Table",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_SURVEYANSWER  (
	  AnswerID 			int(11) 		NOT NULL 	auto_increment,
	  eEnrolRecordID	int(11) 		NOT NULL,
	  UserID 			int(11)  		NOT NULL,
	  UserName 			varchar(255) 	DEFAULT NULL,
	  Answer 			mediumtext  	DEFAULT NULL, 
	  RecordType		char(2)			DEFAULT NULL, 
	  DateInput 		datetime 		DEFAULT NULL,
 	  InputBy			int(8) 			DEFAULT NULL,
	  DateModified 		datetime 		DEFAULT NULL, 
	  ModifiedBy 		int(8) 			DEFAULT NULL,
	  PRIMARY KEY (AnswerID),
	  INDEX RecordIDKey (RecordType, eEnrolRecordID),
      INDEX UserID (UserID)		
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-12-12",
	"Add AllFieldsReq for Activity Survey Form",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD AllFieldsReq tinyint(1) DEFAULT 0"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column CoverBanner to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN CoverBanner text AFTER SchoolLogo"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column CoverBannerHideTitle to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN CoverBannerHideTitle tinyint(1) DEFAULT 0 AFTER CoverBanner"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column CoverBannerHideName to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN CoverBannerHideName tinyint(1) DEFAULT 0 AFTER CoverBannerHideTitle"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column InsideBanner to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN InsideBanner text AFTER CoverBannerHideName"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column InsideBannerHideTitle to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN InsideBannerHideTitle tinyint(1) DEFAULT 0 AFTER InsideBanner"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column InsideBannerHideName to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN InsideBannerHideName tinyint(1) DEFAULT 0 AFTER InsideBannerHideTitle"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column InsideBannerHidePageCat to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN InsideBannerHidePageCat tinyint(1) DEFAULT 0 AFTER InsideBannerHideName"
);

$sql_eClassIP_update[] = array(
	"2012-12-13",
	"ePost - Add Column OpenToPublic to table EPOST_NEWSPAPER",
	"ALTER TABLE EPOST_NEWSPAPER ADD COLUMN OpenToPublic tinyint(1) DEFAULT 0 AFTER IssueDate"
);

$sql_eClassIP_update[] = array(
	"2012-12-18",
	"eEnrol - Add table INTRANET_ENROL_QUOTA_SETTING to store the term-based quota settings",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_QUOTA_SETTING (
	    SettingsID  int(8) NOT NULL auto_increment,
		TermNumber int(8) NOT NULL default 0,
		CategoryID int(8) NOT NULL default 0,
		SettingName varchar(100) NOT NULL default '',
	    SettingValue int(8) NOT NULL default 0,
		RecordType varchar(64) default NULL,
	    DateInput datetime default NULL,
	    InputBy int(11) default NULL,
	    DateModified datetime default NULL,
	    ModifiedBy int(11) default NULL,
	    PRIMARY KEY (SettingsID),
		KEY TermCategoryID (TermNumber, CategoryID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2012-12-20",
	"ADD Reason in INTRANET_ENROL_EVENTSTUDENT For Customization",
	"ALTER TABLE INTRANET_ENROL_EVENTSTUDENT ADD COLUMN Reason varchar(255) default NULL"
);


$sql_eClassIP_update[] = array(
	"2012-12-28",
	"Enrolment Club Merit - Merit Record Table",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_MERIT_RECORD(
	  RecordID 			int(11) 		NOT NULL 	auto_increment,
	  EnrolGroupID		int(11) 		NOT NULL,
	  StudentID 		int(11)  		NOT NULL,
	  MeritType			int(11)  		DEFAULT NULL, 
	  MeritCount		float(8,2) 		DEFAULT NULL,
	  DateInput 		datetime 		DEFAULT NULL,
 	  InputBy			int(8) 			DEFAULT NULL,
	  DateModified 		datetime 		DEFAULT NULL, 
	  ModifiedBy 		int(8) 			DEFAULT NULL,
	  PRIMARY KEY (RecordID),
	  INDEX GroupIDKey (MeritType, EnrolGroupID),
      INDEX StudentID (StudentID)		
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-02",
	"eEnrol - Add column IntExt in table ENROLMENT_TO_OLE_SETTING",
	"ALTER TABLE ENROLMENT_TO_OLE_SETTING ADD COLUMN IntExt char(3) default 'INT' After TitleLang"
);

$sql_eClassIP_update[] = array(
	"2013-01-02",
	"Timetable - add RelatedRoomAllocationID to INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP",
	"ALTER TABLE INTRANET_TIMETABLE_ROOM_ALLOCATION_TEMP 	ADD RelatedRoomAllocationID int(11) default NULL After SubjectGroupID,
															ADD Index RelatedRoomAllocationID (RelatedRoomAllocationID)
	"
);

$sql_eClassIP_update[] = array(
	"2013-01-09",
	"iMail - reset INTRANET_CAMPUSMAIL_USED_STORAGE.RequestUpdate to 1 for all users to recalculate total used quota",
	"UPDATE INTRANET_CAMPUSMAIL_USED_STORAGE SET RequestUpdate='1'"
);

$sql_eClassIP_update[] = array(
	"2013-01-10",
	"Add index to MODULE_RECORD_DELETE_LOG",
	"ALTER TABLE MODULE_RECORD_DELETE_LOG ADD INDEX CompositeIndex(Module,LogDate,LogBy)"
);

$sql_eClassIP_update[] = array(
	"2013-01-17",
	"Reply Slip - Add Column ShowUserInfoInResult in table INTRANET_REPLY_SLIP",
	"ALTER TABLE INTRANET_REPLY_SLIP ADD COLUMN ShowUserInfoInResult tinyint(1) NOT NULL Default 2 AFTER AnsAllQuestion"
);

$sql_eClassIP_update[] = array(
	"2013-01-18",
	"eEnrol - Add Column OleCategoryID in table INTRANET_ENROL_CATEGORY",
	"ALTER TABLE INTRANET_ENROL_CATEGORY ADD COLUMN OleCategoryID int(11) NOT NULL Default 0"
);

$sql_eClassIP_update[] = array(
	"2013-01-18",
	"Account Mgmt - Add Index WebSAMSRegNo in table INTRANET_USER",
	"ALTER TABLE INTRANET_USER ADD INDEX WebSAMSRegNo (WebSAMSRegNo)"
);

$sql_eClassIP_update[] = array(
	"2013-01-23",
	"Create table for update log",
	"CREATE TABLE IF NOT EXISTS MODULE_RECORD_UPDATE_LOG
	(
		LogID int(11) NOT NULL auto_increment,
		Module varchar(20) NOT NULL,
		Section varchar(50) default NULL,
		RecordDetail text default NULL,
		UpdateTableName varchar(50) default NULL,
		UpdateRecordID int(11) default NULL,
		LogDate datetime default NULL,
		LogBy int(11) default NULL,
		PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-01-23",
	"iPortfolio - Add index ModifiedDate to table ASSESSMENT_STUDENT_SUBJECT_RECORD",
	"Alter Table ASSESSMENT_STUDENT_SUBJECT_RECORD Add Index ModifiedDate (ModifiedDate)"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-01-23",
	"iPortfolio - Add index ModifiedDate to table ASSESSMENT_STUDENT_MAIN_RECORD",
	"Alter Table ASSESSMENT_STUDENT_MAIN_RECORD Add Index ModifiedDate (ModifiedDate)"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-01-23",
	"iPortfolio - Add index ModifiedDate to table MERIT_STUDENT",
	"Alter Table MERIT_STUDENT Add Index ModifiedDate (ModifiedDate)"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-01-23",
	"iPortfolio - Add index ModifiedDate to table ACTIVITY_STUDENT",
	"Alter Table ACTIVITY_STUDENT Add Index ModifiedDate (ModifiedDate)"
);
# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-01-23",
	"iPortfolio - Add index CourseIdUserIdInputDate to table course_count",
	"Alter Table course_count Add Index CourseIdUserIdInputDate (course_id, user_id, inputdate)"
);
# end of eClass DB update

$sql_eClassIP_update[] = array(
	"2013-01-30",
	"eBooking - Remove Unique Key ItemCode from INTRANET_EBOOKING_ITEM as item is soft delete",
	"Alter Table INTRANET_EBOOKING_ITEM Drop Key ItemCode,
										Add Index ItemCode(ItemCode)"
);


$sql_eClassIP_update[] = array(
	"2013-01-30",
	"Digital Archive - Access Right Group Category ",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY (
		CategoryID int(11) NOT NULL auto_increment,
		CategoryTitle varchar(200) default NULL,
		CategoryCode varchar(32) default NULL,
		DisplayOrder int(2),
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (CategoryID),
		unique key CategoryCode (CategoryCode)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-30",
	"Digital Archive - Access Right Group Category ",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_ACCESS_RIGHT_GROUP_CATEGORY_JOIN (
		GroupID int(11),
		CategoryID int(11),
		DateInput datetime default NULL,
		InputBy int(11) default NULL,
		index GroupID (GroupID),
		index CategoryID (CategoryID),
		unique key ItemIDStudentID (CategoryID,GroupID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"eInventory - Add column Barcode to INVENTORY_ADMIN_GROUP",
	"ALTER TABLE INVENTORY_ADMIN_GROUP ADD COLUMN Barcode varchar(100) DEFAULT NULL AFTER NameEng"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"eInventory - Add index Barcode to INVENTORY_ADMIN_GROUP",
	"ALTER TABLE INVENTORY_ADMIN_GROUP ADD INDEX InventoryAdminGroupBarcode (Barcode)"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"eInventory - Add column Barcode to INVENTORY_FUNDING_SOURCE",
	"ALTER TABLE INVENTORY_FUNDING_SOURCE ADD COLUMN Barcode varchar(100) DEFAULT NULL AFTER NameEng"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"eInventory - Add index Barcode to INVENTORY_FUNDING_SOURCE",
	"ALTER TABLE INVENTORY_FUNDING_SOURCE ADD INDEX FundingBarcode(Barcode)"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"eInventory - Add column Barcode to INVENTORY_ITEM_BULK_EXT",
	"ALTER TABLE INVENTORY_ITEM_BULK_EXT ADD COLUMN Barcode varchar(100) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"eInventory - Add index Barcode to INVENTORY_ITEM_BULK_EXT",
	"ALTER TABLE INVENTORY_ITEM_BULK_EXT ADD INDEX BulkBarcode(Barcode)"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"SBA IES - Increase the size of IES_TASK.Introduction to LONGTEXT",
	"ALTER TABLE IES_TASK CHANGE COLUMN Introduction Introduction LONGTEXT"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"SBA IES - Increase the size of IES_TASK.Description to LONGTEXT",
	"ALTER TABLE IES_TASK CHANGE COLUMN Description Description LONGTEXT"
);

$sql_eClassIP_update[] = array(
	"2013-02-07",
	"SBA IES - Increase the size of IES_STEP.Description to LONGTEXT",
	"ALTER TABLE IES_STEP CHANGE COLUMN Description Description LONGTEXT"
);

// Document Routing - Phase 2 Schema [Begin]
$sql_eClassIP_update[] = array(
	"2013-01-08",
	"Document Routing - Add column VersionNumber to table INTRANET_DR_FILE",
	"ALTER TABLE INTRANET_DR_FILE ADD COLUMN VersionNumber int(11) DEFAULT 1 AFTER RecordType"
);

$sql_eClassIP_update[] = array(
	"2013-01-08",
	"Document Routing - Add column SizeInBytes to table INTRANET_DR_FILE",
	"ALTER TABLE INTRANET_DR_FILE ADD COLUMN SizeInBytes int(30) DEFAULT 0 AFTER RecordType"
);

$sql_eClassIP_update[] = array(
	"2013-01-08",
	"Document Routing - Add column EnableLock to table INTRANET_DR_ROUTES",
	"ALTER TABLE INTRANET_DR_ROUTES ADD EnableLock varchar(1) DEFAULT '0' AFTER ReplySlipID"
);

$sql_eClassIP_update[] = array(
	"2013-01-08",
	"Document Routing - Add column LockDuration to table INTRANET_DR_ROUTES",
	"ALTER TABLE INTRANET_DR_ROUTES ADD LockDuration int(11) COMMENT 'count in minutes' AFTER EnableLock"
);

$sql_eClassIP_update[] = array(
	"2013-01-08",
	"Document Routing - Add column ReplySlipReleaseType to table INTRANET_DR_ROUTES",
	"ALTER TABLE INTRANET_DR_ROUTES ADD ReplySlipReleaseType varchar(1) DEFAULT '1' COMMENT '1:Always; 2:Submitted; 3:Route completed' AFTER ReplySlipID"
);

$sql_eClassIP_update[] = array(
	"2013-01-08",
	"Document Routing - Create table INTRANET_DR_ROUTE_LOCK for locking route feedback comment",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_ROUTE_LOCK (
	  LockID int(11) NOT NULL auto_increment,
	  RouteID int(11) NOT NULL,
	  LastLockBy int(11),
	  LastLockTime datetime,
	  PRIMARY KEY (LockID),
	  KEY RouteID (RouteID),
	  KEY LastLockBy (LastLockBy),
	  KEY LastLockTime (LastLockTime)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-08",
	"Document Routing - Create table INTRANET_DR_COMMENT_DRAFT for saving route feedback comment",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_COMMENT_DRAFT (
	  DraftID int(11) NOT NULL auto_increment,
	  RouteID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  Comment text,
	  DateInput datetime,
	  DateModified datetime,
	  PRIMARY KEY (DraftID),
	  KEY RouteID(RouteID),
	  KEY UserID(UserID) 
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-23",
	"Table Reply Slip - Create table INTRANET_TABLE_REPLY_SLIP",
	"CREATE TABLE IF NOT EXISTS INTRANET_TABLE_REPLY_SLIP (
	  ReplySlipID int(11) NOT NULL auto_increment,
	  LinkToModule varchar(128) default NULL,
	  LinkToType varchar(128) default NULL,
	  LinkToID int(11) default NULL,
	  Title varchar(128) default NULL,
	  Description text,
	  AnsAllQuestion tinyint(1) NOT NULL,
	  ShowUserInfoInResult tinyint(1) NOT NULL,
	  RecordType tinyint(2) NOT NULL,
	  RecordStatus tinyint(2) NOT NULL,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (ReplySlipID),
	  KEY ModuleForm (LinkToModule,LinkToType,LinkToID),
	  KEY RecordType (RecordType)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-23",
	"Table Reply Slip - Create table INTRANET_TABLE_REPLY_SLIP_ITEM",
	"CREATE TABLE IF NOT EXISTS INTRANET_TABLE_REPLY_SLIP_ITEM (
	  ItemID int(11) NOT NULL auto_increment,
	  ReplySlipID int(11) NOT NULL,
	  RecordType varchar(2) NOT NULL,
	  Content text NOT NULL,
	  RowOrder int(4) NOT NULL,
	  ColumnOrder int(4) NOT NULL,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (ItemID),
	  KEY ReplySlipID (ReplySlipID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-23",
	"Table Reply Slip - Create table INTRANET_TABLE_REPLY_SLIP_OPTION",
	"CREATE TABLE IF NOT EXISTS INTRANET_TABLE_REPLY_SLIP_OPTION (
	  OptionID int(11) NOT NULL auto_increment,
	  ReplySlipID int(11) NOT NULL,
	  UniqueID varchar(100) NOT NULL,
	  RecordType varchar(3) NOT NULL,
	  Content text NOT NULL,
	  DisplayOrder int(4) NOT NULL,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (OptionID),
	  KEY ReplySlipID (ReplySlipID),
	  KEY UniqueID (UniqueID),
	  KEY RecordType (RecordType)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-23",
	"Table Reply Slip - Create table INTRANET_TABLE_REPLY_SLIP_USER_ANSWER",
	"CREATE TABLE IF NOT EXISTS INTRANET_TABLE_REPLY_SLIP_USER_ANSWER (
	  UserAnswerID int(11) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  ReplySlipID int(11) NOT NULL,
	  ItemID int(11) NOT NULL,
	  Content text,
	  InputDate datetime NOT NULL,
	  InputBy int(11) NOT NULL,
	  ModifiedDate datetime NOT NULL,
	  ModifiedBy int(11) NOT NULL,
	  PRIMARY KEY  (UserAnswerID),
	  KEY UserReplySlipItem (UserID,ReplySlipID,ItemID)
	) ENGINE=INNODB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-01-30",
	"Document Routing - Add column ReplySlipType to table INTRANET_DR_ROUTES",
	"ALTER TABLE INTRANET_DR_ROUTES ADD ReplySlipType varchar(1) DEFAULT '1' COMMENT '1:Question list; 2:Table' AFTER ReplySlipID"
);

$sql_eClassIP_update[] = array(
	"2013-01-30",
	"Document Routing - Add column ShowUserInfoInResult to table INTRANET_REPLY_SLIP",
	"ALTER TABLE INTRANET_REPLY_SLIP ADD ShowUserInfoInResult tinyint(1) NOT NULL DEFAULT '2' AFTER AnsAllQuestion"
);
// Document Routing - Phase 2 Schema [End]

$sql_eClassIP_update[] = array(
	"2013-03-11",
	"eInvnetory - carete field for store remvoe invoice record user",
	"alter table INVENTORY_ITEM_BULK_LOG add DateModifiedBy int(11)  default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-03-12",
	"eInventory invoice update history log",
	"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_BULK_INV_UPDATE_LOG (
		RecordID int(11) NOT NULL auto_increment,
		LogID int(11) NOT NULL,
		ItemID int(11) NOT NULL,
		Action int(11) NOT NULL,
		InvoiceNoFrom varchar(255) default NULL,
		InvoiceNoTo varchar(255) default NULL,
		PurchaseDateFrom date default NULL,
		PurchaseDateTo date default NULL,
		QtyChangeFrom int(11) default NULL,
		QtyChangeTo int(11) default NULL,
		PurchasedPriceFrom double(20,2) default NULL,
		PurchasedPriceTo double(20,2) default NULL,
		UnitPriceFrom double(20,2) default NULL,
		UnitPriceTo double(20,2) default NULL,
		DateInput datetime,
		InputBy int(11) NOT NULL,
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2013-03-19",
	"SMS - create field ScheduleDateTime in table INTRANET_SMS2_SOURCE_MESSAGE",
	"alter table INTRANET_SMS2_SOURCE_MESSAGE add ScheduleDateTime datetime default NULL"
);


$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_BOOK` add `BookCode_BAK` varchar(16) NOT NULL after BookCode"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_BOOK` change `BookCode` `BookCode` varchar(16) default NULL"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_BOOK_UNIQUE` add `Barcode_BAK` varchar(128) character set utf8 collate utf8_unicode_ci default NULL after BarCode"
);


$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_BOOK_UNIQUE` change `BarCode` `BarCode` varchar(128) character set utf8 collate utf8_unicode_ci default NULL"
);


$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_BOOK_UNIQUE` change `InvoiceNumber` `InvoiceNumber` varchar(64) character set utf8 collate utf8_unicode_ci default NULL,
	 	change `Distributor` `Distributor` varchar(64) character set utf8 collate utf8_unicode_ci default NULL,
  		change `Discount` `Discount` varchar(64) character set utf8 collate utf8_unicode_ci default NULL, 
  		change `PurchaseDate` `PurchaseDate` varchar(64) character set utf8 collate utf8_unicode_ci default NULL,
  		change `PurchaseNote` `PurchaseNote` varchar(64) character set utf8 collate utf8_unicode_ci default NULL "
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_GROUP_CIRCULATION` add `MaxFine` float NOT NULL default '-1' after OverDueCharge"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_NOTIFY_SETTING` add PRIMARY KEY  (`name`)"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_NOTIFY_SETTING` add  UNIQUE KEY `setting_name` (`name`) "
);


$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_NOTIFY_SETTING` add KEY `enable` (`enable`)"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_OVERDUE_LOG` add `DateCreated` timestamp NOT NULL default CURRENT_TIMESTAMP AFTER `LastModifiedBy`"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"eLib + ",
	"alter table `LIBMS_RESERVATION_LOG` add `RecordStatus` varchar(16) default NULL AFTER `ExpiryDate`"
);


$sql_LIBMS_update[] = array(
	"2013-03-21",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_SYSTEM_SETTING` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(64) NOT NULL,
  `type` enum('BOOL','INT','STRING') NOT NULL,
  `value` varchar(255) NOT NULL,
  `default` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_BOOK_CATEGORY_TMP` (
  `BookCategoryCode` varchar(8) NOT NULL,
  `DescriptionEn` varchar(64) default NULL,
  `DescriptionChi` varchar(64) default NULL,
  `DateModified` datetime default NULL,
  `LastModifiedBy` int(11) default NULL,
  UNIQUE KEY `BookCategoryCode` (`BookCategoryCode`),
  UNIQUE KEY `both_en_chi` (`DescriptionEn`,`DescriptionChi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_LOCATION_TMP` (
  `LocationCode` varchar(8) NOT NULL,
  `DescriptionEn` varchar(64) default NULL,
  `DescriptionChi` varchar(64) default NULL,
  `DateModified` datetime default NULL,
  `LastModifiedBy` int(11) default NULL,
  UNIQUE KEY `LocationCode` (`LocationCode`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-03-21",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_OPEN_TIME_SPECIAL_TMP` (
  `OpenTimeSpecialID` int(11) NOT NULL auto_increment,
  `DateFrom` date default NULL,
  `DateEnd` date default NULL,
  `OpenTime` time default NULL,
  `CloseTime` time default NULL,
  `Open` tinyint(1) default NULL,
  `DateModified` datetime default NULL,
  `LastModifiedBy` int(11) default NULL,
  PRIMARY KEY  (`OpenTimeSpecialID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;"
);



$sql_LIBMS_update[] = array(
	"2013-03-21",
	"LIBMS",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(1, 'max_overdue_limit', 'INT', '100', '-1'),
(2, 'override_password', 'STRING', '', ''),
(3, 'global_lockout', 'BOOL', '0', '0');"
);

# end of eClass DB update

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-03-22",
	"iPortfolio - Dynamic Report - store generated records for student",
	"CREATE TABLE `student_report_generated_record` (
	  `record_id` int(11) NOT NULL auto_increment,
	  `template_id` int(11) NOT NULL,
	  `student_id` int(11) NOT NULL,
	  `year_class_id` int(11) NOT NULL,
	  `report_title` varchar(255) NOT NULL,
	  `issue_date` date NOT NULL,
	  `slp_record_type` varchar(3) NOT NULL default 'all' COMMENT 'all | sel',
	  `display_full_mark` varchar(1) NOT NULL default '1',
	  `record_status` tinyint(3) NOT NULL COMMENT '1:released, 0:unreleased',
	  `generate_settings` text,
	  `inputdate` datetime NOT NULL,
	  `inputby` int(11) NOT NULL,
	  `modifieddate` datetime NOT NULL,
	  `modifiedby` int(11) NOT NULL,
	  PRIMARY KEY  (`record_id`),
	  KEY `template_student` (`template_id`,`student_id`),
	  KEY `student_id` (`student_id`),
	  KEY `report_title` (`report_title`), 
	  KEY `record_status` (`record_status`),
	  KEY `inputdate` (`inputdate`),
	  KEY `inputby` (`inputby`),
	  KEY `modifieddate` (`modifieddate`),
	  KEY `modifiedby` (`modifiedby`) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClass_update[] = array(
	"2013-03-22",
	"iPortfolio - create table student_report_generated_record_academic_year",
	"CREATE TABLE `student_report_generated_record_academic_year`(
		`record_id` int(11) NOT NULL,
		`academic_year_id` int(11) NOT NULL,
		KEY `mapping_id` (`record_id`,`academic_year_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2013-03-26",
"eEnrolment - Achievement Comment Bank",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_COMMENT_BANK
(
	CommentID		int(11)		auto_increment,
	CommentCode		varchar(16)	default NULL,
	Comment    		text		default NULL,
	RecordStatus	int(1)		default 1,
	DateInput		datetime	default NULL,
	DateModified	datetime	default NULL,
	PRIMARY KEY (CommentID),
	INDEX RecordStatusIndex(RecordStatus) 	
)ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2013-03-26",
"Subject eResources - Group",
"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP (
	GroupID			int(11)		auto_increment,
	Code			varchar(50)	NOT NULL,	
	GroupTitle		varchar(20)	default NULL,
	Description		text,
	SubjectID		int(11)		default NULL,		
	DateInput		datetime	default NULL,
	InputBy 		int(11) 	default NULL,
	DateModified	datetime	default NULL,
	ModifiedBy		int(11)		default NULL,
	PRIMARY KEY (GroupID),
	INDEX Code (Code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2013-03-26",
"Subject eResources - Group Member",
"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_MEMBER (
	GroupID		int(11) 	NOT NULL,
	UserID		int(11)		NOT NULL,
	IsAdmin		int(11)		NOT NULL,
	DateInput	datetime	default NULL,
	InputBy		int(11)		default NULL,
	PRIMARY KEY (GroupID, UserID),
	INDEX IsAdmin (IsAdmin),
	INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2013-03-26",
"Subject eResources - Classlevel",
"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP_CLASSLEVEL (
	GroupID			int(11) 	NOT NULL,
	ClassLevelID	int(11) 	NOT NULL,
	DateInput		datetime 	default NULL,
	InputBy			int(11)		default NULL,
	PRIMARY KEY (GroupID, ClassLevelID),
	INDEX ClassLevelID (ClassLevelID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2013-03-28",
	"Amend Group Title - change varchar to 200",
	"ALTER table DIGITAL_ARCHIVE_SUBJECT_RESOURCE_ACCESS_RIGHT_GROUP MODIFY GroupTitle varchar(200) default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-04-09",
	"iMail plus - Create temp table MAIL_TEMP_UPLOAD_FOLDER to store folders for plupload",
	"CREATE TABLE IF NOT EXISTS MAIL_TEMP_UPLOAD_FOLDER(
	  FolderID int(11) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  ComposeTime int(11) NOT NULL,
	  PRIMARY KEY(FolderID),
	  INDEX CompositeKey(UserID,ComposeTime)  
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-04-16",
	"Student Attendance - Add column DayType to CARD_STUDENT_DAILY_REMARK",
	"ALTER TABLE CARD_STUDENT_DAILY_REMARK ADD COLUMN DayType int(2) AFTER RecordDate"
);

// UNIQUE KEY and INDEX are modified at student_attendance_daily_remark_add_daytype.php after data patched because NULL value cannot used in unique key
$sql_eClassIP_update[] = array(
	"2013-04-16",
	"Student Attendance - Add column DayType to CARD_STUDENT_DAILY_REMARK",
	"ALTER TABLE CARD_STUDENT_DAILY_REMARK ADD COLUMN DayType int(2) AFTER RecordDate"
);
$sql_eClassIP_update[] = array(
	"2013-04-25",
	"SBA_LS - Teacher can attach files in Task Introduction",
	"ALTER TABLE IES_TASK ADD COLUMN Attachments varchar(255) AFTER Description"
);
$sql_eClassIP_update[] = array(
	"2013-04-25",
	"SBA_LS - Teacher can attach files in Steps",
	"ALTER TABLE IES_STEP ADD COLUMN Attachments varchar(255) AFTER Question"
);


$sql_eClassIP_update[] = array(
	"2013-04-29",
	"eDiscipline - change conduct mark support with decimal ",
	"alter table DISCIPLINE_MERIT_ITEM change ConductScore ConductScore float(8,1)"
);

$sql_eClassIP_update[] = array(
	"2013-04-29",
	"eDiscipline - change conduct mark support with decimal ",
	"alter table DISCIPLINE_STUDENT_CONDUCT_BALANCE change ConductScore ConductScore float(8,1)"
);

$sql_eClassIP_update[] = array(
	"2013-04-29",
	"eDiscipline - change conduct mark support with decimal ",
	"alter table DISCIPLINE_MERIT_RECORD change ConductScoreChange ConductScoreChange float(8,1), change ConductScoreAfter ConductScoreAfter float(8,1)"
);

$sql_eClassIP_update[] = array(
	"2013-04-29",
	"eDiscipline - change conduct mark support with decimal ",
	"alter table DISCIPLINE_ACCU_CATEGORY_UPGRADE_RULE change ConductScore ConductScore float(8,1)"
);

$sql_eClassIP_update[] = array(
	"2013-04-29",
	"eDiscipline - change conduct mark support with decimal ",
	"alter table DISCIPLINE_ACCU_CATEGORY_PERIOD_SETTING change UpdatedDeductConductScore UpdatedDeductConductScore float(8,1)"
);

$sql_eClassIP_update[] = array(
	"2013-04-29",
	"eDiscipline - change conduct mark support with decimal ",
	"alter table DISCIPLINE_CONDUCT_ADJUSTMENT change AdjustMark AdjustMark float(8,1)"
);

$sql_eClassIP_update[] = array(
	"2013-04-29",
	"eDiscipline - change conduct mark support with decimal ",
	"alter table DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG change FromScore FromScore float(8,1), change ToScore ToScore float(8,1)"
);

$sql_eClassIP_update[] = array(
	"2013-05-07",
	"eEnrolment - Add fields for customization",
	"Alter Table INTRANET_ENROL_EVENTINFO 
	 Add Location 		varchar(200) 	default NULL,
	 Add Transport 		varchar(255) 	default NULL,
	 Add RewardOrCert	TEXT 			default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-05-07",
	"eEnrolment - Add Organizer fields for customization",
	"Alter Table INTRANET_ENROL_EVENTINFO 
	 Add Organizer varchar(200) default NULL after Location"
);


$sql_LIBMS_update[] = array(
	"2013-05-08",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_STOCKTAKE_SCHEME` (
	  `StocktakeSchemeID` int(11) NOT NULL auto_increment,
	  `SchemeTitle` varchar(64) default NULL,
	  `Description` text default NULL,
	  `StartDate` date default NULL,
	  `EndDate` date default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (StocktakeSchemeID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-05-08",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_STOCKTAKE_LOG` (
	  `StocktakeLogID` int(11) NOT NULL auto_increment,
	  `StocktakeSchemeID` int(11) NOT NULL,
	  `BookID` int(11) NOT NULL,
	  `BookUniqueID` int(11) NOT NULL,
	  `Result` varchar(64) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (StocktakeLogID),
	  index BookUniqueID (BookUniqueID),
  		UNIQUE KEY `StocktakeLog` (`StocktakeSchemeID`, `BookID`, `BookUniqueID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);



$sql_eClassIP_update[] = array(
	"2013-05-11",
	"Create table INTRANET_APP_NOTIFY_MESSAGE_TARGET for ASL Parent App",
	"alter table INTRANET_APP_NOTIFY_MESSAGE_TARGET charset=utf8;"
);


$sql_eClassIP_update[] = array(
	"2013-05-11",
	"Create table INTRANET_APP_NOTIFY_MESSAGE_TARGET for ASL Parent App",
	"alter table INTRANET_APP_NOTIFY_MESSAGE_TARGET ENGINE=InnoDB;"
);


$sql_eClassIP_update[] = array(
	"2013-05-11",
	"Create table INTRANET_APP_NOTIFY_MESSAGE_TARGET for ASL Parent App",
	"ALTER TABLE INTRANET_APP_NOTIFY_MESSAGE_TARGET ADD MessageTitle varchar(255),
	  ADD MessageContent text"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry (student) add extra fields",
	"
		alter table STUDENT_REGISTRY_STUDENT 
		add ETHNICITY_CODE varchar(3) default NULL,
		add CHURCH varchar(255) default NULL,
		add ADDRESS_ROOM_EN varchar(5) default NULL,
		add ADDRESS_FLOOR_EN varchar(5) default NULL,
		add ADDRESS_BLK_EN varchar(5) default NULL,
		add ADDRESS_BLD_EN varchar(255) default NULL,
		add ADDRESS_EST_EN varchar(255) default NULL,
		add ADDRESS_STR_EN varchar(255) default NULL,
		add ADDRESS_DISTRICT_EN varchar(255) default NULL,
		add ADDRESS_AREA tinyint(1) default NULL,
		add ADDRESS_ROOM_CH varchar(5) default NULL,
		add ADDRESS_FLOOR_CH varchar(5) default NULL,
		add ADDRESS_BLK_CH varchar(5) default NULL,
		add ADDRESS_BLD_CH varchar(255) default NULL,
		add ADDRESS_EST_CH varchar(255) default NULL,
		add ADDRESS_STR_CH varchar(255) default NULL,
		add ADDRESS_DISTRICT_CH varchar(255) default NULL,
		add LAST_SCHOOL_EN varchar(255) default NULL,
		add LAST_SCHOOL_CH varchar(255) default NULL,
		add CHN_COMMERCIAL_CODE1 varchar(4) default NULL,
		add CHN_COMMERCIAL_CODE2 varchar(4) default NULL,
		add CHN_COMMERCIAL_CODE3 varchar(4) default NULL,
		add CHN_COMMERCIAL_CODE4 varchar(4) default NULL,
		add DISTRICT_CODE varchar(2) default NULL,
		add NATION_CODE varchar(3) default NULL
	"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry (parent / guardian) add extra fields",
	"
		alter table STUDENT_REGISTRY_PG 
		add SEQUENCE tinyint(1) default 1,
		add HKID varchar(50) default NULL,
		add RELIGION tinyint(1) default NULL,
		add RELIGION_OTHERS varchar(255) default NULL,
		add CHURCH varchar(255) default NULL,
		add PG_TYPE_OTHERS varchar(255) default NULL,
		add ADDRESS_ROOM_EN varchar(5) default NULL,
		add ADDRESS_FLOOR_EN varchar(5) default NULL,
		add ADDRESS_BLK_EN varchar(5) default NULL,
		add ADDRESS_BLD_EN varchar(255) default NULL,
		add ADDRESS_EST_EN varchar(255) default NULL,
		add ADDRESS_STR_EN varchar(255) default NULL,
		add ADDRESS_DISTRICT_EN varchar(255) default NULL,
		add ADDRESS_AREA tinyint(1) default NULL,
		add DISTRICT_CODE varchar(2) default NULL
	"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry (student) change DateModified field format",
	"alter table STUDENT_REGISTRY_STUDENT change DateModified DateModified datetime"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry (parent / guardian) change DateModified field format",
	"alter table STUDENT_REGISTRY_PG change DateModified DateModified datetime"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry - Contact Person in case of emergency", 
	"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_EMERGENCY_CONTACT (
		RecordID int(11) NOT NULL auto_increment,
		StudentID int(11),
		Contact1 tinyint(1),
		Contact1_Others varchar(255),
		Contact2 tinyint(1),
		Contact2_Others varchar(255),
		Contact3_Title tinyint(1),
		Contact3_EnglishName varchar(255),
		Contact3_ChineseName varchar(255),
		Contact3_Relationship varchar(255),
		Contact3_Phone varchar(255),
		Contact3_Mobile varchar(255),
		DateInput datetime,
		InputBy int(11),
		DateModified datetime,
		ModifyBy int(11),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry - LastClassInfo", 
	"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_LASTCLASSINFO (
		RecordID int(11) NOT NULL auto_increment,
		StudentID int(11),
		YearID int(11),
		ClassName varchar(255),
		ClassNumber varchar(255),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry - BroSis", 
	"CREATE TABLE IF NOT EXISTS STUDENT_REGISTRY_BROSIS (
		RecordID int(11) NOT NULL auto_increment,
		StudentID int(11),
		BS_UserLogin varchar(255),
		Relationship varchar(255),
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"INTRANET_USER add extra fields",
	"	alter table INTRANET_USER 
		add CFirstName varchar(100) default NULL,
		add CLastName varchar(100) default NULL
	"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"STUDENT_REGISTRY_STUDENT change FAMILY_LANG type",
	"alter table STUDENT_REGISTRY_STUDENT  change FAMILY_LANG FAMILY_LANG varchar(3)"
);

$sql_eClassIP_update[] = array(
	"2013-05-14",
	"Student Registry - PRESET_CODE_OPTION", 
	"CREATE TABLE IF NOT EXISTS PRESET_CODE_OPTION (
		CodeID int(11) NOT NULL auto_increment,
		CodeType varchar(20),
		Code varchar(3),
		NameEng varchar(255),
		NameChi varchar(255),
		DisplayOrder int(11),
		RecordType int(11),
		RecordStatus int(11),
		DateInput datetime,
		InputBy int(11),
		InputMethod int(11),
		DateModified datetime,
		ModifiedBy int(11),
		ModifiedMethod int(11),
		PRIMARY KEY (CodeID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_LIBMS_update[] = array(
	"2013-05-16",
	"LIBMS",
	"CREATE TABLE IF NOT EXISTS `LIBMS_WRITEOFF_LOG` (
	  `WriteOffID` int(11) NOT NULL auto_increment,
	  `StocktakeSchemeID` int(11) NOT NULL,
	  `BookID` int(11) NOT NULL,
	  `BookUniqueID` int(11) NOT NULL,
	  `Result` varchar(64) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (WriteOffID),
	  index BookUniqueID (BookUniqueID),
  		UNIQUE KEY `StocktakeLog` (`StocktakeSchemeID`, `BookID`, `BookUniqueID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-05-20",
	"LIBMS",
	"ALTER TABLE `LIBMS_WRITEOFF_LOG` add `PreviousBookStatus` varchar(64) default NULL after `Result` "
);

$sql_eClassIP_update[] = array(
	"2013-05-24",
	"eInventory - update PurchasedPrice data type ",
	"alter table INVENTORY_ITEM_SINGLE_EXT modify PurchasedPrice double(20,2)"
);

$sql_eClassIP_update[] = array(
	"2013-05-28",
	"eDiscipline - add record input date (yy3 cust)",
	"alter table DISCIPLINE_MERIT_RECORD add RecordInputDate date"
);

$sql_eClassIP_update[] = array(
	"2013-05-31",
	"School announcement - fix the problem failed to display the title [Case#2013-0524-0948-35156]",
	"alter table INTRANET_ANNOUNCEMENT change Title  Title varchar(300)"
);


$sql_LIBMS_update[] = array(
	"2013-06-02",
	"eLib + ",
	"ALTER TABLE `LIBMS_BOOK` CHANGE `Introduction` `Introduction` TEXT DEFAULT NULL;"
);

// Change Introduction to longer text
$sql_LIBMS_update[] = array(
	"2013-06-02",
	"eLib + ",
	"ALTER TABLE `LIBMS_CSV_TMP` CHANGE `RemarkInternal` `Introduction` TEXT  collate utf8_unicode_ci DEFAULT NULL;"
);


$sql_eClassIP_update[] = array(
	"2013-06-13",
	"eDiscipline - (YY3 Customization) Add column MaxGainDeductMark in table DISCIPLINE_MERIT_ITEM",
	"alter table DISCIPLINE_MERIT_ITEM add column MaxGainDeductMark float(8,1) default 0 not null after SubScore1;"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"eDiscipline - (YY3 Customization) Add column MaxGainDeductMark in table DISCIPLINE_ITEM_TAG_SETTING",
	"alter table DISCIPLINE_ITEM_TAG_SETTING add column MaxGainMark float(8,1) default 0 not null after TagName;"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"eDiscipline - (YY3 Customization) Add column MaxGainDeductMark in table DISCIPLINE_ITEM_TAG_SETTING",
	"alter table DISCIPLINE_ITEM_TAG_SETTING add column MaxDeductMark float(8,1) default 0 not null after TagName;"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"eDiscipline - (YY3 Customization) create table DISCIPLINE_SEMESTER_MAX_MIN_CONDUCT_MARK",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_SEMESTER_MAX_MIN_CONDUCT_MARK (
		RecordID int(8) NOT NULL auto_increment,
		AcademicYearID int(8) default NULL,
		YearTermID int(8) default NULL,
		MaxConductMark float(8,1) default NULL,
		MinConductMark float(8,1) default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		PRIMARY KEY (RecordID),
		UNIQUE KEY UniqKey(AcademicYearID,YearTermID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"eDiscipline - (YY3 Customization) add column OverflowConductMark,OverflowMeritItemScore,OverflowTagScore to DISCIPLINE_MERIT_RECORD ",
	"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD COLUMN OverflowConductMark varchar(1) DEFAULT NULL, ADD COLUMN OverflowMeritItemScore varchar(1) DEFAULT NULL, ADD COLUMN OverflowTagScore varchar(1) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"eDiscipline - (YY3 Customization) create table DISCIPLINE_CONDUCT_DOWN_GRADE_CONDITION",
	"CREATE TABLE IF NOT EXISTS DISCIPLINE_CONDUCT_DOWN_GRADE_CONDITION (
		ItemID int(11) NOT NULL,
		Quantity int(11) NOT NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		InputBy int(11) default NULL,
		ModifyBy int(11) default NULL, 
		PRIMARY KEY (ItemID)  
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"eDiscipline - (YY3 Customization) add column AdjustedGradeBefore to DISCIPLINE_STUDENT_CONDUCT_BALANCE",
	"ALTER TABLE DISCIPLINE_STUDENT_CONDUCT_BALANCE ADD COLUMN AdjustedGradeBefore varchar(100) DEFAULT NULL COMMENT 'For YY3 Cust' DEFAULT NULL AFTER AdjustedGradeChar"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"eDiscipline - (YY3 Customization) add column ConductScoreFakeChange to DISCIPLINE_MERIT_RECORD",
	"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD COLUMN ConductScoreFakeChange float(8,2) DEFAULT NULL COMMENT 'For YY3 Cust' AFTER ConductScoreAfter"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"ePost - Add Column CoursewareRecordID to table EPOST_WRITING",
	"Alter Table EPOST_WRITING Add Column CoursewareRecordID INT(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-06-18",
	"ePost - Add Column CoursewareRecordID to table EPOST_ARTICLE_SHELF",
	"Alter Table EPOST_ARTICLE_SHELF Add Column CoursewareRecordID INT(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-06-19",
	"SBA_LS - Add Chart Preference in Survey Table",
	"ALTER TABLE IES_SURVEY ADD COLUMN ChartType varchar(255)"
);



$sql_LIBMS_update[] = array(
	"2013-06-24",
	"LIBMS",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(4, 'system_open_date', 'STRING', '', ''),
(5, 'system_end_date', 'STRING', '', '');"
);



$sql_LIBMS_update[] = array(
	"2013-06-25",
	"LIBMS settings for handling overdue return in batch return function",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(6, 'batch_return_handle_overdue', 'BOOL', '1', '1');"
);

$sql_eClassIP_update[] = array(
	"2013-07-05",
	"Change the data field size",
	"ALTER TABLE INTRANET_CIRCULAR CHANGE Description Description mediumtext"
);

$sql_eClassIP_update[] = array(
	"2013-07-05",
	"KIS - Intranet Album table",
	"CREATE TABLE IF NOT EXISTS INTRANET_ALBUM (
	    AlbumID int(8) NOT NULL auto_increment,
	    Title varchar(255) default NULL,
	    Description varchar(255) default NULL,
	    CoverPhotoID int(8) default NULL, /* Cover AlbumPhoto ID */
	    PlaceTaken varchar(255) default NULL,
	    DateTaken datetime default NULL,
	    SharedSince datetime default NULL,
	    SharedUntil datetime default NULL,
	    DateInput datetime default NULL,
	    DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	    InputBy int(8) default NULL,
	    ModifyBy int(8) default NULL,
	    PRIMARY KEY (AlbumID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-07-05",
	"KIS - Intranet Album Photos table",
	"CREATE TABLE IF NOT EXISTS INTRANET_ALBUM_PHOTO (
	    AlbumPhotoID int(8) NOT NULL auto_increment,
	    AlbumID int(8) default NULL,
	    Title varchar(255) default NULL, /* Original File Name */
	    Description varchar(255) default NULL,
	    Sequence int(5) default '-1', /* Order of Photos in Album */
	    Size int(11) default NULL,
	    DateTaken datetime default NULL,
	    DateInput datetime default NULL,
	    DateModified timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	    InputBy int(8) default NULL,
	    ModifyBy int(8) default NULL,
	    PRIMARY KEY (AlbumPhotoID)
       ) ENGINE=InnoDB  DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-07-05",
	"KIS - Intranet Album User permission table",
	"CREATE TABLE IF NOT EXISTS INTRANET_ALBUM_USER (
	    AlbumUserID int(8)  NOT NULL auto_increment,
	    AlbumID int(8) default NULL,
	    RecordID int(8) default NULL,
	    RecordType enum('all','user','group') default NULL,
	    DateInput datetime default NULL,
	    InputBy int(8) default NULL,
	    PRIMARY KEY (AlbumUserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-07-08",
	"Digital Archive - Create table DIGITAL_ARCHIVE_TEMP_UPLOAD_FOLDER to store temp folders for plupload",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_TEMP_UPLOAD_FOLDER(
	  FolderID int(11) NOT NULL auto_increment,
	  UserID int(11) NOT NULL,
	  Folder varchar(255) NOT NULL,
	  InputDate datetime NOT NULL,
	  PRIMARY KEY(FolderID),
	  INDEX CompositeKey(UserID,Folder)  
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-07-10",
	"Digital Archive -> File Format Settings",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_FILE_FORMAT (
		RecordID int(11) NOT NULL auto_increment,
		FileFormat varchar(255) NOT NULL,
		InputBy int(11) default NULL,
		DateInput datetime default NULL,
		PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-07-22",
	"School Settings > Timetable - Add field ReleaseStatus to store if the timetable can be viewed by teachers",
	"ALTER TABLE INTRANET_SCHOOL_TIMETABLE ADD COLUMN ReleaseStatus tinyint(2) DEFAULT 1 AFTER RecordStatus"
);

//2013-07-25 -- eBook License - Charles MA
$sql_eClassIP_update[] = array(
	"2013-07-25",
	"eBook License installation table ",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_INSTALLATION (
     BookID int(11) NOT NULL default '0',
     QuotationID int(11) NOT NULL default '0',
     IsCompulsory varchar(1) default NULL,
     IsEnable varchar(1) default NULL,
     InputDate datetime default NULL,
     PRIMARY KEY (BookID,QuotationID),
     KEY BookID (BookID),
     KEY QuotationID (QuotationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

//2013-07-25 -- eBook License - Charles MA
$sql_eClassIP_update[] = array(
	"2013-07-25",
	"eBook License quotation table ",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_QUOTATION (
	     QuotationID int(11) NOT NULL auto_increment,
	     QuotationNumber varchar(50) default NULL,
	     PeriodFrom datetime default NULL,
	     PeriodTo datetime default NULL,
	     InputDate datetime default NULL,
	     Type varchar(1) default '1',
	     IsActivated varchar(1) default NULL,
	     QuotationBookCount int(11) default NULL,
	     PRIMARY KEY (QuotationID),
	     KEY QuotationNumber (QuotationNumber)
	) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-07-26",
	"eInventory - Create write-off record reject date / by",
	"ALTER TABLE INVENTORY_ITEM_WRITE_OFF_RECORD add RejectDate date after ApprovePerson, add RejectPerson int(11) after RejectDate"
);

// 2013-08-01 [Cameron] add ApplyUserType for SIS
$sql_eClassIP_update[] = array(
	"2013-08-01",
	"eEnrolment - Club",
	"ALTER TABLE `INTRANET_ENROL_GROUPINFO` ADD `ApplyUserType` char(2) default NULL AFTER `RecordStatus`"
);

$sql_eClassIP_update[] = array(
	"2013-08-06",
	"eSport - Enrolment Restriction List ",
	"CREATE TABLE IF NOT EXISTS SPORTS_ENROL_RESTRICTION_LIST (
	     UserID int(11) NOT NULL,
	     Reason varchar(255) default NULL,
	     DateInput datetime default NULL,
	     InputBy int(11) default NULL,
	     DateModified datetime default NULL,
	     ModifyBy int(11) default NULL,
	     PRIMARY KEY (UserID)
	) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8"
);

$sql_LIBMS_update[] = array(
	"2013-08-07",
	"LIBMS Item Management for adding new info (ACNO)",
	"ALTER TABLE `LIBMS_BOOK_UNIQUE` 
		ADD COLUMN `ACNO` varchar(16) default NULL AFTER `UniqueID`, 
		ADD COLUMN `ACNO_BAK` varchar(16) NOT NULL AFTER `ACNO`,
		ADD COLUMN `ACNO_Prefix` varchar(16) NOT NULL AFTER `ACNO_BAK`,
		ADD COLUMN `ACNO_Num` int(10) unsigned zerofill NOT NULL AFTER `ACNO_Prefix`,
		ADD COLUMN `PurchaseByDepartment` varchar(32) DEFAULT NULL AFTER `PurchaseNote`,
		ADD COLUMN `ListPrice` decimal(10,2) DEFAULT NULL AFTER `PurchaseByDepartment`, 
		ADD UNIQUE KEY ACNO (ACNO),
     	ADD INDEX ACNO_Prefix (ACNO_Prefix, ACNO_Num);"
);

// 2013-08-09 [Cameron] add CategoryTypeID for SIS
// 1 - Club (CCA)
// 2 - Activity (Summer Program)
$sql_eClassIP_update[] = array(
	"2013-08-09",
	"eEnrolment - Settings - Category",
	"ALTER TABLE `INTRANET_ENROL_CATEGORY` ADD `CategoryTypeID` int(2) default 0 AFTER `OleCategoryID`"
);

$sql_LIBMS_update[] = array(
	"2013-08-12",
	"LIBMS Item Management for adding new info (LocationCode)",
	"ALTER TABLE `LIBMS_BOOK_UNIQUE` ADD COLUMN `LocationCode` varchar(8) default NULL AFTER `ListPrice` ;"
);


$sql_eClassIP_update[] = array(
	"2013-08-16",
	"Student Registry (student) add extra fields",
	"
		alter table STUDENT_REGISTRY_STUDENT 
		add DATA_CONFIRM_DATE datetime default NULL,
		add DATA_CONFIRM_BY int(11) default NULL
	"
);

$sql_LIBMS_update[] = array(
	"2013-08-21",
	"LIBMS - enlarge the storage of RemarkInternal in table LIBMS_BOOK",
	"ALTER TABLE LIBMS_BOOK MODIFY RemarkInternal varchar(128) default NULL COMMENT 'remark display in admin pages only'"
);

$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Import Class Student add two fields",
	"
		alter table TEMP_CLASS_STUDENT_IMPORT 
		add Column TargetClassNameEn varchar(100) default NULL AFTER ClassNumber,
		add Column TargetClassNumber varchar(100) default NULL AFTER TargetClassNameEn
	"
);

$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Add  FirstPublished to INTRANET_ELIB_BOOK",
	"ALTER table INTRANET_ELIB_BOOK add  FirstPublished varchar(50) default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Add  ePublisher to INTRANET_ELIB_BOOK",
	"ALTER table INTRANET_ELIB_BOOK add  ePublisher varchar(100) default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Add  CopyrightYear to INTRANET_ELIB_BOOK",
	"ALTER table INTRANET_ELIB_BOOK add  CopyrightYear varchar(100) default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Add  CopyrightStatement to INTRANET_ELIB_BOOK",
	"ALTER table INTRANET_ELIB_BOOK add  CopyrightStatement varchar(100) default NULL"
);


$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Add  ForceToOnePageMode  to INTRANET_EBOOK_BOOK ",
	"ALTER table  INTRANET_EBOOK_BOOK add ForceToOnePageMode varchar(1) default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Add  OnePageModeWidth to INTRANET_EBOOK_BOOK ",
	"ALTER table  INTRANET_EBOOK_BOOK add OnePageModeWidth int(11) default NULL"
);


$sql_eClassIP_update[] = array(
	"2013-08-23",
	"Add  OnePageModeHeight  to INTRANET_EBOOK_BOOK",
	"ALTER table  INTRANET_EBOOK_BOOK add OnePageModeHeight int(11) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2013-08-23",
	" eLibrary - add field ACNOList in INTRANET_ELIB_BOOK to store the ACNO of all copies ",
	"alter table INTRANET_ELIB_BOOK add `ACNOList` text default NULL"
);

$sql_eClassIP_update[] = array
(
	"2013-09-03",
	" eLibrary - add field CallNumber in INTRANET_ELIB_BOOK to store book call number ",
	"ALTER TABLE INTRANET_ELIB_BOOK ADD COLUMN CallNumber VARCHAR(128) DEFAULT NULL"
);

$sql_LIBMS_update[] = array(
	"2013-08-26",
	"LIBMS - change the property of ListPrice to varchar in table LIBMS_BOOK",
	"alter table LIBMS_BOOK_UNIQUE change column ListPrice ListPrice varchar(64) default null"
);

$sql_LIBMS_update[] = array(
	"2013-08-26",
	"LIBMS - change the property of PurchasePrice to varchar in table LIBMS_BOOK",
	"alter table LIBMS_BOOK_UNIQUE change column PurchasePrice PurchasePrice varchar(64) default null"
);

$sql_LIBMS_update[] = array(
	"2013-08-28",
	"LIBMS",
	"alter table LIBMS_GROUP add GroupType varchar(2) default NULL after GroupCode"
);

$sql_LIBMS_update[] = array(
	"2013-08-30",
	"LIBMS",
	"CREATE TABLE LIBMS_CSV_TMP_2 (
     id int(15) NOT NULL auto_increment,
     BookTitle varchar(255) NOT NULL default '',
     BookSubTitle varchar(255) default NULL,
     CallNum varchar(255) default NULL,
     CallNum2 varchar(255) default NULL,
     ISBN varchar(255) default NULL,
     ISBN2 varchar(255) default NULL,
     Language varchar(255) default NULL,
     Country varchar(255) default NULL,
     Introduction text,
     Edition varchar(255) default NULL,
     PublishYear varchar(255) default NULL,
     Publisher varchar(255) default NULL,
     PublishPlace varchar(255) default NULL,
     Series varchar(255) default NULL,
     SeriesNum varchar(255) default NULL,
     ResponsibilityCode1 varchar(255) default NULL,
     ResponsibilityBy1 varchar(255) default NULL,
     ResponsibilityCode2 varchar(255) default NULL,
     ResponsibilityBy2 varchar(255) default NULL,
     ResponsibilityCode3 varchar(255) default NULL,
     ResponsibilityBy3 varchar(255) default NULL,
     NoOfPage varchar(255) default NULL,
     BookCategory varchar(255) default NULL,
     BookCirclation varchar(255) default NULL,
     BookResources varchar(255) default NULL,
     Subject varchar(255) default NULL,
     BookInternalremark varchar(255) default NULL,
     OpenBorrow varchar(255) default NULL,
     OpenReservation varchar(255) default NULL,
     RemarkToUser varchar(255) default NULL,
     Tags varchar(255) default NULL,
     URL varchar(255) default NULL,
     ACNO varchar(255) default NULL,
     barcode varchar(255) default NULL,
     BookLocation varchar(255) default NULL,
     AccountDate varchar(255) default NULL,
     PurchaseDate varchar(255) default NULL,
     PurchasePrice varchar(255) default NULL,
     ListPrice varchar(255) default NULL,
     Discount varchar(255) default NULL,
     Distributor varchar(255) default NULL,
     PurchaseByDepartment varchar(255) default NULL,
     PurchaseNote varchar(255) default NULL,
     InvoiceNumber varchar(255) default NULL,
     RecordStatus varchar(16) default NULL,
     DateModified datetime NOT NULL,
     LastModifiedBy int(11) NOT NULL,
     UNIQUE KEY id (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_LIBMS_update[] = array(
	"2013-09-02",
	"Add PaymentLogID to LIBMS_OVERDUE_LOG after PaymentMethod",
	"ALTER TABLE LIBMS_OVERDUE_LOG ADD COLUMN PaymentLogID int(11) DEFAULT NULL AFTER PaymentMethod"
);

$sql_LIBMS_update[] = array(
	"2013-09-03",
	"eLib+ - Add fields for periodical in LIBMS_BOOK",
	"alter table LIBMS_BOOK add PeriodicalCode varchar(64) NOT NULL after BookCode_BAK,
							add ISSN varchar(64) default NULL after ISBN2
	"
);

$sql_LIBMS_update[] = array(
	"2013-09-03",
	"eLib+ - Add fields for periodical in LIBMS_BOOK",
	"alter table LIBMS_BOOK add IsPeriodical tinyint(2) default 0 after OpenReservation"
);

$sql_LIBMS_update[] = array(
	"2013-09-05",
	"LIBMS - Add fields for Accompany Material in LIBMS_BOOK_UNIQUE",
	"alter table LIBMS_BOOK_UNIQUE add AccompanyMaterial varchar(255) default null"
);

$sql_LIBMS_update[] = array(
	"2013-09-06",
	"LIBMS - Change the property of NoOfPage to varchar in LIBMS_BOOK",
	"alter table LIBMS_BOOK change column NoOfPage NoOfPage varchar(32) default null"
);

$sql_LIBMS_update[] = array(
	"2013-09-07",
	"LIBMS - Create periodical order table",
	"CREATE TABLE IF NOT EXISTS `LIBMS_PERIODICAL_ORDER` (
	  `OrderID` int(11) NOT NULL auto_increment,
	  `BookID` int(11) NOT NULL,
	  `OrderStartDate` date default NULL,
	  `OrderEndDate` date default NULL,
	  `Price` decimal(10,2) default NULL,
	  `FrequencyUnit` varchar(8) default NULL,
	  `FrequencyNum` tinyint(2) default NULL,
	  `PublishDateFirst` date default NULL,
	  `PublishDateLast` date default NULL,
	  `FirstIssue` int(8) default NULL,
	  `LastIssue` int(8) default NULL,
	  `Distributor` varchar(64) default NULL,
	  `Remarks` varchar(128) default NULL,
	  `AlertDay` int(8) default NULL,
	  `IsGenerated` tinyint(2) default 0,
	  `DateInput` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (OrderID),
	  KEY BookID (BookID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-09-09",
	"LIBMS - Create periodical item table",
	"CREATE TABLE IF NOT EXISTS `LIBMS_PERIODICAL_ITEM` (
	  `ItemID` int(11) NOT NULL auto_increment,
	  `BookID` int(11) NOT NULL,
	  `OrderID` int(11) NOT NULL,
	  `Quantity` int(8) default 1,
	  `IssueNum` int(8) default NULL,
	  `PublishDate` date default NULL,
	  `RegistrationDate` datetime default NULL,
	  `RegistrationBy` int(11) default NULL,
	  `DateInput` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (ItemID),
	  KEY BookOrderID (BookID, OrderID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-09-10",
	"LIBMS - add GenerateDate in LIBMS_PERIODICAL_ORDER",
	"alter table LIBMS_PERIODICAL_ORDER add column GenerateDate datetime default null after IsGenerated"
);

$sql_LIBMS_update[] = array(
	"2013-09-10",
	"LIBMS settings for handling expired reservation",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(7, 'max_book_reserved_limit', 'INT', '0', '0');"
);

$sql_LIBMS_update[] = array(
	"2013-09-10",
	"LIBMS - add RecordStatus in LIBMS_PERIODICAL_ORDER",
	"alter table LIBMS_PERIODICAL_ORDER add column RecordStatus tinyint(2) default 1 after GenerateDate"
);

$sql_LIBMS_update[] = array(
	"2013-09-10",
	"LIBMS - add RecordStatus in LIBMS_PERIODICAL_ITEM",
	"alter table LIBMS_PERIODICAL_ITEM add column RecordStatus tinyint(2) default 1 after RegistrationBy"
);

$sql_LIBMS_update[] = array(
	"2013-09-11",
	"LIBMS - add PeriodicalItemID in LIBMS_BOOK_UNIQUE",
	"alter table LIBMS_BOOK_UNIQUE add column PeriodicalItemID int(11) default NULL after AccompanyMaterial"
);

$sql_LIBMS_update[] = array(
	"2013-09-11",
	"LIBMS - add Details in LIBMS_PERIODICAL_ITEM",
	"alter table LIBMS_PERIODICAL_ITEM add column Details varchar(64) default null after PublishDate"
);

$sql_LIBMS_update[] = array(
	"2013-09-11",
	"LIBMS - modify OrderID in LIBMS_PERIODICAL_ITEM allow OrderID have null value now",
	"alter table LIBMS_PERIODICAL_ITEM MODIFY column OrderID int(11) default null"
);

$sql_LIBMS_update[] = array(
	"2013-09-11",
	"LIBMS - modify ACNO_Num in LIBMS_BOOK_UNIQUE to support varchar",
	"alter table LIBMS_BOOK_UNIQUE change column ACNO_Num ACNO_Num varchar(16) not null"
);

$sql_LIBMS_update[] = array(
	"2013-09-11",
	"LIBMS - modify Next_Number in LIBMS_ACNO_LOG to support varchar",
	"alter table LIBMS_ACNO_LOG change column Next_Number Next_Number varchar(16) not null"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"LIBMS - Create periodical book table",
	"CREATE TABLE IF NOT EXISTS `LIBMS_PERIODICAL_BOOK` (
	  `PeriodicalBookID` int(11) NOT NULL auto_increment,
	  `PeriodicalCode` varchar(64) NOT NULL,
	  `ResourcesTypeCode` varchar(8) NOT NULL,
	  `CirculationTypeCode` varchar(8) default 1,
	  `BookTitle` varchar(128) default NULL,
	  `Publisher` varchar(64) default NULL,
	  `PublishPlace` varchar(64) default NULL,
	  `ISSN` varchar(64) default NULL,
	  `Alert` varchar(32) default NULL,
	  `BookCategoryCode` varchar(8) default NULL,
	  `PurchaseByDepartment` varchar(32) default NULL,
	  `Remarks` varchar(128) default NULL,
	  `OpenBorrow` tinyint(1) default NULL,
	  `OpenReservation` tinyint(1) default NULL,
	  `RecordStatus` tinyint(1) default NULL,
	  `DateInput` datetime default NULL,
	  `InputBy` int(11) default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (PeriodicalBookID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"LIBMS - modify BookID to PeriodicalBookID in LIBMS_PERIODICAL_ORDER",
	"alter table LIBMS_PERIODICAL_ORDER change column BookID PeriodicalBookID int(11) not null"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"LIBMS - modify BookID to PeriodicalBookID in LIBMS_PERIODICAL_ITEM",
	"alter table LIBMS_PERIODICAL_ITEM change column BookID PeriodicalBookID int(11) not null"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"eLib+ - Add fields PeriodicalBookID and PeriodicalItemID in LIBMS_BOOK",
	"alter table LIBMS_BOOK add PeriodicalBookID int(11) default NULL after IsPeriodical,
							add PeriodicalItemID int(11) default NULL after PeriodicalBookID"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"eLib+ - Add fields PeriodicalBookID in LIBMS_BOOK_UNIQUE",
	"alter table LIBMS_BOOK_UNIQUE add PeriodicalBookID int(11) default NULL after AccompanyMaterial"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"eLib+ - Add index to PeriodicalBookID and PeriodicalItemID in LIBMS_BOOK",
	"alter table LIBMS_BOOK add index PeriodicalBookAndItem (PeriodicalBookID, PeriodicalItemID),
							add index PeriodicalItemID (PeriodicalItemID)"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"eLib+ - Add index to PeriodicalBookID and PeriodicalItemID in LIBMS_BOOK_UNIQUE",
	"alter table LIBMS_BOOK_UNIQUE 	add index PeriodicalBookAndItem (PeriodicalBookID, PeriodicalItemID),
									add index PeriodicalItemID (PeriodicalItemID)"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"LIBMS - modify IssueNum int(8) to Issue varchar(64) in LIBMS_PERIODICAL_ITEM",
	"alter table LIBMS_PERIODICAL_ITEM change column IssueNum Issue varchar(64) default null"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"eLib+ - Add fields GeneratedBy in LIBMS_PERIODICAL_ORDER",
	"alter table LIBMS_PERIODICAL_ORDER add GeneratedBy int(11) default NULL after GenerateDate"
);

$sql_LIBMS_update[] = array(
	"2013-09-12",
	"LIBMS - modify Quantity in LIBMS_PERIODICAL_ITEM default 0 now",
	"alter table LIBMS_PERIODICAL_ITEM MODIFY column Quantity int(8) default 0"
);

$sql_eClassIP_update[] = array(
	"2013-09-13",
	"eNotice - add column OriginalAttachment for check the original attachment path generate from new page",
	"alter table INTRANET_NOTICE add column OriginalAttachment varchar(255) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-09-13",
	"ePayment / ePrinting API - create table PAYMENT_API_REQUEST_LOG",
	"CREATE TABLE IF NOT EXISTS PAYMENT_API_REQUEST_LOG (
		LogID int(11) NOT NULL auto_increment,
		CardID varchar(20) NOT NULL,
		Request varchar(100) NOT NULL,
		Token varchar(100) NOT NULL,
		DateInput datetime default NULL,
		PRIMARY KEY (LogID),
		INDEX ICardID(CardID),
		INDEX IToken(Token),
		INDEX IDateInput(DateInput)
	)ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_LIBMS_update[] = array(
	"2013-09-16",
	"LIBMS - change LocationCode in LIBMS_LOCATION from varchar(8) to varchar(64)",
	"alter table LIBMS_LOCATION modify LocationCode varchar(64) NOT NULL"
);

$sql_LIBMS_update[] = array(
	"2013-09-16",
	"LIBMS - change LocationCode in LIBMS_BOOK_UNIQUE from varchar(8) to varchar(64)",
	"alter table LIBMS_BOOK_UNIQUE modify LocationCode varchar(64) default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-09-17",
	"ePayment - add DateModified and ModifyBy to PAYMENT_ACCOUNT for tracing purpose",
	"ALTER TABLE PAYMENT_ACCOUNT ADD COLUMN DateModified datetime DEFAULT NULL, ADD COLUMN ModifyBy int(11) DEFAULT NULL"
);

$sql_LIBMS_update[] = array(
	"2013-09-24",
	"LIBMS - change CallNum and CallNum2 in LIBMS_BOOK from varchar(64) to varchar(255)",
	"alter table LIBMS_BOOK modify CallNum varchar(255) default NULL COMMENT 'as it can be null or unique, uniqueness should be checked using PHP if it is filled',
							modify CallNum2 varchar(255) default NULL COMMENT 'as it can be null or unique, uniqueness should be checked using PHP if it is filled'"
);

$sql_eClassIP_update[] = array(
	"2013-09-26",
	"Alter Table SPORTS_STUDENT_ENROL_INFO modify column DateModified",
	"ALTER Table SPORTS_STUDENT_ENROL_INFO MODIFY COLUMN DateModified datetime Default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-09-26",
	"Alter Table INTRANET_ARCHIVE_USER add column, follow INTRANET_USER",
	"ALTER TABLE INTRANET_ARCHIVE_USER ADD COLUMN UserEmail varchar(100) DEFAULT NULL,
										ADD COLUMN FirstName varchar(100) DEFAULT NULL,
										ADD COLUMN LastName varchar(100) DEFAULT NULL,
										ADD COLUMN TitleEnglish varchar(100) DEFAULT NULL,
										ADD COLUMN TitleChinese varchar(100) DEFAULT NULL,
										ADD COLUMN ClassLevel varchar(20) DEFAULT NULL,
										ADD COLUMN RFID varchar(20) DEFAULT NULL,
										ADD COLUMN Teaching char(2) DEFAULT NULL,
										ADD COLUMN LastUsed datetime DEFAULT NULL,
										ADD COLUMN LastModifiedPwd datetime DEFAULT NULL,
										ADD COLUMN SessionKey varchar(255) DEFAULT NULL,
										ADD COLUMN SessionLastUpdated datetime DEFAULT NULL,
										ADD COLUMN RecordStatus char(2) DEFAULT NULL,
										ADD COLUMN HKID varchar(10) DEFAULT NULL,
										ADD COLUMN STRN varchar(9) DEFAULT NULL,
										ADD COLUMN InternalUserID varchar(255) DEFAULT NULL,
										ADD COLUMN IDNumber varchar(255) DEFAULT NULL,
										ADD COLUMN PlaceOfBirth varchar(255) DEFAULT NULL,
										ADD COLUMN IntranetUserDateInput datetime DEFAULT NULL,
										ADD COLUMN IntranetUserInputBy int(8) DEFAULT NULL,
										ADD COLUMN IntranetUserDateModified timestamp,
										ADD COLUMN IntranetUserModifyBy int(8) DEFAULT NULL,
										ADD COLUMN ImapUserEmail varchar(100) DEFAULT NULL,
										ADD COLUMN LastLang varchar(2) DEFAULT NULL,
										ADD COLUMN PersonalPhotoLink varchar(255) DEFAULT NULL,
										ADD COLUMN isSystemAdmin tinyint(1) DEFAULT NULL,
										ADD COLUMN HKJApplNo int(10) DEFAULT NULL,
										ADD COLUMN StaffCode varchar(100) DEFAULT NULL,
										ADD COLUMN Barcode varchar(30) DEFAULT NULL,
										ADD COLUMN ApiDateInput datetime DEFAULT NULL,
										ADD COLUMN ApiInputBy int(8) DEFAULT NULL,
										ADD COLUMN ApiDateModified datetime DEFAULT NULL,
										ADD COLUMN ApiModifiyBy int(8) DEFAULT NULL,
										ADD COLUMN CFirstName varchar(100) DEFAULT NULL,
										ADD COLUMN CLastName varchar(100) DEFAULT NULL
										"
);

$sql_eClassIP_update[] = array(
	"2013-09-26",
	"AccountMgmt - Add ApiDateInput to INTRANET_USER",
	"ALTER Table INTRANET_USER add column ApiDateInput datetime default NULL after Barcode"
);

$sql_eClassIP_update[] = array(
	"2013-09-26",
	"AccountMgmt - Add ApiInputBy to INTRANET_USER",
	"ALTER Table INTRANET_USER add column ApiInputBy int(8) default NULL after ApiDateInput"
);

$sql_eClassIP_update[] = array(
	"2013-09-26",
	"AccountMgmt - Add ApiDateModified to INTRANET_USER",
	"ALTER Table INTRANET_USER add column ApiDateModified datetime default NULL after ApiInputBy"
);

$sql_eClassIP_update[] = array(
	"2013-09-26",
	"AccountMgmt - Add ApiModifiyBy to INTRANET_USER",
	"ALTER Table INTRANET_USER add column ApiModifiyBy int(8) default NULL after ApiDateModified"
);

$sql_eClassIP_update[] = array(
	"2013-09-27",
	"ePayment - add InputBy field",
	"ALTER Table PAYMENT_OVERALL_TRANSACTION_LOG add column InputBy int(11)"
);

$sql_LIBMS_update[] = array(
	"2013-09-27",
	"LIBMS - add 2 columns in LIBMS_BORROW_LOG",
	"ALTER Table LIBMS_BORROW_LOG add column DueDate_BAK date default NULL after DueDate,
									add column DueDateModifiedBy int(11) default NULL after LastModifiedBy"
);

$sql_eClassIP_update[] = array(
	"2013-10-04",
	"eAdmission - create table ADMISSION_APPLICATION_SETTING",
	"CREATE TABLE IF NOT EXISTS ADMISSION_APPLICATION_SETTING (
	     SettingID int(11) NOT NULL auto_increment,
	     SchoolYearID int(11) NOT NULL default '0',
	     ClassLevelID int(11) NOT NULL default '0',
	     StartDate datetime default NULL,
	     EndDate datetime default NULL,
	     DayType varchar(100) default NULL,
	     DateInput datetime default NULL,
	     InputBy int(11) default NULL,
	     DateModified datetime default NULL,
	     ModifiedBy int(11) default NULL,
	     PRIMARY KEY (SettingID),
	     KEY SchoolYearClassLevel (SchoolYearID,ClassLevelID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-10-04",
	"eAdmission - create table ADMISSION_SETTING",
	"CREATE TABLE IF NOT EXISTS ADMISSION_SETTING (
     SettingID int(11) NOT NULL auto_increment,
     SchoolYearID int(11) NOT NULL default '0',
     SettingName varchar(100) NOT NULL default '',
     SettingValue text,
     DateInput datetime default NULL,
     InputBy int(11) default NULL,
     DateModified datetime default NULL,
     ModifiedBy int(11) default NULL,
     PRIMARY KEY (SettingID),
     KEY SchoolYearSettingName (SchoolYearID,SettingName)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-10-04",
	"eAdmission - create table ADMISSION_APPLICATION_STATUS",
	"CREATE TABLE IF NOT EXISTS ADMISSION_APPLICATION_STATUS (
	     RecordID int(11) NOT NULL auto_increment,
	     SchoolYearID int(11) NOT NULL default '0',
	     ApplicationID varchar(100) NOT NULL default '0',
	     ReceiptID text,
	     ReceiptDate datetime default NULL,
	     Handler int(11) default NULL,
	     InterviewDate datetime default NULL,
	     Remark text,
	     Status int(11) default NULL,
	     DateInput datetime default NULL,
	     InputBy int(11) default NULL,
	     DateModified datetime default NULL,
	     ModifiedBy int(11) default NULL,
	     PRIMARY KEY (RecordID),
	     KEY Status (Status),
	     KEY SchoolYearApplication (SchoolYearID,ApplicationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
	"2013-10-04",
	"eAdmission - application form - student section",
	"CREATE TABLE IF NOT EXISTS ADMISSION_STU_INFO (
		RecordID int(11) NOT NULL auto_increment,
		ApplicationID varchar(100) NOT NULL,
		ChineseName varchar(255) default NULL,
		EnglishName varchar(255) default NULL,
		Gender char(1) default NULL,
		DOB date default NULL,
		BirthCertNo varchar(255) default NULL,
		PlaceOfBirth varchar(255) default NULL,
		Province varchar(255) default NULL,
		County varchar(255) default NULL,
		HomeTelNo varchar(255) default NULL,
		Address varchar(255) default NULL,
		Email varchar(255) default NULL,
		ReligionCode int(11) default NULL,
		Church varchar(255) default NULL,
		LastSchool varchar(255) default NULL,
		LastSchoolLevel int(11) default NULL,
		DateInput datetime NOT NULL,
		InputBy int(11) NOT NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (RecordID),
		INDEX ApplicationID (ApplicationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-10-04",
	"eAdmission - application form - perant section",
	"CREATE TABLE IF NOT EXISTS ADMISSION_PG_INFO (
		RecordID int(11) NOT NULL auto_increment,
		ApplicationID int(11) NOT NULL,
		PG_TYPE char(1) NOT NULL,
		Relationship varchar(100) default NULL,
		EnglishName varchar(255) default NULL,
		ChineseName varchar(255) default NULL,
		JobTitle varchar(255) default NULL,
		Company varchar(255) default NULL,
		JobPosition varchar(255) default NULL,
		OfficeAddress varchar(255) default NULL,
		OfficeTelNo varchar(255) default NULL,
		Mobile varchar(255) default NULL,
		DateInput datetime NOT NULL,
		InputBy int(11) NOT NULL,
		DateModified datetime default NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (RecordID),
		INDEX ApplicationID (ApplicationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);	
	
$sql_eClassIP_update[] = array(
	"2013-10-04",
	"eAdmission - application form - other info",
	"CREATE TABLE IF NOT EXISTS ADMISSION_OTHERS_INFO (
		RecordID int(11) NOT NULL auto_increment,
		ApplicationID int(11) NOT NULL,
		EBrotherNo int(11) default NULL,
		ESisterNo int(11) default NULL,
		YBrotherNo int(11) default NULL,
		YSisterNo int(11) default NULL,
		ApplyYear int(11) NOT NULL,
		ApplyMonth int(11) NOT NULL,
		ApplyTerm int(11) NOT NULL,
		ApplyDayType1 int(11) NOT NULL,
		ApplyDayType2 int(11) default NULL,
		ApplyDayType3 int(11) default NULL,
		ApplyLevel int(11) NOT NULL,
		ExBSName varchar(255) default NULL,
		ExBSLevel varchar(255) default NULL,
		CurBSName varchar(255) default NULL,
		CurBSLevel varchar(255) default NULL,
		NeedSchoolBus int(11) NOT NULL,
		SchoolBusPlace varchar(255) default NULL,
		KnowUsBy int(11) default NULL,
		KnowUsByOther varchar(255) default NULL,
		DateInput datetime NOT NULL,
		DateModified datetime NOT NULL,
		ModifiedBy int(11) default NULL,
		PRIMARY KEY (RecordID),
		INDEX ApplicationID (ApplicationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_LIBMS_update[] = array(
	"2013-10-07",
	"LIBMS settings for enable student selection by choosing class & class number in Circulation Management",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(8, 'circulation_student_by_selection', 'BOOL', '0', '0');"
);

$sql_eClassIP_update[] = array(
	"2013-10-08",
	"[KIS] School Calendar - add field ClassGroupID to INTRANET_EVENT",
	"ALTER TABLE INTRANET_EVENT ADD COLUMN ClassGroupID int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-10-09",
	"eAdmission - ADMISSION_STU_INFO - MODIFY InputBy DEFAULT NULL",
	"ALTER TABLE ADMISSION_STU_INFO MODIFY COLUMN InputBy int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-10-09",
	"eAdmission - ADMISSION_PG_INFO - MODIFY InputBy DEFAULT NULL",
	"ALTER TABLE ADMISSION_PG_INFO MODIFY COLUMN InputBy int(11) DEFAULT NULL"
);


$sql_eClassIP_update[] = array(
	"2013-10-09",
	"eAdmission - ADMISSION_PG_INFO - MODIFY ApplicationID VARCHAR(100)",
	"ALTER TABLE ADMISSION_PG_INFO MODIFY COLUMN ApplicationID VARCHAR(100) NOT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-10-09",
	"eAdmission - ADMISSION_OTHERS_INFO - MODIFY ApplicationID VARCHAR(100)",
	"ALTER TABLE ADMISSION_OTHERS_INFO MODIFY COLUMN ApplicationID VARCHAR(100) NOT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-10-10",
	"eAdmission - ADMISSION_APPLICATION_SETTING - Store FirstPageContent, LastPageContent here for each form",
	"ALTER TABLE ADMISSION_APPLICATION_SETTING ADD COLUMN FirstPageContent MEDIUMTEXT DEFAULT NULL,  ADD COLUMN LastPageContent MEDIUMTEXT DEFAULT NULL"
);

$sql_LIBMS_update[] = array(
	"2013-10-11",
	"eLib+ - change ISBN and ISBN2 in LIBMS_BOOK from varchar(64) to varchar(255)",
	"ALTER TABLE LIBMS_BOOK MODIFY COLUMN ISBN varchar(255) DEFAULT NULL,
							MODIFY COLUMN ISBN2 varchar(255) DEFAULT NULL"
);


$sql_LIBMS_update[] = array(
	"2013-10-11",
	"eLib+ - add item status to CSV import table",
	"ALTER TABLE LIBMS_CSV_TMP_2 ADD RecordStatus varchar(16) "
);

$sql_LIBMS_update[] = array(
	"2013-10-14",
	"LIBMS settings for due date calculation in Circulation Management",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(9, 'circulation_duedate_method', 'BOOL', '0', '0');"
);

$sql_eClassIP_update[] = array(
	"2013-10-17",
	"eAdmission - ADMISSION_APPLICATION_STATUS - add column to check if client notified parent yet",
	"ALTER TABLE ADMISSION_APPLICATION_STATUS ADD COLUMN isNotified tinyint(1) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-10-22",
	"ePayment - Create table PAYMENT_PAYMENT_ITEM_SUBSIDY to store multiple source of subsidy",
	"CREATE TABLE IF NOT EXISTS PAYMENT_PAYMENT_ITEM_SUBSIDY (
		PaymentID int(11) NOT NULL,
		UserID int(11) NOT NULL,
		SubsidyUnitID int(11) NOT NULL,
		SubsidyAmount double(20,2) NOT NULL,
		SubsidyPICAdmin varchar(255),
		SubsidyPICUserID int(11),
		DateInput datetime,
		DateModified datetime,
		InputBy int(11),
		ModifyBy int(11),
		UNIQUE KEY UniqueKey(PaymentID,UserID,SubsidyUnitID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-10-23",
	"eAdmission - Create table ADMISSION_ATTACHMENT_RECORD to store applicant attachment",
	"CREATE TABLE IF NOT EXISTS ADMISSION_ATTACHMENT_RECORD (
     RecordID int(11) NOT NULL auto_increment,
     ApplicationID varchar(100) NOT NULL,
     AttachmentType varchar(255) default NULL,
     AttachmentName varchar(255) default NULL,
     DateInput datetime NOT NULL,
     InputBy int(11) default NULL,
     PRIMARY KEY (RecordID),
     KEY ApplicationID (ApplicationID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
	"2013-10-23",
	"eAdmission - ADMISSION_STU_INFO - modify column LastSchoolLevel from int to varchar",
	"ALTER TABLE ADMISSION_STU_INFO MODIFY COLUMN LastSchoolLevel VARCHAR(255) DEFAULT NULL"
);

$sql_eClassIP_update[] = array(
	"2013-10-25",
	"Add  InputFormat  to INTRANET_EBOOK_BOOK",
	"ALTER TABLE INTRANET_EBOOK_BOOK ADD  InputFormat varchar(2) default NULL"
);
$sql_LIBMS_update[] = array(
	"2013-10-30",
	"LIBMS settings for handling the display name of the school",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(10, 'school_display_name', 'STRING', '', '');"
);

$sql_eClassIP_update[] = array(
	"2013-10-30",
	"Writing 2.0 - ADD Table",
	"
		CREATE TABLE IF NOT EXISTS  W2_CONTENT_DATA (
		 	cid int(8) NOT NULL auto_increment,
		 	topicName varchar(50) default NULL,
		 	topicIntro mediumtext,
		 	level varchar(50) default NULL,
		 	category varchar(50) default NULL,
		 	step1Content mediumtext,
		 	step2Content mediumtext,
		 	step3Content mediumtext,
		 	step4Content mediumtext,
		 	step5Content mediumtext,
		 	step6Content mediumtext,
		 	r_contentCode varchar(10) default NULL,
		 	schoolCode varchar(10) NOT NULL,
		 	schemeNum int(5) default NULL,
		 	status int(3) NOT NULL COMMENT '0=drafted, 1=published,2=disabled ',
		 	dateInput timestamp NOT NULL default '0000-00-00 00:00:00',
		 	createdBy int(8) default NULL,
		 	dateModified timestamp NOT NULL default '0000-00-00 00:00:00',
		 	modifiedBy int(8) default NULL,
		 	dateGenerated timestamp NOT NULL default '0000-00-00 00:00:00',
		 	datePublished timestamp NOT NULL default '0000-00-00 00:00:00',
		 	powerConceptID int(8) default NULL,
		 	deletedby int(8) default NULL,
		 	dateDeleted timestamp NOT NULL default '0000-00-00 00:00:00',
		 	resource mediumtext,
		 	PRIMARY KEY (cid)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	"
);

$sql_LIBMS_update[] = array(
	"2013-11-01",
	"Add the order field of the label information in LIBMS_LABEL_FORMAT",
	"ALTER TABLE LIBMS_LABEL_FORMAT
	ADD COLUMN info_order_1 varchar(50) Default NULL AFTER max_barcode_height,
	ADD COLUMN info_order_2 varchar(50) Default NULL AFTER info_order_1,
	ADD COLUMN info_order_3 varchar(50) Default NULL AFTER info_order_2,
	ADD COLUMN info_order_4 varchar(50) Default NULL AFTER info_order_3,
	ADD COLUMN info_order_5 varchar(50) Default NULL AFTER info_order_4,
	ADD COLUMN info_order_6 varchar(50) Default NULL AFTER info_order_5;"
);

# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-11-02",
	"iPortfolio (OEA) - OEA with two fields for HKUGAC customization",
	"ALTER TABLE OLE_PROGRAM ADD IsSAS boolean default 0, ADD IsOutsideSchool boolean default 0"
);

$sql_eClassIP_update[] = array(
	"2013-11-05",
	"eDiscipline - (MST Customization) add column FileNumber and RecordNumber to DISCIPLINE_MERIT_RECORD",
	"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD COLUMN FileNumber varchar(255) DEFAULT NULL COMMENT 'For MST Cust', ADD COLUMN RecordNumber varchar(255) DEFAULT NULL COMMENT 'For MST Cust';"
);


$sql_eClassIP_update[] = array(
	"2013-11-05",
	"eLibrary TW Blank Page - add IsFirstPageBlank to INTRANET_ELIB_BOOK",
	"ALTER TABLE INTRANET_ELIB_BOOK ADD IsFirstPageBlank char(2) default '0';"
);

##########
// 2013-11-11 [Cameron] start create table for cardapi >>> payment (ricoh)

$sql_eClassIP_update[] = array(
	"2013-11-11",
	"cardapi - payment",
	"CREATE TABLE IF NOT EXISTS RICOH_TRANS_DETAILS (
  LogID int(11) NOT NULL auto_increment,
  BatchID int(11) NOT NULL default 0,
  LogDate datetime NOT NULL,
  SeqNo int(8) NOT NULL default 0,
  LogType varchar(10) default NULL,
  BWA3Cost float(8,2) default 0,
  BWA4Cost float(8,2) default 0,
  ColorA3Cost float(8,2) default 0,
  ColorA4Cost float(8,2) default 0,
  MonoSides int(8) default 0,
  ColorSides int(8) default 0,
  RicohUserID varchar(255) default NULL,
  PaperSizeCode varchar(10) default NULL, 
  PaperSize varchar(10) default NULL,   
  CardNo varchar(255) not null default '',
  Amount float(8,2) default 0,
  ApiDate datetime default null,
  PRIMARY KEY (LogID),
  UNIQUE INDEX IdxLogDateSeqNo(LogDate,SeqNo)  
)
ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2013-11-12",
	"cardapi - payment",
	"CREATE TABLE IF NOT EXISTS RICOH_TRANS_SUMMARY (  
  BatchID int(11) NOT NULL default 0,
  RicohUserID varchar(255) default NULL,
  CardNo varchar(255) not null default '',
  Amount float(8,2) default 0,
  ApiDate datetime default null,
  KEY (BatchID),
  INDEX RicohUserID(RicohUserID),
  INDEX CardNo(CardNo),
  INDEX ApiDate(ApiDate)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2013-11-15",
	"cardapi - payment: add fields RecordStatus, TransactionTime to RICOH_TRANS_SUMMARY",
	"ALTER TABLE RICOH_TRANS_SUMMARY ADD COLUMN RecordStatus tinyint default 0 COMMENT '0=Outstanding,1=Paid', ADD COLUMN TransactionTime datetime, ADD COLUMN IPUserID int(8);"
);

$sql_eClassIP_update[] = array(
	"2013-11-19",
	"cardapi - payment summary: remove field TransactionTime, add index IdxBatchIDIPUserID to RICOH_TRANS_SUMMARY",
	"ALTER TABLE RICOH_TRANS_SUMMARY DROP COLUMN TransactionTime, ADD INDEX IdxBatchIDIPUserID (BatchID,IPUserID);"
);

$sql_eClassIP_update[] = array(
	"2013-11-20",
	"cardapi - payment summary: add column IPPaymentLogID to RICOH_TRANS_SUMMARY",
	"ALTER TABLE RICOH_TRANS_SUMMARY ADD COLUMN IPPaymentLogID int(11);"
);

// end create table for cardapi >>> payment (ricoh)

$sql_eClassIP_update[] = array(
	"2013-11-13",
	"User Mgmt - Add CardID3 after CardID to INTRANET_USER",
	"ALTER TABLE INTRANET_USER ADD COLUMN CardID3 varchar(255) DEFAULT NULL AFTER CardID"
);

$sql_eClassIP_update[] = array(
	"2013-11-13",
	"User Mgmt - Add CardID2 after CardID to INTRANET_USER",
	"ALTER TABLE INTRANET_USER ADD COLUMN CardID2 varchar(255) DEFAULT NULL AFTER CardID"
);

$sql_eClassIP_update[] = array(
	"2013-11-13",
	"User Mgmt - Add index CardID2 to INTRANET_USER",
	"ALTER TABLE INTRANET_USER ADD INDEX CardID2(CardID2)"
);

$sql_eClassIP_update[] = array(
	"2013-11-13",
	"User Mgmt - Add index CardID3 to INTRANET_USER",
	"ALTER TABLE INTRANET_USER ADD INDEX CardID3(CardID3)"
);

$sql_eClassIP_update[] = array(
	"2013-11-14",
	"ePost - Add Column DisplayTitle to table EPOST_NEWSPAPER_PAGE_ARTICLE",
	"ALTER TABLE EPOST_NEWSPAPER_PAGE_ARTICLE ADD COLUMN DisplayTitle tinyint(1) DEFAULT 1 AFTER DisplayAuthor"
);

$sql_eClassIP_update[] = array(
	"2013-11-14",
	"ePost - Add Column DisplayContent to table EPOST_NEWSPAPER_PAGE_ARTICLE",
	"ALTER TABLE EPOST_NEWSPAPER_PAGE_ARTICLE ADD COLUMN DisplayContent tinyint(1) DEFAULT 1 AFTER DisplayTitle"
);
##########

// Old iMail tables that new clients may miss
$sql_eClassIP_update[] = array(
	"2013-11-15",
	"iMail - recreate external group table as new clients use iMail plus may not have this table",
	"CREATE TABLE `INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL` (
	  `AliasID` int(11) NOT NULL auto_increment,
	  `OwnerID` int(11) default NULL,
	  `AliasName` varchar(255) default NULL,
	  `Remark` mediumtext,
	  `NumberOfEntry` int(11) default NULL,
	  `RecordType` char(2) default NULL,
	  `RecordStatus` char(2) default NULL,
	  `DateInput` datetime default NULL,
	  `DateModified` datetime default NULL,
	  PRIMARY KEY  (`AliasID`),
	  KEY `OwnerID` (`OwnerID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
// Old iMail tables that new clients may miss
$sql_eClassIP_update[] = array(
	"2013-11-15",
	"iMail - recreate external group entry table as new clients use iMail plus may not have this table",
	"CREATE TABLE `INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY` (
	  `EntryID` int(11) NOT NULL auto_increment,
	  `AliasID` int(11) default NULL,
	  `TargetID` int(11) default NULL,
	  `RecordType` char(2) default NULL,
	  `RecordStatus` char(2) default NULL,
	  `DateInput` datetime default NULL,
	  `DateModified` datetime default NULL,
	  PRIMARY KEY  (`EntryID`),
	  UNIQUE KEY `AliasTargetIDRecordType` (`AliasID`,`TargetID`),
	  KEY `AliasID` (`AliasID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
	"2013-11-20",
	"Digital Archive - Subject Resources Rating Record",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_RATING_RECORD (
	     RatingID int(11) NOT NULL auto_increment,
	     UserID int(11) NOT NULL,
	     SubjectResourceID int(11) NOT NULL,
	     Rating int(11) NOT NULL,
	     DateInput datetime default NULL,
	     PRIMARY KEY (RatingID) ,
	     KEY UserID (UserID),
	     KEY SubjectResourceID (SubjectResourceID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
	"2013-11-20",
	"Digital Archive - Subject Resources View Record",
	"CREATE TABLE IF NOT EXISTS DIGITAL_ARCHIVE_SUBJECT_RESOURCE_VIEW_RECORD (
	     ViewID int(11) NOT NULL auto_increment,
	     UserID int(11) default NULL,
	     SubjectResourceID int(11) default NULL,
	     DateModified datetime default NULL,
	     PRIMARY KEY (ViewID),
	     KEY UserID (UserID),
	     KEY SubjectResourceID (SubjectResourceID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_LIBMS_update[] = array(
	"2013-11-25",
	"Add two column in LIBMS_WRITEOFF_LOG",
	"ALTER TABLE LIBMS_WRITEOFF_LOG
	ADD COLUMN Reason varchar(128) Default NULL AFTER PreviousBookStatus,
	ADD COLUMN WriteOffDate datetime Default NULL AFTER Reason;"
);

$sql_eClassIP_update[] = array(
	"2013-11-25",
	"eClassApp - create table APP_USER_DEVICE to store user device data",
	"CREATE TABLE IF NOT EXISTS APP_USER_DEVICE (
	     RecordID int(11) NOT NULL auto_increment,
	     UserID int(11) NOT NULL,
	     DeviceID text NOT NULL,
	     RecordStatus tinyint(2) NOT NULL Default 1,
         DateInput datetime NOT NULL,
	     DateModified datetime NOT NULL,
	     PRIMARY KEY (RecordID),
	     KEY UserIDRecordStatus (UserID, RecordStatus),
	     KEY RecordStatus (RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-11-26",
	"ePayment - Add [Remark] field to PAYMENT_PAYMENT_ITEMSTUDENT",
	"ALTER TABLE PAYMENT_PAYMENT_ITEMSTUDENT ADD COLUMN Remark text DEFAULT NULL AFTER ProcessingTerminalIP"
);

$sql_eClassIP_update[] = array(
	"2013-11-27",
	"system performance log - Memory",
	"CREATE TABLE IF NOT EXISTS INTRANET_SYSTEM_LOG (
	     LogID int(11) NOT NULL auto_increment,
	     UserID int(11) NOT NULL,
	     Script varchar(255),
	     LogType varchar(16) NOT NULL,
	     LogValue int(11) default NULL,
	     LogText text default NULL,
         DateInput datetime NOT NULL,
	     PRIMARY KEY (LogID),
	     UNIQUE Script (Script)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_LIBMS_update[] = array(
	"2013-11-28",
	"LIBMS settings for handling Due Date Reminder",
	"INSERT INTO `LIBMS_SYSTEM_SETTING` (`id`, `name`, `type`, `value`, `default`) VALUES
(11, 'due_date_reminder', 'INT', '0', '0');"
);

$sql_LIBMS_update[] = array(
	"2013-11-28",
	"eLib + ",
	"INSERT INTO `LIBMS_NOTIFY_SETTING` (`name`, `enable`) VALUES
		('due_date_reminder_email', 1);"
);


$sql_eClassIP_update[] = array(
	"2013-12-02",
	"iMail plus - Create table MAIL_AVAS_RULES for AVAS rule settings",
	"CREATE TABLE IF NOT EXISTS MAIL_AVAS_RULES 
	(
		RecordID int(11) NOT NULL auto_increment,
		Rule varchar(255) NOT NULL,
		Score int(11) NOT NULL,
		Type varchar(10) NOT NULL COMMENT '[subject] or [body]',
		DateInput datetime,
		DateModified datetime,
		PRIMARY KEY (RecordID),
		INDEX RuleIndex(Rule),
		INDEX ScoreIndex(Score),
 		INDEX TypeIndex(Type) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
	"2013-12-02",
	"eClassApp - create table APP_REQUEST_LOG to save request and response log",
	"CREATE TABLE IF NOT EXISTS APP_REQUEST_LOG (
	     LogID int(11) NOT NULL auto_increment,
	     RequestText text NOT NULL,
		 ResponseText text NOT NULL,
	     DateInput datetime NOT NULL,
	     PRIMARY KEY (LogID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-12-03",
	"Push Message - create table INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO to save the push message is related to which user",
	"CREATE TABLE IF NOT EXISTS INTRANET_APP_NOTIFY_MESSAGE_TARGET_RELATED_TO (
	     RecordID int(11) NOT NULL auto_increment,
	     NotifyMessageTargetID int(11) NOT NULL,
		 UserID int(11) NOT NULL,
	     DateInput datetime NOT NULL,
	     PRIMARY KEY (RecordID),
		 KEY NotifyMessageTargetID (NotifyMessageTargetID),
		 KEY UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-12-03",
	"Push Message - modify table INTRANET_APP_NOTIFY_MESSAGE_TARGET to save message ReferenceID and IsRead status",
	"ALTER TABLE INTRANET_APP_NOTIFY_MESSAGE_TARGET Add Column DateModified datetime default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-12-03",
	"Push Message - modify table INTRANET_APP_NOTIFY_MESSAGE to save push message service provider",
	"ALTER TABLE INTRANET_APP_NOTIFY_MESSAGE Add Column ServiceProvider varchar(64) default NULL"
);

$sql_eClassIP_update[] = array (
	"2013-12-04",
	"Writing 2.0 - Add marking method related columns to W2_WRITING",
	"ALTER TABLE W2_WRITING ADD ALLOW_SELF_MARKING tinyint(3) default 1 AFTER ALLOW_PEER_MARKING,
							ADD FULLMARK FLOAT default NULL AFTER ALLOW_SELF_MARKING,
							ADD PASSMARK FLOAT default NULL AFTER FULLMARK,
							ADD LOWESTMARK FLOAT default NULL AFTER PASSMARK,
							ADD TEACHER_WEIGHT FLOAT default NULL AFTER LOWESTMARK,
							ADD SELF_WEIGHT FLOAT default NULL AFTER TEACHER_WEIGHT,
							ADD PEER_WEIGHT FLOAT default NULL AFTER SELF_WEIGHT
	"
);

$sql_eClassIP_update[] = array(
	"2013-12-05",
	"eClassApp - alter table APP_REQUEST_LOG to add Encrypted Request Data fields log",
	"ALTER TABLE APP_REQUEST_LOG 	ADD COLUMN RequestTextEncrypted text default null after LogID,
									ADD COLUMN ResponseTextEncrypted text default null after RequestText"
);

$sql_eClassIP_update[] = array(
	"2013-12-09",
	"announcement - Create table INTRANET_ELIB_BOOK_ANNOUNCEMENT to store announcement data ",
	"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_ANNOUNCEMENT (
     AnnouncementID int(11) NOT NULL auto_increment,
     AnnouncementType varchar(5) default NULL,
     Content text,
     ContentXML text,
     DateModified datetime default NULL,
     PRIMARY KEY (AnnouncementID),
     KEY AnnouncementType (AnnouncementType)
	) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8"
);



$sql_eClassIP_update[] = array(
	"2013-12-10",
	"Push Message - create table INTRANET_APP_NOTIFY_MESSAGE_REFERENCE to save the push message reference status from different service provider",
	"CREATE TABLE IF NOT EXISTS INTRANET_APP_NOTIFY_MESSAGE_REFERENCE (
	     RecordID int(11) NOT NULL auto_increment,
		 NotifyMessageID int(11) NOT NULL,
	     NotifyMessageTargetID int(11) NOT NULL,
		 DeviceRecordID int(11) NOT NULL,
		 MulticastID varchar(255) default NULL,
		 MessageID varchar(255) default NULL,
		 ErrorCode varchar(255) default NULL,
		 MessageStatus tinyint(2) default 0,
	     DateInput datetime NOT NULL,
		 DateModified datetime NOT NULL,
	     PRIMARY KEY (RecordID),
		 UNIQUE KEY MessageDevice (NotifyMessageID, NotifyMessageTargetID, DeviceRecordID), 
		 KEY NotifyMessageTargetID (NotifyMessageTargetID),
		 KEY DeviceRecordID (DeviceRecordID),
		 KEY MessageStatus (MessageStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-12-11",
	"eAdmission - ADMISSION_APPLICATION_SETTING - Store DOBStart, DOBEnd here for each form",
	"ALTER TABLE ADMISSION_APPLICATION_SETTING ADD COLUMN DOBStart date default NULL, ADD COLUMN DOBEnd date default NULL"
);

$sql_eClassIP_update[] = array(
	"2013-12-12",
	"Medical Module - Store Student Extra Info StayOverNight",
	"alter table INTRANET_USER_PERSONAL_SETTINGS add stayOverNight tinyint(1), add stayOverNight_DateModified datetime"
);


# this is for eClass DB update (not for IP25 table)
$sql_eClass_update[] = array(
	"2013-12-13",
	"St Paul SLP customization",
	"CREATE TABLE IF NOT EXISTS SLP_STUDENT_SUBJECT_POINTS (
	     RecordID int(11) NOT NULL auto_increment,
		 StudentID int(11) NOT NULL,
		 AcademicYearID int(8) NOT NULL,
		 TermID int(8) default NULL,
		 SubjectID int(11) NOT NULL,
		 SCORE int(2) NOT NULL,
	     DateInput datetime NOT NULL,
		 DateModified datetime NOT NULL,
		 UNIQUE KEY StudentRecord (StudentID, AcademicYearID, TermID, SubjectID), 
	     PRIMARY KEY (RecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field ANSWER_MARKED in W2_STEP_HANDIN to store the latest version of teacher marked answer",
	"ALTER TABLE W2_STEP_HANDIN ADD COLUMN ANSWER_MARKED TEXT DEFAULT NULL AFTER ANSWER"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field VERSION in W2_STEP_HANDIN to store latest version no",
	"ALTER TABLE W2_STEP_HANDIN ADD COLUMN VERSION int(11) DEFAULT '1' AFTER ANSWER_MARKED"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field MARK in W2_STEP_HANDIN to store overall teacher mark",
	"ALTER TABLE W2_STEP_HANDIN ADD COLUMN MARK VARCHAR(16) DEFAULT NULL AFTER VERSION"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field VERSION in W2_PEER_MARKING_DATA to distinguish handin version in one step",
	"ALTER TABLE W2_PEER_MARKING_DATA ADD COLUMN VERSION int(11) DEFAULT '1' AFTER COMMENT"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field MARK in W2_PEER_MARKING_DATA to store peer marking",
	"ALTER TABLE W2_PEER_MARKING_DATA ADD COLUMN MARK VARCHAR(16) DEFAULT NULL AFTER VERSION"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field STICKER in W2_PEER_MARKING_DATA to store stickers given by peers",
	"ALTER TABLE W2_PEER_MARKING_DATA ADD COLUMN STICKER TEXT DEFAULT NULL AFTER MARK"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field STICKER in W2_STEP_STUDENT_COMMENT to store stickers given by teachers",
	"ALTER TABLE W2_STEP_STUDENT_COMMENT ADD COLUMN STICKER TEXT DEFAULT NULL AFTER CONTENT"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field VERSION in W2_STEP_STUDENT_COMMENT to distinguish handin version in one step",
	"ALTER TABLE W2_STEP_STUDENT_COMMENT ADD COLUMN VERSION INT(11) DEFAULT '1' AFTER STICKER"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field STEP_ID in W2_STEP_STUDENT_COMMENT",
	"ALTER TABLE W2_STEP_STUDENT_COMMENT ADD COLUMN STEP_ID INT(11) DEFAULT NULL AFTER COMMENT_ID"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add field USER_ID in W2_STEP_STUDENT_COMMENT",
	"ALTER Table W2_STEP_STUDENT_COMMENT ADD COLUMN USER_ID INT(11) DEFAULT NULL AFTER STEP_ID"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - add KEY STEP_USER in W2_STEP_STUDENT_COMMENT",
	"ALTER TABLE W2_STEP_STUDENT_COMMENT ADD KEY STEP_USER (STEP_ID,USER_ID)"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - create table W2_STEP_HANDIN_HISTORY to store handin history",
	"CREATE TABLE IF NOT EXISTS W2_STEP_HANDIN_HISTORY (
	     STEP_ID int(11) NOT NULL,
	     USER_ID int(11) NOT NULL,
		 STEP_HANDIN_CODE varchar(255) default NULL,
	     ANSWER text,
	     ANSWER_MARKED text,
	     VERSION int(11) default '1',
	     MARK VARCHAR(16) DEFAULT NULL,
	     DATE_INPUT timestamp NULL default NULL,
	     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
	     UNIQUE KEY STEP_USER_VERSION (STEP_ID,USER_ID,VERSION),
	     KEY STEP_ID (STEP_ID),
	     KEY USER_ID (USER_ID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
	"2013-12-13",
	"Writing 2.0 - sync W2_STEP_STUDENT STEP_USER INFOR to W2_STEP_STUDENT_COMMENT ",
	"UPDATE  W2_STEP_STUDENT s INNER JOIN W2_STEP_STUDENT_COMMENT c ON s.COMMENT_ID = c.COMMENT_ID AND s.COMMENT_ID IS NOT NULL SET c.USER_ID = s.USER_ID, c.STEP_ID = s.STEP_ID"
);
$sql_eClassIP_update[] = array(
	"2013-12-16",
	"Medical Module - NEW Table to store medical meal status",
	"CREATE TABLE IF NOT EXISTS `MEDICAL_MEAL_STATUS` (
	  `StatusID` int(11) NOT NULL auto_increment,
	  `StatusName` varchar(128) default NULL,
	  `StatusCode` varchar(128) default NULL,
	  `Color` varchar(20) default NULL,
	  `IsDefault` tinyint(1) default '0',
	  `RecordStatus` tinyint(1) default '1',
	  `DeletedFlag` tinyint(1) default '0',
	  `DeletedBy` int(11) default NULL,
	  `DeletedDate` datetime default NULL,
	  `DateInput` datetime default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  `InputBy` int(11) default NULL,
	  PRIMARY KEY  (`StatusID`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 
	"
);


$sql_eClassIP_update[] = array(
	"2013-12-16",
	"Medical Module - NEW Table to store medical access group details",
	"CREATE TABLE IF NOT EXISTS `MEDICAL_ACCESS_GROUP` (
		  `GroupID` int(11) NOT NULL auto_increment,
		  `GroupTitle` varchar(128) default NULL,
		  `GroupDescription` mediumtext,
		  `GroupAccessRight` text,
		  `RecordStatus` tinyint(1) default '1',
		  `DateInput` datetime default NULL,
		  `DateModified` datetime default NULL,
		  `LastModifiedBy` int(11) default NULL,
		  PRIMARY KEY  (`GroupID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
	"2013-12-16",
	"Medical Module - NEW Table to store medical access group and user mapping",
	"CREATE TABLE `MEDICAL_ACCESS_GROUP_MEMBER` (
		  `GroupMemberID` int(11) NOT NULL auto_increment,
		  `GroupID` int(11) default '0',
		  `UserID` int(11) default '0',
		  `DateInput` datetime default NULL,
		  PRIMARY KEY  (`GroupMemberID`),
		  UNIQUE KEY `GroupUser` (`GroupID`,`UserID`),
		  KEY `GroupID` (`GroupID`),
		  KEY `UserID` (`UserID`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 
	"
);

$sql_eClassIP_update[] = array(
	"2013-12-17",
	"Account Mgmt & iMail plus - Add field ImapUserLogin to INTRANET_USER",
	"ALTER TABLE INTRANET_USER ADD COLUMN ImapUserLogin varchar(100) DEFAULT NULL AFTER ImapUserEmail"
);

$sql_eClassIP_update[] = array(
	"2013-12-18",
	"Message Center - Add table TEMPSTORE_PUSHMESSAGE_FILE_USER for sending push message by csv",
	"CREATE TABLE IF NOT EXISTS TEMPSTORE_PUSHMESSAGE_FILE_USER (
		 UserLogin varchar(255),
		 ParentID int(11),
		 MessageTitle varchar(255),
		 Message text,
		 UserID int(11)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2013-12-18",
	"Message Center - Add RelatedUserID to table TEMPSTORE_PUSHMESSAGE_FILE_USER",
	"Alter Table TEMPSTORE_PUSHMESSAGE_FILE_USER Add column RelatedUserID int(11) default null"
);


$sql_LIBMS_update[] = array(
	"2014-01-02",
	"LIBMS - longer Book Subtitle",
	"ALTER Table `LIBMS_BOOK` change `BookSubTitle` `BookSubTitle` varchar(128) default NULL;"
);

##########
// 2014-01-07 [Adam] Medical System Student Log
$sql_eClassIP_update[] = array(
	"2014-01-07",
	"Medical Module - Student Log",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_LOG (
     RecordID int(11) NOT NULL auto_increment,
     UserID int(11) NOT NULL,
     RecordTime datetime default NULL,
     BehaviourOne int(11) default NULL,
     BehaviourTwo int(11) default NULL,
     Duration time default NULL,
     Remarks mediumtext,
     PIC varchar(255) default NULL,
     DateInput datetime default NULL,
     InputBy int(11) default NULL,
     DateModified datetime default NULL,
     ModifyBy int(11) default NULL,
     PRIMARY KEY (RecordID),
     KEY UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2014-01-07",
	"Medical Module - Student Log",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_LOG_DOCUMENT (
     RecordID int(11) NOT NULL auto_increment,
     StudentLogID int(11) NOT NULL,
     RenameFile varchar(255) NOT NULL,
     OrgFileName varchar(255) NOT NULL,
     FileType varchar(255) default NULL,
     FileSize int(11) default NULL,
     FolderPath varchar(255) default NULL,
     DateInput datetime default NULL,
     InputBy int(11) default NULL,
     PRIMARY KEY (RecordID),
     UNIQUE KEY RenameFile (RenameFile)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2014-01-07",
	"Medical Module - Student Log",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_LOG_LEV1 (
     Lev1ID int(11) NOT NULL auto_increment,
     Lev1Name varchar(128) default NULL,
     Lev1Code varchar(128) default NULL,
     ItemQty int(8) default '0',
     RecordStatus tinyint(1) default '1',
     DeletedFlag tinyint(1) default '0',
     DeletedBy int(11) default NULL,
     DeletedDate datetime default NULL,
     DateInput datetime default NULL,
     DateModified datetime default NULL,
     LastModifiedBy int(11) default NULL,
     InputBy int(11) default NULL,
     PRIMARY KEY (Lev1ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2014-01-07",
	"Medical Module - Student Log",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_LOG_LEV2 (
     Lev2ID int(11) NOT NULL auto_increment,
     Lev2Name varchar(128) default NULL,
     Lev2Code varchar(128) default NULL,
     Lev1ID int(11) default '0',
     ItemQty int(8) default '0',
     IsDefault tinyint(4) default '0',
     RecordStatus tinyint(1) default '1',
     DeletedFlag tinyint(1) default '0',
     DeletedBy int(11) default NULL,
     DeletedDate datetime default NULL,
     DateInput datetime default NULL,
     DateModified datetime default NULL,
     LastModifiedBy int(11) default NULL,
     InputBy int(11) default NULL,
     PRIMARY KEY (Lev2ID),
     KEY Lev1ID (Lev1ID),
     CONSTRAINT MEDICAL_STUDENT_LOG_LEV2_ibfk_1 FOREIGN KEY (Lev1ID) REFERENCES MEDICAL_STUDENT_LOG_LEV1 (Lev1ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2014-01-07",
	"Medical Module - Student Log",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_LOG_LEV3 (
     Lev3ID int(11) NOT NULL auto_increment,
     Lev3Name varchar(128) default NULL,
     Lev3Code varchar(128) default NULL COMMENT 'Arbitrary value, cannot empty and cannot duplicate is ok',
     Lev2ID int(11) default '0',
     RecordStatus tinyint(1) default '1',
     DeletedFlag tinyint(1) default '0',
     DeletedBy int(11) default NULL,
     DeletedDate datetime default NULL,
     DateInput datetime default NULL,
     DateModified datetime default NULL,
     LastModifiedBy int(11) default NULL,
     InputBy int(11) default NULL,
     PRIMARY KEY (Lev3ID),
     UNIQUE KEY Lev3Code (Lev3Code),
     KEY Lev1ID (Lev2ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2014-01-07",
	"Medical Module - Student Log",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_LOG_LEV4 (
     Lev4ID int(11) NOT NULL auto_increment,
     Lev4Name varchar(128) default NULL,
     Lev4Code varchar(128) default NULL COMMENT 'Arbitrary value, cannot empty and cannot duplicate is ok',
     Lev3ID int(11) default '0',
     RecordStatus tinyint(1) default '1',
     DeletedFlag tinyint(1) default '0',
     DeletedBy int(11) default NULL,
     DeletedDate datetime default NULL,
     DateInput datetime default NULL,
     DateModified datetime default NULL,
     LastModifiedBy int(11) default NULL,
     InputBy int(11) default NULL,
     PRIMARY KEY (Lev4ID),
     UNIQUE KEY Lev4Code (Lev4Code),
     KEY Lev1ID (Lev3ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2014-01-07",
	"Medical Module - Student Log",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_LOG_PARTS (
     RecordID int(11) NOT NULL auto_increment,
     StudentLogID int(11) NOT NULL,
     Level3ID int(11) NOT NULL,
     Level4ID int(11) NOT NULL,
     DateInput datetime default NULL,
     InputBy int(11) default NULL,
     DateModified datetime default NULL,
     ModifyBy int(11) default NULL,
     PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array(
	"2014-01-09",
	"Writing 2.0 - add grade in W2_CONTENT_DATA",
	"ALTER Table W2_CONTENT_DATA ADD COLUMN grade varchar(255) default NULL AFTER category"
);
##########
// 2014-01-13 [Adam] Writing 2.0 Templates

$sql_eClassIP_update[] = array(
	"2014-01-13",
	"Writing 2.0 - Template Style",
	"ALTER TABLE W2_WRITING
	ADD COLUMN TEMPLATE_ID int(8)"
);

$sql_eClassIP_update[] = array(
	"2014-01-13",
	"Writing 2.0 - Template Style",
	"ALTER TABLE W2_WRITING
	ADD COLUMN CLIPART_ID int(8)"
);

$sql_eClassIP_update[] = array(
	"2014-01-15",
	"eAdmission - create table ADMISSION_ATTACHMENT_SETTING",
	"CREATE TABLE IF NOT EXISTS ADMISSION_ATTACHMENT_SETTING (
		RecordID int(11) NOT NULL auto_increment,
		AttachmentName varchar(255) NOT NULL,
		Sequence int(8) DEFAULT NULL,
		DateInput datetime NOT NULL,
		DateModified datetime NOT NULL,
		InputBy int(11) NOT NULL,
		ModifiedBy int(11) NOT NULL,
		PRIMARY KEY(RecordID),
		INDEX AttachmentName(AttachmentName) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 "
);
$sql_eClassIP_update[] = array(
	"2014-01-16",
	"Writing 2.0 - modify step3content in W2_CONTENT_DATA",
	"ALTER TABLE W2_CONTENT_DATA MODIFY COLUMN step3Content longblob"
);
$sql_eClassIP_update[] = array(
	"2014-01-20",
	"Writing 2.0 - extend schoolCode length in W2_CONTENT_DATA",
	"ALTER TABLE W2_CONTENT_DATA MODIFY COLUMN schoolCode VARCHAR(64)"
);
$sql_eClassIP_update[] = array(
	"2014-01-21",
	"Writing 2.0 - create table TOOLS_POWERBOARD",
	"CREATE TABLE IF NOT EXISTS TOOLS_POWERBOARD(
     PowerBoardID int(8) NOT NULL auto_increment,
     DataContent longblob,
     UserID int(8) default NULL,
     ModuleCode varchar(255) default NULL,
     ImageData longblob,
     InputDate datetime default NULL,
     ModifiedDate datetime default NULL,
     PRIMARY KEY (PowerBoardID)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8"
);
$sql_eClassIP_update[] = array(
	"2014-01-21",
	"Writing 2.0 - add conceptType in W2_CONTENT_DATA",
	"ALTER TABLE W2_CONTENT_DATA ADD COLUMN conceptType varchar(32) default 'powerconcept' AFTER datePublished"
);
$sql_eClassIP_update[] = array(
	"2014-01-21",
	"Writing 2.0 - add powerBoardID in W2_CONTENT_DATA",
	"ALTER TABLE W2_CONTENT_DATA ADD COLUMN powerBoardID int(8) default NULL AFTER powerConceptID"
);
$sql_eClassIP_update[] = array(
	"2014-01-21",
	"Writing 2.0 - add powerBoardID in W2_CONTENT_DATA",
	"ALTER TABLE W2_CONTENT_DATA ADD COLUMN attachment mediumtext default NULL AFTER powerBoardID"
);
$sql_eClassIP_update[] = array(
	"2014-01-21",
	"Writing 2.0 - add grade in W2_CONTENT_DATA",
	"ALTER Table W2_CONTENT_DATA ADD COLUMN grade varchar(255) default NULL AFTER category"
);
$sql_eClassIP_update[] = array(
	"2014-01-21",
	"Writing 2.0 - add grade in W2_CONTENT_DATA",
	"ALTER Table W2_CONTENT_DATA ADD COLUMN teacherAttachment mediumtext AFTER resource"
);
$sql_eClassIP_update[] = array(
	"2014-01-24",
	"eAdmission - ADMISSION_STU_INFO - Add ContactPerson, ContactPersonRelationship",
	"ALTER TABLE ADMISSION_STU_INFO 
		ADD COLUMN ContactPerson VARCHAR(255) DEFAULT NULL AFTER HomeTelNo, 
		ADD COLUMN ContactPersonRelationship VARCHAR(255) DEFAULT NULL AFTER ContactPerson"
);

$sql_eClassIP_update[] = array(
	"2014-01-24",
	"elibrary book - add Edition field",
	"ALTER TABLE INTRANET_ELIB_BOOK ADD Edition varchar(5) default NULL"
);

$sql_eClassIP_update[] = array(
	"2014-01-24",
	"elibrary book - modify Level field",
	"ALTER TABLE INTRANET_ELIB_BOOK MODIFY Level varchar(30) default NULL"
);

## 2014-01-27 [Cameron]
# medical table update 
$sql_eClassIP_update[] = array(
	"2014-01-27",
	"Medical Module - MEDICAL_SLEEP_STATUS",
	"CREATE TABLE IF NOT EXISTS MEDICAL_SLEEP_STATUS (
		StatusID int(11) NOT NULL auto_increment,
		StatusName varchar(128) default NULL,
		StatusCode varchar(128) default NULL,
		Color varchar(20) default NULL,
		RecordStatus tinyint(1) default '1',
		DeletedFlag tinyint(1) default '0',
		DeletedBy int(11) default NULL,
		DeletedDate datetime default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		LastModifiedBy int(11) default NULL,
		InputBy int(11) default NULL,
		ItemQty int(8) default '0',
		PRIMARY KEY (StatusID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-01-27",
	"Medical Module - MEDICAL_SLEEP_STATUS_REASON",
	"CREATE TABLE IF NOT EXISTS MEDICAL_SLEEP_STATUS_REASON (
		ReasonID int(11) NOT NULL auto_increment,
		StatusID int(11) NOT NULL,
		ReasonName varchar(128) default NULL,
		ReasonCode varchar(128) default NULL,
		IsDefault tinyint(1) default '0',
		RecordStatus tinyint(1) default '1',
		DeletedFlag tinyint(1) default '0',
		DeletedBy int(11) default NULL,
		DeletedDate datetime default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		LastModifiedBy int(11) default NULL,
		InputBy int(11) default NULL,
		PRIMARY KEY (ReasonID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-01-27",
	"Medical Module - MEDICAL_BOWEL_STATUS",
	"CREATE TABLE IF NOT EXISTS MEDICAL_BOWEL_STATUS (
		StatusID int(11) NOT NULL auto_increment,
		StatusName varchar(128) default NULL,
		StatusCode varchar(128) default NULL,
		BarCode varchar(128) default NULL,
		Color varchar(20) default NULL,
		IsDefault tinyint(1) default '0',
		RecordStatus tinyint(1) default '1',
		DeletedFlag tinyint(1) default '0',
		DeletedBy int(11) default NULL,
		DeletedDate datetime default NULL,
		DateInput datetime default NULL,
		DateModified datetime default NULL,
		LastModifiedBy int(11) default NULL,
		InputBy int(11) default NULL,
		PRIMARY KEY (StatusID)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-01-27",
	"Medical Module - MEDICAL_STUDENT_SLEEP",
	"CREATE TABLE IF NOT EXISTS MEDICAL_STUDENT_SLEEP (
	     RecordID int(11) NOT NULL auto_increment,
	     UserID int(11) NOT NULL,
	     RecordTime datetime default NULL,
	     SleepID int(11) default NULL,
	     Frequency int(11) default NULL,
	     ReasonID int(11) default NULL,
	     Remarks mediumtext,
	     DateInput datetime default NULL,
	     InputBy int(11) default NULL,
	     DateModified datetime default NULL,
	     ModifyBy int(11) default NULL,
	     PRIMARY KEY (RecordID),
	     KEY UserID (UserID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-01-28",
	"eAdmission - ADMISSION_STU_INFO - Add LangSpokenAtHome",
	"ALTER TABLE ADMISSION_STU_INFO 
		ADD COLUMN LangSpokenAtHome VARCHAR(255) DEFAULT NULL AFTER LastSchoolLevel"
);

$sql_eClassIP_update[] = array(
	"2014-01-28",
	"eAdmission - ADMISSION_OTHERS_INFO - Add ChildDescription, ChildHealth, YourWish",
	"ALTER TABLE ADMISSION_OTHERS_INFO 
		ADD COLUMN ChildDescription VARCHAR(255) DEFAULT NULL AFTER KnowUsByOther,
		ADD COLUMN ChildHealth VARCHAR(255) DEFAULT NULL AFTER ChildDescription,
		ADD COLUMN YourWish VARCHAR(255) DEFAULT NULL AFTER ChildHealth"
);

$sql_eClassIP_update[] = array(
	"2014-01-28",
	"eAdmission - ADMISSION_PG_INFO - Add NativeLanguage",
	"ALTER TABLE ADMISSION_PG_INFO 
		ADD COLUMN NativeLanguage VARCHAR(255) DEFAULT NULL AFTER Mobile"
);

$sql_eClassIP_update[] = array(
	"2014-02-11",
	"eAttendance - create table CARD_STUDENT_APPLY_LEAVE_RECORD to store apply leave records",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_APPLY_LEAVE_RECORD (
		RecordID int(11) NOT NULL auto_increment,
		StudentID int(8) NOT NULL,
		StartDate date NOT NULL,
		EndDate date NOT NULL,
		PeriodType varchar(8) NOT NULL,
		Reason text DEFAULT NULL,
		ApprovalStatus tinyint(2) NOT NULL,
		DocumentStatus tinyint(2) NOT NULL,
		InputDate datetime NOT NULL,
		InputBy int(8) NOT NULL,
		ModifiedDate datetime NOT NULL,
		ModifiedBy int(8) NOT NULL,
		PRIMARY KEY (RecordID),
		INDEX StudentID (StudentID),
		INDEX ApprovalStatus (ApprovalStatus),
		INDEX DocumentStatus (DocumentStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-02-11",
	"eAttendance - create table CARD_STUDENT_APPLY_LEAVE_FILE to store apply leave document",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_APPLY_LEAVE_FILE (
		FileID int(11) NOT NULL auto_increment,
		LeaveRecordID int(8) NOT NULL,
		FilePath varchar(255) NOT NULL,
		FileName varchar(255) NOT NULL,
		InputDate datetime NOT NULL,
		InputBy int(8) NOT NULL,
		ModifiedDate datetime NOT NULL,
		ModifiedBy int(8) NOT NULL,
		PRIMARY KEY (FileID),
		INDEX LeaveRecordID (LeaveRecordID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-02-11",
	"eAttendance - create table CARD_STUDENT_APPLY_LEAVE_FILE to store apply leave settings",
	"CREATE TABLE IF NOT EXISTS CARD_STUDENT_APPLY_LEAVE_SETTING (
		SettingID int(11) NOT NULL auto_increment,
		SettingKey varchar(64) NOT NULL,
		SettingValue text NOT NULL,
		InputDate datetime NOT NULL,
		InputBy int(8) NOT NULL,
		ModifiedDate datetime NOT NULL,
		ModifiedBy int(8) NOT NULL,
		PRIMARY KEY (SettingID),
		INDEX SettingKey (SettingKey)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-02-12",
	"ePOS - Drop ItemName unique key from table POS_ITEM",
	"ALTER TABLE POS_ITEM DROP KEY ItemName"
);

$sql_eClassIP_update[] = array(
	"2014-02-12",
	"ePOS - Add unique key (CategoryID,ItemName) to table POS_ITEM",
	"ALTER TABLE POS_ITEM ADD UNIQUE KEY CategoryIDItemName(CategoryID,ItemName)"
);

$sql_eClassIP_update[] = array(
	"2014-02-13",
	"eAttendance - alter table CARD_STUDENT_APPLY_LEAVE_RECORD to store record delete status",
	"ALTER TABLE CARD_STUDENT_APPLY_LEAVE_RECORD 	ADD COLUMN IsDeleted tinyint(2) default '0' AFTER DocumentStatus,
													ADD INDEX IsDeleted (IsDeleted)"
);

$sql_eClassIP_update[] = array(
	"2014-02-18",
	"eAttendance - alter table CARD_STUDENT_APPLY_LEAVE_SETTING to add unique key for SettingKey",
	"ALTER TABLE CARD_STUDENT_APPLY_LEAVE_SETTING 	Drop KEY SettingKey,
													ADD UNIQUE KEY SettingKeyUnique (SettingKey)"
);


$sql_LIBMS_update[] = array(
	"2014-02-19",
	"LIBMS - Create patron group period table",
	"CREATE TABLE IF NOT EXISTS `LIBMS_GROUP_PERIOD` (
	  `RecordID` int(11) NOT NULL auto_increment,
	  `GroupID` int(11) NOT NULL,
	  `StartDate` date default NULL,
	  `EndDate` date default NULL,
	  `DateModified` datetime default NULL,
	  `LastModifiedBy` int(11) default NULL,
	  PRIMARY KEY (RecordID),
	  KEY GroupID (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$sql_eClassIP_update[] = array(
	"2014-02-19",
	"eAttendance - alter table CARD_STUDENT_APPLY_LEAVE_RECORD to store approval user data",
	"ALTER TABLE CARD_STUDENT_APPLY_LEAVE_RECORD 	ADD COLUMN ApprovalStatusChangedBy int(8) default NULL AFTER ApprovalStatus,
													ADD COLUMN DocumentStatusChangedBy int(8) default NULL AFTER DocumentStatus"
);

$sql_eClassIP_update[] = array(
	"2014-02-19",
	"eAttendance - alter table CARD_STUDENT_APPLY_LEAVE_RECORD to store StartDateType and EndDateType",
	"ALTER TABLE CARD_STUDENT_APPLY_LEAVE_RECORD 	ADD COLUMN StartDateType varchar(8) NOT NULL AFTER StartDate,
													ADD COLUMN EndDateType varchar(8) NOT NULL AFTER EndDate"
);

$sql_eClassIP_update[] = array(
	"2014-02-19",
	"eAttendance - alter table CARD_STUDENT_APPLY_LEAVE_RECORD to store Duration",
	"ALTER TABLE CARD_STUDENT_APPLY_LEAVE_RECORD 	ADD COLUMN Duration varchar(64) DEFAULT NULL AFTER PeriodType"
);
$sql_eClassIP_update[] = array(
	"2014-02-21",
	"Writing 2.0 - extend topicName field in W2_CONTENT_DATA",
	"ALTER Table W2_CONTENT_DATA MODIFY COLUMN topicName VARCHAR(255)"
);

$sql_eClassIP_update[] = array(
	"2014-02-21",
	"eDiscipline - Add a field to classify the detention is added by auto assign or manually (NULL for old records)",
	"ALTER Table DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN isAutoAssign tinyint(1) default NULL"
);

$sql_eClassIP_update[] = array(
	"2014-02-21",
	"eDiscipline - update default to 0 (so that can classify with  NULL / 0 / 1)",
	"ALTER Table DISCIPLINE_DETENTION_STUDENT_SESSION ALTER isAutoAssign set default 0"
);

$sql_eClassIP_update[] = array(
	"2014-02-21",
	"eDiscipline - add datetime for auto assign",
	"ALTER Table DISCIPLINE_DETENTION_STUDENT_SESSION ADD COLUMN AutoAssignDateTime datetime default NULL"
);

$sql_eClassIP_update[] = array
(
	"2014-02-24",
	" eLibrary - add field IsRightToLeft in INTRANET_ELIB_BOOK ",
	"ALTER TABLE INTRANET_ELIB_BOOK ADD IsRightToLeft char(2) default '0'"
);


$sql_eClassIP_update[] = array(
	"2014-03-03",
	"INTRANET_USER password update time",
	"ALTER Table INTRANET_USER ADD COLUMN LastModifiedPwd datetime default NULL after HashedPass"
);


$sql_LIBMS_update[] = array(
	"2014-03-10",
	"LIBMS",
	"ALTER TABLE LIBMS_BOOK change Publisher Publisher varchar(128) default NULL "
);


$sql_LIBMS_update[] = array(
	"2014-03-10",
	"LIBMS",
	"ALTER TABLE LIBMS_BOOK change RemarkInternal RemarkInternal varchar(255) default NULL "
);


$sql_LIBMS_update[] = array(
	"2014-03-10",
	"LIBMS",
	"ALTER TABLE LIBMS_BOOK change RemarkToUser RemarkToUser varchar(255) default NULL "
);

$sql_eClassIP_update[] = array
(
	"2014-03-14",
	"KIS eAdmission - create table ADMISSION_EMAIL_RECORD",
	"CREATE TABLE IF NOT EXISTS ADMISSION_EMAIL_RECORD (
		RecordID int(11) NOT NULL auto_increment,
		SchoolYearID int(11) NOT NULL,
		YearID int(11) NOT NULL,
		Subject text,
		Message mediumtext,
		IsAcknowledge varchar(1),
		AcknowledgeMessage text,
		InputBy int(11) NOT NULL,
		DateInput datetime NOT NULL,
		LastSentBy int(11) NOT NULL,
		LastSentDate datetime NOT NULL,
		PRIMARY KEY (RecordID),
		INDEX ISchoolYearID(SchoolYearID),
		INDEX IYearID(YearID)  
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-03-14",
	"KIS eAdmission - create table ADMISSION_EMAIL_RECEIVER",
	"CREATE TABLE IF NOT EXISTS ADMISSION_EMAIL_RECEIVER (
		ReceiverID int(11) NOT NULL auto_increment,
		EmailRecordID int(11) NOT NULL,
		UserID int(11) NOT NULL,
		SentEmail varchar(256) NOT NULL, 
		SentStatus varchar(1) DEFAULT '1',
		AcknowledgeDate datetime DEFAULT NULL,
		InputBy int(11) NOT NULL,
		DateInput datetime NOT NULL,
		ModifiedBy int(11) NOT NULL,
		DateModified datetime NOT NULL,
		PRIMARY KEY (ReceiverID),
		INDEX IEmailRecordID(EmailRecordID),
		INDEX IUserID(UserID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_LIBMS_update[] = array(
	"2014-03-18",
	"LIBMS - enlarge the storage of Subject in table LIBMS_BOOK",
	"ALTER TABLE LIBMS_BOOK change Subject Subject varchar(64) default NULL"
);

$sql_eClassIP_update[] = array
(
	"2014-03-18",
	"Campusmail - old schema update to avoid version upgrade failure - add column MessageEncoding to INTRANET_CAMPUSMAIL",
	"ALTER TABLE INTRANET_CAMPUSMAIL ADD COLUMN MessageEncoding varchar(60) AFTER Message"
);

$sql_eClassIP_update[] = array
(
	"2014-03-18",
	"Campusmail - old schema update to avoid version upgrade failure - add column isHTML to INTRANET_CAMPUSMAIL",
	"ALTER TABLE INTRANET_CAMPUSMAIL ADD COLUMN isHTML int(11) AFTER MessageEncoding;"
);

$sql_eClassIP_update[] = array
(
	"2014-03-18",
	"Student Attendance - Add column AbsentSession to CARD_STUDENT_STATUS_SESSION_COUNT",
	"ALTER TABLE CARD_STUDENT_STATUS_SESSION_COUNT ADD COLUMN AbsentSession float DEFAULT NULL AFTER OfficalLeaveSession"
);

$sql_eClassIP_update[] = array
(
	"2014-03-18",
	"Student Attendance - Add column WaiveAbsent to CARD_STUDENT_PROFILE_RECORD_REASON",
	"ALTER TABLE CARD_STUDENT_PROFILE_RECORD_REASON ADD COLUMN WaiveAbsent varchar(1) DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2014-03-24",
	"ePayment - Add column PaymentMethod to PAYMENT_PAYMENT_ITEMSTUDENT",
	"ALTER TABLE PAYMENT_PAYMENT_ITEMSTUDENT ADD COLUMN PaymentMethod varchar(1) DEFAULT NULL AFTER Remark"
);

$sql_eClass_update[] = array(
"2014-03-26",
"add TermAssessment to ASSESSMENT_STUDENT_SUBJECT_RECORD for iPortfolio" ,
"ALTER TABLE ASSESSMENT_STUDENT_SUBJECT_RECORD
	ADD COLUMN TermAssessment varchar(16) AFTER YearTermID"
);

$sql_eClassIP_update[] = array
(
	"2014-03-28",
	"Medical Module - Create table MEDICAL_DEFAULT_REMARKS",
	"CREATE TABLE IF NOT EXISTS MEDICAL_DEFAULT_REMARKS (
	     RemarksID int(11) NOT NULL auto_increment,
	     RemarksType varchar(128) NOT NULL,
	     Remarks text,
	     DateInput datetime default NULL,
	     InputBy int(11) default NULL,
	     DateModified datetime default NULL,
	     LastModifiedBy int(11) default NULL,
	     PRIMARY KEY (RemarksID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-03-28",
	"Medical Module - Create table MEDICAL_MESSAGE_SEND",
	"CREATE TABLE MEDICAL_MESSAGE_SEND (
	     SendID int(11) NOT NULL auto_increment,
	     Message text,
	     Sender int(11) NOT NULL,
	     ReceiverList text,
	     SendTime datetime default NULL,
	     PRIMARY KEY (SendID),
	     KEY Sender (Sender)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-03-28",
	"Medical Module - Create table MEDICAL_MESSAGE_RECEIVE",
	"CREATE TABLE IF NOT EXISTS MEDICAL_MESSAGE_RECEIVE (
	     ReceiveID int(11) NOT NULL auto_increment,
	     Message text,
	     Sender int(11) NOT NULL,
	     Receiver int(11) NOT NULL,
	     ReceiveTime datetime default NULL,
	     HasRead tinyint(1) default '0',
	     PRIMARY KEY (ReceiveID),
	     KEY Receiver (Receiver)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-03-28",
	"Medical Module - Create table MEDICAL_DEFAULT_MAIL",
	"CREATE TABLE IF NOT EXISTS MEDICAL_DEFAULT_MAIL (
	     DefaultID int(11) NOT NULL auto_increment,
	     Title text NOT NULL,
	     Message text NOT NULL,
	     RecordStatus tinyint(1) default '1',
	     DateInput datetime default NULL,
	     InputBy int(11) default NULL,
	     DateModified datetime default NULL,
	     ModifyBy int(11) default NULL,
	     PRIMARY KEY (DefaultID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-03-28",
	"ePayment - Add column ModifiedBy to PAYMENT_PRINTING_QUOTA",
	"ALTER TABLE PAYMENT_PRINTING_QUOTA ADD COLUMN ModifiedBy int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2014-03-28",
	"ePayment - Add column ModifiedBy to PAYMENT_PRINTING_QUOTA_CHANGE_LOG",
	"ALTER TABLE PAYMENT_PRINTING_QUOTA_CHANGE_LOG ADD COLUMN ModifiedBy int(11) DEFAULT NULL"
);

$sql_LIBMS_update[] = array(
	"2014-03-28",
	"LIBMS - the temp table for import Marc21",
	"CREATE TABLE LIBMS_MARC_TMP_2 (
     id int(15) NOT NULL auto_increment,
     BookTitle varchar(255) NOT NULL default '',
     BookSubTitle varchar(255) default NULL,
     CallNum varchar(255) default NULL,
     CallNum2 varchar(255) default NULL,
     ISBN varchar(255) default NULL,
     ISBN2 varchar(255) default NULL,
     Language varchar(255) default NULL,
     Country varchar(255) default NULL,
     Introduction text,
     Edition varchar(255) default NULL,
     PublishYear varchar(255) default NULL,
     Publisher varchar(255) default NULL,
     PublishPlace varchar(255) default NULL,
     Series varchar(255) default NULL,
     SeriesNum varchar(255) default NULL,
     ResponsibilityCode1 varchar(255) default NULL,
     ResponsibilityBy1 varchar(255) default NULL,
     ResponsibilityCode2 varchar(255) default NULL,
     ResponsibilityBy2 varchar(255) default NULL,
     ResponsibilityCode3 varchar(255) default NULL,
     ResponsibilityBy3 varchar(255) default NULL,
     NoOfPage varchar(255) default NULL,
     BookCategory varchar(255) default NULL,
     BookCirclation varchar(255) default NULL,
     BookResources varchar(255) default NULL,
     Subject varchar(255) default NULL,
     BookInternalremark varchar(255) default NULL,
     OpenBorrow varchar(255) default NULL,
     OpenReservation varchar(255) default NULL,
     RemarkToUser varchar(255) default NULL,
     Tags varchar(255) default NULL,
     URL varchar(255) default NULL,
     ACNO varchar(255) default NULL,
     barcode varchar(255) default NULL,
     BookLocation varchar(255) default NULL,
     AccountDate varchar(255) default NULL,
     PurchaseDate varchar(255) default NULL,
     PurchasePrice varchar(255) default NULL,
     ListPrice varchar(255) default NULL,
     Discount varchar(255) default NULL,
     Distributor varchar(255) default NULL,
     PurchaseByDepartment varchar(255) default NULL,
     PurchaseNote varchar(255) default NULL,
     InvoiceNumber varchar(255) default NULL,
     DateModified datetime NOT NULL,
     LastModifiedBy int(11) NOT NULL,
     RecordStatus varchar(16) default NULL,
     UNIQUE KEY id (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-03-31",
	"eAdmission - ADMISSION_PG_INFO - Add HKID, Email, MaritalStatus",
	"ALTER TABLE ADMISSION_PG_INFO 
		ADD COLUMN HKID VARCHAR(255) DEFAULT NULL AFTER NativeLanguage,
		ADD COLUMN Email VARCHAR(255) DEFAULT NULL AFTER NativeLanguage,
		ADD COLUMN MaritalStatus char(1) DEFAULT NULL AFTER NativeLanguage"
);

$sql_eClassIP_update[] = array
(
	"2014-04-03",
	"ePayment - Add column Remark to PAYMENT_CREDIT_TRANSACTION",
	"ALTER TABLE PAYMENT_CREDIT_TRANSACTION ADD COLUMN Remark varchar(512) DEFAULT NULL"
);

$sql_LIBMS_update[] = array
(
	"2014-04-07",
	"LIBMS",
	"ALTER TABLE LIBMS_STOCKTAKE_SCHEME 
		ADD COLUMN RecordStartDate date default NULL after EndDate, 
		ADD COLUMN RecordEndDate date default NULL after RecordStartDate"
);

$sql_eClassIP_update[] = array
(
	"2014-04-07",
	"Message Center - add column DeviceOS and DeviceModel in table APP_USER_DEVICE to store more device info",
	"ALTER TABLE APP_USER_DEVICE 	add column DeviceOS varchar(64) NOT NULL default 'Android',
									add column DeviceModel varchar(255) default NULL
	"
);

$sql_eClassIP_update[] = array
(
	"2014-04-10",
	"ePayment - Add columns UsedQuota and TotalQuota to PAYMENT_PRINTING_QUOTA_CHANGE_LOG",
	"ALTER TABLE PAYMENT_PRINTING_QUOTA_CHANGE_LOG ADD COLUMN TotalQuota int(11) DEFAULT NULL AFTER QuotaAfter, ADD COLUMN UsedQuota int(11) DEFAULT NULL AFTER QuotaAfter"
);

$sql_eClassIP_update[] = array
(
	"2014-04-11",
	"eClassApp - create table APP_LOGIN_LOG",
	"CREATE TABLE IF NOT EXISTS APP_LOGIN_LOG  (
	    UserID int(11) NOT NULL default '0',
	    LastLoginTime datetime default NULL,
	    KEY UserID (UserID)
	)"
);

$sql_eClassIP_update[] = array
(
	"2014-04-11",
	"Message Center - add column ServiceProvider in table INTRANET_APP_NOTIFY_MESSAGE_REFERENCE",
	"ALTER TABLE INTRANET_APP_NOTIFY_MESSAGE_REFERENCE add column ServiceProvider varchar(64) NOT NULL"
);

$sql_eClassIP_update[] = array(
	"2014-04-15",
	"Medical Module - MEDICAL_SLEEP_STATUS add column isDefault",
	"ALTER TABLE MEDICAL_SLEEP_STATUS ADD IsDefault tinyint(1) default '0';"
);

$sql_eClassIP_update[] = array
(
	"2014-04-22",
	"Writing 2.0 - Add Table W2_CONTENT_REF_ITEM ",
	"CREATE TABLE IF NOT EXISTS W2_CONTENT_REF_ITEM (
	     REF_ITEM_ID int(11) NOT NULL auto_increment,
	      cid int(8) NOT NULL,
	     REF_CATEGORY_CODE varchar(64) default NULL,
	     INFOBOX_CODE varchar(255) default NULL,
	     TITLE varchar(255) default NULL,
	     DATE_INPUT timestamp NULL default NULL,
	     INPUT_BY int(8) default NULL,
	     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
	     MODIFIED_BY int(8) default NULL,
	     DELETE_STATUS tinyint(3) default '1',
	     DELETED_BY int(8) default NULL,
	     PRIMARY KEY (REF_ITEM_ID),
	     KEY REF_CATEGORY_CODE (REF_CATEGORY_CODE),
	     KEY CONTENT_ID (cid)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array
(
	"2014-04-22",
	"Writing 2.0 - Add Table W2_CONTENT_REF_CATEGORY ",
	"CREATE TABLE W2_CONTENT_REF_CATEGORY (
	     REF_CATEGORY_ID int(11) NOT NULL auto_increment,
	     cid int(8) NOT NULL,
	     REF_CATEGORY_CODE varchar(64) default NULL,
	     INFOBOX_CODE varchar(255) default NULL,
	     TITLE varchar(255) default NULL,
	     CSS_SET int(3) default '0',
	     DATE_INPUT timestamp NULL default NULL,
	     INPUT_BY int(8) default NULL,
	     DATE_MODIFIED timestamp NOT NULL default CURRENT_TIMESTAMP,
	     MODIFIED_BY int(8) default NULL,
	     DELETE_STATUS tinyint(3) default '1',
	     DELETED_BY int(8) default NULL,
	     PRIMARY KEY (REF_CATEGORY_ID),
	     KEY REF_CATEGORY_CODE (REF_CATEGORY_CODE),
	     KEY ActiveCategory (INFOBOX_CODE,DELETE_STATUS),
	     KEY CONTENT_ID (cid)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);
$sql_eClassIP_update[] = array
(
	"2014-04-23",
	"Medical Module - Create table MEDICAL_GROUP_NAME_FILTER",
	"CREATE TABLE IF NOT EXISTS MEDICAL_GROUP_NAME_FILTER (
	     GroupID int(11) NOT NULL auto_increment,
	     CategoryName varchar(128) NOT NULL,
	     DeletedFlag tinyint(1) default '0',
	     DeletedBy int(11) default NULL,
	     DeletedDate datetime default NULL,
	     DateInput datetime default NULL,
	     DateModified datetime default NULL,
	     LastModifiedBy int(11) default NULL,
	     InputBy int(11) default NULL,
	     PRIMARY KEY (GroupID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Create table INTRANET_ENROL_ACTIVITY_NATURE",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_ACTIVITY_NATURE (
		NatureID int(11) NOT NULL auto_increment,
		Nature varchar(255) DEFAULT NULL,
		OleCategoryID int(11) DEFAULT NULL,
		DisplayOrder int(2) DEFAULT NULL,
		DateInput datetime DEFAULT NULL,
		DateModified datetime DEFAULT NULL,
		PRIMARY KEY (NatureID)  
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Create table INTRANET_ENROL_EVENT_NATURE",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENT_NATURE (
		EnrolEventID int(11) NOT NULL,
		NatureID int(11) NOT NULL,
		UNIQUE KEY CompositeKey(EnrolEventID,NatureID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Create table INTRANET_ENROL_EVENT_CATEGORY",
	"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENT_CATEGORY (
		EnrolEventID int(11) NOT NULL,
		CategoryID int(11) NOT NULL,
		UNIQUE KEY CompositeKey(EnrolEventID,CategoryID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Create table INTRANET_EBOOKING_ENROL_EVENT_RELATION",
	"CREATE TABLE IF NOT EXISTS INTRANET_EBOOKING_ENROL_EVENT_RELATION (
		BookingID int(11) NOT NULL,
		EnrolEventID int(11) NOT NULL,
		UNIQUE KEY BookingID_EnrolEventID_UKey(BookingID,EnrolEventID) 
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Add column IsVolunteerService to INTRANET_ENROL_EVENTINFO",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD COLUMN IsVolunteerService tinyint DEFAULT 0"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Add column VolunteerServiceHour to INTRANET_ENROL_EVENTINFO",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD COLUMN VolunteerServiceHour float DEFAULT 0"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Add column ActivityInLocation to INTRANET_ENROL_EVENTINFO",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD COLUMN ActivityInLocation varchar(255) DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Add column ActivityExtLocation to INTRANET_ENROL_EVENTINFO",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD COLUMN ActivityExtLocation varchar(255) DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Add column BookingLocationID to INTRANET_ENROL_EVENTINFO",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD COLUMN BookingLocationID int(11) DEFAULT NULL"
);

if($sys_custom['eEnrolment']['TWGHCYMA'])
{
	$sql_eClassIP_update[] = array
	(
		"2014-04-24",
		"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - patch activity single category to multiple categories",
		"INSERT IGNORE INTO INTRANET_ENROL_EVENT_CATEGORY (EnrolEventID,CategoryID) SELECT EnrolEventID,EventCategory as CategoryID FROM INTRANET_ENROL_EVENTINFO"
	);
}

$sql_eClassIP_update[] = array
(
	"2014-04-24",
	"eClassApp - add table APP_ACCESS_RIGHT to store the access right data of eClass App",
	"CREATE TABLE IF NOT EXISTS APP_ACCESS_RIGHT (
		RecordID int(11) NOT NULL auto_increment,
		AppType varchar(16) NOT NULL,
		Module varchar(32) NOT NULL,
		YearID int(8) NOT NULL,
		RecordStatus tinyint(2) default 1 NOT NULL,
		InputDate datetime NOT NULL,
		InputBy int(8) NOT NULL,
		ModifiedDate datetime NOT NULL,
		ModifiedBy int(8) NOT NULL,
		PRIMARY KEY (RecordID),
		UNIQUE KEY AppFormModule (AppType, YearID, Module)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
	"2014-04-25",
	"eEnrolment - Add fields for customization to store Activity Gathering and Dismiss Location",
	"Alter Table INTRANET_ENROL_EVENTINFO 	Add GatheringLocation varchar(128) default NULL,
											Add DismissLocation varchar(128) default NULL
	"
);

$sql_eClassIP_update[] = array(
	"2014-04-29",
	"eAdmission - ADMISSION_PG_INFO - Add lsSingleParents, LiveWithChild, HasFullTimeJob",
	"ALTER TABLE ADMISSION_PG_INFO 
		ADD COLUMN HasFullTimeJob char(1) DEFAULT NULL AFTER HKID,
		ADD COLUMN IsLiveWithChild char(1) DEFAULT NULL AFTER HKID,
		ADD COLUMN lsSingleParents char(1) DEFAULT NULL AFTER HKID"
);

$sql_eClassIP_update[] = array
(
	"2014-05-02",
	"eClassApp - add table APP_SCHOOL_IMAGE to store the school image data of eClass App",
	"CREATE TABLE IF NOT EXISTS APP_SCHOOL_IMAGE (
		RecordID int(11) NOT NULL auto_increment,
		AppType varchar(16) NOT NULL,
		ImageType varchar(32) NOT NULL,
		ImagePath varchar(255) NOT NULL,
		RecordStatus tinyint(2) default 1 NOT NULL,
		InputDate datetime NOT NULL,
		InputBy int(8) NOT NULL,
		ModifiedDate datetime NOT NULL,
		ModifiedBy int(8) NOT NULL,
		PRIMARY KEY (RecordID),
		UNIQUE KEY AppActiveImage (AppType, ImageType, RecordStatus)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
	"2014-05-02",
	"Student Registry - UCCKE add field",
	"ALTER TABLE STUDENT_REGISTRY_BROSIS ADD COLUMN StudentName varchar(255) DEFAULT NULL"
);
/*
$sql_eClassIP_update[] = array
(
	"2014-05-02",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Add column RelatedGroupID to INTRANET_ENROL_EVENTINFO",
	"ALTER TABLE INTRANET_ENROL_EVENTINFO ADD COLUMN RelatedGroupID int(11) DEFAULT NULL"
);
*/
$sql_eClassIP_update[] = array
(
	"2014-05-02",
	"eEnrolment Customization for TWGHs C Y MA MEMORIAL COLLEGE - Add column RelatedGroupID to INTRANET_ENROL_GROUPINFO",
	"ALTER TABLE INTRANET_ENROL_GROUPINFO ADD COLUMN RelatedGroupID int(11) DEFAULT NULL"
);

$sql_eClassIP_update[] = array
(
	"2014-05-13",
	"Document Routing - create table INTRANET_DR_FILE_STAR for storing user starred feedback files",
	"CREATE TABLE IF NOT EXISTS INTRANET_DR_FILE_STAR (
	  FileID int(11) NOT NULL,
	  UserID int(11) NOT NULL,
	  DateInput datetime NOT NULL,
	  UNIQUE KEY StarIndex (UserID,FileID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

?>