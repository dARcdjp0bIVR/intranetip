<?php
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/
$Lang['PowerPortfolio']['Title'] = "Power Portfolio for KIS";

$Lang['PowerPortfolio']['General']['NoRecord'] = 'No Record';
$Lang['PowerPortfolio']['General']['PleaseConfirmChanges'] = 'Please caution updated settings might affect other functions of eReportCard (Kindergarten).';
$Lang['PowerPortfolio']['General']['PleaseConfirmScoreUpdate'] = 'Existing input data will be overwritten.';
$Lang['PowerPortfolio']['General']['Level'] = "<!--level_num--> layers";
$Lang['PowerPortfolio']['General']['Max'] = "Max";
$Lang['PowerPortfolio']['General']['MaxAverage'] = "Max. Average";
$Lang['PowerPortfolio']['General']['Min'] = "Min";
$Lang['PowerPortfolio']['General']['GradingDetails'] = "Grade Description";
$Lang['PowerPortfolio']['General']['LastModifiedDate'] = "Last Modified";

$Lang['PowerPortfolio']['Management']['DataTransition']['Title'] = "Data Handling";
$Lang['PowerPortfolio']['Management']['DataTransition']['Transition'] = "Transition";
$Lang['PowerPortfolio']['Management']['DataTransition']['ActiveAcademicYear'] = "Active Academic Year";
$Lang['PowerPortfolio']['Management']['DataTransition']['CurrentAcademicYear'] = "Current Academic Year";
$Lang['PowerPortfolio']['Management']['DataTransition']['NewAcademicYear'] = "New academic year";
$Lang['PowerPortfolio']['Management']['DataTransition']['OtherAcademicYears'] = "Other academic years";
$Lang['PowerPortfolio']['Management']['DataTransition']['DataTransitionWarning1'] = "Please proceed this action before:<br />- start using Power Portfolio for a new academic year, or,<br />- restore Power Portfolio to use the data of previous academic years";
$Lang['PowerPortfolio']['Management']['DataTransition']['DataTransitionWarning2'] = "The data from the current active academic year will be preserved.";
$Lang['PowerPortfolio']['Management']['DataTransition']['ConfirmCreateNewYearDatabase'] = "The template data of the new academic year will be copied from school year <!--academicYearName-->. Are you sure you want to create new academic year data for the system?";
$Lang['PowerPortfolio']['Management']['DataTransition']['ConfirmTransition'] = "Proceed to do selected data transition?";

$Lang['PowerPortfolio']['Management']['InputScore']['Title'] = "Continuously Assessment Record";
$Lang['PowerPortfolio']['Management']['InputScore']['InputStatus'] = "Input Status";
$Lang['PowerPortfolio']['Management']['InputScore']['Topic'] = "Topic";
$Lang['PowerPortfolio']['Management']['InputScore']['Rubric'] = "Rubrics";
$Lang['PowerPortfolio']['Management']['InputScore']['GradeDescription'] = "Grade Description";
$Lang['PowerPortfolio']['Management']['InputScore']['ClassAndClassNo'] = "Class & Class No.";
$Lang['PowerPortfolio']['Management']['InputScore']['StudentName'] = "Name of Student";
$Lang['PowerPortfolio']['Management']['InputScore']['FormatWarningMsg'] = "Data format incorrect";
$Lang['PowerPortfolio']['Management']['InputScore']['AccordingClass'] = "Group By Class";
$Lang['PowerPortfolio']['Management']['InputScore']['AccordingStudent'] = "Group By Student";
$Lang['PowerPortfolio']['Management']['InputScore']['Student'] = "Student";
$Lang['PowerPortfolio']['Management']['InputScore']['Score'] = "Mark";
$Lang['PowerPortfolio']['Management']['InputScore']['N.A.'] = "Not Available";

$Lang['PowerPortfolio']['Management']['Activity']['Title'] = 'Activity Record';
$Lang['PowerPortfolio']['Management']['Activity']['InputStatus'] = "Input Status";
$Lang['PowerPortfolio']['Management']['Activity']['AccordingClass'] = "According by Class";
$Lang['PowerPortfolio']['Management']['Activity']['AccordingStudent'] = "According by Student";
$Lang['PowerPortfolio']['Management']['Activity']['ActivityPhoto'] = "Activity photo (Maximum 5Mb)";
$Lang['PowerPortfolio']['Management']['Activity']['Description'] = "Description (Maximum 100 words)";
$Lang['PowerPortfolio']['Management']['Activity']['Student'] = "Student";

$Lang['PowerPortfolio']['Reprots']['CAReport']['Title'] = "Continuously Assessment Report";
$Lang['PowerPortfolio']['Reprots']['CAReport']['Report'] = "Report";
$Lang['PowerPortfolio']['Reprots']['CAReport']['ReleaseDate'] = "Release Date";
$Lang['PowerPortfolio']['Reprots']['CAReport']['ReleaseDate'] = "Topic";

$Lang['PowerPortfolio']['Settings']['Rubrics']['Title'] = "Rubrics";
$Lang['PowerPortfolio']['Settings']['Rubrics']['Name'] = "Name";
$Lang['PowerPortfolio']['Settings']['Rubrics']['TargetForm'] = "Class Level";
$Lang['PowerPortfolio']['Settings']['Rubrics']['Levels'] = "No. of Layer";
$Lang['PowerPortfolio']['Settings']['Rubrics']['GradingNum'] = "No. of Grade";
$Lang['PowerPortfolio']['Settings']['Rubrics']['GradingDetails'] = "Grade";
$Lang['PowerPortfolio']['Settings']['Rubrics']['TermResultGradingNum'] = "No. of Grade for Term Assessment";
$Lang['PowerPortfolio']['Settings']['Rubrics']['TermResultGrades'] = "Grade for Term Assessment";
$Lang['PowerPortfolio']['Settings']['Rubrics']['SubTitle'] = "Rubrics Details";
$Lang['PowerPortfolio']['Settings']['Rubrics']['AddItem'] = 'Add Item';
$Lang['PowerPortfolio']['Settings']['Rubrics']['AddSubCat'] = 'Add Sub-Category';
$Lang['PowerPortfolio']['Settings']['Rubrics']['AddCat'] = 'Add Category';
$Lang['PowerPortfolio']['Settings']['Rubrics']['PleaseInputName'] = 'Please input name.';
$Lang['PowerPortfolio']['Settings']['Rubrics']['ImportRubricIndex'] = 'Import Rubric Index';
$Lang['PowerPortfolio']['Settings']['Rubrics']['Category'] = 'Category';
$Lang['PowerPortfolio']['Settings']['Rubrics']['SubCategory'] = 'Sub-Category';
$Lang['PowerPortfolio']['Settings']['Rubrics']['Item'] = 'Item';
$Lang['PowerPortfolio']['Settings']['Rubrics']['RubricIndexHint'] = 'Do not edit or remove any Item if "Import Rubric Index" function will be in used';
$Lang['PowerPortfolio']['Settings']['Rubrics']['ImportReference']['Title'] = 'Click to inspect available rubric setting';
$Lang['PowerPortfolio']['Settings']['Rubrics']['ImportReference']['SubCatRemark'] = '<span class="tabletextremark">(If rubric index setting level is 2, please remain empty)</span>';

$Lang['PowerPortfolio']['Settings']['Topic']['Title'] = "Topics";
$Lang['PowerPortfolio']['Settings']['Topic']['Name'] = "Topic Name";
$Lang['PowerPortfolio']['Settings']['Topic']['TargetForm'] = "Class Level";
$Lang['PowerPortfolio']['Settings']['Topic']['Rubrics'] = "Rubrics";
$Lang['PowerPortfolio']['Settings']['Topic']['AssessmentPeriod'] = "Assessment Period";
$Lang['PowerPortfolio']['Settings']['Topic']['InputPeriod'] = "Input Result Period";
$Lang['PowerPortfolio']['Settings']['Topic']['Selected'] = "selected";

$Lang['PowerPortfolio']['Settings']['Activity']['Title'] = 'Activity Records';
$Lang['PowerPortfolio']['Settings']['Activity']['Name'] = 'Name';
$Lang['PowerPortfolio']['Settings']['Activity']['ReportName'] = 'Report Name';
$Lang['PowerPortfolio']['Settings']['Activity']['ApplyTopic'] = 'Use Topic';
$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTopic'] = 'Topic';
$Lang['PowerPortfolio']['Settings']['Activity']['ActivityTitle'] = 'Title';
$Lang['PowerPortfolio']['Settings']['Activity']['TargetForm'] = 'Class Level';
$Lang['PowerPortfolio']['Settings']['Activity']['Date'] = 'Date';
$Lang['PowerPortfolio']['Settings']['Activity']['Duration'] = 'Duration (mins)';
$Lang['PowerPortfolio']['Settings']['Activity']['Location'] = 'Location';
$Lang['PowerPortfolio']['Settings']['Activity']['Type'] = 'Type';
$Lang['PowerPortfolio']['Settings']['Activity']['ObserveArea'] = 'Target Rubrics';
$Lang['PowerPortfolio']['Settings']['Activity']['TypeArr']['Photo'] = 'Photo';
$Lang['PowerPortfolio']['Settings']['Activity']['TypeArr']['Text'] = 'Description';
$Lang['PowerPortfolio']['Settings']['Activity']['TypeArr']['PhotoText'] = 'Photo and Description';

$Lang['PowerPortfolio']['Settings']['Zone']['Title'] = "Learning Zone";
$Lang['PowerPortfolio']['Settings']['Zone']['Name'] = "Name";
$Lang['PowerPortfolio']['Settings']['Zone']['Topic'] = "Topic";
$Lang['PowerPortfolio']['Settings']['Zone']['TopicCode'] = "Topic Code";
$Lang['PowerPortfolio']['Settings']['Zone']['Quota'] = "Quota";
$Lang['PowerPortfolio']['Settings']['Zone']['Image'] = "Icon";
$Lang['PowerPortfolio']['Settings']['Zone']['ImageCode'] = "Icon Code";
$Lang['PowerPortfolio']['Settings']['Zone']['ImportReference']['Topic'] = 'Click here to check topics';
$Lang['PowerPortfolio']['Settings']['Zone']['ImportReference']['Image'] = 'Click here to check images';

$Lang['PowerPortfolio']['Settings']['Tool']['Title'] = "Tools";
$Lang['PowerPortfolio']['Settings']['Tool']['Name'] = "Name";
$Lang['PowerPortfolio']['Settings']['Tool']['ApplyForm'] = 'Class Level';
$Lang['PowerPortfolio']['Settings']['Tool']['Topic'] = 'Topic';
$Lang['PowerPortfolio']['Settings']['Tool']['Zone'] = 'Learning Zone';
$Lang['PowerPortfolio']['Settings']['Tool']['Rubrics'] = 'Rubrics';
$Lang['PowerPortfolio']['Settings']['Tool']['Photo'] = "Photo";
$Lang['PowerPortfolio']['Settings']['Tool']['AddRubricIndex'] = "Add rubrics";

$Lang['PowerPortfolio']['Settings']['Comment']['Title'] = "Comment";
$Lang['PowerPortfolio']['Settings']['Comment']['Name'] = "Name";
$Lang['PowerPortfolio']['Settings']['Comment']['Category'] = "Category";
$Lang['PowerPortfolio']['Settings']['Comment']['CategoryName'] = "Category Name";
$Lang['PowerPortfolio']['Settings']['Comment']['Content'] = "Content";
$Lang['PowerPortfolio']['Settings']['Comment']['Count'] = "Count";
$Lang['PowerPortfolio']['Settings']['Comment']['ImportReference']['CategoryName'] = 'Click here to check existing category names';

$Lang['PowerPortfolio']['Settings']['Report']['Title'] = "Report Settings";
$Lang['PowerPortfolio']['Settings']['Report']['Name'] = "Name";
$Lang['PowerPortfolio']['Settings']['Report']['TargetForm'] = "Class Level";
$Lang['PowerPortfolio']['Settings']['Report']['Period'] = "Assessment Period";
$Lang['PowerPortfolio']['Settings']['Report']['Cover'] = "Cover Page";
$Lang['PowerPortfolio']['Settings']['Report']['CoverIncludeInfo'] = "Include gender, height and weight";
$Lang['PowerPortfolio']['Settings']['Report']['CoverTitle'] = "Cover Page Header";
$Lang['PowerPortfolio']['Settings']['Report']['CoverTitleDefault'] = "幼兒評量紀錄";
$Lang['PowerPortfolio']['Settings']['Report']['CoverSubTitle'] = "Cover Page Subheading";
$Lang['PowerPortfolio']['Settings']['Report']['CAReport'] = "Continuous Assessment Report";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportTitle'] = "Report Header";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportTitleDefault'] = "持續性評估紀錄";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportSelectTopic'] = "Topic";
$Lang['PowerPortfolio']['Settings']['Report']['CAReportTopicCount'] = "Topic Count";
$Lang['PowerPortfolio']['Settings']['Report']['ActivityRecords'] = "Activity Records";
$Lang['PowerPortfolio']['Settings']['Report']['ActivitySelect'] = "Activity";
$Lang['PowerPortfolio']['Settings']['Report']['ActivityCount'] = "Activity Count";
$Lang['PowerPortfolio']['Settings']['Report']['TermAssessment'] = "Term Assessment";
$Lang['PowerPortfolio']['Settings']['Report']['TermAssessmentIncluded'] = "Include Term Assessment";
$Lang['PowerPortfolio']['Settings']['Report']['TermAssessmentTitle'] = "Assessment Header";
$Lang['PowerPortfolio']['Settings']['Report']['TermComment'] = "Term Comment";
$Lang['PowerPortfolio']['Settings']['Report']['TermCommentIncluded'] = "Include Term Comment";
$Lang['PowerPortfolio']['Settings']['Report']['TermCommentTitle'] = "Comment Header";
$Lang['PowerPortfolio']['Settings']['Report']['PageLastPage'] = "Last Paage";
$Lang['PowerPortfolio']['Settings']['Report']['PageLastPageIncludeInfo'] = "Include Attendnace Info";

$Lang['PowerPortfolio']['Setting']['Code'] = "Code";
$Lang['PowerPortfolio']['Setting']['Type'] = "Type";
$Lang['PowerPortfolio']['Setting']['Name'] = "Name";
$Lang['PowerPortfolio']['Setting']['NameB5'] = "Name (Chinese)";
$Lang['PowerPortfolio']['Setting']['NameEN'] = "Name (English)";
$Lang['PowerPortfolio']['Setting']['ApplyForm'] = "Apply Form";
$Lang['PowerPortfolio']['Setting']['ZoneTopic'] = "Related Topic";
$Lang['PowerPortfolio']['Setting']['ZoneQuota'] = "Quota";
$Lang['PowerPortfolio']['Setting']['PhotoRemarks'] = "Photos with size 300px x 300px (W x H) were recommanded";
$Lang['PowerPortfolio']['Setting']['PleaseInputContent'] = "Please input content";
$Lang['PowerPortfolio']['Setting']['PleaseInputNum'] = "Please input number";
$Lang['PowerPortfolio']['Setting']['PleaseSelectForm'] = "Please select form level";
$Lang['PowerPortfolio']['Setting']['PleaseOneOption'] = "Please select one option";
$Lang['PowerPortfolio']['Setting']['Photo'] = "Photo";
$Lang['PowerPortfolio']['Setting']['Picture'] = "Zone Picture";
$Lang['PowerPortfolio']['Setting']['AbilityIndex'] = "Ability Index";
$Lang['PowerPortfolio']['Setting']['Year'] = "Year";
$Lang['PowerPortfolio']['Setting']['Remarks'] = "Remarks";
$Lang['PowerPortfolio']['Setting']['ToolCategory'] = "Teaching Tools' Category";
$Lang['PowerPortfolio']['Setting']['Chapter'] = "Chapter";
$Lang['PowerPortfolio']['Setting']['ActionType'] = "ACtion Type";

$Lang['PowerPortfolio']['Setting']['InputWarning'] = "Please Input ";
$Lang['PowerPortfolio']['Setting']['DuplicateWarning'] = " duplicated";
$Lang['PowerPortfolio']['Setting']['EditWarning']['NoMoreThan1'] = "Edit item cannot more than one";
$Lang['PowerPortfolio']['Setting']['EditWarning']['PleaseSelectEdit'] = "Please Select Edit Item";
$Lang['PowerPortfolio']['Setting']['EditWarning']['CommentTooLong'] = "The comment exceeded the maximum number of characters limit. (Maximum: <!--max--> characters)";
$Lang['PowerPortfolio']['Setting']['DeleteWarning']['PleaseSelectDelete'] = "Please Select Delete Item";
$Lang['PowerPortfolio']['Setting']['ImportWarning']['CSVFormat'] = 'Wrong file format. The uploaded file must be in .csv or .txt format.';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['EmptyContent'] = 'File content cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ErrorCount'] = 'Total of error';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['RecordCount'] = 'Total Record';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['InsertCount'] = 'Total of success';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['RubricSettingEdited'] = 'This rubric setting has been edited';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ItemNotFound']['Topic'] = 'No Topic Found';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ItemNotFound']['RubricSetting'] = 'Rubric Setting not found';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Name'] = 'Name cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Topic'] = 'Topic cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Quota'] = 'Quota cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Category'] = 'Category cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['CommentCategory'] = 'Comment Category cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['Comment'] = 'Comment cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldEmpty']['SubCategory'] = 'Sub-Category cannot be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['FieldInvalid']['SubCategory'] = 'Sub-Category should be empty';
$Lang['PowerPortfolio']['Setting']['ImportWarning']['ZonePicture'] = "Picture code must be ";
$Lang['PowerPortfolio']['Setting']['ImportWarning']['WrongInputPeriod'] = "Input result period cannot start earlier then the start date of assessment period";
$Lang['PowerPortfolio']['Setting']['ImportWarning']['WrongInputPeriod2'] = "Input result period cannot end earlier then the end date of assessment period";

/*
$Lang['PowerPortfolio']['Management']['LearningZone']['Title'] = "Learning Zone";

$Lang['PowerPortfolio']['Management']['TopicTimeTable']['Title'] = "Learning TimeTable";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['StartDate'] = "Start Date";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['EndDate'] = "End Date";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['PleaseEnterTopic'] = "Please Enter Topic";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['PleaseEnterYear'] = "Please Enter Year";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['NoLearningTool'] = "No Learning Tool";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['NumOfZones'] = "Number of zones";
$Lang['PowerPortfolio']['Management']['TopicTimeTable']['PleaseConfirmChanges'] = 'Zone Settings are updated. Please click the "Submit" button to confirm the changes.';

$Lang['PowerPortfolio']['Management']['ToolScore']['Title'] = "Input Score Revision (Learning Tool)";
$Lang['PowerPortfolio']['Management']['ToolScore']['ToolList'] = "Learning Tool List";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputScore'] = "Input Score";
$Lang['PowerPortfolio']['Management']['ToolScore']['AllTimeTable'] = "All TimeTable";
$Lang['PowerPortfolio']['Management']['ToolScore']['TimeTable'] = "TimeTable";
$Lang['PowerPortfolio']['Management']['ToolScore']['TitleName'] = "Topic";
$Lang['PowerPortfolio']['Management']['ToolScore']['ClassName'] = "Class";
$Lang['PowerPortfolio']['Management']['ToolScore']['TeachingTools'] = "Teaching Tool";
$Lang['PowerPortfolio']['Management']['ToolScore']['ClassNumber'] = "Class No";
$Lang['PowerPortfolio']['Management']['ToolScore']['StudentName'] = "Student";
$Lang['PowerPortfolio']['Management']['ToolScore']['AddNewInputScoreRow'] = "Add Input Score Rows";
$Lang['PowerPortfolio']['Management']['ToolScore']['MaxAddNewInputScoreRow'] = "Added row number cannot greater than 20";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputMarkInvalid'] = "Invalid input mark.";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputMarkCannotBeNegative'] = "The input mark cannot be negative.";
$Lang['PowerPortfolio']['Management']['ToolScore']['InputMarkCannotBeLargerThanFullMark'] = "The input mark cannot be larger than the full mark.";

$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Title'] = "Input Score Revision (Subjects' Ability Targets)";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputScore'] = "Input Score";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Form'] = 'Form';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Subject'] = 'Subject';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['TermTimeTable'] = 'Term / Learning TimeTable';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['Term'] = 'Term';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['TimeTable'] = 'Learning TimeTable';
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputMarkInvalid'] = "Invalid input mark.";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputMarkCannotBeNegative'] = "The input mark cannot be negative.";
$Lang['PowerPortfolio']['Management']['SubjectIndexScore']['InputMarkCannotBeLargerThanFullMark'] = "The input mark cannot be larger than the full mark.";

$Lang['PowerPortfolio']['Management']['LanguageBehavior']['Title'] = "Language Proficiency and Behavioural Indicators";
$Lang['PowerPortfolio']['Management']['LanguageBehavior']['InputScore'] = "Input Score";

$Lang['PowerPortfolio']['Management']['OtherInfo']['Title'] = "Other Info";
$Lang['PowerPortfolio']['Management']['OtherInfoArr']['summary'] = "Student Info";
$Lang['PowerPortfolio']['Management']['OtherInfoArr']['award'] = "Award";
$Lang['PowerPortfolio']['Management']['OtherInfoWarningArr']['InputGrade'] = "Input Grade";
$Lang['PowerPortfolio']['Management']['OtherInfoWarningArr']['InputNumber'] = "Input Number";

$Lang['PowerPortfolio']['Management']['GenerateReports']['Title'] = "Print Report Card";

$Lang['PowerPortfolio']['Management']['ExportPerformance']['Title'] = "Export Student Performance";
$Lang['PowerPortfolio']['Management']['ExportYearlyPerformance']['Title'] = "Export Student Yearly Performance";

$Lang['PowerPortfolio']['Management']['Device']['Title'] = "Tablet Interface";
$Lang['PowerPortfolio']['Management']['Device']['LearningZone'] = "Learning Zone";
$Lang['PowerPortfolio']['Management']['Device']['InputScore'] = "Input Lesson Score";
$Lang['PowerPortfolio']['Management']['Device']['Class'] = "班別";
$Lang['PowerPortfolio']['Management']['Device']['Topic'] = "主題";
$Lang['PowerPortfolio']['Management']['Device']['NoClasses'] = "無所屬班別";
$Lang['PowerPortfolio']['Management']['Device']['NoTopics'] = "無可以選擇主題";
$Lang['PowerPortfolio']['Management']['Device']['NotSelectAnyClasses'] = "無選擇任何班別";
$Lang['PowerPortfolio']['Management']['Device']['NotSelectAnyTopics'] = "無選擇任何主題";
$Lang['PowerPortfolio']['Management']['Device']['CompleteInputScore'] = "已評分";
$Lang['PowerPortfolio']['Management']['Device']['IncompleteInputScore'] = "未評分";
$Lang['PowerPortfolio']['Management']['Device']['AllEntryStatus'] = "全部";
$Lang['PowerPortfolio']['Management']['Device']['InZoneStatus'] = "已進入";
$Lang['PowerPortfolio']['Management']['Device']['CompleteStatus'] = "已完成";
$Lang['PowerPortfolio']['Management']['Device']['IncompleteStatus'] = "未完成";
$Lang['PowerPortfolio']['Management']['Device']['NoQuota'] = "學習區已滿";
$Lang['PowerPortfolio']['Management']['Device']['PleaseLeaveFirst1'] = "你仍未離開學習區[";
$Lang['PowerPortfolio']['Management']['Device']['PleaseLeaveFirst2'] = "]";
$Lang['PowerPortfolio']['Management']['Device']['PleaseSelectTool'] = "請選擇教具";

$Lang['PowerPortfolio']['Setting']['MacauAbilityIndex']['Title'] = "The Requirements of Basic Academic Attainments";
$Lang['PowerPortfolio']['Setting']['MacauAbilityIndex']['Code'] = "Code";
$Lang['PowerPortfolio']['Setting']['MacauAbilityIndex']['Name'] = "The Requirements of Basic Academic Attainments";

$Lang['PowerPortfolio']['Setting']['TaiwanAbilityIndex']['Title'] = "Ability Targets";
$Lang['PowerPortfolio']['Setting']['TaiwanAbilityIndex']['Code'] = "Code";
$Lang['PowerPortfolio']['Setting']['TaiwanAbilityIndex']['Name'] = "Ability Targets";

$Lang['PowerPortfolio']['Management']['AbilityRemarks']['Title'] = 'Ability Target Grading Remark (Form-based & Term-based)';
$Lang['PowerPortfolio']['Management']['AbilityRemarks']['Reset'] = 'Reset Remark';
$Lang['PowerPortfolio']['Management']['AbilityRemarks']['ConfirmResetRemark'] = 'Confirm to reset remark?';
$Lang['PowerPortfolio']['Management']['AbilityRemarks']['ChangeAbilityResetAll'] = 'All related remarks will be reset as well. Are you sure you want to change to the selected Ability Targets?';

$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Title'] = 'Ability Target Grading Remark';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Grading'] = 'Grading';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['UpperLimit'] = 'Grading Range';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Comment'] = 'Ability Target Grading Remark (Original)';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['OriComment'] = 'Ability Target Grading Remark (Modified)';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['NewComment'] = 'Ability Target Grading Remark (Form-based & Term-based)';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Term'] = 'Academic Term';
$Lang['PowerPortfolio']['Setting']['AbilityRemarks']['Year'] = 'Academic Year';

$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['Title'] = "Ability Target Mapping";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['MacauCode'] = "Macau Code";
//$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "Taiwan Codes";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['TaiwanCode'] = "Ability Targets' Codes";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllMacauCat'] = "All Macau Codes";
//$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "All Taiwan Codes";
//$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "All Taiwan Class Levels";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanCat'] = "All Ability Targets' Codes";
$Lang['PowerPortfolio']['Setting']['AbilityIndexMapping']['AllTaiwanLevel'] = "All Ability Targets' Class Levels";

$Lang['PowerPortfolio']['Setting']['FormTopic']['Title'] = "Form Topics";
$Lang['PowerPortfolio']['Setting']['LearningZone']['Title'] = "Learning Zone";
$Lang['PowerPortfolio']['Setting']['TeachingTool']['Title'] = "Teaching Tools";
$Lang['PowerPortfolio']['Setting']['TeachingToolCategory']['Title'] = "Teaching Tools' Category";
$Lang['PowerPortfolio']['Setting']['LanguageBehavior']['Title'] = "Language Proficiency and Behavioural Indicators";
$Lang['PowerPortfolio']['Setting']['LanguageBehaviorItem']['Title'] = "Language Proficiency and Behavioural Indicators' Items";
$Lang['PowerPortfolio']['Setting']['LanguageBehavior']['Category1'] = "Language Proficiency";
$Lang['PowerPortfolio']['Setting']['LanguageBehavior']['Category2'] = "Behavioural Indicators";

$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Title'] = 'Subjects\' Ability Targets';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Form'] = 'Year';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Subject'] = 'Subject';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['TermTimeTable'] = 'Term / Learning TimeTable';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['Term'] = 'Term';
$Lang['PowerPortfolio']['Setting']['SubjectMapping']['TimeTable'] = 'Learning TimeTable';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TermInUse'] = 'Term is already applied to this form and subject';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TermUsed'] = 'This term is already used';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TermEmpty'] = 'Please Input Term';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TopicInUse'] = 'TimeTable is already applied to this form and subject';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TopicUsed'] = 'TimeTable is already applied to this form and subject';
$Lang['PowerPortfolio']['Setting']['SubjectMappingJSArr']['TopicEmpty'] = 'Please Input TimeTable';

$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['ModuleTitle'] = 'Ability Targets\' Category Selection (Form-based & Term-based)';
$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['Title'] = 'Ability Target Grading Remark Selection';
$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['Selections'] = 'Selections';
$Lang['PowerPortfolio']['Management']['AbilityRemarksSelections']['None'] = 'None of Selected';

$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['Title'] = 'Schedule';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['Term'] = 'Term';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['StartDate'] = 'Start Date';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['StartTime'] = 'Start Time';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['EndDate'] = 'End Date';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['EndTime'] = 'End Time';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['ModifiedDate'] = 'Last Modified Date';
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['InputScorePeriod'] = "Submission";
$Lang['PowerPortfolio']['Management']['ViewEditPeriodSetting']['ViewReportPeriod'] = "Print Report";

$Lang['PowerPortfolio']['Management']['AwardGeneration']['Title'] = "Select / Generate Student Award";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['StudentAward'] = "Student Award";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['SelectAward'] = "Select Award";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['GenerateAward'] = "Generate Award";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReceivedAwardCount'] = "No. of Awarded Students";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['LastGenerate'] = "Last Generate";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['GenerateAll'] = "Generate Award for all classes";

$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoAward'] = "Please select an award.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoClass'] = "Please select a class.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoStudentInClass'] = "There is no Student in this Class";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['NoAwardedStudentSettings'] = "There is no Student getting this Award";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['AtLeastOneStudent'] = "Please select at least one student.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['DeleteAward'] = "Are you sure you want to delete the award of this student?";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['WarningArr']['ReGenerateAward'] = "The generated awards will be overwritten if you generate the awards now. Are you sure you want to generate the awards now?";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateSuccess'] = "1|=|Award Generated Successfully.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardGenerateFailed'] = "0|=|Award Generation Failed.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteSuccess'] = "1|=|Award Deleted Successfully.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardDeleteFailed'] = "0|=|Award Deletion Failed.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddSuccess'] = "1|=|Award Added Successfully.";
$Lang['PowerPortfolio']['Management']['AwardGeneration']['ReturnMsgArr']['AwardAddFailed'] = "0|=|Award Addition Failed.";

$Lang['PowerPortfolio']['Setting']['AwardsList']['Title'] = "Student Awards";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardType'] = "Award Type";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardTypeArr']['Generate'] = "Generated by System";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardTypeArr']['ManualInput'] = "Manual Input";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardTypeArr']['G'] = "Award Generated by System";
$Lang['PowerPortfolio']['Setting']['AwardsList']['AwardTypeArr']['I'] = "Award Selected by Teacher";

# Settings > Admin Group
$Lang['PowerPortfolio']['Setting']['AdminGroup']['MenuTitle'] = 'Admin Group';
$Lang['PowerPortfolio']['Setting']['AdminGroup']['Title'] = 'Admin Group';
$Lang['PowerPortfolio']['Setting']['AdminGroup']['NumOfMember'] = "No. of Members";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['NewAdminGroupStep'][1] = "Info & Access Right";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['NewAdminGroupStep'][2] = "Select Member(s)";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupCode'] = "Admin Group Code";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupName'] = "Admin Group Name";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupList'] = "Admin Group List";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['MemberList'] = "Member List";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AddMember'] = "Add Member";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AdminGroupInfo'] = "Admin Group Info";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['AccessRight'] = "Access Right";

$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddSuccess'] = "1|=|Admin Group Added.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddFailed'] = "0|=|Admin Group Addition Failed.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['EditSuccess'] = "1|=|Admin Group Edited.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['EditFailed'] = "0|=|Admin Group Edition Failed.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteSuccess'] = "1|=|Admin Group(s) Deleted.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteFailed'] = "0|=|Admin Group(s) Deletion Failed.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddMemberSuccess'] = "1|=|Admin Group Member Added.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['AddMemberFailed'] = "0|=|Admin Group Member Addition Failed.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberSuccess'] = "1|=|Admin Group Member Deleted.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ReturnMessage']['DeleteMemberFailed'] = "0|=|Admin Group Member Deletion Failed.";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['ModifiedDate'] = 'Last Modified Date';
$Lang['PowerPortfolio']['Setting']['AdminGroup']['WarningArr']['SelectionContainsMember'] = "The highlighted options contain member(s) of this Admin Group already";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['WarningArr']['Blank']['Code'] = "Code cannot be blank";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['WarningArr']['Blank']['Name'] = "Name cannot be blank";
$Lang['PowerPortfolio']['Setting']['AdminGroup']['WarningArr']['InUse']['Code'] = "Code is in use already";

$Lang['PowerPortfolio']['Management']['ArchiveReport']['Title'] = "Report Card Archive";
$Lang['PowerPortfolio']['Management']['ArchiveReport']['LastArchived'] = "Last Archive";
$Lang['PowerPortfolio']['Management']['ArchiveReport']['ConfirmArchiveReport'] = 'Existing Report Card Archive will be overwritten. Are you sure you want to continue?';

$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['Title'] = "Print Archived Report";
$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['WarningArr']['NoMatchTerm'] = "No matched term";
$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['WarningArr']['NoMatchClass'] = "No matched class";
$Lang['PowerPortfolio']['Management']['PrintArchiveReport']['WarningArr']['NoMatchStudent'] = "No matched student";
*/
?>