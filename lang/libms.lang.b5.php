<?php
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/

### Save this file in utf-8 format ######

/******************************************* Changes log **********************************************
 *
 *  IP 2.5 VERSION !!!!!!!!!!!!!!
 *
 * Date: 2019-11-01 Tommy
 *       - added $Lang["libms"]["book"]["showOnlyBookCoverRecord"]
 *
 ******************************************************************************************************/

$Lang['libms']['library'] = "eLibrary <i>plus</i>";

$Lang['libms']['SQL']['UserNameFeild'] = "ChineseName";


#######################################################################//sqlfeild####
$Lang["libms"]["sql_field"]["Description"] = "DescriptionChi";
$Lang["libms"]["sql_field"]["User"]["name"] = "ChineseName";
#################################################

$Lang["libms"]["library"] = ($plugin['eLib_Lite']?"電子圖書館":"eLibrary <i>plus</i> 綜合圖書館");
$Lang["libms"]["status"]["na"] = "不適用";
$Lang["libms"]["value_follows_parent"] = "(與書目相同)";
$Lang["libms"]["general"]["ajaxError"] = "AJAX 錯誤!";
$Lang["libms"]["general"]["ajaxReturnNothing"] = "Ajax 沒有傳回結果,請再試一次。如果問題持續,請確定設定正確或者聯絡你的系統管理員。";
$Lang["libms"]["general"]["new"] = "新增";
$Lang["libms"]["general"]["edit"] = "編輯";
$Lang["libms"]["general"]["del"] = "刪除";
$Lang["libms"]["general"]["auto"] = "自動";
$Lang["libms"]["general"]["customize"] = "自選";
$Lang["libms"]["general"]["select"] = "選擇";
$Lang["libms"]["general"]["selectfile"] = "選擇檔案";
$Lang["libms"]['General']['warnSelectcsvFile'] = '請選擇匯入的檔案';
$Lang["libms"]["general"]["chinese"] = "中文";
$Lang["libms"]["general"]["english"] = "英文";
$Lang["libms"]["management"]["title"] = "管理";
$Lang['libms']['bookmanagement']['available_reserve'] = "可取預約";
$Lang["libms"]["bookmanagement"]["book_list"] = "書目";
$Lang['libms']['bookmanagement']['item_list'] = "項目登錄";
$Lang["libms"]["bookmanagement"]["book_import"] = "匯入書目";
$Lang["libms"]["bookmanagement"]["book_label"] = "標貼";
$Lang["libms"]["bookmanagement"]["book_labelformat"] = "標貼格式";
$Lang['libms']['bookmanagement']['book_reserve'] = "現時預約";
$Lang['libms']['bookmanagement']['book_details'] = "書目資料";
$Lang['libms']['bookmanagement']['circulation_details'] = "流通資料";
$Lang['libms']['bookmanagement']['purchase_details'] = "購買資料";
$Lang['libms']['bookmanagement']['ClassName'] = "班別";
$Lang['libms']['bookmanagement']['ClassNumber'] = "學號";
$Lang['libms']['bookmanagement']['LoanDate'] = "借書日期";
$Lang['libms']['bookmanagement']['DueDate'] = "到期日";
$Lang['libms']['bookmanagement']['ReturnDate'] = "還書日期";
$Lang['libms']['bookmanagement']['ebook_tags'] = "電子圖書標籤";
$Lang['libms']['bookmanagement']['ebook'] = "電子圖書";
$Lang['libms']['bookmanagement']['expired_book_reserve'] = "逾期預約";
$Lang['libms']['bookmanagement']['Advanced'] = "進階";
$Lang["libms"]["bookmanagement"]["follow_parent"] = "(跟隨書目)";
$Lang["libms"]["bookmanagement"]["unclassified"] = "未設分類";
$Lang["libms"]["bookmanagement"]["unnumbered"] = "未編號";
$Lang['libms']['bookmanagement']['ebook_license_quotation_error'] = "電子圖書訂單資料錯誤";
$Lang['libms']['bookmanagement']['import_ebook_tags'] = "匯入電子圖書標籤";
$Lang['libms']['bookmanagement']['import_ebook_tags_error_add'] = "新增標籤錯誤";
$Lang['libms']['bookmanagement']['import_ebook_tags_error_no_book'] = "找不到書本";
$Lang['libms']['bookmanagement']['SelectItemToExport'] = "選擇項目以匯出書目";
$Lang['libms']['bookmanagement']['ReturnLostBook'] = "報失後歸還";
$Lang['libms']['bookmanagement']['show_or_hide'] = "顯示/隱藏";
$Lang["libms"]["book_export_marc21"]="匯出 MARC21";
$Lang["libms"]["book_export_without_writeoff"]="匯出(不包括註銷書)";
$Lang['libms']['batch_edit']['menu'] = "批量修改";
$Lang['libms']['batch_edit']['edit_return_date'] = "到期日期";
$Lang['libms']['batch_edit']['edit_penal_sum'] = "罰款";
$Lang['libms']['batch_edit']['edit_book_info'] = "圖書資料";
$Lang['libms']['batch_edit']['edit_tags'] = "標籤字眼";
$Lang['libms']['batch_edit']['edit_subject'] = "科目";
$Lang['libms']['import_book_cover'] = "匯入圖書封面";
$Lang['libms']['batch_edit']['edit_books'] = "書目資料";
$Lang['libms']['batch_edit']['edit_items'] = "項目資料";
$Lang['libms']['batch_edit']['edit_recommend'] = "推介好書";
$Lang['libms']['batch_edit']['edit_book_info_remark'] = "如不需更改，請留空此格";

$Lang['libms']['batch_edit']['edit_return_date_same'] = "更改為另一指定日期";
$Lang['libms']['batch_edit']['edit_return_date_defer'] = "延期至";
$Lang['libms']['batch_edit']['edit_return_date_early'] = "提前至";
$Lang['libms']['batch_edit']['record_found'] = "找到以下 %s 個借書紀錄，";
$Lang['libms']['batch_edit']['modify_due_date'] = "現修改還書日期";
$Lang["libms"]["batch_edit"]["Day"] = "日";
$Lang["libms"]["batch_edit"]["send_email"] = "發電郵通知";
$Lang["libms"]["batch_edit"]["send_email_yes"] = "要發";
$Lang["libms"]["batch_edit"]["send_email_no"] = "不需";
$Lang["libms"]["batch_edit"]["DueDate"] = "到期日期";
$Lang["libms"]["batch_edit"]["Bak_DueDate"] = "原先的到期日期";
$Lang['libms']['batch_edit']['record_updated'] = "已更新以下 %s 個借書紀錄";
$Lang['libms']['batch_edit']['PleaseSelectGroup'] = "請選擇至少一個組別";
$Lang['libms']['batch_edit']['PleaseSelectClass'] = "請選擇至少一個班別";
$Lang['libms']['batch_edit']['PleaseSelectDateAfterToday'] = "請選擇今天或以後的日子";

$Lang['libms']['import_format'] = "資料匯入格式";
$Lang['libms']['import_marc21'] = "MARC 21";
$Lang['libms']['import_CSV'] = "CSV";
$Lang['libms']['import_TXT'] = "TXT";

$Lang['libms']['import_book_cover_file'] = "封面圖檔";
$Lang['libms']['import_book_cover_file_types'] = "(jpg / png / zip)";
$Lang['libms']['import_book_cover_file_remark'] = "1. 圖檔名稱需為圖書的 ISBN 或 登錄號碼 (例如： bok002138.jpg)<br> 
2. 如須同時上載多個圖檔，請使用壓縮檔案(.zip)<br>
3. 壓縮檔案內不能存在文件夾";
$Lang['libms']['import_book_cover_file_name'] = "圖檔名稱";
$Lang['libms']['select_image_file'] = "選擇JPG/PNG/ZIP檔案";
$Lang['libms']['invalid_acno'] = "找不到登錄號碼";
$Lang['libms']['invalid_isbn'] = "找不到 ISBN";
$Lang['libms']['correct'] = "正確";
$Lang['libms']['invalid_book_cover_file_types'] = "請選擇圖片(.jpg,.png)或壓縮(.zip)檔案";

## Stock Take
$Lang['libms']['stocktake_status']['FOUND'] = "已找到";
$Lang['libms']['stocktake_status']['WRITEOFF'] = "已註銷";
$Lang['libms']['stocktake_status']['NOTTAKE'] = "未盤點";
$Lang['libms']['stocktake_status_bookfound']['BookFound'] = "此書已找到";
$Lang['libms']['bookmanagement']['ImportTXT'] = "TXT 資料檔";
$Lang['libms']['bookmanagement']['TXTFormat'] = "(.txt 檔案)";
$Lang['libms']['bookmanagement']['LastFive'] = "最近5個盤點";
$Lang['libms']['bookmanagement']['LastFour'] = "最近4個盤點";
$Lang['libms']['bookmanagement']['LastThree'] = "最近3個盤點";
$Lang['libms']['bookmanagement']['LastTwo'] = "最近2個盤點";
$Lang['libms']['bookmanagement']['LastOne'] = "最近1個盤點";
$Lang['libms']['bookmanagement']['Relocate'] = "更換";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample'] = "按此下載範例";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample1'] = "按此下載範例1";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample2'] = "按此下載範例2";

$Lang['libms']['bookmanagement']['Reason']= "原因";
$Lang['libms']['bookmanagement']['WriteOffDate'] = "註銷日期";
$Lang["libms"]["bookmanagement"]["ImportStockTake"] = array("<font color = red>*</font>條碼","新位置代碼");
$Lang["libms"]["bookmanagement"]["ImportWriteOff"] = array("<font color = red>*</font>條碼","註銷日期","原因");
$Lang["libms"]["bookmanagement"]["BarcodeDuplicate"] = "條碼重複";
$Lang["libms"]["bookmanagement"]["BarcodeNotExist"] = "條碼資料不符";
$Lang["libms"]["bookmanagement"]["import"] = "匯入";
//$Lang["libms"]["bookmanagement"]["ImportBorrowRecord"] = array("<font color = red>*</font>使用者條碼","<font color = red>*</font>圖書條碼","<font color = red>*</font>借出日期", "<font color = red>*</font>借出時間","<font color = red>*</font>到期日期","<font color = red>*</font>續借次數","還書日期","還書時間","<font color = red>*</font>借還狀態");
$Lang["libms"]["bookmanagement"]["ImportBorrowRecord"] = array("<font color = red>*</font>登錄號碼","<font color = red>*</font>借出日期","<font color = red>*</font>借出時間", "<font color = red>*</font>到期日期","還書日期","<font color = red>*</font>借還狀態","<font color = red>*</font>罰款","讀者英文全名","讀者中文全名","<font color = red>*</font>讀者條碼","班別","學號");
$Lang['libms']["bookmanagement"]['ConfirmToTerminate'] = "是否確定要終止發送電郵？";
$Lang['libms']["bookmanagement"]['SendingTo1']= "正在新增到第 ";
$Lang['libms']["bookmanagement"]['SendingTo2']= " 個借還紀錄 ... ";

$Lang['libms']['bookmanagement']['stocktakeAndWriteOff'] = "盤點及註銷";
$Lang['libms']['bookmanagement']['stock-take'] = "盤點";
$Lang['libms']['bookmanagement']['stock-takeProcess'] = "盤點處理";
$Lang['libms']['bookmanagement']['stocktakeRecord'] = "盤點紀錄";
$Lang['libms']['bookmanagement']['writeoffRecord'] = "註銷紀錄";
$Lang['libms']['bookmanagement']['location'] = "地點";
$Lang['libms']['bookmanagement']['newLocationCode'] = "新位置代碼";
$Lang['libms']['bookmanagement']['total'] = "總數";
$Lang['libms']['bookmanagement']['progress'] = "進度";
$Lang['libms']['bookmanagement']['lastRecord'] = "最新紀錄";
$Lang['libms']['bookmanagement']['lastTakenBy'] = "紀錄自";
$Lang['libms']['bookmanagement']['overall'] = "整體";
$Lang['libms']['bookmanagement']['stocktakeList'] = "盤點清單";
$Lang['libms']['bookmanagement']['barcode'] = "條碼";
$Lang['libms']['bookmanagement']['bookTitle'] = "書名";
$Lang['libms']['bookmanagement']['bookStatus'] = "項目狀態";
$Lang['libms']['bookmanagement']['bookStatusIsLost'] = "這是遺失的圖書";
$Lang['libms']['bookmanagement']['result'] = "結果";
$Lang['libms']['bookmanagement']['originalLocation'] = "原本位置";
$Lang['libms']['bookmanagement']['ItemLocation'] = "館藏位置";
$Lang['libms']['bookmanagement']['ChangeItemLocationWarning'] = "你是否確定要更換館藏位置到";
$Lang['libms']['bookmanagement']['relocate'] = "更換館藏位置到此";
$Lang['libms']['bookmanagement']['Stock-takeTime'] = "盤點時間";
$Lang['libms']['bookmanagement']['EnterOrScanItemBarcode'] = "輸入或掃瞄物品條碼";
$Lang['libms']['bookmanagement']['BookLocationUpdateFailed'] = "圖書地點更改失敗";
$Lang['libms']['bookmanagement']['BookLocationUpdateSuccessfully'] = "圖書地點更改成功";
$Lang['libms']['bookmanagement']['BookLocationUpdated'] = "已更改";
$Lang['libms']['bookmanagement']['SelectAnotherLocation'] = "選擇另一位置";
$Lang['libms']['bookmanagement']['StocktakeStatus'] = "盤點狀態"; 
$Lang['libms']['bookmanagement']['scope'] = "範圍"; 
$Lang['libms']['bookmanagement']['allBooks'] = "所有圖書";
$Lang['libms']['bookmanagement']['alleBooks'] = "所有電子書";
$Lang['libms']['bookmanagement']['showneBooks'] = "顯示的電子書";
$Lang['libms']['bookmanagement']['hiddeneBooks'] = "隱藏的電子書";
$Lang['libms']['bookmanagement']['selectedBooks'] = "選擇圖書";
$Lang['libms']['bookmanagement']['lastStockTake'] = "最新盤點";
$Lang['libms']['bookmanagement']['writeOff'] = "註銷";
$Lang['libms']['bookmanagement']['ConfirmWriteOff'] = "你是否確定註銷圖書?";
$Lang['libms']['bookmanagement']['cancelWriteOff'] = "取消註銷";
$Lang['libms']['bookmanagement']['WriteOffBy'] = "註銷自";
$Lang['libms']['bookmanagement']['WriteOffTime'] = "註銷時間";
$Lang['libms']['bookmanagement']['totalBorrow'] = "借出總數";
$Lang['libms']['bookmanagement']['last_borrow_time'] = "最後借出日期";
$Lang['libms']['bookmanagement']['last_borrow_user'] = "讀者";
$Lang['libms']['bookmanagement']['deleted_user_legend'] = '<font style="color:red;">*</font> 表示該用戶已被刪除或已離校。';
$Lang['libms']['bookmanagement']['SearchHelp'] = '<ol><li>可輸入書名、副題、登錄號碼、條碼字句、出版商、索書號、索書號２、ISBN、ISBN２、著者名１、著者名２、著者名３或集叢來搜索。</li><li>可使用引號括起登錄號碼，搜索出登錄號碼完全相同的書目／項目，例如："B100238"。</li><li>可於兩個登錄號碼中間加入「~」，按登錄號碼搜索出中間的書目／項目，例如：B100231~B100250。</li><li>可於登錄號碼前綴後面加入「^」，搜索出最大登錄號碼的書目／項目，例如：C10^。</li></ol>';
$Lang['libms']['bookmanagement']['BookItemSearchHelp'] = '<ol><li>可輸入書名、登錄號碼、條碼字句、經銷商或出版商來搜索。</li><li>可使用引號括起登錄號碼，搜索出登錄號碼完全相同的書目／項目，例如："B100238"。</li><li>可於兩個登錄號碼中間加入「~」，按登錄號碼搜索出中間的書目／項目，例如：B100231~B100250。</li><li>可於登錄號碼前綴後面加入「^」，搜索出最大登錄號碼的書目／項目，例如：C10^。</li></ol>';
$Lang['libms']['bookmanagement']['NoLocation'] = '沒有位置';
$Lang['libms']['bookmanagement']['IsWriteOffBook'] = "圖書已被註銷，不能進行盤點。";
$Lang['libms']['bookmanagement']['ConfirmSuspectedMissing'] = "你是否確定把圖書狀態轉為懷疑不見 ?";
$Lang['libms']['bookmanagement']['StocktakeConductedDuringPeriod'] = "盤點期間才可進行盤點";
$Lang['libms']['bookmanagement']['ItemNotForThisStocktake'] = "圖書不屬於此盤點期間內，不能進行盤點。";
$Lang['libms']['bookmanagement']['ConfirmDeleteReservationAndAssignNext'] = "是否確定刪除所選預約并且分配下一個使用者取書?";
$Lang['libms']['bookmanagement']['NextReserve'] = "下一個預約者: ";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['DuplicateBarcode'] = "此物品條碼已經輸入或掃瞄了";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['ConfirmChangeStocktakeName'] = "轉換盤點時段名稱會導致所有已經輸入或掃瞄的物品條碼消失,是否確定?";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['IsWriteOffBook'] = "圖書已被註銷，不用再註銷。";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['WriteOffByBarcode'] = "以物品條碼註銷";
$Lang['libms']['bookmanagement']['AddFromExistingItem'] = "從現有項目加入";
$Lang['libms']['bookmanagement']['AddToThisBook'] = "加入到此書目";
$Lang['libms']['bookmanagement']['warning_add_to_this_book_confirm'] = "你是否確定要加入到此書目？";
$Lang['libms']['bookmanagement']['duplicate_user_barcode'] = "用戶條碼重複了!";
$Lang['libms']['bookmanagement']['user_missing'] = "用戶識別碼空號!";
$Lang['libms']['bookmanagement']['barcode_missing'] = "用戶條碼空號!";
$Lang['libms']['bookmanagement']['purchased'] = "已購買";
$Lang['libms']['bookmanagement']['expiry_date'] = "到期日";
$Lang['libms']['bookmanagement']['exportInvalidRecord'] = "匯出無效紀錄";
$Lang['libms']['bookmanagement']['ConfirmReturnLostBook'] = "是否確定歸還報失圖書";
$Lang['libms']['bookmanagement']['ReturnLostBookRemarkWiePayment'] = "需要先在收費管理模組把相關紀錄(包括逾期及遺失罰款)還原至未繳才可歸還報失圖書";
$Lang['libms']['bookmanagement']['ReturnLostBookRemarkWoePayment'] = "如所選紀錄已繳罰款,系統會取消該紀錄(包括逾期及遺失罰款),然後重新計算逾期罰款(如有)。歸還報失圖書後,請在流通管理處理逾期罰款。";
$Lang['libms']['bookmanagement']['ConfirmShoweBook'] = "是否確定顯示所選圖書?";
$Lang['libms']['bookmanagement']['ConfirmHideeBook'] = "是否確定隱藏所選圖書?";
$Lang['libms']['settings']['stocktake']['from'] = "由";
$Lang['libms']['settings']['stocktake']['to'] = "至";
$Lang['libms']['settings']['stocktake']['LastUpdated'] = "最近更新";
$Lang['libms']['reporting']['date'] = "日期:";
$Lang['libms']['reporting']['dateRange'] = "日期範圍";
$Lang['libms']['reporting']['stocktakeReport'] = "盤點報告";
$Lang['libms']['reporting']['stocktakeSummary'] = "盤點總結";
$Lang['libms']['reporting']['progressDateRangeRemarks'] = "只適用於圖書館資源使用率及罰款";

$Lang['libms']['settings']['stocktake']['recordStartDate']= "註銷後計算日期";
$Lang['libms']['settings']['stocktake']['recordEndDate']= "登錄後計算日期";
$Lang['libms']['settings']['stocktake']['stocktakeDate']= "盤點日子";
$Lang['libms']['settings']['stocktake']['itemRecordDate']= "館藏紀錄計算日子";
$Lang['libms']['settings']['stocktake']['stocktakeDateRemarks'] = '在此時段開放盤點功能';
$Lang['libms']['settings']['stocktake']['itemRecordDateRemarks'] = '在此時間的紀錄會計算在報告內';
$Lang['libms']['settings']['stocktake']['name'] = '盤點時段名稱';
$Lang['libms']['settings']['stocktake']['inputname'] = '請輸入盤點時段名稱。';
$Lang['libms']['settings']['stocktake']['recordStartDateRemarks'] = "此日期前所註銷的項目不用盤點及不會計算在報告內";
$Lang['libms']['settings']['stocktake']['recordEndDateRemarks'] = "此日期後所登錄的項目不用盤點及不會計算在報告內";

$Lang['libms']["settings"]['notification']['title'] = "通告設定";
$Lang['libms']["settings"]['notification']['reserve_email'] = "預約可取電郵通知";
$Lang['libms']["settings"]['notification']['overdue_email'] = "到期日電郵通知";
$Lang['libms']["settings"]['notification']['due_date_reminder_email'] = "到期日前電郵通知";
$Lang['libms']["settings"]['notification']['yes'] = "是";
$Lang['libms']["settings"]['notification']['no'] = "否";
$Lang['libms']["settings"]['notification']['push_message_overdue'] = "到期日推播提示 (eClass App)";
$Lang['libms']["settings"]['notification']['push_message_due_date'] = "到期日前推播提示 (eClass App)";
$Lang['libms']["settings"]['system']['push_message_due_date_reminder'] = "到期[ ]日前推播提示 (eClass App)";
$Lang['libms']["settings"]['system']['push_message_time'] = "推播提示時間 (eClass App)";

$Lang['libms']["settings"]['system']['yes'] = "是";
$Lang['libms']["settings"]['system']['no'] = "否";
$Lang['libms']["settings"]['system']['max_overdue_limit'] = "遲還寬限期 ( -1 = 不限制 ) ";
$Lang['libms']["settings"]['system']['override_password'] = "特許密碼";
$Lang['libms']["settings"]['system']['global_lockout'] = "閉館";
$Lang['libms']["settings"]['system']['batch_return_handle_overdue'] = "於批量還書處理逾期歸還";
$Lang['libms']["settings"]['system']['overdue_epayment'] = "繳付罰款方法";

$Lang['libms']["settings"]['system']['system_open_date'] = "開始借閱日期";
$Lang['libms']["settings"]['system']['system_end_date'] = "最後借閱日期";

$Lang['libms']["settings"]['system']['show_number_of_loans_when_borrow_book'] = "借書時顯示讀者已借閱該書旳次數";
$Lang['libms']["settings"]['system']['leave_user_after_borrow_book'] = "借書後離開該讀者";
$Lang['libms']["settings"]['system']['overdue_epayment_a'] = "現金";
$Lang['libms']["settings"]['system']['overdue_epayment_b'] = "透過 ePayment 即時付款";
$Lang['libms']["settings"]['system']['max_book_reserved_limit'] = "領取預約圖書日數期限 (0 為不限制)";
$Lang['libms']["settings"]['system']['allow_reserve_available_books_in_portal'] = "允許用戶預約可借出的書";
$Lang['libms']["settings"]['system']['school_display_name'] = "顯示學校名稱 (中文)";
$Lang['libms']["settings"]['system']['school_display_name_eng'] = "顯示學校名稱 (英文)";
$Lang['libms']["settings"]['system']['due_date_reminder'] = "到期[  ]日前電郵通知";
$Lang['libms']["settings"]['system']['circulation_student_by_selection'] = "流通管理允許使用班別及班號來選學生";
$Lang['libms']["settings"]['system']['circulation_duedate_method'] = "還書日期設定"; //"以學校日子來計算還書日期（「否」代表計算假期及不開放的日子）";
$Lang['libms']["settings"]['system']['circulation_duedate_method_yes'] = "只計算開館日子";
$Lang['libms']["settings"]['system']['circulation_duedate_method_no'] = "計算所有日子";
$Lang['libms']["settings"]['system']['general'] = "一般";
$Lang['libms']["settings"]['system']['password_setting'] = "密碼設定";
$Lang['libms']["settings"]['system']['book_loan_setting'] = "借書相關設定";
$Lang['libms']["settings"]['system']['book_return_setting'] = "還書相關設定";
$Lang['libms']["settings"]['system']['book_renew_setting'] = "續借相關設定";
$Lang['libms']["settings"]['system']['book_reserve_setting'] = "預約相關設定";
$Lang['libms']["settings"]['system']['circulation_setting'] = "流通管理設定";
$Lang['libms']["settings"]['system']['book_review_setting'] = "書評相關設定";
$Lang['libms']["settings"]['system']['preference_setting'] = "喜好設定";
$Lang['libms']["settings_remark"]['max_overdue_limit'] = "逾期還書達至此日子後不能借書";

$Lang['libms']["settings"]['system']['batch_book_renew_by_confirm'] = "續借確認";
$Lang['libms']["settings"]['system']['batch_book_renew_by_confirm_yes'] = "需要確認";
$Lang['libms']["settings"]['system']['batch_book_renew_by_confirm_no'] = "掃描即續";
$Lang['libms']["settings"]['system']['book_review_allow_edit_own_book_review'] = "允許學生編輯自己的書評";
$Lang['libms']["settings"]['system']['circulation_return_book_by_confirm'] = "還書確認";
$Lang['libms']["settings"]['system']['circulation_return_book_by_confirm_yes'] = "需要確認";
$Lang['libms']["settings"]['system']['circulation_return_book_by_confirm_no'] = "掃描即還";
$Lang['libms']["settings"]['system']['circulation_borrow_return_accompany_material'] = "借還書支援附件借還";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by'] = "遺失書本的罰款依據";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_purchase'] = "購買價格";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_list'] = "訂價";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_fixed'] = "指定價格";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge'] = "遺失書本附加費";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_fixed'] = "定額";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_ratio'] = "按<span id='ratio_based'></span>的比例";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_none'] = "沒有";
$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty'] = "遺失書本罰款額修改";
$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty_yes'] = "允許(要輸入特許密碼)";
$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty_no'] = "不允許";
$Lang['libms']["settings"]['system']['circulation_lost_book_overdue_charge'] = "遺失書本計算過期罰款";
$Lang['libms']["settings"]['system']['circulation_input_non_negative_value'] = "請輸入非負數";
$Lang['libms']["settings"]['system']['notification_input_non_negative_integer'] = "請輸入非負整數";
$Lang['libms']["settings"]['system']['preference_recommend_book_order'] = "推介書本的顯示次序";
$Lang['libms']["settings"]['system']['preference_recommend_book_order_yes'] = "推介日期";
$Lang['libms']["settings"]['system']['preference_recommend_book_order_no'] = "隨機";
$Lang['libms']["settings"]['system']['preference_photo_show'] = "排行榜、書評及我的紀錄顯示的相片";
$Lang['libms']["settings"]['system']['preference_photo_show_yes'] = "學生相片";
$Lang['libms']["settings"]['system']['preference_photo_show_no'] = "個人相片";
$Lang['libms']["settings_remark"]['global_lockout'] = "閉館時只有管理員才能使用本圖書館系統";
$Lang['libms']["settings_alert"]['batch_return_handle_overdue'] = "不可在此處理逾期歸還！<br />請使用<u>讀者借還</u>。";
$Lang['libms']["settings_alert"]['batch_return_handle_overdue_fail'] = "失敗";
$Lang["libms"]["btn"]["enter"] = "輸入";

$Lang["libms"]["settings"]["title"] = "系統設定";
$Lang["libms"]["settings"]["basic"] = "基本設定";
$Lang["libms"]["settings"]["holidays"] = "學校假期";
$Lang["libms"]["settings"]["opentime"] = "開放時間";
$Lang["libms"]["settings"]["PageSettingsSpecialOpenTime"] = "特別開放";
$Lang["libms"]["settings"]["circulation_type"] = "借閱分類";
$Lang["libms"]["settings"]["resources_type"] = "物品分類";
$Lang["libms"]["settings"]["book_category"] = "圖書分類";
$Lang["libms"]["settings"]["book_category_type"] = "圖書分類設定";
$Lang["libms"]["settings"]["book_category_name"] = "圖書分類名稱";
$Lang["libms"]["settings"]["book_location"] = "館藏位置";
$Lang["libms"]["settings"]["responsibility"] = "著者責任";
$Lang["libms"]["settings"]["stock-take"] = "盤點設定";
$Lang["libms"]["settings"]["book_language"] = "語言分類";
$Lang["libms"]["settings"]["msg"]["cannot_delete_used_code"] = "不能刪除已使用的代碼";
$Lang["libms"]["settings"]["symbols"] = "!\"#$%&\'*,/:;<=>?@^`|~";
$Lang["libms"]["settings"]["msg"]["exclude_symbols"] = "代碼不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang['libms']['settings']['msg']['duplicated'] = '重複代碼';
$Lang['libms']['settings']['security']['title'] = "保安設定";
$Lang['libms']['settings']['security']['circulation_terminals'] = "流通管理終端機";
$Lang['libms']['settings']['security']['InstructionTitle'] = '使用指引';
$Lang['libms']['settings']['security']['InstructionContent'] = '在 IP 位址的格上, 可以使用:
		<ol>
			<li>固定 IP 位址 (e.g. 192.168.0.101)</li>
			<li>IP 位址一個範圍 (e.g. 192.168.0.[10-100])</li>
			<li>CISCO Style 範圍(e.g. 192.168.0.0/24)</li>
			<li>容許所有位址 (輸入 0.0.0.0)</li>
		</ol>
		<font color="red">建議該電腦設定固定 IP 地址 (即不採用 DHCP), 以確保資料不會被其他電腦更改.</font>
		<br>
		本系統設有安全措施防範假冒的IP 地址 (Faked IP Address).';
$Lang['libms']['settings']['security']['SettingTitle'] = 'IP 位址';
$Lang['libms']['settings']['security']['SettingControlTitle'] = '請把 IP 地址每行一個輸入<br />你現時的 IP 地址';
$Lang["libms"]["reporting"]["title"] = "報告";
$Lang["libms"]["reporting"]["span"]['table'] = "列表";
$Lang["libms"]["reporting"]["span"]['graph'] = "圖表";
$Lang["libms"]["reporting"]["PageReportingOverdue"] = "過期通知";
$Lang["libms"]["reporting"]["PageReportingFine"] = "罰款報告";
$Lang["libms"]["reporting"]["PageReportingCirculation"] = "借閱報告";
$Lang["libms"]["reporting"]["PageReportingReaderRanking"] = "讀者借閱排行榜";
$Lang["libms"]["reporting"]["PageReportingReaderInactive"] = "不借閱讀者報告";
$Lang["libms"]["reporting"]["PageReportingBookRanking"] = "書本借閱排行榜";

$Lang["libms"]["statistics"]["title"] = "統計";
$Lang["libms"]["reporting"]["PageReportingIntragration"] = "綜合統計";
$Lang["libms"]["reporting"]["PageReportingLending"] = "借閱統計";
$Lang["libms"]["reporting"]["PageReportingBookReport"] = "書本報告";
$Lang['libms']['reporting']['PageReportingLostBookReport'] = "報失書本報告";
$Lang['libms']['reporting']['PageReportingPenaltyReport'] = "班級罰款統計";
$Lang['libms']['reporting']['PageReportingCategoryReport'] = "班級分類或科目借閱統計";
$Lang['libms']['reporting']['PageReportingCategoryReport1'] = "班級借閱統計（按圖書分類）";
$Lang['libms']['reporting']['PageReportingCategoryReport2'] = "班級借閱統計（按科目）";
$Lang['libms']['reporting']['PageReportingCategoryReport3'] = "班級借閱統計（按語言）";
$Lang["libms"]["reporting"]["ReadingFrequency"] = "學生閱讀頻次";
$Lang['libms']['reporting']['stock-take'] = "盤點報告";
$Lang['libms']['reporting']['accessionReport'] = "登錄冊";
$Lang['libms']['reporting']['countingMethod'] = "計算方法";
$Lang['libms']['reporting']['countingMethodByBooks'] = "以書本數量計算";
$Lang['libms']['reporting']['countingMethodByVisit'] = "以到館借閱次數計算";
$Lang['libms']['reporting']['countingMode'] = "計算範圍";

$Lang['libms']['reporting']['overdue']['push_message']['title']="圖書過期通知 Library material(s) expired reminder";
$Lang['libms']['reporting']['overdue']['push_message']['content']="貴子弟所借的圖書館資料已到期，請儘快交還有關書籍，並繳交有關罰款。如你已交還有關圖書，請無需理會此通知。
		
				The following library material(s) which your child borrowed has not return before deadline. Please remind your child to return those books and pay for the penalty. Please ignore this notice if you have already returned.";
$Lang['libms']['reporting']['overdue']['push_message']['content_without_penalty']="貴子弟所借的圖書館資料已到期，請儘快交還有關書籍。如你已交還有關圖書，請無需理會此通知。
		
				The following library material(s) which your child borrowed has not return before deadline. Please remind your child to return those books. Please ignore this notice if you have already returned.";
$Lang['libms']['daily']['push_message']['return_date']['title']="圖書到期提示通知 Library material(s) due date reminder";
$Lang['libms']['daily']['push_message']['return_date']['content']="貴子弟所借的圖書館資料快將到期，請按期交還。如你已交還有關圖書，請無需理會此通知。

The following library material(s) which you borrowed will be due for return soon. Please return it/them on time. Please ignore this notice if you have already returned.";
$Lang['libms']['daily']['push_message']['overdue']['title']="圖書到期通知 Library material(s) due date reminder";
$Lang['libms']['daily']['push_message']['overdue']['content']="貴子弟所借的圖書館資料已到期，請今天內交還。如你已交還有關圖書，請無需理會此通知。

The following library material(s) which you borrowed will be due today. Please return it/them today. Please ignore this notice if you have already returned.";

$Lang["libms"]["action"]["book"] = "書目管理";
$Lang["libms"]["action"]["new_book"] = "新增書目";
$Lang["libms"]["action"]["edit_book"] = "編輯書目";

$Lang["libms"]["action"]["item"] = "項目管理";
$Lang["libms"]["action"]["new_item"] = "新增項目";
$Lang["libms"]["action"]["edit_item"] = "編輯項目";

$Lang["libms"]["action"]["periodical"] = "期刊管理";

$Lang["libms"]["open_time"]["not_open_by_group"] = "<p><font color='red' face='simhei'>不可借書！</font></p>這位讀者";
$Lang["libms"]["open_time"]["not_open_yet"] = "<p><font color='red' face='simhei'>不可借書！</font></p>本年/學期";
$Lang["libms"]["open_time"]["system_period"] = "本年/學期開館日期";
$Lang["libms"]["open_time"]["system_period_remark"] = "如有設定，以上日期會控制開始借書日期及最後還書日期；如同時做此設定及本年/學期開館日期（於系統設定 -> 開放時間），系統只會採用這群組設定。";
$Lang["libms"]["open_time"]["system_period_remark2"] = "如有設定，以上日期會控制開始借書日期及最後還書日期；請每學年更新一次。";
$Lang["libms"]["open_time"]["system_period_open"] = "的允許借書日期 (開始)：";
$Lang["libms"]["open_time"]["system_period_end"] = "的允許借書日期 (最後)：";
$Lang["libms"]["open_time"]["system_period_full"] = "的借書時段：";

$Lang["libms"]["open_time"]["weekday"] = "工作日";
$Lang["libms"]["open_time"]["opentime"] = "開館時間";
$Lang["libms"]["open_time"]["closetime"] = "閉館時間";
$Lang["libms"]["open_time"]["day1"] = "星期一";
$Lang["libms"]["open_time"]["day2"] = "星期二";
$Lang["libms"]["open_time"]["day3"] = "星期三";
$Lang["libms"]["open_time"]["day4"] = "星期四";
$Lang["libms"]["open_time"]["day5"] = "星期五";
$Lang["libms"]["open_time"]["day6"] = "星期六";
$Lang["libms"]["open_time"]["day7"] = "星期日";
$Lang["libms"]["open_time"]["close"] = "閉館";
$Lang["libms"]["open_time"]["open"] = "開館";
$Lang["libms"]["open_time"]["time_alertmsg"] = "24小時制(HH:MM:SS)";
$Lang["libms"]["open_time"]["time_timemsg"] = "時間格式輸入錯誤,24小時制(HH:MM:SS)";
$Lang["libms"]["open_time"]["time_hourmsg"] = "小時輸入錯誤,只能輸入0-23數值";
$Lang["libms"]["open_time"]["time_minsmsg"] = "分鐘輸入錯誤,只能輸入0-59數值";
$Lang["libms"]["open_time"]["time_secmsg"] = "秒鐘輸入錯誤,只能輸入0-59數值";
$Lang["libms"]["open_time"]["time_rangemsg"] = "開館時間必須早於閉館時間。";
$Lang["libms"]["S_OpenTime"]["editall"] = "批次編輯";
$Lang["libms"]["S_OpenTime"]["Date"] = "日期 (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["DateFrom"] = "開始日期 (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["DateEnd"] = "結束日期 (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["Time"] = "時間 (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["OpenTime"] = "開館時間 (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["CloseTime"] = "閉館時間 (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["close"] = "閉館";
$Lang["libms"]["S_OpenTime"]["open"] = "開館";
$Lang["libms"]["S_OpenTime"]["dateformat"] = "日期輸入格式(YYYY-MM-DD)";
$Lang["libms"]["open_time"]["datemsg"] = "日期格式輸入錯誤(YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["yearmsg"] = "年份數值錯誤";
$Lang["libms"]["S_OpenTime"]["monthsmsg"] = "月份只能輸入1-12數值";
$Lang["libms"]["S_OpenTime"]["daymsg"] = "日期數值錯誤";
$Lang["libms"]["S_OpenTime"]["remark"] = "<span class='tabletextrequire'>*</span> 如有特別日子需作開放, 請前往「特別開放」設定";
$Lang["libms"]["open_time"]["time_alertmsg"] = "24小時制(HH:MM:SS)";
$Lang["libms"]["holiday"]["Event"] = "假期";
$Lang["libms"]["holiday"]["DateFrom"] = "開始日期";
$Lang["libms"]["holiday"]["DateEnd"] = "結束日期";
$Lang['libms']['holiday']['disclaimer'] = "<span class='tabletextrequire'>*</span> 參照 學校基本設定 > 校曆表 > 公眾假期及學校假期, 如有需要請與eClass系統管理員聯絡 ";
$Lang['libms']['holiday']['disclaimer_ej'] = "<span class='tabletextrequire'>*</span> 參照綜合平台行政管理中心之內聯網管理 > 行政管理 > 校曆表 > 假期, 如有需要請與eClass系統管理員聯絡 ";
$Lang["libms"]["portal"]["book_total"] = "本書";
$Lang["libms"]["circulation_type"]["code"] = "借閱分類代碼";
$Lang["libms"]["circulation_type"]["description_en"] = "英文名稱";
$Lang["libms"]["circulation_type"]["description_b5"] = "中文名稱";
$Lang["libms"]["resources_type"]["code"] = "物品分類代碼";
$Lang["libms"]["resources_type"]["description_en"] = "英文名稱";
$Lang["libms"]["resources_type"]["description_b5"] = "中文名稱";
$Lang["libms"]["book_category"]["code"] = "圖書分類代碼";
$Lang["libms"]["book_category"]["description_en"] = "英文分類名稱";
$Lang["libms"]["book_category"]["description_b5"] = "中文分類名稱";
$Lang["libms"]["book_location"]["code"] = "圖書位置代碼";
$Lang["libms"]["book_location"]["description_en"] = "英文位置名稱";
$Lang["libms"]["book_location"]["description_b5"] = "中文位置名稱";
$Lang["libms"]["responsibility"]["code"] = "著者責任代碼";
$Lang["libms"]["responsibility"]["description_en"] = "英文著者名稱";
$Lang["libms"]["responsibility"]["description_b5"] = "中文著者名稱";
$Lang["libms"]["book_language"]["code"] = "語言分類代碼";
$Lang["libms"]["book_language"]["description_en"] = "英文語言名稱";
$Lang["libms"]["book_language"]["description_b5"] = "中文語言名稱";
$Lang["libms"]["book_language"]["type"] = "圖書分類類型";
$Lang["libms"]["book"]["info"] = "基本資料";
$Lang["libms"]["book"]["info_category"] = "分類資料";
$Lang["libms"]["book"]["info_circulation"] = "借閱資料";
$Lang["libms"]["book"]["info_publish"] = "出版資料";
$Lang["libms"]["book"]["info_purchase"] = "購買資料";
$Lang["libms"]["book"]["info_other"] = "其他資料";
$Lang["libms"]["book"]["info_book"] = "書本條碼";
$Lang["libms"]["book"]["code"] = "登錄號碼";
$Lang["libms"]["book"]["title"] = "書名";
$Lang["libms"]["book"]["subtitle"] = "副題";
$Lang["libms"]["book"]["introduction"] = "書本簡介";
$Lang["libms"]["book"]["call_number"] = "索書號";
$Lang["libms"]["book"]["call_number_desc"] = "分類號";
$Lang["libms"]["book"]["call_number2"] = "索書號２";
$Lang["libms"]["book"]["call_number2_desc"] = "著者碼";
$Lang["libms"]["book"]["ISBN"] = "ISBN";
$Lang["libms"]["book"]["ISBN2"] = "ISBN２";
$Lang["libms"]["book"]["barcode"] = "條碼";
$Lang["libms"]["book"]["language"] = "語言";
$Lang["libms"]["book"]["country"] = "國家";
$Lang["libms"]["book"]["edition"] = "版本";
$Lang["libms"]["book"]["publisher"] = "出版商";
$Lang["libms"]["book"]["publish_place"] = "出版地";
$Lang["libms"]["book"]["publish_year"] = "出版年份";
$Lang["libms"]["book"]["DIMEN"] = "高廣、尺寸";
$Lang["libms"]["book"]["ILL"] = "圖片";
$Lang["libms"]["book"]["AccompanyMaterial"] = "附件";
$Lang["libms"]["book"]["number_of_page"] = "頁數";
$Lang["libms"]["book"]["number_of_copy"] = "書量";
$Lang["libms"]["book"]["number_of_copy_available"] = "可借書量";
$Lang["libms"]["book"]["book_status"] = "項目狀態";
$Lang["libms"]["book"]["purchase_date"] = "購買日期";
$Lang["libms"]["book"]["distributor"] = "經銷商";
$Lang["libms"]["book"]["purchase_note"] = "購買備註";
$Lang["libms"]["book"]["purchase_by_department"] = "購買部門";
$Lang["libms"]["book"]["list_price"] = "訂價";
$Lang["libms"]["book"]["discount"] = "折扣";
$Lang["libms"]["book"]["purchase_price"] = "購買價格";
$Lang["libms"]["book"]["account_date"] = "登錄日期";
$Lang["libms"]["book"]["subject"] = "科目";
$Lang["libms"]["book"]["series"] = "集叢";
$Lang["libms"]["book"]["series_number"] = "集叢編號";
$Lang["libms"]["book"]["book_series_number"] = "書目集叢編號";
$Lang["libms"]["book"]["item_series_number"] = "項目集叢編號";
$Lang["libms"]["book"]["responsibility_code1"] = "著作方式１";
$Lang["libms"]["book"]["responsibility_by1"] = "著者名１";
$Lang["libms"]["book"]["responsibility_code2"] = "著作方式２";
$Lang["libms"]["book"]["responsibility_by2"] = "著者名２";
$Lang["libms"]["book"]["responsibility_code3"] = "著作方式３";
$Lang["libms"]["book"]["responsibility_by3"] = "著者名３";
$Lang["libms"]["book"]["remark_internal"] = "備註（給管理員）";
$Lang["libms"]["book"]["remark_to_user"] = "借還提示";
$Lang["libms"]["book"]["URL"] = "相關網址";
$Lang["libms"]["book"]["cover_image"] = "封面圖像";
$Lang["libms"]["book"]["delete_cover_image"] = "刪除封面圖像";
$Lang["libms"]["book"]["borrow_reserve"] = "允許";
$Lang["libms"]["book"]["open_borrow"] = "借閱";
$Lang["libms"]["book"]["open_reserve"] = "預借";
$Lang["libms"]["book"]["tags"] = "標籤字眼";
$Lang["libms"]["book"]["tag_books"] = "書目";
$Lang["libms"]["book"]["last_modified"] = "最後修改時間";
$Lang["libms"]["book"]["multiple_selection"] = "可多選";
$Lang["libms"]["book"]["book_total"] = "本書目";
$Lang["libms"]["book"]["warning_no_new_tag"] = "請輸入新名稱！";
$Lang["libms"]["book"]["warning_tag_edit_confirm"] = "你是否確定要進行修改？";
$Lang["libms"]["book"]["title_origin"] = "原本名稱";
$Lang["libms"]["book"]["title_new"] = "新名稱";
$Lang["libms"]["book"]["tags_input"] = "*請用[Enter]鍵新增分隔";
$Lang["libms"]["book"]["add"] = "新增";
$Lang["libms"]["book"]["delete"] = "删除";
$Lang["libms"]["book"]["code_format"] = "必須英文字母加數字，如 \"FICTION000021\"";
$Lang["libms"]["book"]["lost_date"] = "報失日期";
$Lang["libms"]["book"]["user_name"] = "讀者";
$Lang["libms"]["book"]["class"] = "班別班號";
$Lang["libms"]["book"]["reserve_time"] = "預約時間";
$Lang["libms"]["book"]["reserve_sum"] = "預約人數";
$Lang['libms']['book']['all_cir_type'] = "全部借閱類別";
$Lang['libms']['book']['all_res_type'] = "全部物品類別";
$Lang['libms']['book']['invoice_number'] = "發票編號";
$Lang['libms']['book']['use_acno_as_barcode'] = "使用登錄號碼";
$Lang['libms']['book']['item_account_date'] = "登錄日期";
$Lang['libms']['book']['view_item'] = "檢視項目";
$Lang["libms"]["book"]["setting"]["filter_submit"] = '套用'; 
$Lang["libms"]["book"]["alert"]["numeric"] = '請輸入正確數字';
$Lang["libms"]["book"]["alert"]["item_book_title"] = '書名不正確';
$Lang["libms"]["book"]["alert"]["incorrect_anco"] = '登錄號碼不正確';
$Lang["libms"]["book"]["alert"]["fillin_anco"] = '請填寫登錄號碼';
$Lang['libms']['book']['select']['Book_Status'] = '圖書狀態';
$Lang['libms']['book']['select']['BookCategoryCode'] = '圖書分類';
$Lang['libms']['book']['select']['ResourcesTypeCode'] = '物品分類';
$Lang['libms']['book']['select']['CirculationTypeCode'] = '借閱分類';
$Lang['libms']['book']['btn']['SubmitAndAddItem'] = "呈送及新增項目";
$Lang['libms']['book']['status']['remark'] = "代表該項目的書目已被設定為不允許借閱（不論項目是什麼狀態）；如有需要，可編輯書目來修改該設定。";
$Lang['libms']['book']['iteminfo'] = "館藏信息";

$Lang['libms']['book']['reservation_disallowed'] = "書目不允許預約";
$Lang['libms']['book']['borrow_disallowed'] = "書目不允許借出";
$Lang['libms']['book']['updateBookStatusTo'] = "更新圖書狀態至";
$Lang['libms']['book']['expiredAndPassedToNext'] = "逾期有下一位預約";
$Lang['libms']['book']['expired'] = "逾期未取";
$Lang['libms']['book']['allRecord'] = "全部紀錄";
$Lang["libms"]["book"]["item_remark_to_user"] = "項目借還提示";
$Lang["libms"]["book"]["importMarc21"] = "加入Marc21";
$Lang["libms"]["book"]["showOnlyBookCoverRecord"] = "顯示有圖書封面的紀錄";

$Lang["libms"]["label"]["labelformat_list"] = "標籤格式";
$Lang["libms"]["label"]["name"] = "標籤名稱";
$Lang["libms"]["label"]["paper-size-width"] = "紙張闊度";
$Lang["libms"]["label"]["paper-size-height"] = "紙張高度";
$Lang["libms"]["label"]["NX"] = "一列標籤數量";
$Lang["libms"]["label"]["NY"] = "一欄標籤數量";
$Lang["libms"]["label"]["metric"] = "量度單位";
$Lang["libms"]["label"]["metric_inch"] = "英寸";
$Lang["libms"]["label"]["metric_mm"] = "毫米";
$Lang["libms"]["label"]["unit"]["in"] = "英寸";
$Lang["libms"]["label"]["unit"]["mm"] = "毫米";
$Lang["libms"]["label"]["text_alignment"] = "文本對齊";
$Lang["libms"]["label"]["text_alignment_option"]["L"] = "左邊";
$Lang["libms"]["label"]["text_alignment_option"]["C"] = "中間";
$Lang["libms"]["label"]["text_alignment_option"]["R"] = "右邊";
$Lang["libms"]["label"]["piece"] = "張";
$Lang["libms"]["label"]["SpaceX"] = "標籤之間相距闊度";
$Lang["libms"]["label"]["SpaceY"] = "標籤之間相距高度";
$Lang["libms"]["label"]["width"] = "標籤寬度";
$Lang["libms"]["label"]["height"] = "標籤高度";
$Lang["libms"]["label"]["font_size"] = "字體大小";
$Lang["libms"]["label"]["is_bold"] = "粗體字型";
$Lang["libms"]["label"]["is_bold_remark"] = "(只適用於書脊標籤)";
$Lang["libms"]["label"]["printing_margin_h"] = "標籤邊緣和文字之間相距闊度";
$Lang["libms"]["label"]["printing_margin_v"] = "標籤邊緣和文字之間相距高度";
$Lang["libms"]["label"]["max_barcode_width"] = "條碼闊度";
$Lang["libms"]["label"]["max_barcode_height"] = "條碼高度";
$Lang["libms"]["label"]["lMargin"] = "紙張和標籤左邊相距空間";
$Lang["libms"]["label"]["tMargin"] = "紙張和標籤上方相距空間";
$Lang["libms"]["label"]["input_msg"] = "請輸入數字e.g.(10.75)";
$Lang["libms"]["label"]["lineHeight"] = "行距";
$Lang["libms"]["label"]["info_order"] = "列印資料次序 (只適用於書本條碼及書脊標籤)";
$Lang["libms"]["label"]["BookTitle"] = "書名 ";
$Lang["libms"]["label"]["LocationCode"] = "位置";
$Lang["libms"]["label"]["CallNumCallNum2"] = "索書號";
$Lang["libms"]["label"]["BarCode"] = "條碼";
$Lang["libms"]["label"]["BookCode"] = "登錄號碼";
$Lang["libms"]["label"]["print_school_name"] = "學校名稱";
$Lang["libms"]["label"]['msg']["labelInfoError"] = "列印資料次序不能與其他相同！";
$Lang["libms"]["label"]["msg"]["labelInfoErrorFillAll"] = "請選擇所有列印資料次序，否則請把所有列印資料次序設定為不適用。";
$Lang["libms"]["label"]["msg"]["no_template"] = "請先於 [標貼格式] 裡建立標籤格式!";
$Lang["libms"]["export_book"]["select_all_remark"] = "修改資料後可再匯入至系統";
$Lang['libms']['import']['msg']['error_required_missing']='必要格不可留空 格：';
$Lang['libms']['import']['msg']['error_format_mismatch']='格式不符 格：';
$Lang['libms']['import']['msg']['dup_key'] = '重複代碼 格：';

$Lang["libms"]["import_book"]["file_format"] = "匯入檔案格式ISO2709";
$Lang["libms"]["import_book"]["file_charset"] = "檔案的字符集";
$Lang["libms"]["import_book"]["BIG5"] = "BIG5";
$Lang["libms"]["import_book"]["BIG5HKSCS"] = "BIG5HKSCS";
$Lang["libms"]["import_book"]["UTF8"] = "UTF-8";
$Lang["libms"]["import_book"]["matched_by_acno"] = "按照 ACNO 登錄號碼";
$Lang["libms"]["import_book"]["total_record"] = "紀錄";
$Lang["libms"]["import_book"]["total_record_modified"] = "更新紀錄";
$Lang["libms"]["import_book"]["preview"] = "預覽";
$Lang["libms"]["import_book"]["title"] = "書名:";
$Lang["libms"]["import_book"]["author"] = "著者:";
$Lang["libms"]["import_book"]["upload_fail"] = "上載不成功:";
$Lang["libms"]["import_book"]["contact_admin"] = ",請與系統系統管理員聯繫";
$Lang["libms"]["import_book"]["processing_db"] = "正匯入到資料庫:";
$Lang["libms"]["import_book"]["process_count"] = "匯入數量:";
$Lang["libms"]["import_book"]["upload_success_file_ready_to_import"] = "完成檢定，可以匯入檔案數量: ";
$Lang["libms"]["import_book"]["upload_success_ready_to_import"] = "完成檢定，可以匯入數量: ";
$Lang["libms"]["import_book"]["upload_fail_ready_to_import"] = "不可匯入數量: ";
$Lang["libms"]["import_book"]["Result"]["Success"] = "匯入完成";
$Lang["libms"]["import_book"]["Result"]["Fail"] = "匯入失敗";
$Lang["libms"]["import_book"]["TypeInvalid"] = "種類無效。";
$Lang["libms"]["import_book"]["DataMissing"] = "資料不足。";
$Lang["libms"]["import_book"]["BookCodeAlreadyExist"] = "登錄號碼已存在";
$Lang["libms"]["import_book"]["ACNO_not_found"] = "找不到相關登錄號碼";
$Lang["libms"]["import_book"]["ACNO_no_AUTO"] = "不可使用 \"AUTO\", 必須輸入已有紀錄的 ACNO";
$Lang["libms"]["import_book"]["BarcodeAlreadyExist"] = "條碼已存在";
$Lang["libms"]["import_book"]["BarcodeAlreadyBeUsed"] = "條碼已被其他書本使用了";
$Lang["libms"]["import_book"]["BarcodeMissing"] = "必須輸入條碼";
$Lang["libms"]["import_book"]["BarcodeWrongFormat"] = "條碼格式不符";
$Lang["libms"]["import_book"]["AncoWrongFormat"] = "登錄號碼格式不符";
$Lang["libms"]["import_book"]["BookTitleEmpty"] = "書名資料不足";
$Lang["libms"]["import_book"]["ResponsibilityNotExist"] = "著作方式資料不符";
$Lang["libms"]["import_book"]["BookCategoryNotExist"] = "圖書分類資料不符";
$Lang["libms"]["import_book"]["BookCirclationNotExist"] = "借閱分類資料不符";
$Lang["libms"]["import_book"]["BookResourcesNotExist"] = "物品分類資料不符";
$Lang["libms"]["import_book"]["BookLocationNotExist"] = "館藏位置資料不符";
$Lang["libms"]["import_book"]["alreadyexist"] = "已經存在。";
$Lang["libms"]["import_book"]["doubleused"] = "重複使用。";
$Lang["libms"]["import_book"]["CancelCheck"] = "是否確定取消檢測？";
$Lang["libms"]["import_book"]["AlreadyChecked"] = "已檢測：";
$Lang["libms"]["import_book"]["StartToChecking"] = "現在開始檢測。。。";
$Lang["libms"]["import_book"]["CancelImport"] = "是否確定取消匯入？";
$Lang["libms"]["import_book"]["AlreadyImportedBooks"] = "已匯入書目：";
$Lang["libms"]["import_book"]["AlreadyImportedItems"] = "已匯入項目：";
$Lang["libms"]["import_book"]["StartToImporting"] = "現在開始匯入。。。";
$Lang["libms"]["import_book"]["CheckImport"] = "檢測完成，現在可以匯入。。。";
$Lang["libms"]["import_book"]["ImportMarc21Alert"] = "以下顯示了系統讀到的資料，請檢查有沒有問題，然後才繼續匯入。";
$Lang["libms"]["import_book"]["FirstBook"] = "第一本書：  ";
$Lang["libms"]["import_book"]["SecondBook"] = "第二本書： ";
$Lang["libms"]["import_book"]["ThirdBook"] = "第三本書： ";
$Lang["libms"]["import_book"]["ContinueImport"] = "繼續匯入";
$Lang["libms"]["import_book"]["ISBNWrongFormat"] = "ISBN號碼格式不符";
$Lang["libms"]["import_book"]["ISBN2WrongFormat"] = "ISBN2號碼格式不符";
$Lang["libms"]["import_book"]["acnoautofill"] = "<span class='tabletextrequire'>@</span> 登錄號碼如未輸入．系統會自動產生。<br /> <br /> <span class='tabletextrequire'>+</span> 如要系統會產生條碼，請填上'AUTO'。如使用登錄號碼作為條碼，請填上'ACNO'。";
//$Lang["libms"]["import_book"]["ImportCSVDataCol_New"] = array("<font color = red>*</font>書目","副題","<font color = red>*@</font>登錄號碼","索書號","索書號2","ISBN","ISBN2","語言","國家","書本簡介","版本","出版年份","出版商","出版地","集叢","集叢編號","<font color = red>&</font>著作方式１","著者名１","<font color = red>&</font>著作方式２","著者名２","<font color = red>&</font>著作方式３","著者名３","頁數","<font color = red>&</font>圖書分類","<font color = red>&</font>借閱分類","<font color = red>&</font>物品分類","<font color = red>&</font>館藏位置","科目","登錄日期","備註","購買日期","購買價格","訂價","折扣","經銷商","購買部門","購買備註","書量","允許借閱","允許預借","借還提示","標籤字眼","相關網址","<font color = red>+</font>條碼1","<font color = red>+</font>條碼2","<font color = red>+</font>條碼3","<font color = red>+</font>條碼4","<font color = red>+</font>條碼5","<font color = red>+</font>條碼6","<font color = red>+</font>條碼7","<font color = red>+</font>條碼8","<font color = red>+</font>條碼9","<font color = red>+</font>條碼10","<font color = red>+</font>條碼11","<font color = red>+</font>條碼12","<font color = red>+</font>條碼13","<font color = red>+</font>條碼14","<font color = red>+</font>條碼15","<font color = red>+</font>條碼16","<font color = red>+</font>條碼17","<font color = red>+</font>條碼18","<font color = red>+</font>條碼19","<font color = red>+</font>條碼20");
$Lang["libms"]["import_book"]["ImportCSVDataCol_New2"] = array("<font color = red>*</font>書目",
"副題",
"索書號",
"索書號2",
"ISBN",
"ISBN2",
"語言",
"國家",
"書本簡介",
"版本",
"出版年份",
"出版商",
"出版地",
"集叢",
"集叢編號",
"著作方式１  <a href='javascript:viewCodes(1)' class='tablelink'>[按此查詢代碼]</a>",
"著者名１",
"著作方式２  <a href='javascript:viewCodes(1)' class='tablelink'>[按此查詢代碼]</a>",
"著者名２",
"著作方式３  <a href='javascript:viewCodes(1)' class='tablelink'>[按此查詢代碼]</a>",
"著者名３",
"高廣、尺寸",
"圖片",
"頁數",
"圖書分類  <a href='javascript:viewCodes(2)' class='tablelink'>[按此查詢代碼]</a>",
"借閱分類  <a href='javascript:viewCodes(3)' class='tablelink'>[按此查詢代碼]</a>",
"物品分類  <a href='javascript:viewCodes(4)' class='tablelink'>[按此查詢代碼]</a>",
"科目",
"備註",
"允許借閱  <span class='tabletextremark'>（1 - 允許, 0 - 不允許）</span>",
"允許預借  <span class='tabletextremark'>（1 - 允許, 0 - 不允許）</span>",
"借還提示(書目)",
"標籤字眼",
"相關網址",
"<font color = red>*@</font>登錄號碼 <span id='acno_type1'>（若只想匯入書目／未有項目，留空此欄及後面所有欄）</span> <span id='acno_type2' style='display:none;'><font color='red'>（必須輸入 ）</font></span> ",
"<font color = red>*+</font>條碼",
"館藏位置 <a href='javascript:viewCodes(5)' class='tablelink'>[按此查詢代碼]</a>",
"<font color = red>#</font>登錄日期",
"<font color = red>#</font>購買日期",
"購買價格",
"訂價",
"折扣",
"經銷商",
"購買部門",
"購買備註",
"發票編號",
"附件",
"借還提示(項目)",
"<font color='red'>^</font>項目狀態   <span class='tabletextremark'>（預設值：可借出）</span> <a href='javascript:viewCodes(6)' class='tablelink'>[按此查詢代碼]</a>",
"項目借閱分類  <a href='javascript:viewCodes(3)' class='tablelink'>[按此查詢代碼]</a> (留空表示跟隨書目之借閱分類)",
"項目集叢編號");
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookTitle"] = "書目";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookSubtitle"] = "副題";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["CallNum"] = "索書號";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["CallNum2"] = "索書號2";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ISBN"] = "ISBN";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ISBN2"] = "ISBN2";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Language"] = "語言";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Country"] = "國家";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Introduction"] = "書本簡介";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Edition"] = "版本";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["PublishYear"] = "出版年份";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Publisher"] = "出版商";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["PublishPlace"] = "出版地";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Series"] = "集叢";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["SeriesNum"] = "集叢編號";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemSeriesNum"] = "項目集叢編號";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityCode1"] = "著作方式１";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityBy1"] = "著者名１";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityCode2"] = "著作方式２";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityBy2"] = "著者名２";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityCode3"] = "著作方式３";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResponsibilityBy3"] = "著者名３";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Dimension"] = "高廣、尺寸";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ILL"] = "圖片";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["NoOfPage"] = "頁數";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookCategoryCode"] = "圖書分類";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["CirculationTypeCode"] = "借閱分類";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ResourcesTypeCode"] = "物品分類";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Subject"] = "科目";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookInternalremark"] = "備註";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["OpenBorrow"] = "允許借閱";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["OpenReservation"] = "允許預借";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["BookRemarkToUser"] = "借還提示(書目)";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Tags"] = "標籤字眼";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["URL"] = "相關網址";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ACNO"] = "登錄號碼";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["BarCode"] = "條碼";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["LocationCode"] = "館藏位置";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["AccountDate"] = "登錄日期";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchaseDate"] = "購買日期";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchasePrice"] = "購買價格";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ListPrice"] = "訂價";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Discount"] = "折扣";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["Distributor"] = "經銷商";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchaseByDepartment"] = "購買部門";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["PurchaseNote"] = "購買備註";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["InvoiceNumber"] = "發票編號";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["AccompanyMaterial"] = "附件";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemRemarkToUser"] = "借還提示(項目)";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemStatus"] = "項目狀態";
$Lang["libms"]["import_book"]["ImportCSVDataCol"]["ItemCirculationTypeCode"] = "項目借閱分類";
$Lang["libms"]["import_book"]["Code"] = "代碼";
$Lang["libms"]["import_book"]["TitleEng"] = "英文名稱";
$Lang["libms"]["import_book"]["TitleChi"] = "中文名稱";

$Lang["libms"]["import_book"]["CannotChangeFromWriteoff"] = "不能更改註銷項目的狀態";
$Lang["libms"]["import_book"]["CannotChangeToWriteoff"] = "不能更改項目狀態至註銷";
$Lang["libms"]["import_book"]["LastUpdateDate"] = "格式最後更改日期";
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode1"] = "著作方式1不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode2"] = "著作方式2不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode3"] = "著作方式3不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookCategory"] = "圖書分類代號不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookCirclation"] = "借閱分類代號不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookResources"] = "物品分類代號不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookLocation"] = "館藏位置代號不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ItemCirculationTypeCode"] = "項目借閱分類代號不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["Language"] = "語言不能包含這些符號 ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["Warning"]["StatusChange"] = "項目狀態由%s更改到%s";
$Lang["libms"]["import_book"]["Warning"]["StatusChange2"] = "項目狀態更改到%s";
$Lang["libms"]["batch_edit"]["ByReturnDate"] = "按到期日期";
$Lang["libms"]["batch_edit"]["ByBorrowDate"] ="按借書日期";

$Lang["libms"]["batch_edit"]["ByBarcode"] ="按條碼";
$Lang["libms"]["batch_edit"]["ByACNO"] ="按登錄號碼";
$Lang["libms"]["batch_edit"]["ByACNOList"] ="按登錄號碼清單";
$Lang["libms"]["batch_edit"]["ByAccountDate"] ="按登錄日期";
$Lang["libms"]["batch_edit"]["ByOthers"] ="按其他條件";
$Lang["libms"]["batch_edit"]["PleaseFillAboveField"] ="請填上以上項目";
$Lang["libms"]["batch_edit"]["PleaseSelect"] = "請選擇";
$Lang["libms"]["batch_edit"]["AllowBorrow"] = "允許借閱";
$Lang["libms"]["batch_edit"]["AllowReserve"] = "允許預借";
$Lang["libms"]["batch_edit"]["Apply"] = "套用";
$Lang["libms"]["batch_edit"]["Yes"] = "是";
$Lang["libms"]["batch_edit"]["ConfirmUpdate"] = "是否確定更改";
$Lang["libms"]["batch_edit"]["InputKeywordToSearch"] = "請輸入關鍵字來搜尋";
$Lang["libms"]["batch_edit"]["SelectAtLeastOneClassLevel"] = "請選取至少一個班級";
$Lang["libms"]["batch_edit"]["SelectAtLeastOneItem"] = "請選取至少一個項目";
$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateSuccess"] = "項紀錄已經更新";
$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateUnsuccess"] = "0|=|更新紀錄失敗";
$Lang["libms"]["batch_edit"]["PanelTitle"]["BookSearch"] = "書目搜尋";
$Lang["libms"]["batch_edit"]["PanelTitle"]["ItemSearch"] = "項目搜尋";
$Lang["libms"]["batch_edit"]["PanelTitle"]["SelectedRecord"] = "選擇紀錄";
$Lang["libms"]["batch_edit"]["PanelTitle"]["ChangeRecord"] = "修改紀錄";
$Lang["libms"]["batch_edit"]["InvalidDateFormat"] = "日期格式錯誤";
$Lang["libms"]["batch_edit"]["ClearList"] = "清除所有紀錄";
$Lang["libms"]["batch_edit"]["ConfirmClearList"] = "是否確定清除所有紀錄?";
$Lang["libms"]["batch_edit"]["ConfirmClearListForSearch"] = "新的搜尋會清除所有紀錄,是否確定?";
$Lang["libms"]["batch_edit"]["msg"]["no_barcode"] = "找不到這條碼";
$Lang["libms"]["batch_edit"]["msg"]["scanned"] = "此書已被掃描";
$Lang["libms"]["batch_edit"]["Author"] = "著者";
$Lang["libms"]["batch_edit"]["BookFormat"] = "類別";
$Lang["libms"]["batch_edit"]["BookTitle"] = "書名";
$Lang["libms"]["batch_edit"]["CallNumber"] = "索書號";
$Lang["libms"]["batch_edit"]["ClassLevel"] = "級別";
$Lang["libms"]["batch_edit"]["Description"] = "推介原因";
$Lang["libms"]["batch_edit"]["RecommendedBy"] = "推介人";
$Lang["libms"]["batch_edit"]["RecommendedLevel"] = "推介級別";
$Lang["libms"]["batch_edit"]["RecommendedOn"] = "推介日期";
$Lang["libms"]["batch_edit"]["RecommendRemark"] = "附有「<span class='tabletextrequire'>*</span>」的書目表示已經有推介,呈送後會覆寫原有的推介內容!";
$Lang["libms"]["batch_edit"]["OneACNOPerRow"] = "每行只輸入一個登錄號碼";
$Lang["libms"]["pre_label"]["print_label_border"] = "列印標籤邊框";
$Lang["libms"]["pre_label"]["bookcode"] = "登錄號碼";
$Lang["libms"]["pre_label"]["bookcode_number"] = "登錄號碼數字";
$Lang["libms"]["pre_label"]["search"] = "查詢";
$Lang["libms"]["pre_label"]["add"] = "增加";
$Lang["libms"]["pre_label"]["bookid"] = "書本編號";
$Lang["libms"]["pre_label"]["barcode"] = "書本條碼";
$Lang["libms"]["pre_label"]["spine_label"] = "書脊標籤";
$Lang["libms"]["pre_label"]["callno"] = "索書號";
$Lang["libms"]["pre_label"]["callno2"] = "索書號2";
$Lang["libms"]["pre_label"]["location"] = "位置";
$Lang["libms"]["pre_label"]["school_name"] = "學校名稱";
$Lang["libms"]["pre_label"]["title"] = "書名";
$Lang["libms"]["pre_label"]["toogle"] = "切換";
$Lang["libms"]["pre_label"]["label_format"] = "標籤格式";
$Lang["libms"]["pre_label"]["label_new_format"] = "新增標籤格式";
$Lang["libms"]["pre_label"]["label_edit_format"] = "標籤格式管理";
$Lang["libms"]["pre_label"]["no_match"] = "找不到相關記錄";
$Lang["libms"]["pre_label"]["select_label"] = "選擇標籤";
$Lang["libms"]["pre_label"]["print_value"] = "請選擇列印資料";
$Lang["libms"]["pre_label"]["print_book"] = "請選擇列印書籍";
$Lang["libms"]["pre_label"]["action"] = "執行";
$Lang["libms"]["pre_label"]["prefix_not_match"] = "登錄號碼字首不相符";
$Lang["libms"]["pre_label"]["search_comment"] = "*注意-過多標籤數量可能會使瀏覽器癱瘓";
$Lang['libms']['pre_label']['ReaderBarcode'] = "讀者標貼";
$Lang['libms']['pre_label']['PleaseSelectAGroup'] = "請選擇一個群組";
$Lang['libms']['pre_label']['PleaseSelectReader'] = "請選擇讀者";
$Lang['libms']['pre_label']['BarcodeNo'] = "條碼編號";
$Lang['libms']['pre_label']['StartPosition'] = "起始位置";

$Lang["libms"]["gen_label"]["no_book_selected"] = "錯誤:未揀選書籍";
$Lang["libms"]["gen_label"]["no_label_format"] = "錯誤:未揀選標籤格式";
$Lang["libms"]["gen_label"]["no_print_fields"] = "錯誤:沒有列印的資料";
$Lang["libms"]["group_management"] = "讀者群組管理";
$Lang["libms"]["FormClassMapping"]["CtrlMultiSelectMessage"] = "按CTRL選擇更多";
$Lang["libms"]["FormClassMapping"]["Or"] = "或";
$Lang["libms"]["GroupManagement"]["AccessRight"] = "進入權限";
$Lang["libms"]["GroupManagement"]["AddUser"] = "增加使用者";
$Lang["libms"]["GroupManagement"]["Alumni"] = "校友";
$Lang["libms"]["GroupManagement"]["BelongToSameForm"] = "屬於相同級別";
$Lang["libms"]["GroupManagement"]["BelongToSameFormClass"] = "屬於相同班別";
$Lang["libms"]["GroupManagement"]["BelongToSameGroup"] = "屬於相同群組";
$Lang["libms"]["GroupManagement"]["BelongToSameSubject"] = "屬於相同科目";
$Lang["libms"]["GroupManagement"]["BelongToSameSubjectGroup"] = "屬於相同科目群組";
$Lang["libms"]["GroupManagement"]["CanSendTo"] = "可以發送";
$Lang["libms"]["GroupManagement"]["ClassName"] = "班別";
$Lang["libms"]["GroupManagement"]["ClassNumber"] = "學號";
$Lang["libms"]["GroupManagement"]["ClassLevel"] = "班級";
$Lang["libms"]["GroupManagement"]["Delete"] = "删除";
$Lang["libms"]["GroupManagement"]["ExportGroupDetail"] = "匯出群組細節";
$Lang["libms"]["GroupManagement"]["Identity"] = "用戶分類";
$Lang["libms"]["GroupManagement"]["ModuleTitle"] = "群組管理";
$Lang["libms"]["GroupManagement"]["ClassManagement"] = "班別管理";
$Lang["libms"]["GroupManagement"]["Name"] = "名稱";
$Lang["libms"]["GroupManagement"]["New"] = "加入";
$Lang["libms"]["GroupManagement"]["NewGroup"] = "新群組";
$Lang["libms"]["GroupManagement"]["Parent"] = "家長";
$Lang["libms"]["GroupManagement"]["PrintGroupDetail"] = "列印群組細節";
$Lang["libms"]["GroupManagement"]["RemoveThisGroup"] = "删除這群組";
$Lang["libms"]["GroupManagement"]["GroupList"] = "群組清單";
$Lang["libms"]["GroupManagement"]["GroupName"] = "群組名稱";
$Lang["libms"]["GroupManagement"]["GroupTitle"] = "群組名稱";
$Lang["libms"]["GroupManagement"]["SearchUser"] = "搜尋用戶";
$Lang["libms"]["GroupManagement"]["SelectAll"] = "全選";
$Lang["libms"]["GroupManagement"]["SelectedUser"] = "選擇使用者";
$Lang["libms"]["GroupManagement"]["SelectIdentity"] = "選擇特徵";
$Lang["libms"]["GroupManagement"]["Student"] = "學生";
$Lang["libms"]["GroupManagement"]["SubmitAndEdit"] = "提交再修改";
$Lang["libms"]["GroupManagement"]["SupportStaff"] = "非教學職務員工";
//$Lang["libms"]["GroupManagement"]["Targeting"] = "Targeting";
$Lang["libms"]["GroupManagement"]["TeachingStaff"] = "教師";
$Lang["libms"]["GroupManagement"]["ToAll"] = "全部";
$Lang["libms"]["GroupManagement"]["ToAllUser"] = "全部用戶";
$Lang["libms"]["GroupManagement"]["TopManagement"] = "權限管理";
$Lang["libms"]["GroupManagement"]["UncheckForMoreOptions"] = "取消更多選項";
$Lang["libms"]["GroupManagement"]["UserList"] = "用戶清單";
$Lang["libms"]["GroupManagement"]["Users"] = "用戶們";
$Lang["libms"]["GroupManagement"]["UserType"] = "用戶種類";
$Lang["libms"]["SubjectClassMapping"]["SelectClass"] = "選擇班別";
$Lang["libms"]["GroupManagement"]["GroupNameDuplicateWarning"] = "警告:群組名稱相同";
$Lang["libms"]["GroupManagement"]["GroupCodeDuplicateWarning"] = "警告:群組編號相同";
//sqlfeildname//$Lang["libms"]["SQL"]["UserNameFeild"] = "ChineseName";
$Lang["libms"]["SQL"]["CirDescription"] = "DescriptionChi";
//endofSQL$Lang["libms"]["GroupManagement"]["GroupTitle"] = "群組名稱";
$Lang["libms"]["GroupManagement"]["GroupCode"] = "群組編號";
$Lang["libms"]["GroupManagement"]["IsDefault"] = "是否預設";
$Lang["libms"]["GroupManagement"]["GroupSavedUnsuccess"] = "0|=|群組儲存不成功";
$Lang["libms"]["GroupManagement"]["GroupSavedSuccess"] = "1|=|群組儲存成功";
$Lang["libms"]["GroupManagement"]["GroupCodeUpdateUnsuccess"] = "0|=|群組編號更新不成功";
$Lang["libms"]["GroupManagement"]["GroupCodeUpdateSuccess"] = "1|=|群組編號更新成功";
$Lang["libms"]["GroupManagement"]["GroupRemoveUnsuccess"] = "0|=|删除群組不成功";
$Lang["libms"]["GroupManagement"]["GroupRemoveSuccess"] = "1|=|删除群組成功";
$Lang["libms"]["GroupManagement"]["DeleteWarning"] = "你是否確定要删除該紀錄?";
$Lang["libms"]["GroupManagement"]["DefaultRule"] = "預設規則";
$Lang["libms"]["GroupManagement"]["GroupRule"] = "借還規則";
$Lang["libms"]["GroupManagement"]["GroupPeriod"] = "時段 (可選擇)";
$Lang["libms"]["GroupManagement"]["ClassManagementMembers"] = "班管管理員";
$Lang["libms"]["GroupManagement"]["ImportUserBarcodeColumns"] = array("<font color = red>*</font>內聯網帳號","<font color = red>*</font>條碼");
$Lang["libms"]["GroupManagement"]["ImportUserBarcode"] = "匯入用戶條碼";
$Lang["libms"]["GroupManagement"]["Import"]["UserBarcode"] = "用戶條碼";
$Lang["libms"]["GroupManagement"]["Import"]["UserLogin"] = "內聯網帳號";
########### Admin ###############
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["name"] = "管理員";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["settings"] = "系統設定";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["group management"] = "讀者群組管理";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["stock-take and write-off"] = "盤點及註銷";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["stock-take"] = "盤點";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["write-off"] = "註銷";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["book management"] = "書本管理 (書目、項目登錄、期刊、批量修改、標貼、現時預約及報失後歸還）";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["portal settings"] = "前端主頁設定";
########### Admin ###############

$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["name"] = "流通管理";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["access_without_open_hour"] = "&nbsp;- 閉館時存取";
$Lang['libms']['GroupManagement']['Right']['circulation management']['not_allow_class_management_circulation'] = '&nbsp;- 不允許借還班管位置的書';
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["borrow"] = "借書";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["reserve"] = "預約";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["return"] = "還書";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["renew"] = "續借";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["overdue"] = "過期罰款";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["batch_return_and_renew"] = "批量還書及續借";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["lost"] = "報失";

##### Report Start#####
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["name"] = "報告";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["daily summary"] = "每日總結";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["overdue"] = "過期";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["income"] = "收入";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["borrow records"] = "借出記錄";

$Lang["libms"]["GroupManagement"]["Right"]["reports"]["accession report"] = "登錄冊";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["book report"] = "書本報告";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["loan report"] = "借閱報告";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["overdue report"] = "過期通知";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["penalty report"] = "罰款報告";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["lost report"] = "報失書本報告";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["reader ranking"] = "讀者借閱排行榜";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["inactive reader"] = "不借閱讀者報告";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["book ranking"] = "書本借閱排行榜";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["stock-take report"] = "盤點報告";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"] = "註銷報告";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["progress report"] = "館藏及使用報告";

##### Report End #####
########## Statistic Start #########
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["name"] = "統計";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["summary"] = "綜合統計";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["general stats"] = "借閱統計";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["category vs class or form"] = "班級分類或科目借閱統計";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["frequency by form"] = "學生閱讀頻次";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["penalty stats"] = "班級罰款統計";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["ebook statistics"] = "電子書閱讀統計";
########## Statistic End  #########

$Lang["libms"]["GroupManagement"]["Right"]["service"]["name"] = "前端服務";
$Lang["libms"]["GroupManagement"]["Right"]["service"]["reserve"] = "預約";
$Lang["libms"]["GroupManagement"]["Right"]["service"]["renew"] = "續借";
$Lang["libms"]["GroupManagement"]["Rule"]["CirDescription"] = "分類名稱";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitBorrow"] = "借閱限制(本)";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitRenew"] = "續借限制(次)";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitReserve"] = "預約限制(本)";
$Lang["libms"]["GroupManagement"]["Rule"]["ReturnDuration"] = "歸還期限(天)";
$Lang["libms"]["GroupManagement"]["Rule"]["OverDueCharge"] = "過期收費(每天罰)";
$Lang["libms"]["GroupManagement"]["Rule"]["MaxFine"] = "最大過期收費(每本)";
$Lang["libms"]["GroupManagement"]["Msg"]["chgrp_confirm"] = "以下讀者已有組別，請確認應變更組別。";
$Lang["libms"]["GroupManagement"]["Msg"]["no_group_right"] = "因這位用戶未屬任何讀者組別或該組別的借還規則未有設定，所以不允許借書。";

$Lang["libms"]["GroupManagement"]["Rule"]["policy_remark"] = "<u>注意</u>：首行的設定<b>預設規則</b>除會應用於未有借閱分類的圖書外，還代表總借閱數量上限。";
$Lang["libms"]["GroupManagement"]["index"]["remark"] = "每位用戶只能歸入一個組別；而沒有入組或組內的借還規則未設定，該用戶便不能借書。";

$Lang["libms"]["book_management"] = "書本管理";
$Lang["libms"]["circulation_management"] = "借還管理";
$Lang["libms"]["Circulation"]["detailrecord"] = "詳細紀錄";
$Lang["libms"]["Circulation"]["borrowedBooks"] = "已借書本";
$Lang["libms"]["Circulation"]["detailrecord"] = "詳細紀錄";
$Lang["libms"]["Circulation"]["action_success"] = "操作成功";
$Lang["libms"]["Circulation"]["action_fail"] = "操作失敗";
$Lang["libms"]["Circulation"]["BookBorrowHistory"] = "書本借閱紀錄";
$Lang["libms"]["Circulation"]["ReaderBorrowHistory"] = "讀者借閱紀錄";
$Lang["libms"]["Circulation"]["PleaseEnterPassword"] = "請輸入密碼";
$Lang["libms"]["Circulation"]["WrongPassword"] = "密碼不正確";
$Lang["libms"]["Circulation"]["NoPermissionHandleBook"] = "你沒有權限處理此圖書!";
$Lang["libms"]["Circulation"]["NoPermissionHandleUser"] = "你沒有權限處理此讀者!";
$Lang["libms"]["Circulation"]["BorrowAccompayMaterial"] = "是否確定同時借閱附件?";
$Lang["libms"]["Circulation"]["BorrowWithAccompayMaterial"] = "同時借閱附件";
$Lang["libms"]["Circulation"]["RenewAccompayMaterial"] = "是否確定同時續借附件?";
$Lang["libms"]["Circulation"]["RenewWithAccompayMaterial"] = "同時續借附件";
$Lang["libms"]["Circulation"]["ReturnAccompayMaterial"] = "是否確定一併歸還附件?";
$Lang["libms"]["Circulation"]["NotBorrowAccompayMaterial"] = "沒有借閱附件";
$Lang["libms"]["Circulation"]["ClickToViewBorrowHistory"] = "請點擊書名以瀏覽書本借閱紀錄";
$Lang['libms']['Circulation']['RemarkNotAllowToBorrowReturnClassManagementBooks'] = '^ 不允許借還班管位置的書';
$Lang['libms']['CirculationManagement']['user_group_not_found'] = "該用戶還未被加入讀者群組";

$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'] = "預約可取通知";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'] = <<<EOF
你預約的 《%s》 已經在圖書館裡，可以給你借閱了。
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'] = <<<EOF
你預約的 《%s》 已經在圖書館裡，可以給你借閱了，請於 %s 或之前到圖書館借閱。
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['subject'] = "預約取消通知";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['mail_body'] = <<<EOF
請留意，圖書館管理員已於 %s 取消了你的預約：《%s》。
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['subject'] = "預約取消通知";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['mail_body'] = <<<EOF
請留意，你已於 %s 取消了你的預約：《%s》。
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['subject'] = "預約取消通知(已逾期)";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['mail_body'] = <<<EOF
請留意，閣下已過了取書的有效日期，系統已於 %s 取消了你的預約：《%s》。
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['subject'] = "預約等候通知";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['mail_body'] = <<<EOF
請留意，閣下預約的圖書 《%2\$s》已於 %1\$s 更新到等候名單。
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['due_date_change_reminder']['subject'] = "更改到期日電郵提示";
$Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['subject'] = "到期日電郵提示通知";
$Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['mail_body'] = <<<EOF
請留意，你借閱的圖書 《%s》 將於 %s 到期，請於到期日前歸還。
EOF;

$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['CallNum']= "索書號";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle']= "書名";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Publisher']= "出版商";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author']= "著 / 作者";
$Lang['libms']['CirculationManagement']['book_search']['title']['BookTitle']= "書名";
$Lang['libms']['CirculationManagement']['book_search']['title']['Author']= "著 / 作者";
$Lang['libms']['CirculationManagement']['book_search']['title']['ACNO']= "登錄號碼";
$Lang['libms']['CirculationManagement']['book_search']['title']['CallNum']= "索書號";

$Lang['libms']['CirculationManagement']['numberOfLoans'] = "借閱次數";
$Lang['libms']['CirculationManagement']['barcode_user_not_found'] = "找不到這用戶條碼"; 
$Lang['libms']['CirculationManagement']['library_closed'] = "閉館中";
$Lang['libms']['CirculationManagement']['batch_return'] = "批量還書";
$Lang['libms']['CirculationManagement']['batch_renew'] = "批量續借";
$Lang['libms']['CirculationManagement']['search_book_record'] = "搜尋書本";
$Lang['libms']['CirculationManagement']['batch_return_enter'] = "進入";
$Lang['libms']['CirculationManagement']['normal_circulation'] = "讀者借還";
$Lang["libms"]["CirculationManagement"]["status"] = "狀態";
$Lang["libms"]["CirculationManagement"]["add"] = "加入";
$Lang["libms"]["CirculationManagement"]["confirm"] = "確定";
$Lang["libms"]["CirculationManagement"]["cancel"] = "取消";
$Lang["libms"]["CirculationManagement"]["user_barcode"] = "用戶條碼";
$Lang["libms"]["CirculationManagement"]["student_only"] = "或選擇學生";
$Lang["libms"]["CirculationManagement"]["user_barcode_ByLin"] = "User barcode 用戶條碼";
$Lang["libms"]["CirculationManagement"]["barcode"] = "條碼";
$Lang["libms"]["CirculationManagement"]["enter_code"] = "輸入登錄號碼 / 條碼 / 索書號 / ISBN";
$Lang["libms"]["CirculationManagement"]["bkcode_acno"] = "索書號 / ACNO";
$Lang["libms"]["CirculationManagement"]['reserve_page']["searchby"] = "書名 / 作者  / ACNO / 索書號";
$Lang["libms"]["CirculationManagement"]['search_book']["searchby"] = "書名 / 索書號 / 條碼";
$Lang["libms"]["CirculationManagement"]["booktitle"] = "書名";
$Lang["libms"]["CirculationManagement"]["moredetail"] = "詳細紀錄";
$Lang["libms"]["CirculationManagement"]["location"] = "館藏位置";
$Lang["libms"]["CirculationManagement"]["borrowday"] = "借書日期";
$Lang["libms"]["CirculationManagement"]["returnday"] = "還書日期";
$Lang["libms"]["CirculationManagement"]["renewday"] = "續借日期";
$Lang["libms"]["CirculationManagement"]["dueday"] = "到期日";
$Lang["libms"]["CirculationManagement"]["reserveday"] = "預約日期";
$Lang["libms"]["CirculationManagement"]["overdueday"] = "逾期日數";
$Lang["libms"]["CirculationManagement"]["renew"] = "續借";
$Lang["libms"]["CirculationManagement"]["renews"] = "續借";
$Lang["libms"]["CirculationManagement"]["lost"] = "報失";
$Lang["libms"]["CirculationManagement"]["borrow"] = "借書";
$Lang["libms"]["CirculationManagement"]["return"] = "還書";
$Lang["libms"]["CirculationManagement"]["reserve"] = "預約";
$Lang["libms"]["CirculationManagement"]["overdue"] = "繳交罰款";
$Lang["libms"]["CirculationManagement"]["statusnow"] = "現有紀錄";
$Lang["libms"]["CirculationManagement"]["all_loan_records"] = "全部借閱紀錄";
$Lang["libms"]["CirculationManagement"]["leave"] = "離開";
$Lang["libms"]["CirculationManagement"]["no_borrow"] = "借書數目";
$Lang["libms"]["CirculationManagement"]["no_renew"] = "續借書數目";
$Lang["libms"]["CirculationManagement"]["no_return"] = "成功還書數目";
$Lang["libms"]["CirculationManagement"]["no_batch_renew"] = "成功續借數目";
$Lang["libms"]["CirculationManagement"]["no_lost"] = "報失書數目";
$Lang["libms"]["CirculationManagement"]["no_reserve"] = "預約書數目";
$Lang["libms"]["CirculationManagement"]["no_to_return"] = "還書數目";
$Lang["libms"]["CirculationManagement"]["ppl_reserved"] = "已預約人數";
$Lang["libms"]["CirculationManagement"]["n_renew"] = "不可續借";
$Lang["libms"]["CirculationManagement"]["y_renew"] = "可以續借";
$Lang["libms"]["CirculationManagement"]["y_reserve"] = "可以預約";
$Lang["libms"]["CirculationManagement"]["n_reserve"] = "不可預約";
$Lang["libms"]["CirculationManagement"]["y_borrow"] = "可以借閱";
$Lang["libms"]["CirculationManagement"]["n_borrow"] = "不可借閱";
$Lang["libms"]["CirculationManagement"]["renew_left"] = "續借餘額";
$Lang["libms"]["CirculationManagement"]["overdue_day"] = "逾期（天）";
$Lang['libms']['CirculationManagement']['overdue_type'] ="罰款類型";
$Lang["libms"]["CirculationManagement"]["overdue_with_day"] = "逾期（%d 天）";
$Lang["libms"]["CirculationManagement"]["payment"] = "罰款";
$Lang["libms"]["CirculationManagement"]["free_payment"] = "豁免罰款";
$Lang["libms"]["CirculationManagement"]["paied"] = "已繳交";
$Lang["libms"]["CirculationManagement"]["paid_or_waive"] = "已繳交/已豁免";
$Lang["libms"]["CirculationManagement"]["pay_now"] = "現繳交";
$Lang["libms"]["CirculationManagement"]["booked"] = "被人預約";
$Lang["libms"]["CirculationManagement"]["booked_alert"] = "已被人預約！";
$Lang["libms"]["CirculationManagement"]["left"] = "尚欠";
$Lang["libms"]["CirculationManagement"]["total_pay"] = "今次總交";
$Lang["libms"]["CirculationManagement"]["reserverperiod"] = "借取期限";
$Lang["libms"]["CirculationManagement"]["trim"] = "(修整中)";
$Lang["libms"]["CirculationManagement"]["action"] = "執行";
$Lang["libms"]["CirculationManagement"]["borrowed"] = "已借書本";
$Lang["libms"]["CirculationManagement"]["reserved"] = "已預約書本";
$Lang["libms"]["CirculationManagement"]["book_overdue"] = "逾期未還";
$Lang["libms"]["CirculationManagement"]["borrow_limit"] = "借書限額";
$Lang["libms"]["CirculationManagement"]["limit_left"] = "剩餘限額";
$Lang["libms"]["CirculationManagement"]["book_reserve"] = "預借書本";
$Lang["libms"]["CirculationManagement"]["left_pay"] = "尚欠罰款";
$Lang["libms"]["CirculationManagement"]["past_record"] = "過往紀錄";
$Lang["libms"]["CirculationManagement"]["FineDate"] = "罰款日期（還書日期）";
$Lang["libms"]["CirculationManagement"]["borrow_accompany_material"] = "借閱附件";
$Lang["libms"]["CirculationManagement"]["borrow_accompany_material_yes"] = "有";
$Lang["libms"]["CirculationManagement"]["borrow_accompany_material_no"] = "沒有";
$Lang["libms"]["CirculationManagement"]["additional_charge"] = "附加費";
$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'] = "此書狀態:";
$Lang['libms']['CirculationManagement']['msg']['book_item_status']['not_on_loan'] = "此書暫時沒有借出！";
$Lang["libms"]["CirculationManagement"]["msg"]["over_borrow_limit"] = "讀者已經到達借閱上限";
$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_limit"] = "讀者已經到達預約上限";
$Lang["libms"]["CirculationManagement"]["msg"]["over_renew_limit"] = "讀者已經到達續借上限";
$Lang["libms"]["CirculationManagement"]["msg"]["over_borrow_cat_limit"] = "讀者已經到達此書類借閱上限";
$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_cat_limit"] = "讀者已經到達此書類預約上限";
$Lang["libms"]["CirculationManagement"]["msg"]["over_renew_cat_limit"] = "讀者已經到達此書類續借上限";
$Lang["libms"]["CirculationManagement"]["msg"]["renew_but_overdue"] = "已逾期，要先還書及繳交罰款！";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_borrow_limit"] = "此讀者不能借閱此書類";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_reserve_limit"] = "此讀者不能預約此書類";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_renew_limit"] = "此讀者不能續借此書類";
$Lang['libms']['CirculationManagement']['msg']['notAllowBorrowReturnClassManagementBooks'] = '不允許借還班管位置的書';
$Lang['libms']['CirculationManagement']['msg']['notAllowReturnWriteoffBooks'] = '不允許歸還已註銷的書';
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_borrow"] = "此書目不能借出";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_borrow_book"] = "此書本不能借出";
$Lang["libms"]["CirculationManagement"]["msg"]["already_borrowed"] = "此書已被 [user_name] 借出";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve"] = "此書目不能預約";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_no_copy"] = "此書目沒有項目可被預約";
$Lang["libms"]["CirculationManagement"]["msg"]["exceeds_reserve"] = "你已到達了預約限額";
$Lang["libms"]["CirculationManagement"]["msg"]["exceeds_reserve_of_book_category"] = "你已到達了該圖書類別的預約限額";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_book"] = "此書本不能預約";
$Lang["libms"]["CirculationManagement"]["msg"]["scanned"] = "此書已被掃描";
$Lang["libms"]["CirculationManagement"]["msg"]["inlist"] = "此書已在表列中";
$Lang["libms"]["CirculationManagement"]["msg"]["force_borrow"] = "已特許外借";
$Lang["libms"]["CirculationManagement"]["msg"]["force_reserve"] = "已特許預約";
$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"] = "特許續借";
$Lang["libms"]["CirculationManagement"]["msg"]["f_reserve"] = "特許預借";
$Lang["libms"]["CirculationManagement"]["msg"]["f_borrow"] = "特許借出";
$Lang["libms"]["CirculationManagement"]["msg"]["reserved"] = "已被預約";
$Lang["libms"]["CirculationManagement"]["msg"]["comfirm_leave"] = "[注意] 紀錄還未被處理，確定要放棄及離開嗎？";
$Lang["libms"]["CirculationManagement"]["msg"]["n_barcode"] = "找不到這條碼";
$Lang["libms"]["CirculationManagement"]["msg"]["n_callno_acno"] = "找不到這 書名 / 索書號 / ACNO / 作者";
$Lang["libms"]["CirculationManagement"]["msg"]["search_book_not_found"] = "找不到這 書名 / 索書號 / 條碼";
$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"] = "已逾期";
$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"] = "天";
$Lang['libms']['CirculationManagement']['msg']['overdue_limit'] = "讀者已經到逾期未還上限, 請從速還書";
$Lang['libms']['CirculationManagement']['msg']['please_pick_a_book'] = "請選擇書目";
$Lang['libms']['CirculationManagement']['msg']['searching_string_too_short']= '搜尋關鍵字太短';
$Lang['libms']['CirculationManagement']['msg']['fix_error'] = "請更正錯誤";
$Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"] = "找不到借出紀錄";
$Lang['libms']['CirculationManagement']['msg']['batch_return'] = "你現在可以處理大量還書。如需清除本頁的紀錄，可按「消除」。";
$Lang['libms']['CirculationManagement']['msg']['batch_renew'] = "你現在可以處理圖書續借。";
$Lang['libms']['CirculationManagement']['msg']['circulation_index'] = "你現在可以選擇處理大量還書、為個別讀者進行借還書或搜尋書本。";
$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed'] = "你還未輸入或選取要處理的書目！";
$Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"] = "圖書已被預約！";
$Lang['libms']['CirculationManagement']['msg']['user_reserved'] = "圖書已被其他讀者預約了!";
$Lang["libms"]["CirculationManagement"]["borrowed_by"] = "讀者";
$Lang["libms"]["CirculationManagement"]["valid_date"] = "有效日期";
$Lang['libms']['CirculationManagement']['msg']['reserved_by'] = "預約者";
$Lang['libms']['CirculationManagement']['msg']['expiry_date'] = "有效日期至";
$Lang['libms']['CirculationManagement']['msg']['no_overdue_record'] = "暫時未有待處理之罰款紀錄。";
$Lang["libms"]["CirculationManagement"]['msg']["AllOverduePaymentPaidWithePayment"] = "所有逾期罰款會以ePayment繳付。";
$Lang['libms']['CirculationManagement']['msg']['already_quit'] = "讀者已經離開";
$Lang['libms']['CirculationManagement']['msg']['please_input_additional_charge'] = "請輸入附加費";
$Lang["libms"]["CirculationHotKeys"]["Exit"] = "[Esc]";
$Lang["libms"]["CirculationHotKeys"]["Borrow"] = "[F2]";
$Lang["libms"]["CirculationHotKeys"]["Return"] = "[F3]";
$Lang["libms"]["CirculationHotKeys"]["Renew"] = "[F4]";
$Lang["libms"]["CirculationHotKeys"]["Lost"] = "[F6]";
$Lang["libms"]["CirculationHotKeys"]["Reserve"] = "[F7]";
$Lang["libms"]["CirculationHotKeys"]["Overdue"] = "[F8]";
$Lang["libms"]["CirculationHotKeys"]["Cancel"] = "[F5]";
$Lang["libms"]["CirculationHotKeys"]["Confirm"] = "[F9]";
$Lang["libms"]["CirculationHotKeys"]["Details"] = "[F10]";
$Lang["libms"]["CirculationHotKeys"]["BatchReturn"] = "[F1]";


$Lang["libms"]["book_status"]["NORMAL"] = "可借出";
$Lang["libms"]["book_status"]["SHELVING"] = "待上架";
$Lang["libms"]["book_status"]["BORROWED"] = "借出中";
$Lang["libms"]["book_status"]["DAMAGED"] = "損壞";
$Lang["libms"]["book_status"]["REPAIRING"] = "修復中";
$Lang["libms"]["book_status"]["RESERVED"] = "預留";
$Lang["libms"]["book_status"]["RESTRICTED"] = "展出中";
$Lang["libms"]["book_status"]["WRITEOFF"] = "註銷";
$Lang["libms"]["book_status"]["SUSPECTEDMISSING"] = "懷疑不見";
$Lang["libms"]["book_status"]["LOST"] = "遺失";
$Lang["libms"]["book_status"]["ORDERING"] = "訂購中";

$Lang["libms"]["book_reserved_status"]["WAITING"] = "未可取書";
$Lang["libms"]["book_reserved_status"]["READY"] = "可取書";

//reportingpart...................................................................................................................................................................................................................................................................................................
$Lang["libms"]["report"]["bookinfo"] = "基本資料";
$Lang["libms"]["report"]["country"] = "地區";
$Lang["libms"]["report"]["language"] = "語言";
$Lang["libms"]["report"]["time_period"] = "時段";
$Lang["libms"]["report"]["SchoolYear"] = "學年";
$Lang["libms"]["report"]["Semester"] = "學期";
$Lang["libms"]["report"]["period_date_from"] = "由";
$Lang["libms"]["report"]["period_date_to"] = "至";
$Lang["libms"]["report"]["display"] = "顯示";
$Lang["libms"]["report"]["circulation_type"] = "借閱分類";
$Lang["libms"]["report"]["book_category"] = "圖書分類";
$Lang["libms"]["report"]["subject"] = "科目";
$Lang["libms"]["report"]["book_status"] = "項目狀態";
$Lang["libms"]["report"]["book_status_all"] = "所有書本";
$Lang["libms"]["report"]["book_status_borrow"] = "借出書本";
$Lang["libms"]["report"]["book_status_renew"] = "續借書本";
$Lang["libms"]["report"]["target"] = "對象";
$Lang["libms"]["report"]["findByGroup"] = "組別";
$Lang["libms"]["report"]["findByStudent"] = "學生";
$Lang["libms"]["report"]["amount"] = "金額";
$Lang["libms"]["report"]["due_time"] = "到期日";
$Lang['libms']['report']['penaltyDate'] = '過期罰款日期';
$Lang["libms"]["report"]["show_penalty"] = "顯示罰款";
$Lang["libms"]["report"]["show_purchase_price"] = "顯示購買價格";
$Lang["libms"]["report"]["show_list_price"] = "顯示訂價";
$Lang["libms"]["report"]["excluding_in_push_notification"] = "(此資料不會顯示在推送訊息中)";
$Lang["libms"]["report"]["due_time_ByLin"] = "Due date 到期日";
$Lang["libms"]["report"]["borrowdate"] = "借出時間";
$Lang["libms"]["report"]["returndate"] = "還書時間";
$Lang["libms"]["report"]["sortby"] = "排序";
$Lang["libms"]["report"]["show_last_modified_by"] = "顯示在流通管理介面作最後修改的用戶及時間";
$Lang["libms"]["report"]["listby"] = "列出形式";
$Lang["libms"]["report"]["classno"] = "班號";
$Lang["libms"]["report"]["ACNO"] = "登錄號碼";
$Lang["libms"]["report"]["ACNO_ByLin"] = "ACNO 登錄號碼";
$Lang["libms"]["report"]["frequency"] = "次數";
$Lang["libms"]["report"]["number"] = "數目";
$Lang["libms"]["report"]["name"] = "名稱";
$Lang["libms"]["report"]["by_form_class"] = "按班級";
$Lang["libms"]["report"]["by_amount"] = "按數量";
$Lang["libms"]["report"]["ascending"] = "遞增";
$Lang["libms"]["report"]["descending"] = "遞減";
$Lang["libms"]["report"]["cannot_define"] = " - 未分類 - ";
$Lang["libms"]["report"]["print"] = "列印";
$Lang["libms"]["report"]["mailToStudent"] = "電郵給學生";
$Lang["libms"]["report"]["mailToParent"] = "電郵給家長";
$Lang["libms"]["report"]["selectAll"] = "全選";
$Lang["libms"]["report"]["PressCtrlKey"] = "(可按Ctrl鍵選擇多項)";
$Lang["libms"]["report"]["Submit"] = "呈送";
$Lang["libms"]["report"]["Form"] = "級別";
$Lang["libms"]["report"]["Class"] = "班別";
$Lang["libms"]["report"]["Student"] = "學生姓名";
$Lang["libms"]["report"]["ListNumber"] = "列出數量";
$Lang["libms"]["report"]["Resource"] = "物品分類";
$Lang["libms"]["report"]["totalreserve"] = "預約次數";
$Lang["libms"]["report"]["totalborrow"] = "借出次數";
$Lang["libms"]["report"]["totalamount"] = "所得之罰款";
$Lang["libms"]["report"]["totaloverdue"] = "逾期歸還次數";
$Lang["libms"]["report"]["barcode"] = "條碼";
$Lang["libms"]["report"]["recordstatus"] = "狀況";
$Lang["libms"]["report"]["statistical_category"] = "統計類別";
$Lang["libms"]["report"]["loans_less_than"] = "借閱次數";
$Lang["libms"]["report"]["loans_less_than_unit"] = "次或以下";
$Lang["libms"]["report"]["Gender"] = "性別";
$Lang["libms"]["report"]["Male"] = "男";
$Lang["libms"]["report"]["Female"] = "女";
$Lang["libms"]["report"]["onloan"] = "借出數量";
$Lang["libms"]["report"]["targetGroup"] = "組別";
$Lang["libms"]["report"]["targetGroupMember"] = "組員";	
$Lang["libms"]["report"]["ClassLevel"] = "班級";
$Lang["libms"]["report"]["ClassName"] = "班別";
$Lang["libms"]["report"]["ClassName_ByLin"] = "Class 班別";
$Lang["libms"]["report"]["ClassNumber"] = "學號";
$Lang["libms"]["report"]["ClassNumber_ByLin"] = "Class number 學號";
$Lang["libms"]["report"]["username"] = "讀者姓名";
$Lang["libms"]["report"]["username_ByLin"] = "User 讀者姓名";
$Lang["libms"]["report"]["ChineseName"] = "中文全名";
$Lang["libms"]["report"]["EnglishName"] = "英文全名";
$Lang["libms"]["report"]["booktitle"] = "書名";
$Lang["libms"]["report"]["booktitle_ByLin"] = "Book 書名";
$Lang["libms"]["report"]["LendingTotal"] = "借閱數量";
$Lang["libms"]["report"]["CallNum"] = "索書號";
$Lang["libms"]["report"]["ResponsibilityBy"] = "作者";
$Lang["libms"]["report"]["Publisher"] = "出版商";
$Lang["libms"]["report"]["Edition"] = "版本";
$Lang["libms"]["report"]["ISBN"]="ISBN";
$Lang["libms"]["report"]["tags"] = "標籤字眼";
$Lang["libms"]["report"]["search"]="查詢  ( ACNO / 書名 / ISBN )";
$Lang["libms"]["report"]["pay"]="罰款";
$Lang["libms"]["report"]["pay_ByLin"]="Penal 罰款";
$Lang["libms"]["report"]["payment"]["title"] = "罰款統計";
$Lang["libms"]["report"]["payment"]["OVERDUE"] = "欠收罰款";
$Lang["libms"]["report"]["payment"]["PAID"] = "已收罰款";
$Lang["libms"]["report"]["total"] = "總數";
$Lang["libms"]["report"]["total_ByLin"] = "Total 總數";
$Lang["libms"]["report"]["LastModifiedBy"]="最後修改用戶";
$Lang["libms"]["report"]["LastModified"]="最後修改日期";
$Lang["libms"]["report"]["bookrecord"]["title"]= "數量記錄";
$Lang["libms"]["report"]["bookrecord"]["LOST"] = "報失書籍";
$Lang["libms"]["report"]["bookrecord"]["WRITEOFF"] = "註銷書籍";
$Lang["libms"]["report"]["bookrecord"]["NEW"] = "新增書藉";
$Lang["libms"]["report"]["IncludeLeftStudent"] = "包括離校生";
$Lang["libms"]["report"]["left_student_indicator"] = "<span style='color: red'> * </span>表示該學生已離校";

$Lang["libms"]["report"]["overdue_email"]["Subject"] = "到期通知";
$Lang["libms"]["report"]["overdue_email"]["Subject_ByLin"] = "Due Date Reminder 到期通知";
$Lang["libms"]["report"]["overdue_email"]["introduction_ByLin"] = "The following library material(s) which you borrowed is/are already due for return. Please return it/them now. Please ignore this notice if you have already returned. <br />你所借的下列書本或物品現已到期，請立即交還。如你已交還，請無需理會此通知書。";
$Lang["libms"]["report"]["overdue_remark"]["remark"] = "備註";
$Lang["libms"]["report"]["overdue_remark"]["history"] = "過往備註";
$Lang["libms"]["report"]["overdue_remark"]["mandatory"] = "請輸入備註!";
$Lang["libms"]["report"]["overdue_remark"]["please_select"] = "-- 請選擇備註 --";
$Lang["libms"]["report"]["overdue_remark"]["server_error"] = "錯誤: 未能取得備註,請稍後重試。";

$Lang["libms"]["report"]["toolbar"]['Btn']['AddRemark'] = "增加備註";
$Lang["libms"]["report"]["toolbar"]['Btn']['Export'] = "匯出";
$Lang["libms"]["report"]["toolbar"]['Btn']['Print'] = "列印";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailuser'] = "電郵給用戶";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailparent'] = "電郵給家長";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailboth'] = "電郵給用戶及家長";
$Lang["libms"]["report"]["toolbar"]['Btn']['sendPushMessageToParent'] = "推播訊息給家長";
$Lang["libms"]["report"]["toolbar"]['Btn']['Close'] = "關閉";


$Lang["libms"]["report"]["printdate"] = "列印日期";
$Lang["libms"]["report"]["printdate_ByLin"] = "Date of printing 列印日期";
$Lang["libms"]["report"]["noOfDayOverdue"] = "過期日數";
$Lang["libms"]["report"]["noOfDayOverdue_ByLin"] = "Overdue (days) 過期日數";

$Lang["libms"]["report"]["showoption"] = "顯示選項";
$Lang["libms"]["report"]["hideoption"] = "隱藏選項";
$Lang["libms"]["report"]["norecord"] = "沒有符合紀錄";


$Lang["libms"]["report"]["fine_date"] = "罰款日期（還書日期）";
$Lang["libms"]["report"]["fine_status"] = "繳款狀態";
$Lang["libms"]["report"]["fine_status_all"] = "全部";
$Lang["libms"]["report"]["fine_outstanding"] = "未繳";
$Lang["libms"]["report"]["fine_waived"] = "豁免";
$Lang["libms"]["report"]["fine_handle_date"] = "繳交時間";
$Lang["libms"]["report"]["fine_handle_date_or_last_updated"] = "繳交時間/最後修改時間";
$Lang["libms"]["report"]["fine_type"] = "罰款類別";
$Lang["libms"]["report"]["fine_type_overdue"] = "逾期罰款";
$Lang["libms"]["report"]["fine_type_lost"] = "遺失罰款";
$Lang["libms"]["report"]["fine_amended"] = "罰款曾經修改";
$Lang["libms"]["report"]["handled_by"] = "經手人";
$Lang["libms"]["report"]["library_stock"] = "圖書館藏";
$Lang["libms"]["report"]["library_resources"] = "圖書館資源使用率";
$Lang["libms"]["report"]['Others'] = "其他";
$Lang["libms"]["report"]['daily_averages_issue'] = "平均每天使用量";
$Lang["libms"]["report"]['listTotalNumberOfPerson'] = "列出人數總和";
$Lang["libms"]["report"]['TotalNumberOfStudent'] = "學生人數總和";
$Lang["libms"]["report"]['EnableDeletionLine'] = "使用刪除線";
$Lang["libms"]["report"]["displayGenderTotal"] = "顯示男女總數";
$Lang["libms"]["report"]["percentage"] = "百分比";
$Lang["libms"]["report"]["Times"] = "次數";
$Lang['libms']['report']['PageBreakForEachUser'] = "列印時每一用戶作新分頁";
$Lang['libms']['report']['NewPageBreak'] = "列印時開新分頁";
$Lang['libms']['report']['WriteoffPrintTotal'] = "總冊數";
$Lang['libms']['report']['ApprovedBy'] = "批核人/ 校長簽署";
$Lang['libms']['report']['SubmittedBy'] = "負責人簽署";
/*
 * $Lang["libms"]["CirculationManagement"]["free_payment"] = "豁免罰款";
$Lang["libms"]["CirculationManagement"]["paied"] = "已繳交";
 */

$Lang['libms']['report']['msg']['searching_string_too_short']= '搜尋關鍵字太短!';
$Lang['libms']['report']['msg']['empty_search']= '請填上查詢資料';
$Lang['libms']['report']['msg']['please_pick_a_book'] = "請選擇書目";
$Lang['libms']['report']['msg']['please_select_academic_year'] = "請選擇學年";
$Lang['libms']['report']['msg']['please_select_category'] = "請選擇最少一項圖書分類";
$Lang['libms']['report']['msg']['please_select_circategory'] = "請選擇最少一項借閱分類";
$Lang['libms']['report']['msg']['please_select_display_item'] = "請選擇最少一個顯示項目";
$Lang['libms']['report']['msg']['please_select_subject'] = "請選擇最少一項科目";
$Lang['libms']['report']['msg']['please_select_language'] = "請選擇最少一項 語言";
$Lang['libms']['report']['msg']['please_select_class'] = "請選擇最少一個班別";
$Lang['libms']['report']['msg']['please_select_form'] = "請選擇最少一個級別";
$Lang['libms']['report']['msg']['please_select_group'] = "請選擇最少一個組別";
$Lang['libms']['report']['msg']['please_select_group_member'] = "請選擇最少一個組員";
$Lang['libms']['report']['msg']['please_select_student'] = "請選擇最少一個學生";
$Lang['libms']['report']['msg']['wrong_due_date_range'] = "到期日必須早於今天。";
$Lang['libms']['report']['msg']['dueDateShouldBeEarlierThanPenaltyDate'] = "到期日必須早於過期罰款日期。";
$Lang['libms']['report']['1weekormore'] = "每周一次或以上";
$Lang['libms']['report']['2week'] = "每兩星期一次";
$Lang['libms']['report']['1month'] = "每月一次";
$Lang['libms']['report']['1monthless'] = "每月少於一次";
$Lang['libms']['report']['none'] = "從不";
$Lang['libms']['report']['totalTimesOfBorrow'] = "總借出次數";
$Lang['libms']['report']['all_records'] = "所有紀錄";
$Lang['libms']['report']['ForReferenceOnly'] = "只供參考";
$Lang['libms']['report']['LostBookPaymentChanged'] = "遺失書本罰款額曾經修改";
$Lang['libms']['report']['LostBookReturned'] = "遺失的圖書館資料已被歸還";
$Lang['libms']['report']['NegagivePaidDenotesCancelOverdue'] = "已繳交為負數表示取消之前一項已繳交的罰款";
$Lang["libms"]["import"]['BookCat']['title'] = "圖書分類匯入";
$Lang["libms"]["import"]['BookCat']['error_msg']['upload_fail'] = "上載失敗";
$Lang["libms"]["import"]['BookCat']['error_msg']['contact_admin'] = "請與系統系統管理員聯繫";


$Lang["libms"]["import"]['BookCat']['fields']['BookCategoryCode']['name']="圖書分類代碼";
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionEn']['name']="英文分類名稱";
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionChi']['name']="中文分類名稱";
$Lang["libms"]["import"]['BookCat']['fields']['BookCategoryCode']['req']=true;
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionEn']['req'] =true;
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionChi']['req']=true;

$Lang["libms"]["import"]['BookLocation']['title'] = "館藏位置匯入";
$Lang["libms"]["import"]['BookLocation']['error_msg']['upload_fail'] = "上載失敗";
$Lang["libms"]["import"]['BookLocation']['error_msg']['contact_admin'] = "請與系統系統管理員聯繫";


$Lang["libms"]["import"]['BookLocation']['fields']['BookLocationCode']['name']="館藏位置代碼";
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionEn']['name']="英文分類名稱";
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionChi']['name']="中文分類名稱";
$Lang["libms"]["import"]['BookLocation']['fields']['BookLocationCode']['req']=true;
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionEn']['req'] =true;
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionChi']['req']=true;


$Lang["libms"]["import"]['SpeicalOpenTime']['title'] = "特別開放匯入";
$Lang["libms"]["import"]['SpeicalOpenTime']['error_msg']['upload_fail'] = "上載失敗";
$Lang["libms"]["import"]['SpeicalOpenTime']['error_msg']['contact_admin'] = "請與系統系統管理員聯繫";


$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateFrom']['name']="開始日期 (YYYY-MM-DD)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateEnd']['name'] ="結束日期 (YYYY-MM-DD)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['OpenTime']['name']="開館時間 (HH:MM:SS)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['CloseTime']['name']="閉館時間 (HH:MM:SS)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['Open']['name']="開館 1/ 閉館 0";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateFrom']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateEnd']['req'] =true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['OpenTime']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['CloseTime']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['Open']['req']=true;

$Lang['libms']['import_book']['and'] = "和";
$Lang['libms']['import_book']['book_records'] = "條書目紀錄";
$Lang['libms']['import_book']['item_records'] = "條項目紀錄";
$Lang['libms']['import_book']['DateFormat'] = "<span class='tabletextrequire'>#</span> 日期的格式必須為 YYYY-MM-DD 或 DD/MM/YYYY. 如你使用 Excel 編輯, 請在日期的資料上更改儲存格格式";
$Lang['libms']['import_book']['RecordStatus'] = "<span class='tabletextrequire'>^</span> 匯入功能不會處理已註銷項目的狀態，只有註銷管理才能更改註銷紀錄。";

$Lang['General']['InvalidDateFormat'] = "*日期格式不符";
$Lang['General']['WrongDateLogic'] = "日期錯誤. 結束日期不能早於開始日期。";


$Lang['General']['ReturnMessage']['FailedToDelLoanAsPaymentSettled'] = '0|=|刪除紀錄失敗 (因為已繳交了罰款紀錄)';
$Lang['General']['ReturnMessage']['CannotDeleteOnLoanACNO'] = "0|=|不能刪除已借出的圖書: ACNO %s";
$Lang['General']['ReturnMessage']['CannotDeleteOnLoanBook'] = "0|=|不能刪除已借出的圖書: %s";
$Lang['General']['ReturnMessage']['ItemStatusUpdateUnsuccess'] = '0|=|更新紀錄失敗。不能更新[註銷]項目狀態。';
$Lang['General']['ReturnMessage']['UpdateSuccessWithWriteOffItem'] = '1|=|紀錄已經更新。([註銷]狀況除外)';
# 杜威分類法(Dewey Decimal Classification)
$PRESET_BOOK_CATEGORY["DDC"][] = array("000", "總類", "Computer science, knowledge & systems");
$PRESET_BOOK_CATEGORY["DDC"][] = array("010", "目錄學", "Bibliographies"); 
$PRESET_BOOK_CATEGORY["DDC"][] = array("020", "圖書館及資訊科學", "Library & information sciences"); 
$PRESET_BOOK_CATEGORY["DDC"][] = array("030", "百科全書", "Encyclopedias & books of facts"); 
//$PRESET_BOOK_CATEGORY["DDC"][] = array("040", "Biographies", "Biographies"); 
$PRESET_BOOK_CATEGORY["DDC"][] = array("050", "期刊", "Magazines, journals & serials"); 
$PRESET_BOOK_CATEGORY["DDC"][] = array("060", "會社及博物館", "Associations, organizations & museums"); 
$PRESET_BOOK_CATEGORY["DDC"][] = array("070", "新聞學；出版	", "News media, journalism & publishing"); 
//$PRESET_BOOK_CATEGORY["DDC"][] = array("080", "普通收藏品", "General collections"); 
//$PRESET_BOOK_CATEGORY["DDC"][] = array("090", "手抄本和珍本書", "Manuscripts & rare books"); 


$PRESET_BOOK_CATEGORY["DDC"][] = array("100", "哲學類", "Philosophy");
$PRESET_BOOK_CATEGORY["DDC"][] = array("110", "超自然現象及迷信", "Metaphysics");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("120", "哲學類", "Epistemology");
$PRESET_BOOK_CATEGORY["DDC"][] = array("130", "超自然現象；術數及迷信", "Parapsychology and occultism");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("140", "哲學類", "Philosophical schools of thought");
$PRESET_BOOK_CATEGORY["DDC"][] = array("150", "心理學", "Psychology");
$PRESET_BOOK_CATEGORY["DDC"][] = array("160", "邏輯學", "Logic");
$PRESET_BOOK_CATEGORY["DDC"][] = array("170", "倫理學", "Ethics (Moral philosophy)");
$PRESET_BOOK_CATEGORY["DDC"][] = array("180", "東方哲學", "Ancient, medieval, and Eastern philosophy");
$PRESET_BOOK_CATEGORY["DDC"][] = array("190", "近代哲學", "Modern Western philosophy");


$PRESET_BOOK_CATEGORY["DDC"][] = array("200", "宗教類", "Religion");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("210", "宗教類", "Natural theology");
$PRESET_BOOK_CATEGORY["DDC"][] = array("220", "聖經", "Bible");
$PRESET_BOOK_CATEGORY["DDC"][] = array("230", "基督教", "Christian theology");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("240", "宗教類", "Christian moral & devotional theology");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("250", "宗教類", "Christian orders & local church");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("260", "宗教類", "Christian social theology");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("270", "宗教類", "Christian church history");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("280", "宗教類", "Christian denominations & sects");
$PRESET_BOOK_CATEGORY["DDC"][] = array("290", "比較宗教學及其他宗教", "Other & comparative religions");


$PRESET_BOOK_CATEGORY["DDC"][] = array("300", "社會科學類", "Social sciences");
$PRESET_BOOK_CATEGORY["DDC"][] = array("310", "統計", "General statistics");
$PRESET_BOOK_CATEGORY["DDC"][] = array("320", "政治", "Political science");
$PRESET_BOOK_CATEGORY["DDC"][] = array("330", "經濟", "Economics");
$PRESET_BOOK_CATEGORY["DDC"][] = array("340", "法律", "Law");
$PRESET_BOOK_CATEGORY["DDC"][] = array("350", "公共行政；軍事", "Public administration");
$PRESET_BOOK_CATEGORY["DDC"][] = array("360", "社會問題及服務", "Social services; association");
$PRESET_BOOK_CATEGORY["DDC"][] = array("370", "教育", "Education");
$PRESET_BOOK_CATEGORY["DDC"][] = array("380", "商業", "Commerce, communications, transport");
$PRESET_BOOK_CATEGORY["DDC"][] = array("390", "禮俗", "Customs, etiquette, folklore");


$PRESET_BOOK_CATEGORY["DDC"][] = array("400", "語文學", "Language");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("410", "語文學", "Linguistics");
$PRESET_BOOK_CATEGORY["DDC"][] = array("420", "英語", "English & Old English");
$PRESET_BOOK_CATEGORY["DDC"][] = array("430", "德語", "Germanic languages; German");
$PRESET_BOOK_CATEGORY["DDC"][] = array("440", "法語", "Romance languages; French");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("450", "語文學", "Italian, Romanian, Rhaeto-Romanic");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("460", "語文學", "Spanish & Portuguese languages");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("470", "語文學", "Italic languages; Latin");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("480", "語文學", "Hellenic languages; Classical Greek");
$PRESET_BOOK_CATEGORY["DDC"][] = array("490", "中國語言文字", "Other languages");


$PRESET_BOOK_CATEGORY["DDC"][] = array("500", "自然科學類", "Sciences");
$PRESET_BOOK_CATEGORY["DDC"][] = array("510", "數學", "Mathematics");
$PRESET_BOOK_CATEGORY["DDC"][] = array("520", "天文", "Astronomy & allied sciences");
$PRESET_BOOK_CATEGORY["DDC"][] = array("530", "物理", "Physics");
$PRESET_BOOK_CATEGORY["DDC"][] = array("540", "化學", "Chemistry & allied sciences");
$PRESET_BOOK_CATEGORY["DDC"][] = array("550", "地理及地質學", "Earth sciences");
$PRESET_BOOK_CATEGORY["DDC"][] = array("560", "古生物學", "Paleontology; Paleozoology");
$PRESET_BOOK_CATEGORY["DDC"][] = array("570", "生物學", "Life sciences");
$PRESET_BOOK_CATEGORY["DDC"][] = array("580", "植物學", "Plants");
$PRESET_BOOK_CATEGORY["DDC"][] = array("590", "動物學", "Zoological sciences/Animals");


$PRESET_BOOK_CATEGORY["DDC"][] = array("600", "應用科學類", "Technology (Applied sciences)");
$PRESET_BOOK_CATEGORY["DDC"][] = array("610", "醫藥", "Medical sciences; Medicine");
$PRESET_BOOK_CATEGORY["DDC"][] = array("620", "工程", "Engineering & Applied operations");
$PRESET_BOOK_CATEGORY["DDC"][] = array("630", "農業", "Agriculture");
$PRESET_BOOK_CATEGORY["DDC"][] = array("640", "家政及家事", "Home economics & family living");
$PRESET_BOOK_CATEGORY["DDC"][] = array("650", "管理學", "Management & auxiliary services");
$PRESET_BOOK_CATEGORY["DDC"][] = array("660", "應用化學；化學工藝", "Chemical engineering");
$PRESET_BOOK_CATEGORY["DDC"][] = array("670", "製造", "Manufacturing");
$PRESET_BOOK_CATEGORY["DDC"][] = array("680", "特種產品製造", "Manufacture for specific uses");
$PRESET_BOOK_CATEGORY["DDC"][] = array("690", "建築", "Buildings");


$PRESET_BOOK_CATEGORY["DDC"][] = array("700", "美術類", "Arts");
$PRESET_BOOK_CATEGORY["DDC"][] = array("710", "都市設計", "Civic & landscape art");
$PRESET_BOOK_CATEGORY["DDC"][] = array("720", "建築藝術", "Architecture");
$PRESET_BOOK_CATEGORY["DDC"][] = array("730", "雕塑", "Plastic arts; Sculpture");
$PRESET_BOOK_CATEGORY["DDC"][] = array("740", "圖案；裝飾", "Drawing & decorative arts");
$PRESET_BOOK_CATEGORY["DDC"][] = array("750", "書畫美術", "Painting & paintings");
$PRESET_BOOK_CATEGORY["DDC"][] = array("760", "印刷藝術", "Graphic arts; Printmaking & prints");
$PRESET_BOOK_CATEGORY["DDC"][] = array("770", "攝影", "Photography & photographs");
$PRESET_BOOK_CATEGORY["DDC"][] = array("780", "音樂", "Music");
$PRESET_BOOK_CATEGORY["DDC"][] = array("790", "娛樂及表演藝術", "Recreational & performing arts");


$PRESET_BOOK_CATEGORY["DDC"][] = array("800", "文學類", "Literature");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("810", "文", "American literature in English");
$PRESET_BOOK_CATEGORY["DDC"][] = array("820", "英國文學", "English & Old English literatures");
$PRESET_BOOK_CATEGORY["DDC"][] = array("830", "德國文學", "German & related literatures");
$PRESET_BOOK_CATEGORY["DDC"][] = array("840", "法國文學及歐洲各國文學", "Literatures of Romance languages");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("850", "文", "Italian, Romanian, Rhaeto-Romanic");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("860", "文", "Spanish & Portuguese literatures");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("870", "文", "Italic literatures; Latin literature");
//$PRESET_BOOK_CATEGORY["DDC"][] = array("880", "文", "Hellenic literatures; Classical Greek");
$PRESET_BOOK_CATEGORY["DDC"][] = array("890", "中國文學", "Literatures of other languages");


$PRESET_BOOK_CATEGORY["DDC"][] = array("900", "史地類", "History & geography");
$PRESET_BOOK_CATEGORY["DDC"][] = array("910", "地理及旅遊", "Geography & travel");
$PRESET_BOOK_CATEGORY["DDC"][] = array("920", "傳記", "Biography, genealogy, insignia");
$PRESET_BOOK_CATEGORY["DDC"][] = array("930", "古代史", "History of ancient world (to c. 499)");
$PRESET_BOOK_CATEGORY["DDC"][] = array("940", "歐洲史", "General history of Europe");
$PRESET_BOOK_CATEGORY["DDC"][] = array("950", "亞洲史", "General history of Asia; Far East");
$PRESET_BOOK_CATEGORY["DDC"][] = array("960", "非洲", "General history of Africa");
$PRESET_BOOK_CATEGORY["DDC"][] = array("970", "北美洲以及中美洲", "General history of North America");
$PRESET_BOOK_CATEGORY["DDC"][] = array("980", "南美洲", "General history of South America");
$PRESET_BOOK_CATEGORY["DDC"][] = array("990", "世界其他部份", "General history of other areas");


# 賴永祥分類法

$PRESET_BOOK_CATEGORY["LWC"][] = array("000", "總類", "總類");
$PRESET_BOOK_CATEGORY["LWC"][] = array("010", "目錄學總論", "目錄學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("020", "圖書館學總論", "圖書館學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("030", "國學總論", "國學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("040", "類書總論；百科全書總論", "類書總論；百科全書總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("050", "普通雜誌", "普通雜誌");
$PRESET_BOOK_CATEGORY["LWC"][] = array("060", "普通會社總論", "普通會社總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("070", "普通論叢", "普通論叢");
$PRESET_BOOK_CATEGORY["LWC"][] = array("080", "普通叢書", "普通叢書");
$PRESET_BOOK_CATEGORY["LWC"][] = array("090", "群經", "群經");
$PRESET_BOOK_CATEGORY["LWC"][] = array("100", "哲學類 ", "哲學類 ");
$PRESET_BOOK_CATEGORY["LWC"][] = array("110", "思想學問概說", "思想學問概說");
$PRESET_BOOK_CATEGORY["LWC"][] = array("120", "中國哲學總論", "中國哲學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("130", "東方哲學總論", "東方哲學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("140", "西洋哲學總論", "西洋哲學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("150", "理論學", "理論學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("160", "形上學總論", "形上學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("170", "心理學總論", "心理學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("180", "美學總論", "美學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("190", "倫理學總論", "倫理學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("200", "宗教類", "宗教類");
$PRESET_BOOK_CATEGORY["LWC"][] = array("210", "比較宗教學", "比較宗教學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("220", "佛教總論", "佛教總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("230", "道教", "道教");
$PRESET_BOOK_CATEGORY["LWC"][] = array("240", "基督教", "基督教");
$PRESET_BOOK_CATEGORY["LWC"][] = array("250", "回教", "回教");
$PRESET_BOOK_CATEGORY["LWC"][] = array("260", "猶太教", "猶太教");
$PRESET_BOOK_CATEGORY["LWC"][] = array("270", "其他宗教", "其他宗教");
$PRESET_BOOK_CATEGORY["LWC"][] = array("280", "神話學", "神話學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("290", "術數,迷信,奇跡", "術數,迷信,奇跡");
$PRESET_BOOK_CATEGORY["LWC"][] = array("300", "科學類", "科學類");
$PRESET_BOOK_CATEGORY["LWC"][] = array("310", "數學", "數學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("320", "天文學", "天文學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("330", "物理學", "物理學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("340", "化學", "化學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("350", "地質學", "地質學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("360", "生物科學", "生物科學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("370", "植物學", "植物學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("380", "動物學", "動物學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("390", "人類學", "人類學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("400", "應用科學類", "應用科學類");
$PRESET_BOOK_CATEGORY["LWC"][] = array("410", "醫藥", "醫藥");
$PRESET_BOOK_CATEGORY["LWC"][] = array("420", "家政", "家政");
$PRESET_BOOK_CATEGORY["LWC"][] = array("430", "農業", "農業");
$PRESET_BOOK_CATEGORY["LWC"][] = array("440", "工程學", "工程學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("450", "礦冶", "礦冶");
$PRESET_BOOK_CATEGORY["LWC"][] = array("460", "化學工程", "化學工程");
$PRESET_BOOK_CATEGORY["LWC"][] = array("470", "製造總論", "製造總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("480", "商業,各種營業", "商業,各種營業");
$PRESET_BOOK_CATEGORY["LWC"][] = array("490", "商學,經營學", "商學,經營學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("500", "社會科學類", "社會科學類");
$PRESET_BOOK_CATEGORY["LWC"][] = array("510", "統計學", "統計學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("520", "教育", "教育");
$PRESET_BOOK_CATEGORY["LWC"][] = array("530", "禮俗", "禮俗");
$PRESET_BOOK_CATEGORY["LWC"][] = array("540", "社會學", "社會學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("550", "經濟學", "經濟學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("560", "財政學", "財政學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("570", "政治學", "政治學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("580", "法律", "法律");
$PRESET_BOOK_CATEGORY["LWC"][] = array("590", "軍事", "軍事");
$PRESET_BOOK_CATEGORY["LWC"][] = array("600", "中國史地", "中國史地");
$PRESET_BOOK_CATEGORY["LWC"][] = array("610", "中國通史", "中國通史");
$PRESET_BOOK_CATEGORY["LWC"][] = array("620", "中國斷代史", "中國斷代史");
$PRESET_BOOK_CATEGORY["LWC"][] = array("630", "中國文化史", "中國文化史");
$PRESET_BOOK_CATEGORY["LWC"][] = array("640", "中國外交史", "中國外交史");
$PRESET_BOOK_CATEGORY["LWC"][] = array("650", "中國史料", " 中國史料");
$PRESET_BOOK_CATEGORY["LWC"][] = array("660", "中國地理總志", "中國地理總志");
$PRESET_BOOK_CATEGORY["LWC"][] = array("670", "中國地方志總論", "中國地方志總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("680", "中國地理類志", "中國地理類志");
$PRESET_BOOK_CATEGORY["LWC"][] = array("690", "中國遊記", "中國遊記");
$PRESET_BOOK_CATEGORY["LWC"][] = array("710", "世界史地 ", "世界史地 ");
$PRESET_BOOK_CATEGORY["LWC"][] = array("720", "海洋志", "海洋志");
$PRESET_BOOK_CATEGORY["LWC"][] = array("730", "亞洲史地", "亞洲史地");
$PRESET_BOOK_CATEGORY["LWC"][] = array("740", "歐洲史地", "歐洲史地");
$PRESET_BOOK_CATEGORY["LWC"][] = array("750", "美洲史地", "美洲史地");
$PRESET_BOOK_CATEGORY["LWC"][] = array("760", "非洲史地", "非洲史地");
$PRESET_BOOK_CATEGORY["LWC"][] = array("770", "澳洲及其他各國", "澳洲及其他各國");
$PRESET_BOOK_CATEGORY["LWC"][] = array("780", "傳記總論", "傳記總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("790", "文物考古總論", "文物考古總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("800", "語言文學類 ", "語言文學類 ");
$PRESET_BOOK_CATEGORY["LWC"][] = array("810", "文學總論", "文學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("820", "中國文學總論", "中國文學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("830", "中國文學總集", "中國文學總集");
$PRESET_BOOK_CATEGORY["LWC"][] = array("840", "中國文學別集", "中國文學別集");
$PRESET_BOOK_CATEGORY["LWC"][] = array("850", "各地方文學", "各地方文學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("860", "東洋文學", "東洋文學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("870", "西洋文學", "西洋文學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("880", "俄國文學", "俄國文學");
$PRESET_BOOK_CATEGORY["LWC"][] = array("890", "新聞學總論", "新聞學總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("900", "藝術類 ", "藝術類 ");
$PRESET_BOOK_CATEGORY["LWC"][] = array("910", "音樂總論", "音樂總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("920", "建築", "建築");
$PRESET_BOOK_CATEGORY["LWC"][] = array("930", "雕塑總論", "雕塑總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("940", "書畫", "書畫");
$PRESET_BOOK_CATEGORY["LWC"][] = array("950", "攝影總論", "攝影總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("960", "圖案;裝飾", "圖案;裝飾");
$PRESET_BOOK_CATEGORY["LWC"][] = array("970", "技藝總論", "技藝總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("980", "戲劇總論", "戲劇總論");
$PRESET_BOOK_CATEGORY["LWC"][] = array("990", "遊戲;娛樂;休閒", "遊戲;娛樂;休閒");


$Lang["libms"]["periodicalMgmt"]["Periodical"] = "期刊";
$Lang["libms"]["periodicalMgmt"]["PeriodicalInfo"] = "期刊資料";
$Lang["libms"]["periodicalMgmt"]["Basic"] = "基本";
$Lang["libms"]["periodicalMgmt"]["Ordering"] = "訂購";
$Lang["libms"]["periodicalMgmt"]["Registration"] = "登錄";
$Lang["libms"]["periodicalMgmt"]["PeriodicalCode"] = "期刊編號";
$Lang["libms"]["periodicalMgmt"]["PeriodicalTitle"] = "期刊書目";
$Lang["libms"]["periodicalMgmt"]["ISSN"] = "ISSN";
$Lang["libms"]["periodicalMgmt"]["Alert"] = "提示";
$Lang["libms"]["periodicalMgmt"]["Status"] = "狀態";
$Lang["libms"]["periodicalMgmt"]["DefaultStatus"] = "預設狀態";
$Lang["libms"]["periodicalMgmt"]["OrderPeriod"] = "訂購時期";
$Lang["libms"]["periodicalMgmt"]["StartDate"] = "開始";
$Lang["libms"]["periodicalMgmt"]["EndDate"] = "結束";
$Lang["libms"]["periodicalMgmt"]["OrderStartDate"] = "訂購日期";
$Lang["libms"]["periodicalMgmt"]["OrderEndDate"] = "到期日期";
$Lang["libms"]["periodicalMgmt"]["OrderPrice"] = "訂購價";
$Lang["libms"]["periodicalMgmt"]["PublishFrequency"] = "出版頻次";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyPerUnit"] = "每 <!--frequencyNum--> <!--frequencyUnit--> 出版一次";
$Lang["libms"]["periodicalMgmt"]["PublishDate"] = "出版日期";
$Lang["libms"]["periodicalMgmt"]["FirstIssue"] = "第一期";
$Lang["libms"]["periodicalMgmt"]["LastIssue"] = "最後一期";
$Lang["libms"]["periodicalMgmt"]["IssueOrCode"] = "期號 / 編號";
$Lang["libms"]["periodicalMgmt"]["Remarks"] = "備註";
$Lang["libms"]["periodicalMgmt"]["RemarksAndAlert"] = "備註及提示";
$Lang["libms"]["periodicalMgmt"]["RenewAlertDay"] = "續訂提示日數";
$Lang["libms"]["periodicalMgmt"]["RenewAlertDayRemarks"] = "選項 0 指沒有提示";
$Lang["libms"]["periodicalMgmt"]["Generate"] = "編制";
$Lang["libms"]["periodicalMgmt"]["Generated"] = "已編制";
$Lang["libms"]["periodicalMgmt"]["NotGeneratedYet"] = "未編制";
$Lang["libms"]["periodicalMgmt"]["GenerateStatus"] = "編制狀況";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateRemarks"] = "系統將會產生以下期刊項目。";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning1"] = "此訂購曾產生項目紀錄，有關此訂購的舊有期刊項目將被刪除，系統將重新產生此訂購的期刊項目。";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning2"] = "已登錄的書目及項目將不受影響，系統只會重新產生期刊相關的項目及登錄資料。";
$Lang["libms"]["periodicalMgmt"]["From"] = "由";
$Lang["libms"]["periodicalMgmt"]["To"] = "至";
$Lang["libms"]["periodicalMgmt"]["NoIssueCanBeGenerated"] = "未能就訂購設定編制項目資料";
$Lang["libms"]["periodicalMgmt"]["PeriodicalRenewAlert"] = "期刊續訂提示";
$Lang["libms"]["periodicalMgmt"]["LastPublishDate"] = "最後出版日期";
$Lang["libms"]["periodicalMgmt"]["Register"] = "登錄";
$Lang["libms"]["periodicalMgmt"]["Registered"] = "已登錄";
$Lang["libms"]["periodicalMgmt"]["NotRegistered"] = "未登錄";
$Lang["libms"]["periodicalMgmt"]["RegistrationStatus"] = "登錄狀況";
$Lang["libms"]["periodicalMgmt"]["RegistrationDate"] = "登錄日期";
$Lang["libms"]["periodicalMgmt"]["LastRegistrationDate"] = "最後登錄日期";
$Lang["libms"]["periodicalMgmt"]["RegisteredItemNum"] = "已登錄項目數目";
$Lang["libms"]["periodicalMgmt"]["Details"] = "細節";
$Lang["libms"]["periodicalMgmt"]["NoOfOrder"] = "訂購次數";
$Lang["libms"]["periodicalMgmt"]["NoOfItem"] = "項目數目";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Day"] = "日";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Week"] = "週";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Month"] = "月";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Year"] = "年";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SelectItem"] = "請選擇項目";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputValue"] = "請輸入資料";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputPositiveNum"] = "請輸入正數";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["CodeInUse"] = "編號已被使用";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["PublishDateInUse"] = "已有相同項目於此日期出版";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SaveBasicInfoFirst"] = "請先儲存基本資料";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"] = "如要離開此頁面，所有已更改的資料將不被儲存。你是否確定要離開此頁面？";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["StartDateMustBeEarlierThanEndDate"] = "開始日期必須早於結束日期";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["AddSuccess"] = "1|=|紀錄已經新增";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["AddUnsuccess"] = "0|=|新增紀錄失敗";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["UpdateSuccess"] = "1|=|紀錄已經更新";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["UpdateUnsuccess"] = "0|=|更新紀錄失敗";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["DeleteSuccess"] = "1|=|紀錄已經刪除";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["DeleteUnsuccess"] = "0|=|刪除紀錄失敗";

$Lang['libms']['NoRecordAtThisMoment'] = "暫時仍未有任何紀錄";

$Lang["libms"]["import_book_new"]["ImportCSVDataCol"] = array("<font color='red'>*</font>編號","<font color='red'>*</font>書名", "<font color='red'>*</font>作者", "系列編輯","類型","子類型","中圖分類", "杜威分類", "等級", "出版社", "語言", "出版年", "版本", "電子出版社", "簡介", "ISBN");

$Lang["libms"]["general"]["not_need_to_reply_email"] = "不用回覆此電郵";


$Lang['libms']['S_OpenTime']['SameDayReturn'] = "不准即日借還";  // settings string
$Lang['libms']["settings_alert"]['SameDayReturn'] = "不能即日借還"; //return msg 

$Lang['libms']["settings"]['system']['waive_password'] = "豁免罰款須輸入特許密碼";  // password is needed for Waive 

$Lang['libms']["settings"]['system']['circulation_teacher_by_selection'] = "流通管理允許使用清單來選取老師";
$Lang['libms']["settings"]['system']['circulation_display_accompany_material'] = "顯示借還提示時，同時顯示附件內容";
$Lang["libms"]["CirculationManagement"]["teacher_only"] = "或選擇老師"; // Teacher Selection 


$Lang["libms"]["UserSuspend"]["ModuleTitle"] = "暫停用戶";
$Lang["libms"]["UserSuspend"]["Suspend"] = "停用"; 
$Lang["libms"]["UserSuspend"]["Activate"] = "重啟";
$Lang["libms"]["UserSuspend"]["Instruction"] = "從用戶列表選取用戶增加至暫停用戶名單。<br/>如將用戶停用，即該用戶無法借閱書籍。";
$Lang["libms"]["UserSuspend"]["Suspended"] = "此用戶已被停用，因此無法借閱書籍。";
$Lang["libms"]["UserSuspend"]["SuspendedBy"] = "最後更新者";
$Lang["libms"]["UserSuspend"]["SuspendedStatus"] = "已被停用";
$Lang["libms"]["UserSuspend"]["SuspendedDate"] = "被停用日期";
$Lang["libms"]["UserSuspend"]["SuspendedList"] = "已停用列表";
$Lang["libms"]["UserSuspend"]["thisSelectedUser"] = "本次停用名單";
$Lang["libms"]["UserSuspend"]["UserList"] = "用戶列表";

$Lang["libms"]["SearchUser"]["ModuleTitle"] = "讀者搜尋";
$Lang["libms"]["SearchUser"]["Instruction"] = "從分類選項選取用戶或輸入關鍵字搜尋";
$Lang["libms"]["SearchUser"]["AllClasses"] = "所有班別";
$Lang["libms"]["SearchUser"]["Remark"] = "備註";
$Lang["libms"]["SearchUser"]["RemarkSave"] = "備註已更新!";
$Lang["libms"]["SearchUser"]["RemarkSaveButton"] = "更新備註";
$Lang["libms"]["SearchUser"]["RemarkSaveConfirm"] = "確認更新備註?";
$Lang["libms"]["SearchUser"]["RemarkSaveFail"] = "備註未能更新，請重試!";
$Lang["libms"]["SearchUser"]["UpdateRemarks"] = "更新紀錄";
$Lang["libms"]["SearchUser"]["hasNoRemark"] = "無";
$Lang["libms"]["SearchUser"]["hasRemark"] = "有";
$Lang["libms"]["SearchUser"]["AllUserType"] = "所有類別";

$Lang["libms"]["SearchUser"]["ReturnBookTitle"] = "借閱記錄";
$Lang["libms"]["SearchUser"]["ReturnBookReturnMsgFail"] = "以下書籍未能歸還:\n";
$Lang["libms"]["SearchUser"]["ReturnBookConfirm"] = "確認歸還選取書籍?";

$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['BORROWED'] = "借閱中";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['LOST'] = "已報失";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['RENEWED'] = "已續借";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['RETURNED'] = "已歸還";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['Overdue'] = "已逾期";

$Lang["libms"]["SearchUser"]["OverdueWaiveConfirm"] = "確認豁免選取罰款?";
$Lang["libms"]["SearchUser"]["OverduePayConfirm"] = "確認繳交選取罰款?";
$Lang["libms"]["SearchUser"]["OverduePay"] = "繳交";

$Lang["libms"]["SearchUser"]["CancelReservedConfirm"] = "確認取消選取預約?";
$Lang["libms"]["SearchUser"]["CancelReserved"] = $Lang["libms"]["CirculationManagement"]["cancel"].$Lang["libms"]["CirculationManagement"]["reserve"];

$Lang["libms"]["eBookStat"]["ModuleTitle"] = "電子書閱讀統計";
$Lang["libms"]["eBookStat"]["TotalBooks"] = "書本總數";
$Lang["libms"]["eBookStat"]["TotalReads"] = "點擊總數";
$Lang["libms"]["eBookStat"]["TotalReviews"] = "書評總數";
$Lang["libms"]["eBookStat"]["LastReads"] = "最近點閱";
$Lang["libms"]["eBookStat"]["Back"] = "返回列表";
$Lang["libms"]["eBookStat"]["ReviewContent"] = "書評內容";
$Lang["libms"]["eBookStat"]["ReviewRating"] = "評分";
$Lang["libms"]["eBookStat"]["ReviewHelpful"] = "讚";
$Lang["libms"]["eBookStat"]["ReviewDate"] = "建立日期";
$Lang["libms"]["eBookStat"]["incAll"] = "顯示所有用戶 (包括無書評及閱讀記錄之用戶)";
$Lang["libms"]["eBookStat"]["showReadRecordOnly"] = "只顯示有書評或閱讀記錄之用戶";
$Lang["libms"]["eBookStat"]["showPercentCol"] = "顯示閱讀進度達至";
$Lang["libms"]["eBookStat"]["showPercentCol2"] = "的用戶";
$Lang["libms"]["eBookStat"]["showPercentDownCol"] = "顯示閱讀進度少於";
$Lang["libms"]["eBookStat"]["totalBooksByPercentage"] = "進度達至%s%%的書本總數";
$Lang["libms"]["eBookStat"]["totalReadByPercentage"] = "進度達至%s%%的點擊總數";
$Lang["libms"]["eBookStat"]["totalBooksByPercentageDown"] = "進度少於%s%%的書本總數";
$Lang["libms"]["eBookStat"]["totalReadByPercentageDown"] = "進度少於%s%%的點擊總數";

$Lang["libms"]["ajaxLoading"] = "載入中...";
$Lang["libms"]["tableNoCheckedCheckbox"] = "沒有選取項目";
$Lang['General']['Instruction'] = "使用指引";
$Lang["libms"]["transfer_book"]["action_completed"] = "已完成工序";
$Lang["libms"]["transfer_book"]["add_setting_record"] = "新增設定資料";
$Lang["libms"]["transfer_book"]["book_transfer_format"] = "轉換檔案";
$Lang["libms"]["transfer_book"]["confirm"]['add_setting_record'] = "是否確定要新增設定資料?";
$Lang["libms"]["transfer_book"]["confirm"]['drop_ceo_import_table'] = "是否確定要刪除ceo匯入資料表?";
$Lang["libms"]["transfer_book"]["confirm"]['drop_tmp_import_table'] = "是否確定要刪除臨時匯入資料表?";
$Lang["libms"]["transfer_book"]["data_copy_to_db"] = "資料已匯入暫存資料庫,可匯出eLibrary Plus格式的數量: ";
$Lang["libms"]["transfer_book"]["delete_old_data"] = "刪除舊資料";
$Lang["libms"]["transfer_book"]["drop_ceo_import_table"] = "刪除ceo匯入資料表";
$Lang["libms"]["transfer_book"]["drop_tmp_import_table"] = "刪除臨時匯入資料表";
$Lang["libms"]["transfer_book"]["export_elibplus_format"] = "匯出 eLibrary Plus 檔案以匯入";
$Lang["libms"]["transfer_book"]["total_error_rows"] = "錯誤資料總行數: ";
$Lang["libms"]["transfer_book"]["total_insert_error_rows"] = "新增到暫存資料表時錯誤資料總行數: ";
$Lang["libms"]["transfer_book"]["select_school"] = "請選擇學校";
$Lang["libms"]["portal"]["add_news"] = "新增消息";
$Lang["libms"]["portal"]["admin_settings"] = "設定及圖書管理";
$Lang["libms"]["portal"]["advanced_search"] = "進階搜尋";
$Lang["libms"]["portal"]["advancedSearch"]["sortBy"]["bookID"] = "書本添加順序";
$Lang["libms"]["portal"]["all"] = "全部";
$Lang["libms"]["portal"]["best_rated"] = "最佳評分";
$Lang["libms"]["portal"]["book"]["I_shared_to"] = "我把此書分享給 ";
$Lang["libms"]["portal"]["book"]["total_reviews"] = "書評 (%s)";
$Lang["libms"]["portal"]["book"]["total_shared_by_me"] = "我的分享 (%s)";
$Lang["libms"]["portal"]["book"]["total_shared_to_me"] = "與我分享 (%s)";
$Lang["libms"]["portal"]["book_detail"]["cancel_reserved_confirm"] = "確認取消本書預約?";
$Lang["libms"]["portal"]["book_details"] = "詳細資料";
$Lang["libms"]["portal"]["book_title"] = "書名";
$Lang["libms"]["portal"]["books_found"] = "本書";
$Lang["libms"]["portal"]["browse_by_categories"] = "以圖書類別瀏覽";
$Lang["libms"]["portal"]["borrowed"] = "已借閱";
$Lang["libms"]["portal"]["closing"] = "休館中";
$Lang["libms"]["portal"]["demo"]["read_info"] = "悅讀資訊";
$Lang["libms"]["portal"]["eBooks"] = "電子書";
$Lang["libms"]["portal"]["eBooks_ranking"] = "電子書排行榜";
$Lang["libms"]["portal"]["edit_news"] = "編輯消息";
$Lang["libms"]["portal"]["edit_rules"] = "編輯規則";
$Lang["libms"]["portal"]["expand"] = "展開";
$Lang["libms"]["portal"]["hide"] = "隱藏";
$Lang["libms"]["portal"]["me"] = "我";
$Lang["libms"]["portal"]["month"]["Jan"] = "一月";
$Lang["libms"]["portal"]["month"]["Feb"] = "二月";
$Lang["libms"]["portal"]["month"]["Mar"] = "三月";
$Lang["libms"]["portal"]["month"]["Apr"] = "四月";
$Lang["libms"]["portal"]["month"]["May"] = "五月";
$Lang["libms"]["portal"]["month"]["Jun"] = "六月";
$Lang["libms"]["portal"]["month"]["Jul"] = "七月";
$Lang["libms"]["portal"]["month"]["Aug"] = "八月";
$Lang["libms"]["portal"]["month"]["Sep"] = "九月";
$Lang["libms"]["portal"]["month"]["Oct"] = "十月";
$Lang["libms"]["portal"]["month"]["Nov"] = "十一月";
$Lang["libms"]["portal"]["month"]["Dec"] = "十二月";
$Lang["libms"]["portal"]["most_active_readers"] = "點閱最多的讀者";
$Lang["libms"]["portal"]["most_hit"] = "最高點閱";
$Lang["libms"]["portal"]["most_loan"] = "借閱最多";
$Lang["libms"]["portal"]["my_friend"] = "書友";
$Lang["libms"]["portal"]["my_friends"] = "書友互動";
$Lang["libms"]["portal"]["my_record"]["cancel_reserve_fail"] = "取消預約失敗";
$Lang["libms"]["portal"]["my_record"]["checkout_due_date"] = "預約保留期限";
$Lang["libms"]["portal"]["myfriends"]["accept"] = "接受";
$Lang["libms"]["portal"]["myfriends"]["accept_friend_request"] = "已接受加你做書友";
$Lang["libms"]["portal"]["myfriends"]["add_friends"] = "新增書友";
$Lang["libms"]["portal"]["myfriends"]["back"] = "返回";
$Lang["libms"]["portal"]["myfriends"]["book_total"] = "本書";
$Lang["libms"]["portal"]["myfriends"]["borrow_a_book"] = "借了一本書";
$Lang["libms"]["portal"]["myfriends"]["comments"] = "感想";
$Lang["libms"]["portal"]["myfriends"]["ebook_total"] = "本電子書";
$Lang["libms"]["portal"]["myfriends"]["friend_profile"] = "書友摘要";
$Lang["libms"]["portal"]["myfriends"]["friends_activities"] = "書友動態";
$Lang["libms"]["portal"]["myfriends"]["friends_list"] = "書友";
$Lang["libms"]["portal"]["myfriends"]["at"] = "於";
$Lang["libms"]["portal"]["myfriends"]["identity"]["NonTeachingStaff"] = "非教學職務員工";
$Lang["libms"]["portal"]["myfriends"]["identity"]["Staff"] = "教職員";
$Lang["libms"]["portal"]["myfriends"]["identity"]["TeachingStaff"] = "教學職務員工";
$Lang["libms"]["portal"]["myfriends"]["ignore"] = "拒絕";
$Lang["libms"]["portal"]["myfriends"]["new"] = "新";
$Lang["libms"]["portal"]["myfriends"]["no_friends"] = "暫時沒有書友";
$Lang["libms"]["portal"]["myfriends"]["no_friends_activities"] = "暫時沒有書友動態";
$Lang["libms"]["portal"]["myfriends"]["no_notifications"] = "暫時沒有通知";
$Lang["libms"]["portal"]["myfriends"]["notifications"] = "通知";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["ebooks_viewed"] = "電子書閱讀紀錄";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["books_borrowed"] = "借閱紀錄";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["reviews"] = "書評紀錄";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["sharing"] = "分享紀錄";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["borrowers"] = "借閱者";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["ebook"] = "本電子書";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_borrowing"] = "暫時沒有書友借閱排名";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_reading"] = "暫時沒有書友點閱排名";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_review"] = "暫時沒有書友書評排名";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["pbook"] = "本書";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["review"] = "書評";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["reviewers"] = "書評作者";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["viewers"] = "點閱者";
$Lang["libms"]["portal"]["myfriends"]["reading_date"] = "閱讀日期";
$Lang["libms"]["portal"]["myfriends"]["remove_request"] = "移除請求";
$Lang["libms"]["portal"]["myfriends"]["review_a_book"] = "寫了一則書評";
$Lang["libms"]["portal"]["myfriends"]["search_friends"] = "搜尋書友";
$Lang["libms"]["portal"]["myfriends"]["selected_friends"] = "已選 (%s)";
$Lang["libms"]["portal"]["myfriends"]["send_friend_request"] = "請求加你做書友";
$Lang["libms"]["portal"]["myfriends"]["share_a_book"] = "與你分享了一本書";
$Lang["libms"]["portal"]["myfriends"]["share_a_book_to_me"] = " 與我分享了一本書";
$Lang["libms"]["portal"]["myfriends"]["share_a_book_by_me"] = "我分享了一本書給 ";
$Lang["libms"]["portal"]["myfriends"]["shared_by"] = "分享者";
$Lang["libms"]["portal"]["myfriends"]["status"]["wainting_for_accept"] = "等待接受中";
$Lang["libms"]["portal"]["myfriends"]["summary"]["books_borrowed"] = "借書總數";
$Lang["libms"]["portal"]["myfriends"]["summary"]["ebook_viewed"] = "電子書點閱總數";
$Lang["libms"]["portal"]["myfriends"]["summary"]["no_of_reviews"] = "書評總數";
$Lang["libms"]["portal"]["myfriends"]["unfriend"] = "移除書友";
$Lang["libms"]["portal"]["myfriends"]["warning"]["add_friends"] = "只有學生或老師才可新增書友";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_delete_notification"] = "你是否確定刪除這則告示?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_delete_shared_to_me"] = "你是否確定刪除這項與我的分享?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_remove_friend_request"] = "你是否確定刪除此書友請求?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_unfriend"] = "你是否確定刪除此書友?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["select_at_least_one_friend"] = "請選擇最少一個書友";
$Lang["libms"]["portal"]["myfriends"]["warning"]["select_class"] = "請選擇班別";
$Lang["libms"]["portal"]["myfriends"]["warning"]["select_friend"] = "請選擇書友名字";
$Lang["libms"]["portal"]["navigation"]["display_per_page"] = "每頁顯示";
$Lang["libms"]["portal"]["navigation"]["item"] = "項";
$Lang["libms"]["portal"]["navigation"]["num_name"] = "第";
$Lang["libms"]["portal"]["navigation"]["page"] = "頁";
$Lang["libms"]["portal"]["news"]["start_date_empty"] = "開始日期不能留空";
$Lang["libms"]["portal"]["news"]["end_date_empty"] = "結束日期不能留空";
$Lang["libms"]["portal"]["next_week"] = "下星期";
$Lang["libms"]["portal"]["not_open"] = "尚未	開放";
$Lang["libms"]["portal"]["off_shelf"] = "此書已下架";
$Lang["libms"]["portal"]["opening"] = "開放中";
$Lang["libms"]["portal"]["pBooks"] = "實體書";
$Lang["libms"]["portal"]["pBooks_ranking"] = "實體書排行榜";
$Lang["libms"]["portal"]["pinned"] = "置頂";
$Lang["libms"]["portal"]["previous_week"] = "上星期";
$Lang["libms"]["portal"]["quota"]["loan"] = "借書限額";
$Lang["libms"]["portal"]["quota"]["reserve"] = "預約限額";
$Lang["libms"]["portal"]["reader"] = "讀者";
$Lang["libms"]["portal"]["recommend"] = "推介此書";
$Lang["libms"]["portal"]["recommendbookto"] = "推介好書給";
$Lang["libms"]["portal"]["related_books"] = "相關書籍";
$Lang["libms"]["portal"]["require_field"] = "附有<span style='color: red'> * </span>的項目必須填寫";
$Lang["libms"]["portal"]["reserved"] = "已預約";
$Lang["libms"]["portal"]["result_list"]["hits_loans"] = "點閱率/借閱";
$Lang["libms"]["portal"]["reviews"] = "書評";
$Lang["libms"]["portal"]["setting"]["announcement"] = "告示";
$Lang["libms"]["portal"]["setting"]["book_category"] = "圖書分類";
$Lang["libms"]["portal"]["setting"]["carousel"] = "巡迴顯示";
$Lang["libms"]["portal"]["setting"]["carousel_ebook_inteval"] = "電子書巡迴顯示時間間隔";
$Lang["libms"]["portal"]["setting"]["carousel_pbook_inteval"] = "實體書巡迴顯示時間間隔";
$Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_min"] = "分";
$Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_sec"] = "秒";
$Lang["libms"]["portal"]["setting"]["carousel_remark"] = "時間間隔設定為零表示停止巡迴顯示";
$Lang["libms"]["portal"]["setting"]["carousel_stop"] = "停止巡迴顯示";
$Lang["libms"]["portal"]["setting"]["data_range"] = "數據範圍";
$Lang["libms"]["portal"]["setting"]["display_advanced_search"] = "顯示進階搜尋";
$Lang["libms"]["portal"]["setting"]["display_best_rated"] = "顯示最佳評分書目";
$Lang["libms"]["portal"]["setting"]["display_category_chi_ebooks"] = "顯示中文電子書分類";
$Lang["libms"]["portal"]["setting"]["display_category_eng_ebooks"] = "顯示英文電子書分類";
$Lang["libms"]["portal"]["setting"]["display_category_physical_books"] = "顯示實體書分類";
$Lang["libms"]["portal"]["setting"]["display_category_tags"] = "顯示標籤";
$Lang["libms"]["portal"]["setting"]["display_keyword_search"] = "顯示關鍵字搜尋";
$Lang["libms"]["portal"]["setting"]["display_list_all_books"] = "顯示全部顯示";
$Lang["libms"]["portal"]["setting"]["display_most_active_borrowers"] = "顯示借書最多的讀者";
$Lang["libms"]["portal"]["setting"]["display_most_active_reviewers"] = "顯示最活躍的書評作者";
$Lang["libms"]["portal"]["setting"]["display_most_helpful_reviews"] = "顯示最有用書評";
$Lang["libms"]["portal"]["setting"]["display_most_hit"] = "顯示最高點閱書目";
$Lang["libms"]["portal"]["setting"]["display_most_loan"] = "顯示借閱最多書目";
$Lang["libms"]["portal"]["setting"]["display_new"] = "顯示最新書目";
$Lang["libms"]["portal"]["setting"]["display_news"] = "顯示最新消息";
$Lang["libms"]["portal"]["setting"]["display_opening_hours"] = "顯示開放時間";
$Lang["libms"]["portal"]["setting"]["display_recommend"] = "顯示推介書目";
$Lang["libms"]["portal"]["setting"]["display_rules"] = "顯示規則";
$Lang["libms"]["portal"]["setting"]["edit_settings"] = "編輯設定";
$Lang["libms"]["portal"]["setting"]["keyword_search"] = "關鍵字搜尋";
$Lang["libms"]["portal"]["setting"]["my_friends"] = "書友互動";
$Lang["libms"]["portal"]["setting"]["my_friends_search_scope"] = "學生可搜尋的書友範圍";
$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["all_classes"] = "全校";
$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_class"] = "同班";
$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_classlevel"] = "同級";
$Lang["libms"]["portal"]["setting"]["opac_allow_search_ebook"] = "允許搜尋電子書";
$Lang["libms"]["portal"]["setting"]["opac_remark_allow_search_ebook"] = "必須開啟「允許搜尋電子書」";
$Lang["libms"]["portal"]["setting"]["open_my_friends"] = "開放書友互動功能";
$Lang["libms"]["portal"]["setting"]["open_opac"] = "開放OPAC";
$Lang["libms"]["portal"]["setting"]["others"] = "其他";
$Lang["libms"]["portal"]["setting"]["ranking"] = "排行榜";
$Lang["libms"]["portal"]["settings"] = "主頁設定";
$Lang["libms"]["portal"]["settings_tab_internal"] = "內部";
$Lang["libms"]["portal"]["settings_tab_opac"] = "OPAC";
$Lang["libms"]["portal"]["share"] = "分享";
$Lang["libms"]["portal"]["statistics"]["totalHitsChi"] = "中文書點擊總數";
$Lang["libms"]["portal"]["statistics"]["totalHitsEng"] = "英文書點擊總數";
$Lang["libms"]["portal"]["temp_close"] = "暫停開放";
$Lang["libms"]["portal"]["theme_settings"] = "設定個人化主題";
$Lang["libms"]["portal"]["totalRecords"] = "個紀錄";
$Lang["libms"]["portal"]["type_here"] = "請輸入...";
$Lang["libms"]["portal"]["warning"]["allow_edit_one_review_only"] = "每次只能編輯一個書評";
$Lang["libms"]["portal"]["warning"]["fail_del_tmp_attachment"] = "移除臨時附件失敗!";
$Lang["libms"]["opac"]["member_login"] = "登入";
$Lang["libms"]["iPortfolio"]["BookCategory"] = "類別";
$Lang["libms"]["iPortfolio"]["BorrowReadDate"] = "借書/閱讀日期";
$Lang["libms"]["iPortfolio"]["PhysicalOrEBook"] = "實體書/電子書";
?>