<?php
// Modifying by:
// left menu
$Lang['ePCM']['ManagementArr']['MenuTitle'] = 'Management';
$Lang['ePCM']['ManagementArr']['ViewArr']['MenuTitle'] = 'Current Application';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['MenuTitle'] = 'Finished Application';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['OtherApplication'] = 'Misc Expenditure';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Finished'] = 'Completed Applications';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Terminated'] = 'Terminated Applications';
$Lang['ePCM']['ReportArr']['MenuTitle'] = 'Reports';
$Lang['ePCM']['ReportArr']['PCMRecordSummaryArr']['MenuTitle'] = 'Procurement Record Summary';
$Lang['ePCM']['ReportArr']['FileCodeSummaryArr']['MenuTitle'] = 'File Code Summary';
$Lang['ePCM']['ReportArr']['BudgetExpensesReportArr']['MenuTitle'] = 'Budget and Expenses Report';
$Lang['ePCM']['SettingsArr']['MenuTitle'] = 'Settings';
$Lang['ePCM']['SettingsArr']['GeneralArr']['MenuTitle'] = 'General';
$Lang['ePCM']['SettingsArr']['SupplierArr']['MenuTitle'] = 'Supplier';
$Lang['ePCM']['SettingsArr']['SupplierArr']['TabTitle'] = 'Supplier';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TabTitle'] = 'Supplier Type';
$Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'] = 'Group';
$Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'] = 'Category';
$Lang['ePCM']['SettingsArr']['RoleArr']['MenuTitle'] = 'Role';
$Lang['ePCM']['SettingsArr']['RuleArr']['MenuTitle'] = 'Rule';
$Lang['ePCM']['SettingsArr']['SecurityArr']['MenuTitle'] = 'Security';
$Lang['ePCM']['SettingsArr']['AlertArr']['MenuTitle'] = 'Alert';
$Lang['ePCM']['SettingsArr']['SampleDocumentArr']['MenuTitle'] = 'Sample Document';
$Lang['ePCM']['SettingsArr']['FundingSource']['MenuTitle'] = 'Funding Source';

// General
$Lang['ePCM']['General']['Select'] = '- Select -';
$Lang['ePCM']['General']['Click here to download sample'] = 'Click here to download sample';
$Lang['ePCM']['General']['SaveAsDraft'] = 'Save as Draft';
$Lang['ePCM']['General']['Notice'] = 'Notice';
$Lang['ePCM']['General']['Enable'] = 'Enable';
$Lang['ePCM']['General']['Disable'] = 'Disable';
$Lang['ePCM']['General']['Principal'] = 'Principal';
$Lang['ePCM']['General']['VicePrincipal'] = 'Vice Principal';
$Lang['ePCM']['General']['NoRecord'] = 'No record at the moment';
$Lang['ePCM']['General']['jsWaitAllFilesUploaded'] = "* please wait until all files are completed uploading.";
$Lang['ePCM']['General']['ErrorCode']='Error Code';
$Lang['ePCM']['General']['Description']='Description';
$Lang['ePCM']['General']['LineNumber']='Line Number';
$Lang['ePCM']['General']['DataColumnDefault']='Correct Data Column';
$Lang['ePCM']['General']['DataColumnUser']='User input';

$Lang['ePCM']['Import']['Error']['NO_FILE'] = 'Please select import file';
$Lang['ePCM']['Import']['Error']['INCORRECT_EXT'] = 'Incorrect file extension';
$Lang['ePCM']['Import']['Error']['INCORRECT_HEADING'] = 'Headings unmatched';
$Lang['ePCM']['Import']['Error']['EMPTY_TYPE_NAME_CHI'] = 'Type name (Chi) is empty';
$Lang['ePCM']['Import']['Error']['EMPTY_TYPE_NAME_ENG'] = 'Type name (Eng) is empty';
$Lang['ePCM']['Import']['Error']['EMPTY_SUPPLIER_NAME_CHI']='Supplier name (Chi) is empty';
$Lang['ePCM']['Import']['Error']['EMPTY_SUPPLIER_NAME_ENG']='Supplier name (Eng) is empty';
$Lang['ePCM']['Import']['Error']['EMPTY_PHONE']='Phone is empty';
$Lang['ePCM']['Import']['Error']['EMPTY_TYPE']='Type is empty';
$Lang['ePCM']['Import']['Error']['INVALID_TYPE']='Type do not exist';

$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_1'] = 'Funding source (1) is empty';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_2'] = 'Funding source (2) is empty';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_SAME'] = 'Funding source (1) and Funding source (2) cannot be the same';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_1'] = 'Funding price (1) cannot be 0';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_2'] = 'Funding price (2) cannot be 0';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_Total'] = 'The total funding price cannot be greater than the deal price';

// Symbol
$Lang['PCM']['Symbol']['Colon'] = ': ';
$Lang['PCM']['Symbol']['DollarSign'] = '$ ';

// General Setting
$Lang['ePCM']['GeneralSetting']['CodeGeneration'] = 'Case code generate method';
$Lang['ePCM']['GeneralSetting']['CodeGenerationSequence'] = 'Case code Sequence number generate method';
$Lang['ePCM']['GeneralSetting']['CodeNumberOfDigit'] = 'Case code Sequence number digits';
$Lang['ePCM']['GeneralSetting']['emailNotification'] = 'Enable email notification';
$Lang['ePCM']['GeneralSetting']['eClassAppNotification'] = 'Enable eClassApp notification';
$Lang['ePCM']['GeneralSetting']['MiscExpenditure'] = 'Enable Misc Expenditure';
$Lang['ePCM']['GeneralSetting']['MiscExpenditureApproval'] = 'Need Approval';
    $Lang['ePCM']['GeneralSetting']['CronJobEnableEmail']='Enable auto notification (Email)';
$Lang['ePCM']['GeneralSetting']['CronJobEnablePushNoti']='Enable auto notification (eClassApp)';
$Lang['ePCM']['GeneralSetting']['caseApprovalMethod'] = 'Case approval method';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Case Code must include Sequence Number'] = 'Case code must include Sequence Number';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Case Code partials must not be duplicated'] = 'Case code partials must not be duplicated';
$Lang['ePCM']['GeneralSetting']['ControlArr']['N/A'] = 'N/A';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Academic Year'] = 'Academic year';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Category Code'] = 'Category code';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Financial Item Code'] = 'Financial item code';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Sequence Number'] = 'Sequence number';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Group code'] = 'Group code';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Please choose Academic Year'] = 'Please choose Academic Year';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Please enter case code sequence digits'] = 'Please enter case code sequence digits';
$Lang['ePCM']['GeneralSetting']['ControlArr']['General'] = 'General';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Notification'] = 'Notification';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Rule Code']='Rule Code';
$Lang['ePCM']['GeneralSetting']['ExceedBudgetPercentage']='Allow n% of Budget exceed of both Group\'s and Financial item\'s budget';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Unlimited']='No Limit';
$Lang['ePCM']['GeneralSetting']['ControlArr']['CannotExceed']='Cannot Exceed';
$Lang['ePCM']['caseApprovalMethod']['all']='ALL approver approve the case';
$Lang['ePCM']['caseApprovalMethod']['oneVicePrincipalAndAllOthers']='ONE vice principal (if applicable) and ALL other approvers';
// content - Setting > Security
$Lang['ePCM']['SettingsArr']['SecurityArr']['AuthenticationArr']['TabTitle'] = 'Access setting';
$Lang['ePCM']['SettingsArr']['SecurityArr']['AuthenticationArr']['SettingTitle'] = 'Authenticate when first time access';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['TabTitle'] = 'Network settings';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['InstructionTitle'] = 'Instruction';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['InstructionContent'] = 'In IP Address box, you can input:
		<ol>
			<li>Exact IP Address (e.g. 192.168.0.101)</li>
			<li>IP Address Range (e.g. 192.168.0.[10-100])</li>
			<li>CISCO Style Subnet Range (e.g. 192.168.0.0/24)</li>
			<li>Allow All incoming IP address (Input 0.0.0.0)</li>
		</ol>
		<!--<font color="red">For Card reader-connecting PC, system suggests you should set that machine in FIXED IP address (i.e. Not use DHCP), in order to ensure data transmission source is legitimate.</font>
		<br>//-->
		This System has security policy to prevent Faked IP Address attack.';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['SettingTitle'] = 'IP address';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['SettingControlTitle'] = 'Please input the IP address in each line<br />Your Current IP Address';

// Supplier
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['All Types'] = 'All types';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Active'] = 'Active';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Inactive'] = 'Inactive';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Activate'] = 'Activate';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Inactivate'] = 'Inactivate';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose a supplier'] = 'Please choose a supplier';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose not more than one supplier'] = 'Please choose not more than one supplier';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose at least one supplier'] = 'Please choose at least one supplier';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] = 'Confirm delete?';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Add Contact Person'] = 'Add contact person';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Remove Contact Person'] = 'Remove contact person';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Code'] = 'Please input code';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'] = 'Please input supplier Name';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input an URL'] = 'Please input an URL';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Phone'] = 'Please input phone';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input an Email Address'] = 'Please input an email address';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Type'] = 'Please input type';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Activate?'] = 'Confrim Activate?';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Inactivate?'] = 'Confrim inactivate?';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['All'] = 'Active / Inactive';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier'] = 'New Supplier';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier Type'] = 'New Supplier Type';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Contact Name']='Please input Contact Name';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Position']='Please input Position';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input valid Phone']='Please input valid Phone';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input valid Fax']='Please input valid Fax';

$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'] = 'Supplier';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Code'] = 'Code';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name'] = 'Supplier name (English)';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name Chi'] = 'Supplier name (Chinese)';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Website'] = 'Website';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Description'] = 'Description';
$Lang['ePCM']['SettingsArr']['SupplierArr']['RegisteredLetter'] = 'Registered Letter';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'] = 'Phone';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Fax'] = 'Fax';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Email'] = 'Email';
$Lang['ePCM']['SettingsArr']['SupplierArr']['InPerson'] = 'In Person';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Type'] = 'Type';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Quote Invitation'] = 'Quote invitation';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Quote'] = 'Quote';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Bid Invitation'] = 'Bid invitation';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Bid'] = 'Bid';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Deal'] = 'Deal';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Last Deal Date'] = 'Last deal date';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Contact Person'] = 'Contact person';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Contact Name'] = 'Contact name';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Position'] = 'Position';

$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeName'] = 'Type Name (English)';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeNameChi'] = 'Type Name (Chinese)';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'] = 'Please input Type Name';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose at least one supplier type']='Please choose at least one supplier type';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Confrim Delete?']='Confrim Delete?';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Duplicated Name']='Duplicated Name';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose a supplier type']='Please choose a supplier type';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose not more than one supplier type']='Please choose not more than one supplier type';

$Lang['ePCM']['SettingsArr']['SupplierArr']['Import']['Reference']['Type']='Click here to check Type Code';

$Lang['ePCM']['SettingsArr']['SupplierArr']['Total'] = 'Total';

// General Language
$Lang['ePCM']['SettingsArr']['New'] = 'New';
$Lang['ePCM']['SettingsArr']['Edit'] = 'Edit';

// Group
$Lang['ePCM']['Group']['Group'] = 'Group';
$Lang['ePCM']['Group']['GroupItem'] = 'Group Item';
$Lang['ePCM']['Group']['UserLogin'] = '(Input multiple UserLogin by adding "," between each UserLogin)';
$Lang['ePCM']['Group']['MemberInput'] = '(Input multiple Member by adding "," between each Member)';
$Lang['ePCM']['Group']['GroupHeadIncorrect'] = 'UserLogin not existed';
$Lang['ePCM']['Group']['NotATeacher'] = "This is not a teacher, please verify it";
$Lang['ePCM']['Group']['Code'] = 'Code';
$Lang['ePCM']['Group']['GroupName'] = 'Group name (English)';
$Lang['ePCM']['Group']['GroupNameChi'] = 'Group name (Chinese)';
$Lang['ePCM']['Group']['GroupCode'] = 'Group Code';
$Lang['ePCM']['Group']['Item']['FinancialCode'] = 'Code';
$Lang['ePCM']['Group']['Item']['FinancialItemChi'] = 'Financial item (Chinese)';
$Lang['ePCM']['Group']['Item']['FinancialItemEng'] = 'Financial item (English)';
$Lang['ePCM']['Group']['GroupNotExist'] = 'Group does not exist';
$Lang['ePCM']['Group']['ItemNotExist'] = 'Item does not exist';
$Lang['ePCM']['Group']['Item']['DuplicatedCode'] = 'Code is duplicated';
$Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroup'] = 'Department/Group';
$Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupItem'] = 'Department/Group item';
$Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupBudget'] = 'Department/Group item budget';
$Lang['ePCM']['Group']['Item']['Budget']['financialItem'] = 'Financial Item';
$Lang['ePCM']['Group']['Item']['Budget']['fundingSource'] = 'Funding Source';
$Lang['ePCM']['Group']['Item']['Budget']['defaultFunding'] = 'Default Funding Source';
$Lang["ePCM"]["Group"]["GroupCodeEnquiry"] = '[Press to show Group Code]';
$Lang['ePCM']['Group']['FundingSourceEnquiry'] = '[Press to show Funding Source]';
$Lang['ePCM']['Group']['RelatedGroupFinancialItemEnquiry'] = '[Press to show related Group Financial Item]';
$Lang['ePCM']['Group']['ItemCode'] = 'Item Code';
$Lang['ePCM']['Group']['Budget'] = 'Budget';
$Lang['ePCM']['Group']['GroupBudget'] = 'Group Overall Budget';
$Lang['ePCM']['Group']['BudgetRemain'] = 'Budget remain';
$Lang['ePCM']['Group']['Member'] = 'Member';
$Lang['ePCM']['Group']['SetGroupHead'] = 'Set to group head';
$Lang['ePCM']['Group']['RemoveGroupHead'] = 'Remove from group head';
$Lang['ePCM']['Group']['GroupItemName'] = 'Financial item (English)';
$Lang['ePCM']['Group']['GroupItemNameChi'] = 'Financial item (Chinese)';
$Lang['ePCM']['Group']['GroupHead'] = 'Group head';
$Lang['ePCM']['Setting']['Group']['AddMember'] = 'Add member';
$Lang['ePCM']['Setting']['Group']['Edit Member'] = 'Edit member';
$Lang['ePCM']['Group']['GroupHeadRemarks'] = '<span class="tabletextrequire">*</span> Group Head';
$Lang['ePCM']['Group']['Please input Code'] = 'Please input Code';
$Lang['ePCM']['Group']['Please input Group Name'] = 'Please input Group Name';
$Lang['ePCM']['Group']['Please input Item Name'] = 'Please input Item Name';
$Lang['ePCM']['Group']['Duplicated code'] = 'Duplicated code';
$Lang['ePCM']['Group']['Warning']['ChooseAGroup'] = 'Please select a group';
$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroup'] = 'Please select at lease one group';
$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroup'] = 'Please choose not more than one group';
$Lang['ePCM']['Group']['Warning']['ChooseAGroupMember'] = 'Please select a group member';
$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupMember'] = 'Please select at lease one group member';
$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroupMember'] = 'Please choose not more than one group member';
$Lang['ePCM']['Group']['Warning']['ChooseAGroupItem'] = 'Please select a Financial item';
$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupItem'] = 'Please select at lease one Financial item';
$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroupItem'] = 'Please choose not more than one Financial item';
$Lang['ePCM']['Group']['Warning']['ConfirmSetToAdmin'] = 'Confirm setting user(s) to admin';
$Lang['ePCM']['Group']['Warning']['ConfirmRemoveFromAdmin'] = 'Confirm removing user(s) from admin';
$Lang['ePCM']['Group']['Import']['ImportFromInv']='Import from Resource Management Group (eInventory)';
$Lang['ePCM']['Group']['Import']['ChooseGroup']='Choose Group';
$Lang['ePCM']['Group']['Import']['AdditionalImport']='Additional Import';
$Lang['ePCM']['Group']['Import']['ImportGroupMember']='Import Group Member';
$Lang['ePCM']['Group']['Import']['ImportGroupHead']='Import Group Leader as Group Head';
$Lang['ePCM']['Group']['Import']['Warning']['ChooseOneOrMoreGroup'] = 'Please select at lease one group';
$Lang['ePCM']['Group']['Import']['Step1']='Select Import Criteria';
$Lang['ePCM']['Group']['Import']['Step2']='Data Verification';
$Lang['ePCM']['Group']['Import']['Step3']='Imported Result';
$Lang['ePCM']['Group']['Import']['GroupNameChi']='Group Name (Chinese)';
$Lang['ePCM']['Group']['Import']['GroupNameEng']='Group Name (English)';
$Lang['ePCM']['Group']['Import']['Code']='Code';
$Lang['ePCM']['Group']['Import']['GroupMember']='Member';
$Lang['ePCM']['Group']['Import']['GroupHead']=' (# as Group Head)';
$Lang['ePCM']['Group']['Import']['ImportStatus']='Import Status';
$Lang['ePCM']['Group']['Import']['Error']['CodeExist']='Code has been used';
// Group Budget
$Lang['ePCM']['Group']['BudgetArr']['ControlArr']['Edit Budget'] = 'Edit budget';

$Lang['ePCM']['Group']['BudgetArr']['Academic Year'] = 'Academic year';
$Lang['ePCM']['Group']['BudgetArr']['Budget'] = 'Budget';
$Lang['ePCM']['Group']['BudgetArr']['Item Budget'] = 'Financial item budget';
$Lang['ePCM']['Group']['BudgetArr']['Total'] = 'Total';
$Lang['ePCM']['Group']['BudgetArr']['Please input Academic Year'] = 'Please input Academic Year';
$Lang['ePCM']['Group']['BudgetArr']['Budget must be positive'] = 'Budget must be positive';

// Category
$Lang['ePCM']['Category']['Category'] = 'Category';
$Lang['ePCM']['Category']['Code'] = 'Code';
$Lang['ePCM']['Category']['CategoryName'] = 'Category name (English)';
$Lang['ePCM']['Category']['CategoryNameChi'] = 'Category name (Chinese)';
$Lang['ePCM']['Category']['Please input Code'] = 'Please input Code';
$Lang['ePCM']['Category']['Please input Category Name'] = 'Please input Category Name';
$Lang['ePCM']['Category']['Duplicated code'] = 'Duplicated code';

$Lang['ePCM']['Category']['Warning']['ChooseACategory'] = 'Please select a category';
$Lang['ePCM']['Category']['Warning']['ChooseOneOrMoreCategory'] = 'lease select at least one category';
$Lang['ePCM']['Category']['Warning']['ChooseNoMoreThanACategory'] = 'Please select no more than one category';

// Rule
$Lang['ePCM']['Setting']['Rule']['FloorPrice'] = 'Price range (Lower limit)';
$Lang['ePCM']['Setting']['Rule']['CeillingPrice'] = 'Price range (Upper limit)';
$Lang['ePCM']['Setting']['Rule']['CaseApproval'] = 'Case approval';
$Lang['ePCM']['Setting']['Rule']['ApprovalMode'] = 'Approval Mode';
$Lang['ePCM']['Setting']['Rule']['ApprovalTogether'] = 'Approve simultaneously';
$Lang['ePCM']['Setting']['Rule']['ApprovalSeperately'] = 'Approve sequentially according to selected sequence';
$Lang['ePCM']['Setting']['Rule']['ApprovalSeperatelyRemarks'] = '<span class="tabletextremark"> (Group head -> Vice Principal -> Principal)</span>'; 
$Lang['ePCM']['Setting']['Rule']['QuotationType'] = 'Quotation type';
$Lang['ePCM']['Setting']['Rule']['QuotationApproval'] = 'Input result';
$Lang['ePCM']['Setting']['Rule']['TenderAuditing'] = 'Tender auditing';
$Lang['ePCM']['Setting']['Rule']['TenderApproval'] = 'Tender approval';
$Lang['ePCM']['Setting']['Rule']['MinInvitation'] = 'Minimum quotation invitation to be sent ';
$Lang['ePCM']['Setting']['Rule']['NumTemplateFile'] = 'Total Number of rule template file';
$Lang['ePCM']['Setting']['Rule']['NoNeed'] = 'N/A';
$Lang['ePCM']['Setting']['Rule']['Verbal']='Verbal Quotation';
$Lang['ePCM']['Setting']['Rule']['Quotation'] = 'Written Quotation';
$Lang['ePCM']['Setting']['Rule']['Tender'] = 'Tender';
$Lang['ePCM']['Setting']['Rule']['GroupHead'] = 'Group head';
$Lang['ePCM']['Setting']['Rule']['GroupHeadAndPricipal'] = 'Group head and principal';
$Lang['ePCM']['Setting']['Rule']['GroupHeadAndVicePrincipal']='Group head and vice principal';
$Lang['ePCM']['Setting']['Rule']['GroupHeadAndVicePrincipalAndPrincipal']='Group head, vice principal and principal';
$Lang['ePCM']['Setting']['Rule']['VicePrincipalAndPrincipal'] = 'Vice principal and principal';
$Lang['ePCM']['Setting']['Rule']['Remarks']['PriceRange']='e.g.: From 5001 To 20000';
$Lang['ePCM']['Setting']['Rule']['Remarks']['rollbackTemplateFile'] = '<span class="tabletextrequire">*</span><span class="tabletextremark">To cancel changes on the rule template files, please click "Cancel"';

$Lang['ePCM']['Setting']['Rule']['PriceRange'] = 'Price range';
$Lang['ePCM']['Setting']['Rule']['From'] = 'From';
$Lang['ePCM']['Setting']['Rule']['To'] = 'To';
$Lang['ePCM']['Setting']['Rule']['And'] = ' and ';
$Lang['ePCM']['Setting']['Rule']['Please input code']='Please input code';
$Lang['ePCM']['Setting']['Rule']['Please input price range (from)'] = 'Please input price range (from)';
$Lang['ePCM']['Setting']['Rule']['Please input price range (to)'] = 'Please input price range (to)';
$Lang['ePCM']['Setting']['Rule']['Please choose case approval'] = 'Please choose case approval';
$Lang['ePCM']['Setting']['Rule']['Please select quotation type'] = 'Please select quotation type';
$Lang['ePCM']['Setting']['Rule']['BlankAsUnlimited'] = 'Leave Blank for No Upper Limit';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['TenderAuditing']='Please choose from tender auditing';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['TenderApproval']='Please choose from tender approval';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['QuotationApproval']='Please choose from quotation auditing';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['MinQuote']='Please choose from minimum quotation invitation';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['Approved']='Object is already approved';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['ApproveRight']='Only Group Header and Admin can approve the case';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['RejectRight']='Only Group Header and Admin can reject the case';

$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseARule'] = 'Please select a rule';
$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseOneOrMoreRule'] = 'Please select at least one rule';
$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseNoMoreThanARule'] = 'Please select no more than one rule';
$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][0] = 'The smallest floor price must be $0';
$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][1] = 'The largest ceilling price must be blank (no limit)';
$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][2] = 'Any price must belong to only one rule';
$Lang['ePCM']['Setting']['Rule']['Warning']['SoemthingWrong'] = 'The followings is violated:';
$Lang['ePCM']['Setting']['Rule']['PriceRangeNegative'] = 'Price Range value should be positive integer';
$Lang['ePCM']['Setting']['Rule']['floorPriceLarger'] = 'Floor price must not larger than ceilling price';
$Lang['ePCM']['Setting']['Rule']['All'] = 'All ';
$Lang['ePCM']['Setting']['Rule']['AtLeast'] = 'At least <!--n--> ';
$Lang['ePCM']['Setting']['Rule']['PleaseSelectRole'] = 'Please select at least 1 role to approval/ endorse';
$Lang['ePCM']['Setting']['Rule']['AllPeople'] = 'All';
$Lang['ePCM']['Setting']['Rule']['Required'] = 'Required';
$Lang['ePCM']['Setting']['Rule']['Approve'] = 'to approve';
$Lang['ePCM']['Setting']['Rule']['Reset'] = 'Reset';
// Role
$Lang['ePCM']['Setting']['Role']['Role'] = 'Role';
$Lang['ePCM']['Setting']['Role']['RoleName'] = 'Role name (English)';
$Lang['ePCM']['Setting']['Role']['RoleNameChi'] = 'Role name (Chinese)';
$Lang['ePCM']['Setting']['Role']['Member'] = 'Member';
$Lang['ePCM']['Setting']['Role']['IsPrincipal'] = 'Set as principal';
$Lang['ePCM']['Setting']['Role']['Responsibility'] = 'Responsibility';
$Lang['ePCM']['Setting']['Role']['Please input Role Name'] = 'Please input Role Name';
$Lang['ePCM']['Setting']['Role']['CopyFromOtherAcademicYear']='Copy from other academic year';

$Lang['ePCM']['Setting']['Role']['Warning']['ChooseARole'] = 'Please select a role';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseOneOrMoreRole'] = 'Please select at least one role';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseNoMoreThanARole'] = 'Please select no more than one role';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseARoleMember'] = 'Please select a role member';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseOneOrMoreRoleMember'] = 'Please select at least one role member';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseNoMoreThanARoleMember'] = 'Please select at least one role member';
$Lang['ePCM']['Setting']['Rule']['Warning']['PriceBetween']='Price Between ';
$Lang['ePCM']['Setting']['Rule']['Warning']['PriceMissing']=' is missing from rules';
$Lang['ePCM']['Setting']['Rule']['Warning']['PriceOverlapping']=' is overlapped by more than one rule';
$Lang['ePCM']['Setting']['Rule']['UnlimitedCeillingInNonLastRule'] = 'Ceilling Price left blank in non-largest price rule';

$Lang['ePCM']['Setting']['Role']['DeletedUserLegend'] = '<font style="color:red;">*</font> Representing that the person has been deleted or left already.';

$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseAItem'] = 'Please select an item';
$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseOneOrMoreItem'] = 'Please select no more than one item';

$Lang['ePCM']['Setting']['Rule']['uploadSampleFile'] = 'Upload rule sample files';

$Lang['ePCM']['Setting']['Yes'] = 'Yes';
$Lang['ePCM']['Setting']['No'] = 'No';
$Lang['ePCM']['Setting']['Template'][1] = 'Application form';
$Lang['ePCM']['Setting']['Template'][2] = 'Invitation documents';
$Lang['ePCM']['Setting']['Template'][3] = 'Declaration of interests';
$Lang['ePCM']['Setting']['Template'][4] = 'Approval form';

// Case
$Lang['ePCM']['Mgmt']['Case']['CaseName'] = 'Name';
$Lang['ePCM']['Mgmt']['Case']['CaseName2'] = 'Purchase Item';
$Lang['ePCM']['Mgmt']['Case']['CaseApplication'] = 'Case Application';
$Lang['ePCM']['Mgmt']['Case']['CaseCode'] = 'Case code';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['ApprovalStatus'] = 'Approval status';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approve'] = 'Approve';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Reject'] = 'Reject';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Rejected'] = 'Rejected';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Pending'] = 'Pending for approval';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved'] = 'Approved';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Feedback'] = 'Feedback';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsed'] = 'Endorsed';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['AdminAction'] = 'By <!--adminName--> (on behalf of the user)';
$Lang['ePCM']['Mgmt']['Case']['CaseApplication'] = 'Case application';
$Lang['ePCM']['Mgmt']['Case']['CaseApproval'] = 'Case approval';
$Lang['ePCM']['Mgmt']['Case']['Endorsement'] = 'Endorsement';
$Lang['ePCM']['Mgmt']['Case']['Tender'] = 'Tender';
$Lang['ePCM']['Mgmt']['Case']['TenderOpening'] = 'Tender opening';
$Lang['ePCM']['Mgmt']['Case']['TenderOpeningStartDate'] = 'Start Date';
$Lang['ePCM']['Mgmt']['Case']['TenderOpeningEndDate'] = 'Deadlines';
$Lang['ePCM']['Mgmt']['Case']['TenderApprovalStartDate'] = 'Start Date';
$Lang['ePCM']['Mgmt']['Case']['TenderApprovalEndDate'] = 'Deadlines';
$Lang['ePCM']['Mgmt']['Case']['TenderApproval'] = 'Tender approval';
$Lang['ePCM']['Mgmt']['Case']['Quotation'] = 'Quotation';
$Lang['ePCM']['Mgmt']['Case']['QuotationApproval'] = 'Input Result';
$Lang['ePCM']['Mgmt']['Case']['FinishOn'] = 'Finished on <!--datetime-->.';
$Lang['ePCM']['Mgmt']['Case']['Finished'] = 'Finished';
$Lang['ePCM']['Mgmt']['Case']['NotFinished'] = 'In progress';
$Lang['ePCM']['Mgmt']['Case']['LastUpdate'] = 'Last update';
$Lang['ePCM']['Mgmt']['Case']['Purpose'] = 'Purpose';
$Lang['ePCM']['Mgmt']['Case']['Budget'] = 'Budget';
$Lang['ePCM']['Mgmt']['Case']['Applicant'] = 'Applicant';
$Lang['ePCM']['Mgmt']['Case']['Status']['Status'] = 'Status';
$Lang['ePCM']['Mgmt']['Case']['Status']['AllStatus'] = 'All Status';
$Lang['ePCM']['Mgmt']['Case']['Status']['Approved'] = 'Approved';
$Lang['ePCM']['Mgmt']['Case']['Status']['Pending'] = 'Pending';
$Lang['ePCM']['Mgmt']['Case']['Status']['Reject'] = 'Rejected';
$Lang['ePCM']['Mgmt']['Case']['Status']['AllStatusForConfirmation'] = 'All Status';
$Lang['ePCM']['Mgmt']['Case']['Status']['ConfirmStatus'] = 'Confirm Status';
$Lang['ePCM']['Mgmt']['Case']['Status']['Confirmed'] = 'Confirmed';
$Lang['ePCM']['Mgmt']['Case']['Status']['PendingForConfirmation'] = 'Pending';
$Lang['ePCM']['Mgmt']['Case']['ChooseIndividual'] = 'Choose Applicant (Admin only)';
$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'] = 'Application date';
$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['Status']='Reply Status';
$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['NoReply']='No Reply';
$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['Submitted']='Submitted';
$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Rejected'] = 'Rejected';
$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Pending'] = 'Waiting for reply';
$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Accepted'] = 'Accepted';
$Lang['ePCM']['Mgmt']['Case']['Category'] = 'Category';
$Lang['ePCM']['Mgmt']['Case']['Group'] = 'Group';
$Lang['ePCM']['Mgmt']['Case']['FinancialItem'] = 'Financial item';
$Lang['ePCM']['Mgmt']['Case']['Approver'] = 'Approver';
$Lang['ePCM']['Mgmt']['Case']['Endorser'] = 'Endorser';
$Lang['ePCM']['Mgmt']['Case']['Remarks'] = 'Remarks';
$Lang['ePCM']['Mgmt']['Case']['Document'] = 'Attachment';
$Lang['ePCM']['Mgmt']['Case']['InvitationLetter']='Invitation Letter';
$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Individual'] = 'Individual';
$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Group'] = 'Group';
$Lang['ePCM']['Mgmt']['Case']['InputResult'] = 'Input Result';
$Lang['ePCM']['Mgmt']['Case']['InvitationOfSuppliersStr'] = '{-numOfQuotation-} Invitation(s) sent to supplier(s)';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['ContactPerson'] = 'Contact person';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['ResponseStatus'] = 'Response status';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['ResponseDate'] = 'Response date';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['AddInvitation'] = 'Select supplier';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['Reason'] = 'Reasons';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['NewSupplierType'] = 'New Type';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['SchoolType'] = 'General Type';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['GroupType'] = 'Custom Type';
$Lang['ePCM']['Mgmt']['Case']['Warning']['NotEnoughSupplier'] = 'At lease {-MinQuote-} invitations is required. Reasons must be filled if requirement could not be acheived.';
$Lang['ePCM']['Mgmt']['Case']['Warning']['NeedStartNewCase'] = 'This case would be replaced by a new case due to approval invitations were already sent. Case approval invitation will send once re-starting a new case. Are you sure to do so?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsDeleted'] = 'This case has been deleted, all information are reference only';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Termination'] = 'Are you sure terminating this case?';
$Lang['ePCM']['Mgmt']['Case']['UndoTermination']='Undo Termination';
$Lang['ePCM']['Mgmt']['Case']['Warning']['SelectApprovalStatus'] = 'Please select approval status';
$Lang['ePCM']['Mgmt']['Case']['Warning']['SelectSupplier'] = 'Please select at lease one supplier';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputRemark'] = 'Please input remarks';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputReason'] = 'Please input reasons';
$Lang['ePCM']['Mgmt']['Case']['Warning']['PositiveDealPrice'] = 'Please input a positive deal price';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputDeleteReason'] = 'Please input reason of termination';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputQuoteDate'] = 'Please input quotation date';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Confrim2NextStage'] = 'Confrim going to next stage?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputEndDateFirst'] = 'Please input End date first';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DeleteQuotation'] = 'Are you sure deleting this invitation?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderOpeningStartDate'] = 'Please select date no earlier than Tender closing deadline';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderOpeningEndDate'] = 'Please select date no earlier than Tender opening start date';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderApprovalStartDate'] = 'Please select date no earlier than Tender opening deadline';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderApprovalEndDate'] = 'Please select date no earlier than Tender approval start date';
$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsRejected'] = 'This case has been rejected, click edit button on the right-hand corner of the page to resubmit the application';
$Lang['ePCM']['Mgmt']['Case']['Warning']['SubmitDraft'] = 'Are you sure submitting this draft?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['ConfirmReject'] = 'Are you sure to reject?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['TenderCustWarning'] = 'Please select whether need to tender';
$Lang['ePCM']['Mgmt']['Case']['ItemName'] = 'Item Name';
$Lang['ePCM']['Mgmt']['Case']['Confirm'] = 'Confirm';
$Lang['ePCM']['Mgmt']['Case']['UndoConfirm'] = 'Undo Confirm';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Confirmed']='Object is already confirmed';
$Lang['ePCM']['Mgmt']['Case']['Warning']['ApplicationIsConfirmed']='Application is already confirmed';
$Lang['ePCM']['Mgmt']['Case']['Warning']['ApplicationIsNotConfirmed']='Application is not confirmed';

$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['CaseName']='Please input case name';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Category']='Please select category';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Reason']='Please input Reason/Objective';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Group']='Please select group';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Item']='Please select item';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Budget']='Please input budget';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['NotInteger']='Please input integer';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['ApplicantType']='Please select applicant type';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DealPrice']='Please input deal price';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DecimalPoint'] = 'Please accurate to only second decimal places';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DuplicatedFunding'] = 'Please dont enter duplicated funding source';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['OverFundingSourceBudget'] = 'Over funding source budget';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['FundingOverBudget'] = 'Sum of funding source budget cannot be greater than budget';

$Lang['ePCM']['Mgmt']['Case']['Deadlines'] = 'Deadlines';
$Lang['ePCM']['Mgmt']['Case']['TenderClosing'] = 'Tender Closing';
$Lang['ePCM']['Mgmt']['Case']['OnlyNDaysLeft'][0] = 'Only';
$Lang['ePCM']['Mgmt']['Case']['OnlyNDaysLeft'][1] = 'Day(s) Left';
$Lang['ePCM']['Mgmt']['Case']['AutoNotify'] = 'System Notification';
$Lang['ePCM']['Mgmt']['Case']['DaysBefore'] = ' day(s) before';
$Lang['ePCM']['Mgmt']['Case']['NotifyTarget'] = 'User to be notified';
$Lang['ePCM']['Mgmt']['Case']['ToNextStage'] = 'To next stage';
$Lang['ePCM']['Mgmt']['Case']['UploadQuoation'] = 'Upload Quotations';
$Lang['ePCM']['Mgmt']['Case']['UploadTenders'] = 'Upload Tenders';
$Lang['ePCM']['Mgmt']['Case']['UploadDocument'] = 'Upload Documents';
$Lang['ePCM']['Mgmt']['Case']['Declaration'] = 'Declaration of interests';
$Lang['ePCM']['Mgmt']['Case']['QuotationDate'] = 'Quotation Date';
$Lang['ePCM']['Mgmt']['Case']['TenderDate'] = 'Tender Date';
$Lang['ePCM']['Mgmt']['Case']['LastModified'] = 'Last Modified (Time and User)';
$Lang['ePCM']['Mgmt']['Case']['LateSubmition'] = 'Late Submition';
$Lang['ePCM']['Mgmt']['Case']['Result']['DealPrice'] = 'Deal Price';
$Lang['ePCM']['Mgmt']['Case']['Result']['DealDate'] = 'Deal Date';
$Lang['ePCM']['Mgmt']['Case']['Result']['Details'] = 'Details';
$Lang['ePCM']['Mgmt']['Case']['Result']['Description'] = 'Remarks';
$Lang['ePCM']['Mgmt']['Case']['Result']['FundingSource'] = 'Funding Source';
$Lang['ePCM']['Mgmt']['Case']['Result']['Reason'] = 'Reason for not accepting the lowest offer';
$Lang['ePCM']['Mgmt']['Case']['Result']['ReasonWarning'] = 'Please input reason for not accepting the lowest offer';
$Lang['ePCM']['Mgmt']['Case']['Result']['RecordDay'] = 'Record Date';
$Lang['ePCM']['Mgmt']['Case']['Result']['Amount'] = 'Amount';
$Lang['ePCM']['Mgmt']['Case']['ApplicationReceived'] = 'Application is well received';
$Lang['ePCM']['Mgmt']['Case']['EmailNoti'] = 'Send Email Notification';
$Lang['ePCM']['Mgmt']['Case']['PushNoti'] = 'Send Push Notification (eClassApp)';
$Lang['ePCM']['Mgmt']['Case']['ApplicationWillBeApprovedBy'] = 'Your application will be approved by';
$Lang['ePCM']['Mgmt']['Case']['PreviousRecord'] = 'Previous Record';
$Lang['ePCM']['Mgmt']['Case']['Termination'] = 'Termination';
$Lang['ePCM']['Mgmt']['Case']['UndoTermination'] = 'Undo Termination';
$Lang['ePCM']['Mgmt']['Case']['DeleteDraft'] = 'Delete Draft';
$Lang['ePCM']['Mgmt']['Case']['DeleteReason'] = 'Reason of termination';
$Lang['ePCM']['Mgmt']['Case']['DeleteReason2'] = 'Reason';
$Lang['ePCM']['Mgmt']['Case']['Email']['Title'] = '[eProcurement] A new case for approval - <!--CASE_CODE-->';
$Lang['ePCM']['Mgmt']['Case']['Email']['Content'] = 'You have a new case for approval. (<!--CASE_CODE-->)';
$Lang['ePCM']['Mgmt']['Case']['PushMessage']['Title'] = '[採購系統] 有一申請等侯批核 - <!--CASE_CODE-->
[eProcurement] A new case for approval - <!--CASE_CODE-->';
$Lang['ePCM']['Mgmt']['Case']['PushMessage']['Content'] = '你有一申請等侯批核。 (<!--CASE_CODE-->) <!--CASE_NAME-->。 請登入內聯網檢視詳情。
You have a new case for approval. (<!--CASE_CODE-->) <!--CASE_NAME-->. Please log in to the intranet for further details.';
$Lang['ePCM']['Mgmt']['Case']['Sent successfully'] = 'Sent successfully';
$Lang['ePCM']['Mgmt']['Case']['Failed to send'] = 'Failed to send';
$Lang['ePCM']['Mgmt']['Case']['Failed to send mail'] = 'Failed to send mail to following users internal or Alternative email:';
$Lang['ePCM']['Mgmt']['Case']['Failed to send pushMessage'] = 'Failed to send push message to following users:';
$Lang['ePCM']['Mgmt']['Case']['Sending'] = 'Sending';
$Lang['ePCM']['Mgmt']['Case']['IsDeleted'] = 'Is terminated';
$Lang['ePCM']['Mgmt']['Case']['EmailToSupplier'] = 'Send Invitation Email';
$Lang['ePCM']['Mgmt']['Case']['BudgetLeft'] = '${-Budget-} left';
$Lang['ePCM']['Mgmt']['Case']['Warning']['StopNewApplication'] = 'New application is locked for the following reason(s):';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Reason']['RulesInvalid'] = 'Invalid rules are set, please contact your eProcurement administrator';
$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Group']='Budget exceeded group preset budget limit';
$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Item']='Budget exceeded financial item preset budget limit';
$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem']='Budget exceeded both group and financial item preset budget limit';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Duplicated']['Code']='Code is duplicated';
$Lang['ePCM']['Mgmt']['Case']['SubmitDraft']='Submit Draft';
$Lang['ePCM']['Mgmt']['Case']['No Emails are sent']='No Emails are sent';
$Lang['ePCM']['Mgmt']['Case']['Emails are sent']='Emails are sent';
$Lang['ePCM']['Mgmt']['Case']['Sending']='Sending';
$Lang['ePCM']['Mgmt']['Case']['Re-send Invitation Email']='Re-send Invitation Email';
$Lang['ePCM']['Mgmt']['Case']['Latest Email Sent Date']='Latest Email Sent Date';
$Lang['ePCM']['Mgmt']['Case']['InputPriceComparison']='Edit Price Comparison Table';
$Lang['ePCM']['Mgmt']['Case']['PriceComparisonTable']='Price Comparison Table';
$Lang['ePCM']['Mgmt']['Case']['AddNewPriceItem']='Add Price Item';

$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['FormTitle'] = 'Procurement Application Form';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['PrintBtn'] = 'Print Application Form';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['PrincipalApproval'] = 'Approved by Principal';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['DateOfApproval'] = 'Date of Approval';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['CaseName'] = 'CaseName';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['CategoryName'] = 'Category Name ';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Description'] = 'Purpose';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['GroupName'] = 'Group Name';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['ItemName'] = 'ItemName';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Budget'] = 'Budget';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Remarks'] = 'Remarks';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['FundingSourceArr'] = 'Funding Source';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Location'] = 'Location';

$Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnEn'] = 'Export Purchase-by-Oral Quotation  Form (ANNEX II)';
$Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnB5'] = 'Export Purchase-by-Oral Quotation  Form (ANNEX II)(CHI)';

$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['AllType']="All Quotation Type";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['N']="N/A";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['V']="Verbal Quotation";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['Q']="Written Quotation";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['T']="Tender";

$Lang['ePCM']['Mgmt']['Case']['Filter']['Category']['AllType']="All Category";
$Lang['ePCM']['Mgmt']['Case']['Filter']['FinancialItem']['AllType'] = "All Financial item";
$Lang['ePCM']['Mgmt']['Case']['Filter']['Supplier']['AllType'] = "All Supplier";
$Lang['ePCM']['Mgmt']['Case']['Filter']['Applicant']['AllType'] = "All Applicant";

$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox2']['Q']='Please prepare invitation letters and written quotation/tender document with the terms and conditions for each written quotation/tender exercise. All written quotation/tender documents should be sent to the selected suppliers by registered mail. Schools should maintain on file the recorded delivery receipts issued by the post office, showing the names of the suppliers and the date of despatch for audit purpose. As backup and for vendors to acknowledge directly, invitation by email is provided here.';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox2']['T']='Please prepare invitation letters and written quotation/tender document with the terms and conditions for each written quotation/tender exercise. All written quotation/tender documents should be sent to the selected suppliers by registered mail. Schools should maintain on file the recorded delivery receipts issued by the post office, showing the names of the suppliers and the date of despatch for audit purpose. As backup and for vendors to acknowledge directly, invitation by email is provided here.';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox2']['V']='Please record quotation information. Input price comparison table and/or send invitation email if needed.';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox3']['Q']='After vetting the written quotation documents, requesting the subject teacher(s)/administrative staff to make recommendations on the written quotation and completing the relevant parts of the Written Quotation Summary and Approval Record, the written quotation opening authority will then pass this record and the written quotation documents to the approving authority for consideration and approval.';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox3']['T']='After vetting the written tender documents, requesting the subject teacher(s)/administrative staff to make recommendations on the written tender and completing the relevant parts of the Written Tender Summary and Approval Record, the written tender opening authority will then pass this record and the written tender documents to the approving authority for consideration and approval.';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox4']['Q']='After considering and approval, the approving authority must sign the Written Quotation/Tender Summary and Approval Record and store the soft copy here for school record.';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox4']['T']='After considering and approval, the approving authority must sign the Written Quotation/Tender Summary and Approval Record and store the soft copy here for school record.';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox4']['V']='Please input procurement result, related documents could be uploaded for records';

$Lang['ePCM']['Mgmt']['Case']['UpdateFailed']='Update Failed';
$Lang['ePCM']['Mgmt']['Case']['RelatedUsers']='Related Users';
$Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['ZeroPercent']='(Budget cannot exceeds of both <strong>Group\'s</strong> and <strong>Financial item\'s</strong> budget remains)';
$Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['NPercent']='(Budget cannot exceeds of both <strong>Group\'s</strong> and <strong>Financial item\'s</strong> budget remains for <!--percent-->%)';
$Lang['ePCM']['Mgmt']['Case']['FundingSource'] = 'Funding Source';
$Lang['ePCM']['Mgmt']['Case']['MustTender'] = 'Require tender';

$Lang['ePCM']['Mgmt']['FundingName']['NotApplicable']= 'Not Applicable';
$Lang['ePCM']['Mgmt']['FundingName']['School']='School';
$Lang['ePCM']['Mgmt']['FundingName']['Government']='Government';
$Lang['ePCM']['Mgmt']['FundingName']['SponsoringBody']='Sponsoring Body';
$Lang['ePCM']['Mgmt']['Funding']['Unit_Price']='Unit_Price';
//Download Documents
$Lang['ePCM']['Mgmt']['DownloadDocuments']['Summary']['Eng']['Q']="Download Quotation Summary and Approval Record (English Version)";
$Lang['ePCM']['Mgmt']['DownloadDocuments']['Summary']['Eng']['T']="Download Tender Summary and Approval Record (English Version)";
$Lang['ePCM']['Mgmt']['UploadDocuments']['Summary'] = 'Upload Signed Summary';

// Quotation
$Lang['ePCM']['Mgmt']['Quotation']['ContactMethod'] = 'Invitation By';

// Email to Supplier
$Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Subject'] = '報價邀請 (<!--CASE_NUMBER-->)';
$Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Body'] = '' . '敬啟者：<br/>' . '<br/>' . '現誠邀 貴公司 (<!--SUPPLIER_NAME-->)承投本校「<!--CASE_NAME-->」(編號<!--CASE_NUMBER-->)。詳情請參附件。<br/>' . '<br/>' . '另外，煩請按以下連結確認，以讓本校得知 貴公司已收到此邀請。'.'<br/>'.'<!--TOKEN_END_DATE-->'.'<br/>' . '<br/>' . '<!--TOKEN_URL_START--><!--TOKEN_URL--><!--TOKEN_URL_END--><br/>'.'以下密碼供 貴公司在以上連結下載有關文件: <!--API_PASSWORD--><br/>'. '<br/>' . '<!--SCHOOL_NAME-->';
// $Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Password']='以下密碼供 貴公司在以上連結下載有關文件: ';
$Lang['ePCM']['Mgmt']['Email']['Preview']='Preview Email';


// Login
$Lang['ePCM']['Index']['Login']['Authentication'] = 'Authentication';
$Lang['ePCM']['Index']['Login']['Please login with your user password.'] = 'Please login with your user password.';
$Lang['ePCM']['Index']['Login']['Password'] = 'Password';
$Lang['ePCM']['Index']['Login']['InputPassword'] = "Please input password";

// IP Failed
$Lang['ePCM']['Index']['NetworkIPFail']['Message'] = 'Your computer is not allowed to access eProcurement';

// Report:: Common
$Lang['ePCM']['Report']['ReportFormat']['ReportFormat'] = 'Report Format';
$Lang['ePCM']['Report']['ReportFormat']['Pdf'] = 'PDF';
$Lang['ePCM']['Report']['ReportFormat']['Csv'] = 'CSV';
$Lang['ePCM']['Report']['Order'] = 'Order';
$Lang['ePCM']['Report']['Code'] = 'Code';
$Lang['ePCM']['Report']['ApplyDate'] = 'Apply Date';
// Report::FCS
$Lang['ePCM']['Report']['FCS']['DateRange'] = 'Date';
$Lang['ePCM']['Report']['FCS']['to'] = 'to';
$Lang['ePCM']['Report']['FCS']['Category'] = 'Category';
$Lang['ePCM']['Report']['FCS']['Group'] = 'Group';
$Lang['ePCM']['Report']['FCS']['AmountRange'] = 'Budget Range';
$Lang['ePCM']['Report']['FCS'][' or above'] = ' or above';
$Lang['ePCM']['Report']['FCS']['Status'] = 'Status';
$Lang['ePCM']['Report']['FCS']['Application in progress'] = 'Application in progress';
$Lang['ePCM']['Report']['FCS']['Completed'] = 'Completed';
$Lang['ePCM']['Report']['FCS']['Rejected'] = 'Rejected';
$Lang['ePCM']['Report']['FCS']['Get Report'] = 'Get Report';
$Lang['ePCM']['Report']['FCS']['Please input Date'] = 'Please input Date';
$Lang['ePCM']['Report']['FCS']['Invalid Date Range'] = 'Invalid Date Range';
$Lang['ePCM']['Report']['FCS']['Please choose a Group'] = 'Please choose a Group';
$Lang['ePCM']['Report']['FCS']['AcademicYear'] = '';
$Lang['ePCM']['Report']['FCS']['File Code Summary'] = 'Procurement File Code Summary';
$Lang['ePCM']['Report']['FCS']['Code'] = 'Code';
$Lang['ePCM']['Report']['FCS']['Name'] = 'Name';
$Lang['ePCM']['Report']['FCS']['Application Date'] = 'Application Date';
$Lang['ePCM']['Report']['FCS']['Applicant'] = 'Applicant';
$Lang['ePCM']['Report']['FCS']['Budget'] = 'Budget';
$Lang['ePCM']['Report']['FCS']['Remarks'] = 'Remarks';
$Lang['ePCM']['Report']['FCS']['No Applications'] = 'No Applications';
$Lang['ePCM']['Report']['FCS']['Academic Year'] = 'Academic year';
$Lang['ePCM']['Report']['FCS']['Please input Academic Year'] = 'Please input Academic Year';
$Lang['ePCM']['Report']['FCS']['TStart'] = 'Tender Start Date';
$Lang['ePCM']['Report']['FCS']['TEnd'] = 'Tender End Date';
$Lang['ePCM']['Report']['FCS']['QStart'] = 'Quotation Start Date';
$Lang['ePCM']['Report']['FCS']['QEnd'] = 'Quotation End Date';
$Lang['ePCM']['Report']['FCS']['Category'] = 'Category';
$Lang['ePCM']['Report']['FCS']['IncludeZeroExpenses'] = 'Include items with zero expenses';
// Report: PCMRS
$Lang['ePCM']['Report']['PCMRS']['Title'] = 'Procurement Record Summary';

// Report::BER
$Lang['ePCM']['Report']['BER']['AcademicYear']='';
$Lang['ePCM']['Report']['BER']['Budget and Expenses Report']='Budget and Expenses Report';
$Lang['ePCM']['Report']['BER']['Budget']='Budget';
$Lang['ePCM']['Report']['BER']['Dollar']='Dollar';
$Lang['ePCM']['Report']['BER']['Item']='Item';
$Lang['ePCM']['Report']['BER']['Deal Price']='Deal Price';
$Lang['ePCM']['Report']['BER']['Sum']='Sum';
$Lang['ePCM']['Report']['BER']['No Items']='No Items';
$Lang['ePCM']['Report']['BER']['Difference']='Remaining';
$Lang['ePCM']['Report']['BER']['Items'] = 'Items';
$Lang['ePCM']['Report']['BER']['ApplicationType']='Record Type';
$Lang['ePCM']['Report']['BER']['ProcurementRecords'] = 'Procurement Expenditure';
$Lang['ePCM']['Report']['BER']['OtherRecords'] = 'Misc Expenditure';
// Push Notification
$Lang['ePCM']['ScheduledPushNotification']['Deadline']['Title'] = '採購系統申請書面報價/截標快將到期';
$Lang['ePCM']['ScheduledPushNotification']['Deadline']['Message'] = '你的採購系統申請，其書面報價/截標快將到期。';
$Lang['ePCM']['DeclarationInstruction']='Staff involved in procurement and supplies duties should be required to sign an undertaking that they would declare in writing to the SMCs/IMCs any current or future connection they or their families have with suppliers or contractors (e.g. being relatives, owners, shareholders) as soon as they become aware of it. <br> 
		Schools are required to properly record any declarations (a standard form is provided at Annex I for reference) or disclosures made and necessary action taken to avoid any actual or perceived conflict of interest. <br> 
		The staff declaring conflict of interest have to refrain from processing the related quotation/tender or follow the instruction of SMCs/IMCs. <br> 
		This requirement should be annually brought to the notice of the staff involved by means of a circular which they should be required to sign to indicate that they have read and understood it.';

$Lang['ePCM']['Template'] = 'Template';
$Lang['ePCM']['RoleManagement']['SMC'] = 'SMC';
$Lang['ePCM']['FundingSource']['FundingSource'] = 'Funding Source';
$Lang['ePCM']['FundingSource']['RecordStatus']['ALL'] = 'ALL Status';
// $Lang['ePCM']['FundingSource']['RecordStatus'][0] = 'Inactivated';
$Lang['ePCM']['FundingSource']['RecordStatus'][1] = 'Activated';
$Lang['ePCM']['FundingSource']['RecordStatus'][2] = 'Deactivated';
$Lang['ePCM']['FundingSource']['EnglishName'] = 'English Name';
$Lang['ePCM']['FundingSource']['ChineseName'] = 'Chinese Name';
$Lang['ePCM']['FundingSource']['Funding'] = 'Funding';
$Lang['ePCM']['FundingSource']['Category'] = 'Funding Source Category';
$Lang['ePCM']['FundingSource']['Budget'] = 'Amount';
$Lang['ePCM']['FundingSource']['RemainingBudget'] = 'Remaining amount';
$Lang['ePCM']['FundingSource']['DeadLine'] = 'Expiry Date';
$Lang['ePCM']['FundingSource']['CaseUsingFunding'] = 'Case(s) using funding';
$Lang['ePCM']['FundingSource']['RecordStatus']['RecordStatus'] = 'Record Status';
$Lang['ePCM']['FundingSource']['Add'] = 'Add';
$Lang['ePCM']['FundingSource']['Edit'] = 'Edit';
$Lang['ePCM']['FundingSource']['DeleteAlert'] = 'Are u confirm to delete related record';
$Lang['ePCM']['FundingSource']['EditAlert'] = 'This record cannot be empty';
$Lang['ePCM']['FundingSource']['UsingBudget'] = 'Using Budget';
$Lang['ePCM']['FundingSource']['ApprovalStatus']['0'] = 'Pending for endorsement';
$Lang['ePCM']['FundingSource']['ApprovalStatus']['1'] = 'Endorsed';
$Lang['ePCM']['FundingSource']['ApprovalStatus']['2'] = 'Rejected';
$Lang['ePCM']['FundingSource']['RecordStatusIncorrect'] = 'Incorrect record status';
$Lang['ePCM']['FundingSource']['FundingIncorrect'] = 'Funding is not existed';
$Lang['ePCM']['FundingSource']['FundingCatIncorrect'] = 'Funding category is not existed';
$Lang['ePCM']['FundingSource']['ImportCheckFundingCategory'] = 'Press to show funding category';
##### Import Helper & Validator [start] #####
$Lang['ePCM']['Import']['Step2Loading'] = 'Please wait, validating the file contents.';
$Lang['ePCM']['Import']['Remarks'] = 'Remarks';
$Lang['ePCM']['Import']['Success'] = 'Successfully import <!--count--> records.';
$Lang['ePCM']['Import']['Done'] = 'Import is done already.';
$Lang['ePCM']['Import']['Fail1'] = 'Some records in not imported, please re-try.';
$Lang['ePCM']['Import']['Fail2'] = 'Fail';
$Lang['ePCM']['Import']['Ref']['Code'] = 'Code';
$Lang['ePCM']['Import']['DateFormatRemarks'] = 'Format：YYYY-MM-DD';
$Lang['ePCM']['Import']['Validator']['required'] = '<!--name--> cannot be empty';
$Lang['ePCM']['Import']['Validator']['maxlength'] = 'The length of <!--name--> cannot exceed <!--length--> characters';
$Lang['ePCM']['Import']['Validator']['date'] = 'Date format of <!--name--> invalid';
$Lang['ePCM']['Import']['Validator']['numeric'] = '<!--name--> must be numeric';
##### Import Helper & Validator [end] #####

$Lang['Btn']['close']= 'close';

// cust lang here [start]
if($sys_custom['ePCM_skipCaseApproval']) {
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['AdminAction'] = 'By <!--adminName--> (on behalf of the user)';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Pending'] = 'Pending for endorsement';
    $Lang['ePCM']['Mgmt']['Case']['CaseApproval'] = 'Endorsement';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['ApprovalStatus'] = 'Endorsement status';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved'] = 'Endorsed';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approve'] = 'Endorse';
}

$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['AllStatus'] = 'All Endorsement Status';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CasePendingForYourApproval'] = 'Case Pending For Your Approval';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CaseYouApproved'] = 'Case You Approved';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CaseYouRejected'] = 'Case You Rejected';

$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage']['AllStage']="All Stage";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][0]="My Draft";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][1]="Case application";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][2]="Verbal Quotation / Quotation / Tender";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][3]="Upload Quotations / Tender opening";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][4]="Quotation / Tender approval";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][5]=$Lang['ePCM']['Mgmt']['Case']['Endorsement'];
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][6]=$Lang['ePCM']['Mgmt']['Case']['Endorsement'];
// cust lang here [end]

if (!$NoLangWordings && is_file("$intranet_root/templates/ePCM_lang.$intranet_session_language.customized.php"))
{
    include("$intranet_root/templates/ePCM_lang.$intranet_session_language.customized.php");
}
?>