<?php
// Modifing by:
// left menu

$Lang['ePCM']['ManagementArr']['MenuTitle'] = '管理';
$Lang['ePCM']['ManagementArr']['ViewArr']['MenuTitle'] = '現時申請';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['MenuTitle'] = '完成的申請';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['OtherApplication'] = '雜項支出';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Finished'] = '完成的申請';
$Lang['ePCM']['ManagementArr']['FinishedViewArr']['Tab']['Terminated'] = '已中止的申請';
$Lang['ePCM']['ReportArr']['MenuTitle'] = '報告';
$Lang['ePCM']['ReportArr']['PCMRecordSummaryArr']['MenuTitle'] = '採購紀錄總表';
$Lang['ePCM']['ReportArr']['FileCodeSummaryArr']['MenuTitle'] = '檔案編號總表';
$Lang['ePCM']['ReportArr']['BudgetExpensesReportArr']['MenuTitle'] = '預算及支出報告';
$Lang['ePCM']['SettingsArr']['MenuTitle'] = '設定';
$Lang['ePCM']['SettingsArr']['GeneralArr']['MenuTitle'] = '基本設定';
$Lang['ePCM']['SettingsArr']['RoleArr']['MenuTitle'] = '審批委員/職級';
$Lang['ePCM']['SettingsArr']['RuleArr']['MenuTitle'] = '價格範圍及報價要求';
$Lang['ePCM']['SettingsArr']['CategoryArr']['MenuTitle'] = '項目類別';
$Lang['ePCM']['SettingsArr']['GroupArr']['MenuTitle'] = '部門/科組';
$Lang['ePCM']['SettingsArr']['SupplierArr']['MenuTitle'] = '供應商';
$Lang['ePCM']['SettingsArr']['SupplierArr']['TabTitle'] = '供應商';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TabTitle'] = '供應商類型';
$Lang['ePCM']['SettingsArr']['AlertArr']['MenuTitle'] = '提示';
$Lang['ePCM']['SettingsArr']['SecurityArr']['MenuTitle'] = '保安設定';
$Lang['ePCM']['SettingsArr']['SampleDocumentArr']['MenuTitle'] = '文件範例';
$Lang['ePCM']['SettingsArr']['FundingSource']['MenuTitle'] = '資金來源';

// General
$Lang['ePCM']['General']['Select'] = '- 選擇 -';
$Lang['ePCM']['General']['Click here to download sample'] = '按此下載範例';
$Lang['ePCM']['General']['SaveAsDraft'] = '儲存草稿';
$Lang['ePCM']['General']['Notice'] = '提示';
$Lang['ePCM']['General']['Enable'] = '啟用';
$Lang['ePCM']['General']['Disable'] = '不啟用';
$Lang['ePCM']['General']['Principal'] = '校長';
$Lang['ePCM']['General']['VicePrincipal'] = '副校長';
$Lang['ePCM']['General']['NoRecord'] = '暫時沒有紀錄';
$Lang['ePCM']['General']['jsWaitAllFilesUploaded'] = "* 請等待直至所有文件完成上傳。";
$Lang['ePCM']['General']['ErrorCode'] = '錯誤編碼';
$Lang['ePCM']['General']['Description'] = '描述';
$Lang['ePCM']['General']['LineNumber'] = '行數';
$Lang['ePCM']['General']['DataColumnDefault'] = '正確資料欄目';
$Lang['ePCM']['General']['DataColumnUser'] = '用戶輸入';

$Lang['ePCM']['Import']['Error']['NO_FILE'] = '請選擇匯入檔案';
$Lang['ePCM']['Import']['Error']['INCORRECT_EXT'] = '副檔名不正確';
$Lang['ePCM']['Import']['Error']['INCORRECT_HEADING'] = '檔案欄目不對應';
$Lang['ePCM']['Import']['Error']['EMPTY_TYPE_NAME_CHI'] = '供應商類型 (中文)留空';
$Lang['ePCM']['Import']['Error']['EMPTY_TYPE_NAME_ENG'] = '供應商類型 (英文)留空';
$Lang['ePCM']['Import']['Error']['EMPTY_SUPPLIER_NAME_CHI'] = '供應商名稱 (中文)留空';
$Lang['ePCM']['Import']['Error']['EMPTY_SUPPLIER_NAME_ENG'] = '供應商名稱 (英文)留空';
$Lang['ePCM']['Import']['Error']['EMPTY_PHONE'] = '電話留空';
$Lang['ePCM']['Import']['Error']['EMPTY_TYPE'] = '供應商類型留空';
$Lang['ePCM']['Import']['Error']['INVALID_TYPE'] = '供應商類型不存在';

$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_1'] = '資金來源(1)不存在';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_2'] = '資金來源(2)不存在';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_SAME'] = '資金來源(1)與 資金來源(2)不能相同';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_1'] = '	單位價格 (1) 不能為0';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_2'] = '單位價格 (2) 不能為0';
$Lang['ePCM']['Import']['Error']['INVALID_FUNDING_PRICE_Total'] = '單位價格的總額不能大於實際數額';

// Symbol
$Lang['PCM']['Symbol']['Colon'] = '：';
$Lang['PCM']['Symbol']['DollarSign'] = '$ ';

// General Setting
$Lang['ePCM']['GeneralSetting']['CodeGeneration'] = '採購項目編號產生辦法';
$Lang['ePCM']['GeneralSetting']['CodeGenerationSequence'] = '採購項目編號序列號產生辦法';
$Lang['ePCM']['GeneralSetting']['CodeNumberOfDigit'] = '採購項目編號序列號數字數目';
$Lang['ePCM']['GeneralSetting']['emailNotification'] = '啟用電子郵件通知';
$Lang['ePCM']['GeneralSetting']['eClassAppNotification'] = '啟用 eClassApp 通知';
$Lang['ePCM']['GeneralSetting']['MiscExpenditure'] = '啟用雜項支出';
$Lang['ePCM']['GeneralSetting']['MiscExpenditureApproval'] = '需要批核';
$Lang['ePCM']['GeneralSetting']['CronJobEnableEmail'] = '啟用自動電郵通知';
$Lang['ePCM']['GeneralSetting']['CronJobEnablePushNoti'] = '啟用自動eClassApp通知';
$Lang['ePCM']['GeneralSetting']['caseApprovalMethod'] = '批核採購所需條件';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Case Code must include Sequence Number'] = '代碼必須包括序列號';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Case Code partials must not be duplicated'] = '代碼不得重複';
$Lang['ePCM']['GeneralSetting']['ControlArr']['N/A'] = '不適用';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Academic Year'] = '學年';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Category Code'] = '類別代碼';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Financial Item Code'] = '財政項目代碼';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Sequence Number'] = '序列號';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Group code'] = '部門/科組代碼';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Please choose Academic Year'] = '請選擇學年';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Please enter case code sequence digits'] = '請輸入採購項目編號序列號數字數目';
$Lang['ePCM']['GeneralSetting']['ControlArr']['General'] = '一般';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Notification'] = '提示';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Rule Code'] = '價格範圍及報價要求代碼';
$Lang['ePCM']['GeneralSetting']['ExceedBudgetPercentage'] = '允許超越部門/科組及財政項目財政預算n%的申請';
$Lang['ePCM']['GeneralSetting']['ControlArr']['Unlimited'] = '無上限';
$Lang['ePCM']['GeneralSetting']['ControlArr']['CannotExceed'] = '不能超越';
$Lang['ePCM']['caseApprovalMethod']['all'] = '全部批核者';
$Lang['ePCM']['caseApprovalMethod']['oneVicePrincipalAndAllOthers'] = '一位副校長 (如適用)及 全部其他批核者';
// content - Setting > Security
$Lang['ePCM']['SettingsArr']['SecurityArr']['AuthenticationArr']['TabTitle'] = '訪問設定';
$Lang['ePCM']['SettingsArr']['SecurityArr']['AuthenticationArr']['SettingTitle'] = '當首次進入需要進行登入認證';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['TabTitle'] = '網絡設定';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['InstructionTitle'] = '使用指引';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['InstructionContent'] = '在 IP 位址的格上, 可以使用:
		<ol>
			<li>固定 IP 位址 (e.g. 192.168.0.101)</li>
			<li>IP 位址一個範圍 (e.g. 192.168.0.[10-100])</li>
			<li>CISCO Style 範圍(e.g. 192.168.0.0/24)</li>
			<li>容許所有位址 (輸入 0.0.0.0)</li>
		</ol>
		<!--<font color="red">連接讀咭器的電腦, 建議該電腦設定固定 IP 地址 (即不採用 DHCP), 以確保資料不會被其他電腦更改.</font>
		<br>//-->
		本系統設有安全措施防範假冒的IP 地址 (Faked IP Address).';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['SettingTitle'] = 'IP 位址';
$Lang['ePCM']['SettingsArr']['SecurityArr']['NetworkArr']['SettingControlTitle'] = '請把 IP 地址每行一個輸入<br />你現時的 IP 地址';

// Supplier
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['All Types'] = '所有類型';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Active'] = '活躍';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Inactive'] = '非活躍';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Activate'] = '設定為活躍';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Inactivate'] = '設定為非活躍';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose a supplier'] = '請選擇一個供應商';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose not more than one supplier'] = '請選擇不超過一個供應商';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please choose at least one supplier'] = '請至少選擇一個供應商';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Delete?'] = '確認刪除？';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Add Contact Person'] = '增加聯絡人';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Remove Contact Person'] = '刪除聯絡人';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Code'] = '請輸入代碼';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Supplier Name'] = '請輸入供應商名稱';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input an URL'] = '請輸入網址';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Phone'] = '請輸入電話';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input an Email Address'] = '請輸入電子郵件地址';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Type'] = '請輸入類型';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Activate?'] = '確認設定為活躍？';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Confrim Inactivate?'] = '確認設定為非活躍？';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['All'] = '活躍 / 非活躍';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier'] = '新增供應商';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['New Supplier Type'] = '新增供應商類型';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Contact Name'] = '請輸入聯絡人姓名';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input Position'] = '請輸入職位';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input valid Phone'] = '請輸入有效的電話';
$Lang['ePCM']['SettingsArr']['SupplierArr']['ControlArr']['Please input valid Fax'] = '請輸入有效的傳真';


$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier'] = '供應商';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Code'] = '代碼';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name'] = '供應商 (英文)';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Supplier Name Chi'] = '供應商 (中文)';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Website'] = '網站';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Description'] = '描述';
$Lang['ePCM']['SettingsArr']['SupplierArr']['RegisteredLetter'] = '掛號信';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Phone'] = '電話';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Fax'] = '傳真';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Email'] = '電子郵件';
$Lang['ePCM']['SettingsArr']['SupplierArr']['InPerson'] = '面談';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Type'] = '類型';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Quote Invitation'] = '書面報價邀請';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Quote'] = '回覆書面報價';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Bid Invitation'] = '招標邀請';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Bid'] = '投標';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Deal'] = '成功交易';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Last Deal Date'] = '最後交易日期';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Contact Person'] = '聯絡人';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Contact Name'] = '聯絡人姓名';
$Lang['ePCM']['SettingsArr']['SupplierArr']['Position'] = '職位';

$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeName'] = '類型名稱 (英文)';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['TypeNameChi'] = '類型名稱 (中文)';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please input Name'] = '請輸入類型名稱';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose at least one supplier type'] = '請至少選擇一個供應商類型';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Confrim Delete?'] = '確認刪除？';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Duplicated Name'] = '重複的類型名稱';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose a supplier type'] = '請選擇一個供應商類型';
$Lang['ePCM']['SettingsArr']['SupplierTypeArr']['ControlArr']['Please choose not more than one supplier type'] = '請選擇不超過一個供應商類型';

$Lang['ePCM']['SettingsArr']['SupplierArr']['Import']['Reference']['Type'] = '按此查詢類型編號';

$Lang['ePCM']['SettingsArr']['SupplierArr']['Total'] = '總額';

// General Language
$Lang['ePCM']['SettingsArr']['New'] = '新增';
$Lang['ePCM']['SettingsArr']['Edit'] = '編輯';

// Group
$Lang['ePCM']['Group']['Group'] = '部門/科組';
$Lang['ePCM']['Group']['GroupItem'] = '部門項目';
$Lang['ePCM']['Group']['UserLogin'] = '(若輸入多一個內聯網帳號，請在帳號之間加上「,」)';
$Lang['ePCM']['Group']['MemberInput'] = '(若輸入多一位成員，請在成員之間加上「,」)';
$Lang['ePCM']['Group']['GroupHeadIncorrect'] = '用戶不存在';
$Lang['ePCM']['Group']['NotATeacher'] = "此身份並不是老師身份，請再檢查";
$Lang['ePCM']['Group']['GroupName'] = '部門/科組名稱 (英文)';
$Lang['ePCM']['Group']['GroupNameChi'] = '部門/科組名稱 (中文)';
$Lang['ePCM']['Group']['GroupCode'] = '部門/科組';
$Lang['ePCM']['Group']['Code'] = '代碼';
$Lang['ePCM']['Group']['Item']['FinancialCode'] = '財政項目代碼';
$Lang['ePCM']['Group']['Item']['FinancialItemChi'] = '財政項目 (中文)';
$Lang['ePCM']['Group']['Item']['FinancialItemEng'] = '財政項目 (英文)';
$Lang['ePCM']['Group']['GroupNotExist'] = '部門/科組不存在';
$Lang['ePCM']['Group']['ItemNotExist'] = '財政項目不存在';
$Lang['ePCM']['Group']['Item']['DuplicatedCode'] = '財政項目代碼不能重複';
$Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroup'] = '部門/科組';
$Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupBudget'] = '部門/科組項目預算';
$Lang['ePCM']['Group']['Item']['Budget']['departmentOrGroupItem'] = '部門/科組財政項目';
$Lang['ePCM']['Group']['Item']['Budget']['financialItem'] = '財政項目';
$Lang['ePCM']['Group']['Item']['Budget']['fundingSource'] = '對應資金來源';
$Lang['ePCM']['Group']['Item']['Budget']['defaultFunding'] = '預設對應資金來源';
$Lang["ePCM"]["Group"]["GroupCodeEnquiry"] = '[按此查詢部門/科組代號]';
$Lang['ePCM']['Group']['FundingSourceEnquiry'] = '[按此查詢資金來源代號]';
$Lang['ePCM']['Group']['RelatedGroupFinancialItemEnquiry'] = '[按此查詢對應部門/科組的財政項目代號]';
$Lang['ePCM']['Group']['ItemCode'] = '財政項目代碼';
$Lang['ePCM']['Group']['Budget'] = '預算';
$Lang['ePCM']['Group']['GroupBudget'] = '部門/科組總預算';
$Lang['ePCM']['Group']['BudgetRemain'] = '剩餘預算';
$Lang['ePCM']['Group']['Member'] = '成員';
$Lang['ePCM']['Group']['SetGroupHead'] = '設定為部門/科組上級';
$Lang['ePCM']['Group']['RemoveGroupHead'] = '移除部門/科組上級';
$Lang['ePCM']['Group']['GroupHead'] = '部門/科組上級';
$Lang['ePCM']['Group']['GroupItemName'] = '財政項目 (英文)';
$Lang['ePCM']['Group']['GroupItemNameChi'] = '財政項目 (中文)';
$Lang['ePCM']['Setting']['Group']['AddMember'] = '新增成員';
$Lang['ePCM']['Setting']['Group']['Edit Member'] = '編輯成員';
$Lang['ePCM']['Group']['GroupHeadRemarks'] = '「<span class="tabletextrequire">*</span>」為部門/科組上級';
$Lang['ePCM']['Group']['Please input Code'] = '請輸入代碼';
$Lang['ePCM']['Group']['Please input Group Name'] = '請輸入 部門/科組名稱';
$Lang['ePCM']['Group']['Please input Item Name'] = '請輸入財政項目';
$Lang['ePCM']['Group']['Duplicated code'] = '重複的代碼';
$Lang['ePCM']['Group']['Warning']['ChooseAGroup'] = '請選擇部門/科組';
$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroup'] = '請選擇至少一個部門/科組';
$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroup'] = '請選擇不多於一個部門/科組';
$Lang['ePCM']['Group']['Warning']['ChooseAGroupMember'] = '請選擇部門/科組成員';
$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupMember'] = '請選擇至少一個部門/科組成員';
$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroupMember'] = '請選擇不多於一個部門/科組成員';
$Lang['ePCM']['Group']['Warning']['ChooseAGroupItem'] = '請選擇個財政項目';
$Lang['ePCM']['Group']['Warning']['ChooseOneOrMoreGroupItem'] = '請選擇至少一個個財政項目';
$Lang['ePCM']['Group']['Warning']['ChooseNoMoreThanAGroupItem'] = '請選擇不多於一個財政項目';
$Lang['ePCM']['Group']['Warning']['ConfirmSetToAdmin'] = '你確認設定用戶為部門/科組上級';
$Lang['ePCM']['Group']['Warning']['ConfirmRemoveFromAdmin'] = '你確認從部門/科組上級中移除用戶';
$Lang['ePCM']['Group']['Import']['ImportFromInv'] = '從物品管理小組(資產管理行政系統)匯入';
$Lang['ePCM']['Group']['Import']['ChooseGroup'] = '選擇小組';
$Lang['ePCM']['Group']['Import']['AdditionalImport'] = '附加匯入項目';
$Lang['ePCM']['Group']['Import']['ImportGroupMember'] = '匯入成員';
$Lang['ePCM']['Group']['Import']['ImportGroupHead'] = '匯入組長為部門/科組上級';
$Lang['ePCM']['Group']['Import']['Warning']['ChooseOneOrMoreGroup'] = '請選擇至少一個小組';
$Lang['ePCM']['Group']['Import']['Step1'] = '選擇匯入範圍';
$Lang['ePCM']['Group']['Import']['Step2'] = '資料驗證';
$Lang['ePCM']['Group']['Import']['Step3'] = '匯入結果';
$Lang['ePCM']['Group']['Import']['GroupNameChi'] = '小組名稱 (中文)';
$Lang['ePCM']['Group']['Import']['GroupNameEng'] = '小組名稱 (英文)';
$Lang['ePCM']['Group']['Import']['Code'] = '代碼';
$Lang['ePCM']['Group']['Import']['GroupMember'] = '成員';
$Lang['ePCM']['Group']['Import']['GroupHead'] = '(#為小組組長)';
$Lang['ePCM']['Group']['Import']['ImportStatus'] = '匯入狀況';
$Lang['ePCM']['Group']['Import']['Error']['CodeExist'] = '代碼已被使用';
// Group Budget
$Lang['ePCM']['Group']['BudgetArr']['ControlArr']['Edit Budget'] = '編輯財政預算';

$Lang['ePCM']['Group']['BudgetArr']['Academic Year'] = '學年';
$Lang['ePCM']['Group']['BudgetArr']['Budget'] = '預算';
$Lang['ePCM']['Group']['BudgetArr']['Item Budget'] = '財政項目預算';
$Lang['ePCM']['Group']['BudgetArr']['Total'] = '總額';
$Lang['ePCM']['Group']['BudgetArr']['Please input Academic Year'] = '請輸入學年';
$Lang['ePCM']['Group']['BudgetArr']['Budget must be positive'] = '預算必須為正數';

// Category
$Lang['ePCM']['Category']['Category'] = '類別';
$Lang['ePCM']['Category']['Code'] = '代碼';
$Lang['ePCM']['Category']['CategoryName'] = '類別名稱 (英文)';
$Lang['ePCM']['Category']['CategoryNameChi'] = '類別名稱 (中文)';
$Lang['ePCM']['Category']['Please input Code'] = '請輸入代碼';
$Lang['ePCM']['Category']['Please input Category Name'] = '請輸入類別名稱';
$Lang['ePCM']['Category']['Duplicated code'] = '重複的代碼';

$Lang['ePCM']['Category']['Warning']['ChooseACategory'] = '請選擇類別';
$Lang['ePCM']['Category']['Warning']['ChooseOneOrMoreCategory'] = '請選擇至少一個類別';
$Lang['ePCM']['Category']['Warning']['ChooseNoMoreThanACategory'] = '請選擇不多於一個類別';

// Rule
$Lang['ePCM']['Setting']['Rule']['FloorPrice'] = '開始價格';
$Lang['ePCM']['Setting']['Rule']['CeillingPrice'] = '上限價格';
$Lang['ePCM']['Setting']['Rule']['CaseApproval'] = '批核採購申請';
$Lang['ePCM']['Setting']['Rule']['ApprovalMode'] = '批核模式';
$Lang['ePCM']['Setting']['Rule']['ApprovalTogether'] = '同時批核';
$Lang['ePCM']['Setting']['Rule']['ApprovalSeperately'] = '按選擇次序批核';
$Lang['ePCM']['Setting']['Rule']['ApprovalSeperatelyRemarks'] = '&nbsp;<span class="tabletextremark">(科組/部門上級 -> 副校長 -> 校長)</span>';
$Lang['ePCM']['Setting']['Rule']['QuotationType'] = '報價/招標類型';
$Lang['ePCM']['Setting']['Rule']['QuotationApproval'] = '輸入結果';
$Lang['ePCM']['Setting']['Rule']['TenderAuditing'] = '標書審核';
$Lang['ePCM']['Setting']['Rule']['TenderApproval'] = '標書批核';
$Lang['ePCM']['Setting']['Rule']['MinInvitation'] = '邀請供應商下限';
$Lang['ePCM']['Setting']['Rule']['NumTemplateFile'] = '價格範圍及報價要求相關文件範本的總數';

$Lang['ePCM']['Setting']['Rule']['NoNeed'] = '無需';
$Lang['ePCM']['Setting']['Rule']['Verbal'] = '口頭報價';
$Lang['ePCM']['Setting']['Rule']['Quotation'] = '書面報價';
$Lang['ePCM']['Setting']['Rule']['Tender'] = '招標';
$Lang['ePCM']['Setting']['Rule']['GroupHead'] = '部門/科組上級';
$Lang['ePCM']['Setting']['Rule']['GroupHeadAndPricipal'] = '部門/科組上級及校長';
$Lang['ePCM']['Setting']['Rule']['GroupHeadAndVicePrincipal'] = '部門/科組上級及副校長';
$Lang['ePCM']['Setting']['Rule']['GroupHeadAndVicePrincipalAndPrincipal'] = '部門/科組上級及副校長及校長';
$Lang['ePCM']['Setting']['Rule']['VicePrincipalAndPrincipal'] = '副校長及校長';
$Lang['ePCM']['Setting']['Rule']['Remarks']['PriceRange'] = '例如: 由 5001 至 20000';

$Lang['ePCM']['Setting']['Rule']['PriceRange'] = '價格範圍';
$Lang['ePCM']['Setting']['Rule']['From'] = '由';
$Lang['ePCM']['Setting']['Rule']['To'] = '至';
$Lang['ePCM']['Setting']['Rule']['And'] = '及';
$Lang['ePCM']['Setting']['Rule']['Please input code'] = '請輸入代號';
$Lang['ePCM']['Setting']['Rule']['Please input price range (from)'] = '請輸入價格範圍(由)';
$Lang['ePCM']['Setting']['Rule']['Please input price range (to)'] = '請輸入價格範圍(至)';
$Lang['ePCM']['Setting']['Rule']['Please choose case approval'] = '請選擇批核申請';
$Lang['ePCM']['Setting']['Rule']['Please select quotation type'] = '請選擇 報價/招標類型';
$Lang['ePCM']['Setting']['Rule']['BlankAsUnlimited'] = '留空為無上限';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['TenderAuditing'] = '請選擇標書審核';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['TenderApproval'] = '請選擇標書批核';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['QuotationApproval'] = '請選擇報價審批';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['MinQuote'] = '請選擇邀請供應商下限';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['Approved'] = '物品已批核';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['ApproveRight'] = '只有組長可以批核';
$Lang['ePCM']['Setting']['Rule']['Warning']['Select']['RejectRight'] = '只有組長可以拒絕';

$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseARule'] = '請選擇規則';
$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseOneOrMoreRule'] = '請選擇至少一個規則';
$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseNoMoreThanARule'] = '請選擇不多於一個規則';
$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][0] = '最小的開始價格必需是 $0';
$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][1] = '最大的上限價格必需留空 (無上限)';
$Lang['ePCM']['Setting']['Rule']['Warning']['Instruction'][2] = '每單一價格只可屬於一條規則';
$Lang['ePCM']['Setting']['Rule']['Warning']['SoemthingWrong'] = '以下設定不正確:';
$Lang['ePCM']['Setting']['Rule']['Warning']['PriceBetween'] = '價格在';
$Lang['ePCM']['Setting']['Rule']['Warning']['PriceMissing'] = '未有包括在內';
$Lang['ePCM']['Setting']['Rule']['Warning']['PriceOverlapping'] = '存在多於一條規則';
$Lang['ePCM']['Setting']['Rule']['PriceRangeNegative'] = '價格範圍必須為正整數';
$Lang['ePCM']['Setting']['Rule']['UnlimitedCeillingInNonLastRule'] = '上限價格於非最大價格的規則留空';
$Lang['ePCM']['Setting']['Rule']['floorPriceLarger'] = '下限價格不能大於上限價格';
$Lang['ePCM']['Setting']['Rule']['All'] = '所有';
$Lang['ePCM']['Setting']['Rule']['AtLeast'] = '最少<!--n-->位';
$Lang['ePCM']['Setting']['Rule']['PleaseSelectRole'] = '請至少選擇一個審批委員';
$Lang['ePCM']['Setting']['Rule']['AllPeople'] = '所有人';
$Lang['ePCM']['Setting']['Rule']['Required'] = '需要';
$Lang['ePCM']['Setting']['Rule']['Approve'] = '批核';
$Lang['ePCM']['Setting']['Rule']['Reset'] = '重新選擇';
$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseAItem'] = '請選擇物品';
$Lang['ePCM']['Setting']['Rule']['Warning']['ChooseOneOrMoreItem'] = '請選擇不多於一個物品';

$Lang['ePCM']['Setting']['Rule']['uploadSampleFile'] = '上載價格範圍及報價要求相關文件範本';
$Lang['ePCM']['Setting']['Rule']['Remarks']['rollbackTemplateFile'] = '<span class="tabletextrequire">*</span><span class="tabletextremark">如需要取消對價格範圍及報價要求的相關文件範本所作出的更改，請按「取消」';

// Role
$Lang['ePCM']['Setting']['Role']['Role'] = '審批角色';
$Lang['ePCM']['Setting']['Role']['RoleName'] = '審批角色名稱 (英文)';
$Lang['ePCM']['Setting']['Role']['RoleNameChi'] = '審批角色名稱 (中文)';
$Lang['ePCM']['Setting']['Role']['Member'] = '成員';
$Lang['ePCM']['Setting']['Role']['IsPrincipal'] = '設定為校長';
$Lang['ePCM']['Setting']['Role']['Responsibility'] = '審批範疇';
$Lang['ePCM']['Setting']['Role']['Please input Role Name'] = '請輸入審批角色名稱';
$Lang['ePCM']['Setting']['Role']['CopyFromOtherAcademicYear']='從其他年度複製';

$Lang['ePCM']['Setting']['Role']['Warning']['ChooseARole'] = '請選擇審批委員/職級';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseOneOrMoreRole'] = '請選擇至少一個審批委員/職級';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseNoMoreThanARole'] = '請選擇不多於一個審批委員/職級';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseARoleMember'] = '請選擇審批委員/職級成員';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseOneOrMoreRoleMember'] = '請選擇至少一個審批委員/職級成員';
$Lang['ePCM']['Setting']['Role']['Warning']['ChooseNoMoreThanARoleMember'] = '請選擇不多於一個審批委員/職級成員';

$Lang['ePCM']['Setting']['Role']['DeletedUserLegend'] = '<font style="color:red;">*</font> 表示該用戶已被刪除或已離校。';

$Lang['ePCM']['Setting']['Yes'] = '是';
$Lang['ePCM']['Setting']['No'] = '不是';
$Lang['ePCM']['Setting']['Template'][1] = '申請表';
$Lang['ePCM']['Setting']['Template'][2] = '報價/招標邀請書';
$Lang['ePCM']['Setting']['Template'][3] = '利益申報備忘';
$Lang['ePCM']['Setting']['Template'][4] = '報價/投標批摘要及批核紀錄表';

// Case
$Lang['ePCM']['Mgmt']['Case']['CaseName'] = '名稱';
$Lang['ePCM']['Mgmt']['Case']['CaseName2'] = '採購物品/服務';
$Lang['ePCM']['Mgmt']['Case']['CaseCode'] = '採購編號';
$Lang['ePCM']['Mgmt']['Case']['CaseApplication'] = '採購申請';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['ApprovalStatus'] = '批核狀況';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approve'] = '批准';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Reject'] = '拒絕';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Rejected'] = '已拒絕';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Pending'] = '未批准';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved'] = '已批准';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Feedback'] = '回應';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsed'] = '已確認';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['AdminAction'] = '由 <!--adminName--> 代為批核';
$Lang['ePCM']['Mgmt']['Case']['CaseApplication'] = '申請採購';
$Lang['ePCM']['Mgmt']['Case']['CaseApproval'] = '批核採購';
$Lang['ePCM']['Mgmt']['Case']['Endorsement'] = '採購確認';
$Lang['ePCM']['Mgmt']['Case']['Tender'] = '招標';
$Lang['ePCM']['Mgmt']['Case']['TenderOpening'] = '開標審核';
$Lang['ePCM']['Mgmt']['Case']['TenderOpeningStartDate'] = '開始時間';
$Lang['ePCM']['Mgmt']['Case']['TenderOpeningEndDate'] = '審核限期';
$Lang['ePCM']['Mgmt']['Case']['TenderApprovalStartDate'] = '開始時間';
$Lang['ePCM']['Mgmt']['Case']['TenderApprovalEndDate'] = '審批限期';
$Lang['ePCM']['Mgmt']['Case']['TenderApproval'] = '標書批核';
$Lang['ePCM']['Mgmt']['Case']['Quotation'] = '書面報價';
$Lang['ePCM']['Mgmt']['Case']['QuotationApproval'] = '輸入結果';
$Lang['ePCM']['Mgmt']['Case']['FinishOn'] = '已於<!--datetime-->完成。';
$Lang['ePCM']['Mgmt']['Case']['Finished'] = '已完成';
$Lang['ePCM']['Mgmt']['Case']['NotFinished'] = '未完成';
$Lang['ePCM']['Mgmt']['Case']['LastUpdate'] = '最近更新';
$Lang['ePCM']['Mgmt']['Case']['Purpose'] = '目的';
$Lang['ePCM']['Mgmt']['Case']['Budget'] = '預算';
$Lang['ePCM']['Mgmt']['Case']['Applicant'] = '申請人';
$Lang['ePCM']['Mgmt']['Case']['Status']['Status'] = '批核狀況';
$Lang['ePCM']['Mgmt']['Case']['Status']['AllStatus'] = '所有批核狀況';
$Lang['ePCM']['Mgmt']['Case']['Status']['Approved'] = '已批核';
$Lang['ePCM']['Mgmt']['Case']['Status']['Pending'] = '待批';
$Lang['ePCM']['Mgmt']['Case']['Status']['Reject'] = '已拒絕';
$Lang['ePCM']['Mgmt']['Case']['Status']['AllStatusForConfirmation'] = '所有確認狀況';
$Lang['ePCM']['Mgmt']['Case']['Status']['ConfirmStatus'] = '確認狀況';
$Lang['ePCM']['Mgmt']['Case']['Status']['Confirmed'] = '已確認';
$Lang['ePCM']['Mgmt']['Case']['Status']['PendingForConfirmation'] = '待確認';
$Lang['ePCM']['Mgmt']['Case']['ChooseIndividual'] = '選擇申請人(只限系統管理員)';
$Lang['ePCM']['Mgmt']['Case']['ApplicationDate'] = '申請日期';
$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['Status'] = '回覆狀態';
$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['NoReply'] = '未有回應';
$Lang['ePCM']['Mgmt']['Case']['QuotationUpload']['Submitted'] = '已提交';
$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Rejected'] = '放棄投標';
$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Pending'] = '未有回應';
$Lang['ePCM']['Mgmt']['Case']['QuotationStatus']['Accepted'] = '確認收到';
$Lang['ePCM']['Mgmt']['Case']['Category'] = '類別';
$Lang['ePCM']['Mgmt']['Case']['Group'] = '部門/科組';
$Lang['ePCM']['Mgmt']['Case']['FinancialItem'] = '納入財政項目';
$Lang['ePCM']['Mgmt']['Case']['Approver'] = '批核者';
$Lang['ePCM']['Mgmt']['Case']['Endorser'] = '確認者';
$Lang['ePCM']['Mgmt']['Case']['Remarks'] = '備註';
$Lang['ePCM']['Mgmt']['Case']['Document'] = '文件';
$Lang['ePCM']['Mgmt']['Case']['InvitationLetter'] = '邀請書';
$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Individual'] = '個人名義';
$Lang['ePCM']['Mgmt']['Case']['ApplicantType']['Group'] = '部門/科組名義';
$Lang['ePCM']['Mgmt']['Case']['InputResult'] = '輸入結果';
$Lang['ePCM']['Mgmt']['Case']['InvitationOfSuppliersStr'] = '已邀請以下 {-numOfQuotation-} 間供應商';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['ContactPerson'] = '聯絡人';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['ResponseStatus'] = '回應';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['ResponseDate'] = '回應日期';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['AddInvitation'] = '選擇供應商';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['Reason'] = '未達到供應商下限原因';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['NewSupplierType'] = '新增類型';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['SchoolType'] = '校本類型';
$Lang['ePCM']['Mgmt']['Case']['Supplier']['GroupType'] = '自訂類型';
$Lang['ePCM']['Mgmt']['Case']['Warning']['NotEnoughSupplier'] = '本項目需要至少{-MinQuote-}個供應商邀請，如未能達到有關要求，必須填寫原因才可進入下一階段';
$Lang['ePCM']['Mgmt']['Case']['Warning']['NeedStartNewCase'] = '由於本個案已傳送批核邀請，個案將會重新建立，並再次傳送批核邀請。你確定要這樣做嗎?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsDeleted'] = '本個案已被中止，內容只供參考';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Termination'] = '你是否確定中止有關申請?';
$Lang['ePCM']['Mgmt']['Case']['UndoTermination'] = '取消放棄採購';
$Lang['ePCM']['Mgmt']['Case']['Warning']['SelectApprovalStatus'] = '請選擇批核狀況';
$Lang['ePCM']['Mgmt']['Case']['Warning']['SelectSupplier'] = '請選擇至少一個供應商';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputRemark'] = '請填寫備註';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputReason'] = '請填寫原因';
$Lang['ePCM']['Mgmt']['Case']['Warning']['PositiveDealPrice'] = '請填寫正確成交金額';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputDeleteReason'] = '請填寫中止申請厡因';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputQuoteDate'] = '請填寫報價日期';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Confrim2NextStage'] = '你確定往下一階段?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['InputEndDateFirst'] = '請先輸入截止日期';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DeleteQuotation'] = '你是否確定刪除本項的報價/招標邀請?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DeleteQuotation'] = '你是否確定刪除本項的報價/招標邀請?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderOpeningStartDate'] = '請選擇日子不早於招標截止日期';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderOpeningEndDate'] = '請選擇日子不早於開標審核開始時間';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderApprovalStartDate'] = '請選擇日子不早於開標審核限期';
$Lang['ePCM']['Mgmt']['Case']['Warning']['DateInvalid']['TenderApprovalEndDate'] = '請選擇日子不早於標書批核開始時間';
$Lang['ePCM']['Mgmt']['Case']['Warning']['CaseIsRejected'] = '本個案已被拒絕，如要重新申請，請按本頁面右上角編輯按鍵重新提交申請';
$Lang['ePCM']['Mgmt']['Case']['Warning']['SubmitDraft'] = '你確定提交本草稿?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['ConfirmReject'] = '你確定拒絕?';
$Lang['ePCM']['Mgmt']['Case']['Warning']['TenderCustWarning'] = '請選擇是否進行招標';
$Lang['ePCM']['Mgmt']['Case']['ItemName'] = '物品名稱';
$Lang['ePCM']['Mgmt']['Case']['Confirm'] = '確認';
$Lang['ePCM']['Mgmt']['Case']['UndoConfirm'] = '取消確認';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Confirmed'] = '物品已確認';
$Lang['ePCM']['Mgmt']['Case']['Warning']['ApplicationIsConfirmed'] = '申請已確認';
$Lang['ePCM']['Mgmt']['Case']['Warning']['ApplicationIsNotConfirmed'] = '申請未確認';

$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['CaseName'] = '請填寫名稱';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Category'] = '請選擇類別';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Reason'] = '請填寫目的';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Group'] = '請選擇部門/科組';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Item'] = '請選擇財政項目';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['Budget'] = '請填寫預算';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['NotInteger'] = '請輸入整數';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['ApplicantType'] = '請選擇申請人';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DealPrice'] = '請填寫成交金額';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DecimalPoint'] = '請輸入至小數後兩位';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['DuplicatedFunding'] = '請不要重覆資金來源';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['OverFundingSourceBudget'] = '超出金額上限';
$Lang['ePCM']['Mgmt']['Case']['Warning']['JSChecking']['FundingOverBudget'] = '動用資金金額總和大於預算';

$Lang['ePCM']['Mgmt']['Case']['Deadlines'] = '截止日期';
$Lang['ePCM']['Mgmt']['Case']['TenderClosing'] = '截標';
$Lang['ePCM']['Mgmt']['Case']['OnlyNDaysLeft'][0] = '還剩';
$Lang['ePCM']['Mgmt']['Case']['OnlyNDaysLeft'][1] = '天';
$Lang['ePCM']['Mgmt']['Case']['AutoNotify'] = '自動提醒';
$Lang['ePCM']['Mgmt']['Case']['DaysBefore'] = '天前';
$Lang['ePCM']['Mgmt']['Case']['NotifyTarget'] = '提醒對象';
$Lang['ePCM']['Mgmt']['Case']['ToNextStage'] = '往下一階段';
$Lang['ePCM']['Mgmt']['Case']['UploadQuoation'] = '上載報價文件';
$Lang['ePCM']['Mgmt']['Case']['UploadTenders'] = '上載投標書';
$Lang['ePCM']['Mgmt']['Case']['UploadDocument'] = '上載文件';
$Lang['ePCM']['Mgmt']['Case']['Declaration'] = '利益申報備忘';
$Lang['ePCM']['Mgmt']['Case']['QuotationDate'] = '報價日期';
$Lang['ePCM']['Mgmt']['Case']['TenderDate'] = '投標日期';
$Lang['ePCM']['Mgmt']['Case']['LastModified'] = '最後更新 (時間及人物)';
$Lang['ePCM']['Mgmt']['Case']['LateSubmition'] = '過期提交';
$Lang['ePCM']['Mgmt']['Case']['Result']['DealPrice'] = '成交金額';
$Lang['ePCM']['Mgmt']['Case']['Result']['DealDate'] = '成交日期';
$Lang['ePCM']['Mgmt']['Case']['Result']['Details'] = '詳細資料';
$Lang['ePCM']['Mgmt']['Case']['Result']['Description'] = '批核備註';
$Lang['ePCM']['Mgmt']['Case']['Result']['FundingSource'] = '資金來源';
$Lang['ePCM']['Mgmt']['Case']['Result']['Reason'] = '不採納最低報價原因';
$Lang['ePCM']['Mgmt']['Case']['Result']['ReasonWarning'] = '請輸入不採納最低報價原因';
$Lang['ePCM']['Mgmt']['Case']['Result']['RecordDay'] = '記錄日期';
$Lang['ePCM']['Mgmt']['Case']['Result']['Amount'] = '金額';
$Lang['ePCM']['Mgmt']['Case']['ApplicationReceived'] = '已收到閣下的申請';
$Lang['ePCM']['Mgmt']['Case']['EmailNoti'] = '發出電郵通知';
$Lang['ePCM']['Mgmt']['Case']['PushNoti'] = '發出推播通知 (eClassApp)';
$Lang['ePCM']['Mgmt']['Case']['ApplicationWillBeApprovedBy'] = '閣下的申請將會由以下負責人批核';
$Lang['ePCM']['Mgmt']['Case']['PreviousRecord'] = '過往紀錄';
$Lang['ePCM']['Mgmt']['Case']['Termination'] = '放棄採購';
$Lang['ePCM']['Mgmt']['Case']['DeleteDraft'] = '刪除草稿';
$Lang['ePCM']['Mgmt']['Case']['DeleteReason'] = '放棄申請個案原因';
$Lang['ePCM']['Mgmt']['Case']['DeleteReason2'] = '個案中止原因';
$Lang['ePCM']['Mgmt']['Case']['Email']['Title'] = '[採購系統] 有一申請等侯批核 - <!--CASE_CODE-->';
$Lang['ePCM']['Mgmt']['Case']['Email']['Content'] = '你有一申請等侯批核。 (<!--CASE_CODE-->)';
$Lang['ePCM']['Mgmt']['Case']['PushMessage']['Title'] = '[採購系統] 有一申請等侯批核 - <!--CASE_CODE-->
[eProcurement] A new case for approval - <!--CASE_CODE-->';
$Lang['ePCM']['Mgmt']['Case']['PushMessage']['Content'] = '你有一申請等侯批核。 (<!--CASE_CODE-->) <!--CASE_NAME-->。 請登入內聯網檢視詳情。
You have a new case for approval. (<!--CASE_CODE-->) <!--CASE_NAME-->. Please log in to the intranet for further details.';
$Lang['ePCM']['Mgmt']['Case']['Sent successfully'] = '已經發出';
$Lang['ePCM']['Mgmt']['Case']['Failed to send'] = '不能發出';
$Lang['ePCM']['Mgmt']['Case']['Failed to send mail'] = '不能發出郵件到以下用戶的內聯網或備用電郵:';
$Lang['ePCM']['Mgmt']['Case']['Failed to send pushMessage'] = '未能成功發送推送訊息到以下用戶:';
$Lang['ePCM']['Mgmt']['Case']['Sending'] = '發送中';
$Lang['ePCM']['Mgmt']['Case']['IsDeleted'] = '已中止';
$Lang['ePCM']['Mgmt']['Case']['EmailToSupplier'] = '發出電郵邀請';
$Lang['ePCM']['Mgmt']['Case']['BudgetLeft'] = '還剩: ${-Budget-}';
$Lang['ePCM']['Mgmt']['Case']['Warning']['StopNewApplication'] = '提交新申請已被鎖定，原因如下:';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Reason']['RulesInvalid'] = '價格範圍及報價要求設定錯誤，請聯絡採購系統的管理員';
$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Group'] = '預算超越部門/科組預算';
$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['Item'] = '預算超越財政項目預算';
$Lang['ePCM']['Mgmt']['Case']['Warning']['BudgetExceed']['GroupItem'] = '預算超越部門/科組及財政項目預算';
$Lang['ePCM']['Mgmt']['Case']['Warning']['Duplicated']['Code'] = '採購編號己被使用';
$Lang['ePCM']['Mgmt']['Case']['SubmitDraft'] = '提交草稿';
$Lang['ePCM']['Mgmt']['Case']['No Emails are sent'] = '沒有送出電郵';
$Lang['ePCM']['Mgmt']['Case']['Emails are sent'] = '電郵已經送出';
$Lang['ePCM']['Mgmt']['Case']['Sending'] = '傳送中';
$Lang['ePCM']['Mgmt']['Case']['Re-send Invitation Email'] = '重發電郵邀請';
$Lang['ePCM']['Mgmt']['Case']['Latest Email Sent Date'] = '最後送出電郵日期';
$Lang['ePCM']['Mgmt']['Case']['InputPriceComparison'] = '編輯價格比較表';
$Lang['ePCM']['Mgmt']['Case']['PriceComparisonTable'] = '價格比較表';
$Lang['ePCM']['Mgmt']['Case']['AddNewPriceItem'] = '增加價格項目';

$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['FormTitle'] = '採購申請表';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['PrintBtn'] = '列印採購申請表';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['PrincipalApproval'] = '校長批核';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['DateOfApproval'] = '批核日期';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['CaseName'] = '名稱';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['CategoryName'] = '類別名稱 ';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Description'] = '目的';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['GroupName'] = '部門/科組';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['ItemName'] = '納入財政項目';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Budget'] = '預算';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Remarks'] = '備註';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['FundingSourceArr'] = '資金來源';
$Lang['ePCM']['Mgmt']['Case']['ApplicationForm']['Location'] = '地點';

$Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnEn'] = '匯出按口頭報價購貨表格(附件II)(英文)';
$Lang['ePCM']['Mgmt']['Case']['Purchase-by-OralQuotationForm']['ExportBtnB5'] = '匯出按口頭報價購貨表格(附件II)(中文)';

$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage']['AllStage']="所有階段";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][0]="我的草稿";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][1]="申請採購";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][2]="口頭報價/書面報價/招標";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][3]="上載報價文件/開標審核";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][4]="輸入結果/標書批核";

$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['AllType']="所有採購類型";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['N']="無需";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['V']="口頭報價";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['Q']="書面報價";
$Lang['ePCM']['Mgmt']['Case']['Filter']['QuotationType']['T']="招標";

$Lang['ePCM']['Mgmt']['Case']['Filter']['Category']['AllType']="所有類別";
$Lang['ePCM']['Mgmt']['Case']['Filter']['FinancialItem']['AllType'] = "所有納入財政項目";
$Lang['ePCM']['Mgmt']['Case']['Filter']['Supplier']['AllType'] = "所有供應商";
$Lang['ePCM']['Mgmt']['Case']['Filter']['Applicant']['AllType'] = "所有申請人";

$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox2']['Q'] = '請先擬定邀請書及書面報價/招標文件採用的條款和條件，必須清楚註明截止報價/截標的日期及時間，然後上載到系統。所有書面報價/投標文件應以掛號的方式寄交獲邀報價/投標的供應商。郵政署發出的記錄派遞收條上載有供應商的姓名及寄發日期，學校應把這些收條存檔，以供查核。如有需要，可使用以下的電郵功能發出邀請及記錄供應商之回應。';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox2']['T'] = '請先擬定邀請書及書面報價/招標文件採用的條款和條件，必須清楚註明截止報價/截標的日期及時間，然後上載到系統。所有書面報價/投標文件應以掛號的方式寄交獲邀報價/投標的供應商。郵政署發出的記錄派遞收條上載有供應商的姓名及寄發日期，學校應把這些收條存檔，以供查核。如有需要，可使用以下的電郵功能發出邀請及記錄供應商之回應。';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox2']['V'] = '請紀錄口頭報價的供應商，如有需要，可輸入價格比較表或可使用以下的電郵功能發出邀請。';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox3']['Q'] = '開啟書面報價單的人員在檢查書面報價文件，要求負責有關科目的教師/行政人員對書面報價單作出推薦，以及填寫「書面報價摘要及批核紀錄表」的有關部分後，須把該紀錄表連同書面報價一併交給批核人員考慮和批准。';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox3']['T'] = '開啟標書的人員在檢查書面報價/投標文件，要求負責有關科目的教師/行政人員對標書作出推薦，以及填寫「投標摘要及批核紀錄表」的有關部分後，須把該紀錄表連同投標文件一併交給批核人員考慮和批准。';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox4']['Q'] = '除了在此輸入結果，可選擇下載及列印「書面報價或投標摘要及批核紀錄表」，讓相關人員簽署後，上載回系統作紀錄。';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox4']['T'] = '除了在此輸入結果，可選擇下載及列印「書面報價或投標摘要及批核紀錄表」，讓相關人員簽署後，上載回系統作紀錄。';
$Lang['ePCM']['Mgmt']['Case']['StepBoxRemarks']['StepBox4']['V'] = '請在此輸入結果，如有需要，可上載有關文件以作紀錄。';

$Lang['ePCM']['Mgmt']['Case']['UpdateFailed'] = '更新失敗';
$Lang['ePCM']['Mgmt']['Case']['RelatedUsers'] = '相關用戶';
$Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['ZeroPercent'] = '(預算不能超越<strong>部門/科組</strong>及<strong>財政項目</strong>)';
$Lang['ePCM']['Mgmt']['Case']['ExceedBudget']['Notice']['NPercent'] = '(預算不能超越<strong>部門/科組</strong>及<strong>財政項目</strong>剩餘餘額的<!--percent-->%)';
$Lang['ePCM']['Mgmt']['Case']['FundingSource'] = '資金來源';
$Lang['ePCM']['Mgmt']['Case']['MustTender'] = '必須招標';

$Lang['ePCM']['Mgmt']['FundingName']['NotApplicable']= '不適用';
$Lang['ePCM']['Mgmt']['FundingName']['School']='學校';
$Lang['ePCM']['Mgmt']['FundingName']['Government']='政府';
$Lang['ePCM']['Mgmt']['FundingName']['SponsoringBody']='辦學團體';
$Lang['ePCM']['Mgmt']['Funding']['Unit_Price']='單位價格';

//Download Documents
$Lang['ePCM']['Mgmt']['DownloadDocuments']['Summary']['Eng']['Q']="下載書面報價摘要及批核紀錄表 (英文版本)";
$Lang['ePCM']['Mgmt']['DownloadDocuments']['Summary']['Eng']['T']="下載投標摘要及批核紀錄表 (英文版本)";
$Lang['ePCM']['Mgmt']['UploadDocuments']['Summary'] = '上載已簽署紀錄表';

// Quotation
$Lang['ePCM']['Mgmt']['Quotation']['ContactMethod'] = '邀請方法';

// Email to Supplier
$Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Subject'] = '報價邀請 (<!--CASE_NUMBER-->)';
$Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Body'] = '' . '敬啟者：<br/>' . '<br/>' . '現誠邀 貴公司 (<!--SUPPLIER_NAME-->)承投本校「<!--CASE_NAME-->」(編號<!--CASE_NUMBER-->)。詳情請參附件。<br/>' . '<br/>' . '另外，煩請按以下連結確認，以讓本校得知 貴公司已收到此邀請。'.'<br/>'.'<!--TOKEN_END_DATE-->'.'<br/>' . '<br/>' . '<!--TOKEN_URL_START--><!--TOKEN_URL--><!--TOKEN_URL_END--><br/>'.'以下密碼供 貴公司在以上連結下載有關文件: <!--API_PASSWORD--><br/>'. '<br/>' . '<!--SCHOOL_NAME-->';
// $Lang['ePCM']['Mgmt']['Email']['ToSupplier']['Password'] = '以下密碼供 貴公司在以上連結下載有關文件: ';
$Lang['ePCM']['Mgmt']['Email']['Preview'] = '預覽電郵';


// Login
$Lang['ePCM']['Index']['Login']['Authentication'] = '登入認證';
$Lang['ePCM']['Index']['Login']['Please login with your user password.'] = '請輸入用戶密碼進行登入認證。';
$Lang['ePCM']['Index']['Login']['Password'] = '密碼';
$Lang['ePCM']['Index']['Login']['InputPassword'] = "請輸入密碼";

// IP Failed
$Lang['ePCM']['Index']['NetworkIPFail']['Message'] = '您的電腦不能訪問採購系統';

// Report:: Common
$Lang['ePCM']['Report']['ReportFormat']['ReportFormat'] = '輸出格式';
$Lang['ePCM']['Report']['ReportFormat']['Pdf'] = 'PDF';
$Lang['ePCM']['Report']['ReportFormat']['Csv'] = 'CSV';
$Lang['ePCM']['Report']['Order'] = '排序';
$Lang['ePCM']['Report']['Code'] = '編號';
$Lang['ePCM']['Report']['ApplyDate'] = '申請日期';
// Report::FCS
$Lang['ePCM']['Report']['FCS']['DateRange'] = '日期';
$Lang['ePCM']['Report']['FCS']['to'] = '至';
$Lang['ePCM']['Report']['FCS']['Category'] = '項目類別';
$Lang['ePCM']['Report']['FCS']['Group'] = '部門/科組';
$Lang['ePCM']['Report']['FCS']['AmountRange'] = '預算金額範圍';
$Lang['ePCM']['Report']['FCS'][' or above'] = '或以上';
$Lang['ePCM']['Report']['FCS']['Status'] = '狀態';
$Lang['ePCM']['Report']['FCS']['Application in progress'] = '申請進行中';
$Lang['ePCM']['Report']['FCS']['Completed'] = '已完成';
$Lang['ePCM']['Report']['FCS']['Rejected'] = '已被拒絕';
$Lang['ePCM']['Report']['FCS']['Get Report'] = '產生報表';
$Lang['ePCM']['Report']['FCS']['Please input Date'] = '請輸入日期';
$Lang['ePCM']['Report']['FCS']['Invalid Date Range'] = '無效的日期範圍';
$Lang['ePCM']['Report']['FCS']['Please choose a Group'] = '請選擇 部門/科組';
$Lang['ePCM']['Report']['FCS']['AcademicYear'] = '年度';
$Lang['ePCM']['Report']['FCS']['File Code Summary'] = '採購檔案編號總表';
$Lang['ePCM']['Report']['FCS']['Code'] = '編號';
$Lang['ePCM']['Report']['FCS']['Name'] = '採購物品/服務';
$Lang['ePCM']['Report']['FCS']['Application Date'] = '申請日期';
$Lang['ePCM']['Report']['FCS']['Applicant'] = '申請人';
$Lang['ePCM']['Report']['FCS']['Budget'] = '購買價格';
$Lang['ePCM']['Report']['FCS']['Remarks'] = '備註';
$Lang['ePCM']['Report']['FCS']['No Applications'] = '沒有任何申請';
$Lang['ePCM']['Report']['FCS']['Academic Year'] = '學年';
$Lang['ePCM']['Report']['FCS']['Please input Academic Year'] = '請輸入學年';
$Lang['ePCM']['Report']['FCS']['TStart'] = '招標日期';
$Lang['ePCM']['Report']['FCS']['TEnd'] = '截標日期';
$Lang['ePCM']['Report']['FCS']['QStart'] = '邀請報價日期';
$Lang['ePCM']['Report']['FCS']['QEnd'] = '截止報價日期';
$Lang['ePCM']['Report']['FCS']['Category'] = '項目';
$Lang['ePCM']['Report']['FCS']['IncludeZeroExpenses'] = '包括沒有任何支出的財政項目';
// Report::BER
$Lang['ePCM']['Report']['BER']['AcademicYear'] = '年度';
$Lang['ePCM']['Report']['BER']['Budget and Expenses Report'] = '預算及支出報告';
$Lang['ePCM']['Report']['BER']['Budget'] = '預算';
$Lang['ePCM']['Report']['BER']['Dollar'] = '元';
$Lang['ePCM']['Report']['BER']['Item'] = '財政項目';
$Lang['ePCM']['Report']['BER']['Deal Price'] = '實際數額';
$Lang['ePCM']['Report']['BER']['Sum'] = '總計';
$Lang['ePCM']['Report']['BER']['No Items'] = '沒有財政項目';
$Lang['ePCM']['Report']['BER']['Difference'] = '剩餘';
$Lang['ePCM']['Report']['BER']['Items'] = '財政項目';
$Lang['ePCM']['Report']['BER']['ApplicationType'] = '紀錄類型';
$Lang['ePCM']['Report']['BER']['ProcurementRecords'] = '採購支出';
$Lang['ePCM']['Report']['BER']['OtherRecords'] = '雜項支出';

// Report: PCMRS
$Lang['ePCM']['Report']['PCMRS']['Title'] = '採購紀錄總表';

// Push Notification Template
$Lang['ePCM']['ScheduledPushNotification']['Deadline']['Title'] = '採購系統申請書面報價/截標快將到期';
$Lang['ePCM']['ScheduledPushNotification']['Deadline']['Message'] = '你的採購系統申請，其書面報價/截標快將到期。';
$Lang['ePCM']['DeclarationInstruction'] = '學校應要求負責採購及物料供應職務的人員簽署承諾書，承諾如得知其本人或家人現正或將會與供應商有密切的連繫(例如：親戚、僱主、股東 等 ) ，會盡早向校董會或法團校董會作出書面申報。<br>
		學校必須妥為記錄就利益衝突所作出的申報（ 可 使用標準表格，請參閱附 件 I）或有關資料的披露及所採取的必要行動，以避免任何實際或被視為可出現的利益衝突。<br>
		已申報利益衝突的員工須避免處理相關的報價/投標，或應遵從校董會/法團校董會的指示。<br>
		校方每年應發出通告，向校內人員說明這項規定，並要求員工簽署，證明已細閱及了解通告的內容。<br>';

$Lang['ePCM']['Template'] = '範本';
$Lang['ePCM']['RoleManagement']['SMC'] = '校董';
$Lang['ePCM']['FundingSource']['FundingSource'] = '資金來源';
$Lang['ePCM']['FundingSource']['RecordStatus']['ALL'] = '所有';
// $Lang['ePCM']['FundingSource']['RecordStatus'][0] = "未啟用"; 
$Lang['ePCM']['FundingSource']['RecordStatus'][1] = "使用中";
$Lang['ePCM']['FundingSource']['RecordStatus'][2] = "已停用";
$Lang['ePCM']['FundingSource']['EnglishName'] = '英文名稱';
$Lang['ePCM']['FundingSource']['ChineseName'] = '中文名稱';
$Lang['ePCM']['FundingSource']['Funding'] = '批次';
$Lang['ePCM']['FundingSource']['Category'] = '資金來源類別';
$Lang['ePCM']['FundingSource']['Budget'] = '金額';
$Lang['ePCM']['FundingSource']['RemainingBudget'] = '餘額';
$Lang['ePCM']['FundingSource']['DeadLine'] = '到期日';
$Lang['ePCM']['FundingSource']['CaseUsingFunding'] = '使用資金之採購項目';
$Lang['ePCM']['FundingSource']['RecordStatus']['RecordStatus'] = '狀況';
$Lang['ePCM']['FundingSource']['Add'] = '新增';
$Lang['ePCM']['FundingSource']['Edit'] = '編輯';
$Lang['ePCM']['FundingSource']['DeleteAlert'] = '你是否確定刪除有關紀錄';
$Lang['ePCM']['FundingSource']['EditAlert'] = '此紀錄不能為空白';
$Lang['ePCM']['FundingSource']['UsingBudget'] = '動用資金預算 ';
$Lang['ePCM']['FundingSource']['ApprovalStatus']['0'] = '未確認';
$Lang['ePCM']['FundingSource']['ApprovalStatus']['1'] = '已確認';
$Lang['ePCM']['FundingSource']['ApprovalStatus']['2'] = '已拒絕';
$Lang['ePCM']['FundingSource']['RecordStatusIncorrect'] = '狀況不正確';
$Lang['ePCM']['FundingSource']['FundingIncorrect'] = '資金來源不存在';
$Lang['ePCM']['FundingSource']['FundingCatIncorrect'] = '資金來源不存在';
$Lang['ePCM']['FundingSource']['ImportCheckFundingCategory'] = '按此查詢資金來源類別代號';
##### Import Helper & Validator [start] #####
$Lang['ePCM']['Import']['Step2Loading'] = '正在核對紀錄，請稍候 。';
$Lang['ePCM']['Import']['Remarks'] = '備註';
$Lang['ePCM']['Import']['Success'] = '完成，已匯入 <!--count--> 項紀綠。';
$Lang['ePCM']['Import']['Done'] = '相關匯入程序已完成。';
$Lang['ePCM']['Import']['Fail1'] = '部分項目未能匯入，請重新匯入。';
$Lang['ePCM']['Import']['Fail2'] = '錯誤。';
$Lang['ePCM']['Import']['Ref']['Code'] = '代號';
$Lang['ePCM']['Import']['DateFormatRemarks'] = '格式：YYYY-MM-DD';
$Lang['ePCM']['Import']['Validator']['required'] = '<!--name-->不能留空';
$Lang['ePCM']['Import']['Validator']['maxlength'] = '<!--name-->長度不可以超過<!--length-->個字元';
$Lang['ePCM']['Import']['Validator']['date'] = '<!--name-->日期格式不正確';
$Lang['ePCM']['Import']['Validator']['numeric'] = '<!--name-->數字格式不正確';
##### Import Helper & Validator [end] #####

$Lang['Btn']['close']= '關閉';

// cust lang here [start]
if($sys_custom['ePCM_skipCaseApproval']) {
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['AdminAction'] = '由 <!--adminName--> 代為確認';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Pending'] = '未確認';
    $Lang['ePCM']['Mgmt']['Case']['CaseApproval'] = '採購確認';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['ApprovalStatus'] = '確認狀況';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approved'] = '已確認';
    $Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Approve'] = '確認';
}

$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['AllStatus'] = '所有採購確認狀況';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CasePendingForYourApproval'] = '你未確認的採購';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CaseYouApproved'] = '你已確認的採購';
$Lang['ePCM']['Mgmt']['Case']['ApprovalStatus']['Endorsement']['CaseYouRejected'] = '你已拒絕的採購';


$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage']['AllStage']="所有階段";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][0]="我的草稿";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][1]="申請採購";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][2]="口頭報價/書面報價/招標";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][3]="上載報價文件/開標審核";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][4]="輸入結果/標書批核";
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][5]=$Lang['ePCM']['Mgmt']['Case']['Endorsement'];
$Lang['ePCM']['Mgmt']['Case']['Filter']['CurrentStage'][6]=$Lang['ePCM']['Mgmt']['Case']['Endorsement'];
// cust lang here [end]

if (!$NoLangWordings && is_file("$intranet_root/templates/ePCM_lang.$intranet_session_language.customized.php"))
{
    include("$intranet_root/templates/ePCM_lang.$intranet_session_language.customized.php");
}
?>