<?php
//using:  
//A
$Lang['eClassApp']['AccountPageBanner'] = 'Account Page Banner';
$Lang['eClassApp']['AcceptPushMessage'] = 'Registered for push message';
$Lang['eClassApp']['AddMember'] = 'Add Member';
$Lang['eClassApp']['AddNoOfCode'] = 'Add No. of Code';
$Lang['eClassApp']['AllEnableStatus'] = 'All Enable Status';
$Lang['eClassApp']['AllParent'] = 'All Parents';
$Lang['eClassApp']['AllRegistrationStatus'] = 'All Registration Status';
$Lang['eClassApp']['AllStatus'] = 'All Status';
$Lang['eClassApp']['AuthorizationCode'] = 'Authorization Code';
$Lang['eClassApp']['AdvancedSetting'] = 'Advanced Setting';
$Lang['eClassApp']['AdvancedSettingList']['PushMessageRight'] = 'Push Message Right';
$Lang['eClassApp']['AllowClassTeacherSendMessage'] = 'Allow class teacher to send push message';
$Lang['eClassApp']['AllowClubAndActivityPICSendMessage'] = 'Allow club admin to send push message';
$Lang['eClassApp']['AllowDeleteOwnPushMessage'] = 'Allow non-admin users to delete own push message records';
$Lang['eClassApp']['AllowSendPushMessageToClassStudent'] = 'Allow to send push message to own class parents';
$Lang['eClassApp']['AllowSendPushMessageToAllStudent'] = 'Allow to send push message to all parents';
$Lang['eClassApp']['AllowSendPushMessageToClassStudentApp'] = 'Allow to send push message to own class students';
$Lang['eClassApp']['AllowSendPushMessageToAllStudentApp'] = 'Allow to send push message to all students';
$Lang['eClassApp']['AllowViewAllStudentList'] = 'Allow to view all students';
$Lang['eClassApp']['AllowViewStudentContact'] = 'Allow to view all student\'s contact info';
$Lang['eClassApp']['AllowViewStudentContactClass'] = 'Allow to view own class student\'s contact info';
$Lang['eClassApp']['AllowTeacherReceivePushMessageWhileLoggedOut'] = 'Allow teacher to receive push message while logged out';
$Lang['eClassApp']['AppHomePageDisplay'] = 'App Homepage Display';
$Lang['eClassApp']['AppEAttendanceDisplay'] = 'App "eAttendance" Display';

//B
$Lang['eClassApp']['BackgroundImage'] = 'HomePage Banner';
$Lang['eClassApp']['Blacklist'] = 'Blacklist';

//C
$Lang['eClassApp']['CannotUpdateSchoolBannerduringTrialPeroid'] = 'Background image cannot be changed within trial period.';
$Lang['eClassApp']['CommonFunction'] = 'Common Function';
$Lang['eClassApp']['ClassTeacher'] = 'Class Teacher';
$Lang['eClassApp']['Content'] = 'Content';
$Lang['eClassApp']['ComposeMessage'] = 'Compose Message';

//D
$Lang['eClassApp']['DigitalChannels']['UntitledAlbum'] = 'Untitled Album';
$Lang['eClassApp']['DigitalChannels']['PhotoTitle'] = 'Title';
$Lang['eClassApp']['DigitalChannels']['AlbumTitle'] = 'Album';
$Lang['eClassApp']['DigitalChannels']['Category'] = 'Category';
$Lang['eClassApp']['DigitalChannels']['UploadDate'] = 'Date Input';
$Lang['eClassApp']['DigitalChannels']['PhotoViews'] = 'view';
$Lang['eClassApp']['DigitalChannels']['PhotoFavorite'] = 'favorite';
$Lang['eClassApp']['DigitalChannels']['PhotoComment'] = 'comment';
$Lang['eClassApp']['DigitalChannels']['Comment_no_empty'] = 'Please fill in the comment.';
$Lang['eClassApp']['DigitalChannels']['EventTitle'] = 'Event Title';
$Lang['eClassApp']['DigitalChannels']['EventDate'] = 'Event Date';

//E
$Lang['eClassApp']['EnableStatus'] = 'Enable Status';
$Lang['eClassApp']['EnableStudentApp'] = 'Enable Student App';
$Lang['eClassApp']['eNoticeS']['SignResults'] = 'Progress';
$Lang['eClassApp']['eNoticeS']['All'] = 'All';
$Lang['eClassApp']['eNoticeS']['Signed'] = 'Signed';
$Lang['eClassApp']['eNoticeS']['Unsigned'] = 'Unsigned';
$Lang['eClassApp']['eNoticeS']['Parent-signedNotice'] = 'Parent-signed Notice';
$Lang['eClassApp']['eNoticeS']['Student-signedNotice'] = 'Student-signed Notice';
$Lang['eClassApp']['eNoticeS']['BackToList'] = 'Back to List';
$Lang['eClassApp']['eHomework']['DeleteSuccess'] = 'Record deleted.';
$Lang['eClassApp']['eHomework']['NoDeleteAccRecord'] = 'Record delete failed, don`t have the access right to delete record.';
$Lang['eClassApp']['eHomework']['NeedToHand-in'] = 'Need to hand-in';
$Lang['eClassApp']['eHomework']['NoNeedToHand-in'] = 'No need to hand-in';
$Lang['eClassApp']['eHomework']['NeedToCollectHomework'] = 'Need to collect homework';
$Lang['eClassApp']['eHomework']['NoNeedToCollectHomework'] = 'No need to collect homework';
$Lang['eClassApp']['eHomework']['UpcomingExpiringHomeworks'] = 'Not yet expired homework';
$Lang['eClassApp']['eHomework']['PastExpiringHomeworks'] = 'Expired homework';
$Lang['eClassApp']['eHomework']['AllHomeworks'] = 'All homeworks';
$Lang['eClassApp']['eHomework']['All'] = 'All';
$Lang['eClassApp']['eHomework']['NotAllCollected'] = 'Not all collected';
$Lang['eClassApp']['eEnrollment']['NotTake'] = 'Not Taken';
$Lang['eClassApp']['eEnrollment']['HasTaken'] = 'Has Taken';
$Lang['eClassApp']['eEnrollment']['RecordDate'] = 'Date';
$Lang['eClassApp']['eEnrollment']['RecordHasUpdated'] = 'Record has updated.';
$Lang['eClassApp']['eEnrollment']['Name'] = 'Name';
$Lang['eClassApp']['eEnrollment']['Amount'] = 'Amount';
$Lang['eClassApp']['eEnrollment']['AreYouSureToSubmit'] = 'Are you sure to submit?';
$Lang['eClassApp']['eEnrollment']['MultiSelect'] = 'Multi Select';
$Lang['eClassApp']['eEnrollment']['TurnTo'] = 'Turn to';
$Lang['eClassApp']['eEnrollment']['Confirm'] = 'Confirm';

//F
$Lang['eClassApp']['FunctionAccessRight'] = 'Function Access Right';

//G
$Lang['eClassApp']['GroupQuota'] = 'Group Quota';
$Lang['eClassApp']['Group'] = 'Group';
$Lang['eClassApp']['GroupMessage']['CommunicationMode'] = 'Communication Mode';
$Lang['eClassApp']['GroupMessage']['CommunicationMode_Normal'] = 'Normal Mode';
$Lang['eClassApp']['GroupMessage']['CommunicationMode_DisableParentToParent'] = 'Parent Limited Mode';
$Lang['eClassApp']['GroupMessage']['CannotChangeLater'] = 'This cannot be changed after set.';
$Lang['eClassApp']['GroupMessage']['ChangeCommunicateModeWarning1'] = 'Parents can read all messages from teacher or other parents in the same group';
$Lang['eClassApp']['GroupMessage']['ChangeCommunicateModeWarning2'] = 'Parents can read the messages from teacher only; Parent\'s reply/message can be read by teacher only.';
$Lang['eClassApp']['GroupMessageBatchCreate'] = 'By mapping to current class structure';

//H
$Lang['eClassApp']['HKUFlu']['PushMessageTitle'] = '香港大學疾病監測計劃';
$Lang['eClassApp']['HKUFlu']['PushMessageContent'] = '多謝參與香港大學疾病監測計劃。
	因學校考勤系統顯示　貴子女%NAME%今天缺席，請按以下連結為　貴子女提供缺席的相關資料。
	
	缺席日期：%DATE%';

//I
$Lang['eClassApp']['ImageResolutionRemarks'] = "Suggested resolution: <!--width-->px * <!--height-->px (width * height)";
$Lang['eClassApp']['ImageResolutionWarning'] = "Large images will increase the loading of school eClass server. Therefore, we recommend users to upload images with suggested image resolution.";
$Lang['eClassApp']['Info'] = "Info";
$Lang['eClassApp']['InUse'] = 'In use';
$Lang['eClassApp']['iPortfolio']['AllForms'] = "All Forms";
$Lang['eClassApp']['iPortfolio']['ReadBySystem'] = "Read by system";
$Lang['eClassApp']['iPortfolio']['SelectOEATitlePairingMethod'] = "Select OEA title pairing method";
$Lang['eClassApp']['iPortfolio']['NewOLE'] = "New OLE";
$Lang['eClassApp']['iPortfolio']['AssignStudentToProgramme'] = "Assign student to programme";
$Lang['eClassApp']['iPortfolio']['ViewThisProgramme'] = "View this programme";
$Lang['eClassApp']['iPortfolio']['AddCommonRecordForStudent'] = "Add common record for student";
$Lang['eClassApp']['iPortfolio']['Selected'] = "Selected";
$Lang['eClassApp']['iPortfolio']['SaveRecord'] = "Save record";
$Lang['eClassApp']['iPortfolio']['AreYouSureToSaveTheRecord'] = "Are you sure to save the record？";
$Lang['eClassApp']['iPortfolio']['Programme'] = "Programme";
$Lang['eClassApp']['iPortfolio']['Student'] = "Student";
$Lang['eClassApp']['iPortfolio']['ActivityTotalHours'] = "Activity Total Hours";
$Lang['eClassApp']['iPortfolio']['Items'] = "Items";
$Lang['eClassApp']['iPortfolio']['TotalRecordsApproved'] = "Total Records (Approved)";
$Lang['eClassApp']['iPortfolio']['TotalRecords'] = "Total Records";

//J
$Lang['eClassApp']['jsWarning']['deleteAccountPageBanner'] = "Are you sure you want to delete account page banner?";
$Lang['eClassApp']['jsWarning']['deleteBackgroundImage'] = "Are you sure you want to delete homepage banner?";
$Lang['eClassApp']['jsWarning']['deleteSchoolBadge'] = "Are you sure you want to delete school badge?";
$Lang['eClassApp']['jsWarning']['reachedNumOfGroupLimit'] = "Reached the limit of number of groups already.";
$Lang['eClassApp']['jsWarning']['reachedNumOfMemberLimit'] = "Reached the limit of number of members already.";
$Lang['eClassApp']['jsWarning']['selectAtLeastOneParent'] = "Please select at least one parent.";
$Lang['eClassApp']['jsWarning']['selectedMemberExceedMemberLimit'] = "Number of selected members exceeded the quota limit.";
$Lang['eClassApp']['jsWarning']['schoolWebsiteURLFormatIncorrect'] = 'Incorrect School Website URL.';

//K


//L
$Lang['eClassApp']['LastLoginTime'] = 'Last Login Time';
$Lang['eClassApp']['LoggedInUser'] = 'Logged in user';
$Lang['eClassApp']['LoginStatus'] = 'Login Status';

//M
$Lang['eClassApp']['MemberList'] = 'Member List';
$Lang['eClassApp']['MemberQuota'] = 'Member Quota';
$Lang['eClassApp']['Module'] = 'Module';
$Lang['eClassApp']['ModuleNameAry']['ApplyLeave'] = 'Apply Leave';
$Lang['eClassApp']['ModuleNameAry']['CrossBoundarySchoolCoaches'] = 'Cross-boundary School Coaches';
$Lang['eClassApp']['ModuleNameAry']['eAttendance'] = 'eAttendance';
$Lang['eClassApp']['ModuleNameAry']['eCircular'] = 'eCircular';
$Lang['eClassApp']['ModuleNameAry']['eHomework'] = 'eHomework';
$Lang['eClassApp']['ModuleNameAry']['eNotice'] = 'eNotice';
$Lang['eClassApp']['ModuleNameAry']['ePayment'] = 'ePayment';
$Lang['eClassApp']['ModuleNameAry']['GroupMessage'] = 'Group Message';
$Lang['eClassApp']['ModuleNameAry']['iMail'] = 'iMail';
$Lang['eClassApp']['ModuleNameAry']['SchoolCalendar'] = 'School Calendar';
$Lang['eClassApp']['ModuleNameAry']['SchoolNews'] = 'School News';
$Lang['eClassApp']['ModuleNameAry']['TeacherStatus'] = 'Staff List';
$Lang['eClassApp']['ModuleNameAry']['eEnrolment'] = 'eEnrolment';
$Lang['eClassApp']['ModuleNameAry']['StudentStatus'] = 'Student List';
if ($sys_custom['DHL']) {
	$Lang['eClassApp']['ModuleNameAry']['SchoolInfo'] = 'Company Info';
} else {
	$Lang['eClassApp']['ModuleNameAry']['SchoolInfo'] = 'School Info';
}
$Lang['eClassApp']['ModuleNameAry']['StaffAttendance'] = 'Staff Attendance';
$Lang['eClassApp']['ModuleNameAry']['StudentPerformance'] = 'Student Performance';
$Lang['eClassApp']['ModuleNameAry']['flippedchannel'] = 'Flipped Channels';
$Lang['eClassApp']['ModuleNameAry']['digitalChannels'] = 'Digital Channels';
$Lang['eClassApp']['ModuleNameAry']['photoAlbum'] = 'Photo Album';
$Lang['eClassApp']['ModuleNameAry']['takeAttendance'] = 'eAttendance';
$Lang['eClassApp']['ModuleNameAry']['medicalCaring'] = 'Medical Caring';
$Lang['eClassApp']['ModuleNameAry']['weeklyDiary'] = 'Weekly diary & Newspaper cutting';
$Lang['eClassApp']['ModuleNameAry']['eLibPlus'] = 'eLibrary Plus';
$Lang['eClassApp']['ModuleNameAry']['timetable'] = 'Timetable';
$Lang['eClassApp']['ModuleNameAry']['hkuFlu'] = 'HKUSPH';
$Lang['eClassApp']['ModuleNameAry']['classroom'] = 'Classroom';
$Lang['eClassApp']['ModuleNameAry']['eBooking'] = 'eBooking';
$Lang['eClassApp']['ModuleNameAry']['eDiscipline'] = 'eDiscipline';
$Lang['eClassApp']['ModuleNameAry']['hostelTakeAttendance'] = 'Hostel take attendance';
$Lang['eClassApp']['ModuleNameAry']['iPortfolio'] = 'iPortfolio';
$Lang['eClassApp']['ModuleNameAry']['eSchoolBus'] = 'eSchoolBus';
$Lang['eClassApp']['ModuleNameAry']['eEnrolmentUserView'] = 'eEnrolment';
$Lang['eClassApp']['ModuleNameAry']['reprintCard'] = 'Reprint Card';
$Lang['eClassApp']['ModuleNameAry']['ePOS'] = 'ePOS';
$Lang['eClassApp']['ModuleNameAry']['eInventory'] = 'eInventory (Asset Management)';
$Lang['eClassApp']['ModuleNameAry']['eLearningTimetable'] = 'eLearning Timetable';
$Lang['eClassApp']['ModuleNameAry']['bodyTemperature'] = 'Body Temperature';

//N
$Lang['eClassApp']['NoOfCode'] = 'No. of Code';
$Lang['eClassApp']['NoOfUsedCode'] = 'No. of Used Code';
$Lang['eClassApp']['NotAcceptPushMessage'] = 'Not registered for push message';
$Lang['eClassApp']['NotInUse'] = 'Not in use';
$Lang['eClassApp']['NotLoggedInUser'] = 'Not logged in user';
$Lang['eClassApp']['NotYetRegistered'] = 'Not yet registered';
$Lang['eClassApp']['NonClassTeacher'] = 'Other Teacher/Staff';
$Lang['eClassApp']['NonTeacher'] = 'Non Teacher';

//O


//P
$Lang['eClassApp']['ParentApp'] = 'Parent App';
$Lang['eClassApp']['PushMessage'] = 'Push Message';
$Lang['eClassApp']['StudentPerformance']['Term'] = 'Term';
$Lang['eClassApp']['StudentPerformance']['Student'] = 'Student';
$Lang['eClassApp']['StudentPerformance']['Number'] = 'Class No.';
$Lang['eClassApp']['StudentPerformance']['Name'] = 'Name';
$Lang['eClassApp']['StudentPerformance']['LatestScore'] = 'Latest';
$Lang['eClassApp']['StudentPerformance']['TotalScore'] = 'Score';
$Lang['eClassApp']['StudentPerformance']['TotalComment'] = 'CMT';
$Lang['eClassApp']['StudentPerformance']['TotalActScore'] = 'Total Score';
$Lang['eClassApp']['StudentPerformance']['Comment'] = 'Comments';
$Lang['eClassApp']['StudentPerformance']['ActScore'] = 'Score';
$Lang['eClassApp']['StudentPerformance']['Submit'] = 'Submit';
$Lang['eClassApp']['StudentPerformance']['NumOfComment'] = 'No. of comment';

//Q


//R
$Lang['eClassApp']['Registered'] = 'Registered';
$Lang['eClassApp']['RegistrationDate'] = 'Registration Date';
$Lang['eClassApp']['RegistrationStatus'] = 'Registration Status';
$Lang['eClassApp']['Recipient'] = 'Recipient(s)';

//S
$Lang['eClassApp']['SchoolBadge'] = 'School Badge';
$Lang['eClassApp']['SchoolImage'] = 'School Image';
$Lang['eClassApp']['SelectUser'] = 'Select User(s)';
$Lang['eClassApp']['SendTapCardPushMessage'] = 'Send tap card push message';
$Lang['eClassApp']['SendTapCardPushMessageOnNonSchoolDay'] = 'Send tap card push message on non-school day';
$Lang['eClassApp']['SendTapCardPushMessageType'] = 'Send tap card push message type';
$Lang['eClassApp']['SetAsAdmin'] = 'Set as admin';
$Lang['eClassApp']['SetAsMember'] = 'Set as member';
$Lang['eClassApp']['SchoolInfo']['Sub-menusOrItems'] = 'Num of sub-items';
$Lang['eClassApp']['SchoolInfo']['AlertDelete1'] = 'Please check item(s) to delete.';
$Lang['eClassApp']['SchoolInfo']['AlertDelete2'] = 'It will delete the sub-items as well, are you sure you want to delete?';
$Lang['eClassApp']['SchoolInfo']['AlertEnglishTitle'] = 'Please input the English Title.';
$Lang['eClassApp']['SchoolInfo']['AlertChineseTitle'] = 'Please input the Chinese Title.';
$Lang['eClassApp']['SchoolInfo']['AlertChangeStatus'] = 'Please check item(s) to change status.';
$Lang['eClassApp']['SchoolInfo']['AlertInputUrl'] = 'Please input the Hyperlink.';
$Lang['eClassApp']['SchoolInfo']['AlertFileMissing'] = 'Please upload a PDF file.';
$Lang['eClassApp']['SchoolInfo']['AlertWrongFileType'] = 'Please upload a PDF file.';
$Lang['eClassApp']['SchoolInfo']['AlertFileOvercast'] = 'The old PDF file be overwritten by the new PDF file, continue to modify?';
$Lang['eClassApp']['SchoolInfo']['Icon'] = 'Icon';
$Lang['eClassApp']['SchoolInfo']['Type'] = 'Type';
$Lang['eClassApp']['SchoolInfo']['NoIcon'] = 'No Icon';
$Lang['eClassApp']['SchoolInfo']['DefaultIcon'] = 'Default Icon';
$Lang['eClassApp']['SchoolInfo']['CustomIcon'] = 'Custom Icon';
$Lang['eClassApp']['SchoolInfo']['Menu'] = 'Menu';
$Lang['eClassApp']['SchoolInfo']['Item'] = 'Item';
$Lang['eClassApp']['SchoolInfo']['Content'] = 'Content';
$Lang['eClassApp']['SchoolInfo']['Hyperlink'] = 'Hyperlink';
$Lang['eClassApp']['SchoolInfo']['Eng'] = 'ENG';
$Lang['eClassApp']['SchoolInfo']['Chi'] = 'CHI';
$Lang['eClassApp']['SchoolInfo']['ItemTitle'] = 'Item Title';
$Lang['eClassApp']['SchoolInfo']['SelectedMenu'] = 'Selected Menu';
$Lang['eClassApp']['SchoolInfo']['Status'] = 'Status';
$Lang['eClassApp']['SchoolInfo']['Public'] = 'Public';
$Lang['eClassApp']['SchoolInfo']['Private'] = 'Private';
$Lang['eClassApp']['SchoolInfo']['PDF'] = 'PDF File';
$Lang['eClassApp']['SchoolInfo']['ReturnMsgArr']['ReorderSuccess'] = '1|=|Re-ordered Successfully.';
$Lang['eClassApp']['SchoolInfo']['ReturnMsgArr']['ReorderFailed'] = '0|=|Re-order Failed.';
$Lang['eClassApp']['StudentStatus']['MALE'] = 'Male';
$Lang['eClassApp']['StudentStatus']['FEMALE'] = 'Female';
$Lang['eClassApp']['StudentStatus']['ONTIME'] = 'On time';
$Lang['eClassApp']['StudentStatus']['LATE'] = 'Late';
$Lang['eClassApp']['StudentStatus']['OUT'] = 'Outing';
$Lang['eClassApp']['StudentStatus']['EARLYLEAVE'] = 'Early Leave';
$Lang['eClassApp']['StudentStatus']['UNAVAILABLE'] = 'Absent';
$Lang['eClassApp']['StudentStatus']['BIRTHDATE'] = 'Birth Date';
$Lang['eClassApp']['StudentStatus']['STATUS'] = 'Status';
$Lang['eClassApp']['StudentStatus']['CONTACT'] = 'Contact';
$Lang['eClassApp']['StudentStatus']['HOME'] = 'Home';
$Lang['eClassApp']['StudentStatus']['MOBILEPHONE'] = 'Mobile';
$Lang['eClassApp']['StudentStatus']['EMAIL'] = 'Email';
$Lang['eClassApp']['StudentStatus']['ADDRESS'] = 'Address';
$Lang['eClassApp']['StudentStatus']['NOACCESSRIGHT'] = 'Current Module Is Only For Class Teacher.';
$Lang['eClassApp']['StudentStatus']['REMARK'] = 'Class teacher can view own class\'s student list by default.';
$Lang['eClassApp']['StudentStatus']['EMERGENCYCONTACT'] = 'Emergency Contact';
$Lang['eClassApp']['ShowStudentAttendanceStatus']= 'Show attendance status in app homepage';
$Lang['eClassApp']['ShowStudentAttendanceTime']= 'Show attendance time in app homepage';
$Lang['eClassApp']['ShowStudentAttendanceLeaveSection']= 'Show leave school attendance section in app homepage';
$Lang['eClassApp']['ShowStudentAttendanceDetails']= 'Show "eAttendance" in app';
$Lang['eClassApp']['ShowAttendanceTimeInEAttendance']= 'Show attendance time in app\'s "eAttendance"';
$Lang['eClassApp']['ShowStatusStatisticsInEAttendance']= 'Show attendance status statistics in app\'s "eAttendance"';
$Lang['eClassApp']['Send'] = 'Send';
$Lang['eClassApp']['SendPushMessageToParentOfTheSelectedStudents'] = 'Send push notification to the parent(s) of the selected student(s) below';
$Lang['eClassApp']['SendSuccess'] = 'Send successfully!';
$Lang['eClassApp']['SendFail'] = 'Fail to send message, please retry later.';
$Lang['eClassApp']['StudentApp'] = 'Student App';
$Lang['eClassApp']['StudentAttendance']['ShowStudentTag'] = "Student Tags"; 
$Lang['eClassApp']['StudentAttendance']['ShowNames'] = 'Show Names';
$Lang['eClassApp']['StudentAttendance']['ShowClassAndNum'] = 'Show Class and No.';
$Lang['eClassApp']['StudentAttendance']['ShowFullTag'] = 'show completed tags';
$Lang['eClassApp']['StudentAttendance']['ShowGroup'] = 'show group name';
$Lang['eClassApp']['StudentAttendance']['NotShowTag'] = 'Not Showing Tags';
$Lang['eClassApp']['StudentAttendance']['TakeAttendance'] = 'Take Attendance';
$Lang['eClassApp']['StudentAttendance']['EarlyLeave'] = 'Early Leave';
$Lang['eClassApp']['SchoolWebsiteURL'] = "School Website URL";

//T
$Lang['eClassApp']['TeacherApp'] = 'Teacher App';
$Lang['eClassApp']['Title'] = 'Title';
$Lang['eClassApp']['Teacher'] = 'Teacher';
$Lang['eClassApp']['TapCardStatusAry']['Arrival'] = 'Arrival';
$Lang['eClassApp']['TapCardStatusAry']['Leave'] = 'Leave';

//U
$Lang['eClassApp']['TeacherStatus'] = 'Teacher Status';
$Lang['eClassApp']['TeacherStatusAccessRole'] = 'Authorized Role';
$Lang['eClassApp']['TeacherStatusAccessTeacher'] = 'Authorized Staff';
$Lang['eClassApp']['TeacherStatusAllTeacher'] = 'All Staffs';
$Lang['eClassApp']['TeacherStatusSelectAtLeastOne'] = 'Please Choose At Least One Teaching Staff.';
$Lang['eClassApp']['TeacherStatus_Nonarrival'] = 'NonArrival';
$Lang['eClassApp']['TeacherStatus_Leave'] = 'Leave';
$Lang['eClassApp']['TeacherStatus_Arrival'] = 'Arrival';
$Lang['eClassApp']['TeacherStatus_Search'] = 'Search...';
$Lang['eClassApp']['TeacherStatus_UserPhoneNo'] = 'Current User Has No Phone Contact.';
$Lang['eClassApp']['TeacherStatus_UserNoAccessRight'] = 'Current Module Has No Access Right.';
$Lang['eClassApp']['TeacherStatusAccessGroup'] = 'Able-to-view group';
$Lang['eClassApp']['TeacherStatusEnableAllGroup'] = 'Enable all groups';
$Lang['eClassApp']['TeacherStatusEnableSpecificGroup'] = 'Enable specific group(s)';
$Lang['eClassApp']['TeacherStatusDisableGroup'] = 'Disable';
$Lang['eClassApp']['TeacherStatusGetGroup'] = 'Choose Group';
$Lang['eClassApp']['TeacherStatusAuthorizedGroup'] = 'Authorized Group';
$Lang['eClassApp']['TeacherStatusSelectAtLeastOneGroup'] = 'Please Choose At Least One Group.';
$Lang['eClassApp']['TakeAttendance']['ArrivalTime'] = 'Arrival time';
$Lang['eClassApp']['TakeAttendance']['LeaveTime'] = 'Leave time';

//V

//W
$Lang['eClassApp']['Warning']['CannotSignBecauseNoAuthCode'] = 'You cannot sign this notice because this device is not authorized.';
$Lang['eClassApp']['Warning']['EmptyTitle'] = 'Please fill in the message title';
$Lang['eClassApp']['Warning']['EmptyContent'] = 'Please fill in the message content';
$Lang['eClassApp']['Warning']['NoRecipient'] = 'Please select at least one recipient';
$Lang['eClassApp']['Warning']['PleaseSelectAtLeastOneOption'] = 'Please select at least one option';

$Lang['eClassApp']['WebModules']['DeleteConfirm']= 'Delete Confirm';
$Lang['eClassApp']['WebModules']['DeleteConfirm2'] = 'Are you sure to delete this record?';
$Lang['eClassApp']['WebModules']['Delete'] = 'Delete';
$Lang['eClassApp']['WebModules']['Close'] = 'Close';

//X


//Y


//Z

/*
if($sys_custom['DHL'] && file_exists($intranet_root.'/lang/DHL/dhl_lang.en.php'))
{
	include($intranet_root.'/lang/DHL/dhl_lang.en.php');
}
*/
if (isset($sys_custom['Project_Label']) && $sys_custom['Project_Alias']) {
	if (file_exists($intranet_root.'/lang/' . $sys_custom['Project_Label']. '/' . $sys_custom['Project_Alias'] . 'lang.en.php')) {
		include($intranet_root.'/lang/' . $sys_custom['Project_Label']. '/' . $sys_custom['Project_Alias'] . 'lang.en.php');
	}
}
if (!$NoLangWordings && is_file("$intranet_root/templates/lang.$intranet_session_language.customized.php"))
{
  include("$intranet_root/templates/lang.$intranet_session_language.customized.php");
}
?>