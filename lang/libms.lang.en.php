<?php
/*
* using by :
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/

### Save this file in utf-8 format ######

/******************************************* Changes log **********************************************
 *
 * IP 2.5 VERSION
 * 
 * Date: 2019-11-01 Tommy
 *       - added $Lang["libms"]["book"]["showOnlyBookCoverRecord"]
 * 
 ******************************************************************************************************/


$Lang['libms']['library'] = "eLibrary <i>plus</i>";

$Lang['libms']['SQL']['UserNameFeild'] = "EnglishName";


#######################################################################//sqlfeild####
$Lang["libms"]["sql_field"]["Description"] = "DescriptionEn";
$Lang["libms"]["sql_field"]["User"]["name"] = "EnglishName";
#################################################
$Lang["libms"]["library"] = ($plugin['eLib_Lite']?"eLibrary":"eLibrary <i>plus</i>");
$Lang["libms"]["status"]["na"] = "N/A";
$Lang["libms"]["value_follows_parent"] = "(follow bibliography)";
$Lang["libms"]["general"]["ajaxError"] = "Error on AJAX call!";
$Lang["libms"]["general"]["ajaxReturnNothing"] = "Ajax call return nothing. Please try again. If problem persist, make sure the setting is complete / contact your administrator";
$Lang["libms"]["general"]["new"] = "New";
$Lang["libms"]["general"]["edit"] = "Edit";
$Lang["libms"]["general"]["del"] = "Delete";
$Lang["libms"]["general"]["auto"] = "AUTO";
$Lang["libms"]["general"]["customize"] = "Customize";
$Lang["libms"]["general"]["select"] = "Select";
$Lang["libms"]["general"]["selectfile"] = "Select file";
$Lang["libms"]['General']['warnSelectcsvFile'] = "Please choose a file to import";
$Lang["libms"]["general"]["chinese"] = "Chinese";
$Lang["libms"]["general"]["english"] = "English";
$Lang["libms"]["management"]["title"] = "Management";
$Lang['libms']['bookmanagement']['available_reserve'] = "Available Reservation";
$Lang["libms"]["bookmanagement"]["book_list"] = "Bibliography";
$Lang['libms']['bookmanagement']['item_list'] = "Item";
$Lang["libms"]["bookmanagement"]["book_import"] = "Import";
$Lang["libms"]["bookmanagement"]["book_label"] = "Label";
$Lang["libms"]["bookmanagement"]["book_labelformat"] = "Label Paper";
$Lang['libms']['bookmanagement']['book_reserve'] = "Current Reservation";
$Lang['libms']['bookmanagement']['book_details'] = "Book Details";
$Lang['libms']['bookmanagement']['circulation_details'] = "Circulations";
$Lang['libms']['bookmanagement']['purchase_details'] = "Purchases";
$Lang['libms']['bookmanagement']['ebook_tags'] = "eBook Tags";
$Lang['libms']['bookmanagement']['ebook'] = "eBook";
$Lang['libms']['bookmanagement']['expired_book_reserve'] = "Expired Reservation";
$Lang['libms']['bookmanagement']['Advanced'] = "Advanced";
$Lang["libms"]["bookmanagement"]["follow_parent"] = "(follow bibliography)";
$Lang["libms"]["bookmanagement"]["unclassified"] = "Unclassified";
$Lang["libms"]["bookmanagement"]["unnumbered"] = "Unnumbered";
$Lang['libms']['bookmanagement']['ebook_license_quotation_error'] = "There is error when retrieving quotation info";
$Lang['libms']['bookmanagement']['import_ebook_tags'] = "Import eBook Tags";
$Lang['libms']['bookmanagement']['import_ebook_tags_error_add'] = "Error in adding tags";
$Lang['libms']['bookmanagement']['import_ebook_tags_error_no_book'] = "Book not found";
$Lang['libms']['bookmanagement']['SelectItemToExport'] = "Select fields to be included in export book file";
$Lang['libms']['bookmanagement']['ReturnLostBook'] = "Return Lost Book";
$Lang['libms']['bookmanagement']['show_or_hide'] = "show/hide";
$Lang["libms"]["book_export_marc21"]="Export in MARC21";
$Lang["libms"]["book_export_without_writeoff"]="Export (exclude write-off books)";
$Lang['libms']['batch_edit']['menu'] = "Batch Edit";
$Lang['libms']['batch_edit']['edit_return_date'] = "Due Date";
$Lang['libms']['batch_edit']['edit_penal_sum'] = "Penalty Sum";
$Lang['libms']['batch_edit']['edit_book_info'] = "Book Info";
$Lang['libms']['batch_edit']['edit_tags'] = "Tags";
$Lang['libms']['batch_edit']['edit_subject'] = "Subject";
$Lang['libms']['import_book_cover'] = "Import Book Cover";
$Lang['libms']['batch_edit']['edit_books'] = "Book Details";
$Lang['libms']['batch_edit']['edit_items'] = "Book Item Details";
$Lang['libms']['batch_edit']['edit_recommend'] = "Recommend Books";
$Lang['libms']['batch_edit']['edit_book_info_remark'] = "leave it blank for no change";

$Lang['libms']['batch_edit']['edit_return_date_same'] = "Change to a particular date";
$Lang['libms']['batch_edit']['edit_return_date_defer'] = "Defer for";
$Lang['libms']['batch_edit']['edit_return_date_early'] = "To advance of";
$Lang['libms']['batch_edit']['record_found'] = " %s loan record(s) have/has been found,";
$Lang['libms']['batch_edit']['modify_due_date'] = "Change the Due Date to";
$Lang["libms"]["batch_edit"]["Day"] = "Day(s)";
$Lang["libms"]["batch_edit"]["send_email"] = "Send Notification Email";
$Lang["libms"]["batch_edit"]["send_email_yes"] = "Yes";
$Lang["libms"]["batch_edit"]["send_email_no"] = "No";
$Lang["libms"]["batch_edit"]["DueDate"] = "Due Date";
$Lang["libms"]["batch_edit"]["Bak_DueDate"] = "Original Due Date";
$Lang['libms']['batch_edit']['record_updated'] = " %s loan record(s) have/has been updated";
$Lang['libms']['batch_edit']['PleaseSelectGroup'] = "Please select at least one group";
$Lang['libms']['batch_edit']['PleaseSelectClass'] = "Please select at least one class";
$Lang['libms']['batch_edit']['PleaseSelectDateAfterToday'] = "Please enter the date on or after today";

$Lang['libms']['import_format'] = "Data Format";
$Lang['libms']['import_marc21'] = "MARC 21";
$Lang['libms']['import_CSV'] = "CSV";
$Lang['libms']['import_TXT'] = "TXT";


$Lang['libms']['import_book_cover_file'] = "Cover Image";
$Lang['libms']['import_book_cover_file_types'] = "(jpg / png / zip)";
$Lang['libms']['import_book_cover_file_remark'] = "1. Image filename must be the book's ISBN or ACNO (e.g. bok002138.jpg).<br> 
2. To upload multiple files at one time, please use zip.<br>
3. Please do not include any folders inside the zip file.";
$Lang['libms']['import_book_cover_file_name'] = "Image filename";
$Lang['libms']['select_image_file'] = "Select JPG/PNG/ZIP file";
$Lang['libms']['invalid_acno'] = "ACNO is not found";
$Lang['libms']['invalid_isbn'] = "ISBN is not found";
$Lang['libms']['correct'] = "Correct";
$Lang['libms']['invalid_book_cover_file_types'] = "Please select Image(.jpg,.png) or Zip(.zip) file";

## Stock Take
$Lang['libms']['stocktake_status']['FOUND'] = "Found";
$Lang['libms']['stocktake_status']['WRITEOFF'] = "Write-off";
$Lang['libms']['stocktake_status']['NOTTAKE'] = "Not Take";
$Lang['libms']['stocktake_status_bookfound']['BookFound'] = "Book Found";	// don't put it in stocktake_status array as the array is used for program loop
$Lang['libms']['bookmanagement']['ImportTXT'] = "TXT Scoure File";
$Lang['libms']['bookmanagement']['TXTFormat'] = "(.txt file)";
$Lang['libms']['bookmanagement']['LastFive'] = "Last Five Record";
$Lang['libms']['bookmanagement']['LastFour'] = "Last Four Record";
$Lang['libms']['bookmanagement']['LastThree'] = "Last Three Record";
$Lang['libms']['bookmanagement']['LastTwo'] = "Last Two Record";
$Lang['libms']['bookmanagement']['LastOne'] = "Last One Record";
$Lang['libms']['bookmanagement']['Relocate'] = "Relocate";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample'] = "Click here to download sample";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample1'] = "Click here to download sample1";
$Lang['libms']['bookmanagement']['ClickHereToDownloadSample2'] = "Click here to download sample2";

$Lang['libms']['bookmanagement']['Reason'] = "Reason";
$Lang['libms']['bookmanagement']['WriteOffDate'] = "Write-off Date";
$Lang["libms"]["bookmanagement"]["ImportStockTake"] = array("<font color = red>*</font>Barcode","New Location Code");
$Lang["libms"]["bookmanagement"]["ImportWriteOff"] = array("<font color = red>*</font>Barcode","Write-off Date","Reason");
$Lang["libms"]["bookmanagement"]["BarcodeDuplicate"] = "Barcode is duplicated";
$Lang["libms"]["bookmanagement"]["BarcodeNotExist"] = "Barcode does not exist";
$Lang["libms"]["bookmanagement"]["import"] = "Import";
//$Lang["libms"]["bookmanagement"]["ImportBorrowRecord"] = array("<font color = red>*</font>User Barcode","<font color = red>*</font>Book Barcode","<font color = red>*</font>Borrow Date", "<font color = red>*</font>Borrow Time","<font color = red>*</font>Due Date","<font color = red>*</font>Renewal Time","Returned Date","Returned Time","<font color = red>*</font>Record Status");
$Lang["libms"]["bookmanagement"]["ImportBorrowRecord"] = array("<font color = red>*</font>ACNO","<font color = red>*</font>OUTDATE","<font color = red>*</font>OUTTIME", "<font color = red>*</font>DUEDATE","RTNDATE","<font color = red>*</font>STATUS","<font color = red>*</font>FINE","ENAME","CNAME","<font color = red>*</font>USERBARCODE","CLASSNAME","CLASSNUMBER");
$Lang['libms']["bookmanagement"]['ConfirmToTerminate']= "Are you sure you want to terminate?";
$Lang["libms"]["bookmanagement"]['SendingTo1']= "Adding from ";
$Lang["libms"]["bookmanagement"]['SendingTo2']= " records now ... ";

$Lang['libms']['bookmanagement']['stocktakeAndWriteOff'] = "Stock-take & Write-off";
$Lang['libms']['bookmanagement']['stock-take'] = "Stock-take";
$Lang['libms']['bookmanagement']['stock-takeProcess'] = "Stock-take Process";
$Lang['libms']['bookmanagement']['stocktakeRecord'] = "Stock-take Record";
$Lang['libms']['bookmanagement']['writeoffRecord'] = "Write-off Record";
$Lang['libms']['bookmanagement']['location'] = "Location";
$Lang['libms']['bookmanagement']['newLocationCode'] = "New Location Code";
$Lang['libms']['bookmanagement']['total'] = "Total";
$Lang['libms']['bookmanagement']['progress'] = "Progress";
$Lang['libms']['bookmanagement']['lastRecord'] = "Last record";
$Lang['libms']['bookmanagement']['lastTakenBy'] = "Last taken-by";
$Lang['libms']['bookmanagement']['overall'] = "Overall";

$Lang['libms']['bookmanagement']['stocktakeList'] = "Stock-take List";
$Lang['libms']['bookmanagement']['barcode'] = "Barcode";
$Lang['libms']['bookmanagement']['bookTitle'] = "Book Title";
$Lang['libms']['bookmanagement']['bookStatus'] = "Book Status";
$Lang['libms']['bookmanagement']['bookStatusIsLost'] = "This is a lost book";
$Lang['libms']['bookmanagement']['result'] = "Result";
$Lang['libms']['bookmanagement']['originalLocation'] = "Original Location";
$Lang['libms']['bookmanagement']['ItemLocation'] = "Item Location";
$Lang['libms']['bookmanagement']['ChangeItemLocationWarning'] = "Do you want to change item location to ";
$Lang['libms']['bookmanagement']['relocate'] = "Relocate item to here";
$Lang['libms']['bookmanagement']['Stock-takeTime'] = "Stock-take Time";
$Lang['libms']['bookmanagement']['EnterOrScanItemBarcode'] = "Enter or scan item barcode";
$Lang['libms']['bookmanagement']['BookLocationUpdateFailed'] = "Book location update failed";
$Lang['libms']['bookmanagement']['BookLocationUpdateSuccessfully'] = "Book Location update successfully";
$Lang['libms']['bookmanagement']['BookLocationUpdated'] = "Updated";
$Lang['libms']['bookmanagement']['SelectAnotherLocation'] = "Select another location";
$Lang['libms']['bookmanagement']['StocktakeStatus'] = "Stock-take Status"; 
$Lang['libms']['bookmanagement']['scope'] = "Scope"; 
$Lang['libms']['bookmanagement']['allBooks'] = "All Books";
$Lang['libms']['bookmanagement']['alleBooks'] = "All eBooks";
$Lang['libms']['bookmanagement']['showneBooks'] = "Show";
$Lang['libms']['bookmanagement']['hiddeneBooks'] = "Hide";
$Lang['libms']['bookmanagement']['selectedBooks'] = "Selected Books";
$Lang['libms']['bookmanagement']['lastStockTake'] = "Last stock-take";
$Lang['libms']['bookmanagement']['writeOff'] = "Write-off";
$Lang['libms']['bookmanagement']['ConfirmWriteOff'] = "Do you confirm to write-off the book(s)?";
$Lang['libms']['bookmanagement']['cancelWriteOff'] = "Cancel Write-off";
$Lang['libms']['bookmanagement']['WriteOffBy'] = "Write-off By";
$Lang['libms']['bookmanagement']['WriteOffTime'] = "Write-off Time";
$Lang['libms']['bookmanagement']['totalBorrow'] = "Loans";
$Lang['libms']['bookmanagement']['last_borrow_time'] = "Last Loan Date";
$Lang['libms']['bookmanagement']['last_borrow_user'] = "Last Loaned By";
$Lang['libms']['bookmanagement']['ClassName'] = "Class";
$Lang['libms']['bookmanagement']['ClassNumber'] = "Class Number";
$Lang['libms']['bookmanagement']['LoanDate'] = "Loan Date";
$Lang['libms']['bookmanagement']['DueDate'] = "Due Date";
$Lang['libms']['bookmanagement']['ReturnDate'] = "Return Date";
$Lang['libms']['bookmanagement']['deleted_user_legend'] = '<font style="color:red;">*</font> Representing that the person has been deleted or left already.';
$Lang['libms']['bookmanagement']['SearchHelp'] = '<ol><li>type a keyword to search on ACNO, Barcode, Book title, Book title 2, Call number, Call number 2, ISBN, ISBN 2, Publisher, Responsibility by, Responsibility by 2, Responsibility by 3 and Series</li><li>use quotes on a ACNO to search a book with the exact ACNO, e.g. "B100238"</li><li>use "~" to search books within an ACNO range , e.g. B100231~B100250</li><li>use "^" to search books having max ACNO for specific prefix, e.g. C10^</li></ol>';
$Lang['libms']['bookmanagement']['BookItemSearchHelp'] = '<ol><li>type a keyword to search on ACNO, Barcode, Book title, Distributor and Publisher</li><li>use quotes on a ACNO to search a book with the exact ACNO, e.g. "B100238"</li><li>use "~" to search books within an ACNO range , e.g. B100231~B100250</li><li>use "^" to search books having max ACNO for specific prefix, e.g. C10^</li></ol>';
$Lang['libms']['bookmanagement']['NoLocation'] = 'No Location';
$Lang['libms']['bookmanagement']['IsWriteOffBook'] = "This book cannot be stock-taken since the book status is Write-off.";
$Lang['libms']['bookmanagement']['ConfirmSuspectedMissing'] = "Do you confirm to the book status to  Suspected Missing?";
$Lang['libms']['bookmanagement']['StocktakeConductedDuringPeriod'] = "Stocktake can only be conducted during stocktake period";
$Lang['libms']['bookmanagement']['ItemNotForThisStocktake'] = "This book cannot be stock-taken since it is not included in this stocktake period.";
$Lang['libms']['bookmanagement']['ConfirmDeleteReservationAndAssignNext'] = "Are you sure you want to delete the selected items and change the earliest booking in waiting list to ready?";
$Lang['libms']['bookmanagement']['NextReserve'] = "Next reserve: ";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['DuplicateBarcode'] = "This barcode has been inputed/scanned";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['ConfirmChangeStocktakeName'] = "Change the name of stock-take period will cause loss of barcode being inputed/scanned,are you sure?";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['IsWriteOffBook'] = "This book has been written-off and does not need to redo!";
$Lang['libms']['bookmanagement']['writeOffByBarcode']['WriteOffByBarcode'] = "Write-off by barcode";
$Lang['libms']['bookmanagement']['AddFromExistingItem'] = "Add from existing item";
$Lang['libms']['bookmanagement']['AddToThisBook'] = "Add to this book";
$Lang['libms']['bookmanagement']['warning_add_to_this_book_confirm'] = "Are you sure you want to add to this book?";
$Lang['libms']['bookmanagement']['duplicate_user_barcode'] = "User BarCode is duplicated!";
$Lang['libms']['bookmanagement']['user_missing'] = "User is missing!";
$Lang['libms']['bookmanagement']['barcode_missing'] = "BarCode is missing!";
$Lang['libms']['bookmanagement']['purchased'] = "Purchased";
$Lang['libms']['bookmanagement']['expiry_date'] = "Expiry date";
$Lang['libms']['bookmanagement']['exportInvalidRecord'] = "Export invalid record";
$Lang['libms']['bookmanagement']['ConfirmReturnLostBook'] = "Are you sure to return this lost book";
$Lang['libms']['bookmanagement']['ReturnLostBookRemarkWiePayment'] = "Please undo payment for corresponding record (including overdue and lost book) in ePayment module before return the book(s)";
$Lang['libms']['bookmanagement']['ReturnLostBookRemarkWoePayment'] = "System will cancel the related records (including overdue and lost book) if selected records have been settled. New overdue record will be created (if any) after return book, please go to circulation page to settle the overdue record";
$Lang['libms']['bookmanagement']['ConfirmShoweBook'] = "Are you sure to show selected eBooks?";
$Lang['libms']['bookmanagement']['ConfirmHideeBook'] = "Are you sure to hide selected eBooks?";
$Lang['libms']['settings']['stocktake']['from']= "from";
$Lang['libms']['settings']['stocktake']['to']= "to";
$Lang['libms']['settings']['stocktake']['LastUpdated'] = "Last Updated";
$Lang['libms']['reporting']['date'] = "Date:";
$Lang['libms']['reporting']['dateRange'] = "Date Range";
$Lang['libms']['reporting']['stocktakeReport'] = "Stock-take Report";
$Lang['libms']['reporting']['stocktakeSummary'] = "Stock-take Summary";
$Lang['libms']['reporting']['progressDateRangeRemarks'] = "For Use of Library Resources and Penalty only";

$Lang['libms']['settings']['stocktake']['recordStartDate']= "Write-off cut-off date";
$Lang['libms']['settings']['stocktake']['recordEndDate']= "Accession cut-off date";
$Lang['libms']['settings']['stocktake']['stocktakeDate']= "Stock-take Date";
$Lang['libms']['settings']['stocktake']['itemRecordDate']= "Record Date";
$Lang['libms']['settings']['stocktake']['stocktakeDateRemarks'] = 'Open Stocktake function within the dates';
$Lang['libms']['settings']['stocktake']['itemRecordDateRemarks'] = 'Records within the dates will count in report';
$Lang['libms']['settings']['stocktake']['name'] = 'Name of Stock-take Period';
$Lang['libms']['settings']['stocktake']['inputname'] = 'Please enter Name of Stock-take Period.';
$Lang['libms']['settings']['stocktake']['recordStartDateRemarks'] = "Stock-take report will not count the items written-off before that date";
$Lang['libms']['settings']['stocktake']['recordEndDateRemarks'] = "No stock-take is required for items registered after that date";
### Stock Take End


$Lang['libms']["settings"]['notification']['title'] = "Notification";
$Lang['libms']["settings"]['notification']['reserve_email'] = "Notify user by email when reserved book is available";
$Lang['libms']["settings"]['notification']['overdue_email'] = "Notify user by email on due date";
$Lang['libms']["settings"]['notification']['due_date_reminder_email'] = "Notify user by email before due date";
$Lang['libms']["settings"]['notification']['yes'] = "Yes";
$Lang['libms']["settings"]['notification']['no'] = "No";
$Lang['libms']["settings"]['notification']['push_message_overdue'] = "Notify user by push message on due date (eClass App)";
$Lang['libms']["settings"]['notification']['push_message_due_date'] = "Notify user by push message before due date (eClass App)";
$Lang['libms']["settings"]['system']['push_message_due_date_reminder'] = "Notify user by push message [ ] day(s) before Due Date (eClass App)";
$Lang['libms']["settings"]['system']['push_message_time'] = "Time of push message (eClass App)";


$Lang['libms']["settings"]['system']['yes'] = "Yes";
$Lang['libms']["settings"]['system']['no'] = "No";
$Lang['libms']["settings"]['system']['max_overdue_limit'] = "Overdue Grace Period( -1 = No Limit ) ";
$Lang['libms']["settings"]['system']['override_password'] = "Override Password";
$Lang['libms']["settings"]['system']['global_lockout'] = "Global Lockout";
$Lang['libms']["settings"]['system']['batch_return_handle_overdue'] = "Handle Overdue loans at Batch Book Return";
$Lang['libms']["settings"]['system']['show_number_of_loans_when_borrow_book'] = "show loan times of the borrower at circulation";
$Lang['libms']["settings"]['system']['leave_user_after_borrow_book'] = "leave user after borrow book(s)";
$Lang['libms']["settings"]['system']['max_book_reserved_limit'] = "Reservation valid period (0 means unlimited)";
$Lang['libms']["settings"]['system']['allow_reserve_available_books_in_portal'] = "Allow users to reserve available books";
$Lang['libms']["settings"]['system']['school_display_name'] = "Display Name of the School (CHI)";
$Lang['libms']["settings"]['system']['school_display_name_eng'] = "Display Name of the School (ENG)";
$Lang['libms']["settings"]['system']['due_date_reminder'] = "Notify user by email [  ] day(s) before Due Date";
$Lang['libms']["settings"]['system']['circulation_student_by_selection'] = "Select a student from class and class number list at circulation";
$Lang['libms']["settings"]['system']['circulation_duedate_method'] = "Counting school days only to calculate due date (\"No\" means holidays and non-open days will be counted)";

$Lang['libms']["settings"]['system']['circulation_duedate_method'] = "Due-date calculation"; //"以學校日子來計算還書日期（「否」代表計算假期及不開放的日子）";
$Lang['libms']["settings"]['system']['circulation_duedate_method_yes'] = "counting on opening days only";
$Lang['libms']["settings"]['system']['circulation_duedate_method_no'] = "counting on all days";
$Lang['libms']["settings"]['system']['general'] = "General";
$Lang['libms']["settings"]['system']['password_setting'] = "Password Setting";
$Lang['libms']["settings"]['system']['book_loan_setting'] = "Book Loan Setting";
$Lang['libms']["settings"]['system']['book_return_setting'] = "Book Return Setting";
$Lang['libms']["settings"]['system']['book_renew_setting'] = "Book Renew Setting";
$Lang['libms']["settings"]['system']['book_reserve_setting'] = "Book Reserve Setting";
$Lang['libms']["settings"]['system']['circulation_setting'] = "Circulation Setting";
$Lang['libms']["settings"]['system']['book_review_setting'] = "Book Review Setting";
$Lang['libms']["settings"]['system']['preference_setting'] = "Preference Setting";
$Lang['libms']["settings_remark"]['max_overdue_limit'] = "Cannot borrow book after the day";

$Lang['libms']["settings"]['system']['batch_book_renew_by_confirm'] = "Renew book confirmation";
$Lang['libms']["settings"]['system']['batch_book_renew_by_confirm_yes'] = "Need confirmation";
$Lang['libms']["settings"]['system']['batch_book_renew_by_confirm_no'] = "Scan barcode only";
$Lang['libms']["settings"]['system']['book_review_allow_edit_own_book_review'] = "Allow students to edit their own book reviews";
$Lang['libms']["settings"]['system']['circulation_return_book_by_confirm'] = "Return book confirmation";
$Lang['libms']["settings"]['system']['circulation_return_book_by_confirm_yes'] = "Need confirmation";
$Lang['libms']["settings"]['system']['circulation_return_book_by_confirm_no'] = "Scan barcode only";
$Lang['libms']["settings"]['system']['circulation_borrow_return_accompany_material'] = "Borrow/Return library materials with Accompany Material";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by'] = "Penalty of lost book basis";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_purchase'] = "Purchase Price";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_list'] = "List Price";
$Lang['libms']["settings"]['system']['circulation_lost_book_price_by_fixed'] = "Fixed Price";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge'] = "Additional charge for lost book";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_fixed'] = "By fixed amount";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_by_ratio'] = "By proportional of <span id='ratio_based'></span>";
$Lang['libms']["settings"]['system']['circulation_lost_book_additional_charge_none'] = "No";
$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty'] = "Allow to change penalty amount for lost book";
$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty_yes'] = "Allow (Need to input override password)";
$Lang['libms']["settings"]['system']['circulation_lost_book_allow_change_penalty_no'] = "Not allow";
$Lang['libms']["settings"]['system']['circulation_lost_book_overdue_charge'] = "Overdue charge for lost book";
$Lang['libms']["settings"]['system']['circulation_input_non_negative_value'] = "Please input non-negative value";
$Lang['libms']["settings"]['system']['notification_input_non_negative_integer'] = "Please input non-negative integer";
$Lang['libms']["settings"]['system']['preference_recommend_book_order'] = "Display order of recommended books";
$Lang['libms']["settings"]['system']['preference_recommend_book_order_yes'] = "Recommendation Date";
$Lang['libms']["settings"]['system']['preference_recommend_book_order_no'] = "Random";
$Lang['libms']["settings"]['system']['preference_photo_show'] = "Photos shown in Ranking, Book Review and My Record";
$Lang['libms']["settings"]['system']['preference_photo_show_yes'] = "Official Photo";
$Lang['libms']["settings"]['system']['preference_photo_show_no'] = "Personal Photo";
$Lang["libms"]["btn"]["enter"] = "Enter";

$Lang['libms']["settings"]['system']['overdue_epayment'] = "Fine Payment Method";

$Lang['libms']["settings"]['system']['overdue_epayment_a'] = "Cash";
$Lang['libms']["settings"]['system']['overdue_epayment_b'] = "via ePayment";


$Lang['libms']["settings_remark"]['global_lockout'] = "Only the administrator can access and use this module if Global Lockout is set";
$Lang['libms']["settings_alert"]['batch_return_handle_overdue'] = "Overdue return is disallowed here! <br />Please use <u>Circulation for a Patron</u>.";
$Lang['libms']["settings_alert"]['batch_return_handle_overdue_fail'] = "Failed";


$Lang["libms"]["settings"]["title"] = "Settings";
$Lang["libms"]["settings"]["basic"] = "Basic Settings";
$Lang["libms"]["settings"]["holidays"] = "Holiday";
$Lang["libms"]["settings"]["opentime"] = "Open Hour";
$Lang["libms"]["settings"]["PageSettingsSpecialOpenTime"] = "Special Time";
$Lang["libms"]["settings"]["circulation_type"] = "Circulation Type";
$Lang["libms"]["settings"]["resources_type"] = "Resources Type";
$Lang["libms"]["settings"]["book_category"] = "Book Category";
$Lang["libms"]["settings"]["book_category_type"] = "Book Category Type";
$Lang["libms"]["settings"]["book_category_name"] = "Book Category Name";
$Lang["libms"]["settings"]["book_location"] = "Location";
$Lang["libms"]["settings"]["responsibility"] = "Responsibility";
$Lang["libms"]["settings"]["stock-take"] = "Stock-take";
$Lang["libms"]["settings"]["book_language"] = "Book Language";
$Lang["libms"]["settings"]["msg"]["cannot_delete_used_code"] = "Cannot delete code that's already been used!";
$Lang["libms"]["settings"]["symbols"] = "!\"#$%&\'*,/:;<=>?@^`|~";
$Lang["libms"]["settings"]["msg"]["exclude_symbols"] = "Please exclude these symbols in code field ".$Lang["libms"]["settings"]["symbols"];
$Lang['libms']['settings']['msg']['duplicated'] = 'Duplicated Code';
$Lang['libms']['settings']['security']['title'] = "Security Settings";
$Lang['libms']['settings']['security']['circulation_terminals'] = "Circulations Terminal";
$Lang['libms']['settings']['security']['InstructionTitle'] = 'Instruction';
$Lang['libms']['settings']['security']['InstructionContent'] = 'In IP Address box, you can input:
		<ol>
			<li>Exact IP Address (e.g. 192.168.0.101)</li>
			<li>IP Address Range (e.g. 192.168.0.[10-100])</li>
			<li>CISCO Style Subnet Range (e.g. 192.168.0.0/24)</li>
			<li>Allow All incoming IP address (Input 0.0.0.0)</li>
		</ol>
		<font color="red">System suggests you should set that machine in FIXED IP address (i.e. Not use DHCP), in order to ensure data transmission source is legitimate.</font>
		<br>
		This System has security policy to prevent Faked IP Address attack.';
$Lang['libms']['settings']['security']['SettingTitle'] = 'IP address';
$Lang['libms']['settings']['security']['SettingControlTitle'] = 'Please input the IP address in each line<br />Your Current IP Address';
$Lang["libms"]["reporting"]["title"] = "Reports";
$Lang["libms"]["reporting"]["span"]['table'] = "Table";
$Lang["libms"]["reporting"]["span"]['graph'] = "Graph";
$Lang["libms"]["reporting"]["PageReportingOverdue"] = "Overdue Notice";
$Lang["libms"]["reporting"]["PageReportingFine"] = "Penalty Report";
$Lang["libms"]["reporting"]["PageReportingCirculation"] = "Circulation Report";
$Lang["libms"]["reporting"]["PageReportingReaderRanking"] = "Patron Ranking";
$Lang["libms"]["reporting"]["PageReportingReaderInactive"] = "Inactive Patron";
$Lang["libms"]["reporting"]["PageReportingBookRanking"] = "Book Ranking";

$Lang["libms"]["statistics"]["title"] = "Statistics";
$Lang["libms"]["reporting"]["PageReportingIntragration"] = "Summary";
$Lang["libms"]["reporting"]["PageReportingLending"] = "General Stats";
$Lang["libms"]["reporting"]["PageReportingBookReport"] = "Book Report";
$Lang['libms']['reporting']['PageReportingLostBookReport'] = "Lost Report";
$Lang['libms']['reporting']['PageReportingPenaltyReport'] = "Penalty Stats";
$Lang['libms']['reporting']['PageReportingCategoryReport'] = "Category vs Class/Form";
$Lang['libms']['reporting']['PageReportingCategoryReport1'] = "Statistics (by book category)";
$Lang['libms']['reporting']['PageReportingCategoryReport2'] = "Statistics (by subject)";
$Lang['libms']['reporting']['PageReportingCategoryReport3'] = "Statistics (by language)";
$Lang["libms"]["reporting"]["ReadingFrequency"] = "Frequency by Form";
$Lang['libms']['reporting']['stock-take'] = "Stock-take Report";
$Lang['libms']['reporting']['accessionReport'] = "Accession Report";
$Lang['libms']['reporting']['countingMethod'] = "Counting method";
$Lang['libms']['reporting']['countingMethodByBooks'] = " Base on the number of materials borrowed";
$Lang['libms']['reporting']['countingMethodByVisit'] = "Base on the frequency of visiting the library to borrow materials";
$Lang['libms']['reporting']['countingMode'] = "Date mode";

$Lang['libms']['reporting']['overdue']['push_message']['title']="圖書過期通知 Library material(s) expired reminder";
$Lang['libms']['reporting']['overdue']['push_message']['content']="貴子弟所借的圖書館資料已到期，請儘快交還有關書籍，並繳交有關罰款。如你已交還有關圖書，請無需理會此通知。
		
				The following library material(s) which your child borrowed has not return before deadline. Please remind your child to return those books and pay for the penalty. Please ignore this notice if you have already returned.";
$Lang['libms']['reporting']['overdue']['push_message']['content_without_penalty']="貴子弟所借的圖書館資料已到期，請儘快交還有關書籍。如你已交還有關圖書，請無需理會此通知。
		
				The following library material(s) which your child borrowed has not return before deadline. Please remind your child to return those books. Please ignore this notice if you have already returned.";
$Lang['libms']['daily']['push_message']['return_date']['title']="圖書到期提示通知 Library material(s) due date reminder";
$Lang['libms']['daily']['push_message']['return_date']['content']="貴子弟所借的圖書館資料快將到期，請按期交還。如你已交還有關圖書，請無需理會此通知。

The following library material(s) which you borrowed will be due for return soon. Please return it/them on time. Please ignore this notice if you have already returned.";
$Lang['libms']['daily']['push_message']['overdue']['title']="圖書到期通知 Library material(s) due date reminder";
$Lang['libms']['daily']['push_message']['overdue']['content']="貴子弟所借的圖書館資料已到期，請今天內交還。如你已交還有關圖書，請無需理會此通知。

The following library material(s) which you borrowed will be due today. Please return it/them today. Please ignore this notice if you have already returned.";

$Lang["libms"]["action"]["book"] = "Book Management";
$Lang["libms"]["action"]["new_book"] = "New book";
$Lang["libms"]["action"]["edit_book"] = "Edit book";

$Lang["libms"]["action"]["item"] = "Item Management";
$Lang["libms"]["action"]["new_item"] = "New Item";
$Lang["libms"]["action"]["edit_item"] = "Edit Item";

$Lang["libms"]["action"]["periodical"] = "Periodical Management";

$Lang["libms"]["open_time"]["not_open_by_group"] = "<p><font color='red' face='simhei'>Not allow to loan now!</font></p> This user can loan ";
$Lang["libms"]["open_time"]["not_open_yet"] = "<p><font color='red' face='simhei'>Not allow to loan!</font></p> Loan service is available ";
$Lang["libms"]["open_time"]["system_period"] = "Period (school year or term)";
$Lang["libms"]["open_time"]["system_period_remark"] = "If set, start date and last return date of circulations will be controlled. System will adopt this setting and ignore the system Period (school year or term) which is set under Settings -> Open Hour.";
$Lang["libms"]["open_time"]["system_period_remark2"] = "If set, start date and last return date of circulations will be controlled. It should be updated in every academic school year.";
$Lang["libms"]["open_time"]["system_period_open"] = "starting on";
$Lang["libms"]["open_time"]["system_period_end"] = "till";
$Lang["libms"]["open_time"]["system_period_full"] = "";  # no word is needed in English version!


$Lang["libms"]["open_time"]["weekday"] = "Open Hours";
$Lang["libms"]["open_time"]["opentime"] = "From";
$Lang["libms"]["open_time"]["closetime"] = "To";
$Lang["libms"]["open_time"]["day1"] = "Monday";
$Lang["libms"]["open_time"]["day2"] = "Tuesday";
$Lang["libms"]["open_time"]["day3"] = "Wednesday";
$Lang["libms"]["open_time"]["day4"] = "Thursday";
$Lang["libms"]["open_time"]["day5"] = "Friday";
$Lang["libms"]["open_time"]["day6"] = "Saturday";
$Lang["libms"]["open_time"]["day7"] = "Sunday";
$Lang["libms"]["open_time"]["close"] = "Close";
$Lang["libms"]["open_time"]["open"] = "Open";
$Lang["libms"]["open_time"]["time_alertmsg"] = "24 hours format(HH:MM:SS)";
$Lang["libms"]["open_time"]["time_timemsg"] = "Invalid time format, please use 24 hours format(HH:MM:SS)!";
$Lang["libms"]["open_time"]["time_hourmsg"] = "Invalid hour";
$Lang["libms"]["open_time"]["time_minsmsg"] = "Invalid minute";
$Lang["libms"]["open_time"]["time_secmsg"] = "Invalid second";
$Lang["libms"]["open_time"]["time_rangemsg"] = "Open hour must be earlier than close hour.";
$Lang["libms"]["S_OpenTime"]["editall"] = "Batch edit";
$Lang["libms"]["S_OpenTime"]["Date"] = "Date (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["DateFrom"] = "Start date (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["DateEnd"] = "End date (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["Time"] = " Time (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["OpenTime"] = "Open hour (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["CloseTime"] = "Close hour (HH:MM:SS)";
$Lang["libms"]["S_OpenTime"]["close"] = "Close";
$Lang["libms"]["S_OpenTime"]["open"] = "Open";
$Lang["libms"]["S_OpenTime"]["dateformat"] = "Date format (YYYY-MM-DD)";
$Lang["libms"]["open_time"]["datemsg"] = "Invalid date format, please follow (YYYY-MM-DD)";
$Lang["libms"]["S_OpenTime"]["yearmsg"] = "Invalid year";
$Lang["libms"]["S_OpenTime"]["monthsmsg"] = "Invalid month";
$Lang["libms"]["S_OpenTime"]["daymsg"] = "Invalid day";
$Lang["libms"]["S_OpenTime"]["remark"] = "<span class='tabletextrequire'>*</span> For special open/close setting, please go to 'Special Time'.";
$Lang["libms"]["open_time"]["time_alertmsg"] = "24 hours format(HH:MM:SS)";
$Lang["libms"]["holiday"]["Event"] = "Holiday";
$Lang["libms"]["holiday"]["DateFrom"] = "Start date";
$Lang["libms"]["holiday"]["DateEnd"] = "End date";
$Lang['libms']['holiday']['disclaimer'] = "<span class='tabletextrequire'>*</span> Refering to School Settings > School Calendar > Holidays, please contact eClass System Admin should you need further assistance.";
$Lang['libms']['holiday']['disclaimer_ej'] = "<span class='tabletextrequire'>*</span> Refering to eClass Administrative Console > Intranet > Admin Mgmt > School Calendar > Holidays, please contact eClass System Admin should you need further assistance.";
$Lang["libms"]["portal"]["book_total"] = "book(s)";
$Lang["libms"]["circulation_type"]["code"] = "Circulation Code";
$Lang["libms"]["circulation_type"]["description_en"] = "Title in English";
$Lang["libms"]["circulation_type"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["resources_type"]["code"] = "Resources Code";
$Lang["libms"]["resources_type"]["description_en"] = "Title in English";
$Lang["libms"]["resources_type"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["book_category"]["code"] = "Category Code";
$Lang["libms"]["book_category"]["description_en"] = "Title in English";
$Lang["libms"]["book_category"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["book_location"]["code"] = "Location Code";
$Lang["libms"]["book_location"]["description_en"] = "Title in English";
$Lang["libms"]["book_location"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["responsibility"]["code"] = "Responsibility Code";
$Lang["libms"]["responsibility"]["description_en"] = "Title in English";
$Lang["libms"]["responsibility"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["book_language"]["code"] = "Language Code";
$Lang["libms"]["book_language"]["description_en"] = "Title in English";
$Lang["libms"]["book_language"]["description_b5"] = "Title in Chinese";
$Lang["libms"]["book_language"]["type"] = "Category Type";
$Lang["libms"]["book"]["info"] = "Basic information";
$Lang["libms"]["book"]["info_category"] = "Categorization";
$Lang["libms"]["book"]["info_circulation"] = "Circulation";
$Lang["libms"]["book"]["info_publish"] = "Publishing details";
$Lang["libms"]["book"]["info_purchase"] = "Purchase details";
$Lang["libms"]["book"]["info_other"] = "Miscellaneous";
$Lang["libms"]["book"]["info_book"] = "Barcode";
$Lang["libms"]["book"]["code"] = "ACNO";
$Lang["libms"]["book"]["title"] = "Book title";
$Lang["libms"]["book"]["subtitle"] = "Book title 2";
$Lang["libms"]["book"]["introduction"] = "Description";
$Lang["libms"]["book"]["call_number"] = "Call number";
$Lang["libms"]["book"]["call_number_desc"] = "Classification Code";
$Lang["libms"]["book"]["call_number2"] = "Call number 2";
$Lang["libms"]["book"]["call_number2_desc"] = "Author Code";
$Lang["libms"]["book"]["ISBN"] = "ISBN";
$Lang["libms"]["book"]["ISBN2"] = "ISBN 2";
$Lang["libms"]["book"]["barcode"] = "Barcode";
$Lang["libms"]["book"]["language"] = "Language";
$Lang["libms"]["book"]["country"] = "Country";
$Lang["libms"]["book"]["edition"] = "Edition";
$Lang["libms"]["book"]["publisher"] = "Publisher";
$Lang["libms"]["book"]["publish_place"] = "Publication place";
$Lang["libms"]["book"]["publish_year"] = "Year of publishing";
$Lang["libms"]["book"]["DIMEN"] = "Dimension";
$Lang["libms"]["book"]["ILL"] = "ILL";
$Lang["libms"]["book"]["AccompanyMaterial"] = "Accompany Material";
$Lang["libms"]["book"]["number_of_page"] = "Number of pages";
$Lang["libms"]["book"]["number_of_copy"] = "Number of copies";
$Lang["libms"]["book"]["number_of_copy_available"] = "Available";
$Lang["libms"]["book"]["book_status"] = "Status";
$Lang["libms"]["book"]["purchase_date"] = "Date of purchase";
$Lang["libms"]["book"]["distributor"] = "Distributor";
$Lang["libms"]["book"]["purchase_note"] = "Note of purchase";
$Lang["libms"]["book"]["purchase_by_department"] = "Purchased by department";
$Lang["libms"]["book"]["list_price"] = "List price";
$Lang["libms"]["book"]["discount"] = "Discount";
$Lang["libms"]["book"]["purchase_price"] = "Purchase price";
$Lang["libms"]["book"]["account_date"] = "Account date";
$Lang["libms"]["book"]["subject"] = "Subject";
$Lang["libms"]["book"]["series"] = "Series";
$Lang["libms"]["book"]["series_number"] = "Series number";
$Lang["libms"]["book"]["book_series_number"] = "Book series number";
$Lang["libms"]["book"]["item_series_number"] = "Item series number";
$Lang["libms"]["book"]["responsibility_code1"] = "Responsibility ";
$Lang["libms"]["book"]["responsibility_by1"] = "Responsibility by ";
$Lang["libms"]["book"]["responsibility_code2"] = "Responsibility 2";
$Lang["libms"]["book"]["responsibility_by2"] = "Responsibility by 2";
$Lang["libms"]["book"]["responsibility_code3"] = "Responsibility 3";
$Lang["libms"]["book"]["responsibility_by3"] = "Responsibility by 3";
$Lang["libms"]["book"]["remark_internal"] = "Remark (for administrator)";
$Lang["libms"]["book"]["remark_to_user"] = "Note for circulation";
$Lang["libms"]["book"]["URL"] = "Relevant link";
$Lang["libms"]["book"]["cover_image"] = "Book cover";
$Lang["libms"]["book"]["delete_cover_image"] = "Delete Book cover";
$Lang["libms"]["book"]["borrow_reserve"] = "Allow";
$Lang["libms"]["book"]["open_borrow"] = "Loan";
$Lang["libms"]["book"]["open_reserve"] = "Reserve";
$Lang["libms"]["book"]["tags"] = "Tags";
$Lang["libms"]["book"]["tag_books"] = "Books";
$Lang["libms"]["book"]["last_modified"] = "Last Modified";
$Lang["libms"]["book"]["multiple_selection"] = "Multiple";
$Lang["libms"]["book"]["book_total"] = "book(s)";
$Lang["libms"]["book"]["warning_no_new_tag"] = "Please input a new tag!";
$Lang["libms"]["book"]["warning_tag_edit_confirm"] = "Are you sure you want to make the change?";
$Lang["libms"]["book"]["title_origin"] = "Target tag(s)";
$Lang["libms"]["book"]["title_new"] = "New tag";
$Lang["libms"]["book"]["tags_input"] = "* please press [Enter] to seperate tags";
$Lang["libms"]["book"]["add"] = "Add copy";
$Lang["libms"]["book"]["delete"] = "Remove copy";
$Lang["libms"]["book"]["code_format"] = "must start with letters and end with numbers, e.g. \"FICTION000021\"";
$Lang["libms"]["book"]["lost_date"] = "Reported Date";
$Lang["libms"]["book"]["user_name"] = "Name";
$Lang["libms"]["book"]["class"] = "Class Number";
$Lang["libms"]["book"]["reserve_time"] = "Reservation Time";
$Lang["libms"]["book"]["reserve_sum"] = "Reservation Count";
$Lang['libms']['book']['invoice_number'] = "Invoice Number";
$Lang['libms']['book']['use_acno_as_barcode'] = "Use ACNO";
$Lang['libms']['book']['item_account_date'] = "Account Date";
$Lang['libms']['book']['view_item'] = "View Item";
$Lang['libms']['book']['all_cir_type'] = "All Circulation Type";
$Lang['libms']['book']['all_res_type'] = "All Resource Type";
$Lang["libms"]["book"]["alert"]["numeric"] = 'Please enter correct number';
$Lang["libms"]["book"]["alert"]["item_book_title"] = 'Book title is not correct';
$Lang["libms"]["book"]["alert"]["incorrect_anco"] = 'ACNO is not correct';
$Lang["libms"]["book"]["alert"]["fillin_anco"] = 'Please fill in ACNO';
$Lang["libms"]["book"]["setting"]["filter_submit"] = 'Apply';
$Lang['libms']['book']['select']['Book_Status'] = 'Book Status';
$Lang['libms']['book']['select']['BookCategoryCode'] = 'Book Category';
$Lang['libms']['book']['select']['ResourcesTypeCode'] = 'Resources Type';
$Lang['libms']['book']['select']['CirculationTypeCode'] = 'Circulation Type';
$Lang['libms']['book']['btn']['SubmitAndAddItem'] = "Submit & Add Item";
$Lang['libms']['book']['status']['remark'] = "represents the book is set to disallow loaning (no matter what the item status is). You may edit the book details to make change if necessary.";

$Lang['libms']['book']['reservation_disallowed'] = "Disallowed reservation";
$Lang['libms']['book']['borrow_disallowed'] = "Disallowed loan ";
$Lang['libms']['book']['updateBookStatusTo'] = "Update Book Status to";
$Lang['libms']['book']['expiredAndPassedToNext'] = "Expired and passed to next";
$Lang['libms']['book']['expired'] = "Expired";
$Lang['libms']['book']['allRecord'] = "All";
$Lang['libms']['book']['iteminfo'] = "Item Information";
$Lang["libms"]["book"]["item_remark_to_user"] = "Note for circulation (Item)";
$Lang["libms"]["book"]["importMarc21"] = "Add Marc21";
$Lang["libms"]["book"]["showOnlyBookCoverRecord"] = "Show book(s) which has a book cover";

$Lang["libms"]["label"]["labelformat_list"] = "Label paper";
$Lang["libms"]["label"]["name"] = "Template title";
$Lang["libms"]["label"]["paper-size-width"] = "Width of paper";
$Lang["libms"]["label"]["paper-size-height"] = "Height of paper";
$Lang["libms"]["label"]["NX"] = "Number of labels per row";
$Lang["libms"]["label"]["NY"] = "Number of labels per column";
$Lang["libms"]["label"]["metric"] = "Unit";
$Lang["libms"]["label"]["metric_inch"] = "inch";
$Lang["libms"]["label"]["metric_mm"] = "mm";
$Lang["libms"]["label"]["unit"]["in"] = "inch";
$Lang["libms"]["label"]["unit"]["mm"] = "mm";
$Lang["libms"]["label"]["text_alignment"] = "Text alignment";
$Lang["libms"]["label"]["text_alignment_option"]["L"] = "Left";
$Lang["libms"]["label"]["text_alignment_option"]["C"] = "Center";
$Lang["libms"]["label"]["text_alignment_option"]["R"] = "Right";
$Lang["libms"]["label"]["piece"] = "piece";
$Lang["libms"]["label"]["SpaceX"] = "Horizontal space between labels";
$Lang["libms"]["label"]["SpaceY"] = "Vertical space between labels";
$Lang["libms"]["label"]["width"] = "Width of label";
$Lang["libms"]["label"]["height"] = "Height of label";
$Lang["libms"]["label"]["font_size"] = "Font size";
$Lang["libms"]["label"]["is_bold"] = "Bold Font";
$Lang["libms"]["label"]["is_bold_remark"] = "(Apply to Spine Label only)";
$Lang["libms"]["label"]["printing_margin_h"] = "Left margin between label and text";
$Lang["libms"]["label"]["printing_margin_v"] = "Top margin between label and text";
$Lang["libms"]["label"]["max_barcode_width"] = "Width of barcode";
$Lang["libms"]["label"]["max_barcode_height"] = "Height of barcode";
$Lang["libms"]["label"]["lMargin"] = "Left margin between paper and label";
$Lang["libms"]["label"]["tMargin"] = "Top margin between paper and label";
$Lang["libms"]["label"]["input_msg"] = "Please input valid number, e.g.(10.75)";
$Lang["libms"]["label"]["lineHeight"] = "Line height";
$Lang["libms"]["label"]["info_order"] = "Order of label information (For Barcode Label and Spine Label only)";
$Lang["libms"]["label"]["BookTitle"] = "Book title";
$Lang["libms"]["label"]["LocationCode"] = "Location";
$Lang["libms"]["label"]["CallNumCallNum2"] = "Call number";
$Lang["libms"]["label"]["BarCode"] = "Barcode";
$Lang["libms"]["label"]["BookCode"] = "ACNO";
$Lang["libms"]["label"]["print_school_name"] = "School name";
$Lang["libms"]["label"]["msg"]["labelInfoError"] = "The order of the label information must not be equal with each other!";
$Lang["libms"]["label"]["msg"]["labelInfoErrorFillAll"] = "Please select all Order of label information! If no needed, select all the fields with \"NA\"!";
$Lang["libms"]["label"]["msg"]["no_template"] = "Please add at least one template in Label Paper!";
$Lang["libms"]["export_book"]["select_all_remark"] = "which can be imported back to the system";
$Lang['libms']['import']['msg']['error_required_missing']='missing required field. column:';
$Lang['libms']['import']['msg']['error_format_mismatch']='format mismatch. column:';
$Lang['libms']['import']['msg']['dup_key'] = 'Duplicated Code. column:';

$Lang["libms"]["import_book"]["file_format"] = "Import format - ISO2709";
$Lang["libms"]["import_book"]["file_charset"] = "File charact set";
$Lang["libms"]["import_book"]["BIG5"] = "BIG5";
$Lang["libms"]["import_book"]["BIG5HKSCS"] = "BIG5HKSCS";
$Lang["libms"]["import_book"]["UTF8"] = "UTF-8";
$Lang["libms"]["import_book"]["matched_by_acno"] = "mapped by ACNO";
$Lang["libms"]["import_book"]["total_record"] = "Number of entries ";
$Lang["libms"]["import_book"]["total_record_modified"] = "Number of entries updated ";
$Lang["libms"]["import_book"]["preview"] = "Preview";
$Lang["libms"]["import_book"]["title"] = "Title:";
$Lang["libms"]["import_book"]["author"] = "Author:";
$Lang["libms"]["import_book"]["upload_fail"] = "Filed to upload:";
$Lang["libms"]["import_book"]["contact_admin"] = ", please contact your administrator";
$Lang["libms"]["import_book"]["processing_db"] = "Import in progress:";
$Lang["libms"]["import_book"]["process_count"] = "Number of records import:";
$Lang["libms"]["import_book"]["upload_success_file_ready_to_import"] = "Data validated, number of files to import: ";
$Lang["libms"]["import_book"]["upload_success_ready_to_import"] = "Data validated, number of records to import: ";
$Lang["libms"]["import_book"]["upload_fail_ready_to_import"] = "Number of invalid records: ";
$Lang["libms"]["import_book"]["Result"]["Success"] = "Import Completed";
$Lang["libms"]["import_book"]["Result"]["Fail"] = "Import failed";
$Lang["libms"]["import_book"]["TypeInvalid"] = "Invalid type";
$Lang["libms"]["import_book"]["DataMissing"] = "Missing data";
$Lang["libms"]["import_book"]["BookCodeAlreadyExist"] = "same ACNO is used by current record";
$Lang["libms"]["import_book"]["ACNO_not_found"] = "ACNO doesn't exist";
$Lang["libms"]["import_book"]["ACNO_no_AUTO"] = "\"AUTO\" must not be used";
$Lang["libms"]["import_book"]["BarcodeAlreadyExist"] = "same barcode is used by current record";
$Lang["libms"]["import_book"]["BarcodeAlreadyBeUsed"] = "same barcode is used by other record";
$Lang["libms"]["import_book"]["BarcodeMissing"] = "barcode is missing";
$Lang["libms"]["import_book"]["BarcodeWrongFormat"] = "Barcode in wrong format";
$Lang["libms"]["import_book"]["AncoWrongFormat"] = "ACNO number in wrong format";
$Lang["libms"]["import_book"]["BookTitleEmpty"] = "Book title is empty";
$Lang["libms"]["import_book"]["ResponsibilityNotExist"] = "Responsibility code is not exist";
$Lang["libms"]["import_book"]["BookCategoryNotExist"] = "Book Category is not exist";
$Lang["libms"]["import_book"]["BookCirclationNotExist"] = "Book Circlation is not exist";
$Lang["libms"]["import_book"]["BookResourcesNotExist"] = "Book Resources is not exist";
$Lang["libms"]["import_book"]["BookLocationNotExist"] = "Book Location is not exist";
$Lang["libms"]["import_book"]["alreadyexist"] = "already exist.";
$Lang["libms"]["import_book"]["doubleused"] = "double used.";
$Lang["libms"]["import_book"]["CancelCheck"] = "Do you want to cancel check?";
$Lang["libms"]["import_book"]["AlreadyChecked"] = "Already checked: ";
$Lang["libms"]["import_book"]["StartToChecking"] = "Start to checking...";
$Lang["libms"]["import_book"]["CancelImport"] = "Do you want to cancel import?";
$Lang["libms"]["import_book"]["AlreadyImportedBooks"] = "Already imported books: ";
$Lang["libms"]["import_book"]["AlreadyImportedItems"] = "Already imported items: ";
$Lang["libms"]["import_book"]["StartToImporting"] = "Start to importing...";
$Lang["libms"]["import_book"]["CheckImport"] = "Check is completed, you can import now...";
$Lang["libms"]["import_book"]["ImportMarc21Alert"] = "Below is the book data read from your file. Please check and then continue.";
$Lang["libms"]["import_book"]["FirstBook"] = "The first book: ";
$Lang["libms"]["import_book"]["SecondBook"] = "The second book: ";
$Lang["libms"]["import_book"]["ThirdBook"] = "The third book: ";
$Lang["libms"]["import_book"]["ContinueImport"] = "Continue import";
$Lang["libms"]["import_book"]["ISBNWrongFormat"] = "ISBN number in wrong format";
$Lang["libms"]["import_book"]["ISBN2WrongFormat"] = "ISBN2 number in wrong format";
$Lang["libms"]["import_book"]["acnoautofill"] = "<span class='tabletextrequire'>@</span> If ACNO is empty, server will generate it automatically.<br /> <br /> <span class='tabletextrequire'>+</span> For barcode generation, please fill in 'AUTO'. If ACNO is used as barcode, please fill in 'ACNO'.";
$Lang["libms"]["import_book"]["ImportCSVDataCol_New"] = array("<font color = red>*</font>Book Title","Sub-title","<font color = red>*</font>ACNO","Call Number","Call Number 2","ISBN","ISBN2","Language","Country","Introduction","Edition","Publish Year","Publisher","Publish Place","Series","Series Number","<font color = red>&</font>Responsibility Code 1","Responsibility By 1","<font color = red>&</font>Responsibility Code 2","Responsibility By 2","<font color = red>&</font>Responsibility Code 3","Responsibility By3","Number of Page","<font color = red>&</font>Book Category Code","<font color = red>&</font>Circulation Type Code","<font color = red>&</font>Resources Type Code","<font color = red>&</font>Location Code","Subject","Account Date","Remark Internal","Purchase Date","Purchase Price","List Price","Discount","Distributor","Purchase By Department","Purchase Note","Number Of Copy","Allow to Borrow","Allow to Reserve","Remark To User","Tag","Related URL","<font color = red>+</font>barcode1","<font color = red>+</font>barcode2","<font color = red>+</font>barcode3","<font color = red>+</font>barcode4","<font color = red>+</font>barcode5","<font color = red>+</font>barcode6","<font color = red>+</font>barcode7","<font color = red>+</font>barcode8","<font color = red>+</font>barcode9","<font color = red>+</font>barcode10","<font color = red>+</font>barcode11","<font color = red>+</font>barcode12","<font color = red>+</font>barcode13","<font color = red>+</font>barcode14","<font color = red>+</font>barcode15","<font color = red>+</font>barcode16","<font color = red>+</font>barcode17","<font color = red>+</font>barcode18","<font color = red>+</font>barcode19","<font color = red>+</font>barcode20");
$Lang["libms"]["import_book"]["ImportCSVDataCol_New2"] = array("<font color = red>*</font>Book Title",
"Sub-title",
"Call Number",
"Call Number 2",
"ISBN",
"ISBN2",
"Language",
"Country",
"Introduction",
"Edition",
"Publish Year",
"Publisher",
"Publish Place",
"Series",
"Series Number",
"Responsibility Code 1  <a href='javascript:viewCodes(1)' class='tablelink'>[Click here to check codes]</a>",
"Responsibility By 1",
"Responsibility Code 2  <a href='javascript:viewCodes(1)' class='tablelink'>[Click here to check codes]</a>",
"Responsibility By 2",
"Responsibility Code 3  <a href='javascript:viewCodes(1)' class='tablelink'>[Click here to check codes]</a>",
"Responsibility By3",
"Dimension",
"ILL",
"Number of Page",
"Book Category Code  <a href='javascript:viewCodes(2)' class='tablelink'>[Click here to check codes]</a>",
"Circulation Type Code  <a href='javascript:viewCodes(3)' class='tablelink'>[Click here to check codes]</a>",
"Resources Type Code  <a href='javascript:viewCodes(4)' class='tablelink'>[Click here to check codes]</a>",
"Subject",
"Remark Internal",
"Allow to Borrow",
"Allow to Reserve",
"Remark To User (Book)",
"Tag",
"Related URL",
"<font color = red>*@</font>ACNO <span id='acno_type1'>(item information starts here, leave it blank if no item yet)</span> <span id='acno_type2' style='display:none;'><font color='red'>(Mandatory field)</font></span>",
"<font color = red>*+</font>barcode",
"Location Code  <a href='javascript:viewCodes(5)' class='tablelink'>[Click here to check codes]</a>",
"Account Date",
"Purchase Date",
"Purchase Price",
"List Price",
"Discount",
"Distributor",
"Purchase By Department",
"Purchase Note",
"Invoice Number",
"Accompany Material",
"Remark To User (Item)",
"<font color='red'>^</font>Item Status (Default: available) <a href='javascript:viewCodes(6)' class='tablelink'>[Click here to check codes]</a>",
"Item Circulation Type Code  <a href='javascript:viewCodes(3)' class='tablelink'>[Click here to check codes]</a> (leave it blank will follow Bibliography)",
"Series Number (Item)");



$Lang["libms"]["import_book"]["Code"] = "Code";
$Lang["libms"]["import_book"]["TitleEng"] = "Title in English";
$Lang["libms"]["import_book"]["TitleChi"] = "Title in Chinese";

$Lang["libms"]["import_book"]["CannotChangeFromWriteoff"] = "Item Status cannot be changed since this is a Write-off book";
$Lang["libms"]["import_book"]["CannotChangeToWriteoff"] = "Item Status cannot be changed to 'Write-off'";
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode1"] = "Responsibility Code 1 cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode2"] = "Responsibility Code 2 cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ResponsibilityCode3"] = "Responsibility Code 3 cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookCategory"] = "Book Category Code cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookCirclation"] = "Circulation Type Code cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookResources"] = "Resources Type Code cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["BookLocation"] = "Location Code cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["ItemCirculationTypeCode"] = "Item Circulation Type Code cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["ExcludeSymbols"]["Language"] = "Language cannot include these symbols ".$Lang["libms"]["settings"]["symbols"];
$Lang["libms"]["import_book"]["Warning"]["StatusChange"] = "Item status changed from %s to %s";
$Lang["libms"]["import_book"]["Warning"]["StatusChange2"] = "Item status changed to %s";
$Lang["libms"]["import_book"]["LastUpdateDate"] = "Last Update Date";
$Lang["libms"]["batch_edit"]["ByReturnDate"] = "By Due Date";
$Lang["libms"]["batch_edit"]["ByBorrowDate"] ="By Loan Date";

$Lang["libms"]["batch_edit"]["ByBarcode"] ="By Barcode";
$Lang["libms"]["batch_edit"]["ByACNO"] ="By ACNO";
$Lang["libms"]["batch_edit"]["ByACNOList"] ="By ACNO List";
$Lang["libms"]["batch_edit"]["ByAccountDate"] ="By Account date";
$Lang["libms"]["batch_edit"]["ByOthers"] ="By Others";
$Lang["libms"]["batch_edit"]["PleaseFillAboveField"] ="Please fill the above field.";
$Lang["libms"]["batch_edit"]["PleaseSelect"] = "Please select";
$Lang["libms"]["batch_edit"]["AllowBorrow"] = "Allow Loan";
$Lang["libms"]["batch_edit"]["AllowReserve"] = "Allow Reserve";
$Lang["libms"]["batch_edit"]["Apply"] = "Apply";
$Lang["libms"]["batch_edit"]["Yes"] = "Yes";
$Lang["libms"]["batch_edit"]["ConfirmUpdate"] = "Are you sure to update ";
$Lang["libms"]["batch_edit"]["InputKeywordToSearch"] = "Please input keyword to search";
$Lang["libms"]["batch_edit"]["SelectAtLeastOneClassLevel"] = "Please select at least one class level";
$Lang["libms"]["batch_edit"]["SelectAtLeastOneItem"] = "Please select at least one item";
$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateSuccess"] = " Record(s) Updated.";
$Lang["libms"]["batch_edit"]["ReturnMessage"]["UpdateUnsuccess"] = "0|=|Record Update Failed.";
$Lang["libms"]["batch_edit"]["PanelTitle"]["BookSearch"] = "Book Search";
$Lang["libms"]["batch_edit"]["PanelTitle"]["ItemSearch"] = "Item Search";
$Lang["libms"]["batch_edit"]["PanelTitle"]["SelectedRecord"] = "Selected Record";
$Lang["libms"]["batch_edit"]["PanelTitle"]["ChangeRecord"] = "Change Record";
$Lang["libms"]["batch_edit"]["InvalidDateFormat"] = "Invalid Date Format";
$Lang["libms"]["batch_edit"]["ClearList"] = "Clear All Records";
$Lang["libms"]["batch_edit"]["ConfirmClearList"] = "Are you sure to clear all records?";
$Lang["libms"]["batch_edit"]["ConfirmClearListForSearch"] = "New Search will clear all current records, are you sure to continue?";
$Lang["libms"]["batch_edit"]["msg"]["no_barcode"] = "No book is found!";
$Lang["libms"]["batch_edit"]["msg"]["scanned"] = "already in the list!";
$Lang["libms"]["batch_edit"]["Author"] = "Author";
$Lang["libms"]["batch_edit"]["BookFormat"] = "Category";
$Lang["libms"]["batch_edit"]["BookTitle"] = "Book Title";
$Lang["libms"]["batch_edit"]["CallNumber"] = "Call Number";
$Lang["libms"]["batch_edit"]["ClassLevel"] = "Class Level";
$Lang["libms"]["batch_edit"]["Description"] = "Recommend Reason";
$Lang["libms"]["batch_edit"]["RecommendedBy"] = "Recommended By";
$Lang["libms"]["batch_edit"]["RecommendedLevel"] = "Recommended Level";
$Lang["libms"]["batch_edit"]["RecommendedOn"] = "Recommended On";
$Lang["libms"]["batch_edit"]["RecommendRemark"] = "<span class='tabletextrequire'>*</span>The book(s) is already in recommended list, it'll be overwritten after submit";
$Lang["libms"]["batch_edit"]["OneACNOPerRow"] = "Input ONE ACNO per row";
$Lang["libms"]["pre_label"]["print_label_border"] = " Including label border";
$Lang["libms"]["pre_label"]["bookcode"] = "ACNO";
$Lang["libms"]["pre_label"]["bookcode_number"] = "ACNO number";
$Lang["libms"]["pre_label"]["search"] = "Search";
$Lang["libms"]["pre_label"]["add"] = "Add";
$Lang["libms"]["pre_label"]["bookid"] = "BookID";
$Lang["libms"]["pre_label"]["barcode"] = "Barcode";
$Lang["libms"]["pre_label"]["spine_label"] = "Spine Label";
$Lang["libms"]["pre_label"]["callno"] = "Call number";
$Lang["libms"]["pre_label"]["callno2"] = "Call number 2";
$Lang["libms"]["pre_label"]["location"] = "Location";
$Lang["libms"]["pre_label"]["school_name"] = "School name";
$Lang["libms"]["pre_label"]["title"] = "Book title";
$Lang["libms"]["pre_label"]["toogle"] = "Toogle";
$Lang["libms"]["pre_label"]["label_format"] = "Label paper";
$Lang["libms"]["pre_label"]["label_new_format"] = "Add label paper";
$Lang["libms"]["pre_label"]["label_edit_format"] = "Edit label paper";
$Lang["libms"]["pre_label"]["no_match"] = "No record is found!";
$Lang["libms"]["pre_label"]["select_label"] = "Label type";
$Lang["libms"]["pre_label"]["print_value"] = "Contents to print";
$Lang["libms"]["pre_label"]["print_book"] = "Book(s) involved";
$Lang["libms"]["pre_label"]["action"] = "Action";
$Lang["libms"]["pre_label"]["prefix_not_match"] = "Prefix is not match!";
$Lang["libms"]["pre_label"]["search_comment"] = "* Attention: It's suggested to print at most 100 labels at a time. Otherwise, it may lead to browser crash.";
$Lang['libms']['pre_label']['ReaderBarcode'] = "Reader Label";
$Lang['libms']['pre_label']['PleaseSelectAGroup'] = "Please select a group";
$Lang['libms']['pre_label']['PleaseSelectReader'] = "Please select reader(s)";
$Lang['libms']['pre_label']['BarcodeNo'] = "Barcode Number";
$Lang['libms']['pre_label']['StartPosition'] = "Start Print Position";

$Lang["libms"]["gen_label"]["no_book_selected"] = "Error: no book has been selected!";
$Lang["libms"]["gen_label"]["no_label_format"] = "Error: no label paper has been selected!";
$Lang["libms"]["gen_label"]["no_print_fields"] = "Error: no content has been selected for printing!";
$Lang["libms"]["group_management"] = "Patron Group Management";
$Lang["libms"]["FormClassMapping"]["CtrlMultiSelectMessage"] = "Use CTRL to select multiple items";
$Lang["libms"]["FormClassMapping"]["Or"] = " or ";
$Lang["libms"]["GroupManagement"]["AccessRight"] = "Access right";
$Lang["libms"]["GroupManagement"]["AddUser"] = "Add user(s)";
$Lang["libms"]["GroupManagement"]["Alumni"] = "Alumni";
$Lang["libms"]["GroupManagement"]["BelongToSameForm"] = "... of the same Form";
$Lang["libms"]["GroupManagement"]["BelongToSameFormClass"] = "... of the same Class";
$Lang["libms"]["GroupManagement"]["BelongToSameGroup"] = "... of the same Group";
$Lang["libms"]["GroupManagement"]["BelongToSameSubject"] = "... of the same Subject";
$Lang["libms"]["GroupManagement"]["BelongToSameSubjectGroup"] = "... of the same Subject Group";
$Lang["libms"]["GroupManagement"]["CanSendTo"] = "Can send to:";
$Lang["libms"]["GroupManagement"]["ClassName"] = "Class";
$Lang["libms"]["GroupManagement"]["ClassNumber"] = "Class number";
$Lang["libms"]["GroupManagement"]["ClassLevel"] = "Class level";
$Lang["libms"]["GroupManagement"]["Delete"] = "Remove";
//$Lang["libms"]["GroupManagement"]["ExportGroupDetail"] = "匯出群組細節";
$Lang["libms"]["GroupManagement"]["Identity"] = "Identity";
$Lang["libms"]["GroupManagement"]["ModuleTitle"] = "Group Management";
$Lang["libms"]["GroupManagement"]["ClassManagement"] = "Class Management";
$Lang["libms"]["GroupManagement"]["Name"] = "Name";
$Lang["libms"]["GroupManagement"]["New"] = "Add";
$Lang["libms"]["GroupManagement"]["NewGroup"] = "New group";
$Lang["libms"]["GroupManagement"]["Parent"] = "Parent";
//$Lang["libms"]["GroupManagement"]["PrintGroupDetail"] = "列印群組細節";
$Lang["libms"]["GroupManagement"]["RemoveThisGroup"] = "Delete group";
$Lang["libms"]["GroupManagement"]["GroupList"] = "Group list";
$Lang["libms"]["GroupManagement"]["GroupName"] = "Group name";
$Lang["libms"]["GroupManagement"]["GroupTitle"] = "Group name";
$Lang["libms"]["GroupManagement"]["SearchUser"] = "Search user";
$Lang["libms"]["GroupManagement"]["SelectAll"] = "Select all";
$Lang["libms"]["GroupManagement"]["SelectedUser"] = "Selected user(s)";
$Lang["libms"]["GroupManagement"]["SelectIdentity"] = "select identity";
$Lang["libms"]["GroupManagement"]["Student"] = "Student";
$Lang["libms"]["GroupManagement"]["SubmitAndEdit"] = "Submit and then New";
$Lang["libms"]["GroupManagement"]["SupportStaff"] = "Non-teaching Staff";
//$Lang["libms"]["GroupManagement"]["Targeting"] = "Targeting";
$Lang["libms"]["GroupManagement"]["TeachingStaff"] = "Teacher";
$Lang["libms"]["GroupManagement"]["ToAll"] = "All";
$Lang["libms"]["GroupManagement"]["ToAllUser"] = "All users";
$Lang["libms"]["GroupManagement"]["TopManagement"] = "Access Right";
$Lang["libms"]["GroupManagement"]["UncheckForMoreOptions"] = "Cancel selections";
$Lang["libms"]["GroupManagement"]["UserList"] = "Group Members";
$Lang["libms"]["GroupManagement"]["Users"] = "Members";
$Lang["libms"]["GroupManagement"]["UserType"] = "User identity";
$Lang["libms"]["SubjectClassMapping"]["SelectClass"] = "Select class";
$Lang["libms"]["GroupManagement"]["GroupNameDuplicateWarning"] = "Warning: same group name exists!";
$Lang["libms"]["GroupManagement"]["GroupCodeDuplicateWarning"] = "Warning: same group code exists!";
//sqlfeildname//$Lang["libms"]["SQL"]["UserNameFeild"] = "ChineseName";
$Lang["libms"]["SQL"]["CirDescription"] = "DescriptionEn";
//endofSQL$Lang["libms"]["GroupManagement"]["GroupTitle"] = "群組名稱";
$Lang["libms"]["GroupManagement"]["GroupCode"] = "Group code";
$Lang["libms"]["GroupManagement"]["IsDefault"] = "Default?";
$Lang["libms"]["GroupManagement"]["GroupSavedUnsuccess"] = "0|=|Record Add Failed.";
$Lang["libms"]["GroupManagement"]["GroupSavedSuccess"] = "1|=|Record Added.";
$Lang["libms"]["GroupManagement"]["GroupCodeUpdateUnsuccess"] = "0|=|Group Code Update Failed.";
$Lang["libms"]["GroupManagement"]["GroupCodeUpdateSuccess"] = "1|=|Group Code Updated.";
$Lang["libms"]["GroupManagement"]["GroupRemoveUnsuccess"] = "0|=|Record Delete Failed.";
$Lang["libms"]["GroupManagement"]["GroupRemoveSuccess"] = "1|=|Record Deleted.";
$Lang["libms"]["GroupManagement"]["DeleteWarning"] = "Are you sure you want to delete the record?";
$Lang["libms"]["GroupManagement"]["DefaultRule"] = "Default";
$Lang["libms"]["GroupManagement"]["GroupRule"] = "Circulation Policy";
$Lang["libms"]["GroupManagement"]["GroupPeriod"] = "Period (optional)";
$Lang["libms"]["GroupManagement"]["ClassManagementMembers"] = "Class Management Administrator(s)";
$Lang["libms"]["GroupManagement"]["ImportUserBarcodeColumns"] = array("<font color = red>*</font>UserLogin","<font color = red>*</font>Barcode");
$Lang["libms"]["GroupManagement"]["ImportUserBarcode"] = "Import User Barcode";
$Lang["libms"]["GroupManagement"]["Import"]["UserBarcode"] = "User Barcode";
$Lang["libms"]["GroupManagement"]["Import"]["UserLogin"] = "Login ID";
########### Admin ################
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["name"] = "Admin Function";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["settings"] = "Settings";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["group management"] = "Patron Group Management";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["stock-take and write-off"] = "Stock-take & Write-off";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["stock-take"] = "Stock-take";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["write-off"] = "Write-off";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["book management"] = "Book Management (Book, Item, Periodical, Batch Edit, Label, Current Reservation, Return Lost Book)";
$Lang["libms"]["GroupManagement"]["Right"]["admin"]["portal settings"] = "Portal Settings";
########### Admin ################

$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["name"] = "Circulation";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["access_without_open_hour"] = "&nbsp;- Access without open hour";
$Lang['libms']['GroupManagement']['Right']['circulation management']['not_allow_class_management_circulation'] = '&nbsp;- Not allow borrowing and returning books in class management location';
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["borrow"] = "Loan";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["reserve"] = "Reserve";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["return"] = "Return";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["renew"] = "Renew";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["overdue"] = "Overdue payment";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["batch_return_and_renew"] = "Batch Return and Renew";
$Lang["libms"]["GroupManagement"]["Right"]["circulation management"]["lost"] = "Report lost";

##### Report Start#####
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["name"] = "Reports";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["daily summary"] = "Summary";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["overdue"] = "Overdue";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["income"] = "Income";
//$Lang["libms"]["GroupManagement"]["Right"]["reports"]["borrow records"] = "Loan";

$Lang["libms"]["GroupManagement"]["Right"]["reports"]["accession report"] = "Accession Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["book report"] = "Book Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["loan report"] = "Loan Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["overdue report"] = "Overdue Notice";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["penalty report"] = "Penalty Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["lost report"] = "Lost Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["reader ranking"] = "Patron Ranking";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["inactive reader"] = "Inactive Patron";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["book ranking"] = "Book Ranking";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["stock-take report"] = "Stock-take Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["write-off report"] = "Write-off Report";
$Lang["libms"]["GroupManagement"]["Right"]["reports"]["progress report"] = "Progress Report";

##### Report End #####
########## Statistic Start #########
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["name"] = "Statistics";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["summary"] = "Summary";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["general stats"] = "General Stats";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["category vs class or form"] = "Category vs Class/Form";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["frequency by form"] = "Frequency by Form";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["penalty stats"] = "Penalty Stats";
$Lang["libms"]["GroupManagement"]["Right"]["statistics"]["ebook statistics"] = "eBooks Stats";
########## Statistic End  #########

$Lang["libms"]["GroupManagement"]["Right"]["service"]["name"] = "End-user Services";
$Lang["libms"]["GroupManagement"]["Right"]["service"]["reserve"] = "Reserve";
$Lang["libms"]["GroupManagement"]["Right"]["service"]["renew"] = "Renew";
$Lang["libms"]["GroupManagement"]["Rule"]["CirDescription"] = "Circulation Type";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitBorrow"] = "Loan (No. of books)";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitRenew"] = "Renew (No. of times)";
$Lang["libms"]["GroupManagement"]["Rule"]["LimitReserve"] = "Reserve (No. of books)";
$Lang["libms"]["GroupManagement"]["Rule"]["ReturnDuration"] = "Duration (No. of days)";
$Lang["libms"]["GroupManagement"]["Rule"]["OverDueCharge"] = "Overdue (fine per day)";
$Lang["libms"]["GroupManagement"]["Rule"]["MaxFine"] = "Maxium Fine (per book)";
$Lang["libms"]["GroupManagement"]["Msg"]["chgrp_confirm"] = "The following user belongs to other groups. Please confirm group change.";
$Lang["libms"]["GroupManagement"]["Msg"]["no_group_right"] = "No loaning is permitted until this user belongs to a patron group and has granted circulation policy!";

$Lang["libms"]["GroupManagement"]["Rule"]["policy_remark"] = "<u>Remark</u>: The <b>Default Policy</b> refers to maximum number of borrowed item(s), which includes item(s) without circulation type.";
$Lang["libms"]["GroupManagement"]["index"]["remark"] = "Each user can belongs to one group only. If the user is not yet assgined to a group or his/her group has no circulation policy set, loaning will not be allowed.";

$Lang["libms"]["book_management"] = "Book management";
$Lang["libms"]["circulation_management"] = "Circulation management";
$Lang["libms"]["Circulation"]["detailrecord"] = "Detailed Records";
$Lang["libms"]["Circulation"]["borrowedBooks"] = "Loan Records";
$Lang["libms"]["Circulation"]["detailrecord"] = "Detailed Records";
$Lang["libms"]["Circulation"]["action_success"] = "succeed!";
$Lang["libms"]["Circulation"]["action_fail"] = "failed!";
$Lang["libms"]["Circulation"]["BookBorrowHistory"] = "Loan Records";
$Lang["libms"]["Circulation"]["ReaderBorrowHistory"] = "Patron Loan Records";
$Lang["libms"]["Circulation"]["PleaseEnterPassword"] = "Please enter password";
$Lang["libms"]["Circulation"]["WrongPassword"] = "Wrong Password";
$Lang["libms"]["Circulation"]["NoPermissionHandleBook"] = "You do not have permission to handle this book!";
$Lang["libms"]["Circulation"]["NoPermissionHandleUser"] = "You do not have permission to handle this user!";
$Lang["libms"]["Circulation"]["BorrowAccompayMaterial"] = "Are you sure to borrow this book together with accompany material?";
$Lang["libms"]["Circulation"]["BorrowWithAccompayMaterial"] = "Borrow this book together with accompany material";
$Lang["libms"]["Circulation"]["RenewAccompayMaterial"] = "Are you sure to renew this book together with accompany material?";
$Lang["libms"]["Circulation"]["RenewWithAccompayMaterial"] = "Renew this book together with accompany material";
$Lang["libms"]["Circulation"]["ReturnAccompayMaterial"] = "Are you sure to return this book together with accompany material?";
$Lang["libms"]["Circulation"]["NotBorrowAccompayMaterial"] = "Did not borrow accompany material";
$Lang["libms"]["Circulation"]["ClickToViewBorrowHistory"] = "Please click book title to view loan records";
$Lang['libms']['Circulation']['RemarkNotAllowToBorrowReturnClassManagementBooks'] = '^ Not allow borrowing and returning class management books';
$Lang['libms']['CirculationManagement']['user_group_not_found'] = "This user does not belong to any patron group!";

$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['subject'] = "Notification of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body'] = <<<EOF
The book 《%s》 you reserved is now available for loan.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_ready']['mail_body_with_due_date'] = <<<EOF
The book 《%s》 you reserved is now available for loan. Please borrow it on or before %s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['subject'] = "Cancellation of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_admin']['mail_body'] = <<<EOF
Please be informed that your book 《%2\$s》 reserved has been cancelled by the library administrator on %1\$s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['subject'] = "Cancellation of book reserved";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove']['mail_body'] = <<<EOF
Please be informed that your book 《%2\$s》 reserved has been cancelled by you on %1\$s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['subject'] = "Cancellation of book reserved (Expired)";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_remove_system']['mail_body'] = <<<EOF
Please be informed that your book 《%2\$s》 reserved has been cancelled by system since the reservation gets expired on %1\$s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['subject'] = "Reserved book change to waiting list";
$Lang["libms"]["CirculationManagement"]["mail"]['reserve_move_to_wait']['mail_body'] = <<<EOF
Please be informed that your reserved book 《%2\$s》 has been changed to waiting list on %1\$s.
EOF;
$Lang["libms"]["CirculationManagement"]["mail"]['due_date_change_reminder']['subject'] = "Due Date Change Reminder";
$Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['subject'] = "Due Date Reminder";
$Lang["libms"]["CirculationManagement"]["mail"]['due_date_reminder']['mail_body'] = <<<EOF
Please be informed that the due date of your borrowed book(s) 《%s》 is/are on %s. Please return the book(s) on or before the due date.
EOF;

$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['CallNum']= "Call Number";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['BookTitle']= "Book Title";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Publisher']= "Publisher";
$Lang['libms']['CirculationManagement']['reserve_ajax']['title']['Author']= "Author";
$Lang['libms']['CirculationManagement']['book_search']['title']['BookTitle']= "Book Title";
$Lang['libms']['CirculationManagement']['book_search']['title']['Author']= "Author";
$Lang['libms']['CirculationManagement']['book_search']['title']['ACNO']= "ACNO";
$Lang['libms']['CirculationManagement']['book_search']['title']['CallNum']= "Call Number";

$Lang['libms']['CirculationManagement']['numberOfLoans'] = "loan times";
$Lang['libms']['CirculationManagement']['barcode_user_not_found'] = "No user is found according to the barcode."; 
$Lang['libms']['CirculationManagement']['library_closed'] = "In closing";
$Lang['libms']['CirculationManagement']['batch_return'] = "Batch Book Returns";
$Lang['libms']['CirculationManagement']['batch_renew'] = "Batch Book Renews";
$Lang['libms']['CirculationManagement']['search_book_record'] = "Book Search";
$Lang['libms']['CirculationManagement']['batch_return_enter'] = "Enter";
$Lang['libms']['CirculationManagement']['normal_circulation'] = "Circulation for a Patron";
$Lang["libms"]["CirculationManagement"]["status"] = "Status";
$Lang["libms"]["CirculationManagement"]["add"] = "Add";
$Lang["libms"]["CirculationManagement"]["confirm"] = "Confirm";
$Lang["libms"]["CirculationManagement"]["cancel"] = "Cancel";
$Lang["libms"]["CirculationManagement"]["user_barcode"] = "User barcode";
$Lang["libms"]["CirculationManagement"]["student_only"] = "or student selection";
$Lang["libms"]["CirculationManagement"]["user_barcode_ByLin"] = "User barcode 用戶條碼";
$Lang["libms"]["CirculationManagement"]["barcode"] = "Barcode";
$Lang["libms"]["CirculationManagement"]["enter_code"] = "Input ACNO/Barcode/Call number/ISBN";
$Lang["libms"]["CirculationManagement"]["bkcode_acno"] = "Call number／ACNO";
$Lang["libms"]["CirculationManagement"]['reserve_page']["searchby"] = "Book title / Author / ACNO / Call number ";
$Lang["libms"]["CirculationManagement"]['search_book']["searchby"] = "Book title / Call number / Barcode";
$Lang["libms"]["CirculationManagement"]["booktitle"] = "Book title";
$Lang["libms"]["CirculationManagement"]["moredetail"] = "More details";
$Lang["libms"]["CirculationManagement"]["location"] = "Location";
$Lang["libms"]["CirculationManagement"]["borrowday"] = "Loan date";
$Lang["libms"]["CirculationManagement"]["returnday"] = "Return date";
$Lang["libms"]["CirculationManagement"]["renewday"] = "Renew date";
$Lang["libms"]["CirculationManagement"]["dueday"] = "Due date";
$Lang["libms"]["CirculationManagement"]["reserveday"] = "Reserve date";
$Lang["libms"]["CirculationManagement"]["overdueday"] = "NO. of days past due";
$Lang["libms"]["CirculationManagement"]["renew"] = "Renew";
$Lang["libms"]["CirculationManagement"]["renews"] = "Renews";
$Lang["libms"]["CirculationManagement"]["lost"] = "Report lost";
$Lang["libms"]["CirculationManagement"]["borrow"] = "Loan";
$Lang["libms"]["CirculationManagement"]["return"] = "Return";
$Lang["libms"]["CirculationManagement"]["reserve"] = "Reserve";
$Lang["libms"]["CirculationManagement"]["overdue"] = "Settle overdue fine";
$Lang["libms"]["CirculationManagement"]["statusnow"] = "Current records";
$Lang["libms"]["CirculationManagement"]["all_loan_records"] = "All loan records";
$Lang["libms"]["CirculationManagement"]["leave"] = "Quit";
$Lang["libms"]["CirculationManagement"]["no_borrow"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_renew"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_return"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_batch_renew"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_lost"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_reserve"] = "Total";
$Lang["libms"]["CirculationManagement"]["no_to_return"] = "Total";
$Lang["libms"]["CirculationManagement"]["ppl_reserved"] = "Total";
$Lang["libms"]["CirculationManagement"]["n_renew"] = "Renew: disallowed";
$Lang["libms"]["CirculationManagement"]["y_renew"] = "Renew: allowed";
$Lang["libms"]["CirculationManagement"]["y_reserve"] = "Reserve: allowed";
$Lang["libms"]["CirculationManagement"]["n_reserve"] = "Reserve: disallowed";
$Lang["libms"]["CirculationManagement"]["y_borrow"] = "Loan: allowed";
$Lang["libms"]["CirculationManagement"]["n_borrow"] = "Loan: disallowed";
$Lang["libms"]["CirculationManagement"]["renew_left"] = "Renew left";
$Lang["libms"]["CirculationManagement"]["overdue_day"] = "Overdue (days)";
$Lang['libms']['CirculationManagement']['overdue_type'] ="Penalty Type";
$Lang["libms"]["CirculationManagement"]["overdue_with_day"] = "Overdue (%d days)";
$Lang["libms"]["CirculationManagement"]["payment"] = "Fine";
$Lang["libms"]["CirculationManagement"]["free_payment"] = "Waive";
$Lang["libms"]["CirculationManagement"]["paied"] = "Payment settled";
$Lang["libms"]["CirculationManagement"]["paid_or_waive"] = "Payment settled/waived";
$Lang["libms"]["CirculationManagement"]["pay_now"] = "Amount to pay";
$Lang["libms"]["CirculationManagement"]["booked"] = "Reserved by other(s)";
$Lang["libms"]["CirculationManagement"]["booked_alert"] = "Reserved by other(s)!";
$Lang["libms"]["CirculationManagement"]["left"] = "Remained";
$Lang["libms"]["CirculationManagement"]["total_pay"] = "Total";
$Lang["libms"]["CirculationManagement"]["reserverperiod"] = "Reserve period";
$Lang["libms"]["CirculationManagement"]["trim"] = "(repairing)";
$Lang["libms"]["CirculationManagement"]["action"] = "Action";
$Lang["libms"]["CirculationManagement"]["borrowed"] = "Loaned";
$Lang["libms"]["CirculationManagement"]["reserved"] = "Reserved";
$Lang["libms"]["CirculationManagement"]["book_overdue"] = "Overdue";
$Lang["libms"]["CirculationManagement"]["borrow_limit"] = "Loan quota";
$Lang["libms"]["CirculationManagement"]["limit_left"] = "Quota left";
$Lang["libms"]["CirculationManagement"]["book_reserve"] = "Reserved";
$Lang["libms"]["CirculationManagement"]["left_pay"] = "Fine";
$Lang["libms"]["CirculationManagement"]["past_record"] = "Past records";
$Lang["libms"]["CirculationManagement"]["FineDate"] = "Fine Date (Return Date)";
$Lang["libms"]["CirculationManagement"]["borrow_accompany_material"] = "Borrow with accompany material";
$Lang["libms"]["CirculationManagement"]["borrow_accompany_material_yes"] = "Yes";
$Lang["libms"]["CirculationManagement"]["borrow_accompany_material_no"] = "No";
$Lang["libms"]["CirculationManagement"]["additional_charge"] = "Additional Charge";
$Lang['libms']['CirculationManagement']['msg']['book_item_status']['general'] = "This book status is ";
$Lang['libms']['CirculationManagement']['msg']['book_item_status']['not_on_loan'] = "This book is not on loaned!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_borrow_limit"] = "Loan limit is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_limit"] = "Reservation limit is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_renew_limit"] = "Renewal limit is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_borrow_cat_limit"] = "Loan limit on circulation type is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_reserve_cat_limit"] = "Reservation limit on circulation type is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["over_renew_cat_limit"] = "Renewal limit on circulation type is reached!";
$Lang["libms"]["CirculationManagement"]["msg"]["renew_but_overdue"] = "Over due! Must return it and settle payment.";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_borrow_limit"] = "Not allowed to loan by this user.";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_reserve_limit"] = "Not allowed to reserve by this user.";
$Lang["libms"]["CirculationManagement"]["msg"]["zero_renew_limit"] = "Not allowed to renew by this user.";
$Lang['libms']['CirculationManagement']['msg']['notAllowBorrowReturnClassManagementBooks'] = 'Not allow borrowing and returning class management books';
$Lang['libms']['CirculationManagement']['msg']['notAllowReturnWriteoffBooks'] = 'Not allow returning write-off books';
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_borrow"] = "cannot be loaned";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_borrow_book"] = "cannot be loaned";
$Lang["libms"]["CirculationManagement"]["msg"]["already_borrowed"] = "this book is borrowed by [user_name]";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve"] = "cannot be reserved";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_no_copy"] = "no item for reservation";
$Lang["libms"]["CirculationManagement"]["msg"]["exceeds_reserve"] = "you already reach the reservation limit";
$Lang["libms"]["CirculationManagement"]["msg"]["exceeds_reserve_of_book_category"] = "you already reach the reservation limit of this book category";
$Lang["libms"]["CirculationManagement"]["msg"]["cannot_reserve_book"] = "cannot be reserved";
$Lang["libms"]["CirculationManagement"]["msg"]["scanned"] = "already in the list!";
$Lang["libms"]["CirculationManagement"]["msg"]["inlist"] = "The book is already in the list!";
$Lang["libms"]["CirculationManagement"]["msg"]["force_borrow"] = "forced to loan";
$Lang["libms"]["CirculationManagement"]["msg"]["force_reserve"] = "forced to reserve";
$Lang["libms"]["CirculationManagement"]["msg"]["f_renew"] = "force to renew";
$Lang["libms"]["CirculationManagement"]["msg"]["f_reserve"] = "force to reserve";
$Lang["libms"]["CirculationManagement"]["msg"]["f_borrow"] = "force to loan";
$Lang["libms"]["CirculationManagement"]["msg"]["reserved"] = "on reserve";
$Lang["libms"]["CirculationManagement"]["msg"]["comfirm_leave"] = "[IMPORTANT] Record(s) have not been submitted yet! Are you sure you want to discard?";
$Lang["libms"]["CirculationManagement"]["msg"]["n_barcode"] = "No book is found!";
$Lang["libms"]["CirculationManagement"]["msg"]["n_callno_acno"] = "No record match your query";
$Lang["libms"]["CirculationManagement"]["msg"]["search_book_not_found"] = "No record match your query";
$Lang["libms"]["CirculationManagement"]["msg"]["overdue1"] = "Overdue!";
$Lang["libms"]["CirculationManagement"]["msg"]["overdue2"] = "day(s)";
$Lang['libms']['CirculationManagement']['msg']['overdue_limit'] = "Overdue grace period is expired! Please return the overdring book.";
$Lang['libms']['CirculationManagement']['msg']['please_pick_a_book'] = "Please pick a book";
$Lang['libms']['CirculationManagement']['msg']['searching_string_too_short']= 'Searching string is too short!';
$Lang['libms']['CirculationManagement']['msg']['fix_error'] = "Please fix input error.";
$Lang["libms"]["CirculationManagement"]["return_ajax"]["no_borrow_record"] = "No loan record is found!";
$Lang['libms']['CirculationManagement']['msg']['batch_return'] = "You can handle book returns for different users here.";
$Lang['libms']['CirculationManagement']['msg']['batch_renew'] = "You can handle book renewals for different users here.";
$Lang['libms']['CirculationManagement']['msg']['circulation_index'] = "You can choose to handle batch book returns, circulation for an user or searching book.";
$Lang['libms']['CirculationManagement']['msg']['no_record_to_proceed'] = "Please input or select at least one book!";
$Lang["libms"]["CirculationManagement"]["msg"]["Hold_Book"] = "The book is reserved!";
$Lang['libms']['CirculationManagement']['msg']['user_reserved'] = "The book is reserved by other reader!";
$Lang['libms']['CirculationManagement']['msg']['reserved_by'] = "Reserved by";
$Lang['libms']['CirculationManagement']['msg']['expiry_date'] = "Expiry Date";
$Lang['libms']['CirculationManagement']['msg']['no_overdue_record'] = "There is no payment to be settled from returned record(s).";
$Lang["libms"]["CirculationManagement"]['msg']["AllOverduePaymentPaidWithePayment"] = "All overdue payments will be paid with ePayment.";
$Lang['libms']['CirculationManagement']['msg']['already_quit'] = "User already quit";
$Lang['libms']['CirculationManagement']['msg']['please_input_additional_charge'] = "Please input additional charge";
$Lang["libms"]["CirculationManagement"]["borrowed_by"] = "Loaned by";
$Lang["libms"]["CirculationManagement"]["valid_date"] = "Expiry Date";
$Lang["libms"]["CirculationManagement"]['msg']["AllOverduePaymentPaidWithePayment"] = "All overdue payments will be paid with ePayment.";
$Lang["libms"]["CirculationHotKeys"]["Exit"] = "[Esc]";
$Lang["libms"]["CirculationHotKeys"]["Borrow"] = "[F2]";
$Lang["libms"]["CirculationHotKeys"]["Return"] = "[F3]";
$Lang["libms"]["CirculationHotKeys"]["Renew"] = "[F4]";
$Lang["libms"]["CirculationHotKeys"]["Lost"] = "[F6]";
$Lang["libms"]["CirculationHotKeys"]["Reserve"] = "[F7]";
$Lang["libms"]["CirculationHotKeys"]["Overdue"] = "[F8]";
$Lang["libms"]["CirculationHotKeys"]["Cancel"] = "[F5]";
$Lang["libms"]["CirculationHotKeys"]["Confirm"] = "[F9]";
$Lang["libms"]["CirculationHotKeys"]["Details"] = "[F10]";
$Lang["libms"]["CirculationHotKeys"]["BatchReturn"] = "[F1]";



//$Lang["libms"]["book_status"]["CANCEL"] = "取消";
//$Lang["libms"]["book_status"]["DELETE"] = "删除";
$Lang["libms"]["book_status"]["NORMAL"] = "Available";
$Lang["libms"]["book_status"]["SHELVING"] = "Processing";
$Lang["libms"]["book_status"]["BORROWED"] = "on Loan";
$Lang["libms"]["book_status"]["DAMAGED"] = "Damaged";
$Lang["libms"]["book_status"]["REPAIRING"] = "Repairing";
$Lang["libms"]["book_status"]["RESERVED"] = "on Reserve";
$Lang["libms"]["book_status"]["RESTRICTED"] = "on Display";
$Lang["libms"]["book_status"]["WRITEOFF"] = "Write-off";
$Lang["libms"]["book_status"]["SUSPECTEDMISSING"] = "Suspected missing";
$Lang["libms"]["book_status"]["LOST"] = "Lost";
$Lang["libms"]["book_status"]["ORDERING"] = "on Order";

$Lang["libms"]["book_reserved_status"]["WAITING"] = "Waiting";
$Lang["libms"]["book_reserved_status"]["READY"] = "Ready";

//reportingpart...................................................................................................................................................................................................................................................................................................
$Lang["libms"]["report"]["bookinfo"] = "Book Information";
$Lang["libms"]["report"]["country"] = "Country";
$Lang["libms"]["report"]["language"] = "Language";
$Lang["libms"]["report"]["time_period"] = "Period";
$Lang["libms"]["report"]["SchoolYear"] = "Academic year";
$Lang["libms"]["report"]["Semester"] = "Term";
$Lang["libms"]["report"]["period_date_from"] = "From";
$Lang["libms"]["report"]["period_date_to"] = " to";
$Lang["libms"]["report"]["display"] = "Display";
$Lang["libms"]["report"]["circulation_type"] = "Circulation type";
$Lang["libms"]["report"]["book_category"] = "Book category";
$Lang["libms"]["report"]["subject"] = "Subject";
$Lang["libms"]["report"]["book_status"] = "Book status";
$Lang["libms"]["report"]["book_status_all"] = "All books";
$Lang["libms"]["report"]["book_status_borrow"] = "Books on loan";
$Lang["libms"]["report"]["book_status_renew"] = "Books on renew";
$Lang["libms"]["report"]["target"] = "Target";
$Lang["libms"]["report"]["findByGroup"] = "Group";
$Lang["libms"]["report"]["findByStudent"] = "Student";
$Lang["libms"]["report"]["amount"] = "Amount";
$Lang["libms"]["report"]["due_time"] = "Due date";
$Lang['libms']['report']['penaltyDate'] = 'Date of penalty';
$Lang["libms"]["report"]["show_penalty"] = "Show penalty";
$Lang["libms"]["report"]["show_purchase_price"] = "Show purchase price";
$Lang["libms"]["report"]["show_list_price"] = "Show list price";
$Lang["libms"]["report"]["excluding_in_push_notification"] = "(Excluding in Push Notification)";
$Lang["libms"]["report"]["due_time_ByLin"] = "Due date 到期日";
$Lang["libms"]["report"]["borrowdate"] = "Loan time";
$Lang["libms"]["report"]["returndate"] = "Return time";
$Lang["libms"]["report"]["listby"] = "List by";
$Lang["libms"]["report"]["sortby"] = "Sort by";
$Lang["libms"]["report"]["show_last_modified_by"] = "Show last modified by person and time in circulation page";
$Lang["libms"]["report"]["classno"] = "Class number";
$Lang["libms"]["report"]["ACNO"] = "ACNO";
$Lang["libms"]["report"]["ACNO_ByLin"] = "ACNO 登錄號碼";
$Lang["libms"]["report"]["frequency"] = "Number of times";
$Lang["libms"]["report"]["number"] = "Total";
$Lang["libms"]["report"]["name"] = "Title";
$Lang["libms"]["report"]["by_form_class"] = "form/class";
$Lang["libms"]["report"]["by_amount"] = "total";
$Lang["libms"]["report"]["ascending"] = "ascending";
$Lang["libms"]["report"]["descending"] = "descending";
$Lang["libms"]["report"]["cannot_define"] = " - unclassified - ";
$Lang["libms"]["report"]["print"] = "Print";
$Lang["libms"]["report"]["mailToStudent"] = "Email to students";
$Lang["libms"]["report"]["mailToParent"] = "Email to parents";
$Lang["libms"]["report"]["selectAll"] = "Select all";
$Lang["libms"]["report"]["PressCtrlKey"] = "(Use CTRL to select multiple items)";
$Lang["libms"]["report"]["Submit"] = "Submit";
$Lang["libms"]["report"]["Form"] = "Form";
$Lang["libms"]["report"]["Class"] = "Class";
$Lang["libms"]["report"]["Student"] = "User";
$Lang["libms"]["report"]["ListNumber"] = "List Number";
$Lang["libms"]["report"]["Resource"] = "Resource Category";
$Lang["libms"]["report"]["totalreserve"] = "Total Reserve";
$Lang["libms"]["report"]["totalborrow"] = "Total Borrow";
$Lang["libms"]["report"]["totalamount"] = "Total Amount Received";
$Lang["libms"]["report"]["totaloverdue"] = "Total Overdue";
$Lang["libms"]["report"]["barcode"] = "Barcode";
$Lang["libms"]["report"]["recordstatus"] = "Record Status";
$Lang["libms"]["report"]["statistical_category"] = "Statistical Category";
$Lang["libms"]["report"]["loans_less_than"] = "With record(s)";
$Lang["libms"]["report"]["loans_less_than_unit"] = " or less";
$Lang["libms"]["report"]["Gender"] = "Gender";
$Lang["libms"]["report"]["Male"] = "Male";
$Lang["libms"]["report"]["Female"] = "Female";
$Lang["libms"]["report"]["onloan"] = "On Loan";
$Lang["libms"]["report"]["targetGroup"] = "Group";
$Lang["libms"]["report"]["targetGroupMember"] = "Group Member";	
$Lang["libms"]["report"]["ClassLevel"] = "Level";
$Lang["libms"]["report"]["ClassName"] = "Class";
$Lang["libms"]["report"]["ClassName_ByLin"] = "Class 班別";
$Lang["libms"]["report"]["ClassNumber"] = "Class number";
$Lang["libms"]["report"]["ClassNumber_ByLin"] = "Class number 學號";
$Lang["libms"]["report"]["username"] = "User";
$Lang["libms"]["report"]["username_ByLin"] = "User 讀者姓名";
$Lang["libms"]["report"]["ChineseName"] = "Chinese name";
$Lang["libms"]["report"]["EnglishName"] = "English name";
$Lang["libms"]["report"]["booktitle"] = "Book";
$Lang["libms"]["report"]["booktitle_ByLin"] = "Book 書本";
$Lang["libms"]["report"]["LendingTotal"] = "Total";
$Lang["libms"]["report"]["CallNum"] = "Call No.";
$Lang["libms"]["report"]["ResponsibilityBy"] = "Responsibility By";
$Lang["libms"]["report"]["Publisher"] = "Publisher";
$Lang["libms"]["report"]["Edition"] = "Edition";
$Lang["libms"]["report"]["ISBN"]="ISBN";
$Lang["libms"]["report"]["tags"] = "Tags";
$Lang["libms"]["report"]["search"]="Search (ACNO / Book Title / ISBN)";
$Lang["libms"]["report"]["pay"]="Penalty";
$Lang["libms"]["report"]["pay_ByLin"]="Penalty 罰款";
$Lang["libms"]["report"]["payment"]["title"] = "Sum";
$Lang["libms"]["report"]["payment"]["OVERDUE"] = "Outstanding";
$Lang["libms"]["report"]["payment"]["PAID"] = "Payment received";
$Lang["libms"]["report"]["total"] = "Total";
$Lang["libms"]["report"]["total_ByLin"] = "Total 總數";
$Lang["libms"]["report"]["LastModifiedBy"]="Last Modified By";
$Lang["libms"]["report"]["LastModified"]="Last Modified";
$Lang["libms"]["report"]["bookrecord"]["title"]= "Book";
$Lang["libms"]["report"]["bookrecord"]["LOST"] = "Lost";
$Lang["libms"]["report"]["bookrecord"]["WRITEOFF"] = "Write-off";
$Lang["libms"]["report"]["bookrecord"]["NEW"] = "New";
$Lang["libms"]["report"]["IncludeLeftStudent"] = "Include Left Student";
$Lang["libms"]["report"]["left_student_indicator"] = "A <span style='color: red'> * </span> denotes left student";

$Lang["libms"]["report"]["overdue_email"]["Subject"] = "Due Date Reminder";
$Lang["libms"]["report"]["overdue_email"]["Subject_ByLin"] = "Due Date Reminder 到期通知";
$Lang["libms"]["report"]["overdue_email"]["introduction_ByLin"] = "The following library material(s) which you borrowed is/are already due for return. Please return it/them now. Please ignore this notice if you have already returned. <br />你所借的下列書本或物品現已到期，請立即交還。如你已交還，請無需理會此通知書。";
$Lang["libms"]["report"]["overdue_remark"]["remark"] = "Remark";
$Lang["libms"]["report"]["overdue_remark"]["history"] = "Remark History";
$Lang["libms"]["report"]["overdue_remark"]["mandatory"] = "Please input overdue remark!";
$Lang["libms"]["report"]["overdue_remark"]["please_select"] = "-- Please select remark --";
$Lang["libms"]["report"]["overdue_remark"]["server_error"] = "Error: cannot get overdue remark, please try it later.";

$Lang["libms"]["report"]["toolbar"]['Btn']['AddRemark'] = "Add Remark";
$Lang["libms"]["report"]["toolbar"]['Btn']['Export'] = "Export";
$Lang["libms"]["report"]["toolbar"]['Btn']['Print'] = "Print";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailuser'] = "Email to user";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailparent'] = "Email to parent";
$Lang["libms"]["report"]["toolbar"]['Btn']['emailboth'] = "Email to user and parent";
$Lang["libms"]["report"]["toolbar"]['Btn']['sendPushMessageToParent'] = "Send Push Message to parent";
$Lang["libms"]["report"]["toolbar"]['Btn']['Close'] = "Close";


$Lang["libms"]["report"]["printdate"] = "Date of printing";
$Lang["libms"]["report"]["printdate_ByLin"] = "Date of printing 列印日期";
$Lang["libms"]["report"]["noOfDayOverdue"] = "Overdue (days)";
$Lang["libms"]["report"]["noOfDayOverdue_ByLin"] = "Overdue (days) 過期日數";

$Lang["libms"]["report"]["showoption"] = "Display options";
$Lang["libms"]["report"]["hideoption"] = "Hide options";
$Lang["libms"]["report"]["norecord"] = "No record found!";

$Lang["libms"]["report"]["fine_date"] = "Penalty Date";
$Lang["libms"]["report"]["fine_status"] = "Payment Status";
$Lang["libms"]["report"]["fine_status_all"] = "All";
$Lang["libms"]["report"]["fine_outstanding"] = "Outstanding";
$Lang["libms"]["report"]["fine_waived"] = "Waived";
$Lang["libms"]["report"]["fine_handle_date"] = "Paid On";
$Lang["libms"]["report"]["fine_handle_date_or_last_updated"] = "Paid On/Last Modified";
$Lang["libms"]["report"]["fine_type"] = "Penalty Reason";
$Lang["libms"]["report"]["fine_type_overdue"] = "Overdue";
$Lang["libms"]["report"]["fine_type_lost"] = "Lost book";
$Lang["libms"]["report"]["fine_amended"] = "Payment amended";
$Lang["libms"]["report"]["handled_by"] = "Handled By";
$Lang["libms"]["report"]["library_stock"] = "Library Stock";
$Lang["libms"]["report"]["library_resources"] = "Use of Library Resources";
$Lang["libms"]["report"]['Others'] = "Others";
$Lang["libms"]["report"]['daily_averages_issue'] = "Daily Averages Issue (vol.)";
$Lang["libms"]["report"]['listTotalNumberOfPerson'] = "List the Total number of student by";
$Lang["libms"]["report"]['TotalNumberOfStudent'] = "Total number of student";
$Lang["libms"]["report"]['EnableDeletionLine'] = "Enable Deletion Line";

$Lang['libms']['report']['msg']['searching_string_too_short']= 'Searching string is too short!';
$Lang['libms']['report']['msg']['empty_search']= 'Please input search words';
$Lang['libms']['report']['msg']['please_pick_a_book'] = "Please pick a book";
$Lang['libms']['report']['msg']['please_select_academic_year'] = "Please select academic year";
$Lang['libms']['report']['msg']['please_select_category'] = "Please pick at least one category";
$Lang['libms']['report']['msg']['please_select_circategory'] = "Please pick at least one circulation type";
$Lang['libms']['report']['msg']['please_select_display_item'] = "Please pick at least one item to display";
$Lang['libms']['report']['msg']['please_select_subject'] = "Please pick at least one subject";
$Lang['libms']['report']['msg']['please_select_language'] = "Please pick at least one language";
$Lang['libms']['report']['msg']['please_select_class'] = "Please pick at least one class";
$Lang['libms']['report']['msg']['please_select_form'] = "Please pick at least one form";
$Lang['libms']['report']['msg']['please_select_group'] = "Please pick at least one group";
$Lang['libms']['report']['msg']['please_select_group_member'] = "Please pick at least one group member";
$Lang['libms']['report']['msg']['please_select_student'] = "Please pick at least one student";
$Lang['libms']['report']['msg']['wrong_due_date_range'] = "Due date must be equal or earlier than today.";
$Lang['libms']['report']['msg']['dueDateShouldBeEarlierThanPenaltyDate'] = "Due date must be earlier than date of penalty.";
$Lang['libms']['report']['1weekormore'] = "once a month or more";
$Lang['libms']['report']['2week'] = "Once every two weeks";
$Lang['libms']['report']['1month'] = "once a month";
$Lang['libms']['report']['1monthless'] = "less than once a month";
$Lang['libms']['report']['none'] = "Never";
$Lang['libms']['report']['totalTimesOfBorrow'] = "Total number of loan";
$Lang['libms']['report']['all_records'] = "All Records";
$Lang['libms']['report']['ForReferenceOnly'] = "For reference only";
$Lang['libms']['report']['LostBookPaymentChanged'] = "Penalty amount for lost book is amended";
$Lang['libms']['report']['LostBookReturned'] = "The lost item has been returned";
$Lang['libms']['report']['NegagivePaidDenotesCancelOverdue'] = "Negative value of 'Payment settled' denotes cancelling previous paid record";
$Lang["libms"]["report"]["displayGenderTotal"] = "Display total by gender";
$Lang["libms"]["report"]["percentage"] = "Percentage";
$Lang["libms"]["report"]["Times"] = "Times";
$Lang['libms']['report']['PageBreakForEachUser'] = "New page for each user when printing";
$Lang['libms']['report']['NewPageBreak'] = "New page when printing";
$Lang['libms']['report']['WriteoffPrintTotal'] = "Total";
$Lang['libms']['report']['ApprovedBy'] = "Approved By";
$Lang['libms']['report']['SubmittedBy'] = "Submitted By";
$Lang["libms"]["import"]['BookCat']['title'] = "Book Category Import";
$Lang["libms"]["import"]['BookCat']['error_msg']['upload_fail'] = "Upload Fail";
$Lang["libms"]["import"]['BookCat']['error_msg']['contact_admin'] = "Please contact system administrator";


$Lang["libms"]["import"]['BookCat']['fields']['BookCategoryCode']['name']="Book Category Code";
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionEn']['name'] ="English Description";
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionChi']['name']="Chinese Description";
$Lang["libms"]["import"]['BookCat']['fields']['BookCategoryCode']['req']=true;
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionEn']['req'] =true;
$Lang["libms"]["import"]['BookCat']['fields']['DescriptionChi']['req']=true;

$Lang["libms"]["import"]['BookLocation']['title'] = "Book Location Import";
$Lang["libms"]["import"]['BookLocation']['error_msg']['upload_fail'] = "Upload Fail";
$Lang["libms"]["import"]['BookLocation']['error_msg']['contact_admin'] = "Please contact system administrator";


$Lang["libms"]["import"]['BookLocation']['fields']['BookLocationCode']['name']="Book Location Code";
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionEn']['name'] ="English Description";
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionChi']['name']="Chinese Description";
$Lang["libms"]["import"]['BookLocation']['fields']['BookLocationCode']['req']=true;
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionEn']['req'] =true;
$Lang["libms"]["import"]['BookLocation']['fields']['DescriptionChi']['req']=true;


$Lang["libms"]["import"]['SpeicalOpenTime']['title'] = "Special Opening Time Import";
$Lang["libms"]["import"]['SpeicalOpenTime']['error_msg']['upload_fail'] = "Upload Fail";
$Lang["libms"]["import"]['SpeicalOpenTime']['error_msg']['contact_admin'] = "Please contact system administrator";


$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateFrom']['name']="Date From (YYYY-MM-DD)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateEnd']['name'] ="Date End (YYYY-MM-DD)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['OpenTime']['name']="Open Time (HH:MM:SS)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['CloseTime']['name']="Close Time (HH:MM:SS)";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['Open']['name']="Open 1/ Close 0";
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateFrom']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['DateEnd']['req'] =true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['OpenTime']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['CloseTime']['req']=true;
$Lang["libms"]["import"]['SpeicalOpenTime']['fields']['Open']['req']=true;

$Lang['libms']['import_book']['and'] = "and";
$Lang['libms']['import_book']['book_records'] = "book(s)";
$Lang['libms']['import_book']['item_records'] = "item(s)";
$Lang['libms']['import_book']['DateFormat'] = "<span class='tabletextrequire'>#</span> Date format must be YYYY-MM-DD or DD/MM/YYYY. If you use Excel to edit, please change the date format in Format Cells";
$Lang['libms']['import_book']['RecordStatus'] = "<span class='tabletextrequire'>^</span> Once a book is written-off, its status cannot be changed by import. It can only be handled in Write-Off Management.";


$Lang['General']['WrongDateLogic'] = "Invalid date. Start date must be earlier than End date.";

$Lang["libms"]["periodicalMgmt"]["Periodical"] = "Periodical";
$Lang["libms"]["periodicalMgmt"]["PeriodicalInfo"] = "Periodical Information";
$Lang["libms"]["periodicalMgmt"]["Basic"] = "Basic";
$Lang["libms"]["periodicalMgmt"]["Ordering"] = "Order";
$Lang["libms"]["periodicalMgmt"]["Registration"] = "Registration";
$Lang["libms"]["periodicalMgmt"]["PeriodicalCode"] = "Periodical Code";
$Lang["libms"]["periodicalMgmt"]["PeriodicalTitle"] = "Periodical Title";
$Lang["libms"]["periodicalMgmt"]["ISSN"] = "ISSN";
$Lang["libms"]["periodicalMgmt"]["Alert"] = "Alert";
$Lang["libms"]["periodicalMgmt"]["Status"] = "Status";
$Lang["libms"]["periodicalMgmt"]["DefaultStatus"] = "Default Status";
$Lang["libms"]["periodicalMgmt"]["OrderPeriod"] = "Order Period";
$Lang["libms"]["periodicalMgmt"]["StartDate"] = "Start Date";
$Lang["libms"]["periodicalMgmt"]["EndDate"] = "End Date";
$Lang["libms"]["periodicalMgmt"]["OrderStartDate"] = "Order Start Date";
$Lang["libms"]["periodicalMgmt"]["OrderEndDate"] = "Order End Date";
$Lang["libms"]["periodicalMgmt"]["OrderPrice"] = "Order Price";
$Lang["libms"]["periodicalMgmt"]["PublishFrequency"] = "Publish Frequency";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyPerUnit"] = "Publish every <!--frequencyNum--> <!--frequencyUnit--> per issue";
$Lang["libms"]["periodicalMgmt"]["PublishDate"] = "Publish Date";
$Lang["libms"]["periodicalMgmt"]["FirstIssue"] = "First Issue";
$Lang["libms"]["periodicalMgmt"]["LastIssue"] = "Last Issue";
$Lang["libms"]["periodicalMgmt"]["IssueOrCode"] = "Issue No. / Code";
$Lang["libms"]["periodicalMgmt"]["Remarks"] = "Remarks";
$Lang["libms"]["periodicalMgmt"]["RemarksAndAlert"] = "Remarks & Alert";
$Lang["libms"]["periodicalMgmt"]["RenewAlertDay"] = "Renew Alert Day(s)";
$Lang["libms"]["periodicalMgmt"]["RenewAlertDayRemarks"] = "0 means no alert";
$Lang["libms"]["periodicalMgmt"]["Generate"] = "Generate";
$Lang["libms"]["periodicalMgmt"]["Generated"] = "Generated";
$Lang["libms"]["periodicalMgmt"]["NotGeneratedYet"] = "Not Generated Yet";
$Lang["libms"]["periodicalMgmt"]["GenerateStatus"] = "Generate Status";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateRemarks"] = "System will generate the following periodical items.";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning1"] = "This order has generated item(s) already. System will delete the current periodical item(s) related to this order and re-generate new periodical item(s).";
$Lang["libms"]["periodicalMgmt"]["OrderGenerateWarning2"] = "Registered book(s) will not be affected. System will re-generate the periodical-related data only.";
$Lang["libms"]["periodicalMgmt"]["From"] = "From";
$Lang["libms"]["periodicalMgmt"]["To"] = "To";
$Lang["libms"]["periodicalMgmt"]["NoIssueCanBeGenerated"] = "No issues can be generated based on the order settings";
$Lang["libms"]["periodicalMgmt"]["PeriodicalRenewAlert"] = "Periodical Renew Alert";
$Lang["libms"]["periodicalMgmt"]["LastPublishDate"] = "Last Publish Date";
$Lang["libms"]["periodicalMgmt"]["Register"] = "Register";
$Lang["libms"]["periodicalMgmt"]["Registered"] = "Registered";
$Lang["libms"]["periodicalMgmt"]["NotRegistered"] = "Not Registered Yet";
$Lang["libms"]["periodicalMgmt"]["RegistrationStatus"] = "Registration Status";
$Lang["libms"]["periodicalMgmt"]["RegisteredItemNum"] = "Registered Item No.";
$Lang["libms"]["periodicalMgmt"]["RegistrationDate"] = "Registration Date";
$Lang["libms"]["periodicalMgmt"]["LastRegistrationDate"] = "Last Registration Date";
$Lang["libms"]["periodicalMgmt"]["Details"] = "Details";
$Lang["libms"]["periodicalMgmt"]["NoOfOrder"] = "No. of order";
$Lang["libms"]["periodicalMgmt"]["NoOfItem"] = "No. of item";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Day"] = "Day(s)";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Week"] = "Week(s)";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Month"] = "Month(s)";
$Lang["libms"]["periodicalMgmt"]["PublishFrequencyAry"]["Year"] = "Year(s)";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SelectItem"] = "Please select an item";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputValue"] = "Please input a value";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["InputPositiveNum"] = "Please input a positive value";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["CodeInUse"] = "The code is in-use already";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["PublishDateInUse"] = "The publish date is occupied already";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["SaveBasicInfoFirst"] = "Please save basic info first";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["FormChanged"] = "The updated values will not be saved if you leave the page now. Are you sure you want to leave the page?";
$Lang["libms"]["periodicalMgmt"]["WarningMessage"]["StartDateMustBeEarlierThanEndDate"] = "Start date must be earlier than end date";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["AddSuccess"] = "1|=|Record Added.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["AddUnsuccess"] = "0|=|Record Addition Failed.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["UpdateSuccess"] = "1|=|Record Updated.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["UpdateUnsuccess"] = "0|=|Record Update Failed.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["DeleteSuccess"] = "1|=|Record Deleted.";
$Lang["libms"]["periodicalMgmt"]["ReturnMessage"]["DeleteUnsuccess"] = "0|=|Record Delete Failed.";

$Lang['libms']['NoRecordAtThisMoment'] = "There is no record at this moment.";

$Lang["libms"]["import_book_new"]["ImportCSVDataCol"] = array("<font color='red'>*</font>Book ID","<font color='red'>*</font>Book Title", "<font color='red'>*</font>Author", "Series Editor","Category","SubCategory","Chinese Library Classification", "Dewey Decimal Classification", "Level", "Publisher", "Language", "Published", "Edition", "ePublisher", "Preface", "ISBN");


$Lang['General']['ReturnMessage']['FailedToDelLoanAsPaymentSettled'] = '0|=|Record Delete Failed (because the over-due payment was settled)';
$Lang['General']['ReturnMessage']['CannotDeleteOnLoanACNO'] = "0|=|Cannot delete on-loan books: ACNO %s";
$Lang['General']['ReturnMessage']['CannotDeleteOnLoanBook'] = "0|=|Cannot delete on-loan books: %s";
$Lang['General']['ReturnMessage']['ItemStatusUpdateUnsuccess'] = '0|=|Record Update Failed. [Write-off] item cannot be updated.';
$Lang['General']['ReturnMessage']['UpdateSuccessWithWriteOffItem'] = '1|=|Record Updated. (excepted [Write-off] status)';
$Lang["libms"]["general"]["not_need_to_reply_email"] = "Not need to reply this email";

$Lang['libms']["settings_alert"]['SameDayReturn'] = "Cannot return at the same day "; //return msg 
$Lang['libms']['S_OpenTime']['SameDayReturn'] = "Return at the same day is not allowed ";  // settings string
$Lang['libms']["settings"]['system']['waive_password'] = "Waive charges via Override Password";
$Lang["libms"]["CirculationManagement"]["teacher_only"] = "or teacher selection"; // Teacher Selection

$Lang['libms']["settings"]['system']['circulation_teacher_by_selection'] = "Select a teacher from teacher list";
$Lang['libms']["settings"]['system']['circulation_display_accompany_material'] = "Display Note for Circulation together with Accompany Material";

$Lang["libms"]["UserSuspend"]["ModuleTitle"] = "Suspended Users"; 
$Lang["libms"]["UserSuspend"]["Suspend"] = "Suspend"; 
$Lang["libms"]["UserSuspend"]["Activate"] = "Activate"; 
$Lang["libms"]["UserSuspend"]["Instruction"] = "Select and add user(s) from User List.<br/>Suspended Users are not allowed to borrow books.";
$Lang["libms"]["UserSuspend"]["UserList"] = "User List";
$Lang["libms"]["UserSuspend"]["Suspended"] = "Access denied due to this user has been suspended.";
$Lang["libms"]["UserSuspend"]["SuspendedList"] = "Suspended List";
$Lang["libms"]["UserSuspend"]["SuspendedStatus"] = "Suspended";
$Lang["libms"]["UserSuspend"]["SuspendedBy"] = "Suspended By";
$Lang["libms"]["UserSuspend"]["SuspendedDate"] = "Suspended Date";
$Lang["libms"]["UserSuspend"]["thisSelectedUser"] = "Suspended users in this time";
$Lang["libms"]["UserSuspend"]["UserList"] = "User List";

$Lang["libms"]["SearchUser"]["ModuleTitle"] = "Search by User";
$Lang["libms"]["SearchUser"]["Instruction"] = "Select user(s) from filters or by enter keywords";
$Lang["libms"]["SearchUser"]["AllClasses"] = "All Classes";
$Lang["libms"]["SearchUser"]["Remark"] = "Remarks";
$Lang["libms"]["SearchUser"]["RemarkSave"] = "Updated!";
$Lang["libms"]["SearchUser"]["RemarkSaveFail"] = "Failed to update, please try again!";
$Lang["libms"]["SearchUser"]["RemarkSaveConfirm"] = "Are you sure to save this remarks?";
$Lang["libms"]["SearchUser"]["UpdateRemarks"] = "Update";
$Lang["libms"]["SearchUser"]["hasNoRemark"] = "No";
$Lang["libms"]["SearchUser"]["hasRemark"] = "Yes";
$Lang["libms"]["SearchUser"]["AllUserType"] = "All User Identity";

$Lang["libms"]["SearchUser"]["ReturnBookTitle"] = "Loan";
$Lang["libms"]["SearchUser"]["ReturnBookReturnMsgFail"] = "Books have not been returned:\n";
$Lang["libms"]["SearchUser"]["ReturnBookConfirm"] = "Are you sure to return selected book(s)?";

$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['BORROWED'] = "Borrowing";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['LOST'] = "Lost";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['RENEWED'] = "Renewed";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['RETURNED'] = "Returned";
$Lang["libms"]["SearchUser"]["ReturnBookStatus"]['Overdue'] = "Overdue";

$Lang["libms"]["SearchUser"]["OverdueWaiveConfirm"] = "Are you sure to waive selected charges?";
$Lang["libms"]["SearchUser"]["OverduePayConfirm"] = "Are you sure to pay selected charges?";
$Lang["libms"]["SearchUser"]["OverduePay"] = "Pay";

$Lang["libms"]["SearchUser"]["CancelReservedConfirm"] = "Are you sure to cancel selected reservation(s) ?";
$Lang["libms"]["SearchUser"]["CancelReserved"] = $Lang["libms"]["CirculationManagement"]["cancel"].' '.$Lang["libms"]["CirculationManagement"]["reserve"];

$Lang["libms"]["eBookStat"]["ModuleTitle"] = "eBooks Statistics";
$Lang["libms"]["eBookStat"]["TotalBooks"] = "Number of Book(s)";
$Lang["libms"]["eBookStat"]["TotalReads"] = "Total hits";
$Lang["libms"]["eBookStat"]["TotalReviews"] = "Book Review(s)";
$Lang["libms"]["eBookStat"]["LastReads"] = "Last Read";
$Lang["libms"]["eBookStat"]["Back"] = "Class/Group List";
$Lang["libms"]["eBookStat"]["ReviewContent"] = "Review Content";
$Lang["libms"]["eBookStat"]["ReviewRating"] = "Rating";
$Lang["libms"]["eBookStat"]["ReviewHelpful"] = "Like(s)";
$Lang["libms"]["eBookStat"]["ReviewDate"] = "Date";
$Lang["libms"]["eBookStat"]["incAll"] = "Show all users in Group/Class (Has neither book review nor reading record)";
$Lang["libms"]["eBookStat"]["showReadRecordOnly"] = "Show users having book review or reading record only";
$Lang["libms"]["eBookStat"]["showPercentCol"] = "Show readers with progress reaches";
$Lang["libms"]["eBookStat"]["showPercentCol2"] = "";
$Lang["libms"]["eBookStat"]["showPercentDownCol"] = "Show readers with progress less than";
$Lang["libms"]["eBookStat"]["totalBooksByPercentage"] = "Total number of books with progress reaches %s%%";
$Lang["libms"]["eBookStat"]["totalReadByPercentage"] = "Total number of hits with progress reaches %s%%";
$Lang["libms"]["eBookStat"]["totalBooksByPercentageDown"] = "Total number of books with progress less than %s%%";
$Lang["libms"]["eBookStat"]["totalReadByPercentageDown"] = "Total number of hits with progress less than %s%%";

$Lang["libms"]["ajaxLoading"] = "Loading ...";
$Lang["libms"]["tableNoCheckedCheckbox"] = "Please select at least one item";
$Lang["libms"]["transfer_book"]["action_completed"] = "The requested action has been completed";
$Lang["libms"]["transfer_book"]["add_setting_record"] = "Add setting record";
$Lang["libms"]["transfer_book"]["book_transfer_format"] = "Transfer Format";
$Lang["libms"]["transfer_book"]["confirm"]['add_setting_record'] = "Are you sure to add setting record?";
$Lang["libms"]["transfer_book"]["confirm"]['drop_ceo_import_table'] = "Are you sure to drop ceo import table?";
$Lang["libms"]["transfer_book"]["confirm"]['drop_tmp_import_table'] = "Are you sure to drop temp import table?";
$Lang["libms"]["transfer_book"]["data_copy_to_db"] = "Data has been imported to temp table, number of record ready to export in eLibrary Plus format: ";
$Lang["libms"]["transfer_book"]["delete_old_data"] = "Delete Old Data";
$Lang["libms"]["transfer_book"]["drop_ceo_import_table"] = "Drop ceo Import Table";
$Lang["libms"]["transfer_book"]["drop_tmp_import_table"] = "Drop temp Import Table";
$Lang["libms"]["transfer_book"]["export_elibplus_format"] = "Export eLibrary Plus format for import";
$Lang["libms"]["transfer_book"]["total_error_rows"] = "Total number of wrong rows: ";
$Lang["libms"]["transfer_book"]["total_insert_error_rows"] = "Total number of wrong rows when insert into temp table: ";
$Lang["libms"]["transfer_book"]["select_school"] = "Please select school";
$Lang["libms"]["portal"]["add_news"] = "Add News";
$Lang["libms"]["portal"]["admin_settings"] = "Admin Settings";
$Lang["libms"]["portal"]["advanced_search"] = "Advanced Search";
$Lang["libms"]["portal"]["advancedSearch"]["sortBy"]["bookID"] = "BookID";
$Lang["libms"]["portal"]["all"] = "All";
$Lang["libms"]["portal"]["best_rated"] = "Best Rated";
$Lang["libms"]["portal"]["book"]["I_shared_to"] = "I shared this book to ";
$Lang["libms"]["portal"]["book"]["total_reviews"] = "Reviews (%s)";
$Lang["libms"]["portal"]["book"]["total_shared_by_me"] = "Shared by me (%s)";
$Lang["libms"]["portal"]["book"]["total_shared_to_me"] = "Shared to me (%s)";
$Lang["libms"]["portal"]["book_detail"]["cancel_reserved_confirm"] = "Are you sure to cancel this book reservation?";
$Lang["libms"]["portal"]["book_details"] = "Details";
$Lang["libms"]["portal"]["book_title"] = "Book";
$Lang["libms"]["portal"]["books_found"] = "book(s) found";
$Lang["libms"]["portal"]["browse_by_categories"] = "Browse by Categories";
$Lang["libms"]["portal"]["borrowed"] = "Borrowed";
$Lang["libms"]["portal"]["closing"] = "Closed";
$Lang["libms"]["portal"]["demo"]["read_info"] = "悅讀資訊";
$Lang["libms"]["portal"]["eBooks"] = "eBooks";
$Lang["libms"]["portal"]["edit_news"] = "Edit News";
$Lang["libms"]["portal"]["edit_rules"] = "Edit Rules";
$Lang["libms"]["portal"]["expand"] = "expand";
$Lang["libms"]["portal"]["eBooks_ranking"] = "eBooks Ranking";
$Lang["libms"]["portal"]["hide"] = "Hide";
$Lang["libms"]["portal"]["me"] = "Me";
$Lang["libms"]["portal"]["month"]["Jan"] = "Jan";
$Lang["libms"]["portal"]["month"]["Feb"] = "Feb";
$Lang["libms"]["portal"]["month"]["Mar"] = "Mar";
$Lang["libms"]["portal"]["month"]["Apr"] = "Apr";
$Lang["libms"]["portal"]["month"]["May"] = "May";
$Lang["libms"]["portal"]["month"]["Jun"] = "Jun";
$Lang["libms"]["portal"]["month"]["Jul"] = "Jul";
$Lang["libms"]["portal"]["month"]["Aug"] = "Aug";
$Lang["libms"]["portal"]["month"]["Sep"] = "Sep";
$Lang["libms"]["portal"]["month"]["Oct"] = "Oct";
$Lang["libms"]["portal"]["month"]["Nov"] = "Nov";
$Lang["libms"]["portal"]["month"]["Dec"] = "Dec";
$Lang["libms"]["portal"]["most_active_readers"] = "Most Active Readers";
$Lang["libms"]["portal"]["most_hit"] = "Most Hit";
$Lang["libms"]["portal"]["most_loan"] = "Most Loan";
$Lang["libms"]["portal"]["my_friend"] = "My Friend";
$Lang["libms"]["portal"]["my_friends"] = "My Friends";
$Lang["libms"]["portal"]["my_record"]["cancel_reserve_fail"] = "Cancel reserve fail";
$Lang["libms"]["portal"]["my_record"]["checkout_due_date"] = "Check out Due Date";
$Lang["libms"]["portal"]["myfriends"]["accept"] = "Accept";
$Lang["libms"]["portal"]["myfriends"]["accept_friend_request"] = " has accepted your friend request.";
$Lang["libms"]["portal"]["myfriends"]["add_friends"] = "Add Friends";
$Lang["libms"]["portal"]["myfriends"]["back"] = "BACK";
$Lang["libms"]["portal"]["myfriends"]["book_total"] = "book(s)";
$Lang["libms"]["portal"]["myfriends"]["borrow_a_book"] = " borrowed a book.";
$Lang["libms"]["portal"]["myfriends"]["comments"] = "Comments";
$Lang["libms"]["portal"]["myfriends"]["ebook_total"] = "ebook(s)";
$Lang["libms"]["portal"]["myfriends"]["friend_profile"] = "Friend Profile";
$Lang["libms"]["portal"]["myfriends"]["friends_activities"] = "Friends' Activities";
$Lang["libms"]["portal"]["myfriends"]["friends_list"] = "Friend List";
$Lang["libms"]["portal"]["myfriends"]["at"] = "at";
$Lang["libms"]["portal"]["myfriends"]["identity"]["NonTeachingStaff"] = "Non-teaching Staff";
$Lang["libms"]["portal"]["myfriends"]["identity"]["Staff"] = "Staff";
$Lang["libms"]["portal"]["myfriends"]["identity"]["TeachingStaff"] = "Teaching Staff";
$Lang["libms"]["portal"]["myfriends"]["ignore"] = "Ignore";
$Lang["libms"]["portal"]["myfriends"]["new"] = "new";
$Lang["libms"]["portal"]["myfriends"]["no_friends"] = "There is no friends";
$Lang["libms"]["portal"]["myfriends"]["no_friends_activities"] = "There is no friend's activity.";
$Lang["libms"]["portal"]["myfriends"]["no_notifications"] = "There is no notice.";
$Lang["libms"]["portal"]["myfriends"]["notifications"] = "Notice";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["ebooks_viewed"] = "eBooks Viewed";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["books_borrowed"] = "Books Borrowed";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["reviews"] = "Reviews";
$Lang["libms"]["portal"]["myfriends"]["profile_tab"]["sharing"] = "Sharing";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["borrowers"] = "Borrowers";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["ebook"] = " eBook(s)";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_borrowing"] = "There is no ranking in friend's borrowing.";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_reading"] = "There is no ranking in friend's reading.";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["no_friends_review"] = "There is no ranking in friend's review.";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["pbook"] = "book(s)";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["review"] = "review(s)";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["reviewers"] = "Reviewers";
$Lang["libms"]["portal"]["myfriends"]["ranking"]["viewers"] = "Viewers";
$Lang["libms"]["portal"]["myfriends"]["reading_date"] = "Reading Date";
$Lang["libms"]["portal"]["myfriends"]["remove_request"] = "Remove Request";
$Lang["libms"]["portal"]["myfriends"]["review_a_book"] = " reviewed a book.";
$Lang["libms"]["portal"]["myfriends"]["search_friends"] = "Search Friends";
$Lang["libms"]["portal"]["myfriends"]["selected_friends"] = "Selected (%s)";
$Lang["libms"]["portal"]["myfriends"]["send_friend_request"] = " sent you a friend request.";
$Lang["libms"]["portal"]["myfriends"]["share_a_book"] = " shared a book to you.";
$Lang["libms"]["portal"]["myfriends"]["share_a_book_to_me"] = " shared a book to me.";
$Lang["libms"]["portal"]["myfriends"]["share_a_book_by_me"] = "I shared a book to ";
$Lang["libms"]["portal"]["myfriends"]["shared_by"] = "Shared by";
$Lang["libms"]["portal"]["myfriends"]["status"]["wainting_for_accept"] = "Waiting for accept";
$Lang["libms"]["portal"]["myfriends"]["summary"]["books_borrowed"] = "Books borrowed";
$Lang["libms"]["portal"]["myfriends"]["summary"]["ebook_viewed"] = "eBooks viewed";
$Lang["libms"]["portal"]["myfriends"]["summary"]["no_of_reviews"] = "No. of reviews";
$Lang["libms"]["portal"]["myfriends"]["unfriend"] = "Unfriend";
$Lang["libms"]["portal"]["myfriends"]["warning"]["add_friends"] = "Only student or teacher can add friends";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_delete_notification"] = "Are you sure to delete this notification?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_delete_shared_to_me"] = "Are you sure to delete this shared to me?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_remove_friend_request"] = "Are you sure to delete this friend request?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["confirm_unfriend"] = "Are you sure to unfriend this student?";
$Lang["libms"]["portal"]["myfriends"]["warning"]["select_at_least_one_friend"] = "Please select at least one friend!";
$Lang["libms"]["portal"]["myfriends"]["warning"]["select_class"] = "Please select class";
$Lang["libms"]["portal"]["myfriends"]["warning"]["select_friend"] = "Please select friend's name";
$Lang["libms"]["portal"]["navigation"]["display_per_page"] = "Display";
$Lang["libms"]["portal"]["navigation"]["item"] = "/Page";
$Lang["libms"]["portal"]["navigation"]["num_name"] = "";
$Lang["libms"]["portal"]["navigation"]["page"] = "Page";
$Lang["libms"]["portal"]["news"]["start_date_empty"] = "Start Date cannot be empty";
$Lang["libms"]["portal"]["news"]["end_date_empty"] = "End Date cannot be empty";
$Lang["libms"]["portal"]["next_week"] = "Next Week";
$Lang["libms"]["portal"]["not_open"] = "Not Open";
$Lang["libms"]["portal"]["off_shelf"] = "Discontinued";
$Lang["libms"]["portal"]["opening"] = "Open";
$Lang["libms"]["portal"]["pBooks"] = "Physical Books";
$Lang["libms"]["portal"]["pBooks_ranking"] = "Physical Books Ranking";
$Lang["libms"]["portal"]["pinned"] = "Pinned";
$Lang["libms"]["portal"]["previous_week"] = "Previous Week";
$Lang["libms"]["portal"]["quota"]["loan"] = "Quota";
$Lang["libms"]["portal"]["quota"]["reserve"] = "Quota";
$Lang["libms"]["portal"]["reader"] = "Reader";
$Lang["libms"]["portal"]["recommend"] = "Recommend";
$Lang["libms"]["portal"]["recommendbookto"] = "Recommended to";
$Lang["libms"]["portal"]["related_books"] = "Related Books";
$Lang["libms"]["portal"]["require_field"] = "<span style='color: red'> * </span>Mandatory field(s)";
$Lang["libms"]["portal"]["reserved"] = "Reserved";
$Lang["libms"]["portal"]["result_list"]["hits_loans"] = "Hits/Loans";
$Lang["libms"]["portal"]["reviews"] = "Reviews";
$Lang["libms"]["portal"]["settings"] = "Portal Settings";
$Lang["libms"]["portal"]["settings_tab_internal"] = "Internal";
$Lang["libms"]["portal"]["settings_tab_opac"] = "OPAC";
$Lang["libms"]["portal"]["setting"]["announcement"] = "Announcement";
$Lang["libms"]["portal"]["setting"]["book_category"] = "Book Category";
$Lang["libms"]["portal"]["setting"]["carousel"] = "Carousel";
$Lang["libms"]["portal"]["setting"]["carousel_ebook_inteval"] = "Interval for eBook Carousel";
$Lang["libms"]["portal"]["setting"]["carousel_pbook_inteval"] = "Interval for physical book Carousel";
$Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_min"] = "Minute(s)";
$Lang["libms"]["portal"]["setting"]["carousel_inteval_unit_sec"] = "Second(s)";
$Lang["libms"]["portal"]["setting"]["carousel_remark"] = "Carousel sliding is automatically stopped if internal is set to zero";
$Lang["libms"]["portal"]["setting"]["carousel_stop"] = "Stop carousel sliding";
$Lang["libms"]["portal"]["setting"]["data_range"] = "Data Range";
$Lang["libms"]["portal"]["setting"]["display_advanced_search"] = "Display Advanced Search";
$Lang["libms"]["portal"]["setting"]["display_best_rated"] = "Display best rated books";
$Lang["libms"]["portal"]["setting"]["display_category_chi_ebooks"] = "Display Chinese eBook category";
$Lang["libms"]["portal"]["setting"]["display_category_eng_ebooks"] = "Display English eBook category";
$Lang["libms"]["portal"]["setting"]["display_category_physical_books"] = "Display physical book category";
$Lang["libms"]["portal"]["setting"]["display_category_tags"] = "Display tags";
$Lang["libms"]["portal"]["setting"]["display_keyword_search"] = "Display Keyword Search";
$Lang["libms"]["portal"]["setting"]["display_list_all_books"] = "Display list all books";
$Lang["libms"]["portal"]["setting"]["display_most_active_borrowers"] = "Display Most Active Borrowers";
$Lang["libms"]["portal"]["setting"]["display_most_active_reviewers"] = "Display Most Active Reviewers";
$Lang["libms"]["portal"]["setting"]["display_most_helpful_reviews"] = "Display Most Helpful Reviews";
$Lang["libms"]["portal"]["setting"]["display_most_hit"] = "Display most hit books";
$Lang["libms"]["portal"]["setting"]["display_most_loan"] = "Display most loan books";
$Lang["libms"]["portal"]["setting"]["display_new"] = "Display new books";
$Lang["libms"]["portal"]["setting"]["display_news"] = "Display news";
$Lang["libms"]["portal"]["setting"]["display_opening_hours"] = "Display opening hours";
$Lang["libms"]["portal"]["setting"]["display_recommend"] = "Display recommend books";
$Lang["libms"]["portal"]["setting"]["display_rules"] = "Display rules";
$Lang["libms"]["portal"]["setting"]["edit_settings"] = "Edit Settings";
$Lang["libms"]["portal"]["setting"]["keyword_search"] = "Keyword Search";
$Lang["libms"]["portal"]["setting"]["my_friends"] = "My Friends";
$Lang["libms"]["portal"]["setting"]["my_friends_search_scope"] = "My friends scope of students";
$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["all_classes"] = "All classes";
$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_class"] = "Same class";
$Lang["libms"]["portal"]["setting"]["my_friends_scope"]["same_classlevel"] = "Same class level";
$Lang["libms"]["portal"]["setting"]["opac_allow_search_ebook"] = "Allow searching eBooks";
$Lang["libms"]["portal"]["setting"]["opac_remark_allow_search_ebook"] = "Please set \"Allow searching eBooks\" to Yes";
$Lang["libms"]["portal"]["setting"]["open_my_friends"] = "Open My Friends function";
$Lang["libms"]["portal"]["setting"]["open_opac"] = "Open OPAC";
$Lang["libms"]["portal"]["setting"]["others"] = "Others";
$Lang["libms"]["portal"]["setting"]["ranking"] = "Ranking";
$Lang["libms"]["portal"]["share"] = "Share";
$Lang["libms"]["portal"]["statistics"]["totalHitsChi"] = "Total hits (Chi)";
$Lang["libms"]["portal"]["statistics"]["totalHitsEng"] = "Total hits (Eng)";
$Lang["libms"]["portal"]["temp_close"] = "Closed";
$Lang["libms"]["portal"]["theme_settings"] = "Theme Settings";
$Lang["libms"]["portal"]["totalRecords"] = "records";
$Lang["libms"]["portal"]["type_here"] = "Type here...";
$Lang["libms"]["portal"]["warning"]["allow_edit_one_review_only"] = "Allow editing one review only at a time";
$Lang["libms"]["portal"]["warning"]["fail_del_tmp_attachment"] = "Fail to remove temporary attachment!";
$Lang["libms"]["opac"]["member_login"] = "Login";
$Lang["libms"]["iPortfolio"]["BookCategory"] = "Category";
$Lang["libms"]["iPortfolio"]["BorrowReadDate"] = "Borrow/Read Date";
$Lang["libms"]["iPortfolio"]["PhysicalOrEBook"] = "Physical Books/eBooks";
?>