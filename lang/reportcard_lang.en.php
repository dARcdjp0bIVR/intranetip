<?php 


$eReportCard['GradingScheme'] = "Grading Scheme";

$eReportCard['SchemeSettings'] = "Scheme Settings";
$eReportCard['SchemeSettings_GradingSchemes'] = "Grading Schemes Setting";
$eReportCard['SchemeSettings_SubjectGradings'] = "Subject Grading Setting";
$eReportCard['SchemeSettings_CalMethod'] = "Calculation Setting";
$eReportCard['SchemeSettings_Highlights'] = "Highlights Setting";

$eReportCard['ReportBuilder'] = "Report Builder";
$eReportCard['ReportBuilder_Template'] = "Report Template Builder";
$eReportCard['ReportBuilder_TitlesCustomization'] = "Report Titles Customization";

$eReportCard['MonitorMarksheetSubmissionProgress'] = "Marksheet Submission Progress";
$eReportCard['MonitorResultVerificationProgress'] = "Result Verification Progress";
$eReportCard['ReportGeneration'] = "Report Generation";
$eReportCard['MarksheetCollection'] = "Marksheet Collection";
$eReportCard['MarksheetCollectionPeriodSetting'] = "Marksheet Collection Settings";
$eReportCard['ResultVerificationPeriodSetting'] = "Result Verification Setting";
$eReportCard['DefaultSubjectGradings'] = "Default Subject Grading";
$eReportCard['SpecialSubjectGradings'] = "Special Subject Grading";
$eReportCard['ViewGradingSchemeDetails'] = "View Grading Scheme Details";
$eReportCard['GradingSchemeDetails'] = "Grading Scheme Details";
$eReportCard['MarksheetSubmission'] = "Marksheet Submission";
$eReportCard['VerifyAssessmentResult'] = "Result Verification";
$eReportCard['MarksheetRevision'] = "Marksheet Revision";

$eReportCard['CalculationMethod'] = "Calculation Method";
$eReportCard['CalculationMethodRule'] = "Adjust Full mark of each subject to 100 before applying weight";
$eReportCard['CalculationOrder'] = "Calculation Order";
$eReportCard['CalculationOrderHorizontal'] = "Use term subject mark calculated from all components<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Use term average mark calculated from all subjects<br>&nbsp;";
$eReportCard['CalculationOrderVertical'] = "Use mark of each component calculated from all assessments<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Use mark of each subject calculated from all terms";

$eReportCard['Scheme'] = "Scheme";
$eReportCard['Pass'] = "Pass";
$eReportCard['Fail'] = "Fail";
$eReportCard['LowerLimit'] = "Lower Limit";
$eReportCard['Grade'] = "Grade";
$eReportCard['GradePoint'] = "GradePoint";
$eReportCard['Distinction'] = "Distinction";
$eReportCard['HighlightOverall'] = "Apply Highlights to";
$eReportCard['Weight'] = "Weight";
$eReportCard['WeightingGrandTotal'] = "Weighting to Grand Total";
$eReportCard['AddMore'] = "Add More";
$eReportCard['AddNewRecord'] = "Add New Record";
$eReportCard['Form'] = "Form";
$eReportCard['Forms'] = "Form(s)";
$eReportCard['SelectForms'] = "Select Form(s):";
$eReportCard['FullYear'] = "Full Year";
$eReportCard['ReportType'] = "Report Type";
$eReportCard['WeightedOverallResult'] = "Weighted Overall Result";
$eReportCard['AddSubject'] = "Add Subject";
$eReportCard['Beginning'] = "Beginning";
$eReportCard['SetBasicInfo'] = "Set Basic Information";
$eReportCard['DeleteAllSubject'] = "Delete All Subjects";
$eReportCard['Private'] = "Private";
$eReportCard['Public'] = "Public";
$eReportCard['Column'] = "Column";
$eReportCard['AddColumn'] = "Add Column";
$eReportCard['RemoveColumn'] = "Remove Column";
$eReportCard['EditColumn'] = "Edit Column";
$eReportCard['ColumnTitle'] = "Column Title";
$eReportCard['ShowFullMark'] = "Show Full Mark";
$eReportCard['HideOverallResult'] = "Hide Overall Result";
$eReportCard['ShowPosition'] = "Show Position";
$eReportCard['ShowOverallPosition'] = "Show Overall Position";
$eReportCard['HidePosition'] = "Do not Show Position";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";
$eReportCard['PositionRange'] = "Position Range";
$eReportCard['EditSubjectPos'] = "Edit Subject Position";
$eReportCard['ColumnManage'] = "Manage Column";
$eReportCard['SubjectManage'] = "Manage Subject";
$eReportCard['NoRecord'] = "No Record";
$eReportCard['StartDate'] = "Start Date";
$eReportCard['EndDate'] = "End Date";
$eReportCard['Bold'] = "Bold";
$eReportCard['Italic'] = "Italic";
$eReportCard['Underline'] = "Underline";
$eReportCard['Color'] = "Color";
$eReportCard['Red'] = "Red";
$eReportCard['Green'] = "Green";
$eReportCard['Blue'] = "Blue";
$eReportCard['Notification'] = "Notification";
$eReportCard['Feedback'] = "Feedback";
$eReportCard['Feedbacks'] = "Feedback(s)";
$eReportCard['StudentFeedback'] = "Student Feedback";
$eReportCard['MyRequest'] = "My Request";
$eReportCard['Subject'] = "Subject";
$eReportCard['SubjectTitle'] = "Subject";
$eReportCard['Class'] = "Class";
$eReportCard['NotifyStudent'] = "Notify Student";
$eReportCard['HideNotification'] = "Hide Notification";
$eReportCard['SelectSubject'] = "Select Subject";
$eReportCard['SubmitMarksheet'] = "Submit Marksheet";
$eReportCard['OverallResult'] = "Overall Result";
$eReportCard['File'] = "File";
$eReportCard['DownloadCSVFile'] = "Download CSV file template";
$eReportCard['MainMarksheet'] = "Main Marksheet";
$eReportCard['Deadline'] = "Deadline";
$eReportCard['Appeal'] = "Appeal";
$eReportCard['Appeals'] = "Appeal(s)";
$eReportCard['ViewResult'] = "View Result";
$eReportCard['Close'] = "Close";
$eReportCard['SendingDate'] = "Sending Date";
$eReportCard['EditMarksheet'] = "Edit Marksheet";
$eReportCard['MarkAllRead'] = "Mark All Read";
$eReportCard['Report'] = "Report";
$eReportCard['Reports'] = "Reports";
$eReportCard['Confirmed'] = "Completed";
$eReportCard['RankingType'] = "How to display same rankings";
$eReportCard['ResultDisplayType'] = "Result Display Type";
$eReportCard['MarksheetSubmissionPeriod'] = "Marksheet Submission Period";
$eReportCard['ResultVerificationPeriod'] = "Result Verification Period";
$eReportCard['ImportMarks'] = "Import Marks";
$eReportCard['ExportMarks'] = "Export Marks";
$eReportCard['GradingType'] = "Grading Type";
$eReportCard['SummaryTable'] = "Summary Table";
$eReportCard['SetTableDetails'] = "Set Table Details";
$eReportCard['TableDetails'] = "Table Details";
$eReportCard['CalculateOverallResult'] = "Calculate Overall Result";
$eReportCard['ConfirmMarksheet'] = "Complete Marksheet";
$eReportCard['ShowMainMarksheet'] = "Show Main Marksheet";
$eReportCard['ShowAppealStudentOnly'] = "Show Appealing Student Only";
$eReportCard['ShowClassStudentStatus'] = "Show Class Student Status";
$eReportCard['Generate'] = "Generate";
$eReportCard['LastGenerated'] = "Last Generated";
$eReportCard['Transit'] = "Transit";
$eReportCard['ReportPrinting'] = "Report Printing";
$eReportCard['VerificationReportPriting'] = "Print Result Verification Report";
$eReportCard['TransitData'] = "Data Transition";
$eReportCard['TransitDataRemind'] = "Please proceed this action before starting the assessment reports for a new semester. The assessment data from the previous semester will be preserved.";
$eReportCard['NoReport'] = 'There is no report at the moment.';
$eReportCard['LastModifiedDate'] = "Last Modified Date";
$eReportCard['LastModifiedBy'] = "Last Modified By";
$eReportCard['ViewScoreList'] = "View Score List";
$eReportCard['ViewScoreSummary'] = "View Score Summary";
$eReportCard['ScoreList'] = "Score List";
$eReportCard['ScoreSummary'] = "Score Summary";
$eReportCard['PrintPage'] = "Print Page";
$eReportCard['TeacherComment'] = "Teacher Comment";
$eReportCard['AllowClassTeacherComment'] = "Allow class teacher's comment";
$eReportCard['AllowSubjectTeacherComment'] = "Allow subject teacher's comment";
$eReportCard['Comment'] = "Comment";
$eReportCard['ImportClassTeacherComment'] = "Import Class Teacher Comment";
$eReportCard['ImportSubjectTeacherComment'] = "Import Subject Teacher Comment";
$eReportCard['TextInputMode'] = "Text Input Mode";
$eReportCard['SelectionListMode'] = "Selection List Mode";
$eReportCard['ComponentSubjectSettingMsg'] = "If there are component subjects in the report:";
$eReportCard['ResultDisplayType1'] = "Display the overall result of Parent Subject AND Component Subjects";
$eReportCard['ResultDisplayType2'] = "Display the overall result of Parent Subject ONLY";
$eReportCard['ResultDisplayType3'] = "Display the overall result of Component Subjects ONLY";
$eReportCard['RankingType1'] = "Ranking Type I (e.g 1, 2, 3, 3, 4, 5)";
$eReportCard['RankingType2'] = "Ranking Type II (e.g 1, 2, 3, 3, 5, 6)";
$eReportCard['ResultCalType'] = "Result Calculation Type";
$eReportCard['ParentSubjectCalMethod1'] = "Calculate the Parent Subject result by the raw marks of Component Subjects";
$eReportCard['ParentSubjectCalMethod2'] = "Calculate the Parent Subject result by the overall results of Component Subjects";
$eReportCard['DataDeletion'] = "Data Deletion";
$eReportCard['DataDeletionRemind'] = "This process will remove assessment data including teacher comments.";
$eReportCard['Period'] = "Period";
$eReportCard['NumberOfRecord'] = "Number of Record";
$eReportCard['SubjectTitleDisplay'] = "Subject Title Display";
$eReportCard['SubjectTitleDisplayBoth'] = "Display BOTH English Title and Chinese Title";
$eReportCard['SubjectTitleDisplayChi'] = "Display Chinese Title ONLY";
$eReportCard['SubjectTitleDisplayEng'] = "Display English Title ONLY";
$eReportCard['ShowColumnPercentage'] = "Show Column Percentage";
$eReportCard['NotAllowToInput'] = "Not allow to input";
### New ###
$eReportCard['SelectAllAs'] = "Select All As";
$eReportCard['DefaultTitle'] = "Default Title";
$eReportCard['CustomTitle'] = "Custom Title";
$eReportCard['Chi'] = "Chi";
$eReportCard['Eng'] = "Eng";
$eReportCard['InfoUpload'] = "Info Upload";
$eReportCard['SummaryInfoUpload'] = "Summary Info Upload";
$eReportCard['AwardRecordUpload'] = "Awards Record Upload";
$eReportCard['MeritRecordUpload'] = "Merits & Demerits Record Upload";
$eReportCard['ECARecordUpload'] = "ECA Record Upload";
$eReportCard['RemarkUpload'] = "Remark Upload";

$eReportCard['InterSchoolCompetitionUpload'] = "Inter-school Competitions Record Upload";
$eReportCard['SchoolServiceUpload'] = "School Service Record Upload";
$eReportCard['InterSchoolCompetition'] = "Inter-school Competitions Record";
$eReportCard['SchoolService'] = "School Service Record";

$eReportCard['DownloadCSVTemplate'] = "dowload sample";
$eReportCard['FileUpload'] = "FileUpload";
$eReportCard['FileUploadRemind'] = "注意：上載之檔案必須以年份、學期及班別命名。(例子：2006_1_1A.csv。第一個底線(_)後的數字代表學期︰0 代表全年；1 代表第一個學期；2 代表第二個學期；如此類推)。第二個底線(_)後的代表班別，若班別名稱有'/'號則不用輸入'/'。(例子：班別名稱：1A/Art，檔案名稱：2006_0_1AArt.csv)";
$eReportCard['ImportErrorWrongFormat'] = "Wrong import format";
$eReportCard['FileRemoveConfirm'] = "Are you sure you want to remove the file?";
$eReportCard['FileRemoveAllConfirm'] = "Are you sure you want to remove all files?";
$eReportCard['MarkTypeDisplay'] = "Mark Type Display<br />(Only applicable to subject with Score Scale)";
$eReportCard['SubMSDisplay'] = "Show Marks in SubMS";
$eReportCard['Marks'] = "Marks";
$eReportCard['Header'] = "Header";
$eReportCard['Footer'] = "Footer";
$eReportCard['BilingualReportTitle'] = "Bilingual Title Display";

$eReportCard['NotNoHeader'] = "With Default Header";
$eReportCard['IsNoHeader'] = "No Header. ";
$eReportCard['NoHeaderBr'] = "Number of empty line: ";
$eReportCard['LineHeight'] = "Line height";
$eReportCard['JSLineHeight'] = "Line height should be an non-zero positive number.";
$eReportCard['LineHeight'] = "Line height";
$eReportCard['JSLineHeight'] = "Line height should be an non-zero positive number.";
$eReportCard['SignatureWidth'] = "Signature width";
$eReportCard['JSSignatureWidth'] = "Signature width should be an non-zero positive number.";
$eReportCard['AllowClassTeacherUploadCSV'] = "Allow class teacher upload info";
$eReportCard['PresetComment'] = "Preset Comment";
$eReportCard['ClassPresetComment'] = "Class Teacher";
$eReportCard['SubjectPresetComment'] = "Subject Teacher";

$eReportCard['ReportCard'] = "Report Card";
$eReportCard['PositionReport'] = "Position Report";
$eReportCard['ImprovementReport'] = "Improvement Report";
$eReportCard['HonourStudent'] = "Honour Student";
$eReportCard['Statistic'] = "Statistics";
$eReportCard['TopX'] = "Top X";
$eReportCard['PositionTitle'] = "Positions";
$eReportCard['ByTotal'] = "Grand Total";
$eReportCard['ByGPA'] = "GPA";
$eReportCard['OrderBy'] = "Order By";
$eReportCard['AllFormTopX'] = "Top <!--TopX--> in Form";
$eReportCard['AllClassTopX'] = "Top <!--TopX--> in Class";
$eReportCard['jsNonzeroAlert'] = "Please input an non-zero number.";
$eReportCard['TotalImproved'] = "Grand Total Improved";
$eReportCard['GPAImproved'] = "GPA Improved";
$eReportCard['headcount'] = "No of Students";
$eReportCard['MarkRange'] = "Marks";
$eReportCard['GradeStat'] = "Grade Statistics";
$eReportCard['MarkStat'] = "Marks Statistics";
$eReportCard['MeanStat'] = "Mean Statistics";
$eReportCard['Semester'] = " Semester";
$eReportCard['ResultStat'] = " Result Statistics";
$eReportCard['JudgeBy'] = "Academic Result";
$eReportCard['Over'] = "over";
$eReportCard['morethan'] = "More than";
$eReportCard['morethanOrEqual'] = "More than or equal to";
$eReportCard['lessthan'] = "Less than";
$eReportCard['NoOf'] = "Number of <!--Item-->";
$eReportCard['OverMark'] = "Marks";
$eReportCard['DateInvalid'] = "Invalid date";
$eReportCard['AcademicYear'] = "Academic Year";
$eReportCard['DecimalPoint'] = "Decimal Point";

$eReportCard['AcademicResults'] = "Academic Results";
$eReportCard['ConductAssessment'] = "Conduct Assessment";
$eReportCard['Activities'] = "Activities";
$eReportCard['days'] = "day(s)";
$eReportCard['merits'] = "merit(s)";
$eReportCard['demerits'] = "demerit(s)";
### New ###

$eReportCard['DataRemovalMsg'] = "Are you sure you want to delete data?";
$eReportCard['TransitDataMsg'] = "Are you sure you want to transit data?";
$eReportCard['RemoveItemAlert'] = "Are you sure you want to delete selected item(s)?";
$eReportCard['RemoveAllItemAlert'] = "Are you sure you want to delete all items?";
$eReportCard['HideNotifyAlert'] = "Are you sure you want to hide notification?";
$eReportCard['InvalidDateFormatAlert']="Invalid Date Format!";
$eReportCard['ConfirmResultAlert'] = "Are you sure you have verified the result?";
$eReportCard['ConfirmMarksheetAlert'] = "Are you sure you have submitted the marksheet?";
$eReportCard['InvalidPeriod'] = "Start time must not be greater than end time!";
$eReportCard['ConfirmedMsg'] = "Result Confirmed!";
$eReportCard['CalOverallComponentResult'] = "Calculate Overall Results from Component Subject(s)";
$eReportCard['SubmitMarksheetConfirm'] = "Results in this page will be saved. Are you sure you want to continue?";
$eReportCard['ChangeInputModeConfirm'] = "Please make sure all the records have been saved before changing the input mode. Are you sure you want to continue?";
$eReportCard['MarkRemind'] = "Remark: \"abs\" stands for absent; \"/\" for no enrollment or exempt.";
$eReportCard['MarksheetCollectionPeriodRemind'] = "Please define the period for marksheets collection. Teacher can only submit marksheet within the period.";
$eReportCard['MarksheetVerificationPeriodRemind'] = "Please define the period for result verification. Student can only verifiy marksheet within the period.";
$eReportCard['NoReportBuiltMsg'] = "There is no report in this moment.";
$eReportCard['SaveAndUpdateMainMS'] = "Save and Update the Main Marksheet";
$eReportCard['InvalidInputMsg'] = "Invalid Input!";

$i_con_msg_notify = "<font color=green>Notified Student.</font>\n";
$i_con_msg_notify_failed = "<font color=red>Failed to Notify Student.</font>\n";
$i_con_msg_hide_notify = "<font color=green>Notification Hide.</font>\n";
$i_con_msg_hide_notify_failed = "<font color=red>Failed to Hide Notification.</font>\n";
$i_con_msg_report_generate = "<font color=green>Report Generated.</font>\n";
$i_con_msg_report_generate_failed = "<font color=red>Failed to Generate Report! Cannot find any result.</font>\n";
$i_con_msg_data_transit = "<font color=green>Data Transited.</font>\n";
$i_con_msg_data_transit_failed = "<font color=red>Failed to Transit Data.</font>\n";
$i_con_msg_upload = "<font color=green>File uploaded successfully.</font>\n";
$i_con_msg_upload_failed = "<font color=red>Failed to upload file.</font>\n";
$i_con_msg_wrong_row = "<font color=red>Invalid data on row %s.</font>\n";
$i_con_msg_wrong_csv_header = "<font color=red>Wrong CSV header.</font>\n";
$eReportCard['NeedToReGenAfterUpdate'] = "<font color=red>Record Updated. Please remember to re-save related marksheet and re-generate related report template.</font>\n";

$eReportCard['CalWeightedMark'] = "Calculate Weighted Marks from Raw Marks";
$eReportCard['CalComponentMark'] = "Calculate Marks from Component Subject(s)";
$eReportCard['GoToBottom'] = "↓Go to the bottom";
$eReportCard['GoToTop'] = "↑Go to the top";
$eReportCard['MarksheetCollectionNotInPeriodMsg'] = "You cannot input mark now.";
$eReportCard['ResultVerificationNotInPeriodMsg'] = "You cannot verify the result now.";
$eReportCard['VerifyResultRemind'] = "<b><font color=red>Please check your marksheets. Any appeal for the mark will not be accepted after the deadline!</font></b>";
$eReportCard['DuplicatedClassReportRemind'] = "<b><font color=red>The same form (e.g. F.1, F.2) cannot be repeated in 2 or more public reports. The following form(s) is/are repeated.</font></b>";
$eReportCard['SubMS'] = "Sub-MS";
$eReportCard['SubMarksheet'] = "Sub-Marksheet";
$eReportCard['ShowSubMarksheet'] = "Show Sub-Marksheet";
$eReportCard['RawMarks'] = "Raw Marks";
$eReportCard['WeightedMarks'] = "Weighted Marks";
$eReportCard['ConvertedGrade'] = "Converted Grades";
$eReportCard['MarksheetConfirmationRemind'] = "<b><font color=red>請必須在登分後按「確定分紙」以確定完成登分。</font></b>";
$eReportCard['InsertAfter'] = "Insert After";
$eReportCard['Enabled'] = "Enabled";
$eReportCard['Scale'] = "Scale";
$eReportCard['ScoreScale'] = "Score Scale";
$eReportCard['GradeScale'] = "Grade Scale";
$eReportCard['Honor'] = "Honour";
$eReportCard['PassFail'] = "Pass/Fail";
$eReportCard['HonorBased'] = "Honour Based";
$eReportCard['PassFailBased'] = "Pass/Fail Based";
$eReportCard['CommonTest'] = "Common Test";
$eReportCard['FinalExam'] = "Final Exam";
$eReportCard['CourseMark'] = "Course Mark";
$eReportCard['Custom'] = "Custom";
$eReportCard['MarkSummary'] = "Mark Summary (Auto-Calculated by system)";
$eReportCard['WeightMarkSummary'] = "Weight (Mark Summary)";
$eReportCard['WeightTotalSummary'] = "Weight (Total)";
$eReportCard['Enabled'] = "Enabled";
$eReportCard['HideNotEnrolledSubject'] = "Hide Not Enrolled Subject";
$eReportCard['Submitting'] = "Submitting";
$eReportCard['Submitted'] = "Submitted";
$eReportCard['GetPreviousSemesterResult'] = "Retrieve Previous Term's Result";
$eReportCard['Schemes_FullMark'] = "Full Mark";
$eReportCard['StudentInfo'] = "Student Information";
$eReportCard['Summary'] = "Summary";
$eReportCard['Misc'] = "Misc";

$eReportCard['GradingSchemeSubjectsOnly'] = "The following selection box display subjects with defined grading scheme only.";
$eReportCard['UpdateGradingSchemeUpdateTemplate'] = "Please update the report template to apply the updated scheme.";

##################################################################################
######################## Wording Report Settings and Titles ######################
$eReportCard['Name'] = "Name";
$eReportCard['ClassName'] = "Class Name";
$eReportCard['ClassNumber'] = "Class Number";
$eReportCard['ClassTeacher'] = "Class Teacher";
$eReportCard['DateOfIssue'] = "Date of Issue";
$eReportCard['StudentNo'] = "Student No.";
$eReportCard['DateOfBirth'] = "Date Of Birth";
$eReportCard['Gender'] = "Gender";
$eReportCard['EngSubject'] = "Subject";
$eReportCard['ChiSubject'] = "科目";
$eReportCard['FullMark'] = "Full Mark";
$eReportCard['SubjectUnit'] = "Units";
$eReportCard['SubjectTaken'] = "Subject Taken";
$eReportCard['OverallResult'] = "Overall Result";
$eReportCard['WholeTerm'] = "Whole Term";
$eReportCard['Mark'] = "Mark";
$eReportCard['Position'] = "Position";
$eReportCard['GrandTotal'] = "Grand Total";
$eReportCard['GPA'] = "Grade Point Average (GPA)";
$eReportCard['AverageMark'] = "Average Mark";
$eReportCard['FormPosition'] = "Position in Form";
$eReportCard['ClassPosition'] = "Position in Class";
$eReportCard['ClassPupilNumber'] = "No. of Pupils in Class"; 
$eReportCard['FormPupilNumber'] = "No. of Pupils in Form";
$eReportCard['Conduct'] = "Conduct";
$eReportCard['Politeness'] = "Politeness";
$eReportCard['Behaviour'] = "Behaviour";
$eReportCard['Application'] = "Application";
$eReportCard['Tidiness'] = "Tidiness";
$eReportCard['AbsenceFromECA'] = "Absence from ECA";

$eReportCard['Motivation'] = "Motivation in Learning";
$eReportCard['SelfConfidence'] = "Self-confidence";
$eReportCard['SelfDiscipline'] = "Self-discipline";
$eReportCard['Courtesy'] = "Courtesy";
$eReportCard['Honesty'] = "Honesty";
$eReportCard['Responsibility'] = "Sense of Responsibility";
$eReportCard['Cooperation'] = "Co-operation";

$eReportCard['DaysAbsent'] = "Days Absent";
$eReportCard['TimesLate'] = "Times Late";
$eReportCard['AbsentWOLeave'] = "Periods Absent w/o Leave";
$eReportCard['AbsentWOReason'] = "Days Absent w/o Reason";
$eReportCard['ClassTeacherComment'] = "Class Teacher Comment";
$eReportCard['SubjectTeacherComment'] = "Subject Teacher Comment";
$eReportCard['ClassHighestAverage'] = "Highest Average in Class";
$eReportCard['Awards'] = "Awards";
$eReportCard['MeritsAndDemerits'] = "Merits & Demerits";
$eReportCard['ECA'] = "ECA";
$eReportCard['Remark'] = "Remark";
$eReportCard['Signature'] = "Signature";
$eReportCard['Principal'] = "Principal";
$eReportCard['ParentGuardian'] = "Parent/Guardian";
$eReportCard['SchoolChop'] = "School Chop";
$eReportCard['Attendance'] = "Attendance";
$eReportCard['Merit'] = "Merits";
$eReportCard['MinorCredit'] = 'Minor Credit';
$eReportCard['MajorCredit'] = 'Major Credit';
$eReportCard['Demerit'] = "Demerits";
$eReportCard['MinorFault'] = "Minor Fault";
$eReportCard['MajorFault'] = "Major Fault";
$eReportCard['SchoolReopenDay'] = "School Re-open Day";
$eReportCard['Merits'] = "Merits";
$eReportCard['Demerits'] = "Demerits";
$eReportCard['Offence'] = "Offence";
$eReportCard['Tardiness'] = "Tardiness";
$eReportCard['TardinessOffence'] = "Tardiness";
$eReportCard['Competitions'] = "Competitions";
$eReportCard['Performances'] = "Performances";
$eReportCard['Services'] = "Services";
$eReportCard['Forgetfulness'] = "Forgetfulness";
$eReportCard['Misbehaviour'] = "Misbehaviour";
$eReportCard['PFRemarks'] = "Remark";

$eReportCard_ReportTitleArray["b5"] = array("SchoolChop"=>"校印", "Name"=>"姓名", "ClassName"=>"班別", 
										"ClassNumber"=>"學號", "ClassTeacher"=>"班主任", 
										"AcademicYear"=>"年度", "DateOfIssue"=>"派發日期", "StudentNo"=>"學生編號",
										"DateOfBirth"=>"生日日期", "Gender"=>"性別",
										"Subject"=>"科目", "FullMark"=>"滿分", "OverallResult"=>"總成績", 
										"Mark"=>"分數", "Position"=>"名次",
										"GrandTotal"=>"總分", "GPA"=>"平均績點分",
										"AverageMark"=>"平均分", "FormPosition"=>"級名次", 
										"ClassPosition"=>"班名次", "ClassPupilNumber"=>"全班人數", "FormPupilNumber"=>"全級人數",										
										"Conduct"=>"操行", "Politeness"=>"禮貌", "Behaviour"=>"行為", "Application"=>"專心", "Tidiness"=>"整潔",
										"Motivation"=>"主動學習", "SelfConfidence"=>"自信", "SelfDiscipline"=>"自律", "Courtesy"=>"殷勤", 
										"Honesty"=>"誠實", "Responsibility"=>"責任心", "Cooperation"=>"合作",
										"DaysAbsent"=>"缺課日數", "TimesLate"=>"遲到次數", "AbsentWOLeave"=>"曠課", "ClassTeacherComment"=>"班主任評語",
										"SubjectTeacherComment"=>"科目老師評語", "ClassHighestAverage"=>"全班最高平均分",
										"Awards"=>"獎項", "MeritsAndDemerits"=>"獎懲", "ECA"=>"課外活動", "Remark"=>"備註", 
										"InterSchoolCompetition"=>"聯校比賽", "SchoolService"=>"校內服務", 
										"Signature"=>"簽署", "Principal"=>"校長", "ParentGuardian"=>"家長/監護人",
										"Attendance"=>"考勤", "Merit"=>"優點", "Demerit"=>"缺點",
										"MinorCredit"=>'小優', "MajorCredit"=>'大優', "MinorFault"=>"小過", "MajorFault"=>"大過"
										);
$eReportCard_ReportTitleArray["en"] = array("SchoolChop"=>"School Chop", "Name"=>"Name", "ClassName"=>"Class Name", 
										"ClassNumber"=>"Class Number", "ClassTeacher"=>"Class Teacher", 
										"AcademicYear"=>"Academic Year", "DateOfIssue"=>"Date of Issue", "StudentNo"=>"Student No.",
										"DateOfBirth"=>"Date of Birth", "Gender"=>"Gender",
										"Subject"=>"Subject", "FullMark"=>"Full Mark", "OverallResult"=>"Overall Result", 
										"Mark"=>"Mark", "Position"=>"Position",
										"GrandTotal"=>"Grand Total", "GPA"=>"Grade Point Average (GPA)",
										"AverageMark"=>"Average Mark", "FormPosition"=>"Position in Form", 
										"ClassPosition"=>"Position in Class", "ClassPupilNumber"=>"No. of Pupils in Class", "FormPupilNumber"=>"No. of Pupils in Form",
										"Conduct"=>"Conduct", "Politeness"=>"Politeness", "Behaviour"=>"Behaviour", "Application"=>"Application",
										"Tidiness"=>"Tidiness",
										"Motivation"=>"Motivation in Learning", "SelfConfidence"=>"Self-confidence",
										"SelfDiscipline"=>"Self-discipline","Courtesy"=>"Courtesy","Honesty"=>"Honesty",
										"Responsibility"=>"Sense of Responsibility","Cooperation"=>"Co-operation",										
										"DaysAbsent"=>"Days Absent", "TimesLate"=>"Times Late", 
										"AbsentWOLeave"=>"Periods Absent w/o Leave", "ClassTeacherComment"=>"Class Teacher Comment",
										"SubjectTeacherComment"=>"Subject Teacher Comment", "ClassHighestAverage"=>"Highest Average in Class",
										"Awards"=>"Awards", "MeritsAndDemerits"=>"Merits & Demerits", "ECA"=>"ECA", "Remark"=>"Remark",
										"InterSchoolCompetition"=>"Inter-school Competitions Record",
										"SchoolService"=>"School Service Record",
										"Signature"=>"Signature", "Principal"=>"Principal", "ParentGuardian"=>"Parent/Guardian",
										"Attendance"=>"Attendance", "Merit"=>"Merit", "Demerit"=>"Demerit",
										"MinorCredit"=>"Minor Credit", "MajorCredit"=>"Major Credit", "MinorFault"=>"Minor Fault", "MajorFault"=>"Major Fault");

### For Report Card Bilingual Setting

	$eReportCardBilingual['Name'] = "姓名 Name";
	$eReportCardBilingual['ClassName'] = "班別 Class Name";
	$eReportCardBilingual['ClassNumber'] = "學號 Class Number";
	$eReportCardBilingual['ClassTeacher'] = "班主任<br />Class Teacher";
	$eReportCardBilingual['DateOfIssue'] = "派發日期 Date of Issue";
	$eReportCardBilingual['DateOfBirth'] = "生日日期 Date of Birth";
	$eReportCardBilingual['Gender'] = "性別 Gender";
	$eReportCardBilingual['StudentNo'] = "學生編號 Student No.";
	$eReportCardBilingual['FullMark'] = "滿分<br />Full Mark";
	$eReportCardBilingual['OverallResult'] = "總成績<br />Overall Result";
	$eReportCardBilingual['Mark'] = "分數 Mark";
	$eReportCardBilingual['Position'] = "名次 Position";
	$eReportCardBilingual['GrandTotal'] = "總分 Grand Total";
	$eReportCardBilingual['GPA'] = "平均績點分 Grade Point Average (GPA)";
	$eReportCardBilingual['AverageMark'] = "平均分 Average Mark";
	$eReportCardBilingual['FormPosition'] = "級名次 Position in Form";
	$eReportCardBilingual['ClassPosition'] = "班名次 Position in Class";
	$eReportCardBilingual['ClassPupilNumber'] = "全班人數 No. of Pupils in Class";
	$eReportCardBilingual['FormPupilNumber'] = "全級人數 No. of Pupils in Form";
	$eReportCardBilingual['Conduct'] = "操行 Conduct";
	$eReportCardBilingual['Politeness'] = "禮貌 Politeness";
	$eReportCardBilingual['Behaviour'] = "行為 Behaviour";
	$eReportCardBilingual['Application'] = "專心 Application";
	$eReportCardBilingual['Tidiness'] = "整潔 Tidiness";
	
	$eReportCardBilingual['Motivation'] = "主動學習 Motivation in Learning";
	$eReportCardBilingual['SelfConfidence'] = "自信 Self-confidence";
	$eReportCardBilingual['SelfDiscipline'] = "自律 Self-discipline";
	$eReportCardBilingual['Courtesy'] = "殷勤 Courtesy";
	$eReportCardBilingual['Honesty'] = "誠實 Honesty";
	$eReportCardBilingual['Responsibility'] = "責任心 Sense of Responsibility";
	$eReportCardBilingual['Cooperation'] = "合作 Co-operation";
	
	$eReportCardBilingual['DaysAbsent'] = "缺課日數 Days Absent";
	$eReportCardBilingual['TimesLate'] = "遲到次數 Times Late";
	$eReportCardBilingual['AbsentWOLeave'] = "曠課 Periods Absent w/o Leave";
	$eReportCardBilingual['ClassTeacherComment'] = "班主任評語 Class Teacher Comment";
	$eReportCardBilingual['SubjectTeacherComment'] = "科目老師評語<br />Subject Teacher Comment";
	$eReportCardBilingual['ClassHighestAverage'] = "全班最高平均分 Highest Average in Class";
	$eReportCardBilingual['Awards'] = "獎項 Awards";
	$eReportCardBilingual['MeritsAndDemerits'] = "獎懲<br />Merits & Demerits";
	$eReportCardBilingual['ECA'] = "課外活動 ECA";	
	$eReportCardBilingual['Remark'] = "備註 Remark";
	$eReportCardBilingual['InterSchoolCompetition'] = "聯校比賽紀錄 Inter-school Competitions Record";
	$eReportCardBilingual['SchoolService'] = "學校服務紀錄 School Service Record";
	$eReportCardBilingual['Signature'] = "簽署<br />Signature";
	$eReportCardBilingual['Principal'] = "校長<br />Principal";
	$eReportCardBilingual['ParentGuardian'] = "家長/監護人<br />Parent/Guardian";
	$eReportCardBilingual['SchoolChop'] = "校印<br />School Chop";
	$eReportCardBilingual['Attendance'] = "考勤<br />Attendance";
	$eReportCardBilingual['Merit'] = "優點<br />Merits";
	$eReportCardBilingual['MinorCredit'] = '小優'."<br />Minor Credit";
	$eReportCardBilingual['MajorCredit'] = '大優'."<br />Major Credit";
	$eReportCardBilingual['Demerit'] = "缺點<br />Demerits";
	$eReportCardBilingual['MinorFault'] = "小過<br />Minor Fault";
	$eReportCardBilingual['MajorFault'] = "大過<br />Major Fault";
	$eReportCardBilingual['AcademicYear'] = "學年 Academic Year";
######

$eReportCard['SetIssueDate'] = "Set ".$eReportCard['DateOfIssue'];

######

$eReportCard['ScoreSummaryTitle'][0] = "Number of students";
$eReportCard['ScoreSummaryTitle'][1] = "0-9";
$eReportCard['ScoreSummaryTitle'][2] = "10-19";
$eReportCard['ScoreSummaryTitle'][3] = "20-29";
$eReportCard['ScoreSummaryTitle'][4] = "30-39";
$eReportCard['ScoreSummaryTitle'][5] = "40-49";
$eReportCard['ScoreSummaryTitle'][6] = "50-59";
$eReportCard['ScoreSummaryTitle'][7] = "60-69";
$eReportCard['ScoreSummaryTitle'][8] = "70-79";
$eReportCard['ScoreSummaryTitle'][9] = "80-89";
$eReportCard['ScoreSummaryTitle'][10] = "90 or over";
$eReportCard['ScoreSummaryTitle'][11] = "Highest score";
$eReportCard['ScoreSummaryTitle'][12] = "Lowest score";
$eReportCard['ScoreSummaryTitle'][13] = "Number of outstandings";
$eReportCard['ScoreSummaryTitle'][14] = "Outstanding rate";
$eReportCard['ScoreSummaryTitle'][15] = "Number of passes";
$eReportCard['ScoreSummaryTitle'][16] = "Passing rate";
$eReportCard['ScoreSummaryTitle'][17] = "Number of fails";
$eReportCard['ScoreSummaryTitle'][18] = "Failure rate";
$eReportCard['ScoreSummaryTitle'][19] = "Mean";
$eReportCard['ScoreSummaryTitle'][20] = "Standard Deviation";

###### eReportCard 2008 ######
$eReportCard['SubjectSettings'] = 'Subject Settings';
$eReportCard['FormSettings'] = 'Form Settings';
####### ###### ###### ######## 

######

$eReportCard['DisplaySettingsArray']["StudentInfo"] = array("Name", "ClassName", "ClassNumber", "StudentNo", "ClassTeacher", "AcademicYear", "DateOfIssue", "DateOfBirth", "Gender");
//$eReportCard['DisplaySettingsArray']["Summary"] = array("GrandTotal", "GPA", "AverageMark", "FormPosition", "ClassPosition", "ClassPupilNumber", "FormPupilNumber", "ClassHighestAverage", "Conduct", "Politeness", "Behaviour", "Application", "Tidiness", "Motivation", "SelfConfidence", "SelfDiscipline", "Courtesy", "Honesty", "Responsibility", "Cooperation", "DaysAbsent", "TimesLate", "AbsentWOLeave", "Merits", "Attendance", "Competitions", "Performances", "Services", "Demerits", "Forgetfulness", "Tardiness", "Misbehaviour");
//$eReportCard['DisplaySettingsArray']["Summary"] = array("GrandTotal", "GPA", "AverageMark", "FormPosition", "ClassPosition", "ClassPupilNumber", "FormPupilNumber", "ClassHighestAverage", "Conduct", "Politeness", "Tidiness", "Application", "AbsenceFromECA", "Motivation", "SelfConfidence", "SelfDiscipline", "Courtesy", "Honesty", "Responsibility", "Cooperation", "DaysAbsent", "TimesLate", "AbsentWOReason", "Merits", "Attendance", "Competitions", "Performances", "Services", "Demerits", "Forgetfulness", "Tardiness", "Misbehaviour");
$eReportCard['DisplaySettingsArray']["Summary"] = array("GrandTotal", "GPA", "AverageMark", "FormPosition", "ClassPosition", "ClassPupilNumber", "FormPupilNumber", "ClassHighestAverage", "PFRemarks", "Conduct", "Politeness", "Tidiness", "Application", "AbsenceFromECA", "Motivation", "SelfConfidence", "SelfDiscipline", "Courtesy", "Honesty", "Responsibility", "Cooperation", "DaysAbsent", "TimesLate", "AbsentWOReason", "Merits", "Attendance", "Competitions", "Performances", "Services", "Demerits", "Tardiness", "Forgetfulness", "Misbehaviour", "Offence", "TardinessOffence");
$eReportCard['DisplaySettingsArray']["Misc"] = array("ClassTeacherComment", "SubjectTeacherComment", "Awards", "MeritsAndDemerits", "MinorCredit", "MajorCredit", "MinorFault", "MajorFault", "ECA", "Remark", "InterSchoolCompetition", "SchoolService");
$eReportCard['DisplaySettingsArray']["Signature"] = array("Principal", "ClassTeacher", "ParentGuardian", "SchoolChop");

$ReportTitleFilePath = $PATH_WRT_ROOT."file/reportcard/lang.en.php";
if(file_exists($ReportTitleFilePath))
{
	include_once($ReportTitleFilePath);
}

##################################################################################
##################################################################################
?>
