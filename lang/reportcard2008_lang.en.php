<?php
# Editing by anna
#
# Date: 20200407 Philips
#	added $eReportCard['Reports_StudentYearlyResultReport']
#	added $eReportCard['DataExport_To_WebSAMS']
#
# Date: 20190628 Philips
#   added $eReportCard['GradeSettings']
#
# Date: 20180620 Philips
#   added $eReportCard['SubjectType']
#   added $eReportCard['SubjectCredit']
#   added $eReportCard['SubjectLowerLimit']
#   added $eReportCard['NotSet']
#   added $eReportCard['SubjectCore']
#   added $eReportCard['SubjectElection']
#   added $eReportCard['SubjectUnknown']
#
#
// 2016-10-03 Add $eReportCard['Reports_GradingSchemeSammary']

$eReportCard['ReportCard'] = "Report Card";

// System message after form submit, must have the prefix "$i_con_msg_"
$i_con_msg_wrong_row = "<font color=red>Invalid data on row %s.</font>\n";
$i_con_msg_wrong_csv_header = "<font color=red>Wrong CSV header.</font>\n";
$i_con_msg_report_generate = "<font color=green>Report Generated.</font>\n";
$i_con_msg_report_generate_failed = "<font color=red>Failed to Generate Report! Cannot find any result.</font>\n";
$i_con_msg_promotion_generate = "<font color=green>Promotion Status Generated</font>\n";
$i_con_msg_promotion_generate_failed = "<font color=red>Failed to Generate Promotion Status!</font>\n";
$i_con_msg_delete_comment_cat_success = "<font color=green>Comment Category deleted.</font>\n";
$i_con_msg_delete_comment_cat_failed = "<font color=red>Failed to delete Comment Category.</font>\n";
$i_con_msg_num_records_updated = "Record(s) Updated.";
$i_con_msg_report_archive = "<font color=green>Report Archived.</font>\n";
$i_con_msg_report_archive_failed = "<font color=red>Archive Report Failed.</font>\n";


// Menu
$eReportCard['Management'] = "Management";
$eReportCard['Management_Schedule'] = "Schedule";
$eReportCard['Management_Progress'] = "Progress";
$eReportCard['Management_MarksheetRevision'] = "Marksheet Revision";
$eReportCard['Management_MarksheetSubmission'] = "Marksheet Submission";
$eReportCard['Management_MarksheetVerification'] = "Marksheet Verification";
$eReportCard['Management_ClassTeacherComment'] = "Class Teacher's Comment";
$eReportCard['Management_ConductMark'] = "Conduct Mark"; // for li sing tai hang use only
$eReportCard['Management_OtherInfo'] = "Other Info";
$eReportCard['Management_OtherStudentInfo'] = "Other Student Info"; // for SIS use only
$eReportCard['Management_DataHandling'] = "Data Handling";
$eReportCard['Management_ReportArchive'] = "Report Card Archive";
$eReportCard['Management_Promotion'] = "Promotion Status";
$eReportCard['Management_AcademicProgress'] = "Academic Progress";
$eReportCard['Management_ConductGradeCalculation'] = "Student Conduct Grade";
$eReportCard['Management_ManualInputReExamStatus'] = "Student Re-exam Result";

$eReportCard['NoComment'] = "No Comment";

$eReportCard['Reports'] = "Reports";
$eReportCard['Reports_GenerateReport'] = "Generate & Print Report Card";
$eReportCard['Reports_SubMarksheetReport'] = "Sub-Marksheet Report";
$eReportCard['Reports_AssessmentReport'] = "Assessment Report";
$eReportCard['Reports_GrandMarksheet'] = "Grand Marksheet";
$eReportCard['Reports_OtherReports'] = "Other Reports";
$eReportCard['Reports_ClassHonourReport'] = "Honour Report";
$eReportCard['Reports_ScholarshipReport'] = "Honour Student Scholarship";
$eReportCard['Reports_ConductAwardReport'] = "Conduct Award Report";
$eReportCard['Reports_SubjectPrizeReport'] = "Subject Prize Report";
$eReportCard['Reports_GPATop3Rank'] = "Ranking Report";
$eReportCard['Reports_ExaminationSummary'] = "Examination Summary";
$eReportCard['Reports_StudentResultSummary'] = "Student Result Summary";
$eReportCard['Reports_SubjectSummarySheet'] = "Subject Summary Sheet";
$eReportCard['Reports_GradeDistribution'] = "Grade Distribution";
$eReportCard['Reports_MarkGradeConversion'] = "Mark-Grade Conversion";
$eReportCard['Reports_MarkPositionConversion'] = "Mark-Position Conversion";
$eReportCard['Reports_RawMarkDistribution'] = "Raw Mark Distribution";
$eReportCard['Reports_RemarksReport'] = "Remarks Report";
$eReportCard['Reports_SubjectPercentagePassReport'] = "Subject Percentage Pass Report";
$eReportCard['Reports_MasterReport'] = "Master Report";
$eReportCard['Reports_TrialPromotion'] = "Trial Promotion";
$eReportCard['Reports_HKUGAGradeList'] = "Report Summary";
$eReportCard['Reports_AwardList'] = "Award List";
$eReportCard['Reports_SubjectAcademicResult'] = "Subject Academic Result";
$eReportCard['Reports_FinalComment'] = "Final Comment";
$eReportCard['Reports_ReExamList'] = "Re-exam List";
$eReportCard['Reports_Transcript'] = "Transcript";
$eReportCard['Reports_SubjectFullMarkComponentRatioSettings'] = "Full Mark and Ratio Setttings of Component Subjects";
$eReportCard['Reports_ClassSubjectComponentResultAnalysis'] = "Result Analysis of Component Subjects";
$eReportCard['Reports_TeacherSubjectResultAnalysis'] = "Result of Teaching Subject Analysis";
$eReportCard['Reports_ClassSubjectCAResultAnalysis'] = "Result Analysis of Continuous Assessment of Subject (Class)";
$eReportCard['Reports_TermClassResultAnalysis'] = "Term Result Analysis of Subject";
$eReportCard['Reports_FormSubjectCAResultAnalysis'] = "Result Analysis of Continuous Assessment of Subject (Form)";
$eReportCard['Reports_FormSubjectResultAnalysis'] = "Result Analysis of Subject (Form)";
$eReportCard['Reports_ClassSubjectResultAnalysis'] = "Result Analysis of Subject (Class)";
$eReportCard['Reports_PrintArchivedReport'] = "Print Archived Report";
$eReportCard['Reports_MarkAnalysis'] = "Mark Analysis";
$eReportCard['Reports_GradingReport'] = "Grading Report";
$eReportCard['Reports_YearEndReport'] = "Year End Report";
$eReportCard['Reports_ClassAllocation'] = "Class Allocation";
$eReportCard['Reports_SubjectStat'] = "Statistics Report";
$eReportCard['Reports_MarkupExamSubject'] = "Mark-up Exam Summary";
$eReportCard['Reports_AwardPrediction'] = "Award Prediction List";
$eReportCard['Reports_GradingSchemeSummary'] = "Grading Scheme Summary";
$eReportCard['Reports_FormTeacherCommentReport'] = "Form Teacher's Comments";
$eReportCard['Reports_ReportCardSummary'] = "Report Card Summary";
$eReportCard['Reports_AwardCertificate'] = "Award Certificate";
$eReportCard['Reports_AwardScoreSummary'] = "Award Mark Summary";
$eReportCard['Reports_Testimonial'] = "Testimonial";

$eReportCard['Statistics'] = "Statistics";

$eReportCard['Settings'] = "Settings";
$eReportCard['Settings_BasicSettings'] = "Basic Settings";
$eReportCard['Settings_CommentBank'] = "Comment Bank";
$eReportCard['Settings_Conduct'] = "Conduct";
$eReportCard['Settings_SubjectsAndForm'] = "Subjects & Class Level";
$eReportCard['Settings_ReportCardTemplates'] = "Report Card Template";
$eReportCard['Settings_ExcludeStudents'] = "Exclude Students from Ranking";
$eReportCard['Settings_CurriculumExpectation'] = "Course Description";
$eReportCard['Settings_TeacherExtraInfo'] = "Teacher Extra Information";
$eReportCard['Settings_GradingScheme'] = "Grading Scheme"; 
$eReportCard['Settings_SubjectTopics'] = "Subject Topics";
$eReportCard['Settings_ImportAwards'] = "Import Awards (not involved in award generation process)";
$eReportCard['Settings_AllowAccessIP'] = "Access Right Computer IP";
$eReportCard['Settings_ViewGroupUser'] = "Viewer Group";
$eReportCard['Settings_ChangeClubActivityName'] = "Change Enrolment Club/Activity Name";
$eReportCard['Settings_SubjectStream'] = "Student Subject Stream Settings";
$eReportCard['Settings_PromotionStatusRemarks'] = "Remarks Display for Promotion / Re-exam";
$eReportCard['Settings_SubjectGroupDescription'] = "Subject Group Description";

// Tabs
#$eReportCard['HighlightSettings'] = "Highlight";
$eReportCard['StyleSettings'] = "Style";
$eReportCard['MarkStorageAndDisplay'] = "Mark Storage & Display";
$eReportCard['AbsentAndExemptSettings'] = "Absent & Exempt";
$eReportCard['CalculationSettings'] = "Calculation";
$eReportCard['AccessSettings'] = "Access Settings";
$eReportCard['PostTableHeader'] = "Report Post Table Column";

$eReportCard['SubjectSettings'] = 'Subject Settings';
$eReportCard['FormSettings'] = 'Class Level Settings';
//$eReportCard['CreditSettings'] = 'Credit Settings';

// Calculation Settings
$eReportCard['Term'] = "Term";
$eReportCard['WholeYear'] = "Whole Year";
$eReportCard['SchemeTitle'] = "Grading Scheme";

$eReportCard['Absent'] = "Absent";
$eReportCard['AbsentExcludeWeight'] = "Exclude the weight of the absent assessment";
$eReportCard['AbsentExcludeFullMark'] = "Exclude the full mark of the absent assessment";
$eReportCard['AbsentTreatAsZeroMark'] = "Treat the raw mark of the absent assessment as 0 mark";

$eReportCard['Exempt'] = "Exempt";
$eReportCard['ExemptExcludeWeight'] = "Exclude the weight of the exempted assessment ";
$eReportCard['ExemptExcludeFullMark'] = "Exclude the full mark of the exempted assessment";
$eReportCard['ExemptTreatAsZeroMark'] = "Treat the raw mark of the exempted assessment as 0 mark";

$eReportCard['CalculationMethod'] = "Calculation Method";
$eReportCard['ConsolidateMark'] = "Consolidate marks to weighted marks for subject records (excluding overall marks)";
$eReportCard['AdjustFullMark'] = "Adjust Full mark of each subject to 100 before applying weight";

$eReportCard['CalculationOrder'] = "Calculation Order";
$eReportCard['UseTermMarkFromAll'] = "Use term subject mark calculated from all components";
$eReportCard['UseTermAvgMarkFromAll'] = " Use term average mark calculated from all subjects";
$eReportCard['RightDown'] = "Right-down (R-D)";
$eReportCard['DownRight'] = "Down-right (D-R)";
$eReportCard['TermRightDown'] = "Calculate term subject average for each subject first";
$eReportCard['TermDownRight'] = "Calculate subject component averages across all subjects first";
$eReportCard['YearRightDown'] = "Calculate whole-year average for each subject first";
$eReportCard['YearDownRight'] = "Calculate term averages across all subjects first";
$eReportCard['TermPreviewNotApplicable'] = "The Preview function is not applicable for the report which calculates subject component averages across all subjects first";
$eReportCard['YearPreviewNotApplicable'] = "The Preview function is not applicable for the report which calculates term averages across all subjects first";

// Highlight
$eReportCard['Style'] = "Style";
$eReportCard['Format'] = "Format";
$eReportCard['SpecialSymbol'] = "Special Symbol";
$eReportCard['Sample'] = "Sample";
$eReportCard['Mark'] = "Mark";
$eReportCard['TotalMark'] = "Total Mark";

// Mark Storage & Display 
$eReportCard['AssessmentMark'] = "Assessment Mark";
$eReportCard['SubjectOverallMark'] = "Subject Overall Mark";
$eReportCard['OverallAverage'] = "Overall Average";
$eReportCard['OverallTotal'] = "Overall Total";
$eReportCard['Integer'] = "Integer";
$eReportCard['OneDecimalPlace'] = "1 decimal place";
$eReportCard['TwoDecimalPlaces'] = "2 decimal places";

// Access Settings
$eReportCard['AllowClassTeacherUploadCSV'] = "Allow class teacher manage ".$eReportCard['Management_OtherInfo'];
$eReportCard['AllowTeacherAccessGrandMS'] = "Allow teacher access ".$eReportCard['Reports_GrandMarksheet'];
$eReportCard['AllowTeacherAccessMasterReport'] = "Allow teacher access ".$eReportCard['Reports_MasterReport'];
$eReportCard['EnableVerificationPeriod'] = "Enable Parent & Student Verification Period";
$eReportCard['AuthenticationSetting'] = "Authenticate when first time access";

// Comment Bank
$eReportCard['Subject'] = "Subject";
$eReportCard['Class'] = "Class";
$eReportCard['Code'] = "Code";
$eReportCard['CommentContent'] = "Comment Content";
$eReportCard['CommentContentChi'] = "Comment Content (Chinese)";
$eReportCard['CommentContentEng'] = "Comment Content (English)";
$eReportCard['Category'] = "Category";
$eReportCard['Type'] = "Type";
$eReportCard['AllTeachers'] = "All Teachers";
$eReportCard['ClassTeacher'] = "Class Teacher";
$eReportCard['SubjectTeacher'] = "Subject Teacher";
$eReportCard['AllCategories'] = "All Categories";
$eReportCard['AlertEnterCommentCode'] = "Please enter the Code";
$eReportCard['AlertDuplicateCommentCode'] = "The Code is already exist, please enter a new one.";
$eReportCard['AlertEnterCommentCategory'] = "Please enter the Category";
$eReportCard['AlertEnterCommentContent'] = "Please enter the Comment Content";
$eReportCard['AlertSelectFile'] = "Please select a file first";
$eReportCard['DeleteSelectedCategory'] = "Delete Selected Category";
$eReportCard['AlertDeleteSelectedCategory'] = "Delete selected category will also remove the category proporty of all comment associated to it. Procced deleting?";
$eReportCard['SelectComment'] = "Select Comment";
$eReportCard['CommentList'] = "Comment List";
$eReportCard['Language'] = "Language";

// Grading Differentiation
$eReportCard['Distinction'] = 'Distinction';
$eReportCard['Pass'] = 'Pass';
$eReportCard['Fail'] = 'Fail';

$eReportCard['Removing'] = 'Removing';
$eReportCard['Saving'] = 'Saving';

$eReportCard['Grade'] = 'Grade';
$eReportCard['SelectScheme'] = 'Select Scheme';
$eReportCard['GradePoint'] = "Grade Point";
$eReportCard['GradingRange'] = 'Grading Range';
$eReportCard['GradingScheme'] = 'Grading Scheme';
$eReportCard['GradingTitle'] = "Grading Title";
$eReportCard['GradingType'] = "Grading Type";
$eReportCard['HonorBased'] = "Honour Based";
$eReportCard['UpperLimit'] = "Upper Limit";
$eReportCard['LowerLimit'] = "Lower Limit";
$eReportCard['InputScaleType'] = 'Input Scale';
$eReportCard['PassFailBased'] = "Pass/Fail Based";
$eReportCard['ResultDisplayType'] = 'Display Result';
$eReportCard['SchemesFullMark'] = "Full Mark";
$eReportCard['StudentsFromTop'] = "Students<br>From Top";
$eReportCard['StudentsFromTop_nowrap'] = "Students From Top";
$eReportCard['Mark'] = 'Mark';
$eReportCard['MarkRange'] = 'Mark Range';
$eReportCard['Next'] = 'Next';
$eReportCard['SchemesPassingMark'] = 'Passing Mark';
$eReportCard['PercentageRangeForAllStudents'] = '% Range for all students';
$eReportCard['PercentageRangeForPassedStudents'] = '% Range for passed students';

$eReportCard['AddMore'] = "Add More";
$eReportCard['CopyFrom'] = "Copy from";
$eReportCard['Sift'] = 'Sift';
$eReportCard['ClickToSift'] = 'Click to Sift';
$eReportCard['ClickToUnsift'] = 'Click to Unsift';
$eReportCard['jsAlertNoSubjectSelect'] = "No subject is selected in this Form";
$eReportCard['MoveUp'] = 'Move Up';
$eReportCard['MoveDown'] = 'Move Down';
$eReportCard['NextForm'] = 'Next Class Level';
$eReportCard['PreviousForm'] = 'Previous Class Level';

$eReportCard['jsAlertNoSubjectAssigned'] = 'No Class Level Settings available for copying';

// added on 24 Apr 08
$eReportCard['CopyClassLevelSettingsResult'] = 'Class Level Settings Copy Result';
$eReportCard['GradingSchemeExist'] = 'The Grading Scheme has been assigned.';
$eReportCard['SubmitMarksheetConfirm'] = "Results in this page will be saved. Are you sure you want to continue?";

$eReportCard['RemoveSubMarksheetColumn'] = "Results in this column will be deleted together. Are you sure you want to delete this assessment?";

// ReportCard Template
$eReportCard['InputBasicInformation'] = "Input Basic Information";
$eReportCard['InputSubjectWeight'] = "Input Subject Weight";
$eReportCard['InputOverallSubjectWeight'] = "Input Overall Subject Weight";
$eReportCard['InputAssessmentRatio'] = "Input Assessment Ratio";
$eReportCard['AssessmentRatio'] = "Assessment ratio";
$eReportCard['Assessment'] = "Assessment";
$eReportCard['Ratio'] = "Ratio";
$eReportCard['InputTermsWeight'] = "Input Terms Weight";
$eReportCard['InputTermsRatio'] = "Input Terms Ratio";
$eReportCard['InputAssessmentWeight'] = "Input Assessment Weight";
$eReportCard['InputReportDisplayInformation'] = "Input Report Display Information";
$eReportCard['Finishandpreview'] = "Finish and preview";
$eReportCard['ReportTitle'] = "Report Title";
$eReportCard['SelectTerm'] = "Select Term";
$eReportCard['TermReport'] = "Term Report";
$eReportCard['WholeYearReport'] = "Consolidated Report";
$eReportCard['Main'] = "Main";
$eReportCard['Extra'] = "Extra";
$eReportCard['jsHasMainTermReportAlready'] = "The form has a Main Term Report already.";
$eReportCard['jsHasSameReportAlready'] = "The form has same Report already.";

$eReportCard['ShowAllAssessments'] = "Show All Assessments";
$eReportCard['ShowTermTotalOnly'] = "Show Term Total only";
$eReportCard['ReportType'] = "Report Type";
$eReportCard['SettingUpdateReminder'] = "Setting is updated. Please remember to re-generate it again.";
$eReportCard['NewReport'] = "New Report";
$eReportCard['SkipStep'] = "Are you sure skip this step?";
$eReportCard['RatioBetweenAssesments'] = "Ratio Between Assessments";
$eReportCard['Use'] = "Use";
$eReportCard['MoveLeft'] = "Move to left";
$eReportCard['MoveRight'] = "Move to right";
$eReportCard['NoTermSelected'] = "No Term is selected at this momnent.";
$eReportCard['NoAssesment'] = "No Assessment at this momnent.";
$eReportCard['AddTerm'] = "Add Term";
$eReportCard['AddAssesment'] = "Add Assessment";
$eReportCard['AtBegin'] = "at the beginning";
$eReportCard['NoSubjectSettings'] = "No Subject settings.";
$eReportCard['ColumnTitle'] = "Assessment title";
$eReportCard['MissingDefaultWeight'] = "Missing default weight.";
$eReportCard['DefaultWeightNumeric'] = "The default weight should be in numeric.";
$eReportCard['MissingWeight'] = "Missing weight.";
$eReportCard['WeightNumeric'] = "The weight should be in numeric.";
$eReportCard['SubjectTotleNot100'] = "The subject total weight is not 100%.  Continue?";
$eReportCard['InsertAfter'] = "Insert After";
$eReportCard['ShowPosition'] = "Show Position";
$eReportCard['DonotShowPosition'] = "Do not Show Position";
$eReportCard['ShowPositionInClass'] = "Show Position in Class";
$eReportCard['ShowPositioninClassLevel'] = "Show Position in Class Level";
$eReportCard['AssignToAll'] = "Assign to all";
$eReportCard['OverallSubjectWeight'] = "Overall Subject Weight";
$eReportCard['MissingData'] = "Missing data.";
$eReportCard['RatioNumeric'] = "The ratio should be in numeric.";
$eReportCard['RatioSumNot100'] = "The sum of ratio is not 100%.  Continue?";
$eReportCard['LineHeight'] = "Line Height";
$eReportCard['Header'] = "Header";
$eReportCard['WithDefaul Header'] = "With Default Header";
$eReportCard['NoHeader'] = "No Header.";
$eReportCard['NumberOfEmptyLine'] = "Number of empty line";
$eReportCard['Footer'] = "Footer";
$eReportCard['DisplayStudentInformation'] = "Display Student Information";
$eReportCard['DisplayColumn'] = "Display Column";
$eReportCard['ShowSubjectFullMark'] = "Show Subject Full Mark";
$eReportCard['ShowSubjectOverall'] = "Show Subject Overall";
$eReportCard['DisplayOverallResult'] = "Display Overall Result";
$eReportCard['DisplayGrandAvg'] = "Display Average Mark";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowClassNumOfStudent'] = "Show Number of Students in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";
$eReportCard['ShowFormNumOfStudent'] = "Show Number of Students in Form";
# added on 01 Jan 2008
$eReportCard['MaximumPosition'] = "Maximum Position to be shown";
$eReportCard['MaximumPositionRemarks'] = "* all positions will be shown if the limit is set to 0";
$eReportCard['GreaterThanGrandAverage'] = "Greater than the Average Mark";
$eReportCard['Mark(s)'] = "mark(s)";
$eReportCard['AttendanceDays'] = "Attendance (days)";
$eReportCard['SpecialPercentage'] = "Top % (whole Form) for Grade A*";
$eReportCard['PrintOption'] = "Subject Comment Printing Option";
$eReportCard['PrintOption_1'] = "Print all Subjects";
$eReportCard['PrintOption_2'] = "Print fail Subjects only";
$eReportCard['PrincipalName'] = "Principal Name";


$eReportCard['CommentFrom'] = "Comment From";
$eReportCard['ClassTeache'] = "Class Teacher";
$eReportCard['SubjectTeacher'] = "Subject Teacher";
$eReportCard['Success'] = "Success";
$eReportCard['SuccessWithMissing'] = "Success - some subject(s) is missing";
$eReportCard['TemplateExists'] = "Template is already exists.";
$eReportCard['Copy'] = "Copy";
$eReportCard['CopyTo'] = "Copy to";
$eReportCard['CopyFrom'] = "Copy from";
$eReportCard['CopyResult'] = "Report Card Templates Copy Result";
$eReportCard['SubmissionEndDateNotReached'] = "Submission End Date has not reached yet.";
$eReportCard['VerificationEndDateNotReached'] = "Verification End Date has not reached yet.";
$eReportCard['ClassCommentNotCompleted'] = "Class Comment is not completed.";
$eReportCard['MSNotCompleted'] = "Marksheet is not completed.";
$eReportCard['TemplatePreview'] = "Template Preview";
$eReportCard['Preview'] = "Preview";
$eReportCard['Generate'] = "Generate";
$eReportCard['Adjust'] = "Adjust";
$eReportCard['Generation'] = "Generation";
$eReportCard['ManualAdjustment'] = "Manual Adjustment";
$eReportCard['LastGenerate'] = "Last Generate";
$eReportCard['LastArchive'] = "Last Archive";
$eReportCard['LastAdjust'] = "Last Adjust";
$eReportCard['AdjustmentList'] = "Adjustment List";
$eReportCard['LastPrint'] = "Last Print";
$eReportCard['MarkModifiedOn'] = "Mark has been modified on";
$eReportCard['ReportCardGeneratedOn'] = "Report card has been generated on";
$eReportCard['LastGenerationDate'] = "Last Generation Date";
$eReportCard['LastTransferDate'] = "Last Transfer Date";
$eReportCard['LastExportDate'] = "Last Export Date";
$eReportCard['SchoolYear'] = "School Year";
$eReportCard['Semester'] = "Semester";
$eReportCard['Transfer'] = "Transfer";
$eReportCard['AllTypes'] = "All Types";
$eReportCard['ViewReportCard'] = "View Report Card";

// Schedule
$eReportCard['AllSemesters'] = "All Semesters";
$eReportCard['AllReportCards'] = "All Report Cards";
$eReportCard['AllForms'] = "All Class Levels";
$eReportCard['ReportTitle'] = "Report Title";
$eReportCard['Submission'] = "Submission";
$eReportCard['Verification'] = "Parent & Student Verification";
$eReportCard['PublishReport'] = "Publishing";
$eReportCard['IssueDate'] = "Issue Date";
$eReportCard['Start'] = "Start";
$eReportCard['End'] = "End";
$eReportCard['NoSchedule'] = "No Schedules";
$eReportCard['SubmissionPeriod'] = "Submission Period";
$eReportCard['VerificationPeriod'] = "Parent & Student Verification Period";
$eReportCard['PublishReportPeriod'] = "Report Publishing Period";
$eReportCard['AlertComparePeriod'] = "The Start Date cannot be greater than the End Date.";
$eReportCard['AlertNoSubStartDate'] = "Start Date of Submission Period is empty or invalid, please enter again.";
$eReportCard['AlertNoIssueDate'] = "Issue Date is empty or invalid, please enter again.";
$eReportCard['AlertCompareSubToVer'] = "Verification Period must not be earlier than Submission Period!";

// Other Info
########### Need to be added if more categories are required ###########
$eReportCard['SummaryInfoUpload'] = "Summary Info";
$eReportCard['AwardRecordUpload'] = "Awards Record";
$eReportCard['MeritRecordUpload'] = "Merits & Demerits Record";
$eReportCard['ECARecordUpload'] = "ECA Record";
$eReportCard['RemarkUpload'] = "Remark";
$eReportCard['InterSchoolCompetitionUpload'] = "Inter-school Competitions Record";
$eReportCard['SchoolServiceUpload'] = "School Service Record";
$eReportCard['AttendanceUpload'] = "Attendance Record";
$eReportCard['DemeritUpload'] = "Punishment Record";
$eReportCard['OLEUpload'] = "OLE Record";
$eReportCard['Post'] = "Post Record";
$eReportCard['OthersUpload'] = "Others Record";

$eReportCard['OtherInfoArr']['summary'] = "Summary Info";
$eReportCard['OtherInfoArr']['award'] = "Awards Record";
$eReportCard['OtherInfoArr']['merit'] = "Merits & Demerits Record";
$eReportCard['OtherInfoArr']['eca'] = "ECA Record";
$eReportCard['OtherInfoArr']['remark'] = "Remark";
$eReportCard['OtherInfoArr']['interschool'] = "Inter-school Competitions Record";
$eReportCard['OtherInfoArr']['schoolservice'] = "School Service Record";
$eReportCard['OtherInfoArr']['attendance'] = "Attendance Record";
$eReportCard['OtherInfoArr']['dailyPerformance'] = "Daily Performance Record";
$eReportCard['OtherInfoArr']['demerit'] = "Punishment Record";
$eReportCard['OtherInfoArr']['OLE'] = "OLE Record";
$eReportCard['OtherInfoArr']['assessment'] = "Assessment";
$eReportCard['OtherInfoArr']['post'] = "Post Record";
$eReportCard['OtherInfoArr']['subjectweight'] = "Subject Weight";
$eReportCard['OtherInfoArr']['others'] = "Others Record";
$eReportCard['OtherInfoArr']['additional_comments'] = "Additional Comment";
$eReportCard['OtherInfoArr']['position'] = "Class Position";
$eReportCard['OtherInfoArr']['skills'] = "Generic Skills";
$eReportCard['OtherInfoArr']['themeForInvestigation'] = "Theme For Investigation";
$eReportCard['OtherInfoArr']['TFIPerformance'] = "TFI Performance";
$eReportCard['OtherInfoArr']['foreignStudent'] = "Foreign Student";
$eReportCard['OtherInfoArr']['promotion'] = "Promotion";
$eReportCard['OtherInfoArr']['reading_academy'] = "Readers' Academy";
$eReportCard['OtherInfoArr']['english_as_a_lang'] = "English as a Study Language";
$eReportCard['OtherInfoArr']['otherinfor'] = "Other Information";
$eReportCard['OtherInfoArr']['behaviour'] = "Behaviour";
$eReportCard['OtherInfoArr']['transcript'] = "Transcript";

$eReportCard['ManagementArr']['OtherInfoArr']['ImportDataInstruction'] = "If you want to update existing information, please export and update the original csv file before uploading to the system. Otherwise, the original information of selected class or form will be replaced.";

########################################################################
$eReportCard['File'] = "File";
$eReportCard['AllSchoolYears'] = "All School Years";
$eReportCard['AllTerms'] = "All Terms";
$eReportCard['AllClasses'] = "All Classes";
$eReportCard['FileRemoveConfirm'] = "Are you sure you want to remove the file?";
$eReportCard['FileRemoveAllConfirm'] = "Are you sure you want to remove all files?";
$eReportCard['NoClass'] = "No Class";
$eReportCard['NoSemester'] = "No Semester";
$eReportCard['NoForm'] = "No Form";
$eReportCard['Form'] = "Form";
$eReportCard['FormName'] = "Form";
$eReportCard['AlertExistFile'] = "File already exist for the same class on the same semester.\\nOverwrite it with the new file?";

// Marksheet Revision & Edit
$eReportCard['Marksheet'] = "Marksheet";
$eReportCard['TeacherComment'] = "Teacher Comment";
$eReportCard['Feedback'] = "Feedback";
$eReportCard['LastModifiedDate'] = "Last Modified Date";
$eReportCard['LastModifiedBy'] = "Last Modified By";
$eReportCard['Confirmed'] = "Completed";
$eReportCard['Total'] = "Total";
$eReportCard['RawMarks'] = "Raw Marks";
$eReportCard['WeightedMarks'] = "Weighted Marks";
$eReportCard['ConvertedGrade'] = "Converted Grade";
$eReportCard['InputMarks'] = "Input Marks";
$eReportCard['PreviewMarks'] = "Preview Marks";
$eReportCard['PreviewGrades'] = "Preview Grades";
$eReportCard['Import'] = "Import";
$eReportCard['Export'] = "Export";
//$eReportCard['MarkRemind'] = "Remark: \"abs\" stands for absent; \"/\" for no enrollment or exempt.";
$eReportCard['MarkRemindSet1'] = "Remark: \"+\" stands for absent (zero mark); \"-\" for absent (not considered); \"*\" for dropped; \"/\" for exempt; \"N.A.\" for not assessed.";
$eReportCard['MarkRemindSet1_WithEstimate'] = "Remark: \"+\" stands for absent (zero mark); \"-\" for absent (not considered); \"*\" for dropped; \"/\" for exempt; \"N.A.\" for not assessed; \"#\" for estimated mark.";
$eReportCard['MarkRemindSet2'] = "Remark: \"abs\" stands for absent; \"*\" for dropped; \"/\" for exempt; \"N.A.\" for not assessed.";
$eReportCard['MarkRemindSet_SubMS'] = "Remark: \"+\" stands for absent (zero mark); \"/\" for exempt.";
$eReportCard['SubMS'] = "Sub-MS";
$eReportCard['SubMarksheet'] = "Sub-Marksheet";
$eReportCard['Student'] = "Student";
$eReportCard['StudentNo_short'] = "Class No";
$eReportCard['TermSubjectMark'] = "Term Subject Mark";
$eReportCard['TermSubjectMarkEn'] = "Term Subject Mark";
$eReportCard['TermSubjectMarkCh'] = "學期科目分數";
$eReportCard['OverallSubjectMark'] = "Overall Subject Mark";
$eReportCard['OverallSubjectMarkEn'] = "Overall Subject Mark";
$eReportCard['OverallSubjectMarkCh'] = "科目總分";
$eReportCard['CmpSubjectMarksheet'] = "Component Subject Marksheet";
$eReportCard['CmpSubjects'] = "Component Subjects";

$eReportCard['RecordUpdatedSuccessfully'] = "Record(s) Updated Successfully";
$eReportCard['NoRecordIsUpdated'] = "No Record is updated.";
$eReportCard['RecordShowingMSContainEmptyMark'] = "Record(s) showing Marksheet(s) contain(s) empty mark";
$eReportCard['CurrentStatus'] = "Current Status";
$eReportCard['Reason'] = "Reason";
$eReportCard['Status'] = "Status";

$eReportCard['TeachersComment'] = "Teacher's Comment";
$eReportCard['Period'] = "Period";

$eReportCard['NotCompleted'] = "Not Completed";

$eReportCard['ImportMarks'] = "Import Marks";
$eReportCard['ExportMarks'] = "Export Marks";
$eReportCard['DownloadCSV'] = "Download CSV File";
$eReportCard['DownloadCSVFile'] = "Download CSV file template";
$eReportCard['CSVMarksheetTemplateReminder'] = "In the CSV file template, any input mark under the column, which column title contains (N/A), will be ignored when processing.";

$eReportCard['RemarkAbsent'] = "Absent";
$eReportCard['RemarkAbsentZeorMark'] = "+ : Absent (zero mark)";
$eReportCard['RemarkAbsentNotConsidered'] = "- : Absent (not considered)";
$eReportCard['RemarkDropped'] = "* : Dropped";
$eReportCard['RemarkExempted'] = "/ : Exempted";
$eReportCard['RemarkNotAssessed'] = "N.A. : Not Assessed";

$eReportCard['ClearPosition'] = "Clear Position";
$eReportCard['jsWarningClearPosition'] = 'Are you sure you want to clear all manual adjusted position of the subject in this report?';

$eReportCard['jsFillGradingSchemeTitle'] = 'Please fill in the Grading Scheme Title.';
$eReportCard['jsConfirmDeleteGradingScheme'] = 'Are you sure to delete the selected grading scheme. It would affect other subjects which have used this scheme.';
$eReportCard['jsCheckDisplayResult'] = 'The Type of Display Result of %1$s is not valid as the type of Input Scale is Grade.';
$eReportCard['jsSthMustBePositive'] = 'The %1$s must be positive';
$eReportCard['jsSthMustBeNumericalNumber'] = 'The %1$s must be in form of numerical number.';
$eReportCard['jsSthCannotBeLargerThanFullMark'] = 'The %1$s cannot be larger than the full mark of grading scheme.';
$eReportCard['jsInvalidSth'] = 'Invalid %1$s';
$eReportCard['jsCheckTotalUpperLimit'] = 'The total of UpperLimit should be equal to 100%.';
$eReportCard['jsCheckOverallSubjectWeightedMark'] = 'The Overall Weighted Subject Mark cannot be larger than the full mark.';
# added on 15 Dec 2008
$eReportCard['jsSthCannotBeLessThanPassingMark'] = 'The %1$s cannot be less than the passing mark of grading scheme.';
# added on 16 Dec 2008
$eReportCard['jsLowerLimitCannotBeTheSame'] = 'The lower limits cannot be the same with each other.';


# added on 29 Apr 2008
$eReportCard['jsAlertToCompleteMarksheet'] = 'Are you sure to complete marksheet(s)?';
$eReportCard['jsAlertToIncompleteMarksheet'] = 'The status of Marksheet will be changed to incomplete. Are you sure to process?';
$eReportCard['jsInputMarkInvalid'] = 'Invalid input mark.';
$eReportCard['jsInputMarkCannotBeNegative'] = 'The input mark cannot be negative.';
$eReportCard['jsInputMarkCannotBeLargerThanFullMark'] = 'The input mark cannot be larger than the full mark.';
$eReportCard['jsConfirmMarksheetToIncomplete'] = 'This Marksheet has already been completed, are you sure to update the marksheet? if Yes, it will change the status of marksheet to \"Not Completed\".';
$eReportCard['jsConfirmCmpSubjectMarksheetToIncomplete'] = 'Certain Component Marksheet(s) has/have already been completed, are you sure to update the marksheet? if Yes, it will change the status of marksheet to \"Not Completed\".';
$eReportCard['jsAlertDuplicateGradingTitle'] = 'The Grading Title is already exist, please enter a new title.';
# added on 6 Nov 2008
$eReportCard['jsCheckTotalUpperLimitDistinction&Pass'] = 'The total of UpperLimit of Distinction and Pass should be equal to 100%.';
$eReportCard['jsCheckTotalUpperLimitFail'] = 'The total of UpperLimit of Fail should be equal to 100%.';
# added on 25 Nov 2008
$eReportCard['jsMarkEditedFromSubMS'] = 'Marks were updated according to the Sub-Marksheet. Please click the "Save" button to confirm the changes.';
$eReportCard['jsFirefoxNotAllowPaste'] = 'The copy and paste function is disabled by the browser. Please enter "about:config" in the URL field and set "signed.applets.codebase_principal_support" as "true".';
$eReportCard['jsFillAllNA'] = 'Are you sure you want to fill all empty marks as "N.A."?';
# added on 1 Dec 2009
$eReportCard['jsAlertToRemoveSelectedSubject'] = 'All subject(s) selected will be removed in this selection if you uncheck the option. Are you sure you want to uncheck the option?';
$eReportCard['jsAlertGreaterMaxNumber'] = 'Maximum number of fail subject(s) cannot be greater than the number of selected subject(s)';

$eReportCard['jsSelectGradingScheme'] = 'Please select a Grading Scheme for %1$s.';
$eReportCard['jsSelectInputScale'] = 'Please select a Input Scale for %1$s.';
$eReportCard['jsSelectDisplayScale'] = 'Please select a Display Scale for %1$s.';


# added on 30 Apr 2008
// Marksheet Confirm Complete Status
$eReportCard['MarksheetNotCompleted'] = 'The Marksheet has not completed.';
$eReportCard['MarksheetOfTermReportNotCompleted'] = 'The Marksheet(s) of Term Report(s) has/have not completed.';
$eReportCard['MarksheetOfCmpSubjectNotCompleted'] = 'The Marksheet of Component Subject(s) has/have not completed.';

// Sub-MS
$eReportCard['TransferOverallMarkToMS'] = "Transfer overall mark to marksheet";

######################

// Class Teacher's Comment
$eReportCard['ChangeAllTo'] = "Change All to";
$eReportCard['ClassTeacherCommentTemp']['View'] = "[View]";
$eReportCard['ClassTeacherCommentTemp']['ViewAll'] = "[View All]";
$eReportCard['ClassTeacherCommentTemp']['Hide'] = "[Hide]";
$eReportCard['ClassTeacherCommentTemp']['HideAll'] = "[Hide All]";

// Progress
$eReportCard['NotSet'] = "Not set";
$eReportCard['SendNotification'] = "Send Notification";
$eReportCard['NotStartedYet'] = "Not Started Yet";
$eReportCard['InProgress'] = "In Progress";
$eReportCard['Feeback'] = "Feeback";
$eReportCard['SetToComplete'] = "Set to Complete";
$eReportCard['SetToNotComplete'] = "Set to not Complete";
$eReportCard['NoSubjectGroup'] = "No Subject Group";

// Common 
$eReportCard['SettingsNotCompleted'] = "Incomplete";
$eReportCard['SettingsCompleted'] = "Completed";
$eReportCard['SettingsProgress'] = "Setting Progress";
$eReportCard['ExportStudentInfo'] = "Export Student Information";
$eReportCard['ExportStudentRecords'] = "Export Student Records";

// Report Template
$eReportCard['Template']['StudentInfo']['Name'] = "Name";
$eReportCard['Template']['StudentInfo']['ClassIndexNo'] = "Class Index No.";
$eReportCard['Template']['StudentInfo']['StudentAdmNo'] = "Student Adm. No.";
$eReportCard['Template']['StudentInfo']['UserLogin'] = "User Login";
$eReportCard['Template']['StudentInfo']['STRN'] = "STRN";
$eReportCard['Template']['StudentInfo']['Class'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassName'] = "Class";
$eReportCard['Template']['StudentInfo']['ClassNo'] = "Class No.";
$eReportCard['Template']['StudentInfo']['StudentNo'] = "Student No.";
$eReportCard['Template']['StudentInfo']['ClassTeacher'] = "Class Teacher";
$eReportCard['Template']['StudentInfo']['AcademicYear'] = "Academic Year";
$eReportCard['Template']['StudentInfo']['DateOfIssue'] = "Date of Issue";
$eReportCard['Template']['StudentInfo']['DateOfBirth'] = "Date of Birth";
$eReportCard['Template']['StudentInfo']['Gender'] = "Gender";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['Ch'] = "文組人數";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['En'] = "Enrolled (Art&Com)";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['Ch'] = "理組人數";
//$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['En'] = "Enrolled (Science)";
$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['Ch'] = "文商組人數";
$eReportCard['Template']['StudentInfo']['SubjectStream']['A']['En'] = "Art & Com. Enrolled";
$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['Ch'] = "理組人數";
$eReportCard['Template']['StudentInfo']['SubjectStream']['S']['En'] = "Science Enrolled";
$eReportCard['Template']['SubjectEng'] = "Subject";
$eReportCard['Template']['SubjectChn'] = "科目";
$eReportCard['Template']['SubjectTeacherComment'] = "Subject Teacher's Comment";
$eReportCard['Template']['Ranking'] = "Ranking";
$eReportCard['Template']['ClassTeacher'] = "Class Teacher";
$eReportCard['Template']['Principal'] = "Principal";
$eReportCard['Template']['ParentGuardian'] = "Parent / Guardian";
$eReportCard['Template']['SchoolChop'] = "School Chop";
$eReportCard['Template']['TeacherSignature'] = "Teacher's Signature";
$eReportCard['Template']['PrincipalSignature'] = "Principal's Signature";
$eReportCard['Template']['ParentSignature'] = "Parent's Signature";
$eReportCard['Template']['SubjectOverall'] = "Subject Overall";
$eReportCard['Template']['FirstCombined'] = "First Combined";
$eReportCard['Template']['SecondCombined'] = "Second Combined";
$eReportCard['Template']['OverallCombined'] = "Overall";
$eReportCard['Template']['GrandTotal'] = "Grand Total";
$eReportCard['Template']['GrandTotal2'] = "總分";
$eReportCard['Template']['AverageMark'] = "Average Mark";
$eReportCard['Template']['AverageMark2'] = "平均分";
$eReportCard['Template']['Position'] = "Position";
$eReportCard['Template']['Position2'] = "排名";
$eReportCard['Template']['NoOfStudent'] = "No. of students";
$eReportCard['Template']['NoOfStudent2'] = "學生數目";
$eReportCard['Template']['Grade'] = "Grade";
$eReportCard['Template']['Mark'] = "Mark";
$eReportCard['Template']['Remarks'] = "Remarks";
$eReportCard['Template']['AchievementBand'] = "Achieve't Band";
$eReportCard['Template']['AchievementGrade'] = "Achieve't Grade";
$eReportCard['Template']['1SemestralAssessment'] = "< 1st Semestral Assessment >";
$eReportCard['Template']['2SemestralAssessment'] = "< 2nd Semestral Assessment >";
$eReportCard['Template']['Overall'] = "< --- --- --- Overall --- --- --- >";
$eReportCard['Template']['Attendance'] = "Attendance";
$eReportCard['Template']['Conduct'] = "Conduct";
$eReportCard['Template']['Total'] = "Total";
$eReportCard['Template']['Percentage'] = "Percentage";
$eReportCard['Template']['Passed'] = "Passed";
$eReportCard['Template']['Failed'] = "Failed";
$eReportCard['Template']['Promoted'] = "Promoted";
$eReportCard['Template']['Retained'] = "Retained";
$eReportCard['Template']['Advanced'] = "Advanced";
$eReportCard['Template']['1Sem'] = "1st Sem ";
$eReportCard['Template']['2Sem'] = "2nd Sem";
$eReportCard['Template']['Assessment'] = "Assess't";
$eReportCard['Template']['CCA'] = "CCA";
$eReportCard['Template']['NAPFA'] = "NAPFA";
$eReportCard['Template']['SSPA_Footer'] = 
"注意:<br />
(一) 本表績分須呈教統局升中派位組，請特別留意該績分是否與試卷相同。<br />
(二) 本績分表只填寫考試分數，不包括常分在內。<br />
(三) 合計後如出現小數位，則將小數位四捨五入，音樂及視覺藝術為五分之差。
";
$eReportCard['Template']['SSPA_Signature'] = "教師署名：______________________";
$eReportCard['Template']['ClassPosition'] = "Position in Class";
$eReportCard['Template']['FormPosition'] = "Position in Level";
$eReportCard['Template']['StreamPosition'] = "Position in Stream";
$eReportCard['Template']['SubjGroupPosition'] = "Position in Subject Group";
$eReportCard['Template']['GrandMarksheet'] = "Grand Marksheet";
$eReportCard['Template']['GrandAverage'] = "Grand Average";
$eReportCard['Template']['GrandAverageGrade'] = "Grand Average Grade";
$eReportCard['Template']['ActualAverage'] = "Actual Average";
$eReportCard['Template']['TotalUnitFailed'] = "Total Unit(s) Failed";
$eReportCard['Template']['GrandAverageEn'] = "Grand Average";
$eReportCard['Template']['GrandAverageCh'] = "總平均分";
$eReportCard['Template']['GrandTotalEn'] = "Grand Total";
$eReportCard['Template']['GrandTotalCh'] = "總分";
$eReportCard['Template']['GPAEn'] = "GPA";
$eReportCard['Template']['GPACh'] = "總等級分數平均分";
$eReportCard['Template']['GrandStandardScore'] = "Grand S.D. Score";
$eReportCard['Template']['GrandStandardScoreEn'] = "Grand S.D. Score";
$eReportCard['Template']['GrandStandardScoreCh'] = "總標準分";
$eReportCard['Template']['Annual'] = "Annual";
$eReportCard['ForOverallColumnOnly'] = "For overall column only";

// Other Student Info	# added on 28 Apr 2008
$eReportCard['OtherStudentInfo'] = 'Other Student Info';
$eReportCard['ChangeAllTo'] = "Change All to";
$eReportCard['OtherStudentInfo_Attendance'] = array(1=>'Regular', 2=>'Irregular');
$eReportCard['OtherStudentInfo_Conduct'] = array(4=>'Excellent', 3=>'Very Good', 2=>'Good', 1=>'Satisfactory');
$eReportCard['OtherStudentInfo_NAPFA'] = array(1=>'Nil', 2=>'Gold', 3=>'Silver', 4=>'Bronze', 5=>'Exempted');
$eReportCard['OtherStudentInfoValue_CIPHours'] = 'CIP Hours';
$eReportCard['OtherStudentInfoValue_Attendance'] = 'Attendance';
$eReportCard['OtherStudentInfoValue_Conduct'] = 'Conduct';
$eReportCard['OtherStudentInfoValue_NAPFA'] = 'NAPFA';
$eReportCard['OtherStudentInfoValue_CCARemarks'] = 'CCA Remarks';
$eReportCard['jsCIPHourCannotBeNegative'] = 'The CIP hours value cannot be negative.';
$eReportCard['jsCIPHourInvalid'] = 'Invalid CIP hours';

// Geneate Report
$eReportCard['jsConfirmToGenerateReport'] = 'Manual adjusted marks of the report will be deleted if you generate the report card. Are you sure you want to generate report?';
$eReportCard['ReportPrinting'] = "Report Printing";
$eReportCard['ReportPreview&Printing'] = "Report Previewing & Printing";
$eReportCard['Promotion'] = "Promotion";
$eReportCard['PromotionEdit'] = "Edit Promotion";
$eReportCard['NewClassLevel'] = "New Class Level";
$eReportCard['NewClassLevelAlert'] = "New Class Level must be two characters long";

// Grand Marksheet
$eReportCard['SelectCriteria'] = "Select Criteria";
$eReportCard['AllSubjects'] = "All Subjects";
$eReportCard['GrandMarksheetType'] = "Grand Marksheet Type";
$eReportCard['NoReportAvailable'] = "No Report Available";
$eReportCard['NoTermAvailable'] = "No Term Available";

//$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking", "Pupils' Progress");
$eReportCard['GrandMarksheetTypeOption'] = array("Class Summary", "Level Ranking");
$eReportCard['GrandMarksheetTypeAlert1'] = "Pupils' Progress cannot be calculated for Report with only one term.";
# added on 15 Oct 2008
$eReportCard['ShowSummaryInfo'] = "Show Summary Info";
$eReportCard['ShowSubject'] = "Show Subject(s)"; # added on 3 Dec 2009
$eReportCard['ShowSubjectComponent'] = "Show Subject Component(s)";
$eReportCard['ShowGrandTotal'] = "Show Grand Total";
$eReportCard['ShowGrandAverage'] = "Show Grand Average";
$eReportCard['ShowActualAverage'] = "Show Actual Average";
$eReportCard['ShowTotalUnitFailed'] = "Show Total Unit(s) Failed";
$eReportCard['StudentNameDisplay'] = "Student Name Display";
$eReportCard['ShowGender'] = "Show Gender"; 
$eReportCard['ShowUserLogin'] = "Show User Login"; 
$eReportCard['ShowGPA'] = "Show GPA";
$eReportCard['RankingDisplay'] = "Ranking Display";
# added on 16 Oct 2008
$eReportCard['For_SSPA'] = "For SSPA";
# added on 19 Dec 2008
$eReportCard['ViewFormat'] = "View Format";
$eReportCard['HTML'] = "HTML";
$eReportCard['CSV'] = "CSV";
$eReportCard['PDF'] = "PDF";
$eReportCard['EnglishName'] = "English Name";
$eReportCard['ChineseName'] = "Chinese Name";
$eReportCard['ClassRanking'] = "Class Ranking";
$eReportCard['OverallRanking'] = "Overall Ranking";
# added on 22 Dec 2008
$eReportCard['Template']['Average'] = "Average";
$eReportCard['Template']['SD'] = "Standard Deviation";
$eReportCard['Template']['Variance'] = "Variance";
$eReportCard['Template']['HighestMark'] = "Highest Mark";
$eReportCard['Template']['LowestMark'] = "Lowest Mark";
$eReportCard['Template']['PassingRate'] = "Passing Rate";
# added on 24 Dec 2008
$eReportCard['ShowStatistics'] = "Show Statistics";
$eReportCard['Formula'] = "Formula";

$eReportCard['Template']['PassingNumber'] = "Passing Number";
$eReportCard['Template']['SubjectWeight'] = "Subject Weight";
$eReportCard['Template']['GrandAvgPassNumber'] = "Number of Grand Average Pass";
$eReportCard['Template']['GrandAvgFailNumber'] = "Number of Grand Average Fail";
$eReportCard['Template']['ActualAvgPassNumber'] = "Number of Actual Average Pass";
$eReportCard['Template']['ActualAvgFailNumber'] = "Number of Actual Average Fail";
$eReportCard['SubjectDisplay'] = "Subject Display"; 
$eReportCard['Abbr'] = "Abbreviation ";
$eReportCard['ShortName'] = "Short Form";
$eReportCard['Desc'] = "Description";
$eReportCard['TotalNum'] = "Total number of students";
$eReportCard['GrandMarkSheet']['Gender']["M"] = "Male";
$eReportCard['GrandMarkSheet']['Gender']["F"] = "Female";
$eReportCard['WholeForm'] = "Whole form";
$eReportCard['StatNotShown'] = "If the result of the subject display as grade, the statistic will not be shown";
$eReportCard['NumOfPassSubject'] = "Number of Passed Subject";
$eReportCard['NumOfFailSubject'] = "Number of Failed Subject";

// Data Handling
$eReportCard['DataTransition'] = "Data Transition";
$eReportCard['DataDeletion'] = "Data Deletion";
$eReportCard['DataTransfer_To_iPortfolio'] = "Data Transfer (to iPortfolio)";
$eReportCard['DataExport_To_WebSAMS'] = "Data Export (to WebSAMS)";
$eReportCard['DataTransfer_To_WebSAMS'] = "Data Transfer (to WebSAMS)";
$eReportCard['DataTransitionWarning1'] = "Please proceed this action before:<br />- start using eReportCard for a new academic year, or,<br />- restore eReportCard to use the data of previous academic years";
$eReportCard['DataTransitionWarning2'] = "The data from the current active academic year will be preserved.";
$eReportCard['ActiveAcademicYear'] = "Active Academic Year";
$eReportCard['CurrentAcademicYear'] = "Current Academic Year";
$eReportCard['Transition'] = "Transition";
$eReportCard['NewAcademicYear'] = "New academic year";
$eReportCard['OtherAcademicYears'] = "Other academic years";
$eReportCard['ConfirmCreateNewYearDatabase'] = "The template data of the new academic year will be copied from school year <!--academicYearName-->. Are you sure you want to create new academic year data for the system?";
$eReportCard['ConfirmTransition'] = "Proceed to do selected data transition?";
$eReportCard['DataDeletionWarning'] = "This process will remove selected report card data (including marks and teacher comments).";
$eReportCard['ConfirmDeletion'] = "Proceed to delete selected report card data?";
$eReportCard['NextYearTermNotComplete'] = "The term settings of the next academic year is incomplete.";
$eReportCard['Generated&ArchivedReport'] = "Generated & Archived Records";
$eReportCard['DeleteMarksheetRemarks'] = "Including Marksheet and Sub-marksheet Marks";
$eReportCard['DeleteReportRemarks'] = "Including Generated, Adjusted and Archived Marks";
$eReportCard['TargetDeletionRecord'] = "Target Deletion Record";
$eReportCard['Target'] = "Target";
$eReportCard['jsAtLeastOneDeletionRecord'] = "Please select at least one target deletion record.";
$eReportCard['ManagementArr']['DataHandlingArr']['TransferData'] = "Transfer Data";
$eReportCard['ManagementArr']['DataHandlingArr']['AcademicResult'] = "Academic Result";
$eReportCard['ManagementArr']['DataHandlingArr']['MockExamResult'] = "Mock Exam Result";
$eReportCard['ManagementArr']['DataHandlingArr']['ClassTeacherComment'] = "Class Teacher Comment";
$eReportCard['ManagementArr']['DataHandlingArr']['ForMainTermReportAndConsolidatedReportOnly'] = "For Main Term Report and Consolidated Report only";
$eReportCard['ManagementArr']['DataHandlingArr']['Conduct'] = "Conduct";
$eReportCard['ManagementArr']['DataHandlingArr']['SubjectFullMark'] = "Subject Full Mark";
$eReportCard['ManagementArr']['DataHandlingArr']['WarningArr']['SelectData'] = "Please select a data type";
$eReportCard['ManagementArr']['DataHandlingArr']['WarningArr']['DuplicatedSelection'] = "Please select unique assessment linkage";
$eReportCard['ManagementArr']['DataHandlingArr']['MockExamScoreFrom'] = "From column <!--reportColumn-->";


$button_promotion = "Promotion";
$button_finish_preview = "Finish and Preview";

$eReportCard['DisplayOverallResult'] = "Display Overall Result";
$eReportCard['DisplayGrandAvg'] = "Display Average Mark";
$eReportCard['ShowClassPosition'] = "Show Position in Class";
$eReportCard['ShowFormPosition'] = "Show Position in Form";

# added on 09 Dec 2008
$eReportCard['ManualAdjustMarkInstruction'] = "This page is optional. If you do not input the subject class position for your students, the system will generate the class position according to the marks which you have submitted.";

# added on 5 Jan 2008
$eReportCard['SubmissionStartDate'] = "Submission Start Date";
$eReportCard['SubmissionEndDate'] = "Submission End Date";
$eReportCard['VerificationStartDate'] = "Verification Start Date";
$eReportCard['VerificationEndDate'] = "Verification End Date";
$eReportCard['VerificationPeriodIsNotNow'] = "The current time is not within the marksheet verification period.";


# added on 3 Feb 2009
$eReportCard['FillAllEmptyMarksWithNA'] = "Fill all empty marks with N.A.";
$eReportCard['TotalRecords'] = "Total";

# added on 27 Apr 2009
$eReportCard['Warning'] = "Warning";
$eReportCard['SubjectConductMarkWarning'] = "eReportCard System will display the finalized conduct grade, instead of the converted conduct grade, in the report card once you have submitted this page.";

// Data Handling (to iPortfolio)
$eReportCard['jsConfirmToTransferToiPortfolio'] = 'Are you sure to transfer data to iPortfolio?';
$eReportCard['CurrentAcadermicYear'] = 'Current Acadermic Year';

# 20090627 yatwoon
$eReportCard['NoFormSetting'] = 'There is no Form setting at the moment.';
$eReportCard['orAbove'] = 'or Above';
$eReportCard['AllPass'] = 'All Pass';

// Honour Report
$eReportCard['GPA'] = 'GPA';
$eReportCard['Honour'][1] = 'First Class Honour';
$eReportCard['Honour'][2] = 'Second Class Honour (Division 1)';
$eReportCard['Honour'][3] = 'Second Class Honour (Division 2)';


$eReportCard['AwardName'] = 'Award Name';
$eReportCard['StudentNameEn'] = 'Student Name (Eng)';
$eReportCard['StudentNameCh'] = 'Student Name (Chi)';
$eReportCard['Criteria'] = 'Criteria';
$eReportCard['Template']['GPA'] = 'GPA';
$eReportCard['warnGPANotDesc'] = "Please reselect GPA. The GPA of each class must be greater than lower classes.";
$eReportCard['LastYearSchool'] = 'Last year school';


// Conduct Report
$eReportCard['Excluded']='Excluded ';
$eReportCard['GradeImprovement'] = "Grade Improvement";
$eReportCard['orabove'] = "or above";
$eReportCard['SelectDiffTerm'] = "Please select different terms";
$eReportCard['Reports_ConductProgressReport'] = "Conduct Progress Report";
$eReportCard['OneTermOnly'] = "Conduct Progress Report cannot be used if only 1 term was set up.";
$eReportCard['warnSelectTerm'] = "Please select at least 1 term.";
$eReportCard['warnSelectConduct'] = "Please select at least 1 conduct mark";
$eReportCard['warnTermOrder'] = "Please select terms in proper order";
$eReportCard['warnExcludedAllConduct'] = "Please do not exclude all conduct marks";
$eReportCard['maxRanking'] = "Maximum Ranking";

// Scholarship Report
$eReportCard['TargetHonour'] = "Target Honour";
$eReportCard['AllHonour'] = 'All Honour Classes';
$eReportCard['warnSelectOneTerm'] = "Please select exactly one term for Honour Report";
$eReportCard['warnSelectTwoTerm'] = "Please select at least two terms for Honour Student Schalorship";
$eReportCard['warnSelectHonour'] = "Please select at least one class honour for Honour Student Schalorship";
$eReportCard['ForScholarship'] = "For Scholarship";

// Grading Scheme
$eReportCard['AdditionalCriteria'] = "Additional Criteria";

// Feedback
$eReportCard['AllStatus'] = "-- Status --";
$eReportCard['Closed'] = "Closed";
$eReportCard['NoFeedback'] = "No feedback";
$eReportCard['EditMarksheet'] = "View Marksheet";

// Manual Adjustment
$eReportCard['MarkAdjustSuccess'] = "1|=|Mark Adjusted";
$eReportCard['MarkAdjustFail'] = "0|=|Mark Adjustment failed";

// Archive Report
$eReportCard['jsConfirmToArchiveReport'] = 'Old archived records of this report card will be overwritten. Are you sure you want to archive the report card?';

// Not current academic year warning
$eReportCard['jsNotCurrentAcademicYearInfo'] = '';
$eReportCard['jsNotCurrentAcademicYearInfo'] .= "eReportCard Active Year: |||--eRC Active Year--|||";
$eReportCard['jsNotCurrentAcademicYearInfo'] .= '\n';
$eReportCard['jsNotCurrentAcademicYearInfo'] .= "Current Academic Year: |||--Current Academic Year--|||";
$eReportCard['jsNotCurrentAcademicYearInfo'] .= '\n\n';
$eReportCard['jsNotCurrentAcademicYearTeacher'] = "The eReportCard system is not using the data of the current academic year. Please contact eReportCard Administrator if you want to view and process the data of the current academic year.";
$eReportCard['jsNotCurrentAcademicYearAdmin'] = "The eReportCard system is not using the data of the current academic year. Please switch the eReportCard to the current academic year by going to \\\"Management > Data Handling > Data Transition\\\" if you want to view and process the data of the current academic year.";

$eReportCard['jsWarningSelectClass'] = "Please select at least one class.";


// Sub-Marksheet Report
$eReportCard['Display'] = "Display";
$eReportCard['SubMarksheetColumn'] = "Sub-Marksheet Column";
$eReportCard['NoSubMarksheet'] = "No Sub-Marksheet"; 
$eReportCard['jsSelectSubjectWarning'] = "Please select at least one subject."; 
$eReportCard['jsSelectClassWarning'] = "Please select at least one class."; 
$eReportCard['jsSelectSubjectTeacherWarning'] = "Please select at least one Subject Teacher。"; 
$eReportCard['ApplyToAll'] = "Apply To All";

// Assessment Report
$eReportCard['DataSource'] = 'Data Source';
$eReportCard['DataSourceChoice']['Report'] = 'Report';
$eReportCard['DataSourceChoice']['Marksheet'] = 'Marksheet';

// Promotion Settings
$eReportCard['Settings_PromotionCriteria'] = "Promotion Criteria";
$eReportCard['PromotionCriteriaSettings'] = "Criteria Settings";
$eReportCard['PromotionInstruction'] = "Instruction";
$eReportCard['PromotionInstructionContent'] = "Student satisfying All of the following criteria will be consider as \"Promoted\". Other students will be considered \"Promoted on Trial\". ";
$eReportCard['PassGrandAverage'] = "Pass in Year Grand Average";
$eReportCard['MaximumFailSubject1'] = "Maximum fail in ";
$eReportCard['MaximumFailSubject2'] = "of the following subject(s)";
$eReportCard['SelectedSubject'] = "Selected Subject(s)";
$eReportCard['MaximumNotSubmitAssignment1'] = "Maximum not submitting ";
$eReportCard['MaximumNotSubmitAssignment2'] = "assignment(s)";
$eReportCard['MaximumNotSubmitAssignment3'] = "assignment(s)";
$eReportCard['Value'] = "Value";
$eReportCard['NoPromotionSettings'] = "There is no promotion settings for this form.";

// Examination Summary
$eReportCard['ExaminationSummary']['Subj'] = "Subj";
$eReportCard['ExaminationSummary']['Rank'] = "Rank";
$eReportCard['ExaminationSummary']['Year'] = "Year";
$eReportCard['ExaminationSummary']['Grade'] = "Grade";
$eReportCard['ExaminationSummary']['Mark'] = "Mark";
$eReportCard['ExaminationSummary']['Conduct'] = "Conduct";
$eReportCard['ExaminationSummary']['times'] = "time(s)";
$eReportCard['ExaminationSummary']['days'] = "day(s)";
$eReportCard['ExaminationSummary']['AverageMark'] = "Av.Mark";
$eReportCard['ExaminationSummary']['Absent'] = "Ab";
$eReportCard['ExaminationSummary']['Late'] = "Late";
$eReportCard['ExaminationSummary']['Position'] = "Pos";
$eReportCard['ExaminationSummary']['Cd1'] = "Diligence";
$eReportCard['ExaminationSummary']['Cd2'] = "Discipline";
$eReportCard['ExaminationSummary']['Cd3'] = "Manner";
$eReportCard['ExaminationSummary']['Cd4'] = "Sociability";
$eReportCard['ExaminationSummary']['YearResult'] = "Year Result";
$eReportCard['ExaminationSummary']['Exam'] = "Exam";
$eReportCard['ExaminationSummary']['FormTest'] = "Form Test";


// Subject Summary Sheet
$eReportCard['ClassNo'] = "Class Number";
$eReportCard['SubjectSummarySheet']['Percentage'] = "%";
$eReportCard['SubjectSummarySheet']['MaxMark'] = "Maximum Mark";
$eReportCard['SubjectSummarySheet']['MinMark'] = "Minimum Mark";
$eReportCard['SubjectSummarySheet']['SpecifiedPassingPercentage'] = "Specified Passing Percentage";
$eReportCard['SubjectSummarySheet']['%_OfFullMark'] = "% of Full Mark";
$eReportCard['PassRate'] = "Pass Rate";
$eReportCard['SubjectGroup'] = "Subject Group";
$eReportCard['ViewBy'] = "View By";

// Grade Distribution
$eReportCard["GradeDistribution"]["total_pass"] = "total pass";
$eReportCard["GradeDistribution"]["TotalInForm"] = "total in form";
$eReportCard["GradeDistribution"]["NumOfStudent"] = "Num Of Student";
$eReportCard["GradeDistribution"]["Percentage"] = "Percentage";
$eReportCard["GradeDistribution"]["Class"] = "Class";

# Lang Display of Form Setting 
$eReportCard['LangDisplay'] = "Language Display";
$eReportCard['LangDisplayChoice']['ch'] = "Chinese";
$eReportCard['LangDisplayChoice']['en'] = "English";
$eReportCard['LangDisplayChoice']['both'] = "Chinese and English";

# Display SubjectGroup
$eReportCard['DisplaySubjectGroup'] = "Display Subject Group";

# Subject Ranking Display Settings
$eReportCard['AboveAverageMark'] = "Above Average Mark";
$eReportCard['AboveMark'] = "Above Mark";
$eReportCard['NoSubjectSettingInThisForm'] = "There is no Subject settings for this Form yet";
$eReportCard['PositionRange'] = "Position Range";
$eReportCard['Top'] = "Top";
$eReportCard['Rank'] = "";
$eReportCard['Range'] = "Range";
$eReportCard['GrandPosition'] = "Grand Position";
$eReportCard['PositionRangeDisplayRemarks'] = "* For \"Position Range\", all positions will be displayed if the limit is set to 0";

// Settings > Personal Characteristics
$eReportCard['PersonalCharacteristics'] = "Personal Characteristics";
$eReportCard['PersonalCharacteristicsPool'] = "Personal Characteristics Pool";
$eReportCard['PersonalCharacteristicsFormSettings'] = "Personal Characteristics Form Settings";
$eReportCard['Scale'] = "Scale";
$eReportCard['NewPersonalCharacteristics'] = "New Personal Characteristics";
$eReportCard['EditPersonalCharacteristics'] = "Edit Personal Characteristics";
$eReportCard['Title_EN'] = "Title (English)";
$eReportCard['Title_CH'] = "Title (Chinese)";
$eReportCard['NoPersonalCharacteristicsSetting'] = "No personal characteristics is set.";
$eReportCard['ApplyOtherSubjects'] = "Apply to other subjects of the same Form";
$eReportCard['Excellent_en'] = "Excellent";
$eReportCard['Good_en'] = "Good";
$eReportCard['Satisfactory_en'] = "Satisfactory";
$eReportCard['NeedsImprovement_en'] = "Needs Improvement";
$eReportCard['NotApplicable_en'] = "Not Applicable";
$eReportCard['Excellent_b5'] = "優";
$eReportCard['Good_b5'] = "良";
$eReportCard['Satisfactory_b5'] = "尚可";
$eReportCard['NeedsImprovement_b5'] = "有待改進";
$eReportCard['NotApplicable_b5'] = "不適用";
$eReportCard['DisplayOrder'] = "Display Order";
$eReportCard['PersonalChar']['ExportTitleArr']['En'][] = "Class";
$eReportCard['PersonalChar']['ExportTitleArr']['En'][] = "Class No.";
$eReportCard['PersonalChar']['ExportTitleArr']['En'][] = "Student Name";
$eReportCard['PersonalChar']['ExportTitleArr']['Ch'][] = "班別";
$eReportCard['PersonalChar']['ExportTitleArr']['Ch'][] = "學號";
$eReportCard['PersonalChar']['ExportTitleArr']['Ch'][] = "學生姓名";
$eReportCard['PersonalChar']['ExportTitle']['Comment']['En'] = "Comment";
$eReportCard['PersonalChar']['ExportTitle']['Comment']['Ch'] = "評語";

$eReportCard['Button']['back_to_PersonalCharacteristics'] = "Back to ".$eReportCard['PersonalCharacteristics'];
$eReportCard['Represents'] = "represents";

// Settings > Course Description
$eReportCard['ApplyOtherTerm'] = "Apply to other terms";

// Settings > Report template setting
$eReportCard['BlockMarksheet'] = "Block Marksheet";

// Settings > Viewer Group
$eReportCard["UserAccessPages"] = "Accessible Pages";
$eReportCard["UserAddedDate"] = "Date Added";

$eReportCard['ShowSubjectPosition'] = "Show Subject Position";

$eReportCard["List"] = "List";
$eReportCard["AllPromotionStatus"] = "- All Status -";
$eReportCard["PromotionStatus"][1] = "Promoted";
$eReportCard["PromotionStatus"][2] = "Promoted on Trial";
$eReportCard["PromotionStatus"][3] = "Others";
$eReportCard["Promoted"] = "Promoted";
$eReportCard["PromotedonTrial"] = "Promoted on Trial";
$eReportCard["Retained"] = "Retained";
$eReportCard["EmptyPromotion"] = "(blank)";
$eReportCard["PromotionStatusInstruction"] = "The final status will be displayed as the csv records imported if there are any. Otherwise, the final status will be displayed as the generated status.";
$eReportCard["GenerationStatus"] = "Generation Status";
$eReportCard["FinalStatus"] = "Final Status";
$eReportCard["LastPromotionGeneration"] = "Last Promotion Generation";
$eReportCard["ConductGeneratedOn"] = "Conduct mark has been generated on";
$eReportCard["ReportGeneratedOn"] = "Report Card has been generated on";
$eReportCard["PromotionCriteriaUpdatedOn"] = "Promotion Criteria has been updated on";
$eReportCard["PromotionStatusReport"] = "Promotion Status Report";

$eReportCard["ConductNotGenerated"] = "Conduct mark has not been generated in eDiscipline yet";
$eReportCard["ReportNotGenerated"] = "Report Card has not been generated yet";

$eReportCard['DeletedStudentLegend'] = '<font style="color:red;">*</font> Representing that the student has been left or deleted already.';

$eReportCard['GrandStandardScore'] = "Grand S.D. Score";

$eReportCard['AssessmentRatioRemarks'] = "Please input decimal number, e.g. 0.2";
$eReportCard['AssessmentRatioRemarks2'] = "Please input percentage, e.g. 20";

$eReportCard['TermStartDate'] = "Term Start Date";
$eReportCard['TermEndDate'] = "Term End Date";

$eReportCard['AssessmentRatioRemarks'] = "Please input decimal number,e.g. 0.2";

$eReportCard['jsWarnRatioNotANumber'] = "Ratio must be a number";
$eReportCard['jsWarnFullMarkNotANumber'] = "Full mark must be a number";
$eReportCard['jsWarnPassingMarkNotANumber'] = "Passing mark must be a number";
$eReportCard['jsWarnPassingMarkLargerThanFullMark'] = "Passing mark cannot be greater than full mark";

$eReportCard['Mean'] = "Mean";
$eReportCard['StandardDeviation'] = "Standard Deviation";
$eReportCard['PassingPercentage'] = "Passing percentage";
$eReportCard['PassingMark'] = "Passing mark";
$eReportCard['No.Sat'] = "No. Sat";
$eReportCard['PercentagePass'] = "% pass";
$eReportCard['MaxScore'] = "Max. Score";
$eReportCard['MinScore'] = "Min. Score";
$eReportCard['JobTitle'] = "Title";

$eReportCard['GeneralArr']['GrandAverage'] = "Grand Average";
$eReportCard['GeneralArr']['Ranking'] = "Ranking";

# Management > Academic Progress
$eReportCard['ManagementArr']['AcademicProgressArr']['GenerationOptions'] = "Generation Options";
$eReportCard['ManagementArr']['AcademicProgressArr']['DetermineBy'] = "Determine By";
$eReportCard['ManagementArr']['AcademicProgressArr']['FromReportCard'] = "From Report Card";
$eReportCard['ManagementArr']['AcademicProgressArr']['ToReportCard'] = "To Report Card";
$eReportCard['ManagementArr']['AcademicProgressArr']['FromMark'] = "From Mark";
$eReportCard['ManagementArr']['AcademicProgressArr']['ToMark'] = "To Mark";
$eReportCard['ManagementArr']['AcademicProgressArr']['MarkDifference'] = "Mark Difference";
$eReportCard['ManagementArr']['AcademicProgressArr']['ClassPosition'] = "Class Position";
$eReportCard['ManagementArr']['AcademicProgressArr']['FormPosition'] = "Form Position";
$eReportCard['ManagementArr']['AcademicProgressArr']['ProgressPrize'] = "Progress Prize";
$eReportCard['ManagementArr']['AcademicProgressArr']['TopClassPosition'] = "Top Class Position";
$eReportCard['ManagementArr']['AcademicProgressArr']['TopFormPosition'] = "Top Form Position";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReportRemarks'] = "This report can view the subjects which are input as Mark only.";
$eReportCard['ManagementArr']['AcademicProgressArr']['SetPrize'] = "Set Prize";
$eReportCard['ManagementArr']['AcademicProgressArr']['CancelPrize'] = "Cancel Prize";
$eReportCard['ManagementArr']['AcademicProgressArr']['ProgressPrizeStatus'] = "All Progress Prize Status";
$eReportCard['ManagementArr']['AcademicProgressArr']['WithPrize'] = "With Prize";
$eReportCard['ManagementArr']['AcademicProgressArr']['WithoutPrize'] = "Without Prize";
$eReportCard['ManagementArr']['AcademicProgressArr']['jsWarningArr']['ConfirmGenerate'] = "Generated Academic Progress records will be overwritten if you generate Progress records to this report card. Are you sure you want to generate Academic Progress to this report card?";
$eReportCard['ManagementArr']['AcademicProgressArr']['jsWarningArr']['SameReportSelected'] = "The two selected reports cannot be the same. Please select other reports.";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['GenerateSuccess'] = "1|=|Academic Progress Generated.";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['GenerateFailed'] = "0|=|Academic Progress Generation Failed.";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['ProgressPrizeUpdateSuccess'] = "1|=|Progress Prize Updated.";
$eReportCard['ManagementArr']['AcademicProgressArr']['ReturnMsgArr']['ProgressPrizeUpdateFailed'] = "0|=|Progress Prize Update Failed.";


# Remarks Report
$eReportCard['RemarksReport']['ReportTitle'] = "Master Report (Remarks)";
$eReportCard['RemarksReport']['No.'] = "No.";
$eReportCard['RemarksReport']['Name'] = "Name";
$eReportCard['RemarksReport']['Remarks'] = "Remarks";
$eReportCard['RemarksReport']['RemarksFontSize'] = "Remarks Font Size";

# Subject Pass Percentage Report
$eReportCard['PassPercentageReport']['Subject'] = "Subject";
$eReportCard['PassPercentageReport']['NumOfStudent'] = "No. of student";
$eReportCard['PassPercentageReport']['AdjustedPassingScore'] = "Adjusted passing score";
$eReportCard['PassPercentageReport']['%PassAdjusted'] = "% pass (Adjusted)";
$eReportCard['PassPercentageReport']['%PassRaw'] = "% pass (Raw)";
$eReportCard['PassPercentageReport']['Max'] = "Max.";
$eReportCard['PassPercentageReport']['Min'] = "Min.";
$eReportCard['PassPercentageReport']['Mean'] = "Mean";
$eReportCard['PassPercentageReport']['StandardDeviation'] = "Standard deviation";

# Class Teacher Comment
$eReportCard['ClassTeacherComment'] = "Class Teacher's Comment";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ImportMode'] = "Import Mode";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ImportNew'] = "Import New Comments";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['AppendExisting'] = "Append to Original Comments";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['Student'] = "Student";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['Comment'] = "Comment";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['GenerateReport'] = "Generated Report";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['AdditionalComment'] = "Additional Comment";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ManualAdjustComment'] = "Manual Adjust Comment";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['AddToStudentComment'] = "Add to Student Comment";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['SelectedComment'] = "Selected Comment(s)";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseReportCard'] = "Choose Report Card";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ChooseStudent'] = "Choose Student";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ClassTeacherCommentSubmissionPeriod'] = "Class Teacher's Comment Submission Period";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['OnlyStudentInTheSelectedFormWillBeShown'] = "Only the student in the selected form will be shown for selection.";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentSuccess'] = "1|=|Add Comment Success.";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['ReturnMsgArr']['AddCommentFailed'] = "0|=|Add Comment Failed.";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedCommentWillBeLostIfSearch'] = "All selected comment option will be lost if you search comment. Are you sure you want to search the comment now?";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectAtLeastOneComment'] = "Please select at least one comment.";
$eReportCard['ManagementArr']['ClassTeacherCommentArr']['jsWarningArr']['SelectedStudentWillBeRemovedIfChangedForm'] = "The selected student(s) will be removed if you change the Form now. Are you sure you want to change the Form?";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['ClassName'] = "Class Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['ClassName'] = "Class Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['ClassNumber'] = "Class Number";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['ClassNumber'] = "Class Number";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['WebSAMSRegNo'] = "WebSAMSRegNumber";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['WebSAMSRegNo'] = "WebSAMSRegNumber";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['StudentName'] = "Student Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['StudentName'] = "Student Name";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['CommentContent'] = "Comment Content";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['CommentContent'] = "Comment Content";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['AdditionalComment'] = "Additional Comment";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['AdditionalComment'] = "Additional Comment";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['En']['Conduct'] = "Conduct";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CsvHeaderArr']['Ch']['Conduct'] = "Conduct";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportStudentRemarks'] = "You may use \"Class Name and Class Number\" <b>or</b> \"WebSAMSRegNo\" to do the student mapping.";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentSuccess'] = "Import Class Teacher's Comment(s) Success.";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportCommentFailed'] = "Import Class Teacher's Comment(s) Failed.";


$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['ImportOtherComments'] = "Import Other Comment(s)";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['GoBackToCommentView'] = "Go Back to Class Teacher's Comment View";

$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['InvalidConduct'] = "Invalid conduct category.";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['NotAccessibleStudent'] = "No access right to edit this student's comment.";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentExceedCharacterLimit'] = "Comment exceeds characters limit.";


# Other Info
$eReportCard['ManagementArr']['OtherInfoArr']['WarningMsg'] = "If the Form csv file has been uploaded, all the data in the Class csv files of that Form will be ignored.";
$eReportCard['ManagementArr']['OtherInfoArr']['ExportPastYearData'] = "Export all year data";

# Master Report
$eReportCard['Overall'] = "Overall";
$eReportCard['ReportColumn'] = "Report Column";
$eReportCard['StandardScore'] = "Standard Score";
$eReportCard['RawStandardScore'] = "Raw Standard Score";
$eReportCard['MSScore'] = "Marksheet Score";
$eReportCard['FormPosition'] = "Position in Form";
$eReportCard['ClassPosition'] = "Position in Class";
$eReportCard['StreamPosition'] = "Position in Stream";
$eReportCard['Gender'] = "Gender";
$eReportCard['ClassNumber'] = "Class Number";
$eReportCard['GrandTotalGrade'] = "Grand Total Grade";
$eReportCard['GrandAverageGrade'] = "Grand Average Grade";
$eReportCard['GrandStandardScoreGrade'] = "Grand Standard Score Grade";
$eReportCard['GPAGrade'] = "GPA Grade";
$eReportCard['GrandGrade'] = "Grand Grade";
$eReportCard['NumOfStudent'] = "No. of Students";
$eReportCard['NumOfStudentStudyingInClass'] = "No. of Students Studying In Class";
$eReportCard['NumOfStudentStudyingInForm'] = "No. of Students Studying In Form";
$eReportCard['GrandTotal'] = "Grand Total";
$eReportCard['GrandAverage'] = "Grand Average";
$eReportCard['Average'] = "Average";
$eReportCard['SD'] = "Standard Deviation";
$eReportCard['HighestMark'] = "Highest Mark";
$eReportCard['LowestMark'] = "Lowest Mark";
$eReportCard['PassingRate'] = "Passed Rate";
$eReportCard['PassingNumber'] = "Number of Passed Student";
$eReportCard['FailingRate'] = "Failed Rate";
$eReportCard['FailingNumber'] = "Number of Failed Student";
$eReportCard['SubjectWeight'] = "Subject Weight";
$eReportCard['GrandAvgPassNumber'] = "Number of Grand Average Pass";
$eReportCard['GrandAvgFailNumber'] = "Number of Grand Average Fail";
$eReportCard['ClassAndClassNo'] = "Class & Class No.";
$eReportCard['LowerQuartile'] = "Lower Quartile (Q1)";
$eReportCard['Median'] = "Median (Q2)";
$eReportCard['UpperQuartile'] = "Upper Quartile (Q3)";

$eReportCard['MasterReport']['ReportOption'] = "Report Options";
$eReportCard['MasterReport']['StudentDisplayOption'] = "Student Display Options";
$eReportCard['MasterReport']['SubjectDisplayOption'] = "Subject Display Options";
$eReportCard['MasterReport']['GrandMarkDisplayOption'] = "Grand Mark Display Options";
$eReportCard['MasterReport']['StatisticDisplayOption'] = "Statistic Display Options";
$eReportCard['MasterReport']['SortingOption'] = "Sorting Options";
$eReportCard['MasterReport']['SortBy'] = "Sort by";
$eReportCard['MasterReport']['OtherInfoDisplayOption'] = "Other Information Display Options";
$eReportCard['MasterReport']['ShowOption'] = "Show Options";
$eReportCard['MasterReport']['HideOption'] = "Hide Options";
$eReportCard['MasterReport']['DisplayFormat'] = "Display Format";
$eReportCard['MasterReport']['ClassDisplayArr']['ShowFormSummary'] = "Show all classes of same form in one table";
$eReportCard['MasterReport']['ClassDisplayArr']['ShowClassSummary'] = "Show classes in separate tables";
$eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowFormSummary'] = "Show all subject groups of same form in one table";
$eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowClassSummary'] = "Show subject groups in separate tables";
$eReportCard['MasterReport']['SubjectGroupDisplayArr']['ShowSubjectGroupSubjectOnly'] = "Show subject groups with their corresponding subjects in separate tables";
$eReportCard['MasterReport']['ShowSubjectGroupSubjectOnly'] = "Show corresponding subjects of subject groups";
$eReportCard['MasterReport']['DataDisplay'] = "Data Display";
$eReportCard['MasterReport']['ColumnDataDisplay'] = "Column Data Display";
$eReportCard['MasterReport']['SubjectDataDisplay'] = "Subject Data Display";
$eReportCard['MasterReport']['DisplayNameInOneLine'] = "Display Name in one line";
$eReportCard['MasterReport']['DisplayStyle'] = "Display Style";
$eReportCard['MasterReport']['Border'] = "Border";
$eReportCard['MasterReport']['Space'] = "Space";
$eReportCard['MasterReport']['SubjectComponentSeparator'] = "Subject Component Separator";
$eReportCard['MasterReport']['SubjectSeparator'] = "Subject Separator";
$eReportCard['MasterReport']['BorderBetweenSubjectData'] = "Border Between Subject Data";
$eReportCard['MasterReport']['FontSize'] = "Font Size";
$eReportCard['MasterReport']['StudentBottomLine'] = "Horizontal Separator";
$eReportCard['MasterReport']['Every'] = "Every";
$eReportCard['MasterReport']['Row'] = "row";
$eReportCard['MasterReport']['SubjectComponentFontSize'] = "Subject Component Font Size";
$eReportCard['MasterReport']['ReportStyle1']= "Style 1";
$eReportCard['MasterReport']['ReportStyle2']= "Style 2";
$eReportCard['MasterReport']['ReportStyle3']= "Style 3";
$eReportCard['MasterReport']['ReportStyle4']= "Style 4";
$eReportCard['MasterReport']['DisplayColumnLabel']= "Display Report Column Label";
$eReportCard['MasterReport']['DisplaySubjectDataLabel']= "Display Subject Marks Label";
$eReportCard['MasterReport']['ColumnDisplayOrder']= "Column Display Order";
$eReportCard['MasterReport']['ReportPreset']= "Report Preset";
$eReportCard['MasterReport']['Preset']= "Preset";
$eReportCard['MasterReport']['DefaultPresetName'] = "new preset";
$eReportCard['MasterReport']['DisplaySubjectWithAllNA'] = 'Display Non-taken Elective Subject(s) in Class';
$eReportCard['MasterReport']['EmptySymbol'] = "Empty/NA Data Display";
$eReportCard['MasterReport']['EmptySubjectDataForNA'] = "Empty Subject Data For Subject With N.A.";
$eReportCard['MasterReport']['FormatWithStyle'] = "Format with style";
$eReportCard['MasterReport']['ShowClassTeacher'] = "Show Class Teachers";
$eReportCard['MasterReport']['RegistrationNo'] = "Registration No.";
$eReportCard['MasterReport']['ApplyFailStyleToScore']= "Apply fail style to score";
$eReportCard['MasterReport']['ApplyFailStyleToScoreRemarks']= "If this option is checked, \"Format with style\" options will be overrided";

$eReportCard['MasterReport']['jsWarningArr']['SelectStudentdisplayInfo'] = "Please select at least one Student Data Display.";
$eReportCard['MasterReport']['jsWarningArr']['SelectReportColumn'] = "Please select at least one Report Column.";
$eReportCard['MasterReport']['jsWarningArr']['SelectSubjectData'] = "Please select at least one Subject Data Display.";
$eReportCard['MasterReport']['jsConfirmArr']['ModifyPreset'] = "The preset value will be modified, proceed? ";
$eReportCard['MasterReport']['jsConfirmArr']['DeletePreset'] = "The preset will be deleted, proceed? ";
$eReportCard['MasterReport']['jsConfirmArr']['UseDefaultPresetName'] = "Use ".$eReportCard['MasterReport']['DefaultPresetName']." as preset name?";

$eReportCard['MasterReport']['LastestTerm'] = "Lastest Term";
$eReportCard['MasterReport']['WholeYear'] = "Whole Year";
$eReportCard['MasterReport']['ReportTitle'] = "Report Title";

// Student Info
$eReportCard['MasterReport']['DataShortName']['UserLogin'] = "UserLogin";
$eReportCard['MasterReport']['DataShortName']['ClassName'] = "Class";
$eReportCard['MasterReport']['DataShortName']['ClassNumber'] = "No.";
$eReportCard['MasterReport']['DataShortName']['ChineseName'] = "Chinese Name";
$eReportCard['MasterReport']['DataShortName']['EnglishName'] = "English Name";
$eReportCard['MasterReport']['DataShortName']['Gender'] = "Gender";
$eReportCard['MasterReport']['DataShortName']['RegNo'] = "RegNo";

// Subject Marks
$eReportCard['MasterReport']['DataShortName']['Mark'] = "M";
$eReportCard['MasterReport']['DataShortName']['RawMark'] = "RM";
$eReportCard['MasterReport']['DataShortName']['MSScore'] = "MSS";
$eReportCard['MasterReport']['DataShortName']['SDScore'] = "SD";
$eReportCard['MasterReport']['DataShortName']['RawSDScore'] = "RSD";
$eReportCard['MasterReport']['DataShortName']['Grade'] = "G";
$eReportCard['MasterReport']['DataShortName']['OrderMeritForm'] = "OMF";
$eReportCard['MasterReport']['DataShortName']['OrderMeritClass'] = "OMC";
$eReportCard['MasterReport']['DataShortName']['OrderMeritSubjectGroup'] = "OMSG";
$eReportCard['MasterReport']['DataShortName']['OrderMeritStream'] = "OMST";
$eReportCard['MasterReport']['DataShortName']['SubjectTeacherComment'] = "Subject Teacher's Comment";

// Grand Marks
$eReportCard['MasterReport']['DataShortName']['GrandTotal'] = "GT";
$eReportCard['MasterReport']['DataShortName']['GrandGrade'] = "GG";
$eReportCard['MasterReport']['DataShortName']['GrandAverage'] = "GA";
$eReportCard['MasterReport']['DataShortName']['ActualAverage'] = "AA";
$eReportCard['MasterReport']['DataShortName']['GrandSDScore'] = "GSD";
$eReportCard['MasterReport']['DataShortName']['GPA'] = "GPA";
$eReportCard['MasterReport']['DataShortName']['OrderMeritForm'] = "OMF";
$eReportCard['MasterReport']['DataShortName']['OrderMeritClass'] = "OMC";
$eReportCard['MasterReport']['DataShortName']['NoOfPassSubject'] = "Pass";
$eReportCard['MasterReport']['DataShortName']['NoOfFailSubject'] = "Fail";
$eReportCard['MasterReport']['DataShortName']['Promotion'] = "Prom't";
$eReportCard['MasterReport']['DataShortName']['ClassTeacherComment'] = "Class Teacher's Comment";

// Statistic
$eReportCard['MasterReport']['DataShortName']['Mean'] = "Mean";
$eReportCard['MasterReport']['DataShortName']['SD'] = "Standard Deviation";
$eReportCard['MasterReport']['DataShortName']['HighestMark'] = "Highest Mark";
$eReportCard['MasterReport']['DataShortName']['LowestMark'] = "Lowest Mark";
$eReportCard['MasterReport']['DataShortName']['PassingRate'] = "Passing Rate";
$eReportCard['MasterReport']['DataShortName']['PassingNumber'] = "Passing Number";
$eReportCard['MasterReport']['DataShortName']['FailingRate'] = "Failed Rate";
$eReportCard['MasterReport']['DataShortName']['FailingNumber'] = "Failed Number";
$eReportCard['MasterReport']['DataShortName']['SubjectWeight'] = "Subject Weight";
$eReportCard['MasterReport']['DataShortName']['NumOfStudent'] = "No. of Students";
$eReportCard['MasterReport']['DataShortName']['NumOfStudentStudyingInClass'] = "No. of Students Studying In Class";
$eReportCard['MasterReport']['DataShortName']['NumOfStudentStudyingInForm'] = "No. of Students Studying In Form";
$eReportCard['MasterReport']['DataShortName']['LowerQuartile'] = "Lower Quartile (Q1)";
$eReportCard['MasterReport']['DataShortName']['Median'] = "Median (Q2)";
$eReportCard['MasterReport']['DataShortName']['UpperQuartile'] = "Upper Quartile (Q3)";

// Other Info
$eReportCard['MasterReport']['OtherInfo']['Remark'] = "REMARK";
$eReportCard['MasterReport']['OtherInfo']['Days Absent'] = "DABS";
$eReportCard['MasterReport']['OtherInfo']['Time Late'] = "LATE";
$eReportCard['MasterReport']['OtherInfo']['Conduct'] = "COND";
$eReportCard['MasterReport']['OtherInfo']['Minor Demerit'] = "Minor Demerit";
$eReportCard['MasterReport']['OtherInfo']['Major Demerit'] = "Major Demerit";
$eReportCard['MasterReport']['OtherInfo']['ECA'] = "ECA";
$eReportCard['MasterReport']['OtherInfo']['Minor Credit'] = "Minor Credit";
$eReportCard['MasterReport']['OtherInfo']['Major Credit'] = "Major Credit";
$eReportCard['MasterReport']['OtherInfo']['Minor Fault'] = "Minor Fault";
$eReportCard['MasterReport']['OtherInfo']['Major Fault'] = "Major Fault";
$eReportCard['MasterReport']['OtherInfo']['Merits'] = "Merits";
$eReportCard['MasterReport']['OtherInfo']['Demerits'] = "Demerits";

$eReportCard['MasterReport']['PromotionArr'][1] = "";
$eReportCard['MasterReport']['PromotionArr'][0] = "R";
$eReportCard['MasterReport']['PromotionArr'][2] = "C";

$eReportCard['SelectFrom'] = "Select From";

// Preset Report Name - BIBA Cust
$eReportCard['Template']['ReportName']['ESProgressReport'] = "Progress Report (Comment Only)";
$eReportCard['Template']['ReportName']['MSProgressReport'] = "Progress Report";
$eReportCard['Template']['ReportName']['SemesterReport'] = "Semester Report";
$eReportCard['Template']['ReportName']['FinalReport'] = "Semester Report (Final)";

// Master Report Uccke Cust
$eReportCard['MasterReport']['Marks'] = "Mark";
$eReportCard['MasterReport']['WithPassingMarkEqualsTo'] = "with passing mark equals to ";
$eReportCard['MasterReport']['PercentageOfCh'] = "";
$eReportCard['MasterReport']['PercentageOfEn'] = " of full mark";

$eReportCard['MasterReport']['NoWrap'] = "Avoid content wrapped to the next row";
$eReportCard['MasterReport']['ExcludeComponentSubject'] = "Exclude Component Subject(s)";
// Master Report Sao Jose (Macau) Cust
$eReportCard['MasterReport']['SubjectUnit'] = "Unit";
$eReportCard['MasterReport']['LeftStudent'] = "Left";
$eReportCard['MasterReport']['PrintDate'] = "Print Date";
$eReportCard['MasterReport']['Signature'] = "Signature";
$eReportCard['MasterReport']['SignatureDate'] = "Date";
$eReportCard['MasterReport']['DataShortName']['FailSubjectUnit'] = "Unit";

// Other Info Import
$eReportCard['ImportOtherInfoSuccess'] = "Other info imported successfully.";
$eReportCard['ImportOtherInfoFail'] = "Failed to import other info.";
$eReportCard['ImportWarningArr']['WrongWebSamsRegNo'] = "No such user.";
$eReportCard['ImportWarningArr']['WrongUserLogin'] = "No such user.";
$eReportCard['ImportWarningArr']['WrongClassNameNumber'] = "No such user.";
$eReportCard['ImportWarningArr']['EmptyClassNumber'] = "Empty class number.";
$eReportCard['ImportWarningArr']['EmptyClassName'] = "Empty class name.";
$eReportCard['ImportWarningArr']['EmptyStudentData'] = "Empty student data.";
$eReportCard['ImportWarningArr']['WrongDataFormat'] = "Wrong Format";
$eReportCard['ImportWarningArr']['InvilidMark'] = "Invalid Mark";
$eReportCard['ImportWarningArr']['MarkGreaterThanFullMark'] = "Mark is greater than the Full Mark";
$eReportCard['ImportWarningArr']['EmptyMark'] = "Empty Mark";
$eReportCard['ImportWarningArr']['InputGrade'] = "Input Grade";
$eReportCard['ImportWarningArr']['InputNumber'] = "Input Number";



// Subject Prize Report
$eReportCard['ReportArr']['SubjectPrize']['jsWarningArr']['SubjectGroupRankingMustSelectATerm'] = "A Term must be selected for Subject Group Ranking.";
$eReportCard['ReportArr']['SubjectPrize']['NumOfPrize'] = "Number of Prize";

############################################################

##### For Re-exam module #####
$eReportCard['ReExam'] = "Re-exam";
$eReportCard['ReExamStudentList'] = "Re-exam Student List";
$eReportCard['ReExamScore'] = "Re-exam Score";
$eReportCard['UploadReExamResult'] = "Upload Re-exam Score";
$eReportCard['LastUploadedDate'] = "Last Uploaded Date";
$eReportCard['LastUploadedBy'] = "Last Uploaded By";

$eReportCard['Download'] = "Download";

$eReportCard['Template']['RemarkGraduationExam'] = array();
$eReportCard['Template']['RemarkGraduationExam'][0] = "全科合格，准予參加畢業試。";
$eReportCard['Template']['RemarkGraduationExam'][1] = "符合學校規定，准予參加畢業試。";
$eReportCard['Template']['RemarkGraduationExam'][2] = "成績不合格，應予留級。";

$eReportCard['Template']['RemarkGraduation'] = array();
$eReportCard['Template']['RemarkGraduation'][0] = "全科合格，准予畢業。";
$eReportCard['Template']['RemarkGraduation'][1] = "成績不合格，不得畢業。";
$eReportCard['Template']['RemarkGraduation'][2] = "<!--PassSubjects-->補考合格，准予畢業。";
$eReportCard['Template']['RemarkGraduation'][3][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['RemarkGraduation'][3][1] = "<!--FailedSubjects-->補考不合格，不得畢業。";


$eReportCard['Template']['RemarkPromotion'] = array();
$eReportCard['Template']['RemarkPromotion'][0] = "全科合格，准予升級。";
$eReportCard['Template']['RemarkPromotion'][1] = "成績不合格，應予留級。";
$eReportCard['Template']['RemarkPromotion'][2] = "<!--PassSubjects-->補考合格，准予升級。";
$eReportCard['Template']['RemarkPromotion'][3][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['RemarkPromotion'][3][1] = "<!--FailedSubjects-->補考不合格，應予留級。";
$eReportCard['Template']['RemarkPromotion'][4][0] = "<!--PassSubjects-->補考合格，";
$eReportCard['Template']['RemarkPromotion'][4][1] = "<!--FailedSubjects-->補考不合格，准予帶科升級。";

$eReportCard['Template']['SubjectSeparator'] = "、";

$eReportCard['Template']['NoReExamRequired'] = "There are no report templates which require re-examination.";
##### End of Re-exam module #####

$eReportCard['jsConfirmArr']['GradingSchemeWillBeCovered'] = "The grading scheme will be applied to all reports of the target form, the original grading scheme of these reports will be replaced, proceed?"; 
$eReportCard['jsConfirmArr']['ReportGradingSchemeWillBeCovered'] = "The grading scheme will be applied to the target report, the original grading scheme of the report will be replaced, proceed?";
$eReportCard['jsConfirmArr']['FormGradingSchemeWillBeCovered'] = "The grading scheme will be applied to all reports of the this form, the original grading scheme of the report will be replaced, proceed?";
$eReportCard['GradingSchemeNotSet'] = "Grading scheme was not set yet";

$eReportCard['AllSubjectGroup'] = "All Subject Groups";

$eReportCard['ImportHeader']['ClassName']['En'] = "Class Name";
$eReportCard['ImportHeader']['ClassNumber']['En'] = "Class Number";
$eReportCard['ImportHeader']['UserLogin']['En'] = "User Login";
$eReportCard['ImportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$eReportCard['ImportHeader']['StudentName']['En'] = "Student Name".$Lang['General']['ImportArr']['Reference']['En'];
$eReportCard['ImportHeader']['Subject']['En'] = "Subject";
$eReportCard['ImportHeader']['SubjectComponentCode']['En'] = "Subject Component Code";
$eReportCard['ImportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ImportHeader']['SubjectGroupCode']['En'] = "Subject Group Code";
$eReportCard['ImportHeader']['Feedback']['En'] = "Feedback";
$eReportCard['ImportHeader']['LastModified']['En'] = "Last Modified";
$eReportCard['ImportHeader']['Status']['En'] = "Status";
$eReportCard['ImportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ImportHeader']['ClassName']['Ch'] = "(班別)";
$eReportCard['ImportHeader']['ClassNumber']['Ch'] = "(班號)";
$eReportCard['ImportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$eReportCard['ImportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$eReportCard['ImportHeader']['StudentName']['Ch'] = "(學生姓名)".$Lang['General']['ImportArr']['Reference']['Ch'];
$eReportCard['ImportHeader']['Subject']['Ch'] = "(科目)";
$eReportCard['ImportHeader']['SubjectGroup']['Ch'] = "(科組)";
$eReportCard['ImportHeader']['Feedback']['Ch'] = "(回應)";
$eReportCard['ImportHeader']['LastModified']['Ch'] = "(最後修改日期)";
$eReportCard['ImportHeader']['Status']['Ch'] = "(狀況)";

$eReportCard['ExportHeader']['ClassName']['En'] = "Class Name";
$eReportCard['ExportHeader']['ClassNumber']['En'] = "Class Number";
$eReportCard['ExportHeader']['UserLogin']['En'] = "User Login";
$eReportCard['ExportHeader']['WebSamsRegNo']['En'] = "WebSams Reg. No.";
$eReportCard['ExportHeader']['StudentName']['En'] = "Student Name";
$eReportCard['ExportHeader']['Subject']['En'] = "Subject";
$eReportCard['ExportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ExportHeader']['Feedback']['En'] = "Feedback";
$eReportCard['ExportHeader']['LastModified']['En'] = "Last Modified";
$eReportCard['ExportHeader']['Status']['En'] = "Status";
$eReportCard['ExportHeader']['SubjectGroup']['En'] = "Subject Group";
$eReportCard['ExportHeader']['TeacherComment']['En']="Teacher Comment";
$eReportCard['ExportHeader']['ClassName']['Ch'] = "(班別)";
$eReportCard['ExportHeader']['ClassNumber']['Ch'] = "(班號)";
$eReportCard['ExportHeader']['UserLogin']['Ch'] = "(內聯網帳號)";
$eReportCard['ExportHeader']['WebSamsRegNo']['Ch'] = "(WebSAMS 註冊號碼)";
$eReportCard['ExportHeader']['StudentName']['Ch'] = "(學生姓名)";
$eReportCard['ExportHeader']['Subject']['Ch'] = "(科目)";
$eReportCard['ExportHeader']['SubjectGroup']['Ch'] = "(科組)";
$eReportCard['ExportHeader']['Feedback']['Ch'] = "(回應)";
$eReportCard['ExportHeader']['LastModified']['Ch'] = "(最後修改日期)";
$eReportCard['ExportHeader']['Status']['Ch'] = "(狀況)";
$eReportCard['ExportHeader']['TeacherComment']['Ch']="(老師評語)";


$eReportCard['FailToImportMarksheetScore'] = "Failed to import marksheet score.";

$eReportCard['ClearAllNA'] = "Clear all N.A.";

$eReportCard['Simple'] = "Simple";
$eReportCard['Normal'] = "Normal";
$eReportCard['ReportView'] = "Report View";

$eReportCard['ForeignStd'] = "Foreign Student";
$eReportCard['NormalStd'] = "Normal Student";
$eReportCard['AcademicReport'] = "Academic Report";

$eReportCard['ExtraInfoLabel'] = "Effort";
$eReportCard['Template']['SelectEffort'] = "Select Effort";
$eReportCard['Template']['WrongEffortValue'] = "Wrong Effort Value in the csv";

$eReportCard['DisplayColumnNum'] = "Display Column Number";

# Marksheet Feedback
$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['FeedbackInProgress'] = "Marksheet feedback is in progress";
$eReportCard['ManagementArr']['MarksheetRevisionArr']['MarksheetFeedbackArr']['ExportFeedback'] = "Export feedback";

$eReportCard['ManagementArr']['SchdeuleArr']['ApplySpecificSubmissionPeriod'] = "Apply Specific Submission Period";
$eReportCard['ManagementArr']['SchdeuleArr']['SpecificSubmissionPeriod'] = "Specific Submission Period";
$eReportCard['ManagementArr']['SchdeuleArr']['AvailableTeacher'] = "Available Teacher";
$eReportCard['ManagementArr']['SchdeuleArr']['SelectedTeacher'] = "Selected Teacher";
$eReportCard['ManagementArr']['SchdeuleArr']['WarningArr']['PleaseSelectTeacher'] = "Please select a teacher";
$eReportCard['ManagementArr']['SchdeuleArr']['WarningArr']['PleaseInputDate'] = "Please input a date";

### Award Generation
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardNotYetGeneratedAfterReportGeneration'] = "Award has not been generated yet after report generation.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardGeneration'] = "Award Generation";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GenerateAward'] = "Generate Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratingAward'] = "Generating Award(s)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardSettings'] = "Award Settings";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCriteria'] = "Award Criteria";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardName'] = "Award Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardCode'] = "Award Code";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardForm'] = "Award related Form";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Award'] = "Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Student'] = "Student";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoAwardSettingsForThisReportAndForm'] = "There is no Award Settings for this Report and Form.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['SubjectPrizeSuffix'] = "(Subject)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankRangeDisplay'] = "Ranking from <!--FromRankSelection--> to <!--ToRankSelection--> in <!--RankFieldSelection-->";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PersonalCharDisplay'] = "At least having <!--PersonalCharCodeSelection--> in each Personal Qualities";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['LinkedAwardSettingsDisplay'] = "Ranking from <!--FromRankSelection--> to <!--ToRankSelection--> in <!--AwardSelection-->";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ExcludeStudentFromOriginalAward'] = "Exclude the Student from the original Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DetermineBy'] = "Determine by";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['MinimumImprovement'] = "Minimum Improvement";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Rank(s)'] = "Rank(s)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Mark(s)'] = "Mark(s)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['Quota'] = "Quota";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['FromReport'] = "From Report";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ToReport'] = "To Report";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['QuotaRemarks'] = "Choose \"0\" if there is no limitation.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['MustBePass'] = "Must be Pass";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DoNotIncludeAdjustedMarkRemarks'] = "The manual adjusted mark will <b>NOT</b> be considered in the award generation process.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['BackToReportGeneration'] = "Back to Report Generation";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['StudentBasedList'] = "Student-based List (Includes Adjusted Award Data)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardBasedList'] = "Award-based List (Includes Generated Award Data only)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ViewAward'] = "View Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['StudentName'] = "Student Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['SavingAwardsData'] = "Saving Awards Data";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ClassView'] = "Class View";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardView'] = "Award View";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['NoOfAwardedStudent'] = "No. of Awarded Students";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportStudentAwardSuccess'] = "Import Student Award(s) Success.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportStudentAwardFailed'] = "Import Student Award(s) Failed.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImportOtherAwards'] = "Import Other Award(s)";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GoBackToViewAwards'] = "Go Back to Student Awards View";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['DeleteAward'] = "Delete Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddAward'] = "Add Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AddStudent'] = "Add Student";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['AwardRanking'] = "Award Ranking";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['GeneratedFromReport'] = "Generate from \"<!--reportName-->\"";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ManualInput'] = "Manual Input";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PreviousCriteriaRelationArr']['AND'] = "And";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['PreviousCriteriaRelationArr']['OR'] = "Or";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['Form'] = "Form";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['Class'] = "Class";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['RankTypeArr']['SubjectGroup'] = "Subject Group";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandAverage'] = "Grand Average";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandTotal'] = "Grand Total";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandGPA'] = "Grand GPA";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['GrandSDScore'] = "Grand SD Score";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['Mark'] = "Mark";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['SDScore'] = "SD Score";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritClass'] = "Class Ranking";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritForm'] = "Form Ranking";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ImprovementDeterminationFieldArr']['OrderMeritSubjectGroup'] = "Subject Group Ranking";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['FromRankCannotLargerThenToRank'] = "The value of \"From Rank\" cannot be greater than that of the \"To Rank\"";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['MinImprovement_Mark'] = "\"Minimum Improvement Mark\" must be a positive numeric value";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['MinImprovement_Rank'] = "\"Minimum Improvement Rank\" must be a positive integer";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['GenerateAward'] = "The generated awards will be overwritten if you generate the awards now. Are you sure you want to generate the awards now?";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoClassSettings'] = "There is no Class Settings";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoStudentInClass'] = "There is no Student in this Class";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardSettings'] = "There is no Award Settings";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['NoAwardedStudentSettings'] = "There is no Student getting this Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['WarningMsgArr']['DeleteAward'] = "Are you sure you want to delete the award of this student?";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardGenerateSuccess'] = "1|=|Award Generated Successfully.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardGenerateFailed'] = "0|=|Award Generation Failed.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardSaveSuccess'] = "1|=|Award Saved Successfully.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardSaveFailed'] = "0|=|Award Saving Failed.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardDeleteSuccess'] = "1|=|Award Deleted Successfully.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardDeleteFailed'] = "0|=|Award Deletion Failed.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddSuccess'] = "1|=|Award Added Successfully.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardAddFailed'] = "0|=|Award Addition Failed.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderSuccess'] = "1|=|Award Re-ordered Successfully.";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['ReturnMsgArr']['AwardReorderFailed'] = "0|=|Award Re-order Failed.";

$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['ClassName'] = "Class Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['ClassName'] = "班別";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['ClassNumber'] = "Class Number";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['ClassNumber'] = "班號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['UserLogin'] = "User Login";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['UserLogin'] = "內聯網帳號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['WebSAMSRegNo'] = "WebSAMSRegNo";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['WebSAMSRegNo'] = "WebSAMS 註冊號碼";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['StudentName'] = "Student Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['StudentName'] = "學生姓名";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['Award'] = "Award";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['Award'] = "獎項";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardCode'] = "Award Code";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['AwardCode'] = "獎項代號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['Subject'] = "Subject";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['Subject'] = "學科";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['SubjectWebSAMSCode'] = "Subject WebSAMS Code";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['Ch']['SubjectWebSAMSCode'] = "學科 WebSAMS 代號";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardName'] = "Award Name";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardCode'] = "Award Code";
$Lang['eReportCard']['ReportArr']['ReportGenerationArr']['CsvHeaderArr']['En']['AwardForm'] = "Award related Form";

$Lang['eReportCard']['OverallResult'] = "Overall Result";

$Lang['eReportCard']['WarningArr']['Select']['Class'] = "Please select a class.";
$Lang['eReportCard']['WarningArr']['Select']['SubjectGroup'] = "Please select a subject group.";
$Lang['eReportCard']['WarningArr']['Select']['Subject'] = "Please select a subject.";
$Lang['eReportCard']['WarningArr']['Select']['Award'] = "Please select an award.";
$Lang['eReportCard']['WarningArr']['Select']['AtLeastOneStudent'] = "Please select at least one student.";
$Lang['eReportCard']['WarningArr']['ImageFileExtension'] = "File must be an image (.gif, .jpg, .jpeg, .png, .bmp)";
$Lang['eReportCard']['WarningArr']['Select']['Form'] = "Please select a form.";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['FailSubject'] = "Failed one or more Subjects";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline1'] = "Received Demerit	or Misdemeanor";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline2'] = "Received Minor Misdeed";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['PoorDiscipline3'] = "Received Major Misdeed";
$Lang['eReportCard']['WarningArr']['ConductDisplay']['RejectAwardWhenFailConduct'] = "Cannot add awards for this student as conduct grade received is lower than B";

$Lang['eReportCard']['ImportWarningArr']['empty_row'] = "No data entry.";
$Lang['eReportCard']['ImportWarningArr']['student_not_found'] = "Student not found.";
$Lang['eReportCard']['ImportWarningArr']['option_no_right'] = "No rights to import option(s).";
$Lang['eReportCard']['ImportWarningArr']['code_not_found'] = "Code not found";
$Lang['eReportCard']['ImportWarningArr']['comment_too_long'] = "Comment exceeds words limit.";
$Lang['eReportCard']['ImportWarningArr']['award_not_found'] = "Award not found";
$Lang['eReportCard']['ImportWarningArr']['subject_not_found'] = "Subject not found";
$Lang['eReportCard']['ImportWarningArr']['form_subject_not_found'] = "Subject is not applicable for this Form";
$Lang['eReportCard']['ImportWarningArr']['student_has_award_already'] = "Student has this Award already";
$Lang['eReportCard']['ImportWarningArr']['empty_sg_code'] = "Empty Subject Group Code";
$Lang['eReportCard']['ImportWarningArr']['sg_code_not_found'] = "Subject Group not found";
$Lang['eReportCard']['ImportWarningArr']['comp_code_not_found'] = "Component Subject not found";
$Lang['eReportCard']['ImportWarningArr']['student_not_in_sg'] = "Student is not in this subject group";
$Lang['eReportCard']['ImportWarningArr']['not_teaching_student'] = "You are not teaching this student";
$Lang['eReportCard']['ImportWarningArr']['empty_award_name'] = "Empty Award Name";
$Lang['eReportCard']['ImportWarningArr']['duplicate_award_name'] = "Award Name already in use";
$Lang['eReportCard']['ImportWarningArr']['award_form_not_found'] = "Award related Form not found";

$eReportCard['HKUGAGradeList']['Student'] = "Student";
$eReportCard['HKUGAGradeList']['Achievement'] = "Achievement";
$eReportCard['HKUGAGradeList']['Attitude'] = "Attitude";
$eReportCard['HKUGAGradeList']['EG'] = "EG";
$eReportCard['HKUGAGradeList']['CG'] = "CG";
$eReportCard['HKUGAGradeList']['YG'] = "YG";
$eReportCard['HKUGAGradeList']['Term'] = "Term";
$eReportCard['HKUGAGradeList']['Subject'] = "Subject";
$eReportCard['HKUGAGradeList']['Comments'] = "Comments";
$eReportCard['HKUGAGradeList']['SubjectTeacher'] = "Subject Teacher";
$eReportCard['HKUGAGradeList']['HeadOfDepartment'] = "Head of Dept";
$eReportCard['HKUGAGradeList']['Date'] = "Date";

$eReportCard['HKUGAGradeList']['AchievementArr'][] = "Responses to Stimuli and Coordination In Humans (I)";
$eReportCard['HKUGAGradeList']['AchievementArr'][] = "Responses to Stimuli and Coordination In Humans (II)";
$eReportCard['HKUGAGradeList']['AchievementArr'][] = "Cellular Energetics (II): Photosynthesis & Respiration";

$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Preparation for lessons";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Attitude to learning";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Quality of assignments / presentations";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Presentation of work";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Independent learning";
$eReportCard['HKUGAGradeList']['AttitudeArr'][] = "Participation in class";

$eReportCard['HKUGAGradeList']['GradeArr']['EG'] = "Examination";
$eReportCard['HKUGAGradeList']['GradeArr']['YG'] = "Year Grade";
$eReportCard['HKUGAGradeList']['GradeArr']['CG'] = "Continuous Assessment";

$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['Title'] = "Type of Analysis";
$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['Term'] = "Term";
$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['Assessment'] = "Assessment";
$eReportCard['FormSubjectReportAnalysis']['DataTypeArr']['CA'] = "Assessment (Continuous Assessment)";

### Marksheet Revision
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PreviewReport'] = "Preview Report";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['NoOfEstimatedScore'] = "No. of Estimated Score";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrol'] = "Synchronize from eEnrolment";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SynchronizeFromEnrolWarning'] = "The existing Term Subject Mark will be overwritten if you synchronize the marks from eEnrolment now. Are you sure you want to synchronize the data from eEnrolment?";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['SaveBeforeLeaveWarning'] = "The marksheet has been modified alreay. Do you want to save the updated marksheet before you leave this page?";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['ChangeLostInputWarning'] = "Changing <!--inputfield--> will lost all modified marksheet input. Are you sure you want to continue?";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['UpdateAfterChangeStudent'] = "All modified marksheet input will be UPDATED. Are you sure you want to continue?";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['NoExistingSubjectTopics'] = "No subject topics for this subject.";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['GoToMarksheet'] = "Go to marksheet";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['PrintTime'] = "Print time";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['Signature'] = "Signature";
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['ExportDetails'] = "Export Input Details";

### Schedule
$Lang['eReportCard']['ManagementArr']['MarksheetRevisionArr']['AddTeacher'] = "Add Teacher";

### General
$Lang['eReportCard']['GeneralArr']['ImportStudentRemarks'] = "You may use \"Class Name and Class Number\" <b>or</b> \"User Login\" <b>or</b> \"WebSAMSRegNo\" to do the student mapping.";
$Lang['eReportCard']['GeneralArr']['WarningArr']['HaveScaleDefaultAlready'] = "There is a default Scale already";

## Personal Characteristic Schedule
$eReportCard['PCSubmissionPeriod'] = "Personal Characteristics Submission Period";

$eReportCard['CommentTooLong'] = "The comment(s) on row %s are too long and have been truncated.";
$eReportCard['CommentTooLongInFile'] = "The comment(s) on row %s in the csv file are too long and have been truncated.";
$eReportCard['ConductNotFound'] = "The conduct(s) on row %s cannot be found in the conduct bank.";
$eReportCard['CourseDescription']['Overall'] = "Overall";
$eReportCard['GradingSchemeArr']['Description'] = "Description";
$eReportCard['Comment'] = "Comment";
$eReportCard['AddComment'] = "Add Comment";
$eReportCard['CommentRelatedGrade'] = "Related grade";
$eReportCard['CommentViewPreviousTerm'] = "View comments of previous terms";

# Trial Promotion
$eReportCard['TrialPromotion']['ReportTitle'] = "Trial Promotion List";
$eReportCard['TrialPromotion']['Report'] = 'Report';
$eReportCard['TrialPromotion']['Conduct']='Conduct';
$eReportCard['TrialPromotion']['BelowOneSubject'] = "Below '1' Subject";
$eReportCard['TrialPromotion']['LateAbsTruantTotal'] = "Total number of being late, absent and truant";
$eReportCard['TrialPromotion']['Truant'] = "Truant";
$eReportCard['TrialPromotion']['DisapprovedLeave'] = "Disapproved Leave";
$eReportCard['TrialPromotion']['EarlyLeave'] = "Early Leave";
$eReportCard['TrialPromotion']['Late'] = "Late";
$eReportCard['TrialPromotion']['SickLeave'] = "Sick Leave";
$eReportCard['TrialPromotion']['LeaveWithReason'] = "Leave with Reason";


$eReportCard['TrialPromotion']['trialCSVheader']['En']['Class'] = "Class";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['ClassNo'] = "ClassNo";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Student'] = "Student";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['BelowOneSubject'] = "Below '1' Subject";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Conduct'] = "Conduct";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['LateAbsTruantTotal'] = "Total number of being late, absent and truant";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Truant'] = "Truant";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['DisapprovedLeave'] = "Disapproved Leave";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['EarlyLeave'] = "Early Leave";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['Late'] = "Late";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['SickLeave'] = "Sick Leave";
$eReportCard['TrialPromotion']['trialCSVheader']['En']['LeaveWithReason'] = "Leave with Reason";


$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Class'] = "班別";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['ClassNo'] = "班號";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Student'] = "學生";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['BelowOneSubject'] = "低於'1'的科目";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Conduct'] = "操行";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['LateAbsTruantTotal'] = "缺席, 遲到, 曠課次數";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Truant'] = "曠課";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['DisapprovedLeave'] = "不獲批核缺課";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['EarlyLeave'] = "早退";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['Late'] = "遲到";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['SickLeave'] = "病假";
$eReportCard['TrialPromotion']['trialCSVheader']['Ch']['LeaveWithReason'] = "事假";


### Subject Academic Result
$eReportCard['SubjectAcademicResult']['ReportOption'] = "Report Options";
$eReportCard['SubjectAcademicResult']['StudentDisplayOption'] = "Student Display Options";
$eReportCard['SubjectAcademicResult']['SubjectDisplayOption'] = "Subject Display Options";
$eReportCard['SubjectAcademicResult']['ShowOption'] = "Show Options";
$eReportCard['SubjectAcademicResult']['HideOption'] = "Hide Options";
$eReportCard['SubjectAcademicResult']['StudentReport'] = "Student Report";
$eReportCard['SubjectAcademicResult']['Overall'] = "Overall";

$eReportCard['SubjectAcademicResult']['En']['SchoolName'] = "FANLING KAU YAN COLLEGE";
$eReportCard['SubjectAcademicResult']['En']['Subject'] = "Subject";
$eReportCard['SubjectAcademicResult']['En']['SimpleStat'] = "Simple Statistics";
$eReportCard['SubjectAcademicResult']['En']['ForAmendOnly'] = "For amendment only";
$eReportCard['SubjectAcademicResult']['En']['Name'] = "Name";
$eReportCard['SubjectAcademicResult']['En']['ReceivedDate'] = "Received date";
$eReportCard['SubjectAcademicResult']['En']['Signature'] = "Signature";
$eReportCard['SubjectAcademicResult']['En']['SDO'] = "SDO";
$eReportCard['SubjectAcademicResult']['En']['Office'] = "Office";
$eReportCard['SubjectAcademicResult']['En']['SubjectTeacher'] = "Subject Teacher";
$eReportCard['SubjectAcademicResult']['En']['Checker'] = "Checker";
$eReportCard['SubjectAcademicResult']['En']['SubjCoordinator'] = "Subject Co-ordinator";
$eReportCard['SubjectAcademicResult']['En']['Date'] = "Date";
$eReportCard['SubjectAcademicResult']['En']['GradeScale'] = "Grade Scale";
$eReportCard['SubjectAcademicResult']['En']['Abs'] = "Abs";

$eReportCard['SubjectAcademicResult']['En']['Tot'] = "Tot";
$eReportCard['SubjectAcademicResult']['En']['Mean'] = "Mean";
$eReportCard['SubjectAcademicResult']['En']['SD'] = "SD";
$eReportCard['SubjectAcademicResult']['En']['Max'] = "Max";
$eReportCard['SubjectAcademicResult']['En']['Min'] = "Min";

$eReportCard['SubjectAcademicResult']['En']['Frequency'] = "Frequency";

$eReportCard['SubjectAcademicResult']['En']['Entry'] = "Entry";
$eReportCard['SubjectAcademicResult']['En']['LA'] = "LA";
$eReportCard['SubjectAcademicResult']['En']['LearningAtt'] = "Learning Attitude";
$eReportCard['SubjectAcademicResult']['En']['AvgGradeArr']['Word']="Avg. Grade";
$eReportCard['SubjectAcademicResult']['En']['AvgGradeArr']['Number']="1*,1-6,6*";
$eReportCard['SubjectAcademicResult']['En']['GradePtArr']['Word']="Grade Pt";
$eReportCard['SubjectAcademicResult']['En']['GradePtArr']['Number']="3.2,3-0";
$eReportCard['SubjectAcademicResult']['En']['MarksArr']['Word']="Marks";
$eReportCard['SubjectAcademicResult']['En']['MarksArr']['Number']="100-0";
$eReportCard['SubjectAcademicResult']['En']['GradeArr']['Word']="Grade";
$eReportCard['SubjectAcademicResult']['En']['GradeArr']['Number']="A1-U";
$eReportCard['SubjectAcademicResult']['En']['ReasonArr']['Word']="Reason";
$eReportCard['SubjectAcademicResult']['En']['ReasonArr']['Number']="(For 3.2/1/0)";

### Personal Characteristics
$Lang['eReportCard']['PersonalCharArr']['NoOfManagementUser'] = "No. of Management User";
$Lang['eReportCard']['PersonalCharArr']['DefaultScaleSetAlready'] = "There is a default Scale already";
$Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][1] = "Info";
$Lang['eReportCard']['PersonalCharArr']['NewScaleStepArr'][2] = "Select Management User(s)";
$Lang['eReportCard']['PersonalCharArr']['ReturnMsgArr']['ScaleReorderSuccess'] = "1|=|Scale Re-ordered Successfully.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMsgArr']['ScaleReorderFailed'] = "0|=|Scale Re-order Failed.";
$Lang['eReportCard']['PersonalCharArr']['MemberList'] = "Management User List";
$Lang['eReportCard']['PersonalCharArr']['AddMember'] = "Add Management User";
$Lang['eReportCard']['PersonalCharArr']['ManagementUser'] = "Management User";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['SelectionContainsMember'] = "The highlighted options contain member(s) already";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['NoUserSelectionMeansAllCanEdit'][] = "All users can select this scale if no users are selected to manage this scale.";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['NoUserSelectionMeansAllCanEdit'][] = "If there are members to manage the scale, all other users (even for the eReportCard admin) cannot select this scale if the user is not a management member.";
$Lang['eReportCard']['PersonalCharArr']['WarningArr']['ScaleLinkedToDataAlready'] = "The selected Scale(s) is linked to student data already. Deleting the Scale(s) will affect the student data and this deletion cannot be recovered. Are you sure you want to delete the Scale(s)?";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleSuccess'] = "1|=|Scale Added.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleFailed'] = "0|=|Scale Addition Failed.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['EditScaleSuccess'] = "1|=|Scale Edited.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['EditScaleFailed'] = "0|=|Scale Edition Failed.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleSuccess'] = "1|=|Scale(s) Deleted.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleFailed'] = "0|=|Scale(s) Deletion Failed.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleMemberSuccess'] = "1|=|Scale Management Member Added.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['AddScaleMemberFailed'] = "0|=|Scale Management Member Addition Failed.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleMemberSuccess'] = "1|=|Scale Management Member Deleted.";
$Lang['eReportCard']['PersonalCharArr']['ReturnMessageArr']['DeleteScaleMemberFailed'] = "0|=|Scale Management Member Deletion Failed.";

$Lang['eReportCard']['PersonalCharArr']['ExportFormData'] = "Export Form Data";
$Lang['eReportCard']['PersonalCharArr']['ExportClassData'] = "Export Class Data";
$Lang['eReportCard']['PersonalCharArr']['Comment'] = "Comment";

// Marksheet Verification
$Lang['eReportCard']['AdditionalInfo']['Attendance'] = "Attendance Records";
$Lang['eReportCard']['AdditionalInfo']['Absent'] = "Absent";
$Lang['eReportCard']['AdditionalInfo']['Late'] = "Late";
$Lang['eReportCard']['AdditionalInfo']['Days'] = "Days";
$Lang['eReportCard']['AdditionalInfo']['Period'] = "Period";
$Lang['eReportCard']['AdditionalInfo']['MeritsDemerits'] = "Merits and Demerits";
$Lang['eReportCard']['AdditionalInfo']['Merits'] = "Positive Mark";
$Lang['eReportCard']['AdditionalInfo']['Demerits'] = "Negative Mark";
$Lang['eReportCard']['AdditionalInfo']['MinorMerit'] = "Merit";
$Lang['eReportCard']['AdditionalInfo']['MajorMerit'] = "Major Merit";
$Lang['eReportCard']['AdditionalInfo']['MinorDemerit'] = "Minor Demerit";
$Lang['eReportCard']['AdditionalInfo']['MajorDemerit'] = "Major Demerit";
$Lang['eReportCard']['VerificationStatus'] = "Verification Status";
$Lang['eReportCard']['AllVerificationStatus'] = "All Verification Status";
$Lang['eReportCard']['VerificationStatusComplete'] = "Completed";
$Lang['eReportCard']['VerificationStatusIncomplete'] = "Incomplete";
$Lang['eReportCard']['CompletedVerification'] = "Set Verification Status to COMPLTETE";
$Lang['eReportCard']['IncompletedVerification'] = "Set Verification Status to INCOMPLETE";

$eReportCard['PromoteRetainQuit']['PromotionAndRetention'] = "Promotion and Grade Retention";

// Print Archived Report
$eReportCard['SelectStudentMethod'] = "Select Student Method";
$eReportCard['UserLogin'] = "User Login";
$eReportCard['ClassHistory'] = "Class History";
$eReportCard['SelectStudent'] = "Select Student";
$eReportCard['SelectReport'] = "Select Report";
$eReportCard['AcademicYear'] = "Academic Year";
$eReportCard['StudentNotFound'] = "Student not found.";
$eReportCard['WarningArr']['NoArchivedReport'] = "No archived reports were found.";
$eReportCard['jsWarningArr']['PleaseInputUserLogin'] = "Please input user login.";
$eReportCard['jsWarningArr']['PleaseSelectStudent'] = "Please select a student.";
$eReportCard['jsWarningArr']['PleaseSelectReport'] = "Please select a report.";
$eReportCard['jsWarningArr']['PleaseSelectAcademicYear'] = "Please select academic year.";
$eReportCard['jsWarningArr']['PleaseSelectSemester'] = "Please select semester.";
$eReportCard['jsWarningArr']['InvalidUserLogin'] = "Invalid User Login.";

// Conduct 
$eReportCard['Conduct'] = "Conduct";
$eReportCard['jsWarningArr']['PleaseInputConduct'] = "Please input conduct.";
$eReportCard['jsWarningArr']['ConductExist'] = "Conduct already exists.";

// Foreign Student
$eReportCard['foreignStudentArr']['Instruction'] = "Instruction";
$eReportCard['foreignStudentArr']['Remark'] = "	Is Foreign Student : Y = Foreign Student, N = Local Student";

// Manual Adjustment
$eReportCard['ManualAdjustmentArr']['UpdateAssessmentPosition'] = "Adjust Assessment Position";
$eReportCard['Result'] = "Result";

// Comment Max Length Settings
$Lang['eReportCard']['CommentArr']['MaxNumberOfChar'] = "Max. number of characters";

// Final Comment Report
$Lang['eReportCard']['ReportArr']['FinalCommentArr']['FinalComment'] = "Final Comment";

// Re-exam List
$Lang['eReportCard']['ReportArr']['ReExamListArr']['ReExamSubject'] = "Re-exam Subject";

// Promotion Summary Report
$Lang['eReportCard']['ReportArr']['PromotionArr']['ReportName'] = "Promotion Meeting Report";

// Wong Kam Fai Secondary Transcript
$Lang['eReportCard']['ReportArr']['TranscriptArr']['StudentSource'] = "Student Source";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['FormClass'] = "Form Class";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['UserLogin'] = "User Login";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['ClassHistory'] = "Class History";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfIssue'] = "Date of Issue";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfGraduation'] = "Date of Graduation";
$Lang['eReportCard']['ReportArr']['TranscriptArr']['DateOfLeaving'] = "Date of Leaving";

$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['DeleteSignature'] = "Delete Signature";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['DeleteSchoolChop'] = "Delete School Chop";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['BackToTeacherExtraInfo'] = "Back to Teacher Extra Information";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SignatureFileTypeWarning'] = "Signature file must be an image (.gif, .jpg, .jpeg, .png, .bmp)";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['SchoolChopFileTypeWarning'] = "School chop file must be an image (.gif, .jpg, .jpeg, .png, .bmp)";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['ImagesUploadedSuccessfully'] = "<!--NumOfImage--> image(s) uploaded successfully.";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadedSignature'] = "Uploaded Signature";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['NotYetUploadedSignature'] = "Not yet uploaded Signature";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadedSchoolChop'] = "Uploaded School Chop";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['NotYetUploadedSchoolChop'] = "Not yet uploaded School Chop";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "Use the user login of the staff as the file name of the photo.";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "Photos with size under 1MB were recommanded.";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "Photos must be in .gif, .jpg, .jpeg, .png, or .bmp format.";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "More than 1 photos can be uploaded by archiving them in a zip file (.zip).";
$Lang['eReportCard']['SettingsArr']['TeacherExtraInfoArr']['UploadFileRemarksArr'][] = "Do NOT include any folders inside the zip file.";


$Lang['eReportCard']['GeneralArr']['ToDigitalArchive'] = "To Digital Archive";
$Lang['eReportCard']['GeneralArr']['ArchiveOption'] = "Archive Option";

$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "The report card template has been changed from the original design. Please go to \"".$eReportCard['Reports']." > ".$eReportCard['Reports_GenerateReport']."\" to print and verify the report card template before archiving the report.";
$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "<br />";
//$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "Changes of the template are listed below:";
//$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "<!--changesLog-->";
//$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "<br />";
$Lang['eReportCard']['GeneralArr']['TemplateChangedWarning'][] = "The archived report card records will be permanently deleted and CANNOT be recovered. Press <font style=\"font-size:large;\">Archive</font> to continue if you confirm to archive the report.";

$Lang['eReportCard']['GradingSchemeArr']['removeSchemeWarning'][] = "The following grading scheme(s) is linked to report cards already.  Please go to \"".$eReportCard['Settings']." > ".$eReportCard['Settings_SubjectsAndForm']."\" to update corresponding class level settings after removing the grading scheme(s).";
$Lang['eReportCard']['GradingSchemeArr']['removeSchemeWarning'][] = "The grading scheme(s) will be permanently deleted and CANNOT be recovered. Press <font style=\"font-size:large;\">Submit</font> to continue if you confirm to remove the grading scheme(s).";

$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['CoverPage'] = "Report Cover Page";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['ReportLogo'] = "Report Logo";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['ReportHeader'] = "Report Header";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['GradeDesc_DSE'] = "Grade Description (DSE)";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['ImageArr']['GradeDesc_IB'] = "Grade Description (IB)";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['DeleteImage'] = "Delete Image";
$Lang['eReportCard']['SettingsArr']['TemplateSettingsArr']['DeleteImageFailed'] = "Delete Image Failed";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['NotInSubmissionPeriod'] = "Not in submission period now";
$Lang['eReportCard']['ManagementArr']['ClassTeacherCommentArr']['CommentTooLong'] = "The comment exceeded the maximum number of characters limit.";

$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheet'] = "Marksheet Copy";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheetWarning'] = "All scores of the selected Report and Report Column(s) will be overwritten and cannot be recovered. Press Submit to copy marksheet.";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyMarksheetConfirmWarning'] = "All scores of the selected Report and Report Column(s) will be overwritten and cannot be recovered. Are you sure you want to copy marksheet?";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['SameSelectionWarning'] = "The selected option of \"From Report\" and \"To Report\" cannot be the same";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['FromReport'] = "From Report";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ToReport'] = "To Report";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['AllReportColumn'] = "All Report Columns";
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopySuccess'] = '1|=|Marksheet copied successfully';
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['CopyFailed'] = '0|=|Marksheet copy failed';
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ReportColumnNotMatched'] = '0|=|Number of report columns of the selected report is not matched';
$Lang['eReportCard']['ManagementArr']['DataHandlingArr']['ImportAllSubject'] = "Import All Subject";

$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['AllFormAllSubject'] = 'All forms & all subjects';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ThisFormAllSubject'] = 'This forms with all subjects';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ThisFormThisSubject'] = 'This forms & this subject only';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['FormNameEn'] = 'Form Name';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectCodeEn'] = 'Subject Code';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectNameEn'] = 'Subject Name';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentCodeEn'] = 'Subject Component Code';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentNameEn'] = 'Subject Component Name';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicCodeEn'] = 'Subject Topic Code';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameEnEn'] = 'Subject Topic Name (Eng)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameChEn'] = 'Subject Topic Name (Chi)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicTermEn'] = 'Subject Topic Term';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['FormNameCh'] = '級別名稱';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectCodeCh'] = '學科代號';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectNameCh'] = '學科名稱';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentCodeCh'] = '學科分卷代號';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectComponentNameCh'] = '學科分卷名稱';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicCodeCh'] = '學科範疇代號';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameEnCh'] = '學科範疇名稱 (英文)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicNameChCh'] = '學科範疇名稱 (中文)';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['SubjectTopicTermCh'] = '學科範疇學期';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoForm'] = 'Form Name cannot be empty';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['FormNotExisting'] = 'This Form does not exist';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectCode'] = 'Subject Code cannot be empty';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectNotExisting'] = 'This Subject does not exist';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectComponentNotExisting'] = 'This Subject Component does not exist';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicCode'] = 'Subject Topic Code cannot be empty';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicName'] = 'Subject Topic Name (Chi) and (Eng) cannot both be empty';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicNameEn'] = 'Subject Topic Name (Eng) cannot be empty';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicNameCh'] = 'Subject Topic Name (Chi) cannot be empty';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['NoSubjectTopicTerm'] = 'Subject Topic Term cannot be empty';
$Lang['eReportCard']['SettingsArr']['SubjectTopicsArr']['ImportWarningArr']['SubjectTopicTermNotExisting'] = 'Subject Topic Term does not exist';

$Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['MainSubject'] = 'Main Subject';
$Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['Subject'] = 'Subject';
$Lang['eReportCard']['SettingsArr']['MainSubjectsArr']['TechnicalSubject'] = 'Technical Subject';

$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['PercentageOfStudent'] = "Percentage of student";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['MarkGreaterThanOrEqualToFullMarkPercentage'] = "With full mark equals to <!--textbox-->% of full mark";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Test'] = "Test";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['DailyMark'] = "Daily Mark";
$Lang['eReportCard']['ReportArr']['MarkAnalysisArr']['Exam'] = "Exam";

$Lang['eReportCard']['ReportArr']['AwardPredictionArr']['Predict'] = "Completed Second Term Academic Result (for Prediction)";
$Lang['eReportCard']['ReportArr']['AwardPredictionArr']['Confirm'] = "Completed Third Term Academic Result (for Confirmation)";

$Lang['eReportCard']['ReportArr']['TransferResultToAwardStudentList'] = 'Transfer result to award student list';

$Lang['eReportCard']['ReportArr']['GradingReportArr']['NumOfAcademicYear'] = 'No. of academic years';
$Lang['eReportCard']['ReportArr']['GradingReportArr']['ShowResultWithTerm'] = 'Show this year <!--curTermSel--> and past year <!--pastTermSel--> result';

$Lang['eReportCard']['Test'] = "Test";
$Lang['eReportCard']['Exam'] = "Exam";

$eReportCard['Security'] = "Security";

// Subject Type
// $eReportCard['SubjectType'] = "Subject Type";
// $eReportCard['SubjectCredit'] = "Subject Credit";
// $eReportCard['SubjectLowerLimit'] = "Lower Limit";
// $eReportCard['NotSet'] = 'Not Set';
// $eReportCard['SubjectCore'] = "Compulsory / Core";
// $eReportCard['SubjectElection'] = 'Elective';
// $eReportCard['SubjectUnknown'] = 'Others';

// Subject Type
$eReportCard['SubjectType']['Type'] = 'Subject Type';
$eReportCard['SubjectType']['SubjectUnit'] = 'Subject Unit(s)';
$eReportCard['SubjectType']['SubjectDisplayLimit'] = 'Lowest marks for display';
$eReportCard['SubjectType']['NoSetting'] = 'No settings';
$eReportCard['SubjectType']['NotApplicable'] = "Not Applicable";
$eReportCard['SubjectType']['SubjectNormal'] = 'Core / Normal';
$eReportCard['SubjectType']['SubjectElective'] = 'Elective';
$eReportCard['SubjectType']['SubjectCompetitive'] = 'Competitive';

$eReportCard['GradeSettings'] = "Performance Descriptor";

// Subject Stream
$eReportCard['SubjectStream']['Selection'] = 'Art & Com. / Science';
$eReportCard['SubjectStream']['Art'] = 'Art & Com.';
$eReprotCard['SubjectStream']['Science'] = 'Science';
$eReportCard['SubjectStream']['ReturnMsg']['UpdateSuccess'] = "1|=|Update Settings Successfully";
$eReportCard['SubjectStream']['ReturnMsg']['UpdateFailed'] = "0|=|Update Settings Failed";

$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ConductScore'] = 'Conduct Score';
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ConductGrade'] = 'Conduct Grade';
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['ModifyGrade'] = 'Grade Adjustment';
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['WarningMsgArr']['GenerateConduct'] = "The generated conduct grades will be overwritten if you generate the conduct grades now. Are you sure you want to generate the conduct grades now?";
$Lang['eReportCard']['ManagementArr']['ConductGradeCalculation']['WarningMsgArr']['NotGenerated'] = "Conduct grades has not been generated yet";

$Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['Promotion'] = 'Remarks Display for Promotion';
$Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamPassed'] = 'Remarks Display for Re-exam passed';
$Lang['eReportCard']['SettingArr']['PromotionStatusRemarks']['ReExamFailed'] = 'Remarks Display for Re-exam failed';

$eReportCard['Reports_StudentYearlyResultReport'] = "Export student yearly performance result";
?>