<?php
// using: 
/*
 * Remarks:
 * 1) Arrange the lang variables in alphabetical order
 * 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
 * 3) Use single quote for the array keys and double quote for the lang value
 */
 
// A
$Lang['SLRS']['AvailablePeriods'] = "可代課日期";
$Lang["SLRS"]["AvailablePeriodsTo"] = " 至 ";

// B
$Lang['SLRS']['BasicSettings'] = "基本設定";
$Lang['SLRS']['BtnPrint'] = "列印";
$Lang['SLRS']['BtnExport'] = "匯出";

// C
$Lang["SLRS"]["cancelBtn"] = "取消";
$Lang["SLRS"]["confirmBtn"] = "確定";
$Lang["SLRS"]["changeTo"] = "轉為";

$Lang['SLRS']['CancelLesson'] = "取消課堂";
$Lang['SLRS']['CancelLessonDes']['CancelDate'] = "日期";
$Lang['SLRS']['CancelLessonDes']['DateTimeInput'] = "創建於";
$Lang['SLRS']['CancelLessonDes']['Delete'] = "刪除";
$Lang['SLRS']['CancelLessonDes']['LessonInfo'] = "課堂";
$Lang['SLRS']['CancelLessonDes']['Remark'] = "備註";
$Lang['SLRS']['CancelLessonDes']['Target'] = "對象";
$Lang['SLRS']['CancelLessonDes']['TeacherName'] = "老師名稱";
$Lang['SLRS']['CancelLessonDes']['Timeslot'] = "課節";

$Lang['SLRS']['CancelLessonDes']['msg']["noTimeslotRecord"] = "沒有可取消的課堂。";
$Lang['SLRS']['CancelLessonDes']['msg']["alreadyArrange"] = "此課堂已安排代課。";
$Lang['SLRS']['CancelLessonDes']['msg']["alreadyCancel"] = "此課堂已取消。";
$Lang['SLRS']['CancelLessonDes']['msg']["alreadyExchange"] = "此課堂已安排對調。";

$Lang['SLRS']['CycleWeekMaximunLessons'] = "每個循環週的課擔上限";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["0"] = "無限";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["1"] = "10節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["2"] = "11節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["3"] = "12節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["4"] = "13節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["5"] = "14節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["6"] = "15節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["7"] = "16節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["8"] = "17節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["9"] = "18節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["10"] = "19節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["11"] = "20節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["12"] = "21節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["13"] = "22節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["14"] = "23節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["15"] = "24節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["16"] = "25節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["17"] = "26節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["18"] = "27節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["19"] = "28節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["20"] = "29節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["21"] = "30節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["22"] = "31節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["23"] = "32節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["24"] = "33節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["25"] = "34節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["26"] = "35節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["27"] = "36節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["28"] = "37節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["29"] = "38節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["30"] = "39節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["31"] = "40節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["32"] = "41節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["33"] = "42節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["34"] = "43節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["35"] = "44節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["36"] = "45節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["37"] = "46節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["38"] = "47節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["39"] = "48節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["40"] = "49節";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["41"] = "50節";

$Lang['SLRS']['CycleWeekMaximunLessonsVal']["0"] = "無限";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["1"] = "10節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["2"] = "11節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["3"] = "12節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["4"] = "13節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["5"] = "14節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["6"] = "15節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["7"] = "16節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["8"] = "17節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["9"] = "18節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["10"] = "19節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["11"] = "20節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["12"] = "21節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["13"] = "22節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["14"] = "23節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["15"] = "24節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["16"] = "25節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["17"] = "26節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["18"] = "27節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["19"] = "28節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["20"] = "29節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["21"] = "30節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["22"] = "31節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["23"] = "32節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["24"] = "33節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["25"] = "34節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["26"] = "35節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["27"] = "36節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["28"] = "37節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["29"] = "38節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["30"] = "39節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["31"] = "40節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["32"] = "41節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["33"] = "42節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["34"] = "43節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["35"] = "44節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["36"] = "45節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["37"] = "46節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["38"] = "47節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["39"] = "48節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["40"] = "49節";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["41"] = "50節";

$Lang['SLRS']['ClosedLocation'] = "設定無法使用位置";
$Lang['SLRS']['ClosedLocationTH']["th_Location"] = "位置";
$Lang['SLRS']['ClosedLocationTH']["th_StartDate"] = "開始時間";
$Lang['SLRS']['ClosedLocationTH']["th_EndDate"] = "完結時間";
$Lang['SLRS']['ClosedLocationTH']["th_Reasons"] = "原因";
$Lang['SLRS']['ClosedLocationTH']["please-select"] = "請選擇";

$Lang["SLRS"]["ClosedLocationErr"]["Location"] = "請選擇";
$Lang["SLRS"]["ClosedLocationErr"]["StartDate"] = "請選擇";
$Lang["SLRS"]["ClosedLocationErr"]["EndDate"] = "請選擇";
$Lang["SLRS"]["ClosedLocationErr"]["Reasons"] = "請輸入原因";


// D
$Lang['SLRS']['DailyMaximunLessons'] = "每天的課擔上限";
$Lang['SLRS']['DailyMaximunLessonsDes']["0"] = "無限";
$Lang['SLRS']['DailyMaximunLessonsDes']["1"] = "3節";
$Lang['SLRS']['DailyMaximunLessonsDes']["2"] = "4節";
$Lang['SLRS']['DailyMaximunLessonsDes']["3"] = "5節";
$Lang['SLRS']['DailyMaximunLessonsDes']["4"] = "6節";
$Lang['SLRS']['DailyMaximunLessonsDes']["5"] = "7節";
$Lang['SLRS']['DailyMaximunLessonsDes']["6"] = "8節";
$Lang['SLRS']['DailyMaximunLessonsDes']["7"] = "9節";
$Lang['SLRS']['DailyMaximunLessonsDes']["8"] = "10節";
$Lang['SLRS']['DailyMaximunLessonsVal']["0"] = "999";

$Lang['SLRS']['DailyMaximunLessonsVal']["1"] = "3";
$Lang['SLRS']['DailyMaximunLessonsVal']["2"] = "4";
$Lang['SLRS']['DailyMaximunLessonsVal']["3"] = "5";
$Lang['SLRS']['DailyMaximunLessonsVal']["4"] = "6";
$Lang['SLRS']['DailyMaximunLessonsVal']["5"] = "7";
$Lang['SLRS']['DailyMaximunLessonsVal']["6"] = "8";
$Lang['SLRS']['DailyMaximunLessonsVal']["7"] = "9";
$Lang['SLRS']['DailyMaximunLessonsVal']["8"] = "10";

$Lang['SLRS']['DailyMaximunSubstitution'] = "每天代課上限";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["0"] = "無限";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["1"] = "1節";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["2"] = "2節";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["3"] = "3節";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["4"] = "4節";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["5"] = "5節";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["6"] = "6節";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["7"] = "7節";

$Lang['SLRS']['DailyMaximunSubstitutionVal']["0"] = "999";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["1"] = "1";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["2"] = "2";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["3"] = "3";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["4"] = "4";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["5"] = "5";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["6"] = "6";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["7"] = "7";

$Lang['SLRS']['DailyTeacherSubstitutionReport']['assignedTeacher'] = "代課老師";
$Lang['SLRS']['DailyTeacherSubstitutionReport']["target"] = "對象";
$Lang['SLRS']['DailyTeacherSubstitutionReport']["teacher"] = "教師";

$Lang['SLRS']['Display'] = "顯示";

// E
$Lang['SLRS']['ExternalTeacher'] = "外來代課老師";
$Lang['SLRS']['ExternalTeacherDes']["Teacher"] = "外來代課老師";
$Lang['SLRS']['ExternalTeacherDes']["StartDate"] = "代課開始日期";
$Lang['SLRS']['ExternalTeacherDes']["EndDate"] = "代課完結日期";
$Lang['SLRS']['ExternalTeacherDes']["ToTeacher"] = "暫代";
$Lang['SLRS']['ExternalTeacherDes']["ModifiedDate"] = "最後更新時間";
$Lang['SLRS']['ExternalTeacherDes']["Remark"] = "備註";
$Lang['SLRS']['ExternalTeacherDes']["ErrorMessage"] = "這時段已安排代課，不能在此時段再安排代課";
$Lang['SLRS']['ExternalTeacherDes']["ErrorTeacherMessage"] = "此老師在這時段已有其他外來代課老師代課，不能再申請臨時代課安排";
$Lang['SLRS']['ExternalTeacherDes']["DateErrorMessage"] = "開始日期不能遲於完結日期";
$Lang['SLRS']['ExternalTeacherDes']["EmptyErrorMessage"] = "此項不能空白";
$Lang['SLRS']['ExchangeLocation'] = "更改位置";

// F
$Lang['SLRS']['FirstLessonSubstituion'] = "第一堂(即班主任堂)需不需要安排代課";
$Lang['SLRS']['FirstLessonSubstituionDes']["0"] = "需要";
$Lang['SLRS']['FirstLessonSubstituionDes']["1"] = "不需";

$Lang['SLRS']['FormSixTeacherSubstition'] = "中六同學去應考文憑試，任教中六的老師會不會被安排代課";
$Lang['SLRS']['FormSixTeacherSubstitionDes']["0"] = "仍可代課";
$Lang['SLRS']['FormSixTeacherSubstitionDes']["1"] = "不需代課";

$Lang['SLRS']['ForcedExchangeMsg'] = "以下同節課堂可進行合併對調";
$Lang['SLRS']['ForcedExchange'] = "同節課堂進行合併對調";
$Lang['SLRS']['ForcedExchangePrompt'] = "合併對調後將不可更改對調內容";
$Lang['SLRS']['ForcedExchangeMaster'] = "合併對調同節課堂記錄";
$Lang['SLRS']['ForcedExchangeSlave'] = "合併對調同節課堂記錄 (附屬)";
$Lang['SLRS']['ForcedExchangeSlaveAlert'] = "不能編輯合併對調同節課堂記錄";


// G


// H
$Lang['SLRS']['HolidaySettings'] = "假期分類及影響";

// I
$Lang['SLRS']['InputValidation']["AdjustedType"] = "請選擇調整類型";
$Lang['SLRS']['InputValidation']["AdjustedValue"] = "請選擇調整數值";
// J


// K


// L
$Lang['SLRS']['LessonRearrangement'] = "代課編排";
$Lang["SLRS"]["LessonRemarks"] = "備註";
$Lang['SLRS']['LessonRemarksForOriginalLessonMarks'] = "備註 <br>[<span class='lesson_remarks_txt'>%s</span>]";
$Lang['SLRS']['LessonRemarksForOriginalLesson'] = "備註 <br>[<span class='lesson_remarks_txt'>原本課堂</span>]";

$Lang['SLRS']['LocationNameDisplay'] = "上課地點名稱顯示";
$Lang['SLRS']['LocationNameDisplayDes']["0"] = "全名稱 ";
$Lang['SLRS']['LocationNameDisplayDes']["1"] = "代號 ";
$Lang['SLRS']['LastModified'] = "最後調整";

$Lang['SLRS']['LessonExchange'] = "對調上課安排";
$Lang['SLRS']['LessonExchangeTitle'] = "指定課堂對調安排";
$Lang['SLRS']['LessonExchangeDes']['DateTimeModified'] = "最後更新時間";
$Lang['SLRS']['LessonExchangeDes']['ExchangeLessonTeacher'] = "對調上課的老師";
$Lang['SLRS']['LessonExchangeDes']['ExchangeLesson'] = "相關課堂";
$Lang['SLRS']['LessonExchangeDes']['TimeslotIsNotEmpty'] = "當天此節不是空堂";
$Lang['SLRS']['LessonExchangeDes']['CannotExchangeMyLesson'] = "不能對調同一個課堂";

$Lang['SLRS']['LessonExchangeDes']['OriginalLesson'] = "原本課堂";
$Lang['SLRS']['LessonExchangeDes']['ExchangedLesson'] = "改為課堂";
$Lang['SLRS']['LessonExchangeDes']['Date'] = "日期";
$Lang['SLRS']['LessonExchangeDes']['Teacher'] = "老師";
$Lang['SLRS']['LessonExchangeDes']['Classroom'] = "課堂";
$Lang['SLRS']['LessonExchangeDes']['Location'] = "地點";
$Lang['SLRS']['LessonExchangeDes']['ToBeChanged'] = "更改";
$Lang['SLRS']['LessonExchangeDes']['Cancel'] = "取消";
$Lang['SLRS']['LessonExchangeDes']['DeleteSubstition']= '刪除對調上課安排';
$Lang['SLRS']['LessonExchangeDes']['AnotherClass'] = "對調同節課堂";

// M
$Lang['SLRS']['Menu']['Management'] = '管理';
$Lang['SLRS']['Menu']['Reports'] = '報告';
$Lang['SLRS']['Menu']['Settings'] = '設定';
// N
$Lang['SLRS']['NoSubstitutionCalBalance'] = "當選取'無需代課安排'，影響代課老師結餘";
$Lang['SLRS']['NoSubstitutionCalBalanceDes'][0] = "是";
$Lang['SLRS']['NoSubstitutionCalBalanceDes'][1] = "否";
$Lang['SLRS']['NotAvailableTeacher'] = "無法代課";
$Lang['SLRS']['NotAvailableTeacherRecord'] = "無法代課記錄";
$Lang['SLRS']['NotAvailableTeachers'] = "無法代課老師名單";
$Lang['SLRS']['NotAvailableTeachersTH']["th_UserName"] = "姓名";
$Lang['SLRS']['NotAvailableTeachersTH']["th_Teacher"] = "教職員";
$Lang['SLRS']['NotAvailableTeachersTH']["th_StartDate"] = "開始時間";
$Lang['SLRS']['NotAvailableTeachersTH']["th_EndDate"] = "完結時間";
$Lang['SLRS']['NotAvailableTeachersTH']["th_Reasons"] = "原因";
$Lang['SLRS']['NotAvailableTeachersForm']["please-select"] = "請選擇";

$Lang["SLRS"]["NotAvailableTeachersErr"]["teacher"] = "請選擇";
$Lang["SLRS"]["NotAvailableTeachersErr"]["StartDate"] = "請選擇";
$Lang["SLRS"]["NotAvailableTeachersErr"]["EndDate"] = "請選擇";
$Lang["SLRS"]["NotAvailableTeachersErr"]["Reasons"] = "請輸入原因";
$Lang['SLRS']['NotAvailableTeachersErr']["NotAvailable"] = "此老師現在無法代課，請選擇其他老師";

// O
$Lang['SLRS']['OfficialLeavelAO'] = "因公缺席";
$Lang['SLRS']['OfficialLeavelWS'] = "出席工作坊或座談";
$Lang['SLRS']['OfficialLeavelSLP'] = "有薪病假";
$Lang['SLRS']['OfficialLeavelPL'] = "其他有薪假期 如有薪事假";
$Lang['SLRS']['OfficialLeavelSTL'] = "有薪進修";
$Lang['SLRS']['OfficialLeavelOptDes']["0"] = "影響代課結餘 ";
$Lang['SLRS']['OfficialLeavelOptDes']["1"] = "不影響代課結餘";
$Lang['SLRS']['Or'] = "或";

$Lang['SLRS']['OfficialLeavelAOVal'] = "AO";
$Lang['SLRS']['OfficialLeavelWSVal'] = "WS";
$Lang['SLRS']['OfficialLeavelSLPVal'] = "SLP";
$Lang['SLRS']['OfficialLeavelPLVal'] = "PL";
$Lang['SLRS']['OfficialLeavelSTLVal'] = "STL";

$Lang["SLRS"]["OutOfMaxLessonLimitation"] = "超出每天的課擔上限";
$Lang["SLRS"]["OutOfMaxSubstitutionLimitation"] = "超出每天代課上限";

// P
$Lang['SLRS']['PresetSubstitutionLocation'] = "預設優先代課位置";
$Lang['SLRS']['PresetSubstitutionLocationTH']["SubstitutionLimitPerDay"] = "預設優先代課位置每天上限";
$Lang['SLRS']['PresetSubstitutionLocationTH']["PresetLocation"] = "預設優先代課位置";

// Q


// R
$Lang['SLRS']['ReportGeneration']="產生報告";
$Lang['SLRS']['ReportDate'] = '日期';
$Lang['SLRS']['ReportWarning'] = '附有「%s」的項目必須填寫';
$Lang['SLRS']['ReportDateWarning'] = '請填上日期';
$Lang['SLRS']['ReportDailyTeacherSubstituion']="每日代課安排";
$Lang['SLRS']['ReportLessonExchangeSummary']="對調上課概要";
$Lang['SLRS']['ReportSummaryofClassroomChangeforLessons']="上課地點變更概要";
$Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution']="年度代課統計";
$Lang['SLRS']['ReportTeachersAbsenceSubstitutionSummary']="請假代課概要";

$Lang['SLRS']['ReportPeriod']="課節";
$Lang['SLRS']['ReportAssignedTo']="代課老師";
$Lang['SLRS']['ReportClass']="班別";
$Lang['SLRS']['ReportSubject']="科組";
$Lang['SLRS']['ReportRoom']="課室";
$Lang['SLRS']['ReportPeriod']="課節";
$Lang['SLRS']['ReportTeacher']="老師";
$Lang['SLRS']['ReportTo']="To";
$Lang['SLRS']['ReportAcademicYear']="學年";
$Lang['SLRS']['ReportSemester']="學期";
$Lang['SLRS']['ReportTC']="老師";
// $Lang['SLRS']['ReportPeriodSickLeave']="請假而要人代的課節總數 (A)";
$Lang['SLRS']['ReportPeriodSickLeave']="代課節總數(忽略不影響結餘的假期分類) (A)";
$Lang['SLRS']['ReportPeriodSubstitition']="代課總數 (B)";
$Lang['SLRS']['ReportCarriedPreviousYear']="本學期初始值 (C)";
$Lang['SLRS']['ReportBalance']="代課結餘 (B-A+C)";
$Lang['SLRS']['ReportSubTC']="代課老師";
$Lang['SLRS']['ReportVolunteered']="自薦";
$Lang['SLRS']['ReportExemption']="例外情況";
$Lang['SLRS']['ReportOverall']="全部代課紀錄";

$Lang['SLRS']['ReportSubstitutionFor']="請假老師";
$Lang['SLRS']['ReportDay']="循環日";
$Lang['SLRS']['ReportDate']="日期";
$Lang['SLRS']['ReportLargeText']="增大字體";
$Lang['SLRS']['ReportSmallText']="縮少字體";
$Lang['SLRS']['ReportPrint']="列印";
$Lang['SLRS']['ReportProcessing']="處理進行中...";

$Lang['SLRS']['ReportSendRecord']="發送通知日期";
$Lang['SLRS']['ReportSendBy']="紀錄建立者";
$Lang['SLRS']['ReportRecipient']="代課老師";
$Lang['SLRS']['ReportRecipientStatus']="狀態";
$Lang['SLRS']['ReportGenerationPushMessageBtn'] = "發送通知";
$Lang['SLRS']['ReportGenerationRePushMessageBtn'] = "重新發送通知";
$Lang['SLRS']['ReportGenerationSending'] = "發送中";
$Lang['SLRS']['ReportGenerationSuccess'] = "已發出通知";
$Lang['SLRS']['ReportGenerationFailed'] = "傳送失敗，請重新發送";
$Lang['SLRS']['ReportGenerationNoRecord'] = "沒有資料";
$Lang['SLRS']['ReportGenerationSLRSNoticationEN'] = "Today's subsitution arrangement";
$Lang['SLRS']['ReportNotificationLog'] = "過往通知紀錄";

$Lang['SLRS']['ReportGenerationSLRSNoticationEN'] = "今日代課安排 \n Today's subsitution arrangement";
$Lang['SLRS']['ReportNotificationLog'] = "過往通知紀錄";
// $Lang['SLRS']['ReportGenerationSLRSNoticationTo'] = "致 To: ";
// $Lang['SLRS']['ReportGenerationSLRSNoticationDate'] = "日期  Date:";
$Lang['SLRS']['ReportGenerationSLRSNoticationText'] = "致: __Name__
日期: __DateChi__
請留意 以下的代課安排:
		
To: __Name__
Date: __DateEng__
Please be informed that you are assigned for below subsitution(s):
";
// $Lang['SLRS']['ReportGenerationSLRSNoticationThanks'] = "Thank you for your cooperation~";
$Lang['SLRS']['ReportLoading'] = "更新中...";


// S
$Lang['SLRS']['SLRS'] = "代課編排系統";
$Lang['SLRS']['SystemProperties'] = "系統特性";
$Lang['SLRS']['SystemChari'] = "系統特性";
$Lang['SLRS']['SpecialClassroomLocation'] = "如上課地點是特別室";
$Lang['SLRS']['SpecialClassroomLocationDes']["0"] = "不需更改地點";
$Lang['SLRS']['SpecialClassroomLocationDes']["1"] = "安排到圖書館";
$Lang['SLRS']['SpecialClassroomLocationDes']["2"] = "安排回學生自己課室";

$Lang['SLRS']['StudentDailyReport'] = "學生當日代課安排報告";

$Lang['SLRS']['SubstituteBalance'] = "代課結餘";

$Lang['SLRS']['SubstitutionAfterSickLeave'] = "如老師上一個工作天曾請病假當天不需代課";
$Lang['SLRS']['SubstitutionAfterSickLeaveDes']["0"] = "仍可代課 ";
$Lang['SLRS']['SubstitutionAfterSickLeaveDes']["1"] = "不需代課";

$Lang['SLRS']['SubjectDisplay'] = "科目名稱顯示";
$Lang['SLRS']['SubjectDisplayDes']["0"] = "全名稱 ";
$Lang['SLRS']['SubjectDisplayDes']["1"] = "縮寫 ";
$Lang['SLRS']['SubjectDisplayDes']["2"] = "簡寫 ";
$Lang['SLRS']['SubjectDisplayDes']["3"] = "WebSams代號";

$Lang['SLRS']['SubjectGroupDisplay'] = "科組名稱顯示";
$Lang['SLRS']['SubjectGroupDisplayDes']["0"] = "全名稱 ";
$Lang['SLRS']['SubjectGroupDisplayDes']["1"] = "代號";

$Lang['SLRS']['SubstitutionBalanceDisplay'] = "代課結餘顯示";
$Lang['SLRS']['SubstitutionBalanceDisplayDes']["0"] = "代課者加, 被代者減 ";
$Lang['SLRS']['SubstitutionBalanceDisplayDes']["1"] = "代課者減, 被代者加";

$Lang['SLRS']['SubstituteTeacherSettings'] = "可代課老師";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['UserName'] = "姓名 ";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['Balance'] = "代課結餘 ";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['Priority'] = "代課遴選次序";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['JoinDate'] = "加入日期";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['EditSelectionOrder'] = "編輯遴選次序";


$Lang['SLRS']['SubstitutePrioritySettings'] = "代課優先次序";


$Lang['SLRS']['SubstitutePrioritySettingsDes']['FirstPriority'] = "首要優先條件";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SecondPriority'] = "次要優先條件";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['Adjustment'] = "(調整次序)";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['UpperLimitmation'] = "上限";

$Lang['SLRS']['SubstitutePrioritySettingsDes']['ExternalTeachers'] = "外來代課老師";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['InternalTeachers'] = "校內老師";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['DailyTotalLessons'] = "當天總課擔 (代課計算在內)";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['DailySubstituteLessons'] = "當天代課量 (少的優先)";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SubstituteBalance'] = "代課結餘";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SubstituteFilterOrder'] = "代課遴選次序";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['ClassTeacher'] = "該班的班主任";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SubjectTeacher'] = "該班的科任老師";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SameSubjectTeacher'] = "任教同一科的老師";

$Lang['SLRS']['SelectSubstituteTeachers'] = "請選擇可代課老師";
$Lang['SLRS']['SelectedSubstituteTeachers'] = "已選擇可代課老師";
$Lang['SLRS']['SearchByLoginID'] = "以登入名稱搜尋";

$Lang['SLRS']['SubstitutionBalance'] = "代課結餘";
$Lang['SLRS']['SubstitutionBalanceDes']['UserName'] = '教師';
$Lang['SLRS']['SubstitutionBalanceDes']['YearlyAdd'] = "本年度代課所得";
$Lang['SLRS']['SubstitutionBalanceDes']['YearlyReduce'] = "本年度被代課所扣";
$Lang['SLRS']['SubstitutionBalanceDes']['Balance'] = '現時結餘';
$Lang['SLRS']['SubstitutionBalanceDes']['LastModified'] = '最後調整';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedType'] = '調整類型';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedValue'] = '調整數值';
$Lang['SLRS']['SubstitutionBalanceDes']['Reason'] = '原因';
$Lang['SLRS']['SubstitutionBalanceDes']['BalanceReview'] = '老師(結餘預覽)';
$Lang['SLRS']['SubstitutionBalanceDes']['BalanceTeacher'] = '老師';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedLogReset'] = "重設代課結餘安排";
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedLogResetAppend'] = '更新後，重新加入本學年代課記錄及計算結餘';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedLogResetIgnore'] = '更新後﹐忽略本學年現存代課結餘記錄';

$Lang['SLRS']['SubstitutionBalanceAdjTyp']['AddBalance'] = '增加結餘';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['ReduceBalance'] = '減少結餘';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetBalance'] = '設定結餘';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceInfo'] = '重設學年的起始日期及代課結餘';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceInfoReason'] = '更改起始日期: __RESET_DATE__';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalance'] = '選取"本學年代課起始日期"，調整起始結餘數值及匯入起始日期後的代課記錄結餘';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] = "<ul>";
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] .= "<li><u>此設定將會刪除現有結餘記錄</u></li>";
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] .= "<li> 例如本學年起始日期是  <u>9 月 1 日</u>，重設起始日期為 <u>10 月 1 日</u>及起始值為 '0'  ( 僅限結餘計算，不影響代課安排及校曆表設定 )。<br>系統將會<u>從 10 月 1 日之後重置並<b>以現時設定</b>計算餘額記錄</u>（ <i>忽略  9 月 1 日 和 9 月 30 日 之間的記錄</i> ）</li>";
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] .= "</ul>";
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustmentRecord'] = '調整紀錄';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['Date'] = '日期';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['BeforeBalance'] = '調整前結餘';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['Adjustment'] = '調整';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['AfterBalance'] = '調整後結餘';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['AdjustedType'] = '調整類型';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['Reason'] = '原因';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['InputUser'] = '經手人';

$Lang['SLRS']['SubstitutionArrangement'] = '代課安排';
$Lang['SLRS']['SubstitutionArrangementCreate'] = '新增代課安排';
$Lang['SLRS']['SubstitutionArrangementUpdate'] = '更改代課安排';
$Lang['SLRS']['SubstitutionArrangementDelete'] = '移除代課安排';
$Lang['SLRS']['SubstitutionArrangementHasLeaveRecord'] = '當天有請假記錄';
// $Lang['SLRS']['SubstitutionArrangementHasAlreadySRRecord'] = '當天有請假記錄及已安排代課';
$Lang['SLRS']['SubstitutionArrangementHasAlreadySRRecord'] = '當天有請假記錄';
$Lang['SLRS']['SubstitutionArrangementHasRecord'] = '當天有代課安排記錄';
$Lang['SLRS']['SubstitutionArrangementHasExchangeRecord'] = '當天有對調課堂記錄';
$Lang['SLRS']['SubstitutionArrangementFromExchangeRecord'] = "此課堂由對調安排產生";
$Lang['SLRS']['SubstitutionArrangementHasExchange'] = '已安排對調該課堂';
$Lang['SLRS']['SubstitutionArrangementAlreadySubstitution'] = "已安排代課";
$Lang['SLRS']['SubstitutionArrangementHasRecordDisableExchange'] = "沒有可對調課堂";

$Lang['SLRS']['TempSubstitutionArrangementCreate'] = '新增臨時代課安排';
$Lang['SLRS']['TempSubstitutionArrangementUpdate'] = '更改臨時代課安排';
$Lang['SLRS']['TempSubstitutionArrangementDelete'] = '移除臨時代課安排';

$Lang['SLRS']['SubstitutionArrangementSubTC'] = ' (代課老師)';

$Lang['SLRS']['SubstitutionArrangementDes']['Teacher']= '請假老師';
$Lang['SLRS']['SubstitutionArrangementDes']['Lesson']= '節量';
$Lang['SLRS']['SubstitutionArrangementDes']['Action']= '代課安排';
$Lang['SLRS']['SubstitutionArrangementDes']['DateTimeModified']= '最後更改時間';
$Lang['SLRS']['SubstitutionArrangementDes']['DeleteSubstition']= '刪除請假及代課';
$Lang['SLRS']['SubstitutionArrangementDes']['AddLeave']="新增請假";
$Lang['SLRS']['SubstitutionArrangementDes']['Duration']="時段";
$Lang['SLRS']['SubstitutionArrangementDes']['AffectedLesson']= '受影響課節量';
$Lang['SLRS']['SubstitutionArrangementDes']['InitialDate']= '本學年代課起始日期';
$Lang['SLRS']['SubstitutionArrangementDes']['isExchangeRecord']= '已安排對調該課堂';

$Lang['SLRS']['SubstitutionArrangementDes']['Date']="日期";
$Lang['SLRS']['SubstitutionArrangementDes']['LeaveTeacher']="請假老師";
$Lang['SLRS']['SubstitutionArrangementDes']['Reason']="原因";
$Lang['SLRS']['SubstitutionArrangementDes']['Substitution']="代課";
$Lang['SLRS']['SubstitutionArrangementDes']['TeacherMandatory']="請選擇請假老師";
$Lang['SLRS']['SubstitutionArrangementDes']['ReasonMandatory']="請選擇原因";
$Lang['SLRS']['SubstitutionArrangementDes']['DurationMandatory']="請選擇時段";
$Lang['SLRS']['SubstitutionArrangementDes']['SessionMandatory']="請選擇時段";
$Lang['SLRS']['SubstitutionArrangementDes']['ArrangementMandatory']="請選擇代課安排";

$Lang['SLRS']['SubstitutionArrangementDes']['Session']="時段";

$Lang['SLRS']['SubstitutionSessionDes']['0']="上午";
$Lang['SLRS']['SubstitutionSessionDes']['1']="下午";
$Lang['SLRS']['SubstitutionSessionDes']['2']="全日";
$Lang['SLRS']['SubstitutionSessionVal']['0']="am";
$Lang['SLRS']['SubstitutionSessionVal']['1']="pm";
$Lang['SLRS']['SubstitutionSessionVal']['2']="wd";

$Lang['SLRS']['SubstitutionActionDes']['0']="稍後安排";
$Lang['SLRS']['SubstitutionActionDes']['1']="現在安排";

$Lang['SLRS']['SubstitutionActionVal']['0']="LATER";
$Lang['SLRS']['SubstitutionActionVal']['1']="NOW";

$Lang['SLRS']['SubstitutionArrangementDes']['Location']="地點";
$Lang['SLRS']['SubstitutionArrangementDes']['Balance']="代課結餘";
$Lang['SLRS']['SubstitutionArrangementDes']['DailyLesson']="當天課節量";
$Lang['SLRS']['SubstitutionArrangementDes']['TimeTable']="時間表";
$Lang['SLRS']['SubstitutionArrangementDes']['Reserve']="空堂";
$Lang['SLRS']['SubstitutionArrangementDes']['View']="檢視";
$Lang['SLRS']['SubstitutionArrangementDes']['Arrange']="安排";
$Lang['SLRS']['SubstitutionArrangementDes']['ToBeChanged']="更改";
$Lang['SLRS']['SubstitutionArrangementDes']['Cancel']="取消";
$Lang['SLRS']['SubstitutionArrangementDes']['Timetable']="時間表";
$Lang['SLRS']['SubstitutionArrangementDes']['Room']="課堂";
$Lang['SLRS']['SubstitutionArrangementDes']['SubstitutedLesson']="代課";

$Lang['SLRS']['SubstitutionArrangementDes']['StrongSubstitutedTeacher']="粗體: 外來代課老師 ";
$Lang['SLRS']['SubstitutionArrangementDes']['EmClassTeacher']="斜體: 該班班主任 ";
$Lang['SLRS']['SubstitutionArrangementDes']['More']="更多";

$Lang['SLRS']['SubstitutionArrangementDes']['AMError']="不能再請上午和全日";
$Lang['SLRS']['SubstitutionArrangementDes']['PMError']="不能再請下午和全日";
$Lang['SLRS']['SubstitutionArrangementDes']['WDError']="不能再請全日";
$Lang['SLRS']['SubstitutionArrangementDes']['NoLesson']="這時段沒有課堂";
$Lang['SLRS']['SubstitutionArrangementDes']['WholeDayError'] = "此日期已有代課安排記錄";
$Lang['SLRS']['SubstitutionArrangementDes']['AMExistError'] = "上午已有代課安排記錄";
$Lang['SLRS']['SubstitutionArrangementDes']['PMExistError'] = "下午已有代課安排記錄";

$Lang['SLRS']['SubstitutionArrangementDes']['ExistSupplyTeacherSubstitution'] = "已安排外來代課老師來代課，不能再申請代課安排";
$Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitution'] = "已安排為代課老師，不能新增請假記錄及安排代課，請先更改代課老師";
$Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitutionList'] = "當天已安排為代課老師";
$Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitutionListArrangeAgain'] = "請更改代課老師";


$Lang['SLRS']['SubstitutionArrangementDes']['isVolunteer'] = '自願代課';
$Lang['SLRS']['SubstitutionArrangementDes']['notArranged'] ='沒有安排';
$Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'] = '無需代課安排';
$Lang['SLRS']['SubstitutionArrangementDes']['noSubstitutedTeacher'] ='沒有代課老師安排';
$Lang['SLRS']['SubstitutionArrangementDes']['defaultSubjectGroupNoSubstitution'] ='此課組預設為無需代課安排';
$Lang['SLRS']['SubstitutionArrangementDes']['subjectGroupNoSubstitution'] ='預設課組為無需代課安排';
$Lang['SLRS']['SubstitutionArrangementDes']['InformationUpdated'] ='資料已更改，請留意最新安排';
// T
$Lang['SLRS']['TemporarySubstitutionArrangement']="臨時代課安排";
$Lang['SLRS']['TempSubstitutionArrangement']['StrongSubstitutedTeacher']="粗體: 外來代課老師 ";
$Lang['SLRS']['TempSubstitutionArrangement']['EmClassTeacher']="斜體: 該班班主任 ";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Room']="課堂";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Date']="日期";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['LeaveTeacher']="請假老師";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Location']="地點";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Balance']="代課結餘";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['DailyLesson']="當天課節量";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['TimeTable']="時間表";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Reserve']="空堂";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['ToBeChanged']="更改";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Cancel']="取消";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['View']="檢視";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Teacher'] = "請假老師";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['ToTeacher'] = "請假老師";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Action'] = "代課安排";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['DateTimeModified']='最後更改';
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Lesson']= '節量';
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['AddArrangment']= '新增安排';
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['DuplicationRequest']="不能重覆申請臨時代課安排";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['ExistSupplyTeacherTemporarySubstitution']="已安排外來代課老師來代課，不能再申請臨時代課安排";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['isVolunteer']='自願代課';
// U
$Lang['Sys']['MsgUpdate'] = " 已更新紀錄。 ";
$Lang["SLRS"]["User_Suspended"] = "<span style='color:#f00; line-height:1.5em;'>此用戶已暫停</span>";

// V
$Lang['SLRS']['VoluntarySubstitute'] = "允許自願代課";
$Lang['SLRS']['VoluntarySubstituteDes']["0"] = "是";
$Lang['SLRS']['VoluntarySubstituteDes']["1"] = "否";

$Lang['SLRS']['PrintDetailedOption']['Signature'] = "簽署";
$Lang['SLRS']['PrintDetailedOption']['Remark'] = "備註（自行輸入）";

// W


// X


// Y

// Z


?>