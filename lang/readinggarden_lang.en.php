<?php
// editing by RITA
//include_once('readinggarden_lang.en.marcus.php');
//include_once('readinggarden_lang.en.carlos.php');

$Lang['ReadingGarden']['ReadingGarden'] = "Reading Scheme";

$Lang['ReadingGarden']['Class'] = "Class";
$Lang['ReadingGarden']['Form'] = "Form";
$Lang['ReadingGarden']['BookSource'] = "Source of book";
$Lang['ReadingGarden']['CategoryCode'] = "Category Code";
$Lang['ReadingGarden']['Book'] = "Book";
$Lang['ReadingGarden']['Attachment'] = "Attachment";
$Lang['ReadingGarden']['AnswerSheet'] = "Answer Sheet";
$Lang['ReadingGarden']['Name'] = "Name";

$Lang['ReadingGarden']['StudentRecord'] = "Student Record";
$Lang['ReadingGarden']['MyRecord'] = "My Record";
$Lang['ReadingGarden']['State'] = "Level";
$Lang['ReadingGarden']['AssignedBook'] = "Assigned Book";
$Lang['ReadingGarden']['MyReadingBook'] = "My Reading Record";
$Lang['ReadingGarden']['More'] = "more...";
$Lang['ReadingGarden']['Target'] = "Target";
$Lang['ReadingGarden']['ReadingStatus'] = "Reading Status";
$Lang['ReadingGarden']['BookReport'] = "Book Report";
$Lang['ReadingGarden']['Unread'] = "Unread";
$Lang['ReadingGarden']['Read'] = "Read";
$Lang['ReadingGarden']['SubmitBookReport'] = "Submit Book Report";
$Lang['ReadingGarden']['BookReportSubmitting'] = "Report Submitting...";
$Lang['ReadingGarden']['Like'] = "Like";
$Lang['ReadingGarden']['Unlike'] = "Unlike";
$Lang['ReadingGarden']['likes'] = "like";
$Lang['ReadingGarden']['You'] = "You";
$Lang['ReadingGarden']['AlsoLikeThis'] = "like this.";
$Lang['ReadingGarden']['Comment'] = "Comment";
$Lang['ReadingGarden']['Settings'] = "Settings";
$Lang['ReadingGarden']['Home'] = "Home";
$Lang['ReadingGarden']['AccomplishRate'] = "Accomplish Rate";
$Lang['ReadingGarden']['BracketChinese'] = "(Chinese)";
$Lang['ReadingGarden']['BracketEnglish'] = "(English)";
$Lang['ReadingGarden']['BracketOthers'] = "(Others)";
$Lang['ReadingGarden']['Report'] = "Report";
$Lang['ReadingGarden']['Period'] = "Period";
$Lang['ReadingGarden']['To'] = "to";
$Lang['ReadingGarden']['BookInfo'] = "Book Information";
$Lang['ReadingGarden']['ReadingStatusAndReport'] = "Reading Status and Book Report";
$Lang['ReadingGarden']['ExistingBook'] = "Existing book";
$Lang['ReadingGarden']['ExternalBook'] = "External book";
$Lang['ReadingGarden']['BookSearchResult'] = "Search Result";
$Lang['ReadingGarden']['BookReportRequired'] = "Book Report Required";
$Lang['ReadingGarden']['AddOtherReadingRecord'] = "Add More Reading Record";
$Lang['ReadingGarden']['BackToMyReadingRecord'] = "Back To My Record";
$Lang['ReadingGarden']['DisplayAllCover'] = "Display All Covers";
$Lang['ReadingGarden']['HideAllCover'] = "Hide All Covers";

$Lang['ReadingGarden']['ClassExport'] = "Export (Class)";
$Lang['ReadingGarden']['SchoolExport'] = "Export (School)";
$Lang['ReadingGarden']['Title'] = "Title";
$Lang['ReadingGarden']['Message'] = "Message";
$Lang['ReadingGarden']['EndDate'] = "End Date";
$Lang['ReadingGarden']['DaysAgo'] = "days ago";
$Lang['ReadingGarden']['PostedBy'] = "Posted by";
$Lang['ReadingGarden']['ResultPublishDate'] = "Results announcement date";
$Lang['ReadingGarden']['StudentState'] = "Student Level";
$Lang['ReadingGarden']['MyLike'] = "MyLike";
$Lang['ReadingGarden']['Score'] = "Score";
$Lang['ReadingGarden']['Progress'] = "Progress";
$Lang['ReadingGarden']['Recommend'] = "Recommended";
$Lang['ReadingGarden']['Grade'] = "Grade";
$Lang['ReadingGarden']['Mark'] = "Mark";
$Lang['ReadingGarden']['TeacherComment'] = "Comment";
$Lang['ReadingGarden']['MarkReport'] = "Mark Report";
$Lang['ReadingGarden']['BackToMainPage'] = "Back To Main Page";
$Lang['ReadingGarden']['NoCurrentReadingTarget'] = "There is no current reading target.";
$Lang['ReadingGarden']['InCurrentTarget'] = "In Current Reading Target";
$Lang['ReadingGarden']['Teacher'] = "Teacher";
$Lang['ReadingGarden']['Student'] = "Student";
$Lang['ReadingGarden']['ConfirmArr']['DeleteTeacherReportComment'] = "Are you sure to delete this comment?";
$Lang['ReadingGarden']['ConfirmArr']['DeleteStudentReportComment'] = "Student will be given a level score deduction, are you sure to delete this comment?"; 
$Lang['ReadingGarden']['RecommendBookReport'] = "Recommended Book Report";

$Lang['ReadingGarden']['ReadingStatusArr'][2] = "Unread";
$Lang['ReadingGarden']['ReadingStatusArr'][1] = "Read";

$Lang['ReadingGarden']['ChineseReadingTarget'] = $Lang['ReadingGarden']['Book']." ".$Lang['ReadingGarden']['BracketChinese'];
$Lang['ReadingGarden']['ChineseReportTarget'] = $Lang['ReadingGarden']['Report']." ".$Lang['ReadingGarden']['BracketChinese'];
$Lang['ReadingGarden']['EnglishReadingTarget'] = $Lang['ReadingGarden']['Book']." ".$Lang['ReadingGarden']['BracketEnglish'];
$Lang['ReadingGarden']['EnglishReportTarget'] = $Lang['ReadingGarden']['Report']." ".$Lang['ReadingGarden']['BracketEnglish'];
$Lang['ReadingGarden']['OtherReadingTarget'] = $Lang['ReadingGarden']['Book']." ".$Lang['ReadingGarden']['BracketOthers'];

$Lang['ReadingGarden']['UnclassifiedCategory'] = "uncategorized";
$Lang['ReadingGarden']['AssignedClass']= "Assigned Class(es)";
$Lang['ReadingGarden']['UnassignClass']= "Unassign Classes";
$Lang['ReadingGarden']['DeleteReadingAssignment']= "Delete Assignment";
$Lang['ReadingGarden']['SelectTitleArr']['AllAssignReading'] = " -- All Assigned Readings -- ";
$Lang['ReadingGarden']['AssignReadingToClass']= "Assign Multiple Books";
$Lang['ReadingGarden']['SelectClass'] = "Select Class(es)";
$Lang['ReadingGarden']['SelectReadingAssignment'] = "Select Reading Assignement(s)";
$Lang['ReadingGarden']['SelectedClass'] = "Selected Class(es)";
$Lang['ReadingGarden']['AvailableReadingAssignement'] = "Available Reading Assignement";
$Lang['ReadingGarden']['AssignToClass'] = "Applicable Classes";
$Lang['ReadingGarden']['Confirmation'] = "Confirmation";
$Lang['ReadingGarden']['AssignReadingRemarks'] = "<span class='tabletextrequire'>*</span>Marked assignments will not be assigned to the the classes in this process.";
$Lang['ReadingGarden']['EachBookAssignOnce'] = "Each book can be assigned to a class once only.";

$Lang['ReadingGarden']['AwardTypeName'][1] = $Lang['ReadingGarden']['Individual'];
$Lang['ReadingGarden']['AwardTypeName'][2] = $Lang['ReadingGarden']['Group'];
$Lang['ReadingGarden']['AwardWinnerNotPublish'] = "Winners has not been announced yet.";
$Lang['ReadingGarden']['Announcement'] = "Announcement";
$Lang['ReadingGarden']['All'] = "All";
$Lang['ReadingGarden']['GenerateWinner'] = "Generate Winners";
$Lang['ReadingGarden']['AssignWinner'] = "Assign as Winner";
$Lang['ReadingGarden']['UnassignWinner'] = "Unassign as Winner";
$Lang['ReadingGarden']['WinnerList'] = "Winner List";

$Lang['ReadingGarden']['BookRead'] = "Books Read";
$Lang['ReadingGarden']['ReportSubmit'] = "Reports Submitted";
$Lang['ReadingGarden']['IsWinner'] = "Is Winner";

$Lang['ReadingGarden']['InWinnerList'] = "In winner list";
$Lang['ReadingGarden']['NotInWinnerList'] = "Not in winner list";

$Lang['ReadingGarden']['NoClassesMeetTheRequirement'] = "No class meet the requirement(s).";
$Lang['ReadingGarden']['NoStudentsMeetTheRequirement'] = "No student meet the requirement(s).";

$Lang['ReadingGarden']['BookSourceInput'][1] = "By Admin";
$Lang['ReadingGarden']['BookSourceInput'][2] = "From eLibrary";
$Lang['ReadingGarden']['BookSourceInput'][3] = "By Student";

$Lang['ReadingGarden']['BookSourceInput'][1] = "By Admin";
$Lang['ReadingGarden']['BookSourceInput'][2] = "From eLibrary";
$Lang['ReadingGarden']['BookSourceInput'][3] = "By Student";

$Lang['ReadingGarden']['ViewBook'] = "View Book";
$Lang['ReadingGarden']['ViewState'] = "View State Requirement";
$Lang['ReadingGarden']['StateRequirement'] = "State Requirement";
$Lang['ReadingGarden']['StateList'] = "State List";
$Lang['ReadingGarden']['ScoringRule'] = "Scoring Rule";
$Lang['ReadingGarden']['FileUpload'] = "File Upload";
$Lang['ReadingGarden']['OnlineWriting'] = "Online Writing";
$Lang['ReadingGarden']['WordLimit'] = "Word Limit";
$Lang['ReadingGarden']['WordCount'] = "Word Count";
$Lang['ReadingGarden']['Unlimit'] = "Unlimit";

# General Wordings
$Lang['ReadingGarden']['CreateDate'] = "Date Created";
$Lang['ReadingGarden']['LastModifiedDate'] = "Last Modified";
$Lang['ReadingGarden']['CreatedBy'] = "Created by";
$Lang['ReadingGarden']['LastModifiedBy'] = "Last Modified by";
$Lang['ReadingGarden']['Group'] = "Group";
$Lang['ReadingGarden']['Individual'] = "Individual";
$Lang['ReadingGarden']['Inter-Class'] = "Class";
$Lang['ReadingGarden']['SortBy'] = "Sort by";


//Left Menu Title
$Lang['ReadingGarden']['ManagementArr']['MenuTitle'] = "Management";
$Lang['ReadingGarden']['AnnouncementMgmt']['MenuTitle'] = "Announcement";
$Lang['ReadingGarden']['ForumMgmt']['MenuTitle'] = "Forum";
$Lang['ReadingGarden']['ReportsArr']['MenuTitle'] = "Reports";
$Lang['ReadingGarden']['StatisticArr']['MenuTitle'] = "Statistic";
$Lang['ReadingGarden']['SettingsArr']['MenuTitle'] = "Settings";

//Return Message
$Lang['ReadingGarden']['ReturnMessage']['SettingsSaveSuccess'] = "1|=|Settings Saved";
$Lang['ReadingGarden']['ReturnMessage']['SettingsSaveFail'] = "0|=|Settings Save Failed";
$Lang['ReadingGarden']['ReturnMessage']['AssignToClassSuccess']= "1|=|Readings are assigned to classes successfully.";
$Lang['ReadingGarden']['ReturnMessage']['AssignToClassFail']= "0|=|Failed to assign reading to classes.";
$Lang['ReadingGarden']['ReturnMessage']['UnassignClassSuccess']= "1|=|Reading assignment are unassigned successfully.";
$Lang['ReadingGarden']['ReturnMessage']['UnassignClassFail']= "0|=|Failed to unassign reading assignment.";

//Warning Message
$Lang['ReadingGarden']['WarningMsg']['PleaseInputANumber'] = "Please input a number.";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBookSource'] = "Please select a source of book.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputCategoryName'] = "Please input category code.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputCategoryCode'] = "Please input category name.";
$Lang['ReadingGarden']['WarningMsg']['CategoryCodeExist'] = "Category code is being used by other category.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAssignReadingName'] = "Please input reading assignment name.";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBook']= "Please select a book.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputStateName'] = "Please input level name";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputScoreRequired'] = "Please input Score Range";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAnInteger'] = "Please input an integer";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectAnImage']= "Please select an image.";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectReadingStatus'] = "Please select reading status.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAuthor'] = "Please input an author";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputPublisher'] = "Please input a publisher";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectBookCover'] = "Please select a book cover";
$Lang['ReadingGarden']['WarningMsg']['PleaseComposeAsnwerSheet'] = "Please compose asnwer sheet.";
$Lang['ReadingGarden']['WarningMsg']['ScoreRequiredExist'] = "Score Range exists.";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectClass']= "Please select class(es).";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectForm']= "Please select Form(s).";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectCategory']= "Please select Category(s).";
$Lang['ReadingGarden']['WarningMsg']['PleaseSelectReadingAssignment']= "Please select one or more reading assignment(s).";
$Lang['ReadingGarden']['WarningMsg']['AssignmentAssigned'] = "This assignment has been assigned to this class.";
$Lang['ReadingGarden']['WarningMsg']['BookAssigned'] = "This book has been assigned to this class.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputMessage'] = "Please input message.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputAnnouncementTitle'] = "Please input announcement title.";
$Lang['ReadingGarden']['WarningMsg']['InvalidPublishDate'] = "Results announcement date must not be earlier than end date.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputWriting'] = "Please input writing.";
$Lang['ReadingGarden']['WarningMsg']['WordCountExceedLimit'] = "Out of word limit.";

// Select Title 
$Lang['ReadingGarden']['SelectTitleArr']['AllCategory'] = " -- All Categories -- ";
$Lang['ReadingGarden']['SelectTitleArr']['SelectAssignReading'] = " -- Select Reading Assignment -- ";
$Lang['ReadingGarden']['SelectTitleArr']['NewAssignReading'] = " -- New Reading Assignment -- ";
$Lang['ReadingGarden']['SelectTitleArr']['Status'] = "Select Status";
$Lang['ReadingGarden']['SelectTitleArr']['BookSource'] = "Select Book Source";
$Lang['ReadingGarden']['SelectTitleArr']['AllReadingStatus'] = " -- All Reading Status -- ";

//Confirm Message
$Lang['ReadingGarden']['ConfirmArr']['DeleteCategory'] = "Are you sure you want to delete this category?";
$Lang['ReadingGarden']['ConfirmArr']['DeleteCategoryWithBook'] = "All books under this category will be classified as \"Uncategorized\", are you sure you want to delete this category?";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromClassWithReadingRecord'] = "All reading records related to this reading assignment will be deleted from this class, proceed?";
$Lang['ReadingGarden']['ConfirmArr']['AssignedReadingOfAllClassWillBeUpdate'] = "The reading assignment of other classes will also be modified, proceed?";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromClass']= "Are you sure you want to unassign ALL classes? This action will not remove the book entry.";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReadingFromAllClass']= "Are you sure to delete reading assignment from ALL classes?";
$Lang['ReadingGarden']['ConfirmArr']['DeleteAssignedReading']= "Are you sure to delete the reading assignment(s)?";

$Lang['ReadingGarden']['ConfirmArr']['UnassgnWinner'] = "Are you sure to unassgin selected winner?";
$Lang['ReadingGarden']['ConfirmArr']['WinnerListWasGenerated'] = "Winner list generated will be removed, continue?";

$Lang['ReadingGarden']['Indication']['Normal'] = "Normal";
$Lang['ReadingGarden']['Indication']['InProgress'] = "In Progress";

//ScoringSettings
$Lang['ReadingGarden']['ScoringSettings']['MenuTitle'] = "Scoring Rules";
$Lang['ReadingGarden']['ScoringSettings']['For'] = "for";
$Lang['ReadingGarden']['ScoringSettings']['Every'] = "every";
$Lang['ReadingGarden']['ScoringSettings']['Words'] = "words";
$Lang['ReadingGarden']['ScoringSettings']['Times'] = "time(s)";
$Lang['ReadingGarden']['ScoringSettings']['Time'] = "time";
$Lang['ReadingGarden']['ScoringSettings']['MoreThan'] = "more than";
$Lang['ReadingGarden']['ScoringSettings']['MinsWithin24Hours'] = "mins within 24 hours";
$Lang['ReadingGarden']['ScoringSettings']['ScoringRule'] = "Scoring Rule";
$Lang['ReadingGarden']['ScoringSettings']['PressLikeButton'] = "Like";
$Lang['ReadingGarden']['ScoringSettings']['WriteComment'] = "Write a comment";
$Lang['ReadingGarden']['ScoringSettings']['StartAThreadInForum'] = "Start a thread in forum";
$Lang['ReadingGarden']['ScoringSettings']['FollowAThreadInForum'] = "Follow a thread in forum";
$Lang['ReadingGarden']['ScoringSettings']['AddReadingRecord'] = "Add a reading record";
$Lang['ReadingGarden']['ScoringSettings']['WriteBookReport'] = "Write a book report";
$Lang['ReadingGarden']['ScoringSettings']['TeacherRecommendReport'] = "Book report recommended by teacher";
$Lang['ReadingGarden']['ScoringSettings']['CommentMoreThan'] = "Comment more than";
$Lang['ReadingGarden']['ScoringSettings']['CommentLength'] = "Comment length";
$Lang['ReadingGarden']['ScoringSettings']['Login'] = "Login";

$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDaysWithoutLogin'] = "Consecutive days without login";
$Lang['ReadingGarden']['ScoringSettings']['SuccessiveDays'] = "consecutive days"; 
$Lang['ReadingGarden']['ScoringSettings']['CommentDeletedByAdmin'] = "Comment deleted by admin";
$Lang['ReadingGarden']['ScoringSettings']['ForumThreadDeletedByAdmin'] = "Forum thread deleted by admin";
$Lang['ReadingGarden']['ScoringSettings']['ForumPostDeletedByAdmin'] = "Forum post deleted by admin";

$Lang['ReadingGarden']['ScoringSettings']['InputZeroToDisableRule'] = "Input '0' or leave the field blank to disable the rule.";

//BookSourceSettings
$Lang['ReadingGarden']['BookSourceSettings']['MenuTitle'] = "Source of Book";
$Lang['ReadingGarden']['BookSourceSettings']['BookSource'][1] = "Recommendeded books only";
$Lang['ReadingGarden']['BookSourceSettings']['BookSource'][2] = "All books in book list";
$Lang['ReadingGarden']['BookSourceSettings']['BookSource'][3] = "Any books include student-inputted books)";
$Lang['ReadingGarden']['BookSourceSettings']['PossibleBookSource'] = "Can Write Report for";

//Book Report Setting
$Lang['ReadingGarden']['BookReportSettings']['MenuTitle'] = "Book Report Setting";

//Book Category
$Lang['ReadingGarden']['BookCategorySettings']['MenuTitle'] = "Book Category";
$Lang['ReadingGarden']['BookCategorySettings']['Language'] = "Language";
$Lang['ReadingGarden']['BookCategorySettings']['Chinese'] = "Chinese";
$Lang['ReadingGarden']['BookCategorySettings']['English'] = "English";
$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][1] =  "Chinese";
$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][2] =  "English";
$Lang['ReadingGarden']['BookCategorySettings']['LangArr'][3] =  "Others";
$Lang['ReadingGarden']['BookCategorySettings']['CategoryCode'] = "Category Code";
$Lang['ReadingGarden']['BookCategorySettings']['CategoryName'] = "Category Name";
$Lang['ReadingGarden']['BookCategorySettings']['AddCategory'] = "Add Category";
$Lang['ReadingGarden']['BookCategorySettings']['EditCategory'] = "Edit Category";
$Lang['ReadingGarden']['BookCategorySettings']['DeleteCategory'] = "Delete Category";
$Lang['ReadingGarden']['BookCategorySettings']['MoveCategory'] = "Move Category";
$Lang['ReadingGarden']['BookCategorySettings']['NumOfBooks'] = "Number of Books";
$Lang['ReadingGarden']['BookCategorySettings']['Category'] = "Category";
$Lang['ReadingGarden']['BookCategorySettings']['Uncategorized'] = "Uncategorized";

//RecommendBookSettings
$Lang['ReadingGarden']['RecommendBookSettings']['MenuTitle'] = "Recommended Book";
$Lang['ReadingGarden']['RecommendBookSettings']['AddRecommendBook'] = "Add Recommended Book";
$Lang['ReadingGarden']['RecommendBookSettings']['SelectBook'] = "Select book(s)";
$Lang['ReadingGarden']['RecommendBookSettings']['BookSelected'] = "Book(s) selected";
$Lang['ReadingGarden']['RecommendBookSettings']['From'] = "From";
$Lang['ReadingGarden']['RecommendBookSettings']['EnterDetail'] = "Enter \"[Book Title] [Author] [Publisher]\"";
$Lang['ReadingGarden']['RecommendBookSettings']['EnterCallNumber'] = "Enter [Call Number]";

//AssignReadingSettings
$Lang['ReadingGarden']['AssignReadingSettings']['MenuTitle'] = "Assigned Reading"; //menu name//
$Lang['ReadingGarden']['AssignReadingSettings']['NewAssignReading'] = "New Assign Reading"; //add button//
$Lang['ReadingGarden']['AssignReadingSettings']['EditAssignReading'] = "Edit Assign Reading";
$Lang['ReadingGarden']['AssignReadingSettings']['AssignReadingName'] = "Assignment Name";
$Lang['ReadingGarden']['AssignReadingSettings']['AssignReading'] = "Assign Reading";
$Lang['ReadingGarden']['AssignReadingSettings']['ManageAssignment'] = "Manage Assignment";
$Lang['ReadingGarden']['AssignReadingSettings']['NumberOfReadingRecord'] = "Submitted Reading Record";
$Lang['ReadingGarden']['AssignReadingSettings']['Assignment'] = "Assignment";
$Lang['ReadingGarden']['AssignReadingSettings']['MoveReadingAssignment'] = "Move Reading Assignment";
$Lang['ReadingGarden']['AssignReadingSettings']['UseAnswerSheet'] = "Use";
$Lang['ReadingGarden']['AssignReadingSettings']['UseBookReport'] = "Use";
$Lang['ReadingGarden']['AssignReadingSettings']['FileUpload'] = "File Upload";
$Lang['ReadingGarden']['AssignReadingSettings']['OnlineWriting'] = "Online Writing";
$Lang['ReadingGarden']['AssignReadingSettings']['WordLimit'] = "Word Limit";



//StateSettings
$Lang['ReadingGarden']['StateSettings']['MenuTitle'] = "Level";
$Lang['ReadingGarden']['StateSettings']['StateName'] = "Level Name";
$Lang['ReadingGarden']['StateSettings']['Description'] = "Description";
$Lang['ReadingGarden']['StateSettings']['ScoreRequired'] = "Score Required"; // 20110328
$Lang['ReadingGarden']['StateSettings']['ScoreRange'] = "Score Range";// 20110328
$Lang['ReadingGarden']['StateSettings']['Logo'] = "Logo";
$Lang['ReadingGarden']['StateSettings']['New'] = "New";
$Lang['ReadingGarden']['StateSettings']['NewState'] = "New Level";
$Lang['ReadingGarden']['StateSettings']['EditState'] = "Edit Level";
$Lang['ReadingGarden']['StateSettings']['InputZeroForDefaultState'] = "Input '0' for default level.";

# Book
$Lang['ReadingGarden']['BookSettings']['MenuTitle'] = "Book List";
$Lang['ReadingGarden']['BookSettings']['AllBooks'] = "All Books";


# Books Wordings
$Lang['ReadingGarden']['CallNumber'] = "Call Number";
$Lang['ReadingGarden']['BookName'] = "Book Title";
$Lang['ReadingGarden']['Author'] = "Author";
$Lang['ReadingGarden']['Publisher'] = "Publisher";
$Lang['ReadingGarden']['Description'] = "Description";
$Lang['ReadingGarden']['CoverImage'] = "Cover Image";
$Lang['ReadingGarden']['CategoryName'] = "Category Name";
$Lang['ReadingGarden']['Language'] = "Language";
$Lang['ReadingGarden']['AddBook'] = "Add Book";
$Lang['ReadingGarden']['EditBook'] = "Edit Book";
$Lang['ReadingGarden']['English'] = "English";
$Lang['ReadingGarden']['Chinese'] = "Chinese";
$Lang['ReadingGarden']['Either'] = "Either";
$Lang['ReadingGarden']['Category'] = "Category";
$Lang['ReadingGarden']['SubCategory'] = "Sub-Category";
$Lang['ReadingGarden']['Source'] = "Source";
$Lang['ReadingGarden']['BookReport'] = "Book Report";
$Lang['ReadingGarden']['AssignToForm'] = "Assign To Form";
$Lang['ReadingGarden']['ImportFromELIBRARY'] = "Import from ".$Lang['Header']['Menu']['eLibrary'];
$Lang['ReadingGarden']['FromELIBRARY'] = "Book (from ".$Lang['Header']['Menu']['eLibrary'].")";
$Lang['ReadingGarden']['ViewBookDetail'] = "View book detail";

# Answer Sheet Template Wordings
$Lang['ReadingGarden']['AnswerSheetTemplateSettings']['MenuTitle'] = "Answer Sheet Template";
$Lang['ReadingGarden']['AnswerSheet'] = "Answer Sheet";
$Lang['ReadingGarden']['AnswerSheetTemplate'] = "Answer Sheet Template";
$Lang['ReadingGarden']['TemplateName'] = "Template Name";
$Lang['ReadingGarden']['NewAnswerSheetTemplate'] = "New Answer Sheet Template";

# Award Scheme Wordings
$Lang['ReadingGarden']['AwardScheme'] = "Award Scheme";
$Lang['ReadingGarden']['Award'] = "Award";
$Lang['ReadingGarden']['AwardName'] = "Award Name";
$Lang['ReadingGarden']['Type'] = "Type";
$Lang['ReadingGarden']['TargetDescription'] = "Target (For display in student screen. E.g. \"F1-F3\"";
$Lang['ReadingGarden']['TargetClass'] = "Target Class(es)";
$Lang['ReadingGarden']['MinBookRequired'] = "No. of books to complete";
$Lang['ReadingGarden']['MinBookRequiredInSpecificCategory'] = "Minimum no. of books required in specific categories";
$Lang['ReadingGarden']['MinReportRequired'] = "No. of reports to complete";
$Lang['ReadingGarden']['Period'] = "Period";
$Lang['ReadingGarden']['Winner'] = "Winner";
$Lang['ReadingGarden']['Requirement'] = "Requirement(s)";
$Lang['ReadingGarden']['AddAward'] = "Add Award";
$Lang['ReadingGarden']['AwardTypeName'][1] = $Lang['ReadingGarden']['Individual'];
$Lang['ReadingGarden']['AwardTypeName'][2] = $Lang['ReadingGarden']['Group'];
$Lang['ReadingGarden']['Level'] = "Level";
$Lang['ReadingGarden']['AwardLevel'] = "Award Level";
$Lang['ReadingGarden']['LevelName'] = "Level Name";
$Lang['ReadingGarden']['Detail'] = "Detail";

# Reading Target Wordings
$Lang['ReadingGarden']['ReadingTarget'] = "Reading Target";
$Lang['ReadingGarden']['NewReadingTarget'] = "Add Reading Target";
$Lang['ReadingGarden']['EditReadingTarget'] = "Edit Reading Target";
$Lang['ReadingGarden']['ReadingTargetName'] = "Target Name";
$Lang['ReadingGarden']['NumberOfReadingRequired'] = "No. of Reading Required";
$Lang['ReadingGarden']['NumberOfBookReportRequired'] = "No. of Book Report Required";

# Recommended Book List Wordings
$Lang['ReadingGarden']['RecommendBookList'] = "Recommended Book List";
$Lang['ReadingGarden']['RecommendBook'] = "Recommended Book";
$Lang['ReadingGarden']['RecommendBookReport'] = "Recommended Book Report";
$Lang['ReadingGarden']['Import_Book_Column'] = array("Book Title","Call Number","ISBN","Author","Publisher","Language","Category","Description","Assign To Form","Answer Sheet");
$Lang['SysMgr']['ReadingGarden']['Button']['ImportBook'] ="Import Book";
$Lang['ReadingGarden']['Import_Error']['BookTitleIsMissing']= "Book Title is missing";
$Lang['ReadingGarden']['Import_Error']['InvalidLanguage'] = "Invalid Language";
$Lang['ReadingGarden']['Import_Error']['LanguageIsMissing'] = "Language is missing";
$Lang['ReadingGarden']['Import_Error']['InvalidISBN'] = "Invalid ISBN";
$Lang['ReadingGarden']['Import_Error']['InvalidCallNumber'] = "Invalid Call Number";
$Lang['ReadingGarden']['Import_Error']['InvalidCategory'] = "Invalid Category";
$Lang['ReadingGarden']['Import_Error']['CategoryIsMissing'] = "Category is missing";
$Lang['ReadingGarden']['Import_Error']['InvalidForm'] = "Invalid Form";
$Lang['ReadingGarden']['Import_Error']['InvalidAnswerSheet'] = "Invalid AnswerSheet";
$Lang['SysMgr']['ReadingGarden']['ImportBook']['CategoryRemarks']= "Please base on \"Language\" to choose \"Category\", if there is no \"Category\", please fill in \"Uncategorized\"";

# Reading Record Wordings
$Lang['ReadingGarden']['MyRecord'] = "My Record";
$Lang['ReadingGarden']['ReadingRecord'] = "Reading Record";
$Lang['ReadingGarden']['AddReadingRecord'] = "Add a reading record";
$Lang['ReadingGarden']['EditReadingRecord'] = "Edit Reading Record";
$Lang['ReadingGarden']['DeleteReadingRecord'] = "Delete Reading Record";
$Lang['ReadingGarden']['StartDate'] = "Start Date";
$Lang['ReadingGarden']['FinishedDate'] = "Finished Date";
$Lang['ReadingGarden']['ReadingHours'] = "Reading Hours";
$Lang['ReadingGarden']['Hour(s)'] = "Hour(s)";
$Lang['ReadingGarden']['Progress'] = "Progress";
$Lang['ReadingGarden']['Readed'] = "Read";

# Book Report Wordings
$Lang['ReadingGarden']['HandInBookReport'] = "Upload File";
$Lang['ReadingGarden']['DoAnswerSheet'] = "Do Answer Sheet";
$Lang['ReadingGarden']['EditAnswerSheet'] = "Edit Answer Sheet";
$Lang['ReadingGarden']['DeleteAnswerSheet'] = "Delete Answer Sheet";
$Lang['ReadingGarden']['ViewAnswerSheet'] = "View Answer Sheet";
$Lang['ReadingGarden']['BookReport'] = "Book Report";
$Lang['ReadingGarden']['ViewBookReport'] = "View Book Report";
$Lang['ReadingGarden']['DeleteBookReport'] = "Delete Book Report";
$Lang['ReadingGarden']['Grade'] = "Grade";
$Lang['ReadingGarden']['RecommendThisBookReport'] = "Recommend this book report";
$Lang['ReadingGarden']['Recommended'] = "Recommended";
$Lang['ReadingGarden']['MarkingArea'] = "Marking Area";
$Lang['ReadingGarden']['Mark(s)'] = "Mark(s)";
$Lang['ReadingGarden']['Answer'] = "Answer";
$Lang['ReadingGarden']['EditOnlineWriting'] = "Edit Online Writing";
$Lang['ReadingGarden']['DeleteOnlineWriting'] = "Delete Online Writing";
$Lang['ReadingGarden']['PublicLink'] = "Public link";

# Forum Wordings
$Lang['ReadingGarden']['Forum'] = "Forum";
$Lang['ReadingGarden']['ForumName'] = "Forum Name";
$Lang['ReadingGarden']['MoveForum'] = "Move Forum";
$Lang['ReadingGarden']['StudentCreateTopicRequireApprove'] = "Student Replies Required Approval";
$Lang['ReadingGarden']['Topic'] = "Topic";
$Lang['ReadingGarden']['NewTopic'] = "New Topic";
$Lang['ReadingGarden']['Post'] = "Post";
$Lang['ReadingGarden']['NewPost'] = "New Post";
$Lang['ReadingGarden']['Reply'] = "Reply";
//$Lang['ReadingGarden']['Quote'] = "Quote";
$Lang['ReadingGarden']['Topic(s)'] = "Topic(s)";
$Lang['ReadingGarden']['Post(s)'] = "Post(s)";
$Lang['ReadingGarden']['LastTopicOn'] = "Last topic on";
$Lang['ReadingGarden']['LastPostOn'] = "Last post on";
$Lang['ReadingGarden']['Approved'] = "Approved";
$Lang['ReadingGarden']['WaitingForApproval'] = "Waiting for approval";
$Lang['ReadingGarden']['Rejected'] = "Rejected";
$Lang['ReadingGarden']['Approve'] = "Approve";
$Lang['ReadingGarden']['Reject'] = "Reject";
$Lang['ReadingGarden']['MarkAsOnTop'] = "Mark as on top";
$Lang['ReadingGarden']['RemoveOnTop'] = "Remove on top";
$Lang['ReadingGarden']['NumberOfReply'] = "No. of Reply";
$Lang['ReadingGarden']['LastReplyBy'] = "Last Reply By";
$Lang['ReadingGarden']['LastPostTime'] = "Last Post Time";
$Lang['ReadingGarden']['Content'] = "Content";
$Lang['ReadingGarden']['PostSubject'] = "Post Subject";
$Lang['ReadingGarden']['ThereIsNoReplyAtTheMoment'] = "There is no reply at the moment.";
$Lang['ReadingGarden']['Reply:'] = "RE: ";
$Lang['ReadingGarden']['PostedOn'] = "Posted on";
$Lang['ReadingGarden']['GoToTop'] = "Go to top";
$Lang['ReadingGarden']['PreviousTopic'] = "Previous Topic";
$Lang['ReadingGarden']['NextTopic'] = "Next Topic";

# Comment bank
$Lang['ReadingGarden']['CommentBank'] = "Comment bank";
$Lang['ReadingGarden']['AddComment'] = "Add comment";

# Report Sticker
$Lang['ReadingGarden']['Sticker'] = "Sticker";
$Lang['ReadingGarden']['UploadStickerImage'] = "Upload sticker image";
$Lang['ReadingGarden']['UploadNewStickerImage'] = "Upload new sticker image";
$Lang['ReadingGarden']['CurrentStickerImage'] = "Current sticker image";
$Lang['ReadingGarden']['DescribeStickerDimension'] = "Best dimension W:120px x H:120px";
$Lang['ReadingGarden']['GiveSticker'] = "Give sticker";
$Lang['ReadingGarden']['ClickToRemoveSticker'] = "Click to remove sticker";

# Warning Msg
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBook'] = "Are you sure you want to delete this book?";
$Lang['ReadingGarden']['WarningMsg']['BooksHaveImportedFromeLibrary'] = "Books may have been imported from ".$Lang['Header']['Menu']['eLibrary'].".";
$Lang['ReadingGarden']['WarningMsg']['InvalidCoverImage'] = "The file you want to upload is not a valid image file.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputBookCallNumber'] = "Please input call number.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputBookName'] = "Please input book title.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputISBN'] = "Please input ISBN.";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteCoverImage'] = "Are you sure you want to delete this cover image?";
$Lang['ReadingGarden']['WarningMsg']['AnswerSheetTemplateNameExist'] = "Answer sheet template name already exist.";
$Lang['ReadingGarden']['WarningMsg']['BlankAnswerSheetTemplateName'] = "Please input answer sheet template name.";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBookAnswerSheet'] = "Are you sure you want to delete the answer sheet assigned to this book?";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteAwardScheme'] = "Are you sure you want to delete this award?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputAwardName'] = "Please input award name.";
$Lang['ReadingGarden']['WarningMsg']['AwardNameExist'] = "Award name already exist.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveInteger'] = "Please input a positive integer.";
$Lang['ReadingGarden']['WarningMsg']['InvalidDateFormat'] = "Invalid date format.";
$Lang['ReadingGarden']['WarningMsg']['InvalidDateRange'] = "Invalid date range.";
$Lang['ReadingGarden']['WarningMsg']['ClassAlreadyAdded'] = "Class already added.";
$Lang['ReadingGarden']['WarningMsg']['CategoryAlreadyAdded'] = "Category already added.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputReadingTargetName'] = "Please input target name.";
$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneClasses'] = "Please add at least one class.";
$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastTwoClasses'] = "Please add at least two target classes.";
$Lang['ReadingGarden']['WarningMsg']['ReadingTargetNameExist'] = "Reading target name already exist.";
$Lang['ReadingGarden']['WarningMsg']['HaveOverlappingPeriods'] = "has/have overlapping periods with other target(s).";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteReadingTarget'] = "Are you sure you want to delete this target?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumber'] = "Please input a positive number.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputPositiveNumberWithin100'] = "Please input a positive number smaller or equal to 100.";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteReadingRecord'] = "Associated book report and answer sheet will be deleted as well. Are you sure you want to delete this reading record? ";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteBookReport'] = "Are you sure you want to delete this book report?";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteAnswerSheet'] = "Are you sure you want to delete this answer sheet?";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteOnlineWriting'] = "Are you sure you want to delete this online writing?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputForumName'] = "Please input forum name.";
$Lang['ReadingGarden']['WarningMsg']['ForumNameExist'] = "Forum name already exist.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputTopic'] = "Please input topic.";
$Lang['ReadingGarden']['WarningMsg']['PleaseInputContent'] = "Please input content";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeleteTopic'] = "Are you sure you want to delete these topic(s)?";
$Lang['ReadingGarden']['WarningMsg']['NewTopicNeedApprove'] = "New topic need to be approved by administrator/teacher to become active.";
//$Lang['ReadingGarden']['WarningMsg']['NewPostNeedApprove'] = "New post need to be approved by administrator/teacher to become active.";
$Lang['ReadingGarden']['WarningMsg']['ConfirmDeletePost'] = "Are you sure you want to delete this post?";
$Lang['ReadingGarden']['WarningMsg']['RequestInputStartDateEarlierThanEndDate'] = "Please input a start date earlier than the end date.";
$Lang['ReadingGarden']['WarningMsg']['StartDateGreaterThanToday'] = "Start date cannot be after than today.";
$Lang['ReadingGarden']['WarningMsg']['EndDateGreaterThanToday'] = "End date cannot be after than today.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputLevelName'] = "Please input level name.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputComment'] = "Please input comment.";
$Lang['ReadingGarden']['WarningMsg']['RequestInputStickerImage'] = "Please upload sticker image.";
$Lang['ReadingGarden']['WarningMsg']['RequestCompleteAnswerSheetAllAnswerScore'] = "Please complete all answer(s) and score(s).";

$Lang['ReadingGarden']['WarningMsg']['InvalidRangeOfWordLimit'] = "Invalid range of word limit.";
$Lang['ReadingGarden']['WarningMsg']['RequestAddAtLeastOneAwardLevel'] = "Please add at least one award level.";

#Return Msg
$Lang['ReadingGarden']['ReturnMsg']['AddBookSuccess'] = "1|=|Book has been added successfully.";
$Lang['ReadingGarden']['ReturnMsg']['AddBookFail'] = "0|=|Failed to add book.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookSuccess'] = "1|=|Book has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookFail'] = "0|=|Failed to delete book.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookSuccess'] = "1|=|Book has been updated successfully.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookFail'] = "0|=|Failed to update book.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteCoverImageSuccess'] = "1|=|Book cover image has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteCoverImageFail'] = "0|=|Failed to delete book cover image.";
$Lang['ReadingGarden']['ReturnMsg']['BookImportFromeLibrarySuccess'] = "1|=|Books have been imported successfully.";
$Lang['ReadingGarden']['ReturnMsg']['BookImportFromeLibraryFail'] = "0|=|Failed to import books from eLibrary.";
$Lang['ReadingGarden']['ReturnMsg']['RemoveBookAnswerSheetSuccess'] = "1|=|Answer sheet has been removed successfully.";
$Lang['ReadingGarden']['ReturnMsg']['RemoveBookAnswerSheetFail'] = "1|=|Failed to remove answer sheet.";

$Lang['ReadingGarden']['ReturnMsg']['AddAnswerSheetTemplateSuccess'] = "1|=|Answer sheet template has been added successfully.";
$Lang['ReadingGarden']['ReturnMsg']['AddAnswerSheetTemplateFail'] = "0|=|Failed to add answer sheet template.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAnswerSheetTemplateSuccess'] = "1|=|Answer sheet template has been updated successfully.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAnswerSheetTemplateFail'] = "0|=|Failed to update answer sheet template.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetTemplateSuccess'] = "1|=|Answer sheet template has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetTemplateFail'] = "0|=|Failed to delete answer sheet template.";

$Lang['ReadingGarden']['ReturnMsg']['AddAwardSchemeSuccess'] = "1|=|Award has been added successfully.";
$Lang['ReadingGarden']['ReturnMsg']['AddAwardSchemeFail'] = "0|=|Failed to add award.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAwardSchemeSuccess'] = "1|=|Award has been updated successfully.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateAwardSchemeFail'] = "0|=|Failed to update award.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAwardSchemeSuccess'] = "1|=|Award has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAwardSchemeFail'] = "0|=|Failed to delete award.";

$Lang['ReadingGarden']['ReturnMsg']['AddReadingTargetSuccess'] = "1|=|Reading target has been added successfully.";
$Lang['ReadingGarden']['ReturnMsg']['AddReadingTargetFail'] = "0|=|Failed to add reading target.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingTargetSuccess'] = "1|=|Reading target has been updated successfully.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingTargetFail'] = "0|=|Failed to update reading target.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingTargetSuccess'] = "1|=|Reading target has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingTargetFail'] = "0|=|Failed to delete reading target.";

$Lang['ReadingGarden']['ReturnMsg']['AddReadingRecordSuccess'] = "1|=|Reading record has been added successfully.";
$Lang['ReadingGarden']['ReturnMsg']['AddReadingRecordFail'] = "0|=|Failed to Add a reading record.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingRecordSuccess'] = "1|=|Reading record has been updated successfully.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateReadingRecordFail'] = "0|=|Failed to update reading record.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingRecordSuccess'] = "1|=|Reading record has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteReadingRecordFail'] = "0|=|Failed to delete reading record.";

$Lang['ReadingGarden']['ReturnMsg']['AddBookReportSuccess'] = "1|=|Book report has been added successfully.";
$Lang['ReadingGarden']['ReturnMsg']['AddBookReportFail'] = "0|=|Failed to add book report.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookReportSuccess'] = "1|=|Book report has been updated successfully.";
$Lang['ReadingGarden']['ReturnMsg']['UpdateBookReportFail'] = "0|=|Failed to update book report.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookReportSuccess'] = "1|=|Book report has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteBookReportFail'] = "0|=|Failed to delete book report.";

$Lang['ReadingGarden']['ReturnMsg']['SubmitAnswerSheetSuccess'] = "1|=|Answer sheet has been submitted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['SubmitAnswerSheetFail'] = "0|=|Failed to submit answer sheet.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetSuccess'] = "1|=|Answer sheet has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteAnswerSheetFail'] = "0|=|Failed to delete answer sheet.";

$Lang['ReadingGarden']['ReturnMsg']['SubmitOnlineWritingSuccess'] = "1|=|Online writing has been submitted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['SubmitOnlineWritingFail'] = "0|=|Failed to submit online writing.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteOnlineWritingSuccess'] = "1|=|Online writing has been deleted successfully.";
$Lang['ReadingGarden']['ReturnMsg']['DeleteOnlineWritingFail'] = "0|=|Failed to delete online writing.";

$Lang['ReadingGarden']['ReturnMsg']['AssignSuccess'] = "1|=|Selected student(s) have been assigned as winner(s) successfully.";
$Lang['ReadingGarden']['ReturnMsg']['AssignFail'] = "0|=|Failed to assgin selected student(s) as winnier(s)";
$Lang['ReadingGarden']['ReturnMsg']['UnassignSuccess'] = "1|=|Unassgined selected winner(s) successfully.";
$Lang['ReadingGarden']['ReturnMsg']['UnassignFail'] = "0|=|Failed to unassgin selected winner(s).";

//20110328
$Lang['ReadingGarden']['VieweBook'] = "View eBook";

$Lang['ReadingGarden']['DisplayItem'] = "Display Item";
$Lang['ReadingGarden']['Row'] = "Row";
$Lang['ReadingGarden']['Column'] = "Column";
$Lang['ReadingGarden']['Month'] = "Month";
$Lang['ReadingGarden']['ISBN'] = "ISBN";
$Lang['ReadingGarden']['DisplayBookReport'] = "Display Book Reports";
$Lang['ReadingGarden']['DisplayNoOfMarkedReport'] = "Display Number Of Marked Reports";
$Lang['ReadingGarden']['SeparateByLang'] = "Separate By Language";
$Lang['ReadingGarden']['Ch'] = "Ch";
$Lang['ReadingGarden']['En'] = "En";
$Lang['ReadingGarden']['Other'] = "Others";
$Lang['ReadingGarden']['MarkedReport'] = "Marked Report";
$Lang['ReadingGarden']['Total'] = "Total";

$Lang['ReadingGarden']['AllowAttachment'] = "Allow Attachment";
$Lang['ReadingGarden']['ExtraSetting'] = "Extra Setting";

$Lang['ReadingGarden']['RequireStudentToSelectAwardScheme'] = "Require student to select award scheme";
$Lang['ReadingGarden']['Enable'] = "Enable";
$Lang['ReadingGarden']['Disable'] = "Disable";
$Lang['ReadingGarden']['ReportSettings'] = "Report Options";

$Lang['ReadingGarden']['AdditionalAward'] = "Additional Prizes (Non-exclusive)";
$Lang['ReadingGarden']['DescendingOrder'] = "Descending Order";
$Lang['ReadingGarden']['PrizeName'] = "Prize Name";

$Lang['ReadingGarden']['AllAwardLevel'] = "All Award Level";
$Lang['ReadingGarden']['ApplicableCategory'] = "Applicable Category";
$Lang['ReadingGarden']['ConfirmArr']['YouCanOnlyAnswerOneAnswerSheet'] = "Only one answer sheet can be submited for each reading record. Existing answer of another answer sheet will be deleted. Proceed?";
$Lang['ReadingGarden']['ConfirmArr']['TheModificationOfTheAnswerSheetWillBeDeleted'] = "The modification of this answer sheet will be deleted. Proceed?";

$Lang['ReadingGarden']['FileUploadDesc'] = "File Upload Instruction";
$Lang['ReadingGarden']['OnlineWritingDesc'] = "Online Writing Instruction";
$Lang['ReadingGarden']['ActiveStatus'][1] = "Active";
$Lang['ReadingGarden']['ActiveStatus'][0] = "Disable";

// Popular Reading Award Scheme
$Lang['ReadingGarden']['PopularAwardScheme'] = "Popular Award Scheme";
$Lang['ReadingGarden']['PopularAwardSchemeMgmt']['MenuTitle'] = "Popular Reading Award Scheme"; //menu name//
$Lang['ReadingGarden']['PopularAwardSchemeSettings']['MenuTitle'] = "Popular Reading Award Scheme"; //menu name//
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardSetting'] = "Award";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Award'] = "Award";  
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoringItem'] = "Scoring Item";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['Category'] = "Category";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryCode'] = "Category Code";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryNameEn'] = "Category Name (Eng)";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryNameCh'] = "Category Name (Chi)";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryName'] = "Category Name";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Item'] = "Item"; 
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDesc'] = "Description";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemCode'] = "Item Code";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDescEn'] = "Item Description (Eng)";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ItemDescCh'] = "Item Description (Chi)";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['Score'] = "Score";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['SubmitByStudent'] = "Allow Submit By Student";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AddItem'] = "Add Item";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['DeleteItem'] = "Delete Item";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['MoveItem'] = "Move Item"; 
$Lang['ReadingGarden']['PopularAwardSchemeArr']['EditItem'] = "Edit Item";  
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategorySetting'] = "Category Setting";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['DeleteCategory'] = "Delete Category";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AddPopularAwardSchemeRecord'] = "Add a new record";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['ViewStudentScore'] = "View Students' Score";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['NewAward'] = "New Award";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardName'] = "Award Name";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['AwardRequirement'] = "Additional Requirement";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ScoreRequired'] = "Score Required";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['CategoryIncluded'] = "Category Included";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Times'] = "Times";


### Import Award
$Lang['ReadingGarden']['PopularAwardSchemeArr']['ImportAwardRecord'] = "Import Award Record";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'] = array("Class Name","Class Number","User Login","Item Code","Times");

$Lang['ReadingGarden']['Import_Error']['ItemCodeIsMissing'] = "Item Code is missing";
$Lang['ReadingGarden']['Import_Error']['TimesIsMissing'] = "Times is missing";
$Lang['ReadingGarden']['Import_Error']['InvalidItemCode'] = "Invalid Item Code";
$Lang['ReadingGarden']['Import_Error']['InvalidTimes'] = "Invalid Times";

$Lang['SysMgr']['ReadingGarden']['ImportAwardRecord']['UserInfoRemarks'] = "Please fill in \"" . $Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][0] ."\" and \"" . $Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][1] . "\" or only \"" . $Lang['ReadingGarden']['PopularAwardSchemeArr']['Import_Award_Column'][2] ."\"";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsConfirmArr']['DeleteCategory'] = "Are you sure to delete this category?";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsConfirmArr']['DeleteItem'] = "Are you sure to delete this item?";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryCode'] = "Empty Category Code";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CategoryName'] = "Empty Category Name";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['CodeUnavailable'] = "Code Unavailable";

$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputItemCode'] = "Please input item code.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['ItemCodeExist'] = "Item code exists.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputItemName'] = "Pleaes input item description.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputScore'] = "Please input score.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputNumber'] = "Please input number.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectItem'] = "Please select an item.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectCategory'] = "Please select a category.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseSelectAtLeastOneCategory'] = "Please select at least one category.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['DuplicatedRequirement'] = "Applicable category group duplicated with another requirement.";
$Lang['ReadingGarden']['PopularAwardSchemeArr']['jsWarningArr']['PleaseInputAwardName'] = "Please input award name.";

// Reading Award Scheme
$Lang['ReadingGarden']['ReadingAwardScheme'] = "Reading Award Scheme";


$Lang['ReadingGarden']['ReturnMessage']['AddCategorySuccess'] = "1|=|Category was added successfully.";
$Lang['ReadingGarden']['ReturnMessage']['AddCategoryFail'] = "0|=|Failed to add category.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategorySuccess'] = "1|=|Category was updated successfully.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategoryFail'] = "0|=|Failed to update category.";
$Lang['ReadingGarden']['ReturnMessage']['DeleteCategorySuccess'] = "1|=|Category was deleted successfully.";
$Lang['ReadingGarden']['ReturnMessage']['DeleteCategoryFail'] = "0|=|Failed to delete category.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategoryOrderSuccess'] = "1|=|Categories were re-ordered successfully.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateCategoryOrderFail'] = "0|=|Failed to re-order category.";

$Lang['ReadingGarden']['ReturnMessage']['AddItemSuccess'] = "1|=|Item was added successfully.";
$Lang['ReadingGarden']['ReturnMessage']['AddItemFail'] = "0|=|Failed to add item.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemSuccess'] = "1|=|Item was updated successfully.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemFail'] = "0|=|Failed to update item.";
$Lang['ReadingGarden']['ReturnMessage']['DeleteItemSuccess'] = "1|=|Item was deleted successfully.";
$Lang['ReadingGarden']['ReturnMessage']['DeleteItemFail'] = "0|=|Failed to delete item.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemOrderSuccess'] = "1|=|Categories were re-ordered successfully.";
$Lang['ReadingGarden']['ReturnMessage']['UpdateItemOrderFail'] = "0|=|Failed to re-order item.";

$Lang['ReadingGarden']['ReturnMessage']['AddPopularAwardSchemeRecordSuccess'] = "1|=|Popular Award Scheme record was added successfully.";
$Lang['ReadingGarden']['ReturnMessage']['AddPopularAwardSchemeRecordFail'] = "0|=|Failed to add Popular Award Scheme record.";

$Lang['ReadingGarden']['ReturnMessage']['AddAwardSuccess'] = "1|=|Award was added successfully.";
$Lang['ReadingGarden']['ReturnMessage']['AddAwardFail'] = "0|=|Failed to add award.";
$Lang['ReadingGarden']['ReturnMessage']['EditAwardSuccess'] = "1|=|Award was edited successfully.";
$Lang['ReadingGarden']['ReturnMessage']['EditAwardFail'] = "0|=|Failed to edit award.";
$Lang['ReadingGarden']['ReturnMessage']['DeleteAwardSuccess'] = "1|=|Award was deleteed successfully.";
$Lang['ReadingGarden']['ReturnMessage']['DeleteAwardFail'] = "0|=|Failed to delete award.";




//if (!$NoLangWordings && is_file("$intranet_root/templates/lang_reading_garden.$intranet_session_language.customized.php"))
//{
//  include("$intranet_root/templates/lang_reading_garden.$intranet_session_language.customized.php");
//}


?>