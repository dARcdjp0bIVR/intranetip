<?php
// using : henry chow

/* ------------------ ATTENTION !!!!!!!!!!!!!!!!!!!! ----------------------------*/
/* please DO NOT use CRIMSON Editor to write on this file and it will override some special character
	 you may refer to variable $Lang['StudentAttendance']['AttendLateSymbol'] */
/* for string that start with "1|=|" or "0|=|",
	 that means the variable is for return message from server to client side,
 	 "1" stand for success, "0" stand for Fail*/

$Lang['Header']['HomeTitle'] = "校园综合平台 2.5";
$Lang['Header']['eClassCommunity'] = "eClass Community";
$Lang['Header']['ChangeLang'] = "Change Interface Language";
$Lang['Header']['ENG'] = "ENG";
$Lang['Header']['B5'] = "繁";

$Lang['Header']['Admin']['eClassUpdate'] = "eClass 更新";
$Lang['Header']['Admin']['eClassBackup'] = "eClass 备份";

$Lang['Header']['Menu']['Home'] = "首页";
$Lang['Header']['Menu']['eService'] = "资讯服务";
$Lang['Header']['Menu']['eLearning'] = "教学管理工具";
$Lang['Header']['Menu']['eAdmin'] = "行政管理工具";
$Lang['Header']['Menu']['SchoolSettings'] = "学校基本设定";

$Lang['Header']['Menu']['GeneralManagement'] = "一般事项管理";
$Lang['Header']['Menu']['StaffManagement'] = "职员管理";
$Lang['Header']['Menu']['StudentManagement'] = "学生管理";
$Lang['Header']['Menu']['ResourcesManagement'] = "资源管理";

$Lang['Header']['Menu']['eCircular'] = "教职员通告";
$Lang['Header']['Menu']['eHomework'] = "网上功课表";
$Lang['Header']['Menu']['eReportCard'] = "成绩管理系统";
$Lang['Header']['Menu']['eReportCard_Rubrics'] = "成绩表系统 (评量指标)";
$Lang['Header']['Menu']['eBooking'] = "电子资源预订系统";
$Lang['Header']['Menu']['CampusTV'] = "校园电视台";
$Lang['Header']['Menu']['Organization'] = "组织";
$Lang['Header']['Menu']['eClass'] = "网上教室";
$Lang['Header']['Menu']['eLC'] = "网上课件中心";
$Lang['Header']['Menu']['IES'] = "通识独立专题探究";
$Lang['Header']['Menu']['eLibrary'] = "网上图书馆";
$Lang['Header']['Menu']['eAttednance'] = "出勤管理";
$Lang['Header']['Menu']['eDiscipline'] = "学务管理";
$Lang['Header']['Menu']['eEnrolment'] = "课外活动管理";
$Lang['Header']['Menu']['eSports'] = "运动会管理";
$Lang['Header']['Menu']['SportDay'] = "运动会";
$Lang['Header']['Menu']['SwimmingGala'] = "水运会";
$Lang['Header']['Menu']['eNotice'] = "电子通告系统";
$Lang['Header']['Menu']['eInventory'] = "资产管理行政系统";
$Lang['Header']['Menu']['ePolling'] = "投票";
$Lang['Header']['Menu']['ePayment'] = "智慧卡缴费系统";
$Lang['Header']['Menu']['ePOS'] = "ePOS";
$Lang['Header']['Menu']['ResourcesBooking'] = "资源预订";
$Lang['Header']['Menu']['schoolNews'] = "校园最新消息";
$Lang['Header']['Menu']['eSurvey'] = "问卷调查";

$Lang['Header']['Menu']['iMail'] = "我的电子邮件";
$Lang['Header']['Menu']['iMailGamma'] = "我的电子邮件";
$Lang['Header']['Menu']['iCalendar'] = "我的行事历";
$Lang['Header']['Menu']['eCommunity'] = "社群管理工具";
$Lang['Header']['Menu']['iAccount'] = "我的用户资料";
$Lang['Header']['Menu']['iFolder'] = "我的文件夹";
$Lang['Header']['Menu']['iPortfolio'] = "学习档案";
$Lang['Header']['Menu']['CampusLinks'] = "校园连结";
$Lang['Header']['Menu']['ismartcard'] = "我的智慧卡纪录";
$Lang['Header']['Menu']['RSS'] = "RSS";

# School Settings
$Lang['Header']['Menu']['Site'] = "校园";
$Lang['Header']['Menu']['Role'] = "身分职位";
$Lang['Header']['Menu']['Subject'] = "学科";
$Lang['Header']['Menu']['Subjects'] = "学科";
$Lang['Header']['Menu']['Class'] = "班别";
$Lang['Header']['Menu']['Group'] = "小组";
$Lang['Header']['Menu']['CycleDay'] = "循环日";
$Lang['Header']['Menu']['Timetable'] = "时间表";
$Lang['Header']['Menu']['Period'] = "时段";
$Lang['Header']['Menu']['SchoolCalendar'] = "校历表";

$Lang['Portal']['CampusTV']['Live'] = "直播";
$Lang['Portal']['BrowserChecking']['BrowserTooNew'] = "eClass校园综合平台不完全支援你所使用的浏览器版本。建议你使用Internet Explorer 7进行浏览。";
$Lang['Portal']['Timetable']['TodayTimetable'] = "时间表";
$Lang['Portal']['Timetable']['ViewWholeTimetable'] = '检视完整时间表';

$Lang['Identity']['TeachingStaff'] = "教学职务员工";
$Lang['Identity']['NonTeachingStaff'] = "非教学职务员工";
$Lang['Identity']['Student'] = "学生";
$Lang['Identity']['Parent'] = "家长";
$Lang['Identity']['Guardian'] = "监护人";

$Lang['Btn']['Submit'] = '呈送';
$Lang['Btn']['Reset'] = '重置';
$Lang['Btn']['Cancel'] = '取消';
$Lang['Btn']['AddAll'] = '新增所有';
$Lang['Btn']['AddSelected'] = '新增已选取的';
$Lang['Btn']['RemoveSelected'] = '移除已选项目';
$Lang['Btn']['Remove'] = '移除';
$Lang['Btn']['RemovePhoto'] = '移除相片';
$Lang['Btn']['RemoveAll'] = '移除所有';
$Lang['Btn']['Edit'] = '编辑';
$Lang['Btn']['EditThisGroup'] = '编辑此科组';
$Lang['Btn']['Done'] = '完成';
$Lang['Btn']['Clear'] = '清除';
$Lang['Btn']['Save'] = '储存';
$Lang['Btn']['SaveAs'] = "另存";
$Lang['Btn']['Add'] = '增加';
$Lang['Btn']['Select'] = "选择";
$Lang['Btn']['NotSet'] = "不设定";
$Lang['Btn']['All'] = "全部";
$Lang['Btn']['Submit&AddMore'] = "呈送后继续新增";
$Lang['Btn']['Move'] = "移动";
$Lang['Btn']['Delete'] = "删除";
$Lang['Btn']['New'] = '新增';
$Lang['Btn']['Import'] = '汇入';
$Lang['Btn']['Export'] = '汇出';
$Lang['Btn']['SelectAll'] = '全选';
$Lang['Btn']['Show'] = '显示';
$Lang['Btn']['Hide'] = '隐藏';
$Lang['Btn']['Generate'] = '产生';
$Lang['Btn']['Close'] = '关闭';
$Lang['Btn']['Clone'] = '复制';
$Lang['Btn']['View'] = '检视';
$Lang['Btn']['Apply'] = '套用';
$Lang['Btn']['ApplyToAll'] = '全部套用';
$Lang['Btn']['Back'] = '返回';
$Lang['Btn']['Enable'] = '启用';
$Lang['Btn']['Disable'] = '停用';
$Lang['Btn']['Search'] = '寻找';
$Lang['Btn']['Print'] = '列印';
$Lang['Btn']['Continue'] = '继续';
$Lang['Btn']['Confirm'] = "确定";

$Lang['General']['Loading'] = '载入中...';
$Lang['General']['Yes'] = '是';
$Lang['General']['No'] = '否';
$Lang['General']['ReturnMessage']['AddSuccess'] = '1|=|记录己经新增.';
$Lang['General']['ReturnMessage']['UpdateSuccess'] = '1|=|记录己经更新.';
$Lang['General']['ReturnMessage']['DeleteSuccess'] = '1|=|记录已经删除.';
$Lang['General']['ReturnMessage']['AddUnsuccess'] = '0|=|新增记录失败.';
$Lang['General']['ReturnMessage']['UpdateUnsuccess'] = '0|=|更新记录失败.';
$Lang['General']['ReturnMessage']['DeleteUnsuccess'] = '0|=|删除记录失败.';

$Lang['General']['NoRecordFound'] = "找不到所需记录";
$Lang['General']['NoRecordAtThisMoment'] = "暂时仍未有任何纪录";
$Lang['General']['Remark'] = '备注';
$Lang['General']['TotalRecord'] = '纪录总数';
$Lang['General']['SuccessfulRecord'] = '成功纪录';
$Lang['General']['FailureRecord'] = '失败纪录';
$Lang['General']['SourceFile'] = '资料档案';
$Lang['General']['CSVFileFormat'] = '(.csv 或 .txt 档案)';
$Lang['General']['CSVSample'] = '范本档案';
$Lang['General']['ClickHereToDownloadSample'] = '按此下载范例';
$Lang['General']['ClickToEdit'] = '按一下进行编辑';
$Lang['General']['Record'] = '纪录';
$Lang['General']['Date'] = '日期';
$Lang['General']['warnSelectcsvFile'] = '请选择汇入的档案';
$Lang['General']['NoError'] = "没有错误";
$Lang['General']['Error'] = "错误";
$Lang['General']['warnNoRecordInserted'] = "资料未被储存，请更正错误后再试";
$Lang['General']['Enabled'] = '已启用';
$Lang['General']['Disabled'] = '已停用';
$Lang['General']['LastModifiedBy'] = "最后修改用户";
$Lang['General']['LastModified'] = "最后修改日期";
$Lang['General']['LastUpdatedBy'] = "最后更新用户";
$Lang['General']['LastUpdatedTime'] = "最后更新时间";
$Lang['General']['ChineseName'] = "中文名";
$Lang['General']['EnglishName'] = "英文名";
$Lang['General']['InvalidDateFormat'] = "*日期格式不符";
$Lang['General']['RequiredField'] = "附有「<span class='tabletextrequire'>*</span>」的项目必须填写";
$Lang['General']['Default'] = "预设";
$Lang['General']['Custom'] = "自订";
$Lang['General']['Help'] = "帮助";
$Lang['General']['Today'] = '今天';
$Lang['General']['DaysAgo'] = '日前';
$Lang['General']['LastModifiedInfoRemark'] = '最近更新：由<!--LastModifiedBy-->于<!--DaysAgo-->';
$Lang['General']['Warning'] = '警告';
$Lang['General']['English'] = '英文';
$Lang['General']['Chinese'] = '中文';
$Lang['General']['All'] = '全部';
$Lang['General']['To'] = '至';
$Lang['General']['Or'] = '或';

$Lang['General']['DayType4'][0] = "日";
$Lang['General']['DayType4'][1] = "一";
$Lang['General']['DayType4'][2] = "二";
$Lang['General']['DayType4'][3] = "三";
$Lang['General']['DayType4'][4] = "四";
$Lang['General']['DayType4'][5] = "五";
$Lang['General']['DayType4'][6] = "六";
$Lang['General']['DayType4'][7] = "日";

$Lang['General']['month'][0] = "十二月";
$Lang['General']['month'][1] = "一月";
$Lang['General']['month'][2] = "二月";
$Lang['General']['month'][3] = "三月";
$Lang['General']['month'][4] = "四月";
$Lang['General']['month'][5] = "五月";
$Lang['General']['month'][6] = "六月";
$Lang['General']['month'][7] = "七月";
$Lang['General']['month'][8] = "八月";
$Lang['General']['month'][9] = "九月";
$Lang['General']['month'][10] = "十月";
$Lang['General']['month'][11] = "十一月";
$Lang['General']['month'][12] = "十二月";
$Lang['General']['month'][13] = "一月";

$Lang['General']['SchoolYear'] = "学年";
$Lang['General']['Steps'] = "步骤";

$Lang['General']['ImportExport_Optional']['En'] = "[Optional]";
$Lang['General']['ImportExport_Reference']['En'] = "[Ref]";
$Lang['General']['ImportExport_Optional']['Ch'] = "[非必须的]";
$Lang['General']['ImportExport_Reference']['Ch'] = "[参考用途]";
$Lang['SysMgr']['FormClassMapping']['Import'] = '汇入';
$Lang['SysMgr']['FormClassMapping']['Export'] = '汇出';
$Lang['SysMgr']['FormClassMapping']['AddClass'] = '新增班别';
$Lang['SysMgr']['FormClassMapping']['NewForm'] = '新增级别';
$Lang['SysMgr']['FormClassMapping']['EditForm'] = '新增／编辑级别';
$Lang['SysMgr']['FormClassMapping']['CtrlMultiSelectMessage'] = '(按下CTRL键并点选项目以选择多项。)';
$Lang['SysMgr']['FormClassMapping']['StudentClassNumber'] = '学号';
$Lang['SysMgr']['FormClassMapping']['ClassNumFollowName'] = '根据学生英文名字字母序从新编排';
$Lang['SysMgr']['FormClassMapping']['ClassNumFollowTime'] = '新增学号到现存学号之后';
$Lang['SysMgr']['FormClassMapping']['ClassNumSelectWarning'] = '请选择学号计算方法';
$Lang['SysMgr']['FormClassMapping']['NewClass'] = '新增班别';
$Lang['SysMgr']['FormClassMapping']['ClassTitleEN'] = '班别名称(英文)';
$Lang['SysMgr']['FormClassMapping']['ClassTitleB5'] = '班别名称(中文)';
$Lang['SysMgr']['FormClassMapping']['ClassTeacher'] = '班导师';
$Lang['SysMgr']['FormClassMapping']['AddTeacher'] = '- 选择老师 -';
$Lang['SysMgr']['FormClassMapping']['AddFromClass'] = '- 由班别新增 -';
$Lang['SysMgr']['FormClassMapping']['ClassStudent'] = '学生';
$Lang['SysMgr']['FormClassMapping']['StudentSelected'] = '已选学生';
$Lang['SysMgr']['FormClassMapping']['SelectSchoolYear'] = '- 选择学年 -';
$Lang['SysMgr']['FormClassMapping']['SelectSchoolYearTerm'] = '- 选择学期 -';
$Lang['SysMgr']['FormClassMapping']['AllTerm'] = '全部学期';
$Lang['SysMgr']['FormClassMapping']['Form'] = '级别';
$Lang['SysMgr']['FormClassMapping']['FormSetting'] = '级别设定';
$Lang['SysMgr']['FormClassMapping']['Class'] = '班别';
$Lang['SysMgr']['FormClassMapping']['NumberOfStudent'] = '学生数量';
$Lang['SysMgr']['FormClassMapping']['SubjectTaken'] = '修读科目数量';
$Lang['SysMgr']['FormClassMapping']['NameOfClassEng'] = 'Name of class (English)';
$Lang['SysMgr']['FormClassMapping']['NameOfClassChi'] = 'Name of class (Chinese)';
$Lang['SysMgr']['FormClassMapping']['NameOfClass'] = 'Name of class';
$Lang['SysMgr']['FormClassMapping']['Delete'] = '移取';
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCode'] = '代号';
$Lang['SysMgr']['FormClassMapping']['ClickToEdit'] = $Lang['General']['ClickToEdit'];
$Lang['SysMgr']['FormClassMapping']['Move'] = '移动';
$Lang['SysMgr']['FormClassMapping']['ClassPageTitle'] = '班别';
$Lang['SysMgr']['FormClassMapping']['ClassPageSubTitle'] = '班别列表';
$Lang['SysMgr']['FormClassMapping']['Or'] = '或';
$Lang['SysMgr']['FormClassMapping']['SearchStudent'] = '自动搜寻结果新增(输入学生姓名或登入名称或电子邮件地址进行搜寻)';
$Lang['SysMgr']['FormClassMapping']['ClassInfo'] = '班别资料';
$Lang['SysMgr']['FormClassMapping']['Print'] = '列印';
$Lang['SysMgr']['FormClassMapping']['BasicClassInfo'] = '- 基本资料 -';
$Lang['SysMgr']['FormClassMapping']['RemoveThisClass'] = '移除此班别';
$Lang['SysMgr']['FormClassMapping']['EditThisClass'] = '编辑此班别';
$Lang['SysMgr']['FormClassMapping']['EditClass'] = '更新班别';
$Lang['SysMgr']['FormClassMapping']['ClassTitle'] = '班别名称';
$Lang['SysMgr']['FormClassMapping']['ClassNo'] = '学号';
$Lang['SysMgr']['FormClassMapping']['StudentName'] = '姓名';
$Lang['SysMgr']['FormClassMapping']['ClassNumberNotYetSet'] = '未编排';
$Lang['SysMgr']['FormClassMapping']['DeleteClassWarning'] = '移除班别将一并移除相关之学生、班导师、及科目修读情况资料。此操作不可回复，你是否确定要继续？';
$Lang['SysMgr']['FormClassMapping']['DeleteFormWarning'] = '是否确定要移除此级别？';
$Lang['SysMgr']['FormClassMapping']['SelectYearTerm'] = '- 选择学期 -';
$Lang['SysMgr']['FormClassMapping']['Title'] = '名称';
$Lang['SysMgr']['FormClassMapping']['SelectPreviousYearClass'] = '- 自去年班别新增 -';
$Lang['SysMgr']['FormClassMapping']['SelectStudentWithoutClass'] = '- 自没有分班学生 -';
$Lang['SysMgr']['FormClassMapping']['SubjectTakenPageTitle'] = '详细科目修读情况';
$Lang['SysMgr']['FormClassMapping']['SubjectsApplied'] = '正修读科目';
$Lang['SysMgr']['FormClassMapping']['Total'] = '总数';
$Lang['SysMgr']['FormClassMapping']['ReferenceInfo'] = '- 参考资料 -';
$Lang['SysMgr']['FormClassMapping']['ViewDetails'] = '检视详细资料';
$Lang['SysMgr']['FormClassMapping']['RelatedSubjects'] = '科目修读情况';
$Lang['SysMgr']['FormClassMapping']['Subject'] = '科目';
$Lang['SysMgr']['FormClassMapping']['NoOfStudentsTaken'] = '选修学生数量';
$Lang['SysMgr']['FormClassMapping']['ClassDeleteSuccess'] = '1|=|班别移除成功.';
$Lang['SysMgr']['FormClassMapping']['ClassDeleteUnsuccess'] = '0|=|班别移除失败.';
$Lang['SysMgr']['FormClassMapping']['ClassCreateSuccess'] = '1|=|班别新增成功.';
$Lang['SysMgr']['FormClassMapping']['ClassCreateUnsuccess'] = '0|=|班别新增失败.';
$Lang['SysMgr']['FormClassMapping']['ClassUpdateSuccess'] = '1|=|班别更新成功.';
$Lang['SysMgr']['FormClassMapping']['ClassUpdateUnsuccess'] = '0|=|班别更新失败.';
$Lang['SysMgr']['FormClassMapping']['AllClass'] = '所有班别';
$Lang['SysMgr']['FormClassMapping']['FormCreateSuccess'] = '1|=|级别新增成功.';
$Lang['SysMgr']['FormClassMapping']['FormCreateUnsuccess'] = '0|=|级别新增失败.';
$Lang['SysMgr']['FormClassMapping']['FormDeleteSuccess'] = '1|=|级别移除成功.';
$Lang['SysMgr']['FormClassMapping']['FormDeleteUnsuccess'] = '0|=|级别移除失败.';
$Lang['SysMgr']['FormClassMapping']['FormNameWarning'] = '*级别名称重复或空白.';
$Lang['SysMgr']['FormClassMapping']['ClassTitleWarning'] = '*请填上班别名称.';
$Lang['SysMgr']['FormClassMapping']['FormEditSuccess'] = '1|=|已储存变更。';
$Lang['SysMgr']['FormClassMapping']['FormEditUnsuccess'] = '0|=|级别更改失败.';
$Lang['SysMgr']['FormClassMapping']['DeletedUserLegend'] = '<font style="color:red;">*</font> 表示该用户已被删除.';
$Lang['SysMgr']['FormClassMapping']['ClassTitleDuplicationWarning'] = '<font style="color:red;">*</font> 在同一学年下，班别名称不可重复.';
$Lang['SysMgr']['FormClassMapping']['ClassTitleQuoteWarning'] = '*班别名称不可包含 " 或 \\\'.';
$Lang['SysMgr']['FormClassMapping']['FormWebsamsCodeInvalid'] = "级别 代号无效";
$Lang['SysMgr']['FormClassMapping']['MissingData'] = "资料不足";
$Lang['SysMgr']['FormClassMapping']['NoUserLoginWarning'] = "用户不存在";
$Lang['SysMgr']['FormClassMapping']['ClassNumberIntegerOnlyWarning'] = "学号只可包含数字";
$Lang['SysMgr']['FormClassMapping']['ClassNameQuoteWarning'] = "班别名称不可包含 \" 或 '";
$Lang['SysMgr']['FormClassMapping']['ClassNotExistWarning'] = "班别不存在";
$Lang['SysMgr']['FormClassMapping']['ImportFormClass'] = "汇入级别及班别";
$Lang['SysMgr']['FormClassMapping']['ImportClassStudent'] = "汇入班学生";
$Lang['SysMgr']['FormClassMapping']['ImportClassTeacher'] = "汇入班导师";
$Lang['SysMgr']['FormClassMapping']['SchoolYear'] = "学年";
$Lang['SysMgr']['FormClassMapping']['RecordsImportedSuccessfully'] = "个资料汇入成功";
$Lang['SysMgr']['FormClassMapping']['StudentLoginID'] = "学生登入编号";

if($plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EE1'] = "EE1 - Special Education";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EE2'] = "EE2 - Special Education";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EE3'] = "EE3 - Special Education";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EEP'] = "EEP - Special Education";
}
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['K1'] = "K1 - Nursery Class";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['K2'] = "K2 - Lower Kindergarten";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['K3'] = "K3 - Upper Kindergarten";

if(!$plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['LP'] = "LP - Lower Preparatory";
}

$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P1'] = "P1 - Primary 1";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P2'] = "P2 - Primary 2";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P3'] = "P3 - Primary 3";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P4'] = "P4 - Primary 4";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P5'] = "P5 - Primary 5";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['P6'] = "P6 - Primary 6";

if($plugin['StudentRegistry']){
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['1'] = "1 - Portuguese Primary School";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['2'] = "2 - Portuguese Primary School";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['3'] = "3 - Portuguese Primary School";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['4'] = "4 - Portuguese Primary School";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['5'] = "5 - Portuguese Primary School";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['6'] = "6 - Portuguese Primary School";
 	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['EP6'] = "EP6 - English Secnondary School Prep";
}
if(!$plugin['StudentRegistry']){
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PP'] = "PP - Pre-Primary - Spec";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['PR'] = "PR - Primary - Spec";
}

$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S1'] = "S1 - Secondary 1";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S2'] = "S2 - Secondary 2";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S3'] = "S3 - Secondary 3";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S4'] = "S4 - Secondary 4";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S5'] = "S5 - Secondary 5";
$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S6'] = "S6 - Secondary 6";
if(!$plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['S7'] = "S7 - Secondary 7";
}

if(!$plugin['StudentRegistry']){
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SJ'] = "SJ - Junior Secondary";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['SS'] = "SS - Senior Secondary";
  $Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['UP'] = "UP - Upper Preparatory";
}

if($plugin['StudentRegistry']){
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F1'] = "F1 - English Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F2'] = "F2 - English Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F3'] = "F3 - English Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F4'] = "F4 - English Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F5'] = "F5 - English Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['F6'] = "F6 - English Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['7'] = "7 - Portuguese Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['8'] = "8 - Portuguese Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['9'] = "9 - Portuguese Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['10'] = "10 - Portuguese Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['11'] = "11 - Portuguese Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['12'] = "12 - Portuguese Secondary School";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['ERP'] = "ERP - Primary Recurrent Education";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['ERG'] = "ERG - Junior Secondary Recurrent Education";
	$Lang['SysMgr']['FormClassMapping']['WEBSAMSCodeList']['ERC'] = "ERC - Senior Secondary Recurrent Education";
}

$Lang['SysMgr']['FormClassMapping']['Select']['Form'] = '选择级别';
$Lang['SysMgr']['FormClassMapping']['Select']['Class'] = '选择班别';
$Lang['SysMgr']['FormClassMapping']['Select']['Term'] = '选择学期';
$Lang['SysMgr']['FormClassMapping']['Select']['TeachingStaff'] = '选择教师';
$Lang['SysMgr']['FormClassMapping']['Select']['NonTeachingStaff'] = '选择非教务职员';

$Lang['SysMgr']['FormClassMapping']['All']['Class'] = '全部班别';
$Lang['SysMgr']['FormClassMapping']['All']['TeachingStaff'] = '全部教师';
$Lang['SysMgr']['FormClassMapping']['All']['NonTeachingStaff'] = '全部非教务职员';
$Lang['SysMgr']['FormClassMapping']['All']['Form'] = '全部级别';

// subject class mapping
$Lang['SysMgr']['SubjectClassMapping']['ModuleTitle'] = '学科';
$Lang['SysMgr']['SubjectClassMapping']['Subject'] = '学科';
$Lang['SysMgr']['SubjectClassMapping']['Group'] = '科组';
$Lang['SysMgr']['SubjectClassMapping']['ViewGroup'] = '检视科组';
$Lang['SysMgr']['SubjectClassMapping']['Title'] = '标题';
$Lang['SysMgr']['SubjectClassMapping']['Subject&ComponentTitle'] = '学科及学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupTitle'] = '科组';
$Lang['SysMgr']['SubjectClassMapping']['BatchCreate'] = '快速新增';
$Lang['SysMgr']['SubjectClassMapping']['Import'] = '汇入';
$Lang['SysMgr']['SubjectClassMapping']['Export'] = '汇出';
$Lang['SysMgr']['SubjectClassMapping']['CollapseAll'] = 'Collapse All';
$Lang['SysMgr']['SubjectClassMapping']['ExpandAll'] = 'Expand All';
$Lang['SysMgr']['SubjectClassMapping']['Order'] = '排序';
$Lang['SysMgr']['SubjectClassMapping']['SubjectWEBSAMSCode'] = '代号';
$Lang['SysMgr']['SubjectClassMapping']['SubjectDescription'] = '叙述 (英/中)';
$Lang['SysMgr']['SubjectClassMapping']['AddClass'] = '新增学科组别';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroup'] = '科组';
$Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroup'] = '新增科组';
$Lang['SysMgr']['SubjectClassMapping']['ClassCode'] = '代号';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitleEN'] = '名称(英)';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitleB5'] = '名称(中)';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitle'] = '科组标题';
$Lang['SysMgr']['SubjectClassMapping']['ClassTeacher'] = '老师';
$Lang['SysMgr']['SubjectClassMapping']['GroupTeacher'] = '科组老师';
$Lang['SysMgr']['SubjectClassMapping']['ClassStudent'] = '学生';
$Lang['SysMgr']['SubjectClassMapping']['SelectClass'] = '- 选择班别 -';
$Lang['SysMgr']['SubjectClassMapping']['SelectForm'] = '- 选择级别联系 -';
$Lang['SysMgr']['SubjectClassMapping']['InvalidSelectedStudWarning'] = '*以上学生应要移除，因他们所属级别不在级别联系之上.';
$Lang['SysMgr']['SubjectClassMapping']['InvalidYearIDWarning'] = '*请至少选择一个级别.';
$Lang['SysMgr']['SubjectClassMapping']['ClassTitleWarning'] = '*请输入科组标题.';
$Lang['SysMgr']['SubjectClassMapping']['ClassCodeWarning'] = '*科组代号无效.';
$Lang['SysMgr']['SubjectClassMapping']['ClassCreateSuccess'] = '1|=|新增科组成功.';
$Lang['SysMgr']['SubjectClassMapping']['ClassCreateUnsuccess'] = '0|=|新增科组失败.';
$Lang['SysMgr']['SubjectClassMapping']['DeleteClass'] = '移除科组';
$Lang['SysMgr']['SubjectClassMapping']['EditClass'] = '编辑科组';
$Lang['SysMgr']['SubjectClassMapping']['DeleteClassWarning'] = '移除科组会失去所有组别<->学生关联，此动作不能返回。是否确定要继续进行？';
$Lang['SysMgr']['SubjectClassMapping']['EditSubjectGroup'] = '编辑科组';
$Lang['SysMgr']['SubjectClassMapping']['ClassEditSuccess'] = '1|=|科组更新成功.';
$Lang['SysMgr']['SubjectClassMapping']['ClassEditUnsuccess'] = '0|=|科组更新失败.';
$Lang['SysMgr']['SubjectClassMapping']['ShowAllGroup'] = '显示所有组别';
$Lang['SysMgr']['SubjectClassMapping']['HideAllGroup'] = '隐藏所有组别';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupList'] = '科组清单';
$Lang['SysMgr']['SubjectClassMapping']['RemoveThisGroup'] = '移除此组别';
$Lang['SysMgr']['SubjectClassMapping']['Print'] = '列印';
$Lang['SysMgr']['SubjectClassMapping']['ClassDeleteSuccess'] = '1|=|科组移除成功.';
$Lang['SysMgr']['SubjectClassMapping']['ClassDeleteUnsuccess'] = '0|=|科组移除失败.';
$Lang['SysMgr']['SubjectClassMapping']['Instruction'] = '使用指引';
$Lang['SysMgr']['SubjectClassMapping']['Instruction1'] = '请选择学科及班别。系统将为每个所选的班别建立所选科目的科组。';
$Lang['SysMgr']['SubjectClassMapping']['SelectSubjects'] = '选择学科';
$Lang['SysMgr']['SubjectClassMapping']['SelectClasses'] = '选择班别';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning1'] = '以下的学科-班别组合未能新增，因为班别上的学生已参加了同一学科的其他组别.';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning2'] = '有问题的班别:';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning3'] = '可行的班别:';
$Lang['SysMgr']['SubjectClassMapping']['BatchCheckClassWarning4'] = '你是否确定要继续进行此动作？';
$Lang['SysMgr']['SubjectClassMapping']['FormAssociatedTo'] = '适用于';
$Lang['SysMgr']['SubjectClassMapping']['NumberOfStudentInForm'] = '级别中学生数目';
$Lang['SysMgr']['SubjectClassMapping']['FilterFormList'] = '过滤此列表';
$Lang['SysMgr']['SubjectClassMapping']['CloseFilter'] = '关闭过滤';
$Lang['SysMgr']['SubjectClassMapping']['ClearFilter'] = '清除选项';
$Lang['SysMgr']['SubjectClassMapping']['HideNonRelatedSubject'] = '隐藏空白学科';
$Lang['SysMgr']['SubjectClassMapping']['ViewSubjectGroupWithStudentFrom'] = '只检视含下列级别学生的科组：';

$Lang['SysMgr']['SubjectClassMapping']['LearningCategory'] = '学习领域';
$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCode'] = 'WebSAMS 代号';
$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeComponent'] = 'WebSAMS 代号（学科分卷）';
$Lang['SysMgr']['SubjectClassMapping']['Title'] = '名称';
$Lang['SysMgr']['SubjectClassMapping']['TitleEn'] = '名称 (英文)';
$Lang['SysMgr']['SubjectClassMapping']['TitleCh'] = '名称 (中文)';
$Lang['SysMgr']['SubjectClassMapping']['ComponentName'] = '学科分卷名称';
$Lang['SysMgr']['SubjectClassMapping']['Abbreviation'] = '缩写';
$Lang['SysMgr']['SubjectClassMapping']['AbbreviationEn'] = '缩写 (英文)';
$Lang['SysMgr']['SubjectClassMapping']['AbbreviationCh'] = '缩写 (中文)';
$Lang['SysMgr']['SubjectClassMapping']['ShortForm'] = '简写';
$Lang['SysMgr']['SubjectClassMapping']['ShortFormEn'] = '简写 (英文)';
$Lang['SysMgr']['SubjectClassMapping']['ShortFormCh'] = '简写 (中文)';
$Lang['SysMgr']['SubjectClassMapping']['NoLearningCategory'] = '未分类学科';
$Lang['SysMgr']['SubjectClassMapping']['ParentSubject'] = '主学科';
$Lang['SysMgr']['SubjectClassMapping']['ComponentSubject'] = '学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['Subject'] = '学科';
$Lang['SysMgr']['SubjectClassMapping']['eClassLicenseLeftMessage'] = '你的一般教室许可证有 <!--TotalQuota--> 个，尚有 <!--QuotaLeft--> 个可供使用。你已使用的许可证为 <!--QuotaUsed--> 个。';
$Lang['SysMgr']['SubjectClassMapping']['NotEnoughLicenseMessage'] = '你没有足够的许可证建立eClass教室。';

$Lang['SysMgr']['SubjectClassMapping']['Settings']['LearningCategory'] = '学习领域设定';

$Lang['SysMgr']['SubjectClassMapping']['Add']['LearningCategory'] = '新增学习领域';
$Lang['SysMgr']['SubjectClassMapping']['Add']['Subject'] = '新增学科';
$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectInLearningCategory'] = '于此学习领域新增学科';
$Lang['SysMgr']['SubjectClassMapping']['Add']['SubjectComponent'] = '新增子学科';

$Lang['SysMgr']['SubjectClassMapping']['Edit']['LearningCategory'] = '编辑学习领域';
$Lang['SysMgr']['SubjectClassMapping']['Edit']['Subject'] = '编辑学科';
$Lang['SysMgr']['SubjectClassMapping']['Edit']['SubjectComponent'] = '编辑子学科';

$Lang['SysMgr']['SubjectClassMapping']['Delete']['LearningCategory'] = '删除学习领域';
$Lang['SysMgr']['SubjectClassMapping']['Delete']['Subject'] = '删除学科';
$Lang['SysMgr']['SubjectClassMapping']['Delete']['SubjectComponent'] = '删除子学科';

$Lang['SysMgr']['SubjectClassMapping']['Reorder']['LearningCategory'] = '排列学习领域';
$Lang['SysMgr']['SubjectClassMapping']['Reorder']['Subject'] = '排列学科';
$Lang['SysMgr']['SubjectClassMapping']['Reorder']['SubjectComponent'] = '排列子学科';

$Lang['SysMgr']['SubjectClassMapping']['Setting']['LearningCategory'] = '学习领域设定';

$Lang['SysMgr']['SubjectClassMapping']['View']['SubjectComponent'] = '检视学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['View']['BackToAllSubjectsView'] = '回到全部学科';

$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterWebSAMSCode'] = '请输入WebSAMS代码。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleEn'] = '请输入英文名称。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterTitleCh'] = '请输入中文名称。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterAbbrEn'] = '请输入英文缩写。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterAbbrCh'] = '请输入中文缩写。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterShortFormEn'] = '请输入英文简写。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseEnterShortFormCh'] = '请输入中文简写。';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteLearningCategory'] = '你是否确定删除此学习领域？';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteSubject'] = '你是否确定删除此学科？';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['ConfirmDeleteSubjectComponent'] = '你是否确定删除此子学科？';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseSelectLearningCategory'] = '请选择学习领域。';

$Lang['SysMgr']['SubjectClassMapping']['Warning']['NoSelected']['LearningCategory'] = '* 没有选择学习领域';

$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['WebSAMSCode'] = '* WebSAMS代码空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['Title'] = '* 名称空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleEn'] = '* 英文名称空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['TitleCh'] = '* 中文名称空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrEn'] = '* 英文缩写空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['AbbrCh'] = '* 中文缩写空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormEn'] = '* 英文简写空白';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['Blank']['ShortFormCh'] = '* 中文简写空白';

$Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedCode']['Subject'] = '* 已有其他学科使用此代号';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedCode']['SubjectComponent'] = '* 已有其他学科分卷使用此代号';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['DuplicatedTitle']['LearningCategory'] = '* 已有学习领域使用此名称';
$Lang['SysMgr']['SubjectClassMapping']['Warning']['MustBeInteger']['WebSAMSCode'] = '* WebSAMS代码必须是数字';

$Lang['SysMgr']['SubjectClassMapping']['Select']['LearningCategory'] = '选择学习领域';
$Lang['SysMgr']['SubjectClassMapping']['Select']['Subject'] = '选择学科';
$Lang['SysMgr']['SubjectClassMapping']['Select']['SubjectComponent'] = '选择学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['Select']['SubjectGroup'] = '选择科组';

$Lang['SysMgr']['SubjectClassMapping']['All']['Subject'] = '全部学科';
$Lang['SysMgr']['SubjectClassMapping']['All']['SubjectGroup'] = '全部科组';

$Lang['SysMgr']['SubjectClassMapping']['Button']['Add&Finish'] = $Lang['Btn']['Submit'];
$Lang['SysMgr']['SubjectClassMapping']['Button']['Add&AddMoreSubject'] = $Lang['Btn']['Submit&AddMore'];

$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['LearningCategory'] = '1|=|学习领域新增成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['Subject'] = '1|=|学科新增成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Success']['SubjectComponent'] = '1|=|子学科新增成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['LearningCategory'] = '0|=|学习领域新增失败。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['Subject'] = '0|=|学科新增失败。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Add']['Failed']['SubjectComponent'] = '0|=|子学科新增失败。';

$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['LearningCategory'] = '1|=|学习领域编辑成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['Subject'] = '1|=|学科编辑成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Success']['SubjectComponent'] = '1|=|子学科编辑成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['LearningCategory'] = '0|=|学习领域编辑失败。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['Subject'] = '0|=|学科编辑失败。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Edit']['Failed']['SubjectComponent'] = '0|=|子学科编辑失败。';

$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['LearningCategory'] = '1|=|学习领域删除成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['Subject'] = '1|=|学科删除成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Success']['SubjectComponent'] = '1|=|子学科删除成功。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['LearningCategory'] = '0|=|学习领域删除失败。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['Subject'] = '0|=|学科删除失败。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['Delete']['Failed']['SubjectComponent'] = '0|=|子学科删除失败。';


// Timetable
$Lang['SysMgr']['Timetable']['ModuleTitle'] = '时间表';
$Lang['SysMgr']['Timetable']['Timetable'] = '时间表';

$Lang['SysMgr']['Timetable']['Period'] = '时段';
$Lang['SysMgr']['Timetable']['TimeSession'] = '时段';
$Lang['SysMgr']['Timetable']['Cycle'] = '循环';
$Lang['SysMgr']['Timetable']['CycleDay'] = '循环日';
// 20100326 Ivan: requested by Clement to change back to English
//$Lang['SysMgr']['Timetable']['Day'] = '日';
$Lang['SysMgr']['Timetable']['Day'] = 'Day';
$Lang['SysMgr']['Timetable']['Identity'] = '身份';
$Lang['SysMgr']['Timetable']['AcademicYear'] = '学年';
$Lang['SysMgr']['Timetable']['Term'] = '学期';
$Lang['SysMgr']['Timetable']['Target'] = '目标';
$Lang['SysMgr']['Timetable']['User'] = '用户';
$Lang['SysMgr']['Timetable']['Title'] = '标题';
$Lang['SysMgr']['Timetable']['Name'] = '名称';
$Lang['SysMgr']['Timetable']['StartTime'] = '开始时间';
$Lang['SysMgr']['Timetable']['EndTime'] = '结束时间';
$Lang['SysMgr']['Timetable']['TimeSlot'] = '时段';
$Lang['SysMgr']['Timetable']['Time'] = '时间';
$Lang['SysMgr']['Timetable']['DataFiltering'] = '过滤此时间表';
$Lang['SysMgr']['Timetable']['DisplayOption'] = '检视选项';
$Lang['SysMgr']['Timetable']['SubjectTitle'] = '学科名称';
$Lang['SysMgr']['Timetable']['NumOfStudent'] = '学生数量';
$Lang['SysMgr']['Timetable']['Copy'] = '复制';

$Lang['SysMgr']['Timetable']['Tag']['Management'] = '管理';
$Lang['SysMgr']['Timetable']['Tag']['View'] = '检视/列印';

$Lang['SysMgr']['Timetable']['Add']['RoomAllocation'] = '新增课堂';
$Lang['SysMgr']['Timetable']['Add']['Timetable'] = '新增时间表';
$Lang['SysMgr']['Timetable']['Add']['CycleDay'] = '新增循环日';
$Lang['SysMgr']['Timetable']['Add']['TimeSlot'] = '新增时段';

$Lang['SysMgr']['Timetable']['Delete']['RoomAllocation'] = '删除课堂';
$Lang['SysMgr']['Timetable']['Delete']['Timetable'] = '删除时间表';
$Lang['SysMgr']['Timetable']['Delete']['CycleDay'] = '删除循环日';
$Lang['SysMgr']['Timetable']['Delete']['TimeSlot'] = '删除时段';

$Lang['SysMgr']['Timetable']['Edit']['RoomAllocation'] = '编辑课堂';
$Lang['SysMgr']['Timetable']['Edit']['Timetable'] = '编辑时间表';
$Lang['SysMgr']['Timetable']['Edit']['TimeSlot'] = '编辑时段';

$Lang['SysMgr']['Timetable']['Select']['Period'] = '选择时段';
$Lang['SysMgr']['Timetable']['Select']['Cycle'] = '选择循环';
$Lang['SysMgr']['Timetable']['Select']['CycleDays'] = '选择循环日数';
$Lang['SysMgr']['Timetable']['Select']['ViewMode'] = '选择检视模式';
$Lang['SysMgr']['Timetable']['Select']['Identity'] = '选择身份';
$Lang['SysMgr']['Timetable']['Select']['Timetable'] = '选择时间表';

$Lang['SysMgr']['Timetable']['Button']['Add&AddMoreRoomAllocation'] = $Lang['Btn']['Submit&AddMore'];
$Lang['SysMgr']['Timetable']['Button']['ClearFiltering'] = '不过滤资料';
$Lang['SysMgr']['Timetable']['Button']['ImportLesson'] = '汇入课堂';
$Lang['SysMgr']['Timetable']['Button']['ExportLesson'] = '汇出课堂';

$Lang['SysMgr']['Timetable']['ViewMode'] = '检视/列印对象';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Class'] = '班别';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['SubjectGroup'] = '科组';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Personal'] = '个人';
$Lang['SysMgr']['Timetable']['View']['ViewModeOption']['Room'] = '房间';

$Lang['SysMgr']['Timetable']['Warning']['Blank']['Title'] = '* 请输入名称。';

$Lang['SysMgr']['Timetable']['Warning']['NoCycleSetUpYet'] = '未有任何循环设定。';
$Lang['SysMgr']['Timetable']['Warning']['NoPeriodSetUpYet'] = '未有任何时段设定。';
$Lang['SysMgr']['Timetable']['Warning']['TimeSlotOverlapped'] = '* 时段重迭。';
$Lang['SysMgr']['Timetable']['Warning']['TitleDuplicated'] = '* 标题重复。';
$Lang['SysMgr']['Timetable']['Warning']['OnlyCopiedSubjectGroupCanBeCopied'] = '* 如选择的学期与时间表的学期不相同，系统只会复制透过「复制」来建立的科组到此时间表。';

$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['AcademicYear'] = '请选择学年。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Term'] = '请选择学期。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Period'] = '请选择时段。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Cycle'] = '请选择循环。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Semester'] = '请选择学期。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Building'] = '请选择大楼。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Floor'] = '请选择楼层。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Room'] = '请选择房间。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Timetable'] = '请选择时间表。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['ViewMode'] = '请选择检视模式。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['SubjectGroup'] = '请选择科组。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseSelect']['Subject'] = '请选择学科。';

$Lang['SysMgr']['Timetable']['jsWarning']['PleaseEnter']['Title'] = '请输入标题。';
$Lang['SysMgr']['Timetable']['jsWarning']['PleaseEnter']['Location'] = '请输入地点。';

$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['Timetable'] = '如你删除此时间表，所有相关时段及课堂纪录将被同时删除。你是否确定删除此时间表？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['RoomAllocation'] = '你是否确定删除此课堂？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['CycleDays'] = '所有本日之课堂资料将一并被删除。你是否确定要删除？';
$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['TimeSlot'] = '如你删除此时段，所有相关课堂纪录将被同时删除。你是否确定删除此时段？';

$Lang['SysMgr']['Timetable']['jsWarning']['Delete']['Timetable'] = '如你复制时间表，已于本时间表设定的时段及课堂纪录将被删除。你是否确定复制时间表？';

$Lang['SysMgr']['Timetable']['jsWarning']['RoomUsedAlready'] = '* 此房间已分配给其他科组。';
$Lang['SysMgr']['Timetable']['jsWarning']['SubjectGroupAllocatedAlready'] = '* 此科组已于此时段获分配到其他房间。';
$Lang['SysMgr']['Timetable']['jsWarning']['SubjectGroupUserConflict'] = '* 此科组的学生已参予此时段的科组。';
$Lang['SysMgr']['Timetable']['jsWarning']['StartTimeMustBeEarlierThenEndTime'] = '* 结束时间不能早于开始时间。';
$Lang['SysMgr']['Timetable']['jsWarning']['TimeSlotOverlapped'] = '* 时段不可重迭。';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['Timetable'] = '1|=|时间表新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['CycleDay'] = '1|=|循环日新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['TimeSlot'] = '1|=|时段新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Success']['RoomAllocation'] = '11|=|课堂新增成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['Timetable'] = '0|=|时间表新增失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['CycleDay'] = '0|=|循环日新增失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['TimeSlot'] = '0|=|时段新增失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Add']['Failed']['RoomAllocation'] = '0|=|课堂新增失败。';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['Timetable'] = '1|=|时间表编辑成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['CycleDay'] = '1|=|循环日编辑成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['TimeSlot'] = '1|=|时段编辑成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Success']['RoomAllocation'] = '1|=|课堂编辑成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['Timetable'] = '0|=|时间表编辑失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['CycleDay'] = '0|=|循环日编辑失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['TimeSlot'] = '0|=|时段编辑失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Edit']['Failed']['RoomAllocation'] = '0|=|课堂编辑失败。';

$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['Timetable'] = '1|=|时间表删除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['CycleDay'] = '1|=|循环日删除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['TimeSlot'] = '1|=|时段删除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Success']['RoomAllocation'] = '1|=|课堂删除成功。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['Timetable'] = '0|=|时间表删除失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['CycleDay'] = '0|=|循环日删除失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['TimeSlot'] = '0|=|时段删除失败。';
$Lang['SysMgr']['Timetable']['ReturnMessage']['Delete']['Failed']['RoomAllocation'] = '0|=|课堂删除失败。';

$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Day";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Time Slot";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Time Slot Name ".$Lang['General']['ImportExport_Reference']['En'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Time Slot Time Range ".$Lang['General']['ImportExport_Reference']['En'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Subject Group Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Subject Group Name ".$Lang['General']['ImportExport_Reference']['En'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Building Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Building Name ".$Lang['General']['ImportExport_Reference']['En'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Location Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Location Name ".$Lang['General']['ImportExport_Reference']['En'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Sub-location Code";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Sub-location Name ".$Lang['General']['ImportExport_Reference']['En'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][0][] = "Others Site";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(日)";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(时段)";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(时段名称) ".$Lang['General']['ImportExport_Reference']['Ch'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(时段时间) ".$Lang['General']['ImportExport_Reference']['Ch'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(科组代号)";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(科组名称) ".$Lang['General']['ImportExport_Reference']['Ch'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(大楼代号)";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(大楼名称) ".$Lang['General']['ImportExport_Reference']['Ch'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(位置代号)";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(位置名称) ".$Lang['General']['ImportExport_Reference']['Ch'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(子位置代号)";
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(子位置名称) ".$Lang['General']['ImportExport_Reference']['Ch'];
$Lang['SysMgr']['Timetable']['ExportTimetable_Column'][1][] = "(其他场所)";

// Role Management
$Lang['SysMgr']['RoleManagement']['IdentityRole'] = '身分 / 职位概览';
$Lang['SysMgr']['RoleManagement']['ModuleRole'] = 'Module Role';
$Lang['SysMgr']['RoleManagement']['ModuleTitle'] = '职位';
$Lang['SysMgr']['RoleManagement']['New'] = '新增';
$Lang['SysMgr']['RoleManagement']['Student'] = '学生';
$Lang['SysMgr']['RoleManagement']['Parent'] = '家长';
$Lang['SysMgr']['RoleManagement']['RemoveRole'] = '移除职位';
$Lang['SysMgr']['RoleManagement']['NewRole'] = '新增职位';
$Lang['SysMgr']['RoleManagement']['Identity'] = '身分';
$Lang['SysMgr']['RoleManagement']['SelectIdentity'] = '- 选择身分 -';
$Lang['SysMgr']['RoleManagement']['RoleTitle'] = '身分名称';
$Lang['SysMgr']['RoleManagement']['SubmitAndEdit'] = '递交 &amp; 修改设定';
$Lang['SysMgr']['RoleManagement']['RoleNameDuplicateWarning'] = '*身分名称重复或空白';
$Lang['SysMgr']['RoleManagement']['IdentitySelectWarning'] = '*Please select an identity';
$Lang['SysMgr']['RoleManagement']['RoleCreatedSuccess'] = '1|=|身分新增成功.';
$Lang['SysMgr']['RoleManagement']['RoleCreatedUnsuccess'] = '0|=|身分新增失败.';
$Lang['SysMgr']['RoleManagement']['RoleList'] = '身分清单';
$Lang['SysMgr']['RoleManagement']['AccessRightAndTarget'] = 'Access Right & Targeting';
$Lang['SysMgr']['RoleManagement']['UserList'] = '成员名单';
$Lang['SysMgr']['RoleManagement']['AccessRight'] = '此职位可管理以下所勾选的模组：';
$Lang['SysMgr']['RoleManagement']['Targeting'] = '寄件对象';
$Lang['SysMgr']['RoleManagement']['Name'] = '姓名';
$Lang['SysMgr']['RoleManagement']['UserType'] = '身分';
$Lang['SysMgr']['RoleManagement']['Delete'] = '移除';
$Lang['SysMgr']['RoleManagement']['RemoveThisRole'] = '移除此身分';
$Lang['SysMgr']['RoleManagement']['AddUser'] = '新增用户';
$Lang['SysMgr']['RoleManagement']['Users'] = '用户';
$Lang['SysMgr']['RoleManagement']['RoleRenamedSuccess'] = '1|=|身分重新命名成功.';
$Lang['SysMgr']['RoleManagement']['RoleRenamedUnsuccess'] = '0|=|身分重新命名失败.';
$Lang['SysMgr']['RoleManagement']['AddRoleMemberWarning'] = '*Please select at least one user to add.';
$Lang['SysMgr']['RoleManagement']['AddRoleMemberUnsuccess'] = '0|=|成员新增失败.';
$Lang['SysMgr']['RoleManagement']['AddRoleMemberSuccess'] = '1|=|成员新增成功.';
$Lang['SysMgr']['RoleManagement']['RoleRemoveSuccess'] = '1|=|成员移除成功.';
$Lang['SysMgr']['RoleManagement']['RoleRemoveUnsuccess'] = '0|=|成员移除失败.';
$Lang['SysMgr']['RoleManagement']['RoleDeleteWarning'] = '你是否确定要移除此身分?';
$Lang['SysMgr']['RoleManagement']['RoleMemberDeleteWarning'] = '你是否确定要移除已选择之身分成员?';
$Lang['SysMgr']['RoleManagement']['RoleMemberRemoveSuccess'] = '1|=|身分成员移除成功.';
$Lang['SysMgr']['RoleManagement']['RoleMemberRemoveUnsuccess'] = '0|=|身分成员移除失败.';
$Lang['SysMgr']['RoleManagement']['RoleSavedSuccess'] = '1|=|身分储存成功.';
$Lang['SysMgr']['RoleManagement']['RoleSavedUnsuccess'] = '0|=|身分储存失败.';
$Lang['SysMgr']['RoleManagement']['RoleName'] = '职位';
$Lang['SysMgr']['RoleManagement']['TopManagement'] = '模组管理权限';
$Lang['SysMgr']['RoleManagement']['SearchUser'] = '搜寻用户';
$Lang['SysMgr']['RoleManagement']['SelectedUser'] = '已选择用户';
$Lang['SysMgr']['RoleManagement']['SelectAll'] = '-- 选择全部 --';
$Lang['SysMgr']['RoleManagement']['TeachingStaff'] = '教师';
$Lang['SysMgr']['RoleManagement']['SupportStaff'] = '非教学职员';
$Lang['SysMgr']['RoleManagement']['AllTeachingStaff'] = '所有教师';
$Lang['SysMgr']['RoleManagement']['AllSupportStaff'] = '所有非教学职员';
$Lang['SysMgr']['RoleManagement']['AllStudent'] = '所有学生';
$Lang['SysMgr']['RoleManagement']['MyChildren'] = '我的子女';
$Lang['SysMgr']['RoleManagement']['AllParent'] = '所有家长';
$Lang['SysMgr']['RoleManagement']['MyParent'] = '我的父母';
$Lang['SysMgr']['RoleManagement']['MySubjectGroupStaff'] = '我的科组';
$Lang['SysMgr']['RoleManagement']['MyClassStaff'] = '我的班别';
$Lang['SysMgr']['RoleManagement']['MyFormStudent'] = '我的级别';
$Lang['SysMgr']['RoleManagement']['MySubjectGroupStudent'] = '我的科组';
$Lang['SysMgr']['RoleManagement']['MyClassStudent'] = '我的班别';
$Lang['SysMgr']['RoleManagement']['MyFormParent'] = '我的级别';
$Lang['SysMgr']['RoleManagement']['MySubjectGroupParent'] = '我的科组';
$Lang['SysMgr']['RoleManagement']['MyClassParent'] = '我的班别';
$Lang['SysMgr']['RoleManagement']['ToAllUser'] = '可发送至所有用户：';
$Lang['SysMgr']['RoleManagement']['ToAll'] = '所有...';
$Lang['SysMgr']['RoleManagement']['BelongToSameForm'] = '同一级别的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameFormClass'] = '同一班别的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameSubject'] = '同一科目的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameSubjectGroup'] = '同一科组的...';
$Lang['SysMgr']['RoleManagement']['BelongToSameGroup'] = '同一小组的...';
$Lang['SysMgr']['RoleManagement']['CanSendTo'] = "可发送至：";
$Lang['SysMgr']['RoleManagement']['UncheckForMoreOptions'] = "取消勾选以显示更多选项。";
$Lang['SysMgr']['RoleManagement']['Class'] = "班别";
$Lang['SysMgr']['RoleManagement']['ClassNumber'] = "学号";

$Lang['SysMgr']['RoleManagement']['eDisciplineAdmin'] = '学务管理';
$Lang['SysMgr']['RoleManagement']['eCircularAdmin'] = "教职员通告";
$Lang['SysMgr']['RoleManagement']['eNoticeAdmin'] = "电子通告系统";
$Lang['SysMgr']['RoleManagement']['eReportCardAdmin'] = "成绩表管理";
$Lang['SysMgr']['RoleManagement']['eAttendanceAdmin'] = "教职员出勤系统管理";
$Lang['SysMgr']['RoleManagement']['eAttendanceAdmin'] = "教职员考勤系统管理";
$Lang['SysMgr']['RoleManagement']['eEnrolmentAdmin'] = "课外活动系统管理";
$Lang['SysMgr']['RoleManagement']['eSportsAdmin'] = "运动会管理";
$Lang['SysMgr']['RoleManagement']['eInventoryAdmin'] = "资产管理行政系统";
$Lang['SysMgr']['RoleManagement']['eBookingAdmin'] = "电子资源预订管理系统";
$Lang['SysMgr']['RoleManagement']['eClassAdmin'] = "eClass 管理";
$Lang['SysMgr']['RoleManagement']['ePayment'] = "收费管理";
$Lang['SysMgr']['RoleManagement']['ePOS'] = "ePOS 管理";
$Lang['SysMgr']['RoleManagement']['ePolling'] = "投票管理";
$Lang['SysMgr']['RoleManagement']['eHomework'] = "功课管理";
$Lang['SysMgr']['RoleManagement']['campusLink'] = "校园连结管理";
$Lang['SysMgr']['RoleManagement']['schoolNews'] = "校园最新消息管理";
$Lang['SysMgr']['RoleManagement']['ResourcesBooking'] = "资源预订";
$Lang['SysMgr']['RoleManagement']['StudentAttendance'] = "学生出勤系统管理";
$Lang['SysMgr']['RoleManagement']['iCalendar'] = "行事历管理";
$Lang['SysMgr']['RoleManagement']['eSurveyAdmin'] = "问卷调查";

$Lang['SysMgr']['RoleManagement']['eService'] = "资讯服务";
$Lang['SysMgr']['RoleManagement']['eLearning'] = "教学管理工具";
$Lang['SysMgr']['RoleManagement']['eAdmin'] = "行政管理工具";
$Lang['SysMgr']['RoleManagement']['other'] = "其他";
$Lang['SysMgr']['RoleManagement']['SchoolSettings'] = $Lang['Header']['Menu']['SchoolSettings'];
$Lang['SysMgr']['RoleManagement']['RepairSystem'] = "报修系统";
$Lang['SysMgr']['RoleManagement']['AccountMgmt'] = "用户管理";
$Lang['SysMgr']['RoleManagement']['IES_Admin'] = "通识独立专题探究管理";

// Location
$Lang['SysMgr']['Location']['All']['Floor'] = "所有位置";
$Lang['SysMgr']['Location']['Location'] = '校园';
$Lang['SysMgr']['Location']['LocationList'] = '所有位置';
$Lang['SysMgr']['Location']['Building'] = '大楼';
$Lang['SysMgr']['Location']['Floor'] = '位置';
$Lang['SysMgr']['Location']['Room'] = '子位置';
$Lang['SysMgr']['Location']['Code'] = '代号';
$Lang['SysMgr']['Location']['Barcode'] = '条码';
$Lang['SysMgr']['Location']['Title'] = '名称';
$Lang['SysMgr']['Location']['TitleEn'] = '英文名称';
$Lang['SysMgr']['Location']['TitleCh'] = '中文名称';
$Lang['SysMgr']['Location']['Description'] = '描述';
$Lang['SysMgr']['Location']['ClickToEdit'] = $Lang['General']['ClickToEdit'];
$Lang['SysMgr']['Location']['OthersLocation'] = '其他场所';
$Lang['SysMgr']['Location']['Room&OthersLocation'] = '房间及其他场所';

$Lang['SysMgr']['Location']['Toolbar']['NewLocation'] = '新增场所';
$Lang['SysMgr']['Location']['Toolbar']['Import'] = '汇入';
$Lang['SysMgr']['Location']['Toolbar']['Export'] = '汇出';

$Lang['SysMgr']['Location']['Setting']['Building'] = '编辑大楼';
$Lang['SysMgr']['Location']['Setting']['Floor'] = '编辑位置';
$Lang['SysMgr']['Location']['Setting']['Room'] = '子位置设定';

$Lang['SysMgr']['Location']['Add']['Building'] = '新增大楼';
$Lang['SysMgr']['Location']['Add']['Floor'] = '新增位置';
$Lang['SysMgr']['Location']['Add']['Room'] = '新增子位置';
$Lang['SysMgr']['Location']['Add']['RoomWithinFloor'] = '在此新增子位置';

$Lang['SysMgr']['Location']['Edit']['Room'] = '编辑';

$Lang['SysMgr']['Location']['Delete']['Building'] = '删除';
$Lang['SysMgr']['Location']['Delete']['Floor'] = '删除';
$Lang['SysMgr']['Location']['Delete']['Room'] = '删除';

$Lang['SysMgr']['Location']['Reorder']['Building'] = '移动';
$Lang['SysMgr']['Location']['Reorder']['Floor'] = '移动';
$Lang['SysMgr']['Location']['Reorder']['Room'] = '移动';

$Lang['SysMgr']['Location']['Select']['Building'] = '选择大楼';
$Lang['SysMgr']['Location']['Select']['Floor'] = '选择位置';
$Lang['SysMgr']['Location']['Select']['Room'] = '选择子位置';

$Lang['SysMgr']['Location']['All']['Building'] = '全部大楼';
$Lang['SysMgr']['Location']['All']['Room'] = '全部子位置';

$Lang['SysMgr']['Location']['Button']['Add&Finish'] = $Lang['Btn']['Submit'];
$Lang['SysMgr']['Location']['Button']['Add&AddMoreLocation'] = $Lang['Btn']['Submit&AddMore'];

$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterCode'] = '请输入代号。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleEn'] = '请输入英文名称。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterTitleCh'] = '请输入中文名称。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseEnterDescription'] = '请输入内容。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseSelectBuilding'] = '请选择大楼。';
$Lang['SysMgr']['Location']['jsWarning']['PleaseSelectFloor'] = '请选择位置。';
$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteBuilding'] = '你是否确定删除此大楼？';
$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteFloor'] = '你是否确定删除此位置？';
$Lang['SysMgr']['Location']['jsWarning']['ConfirmDeleteRoom'] = '你是否确定删除此子位置？';


$Lang['SysMgr']['Location']['Warning']['BarcodeNotValid'] = "* 条码格式不正确。";
$Lang['SysMgr']['Location']['Warning']['Blank']['Barcode'] = "* 请输入条码。";
$Lang['SysMgr']['Location']['Warning']['Blank']['Code'] = '* 请输入代号。';
$Lang['SysMgr']['Location']['Warning']['Blank']['TitleEn'] = '* 请输入英文名称。';
$Lang['SysMgr']['Location']['Warning']['Blank']['TitleCh'] = '* 请输入中文名称。';
$Lang['SysMgr']['Location']['Warning']['NoSelected']['Building'] = '* 没有选择大楼';
$Lang['SysMgr']['Location']['Warning']['NoSelected']['Floor'] = '* 没有选择位置';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Building'] = '* 已有其他大楼使用此代号';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Floor'] = '* 已有其他位置使用此代号';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Room'] = '* 已有其他子位置使用此代号';
$Lang['SysMgr']['Location']['Warning']['DuplicatedCode']['Index'] = '显示为红色的代号为重复使用之代号，重复代号将会影响相关汇入结果，请立即更改重复的代号。';
$Lang['SysMgr']['Location']['Warning']['DuplicatedBarcode']['Building'] = '* 已有其他大楼使用此条码';
$Lang['SysMgr']['Location']['Warning']['DuplicatedBarcode']['Floor'] = '* 已有其他位置使用此条码';
$Lang['SysMgr']['Location']['Warning']['DuplicatedBarcode']['Room'] = '* 已有其他子位置使用此条码';
$Lang['SysMgr']['Location']['Warning']['ReloadTimetable']['ClearFilteringWarning'] = '如时间表已设定大量课堂，显示整个时间表可能需时数分钟。你是否确定要显示整个时间表？';

$Lang['SysMgr']['Location']['Remark']['RedWordMeansDuplicatedCode'] = '* 红色显示为已重复使用之编号';

$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Building'] = '1|=|大楼新增成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Floor'] = '1|=|位置新增成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Success']['Room'] = '1|=|子位置新增成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Building'] = '0|=|大楼新增失败。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Floor'] = '0|=|位置新增失败。';
$Lang['SysMgr']['Location']['ReturnMessage']['Add']['Failed']['Room'] = '0|=|子位置新增失败。';

$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Building'] = '1|=|大楼编辑成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Floor'] = '1|=|位置编辑成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Success']['Room'] = '1|=|子位置编辑成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Building'] = '0|=|大楼编辑失败。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Floor'] = '0|=|位置编辑失败。';
$Lang['SysMgr']['Location']['ReturnMessage']['Edit']['Failed']['Room'] = '0|=|子位置编辑失败。';

$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Building'] = '1|=|大楼删除成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Floor'] = '1|=|位置删除成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Success']['Room'] = '1|=|子位置删除成功。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Building'] = '0|=|大楼删除失败。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Floor'] = '0|=|位置删除失败。';
$Lang['SysMgr']['Location']['ReturnMessage']['Delete']['Failed']['Room'] = '0|=|子位置删除失败。';

$Lang['SysMgr']['Location']['FieldTitle']['TotalNumOfSubLocation'] = '子位置总数';


$Lang['SysMgr']['CycleDay']['CycleDayTitle'] = "循环日";
$Lang['SysMgr']['CycleDay']['NewPeriod'] = "新增时区";
$Lang['SysMgr']['CycleDay']['NewTimeZoneHere'] = "在此新增时区";
$Lang['SysMgr']['CycleDay']['EditPeriod'] = "编辑时区";
$Lang['SysMgr']['CycleDay']['EditThisTimeZone'] = "编辑此时区";
$Lang['SysMgr']['CycleDay']['DeletePeriod'] = "删除时区";
$Lang['SysMgr']['CycleDay']['PeriodTitle'] = "日期范围";
$Lang['SysMgr']['CycleDay']['PeriodStart']['FieldTitle'] = "开始日期";
$Lang['SysMgr']['CycleDay']['PeriodEnd']['FieldTitle'] = "结束日期";
$Lang['SysMgr']['CycleDay']['PeriodType']['FieldTitle'] = "类型";
$Lang['SysMgr']['CycleDay']['CycleType']['FieldTitle'] = "循环日编号";
$Lang['SysMgr']['CycleDay']['DaysOfACycle']['FieldTitle'] = "循环日数";
$Lang['SysMgr']['CycleDay']['FirstDay']['FieldTitle'] = "起始日";
$Lang['SysMgr']['CycleDay']['SaturdayCounted']['FieldTitle'] = "计算星期六";
$Lang['SysMgr']['CycleDay']['BackgroundColor']['FieldTitle'] = "显示颜色";
$Lang['SysMgr']['CycleDay']['SaturdayCounted']['Yes'] = "是";
$Lang['SysMgr']['CycleDay']['SaturdayCounted']['No'] = "否";
$Lang['SysMgr']['CycleDay']['PeriodType']['NoCycle'] = "周日";
$Lang['SysMgr']['CycleDay']['PeriodType']['HaveCycle'] = "循环日";
$Lang['SysMgr']['CycleDay']['PeriodType']['Generation'] = "一般计法";
$Lang['SysMgr']['CycleDay']['PeriodType']['FileImport'] = "档案汇入";
$Lang['SysMgr']['CycleDay']['CycleType']['Numeric'] = "数字 (1, 2, 3 ...)";
$Lang['SysMgr']['CycleDay']['CycleType']['Alphabets'] = "字母 (A, B, C ...)";
$Lang['SysMgr']['CycleDay']['CycleType']['Roman'] = "罗马数字 (I, II, III ...)";
$Lang['SysMgr']['CycleDay']['FileImport']['Description'] = "<u><b>档案说明:</b></u><br>
第一栏: 日期 (YYYY-MM-DD)<br>
第二栏: 中文介面的显示 (如: 循环日 A)<br>
第三栏: 英文介面的显示 (如: Day A)<br>
第四栏: 日历介面的显示及定期资源预订等功能使用 (如: A)<br>
";
$Lang['SysMgr']['CycleDay']['FileImport']['Warning'] = "在此日期内的汇入纪录会被清除。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartEmpty'] = "请输入开始日期。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndEmpty'] = "请输入结束日期。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodStartInvalid'] = "开始日期不正确。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodEndInvalid'] = "结束日期不正确。";
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodOverlapped'] = "时段不能重迭。";
$Lang['SysMgr']['CycleDay']['JSWarning']['ConfirmDelete'] = "你是否确定删除已选的时段？";
$Lang['SysMgr']['CycleDay']['JSWarning']['StartPeriodLargerThenEndPeriod'] = "结束日期不能早于开始日期。";
$Lang['SysMgr']['CycleDay']['JSWarning']['SelectAtLeaseOnePeriod'] = '请选择最少一个时段。';
$Lang['SysMgr']['CycleDay']['JSWarning']['PeriodIsNotInTheSelectedSchoolYear'] = '时段并不是在选择学年的范围内。';
$Lang['SysMgr']['CycleDay']['CycleName'] = "名称";
$Lang['SysMgr']['CycleDay']['CycleShortForm'] = "简称";
$Lang['SysMgr']['CycleDay']['GenerateProduction']['ReturnSuccess'] = '1|=|已成功转至使用。';
$Lang['SysMgr']['CycleDay']['GenerateProduction']['ReturnFail'] = '0|=|未能成功转至使用。';
$Lang['SysMgr']['CycleDay']['GeneratePreview']['ReturnSuccess'] = '1|=|已成功产生预览。';
$Lang['SysMgr']['CycleDay']['GeneratePreview']['ReturnFail'] = '0|=|未能成功产生预览。';
$Lang['SysMgr']['CycleDay']['BackToProductionView'] = "返回使用中模式。";
$Lang['SysMgr']['CycleDay']['PeriodManagement'] = "时段管理模式";
$Lang['SysMgr']['CycleDay']['ViewBy']['Title'] = "检视";
$Lang['SysMgr']['CycleDay']['ViewBy']['ViewByCalendar'] = "日历";
$Lang['SysMgr']['CycleDay']['ViewBy']['ViewByList'] = "列表";
$Lang['SysMgr']['CycleDay']['DefinePeriod'] = "定义日期范围";
$Lang['SysMgr']['CycleDay']['GenerateAndCheckPreview'] = "产生及检视预览";
$Lang['SysMgr']['CycleDay']['MakeCurrentPreviewToProduction'] = "把预览转至使用";
$Lang['SysMgr']['CycleDay']['Publish'] = "发布";
$Lang['SysMgr']['CycleDay']['Preview'] = "预览";
$Lang['SysMgr']['CycleDay']['ManagementMode'] = "管理模式";
$Lang['SysMgr']['CycleDay']['LastPublishDate'] = "最后发布日期";
$Lang['SysMgr']['CycleDay']['LastModifiedDate'] = "最后修改日期";
$Lang['SysMgr']['CycleDay']['SettingTitle'] = "时区";


$Lang['SysMgr']['SchoolCalendar']['ModuleTitle'] = "校历表";
$Lang['SysMgr']['SchoolCalendar']['SettingTitle'] = "假期 / 事项";
$Lang['SysMgr']['SchoolCalendar']['NewHolidaysOrEvents'] = "新增假期 / 事项";
$Lang['SysMgr']['SchoolCalendar']['EventType'][0] = "学校事项";
$Lang['SysMgr']['SchoolCalendar']['EventType'][1] = "教学事项";
$Lang['SysMgr']['SchoolCalendar']['EventType'][2] = "小组事项";
$Lang['SysMgr']['SchoolCalendar']['EventType'][3] = "公众假期";
$Lang['SysMgr']['SchoolCalendar']['EventType'][4] = "学校假期";
$Lang['SysMgr']['SchoolCalendar']['SelectType'] = "- 选择种类 -";
$Lang['SysMgr']['SchoolCalendar']['EventStatus']['Publish'] = "已发布";
$Lang['SysMgr']['SchoolCalendar']['EventStatus']['Pending'] = "未发布";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDate'] = "日期";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStartDate'] = "开始日期";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventEndDate'] = "结束日期";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventType'] = "种类";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventTitle'] = "标题";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenue'] = "地点";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventNature'] = "假期 / 事项性质";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventDescription'] = "描述";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventSkipCycleDay'] = "不计算上课日";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventStatus'] = "状况";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['Yes'] = "是";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SkipCycleDay']['No'] = "否";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SchoolHolidayEvent'] = "学校假期 / 事项";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['None'] = "没有";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['School'] = "有";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['Applicable'] = "适用";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventLocation']['NotApplicable'] = "不适用";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['SelectTimeTable'] = "-- 选择时间表格式 --";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['TimeTable'] = "添附此时间表";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ApplyToRelatedEvents'] = "应用于所有有关的假期 / 事项";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['To'] = "至";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['NotCountInCycleDay'] = "不计算循环日";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CountInCycleDay'] = "计算循环日";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['AcademicYearIsNotSet'] = "0|=|请先完成学年设定。";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['CurrentDateNotInTerm'] = "0|=|请先检查学年设定是否正确。";
$Lang['SysMgr']['SchoolCalendar']['Event']['Edit'] = "编辑";
$Lang['SysMgr']['SchoolCalendar']['Event']['EditHolidayEvent'] = "编辑假期 / 事项";
$Lang['SysMgr']['SchoolCalendar']['ToolTip']['DeleteRelatedHolidaysEvents'] = "删除相关假期 / 事项";
$Lang['SysMgr']['SchoolCalendar']['ToolTip']['Expand'] = "显示所有";
$Lang['SysMgr']['SchoolCalendar']['ToolTip']['Hide'] = "显示部分";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['ConfirmDelete'] = "你是否确定要删除此假期 / 事项？";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['ConfirmDeleteRelatedEvents'] = "你是否确定要删除所有相关的假期 / 事项？";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectEventType'] = "请选择种类。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InputEventTitle'] = "请输入标题。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectTimeTable'] = "请选择时间表格式。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['DateRangeNotInSameTerm'] = "日期范围并不是在同一学期内。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InputLocationName'] = "请输入地点。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectLocation'] = "请选择地点。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EventEndDateIsEmpty'] = "请输入结束日期。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['InvalidEventEndDate'] = "请输入有效的结束日期。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EndDateLaterThanStartDate'] = "结束日期不能早于开始日期。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['EventCannotOverTheSchoolYear'] = "假期 / 事项必须设定于同一学年内。";
$Lang['SysMgr']['SchoolCalendar']['JSWarning']['SelectTheGroup'] = "请选择小组。";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['Group'] = "小组";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['ImportHolidayOrEvent'] = "汇入假期 / 事项";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['SelectImportFile'] = "选择 CSV 或 TXT 档案";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ConfirmImportRecord'] = "确认汇入资料";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['ImportedResult'] = "汇入结果";
$Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Submit'] = "呈送";
$Lang['SysMgr']['SchoolCalendar']['Import']['Button']['Cancel'] = "取消";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['DataColumn'] = "资料栏";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][1] = "第一栏：<font color=red>*</font> 开始日期";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][2] = "第二栏：<font color=red>*</font> 结束日期";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][3] = "第三栏：<font color=red>*</font> 种类 (SE - 学校事项，AE - 教学事项，PH - 公众假期，SH - 学校假期，GE - 小组事项)";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][4] = "第四栏：<font color=red>*</font> 标题";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][5] = "第五栏：<font color=red>^</font> 地点";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][6] = "第六栏：假期 / 事项性质";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][7] = "第七栏：描述";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][8] = "第八栏：<font color=red>*</font> 不计算上课日";
$Lang['SysMgr']['SchoolCalendar']['Import']['DataColumn'][9] = "第九栏：<font color=red>**</font> 小组编号";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['Remark'] = "备注";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['DataMissing'] = "资料不足。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['StartDateInvalid'] = "开始日期无效。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['EndDateInvalid'] = "结束日期无效。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['TypeInvalid'] = "种类无效。";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['SkipSchoolDayInvalid'] = "不计算上课日无效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['VenueInvalid'] = "地点无效";
$Lang['SysMgr']['SchoolCalendar']['Import']['Result']['Success'] = "汇入成功";
$Lang['SysMgr']['SchoolCalendar']['Import']['Result']['Fail'] = "汇入失败";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['RequiredField'] = "附有「<span class='tabletextrequire'>*</span>」的项目必须填写";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['VenueFormatExplanation'] = "<span class='tabletextrequire'>^</span> 如是校内场所, 请使用 \"大楼代号>位置代号>子位置代号\" 为资料格式。如是校外地方, 请使用 \"Others>场所名称\" 为资料格式。";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['GroupIDFormatExplanation'] = "<span class='tabletextrequire'>**</span> 请使用\",\"分隔每个小组编号。";

$Lang['SysMgr']['SchoolCalendar']['Print']['Print'] = "列印";
$Lang['SysMgr']['SchoolCalendar']['Print']['Cancel'] = "取消";
$Lang['SysMgr']['SchoolCalendar']['Print']['PrintOptions'] = "列印选项";
//$Lang['SysMgr']['SchoolCalendar']['Print']['PrintWithDefault'] = "预设列印";
//$Lang['SysMgr']['SchoolCalendar']['Print']['PrintWithOptions'] = "自订列印";
$Lang['SysMgr']['SchoolCalendar']['Print']['PrintingColor'] = "列印色彩";
$Lang['SysMgr']['SchoolCalendar']['Print']['Black/White'] = "黑白";
$Lang['SysMgr']['SchoolCalendar']['Print']['Color'] = "彩色";
$Lang['SysMgr']['SchoolCalendar']['Print']['PrintMonthsWithoutEvents'] = "列印没有事件的月份";
$Lang['SysMgr']['SchoolCalendar']['Print']['Yes'] = "是";
$Lang['SysMgr']['SchoolCalendar']['Print']['No'] = "否";
$Lang['SysMgr']['SchoolCalendar']['Print']['MaximumCalendarsPrintedPerPage'] = "每页最多月份";
$Lang['SysMgr']['SchoolCalendar']['Print']['EventTitle'] = "事件标题";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['InvalidDateRange'] = "日期范围无效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['NotInCurrentSchoolYear'] = "该记录不属于本学年";
$Lang['SysMgr']['SchoolCalendar']['Import']['ReturnFail']['IncorrectFileExtention'] = "0|=|资料档案无效";
$Lang['SysMgr']['SchoolCalendar']['Import']['ReturnFail']['InvalidFileFormat'] = "0|=|档案格式无效";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['EventVenueCode'] = "地点编号";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['GroupID'] = "小组编号";
$Lang['SysMgr']['SchoolCalendar']['FieldTitle']['GroupCategory'] = "类别";
$Lang['SysMgr']['SchoolCalendar']['Import']['ErrorMsg']['GroupIDError'] = "小组编号无效";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckSiteCode'] = "< 按此检视场所编号 >";
$Lang['SysMgr']['SchoolCalendar']['Import']['FieldTitle']['CheckGroupCode'] = "< 按此检视小组编号 >";

$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearSetting'] = "学年 / 学期";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYear'] = "学年";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['Term'] = "学期";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearName'] = "名称";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearNameChi'] = "中文名称";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['AcademicYearNameEng'] = "英文名称";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermName'] = "名称";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameChi'] = "名称 (中文)";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermNameEng'] = "名称 (英文)";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermStartDate'] = "开始日期";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['TermEndDate'] = "结束日期";
$Lang['SysMgr']['AcademicYear']['ToolTip']['AcademicYearAddEdit'] = "新增 / 编辑学年";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['SelectAllAcademicYear'] = "-- 所有学年 --";
$Lang['SysMgr']['AcademicYear']['ToolTip']['NewAcademicYear'] = "新增学年";
$Lang['SysMgr']['AcademicYear']['ToolTip']['MoveToArrangeDisplayOrder'] = "移动滑鼠以排列显示次序";
$Lang['SysMgr']['AcademicYear']['ToolTip']['DeleteAcademicYear'] = "删除学年";
$Lang['SysMgr']['AcademicYear']['ToolTip']['AddTerm'] = "新增学期";
$Lang['SysMgr']['AcademicYear']['ToolTip']['EditTerm'] = "编辑学期";
$Lang['SysMgr']['AcademicYear']['ToolTip']['DeleteTerm'] = "删除学期";
$Lang['SysMgr']['AcademicYear']['JSWarning']['TermOverlapped'] = "学期不能重迭。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['AcademicYearOverlapped'] = "学年不能重复。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseTermNameIsEmpty'] = "请输入中文名称。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishTermNameIsEmpty'] = "请输入英文名称。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseAcademicYearNameIsEmpty'] = "请输入中文名称。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishAcademicYearNameIsEmpty'] = "请输入英文名称。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['StartDateIsEmpty'] = "请输入开始日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EndDateIsEmpty'] = "请输入结束日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['InvalidStartDate'] = "请输入有效的开始日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['InvalidEndDate'] = "请输入有效的结束日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EndDateLaterThanStartDate'] = "结束日期不能早于开始日期。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['ChineseTermNameIsDuplicate'] = "中文名称不能重复。";
$Lang['SysMgr']['AcademicYear']['JSWarning']['EnglishTermNameIsDuplicate'] = "英文名称不能重复。";
$Lang['SysMgr']['AcademicYear']['Warning']['TermCannotEditOrDelete'] = "如要删除学期，请先将其包含的资料删除。";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['CurrentSchoolYear'] = "本学年";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings']['Title'] = "重要学年/学期注意事项";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantProperSchoolYearOrTermSettings']['Content'] = "输入学期开始及结束日期时，请务必遵守以下两项原则：<br><br>
1. 学期之间不可留有空间。例如：上学期于1月31日结束，下学期必须于2月1日开始。<br>
2. 上学期的开始日期，与下学期的结束日期(如果贵校有三个学期，则首学期的开始日期与最后一个学期的结束日期)，必须要包含一整年。例如：上学期自9月1日开始，下学期则在下一年8月31日结束。<br><br>
否则，不在学期范围内的日子，系统功能将无法正常使用。因此，各学期的开始和结束日期，不应理解为学校学期的真正开始/结束日期，而是「可以使用系统」的日期范围。";
$Lang['SysMgr']['AcademicYear']['FieldTitle']['ImportantNote_TermSetting'] = "由于在未有包含在任何学期的日子，你将无法正常使用系统功能，请注意：<br><br>
1. 正确填写开始日期：请紧贴上一学期的结束日期。如上一学期结束日期为1月31日，此学期的开始日期应为2月1日。<br><br>
2. 如此学期为学年内的最后一个学期：则此学期的结束日期，必须与首学期的开始日期，相隔一整年。例如：首学期由9月1日开始，则此学期应在下一年8月31日结束。";
$Lang['SysMgr']['AcademicYear']['Warning']['TermDateChecking'] = "由于在未有包含在任何学期的日子，你将无法正常使用系统功能。因此，请确保学期的开始日期紧贴上一学期的结束日期。如上一学期结束日期为1月31日，此学期的开始日期应为2月1日。";

$Lang['SysMgr']['Periods']['FieldTitle']['PeriodsName'] = "时段名称";
$Lang['SysMgr']['Periods']['FieldTitle']['Status'] = "使用状况";
$Lang['SysMgr']['Periods']['FieldTitle']['PeriodRange'] = "日期范围";
$Lang['SysMgr']['Periods']['FieldTitle']['PeriodSetting'] = "时段设定";


## Inventory
if($plugin['Inventory'])
{
	$Lang['eInventory']['FieldTitle']['ExportBuildingCode'] = "Building Code";
	$Lang['eInventory']['FieldTitle']['ExportBuildingTitle'] = "Building";
	$Lang['eInventory']['FieldTitle']['ExportFloorCode'] = "Location Code";
	$Lang['eInventory']['FieldTitle']['ExportFloorTitle'] = "Location";
	$Lang['eInventory']['FieldTitle']['ExportRoomCode'] = "Sub-location Code";
	$Lang['eInventory']['FieldTitle']['ExportRoomTitle'] = "Sub-location";
	$Lang['eInventory']['FieldTitle']['UpdateItemLocation'] = "更改位置";
	$Lang['eInventory']['FieldTitle']['UpdateItemStatus'] = "更新状况";
	$Lang['eInventory']['FieldTitle']['ItemRequestWriteOff'] = "申请报销";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][1]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第一栏 : 物品编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][1]['AjaxLinkTitle'] = "按此查询物品编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][2]['FieldTitle'] = "<span class='tabletextrequire'>* ^</span>第二栏 : 条码";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][3]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第三栏 : 物品名称 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][4]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第四栏 : 物品名称 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][5]['FieldTitle'] = "第五栏 : 详细资料 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][6]['FieldTitle'] = "第六栏 : 详细资料 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][7]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第七栏 : 类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][7]['AjaxLinkTitle'] = "按此查询类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][7]['AjaxLink'] = "CategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][8]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第八栏 : 子类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][8]['AjaxLinkTitle'] = "按此查询子类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][8]['AjaxLink'] = "SubCategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][9]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第九栏 : 管理组别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][9]['AjaxLinkTitle'] = "按此查询管理组别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][9]['AjaxLink'] = "GroupCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][10]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十栏 : 大楼编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][10]['AjaxLinkTitle'] = "按此查询大楼编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][10]['AjaxLink'] = "BuildingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][11]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十一栏 : 位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][11]['AjaxLinkTitle'] = "按此查询位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][11]['AjaxLink'] = "LocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][12]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十二栏 : 子位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][12]['AjaxLinkTitle'] = "按此查询子位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][12]['AjaxLink'] = "SubLocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][13]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十三栏 : 资金来源编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][13]['AjaxLinkTitle'] = "按此查询资金来源编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][13]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][14]['FieldTitle'] = "第十四栏 : 拥有权 (1 - 学校, 2 - 政府, 3 - 办学团体)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][15]['FieldTitle'] = "第十五栏 : 保用日期 (YYYY-MM-DD)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][16]['FieldTitle'] = "第十六栏 : 许可证";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][17]['FieldTitle'] = "第十七栏 : 序号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][18]['FieldTitle'] = "第十八栏 : 牌子";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][19]['FieldTitle'] = "第十九栏 : 供应商名称";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][20]['FieldTitle'] = "第二十栏 : 供应商联络";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][21]['FieldTitle'] = "第二十一栏 : 供应商资料";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][22]['FieldTitle'] = "第二十二栏 : 报价单编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][23]['FieldTitle'] = "第二十三栏 : 标书编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][24]['FieldTitle'] = "第二十四栏 : 发票编号";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][25]['FieldTitle'] = "第二十五栏 : 购入日期 (YYYY-MM-DD)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][26]['FieldTitle'] = "第二十六栏 : 购买金额 (单据总额)";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][27]['FieldTitle'] = "第二十七栏 : 单位价格";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][28]['FieldTitle'] = "第二十八栏 : 维修资料";
	$Lang['eInventory']['FieldTitle']['Import']['Single'][29]['FieldTitle'] = "第二十九栏 : 备注";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][1]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第一栏 : 物品编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][1]['AjaxLinkTitle'] = "按此查询物品编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][1]['AjaxLink'] = "ItemCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][2]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第二栏 : 物品名称 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][3]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第三栏 : 物品名称 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][4]['FieldTitle'] = "第四栏 : 详细资料 (中文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][5]['FieldTitle'] = "第五栏 : 详细资料 (英文)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][6]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第六栏 : 类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][6]['AjaxLinkTitle'] = "按此查询类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][6]['AjaxLink'] = "CategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][7]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第七栏 : 子类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][7]['AjaxLinkTitle'] = "按此查询子类别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][7]['AjaxLink'] = "SubCategoryCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][8]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第八栏 : 管理组别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][8]['AjaxLinkTitle'] = "按此查询管理组别编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][8]['AjaxLink'] = "GroupCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][9]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第九栏 : 大楼编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][9]['AjaxLinkTitle'] = "按此查询大楼编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][9]['AjaxLink'] = "BuildingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][10]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十栏 : 位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][10]['AjaxLinkTitle'] = "按此查询位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][10]['AjaxLink'] = "LocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][11]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十一栏 : 子位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][11]['AjaxLinkTitle'] = "按此查询子位置编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][11]['AjaxLink'] = "SubLocationCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][12]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十二栏 : 资金来源编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][12]['AjaxLinkTitle'] = "按此查询资金来源编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][12]['AjaxLink'] = "FundingCode";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][13]['FieldTitle'] = "第十三栏 : 拥有权 (1 - 学校, 2 - 政府, 3 - 办学团体)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][14]['FieldTitle'] = "<span class='tabletextrequire'>*</span>第十四栏 : 误差总管";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][14]['AjaxLinkTitle'] = "按此查询误差总管";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][14]['AjaxLink'] = "VarianceManager";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][15]['FieldTitle'] = "第十五栏 : 数量";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][16]['FieldTitle'] = "第十六栏 : 供应商名称";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][17]['FieldTitle'] = "第十七栏 : 供应商联络";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][18]['FieldTitle'] = "第十八栏 : 供应商资料";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][19]['FieldTitle'] = "第十九栏 : 报价单编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][20]['FieldTitle'] = "第二十栏 : 标书编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][21]['FieldTitle'] = "第二十一栏 : 发票编号";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][22]['FieldTitle'] = "第二十二栏 : 购入日期 (YYYY-MM-DD)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][23]['FieldTitle'] = "第二十三栏 : 购买金额 (单据总额)";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][24]['FieldTitle'] = "第二十四栏 : 单位价格";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][25]['FieldTitle'] = "第二十五栏 : 维修资料";
	$Lang['eInventory']['FieldTitle']['Import']['Bulk'][26]['FieldTitle'] = "第二十六栏 : 备注";
	
	$Lang['eInventory']['FieldTitle']['BuildingCode'] = "大楼编号";
	$Lang['eInventory']['FieldTitle']['BuildingName'] = "大楼名称";
	$Lang['eInventory']['FieldTitle']['FloorCode'] = "位置编号";
	$Lang['eInventory']['FieldTitle']['FloorName'] = "位置名称";
	$Lang['eInventory']['FieldTitle']['RoomCode'] = "子位置编号";
	$Lang['eInventory']['FieldTitle']['RoomName'] = "子位置名称";
	$Lang['eInventory']['FieldTitle']['Import']['CorrecrBuildingIndexReminder'] = " * 请小心核对csv汇入档之大楼编号是否如上相同。";
	$Lang['eInventory']['FieldTitle']['Import']['CorrecrFloorIndexReminder'] = " * 请小心核对csv汇入档之位置编号是否如上相同。";
	$Lang['eInventory']['FieldTitle']['Import']['CorrecrRoomIndexReminder'] = " * 请小心核对csv汇入档之子位置编号是否如上相同。";
	$Lang['eInventory']['FieldTitle']['AllResourceMgmtGroup'] = " - 所有管理小组 - ";
	$Lang['eInventory']['FieldTitle']['AllFundingSource'] = " - 所有资金来源 - ";
	$Lang['eInventory']['FieldTitle']['AllCategory'] = " - 所有类别 - ";
	$Lang['eInventory']['FieldTitle']['AllType'] = " - 所有种类 - ";
	$Lang['eInventory']['FieldTitle']['StocktakeType']['AllItem'] = "显示所有物品";
	$Lang['eInventory']['FieldTitle']['StocktakeType']['NotDone'] = "只显示未完成盘点的";
	$Lang['eInventory']['FieldTitle']['DeletedStaff'] = "已离职职员";
	
	$Lang['eInventory']['FieldTitle']['Export']['BuildingCode'] = "Building Code";		// For inport use
	$Lang['eInventory']['FieldTitle']['Export']['Building'] = "Building";				// For inport use
	
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Title'] = "离线模式";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Online']['Title'] = "在线模式";
	
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step1'] = "选择 CSV 或 TXT 档案";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step2'] = "确认汇入资料";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Step3'] = "汇入结果";
	
	$Lang['eInventory']['FieldTitle']['Report']['ShowFundingName'] = "显示资金名称";
	
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['RowNum'] = "行#";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Location'] = "位置";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Category'] = "类别";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ItemName'] = "物品名称";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Code'] = "物品编号 / 条码";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Quantity'] = "数量";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['StocktakeDate'] = "日期";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['StocktakeTime'] = "时间";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Remark'] = "备注";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Import'] = "汇入";
	
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemShouldLocation'] = '该物品应该存放于';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['LocationNotFound'] = '位置不存在';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['ItemNotFound'] = '物品不存在';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidQty'] = '数量无效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidDate'] = '日期无效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidDateFormat'] = '日期格式无效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidTime'] = '时间无效';
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['IncorrectFileExtention'] = "0|=|资料档案无效";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['Error']['InvalidFileFormat'] = "0|=|档案格式无效";
	
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ImportMessage']['ReturnImportFail'] = "0|=|离线盘点记录汇入失败";
	$Lang['eInventory']['FieldTitle']['Stocktake']['Offline']['ImportMessage']['ReturnImportSuccess'] = "1|=|离线盘点记录汇入成功";
	
	$Lang['eInventory']['FieldTitle']['DownloadOfflineReaderApplication'] = "下载条码阅读器设定程式";
		
	$Lang['eInventory']['FieldTitle']['LocationBarcode'] = "位置条码";
	
	$Lang['eInventory']['FieldTitle']['EditInvoice'] = "编辑发票";
	$Lang['eInventory']['FieldTitle']['BarcodeWidth'] = "条码阔度";
	$Lang['eInventory']['JSWarning']['InvalidBarcodeWidth'] = "条码阔度无效";
	
	$Lang['eInventory']['FieldTitle']['NoOfSubCategory'] = "子类别数量";
	
	$i_InventorySystem_Category2ImportError[20] = "此类别不是由电子资源预订系统加入";
	$i_InventorySystem_Category2ImportError[21] = "此类别不是由资产管理行政系统加入";
	
	$Lang['eInventory']['WarningArr']['DataChangeAffect_eBooking'] = "资产管理行政系统及电子资源预订系统使用相同的类别设定，如你更新类别设定，更新的设定将会同时影响资产管理行政系统及电子资源预订系统。";
}

## iMail Words
$Lang['iMail']['FieldTitle']['Choose'] = "选择";
$Lang['iMail']['FieldTitle']['ByIdentity'] = "身份";
$Lang['iMail']['FieldTitle']['ByGroup'] = "小组";
$Lang['iMail']['FieldTitle']['Teacher'] = "教师";
$Lang['iMail']['FieldTitle']['NonTeachingStaff'] = "非教学职务员工";
$Lang['iMail']['FieldTitle']['Student'] = "学生";
$Lang['iMail']['FieldTitle']['Parent'] = "家长";

$Lang['iMail']['FieldTitle']['ToIndividualsTeachingStaff'] = "所有教师";
$Lang['iMail']['FieldTitle']['ToFormTeachingStaff'] = "同一班级的教师";
$Lang['iMail']['FieldTitle']['ToClassTeachingStaff'] = "同一班别的教师";
$Lang['iMail']['FieldTitle']['ToSubjectTeachingStaff'] = "同一学科的教师";
$Lang['iMail']['FieldTitle']['ToSubjectGroupTeachingStaff'] = "同一科组的教师";

$Lang['iMail']['FieldTitle']['ToIndividualsStudent'] = "所有学生";
$Lang['iMail']['FieldTitle']['ToFormStudent'] = "同一班级的学生";
$Lang['iMail']['FieldTitle']['ToClassStudent'] = "同一班别的学生";
$Lang['iMail']['FieldTitle']['ToSubjectStudent'] = "同一学科的学生";
$Lang['iMail']['FieldTitle']['ToSubjectGroupStudent'] = "同一科组的学生";

$Lang['iMail']['FieldTitle']['ToIndividualsParents'] = "所有家长";
$Lang['iMail']['FieldTitle']['ToFormParents'] = "同一班级的家长";
$Lang['iMail']['FieldTitle']['ToClassParents'] = "同一班别的家长";
$Lang['iMail']['FieldTitle']['ToSubjectParents'] = "同一学科的家长";
$Lang['iMail']['FieldTitle']['ToSubjectGroupParents'] = "同一学科组别的家长";
$Lang['iMail']['FieldTitle']['TargetParent'] = "的家长";
$Lang['iMail']['FieldTitle']['Group'] = "小组";
$Lang['iMail']['FieldTitle']['MyChildren'] = "我的子女";
$Lang['iMail']['FieldTitle']['MyParent'] = "我的父母";
$Lang['iMail']['FieldTitle']['Category'] = "类别";
$Lang['iMail']['FieldTitle']['SubCategory'] = "子类别";
$Lang['iMail']['FieldTitle']['NonDelivery'] = "以下用户未能接收此讯息，因为他们的邮箱已满。但你仍可以将没有附件之讯息发送给他们。";
$Lang['iMail']['FieldTitle']['EmailAddressSeparationNote'] = "请以 ; 或 , 分隔每个电邮地址";
$Lang['iMail']['ErrorMsg']['MailCannotSendOutSuccessfully'] = "邮件未能成功发送";
$Lang['iMail']['FieldTitle']['ChooseFromExternalRecipient'] = "选择外在收件人";
$Lang['iMail']['FieldTitle']['ChooseFromInternalRecipient'] = "选择内联网收件人";
$Lang['iMail']['FieldTitle']['PersonLeaveSchool'] = "已离校职员 / 学生";
$Lang['iMail']['FieldTitle']['TeacherAndStaff'] = "教师 / 非教学职务员工";
$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets1'] = "收件人名称不能含有( < )";
$Lang['iMail']['JSWarning']['ExternalRecipientNoTriangleBrackets2'] = "收件人名称不能含有( > )";
$Lang['iMail']['JSWarning']['ExternalRecipientNoComma'] = "收件人名称不能含有( , )";
$Lang['iMail']['FieldTitle']['Remark']['CommaCannotSeprateEmailInAutoComplete'] = "(在自动完成中\",\"不能用作分隔每个电邮地址)";
$Lang['iMail']['FieldTitle']['Reminder']['CannotReadMailContent_Title'] = "无法读取邮件内容？";
$Lang['iMail']['FieldTitle']['Reminder']['CannotReadMailContent_Body'] = "由Google Mail发出的邮件如并非以UTF-8编码，将无法显示。请参阅<a class='tablelink' target='_blank' href='http://mail.google.com/support/bin/answer.py?hl=en-GB&ctx=mail&answer=22841'>此文件</a>，了解设定方法。";


$Lang['ResourceBooking']['ImportNote1'] = "<ul><li>时间表名称必须完全正确, 否则资料不能汇入
<li><a href=javascript:newWindow('tscheme.php',0)>检视时间表内容</a>
<li><a href='/templates/get_sample_csv.php?file=itemsample_unicode.csv'> 下载范本档案 (CSV 格式)</a></ul>";

###  eCircular
if($special_feature['circular'])
{
	$Lang['Circular']['Settings'] = "设定";
	$Lang['Circular']['BasicSettings'] = "基本设定";
	$Lang['Circular']['AdminSettings'] = "管理权限设定";
	$Lang['Circular']['DisableCircular'] = "不使用". $Lang['Header']['Menu']['eCircular'];
	$Lang['Circular']['AllowHelpSigning'] = "容许管理人员替收件人更改回条";
	$Lang['Circular']['AllowLateSign'] = "容许迟交";
	$Lang['Circular']['AllowResign'] = "容许收件者更改已签回的回条";
	$Lang['Circular']['DefaultDays'] = "预设签署期限日数";
	$Lang['Circular']['TeacherCanViewAll'] = "让所有老师检视所有通告";
	$Lang['Circular']['AdminLevel_Normal'] = "一般权限";
	$Lang['Circular']['AdminLevel_Normal_Detail'] = $Lang['Circular']['AdminLevel_Normal']." (可发出教职员通告及观看自己所发通告的结果)";
	$Lang['Circular']['AdminLevel_Full'] = "完全控制";
	$Lang['Circular']['AdminLevel_Full_Detail'] = $Lang['Circular']['AdminLevel_Full'] ." (可发出教职员通告及观看所有通告的结果)";
	$Lang['Circular']['StaffName'] = "职员/老师姓名";
	$Lang['Circular']['AdminLevel'] = "管理权限";
	$Lang['Circular']['PleaseSelectStaff'] = "请选择职员/老师";
	$Lang['Circular']['SettingsUpdateSuccess'] = "<font color=green>设定成功</font>";
	$Lang['eNotice']['LateSubmission'] = "逾期签署";
	$Lang['eNotice']['NotAllowReSign'] = "不容许更改回条答案";
	$Lang['eNotice']['Audience'] = "适用对象";
}

### ePolling
	$Lang['Polling']['ToBeReleasedOn'] = '可检阅日期 :'; 
	$Lang['Polling']['InvalidDueDate'] ="限期必须是今天或以后";
	$Lang['Polling']['InvalidReleaseDate'] ="结果公布日期必须是开始日期或以后";
	
/* ------------------- Start of ePayment ------------------- */
if ($plugin['payment']) {
	$Lang['ePayment']['ManualCashInputSuccess'] = '1|=|手动输入成功';
	$Lang['ePayment']['ManualCashInputUnsuccess'] = '0|=|手动输入失败';
	$Lang['ePayment']['DateType'] = '时间类别';
	$Lang['ePayment']['CancelDepositSuccess'] = '1|=|取消记录成功';
	$Lang['ePayment']['CancelDepositUnsuccess'] = '0|=|取消记录失败';
	$Lang['ePayment']['PaymentItemCreateSuccess'] = '1|=|新增缴费项目成功';
	$Lang['ePayment']['PaymentItemCreateUnsuccess'] = '0|=|新增缴费项目失败';
	$Lang['ePayment']['DataImportSuccess'] = '1|=|资料汇入成功';
	$Lang['ePayment']['DataImportUnsuccess'] = '0|=|资料汇入失败';
	$Lang['ePayment']['PaymentStudentItemAddedSuccess'] = '1|=|同户新增成功';
	$Lang['ePayment']['PaymentStudentItemAddedUnsuccess'] = '0|=|用户新增失败';
	$Lang['ePayment']['PaymentStudentItemUpdatedSuccess'] = '1|=|用户更新成功';
	$Lang['ePayment']['PaymentStudentItemUpdatedUnsuccess'] = '0|=|用户更新失败';
	$Lang['ePayment']['PaymentItemDeletedSuccess'] = '1|=|缴费项目移除成功';
	$Lang['ePayment']['PaymentItemDeletedUnsuccess'] = '0|=|缴费项目移除失败';
	$Lang['ePayment']['PaymentItemEditSuccess'] = '1|=|缴费项目更新成功';
	$Lang['ePayment']['PaymentItemEditUnsuccess'] = '0|=|缴费项目更新失败';
	$Lang['ePayment']['PayProcessSuccess'] = '1|=|缴费成功';
	$Lang['ePayment']['PayProcessUnsuccess'] = '0|=|缴费失败';
	$Lang['ePayment']['PayUndoSuccess'] = '1|=|还原成功';
	$Lang['ePayment']['PayUndoUnsuccess'] = '0|=|还原失败';
	$Lang['ePayment']['ArchiveSuccess'] = "1|=|记录整存成功";
	$Lang['ePayment']['ArchiveUnsuccess'] = "0|=|记录整存失败";
	$Lang['ePayment']['ArchiveRestoreSuccess'] = "1|=|记录回复成功";
	$Lang['ePayment']['ArchiveRestoreUnsuccess'] = "0|=|记录回复失败";
	$Lang['ePayment']['CreateSubsidySourceSuccess'] = "1|=|资助来源新增成功";
	$Lang['ePayment']['CreateSubsidySourceUnsuccess'] = "0|=|资助来源新增失败";
	$Lang['ePayment']['UpdateSubsidySourceSuccess'] = "1|=|资助来源更新成功";
	$Lang['ePayment']['UpdateSubsidySourceUnsuccess'] = "0|=|资助来源更新失败";
	$Lang['ePayment']['RefundSuccess'] = "1|=|退款成功";
	$Lang['ePayment']['RefundFail'] = "0|=|退款失败";
	$Lang['ePayment']['DonateSuccess'] = "1|=|捐款成功";
	$Lang['ePayment']['DonateFail'] = "0|=|捐款失败";
	$Lang['ePayment']['PPSRemapSuccess'] = "1|=|缴费聆增值成功";
	$Lang['ePayment']['PPSRemapFail'] = "0|=|缴费聆增值失败";
	$Lang['ePayment']['UnknownRecordSuccess'] = "1|=|不明缴费聆纪录移除成功";
	$Lang['ePayment']['UnknownRecordFail'] = "0|=|不明缴费聆纪录移除失败";
	$Lang['ePayment']['PhotocopyPackageCreateSuccess'] = "1|=|影印套票新增成功";
	$Lang['ePayment']['PhotocopyPackageCreateFail'] = "0|=|影印套票新增失败";
	$Lang['ePayment']['PhotocopyPackageEditSuccess'] = "1|=|影印套票更新成功";
	$Lang['ePayment']['PhotocopyPackageEditFail'] = "0|=|影印套票更新失败";
	$Lang['ePayment']['PhotocopyPackageDeleteSuccess'] = "1|=|影印套票移除成功";
	$Lang['ePayment']['PhotocopyPackageDeleteFail'] = "0|=|影印套票移除失败";
	$Lang['ePayment']['PrintQuotaEditSuccess'] = "1|=|配额更新成功";
	$Lang['ePayment']['PrintQuotaEditFail'] = "0|=|配额更新失败";
	$Lang['ePayment']['PrintQuotaNewSuccess'] = "1|=|配额新增成功";
	$Lang['ePayment']['PrintQuotaNewFail'] = "0|=|配额新增失败";
	$Lang['ePayment']['PrintQuotaResetSuccess'] = "1|=|配额重设成功";
	$Lang['ePayment']['PrintQuotaResetFail'] = "0|=|配额重设失败";
	$Lang['ePayment']['PayAll'] = "全部缴款";
	$Lang['ePayment']['PPSChargeWarning'] = "缴费聆手续费($";
	$Lang['ePayment']['PPSChargeWarning1'] = ", Counter Bill 则为 $";
	$Lang['ePayment']['PPSChargeWarning2'] = ")将自动从所有交易项目中扣除。";
	$Lang['ePayment']['CashDepositImportSuccess'] = "1|=|汇入现金存入记录成功";
	$Lang['ePayment']['PaymentItemStudentDeleteSuccess'] = "1|=|用户移除成功";
	$Lang['ePayment']['PaymentItemStudentDeleteFail'] = "0|=|用户移除失败";
	$Lang['ePayment']['IncludeNotStarted'] = "包括所有未开始的缴费项目";
	$Lang['ePayment']['IEPS'] = "PPS - IEPS";
	$Lang['ePayment']['CounterBill'] = "PPS - CounterBill";
}
/* ------------------- End of ePayment -------------------- */

/* ------------------- Start of Student Attendance ------------------- */
if ($plugin['attendancestudent']) {
	$Lang['StudentAttendance']['SettingApplySuccess'] = "1|=|设定更新成功";
	$Lang['StudentAttendance']['SettingApplyFail'] = "0|=|设定更新失败";
	$Lang['StudentAttendance']['HelperListUpdateSuccess'] = "1|=|学生名单更新成功";
	$Lang['StudentAttendance']['HelperListUpdateFail'] = "0|=|学生名单更新失败";
	$Lang['StudentAttendance']['DailyStatusConfirmSuccess'] = "1|=|每日记录确定成功";
	$Lang['StudentAttendance']['DailyStatusConfirmFail'] = "0|=|每日记录确定失败";
	$Lang['StudentAttendance']['LateListConfirmSuccess'] = "1|=|每日迟到记录确定成功";
	$Lang['StudentAttendance']['LateListConfirmFail'] = "0|=|每日迟到记录确定失败";
	$Lang['StudentAttendance']['AbsenceListConfirmSuccess'] = "1|=|每日缺席记录确定成功";
	$Lang['StudentAttendance']['AbsenceListConfirmFail'] = "0|=|每日缺席记录确定失败";
	$Lang['StudentAttendance']['EarlyLeaveCreateSuccess'] = "1|=|早退记录新增成功";
	$Lang['StudentAttendance']['EarlyLeaveCreateFail'] = "0|=|早退记录新增失败";
	$Lang['StudentAttendance']['EarlyLeaveListConfirmSuccess'] = "1|=|早退记录确定成功";
	$Lang['StudentAttendance']['EarlyLeaveListConfirmFail'] = "0|=|早退记录确定失败";
	$Lang['StudentAttendance']['OfflineRecordImportSuccess'] = "1|=|离线资料汇入成功";
	$Lang['StudentAttendance']['OfflineRecordImportFail'] = "0|=|离线资料汇入失败";
	$Lang['StudentAttendance']['EntryReasonUpdatedSuccess'] = "1|=|出入原因更新成功";
	$Lang['StudentAttendance']['EntryReasonUpdatedFail'] = "0|=|出入原因更新失败";
	$Lang['StudentAttendance']['PresetAbsenceUpdateSuccess'] = "1|=|学生请假纪录更新成功";
	$Lang['StudentAttendance']['PresetAbsenceUpdateFail'] = "0|=|学生请假纪录更新失败";
	$Lang['StudentAttendance']['PresetAbsenceDeleteSuccess'] = "1|=|学生请假纪录移除成功";
	$Lang['StudentAttendance']['PresetAbsenceDeleteFail'] = "0|=|学生请假纪录移除失败";
	$Lang['StudentAttendance']['PresetAbsenceCreateSuccess'] = "1|=|学生请假纪录新增成功";
	$Lang['StudentAttendance']['PresetAbsenceCreateFail'] = "0|=|学生请假纪录新增失败";
	$Lang['StudentAttendance']['MonthlyDataDeleteSuccess'] = "1|=|清除过时资料成功";
	$Lang['StudentAttendance']['MonthlyDataDeleteFail'] = "0|=|清除过时资料失败";
	$Lang['StudentAttendance']['OutingRecordCreateSuccess'] = "1|=|外出纪录新增成功";
	$Lang['StudentAttendance']['OutingRecordCreateFail'] = "0|=|外出纪录新增失败";
	$Lang['StudentAttendance']['OutingRecordUpdateSuccess'] = "1|=|外出纪录更新成功";
	$Lang['StudentAttendance']['OutingRecordUpdateFail'] = "0|=|外出纪录更新失败";
	$Lang['StudentAttendance']['OutingRecordDeleteSuccess'] = "1|=|外出纪录移除成功";
	$Lang['StudentAttendance']['OutingRecordDeleteFail'] = "0|=|外出纪录移除失败";
	$Lang['StudentAttendance']['DetentionRecordCreateSuccess'] = "1|=|留堂纪录新增成功";
	$Lang['StudentAttendance']['DetentionRecordCreateFail'] = "0|=|留堂纪录新增失败";
	$Lang['StudentAttendance']['DetentionRecordUpdateSuccess'] = "1|=|留堂纪录更新成功";
	$Lang['StudentAttendance']['DetentionRecordUpdateFail'] = "0|=|留堂纪录更新失败";
	$Lang['StudentAttendance']['DetentionRecordDeleteSuccess'] = "1|=|留堂纪录移除成功";
	$Lang['StudentAttendance']['DetentionRecordDeleteFail'] = "0|=|留堂纪录移除失败";
	$Lang['StudentAttendance']['ReminderRecordCreateSuccess'] = "1|=|老师约见提示新增成功";
	$Lang['StudentAttendance']['ReminderRecordCreateFail'] = "0|=|老师约见提示新增失败";
	$Lang['StudentAttendance']['ReminderRecordUpdateSuccess'] = "1|=|老师约见提示更新成功";
	$Lang['StudentAttendance']['ReminderRecordUpdateFail'] = "0|=|老师约见提示更新失败";
	$Lang['StudentAttendance']['ReminderRecordDeleteSuccess'] = "1|=|老师约见提示移除成功";
	$Lang['StudentAttendance']['ReminderRecordDeleteFail'] = "0|=|老师约见提示移除失败";
	$Lang['StudentAttendance']['LeaveOptionSaveSuccess'] = "1|=|学生离校选项更新成功";
	$Lang['StudentAttendance']['LeaveOptionSaveFail'] = "0|=|学生离校选项更新失败";
	$Lang['StudentAttendance']['CardIDUpdateSuccess'] = "1|=|智慧卡 ID 更新成功";
	$Lang['StudentAttendance']['CardIDUpdateFail'] = "0|=|智慧卡 ID 更新失败";
	$Lang['StudentAttendance']['ProfileAttendCount'] = "出勤纪录 - 缺席之统计方法设定";
	$Lang['StudentAttendance']['SMSSentSuccessfully'] = "1|=|简讯发送成功";
	$Lang['StudentAttendance']['DataOutOfDate'] = "0|=|资料逾期";
	$Lang['StudentAttendance']['AttendLateSymbol'] = "O";
	$Lang['StudentAttendance']['TeacherIAccountStudentProfileEdit'] = '允许班导师在"我的用户资料"修改学生档案记录原因';
	$Lang['StudentAttendance']['ImportOldTerminalOfflineRecord'] = '从终端机离线记录汇入';
	$Lang['StudentAttendance']['PeriodType'] = '日期范围类别';
	$Lang['StudentAttendance']['CurrentPeriodDay'] = '日期';
	$Lang['StudentAttendance']['AttendanceType'] = '出勤类型';
	
	$Lang['StudentAttendance']['ImportAttendenceData'] = '汇入出勤资料';
	$Lang['StudentAttendance']['ImportAttendDataFormatDesc'] = '第一栏 : Class<br />
																第二栏 : Class No.<br />
																第三栏 : Date (YYYY-MM-DD)<br />
																第四栏 : AMInStatus (1 = 缺席, 2 = 迟到)<br />
																第五栏 : AMInTime (HH:MM:SS)<br />
																第六栏 : AMInWaive (1 = 豁免, 0 = 不豁免)<br />
																第七栏 : AMInReason<br />
																第八栏 : AMOutStatus (3 = 早退)<br />
																第九栏 : AMOutWaive (1 = 豁免, 0 = 不豁免)<br />
																第十栏 : AMOutReason<br />
																第十一栏 : PMInStatus (1 = 缺席, 2 = 迟到)<br />
																第十二栏 : PMInTime (HH:MM:SS)<br />
																第十三栏 : PMInWaive (1 = 豁免, 0 = 不豁免)<br />
																第十四栏 : PMInReason<br />
																第十五栏 : PMOutStatus (3 = 早退)<br />
																第十六栏 : PMOutWaive (1 = 豁免, 0 = 不豁免)<br />
																第十七栏 : PMOutReason<br />';
	$Lang['StudentAttendance']['StudentName'] = "学生姓名";
	$Lang['StudentAttendance']['Class'] = "班别";
	$Lang['StudentAttendance']['ClassNumber'] = "学号";
	$Lang['StudentAttendance']['AMInStatus'] = "上午进入状况";
	$Lang['StudentAttendance']['AMInTime'] = "上午进入时间";
	$Lang['StudentAttendance']['AMInWaive'] = "上午进入豁免";
	$Lang['StudentAttendance']['AMInReason'] = "上午进入原因";
	$Lang['StudentAttendance']['AMOutStatus'] = "上午离开状况";
	$Lang['StudentAttendance']['AMOutWaive'] = "上午离开豁免";
	$Lang['StudentAttendance']['AMOutReason'] = "上午离开原因";
	$Lang['StudentAttendance']['PMInStatus'] = "下午进入状况";
	$Lang['StudentAttendance']['PMInTime'] = "下午进入时间";
	$Lang['StudentAttendance']['PMInWaive'] = "下午进入豁免";
	$Lang['StudentAttendance']['PMInReason'] = "下午进入原因";
	$Lang['StudentAttendance']['PMOutStatus'] = "下午离开状况";
	$Lang['StudentAttendance']['PMOutWaive'] = "下午离开豁免";
	$Lang['StudentAttendance']['PMOutReason'] = "下午离开原因";
	$Lang['StudentAttendance']['Present'] = "正常";
	$Lang['StudentAttendance']['Absent'] = "缺席";
	$Lang['StudentAttendance']['Late'] = "迟到";
	$Lang['StudentAttendance']['EarlyLeave'] = "早退";
	$Lang['StudentAttendance']['Outing'] = "外出活动";
	$Lang['StudentAttendance']['Waived'] = "豁免";
	$Lang['StudentAttendance']['NotWaived'] = "不豁免";
	$Lang['StudentAttendance']['DataDistribution'] = "数据分布";
	$Lang['StudentAttendance']['PMNotFollowAM'] = "默认下午状况不跟上午状况";
	$Lang['StudentAttendance']['DailyAbsentAnalysisReport'] = "每日缺席分析报告";
	$Lang['StudentAttendance']['AbsentSessionType'] = "缺席时段类别";
	$Lang['StudentAttendance']['BothAMPMAbsent'] = "上午和下午皆缺席";
	$Lang['StudentAttendance']['OnlyAMAbsent'] = "只是上午缺席";
	$Lang['StudentAttendance']['OnlyPMAbsent'] = "只是下午缺席";
	$Lang['StudentAttendance']['EntryLeaveDate'] = "学生入学/离校日期设定";
	$Lang['StudentAttendance']['EntryDate'] = "入学";
	$Lang['StudentAttendance']['LeaveDate'] = "离校";
	$Lang['StudentAttendance']['EntryLeavePeriodOverlapWarning'] = "*入学/离校期重迭";
	$Lang['StudentAttendance']['EntryLeavePeriodNullWarning'] = "*请最少输入开始日期或结束日期";
	$Lang['StudentAttendance']['EntryLeavePeriodEndDateLesserThenStartDateWarning'] = "*结束日期必须在开始日期之后";
	$Lang['StudentAttendance']['EntryPeriodSaveSuccess'] = "1|=|入学/离校期储存成功 ";
	$Lang['StudentAttendance']['EntryPeriodSaveFail'] = "0|=|入学/离校期储存失败 ";
	$Lang['StudentAttendance']['EntryPeriodRemoveSuccess'] = "1|=|入学/离校期移除成功 ";
	$Lang['StudentAttendance']['EntryPeriodRemoveFail'] = "0|=|入学/离校期移除失败 ";
	$Lang['StudentAttendance']['EnableEntryLeavePeriodSetting'] = "开启".$Lang['StudentAttendance']['EntryLeaveDate']."设定";
	$Lang['StudentAttendance']['SelectCSVFile'] = "选择CSV档案";
	$Lang['StudentAttendance']['CSVConfirmation'] = "确定";
	$Lang['StudentAttendance']['ImportResult'] = "汇入结果";
	$Lang['StudentAttendance']['EntryPeriodImportDesc'] = "第一栏 : 学生登入名称<br />第二栏 : 入学日期 (YYYY-MM-DD)<br />第三栏 : 离校日期 (YYYY-MM-DD)";
	$Lang['StudentAttendance']['DataImportFail'] = "0|=|资料汇入失败";
	$Lang['StudentAttendance']['UserLogin'] = "用户登入名称";
	$Lang['StudentAttendance']['InvalidDateFormat'] = '日期格式错误';
	$Lang['StudentAttendance']['ClassTeacherTakeOwnClassOnlySetting'] = '在我的智能卡纪录中教职员只可为有关的班别点名';
	$Lang['StudentAttendance']['HasHandinParentLetter'] = "已交家长信";
	$Lang['StudentAttendance']['Notes'] = "附加资料";
	$Lang['StudentAttendance']['SlotTimeFormatWarning'] = "*时间格式不符";
	$Lang['StudentAttendance']['SummaryStatistic'] = "简要纪录";
	$Lang['StudentAttendance']['DaysOfAbsent'] = "缺席日数";
	$Lang['StudentAttendance']['DaysOfLate'] = "迟到日数";
	$Lang['StudentAttendance']['DaysOfEarlyLeave'] = "早退日数";
	$Lang['StudentAttendance']['LateSessions'] = "迟到堂数";
	$Lang['StudentAttendance']['LateSessionsWarning'] = "请于迟到堂数输入正整数";
	$Lang['StudentAttendance']['LateAbsentSessions'] = "迟到/缺席堂数";
	$Lang['StudentAttendance']['LateAbsentSessionsWarning'] = "请于迟到/缺席堂数输入正整数";
	$Lang['StudentAttendance']['ImportRFID'] = "汇入无线射频辨识ID";
	$Lang['StudentAttendance']['RFID'] = "无线射频辨识ID";
	$Lang['StudentAttendance']['SymbolPresent'] = "<span >/</span>";
	$Lang['StudentAttendance']['SymbolAbsent'] = "<span >O</span>";
	$Lang['StudentAttendance']['SymbolLate'] = "<span lang=EN-US style='font-family:Symbol;mso-ascii-font-family:新细明体;mso-hansi-font-family:新细明体;mso-char-type:symbol;mso-symbol-font-family:Symbol'><span style='mso-char-type:symbol;mso-symbol-font-family:Symbol'>&AElig;</span></span>";
	$Lang['StudentAttendance']['SymbolOuting'] = "z";
	$Lang['StudentAttendance']['SymbolEarlyLeave'] = "<span class=GramE><span lang=EN-US style='font-family:新细明体'>#</span></span>";
	$Lang['StudentAttendance']['SymbolSL'] = "<span style='font-family:新细明体'>⊕</span>";
	$Lang['StudentAttendance']['SymbolAR'] = "<span lang=EN-US style='font-family:Webdings;mso-ascii-font-family:新细明体;mso-hansi-font-family:新细明体;mso-char-type:symbol;mso-symbol-font-family:Webdings'><span style='mso-char-type:symbol;mso-symbol-font-family:Webdings'>y</span></span>";
	$Lang['StudentAttendance']['SymbolLE'] = "<span lang=EN-US style='font-family:\"Wingdings 2\";mso-ascii-font-family:新细明体;mso-hansi-font-family:新细明体;mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'><span style='mso-char-type:symbol;mso-symbol-font-family:\"Wingdings 2\"'>U</span></span>";
	$Lang['StudentAttendance']['SymbolTruancy'] = "<span lang=EN-US style='font-family:新细明体'>●</span>";
	$Lang['StudentAttendance']['SymbolWaived'] = "<span class=GramE><span lang=EN-US style='font-family:新细明体'>*</span></span>";
	$Lang['StudentAttendance']['NotationSymbols'] = "考勤符号";
	$Lang['StudentAttendance']['MonthlyStatistic'] = "本月统计";
	$Lang['StudentAttendance']['DailyStatistic'] = "每日统计";
	$Lang['StudentAttendance']['NumberOfAttendants'] = "在席人数";
	$Lang['StudentAttendance']['AveragePresentNumber'] = "平均出席人数";
	$Lang['StudentAttendance']['AverageAbsentNumber'] = "平均缺席人数";
	$Lang['StudentAttendance']['NumberOfPresent'] = "出席数";
	$Lang['StudentAttendance']['NumberOfAbsent'] = "缺席数";
	$Lang['StudentAttendance']['NumberOfLate'] = "迟到数";
	$Lang['StudentAttendance']['NumberOfEarlyLeave'] = "早退数";
	$Lang['StudentAttendance']['NumberOfSchoolDays'] = "本月上课共";
	$Lang['StudentAttendance']['Day'] = "日";
	$Lang['StudentAttendance']['MonthShortForm'] = array("一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月");
	$Lang['StudentAttendance']['ClassName'] = "班别";
	$Lang['StudentAttendance']['Gender'] = "性别";
	$Lang['StudentAttendance']['Others'] = "其它";
	$Lang['StudentAttendance']['Weekday'] = "星期";
	$Lang['StudentAttendance']['AM'] = "上午";
	$Lang['StudentAttendance']['PM'] = "下午";
	$Lang['StudentAttendance']['Confirmed'] = "纪录已确认";
	$Lang['StudentAttendance']['DefaultAbsentReason'] = "默认缺席原因 (如没其他设定)";
	$Lang['StudentAttendance']['DefaultLateReason'] = "默认迟到原因 (如没其他设定)";
	$Lang['StudentAttendance']['DefaultEarlyReason'] = "默认早退原因 (如没其他设定)";
	$Lang['StudentAttendance']['ViewWebsamsReasonCode'] = "检视WebSAMS 原因代码";
	$Lang['StudentAttendance']['OutingDate'] = "活动/外出日期";
	$Lang['StudentAttendance']['OutingStartTime'] = "活动/外出时间";
	$Lang['StudentAttendance']['OutingEndTime'] = "结束时间";
	$Lang['StudentAttendance']['OutingPIC'] = "负责老师";
	$Lang['StudentAttendance']['OutingFrom'] = "出发地点";
	$Lang['StudentAttendance']['OutingTo'] = "活动/外出地点";
	$Lang['StudentAttendance']['OutingReason'] = "活动/外出目的";
	$Lang['StudentAttendance']['OutingNotStartFromSchool'] = "不是由学校出发";
	$Lang['StudentAttendance']['OutingNotYetBack'] = "未/没有返回";
	$Lang['StudentAttendance']['OutingRemark'] = "备注";
	$Lang['StudentAttendance']['DayType'] = "时段";
	$Lang['StudentAttendance']['DayTypeWD'] = "全日";
	$Lang['StudentAttendance']['DayTypeAM'] = "上午";
	$Lang['StudentAttendance']['DayTypePM'] = "下午";
	$Lang['StudentAttendance']['SelectSessionWarning'] = "请选择欲设定的时段";
	$Lang['StudentAttendance']['InvalidTimeWarning'] = "请输入有效的时间";
	$Lang['StudentAttendance']['ImpotOutingDescription'] = "
			第一栏 : 班别 <br>
			第二栏 : 班号 <br>
			第三栏 : 日期 (如留空即为<b>今天</b>) <br>
			第四栏 : 时段 (AM/PM) <br>
			第五栏 : 负责老师姓名 <br>
			第六栏 : 参考外出时间 (HH:mm:ss) <br>
			第七栏 : 参考结束时间 (HH:mm:ss) <br>
			第八栏 : 原因
			";
	$Lang['StudentAttendance']['StudentNotFound'] = "找不到该学生";
	$Lang['StudentAttendance']['ImportMoreThanOneStudentFound'] = "找到多于一位学生";
	$Lang['StudentAttendance']['PresetOutingPICNotFound'] = "找到到负责人";
	$Lang['StudentAttendance']['PresetOutingSetPreviously'] = "系统中找到相同日期, 时段, 学生的纪录";
	$Lang['StudentAttendance']['PresetOutingSetOnSameFile'] = "在同一档中找到相同日期, 时段, 学生的纪录";
	$Lang['StudentAttendance']['PresetOutMUSTFileMissed'] = "请最少输入班名/班号及时段";
	$Lang['StudentAttendance']['PresetOutTimeSlotMissMatch'] = "时段必须为AM或PM";
	$Lang['StudentAttendance']['ApplyToTheseClassAlso'] = "也套用到这些班别上(选择性)";
	$Lang['StudentAttendance']['ApplyToTheseGroupAlso'] = "也套用到这些组别上(选择性)";
	$Lang['StudentAttendance']['FilterNoDataDate'] = "隐藏没有资料的日期";
	$Lang['StudentAttendance']['LunchOut'] = "中午外出";
	$Lang['StudentAttendance']['LastConfirmPerson'] = "最后确认人 ";
	$Lang['StudentAttendance']['Outgoing'] = "外出活动 ";
	$Lang['StudentAttendance']['UnConfirmed'] = "未确认 ";
	$Lang['StudentAttendance']['OfficalLeave'] = "公假";
	$Lang['StudentAttendance']['RequestLeave'] = "请假";
	$Lang['StudentAttendance']['PlayTruant'] = "旷课";
	$Lang['StudentAttendance']['SessionsStat'] = "课节统计";
	$Lang['StudentAttendance']['iSmartCardRemark'] = "老师备注";
	$Lang['StudentAttendance']['eNoticeTemplate'] = "电子通告模板";
	$Lang['StudentAttendance']['CumulativeProfile'] = "累积缺席/迟到";
	$Lang['StudentAttendance']['eNoticeTemplateActive'] = "使用中";
	$Lang['StudentAttendance']['eNoticeTemplateInActive'] = "非使用中";
	$Lang['StudentAttendance']['eNoticeTemplateName'] = "模板名称 ";
	$Lang['StudentAttendance']['eNoticeTemplateCategory'] = "种类 ";
	$Lang['StudentAttendance']['eNoticeTemplateStatus'] = "状况";
	$Lang['StudentAttendance']['eNoticeTemplateReplySlip'] = "附有回条";
	$Lang['StudentAttendance']['eNoticeTemplatePreview'] = "预览";
	$Lang['StudentAttendance']['eNoticeTemplateSubject'] = "标题";
	$Lang['StudentAttendance']['eNoticeTemplateContent'] = "内容";
	$Lang['StudentAttendance']['RecordDate'] = "日期";
	$Lang['StudentAttendance']['AdditionInfo'] = "附加资料";
	$Lang['StudentAttendance']['TimeSlot'] = "时段";
	$Lang['StudentAttendance']['Reason'] = "原因";
	$Lang['StudentAttendance']['FromDate'] = "开始日期";
	$Lang['StudentAttendance']['ToDate'] = "结束日期";
	$Lang['StudentAttendance']['TotalCount'] = "总数";
	$Lang['StudentAttendance']['Insert'] = "加入";
	$Lang['StudentAttendance']['eNotice']['TemplateNameWarning'] = "请输入".$Lang['StudentAttendance']['eNoticeTemplateName'];
	$Lang['StudentAttendance']['eNotice']['SubjectWarning'] = "请输入".$Lang['StudentAttendance']['eNoticeTemplateSubject'];
	$Lang['StudentAttendance']['eNotice']['TemplateSaveSuccess'] = "1|=|".$Lang['StudentAttendance']['eNoticeTemplate']."储存成功 ";
	$Lang['StudentAttendance']['eNotice']['TemplateSaveFail'] = "0|=|".$Lang['StudentAttendance']['eNoticeTemplate']."储存失败 ";
	$Lang['StudentAttendance']['eNotice']['TemplateDeleteSuccess'] = "1|=|".$Lang['StudentAttendance']['eNoticeTemplate']."移除成功 ";
	$Lang['StudentAttendance']['eNotice']['TemplateDeleteFail'] = "0|=|".$Lang['StudentAttendance']['eNoticeTemplate']."移除失败 ";
	$Lang['StudentAttendance']['SendNotice'] = "传送电子通告";
	$Lang['StudentAttendance']['CheckSendNotice'] = "请选择欲传送电子通告的同学";
	$Lang['StudentAttendance']['ShowIfCountMoreThan'] = "显示当纪绿多于";
	$Lang['StudentAttendance']['CumulativeProfileNotice'] = "累积缺席/迟到者发函";
	$Lang['StudentAttendance']['CountMoreThanWarning'] = "此栏必须为数字及不能留空";
	$Lang['StudentAttendance']['eNotice']['NoticeSentSuccess'] = "1|=|电子通告传送成功 ";
	$Lang['StudentAttendance']['eNotice']['PrintNotice'] = "列印电子通告";
	$Lang['StudentAttendance']['ViewWEBSAMSReasonRemark'] = "如需更改，请前往行政综合平台 > 内联网设定 > WebSAMS 整合 > 原因代码管理";
}
/* ------------------- End of Student Attendance -------------------- */

/* ------------------- Start of Lesson Attendance -------------------- */
if ($plugin['attendancelesson']) {
	$Lang['LessonAttendance']['ClassFloorPlanSetup'] = '班别座位表';
	$Lang['LessonAttendance']['ImportLessonAttendanceAttendDataFormatDesc'] = '
																第一栏 : 班别名称<br />
																第二栏 : 座号<br />
																第三栏 : 日期 (YYYY-MM-DD)<br />
																第四栏 : 时间表时段 (1 = 时段 1, 2 = 时段 2 ...etc)<br />
																第五栏 : 出勤状况 (2 = 迟到, 3 = 缺席)<br />
																';
}
/* ------------------- End of Lesson Attendance -------------------- */

###  eNotice
if($plugin['notice'])
{
	$Lang['eNotice']['SchoolNotice'] = "学校通告";
	$Lang['eNotice']['DisciplineNotice'] = "操行纪录系统通告";
	$Lang['eNotice']['Settings'] = "设定";
	$Lang['eNotice']['BasicSettings'] = "基本设定";
	$Lang['eNotice']['DisableNotice'] = "不使用". $Lang['Header']['Menu']['eNotice'];
	$Lang['eNotice']['FullControlGroup'] = "进阶管理小组<br>(可删除通告及代家长更改回条)";
	$Lang['eNotice']['NormalControlGroup'] = "一般管理小组<br>(可发通告及检视回条)";
	$Lang['eNotice']['DisciplineGroup'] = "整批列印操行纪录系统通告";
	$Lang['eNotice']['DisableClassTeacher'] = "不让班导师代家长更改回条";
	$Lang['eNotice']['AllHaveRight'] = "所有教职员可发通告";
	$Lang['eNotice']['DefaultNumDays'] = "预设签署期限日数";
	$Lang['eNotice']['AdminGroupOnly'] = "必须为行政小组";
	$Lang['eNotice']['ParentStudentCanViewAll'] = "让所有家长及学生检视所有通告";
	$Lang['eNotice']['SettingsUpdateSuccess'] = "<font color=green>设定成功</font>";
}

### eSports
if($plugin['Sports']||$plugin['swimming_gala'])
{
	$Lang['Sports']['AdminType'] = "管理员类别";
	$Lang['Sports']['Admin'] = "管理员";
	$Lang['Sports']['Helper'] = "助手";
	$Lang['Sports']['StaffName'] = "职员/老师姓名";
	$Lang['Sports']['HelperName'] = "学生/职员/老师姓名";
 	$Lang['Sports']['AdminSettings'] = "管理设定";
	$Lang['Sports']['AdminLevel_Helper_Detail'] = "";
	$Lang['Sports']['AdminLevel_Admin_Detail'] = "";
	$i_Sports_menu_Admin_Settings = "";
	
	$Lang["eSports"]["Class"] = "班别";
	$Lang["eSports"]["Form"] = "年级";
	$Lang['eSports']['csv']['ClassTitle'] = "班别";
	$Lang['eSports']['csv']['ClassNumber'] = "学号";
	$Lang['eSports']['csv']['EventCode'] = "项目编号";

	$Lang['eSports']['ErrorLog'] = "错误纪录";
	$Lang['eSports']['warnDeleteEnrolRecord'] = "所有目标班别现存的报名纪录会被删除，继续？";
	$Lang['eSports']['warnNoRecordInserted'] = "资料未被储存，请更正错误后再试";
	$Lang['eSports']['Error'] = "错误";
	$Lang['eSports']['DuplicateRecord'] = "项目重复";
	$Lang["eSports"]["WrongAgeGroup"] = "此学生不属于此项目所属的年龄组别";
	$Lang['eSports']['ExceedTotalQuota'] = "超出报名限额(总数)";
	$Lang['eSports']['ExceedQuota'][1] = "超出报名限额(径赛)";
	$Lang['eSports']['ExceedQuota'][2] = "超出报名限额(田赛)";
	$Lang['eSports']['StudentNotExist'] = "没有此学生";
	$Lang['eSports']['NotInTargetClasses'] = "此学生不属于目标班别 / 级别";
	$Lang['eSports']['NoError'] = "没有错误";
	$Lang['eSports']['ImportStatus'] = "汇入状况";

	$Lang['eSports']['EventCode'] = "项目编号";
	$Lang['SwimmingGala']['ExceedQuota'] = "超出报名限额";
	$Lang['eSports']['warnSelectClass'] = "请选择最少一个级别 / 班别";
	$Lang['eSports']['warnSelectcsvFile'] = "请选择汇入的档案";
	$Lang["eSports"]["NoSuchEvent"] = "没有此项目";
	$Lang['eSports']['warnNoEventCode'] = "请输入项目编号";
	
	$Lang['eSports']['FinalRoundSettingWarning'] = "参赛者数目如不超过每组人数数目，赛事将直入决赛。";
	$Lang['eSports']['AutoArrangeWarning'] = "你是否确定要继续自动安排赛程? 目前的赛程安排将被复写，而且不可恢复。";
	$i_Sports_Rank_Pattern = "名次排列";
	$i_Sports_New_Record_Delete = "<font color=green>新纪录已删除</font>";

	$Lang['eSports']['IndividualEvent'] = "个人项目";
	$Lang['eSports']['Individual'] = "个人";
	
	$Lang['eSports']['ExportParticipateCertificate'] = "汇出出席证书";
	
}

### Calendar
$Lang['Calendar']['PrevMonth'] = "上一月";
$Lang['Calendar']['NextMonth'] = "下一月";


### School Setting -> Group
$Lang['Group']['SetAdmin'] = "设定为小组管理员";
$Lang['Group']['CancelAdmin'] = "设定为一般组员";
$Lang['Group']['SetAsMember'] = "设定为一般组员";
$Lang['Group']['Update'] = "更新";
$Lang['Group']['NewRole'] = "新增组别职位";
$Lang['Group']['GroupMgmt'] = "小组";
$Lang['Group']['GroupMgmtCtr'] = "小组";
$Lang['Group']['NoOfMember'] = "组员数量";
$Lang['Group']['Role']['PresetValue'] = "设定为预设";
$Lang['Group']['RoleSetting'] = "小组职位";

$Lang['Group']['GroupCatSetting'] = "小组类别";
$Lang['Group']['WarnHasGroup'] = "方框为黄色的小组类别已设有小组，不能删除。";
$Lang['Group']['DefaultGroup'] = "预设小组类别";

$Lang['Group']['GroupName'] = "名称";
$Lang['Group']['GroupDescription'] = "描述";
$Lang['Group']['Category'] = "类别";
$Lang['Group']['GroupCategory'] = "小组类别";
$Lang['Group']['Type'] = "类型";
$Lang['Group']['StorageQuota'] = "小组储存量(MBytes)";
$Lang['Group']['CanUsePublicAnnouncementEvents'] = "可发放公众宣布、事项通知";
$Lang['Group']['AllowUsingAllGroupTools'] = "允许使用所有小组工具";
$Lang['Group']['AllowUsingGroupTools'] = "只允许使用勾选的小组工具";
$Lang['Group']['SendEmail'] = "发送电子邮件";
$Lang['Group']['GroupAdmin'] = "小组管理员";
$Lang['Group']['SubjectLeader'] = "科长";
$Lang['Group']['QuickAssignAs'] = "立即设定为";
$Lang['Group']['UserList'] = "用户名单";
$Lang['Group']['SelectGroupCategory'] = "选择小组类别";
$Lang['Group']['Recipient'] = "收件者";
$Lang['Group']['Subject'] = "主题";
$Lang['Group']['Comment'] = "内容";
$Lang['Group']['DefaultGroupRole'] = "预设小组职位";
$Lang['Group']['ViewThisCategoryOfGroupOnly'] = "只检视此类别下之小组：";

$Lang['Group']['jsWarning']['SelectGroupCategory'] = "请选择一小组类别。";
$Lang['Group']['warnEmptyGroupName'] = '没有小组名称';
$Lang['Group']['warnPositiveNum'] = '存储量必须为正数';
$Lang['Group']['warnGroupCatNotExist'] = '没有此小组分类';

$Lang['Group']['GroupMember'] = "组员";
$Lang['Group']['StudentNotExist'] = "没有此学生";

$Lang['Group']['EmptyGroupName'] = "没有输入组别";
$Lang['Group']['GroupNotExist'] = "没有此组别";
$Lang['Group']['GroupNotInCurrentYear'] = "组别不属于本年度";

$Lang['Group']['UserNotExist'] = "没有此用户";
$Lang['Group']['EmptyUserInfo'] = "没有用户资料";
$Lang['Group']['IsIdentityGroup'] = "不能加入组员到此组别";
$Lang['Group']['IsECAGroup'] = "学会会员必须于课外活动管理中汇入";

$Lang['Group']['Options']= '选顶';
$Lang['Group']['CopyResult'] = '复制结果';
$Lang['Group']['Confirmation'] = '确认';
$Lang['Group']['CopyFrom']= '由学年';
$Lang['Group']['CopyTo']= '至学年';
$Lang['Group']['Group']= '组别';
$Lang['Group']['CopyGroup']= '复制组别';
$Lang['Group']['CopyMember']= '复制组员';
$Lang['Group']['Member'] = '组员';
$Lang['Group']['CopyFromOtherYear'] = '从其他年度复制';
$Lang['Group']['Copy'] = '复制';
$Lang['Group']['warnSelectGroup'] = '请选择最少一个组别';
$Lang['Group']['warnSelectDifferentYear'] = '请选择不同的年度';
$Lang['Group']['GroupExistInTargetYear'] = '目标年度已有此组别';
$Lang['Group']['GroupCopiedSuccessfully'] = '个组别复制成功';

### eComm
$Lang['Group']['WarnAddGroup'] = "请选择最少一个组别";
$Lang['Group']['WarnStratDayPassed'] = "开始日期必须是今日或以后";
$Lang['Group']['MissingFile'] = "在下列连结找不到档案";

$Lang['Group']['NotEnoughSpace'] = "以下小组没有足够储存空间: ";
$Lang['Group']['Role']['SetPresetRole'] = "是否为预设小组职位";

$Lang['eComm']['NoPublicAnnounceRightEdit'] = "你的组别没有编辑公众宣布的权限";
$Lang['eComm']['NoPublicAnnounceRightDelete'] = "你的组别没有删除公众宣布的权限";
$Lang['eComm']['NotSendToFullGroup'] = "新主题将不会呈送到没有足够储存空间的小组，继续?";
$Lang['eComm']['DeleteInternalOnly'] = "只删除了你的社群内的宣布";
$Lang['eComm']['PleaseInsertTitle'] = "请输入标题";
$i_con_msg_GroupLogo_PhotoWarning = "群组图示只接受'.JPG'、'.GIF'或'.PNG'等格式。";

$eComm['Allow_Att'] = "允许加入附件?";
$eComm['Allow_Forum'] = "允许使用讨论区";
$eComm['ForumDisabled'] = "讨论区不允许使用";

$Lang['eComm']['WaitingForApproval'] = "等候批核";
$Lang['eComm']['ManualInput'] = "手动输入";
$Lang['eComm']['AllowUsingWebsite'] = "接受使用网页";

### eEnrolment
if ($plugin['eEnrollment'])
{
	$Lang['eEnrolment']['MenuTitle']['Settings']['AdminSettings'] = "管理权限设定";

	$Lang['eEnrolment']['UserRole']['EnrolmentAdmin'] = "社团报名管理员";
	$Lang['eEnrolment']['UserRole']['EnrolmentMaster'] = "社团报名管理老师";
	$Lang['eEnrolment']['UserRole']['NormalUser'] = "一般使用者";
	
	$Lang['eEnrolment']['Warning']['AgeRangeInvalid'] = "对象年龄范围不正确";

	$Lang['eEnrolment']['StaffName'] = "职员/老师姓名";
	$Lang['eEnrolment']['AdminLevel'] = "管理权限";
	$Lang['eEnrolment']['AdminSettings'] = "管理权限设定";
	$Lang['eEnrolment']['WholeYear'] = "全年";
	

	$Lang['eEnrolment']['SendEmail'] = "传送电子邮件";	
	$Lang['eEnrolment']['SendEmailAtOnce'] = "在审批后一次性寄电子邮件予学生/家长";
	$Lang['eEnrollment']['SendEmailForEnrollmentResult'] = "对已登记人士发出电子邮件";




	$Lang['eEnrolment']['jsWarning']['DuplicatedPersonInPicAndHelper'] = "同一人不能同时为活动负责人及活动点名助手。";
	$Lang['eEnrolment']['jsWarning']['StudentCannotRemoveTeacher'] = "你没有权限移除有关老师。";
	$Lang['eEnrolment']['jsWarning']['RemoveMyselfWarning'] = "如你从选项中移除自己，你将失去此社团或活动的权限。你确定要从选项中移除自己？";

	$Lang['eEnrolment']['EmailSample'] = "电子邮件范本";
	$Lang['eEnrolment']['ClubEmailSampleArr'][0] = 'Here is the club enrolled result for XXX';
	$Lang['eEnrolment']['ClubEmailSampleArr'][1] = '1. Club 1 - approved';
	$Lang['eEnrolment']['ClubEmailSampleArr'][2] = '2. Club 2 - approved';
	$Lang['eEnrolment']['ClubEmailSampleArr'][3] = '3. Club 3 - rejected';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][0] = 'Here is the activity enrolled result for XXX';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][1] = '1. Activity 1 - approved';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][2] = '2. Activity 2 - approved';
	$Lang['eEnrolment']['ActivityEmailSampleArr'][3] = '3. Activity 3 - rejected';
	
	$Lang['eEnrolment']['SendEmailWithParent'] = '同时寄给家长';
	$Lang['eEnrolment']['AllowClubPICCrateNAActviity'] = '允许学会负责人建立非学会活动';
	
	$Lang['eEnrolment']['DisableCheckingOfNumberOfClubStudentWantToJoin'] = '批核学生时不检查学生参加社团数目的意愿';
	$Lang['eEnrolment']['ImportFailRemarks'] = "红色字代表汇入失败";
	
	# Copy Clubs
	$Lang['eEnrolment']['CopyClub']['NavigationArr']['ClubManagement'] = '学会管理';
	$Lang['eEnrolment']['CopyClub']['NavigationArr']['CopyClub'] = '复制学会';
	$Lang['eEnrolment']['CopyClub']['StepArr'][0] = '选项'; 
	$Lang['eEnrolment']['CopyClub']['StepArr'][1] = '确定';
	$Lang['eEnrolment']['CopyClub']['StepArr'][2] = '复制结果';
	$Lang['eEnrolment']['CopyClub']['CopyFromOtherYearTerm'] = '从其他年度/学期复制';
	$Lang['eEnrolment']['CopyClub']['CopyWarning'][0] = '如执行复制学会，系统将自动删除<b>所有</b>学会的报名纪录。复制学会前，请先确保学会报名纪录可被删除。';
	$Lang['eEnrolment']['CopyClub']['CopyWarning'][1] = '- 请检查已选择的学会是否已复制到目标学年 / 学期，否则该学会于目标学年 / 学期的纪录将会重复。';
	$Lang['eEnrolment']['CopyClub']['FromAcademicYear'] = '由学年';
	$Lang['eEnrolment']['CopyClub']['ToAcademicYear'] = '至学年';
	$Lang['eEnrolment']['CopyClub']['FromTerm'] = '由学期';
	$Lang['eEnrolment']['CopyClub']['ToTerm'] = '至学期';
	$Lang['eEnrolment']['CopyClub']['TermMapping'] = '对应学期';
	$Lang['eEnrolment']['CopyClub']['ClubSelection'] = '选择学会';
	$Lang['eEnrolment']['CopyClub']['Club'] = '学会';
	$Lang['eEnrolment']['CopyClub']['Term'] = '学期';
	$Lang['eEnrolment']['CopyClub']['Copy'] = '复制';
	$Lang['eEnrolment']['CopyClub']['ClubMember'] = '复制会员';
	$Lang['eEnrolment']['CopyClub']['ClubActivity'] = '复制相关活动';
	$Lang['eEnrolment']['CopyClub']['jsWarningArr']['SelectAtLeastOneClub'] = '请选择复制学会。';
	$Lang['eEnrolment']['CopyClub']['BackToClubManagement'] = '回到学会管理';
	$Lang['eEnrolment']['CopyClub']['ClubCopiedSuccessfully'] = '学会复制成功。';
	$Lang['eEnrolment']['CopyClub']['ClubTitleDuplicationRemarks'] = '学会名称于 <!--YearName--> 重复。';
	$Lang['eEnrolment']['CopyClub']['ClubCopyToTheSameTermRemarks'] = '不能将来自同一小组的学会复制到同一学期。';
	
	# From IP20
	$eEnrollment['participant_selection_and_drawing'] = "申请批核及抽签";
	$i_ClubsEnrollment_NumOfSatisfied = "符合个人要求的学生";
	$eEnrollment['front']['wish_enroll_end'] = "&nbsp;个社团。";
	$eEnrollment['front']['wish_enroll_end_event'] = "&nbsp;个活动。";
	$eEnrollment['js_select_semester'] = "请选择学期。";
	$eEnrollment['ClubType'] = "社团类型";
	$eEnrollment['YearBased'] = "全年性";
	$eEnrollment['SemesterBased'] = "学期性";
	$eEnrollment['WholeYear'] = "全年";
	$eEnrollment['MinimumMemberQuota'] = "会员人数下限";
	$eEnrollment['MinimumQuota'] = "人数下限";
	$eEnrollment['ToBeConfirmed'] = "待定";
	$eEnrollment['ChangeClubTypeWarning'] = "如你更改社团类型，社团会员及报名资料将被删除。你是否确定更改社团类型？";
	$eEnrollment['enrol_same_club_once_only'] = "学生只可参加此社团一次";
	$eEnrollment['only_allow_student_to_join_the_club_in_the_first_semester'] = "学生只可于第一学期报名参加此社团";
	$i_ClubsEnrollment_WholeYear = "全年";
	$eEnrollment['semester_cannot_be_match'] = "学期与社团不相配";
	$eEnrollment['semester_not_found'] = "没有此学期";
	$eEnrollment['target_enrolment_semester'] = "是次报名学期";
	$eEnrollment['selectTypeTransferToStudentProflie'] = "请选择最少一个顶目";
	$eEnrollment['add_activity']['act_target'] = "对象级别";	
	$Lang['eEnrolment']['Nature'] = "活动性质";
	$Lang['eEnrolment']['SchoolActivity'] = "校内活动";
	$Lang['eEnrolment']['NonSchoolActivity'] = "校外活动";
	
	
	
	$Lang["eEnrolment"]["ActivityParticipationReport"]["ActivityParticipationReport"] = "活动参与报告";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolYear"] = "学年";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Semester"] = "学期";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Period"] = "时段";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Target"] = "对象";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["PressCtrlKey"] = "可按Ctrl键选择多项";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["ActivityCategory"] = "活动类型";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Nature"] = "性质";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Time(s)"] = "次数";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Hour(s)"] = "时数";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["DisplayUnit"] = "显示单位";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["SortBy"] = "排序";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["All"] = "全部";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["SchoolActivity"] = "校内活动";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Non-schoolActivity"] = "校外活动";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrAbove"] = "或以上";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["OnOrBelow"] = "或以下";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Student"] = "学生";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Class"] = "班级";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassName"] = "班级名称";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["ClassNumber"] = "班级编号";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["AscendingOrder"] = "递增";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["DescendingOrder"] = "递减";
	
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Name"] = "学生姓名";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["TimesFor"] = "次数";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalTimesFor"] = "总次数";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["TotalHoursFor"] = "总时数";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Hours"] = "小时";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Total"] = "合共";
	
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_Class"] = "班级";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_ClassNumber"] = "班级编号";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_StudentName"] = "学生姓名";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_TimesFor"] = "次数";
	$Lang["eEnrolment"]["ActivityParticipationReport"]["export_TotalHoursFor"] = "总时数";
		
	$Lang["eEnrolment"]["ActivityParticipationReport"]["Number"] = "数目";
	
	$eEnrollmentMenu["ActivityParticipationReport"] = "活动参与报告";
	
	$Lang['eEnrolment']['Attendance']['Exempt'] = "豁免";
	$Lang['eEnrolment']['AttendanceHour'] = "出席时数";
	$Lang["eEnrolment"]["DirectToSetting"] = "现在进行设定?";
	$Lang["eEnrolment"]["ShowEntireList"] = "显示所有日期";
	
	$Lang["eEnrolment"]["NoOfEmptyActPerformance"] = "尚未输入表现的活动数";
	$Lang["eEnrolment"]["NoOfEmptyClubPerformance"] = "尚未输入表现的社团数";
	
	$Lang['eEnrolment']['record_type'] = "纪录类型";
	$Lang['eEnrolment']['show_data'] = "显示资料";
	$Lang['eEnrolment']['attendance_percent'] = "出席率";
	$Lang['eEnrolment']['select_club_activity'] = "请最少选择一种纪录类型。";
	$Lang['eEnrolment']['select_role_attendance_performance'] = "请最少选择一项资料。";
	$Lang['eEnrolment']['bulk_print'] = "Bulk print individual enrolment records";
	
	$Lang['eEnrolment']['default_format_print'] = "标准列印";
	$Lang['eEnrolment']['custom_format_print'] = "自定格式列印";
	$Lang['eEnrolment']['delete_selected_date'] = "移除所选日期";
	$Lang['eEnrolment']['ConfirmDelete'] = '你是否确定删除所选日期？';
	$Lang['eEnrolment']['delete_selected_date_warning'] = "请最少选择一个日期。";

	$Lang['eEnrolment']['AddWeeklyAct'] = "加入每周聚会或活动";
	$Lang['eEnrolment']['StartDate'] = "开始日期";
	$Lang['eEnrolment']['EndDate'] = "结束日期";
	$Lang['eEnrolment']['OnEvery'] = "逄星期";
	$Lang['eEnrolment']['PeriodOfTime'] = "选取时期";
	$Lang['eEnrolment']['MustBeLater'] = "必须大于";
	$Lang['eEnrolment']['MustBeEarlier'] = "必须小于";
	$Lang['eEnrolment']['Error'] = "错误 ";
	$Lang['eEnrolment']['Err_IsInvalid'] = "不正确 ";
	$Lang['eEnrolment']['Err_NotSelect'] = "尚未选取";
	
	$Lang['eEnrolment']['disableUpdate'] = "不允许新增或更改资料";
	$Lang['eEnrolment']['disableUpdateAlertMsg'] = "资料已被冻结 - 不允许新增或更改资料";
}


#lslp
if($plugin['lslp'])
{
	$Lang['lslp']['lslp'] = "通识学习平台";
}

# iAccount > student profile
$Lang['iAccount']['InvalidStudentData'] = "学生资料无效";

$Lang['eDiscipline']['CaterPriority'] = "优先次序先决";
### Discipline v12
if($plugin['Disciplinev12'])
{
	$Lang['eDiscipline']['AutoFillIn'] = "自动填充项目";
	$Lang['eDiscipline']['Insert'] = "加入";
	$Lang['eDiscipline']['DeleteLog'] = "资料删除纪录";
	$Lang['eDiscipline']['DeletedDate'] = "删除日期";
	$Lang['eDiscipline']['DeletedBy'] = "删除者";
	$Lang['eDiscipline']['RecordInfo'] = "纪录资料";
	$Lang['eDiscipline']['RecordType'] = "纪录类别";
	$Lang['eDiscipline']['Others'] = "其他";
	$Lang['eDiscipline']['DeleteRecordsInDateRange'] = "删除所选日期范围内的所有纪录";
	$Lang['eDiscipline']['ConfirmDeleteRecordsInDateRange'] = "确定要删除所选日期范围内的所有纪录?";
	$Lang['eDiscipline']['ButtonUnselectAll'] = "取消全选";
	$Lang['eDiscipline']['Month'] = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
	$Lang['eDiscipline']['Month2'] = array('','一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月');
	$Lang['eDiscipline']['CaterPriority'] = "优先次序先决";
	$Lang['eDiscipline']['CustomizedReports'] = "自定报告";
	$Lang['eDiscipline']['SchoolTermNotYetSet'] = "尚未设定学期。";
	$Lang['eDisicpline']['PeriodCannotAcrossSchoolTerm'] = "时段不可跨越不同学期。";
	$Lang['eDiscipline']['PeriodCannotAcrossSchoolYear'] = "时段不可跨越不同学年。";
		
	# 天主教慈幼会伍少梅中学 cust report (Award & Punishment Monthly report)
	$Lang['eDiscipline']['MonthlyAwardPunishmentReport'] = "品行报告表";
	$Lang['eDiscipline']['YearlyAwardPunishmentReport'] = "品行年度报告表";
	$Lang['eDiscipline']['sdbnsm_SchoolName'] = "天主教慈幼会伍少梅中学";
	$Lang['eDiscipline']['sdbnsm_ap_report'] = "品行报告表";
	$Lang['eDiscipline']['sdbnsm_ap_item'] = "品行评注项目";
	$Lang['eDiscipline']['sdbnsm_punish_A'] = "(甲项)记过";
	$Lang['eDiscipline']['sdbnsm_award_B'] = "(乙项)记功";
	$Lang['eDiscipline']['sdbnsm_punish'] = "记过";
	$Lang['eDiscipline']['sdbnsm_award'] = "记功";
	$Lang['eDiscipline']['sdbnsm_times'] = "次数";
	$Lang['eDiscipline']['sdbnsm_times_2'] = "次";
	$Lang['eDiscipline']['sdbnsm_scoreChange'] = "减分";
	$Lang['eDiscipline']['sdbnsm_ap_list'] = "功过栏";
	$Lang['eDiscipline']['sdbnsm_record_date'] = "日期";
	$Lang['eDiscipline']['sdbnsm_item'] = "事项";
	$Lang['eDiscipline']['sdbnsm_teacher_signature'] = "教师签署";
	$Lang['eDiscipline']['sdbnsm_parent_signature'] = "家长签署";
	$Lang['eDiscipline']['sdbnsm_baseMark_1'] = "每月基本品行分为";
	$Lang['eDiscipline']['sdbnsm_baseMark_2'] = "分";
	$Lang['eDiscipline']['sdbnsm_absent'] = "缺席";
	$Lang['eDiscipline']['sdbnsm_half_day'] = "半天";
	$Lang['eDiscipline']['sdbnsm_whole_day'] = "全天";
	$Lang['eDiscipline']['sdbnsm_whole_year'] = "全年";
	$Lang['eDiscipline']['sdbnsm_late'] = "迟到";
	$Lang['eDiscipline']['sdbnsm_earlyLeave'] = "早退";
	$Lang['eDiscipline']['sdbnsm_conduct_mark'] = "品行分";
	$Lang['eDiscipline']['sdbnsm_conduct_grade'] = "品行等级";
	$Lang['eDiscipline']['sdbnsm_classTeacher_signature'] = "班导师签署";
	$Lang['eDiscipline']['sdbnsm_guidance_signature'] = "监护人签署";
	$Lang['eDiscipline']['sdbnsm_teacher_comment'] = "班导师评语";
	$Lang['eDiscipline']['sdbnsm_ap_yearly_report'] = "品行年度报告表";
	### end of cust
	
	$Lang['eDiscipline']['Gain'] = "增加";
	$Lang['eDiscipline']['Deduct'] = "扣减";
	$Lang['eDiscipline']['ChangeReasonNotice'] = "系统乃根据通告\\\"发放原因\\\"提供相关之自动填充项目。更改\\\"发放原因\\\"将引致部份先前插入的自动填充项目无法显示。";
	
	$eDiscipline["Websams_Transition_Step2_Instruction1"] = "系统只会传送 <i>已经批核</i> 及 <i>已经开放</i> 的奖惩纪录。";
};
$Lang['eDiscipline']['JSWarning']['PleaseDoNotSelectMoreThan10Records'] = "请勿选择超过10顶记录";
$Lang['eDiscipline']['JSWarning']['NoMoreVacancy'] = "地点余额不足";
$Lang['eDiscipline']['JSWarning']['SessionDuplicate'] = "时段重复 / 重迭";
$Lang['eDiscipline']['FieldTitle']['DetentionTimes'] = "留堂次数";
$Lang['eDiscipline']['FieldTitle']['DetentionSession'] = "时段";
$Lang['eDiscipline']['FieldTitle']['DetentionReason'] = "原因";
$Lang['eDiscipline']['FieldTitle']['DetentionRemark'] = "备注";
$Lang['eDiscipline']['FieldTitle']['LastDetentionSession'] = "上一次时段";
$Lang['eDiscipline']['FieldTitle']['DetentionAbsentRemark'] = "缺席备注";
$Lang['eDiscipline']['FieldTitle']['LastAbsentDetails'] = "上一次缺席资料";
$Lang['eDiscipline']['FieldTitle']['NumOfStudentsAssignedInThisSession'] = "已分配到此时段的学生人数";

$Lang['eDiscipline']['FieldTitle']['CalculationMethod'] = "操行分累计周期";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByTerm'] = "每个学期重新累计";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ByAcademicYear'] = "每个学年重新累计";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['SystemGenerated'] = "系统管理的操行分";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ManuallyAdjusted'] = "手动调整总计 (累计周期内)";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['ConsolidatedMark'] = "最终得分";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['NoRecordAtThisTerm'] = "本学期暂时仍未有任何记录";
$Lang['eDiscipline']['FieldTitle']['ConductMarkPeriod']['OnlyAffectTheCurrentRecord'] = "更改底分将只会影响本学年的操行分纪录。<br>你可于更改底分后到<b>管理>操行分</b>验证分数变更。";
$Lang['eDiscipline']['FieldTitle']['SelectSchoolYear'] = " - 选择学年 - ";
$Lang['eDiscipline']['FieldTitle']['SelectTerm'] = " - 选择学期 - ";
$Lang['eDiscipline']['FieldTitle']['LastUpdated'] = "最后更新";
$Lang['eDiscipline']['FieldTitle']['RelatedItems'] = "相关物品";

### Event Type
$Lang['EventType']['School'] = "学校事项";
$Lang['EventType']['Academic'] = "教学事项";
$Lang['EventType']['Group'] = "小组事项";
$Lang['EventType']['PublicHoliday'] = "公众假期";
$Lang['EventType']['SchoolHoliday'] = "学校假期";
	
### eClass Update in admin console
$i_adminmenu_sc_eclass_update = "eClass更新";
$i_manual_update = "手动更新";
$i_auto_update = "自动更新";
$i_update_report = "更新报告";
$i_update_islatest = "你已经使用最新版本。";
$i_update_notlatest = "你可以更新到新版本。";
$i_eu_welcome_msg = "欢迎使用eClass自动更新服务!!";
$i_Maintenace_Status = "你的系统保养状况";
$i_eu_not_activated = "你的 eclass 伺服器并未启动eClass自动更新服务.<br><br>请联络我们的客户服务部, 电子邮件: support@broadlearning.com";
$i_System_Status = "系统更新状况";
$i_eu_submit_wizard = "请按\"呈送\"进行下一步。";
$i_eu_contact_cs = "请联络我们的客户服务部, 电子邮件: support@broadlearning.com";
$i_eu_check_connection_pass = "正在测试与更新伺服器的连线：已连线!";
$i_eu_check_connection_fail = "正在测试与更新伺服器的连线：未能连线!";
$i_eu_check_space_pass ="正在检查储存空间：通过!";
$i_eu_check_space_fail ="正在检查储存空间：储存空间不足!";
$i_eu_now_will_up = "你的系统将作如下更新";
$i_eu_from_ver = "由版本: ";
$i_eu_to_ver = "到版本: ";
$i_eu_next_msg = "按\"下一步\"继续更新程序。前往其他页面可取消更新。";
$i_eu_next_button = "下一步";
$i_eu_download_msg="正在下载更新包，请稍候!";
$i_eu_download_completed="更新包下载完成!";
$i_eu_download_next="按\"下一步\"继续更新程序。";
$i_eu_download_progress="进度";
$i_eu_verify_msg="正在测试你的系统，请稍候!";
$i_eu_verifying = "测试中";
$i_eu_verify_done= "测试完成!";
$i_eu_extract_msg ="正在更新你的系统，请稍候!";
$i_eu_extract_msg2 ="打开更新包...";
$i_eu_update = "正在更新你的校园资料库";
$i_eu_update2 = "正在更新你的教室资料库";
$i_eu_success = "成功";
$i_eu_final = "你可以前往以下网页，了解版本更新资讯。";
$i_eu_final2 = "系统更新完成，你的系统已更新到最新版本。";
$i_eu_summary = "eClass 系统更新摘要";
$i_eu_latest_ver = "系统最新版本";
$i_eu_current_ver ="当前系统版本";
$i_eu_error = "错误：系统无法更新";
$i_eu_error2 = "请电邮到support@broadlearning.com，并引用以上错误讯息。";
	
$Lang['SysMgr']['Homework']['Management'] = "管理";
$Lang['SysMgr']['Homework']['HomeworkList'] = "功课纪录表";
$Lang['SysMgr']['Homework']['SubjectLeader'] = "科长";
$Lang['SysMgr']['Homework']['Reports'] = "报告";
$Lang['SysMgr']['Homework']['WeeklyHomeworkList'] = "一周功课纪录表";
$Lang['SysMgr']['Homework']['NotSubmitList'] = "欠交功课清单";
$Lang['SysMgr']['Homework']['HomeworkReport'] = "功课报告";
$Lang['SysMgr']['Homework']['Statistics']= "统计";
$Lang['SysMgr']['Homework']['HomeworkStatistics'] = "功课统计";
$Lang['SysMgr']['Homework']['Settings'] = "设定";
$Lang['SysMgr']['Homework']['BasicSettings'] = "基本设定";
$Lang['SysMgr']['Homework']['HomeworkType'] = "功课类型";
$Lang['SysMgr']['Homework']['AdminSettings'] = "管理权限设定";
$Lang['SysMgr']['Homework']['Record'] = "纪录";
$Lang['SysMgr']['Homework']['NewRecord'] = "新增纪录";
$Lang['SysMgr']['Homework']['ImportRecord'] = "汇入纪录";
$Lang['SysMgr']['Homework']['Edit'] = "编辑";
$Lang['SysMgr']['Homework']['EditRecord'] = "编辑纪录";
$Lang['SysMgr']['Homework']['AssignSubjectLeader'] = "委任科长";
$Lang['SysMgr']['Homework']['ImportSourceFile'] = "资料档案";
$Lang['SysMgr']['Homework']['ImportSourceFileNote'] = "你可于 \"资料档案\" 栏输入CSV资料档的路径，或按\"浏览\"，从你的电脑中选取资料档。";

$Lang['SysMgr']['Homework']['SubjectGroupCode'] = "学科组别代号";
//$Lang['SysMgr']['Homework']['SubjectGroupTitle'] = "学科组别标题";
$Lang['SysMgr']['Homework']['SubjectCode'] = "学科代号";
$Lang['SysMgr']['Homework']['Student'] = "学生";
$Lang['SysMgr']['Homework']['StudentName'] = "学生姓名";
$Lang['SysMgr']['Homework']['Children'] = "子女";
$Lang['SysMgr']['Homework']['AllStudents'] = "所有学生";
$Lang['SysMgr']['Homework']['Form'] = "班级";
$Lang['SysMgr']['Homework']['AllForms'] = "所有班级";
$Lang['SysMgr']['Homework']['Class'] = "班别";
$Lang['SysMgr']['Homework']['ClassNumber'] = "座号";
$Lang['SysMgr']['Homework']['Subject'] = "学科";
$Lang['SysMgr']['Homework']['Topic'] = "标题";
$Lang['SysMgr']['Homework']['Poster'] = "发出人";
$Lang['SysMgr']['Homework']['Workload'] = "工作量";
$Lang['SysMgr']['Homework']['Hours'] = "小时";
$Lang['SysMgr']['Homework']['Teacher'] = "老师";
$Lang['SysMgr']['Homework']['AllTeachers'] = "所有老师";
$Lang['SysMgr']['Homework']['StartDate']="开始日期";
$Lang['SysMgr']['Homework']['DueDate'] = "限期";
$Lang['SysMgr']['Homework']['Description'] = "内容";
$Lang['SysMgr']['Homework']['Attachment'] = "附件";
$Lang['SysMgr']['Homework']['NoAttachment'] = "没有附件";
$Lang['SysMgr']['Homework']['HandinRequired'] = "须缴交";
$Lang['SysMgr']['Homework']['Status'] = "现在状况";;
$Lang['SysMgr']['Homework']['Error'] = "错误";

$Lang['SysMgr']['Homework']['SearchAlert'] = "请输入内容";
$Lang['SysMgr']['Homework']['ToDoList'] = "功课清单";
$Lang['SysMgr']['Homework']['History'] = "纪录";
$Lang['SysMgr']['Homework']['SubjectGroup'] = "学科组别";
$Lang['SysMgr']['Homework']['AllSubjects'] = "所有学科";
$Lang['SysMgr']['Homework']['AllSubjectGroups'] = "所有学科组别";
$Lang['SysMgr']['Homework']['HomeworkHandinlist'] = "功课缴交清单";
$Lang['SysMgr']['Homework']['Handin'] = "已缴交";
$Lang['SysMgr']['Homework']['Total'] = "总数";

$Lang['SysMgr']['Homework']['ImportOtherRecords'] = "汇入其他纪录";
$Lang['SysMgr']['Homework']['PreviousWeek'] = "上一周";
$Lang['SysMgr']['Homework']['CurrentWeek'] = "本周";
$Lang['SysMgr']['Homework']['NextWeek'] = "下一周";
$Lang['SysMgr']['Homework']['NoRecord'] = "暂时仍未有任何纪录";
$Lang['SysMgr']['Homework']['NotSubmitCount'] = "欠交次数";
$Lang['SysMgr']['Homework']['From'] = "由";
$Lang['SysMgr']['Homework']['To'] = "至";
$Lang['SysMgr']['Homework']['HomeworkCount'] = "功课总数";
$Lang['SysMgr']['Homework']['Print'] = "列印";
$Lang['SysMgr']['Homework']['Export'] = "汇出";

$Lang['SysMgr']['Homework']['Disable'] = "不使用功课纪录表";
$Lang['SysMgr']['Homework']['TeacherSearchDisabled'] = "不使用发出人名字搜寻";
$Lang['SysMgr']['Homework']['SubjectSearchDisabled'] = "不使用科目搜寻";
$Lang['SysMgr']['Homework']['SubjectsTaughtSearchDisabled'] = "不使用以教师任教职务搜寻";
$Lang['SysMgr']['Homework']['StartFixed'] = "只可设定今天功课";
$Lang['SysMgr']['Homework']['ParentAllowed'] = "允许家长检视功课";
$Lang['SysMgr']['Homework']['NonTeachingAllowed'] = "非教学职务员工可以新增功课 (所有学科组别)";
$Lang['SysMgr']['Homework']['TeachingRestricted'] = "教学职务员工只可新增任教科目及班别";
$Lang['SysMgr']['Homework']['AllowSubLeader'] = "容许科长输入功课";
$Lang['SysMgr']['Homework']['AllowExport'] = "容许所有职员汇出功课";
$Lang['SysMgr']['Homework']['AllowPast'] = "容许输入旧日纪录";
$Lang['SysMgr']['Homework']['UseHomeworkCollect'] = "显示班导师是否收回有关功课";
$Lang['SysMgr']['Homework']['ClearAll'] = "清除所有功课纪录";
$Lang['SysMgr']['Homework']['ToDateWrong'] = "限期必须是开始日期或以后";

$Lang['SysMgr']['Homework']['AcademicYear'] = "学年";
$Lang['SysMgr']['Homework']['YearTerm'] = "学期";
$Lang['SysMgr']['Homework']['AllYearTerms'] = "所有学期";

$Lang['SysMgr']['Homework']['LastModifiedBy'] = "最后修改老师";
$Lang['SysMgr']['Homework']['Submitted'] = "已交";
$Lang['SysMgr']['Homework']['NotSubmitted'] = "未交";
$Lang['SysMgr']['Homework']['ExpiredWithoutSubmission'] = "逾期未交";
$Lang['SysMgr']['Homework']['LateSubmitted'] = "迟交";
$Lang['SysMgr']['Homework']['UnderProcessing'] = "未处理";
$Lang['SysMgr']['Homework']['Redo'] = "重做";
$Lang['SysMgr']['Homework']['NoNeedSubmit'] = "不需缴交";
$Lang['SysMgr']['Homework']['SetAllToSubmitted'] = "全部转为已交";
$Lang['SysMgr']['Homework']['SetAllToNotSubmitted'] = "全部转为未交";
$Lang['SysMgr']['Homework']['SetAllToLate'] = "全部转为迟交";
$Lang['SysMgr']['Homework']['SetAllToRedo'] = "全部转为重做";
$Lang['SysMgr']['Homework']['SetNotSubmittedToNoNeedSubmit'] = "把未缴交转为不需缴交";
$Lang['SysMgr']['Homework']['SetExpiredWithoutSubmissionToNoNeedSubmit'] = "把逾期未交转为不需缴交";

$Lang['SysMgr']['Homework']['HomeworkListWarning'] = "功课纪录表并不适用于本学期没有任教任何科目的教师用户。";
$Lang['SysMgr']['Homework']['SubjectLeaderWarning'] = "科长并不适用于本学期没有任教任何科目的教师用户。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarning'] = "一周功课纪录表并不适用于本学期没有任教任何科目的教师用户。";
$Lang['SysMgr']['Homework']['NotSubmitListWarning'] = "欠交功课清单并不适用于本学期没有任教任何科目的教师用户。";

$Lang['SysMgr']['Homework']['HomeworkListWarningStudent'] = "功课纪录表并不适用于本学期没有就读任何科目的学生用户。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningStudent'] = "一周功课纪录表并不适用于本学期没有就读任何科目的学生用户。";

$Lang['SysMgr']['Homework']['HomeworkListWarningSubjectLeader'] = "功课纪录表并不适用于本学期不是科长的学生用户。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningSubjectLeader'] = "一周功课纪录表并不适用于本学期不是科长的学生用户。";

$Lang['SysMgr']['Homework']['HomeworkListWarningParent'] = "功课纪录表并不适用于本学期子女没有就读任何科目的家长用户。";
$Lang['SysMgr']['Homework']['WeeklyHomeworkListWarningParent'] = "一周功课纪录表并不适用于本学期子女没有就读任何科目的家长用户。";

$Lang['SysMgr']['Homework']['Remark']['EmptyFields']= "部份栏位没有填上";
$Lang['SysMgr']['Homework']['Remark']['NoSubjectGroup'] = "没有此学科组别";
$Lang['SysMgr']['Homework']['Remark']['NoSubject'] = "没有此学科";
$Lang['SysMgr']['Homework']['Remark']['Handin'] = "请填上'yes'或'no'选择是否需要缴交功课";
$Lang['SysMgr']['Homework']['Remark']['GroupTeacher'] = "此老师没有任教此学科组别";
$Lang['SysMgr']['Homework']['Remark']['SubjectTeacher'] = "此老师没有任教此学科";
$Lang['SysMgr']['Homework']['Remark']['noAccessRight'] = "此用户没有制作功课之权限";
$Lang['SysMgr']['Homework']['Remark']['StartDateError'] = "开始日期早于今天";
$Lang['SysMgr']['Homework']['Remark']['DueDateError'] = "限期早于开始日期";
$Lang['SysMgr']['Homework']['Remark']['NonTeachingStaff'] = "用户不是老师";
$Lang['SysMgr']['Homework']['Remark']['InvalidUser'] = "用户不存在";
$Lang['SysMgr']['Homework']['Remark']['SubjectGroupNotMatch'] = "学科组别不属于此学科";
$Lang['SysMgr']['Homework']['Remark']['Collect'] = "请填上'yes'或'no'选择班导师是否收回有关功课";

$Lang['SysMgr']['Homework']['WeekDay'][]= "星期日";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期一";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期二";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期三";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期四";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期五";
$Lang['SysMgr']['Homework']['WeekDay'][]= "星期六";

$Lang['SysMgr']['Homework']['PleaseSelect']= "-- 请选择 --";
$Lang['SysMgr']['Homework']['Target'] ="对象";

# School News
$Lang['SysMgr']['SchoolNews']['SchoolNews'] = "校园最新消息";
$Lang['SysMgr']['SchoolNews']['Settings'] = "设定";
$Lang['SysMgr']['SchoolNews']['NewRecord'] = "新增";
$Lang['SysMgr']['SchoolNews']['StartDate'] = "开始日期";
$Lang['SysMgr']['SchoolNews']['EndDate'] = "结束日期";
$Lang['SysMgr']['SchoolNews']['Title'] = "项目";
$Lang['SysMgr']['SchoolNews']['Description'] = "内容";
$Lang['SysMgr']['SchoolNews']['Attachment'] = "附件";
$Lang['SysMgr']['SchoolNews']['Status'] = "状况";
$Lang['SysMgr']['SchoolNews']['StatusPublish'] = "已发布";
$Lang['SysMgr']['SchoolNews']['StatusPending'] = "未发布";
$Lang['SysMgr']['SchoolNews']['EmailAlert'] = "发送电子邮件给用户";
$Lang['SysMgr']['SchoolNews']['PublicAnnouncement'] = "公众宣布";
$Lang['SysMgr']['SchoolNews']['EditRecord'] = "编辑";
$Lang['SysMgr']['SchoolNews']['CurrentAttachment'] = "现有附件";
$Lang['SysMgr']['SchoolNews']['AllStatus'] = "所有状况";
$Lang['SysMgr']['SchoolNews']['ViewReadStatus'] = "检视观看情况";
$Lang['SysMgr']['SchoolNews']['Read'] = "已阅";
$Lang['SysMgr']['SchoolNews']['Unread'] = "未阅";
$Lang['SysMgr']['SchoolNews']['ReadList'] = "已阅名单";
$Lang['SysMgr']['SchoolNews']['UnreadList'] = "未阅名单";
$Lang['SysMgr']['SchoolNews']['Name'] = "姓名";
$Lang['SysMgr']['SchoolNews']['Role'] = "身分";
$Lang['SysMgr']['SchoolNews']['WarnSelectGroup'] = "请选择组别";
$Lang['SysMgr']['SchoolNews']['Approval'] = "批核";
$Lang['SysMgr']['SchoolNews']['NoApproval'] = "不经批核";
$Lang['SysMgr']['SchoolNews']['Reject'] = "不批核";
$Lang['SysMgr']['SchoolNews']['Approve'] = "批核";

# campus link
$i_CampusLink_edit = "修改校园连结";
$i_CampusLink_add = "新增校园连结";
$i_CampusLink_edit_fail = "纪录修改失败";
$i_CampusLink_delete_fail = "纪录删除失败";
$i_CampusLink_connect_fail = "连接伺服器失败";
$i_CampusLink_validate = "名称与超连结不可是空的";
$i_CampusLink_delete_warning = "你是否决定删除以下连结";
$i_CampusLink_wrong_addr = "超连结必定以 'http://', 'https://', 'ftp://' 开头";
$i_CampusLink_moveUp = "移上";
$i_CampusLink_moveDown = "移下";
$i_CampusLink_moveUp_success = "成功上移";
$i_CampusLink_moveDown_success = "成功下移";

#icalendar
$iCalendar_iCalendar = '我的行事历';
$iCalendar_iCalendarLite = '我的行事历(基本版)';

#eclass
$Lang['eclass']['warning']['checkRemoveConfrim'] = "你是否想移除以下科目的学科组别:\\n";
$Lang['eclass']['directory']['notCategorized'] = "未分类"; 
$Lang['eclass']['setting']['NAEditEclass'] = "让非教学职务员工在eClass目录页自行开eClass"; 
$Lang['eclass']['warning']['noSubjectGroup'] = "由于并未选择科组(或并无合适的对应科组)，此网上教室将新增到网上教室目录的\"未分类\"类别下。"; 
$Lang['eclass']['default_max_quota'] = "预设教室的容量限额"; 

#subject Group
$Lang['SysMgr']['SubjectClassMapping']['eClass'] = '网上教室';
$Lang['SysMgr']['SubjectClassMapping']['Createelcass']='建立网上教室';
$Lang['SysMgr']['SubjectClassMapping']['elcassCreated'] ='有相关网上教室';
$Lang['SysMgr']['SubjectClassMapping']['warningRemoveSubject'] ='你是否想一同移除该学科组别对应之网上教室?';	
$Lang['SysMgr']['SubjectClassMapping']['createNew'] ='新增';	
$Lang['SysMgr']['SubjectClassMapping']['newStd2eclass'] ='由科组加新的学生到网上教室';
$Lang['SysMgr']['SubjectClassMapping']['noneOfTheAbove'] ='不属于这课室的任何科组';
$Lang['SysMgr']['SubjectClassMapping']['selectFromExisting'] ='由现存的科组选取';
	
### If you have to update the IP20 wordings in IP25, please update here with the same variable name as IP20
$i_ClassNameNumber = "班别/学号";
$i_GroupCategoryNewDefaultRole = "预设小组职位";
$i_RoleDescription = "描述";
$i_GroupCategoryName = "类别";
$i_RoleTitle = "名称";
$i_RoleDescription = "简述";
$i_frontpage_assessment = "评量";

$Lang['SystemHelpter']['SavePageSettingsAsText'] = "Save Page Settings as Text";

### Session Timeout
$Lang['SessionTimeout']['ReadyToTimeout_Header'] = "自动登出提示";
$Lang['SessionTimeout']['ReadyToTimeout_str1'] = "由于你已经一段时间没有对系统进行任何操作，<b>系统将于";
$Lang['SessionTimeout']['ReadyToTimeout_str2'] = "秒后自动登出</b>。";
$Lang['SessionTimeout']['ReadyToTimeout_str3'] = "如要取消自动登出，请按";
$Lang['SessionTimeout']['ReadyToTimeout_str4'] = "。";
$Lang['SessionTimeout']['Continue'] = "继续使用";
$Lang['SessionTimeout']['CancelTimeout'] = "自动登出已经取消。";

$Lang['SessionTimeout']['Timeout_str1'] = "由于你已经一段时间没有对系统进行任何操作，系统已经自动登出。<br>如要再次使用系统，请按";
$Lang['SessionTimeout']['Timeout_str2'] = "。";
$Lang['SessionTimeout']['ReLogin'] = "重新登入";

$Lang['PromotionGuide'] = "学年过渡(升班)指引";
############################################################
#  Please add wodings at the below start at 2009-09-09  (Need to re-order later after checked by Claudio)
############################################################
$eEnrollment['front']['wish_enroll_front'] = "，希望参加&nbsp;";

$Lang['SysMgr']['FormClassMapping']['Term'] = "学期";
$Lang['SysMgr']['FormClassMapping']['EditClassNumber'] = "修改学号";
$Lang['SysMgr']['FormClassMapping']['ClassNumberWarning'] = "学号有误";
$Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedWarning'] = "已有学生使用此学号";
$Lang['SysMgr']['FormClassMapping']['ClassNumberDuplicatedinCSVWarning'] = "在已上传的档案中，已分配此学号予另一学生";
$Lang['SysMgr']['FormClassMapping']['ClassNumberMustBeWithinDigits'] = "学号不能超过九位数字";
$Lang['SysMgr']['FormClassMapping']['ClassGroup'] = "班别组别";
$Lang['SysMgr']['FormClassMapping']['ClassGroupCodeInvalid'] = "组别代号有误"; 
$Lang['SysMgr']['FormClassMapping']['ClassGroupDisabled'] = "组别已停用";
$Lang['SysMgr']['FormClassMapping']['ClassGroupCode'] = "组别代号";
$Lang['SysMgr']['FormClassMapping']['ClassGroupSettings'] = "班别组别设定";
$Lang['SysMgr']['FormClassMapping']['Reorder']['ClassGroup'] = "移动班别组别";
$Lang['SysMgr']['FormClassMapping']['DeleteTips']['ClassGroup'] = "删除班别组别";
$Lang['SysMgr']['FormClassMapping']['Add']['ClassGroup'] = "新增班别组别";
$Lang['SysMgr']['FormClassMapping']['Edit']['ClassGroup'] = "编辑班别组别";
$Lang['SysMgr']['FormClassMapping']['InUse'] = "使用中";
$Lang['SysMgr']['FormClassMapping']['NotInUse'] = "已停用";
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Success']['ClassGroup'] = '1|=|班别组别新增成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Add']['Failed']['ClassGroup'] = '0|=|班别组别新增失败。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Success']['ClassGroup'] = '1|=|班别组别编辑成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Edit']['Failed']['ClassGroup'] = '0|=|班别组别编辑失败。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Success']['ClassGroup'] = '1|=|班别组别删除成功。';
$Lang['SysMgr']['FormClassMapping']['ReturnMessage']['Delete']['Failed']['ClassGroup'] = '0|=|班别组别删除失败。';
$Lang['SysMgr']['FormClassMapping']['Warning']['DuplicatedCode']['ClassGroup'] = '* 已有其他班别组别使用此代号';
$Lang['SysMgr']['FormClassMapping']['jsWarning']['ConfirmDeleteClassGroup'] = '你是否确定删除此班别组别？';
$Lang['SysMgr']['FormClassMapping']['Select']['ClassGroup'] = '选择班别组别';

$Lang['SysMgr']['SubjectClassMapping']['ImportTeacherStudent'] = '汇入老师及学生';
$Lang['SysMgr']['SubjectClassMapping']['ExportTeacherStudent'] = '汇出老师及学生';
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectGroup'] = '汇入科组';
$Lang['SysMgr']['SubjectClassMapping']['ExportSubjectGroup'] = '汇出科组';
$Lang['SysMgr']['SubjectClassMapping']['ImportSubject'] = '汇入学科';
$Lang['SysMgr']['SubjectClassMapping']['ExportSubject'] = '汇出学科';
$Lang['SysMgr']['SubjectClassMapping']['ImportSubjectComponent'] = '汇入学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['ExportSubjectComponent'] = '汇出学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['Term'] = '学期';
$Lang['SysMgr']['SubjectClassMapping']['NoSubjectGroupWarning'] = '没有此科组';
$Lang['SysMgr']['SubjectClassMapping']['TeacherInSubjectGroupAlready'] = '老师已任教此科组';
$Lang['SysMgr']['SubjectClassMapping']['StudentInSubjectGroupAlready'] = '学生已加入此科组';
$Lang['SysMgr']['SubjectClassMapping']['StudentHasJoinedOtherSubjectGroupOfTheSameSubject'] = '学生已参加此科目的其他科组';
$Lang['SysMgr']['SubjectClassMapping']['StudentDoNotMatchFormOfSubjectGroup'] = '科组不允许此班级学生参加';
$Lang['SysMgr']['SubjectClassMapping']['NoSubjectWarning'] = '没有此科目';
$Lang['SysMgr']['SubjectClassMapping']['NoSubjectComponentWarning'] = '没有此学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['SubjectComponentCodeWarning'] = '代号属于学科分卷';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupExistWarning'] = '科组已存在';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupNotExistWarning'] = '没有此科组';
$Lang['SysMgr']['SubjectClassMapping']['NoFormWarning'] = '班级不存在';
$Lang['SysMgr']['SubjectClassMapping']['CreateEclassFieldNotVaild'] = '增加eClass一栏必需填写 "yes" 或 "no"';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeDuplicatedWithinCSVWarning'] = '在已上传的档案中有重复的科组编号';
$Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroupCodeDuplicatedWithinCSVWarning'] = '在已上传的档案中有重复的新科组编号';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCodeUsedWarning'] = '已有其他科组使用此新科组编号';
$Lang['SysMgr']['SubjectClassMapping']['BlankCodeWarning'] = '* 代号空白';
$Lang['SysMgr']['SubjectClassMapping']['Duplicated_LC_CodeWarning'] = '* 已有学习领域使用此代号';
$Lang['SysMgr']['SubjectClassMapping']['NoLearningCategoryWarning'] = '没有此学习领域';
$Lang['SysMgr']['SubjectClassMapping']['SubjectExistWarning'] = '学科已存在';
$Lang['SysMgr']['SubjectClassMapping']['SubjectCodeDuplicatedWithinCSVWarning'] = '在已上传的档案中有重复的科目编号';
$Lang['SysMgr']['SubjectClassMapping']['WebSAMSCodeInUseWarning'] = '已有其他学科或学科分卷使用此WebSAMS代号';
$Lang['SysMgr']['SubjectClassMapping']['ComponentCodeDuplicatedWithinCSVWarning'] = '在已上传的档案中有重复的科目分卷编号';
$Lang['SysMgr']['SubjectClassMapping']['DuplicatedStudent&SubjectGroupWithinCSVWarning'] = '在已上传的档案中，此用户已分配到同一学科分组';
$Lang['SysMgr']['SubjectClassMapping']['TeacherIsNotTeachingStaffWarning'] = '此用户不是教学职员';
$Lang['SysMgr']['SubjectClassMapping']['TimeClashWithOtherSubjectGroup'] = '此用户已参加其他相同时间上课的科组';
$Lang['SysMgr']['SubjectClassMapping']['SubjectCodeLongerThanThreeChar'] = 'WebSAMS代号不可长于三个字';
$Lang['SysMgr']['SubjectClassMapping']['SubjectCodeLongerThan10Char'] = 'WebSAMS代号不可长于10个字';

$Lang['SysMgr']['SubjectClassMapping']['SubjectCode'] = '学科WebSAMS编号';
$Lang['SysMgr']['SubjectClassMapping']['SubjectComponentCode'] = '学科分卷WebSAMS代号';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCode'] = '学科分组编号';
$Lang['SysMgr']['SubjectClassMapping']['LearningCategoryCode'] = '学习领域编号';
$Lang['SysMgr']['SubjectClassMapping']['ApplicableForm'] = '适用班级';
$Lang['SysMgr']['SubjectClassMapping']['CreateEclass'] = '建立网上课室';
$Lang['SysMgr']['SubjectClassMapping']['TimeTableConflict'] = '*以上学生应要移除，因他们已存在于其他学科组别，而在时间表设定上，那些组别与此组别发生冲突。';

// Copy Subject Group
$Lang['SysMgr']['SubjectClassMapping']['Copy'] = '复制';
$Lang['SysMgr']['SubjectClassMapping']['CopySubjectGroup'] = '复制学科组';
$Lang['SysMgr']['SubjectClassMapping']['CopyFromOtherYearTerm'] = '从其他年度/学期复制';
$Lang['SysMgr']['SubjectClassMapping']['Options'] = '选项';
$Lang['SysMgr']['SubjectClassMapping']['CopyResult'] = '复制结果';
$Lang['SysMgr']['SubjectClassMapping']['CopyFrom'] = '复制自';
$Lang['SysMgr']['SubjectClassMapping']['CopyFromAcademicYear'] = '复制自学年';
$Lang['SysMgr']['SubjectClassMapping']['CopyFromTerm'] = '复制自学期';
$Lang['SysMgr']['SubjectClassMapping']['CopyToAcademicYear'] = '复制到学年';
$Lang['SysMgr']['SubjectClassMapping']['CopyToTerm'] = '复制到学期';
$Lang['SysMgr']['SubjectClassMapping']['CopyTimetable'] = '复制时间表';
$Lang['SysMgr']['SubjectClassMapping']['SuccessCountSubjectGroup'] = '能够复制的科组数目';
$Lang['SysMgr']['SubjectClassMapping']['FailCountSubjectGroup'] = '不能复制的科组数目';
$Lang['SysMgr']['SubjectClassMapping']['FailureSubjectGroup'] = '不能复制的科组';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['SubjectGroupCopiedAlready'] = '已复制此科组到此学期';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['TimetableLocationInUse'] = '此科组于时间表中的位置已被使用';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['StudentTimeClash'] = '此科组有学生于时间表中发生时间冲突';
$Lang['SysMgr']['SubjectClassMapping']['jsWarning']['PleaseSelectAtLeastOneSubjectGroup'] = '请至少选择一科组。';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['CopySubjectGroupSuccess'] = '1|=|复制科组成功';
$Lang['SysMgr']['SubjectClassMapping']['ReturnMessage']['CopySubjectGroupFailed'] = '0|=|复制科组失败';
$Lang['SysMgr']['SubjectClassMapping']['QuickEdit'] = '快速编辑';
$Lang['SysMgr']['SubjectClassMapping']['NewSubjectGroupCode'] = '新学科分组编号';
$Lang['SysMgr']['SubjectClassMapping']['NewNameEn'] = '新名称(英文)';
$Lang['SysMgr']['SubjectClassMapping']['NewNameCh'] = '新名称(中文)';
$Lang['SysMgr']['SubjectClassMapping']['SubjectGroupCopySuccessfully'] = '科组复制成功';
$Lang['SysMgr']['SubjectClassMapping']['GoQuickEditInstruction'] = '系统已产生不同的科组编号至各复制科组。你可使用科组版面的「快速编辑」功能来更改各科组的编号及名称。';
$Lang['SysMgr']['SubjectClassMapping']['BackToSubjectGroupPage'] = '回到科组版面';
$Lang['SysMgr']['SubjectClassMapping']['WarningText'] = '警告';
$Lang['SysMgr']['SubjectClassMapping']['WarningArr']['CannotCopyStudentForDifferentAcademicYear'] = '如复制自不同学年，你将不能选择复制科组学生。';
$Lang['SysMgr']['SubjectClassMapping']['CreateeClassIfSubjectGroupDoesNotHaveOne'] = '如所选择的科组未有连结网上教室，为复制到本学期的科组建立网上教室';


$Lang['SysMgr']['RoleManagement']['LessonAttendance'] = '课堂出勤管理';

# Student Lesson Attendance
$Lang['LessonAttendance']['DailyLessonStatus'] = '每天课堂出勤情况';
$Lang['LessonAttendance']['LessonStatusAllConfirmed'] = '全部确认';
$Lang['LessonAttendance']['LessonStatusAllNotConfirmed'] = '全部未确认';
$Lang['LessonAttendance']['LessonStatusPartialConfirmed'] = '部份确认';
$Lang['LessonAttendance']['Confirmed'] = '已确认';
$Lang['LessonAttendance']['NotConfirmed'] = '未确认';
$Lang['LessonAttendance']['LessonClassDetailReport'] = '课堂每日报告';
$Lang['LessonAttendance']['Late'] = '迟到';
$Lang['LessonAttendance']['Absent'] = '缺席';
$Lang['LessonAttendance']['Present'] = '正常';
$Lang['LessonAttendance']['Outing'] = '外出';
$Lang['LessonAttendance']['InvalidDateFormat'] = '日期格式错误';
$Lang['LessonAttendance']['LessonSummaryReport'] = '课堂出勤总结';
$Lang['LessonAttendance']['FromDate'] = '开始日期';
$Lang['LessonAttendance']['ToDate'] = '结束日期';
$Lang['LessonAttendance']['LessonSessionCountAsDay'] = '课堂数目对应整日(缺点)';
$Lang['LessonAttendance']['LessonSessionLateCountAsDay'] = '课堂数目对应整日(迟到)';
$Lang['LessonAttendance']['LessionAttendanceSetting'] = '课堂出勤设定';
$Lang['LessonAttendance']['CountAsDay'] = '换算成日数';
$Lang['LessonAttendance']['ClassSeatingPlanSavedSuccess'] = "1|=|座位表储存成功";
$Lang['LessonAttendance']['LessonAttendanceDataSavedSuccess'] = "1|=|课堂出勤记录储存成功";
$Lang['LessonAttendance']['ImportAttendenceData'] = "汇入课堂出勤记录";
$Lang['LessonAttendance']['Status'] = "状况";
$Lang['LessonAttendance']['ImportAttendanceDataWarningMsg'] = "以下记录发生问题而不会汇入到系统中";

$Lang['StudentAttendance']['SMSTemplateNotSet'] = "未设定简讯范本。";

# Smart Card Student Attendance
$Lang['SmartCard']['StudentAttendence']['ReportSettings'] = "报告设定";
$Lang['SmartCard']['StudentAttendence']['CustomSymbol'] = "自定符号(最多三个字符)";
$Lang['SmartCard']['StudentAttendence']['DefaultSymbol'] = "预设符号";
$Lang['SmartCard']['StudentAttendence']['SchoolDaysStat'] = "上课日统计";

$Lang['SmartCard']['StudentAttendance']['CustomizeStatusSymbols'] = "自定状态符号";
$Lang['SmartCard']['StudentAttendance']['CustomizeReasonSymbols'] = "自定原因符号";
$Lang['SmartCard']['StudentAttendance']['AddRow'] = "新增列";
$Lang['SmartCard']['StudentAttendance']['DeleteRow'] = "删除此列";
$Lang['SmartCard']['StudentAttendance']['ReasonsAreDuplicated'] = "个原因重复了";
$Lang['SmartCard']['StudentAttendance']['SymbolsAreDuplicated'] = "个符号重复了";
$Lang['SmartCard']['StudentAttendance']['CustomizedReasonStat'] = "自定原因统计";

$Lang['SmartCard']['StudentAttendance']['PresetAbsenceInfo'] = "请假资讯";

# Smart Card Staff Attendance
$Lang['SmartCard']['StaffAttendence']['ConfirmTime'] = "最后确认时间";
$Lang['SmartCard']['StaffAttendence']['DutyChanged'] = "上班时间有变动";
$Lang['SmartCard']['StaffAttendence']['UpdateDuty'] = "更新上班时间";
$Lang['SmartCard']['StaffAttendence']['ReportSettings'] = "报告设定";
$Lang['SmartCard']['StaffAttendence']['CustomSymbol'] = "自定符号(最多三个字符)";
$Lang['SmartCard']['StaffAttendence']['DefaultSymbol'] = "预设符号";
$Lang['SmartCard']['StaffAttendence']['StaffMonthlyAttendanceReport'] = "教职员每月出席资料表";
$Lang['SmartCard']['StaffAttendence']['MonthlyAttendanceReport'] = "每月出席资料表";
$Lang['SmartCard']['StaffAttendence']['StatNormal'] = "正常数";
$Lang['SmartCard']['StaffAttendence']['StatAbsent'] = "缺席数";
$Lang['SmartCard']['StaffAttendence']['StatLate'] = "迟到数";
$Lang['SmartCard']['StaffAttendence']['StatEarlyLeave'] = "早退数";
$Lang['SmartCard']['StaffAttendence']['StatHoliday'] = "放假数";
$Lang['SmartCard']['StaffAttendence']['StatOutgoing'] = "外出数";
$Lang['SmartCard']['StaffAttendence']['ShowAllColumns'] = "显示所有栏目";
$Lang['SmartCard']['StaffAttendence']['SelectMethod'] = "选择方法";

$Lang['StudentAttendance']['AbsentSessions'] = "缺席堂数";
$Lang['StudentAttendance']['AbsentSessionsInfo'] = "缺席堂数资讯";
$Lang['StudentAttendance']['AbsentSessionsErrorMsg'] = "请于缺席堂数输入正整数";

$Lang['StudentAttendance']['DailyOperation_MissParentLetter'] = "未有家长信的缺席纪录";
$Lang['StudentAttendance']['NoOfDaySinceAbsentDay'] = "缺席日期后的日数";
$Lang['StudentAttendance']['AbsentDate'] = "缺席日期";
$Lang['StudentAttendance']['LastDisciplineRecord'] = "最后训导纪录日期";

# Payment
$Lang['ePayment']['POSTransactionReport'] = 'POS 交通记录';
$Lang['ePayment']['GrandTotal'] = '总数';
$Lang['ePayment']['InvoiceNumber'] = '发单编号';
$Lang['ePayment']['NoInvoiceNumber'] = '没有发票编号';
$Lang['ePayment']['ItemName'] = '项目名称';
$Lang['ePayment']['Quantity'] = '数量';
$Lang['ePayment']['UnitPrice'] = '单价';
$Lang['ePayment']['RefundReport'] = "退款报告";
$Lang['ePayment']['SelectFormat'] = "选择格式";
$Lang['ePayment']['Format_1'] = "格式 1"; 
$Lang['ePayment']['Format_2'] = "格式 2";
$Lang['Payment']['SignatureText'] = '签名显示';
$Lang['Payment']['StartingNextReceiptNumber'] = '起始收据号码';
$Lang['Payment']['ReceiptNumber'] = '收据号码';
$Lang['Payment']['StartingNextReceiptNumberWarning'] = '起始收据号码必须大于或等于 ';
$Lang['Payment']['StartingNextReceiptNumberNumberWarning'] = '请输入收据号码为数字';
$Lang['ePayment']['POSItemReport'] = "POS 项目统计";
$Lang['ePayment']['POSStudentReport'] = "POS 学生统计";
$Lang['ePayment']['PossibleDuplicatePPSWarning'] = "在系统中找到缴费灵号码/交易时间/总数相同的纪录. 此纪录可能是重复输入.";
$Lang['ePayment']['PossibleDuplicatePPSJSWarning'] = "在系统中找到缴费灵号码/交易时间/总数相同的纪录. 某此纪录可能是重复输入.\\n 确定继续要输入?";
$Lang['ePayment']['ExcludeParentSignature'] = "不显示家长签署<br /><font color='red'>(只适用于缴费通知书)</font>";
$Lang['ePayment']['ViewNotice'] = "检视相关通告";
$Lang['ePayment']['PaymentItemGenerateFromNotice'] = "产生自".$Lang['Header']['Menu']['eNotice']."的缴费项目";
$Lang['ePayment']['SystemProperties'] = "系统属性";
$Lang['ePayment']['SettineValue'] = "设定值";
$Lang['ePayment']['PaymentNoticeFeature'] = "缴费通告功能";
$Lang['ePayment']['SettingApplySuccess'] = "1|=|设定更新成功";

# wording of options in editing Payment Notice
$Lang['ePayment']['Options'] = "选项";
$Lang['ePayment']['NoOfOptions'] = "选项数量";
$Lang['ePayment']['WhetherToPay'] = "选择是否缴费";
$Lang['ePayment']['OnePaymentFromOneCategory'] = "缴付单一款项(单一缴费类别)";
$Lang['ePayment']['OnePaymentFromFewCategories'] = "缴付单一款项(多个缴费类别)";
$Lang['ePayment']['SeveralPaymentFromFewCategories'] = "缴付多笔款项(多个缴费类别)";

# Mun Sang Merit & Demerit Record (cust Report) 
$Lang['eDiscipline']['MonthlyMeritAndDemeritRecord'] = "学生奖惩报告";
$Lang['eDiscipline']['YearlyMeritAndDemeritRecord'] = "学生奖惩年度报告";
$Lang['eDiscipline']['MunSangAPReport_AwardRecord'] = "奖励纪录";
$Lang['eDiscipline']['MunSangAPReport_AwardEventDate'] = "奖励日期";
$Lang['eDiscipline']['MunSangAPReport_AwardItemName'] = "奖励事项";
$Lang['eDiscipline']['MunSangAPReport_Award'] = "奖励";
$Lang['eDiscipline']['MunSangAPReport_PunishRecord'] = "违规纪录";
$Lang['eDiscipline']['MunSangAPReport_PunishEventDate'] = "违规日期";
$Lang['eDiscipline']['MunSangAPReport_PunishItemName'] = "违规事项";
$Lang['eDiscipline']['MunSangAPReport_Punish'] = "处分";
$Lang['eDiscipline']['MunSangAPReport_RecordDate'] = "记录日期";
$Lang['eDiscipline']['MunSangAPReport_Others'] = "其他";
$Lang['eDiscipline']['MunSangSchoolName'] = "民生书院";
$Lang['eDiscipline']['MunSangMeritDemeritRecordTitle'] = "学生奖惩报告";
$Lang['eDiscipline']['MunSangReportNo'] = "报告编号";
$Lang['eDiscipline']['MunSangInputReportNo'] = "报告编号";
$Lang['eDiscipline']['MunSangDate'] = "日期";
$Lang['eDiscipline']['MunSangClass'] = "班级";
$Lang['eDiscipline']['MunSangStudentName'] = "姓名";
$Lang['eDiscipline']['MunSangTimes'] = "次";
$Lang['eDiscipline']['MunSangThisTermRecords'] = "本学年之累积奖惩纪录";
$Lang['eDiscipline']['MunSangTo'] = "至";
$Lang['eDiscipline']['MunSangSignOfTeacher'] = "班导师签署";
$Lang['eDiscipline']['MunSangSignOfTParent'] = "家长签署";
$Lang['eDiscipline']['MunSangFinish'] = "完";
$Lang['eDiscipline']['MunSangAchievement'] = "Achievement";
$Lang['eDiscipline']['MunSangServiceAward'] = "Service Award";
$Lang['eDiscipline']['MunSangBlackMark'] = "Black Mark";
$Lang['eDiscipline']['MunSangSubTotal'] = "Subtotal";
$Lang['eDiscipline']['SomeRecordsCannotAdd'] = "<font color=red>部份累积纪录新增失败。</font>\n";

# 伍少梅 AP report (cust)
$Lang['eDiscipline']['sdbnsm_record'] = "记";
$Lang['eDiscipline']['sdbnsm_times2'] = "次";
$Lang['eDiscipline']['sdbnsm_day'] = "天";
$Lang['eDiscipline']['sdbnsm_conductMark'] = "品行分";
$Lang['eDiscipline']['sdbnsm_mark'] = "分";
$Lang['eDiscipline']['sdbnsm_yy'] = "年";
$Lang['eDiscipline']['sdbnsm_mm'] = "月";
$Lang['eDiscipline']['sdbnsm_dd'] = "日";
$Lang['eDiscipline']['sdbnsm_comma'] = "，";
$Lang['eDiscipline']['sdbnsm_fullStop'] = "。";
$Lang['eDiscipline']['sdbnsm_monthlySummary'] = "本月总结";
$Lang['eDiscipline']['sdbnsm_scoreChange2'] = "加分";
$Lang['eDiscipline']['sdbnsm_add'] = "加";
$Lang['eDiscipline']['sdbnsm_minus'] = "扣";
$Lang['eDiscipline']['sdbnsm_otherAward'] = "其他记功事项";
$Lang['eDiscipline']['sdbnsm_otherPunishment'] = "其他记过事项";
$Lang['eDiscipline']['MunSangMonth'] = "月";
$Lang['eDiscipline']['MunSangTotal']  = "总计(次)";

# 汇基书院(东九龙) customized report [CRM Ref No.: 2009-0923-0926]
$Lang['eDiscipline']['uccke_TermAwardScheme'] = "学期奖励计划";


##### eDis
$eDiscipline['AP_Interval_Value'] = "奖惩纪录数目差额";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_4'] = "按此";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_3'] = "或";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_5'] = "检视科目编号。";

$Lang['eDiscipline']['Tag'] = "标签";
$Lang['eDiscipline']['TagName'] = "标签名称";
$Lang['eDiscipline']['TagList'] = "标签列表";
$Lang['eDiscipline']['NewTag'] = "新增标签";
$Lang['eDiscipline']['EditTag'] = "编辑标签";

$Lang['eDiscipline']['DisplayConductMarkInAP'] = "奖励及惩罚显示操行分";

$Lang['eDiscipline']['compare_different_periods_convertedDate'] = "不同时期的比较 (转换后资料)";

$Lang['eDiscipline']['ClassSummary_ConvertedData'] = "班别摘要 (转换后资料)";
$Lang['eDiscipline']['DisplayIn'] = "显示";
$Lang['eDiscipline']['Misconduct_Detail_Ranking_Report'] = "违规行为排名详细报告";
$Lang['eDiscipline']['AwardPunishment_Detail_Ranking_Report'] = "奖励及惩罚排名详细报告";
$Lang['eDiscipline']['Individual'] = "学生"; 
$Lang['eDiscipline']['OnOrAbove'] = "或以上";
$Lang['eDiscipline']['ListOf'] = "";
$Lang['eDiscipline']['ListOf2'] = "名单";
$Lang['eDiscipline']['GoodConductItem'] = "良好行为项目";
$Lang['eDiscipline']['MisconductItem'] = "违规行为项目";
$Lang['eDiscipline']['MisconductItemReportMsg'] = "排名报告显示违规项目最多的级别、班别或学生。请设定报告时段、对象、及排名方式。";
$Lang['eDiscipline']['AwardPunishmentItemReportMsg'] = "排名报告显示奖励或违规项目最多的级别、班别或学生。请设定报告时段、对象、及排名方式。";

$Lang['eDiscipline']['AddAPStopStep2'] = "完成设定";
$Lang['eDiscipline']['AddAPStopStep2_Remark'] = "如无需设定跟进行动及发送通知，请按\"完成设定\"，否则请按\"继续\"。";

$Lang['eDiscipline']['DetentinoSession_MAX'] = "留堂次数上限";
$Lang['eDiscipline']['Times'] = "次";
$Lang['eDiscipline']['ExportForDetention'] = "汇出留堂格式";
$Lang['eDiscipline']['TermAwardScheme'] = "学期奖励计划";
$Lang['eDiscipline']['NoMeritTypeInUse'] = "没有奖励项目使用中";
$Lang['eDiscipline']['NoDemeriTypeInUse'] = "没有惩罚项目使用中";
$Lang['eDiscipline']['AverageScore'] = "平均分";
$Lang['eDiscipline']['totalStudents'] = "班人数";

$Lang['eDiscipline']['uccke_MeritScore'] = "奖惩分";
$Lang['eDiscipline']['uccke_ConductGrade_Instruction'] = "请选择要计算的年度、学期及班别。";


$eDiscipline['MaxRecordDayBeforeAllow'] = "只容许输入最近特定日数的学务纪录";
$eDiscipline['JSWarning']['RecordDateIsNotAllow'] = "纪录日期无效";
$eDiscipline['Import']['Error']['RecordDateIsNotAllow'] = "纪录日期无效";
$Lang['eDiscipline']['AutoReleaseForAPRecord'] = "于批核后自动开放奖励及惩罚纪录";
$Lang['eDiscipline']['AutoReleaseAGM2AP'] = "自动开放转换自 累积良好行为/违规纪录 的 奖惩纪录";
$Lang['eDiscipline']['AutoReleasedToStudent'] = "纪录已自动开放予学生";
$Lang['eDiscipline']['JSWarning']['EventDateIsNotAllow'] = "发生日期无效";
$Lang['eDiscipline']['DuplicateStudent'] = "学生重复";
$Lang['eDiscipline']['JSWarning']['DetenedRecordCannotCancel'] = "已留堂记录不能取消";
$Lang['eDiscipline']['JSWarning']['AbsentRecordCannotCancel'] = "缺席记录不能取消";
$Lang['eDiscipline']['RejectRecordEmailNotification'] = "于奖励及惩罚纪录不批核后自动发送电子邮件通知";

$Lang['eDiscipline']['PrintNotice'] = "列印通告";

$Lang['eDiscipline']['displayConductMark'] = "显示操行分";
$Lang['eDiscipline']['GM_WeeklyReport'] = "良好及违规行为每周报告";
$Lang['eDiscipline']['Interval'] = "差额";
$Lang['eDiscipline']['TotalTimes'] = "总次数";
$Lang['eDiscipline']['MeetInterval'] = "达标差额";
$Lang['eDiscipline']['AP_AmountReport'] = "奖励及惩罚数量报告";
$Lang['eDiscipline']['WSCSS_MisconductReport'] = "违规行为报告";

$Lang['eDiscipline']['AccessRight_General_Right'] = "基本权限";
$Lang['eDiscipline']['AccessRight_GM_Right'] = "良好及违规行为权限";
$Lang['eDiscipline']['AccessRight_AP_Right'] = "奖励及惩罚权限";
$Lang['eDiscipline']['AccessRight_UserList'] = "成员列表";
$Lang['eDiscipline']['UnselectAll'] = "取消全选";
$Lang['eDiscipline']['SelectAll'] = "全选";
$Lang['eDiscipline']['Items'] = "项目";
$Lang['eDiscipline']['DisplayOtherTeacherGrade'] = "于「操行评级」显示其他科目老师的评级";
$Lang['eDiscipline']['SubmitAndNext'] = "呈送并继续";
$Lang['eDiscipline']['Follow'] = "依循";
$Lang['eDiscipline']['ConductMarkReport'] = "操行分报告";
$Lang['eDiscipline']['MissConductMarkRange'] = "请输入操行分范围";
$Lang['eDiscipline']['StartMarkLargeThenEndMark'] = "开始分数不可大于结束分数";

$iDiscipline['Good_Conduct_No_Conduct_Item'] = "没有此行为项目 / 此行为项目没有权限";
$iDiscipline['AP_No_Access_Right'] = "此项目没有权限";

$Lang['eDiscipline']['FieldTitle']['SeriousLate'] = "严重迟到";
$Lang['eDiscipline']['FieldTitle']['LateMinutes'] = "迟到分钟";
$Lang['eDiscipline']['SendEmailtoClassTeacherWhenStudentTriggerWarningReminderPoint'] = "如有学生触发操行分提示分数, 发出电子邮件通知班导师";
$Lang['eDiscipline']['SendEmailtoDisciplineAdminWhenStudentTriggerWarningReminderPoint'] = "如有学生触发操行分提示分数, 发出电子邮件通知".$eDiscipline["DisciplineAdmin2"];
$Lang['eDiscipline']['EMAIL_CONTENT_WARNING_RULE_str'] = "学生 %%_student_name_%% 的操行分 %%_to_score_%% 少于操行分提示分数 %%_reminder_point%%, 相关的纪录如下:<br>\n<br>\n";

$Lang['eDiscipline']['Report'] = "报告";
$Lang['eDiscipline']['RankingReport'] = "排名报告";
$Lang['eDiscipline']['RankingDetailReport'] = "排名详细报告";

$Lang['eDiscipline']['NeedToReassessInConductMeeting'] = "若第一栏\"注\"显示「<font color='red'>*</font>」即表示该生某些项目之评级须于操行会议作讨论。";
$Lang['eDiscipline']['InvalidConductCategory'] = "行为类别无效";
$Lang['eDiscipline']['CallForActionAfterEffectiveDate'] = "于特定日数后「等待批核」的奖励及惩罚纪录会被要求跟进";
$Lang['eDiscipline']['AP_ActionDueDate'] = "跟进限期";
$Lang['eDiscipline']['ActionDateCompareWithRecordDate'] = $i_RecordDate."需等如或早于".$Lang['eDiscipline']['AP_ActionDueDate'];
$Lang['eDiscipline']['PassedActionDueDate'] = "已过跟进限期";
$Lang['eDiscipline']['1stPeriod'] = "时段一";
$Lang['eDiscipline']['2ndPeriod'] = "时段二";
$Lang['eDiscipline']['3rdPeriod'] = "时段三";
$Lang['eDiscipline']['MFBMCLCT_ConductGradeReport'] = "年度操行评级报告";
$Lang['eDiscipline']['totalNoOf'] = "总数";
$Lang['eDiscipline']['1stTermGrade'] = "上学期操行分";
$Lang['eDiscipline']['2ndTermGrade'] = "下学期操行分";
$Lang['eDiscipline']['Appearance'] = "仪容";
$Lang['eDiscipline']['DateCannotInPast'] = "留堂日期不能早于今天";
$Lang['eDiscipline']['PIC_Not_Available'] = "负责人时间不允许";
$Lang['eDiscipline']['AbsentInConductGradeMeeting'] = "「操行评级会议」缺席名单";
$Lang['eDiscipline']['AbsentList'] = "缺席名单";
$Lang['eDiscipline']['MarkingType'] = "评分类别";
$Lang['eDiscipline']['Marking_NA'] = "不适用";
$Lang['eDiscipline']['Marking_OriginalGrade'] = "原有评级";
$Lang['eDiscipline']['SpecialCaseRevised'] = "特别个案(已完成)";

# eDis 20100125
$Lang["eDiscipline"]["ByYear"] = "按年";
$Lang["eDiscipline"]["BySemester"] = "按学期";
$Lang["eDiscipline"]["ByMonth"] = "按月份(整个月)";
$Lang["eDiscipline"]["BySeparatedMonth"] = "按月份 (学期分开)";
$Lang["eDiscipline"]["ConversionPeriod"] = "转换时段";

$Lang['eDiscipline']['DisplayMode'] = "显示形式";
$Lang['eDiscipline']['DisplayInSummary'] = "摘要";
$Lang['eDiscipline']['DisplayInDetails'] = "详细";

$Lang['eDiscipline']['NotAllowSameItemInSameDay'] = "不容许相同学生于同一天内有「".$eDiscipline['Award_and_Punishment']."」或「".$eDiscipline['Good_Conduct_and_Misconduct']."」相同的项目纪录";
$Lang['eDiscipline']['PaymentNoticeChecking'] = "请输入所有项目资料。";
$Lang['eDiscipline']['ItemAlreadyAdded'] = "已有相同项目在同一".$i_RecordDate."内";

$Lang['eDiscipline']['DisplaySchoolLogo'] = "显示校徽";
$Lang['eDiscipline']['DisplayWaivedRecord'] = "显示豁免纪录";
$Lang['eDiscipline']['DisplayWarningRecord'] = "显示警告纪录";

$Lang['eDiscipline']['APSetting_BatchEmptyInput'] = "不容许加入学生奖惩纪录时没有选择奖惩内容";
$Lang['eDiscipline']['EmptyInput'] = "";
$Lang['eDiscipline']['IsSelected'] = "没有撰择";
$Lang['eDiscipline']['IsEntered'] = "没有输入";

$Lang['eDiscipline']['AddNotice'] = "新增通告";
$Lang['eDiscipline']['DeleteNotice'] = "删除通告";
$Lang['eDiscipline']['ChangeAPItemStatusWarning'] = "部份".$eDiscipline['Good_Conduct_and_Misconduct']."的".$eDiscipline['Setting_Period']."正使用此项目，不能更改其状况。";
$Lang['eDiscipline']['AddToDiscipline'] = "新增至\"".$ip20TopMenu['eDisciplinev12']."\"的惩罚纪录";
$Lang['eDiscipline']['NewLeafTypeA'] = "类别A";
$Lang['eDiscipline']['NewLeafTypeB'] = "类别B";

$Lang['eDiscipline']['OnlyAdminSelectPIC'] = "只允许管理人员选择纪录负责人";
$Lang['eDiscipline']['PIC_SelectionDefaultOwn'] = "预设于\"良好及违规行为\"、\"奖励及惩罚\"、及\"个案纪录\"页面，显示当前用户的纪录(选\"否\"以显示所有用户的纪录)";

$Lang['eDiscipline']['SelectionMode'] = "选择模式";
$Lang['eDiscipline']['RecordisAutoReleased'] = "纪录自动开放";
$Lang['eDiscipline']['RecordisAutoApproved'] = "纪录自动批核";
$Lang['eDiscipline']['RecordCreatedBy'] = "纪录新增于";
$Lang['eDiscipline']['LastCaseNumberRef'] = "最大五个个案编号";
$Lang['eDiscipline']['AddMisconductForAbsent'] = "新增违规行为记录于缺席的学生";
$Lang['eDiscipline']['AddPunishmentForAbsent'] = "新增惩罚记录于缺席的学生";
$Lang['eDiscipline']['TeacherType'] = "教师类别";

$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_CategoryName'];
$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Period'];
$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$iDiscipline['MeritType'];
$Lang['eDiscipline']['Import_GM_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Status'];
$Lang['eDiscipline']['Import_GM_Col'][] = "电子通告模板";
$Lang['eDiscipline']['Import_GM_Col'][] = "严重迟到";
$Lang['eDiscipline']['Import_AlreadyExist'] = "已存在";
$Lang['eDiscipline']['Import_DoesNotExist'] = "不存在";
$Lang['eDiscipline']['ImportCategory'] = "汇入类别";
$Lang['eDiscipline']['ImportItem'] = "汇入项目";
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_ItemCode;
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_ItemName'];
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Category'] ;
$Lang['eDiscipline']['Import_GM_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Status'];
$Lang['eDiscipline']['Import_AP_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_ItemName'];
$Lang['eDiscipline']['Import_AP_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Type'];
$Lang['eDiscipline']['Import_AP_Col'][] = "<span class='tabletextrequire'>*</span>状况";
$Lang['eDiscipline']['Import_AP_Col'][] = "电子通告模板";
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_ItemCode;
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_ItemName'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$iDiscipline['Accumulative_Category'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Conduct_Mark'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_Subscore1;
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$i_Discipline_System_Quantity;
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline["Type"];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "<span class='tabletextrequire'>*</span>".$eDiscipline['Setting_Status'];
$Lang['eDiscipline']['Import_AP_Item_Col'][] = "标签";
$Lang['eDiscipline']['AP_Import_SubScore_Negative'] = $i_Discipline_System_Subscore1."应少于0";
$Lang['eDiscipline']['AP_Import_SubScore_Positive'] = $i_Discipline_System_Subscore1."应大于0";
$Lang['eDiscipline']['EventDate'] = "事件日期";

# Notes for import
$Lang['eDiscipline']['ImportSourceFileNote'] = "你可于\"资料档案\"栏输入CSV或TXT资料档的路径，或按\"浏览\"，从你的电脑中选取资料档。";
$Lang['eDiscipline']['Award_Punishment_Import_Instruct_Note_1_1'] = "你需于CSV或TXT资料档中以项目编号形式，指定每个纪录所属之奖惩项目。你可以";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_2_1'] = "请使用YYYY-MM-DD格式，于\"".$Lang['eDiscipline']['EventDate']."\"栏中输入纪录日期。";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_c_1'] = "储存CSV或TXT资料档。输入资料档的路径，或按\"浏览\"，从你的电脑中选取资料档，然后按<strong>继续</strong>。";
$Lang['eDiscipline']['Good_Conduct_and_Misconduct_Import_Instruct_Note_1_1'] = "你需于CSV或TXT资料档中以项目编号形式，指定每个纪录所属之良好及违规行为项目。你可以";
$Lang['eDiscipline']['Case_Record_Import_Instruct_Note_1_1'] = "请使用YYYY-MM-DD格式，于\"".$Lang['eDiscipline']['EventDate']."\"栏中输入纪录日期。";


##### eEnrolment (updated from ip20)
$eEnrollment['ToBeConfirmed'] = "待定";
$eEnrollment['invalid_admin_role'] = "职员职位不正确";
$eEnrollment['Member_Import_FileDescription'] = "\"班别\",\"学号\",\"职位\",\"表现\",\"老师评语\",\"活跃/非活跃\",\"职员职位\"";
$eEnrollment['All_Member_Import_FileDescription'] = "\"社团名称\", \"学期\", \"班别\",\"学号\",\"职位\",\"表现\",\"老师评语\",\"活跃/非活跃\",\"职员职位\"";
$eEnrollment['All_Member_Import_FileDescription_AdminRoleRemarks'] = "职员职位输入备注";
$eEnrollment['AdminRole'] = "职员职位";
$eEnrollment['All_Member_Import_FileDescription_PICTitle'] = "PIC";
$eEnrollment['All_Member_Import_FileDescription_HelperTitle'] = "HELPER";
$eEnrollment['All_Member_Import_FileDescription_NATitle'] = "N.A.";
$eEnrollment['All_Member_Import_FileDescription_PICRemarks'] = "负责人";
$eEnrollment['All_Member_Import_FileDescription_HelperRemarks'] = "点名助手";
$eEnrollment['All_Member_Import_FileDescription_NARemarks'] = "删除现有的职员职位";
$eEnrollment['All_Member_Import_FileDescription_UnchangeRemarks'] = "如没有输入职员职位，系统将不会更改学生现有的职员职位";
$eEnrollment['All_Participant_Import_FileDescription'] = "\"活动名称\", \"学号\",\"学号\",\"职位\",\"表现\",\"老师评语\",\"活跃/非活跃\",\"职员职位\"";

##### eSurvey
	$Lang['eSurvey']['SurveyList'] = "问卷调查清单";
	$Lang['eSurvey']['StartDate'] = "开始日期";
	$Lang['eSurvey']['EndDate'] = "结束日期";
	$Lang['eSurvey']['Title'] = "题目";
	$Lang['eSurvey']['Creator'] = "制作人";
	$Lang['eSurvey']['LastModified'] = "最后修改日期";
	$Lang['eSurvey']['NewSurvey'] = "新增问卷";
	$Lang['eSurvey']['EditSurvey'] = "编辑问卷";
	$Lang['eSurvey']['SurveyPreview'] = "问卷预览";
	$Lang['eSurvey']['StartDateWarning'] = "开始日期必须是今天或以后";
	$Lang['eSurvey']['PleaseConstructSurvey'] = "请制作问卷";
	$Lang['eSurvey']['InvalidSurvey'] = "问卷无效";
	$i_con_msg_survey_add = "<font color=green>已增加问卷。</font>\n";
	$i_con_msg_survey_update = "<font color=green>问卷已更新。</font>\n";
	$i_con_msg_survey_delete = "<font color=green>问卷已删除。</font>\n";
	$Lang['eSurvey']['SurveyType'] = "问卷对象";
	$Lang['eSurvey']['SpecificGroups'] = "指定小组";
	$Lang['eSurvey']['WholeSchool'] = "全校";
	$Lang['eSurvey']['CurrentSurvey'] = "现有问卷";
	$Lang['eSurvey']['PastSurvey'] = "昔日问卷";
	$Lang['eSurvey']['Answered'] = "已填";
	$Lang['eSurvey']['NotAnswered'] = "未填";
	$Lang['eSurvey']['AnsweredSurvey'] = "已完成问卷";
	$Lang['eSurvey']['NonAnswerSurvey'] = "待完成问卷";
	$Lang['eSurvey']['CompletedDate'] = "完成日期";
	$Lang['eSurvey']['Survey'] = "问卷调查";
	$Lang['eSurvey']['ViewSurveyResult'] = "检视问卷结果";
	$Lang['eSurvey']['SurveyForm'] = "问卷";

##### eNotice
$Lang['eNotice']['ConfirmForPrint'] = "「最后列印日期」将会被更新, 确定列印?";
$Lang['eNotice']['AllSignStatus'] = "所有签署状态";
$Lang['eNotice']['AllPrintStatus'] = "所有列印状态";
$Lang['eNotice']['Printed'] = "已列印";
$Lang['eNotice']['NonPrint'] = "未列印";
$Lang['eNotice']['PrintDate'] = "列印日期"; 
$Lang['eNotice']['AllNoticeTitle'] = "所有通告标题";
$Lang['eNotice']['PaymentNotice'] = "缴费通告";
$Lang['eNotice']['EnablePaymentNotice'] = "启动「缴费通告」";

$Lang['eNotice']['AllAwardRecords'] = "所有奖励纪录";
$Lang['eNotice']['AllPunishRecords'] = "所有惩罚纪录";
$Lang['eNotice']['AllGoodConductRecords'] = "所有良好行为纪录";
$Lang['eNotice']['AllMisconductRecords'] = "所有违规行为纪录";
$Lang['eNotice']['PaymentItemName'] = "缴费项目名称";

$Lang['eNotice']['NotEnoughBalance'] = "你的帐户余额不足，请增值。";
##### From IP20
$i_UserPhotoGuide = "相片只能以'.JPG'的格式上传，相片大小也规定为100 X 130 pixel (宽 x 高)。";
$eEnrollment['student_quota_exceeded'] = "超过学生想参与的社团上限。";

# iForm
$i_Form_pls_fill_in_title = "请输入题目!";

# SLS Library
$Lang['SLSLibrary'] = "SLS图书馆系统";

# SMS
$i_SMS['SMSStatus'][0] = '等待中';
$i_SMS['SMSStatus'][1] = '传送中';
$i_SMS['SMSStatus'][2] = '已接纳';
$i_SMS['SMSStatus'][4] = '不能传送';
$i_SMS['SMSStatus'][5] = '已预定传送';
$i_SMS['SMSStatus']['-39'] = '没有此简讯要求';

$i_SMS['SMSStatus']['-1'] = '学校代号错误';
$i_SMS['SMSStatus']['-2'] = '学校简讯登入帐户或密码错误';
$i_SMS['SMSStatus']['-4'] = '简讯帐户被锁上';
$i_SMS['SMSStatus']['-31'] = '简讯参考编号错误';
$i_SMS['SMSStatus']['-32'] = '简讯类别误';
$i_SMS['SMSStatus']['-99'] = '未能接驳到简讯中心';
$i_SMS['SMSStatus']['-100'] = '互联网接驳出现错误';
$i_SMS['SMSStatus']['FailToSendSMS'] = '传送失败';

$i_SMS['RefreshAllStatus'] = '重新整理所有状况';
$i_SMS['PleaseContactBLForEnquiry'] = '请到 "简讯服务 > 检视已发送讯息" 检视简讯状况及与硕阳数位科技技术支援部联络';
$i_SMS['MultipleStatus'] = '多项状况';

$i_SMS_Notice_Show_Student_With_MobileNo_Only = "不显示没有行动电话号码的收件人";

# eHomework
$Lang['SysMgr']['Homework']['Remark']['Remark'] = "备注";
$Lang['SysMgr']['Homework']['Remark']['NoStudent'] = "没有此学生";
$Lang['SysMgr']['Homework']['Remark']['GroupStudent'] = "此学生不属于此学科组别";
$Lang['SysMgr']['Homework']['Remark']['RecordsImportedSuccessfully'] = "个资料汇入成功";

$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DataColumn'] = "资料栏";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Reference'] = "请参考";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['RequiredField'] = "附有「<span class='tabletextrequire'>*</span>」的项目必须填写";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['Workload'] = "<span class='tabletextrequire'>^</span> 需时必须为整数, 每半小时为一个单位. 例: 2.5 代表 1.25 小时. (0 为少于0.25 小时, 17 为多于8 小时)";
$Lang['SysMgr']['Homework']['Import']['FieldTitle']['DateFormat'] = "<span class='tabletextrequire'>#</span> 日期的格式必须为 YYYY-MM-DD 或 YYYY/MM/DD. 如你使用 Excel 编辑, 请在日期的资料上更改储存格格式";

$Lang['SysMgr']['Homework']['ImportHomework'][] = "第一栏 : <font color=red>*</font> 学科代号";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第二栏 : <font color=red>*</font> 学科组别代号";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第三栏 : <font color=red>*</font> 标题";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第四栏 : <font color=red>*^</font> 工作量";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第五栏 : 老师";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第六栏 : <font color=red>*#</font> 开始日期";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第七栏 : <font color=red>*#</font> 限期";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第八栏 : <font color=red></font> 内容";
$Lang['SysMgr']['Homework']['ImportHomework'][] = "第九栏 : 须缴交";
$Lang['SysMgr']['Homework']['ImportHomework_Col10'] = "第十栏 : 班导师是否收回有关功课";
$Lang['SysMgr']['Homework']['ImportHomework_Col11'] = "功课类别";
$Lang['SysMgr']['Homework']['Column'] = "栏位";

$Lang['SysMgr']['Homework']['StudentList'] = "学生名单";
$Lang['SysMgr']['Homework']['ImportSubjectLeader'][] = "第一栏 : <font color=red>*</font> 学科组别代号";
$Lang['SysMgr']['Homework']['ImportSubjectLeader'][] = "第二栏 : <font color=red>*</font> 班别";
$Lang['SysMgr']['Homework']['ImportSubjectLeader'][] = "第三栏 : <font color=red>*</font> 座号";
$Lang['SysMgr']['Homework']['UseStartDateToGenerateList'] = "以功课开始日期来产日功课列表";
$Lang['SysMgr']['Homework']['MonthlyHomeworkList'] = "每月功课纪录表";
$Lang['SysMgr']['Homework']['currentMonth'] = "本月";

# added by Henry on 20100301
$Lang['SysMgr']['Homework']['MonthlyHomeworkListWarningSubjectLeader'] = "每月家课纪录表并不适用于本学期不是科长的学生用户。";
$Lang['SysMgr']['Homework']['MonthlyHomeworkListWarning'] = "每月家课纪录表并不适用于本学期没有任教任何科目的教师用户。";

$i_CampusLink_title = "校园连结";

# Role Management
$Lang['SysMgr']['RoleManagement']['StaffAttendance'] = "老师出勤系统管理";

# Staff Attendance V3
$Lang['StaffAttendance']['Management'] = "管理";
$Lang['StaffAttendance']['Attendance'] = "出勤";
$Lang['StaffAttendance']['Roster'] = "执勤人员表";
$Lang['StaffAttendance']['OTRecords'] = "超时工作记录";
$Lang['StaffAttendance']['LeaveAndAbsent'] = "请假及缺席";
$Lang['StaffAttendance']['WorkingDays'] = "工作日";
$Lang['StaffAttendance']['FunctionAccess'] = "功能权限";
$Lang['StaffAttendance']['Terminal'] = "终端机";
$Lang['StaffAttendance']['OT'] = "超时工作设定";
$Lang['StaffAttendance']['LeaveReason'] = "原因";
$Lang['StaffAttendance']['GroupMenuTitle'] = "组别";
$Lang['StaffAttendance']['Statistics'] = "统计";
$Lang['StaffAttendance']['Settings'] = "设定";
$Lang['StaffAttendance']['Report'] = "报告";
$Lang['StaffAttendance']['StaffAttendance'] = "职员出勤系统";
$Lang['StaffAttendance']['Group'] = "组别";
$Lang['StaffAttendance']['Individual'] = "个人";
$Lang['StaffAttendance']['GroupType'] = "组别类型";
$Lang['StaffAttendance']['NumberOfMember'] = "组员数目";
$Lang['StaffAttendance']['TimeSlot'] = "时间位置";
$Lang['StaffAttendance']['StartTime'] = "开始时间";
$Lang['StaffAttendance']['EndTime'] = "结束时间";
$Lang['StaffAttendance']['DayCount'] = "日数计算";
$Lang['StaffAttendance']['GroupList'] = "组别名单";
$Lang['StaffAttendance']['GroupTitle'] = "组别名称";
$Lang['StaffAttendance']['GroupTitleDuplicateWarning'] = "*组别名称重复或空白";
$Lang['StaffAttendance']['GroupRenameSuccess'] = "1|=|组别重新命名成功 ";
$Lang['StaffAttendance']['GroupRenameFail'] = "0|=|组别重新命名失败 ";
$Lang['StaffAttendance']['WorkingTimeSlot'] = "- 工作时间位置 -";
$Lang['StaffAttendance']['SlotName'] = "代号";
$Lang['StaffAttendance']['SlotStart'] = "开始";
$Lang['StaffAttendance']['SlotEnd'] = "结束";
$Lang['StaffAttendance']['WavieIn'] = "进入不需拍卡";
$Lang['StaffAttendance']['WavieOut'] = "离开不需拍卡";
$Lang['StaffAttendance']['AddTimeSlot'] = "新增时间位置";
$Lang['StaffAttendance']['WavieSetting'] = "拍卡设定";
$Lang['StaffAttendance']['SlotNameWarning'] = "*时间位置代号重复或空白";
$Lang['StaffAttendance']['SlotTimeFormatWarning'] = "*时间格式不符";
$Lang['StaffAttendance']['SlotTimeIntervalLimitWarning'] = "*开始与结束时间之间不能超过24 小时";
$Lang['StaffAttendance']['DutyCountFormatWarning'] = "*日数计算格式不符";
$Lang['StaffAttendance']['EG'] = "例如";
$Lang['StaffAttendance']['SaveSlotSuccess'] = "1|=|时间位置储存成功 ";
$Lang['StaffAttendance']['SaveSlotFail'] = "0|=|时间位置储存失败 ";
$Lang['StaffAttendance']['DeleteSlotSuccess'] = "1|=|时间位置移除成功 ";
$Lang['StaffAttendance']['DeleteSlotFail'] = "0|=|时间位置移除失败 ";
$Lang['StaffAttendance']['DeleteSlotConfirmMessage'] = "移除时间位置时，所有相关的值勤设定也会一拼移除。确定要继续？";
$Lang['StaffAttendance']['Staff'] = '- 成员 -';
$Lang['StaffAttendance']['StaffName'] = '名称';
$Lang['StaffAttendance']['AddUser'] = '新增成员';
$Lang['StaffAttendance']['AddMemberSuccess'] = "1|=|新增成员成功 ";
$Lang['StaffAttendance']['AddMemberFail'] = "0|=|新增成员失败 ";
$Lang['StaffAttendance']['DeleteMemberConfirmMessage'] = "当从组别中移除成员时。所有相关的值勤设定也会一拼移除。确定要继续？";
$Lang['StaffAttendance']['SelectAtLeastOneWarning'] = "请选取最少一名用户";
$Lang['StaffAttendance']['DeleteMemberSuccess'] = "1|=|成员移除成功 ";
$Lang['StaffAttendance']['DeleteMemberFail'] = "0|=|成员移除失败 ";
$Lang['StaffAttendance']['DeleteGroupWarning'] = "移除组别会将所有成员己设定系值勤设定一拼移除，确定要继续？";
$Lang['StaffAttendance']['DeleteGroupSuccess'] = "1|=|组别移除成功 ";
$Lang['StaffAttendance']['DeleteGroupFail'] = "0|=|组别移除失败 ";
$Lang['StaffAttendance']['AddGroup'] = "新增组别";
$Lang['StaffAttendance']['Normal'] = "正常";
$Lang['StaffAttendance']['Shifting'] = "轮班";
$Lang['StaffAttendance']['SaveGroupSuccess'] = "1|=|组别储存成功 ";
$Lang['StaffAttendance']['SaveGroupFail'] = "0|=|组别储存失败 ";
$Lang['StaffAttendance']['AddMemberWarning'] = "系统将会清除新增组别成员的个人时间位置/值勤设定。确定要继续？";
$Lang['StaffAttendance']['StaffWorkingPeriod'] = "职员工作记录";
$Lang['StaffAttendance']['PeriodStart'] = "开始日期";
$Lang['StaffAttendance']['PeriodEnd'] = "结束日期";
$Lang['StaffAttendance']['AddTWorkingPeriod'] = "增加工作记录";
$Lang['StaffAttendance']['WorkingPeriodOverlapWarning'] = "*工作记录重迭";
$Lang['StaffAttendance']['WorkingPeriodOverlapWarning'] = "*雇用期重迭";
$Lang['StaffAttendance']['WorkingPeriodNullWarning'] = "*请最少输入开始日期或结束日期";
$Lang['StaffAttendance']['SaveWorkingPeriodSuccess'] = "1|=|工作记录储存成功 ";
$Lang['StaffAttendance']['SaveWorkingPeriodFail'] = "0|=|工作记录储存失败 ";
$Lang['StaffAttendance']['DeleteWorkingPeriodSuccess'] = "1|=|工作记录移除成功 ";
$Lang['StaffAttendance']['DeleteWorkingPeriodFail'] = "0|=|工作记录移除失败 ";
$Lang['StaffAttendance']['WorkingPeriodEndDateLesserThenStartDateWarning'] = "*结束日期必须在开始日期之后";
$Lang['StaffAttendance']['From'] = "由";
$Lang['StaffAttendance']['To'] = "至";
$Lang['StaffAttendance']['SkipSchoolCalendar'] = "略过值勤的校历表事件";
$Lang['StaffAttendance']['AddDuty'] = "增加值勤记录";
$Lang['StaffAttendance']['EffectiveDate'] = "有效日期";
$Lang['StaffAttendance']['EffectiveDateNullWarning'] = "*请输入开始日期及结束日期 ";
$Lang['StaffAttendance']['EffectiveEndDateLesserThenStartDateWarning'] = "*结束日期必须在开始日期之后 ";
$Lang['StaffAttendance']['EffectiveDateOverlapWarning'] = "*有效日期重迭";
$Lang['StaffAttendance']['SaveNormalGroupDutyPeriodSuccess'] = "1|=|组别值勤区间设定成功 ";
$Lang['StaffAttendance']['SaveNormalGroupDutyPeriodFail'] = "0|=|组别值勤区间设定失败 ";
$Lang['StaffAttendance']['DutyPeriodList'] = "值勤区间表";
$Lang['StaffAttendance']['Mon'] = "一";
$Lang['StaffAttendance']['Tue'] = "二";
$Lang['StaffAttendance']['Wed'] = "三";
$Lang['StaffAttendance']['Thur'] = "四";
$Lang['StaffAttendance']['Fri'] = "五";
$Lang['StaffAttendance']['Sat'] = "六";
$Lang['StaffAttendance']['Sun'] = "日";
$Lang['StaffAttendance']['NotAssigned'] = "未指派";
$Lang['StaffAttendance']['WeekDay'] = "星期";
$Lang['StaffAttendance']['OnDuty'] = "当值";
$Lang['StaffAttendance']['Leave'] = "休假";
$Lang['StaffAttendance']['EditRoster'] = "更改值勤表";
$Lang['StaffAttendance']['NumberOfDutyPeriod'] = "值勤区间数目";
$Lang['StaffAttendance']['ApplyTo'] = "应用到";
$Lang['StaffAttendance']['StaffList'] = "职员名单";
$Lang['StaffAttendance']['HideList'] = "开关";
$Lang['StaffAttendance']['DutyOverlapWarning'] = "以下职员被指派的时间位置当值时间重迭 ";
$Lang['StaffAttendance']['DutyOver24HourWarning'] = "以下职员被指派的时间位置超过二十四小时 ";
$Lang['StaffAttendance']['DutyWarning'] = "请修正以上问题 ";
$Lang['StaffAttendance']['DutyPeriodDutySaveSuccess'] = "1|=|值勤区间配置储存成功 ";
$Lang['StaffAttendance']['DutyPeriodDutySaveFail'] = "0|=|值勤区间配置储存失败 ";
$Lang['StaffAttendance']['DayOfWeekWarning'] = "*请选择应用到最少一天";
$Lang['StaffAttendance']['DutyApplyDuty'] = "请最少选择一个时间位置 ";
$Lang['StaffAttendance']['SelectDeSelectAll'] = "取消/ 选取全部";
$Lang['StaffAttendance']['SpecialDateSetting'] = "特别日子设定";
$Lang['StaffAttendance']['Month'] = "月份";
$Lang['StaffAttendance']['DutyCalendar'] = "值勤日历";
$Lang['StaffAttendance']['StaffSummary'] = "职员总结";
$Lang['StaffAttendance']['FullDayOutgoing'] = "全日外出工作";
$Lang['StaffAttendance']['FullDayHoliday'] = "全日假期";
$Lang['StaffAttendance']['FullDaySetting'] = "请假要求";
$Lang['StaffAttendance']['DutySetting'] = "值勤设定";
$Lang['StaffAttendance']['EditFullDaySetting'] = "更改请假要求";
$Lang['StaffAttendance']['SettingType'] = "设定类型";
$Lang['StaffAttendance']['FullDaySettingNullWarning'] = "请输入开始及结束日期";
$Lang['StaffAttendance']['FullDaySettingEndDayBeforeStartWarning'] = "结束日期必须在开始日期以后或相同";
$Lang['StaffAttendance']['StartDate'] = '开始日期';
$Lang['StaffAttendance']['EndDate'] = '结束日期';
$Lang['StaffAttendance']['NoSetting'] = '取消设定';
$Lang['StaffAttendance']['FullDaySettingSaveSuccess'] = '1|=|请假要求储存成功 ';
$Lang['StaffAttendance']['FullDaySettingSaveFail'] = '0|=|请假要求储存失败 ';
$Lang['StaffAttendance']['FullDayReason'] = '原因';
$Lang['StaffAttendance']['Outgoing'] = '外出工作';
$Lang['StaffAttendance']['Leave'] = '放假';
$Lang['StaffAttendance']['Total'] = '总共';
$Lang['StaffAttendance']['TargetDateSelectWarning'] = '请最少选取一天';
$Lang['StaffAttendance']['ShiftDutySaveSuccess'] = '1|=|特别值勤设定成功 ';
$Lang['StaffAttendance']['ShiftDutySaveFail'] = '0|=|特别值勤设定失败 ';
$Lang['StaffAttendance']['SelectReason'] = '- 选择原因 -';
$Lang['StaffAttendance']['StaffList'] = '职员名单';
$Lang['StaffAttendance']['SaveDutyPeriodSuccess'] = "1|=|值勤区间设定成功 ";
$Lang['StaffAttendance']['SaveDutyPeriodFail'] = "0|=|值勤区间设定失败 ";
$Lang['StaffAttendance']['DeleteDutyPeriodSuccess'] = "1|=|值勤区间移除成功 ";
$Lang['StaffAttendance']['DeleteDutyPeriodFail'] = "0|=|值勤区间移除失败 ";
$Lang['StaffAttendance']['DutyPeriodDeleteConfim'] = "确定要移除值勤区间？ ";
$Lang['StaffAttendance']['Config'] = "设定";
$Lang['StaffAttendance']['FullTimeSlotOption'] = "全日/ 时段要求";
$Lang['StaffAttendance']['FullDay'] = "全日";
$Lang['StaffAttendance']['TargetDate'] = "日期";
$Lang['StaffAttendance']['DeleteFullDaySettingConfirm'] = "确定要移除请假设定？ ";
$Lang['StaffAttendance']['EditConfirmedDailyRecord'] = "修改已确认值勤记录 ";
$Lang['StaffAttendance']['EditTimeSlot'] = "更改时间位置";
$Lang['StaffAttendance']['Description'] = "叙述";
$Lang['StaffAttendance']['AccessRight'] = "存取权限";
$Lang['StaffAttendance']['View'] = "检视";
$Lang['StaffAttendance']['Manage'] = "管理";
$Lang['StaffAttendance']['Redeem'] = "补偿";
$Lang['StaffAttendance']['Access'] = "存取";

# Settings - Terminal Settings
$Lang['StaffAttendance']['TerminalSettings'] = "智慧卡终端机设定";
$Lang['StaffAttendance']['TerminalIPList'] = "终端机的 IP 位址";
$Lang['StaffAttendance']['TerminalIPInput'] = "请把终端机的 IP 地址每行一个输入";
$Lang['StaffAttendance']['TerminalYourAddress'] = "你目前的 IP 地址";
$Lang['StaffAttendance']['TerminalIgnorePeriod'] = "不接受重复读卡的时间";
$Lang['StaffAttendance']['TerminalIgnorePeriodUnit'] = "分钟";
$Lang['StaffAttendance']['TerminalRemarks'] = "备注";
$Lang['StaffAttendance']['DescriptionTerminalIPSettings'] = "
在 IP 位址的格上, 可以使用:
<ol><li>固定 IP 位址 (e.g. 192.168.0.101)</li>
<li>IP 位址一个范围 (e.g. 192.168.0.[10-100])</li>
<li>CISCO Style 范围(e.g. 192.168.0.0/24)</li>
<li>容许所有位址 (输入 0.0.0.0)</li></ol>
<span class='tabletextrequire'>连接读咭器的电脑, 建议该电脑设定固定 IP 地址 (即不采用 DHCP), 以确保资料不会被其他电脑更改.</span><br>
本系统设有安全措施防范假冒的IP 地址 (Faked IP Address).
";
$Lang['StaffAttendance']['SettingApplySuccess'] = "1|=|设定更新成功";
$Lang['StaffAttendance']['SettingApplyFail'] = "0|=|设定更新失败";

$Lang['StaffAttendance']['IgnoreForOT'] = "在工作日不当作<br />超时工作的时间";
$Lang['StaffAttendance']['NoOTTime'] = "分钟 (999 = 不设超时工作)";
$Lang['StaffAttendance']['CountAsOT'] = "在非工作日当作<br />超时工作";
$Lang['StaffAttendance']['ReasonTypeSettings'] = "原因类别设定";
$Lang['StaffAttendance']['Name'] = "名称";
$Lang['StaffAttendance']['ReasonType'] = "原因类别";
$Lang['StaffAttendance']['Reason'] = "原因";
$Lang['StaffAttendance']['ReportSymbol'] = "报告用符号";
$Lang['StaffAttendance']['Waived'] = "豁免";
$Lang['StaffAttendance']['NotWaived'] = "不豁免";
$Lang['StaffAttendance']['Status'] = "状况";
$Lang['StaffAttendance']['AddReason'] = "新增原因";
$Lang['StaffAttendance']['Active'] = "已使用";
$Lang['StaffAttendance']['InActive'] = "未使用";
$Lang['StaffAttendance']['DeleteReasonSuccess'] = "1|=|成功删除原因";
$Lang['StaffAttendance']['DeleteReasonFail'] = "0|=|删除原因失败";
$Lang['StaffAttendance']['SaveReasonSuccess'] = "1|=|成功保存原因";
$Lang['StaffAttendance']['SaveReasonFail'] = "0|=|保存原因失败";
$Lang['StaffAttendance']['ConfirmDeleteReason'] = "确定要删除此原因?";
$Lang['StaffAttendance']['ReasonReportSymbolExists'] = "原因/报告用符号已经存在";
$Lang['StaffAttendance']['EmptyReason'] = "原因不能留空";
$Lang['StaffAttendance']['GroupName'] = "组别名称";
$Lang['StaffAttendance']['AttendanceRecord'] = "出席记录";
$Lang['StaffAttendance']['InOutRecord'] = "进出记录";
$Lang['StaffAttendance']['AlertTakeFutureAttendance'] = "不能预先确认未来的出席记录";
$Lang['StaffAttendance']['WorkingTimeSlotHeader'] = "工作时间";
$Lang['StaffAttendance']['Leave2'] = "不在校";
$Lang['StaffAttendance']['LastUpdated'] = "最近更新";
$Lang['StaffAttendance']['NoTimeSlot'] = "无工作时间";
$Lang['StaffAttendance']['UnAssignedOff'] = "未分配<br>(放假)";
$Lang['StaffAttendance']['Summary'] = "撮要";
$Lang['StaffAttendance']['SessionAttendance'] = "出席时段";
$Lang['StaffAttendance']['AllStatus'] = "所有状况";
$Lang['StaffAttendance']['Absent'] = "缺席";
$Lang['StaffAttendance']['Present'] = "出席";
$Lang['StaffAttendance']['Late'] = "迟到";
$Lang['StaffAttendance']['EarlyLeave'] = "早退";
$Lang['StaffAttendance']['Outing'] = "外出";
$Lang['StaffAttendance']['Holiday'] = "放假";
$Lang['StaffAttendance']['AbsentSuspected'] = "怀疑缺席";
$Lang['StaffAttendance']['EditAttendance'] = "编辑出席资料";
$Lang['StaffAttendance']['RosterDuty'] = "执勤职责";
$Lang['StaffAttendance']['In'] = "进入";
$Lang['StaffAttendance']['Out'] = "离开";
$Lang['StaffAttendance']['InStation'] = "进入拍卡地点";
$Lang['StaffAttendance']['OutStation'] = "离开拍卡地点";
$Lang['StaffAttendance']['Remark'] = "备注";
$Lang['StaffAttendance']['NoDuty'] = "不用执勤";
$Lang['StaffAttendance']['AllDuty'] = "所有执勤人员";
$Lang['StaffAttendance']['NonWorking'] = "非工作人员";
$Lang['StaffAttendance']['EditStatus'] = "编辑状况";
$Lang['StaffAttendance']['Duty'] = "执勤";
$Lang['StaffAttendance']['TakeAttendanceSuccess'] = "1|=|出席记录确认成功";
$Lang['StaffAttendance']['TakeAttendanceFail'] = "0|=|出席记录确认失败";
$Lang['StaffAttendance']['TakeViewFutureOTWarning'] = "不能检视/编辑未来的超时工作记录";
$Lang['StaffAttendance']['InvalidRedeemWarning'] = "扣除分钟不正确";
$Lang['StaffAttendance']['NoOTRecordWarning'] = "当天没有超时工作记录/扣除分钟数目超过当日的总超时工作时间";
$Lang['StaffAttendance']['InvalidDateRange'] = "日期范围不正确";
$Lang['StaffAttendance']['AllStaff'] = "所有教职员";
$Lang['StaffAttendance']['AtSchool'] = "在校";
$Lang['StaffAttendance']['StaffWithOutstandingMins'] = "未扣除超时时间的教职员";
$Lang['StaffAttendance']['TotalOTMins'] = "总超时分钟";
$Lang['StaffAttendance']['RedeemMins'] = "扣除分钟";
$Lang['StaffAttendance']['OutstandingMins'] = "剩余分钟";
$Lang['StaffAttendance']['OTSummaryList'] = "超时工作撮要表";
$Lang['StaffAttendance']['RedeemRecord'] = "扣除记录";
$Lang['StaffAttendance']['PleaseSelect'] = "请选择";
$Lang['StaffAttendance']['Today'] = "今天";
$Lang['StaffAttendance']['Yesterday'] = "昨天";
$Lang['StaffAttendance']['ThisMonth'] = "本月";
$Lang['StaffAttendance']['SelectPeriod'] = "选择时段";
$Lang['StaffAttendance']['OTMins'] = "超时分钟";
$Lang['StaffAttendance']['RedeemDate'] = "扣除超时分钟的日期";
$Lang['StaffAttendance']['RedeemRecordSuccess'] = "1|=|成功扣除超时记录";
$Lang['StaffAttendance']['RedeemRecordFail'] = "0|=|扣除超时记录失败";
$Lang['StaffAttendance']['DailyLog'] = "每日出席记录";
$Lang['StaffAttendance']['CustomizeAttendanceReport'] = "用户自订出席报告";
$Lang['StaffAttendance']['MonthlySummary'] = "每月总结";
$Lang['StaffAttendance']['DailyDetails'] = "每日详细资料";
$Lang['StaffAttendance']['DaysOfAttendanceStatus'] = "出席状况的日数";
$Lang['StaffAttendance']['DutyTime'] = "执勤时间";
$Lang['StaffAttendance']['RecordTime'] = "记录时间";
$Lang['StaffAttendance']['LateMins'] = "迟到分钟";
$Lang['StaffAttendance']['EarlyLeaveMins'] = "早退分钟";
$Lang['StaffAttendance']['Location'] = "地点";
$Lang['StaffAttendance']['HideReportOptions'] = "隐藏报告选项";
$Lang['StaffAttendance']['ShowReportOptions'] = "显示报告选项";
$Lang['StaffAttendance']['PleaseChoose'] = "请作出配搭";
$Lang['StaffAttendance']['ReportType'] = "报告类别";
$Lang['StaffAttendance']['PersonalReport'] = "个人报告";
$Lang['StaffAttendance']['GroupReport'] = "组别报告";
$Lang['StaffAttendance']['Target'] = "目标";
$Lang['StaffAttendance']['PeriodType'] = "时间类别";
$Lang['StaffAttendance']['MonthlyReport'] = "每月报告";
$Lang['StaffAttendance']['WeeklyReport'] = "每周报告";
$Lang['StaffAttendance']['DailyReport'] = "每日报告";
$Lang['StaffAttendance']['Format'] = "格式";
$Lang['StaffAttendance']['RecordDetails'] = "详细记录";
$Lang['StaffAttendance']['SummaryStatistic'] = "简要记录";
$Lang['StaffAttendance']['WithColumns'] = "显示栏目";
$Lang['StaffAttendance']['InOutTime'] = "进出时间";
$Lang['StaffAttendance']['InOutStation'] = "进出拍卡地点";
$Lang['StaffAttendance']['SlotName2'] = "工作时区名称";
$Lang['StaffAttendance']['InTime'] = "进入时间";
$Lang['StaffAttendance']['OutTime'] = "离开时间";
$Lang['StaffAttendance']['ChangeGroupDescriptionSuccess'] = "1|=|成功更改组别叙述";
$Lang['StaffAttendance']['ChangeGroupDescriptionFail'] = "0|=|更改组别叙述失败";
$Lang['StaffAttendance']['FunctionAccessSettings'] = "功能权限设定";
$Lang['StaffAttendance']['AccessRightDeleteGroupWarning'] = "你确定要删除此权限组别?";
$Lang['StaffAttendance']['AccessRightDeleteGroupMemberWarning'] = "你确定要删除所选权限组别的组员?";
$Lang['StaffAttendance']['UserType'] = "成员类别";
$Lang['StaffAttendance']['RosterReport'] = "执勤人员报告";
$Lang['StaffAttendance']['Max3Chars'] = "最多三个字";
$Lang['StaffAttendance']['WeekdayDistribution'] = "周日分布";
$Lang['StaffAttendance']['TimesOf'] = "次数";
$Lang['StaffAttendance']['ShowDetail'] = "显示细节";
$Lang['StaffAttendance']['HideDetail'] = "隐藏细节";
$Lang['StaffAttendance']['OTReport'] = "超时工作报告";
$Lang['StaffAttendance']['WorkPeriodImportDesc'] = "第一栏 : 名字<br />第二栏 : 开始日期 (YYYY-MM-DD)<br />第三栏 : 结束日期 (YYYY-MM-DD)";
$Lang['StaffAttendance']['DataImportSuccess'] = "1|=|资料汇入成功";
$Lang['StaffAttendance']['DataImportFail'] = "0|=|资料汇入失败";
$Lang['StaffAttendance']['Months'] = array('','一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月');
$Lang['StaffAttendance']['Mins'] = "分钟";
$Lang['StaffAttendance']['Day'] = "日";
$Lang['StaffAttendance']['InEmploymentPeriod'] = "现职";
$Lang['StaffAttendance']['NotInEmploymentPeriod'] = "非现职";
$Lang['StaffAttendance']['OtherIndividuals'] = "个人";
$Lang['StaffAttendance']['DutySetup'] = "职务设定";
$Lang['StaffAttendance']['AttendanceReport'] = "出席报告";
$Lang['StaffAttendance']['DailyLogReport'] = "每日出席纪录";
$Lang['StaffAttendance']['EntryLogReport'] = "完整出入纪录";
$Lang['StaffAttendance']['EntryLogSelectStaffWarning'] = "请最少选择一位职员";
$Lang['StaffAttendance']['RecordDate'] = "纪录日期";
$Lang['StaffAttendance']['RenameGroup'] = "重新命名";
$Lang['StaffAttendance']['ManageMember'] = "管理成员";
$Lang['StaffAttendance']['LastUpdateOn'] = "最后修改时间:";
$Lang['StaffAttendance']['StaffInfo'] = "职员资讯";
$Lang['StaffAttendance']['CardID'] = "智能卡号码";
$Lang['StaffAttendance']['SmartcardWarning'] = "智能卡号码已被其他用户使用";
$Lang['StaffAttendance']['SmartcardIDUpdateSuccess'] = "1|=|智能卡号码更新成功";
$Lang['StaffAttendance']['SmartcardIDUpdateFail'] = "0|=|智能卡号码更新失败";
$Lang['StaffAttendance']['StaffInfoImportDesc'] = "第一栏: 职员登入名称<br />第二栏: 智能卡号码";
$Lang['StaffAttendance']['StaffNotExists'] = "此用户不存在";
$Lang['StaffAttendance']['NoDataFound'] = "没有资料";
$Lang['StaffAttendance']['ImportedAlready'] = "此职员被汇入多于一次";
$Lang['StaffAttendance']['SelectCSVFile'] = "选择CSV档案";
$Lang['StaffAttendance']['CSVConfirmation'] = "确定";
$Lang['StaffAttendance']['ImportResult'] = "汇入结果";
$Lang['StaffAttendance']['UserLogin'] = "职员登入名称";
$Lang['StaffAttendance']['BasicDutySetting'] = "常规职务";
$Lang['StaffAttendance']['SpecialDutySetting'] = "特别职务";
$Lang['StaffAttendance']['SetupStatus'] = "设定状况";
$Lang['StaffAttendance']['MemberStatus'] = "成员";
$Lang['StaffAttendance']['DutyPeriodStatus'] = "职务时段";
$Lang['StaffAttendance']['SettingCompletedStatus'] = "完成";
$Lang['StaffAttendance']['SettingNotCompletedStatus'] = "立即设定";
$Lang['StaffAttendance']['ClickToSetup'] = "按此设定";
$Lang['StaffAttendance']['ClickToView'] = "按此检视";
$Lang['StaffAttendance']['ImportOfflineRecords'] = "汇入离线纪录";
$Lang['StaffAttendance']['ImportOfflineRecordsDesc'] = "请把时间一项的格式设定为 YYYY-MM-DD HH:mm:ss<br />档案内的所有纪录需为同一天的纪录";
$Lang['StaffAttendance']['TimeRecorded'] = "纪录时间";
$Lang['StaffAttendance']['Site'] = "纪录地点";
$Lang['StaffAttendance']['SmartCardID'] = "智能咭 ID";
$Lang['StaffAttendance']['ImportInvalidEntries'] = "汇入资料错误";
$Lang['StaffAttendance']['ImportWarningOneDayOnly'] = "并非所有纪录为同一天的纪录，请分开处理。";
$Lang['StaffAttendance']['AbsenceReport'] = "缺席纪录";
$Lang['StaffAttendance']['LeaveAbsenceReport'] = "请假缺席统计";
$Lang['StaffAttendance']['SaveAndManageMember'] = "储存及管理成员";
$Lang['StaffAttendance']['SaveAndManageTimeSlot'] = $Lang['Btn']['Add']."及新增".$Lang['StaffAttendance']['TimeSlot'];
$Lang['StaffAttendance']['SaveAndManageDutyPeriod'] = $Lang['Btn']['Submit']."及管理职务时段";
$Lang['StaffAttendance']['SaveAndManageRoster'] = $Lang['Btn']['Submit']."及".$Lang['StaffAttendance']['Config'];
$Lang['StaffAttendance']['WholeSchoolSpecialDaySetting'] = '全校性特别职务';
$Lang['StaffAttendance']['Individuals'] = "个人";
$Lang['StaffAttendance']['SelectBy'] = "统计类型";
$Lang['StaffAttendance']['DateRange'] = "日期范围";
$Lang['StaffAttendance']['DaysAbsent'] = "缺席日数";
$Lang['StaffAttendance']['StaffAbsenceReport'] = "教职员缺席统计表";
$Lang['StaffAttendance']['ReportCreationTime'] = "报表建立时间";
$Lang['StaffAttendance']['IncludeWaivedRecords'] = "包括豁免纪录";
$Lang['StaffAttendance']['Template'] = "预设";
$Lang['StaffAttendance']['AddTemplate'] = "新增预设";
$Lang['StaffAttendance']['DateApplied'] = "已套用到(日)";
$Lang['StaffAttendance']['GroupInvolved'] = "受影响".$Lang['StaffAttendance']['Group'];
$Lang['StaffAttendance']['IndividualInvolved'] = "受影响".$Lang['StaffAttendance']['Individual'];
$Lang['StaffAttendance']['RenameTemplate'] = "重新命名";
$Lang['StaffAttendance']['ManageDateApplied'] = "管理已套用日期";
$Lang['StaffAttendance']['EditGroupInvolved'] = "更改涉及者";
$Lang['StaffAttendance']['Others'] = "其他";
$Lang['StaffAttendance']['TotalDays'] = "总日数";
$Lang['StaffAttendance']['StaffLeaveAbsenceReport'] = "教职员请假缺席统计";
$Lang['StaffAttendance']['ManageInvolvedParties'] = "管理涉及者";
$Lang['StaffAttendance']['ManageInvolvedGroup'] = "设定受影响".$Lang['StaffAttendance']['Group'];
$Lang['StaffAttendance']['ManageInvolvedIndividual'] = "设定受影响".$Lang['StaffAttendance']['Individual'];
$Lang['StaffAttendance']['SaveAndMgmtInvolvedGroup'] = $Lang['Btn']['Add']."并前往设定受影响".$Lang['StaffAttendance']['Group'];
$Lang['StaffAttendance']['SaveAndMgmtInvolvedIndividual'] = $Lang['Btn']['Add']."并前往设定受影响".$Lang['StaffAttendance']['Individual'];
$Lang['StaffAttendance']['SaveAndManageDateAssoicated'] = "储存及".$Lang['StaffAttendance']['ManageDateApplied'];
$Lang['StaffAttendance']['DuplicateTemplateNameWarning'] = "*预设名称重复或空白";
$Lang['StaffAttendance']['SaveTemplateSuccess'] = "1|=|预设储存成功";
$Lang['StaffAttendance']['SaveTemplateFail'] = "0|=|预设储存失败";
$Lang['StaffAttendance']['DeleteTemplateWarning'] = "您是否确定要移除此预设?";
$Lang['StaffAttendance']['DeleteTemplateSuccess'] = "1|=|预设移除成功";
$Lang['StaffAttendance']['DeleteTemplateFail'] = "0|=|预设移除失败";
$Lang['StaffAttendance']['CurrentSelection'] = "已套用的日期";
$Lang['StaffAttendance']['TargetDateMustBeAfterTodayWarning'] = "*套用日期必须为今天以后";
$Lang['StaffAttendance']['TargetDateUsedWarning'] = "*此日期已套用于此或其他预设";
$Lang['StaffAttendance']['DateAppliedSuccess'] = "1|=|预设已套用于此日期";
$Lang['StaffAttendance']['DateAppliedFail'] = "0|=|预设已套用失败";
$Lang['StaffAttendance']['DateAppliedWarning'] = "如使用此预设，所有受影响单位的现存职务设定将被覆写。\\n你是否确定要继续?";
$Lang['StaffAttendance']['DateAppliedDeleteWarning'] = "所有涉及者于所选日期内的非全日请假纪录将会移除. \\n你是否确定要继续?";
$Lang['StaffAttendance']['DeleteTemplateTargetDateSuccess'] = "1|=|日期已从预设中移除";
$Lang['StaffAttendance']['DeleteTemplateTargetDateFail'] = "0|=|日期从预设中移除失败";
$Lang['StaffAttendance']['TemplateTargetSlotConflictWarning'] = "系统发现以下涉及者在同一预设中已设定的当值时段发生冲突:";
$Lang['StaffAttendance']['MaxSlotPerSettingWarning1'] = "最多只可套用 ";
$Lang['StaffAttendance']['MaxSlotPerSettingWarning2'] = " 个".$Lang['StaffAttendance']['TimeSlot'];
$Lang['StaffAttendance']['EditDutyRemoveSlotWarning'] = "您是否确定要移除此".$Lang['StaffAttendance']['TimeSlot']."?";
$Lang['StaffAttendance']['Staff'] = "职员";
$Lang['StaffAttendance']['SlotSpecialCharacterCheckWarning'] = '当值时段代号不可含有以下符号: <br>^ @ " \\\\ \\\' : < > , . / ? [ ] ~ ! # $ * ( ) { } | = + -';
$Lang['StaffAttendance']['OTRecordReport'] = '超时工作报告';
$Lang['StaffAttendance']['SpecialRoster'] = '小组/个人特别职务';
$Lang['StaffAttendance']['And'] = '及';
$Lang['StaffAttendance']['AddEditLeaveRecord'] = $Lang['Btn']['Add'].'/'.$Lang['Btn']['Edit'].'请假纪录';
$Lang['StaffAttendance']['ShowReportOptions'] = '显示报告选项';
$Lang['StaffAttendance']['ReportOptions'] = '报告选项';
$Lang['StaffAttendance']['OTRecordsFull'] = "超时工作纪录";
$Lang['StaffAttendance']['AddAndFinish'] = "储存并完成";
$Lang['StaffAttendance']['AddAndProceed'] = "储存并前往下一步";
$Lang['StaffAttendance']['ManageDutyPeriod'] = "管理职务时段";
$Lang['StaffAttendance']['ManageTimeSlot'] = "管理当值时段";
$Lang['StaffAttendance']['ViewDutyPeriod'] = "检视职务时段";
$Lang['StaffAttendance']['ViewTimeSlot'] = "检视当值时段";
$Lang['StaffAttendance']['ViewMember'] = "检视成员";
$Lang['StaffAttendance']['ApplyTimeSlot'] = "设定所选当值时段的职务安排";
$Lang['StaffAttendance']['DragAndDropDutyIntruction'] = "说明: <br>
																												 1. 点击职员名称.<br>
																												 2. 将已选取职员拖曳并放置于目标当值时段.";
$Lang['StaffAttendance']['Count'] = '计算';
$Lang['StaffAttendance']['NotCount'] = '不计算';
$Lang['StaffAttendance']['CurrentMember'] = "现时组员";
$Lang['StaffAttendance']['WarningInputNonNegativeNumber'] = "请输入一个非负数";
$Lang['StaffAttendance']['AllIndividuals'] = "所有个人职员";
$Lang['StaffAttendance']['NativeFormat'] = "内置格式";
$Lang['StaffAttendance']['TerminalFormat'] = "终端机格式";
$Lang['StaffAttendance']['NoCardTappingIn'] = "进入时无需拍卡";
$Lang['StaffAttendance']['NoCardTappingOut'] = "离开时无需拍卡";
$Lang['StaffAttendance']['RealTimeStaffStatus'] = "实时职员状况";
$Lang['StaffAttendance']['LastSlotName'] = "最后".$Lang['StaffAttendance']['TimeSlot'];
$Lang['StaffAttendance']['InSchool'] = "在校";
$Lang['StaffAttendance']['NotInSchool'] = "不在校";
$Lang['StaffAttendance']['PresetStatus'] = "预设状况";
$Lang['StaffAttendance']['RFID'] = "无线射频辨识ID";
$Lang['StaffAttendance']['SmartCard'] = "智能卡";
$Lang['StaffAttendance']['SmartCardImportType'] = "汇入类型";
# Staff Attendance V3 End

# ePOS start
$Lang['ePOS']['Transaction'] = '交易';
$Lang['ePOS']['Inventory'] = '存货';
$Lang['ePOS']['POSTransactionReport'] = 'POS 交易报告';
$Lang['ePOS']['POSItemReport'] = 'POS 项目统计';
$Lang['ePOS']['POSStudentReport'] = 'POS 学生统计';
$Lang['ePOS']['ItemSetting'] = '项目设定';
$Lang['ePOS']['HealthIngredientSetting'] = '健康指标';
$Lang['ePOS']['TerminalSetting'] = '智能咭终端机设定';
$Lang['ePOS']['CategorySetting'] = '项目类别设定';
$Lang['ePOS']['Category&ItemSetting'] = '项目类别及项目';
$Lang['ePOS']['Category'] = '项目类别';
$Lang['ePOS']['Item'] = '项目';
$Lang['ePOS']['Inventory'] = '存货数量';
$Lang['ePOS']['CurrentInventory'] = '现有存货数量';
$Lang['ePOS']['AllowClientProgramConnection'] = '允许POS客户端连接';
$Lang['ePOS']['Name'] = '名称';
$Lang['ePOS']['Code'] = '编号';
$Lang['ePOS']['Photo'] = '相片';
$Lang['ePOS']['Barcode'] = '条码';
$Lang['ePOS']['UnitPrice'] = '单价';
$Lang['ePOS']['RepresentDisabledCategory'] = '表示该项目类别已停用。';
$Lang['ePOS']['IncludeItem'] = '包括项目';
$Lang['ePOS']['AllCategories'] = '所有项目类别';
$Lang['ePOS']['PageLoadingTime'] = '载入此页时间';
$Lang['ePOS']['InventoryNotRealTimeRemarks'] = '以下显示的存货数量是以「载入此页时间」为准。如你要得到最新资料，请重新载入此页面。';
$Lang['ePOS']['IncreaseInventory'] = '增加存货数量';
$Lang['ePOS']['DecreaseInventory'] = '减少存货数量';
$Lang['ePOS']['ViewPhoto'] = '检视相片';
$Lang['ePOS']['ViewLog'] = '检视纪录';
$Lang['ePOS']['IncreaseBy'] = '增加存货数量';
$Lang['ePOS']['DecreaseBy'] = '减少存货数量';
$Lang['ePOS']['InventoryUpdateLog'] = '存货数量更新纪录';
$Lang['ePOS']['UpdateTime'] = '更新时间';
$Lang['ePOS']['UpdatedBy'] = '更新用户';
$Lang['ePOS']['InventoryAfter'] = '更新后存货数量';
$Lang['ePOS']['CategoryPhotoDimensionRemarks'] = '项目类别相片不能大于 95 x 95。';
$Lang['ePOS']['ItemPhotoDimensionRemarks'] = '项目相片不能大于 180 x 120 (宽 x 高)。';
$Lang['ePOS']['NoOfItems'] = '项目数量';

$Lang['ePOS']['Btn']['BackToCategorySettings'] = '返回项目类别设定';

$Lang['ePOS']['Add']['Category'] = '新增项目类别';
$Lang['ePOS']['Add']['Item'] = '新增项目';
$Lang['ePOS']['Add']['HealthIngredient'] = "新增健康食材";

$Lang['ePOS']['Edit']['Category'] = '编辑项目类别';
$Lang['ePOS']['Edit']['Item'] = '编辑项目';
$Lang['ePOS']['Edit']['HealthIngredient'] = "编辑健康食材";

$Lang['ePOS']['Move']['Category'] = '移动项目类别';
$Lang['ePOS']['Move']['Item'] = '移动项目';
$Lang['ePOS']['Move']['HealthIngredient'] = "移动健康食材";

$Lang['ePOS']['Manage']['HealthIngredient'] = '管理项目健康指标';

$Lang['ePOS']['WarningArr']['Blank']['Code'] = '请输入编号。';
$Lang['ePOS']['WarningArr']['Blank']['Name'] = '请输入名称。';
$Lang['ePOS']['WarningArr']['Blank']['UnitPrice'] = '请输入单价。';
$Lang['ePOS']['WarningArr']['Blank']['Barcode'] = '请输入条码。';
$Lang['ePOS']['WarningArr']['Blank']['Adjustment'] = '请输入调节数字';
$Lang['ePOS']['WarningArr']['Blank']['HealthIngredientCode'] = "请输入代码"; 
$Lang['ePOS']['WarningArr']['Blank']['HealthIngredientName'] = "请输入名称";
$Lang['ePOS']['WarningArr']['Blank']['UnitName'] = "请输入单位"; 
$Lang['ePOS']['WarningArr']['Blank']['StandardIntakePerDay'] = "请输入每日标准摄取量";

$Lang['ePOS']['WarningArr']['Duplicated']['Code'] = '代号重复。';
$Lang['ePOS']['WarningArr']['Duplicated']['Name'] = '名称重复。';

$Lang['ePOS']['WarningArr']['NotFloat']['UnitPrice'] = '单价应为数字。';
$Lang['ePOS']['WarningArr']['NotFloat']['StandardIntakePerDay'] = "每日标准摄取量应为数字"; 
$Lang['ePOS']['WarningArr']['NotFloat']['StandardIntakePerDay'] = "每日标准摄取量应为数字"; 

$Lang['ePOS']['WarningArr']['NotInteger']['Adjustment'] = '调节存货数量应为整数。';

$Lang['ePOS']['WarningArr']['GreaterThanZero']['UnitPrice'] = '单价必须大于零。';
$Lang['ePOS']['WarningArr']['GreaterThanZero']['InventoryAdjustment'] = '调节存货数量必须大于零。';

$Lang['ePOS']['WarningArr']['InvalidFile'] = '档案不正确。';
$Lang['ePOS']['WarningArr']['Barcode']['LengthTooLong'] = '条码数不能多于一百字。';
$Lang['ePOS']['WarningArr']['Barcode']['FormatInvalid'] = '条码格式错误。';
$Lang['ePOS']['WarningArr']['NoteEnoughtInventory'] = '没有足够存货。';
$Lang['ePOS']['WarningArr']['Unique']['HealthIngredientCode'] = "代码已被使用"; 
$Lang['ePOS']['WarningArr']['Unique']['HealthIngredientName'] = "名称已被使用";
$Lang['ePOS']['WarningArr']['SelectedPhotoTooLarge'] = '已选择的相片过大。';

$Lang['ePOS']['jsWarningArr']['ChangeClientConnectionSettings'] = '因系统现在允许POS客户端连接，所以系统未能更新项目类别资料。你是否要更新设定为拒绝POS客户端连接？';
$Lang['ePOS']['jsWarningArr']['DeletePhoto'] = '你是否确定要删除相片？';
$Lang['ePOS']['jsWarningArr']['Max10EnableCategory'] = '因项目类别之启用上限为十个类别，所以未能启用此类别。';
$Lang['ePOS']['jsWarningArr']['StartDateCannotGreaterThanEndDate'] = "开始日期必需为结束日期之前。";
$Lang['ePOS']['jsWarningArr']['DeleteHealthIngredient'] = '你是否确定要删除营养指标?';

$Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateSuccess'] = "1|=|智能咭终端机设定成功。";
$Lang['ePOS']['ReturnMessage']['TerminalSettingsUpdateFailed'] = "0|=|智能咭终端机设定失败。";

$Lang['ePOS']['ReturnMessage']['Add']['Success']['Category'] = "1|=|项目类别新增成功。";
$Lang['ePOS']['ReturnMessage']['Add']['Success']['Item'] = "1|=|项目新增成功。";
$Lang['ePOS']['ReturnMessage']['Add']['Failed']['Category'] = "0|=|项目类别新增失败。";
$Lang['ePOS']['ReturnMessage']['Add']['Failed']['Item'] = "0|=|项目新增失败。";
$Lang['ePOS']['ReturnMessage']['Edit']['Success']['Category'] = "1|=|项目类别编辑成功。";
$Lang['ePOS']['ReturnMessage']['Edit']['Success']['Item'] = "1|=|项目编辑成功。";
$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['Category'] = "0|=|项目类别编辑失败。";
$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['Item'] = "0|=|项目编辑失败。";
$Lang['ePOS']['ReturnMessage']['Update']['Success']['Inventory'] = "1|=|存货更新成功。";
$Lang['ePOS']['ReturnMessage']['Update']['Failed']['Inventory'] = "0|=|存货更新失败。";
$Lang['ePOS']['ReturnMessage']['Update']['Failed']['NotEnoughInventory'] = "0|=|没有足够存货。";
$Lang['ePOS']['ReturnMessage']['Add']['Success']['HealthIngredient'] = "1|=|加入健康食材成功";
$Lang['ePOS']['ReturnMessage']['Add']['Failed']['HealthIngredient'] = "0|=|加入健康食材失败";
$Lang['ePOS']['ReturnMessage']['Edit']['Success']['HealthIngredient'] = "1|=|修改健康食材成功";
$Lang['ePOS']['ReturnMessage']['Edit']['Failed']['HealthIngredient'] = "0|=|修改健康食材成功";
$Lang['ePOS']['ReturnMessage']['Delete']['Success']['HealthIngredient'] = "1|=|删除营养指标成功";
$Lang['ePOS']['ReturnMessage']['Delete']['Failed']['HealthIngredient'] = "0|=|删除营养指标失败";

$Lang['ePOS']['SelectDateRange'] = "选择时间范围";
$Lang['ePOS']['From'] = "由";
$Lang['ePOS']['To'] = "至";
$Lang['ePOS']['ProcessBy'] = "处理者";
$Lang['ePOS']['VoidBy'] = "取消者";
$Lang['ePOS']['ProcessDate'] = "处理日期";
$Lang['ePOS']['VoidDate'] = "取消日期";
$Lang['ePOS']['UserName'] = "名称";
$Lang['ePOS']['ClassName'] = "班别";
$Lang['ePOS']['ClassNumber'] = "学号";
$Lang['ePOS']['RefCode'] = "参考编号";
$Lang['ePOS']['Remark'] = "备注";
$Lang['ePOS']['Void'] = "取消此交易";
$Lang['ePOS']['ReportVoidTitle'] = "取消POS 交易";
$Lang['ePOS']['VoidTransactionSuccess'] = "1|=|交易取消成功";
$Lang['ePOS']['VoidTransactionFail'] = "0|=|交易取消失败";
$Lang['ePOS']['VoidTransactionConfirmMsg'] = "此动作不能回复。 \\n您是否确定要取消此交易?";


$Lang['ePOS']['UnitName'] = '单位';
$Lang['ePOS']['StandardIntakePerDay'] = "每日标准摄取量";   
$Lang['ePOS']['HealthIngredient']['InUse'] = "使用";
$Lang['ePOS']['HealthIngredient']['NotInUse'] = "不使用";
$Lang['ePOS']['TransactionTime'] = "交易时间";
$Lang['ePOS']['ReportGenerationTime'] = "报表建立时间";
$Lang['ePOS']['StudentRemoved'] = "<font color=red>*</font> - 表示该用户已被移除.";
$Lang['ePOS']['DisplayFormat'] = "显示方式";
$Lang['ePOS']['PrinterFriendlyPage'] = "可列印格式";
$Lang['ePOS']['CSV'] = 'CSV';
$Lang['ePOS']['POSSalesReport'] = "POS 销售统计";
$Lang['ePOS']['PrintTransactionDetail'] = "列印交易明细";
$Lang['ePOS']['ItemIntake'] = "项目摄取量";
$Lang['ePOS']['Unit'] = "单位";

$Lang['ePOS']['InventoryReturnSetting'] = "取消时回拨存货选项";
$Lang['ePOS']['InventoryReturn'] = "回拨存货数量";
$Lang['ePOS']['InventoryReturnRemark'] = "将会回拨为存货的数量，如交易项目与回拨总数不符，差额将被视为报废。";
$Lang['ePOS']['GeneralTerminalSetting'] = "一般设定";
$Lang['ePOS']['TerminalManagementSetting'] = "终端机管理";
$Lang['ePOS']['TerminalIndependentCategorySetting'] = "终端机独立设定".$Lang['ePOS']['Category'];
$Lang['ePOS']['SiteName'] = '位置';
$Lang['ePOS']['IP'] = '最后连接网络位址';
$Lang['ePOS']['LastConnectTime'] = '最后连接时间';
$Lang['ePOS']['LastPhotoSyncTime'] = '最后同步相片时间';
$Lang['ePOS']['MacAddress'] = '实体网络位址';
$Lang['ePOS']['AddSite'] = 'Add '.$Lang['ePOS']['SiteName'];
$Lang['ePOS']['SiteNameEmptyDuplicateWarning'] = '*站台名称重复或是空白';
$Lang['ePOS']['TerminalSaveSuccess'] = '1|=|终端机储存成功 ';
$Lang['ePOS']['TerminalSaveFail'] = '0|=|终端机储存失败 ';
$Lang['ePOS']['ClearClientTerminalLinkage'] = '解除终端机连接状态';
$Lang['ePOS']['ClearClientTerminalLinkageConfirmWarning'] = '您是否确定要解除此终端机连接状态? \\n(请确定终端机已停止连接, 否则它会自动重新连接)';
$Lang['ePOS']['SetTerminalCategorySetting'] = '设定项目类别关联';
$Lang['ePOS']['Rename'] = '重新名命';
$Lang['ePOS']['RemoveTerminalSetting'] = '所有项目类别关联将会移除。\\n 您是否确定要继续?';
$Lang['ePOS']['TerminalDeleteSuccess'] = '1|=|终端机移除成功 ';
$Lang['ePOS']['TerminalDeleteFail'] = '0|=|终端机移除失败 ';
$Lang['ePOS']['StationName'] = '站台名称';
$Lang['ePOS']['CategoryAssociated'] = '关联的项目类别';
$Lang['ePOS']['TerminalUnassigned'] = '未分类';
$Lang['ePOS']['AssignToAnotherSite'] = '指派到其他'.$Lang['ePOS']['SiteName'];
$Lang['ePOS']['TemplateSaveSuccess'] = '1|=|'.$Lang['ePOS']['SiteName'].'储存成功 ';
$Lang['ePOS']['TemplateSaveFail'] = '0|=|'.$Lang['ePOS']['SiteName'].'储存失败 ';
$Lang['ePOS']['SyncPhotoForThisTerminal'] = '下次连接时同步化相片';
$Lang['ePOS']['LastPhotoRequestBy'] = '最后同步相片要求者';
$Lang['ePOS']['LastPhotoRequestTime'] = '最后同步相片日期';
$Lang['ePOS']['RemoveSite'] = '移除'.$Lang['ePOS']['SiteName'];
$Lang['ePOS']['RemoveSiteWarning'] = '所有相关联的站台将会重新指派到"'.$Lang['ePOS']['TerminalUnassigned'].'". \\n您是否确定要继续?';
$Lang['ePOS']['TemplateDeleteSuccess'] = '1|=|'.$Lang['ePOS']['SiteName'].'移除成功 ';
$Lang['ePOS']['TemplateDeleteFail'] = '0|=|'.$Lang['ePOS']['SiteName'].'移除失败 ';
$Lang['ePOS']['DeleteTerminal'] = '移除站台';
$Lang['ePOS']['SyncPhotoForAllTerminal'] = '所有站台同步相片';
$Lang['ePOS']['TranscationTime'] = '交易时间';
$Lang['ePOS']['ConfirmSyncAllPhoto'] = '您是否确定需要同步所以站台的相片?';
$Lang['ePOS']['TerminalIPList'] = "终端机的 IP 位址";
$Lang['ePOS']['TerminalIPInput'] = "请把终端机的 IP 地址每行一个输入";
$Lang['ePOS']['TerminalYourAddress'] = "你现时的 IP 地址";
$Lang['ePOS']['LogType'] = "纪录类别";
$Lang['ePOS']['TransactionMaking'] = "交易卖出";
$Lang['ePOS']['TransactionVoid'] = "交易退回";
$Lang['ePOS']['InventoryAdd'] = "增加存货";
$Lang['ePOS']['InventoryAdd'] = "减小存货";
$Lang['ePOS']['IncludeTransactionDetail'] = "包括交易相关纪录";
# ePOS end

# subject Group Mapping
$i_subjectGroupMapping_showStat = "展示统计数字";
$i_subjectGroupMapping_hideStat = "隐藏统计数字";

# text to speech
$Lang['TextToSpeech']['Error'] = '0|=|开启MP3失败.';
$Lang['TextToSpeech']['SpeechSetting'] = '语音设定';
$Lang['TextToSpeech']['FullText'] = '全文发声';
$Lang['TextToSpeech']['FullPage'] = '全文发声';
$Lang['TextToSpeech']['Highlighted'] = 'Highlight发声';
$Lang['TextToSpeech']['HighlightedText'] = 'Highlight发声';
$Lang['TextToSpeech']['Voice'] = '声音';
$Lang['TextToSpeech']['Male'] = '男';
$Lang['TextToSpeech']['Female'] = '女';
$Lang['TextToSpeech']['Speed'] = '速度';
$Lang['TextToSpeech']['Slowest'] = '最慢';
$Lang['TextToSpeech']['Slow'] = '慢';
$Lang['TextToSpeech']['Normal'] = '正常';
$Lang['TextToSpeech']['NoHighlightError'] = '请选择文字';
$Lang['TextToSpeech']['NoChineseMaleVoice'] = '中文字不支援男声发音，将会转用女声。';

$i_transfer_file_ownership = '移交文档拥有权';

### eSports
$Lang['eSports']['Arrangement_Schedule_Setting'] = "赛程自动安排设定";
$Lang['eSports']['HiddenAutoArrangeFunction'] = "隐藏「". $i_Sports_menu_Arrangement_Schedule ."」功能";
$Lang['eSports']['RefereeRemark'] = "裁判评语";
$i_con_msg_New_Record_Delete = "<font color=green>新纪录已删除</font>";

### eHomework
$Lang['SysMgr']['Homework']['DefaltHandinRequired'] = "预设功课是必须缴交";
$Lang['SysMgr']['Homework']['ClearHomework'] = "清除功课纪录";
$i_con_msg_homework_clear = "<font color=green>功课纪录已被删除</font>";
$Lang['SysMgr']['Homework']['SelectRecordsCleared'] = "选择要删除的纪录";
$Lang['SysMgr']['Homework']['AllTeachersSubjectLeaders'] = "所有老师及科长";
$Lang['SysMgr']['Homework']['AllSubjectLeaders'] = "所有科长";
$Lang['SysMgr']['Homework']['TeacherSubjectLeader'] = "老师/科长";

### Admin Console
$Lang['SysMgr']['NetWorkContact'] = "此页面可连结其他系统(如防火墙, 电脑病毒扫瞄)之管理版面. <br> 详情可与博文教育有限公司联络.";

### Group
$Lang['Group']['DisplayInCommunity'] = "在社群管理工具显示";

### General
$Lang['Btn']['Export2'] = '汇出(二)';

# 天主教慈幼会伍少梅中学 cust report (Award & Punishment Monthly report)
$Lang['Btn']['Export_without_ap'] = '汇出 (不包括功过栏)';


# imail gamma
$Lang['Gamma']['Sender'] = '寄件人';
$Lang['Gamma']['To']= '到';
$Lang['Gamma']['From']= '由';
$Lang['Gamma']['cc']= 'Cc';
$Lang['Gamma']['bcc']= 'Bcc';
$Lang['Gamma']['Attachment']= '附件';
$Lang['Gamma']['AddAttachment']= '加入附件';
$Lang['Gamma']['RemoveAttachment']= '移除附件';
$Lang['Gamma']['Date'] = '日期';

$Lang['Gamma']['Subject']= '标题';
$Lang['Gamma']['AddBcc'] = '加入Bcc';
$Lang['Gamma']['AddBcc'] = '加入密件副本';
$Lang['Gamma']['SeparateAddrTips'] = '(请以 ; 或 , 分隔每个电邮地址)';
$Lang['Gamma']['Message']= '内容';
$Lang['Gamma']['iMailGamma']= 'iMail Gamma';

$Lang['Warning']['EmptyToFieldWarning'] = "请输入收件人电邮地址";
$Lang['Warning']['EmailAttachmentWarning'] = "你现在未加入任何附件 \\n继续发送电邮?";
$Lang['Warning']['ToMailFormatWarning'] = '请检查 "到" 的电邮格式';
$Lang['Warning']['CcMailFormatWarning'] = '请检查 "Cc" 的电邮格式';
$Lang['Warning']['BccMailFormatWarning'] = '请检查 "Bcc" 的电邮格式';
$Lang['email']['WarningLimitAttachmentSize'] = "附件大小超过 8MB, 请将附件移除。";
$Lang['Warning']['MailFormatWarning'] = "到/Cc/Bcc 的电邮格式错误\\n";

$Lang['Gamma']['AdvancedSearch'] = "进阶搜寻"; 
$Lang['Gamma']['Sent'] = "寄件箱";
$Lang['Gamma']['Junk'] = "杂件箱";

$Lang['Gamma']['reportSpam'] = "ReportSpam";
$Lang['Gamma']['reportNonSpam'] = "ReportNonSpam";
$Lang['Gamma']['UnseenStatus']['ALL'] = "所有邮件";
$Lang['Gamma']['UnseenStatus']['UNSEEN'] = "未读邮件";
$Lang['Gamma']['UnseenStatus']['SEEN'] = "已读邮件";
$Lang['Gamma']['UnseenStatus']['FLAGGED'] = "己标记星号";

$Lang['Gamma']['reportSpam'] = "报告垃圾邮件";
$Lang['Gamma']['reportNonSpam'] = "报告非垃圾邮件";

$Lang['Gamma']['ConfirmMsg']['reportSpam'] = "将已选取的邮件报告为垃圾邮件并移至杂件箱?";
$Lang['Gamma']['ConfirmMsg']['reportNonSpam'] = "将已选取的邮件报告为非垃圾邮件并移至收件箱?";
$Lang['Gamma']['ConfirmMsg']['MarkAsSeen'] = "将已选取的邮件设定为已读邮件?";
$Lang['Gamma']['ConfirmMsg']['MarkAsUnseen'] = "将已选取的邮件设定为未读邮件?";
$Lang['Gamma']['ConfirmMsg']['MarkAsStarred'] = "将已选取的邮件标记为星号?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['reportSpam'] = "将邮件报告为垃圾邮件并移至杂件箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['reportNonSpam'] = "将邮件报告为非垃圾邮件并移至收件箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['removeMail'] = "将邮件移至垃圾箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['moveMail'] = "你确定要移动邮件?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['emptyTrash'] = "你确定要清空垃圾箱?";
$Lang['Gamma']['ConfirmMsg']['ViewMail']['emptyJunk'] = "你确定要清空杂件箱?";

$Lang['Gamma']['FillForwardingEmail'] = " 请输入要转寄到的电邮地址";

$Lang['Gamma']['Remove'] = "删除";
$Lang['Gamma']['Forward']= "转寄";
$Lang['Gamma']['Reply']= "回复";
$Lang['Gamma']['ReplyAll']= "回复全部";

$Lang['Gamma']['MarkAs'] = "标记为";

$Lang['ReturnMsg']['MailSavedSuccess'] = "1|=|邮件储存成功。";
$Lang['ReturnMsg']['MailSavedUnsuccess'] = "0|=|邮件储存失败。";

$Lang['Gamma']['SystemFolderName']["INBOX"] = "收件箱";
$Lang['Gamma']['SystemFolderName']["Sent"] = "寄件箱";
$Lang['Gamma']['SystemFolderName']["Drafts"] = "草稿箱";
$Lang['Gamma']['SystemFolderName']["Junk"] = "杂件箱";
$Lang['Gamma']['SystemFolderName']["Trash"] = "垃圾箱";
 
$Lang['Gamma']['AutoSaveInterval'] = "自动储存草稿时间";
$Lang['Gamma']['Min'] = "分钟";
$Lang['Gamma']['ZeroToDisable'] = "(输入 0 关闭自动储存)";
$Lang['Gamma']['ReturnMsg']['UserPref']['Save']['Success'] = "1|=|用户设定成功";
$Lang['Gamma']['ReturnMsg']['UserPref']['Save']['Failed'] = "0|=|用户设定失败";
$Lang['Gamma']['SelectFromInternalRecipient'] = "从内联网收件人选取";
$Lang['Gamma']['SelectFromExternalRecipient'] = "从外在收件人选取";
$Lang['Gamma']['SelectFromExternalRecipientGroup'] = "从收件组别选取";

$Lang['Gamma']['FolderNameContainDots'] = "邮件夹名称不能含有\".\"";
$Lang['Gamma']['ViewAllImage'] = "检视所有图片";
$Lang['Gamma']['DownloadAllAttachment'] = "下载所有附件";

$Lang['Gamma']['Quota']= "配额";
$Lang['Gamma']['EmailStatus']= "电邮状况";

$Lang['Gamma']['DisplayRelatedMail']= "显示相关邮件";

$Lang['Gamma']['InternalGroupLabel'] = "(校园组别)";
$Lang['Gamma']['ExternalGroupLabel'] = "(外在组别)";
$Lang['Gamma']['DisplayName'] = "显示名称";
$Lang['Gamma']['ReplyEmail'] = "回复电邮地址";

$Lang['Gamma']['iMailArchive'] = "iMail 封存";
$Lang['Gamma']['Recipient'] = "收件者";
$Lang['Gamma']['EmptyTrash'] = "清空垃圾箱";
$Lang['Gamma']['EmptyJunk'] = "清空杂件箱";
$Lang['Gamma']['ReturnMsg']['EmptyTrashSuccess'] = "成功清空垃圾箱。";
$Lang['Gamma']['ReturnMsg']['EmptyTrashUnsuccess'] = "清空垃圾箱失败。";
$Lang['Gamma']['ReturnMsg']['EmptyJunkSuccess'] = "成功清空杂件箱。";
$Lang['Gamma']['ReturnMsg']['EmptyJunkUnsuccess'] = "清空杂件箱失败。";
$Lang['Gamma']['ExpandAllMails'] = "显示所有邮件";
$Lang['Gamma']['CollapseAllMails'] = "隐藏所有邮件";

$Lang['Gamma']['DisallowSendReceive'] = "这些用户不能发送或接收互联网邮件.";
$Lang['Gamma']['DayInTrash'] = "垃圾箱邮件保留";
$Lang['Gamma']['DayInSpam'] = "杂件箱邮件保留";
$Lang['Gamma']['Days'] = "天";
$Lang['Gamma']['MailBoxQuota'] = "邮箱储存量设定";
$Lang['Gamma']['BlockUserToSend'] = "禁止用户发送邮件";
$Lang['Gamma']['GeneralSetting'] = "基本设定";
$Lang['Gamma']['Warning']['PleaseSelectUser'] = "请选择使用者";
$Lang['Gamma']['Warning']['PleaseInputQuota'] = "请输入储存量调整数";
$Lang['Gamma']['Warning']['WrongQuota'] = "储存量调整数不正确";
$Lang['Gamma']['InternalMailOnly'] = "只限内联网电邮";
$Lang['Gamma']['InternalAndInternet'] = "互联网及内联网电邮";

$Lang['Gamma']['UserEmail'] = "备用电邮";
$Lang['Gamma']['Warning']['SendMessageWithoutSubject'] = "要传送没有主旨的邮件吗?";
## eNotice / eCircular
$Lang['Notice']['AnswerAllQuestions'] = "必须回答此通告的所有问题";
####################### This should be placed at the bottom
# if setting of merit/demerit title are set
# load the setting
$merit_demerit_customize_file = "$intranet_root/file/merit.b5.customized.txt";
if(file_exists($merit_demerit_customize_file) && is_file($merit_demerit_customize_file) && filesize($merit_demerit_customize_file)!=0){
	$file_content_merit_demeirt_wordings = get_file_content($merit_demerit_customize_file);
	$lines = explode("\n",$file_content_merit_demeirt_wordings);

	$i_Merit_Merit = $lines[0];
	$i_Merit_MinorCredit = $lines[1];
	$i_Merit_MajorCredit = $lines[2];
	$i_Merit_SuperCredit = $lines[3];
	$i_Merit_UltraCredit = $lines[4];

	$i_Merit_BlackMark = $lines[5];
	$i_Merit_MinorDemerit = $lines[6];
	$i_Merit_MajorDemerit = $lines[7];
	$i_Merit_SuperDemerit = $lines[8];
	$i_Merit_UltraDemerit = $lines[9];
}

$i_Merit_TypeArray = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

$i_Merit_TypeArrayWithWarning = array(
$i_Merit_Merit,
$i_Merit_MinorCredit,
$i_Merit_MajorCredit,
$i_Merit_SuperCredit,
$i_Merit_UltraCredit,
$i_Merit_Warning,
$i_Merit_BlackMark,
$i_Merit_MinorDemerit,
$i_Merit_MajorDemerit,
$i_Merit_SuperDemerit,
$i_Merit_UltraDemerit
);

# Time slot setting
$Lang['eAttendance']['warn_PleaseChooseAtLeastOneItem'] = "请最少选择一个项目";

$Lang['eAttendance']['ClassAttendanceMode'] = "班别点名模式";
$Lang['eAttendance']['ShowAllClasses'] = "所有班级";

$Lang['eAttendance']['PunishmentNullAlertMsg'] = "部份纪录未有任何惩罚数量, 继续?";
$Lang['eAttendance']['DisplayPastPunishmentRecord'] = "显示过去惩罚纪录";

# admin console
$i_general_Principal = "校长";
$i_general_VPrincipal = "副校长";
$i_general_Director = "主任";

# Repair System
$Lang['Header']['Menu']['RepairSystem'] = "报修系统";
$Lang['Menu']['RepairSystem']['Management'] = "管理";
$Lang['Menu']['RepairSystem']['Settings'] = "设定";
$Lang['RepairSystem']['List'] = "清单";
$Lang['RepairSystem']['Category'] = "类别";
$Lang['RepairSystem']['Location'] = "地点";
$Lang['RepairSystem']['MgmtGroup'] = "管理单位";
$Lang['RepairSystem']['LocationList'] = "地点列表";
$Lang['RepairSystem']['LocationNew'] = "新增地点";
$Lang['RepairSystem']['LocationEdit'] = "更新地点";
$Lang['RepairSystem']['LocationName'] = "地点名称";
$Lang['RepairSystem']['CategoryList'] = "类别列表";
$Lang['RepairSystem']['CategoryNew'] = "新增类别";
$Lang['RepairSystem']['CategoryEdit'] = "更新类别";
$Lang['RepairSystem']['CategoryName'] = "类别名称";
$Lang['RepairSystem']['DeleteGroupError'] = "单位已分配到某些类别中，不能删除。";
$Lang['RepairSystem']['NewRequest'] = "新增报修申请";
$Lang['RepairSystem']['ViewRequest'] = "检视报修申请";
$Lang['RepairSystem']['CancelRequest'] = "取消报修申请";
$Lang['RepairSystem']['CancelRequestMsg'] = "你是否确定要取消报修申请?";
$Lang['RepairSystem']['LocationDetail'] = "地点详情";
$Lang['RepairSystem']['RequestInformation'] = "报修详情";
$Lang['RepairSystem']['FollowupInformation'] = "跟进详情";
$Lang['RepairSystem']['RemarkByAdmin'] = "管理员备注";
$Lang['RepairSystem']['on'] = "于";
$Lang['RepairSystem']['LastModify'] = "最后更新";
$Lang['RepairSystem']['EditViewRequest'] = "检视/更新请求";
$Lang['RepairSystem']['AddRemark'] = "加入新备注";
$Lang['RepairSystem']['MgmtGroupMember'] = "管理单位成员";
$Lang['RepairSystem']['SelectMembers'] = "选择成员";
$Lang['RepairSystem']['RemarkHistory'] = "备注纪录";
$Lang['RepairSystem']['ImportDataCol'][0] = "老师代号";
$Lang['RepairSystem']['ImportDataCol'][1] = "<font color=red>*#</font> 纪录日期";
$Lang['RepairSystem']['ImportDataCol'][2] = "<font color=red>*</font> 类别";
$Lang['RepairSystem']['ImportDataCol'][3] = "<font color=red>*</font> 地点";
$Lang['RepairSystem']['ImportDataCol'][4] = "地点详情";
$Lang['RepairSystem']['ImportDataCol'][5] = "报修择要";
$Lang['RepairSystem']['ImportDataCol'][6] = "报修详情";
$Lang['RepairSystem']['ImportDataCol'][7] = "备注";
$Lang['RepairSystem']['ImportDataCol'][8] = "状况代号";
$Lang['RepairSystem']['LocationID'] = "地点代号";
$Lang['RepairSystem']['CategoryID'] = "类别代号";
$Lang['RepairSystem']['UserLoginIncorrect'] = "老师代号不正确";
$Lang['RepairSystem']['RecordDateMissing'] = "没有输入纪录日期";
$Lang['RepairSystem']['RecordDateNotADate'] = "纪录日期格式不正确";
$Lang['RepairSystem']['CategoryMissing'] = "没有输入类别";
$Lang['RepairSystem']['IncorrectCategory'] = "类别不正确";
$Lang['RepairSystem']['LocationMissing'] = "没有输入地点";
$Lang['RepairSystem']['LocationIncorrect'] = "地点代号不正确";
$Lang['RepairSystem']['TitleMissing'] = "没有输入题目";
$Lang['RepairSystem']['ContentMissing'] = "没有输入内容";
$Lang['RepairSystem']['StatusCodeMissing'] = "没有输入状况代号";
$Lang['RepairSystem']['StatusCodeIncorrect'] = "状况代号不正确";
$Lang['RepairSystem']['DateInTheFuture'] = "纪录日期不能输入将来的日期";
$Lang['RepairSystem']['AllRequests'] = "所有报修申请";
$Lang['RepairSystem']['AllYears'] = "所有年度";
$Lang['RepairSystem']['AllMonths'] = "所有月份";
$Lang['RepairSystem']['Request'] = "报修申请";
$Lang['RepairSystem']['RequestDetails'] = "报修详情";
$Lang['RepairSystem']['RequestSummary'] = "报修择要";
$Lang['RepairSystem']['Reporter'] = "填报人";
$Lang['RepairSystem']['Completed'] = "已完成";
$Lang['RepairSystem']['Name'] = "名称";
$Lang['RepairSystem']['ResponsibleGroup'] = "负责单位";
$Lang['RepairSystem']['Member'] = "单位成员";
$Lang['RepairSystem']['CompleteDate'] = "完成日期";
$Lang['RepairSystem']['RequestDate'] = "请求日期";
$Lang['RepairSystem']['AllCategory'] = "全部类别";
$Lang['RepairSystem']['AllLocation'] = "全部地点";
$Lang['RepairSystem']['Processing'] = $i_Discipline_System_Discipline_Case_Record_Processing;

# Country array
$Lang['Country'] = 
array(
"Afghanistan",
"Albania",
"Algeria",
"American Samoa",
"Andorra",
"Angola",
"Anguilla",
"Antarctica",
"Antigua and Barbuda",
"Argentina",
"Armenia",
"Aruba",
"Australia",
"Austria",
"Azerbaijan",
"Bahamas",
"Bahrain",
"Bangladesh",
"Barbados",
"Belarus",
"Belgium",
"Belize",
"Benin",
"Bermuda",
"Bhutan",
"Bolivia",
"Bosnia and Herzegowina",
"Botswana",
"Bouvet Island",
"Brazil",
"British Indian Ocean Territory",
"Brunei Darussalam",
"Bulgaria",
"Burkina Faso",
"Burundi",
"Cambodia",
"Cameroon",
"Canada",
"Cape Verde",
"Cayman Islands",
"Central African Republic",
"Chad",
"Chile",
"China",
"Christmas Island",
"Cocos (Keeling) Islands",
"Colombia",
"Comoros",
"Congo",
"Congo, the Democratic Republic of the",
"Cook Islands",
"Costa Rica",
"Cote d'Ivoire",
"Croatia (Hrvatska)",
"Cuba",
"Cyprus",
"Czech Republic",
"Denmark",
"Djibouti",
"Dominica",
"Dominican Republic",
"East Timor",
"Ecuador",
"Egypt",
"El Salvador",
"Equatorial Guinea",
"Eritrea",
"Estonia",
"Ethiopia",
"Falkland Islands (Malvinas)",
"Faroe Islands",
"Fiji",
"Finland",
"France",
"France, Metropolitan",
"French Guiana",
"French Polynesia",
"French Southern Territories",
"Gabon",
"Gambia",
"Georgia",
"Germany",
"Ghana",
"Gibraltar",
"Greece",
"Greenland",
"Grenada",
"Guadeloupe",
"Guam",
"Guatemala",
"Guinea",
"Guinea-Bissau",
"Guyana",
"Haiti",
"Heard and Mc Donald Islands",
"Holy See (Vatican City State)",
"Honduras",
"Hong Kong",
"Hungary",
"Iceland",
"India",
"Indonesia",
"Iran",
"Iraq",
"Ireland",
"Israel",
"Italy",
"Jamaica",
"Japan",
"Jordan",
"Kazakhstan",
"Kenya",
"Kiribati",
"Korea, Democratic People's Republic of",
"Korea, Republic of",
"Kuwait",
"Kyrgyzstan",
"Lao People's Democratic Republic",
"Latvia",
"Lebanon",
"Lesotho",
"Liberia",
"Libyan Arab Jamahiriya",
"Liechtenstein",
"Lithuania",
"Luxembourg",
"Macau",
"Macedonia, The Former Yugoslav Republic of",
"Madagascar",
"Malawi",
"Malaysia",
"Maldives",
"Mali",
"Malta",
"Marshall Islands",
"Martinique",
"Mauritania",
"Mauritius",
"Mayotte",
"Mexico",
"Micronesia, Federated States of",
"Moldova, Republic of",
"Monaco",
"Mongolia",
"Montserrat",
"Morocco",
"Mozambique",
"Myanmar",
"Namibia",
"Nauru",
"Nepal",
"Netherlands",
"Netherlands Antilles",
"New Caledonia",
"New Zealand",
"Nicaragua",
"Niger",
"Nigeria",
"Niue",
"Norfolk Island",
"Northern Mariana Islands",
"Norway",
"Oman",
"Pakistan",
"Palau",
"Panama",
"Papua New Guinea",
"Paraguay",
"Peru",
"Philippines",
"Pitcairn",
"Poland",
"Portugal",
"Puerto Rico",
"Qatar",
"Reunion",
"Romania",
"Russian Federation",
"Rwanda",
"Saint Kitts and Nevis",
"Saint LUCIA",
"Saint Vincent and the Grenadines",
"Samoa",
"San Marin",
"Sao Tome and Principe",
"Saudi Arabia",
"Senegal",
"Seychelles",
"Sierra Leone",
"Singapore",
"Slovakia (Slovak Republic)",
"Slovenia",
"Solomon Islands",
"Somalia",
"South Africa",
"South Georgia and the South Sandwich Islands",
"Spain",
"Sri Lanka",
"St. Helena",
"St. Pierre and Miquelon",
"Sudan",
"Suriname",
"Svalbard and Jan Mayen Islands",
"Swaziland",
"Sweden",
"Switzerland",
"Syrian Arab Republic",
"Taiwan, Province of China",
"Tajikistan",
"Tanzania",
"Thailand",
"Togo",
"Tokelau",
"Tonga",
"Trinidad and Tobago",
"Tunisia",
"Turkey",
"Turkmenistan",
"Turks and Caicos Islands",
"Tuvalu",
"Uganda",
"Ukraine",
"United Arab Emirates",
"United Kingdom",
"United States",
"United States Minor Outlying Islands",
"Uruguay",
"Uzbekistan",
"Vanuatu",
"Venezuela",
"Viet Nam",
"Virgin Islands (British)",
"Virgin Islands (U.S.)",
"Wallis and Futuna Islands",
"Western Sahara",
"Yemen",
"Yugoslavia",
"ZMZambia",
"Zimbabwe"
);

# eAdmin > Account Management
$Lang['Header']['Menu']['AccountMgmt'] = "用户管理";
$Lang['Header']['Menu']['StudentAccount'] = "学生用户";
$Lang['Header']['Menu']['StaffAccount'] = "教职员用户";
$Lang['Header']['Menu']['ParentAccount'] = "家长用户";
$Lang['Header']['Menu']['StudentRegistry'] = "学籍系统";
$Lang['Menu']['AccountMgmt']['Account'] = "用户";
$Lang['Menu']['AccountMgmt']['Photo'] = "相片";
$Lang['Menu']['AccountMgmt']['Management'] = "管理";
$Lang['Menu']['AccountMgmt']['Reports'] = "报告";
$Lang['Menu']['AccountMgmt']['Settings'] = "设定";
$Lang['Menu']['AccountMgmt']['StudentRegistryInfo'] = "学籍资料";
$Lang['Menu']['AccountMgmt']['AccessRight'] = "权限";
$Lang['Menu']['AccountMgmt']['Statistics'] = "统计";
$Lang['Menu']['AccountMgmt']['Parent'] = "家长统计";
$Lang['Menu']['AccountMgmt']['Student'] = "学生统计";
$Lang['AccountMgmt']['Menu_ThreeAccMgmt'] = " (家长用户, 教职员用户, 学生用户)";
$Lang['AccountMgmt']['Menu_StudentRegistry'] = " (学籍系统)";
$Lang['AccountMgmt']['UserList'] = "用户清单";
$Lang['AccountMgmt']['StudentNotInClass'] = "不在任何班的学生清单";
$Lang['AccountMgmt']['ParentWithoutChildInClass'] = "没有在学学生的家长";
$Lang['AccountMgmt']['NewUser'] = "新增用户";
$Lang['AccountMgmt']['EditUser'] = "更新用户";
$Lang['AccountMgmt']['ExportUser'] = "汇出用户";
$Lang['AccountMgmt']['DeleteUser'] = "删除用户";
$Lang['AccountMgmt']['SystemInfo'] = "系统资料";
$Lang['AccountMgmt']['BasicInfo'] = "基本资料";
$Lang['AccountMgmt']['InternetUsage'] = "互联网用途";
$Lang['AccountMgmt']['Chi'] = "中文";
$Lang['AccountMgmt']['UserDisplayTitle'] = "显示称谓";
$Lang['AccountMgmt']['Retype'] = "再次输入";
$Lang['AccountMgmt']['SelectCountry'] = "选择国家";
$Lang['AccountMgmt']['Tel'] = "电话";
$Lang['AccountMgmt']['Home'] = "住宅";
$Lang['AccountMgmt']['Office'] = "公司";
$Lang['AccountMgmt']['Mobile'] = "手提";
$Lang['AccountMgmt']['SaveAndContinue'] = "储存并继续";
$Lang['AccountMgmt']['ClassGroupInfo'] = "班别及组别资料";
$Lang['AccountMgmt']['UsedByOtherUser'] = "已被其他人使用。";
$Lang['AccountMgmt']['SendPassword'] = "传送密码";
$Lang['AccountMgmt']['RoleID'] = "身份角色代号";
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserLogin;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserPassword;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_identity." (\"T\" = ".$i_campusmail_teaching_staff.", \"NT\" = ".$i_campusmail_non_teaching_staff.")";
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserEmail;
$Lang['AccountMgmt']['StaffImport'][] = $i_SmartCard_CardID;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserEnglishName;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserChineseName;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserNickName;
$Lang['AccountMgmt']['StaffImport'][] = "<font color=red>*</font> ".$i_UserGender." (\"M\" = $i_gender_male, \"F\" = $i_gender_female)";
//$Lang['AccountMgmt']['StaffImport'][] = $i_UserTitle;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserDisplayTitle_English;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserDisplayTitle_Chinese;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserAddress;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserCountry;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserHomeTelNo;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserOfficeTelNo;
$Lang['AccountMgmt']['StaffImport'][] = $i_UserMobileTelNo;
//$Lang['AccountMgmt']['StaffImport'][] = $i_StaffAttendance_GroupID;
//$Lang['AccountMgmt']['StaffImport'][] = $Lang['AccountMgmt']['RoleID'];
$Lang['AccountMgmt']['StaffImport'][] = $i_Mail_AllowSendReceiveExternalMail." (\"Y\" = $i_general_yes, \"N\" = $i_general_no)";
$Lang['AccountMgmt']['StaffImport'][] = $i_Files_OpenAccount." (\"Y\" = $i_general_yes, \"N\" = $i_general_no)";
$Lang['AccountMgmt']['SomeGroupsDoNotExist'] = "部份组别不存在";
$Lang['AccountMgmt']['SomeRolesDoNotExist'] = "部份身份角式不存在";
$Lang['AccountMgmt']['InputStaffDetails'] = "输入教职员资料";
$Lang['AccountMgmt']['SelectGroupAndRole'] = "选择相关组别及身份角色";
$Lang['AccountMgmt']['InputStudentDetails'] = "输入学生资料";
$Lang['AccountMgmt']['InputGuardianDetail'] = "输入监护人资料";
$Lang['AccountMgmt']['YYYYMMDD'] = "yyyy-mm-dd";
$Lang['AccountMgmt']['OtherInfo'] = "其他资料";
$Lang['AccountMgmt']['ClassGroupInfo'] = "班别及组别资料";
$Lang['AccountMgmt']['GuardianInfo'] = "监护人资料";
$Lang['AccountMgmt']['StudentInfo'] = "学生资料";
$Lang['AccountMgmt']['MainGuardian'] = "主监护人";
$Lang['AccountMgmt']['ReceiveSMS'] = "SMS收取人";
$Lang['AccountMgmt']['EmergencyContact'] = "紧急联络人";
$Lang['AccountMgmt']['AlertDeleteEmergencyContact'] = "你选择删除紧急联络人，请选择其他紧急联络人。";
$Lang['AccountMgmt']['InputParentDetails'] = "输入家长资料";
$Lang['AccountMgmt']['SelectCorrespondingStudents'] = "选择相关学生";
$Lang['AccountMgmt']['CheckAvailability'] = "检查可用性";
$Lang['AccountMgmt']['IsAvailable'] = "可以使用。";
$Lang['AccountMgmt']['IsNotAvailable'] = "已有人使用。";
$Lang['AccountMgmt']['RelatedToStudent'] = "学生相关资料";
$Lang['AccountMgmt']['AddAsStudentGuardian'] = "成为学生监护人";
$Lang['AccountMgmt']['SelectMore'] = "选择更多";
$Lang['AccountMgmt']['CopyToAll'] = "套用至所有";
$Lang['AccountMgmt']['AllClassesParent'] = "所有班别家长";
$Lang['AccountMgmt']['ClassNo'] = "学号";
$Lang['AccountMgmt']['StudentName'] = "学生姓名";
$Lang['AccountMgmt']['LoginID'] = "登入编号";
$Lang['AccountMgmt']['Photo'] = "相片";
$Lang['AccountMgmt']['Form'] = "级别";
$Lang['AccountMgmt']['Class'] = "班别";
$Lang['AccountMgmt']['NoOfStudents'] = "学生人数";
$Lang['AccountMgmt']['NoOfStudentsWithoutPhoto'] = "没有相片的学生人数";
$Lang['AccountMgmt']['ConfirmMsg']['RemoveSelectedPhoto'] = "确定要删除所选所选的相片?";
$Lang['AccountMgmt']['UploadPhoto']['StepArr'][] = "上载相片";
$Lang['AccountMgmt']['UploadPhoto']['StepArr'][] = "上载完成";
$Lang['AccountMgmt']['Remarks'] = "备注";
$Lang['AccountMgmt']['File'] = "档案";
$Lang['AccountMgmt']['RemarksList'][] = "建议使用 100px x 130px (阔 x 高)的相片";
$Lang['AccountMgmt']['RemarksList'][] = "只接受JPEG档的相片";
$Lang['AccountMgmt']['RemarksList'][] = "可以以压缩档案(.zip)形式上载多个档案";
$Lang['AccountMgmt']['RemarksList'][] = "压缩档案内不能存在文件夹";
$Lang['AccountMgmt']['RemarksList'][] = "相片的档案名称要与学生的登入编号一样";
$Lang['AccountMgmt']['PhotoAttempt'] = "上载相片数";
$Lang['AccountMgmt']['UploadSuccess'] = "上载成功";
$Lang['AccountMgmt']['UploadFail'] = "上载失败";
$Lang['AccountMgmt']['UploadMorePhoto'] = "上载更多相片";
$Lang['AccountMgmt']['BackToPhotoMgmt'] = "返回相片管理";
$Lang['AccountMgmt']['ManagePhoto'] = "管理相片";
$Lang['AccountMgmt']['WarnMsg']['PleaseSelectJPGorZIP'] = "请选择JPEG档(.jpg)或压缩档(.zip)";
$Lang['AccountMgmt']['ImportStudentAccount'] = "汇入学生户口";
$Lang['AccountMgmt']['ImportParentAccount'] = "汇入家长户口";
$Lang['AccountMgmt']['ImportTeacherAccount'] = "汇入老师户口";
$Lang['AccountMgmt']['ImportAlumniAccount'] = "汇入校友户口";
$Lang['AccountMgmt']['Import'] = "汇入";
$Lang['AccountMgmt']['Export'] = "汇出";
$Lang['AccountMgmt']['FileMailRemarks'] = "系统只会为新 eClass 用户, 在档案/邮件伺服器开启户口. 现有用户需要在 <b>行政管理中心 > 功能设定 > iFolder 储存量 </b> 设定";
$Lang['AccountMgmt']['Options'] = "选项";
$Lang['AccountMgmt']['ImportRemarks'][] = "档案中的 'UserLogin' 须由 'a-z'(小楷), '0-9' 及 '_' 组成, 而且必须使用 'a-z' 为开始字元"; 
$Lang['AccountMgmt']['ImportRemarks'][] = "如果档案中的 'UserLogin' 已经存在, 该帐户所有资料会被更新(包括电邮及密码)"; 
$Lang['AccountMgmt']['ImportRemarks'][] = "如欲不更新电邮及密码, 可在档案中留空该栏";
$Lang['AccountMgmt']['ImportRemarks2'] = "为免遗失号码前端的数字 \"0\"，请在每个 WebSAMS 注册号码前加上 \"#\" 号 (如: #0012345 )";
$Lang['AccountMgmt']['WarnMsg']['PleaseSelectCSVorTXT'] = "请选择CSV档(.csv) 或纯文字档(.txt)";

$Lang['AccountMgmt']['ErrorMsg']['InvalidEmail'] = "电邮格式错误";
$Lang['AccountMgmt']['ErrorMsg']['InvalidLoginEmail'] = "登入名称格式错误";
$Lang['AccountMgmt']['ErrorMsg']['EmailUsedInImport'] = "电邮已被其他汇入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['EmailUsed'] = "电邮已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['DifferentUserType'] = "使用者类别与汇入中的使用者类别不同";
$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsedInImport'] = "智能卡编号已被其他汇入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['SmartCardIDUsed'] = "智能卡编号已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['HKIDUsedInImport'] = "HKID 已被其他汇入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['HKIDUsed'] = "HKID 已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['STRNUsedInImport'] = "STRN 已被其他汇入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['STRNUsed'] = "STRN 已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoUsedInImport'] = "WebSAMS 注册号码已被其他汇入中的使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoUsed']  = "WebSAMS 注册号码已被其他使用者使用";
$Lang['AccountMgmt']['ErrorMsg']['WebSamsRegNoCannotChange'] = "WebSAMS 注册号码不能更改";
$Lang['AccountMgmt']['ErrorMsg']['PleaseAddSymbol'] = "请在WebSAMS 注册号码前加上#号";
$Lang['AccountMgmt']['ErrorMsg']['CannotFindStudent'] = "找不到学生";
$Lang['AccountMgmt']['ExportStudentAccount'] = "汇出学生户口";
$Lang['AccountMgmt']['ExportParentAccount'] = "汇出家长户口";
$Lang['AccountMgmt']['ExportTeacherAccount'] = "汇出老师户口";
$Lang['AccountMgmt']['ExportAlumniAccount'] = "汇出校友户口";
$Lang['AccountMgmt']['CondfirmDeleteAllParent'] = "你是否确定要删除所有家长?";
$Lang['AccountMgmt']['DeleteSelected'] = "删除已选";
$Lang['AccountMgmt']['DeleteAll'] = "删除所有";
$Lang['AccountMgmt']['ConfirmDeleteAllParent'] = "你是否确定删除所有家长户口?";
$Lang['AccountMgmt']['ConfirmArchiveAllStudent'] = "你是否确定存档所有学生户口?";
$Lang['AccountMgmt']['ArchiveAll'] = "存档所有";
$Lang['AccountMgmt']['ArchiveSelected'] = "存档已选";

$Lang['AccountMgmt']['ExportFormat'] = "汇出格式";
$Lang['AccountMgmt']['SpecificColumns'] = "指定资料";
$Lang['AccountMgmt']['Importing'] = "汇入";
$Lang['AccountMgmt']['Settings']['AccessRights'] = "权限设定";
$Lang['AccountMgmt']['Settings']['GroupList'] = "小组列表";
$Lang['AccountMgmt']['Settings']['GroupName'] = "小组名称";
$Lang['AccountMgmt']['Settings']['Description'] = "描述";
$Lang['AccountMgmt']['Settings']['NoOfMembers'] = "成员人数";
$Lang['AccountMgmt']['Settings']['AccessRight'] = "存取权限";
$Lang['AccountMgmt']['Settings']['UserList'] = "用户名单";
$Lang['AccountMgmt']['Settings']['View'] = "检视";
$Lang['AccountMgmt']['Settings']['Manage'] = "管理";
$Lang['AccountMgmt']['Settings']['Access'] = "存取";

# Student Registry
$Lang['StudentRegistry']['ClassList'] = "班别列表";
$Lang['StudentRegistry']['StudentList'] = "学生列表";
$Lang['StudentRegistry']['Class'] = "班别";
$Lang['StudentRegistry']['NoOfStudents'] = "学生人数";
$Lang['StudentRegistry']['AcademicYear'] = "学年";
$Lang['StudentRegistry']['AllRecordStatus'] = "所有状况";
$Lang['StudentRegistry']['Normal'] = "正常记录";
$Lang['StudentRegistry']['WaitForApproval'] = "等待批核更新人数";
$Lang['StudentRegistry']['RecordStatusArray'] = array(0=>$Lang['StudentRegistry']['WaitForApproval'], 1=>$Lang['StudentRegistry']['Normal']);
$Lang['StudentRegistry']['AllRegistryStatus'] = "所有学籍状况";
$Lang['StudentRegistry']['RegistryStatus'] = "学籍状况";
$Lang['StudentRegistry']['StatusApproved'] = "在籍";
$Lang['StudentRegistry']['StatusSuspended'] = "休学";
$Lang['StudentRegistry']['StatusResume'] = "复学";
$Lang['StudentRegistry']['StatusBack'] = "复学";
$Lang['StudentRegistry']['StatusLeft'] = "退学";
$Lang['StudentRegistry']['RegistryStatusArray'] = array(0=>$Lang['StudentRegistry']['StatusSuspended'], 1=>$Lang['StudentRegistry']['StatusApproved'], 3=>$Lang['StudentRegistry']['StatusLeft']);
$Lang['StudentRegistry']['Total'] = "总数";
$Lang['StudentRegistry']['ClassNumber'] = "座号";
$Lang['StudentRegistry']['ChineseName'] = "中文姓名";
$Lang['StudentRegistry']['EnglishName'] = "英文姓名";
$Lang['StudentRegistry']['ForeignName'] = "外文姓名";
$Lang['StudentRegistry']['WebSAMSRegNo'] = "WebSAMS 注册号码";
$Lang['StudentRegistry']['DSEJ_Number'] = "教青局学生编号";
$Lang['StudentRegistry']['LastUpdated'] = "最后更新日期 (更新者)";
$Lang['StudentRegistry']['ApproveStatus'] = "批核更新状态 (批核者)";
$Lang['StudentRegistry']['Approved'] = "已批核";
$Lang['StudentRegistry']['ApprovedBySystem'] = "系统";
$Lang['StudentRegistry']['CSV'] = "CSV";
$Lang['StudentRegistry']['XML'] = "XML - 学年网上注册系统(教青)[方案三和五]";
$Lang['StudentRegistry']['PageName1'] = "第";
$Lang['StudentRegistry']['TotalRecord'] = "合共";
$Lang['StudentRegistry']['UserList'] = "成员列名单";

# Student Registry (for student details - Common)
$Lang['StudentRegistry']['BasicInfo'] = "基本资料";
$Lang['StudentRegistry']['AdvInfo'] = "进阶资料";
$Lang['StudentRegistry']['StudInfo'] = "学生资料";
$Lang['StudentRegistry']['FatherInfo'] = "父亲资料";
$Lang['StudentRegistry']['MotherInfo'] = "母亲资料";
$Lang['StudentRegistry']['GuardianInfo'] = "监护人资料";
$Lang['StudentRegistry']['ContactPersonInfo'] = "联络人资料";
$Lang['StudentRegistry']['EmergencyContactInfo'] = "紧急联络人资料";

$Lang['StudentRegistry']['Save'] = "储存";
$Lang['StudentRegistry']['Cancel'] = "取消";
$Lang['StudentRegistry']['Edit'] = "编辑";
$Lang['StudentRegistry']['Hide'] = "隐藏";
$Lang['StudentRegistry']['Show'] = "展开";
$Lang['StudentRegistry']['Other'] = "其他";
$Lang['StudentRegistry']['EditInfo'] = "编辑资料";
$Lang['StudentRegistry']['Mandatory'] = "必须填写项目";
$Lang['StudentRegistry']['PleaseEnter'] = "请输入";

//$Lang['StudentRegistry']['ChineseName'] = "中文姓名";
//$Lang['StudentRegistry']['EnglishName'] = "英文姓名";
$Lang['StudentRegistry']['EntryDate'] = "入学日期";
$Lang['StudentRegistry']['StudyAt'] = "就读";
$Lang['StudentRegistry']['Grade'] = "年级";
//$Lang['StudentRegistry']['Class'] = "班别";
$Lang['StudentRegistry']['InClassNumber'] = "班内号";
$Lang['StudentRegistry']['Gender'] = "性别";
$Lang['StudentRegistry']['BirthDate'] = "出生日期";
$Lang['StudentRegistry']['BirthPlace'] = "出生地点";
$Lang['StudentRegistry']['Nationality'] = "国籍";
$Lang['StudentRegistry']['HomePhone'] = "住家电话";
$Lang['StudentRegistry']['CellPhone'] = "流动电话";
$Lang['StudentRegistry']['PastEnroll'] = "过去班级";
$Lang['StudentRegistry']['Relationship'] = "与学生关系";
$Lang['StudentRegistry']['RelationOther'] = "与学生关系[其他]";
$Lang['StudentRegistry']['OtherGuardian'] = "其他监护人";
$Lang['StudentRegistry']['ByWho'] = "资料来源";

# Student Registry (for student details - Macau)
$Lang['StudentRegistry']['StudentID'] = "学校内学生代号";
$Lang['StudentRegistry']['PortugueseName'] = "葡文姓名";
$Lang['StudentRegistry']['Province'] = "籍贯";
$Lang['StudentRegistry']['SchoolCode'] = "校部编号";

$Lang['StudentRegistry']['IdCard'] = "身份证明文件";
$Lang['StudentRegistry']['StayPermission'] = "逗留许可";
$Lang['StudentRegistry']['CardType'] = "种类";
$Lang['StudentRegistry']['CardNumber'] = "编号";
$Lang['StudentRegistry']['CardIssuePlace'] = "发出地点";
$Lang['StudentRegistry']['CardIssueDate'] = "本次发出日期";
$Lang['StudentRegistry']['CardValidDate'] = "有效日期";

$Lang['StudentRegistry']['NightRes'] = "夜间住宿";
$Lang['StudentRegistry']['Area'] = "地区";
$Lang['StudentRegistry']['DistrictName'] = "地区之名称";
$Lang['StudentRegistry']['HomeDistrict'] = "居住地区";
$Lang['StudentRegistry']['Address'] = "住址";
$Lang['StudentRegistry']['Street'] = "街名";
$Lang['StudentRegistry']['AddDetail'] = "门牌、大厦、层数、座";

$Lang['StudentRegistry']['Job'] = "职业";
$Lang['StudentRegistry']['StayTogether'] = "与学生同住";
$Lang['StudentRegistry']['Phone'] = "电话号码";

# Student Registry (for student details - Malaysia)
$Lang['StudentRegistry']['Mal_StudentNo'] = "学号";
$Lang['StudentRegistry']['BirthCertNo'] = "报生纸号码";
$Lang['StudentRegistry']['Race'] = "种族";
$Lang['StudentRegistry']['AncestalHome'] = "祖籍";
$Lang['StudentRegistry']['Religion'] = "宗教";
$Lang['StudentRegistry']['PayType'] = "缴费类别";
$Lang['StudentRegistry']['Email'] = "电子邮件";
$Lang['StudentRegistry']['FamilyLang'] = "家庭用语";
$Lang['StudentRegistry']['HousingState'] = "住宿情况";
$Lang['StudentRegistry']['HomeAddress'] = "居住地址";
$Lang['StudentRegistry']['ContactAddress'] = "通讯地址";
$Lang['StudentRegistry']['Door'] = "门牌号码";
$Lang['StudentRegistry']['Road'] = "路名";
$Lang['StudentRegistry']['Garden'] = "花园";
$Lang['StudentRegistry']['Town'] = "市填区";
$Lang['StudentRegistry']['State'] = "州属";
$Lang['StudentRegistry']['Postal'] = "邮区号码";
$Lang['StudentRegistry']['PastPrimarySch'] = "小学就读学校";
$Lang['StudentRegistry']['Siblings'] = "目前在校就读之兄姐 (最高年级者)";
$Lang['StudentRegistry']['ContactPerson'] = "联络人";
$Lang['StudentRegistry']['EducationLevel'] = "教育程度";
$Lang['StudentRegistry']['JobNature'] = "职业性质";
$Lang['StudentRegistry']['JobTitle'] = "职称";
$Lang['StudentRegistry']['OfficeNumber'] = "办公电话";
$Lang['StudentRegistry']['MaritalStatus'] = "婚姻状况";

# export column name (Macau)
$Lang['StudentRegistry']['CSV_field'][0] = array("STUD_ID", "CODE", "NAME_C", "NAME_P", "SEX", "B_DATE", "B_PLACE", "ID_TYPE", "ID_NO", "I_PLACE", "I_DATE", "V_DATE", "S6_TYPE", "S6_IDATE", "S6_VDATE", "NATION", "ORIGIN", "TEL", "R_AREA", "RA_DESC", "AREA", "ROAD", "ADDRESS", "FATHER", "MOTHER", "F_PROF", "M_PROF", "GUARD", "LIVE_SAME", "EC_NAME", "EC_REL", "EC_TEL", "EC_AREA", "EC_ROAD", "EC_ADDRESS", "S_CODE", "GRADE", "CLASS", "C_NO", "G_NAME", "G_RELATION", "G_PROFESSION", "G_AREA", "G_ROAD", "G_ADDRESS", "G_TEL", "GUARDMOBILE");
$Lang['StudentRegistry']['CSV_field'][1] = array("学校内学生代号", "教青局学生编号", "中文姓名", "葡文姓名", "性别", "出生日期", "出生地点", "身份证明文件种类", "身份证号码", "身份证明文件发出地点", "身份证明文件本次发出日期", "身份证明文件有效日期", "逗留许可", "逗留许可本次发出日期", "逗留许可有效日期", "国籍", "籍贯", "电话", "夜间住宿地区", "夜间住宿地区之名称", "居住地区", "住址街名", "住址", "父亲姓名", "母亲姓名", "父亲职业", "母亲职业", "与监护人关系", "与监护人同住", "紧急联络人姓名", "紧急联络人关系", "紧急联络人电话", "紧急联络人居住地区", "紧急联络人住址街名", "紧急联络人住址", "校部编号", "年级", "班别", "班号", "监护人姓名", "监护人与学生关系", "监护人职业", "监护人居住地区", "监护人住址街名", "监护人住址", "监护人电话", "监护人流动电话");

$Lang['StudentRegistry']['Relation'] = array("F"=>"Father", "M"=>"Mother", "G"=>"Guardian", "E"=>"Emergency Contact", "O"=>"Others");

# import column name (Macau)
$Lang['StudentRegistry']['Import_Macau'] = array("班别", "班内号", "学生内联网帐号", "学校内学生代号", "教青局学生编号", "中文姓名", "外文姓名", "入学日期", "校部编号", "性别", "出生日期", "出生地点", "身份证明文件种类", "身份证明文件编号", "身份证明文件发出地点", "身份证明文件本次发出日期", "身份证明文件有效日期", "逗留许可", "逗留许可本次发出日期", "逗留许可有效日期", "国籍", "籍贯", "宗教", "电子邮件", "电话号码", "夜间住宿地区", "夜间住宿地区(补充)", "居住地区", "住址(街名)", "住址(门牌、大厦、层数、座)", "父亲中文姓名", "父亲外文姓名", "父亲职业", "父亲婚姻状况", "父亲住宅电话", "父亲手提电话", "父亲办公电话", "父亲电子邮件", "母亲中文姓名", "母亲外文姓名", "母亲职业", "母亲婚姻状况", "母亲住宅电话", "母亲手提电话", "母亲办公电话", "母亲电子邮件", "监护人中文姓名", "监护人外文姓名", "监护人性别", "监护人职业", "与学生关系", "与学生关系(其他)", "与学生同住", "监护人居住地区", "监护人住址(街名)", "监护人住址(门牌、大厦、层数、座)", "监护人住宅电话", "监护人手提电话", "监护人办公电话", "监护人电子邮件", "紧急联络人中文姓名", "紧急联络人外文姓名", "与紧急联络人关系", "紧急联络人电话", "紧急联络人流动电话", "紧急联络人居住地区", "紧急联络人住址(街名)", "紧急联络人住址(门牌, 大厦, 层数, 座)");

# export column name (Malaysia)
$Lang['StudentRegistry']['Malaysia_CSV'][0] = array("Class Name", "Class No.", "Login Name", "Student No.", "Chinese Name", "English Name", "Gender", "Date of Birth", "Birth Place", "Nationality", "Ancestral Home", "Race", "ID Card No.", "Birth Cert. No.", "Religion", "Family Language", "Payment Type", "Phone No.(Home)", "Email", "Housing Status", "Home Address – (Door)", "Home Address – (Road)", "Home Address – (Garden)", "Home Address – (Town)", "Home Address – (State)", "Home Address – (Postal Code)", "Date of Entry", "Elder Brother/Sister in School", "Primary School", "Father Chinese Name", "Father English Name", "Father Job Occupation", "Father Job Nature", "Father Job Title", "Father Marital Status", "Father Level of Education", "Father Phone No.(Home)", "Father Phone No.(Mobile)", "Father Phone No.(Office)", "Father Contact Address – (Door)", "Father Contact Address – (Road)", "Father Contact Address – (Garden)", "Father Contact Address – (Town)", "Father Contact Address – (State)", "Father Contact Address – (Postal Code)", "Father Postal Code", "Father Email", "Mother Chinese Name", "Mother English Name", "Mother Job Occupation", "Mother Job Nature", "Mother Job Title", "Mother Marital Status", "Mother Level of Education", "Mother Phone No.(Home)", "Mother Phone No.(Mobile)", "Mother Phone No.(Office)", "Mother Contact Address – (Door)", "Mother Contact Address – (Road)", "Mother Contact Address – (Garden)", "Mother Contact Address – (Town)", "Mother Contact Address – (State)", "Mother Contact Address – (Postal Code)", "Mother Postal Code", "Mother Email", "Guardian Chinese Name", "Guardian English Name", "Guardian Gender", "Guardian Relationship", "Guardian Relationship (Other)", "Guardian Live with Student", "Guardian Job Occupation", "Guardian Job Nature", "Guardian Job Title", "Guardian Level of Education", "Guardian Phone No.(Home)", "Guardian Phone No.(Mobile)", "Guardian Phone No.(Office)", "Guardian Contact Address – (Door)", "Guardian Contact Address – (Road)", "Guardian Contact Address – (Garden)", "Guardian Contact Address – (Town)", "Guardian Contact Address – (State)", "Guardian Contact Address – (Postal Code)", "Guardian Postal Code", "Guardian Email");
$Lang['StudentRegistry']['Malaysia_CSV'][1] = array("班别", "座号", "学生内联网账号", "学号", "中文姓名", "英文姓名", "性別", "出生日期", "出生地点", "国籍", "祖籍", "种族", "身份证明文件编号", "报生纸号码", "宗教", "家庭用语", "缴费类别", "住家电话", "电子邮件", "住宿情况", "居住地址 – 门牌号码", "居住地址 – 路名", "居住地址 – 花园", "居住地址 – 市镇区", "居住地址 – 州属", "居住地址 – 邮区号码", "入学日期", "目前就读之兄姐 ", "小学就读学校", "父亲中文姓名", "父亲英文姓名", "父亲职业", "父亲职业性质", "父亲职称", "父亲婚姻状况", "父亲教育程度", "父亲住家电话", "父亲流动电话", "父亲办公电话", "父亲通讯地址 – 门牌号码", "父亲通讯地址 – 路名", "父亲通讯地址 – 花园", "父亲通讯地址 – 市镇区", "父亲通讯地址 – 州属", "父亲通讯地址 – 邮区号码", "父亲邮区号码", "父亲电子邮件", "母亲中文姓名", "母亲英文姓名", "母亲职业", "母亲职业性质", "母亲职称", "母亲婚姻状况", "母亲教育程度", "母亲住家电话", "母亲流动电话", "母亲办公电话", "母亲通讯地址 – 门牌号码", "母亲通讯地址 – 路名", "母亲通讯地址 – 花园", "母亲通讯地址 – 市镇区", "母亲通讯地址 – 州属", "母亲通讯地址 – 邮区号码", "母亲邮区号码", "母亲电子邮件", "监护人中文姓名", "监护人英文姓名", "监护人性別", "监护人与学生关系", "监护人与学生关系(其他)", "监护人与同学同住", "监护人职业", "监护人职业性质", "监护人职称", "监护人教育程度", "监护人住家电话", "监护人流动电话", "监护人办公电话", "监护人通讯地址 – 门牌号码", "监护人通讯地址 – 路名", "监护人通讯地址 – 花园", "监护人通讯地址 – 市镇区", "监护人通讯地址 – 州属", "监护人通讯地址 – 邮区号码", "监护人邮区号码", "监护人电子邮件");

# import column name (Malaysia)
$Lang['StudentRegistry']['Import_Malaysia'] = array("班别", "座号", "学生内联网账号", "学号", "中文姓名", "英文姓名", "性別", "出生日期", "出生地点", "国籍", "祖籍", "种族", "身份证明文件编号", "报生纸号码", "宗教", "家庭用语", "缴费类别", "住家电话", "电子邮件", "住宿情况", "居住地址 – 门牌号码", "居住地址 – 路名", "居住地址 – 花园", "居住地址 – 市镇区", "居住地址 – 州属", "居住地址 – 邮区号码", "入学日期", "目前就读之兄姐 ", "小学就读学校", "父亲中文姓名", "父亲英文姓名", "父亲职业", "父亲职业性质", "父亲职称", "父亲婚姻状况", "父亲教育程度", "父亲住家电话", "父亲流动电话", "父亲办公电话", "父亲通讯地址 – 门牌号码", "父亲通讯地址 – 路名", "父亲通讯地址 – 花园", "父亲通讯地址 – 市镇区", "父亲通讯地址 – 州属", "父亲通讯地址 – 邮区号码", "父亲邮区号码", "父亲电子邮件", "母亲中文姓名", "母亲英文姓名", "母亲职业", "母亲职业性质", "母亲职称", "母亲婚姻状况", "母亲教育程度", "母亲住家电话", "母亲流动电话", "母亲办公电话", "母亲通讯地址 – 门牌号码", "母亲通讯地址 – 路名", "母亲通讯地址 – 花园", "母亲通讯地址 – 市镇区", "母亲通讯地址 – 州属", "母亲通讯地址 – 邮区号码", "母亲邮区号码", "母亲电子邮件", "监护人中文姓名", "监护人英文姓名", "监护人性別", "监护人与学生关系", "监护人与学生关系(其他)", "监护人与同学同住", "监护人职业", "监护人职业性质", "监护人职称", "监护人教育程度", "监护人住家电话", "监护人流动电话", "监护人办公电话", "监护人通讯地址 – 门牌号码", "监护人通讯地址 – 路名", "监护人通讯地址 – 花园", "监护人通讯地址 – 市镇区", "监护人通讯地址 – 州属", "监护人通讯地址 – 邮区号码", "监护人邮区号码", "监护人电子邮件");

$Lang['StudentRegistry']['ResidentStatus'] = array(1=>"永久", 2=>"有限期", 3=>"其他逗留许可");
$Lang['StudentRegistry']['R_Area_Ref'] = array("M"=>"澳门(包括离岛)", "C"=>"中国", "O"=>"其他");
$Lang['StudentRegistry']['Area_Ref'] = array("M"=>"澳门", "T"=>"凼仔", "C"=>"路环", "O"=>"其他");

# export Class column list (Malaysia)
$Lang['StudentRegistry']['Malaysia_Class_CSV_field'][0] = array("Form", "Class", "Total no. of approved student(s)", "Total no. of left student(s)", "Total no. of suspend student(s)", "Total no. of resume student(s)", "Total no. of student(s)"); 
$Lang['StudentRegistry']['Malaysia_Class_CSV_field'][1] = array("级别", "班级", "在学人数", "退学人数", "休学人数", "复学人数", "总人数");

# export Student column list (Malaysia)
$Lang['StudentRegistry']['Malaysia_Student_CSV_field'][0] = array("Student No.", "Class", "Class Number", "English Name", "Chinese Name", "Registry Status", "Reason", "Apply Date", "Last Updated (Updated By)");
$Lang['StudentRegistry']['Malaysia_Student_CSV_field'][1] = array("学号", "班级", "座号", "中文姓名", "英文姓名", "学籍状况", "原因", "申请日期", "最后更新日期 (更新者)");

# Import Checking (Macau)
$Lang['StudentRegistry']['IsMissing'] = "不存在";
$Lang['StudentRegistry']['IsInvalid'] = "不正确";
$Lang['StudentRegistry']['alreadyExist'] = "已存在";
$Lang['StudentRegistry']['StudentInfo'] = "班别/班内号或学生内联网帐号";
$Lang['StudentRegistry']['StudentInfo2'] = "学生资料";
$Lang['StudentRegistry']['FatherName'] = "父亲姓名";
$Lang['StudentRegistry']['MotherName'] = "母亲姓名";
$Lang['StudentRegistry']['GuardianName'] = "监护人姓名";
$Lang['StudentRegistry']['EmergencyContactPerson'] = "紧急联络人姓名";
$Lang['StudentRegistry']['MaritalStatus'] = "婚姻状况";
$Lang['StudentRegistry']['DateFormatRemark'] = "<font color='red'>#</font>";
$Lang['StudentRegistry']['MandatoryRemark'] = "<font color='red'>*</font>";
$i_EventTypeString = array (
"学校事项",
"教学事项",
"假期",
"小组事项",
"学校假期"
);

#Import Checking (Malaysia)
$Lang['StudentRegistry']['SiblingsRemark'] = "<font color='red'>+</font>";
$Lang['StudentRegistry']['SiblingsFormat'] = "请输入兄姐之学号, 如多于一位兄姐于同校就读, 请以「,」将学号分隔";

# Student Registry Status
$Lang['StudentRegistry']['StudentRegistryStatus'] = "学籍状况";
$Lang['StudentRegistry']['RegistryStatus_Approved'] = "在籍";
$Lang['StudentRegistry']['RegistryStatus_Left'] = "退学";
$Lang['StudentRegistry']['RegistryStatus_Suspend'] = "休学";
$Lang['StudentRegistry']['RegistryStatus_Resume'] = "复学";
$Lang['StudentRegistry']['AmountOfApprove'] = "在学人数";
$Lang['StudentRegistry']['AmountOfLeft'] = "退学人数";
$Lang['StudentRegistry']['AmountOfSuspend'] = "休学人数";
$Lang['StudentRegistry']['AmountOfResume'] = "复学人数";
$Lang['StudentRegistry']['TotalAmountOfStudents'] = "总人数";
$Lang['StudentRegistry']['StudentNo'] = "学号";
$Lang['StudentRegistry']['ApplyDate'] = "申请日期";
$Lang['StudentRegistry']['Notice'] = "通告";
$Lang['StudentRegistry']['Reason'] = "原因";
$Lang['StudentRegistry']['View'] = "检视";
$Lang['StudentRegistry']['ReasonAry'] = array("1"=>"转入国中","2"=>"外国升造","3"=>"搬迁","4"=>"经济困难","5"=>"连续旷课7天","6"=>"健康不佳","7"=>"工作","8"=>"适应不良","9"=>"无心向学","10"=>"升学","0"=>"其他");
$Lang['StudentRegistry']['SelectStudent'] = "选择学生";
$Lang['StudentRegistry']['InputLeftInfo'] = "输入退学资料";
$Lang['StudentRegistry']['InputSuspendInfo'] = "输入休学资料";
$Lang['StudentRegistry']['InputResumeInfo'] = "输入复学资料";
$Lang['StudentRegistry']['LeftNotice'] = "退学通告";
$Lang['StudentRegistry']['SuspendNotice'] = "休学通告";
$Lang['StudentRegistry']['ResumeNotice'] = "复学通告";
$Lang['StudentRegistry']['PrintNotice'] = "列印通告";
$Lang['StudentRegistry']['NoStudentRecord'] = "未有学生资料";
$Lang['StudentRegistry']['ApplyYear'] = "申请时年度";
$Lang['StudentRegistry']['ApplyForm'] = "申请时级别";
$Lang['StudentRegistry']['Class'] = "班级";
$Lang['StudentRegistry']['Mobile'] = "手提电话";
$Lang['StudentRegistry']['RelationshipWithStudent'] = "与学生关系";
$Lang['StudentRegistry']['ChangeOfRegistryStatus'] = "学籍状况变更";
$Lang['StudentRegistry']['ApplyFor'] = "申请";
$Lang['StudentRegistry']['PrintNoticeToGuardian'] = "列印通告至监护人";
$Lang['StudentRegistry']['eNoticeTemplate'] = "电子通告样版";
$Lang['StudentRegistry']['eNotice_ContactPersonName'] = "联络人姓名";
$Lang['StudentRegistry']['eNotice_ContactPersonAddress'] = "联络人地址";
$Lang['StudentRegistry']['eNotice_ReasonOfLeft'] = "退学原因";
$Lang['StudentRegistry']['eNotice_DateOfLeft'] = "退学日期";
$Lang['StudentRegistry']['eNotice_ReasonOfSuspend'] = "退学原因";
$Lang['StudentRegistry']['eNotice_DateOfSuspend'] = "休学日期";
$Lang['StudentRegistry']['eNotice_DateOfResume'] = "复学日期";
$Lang['StudentRegistry']['NoNoticeForThisStudent'] = "此学生并无通告";
$Lang['StudentRegistry']['ListOfLeft'] = "退学名单";
$Lang['StudentRegistry']['StatisticsAndReasonOfLeft'] = "退学率及原因统计";
$Lang['StudentRegistry']['ListOfSuspend'] = "休学名单";
$Lang['StudentRegistry']['StatisticsAndReasonOfSuspend'] = "休学率及原因统计";
$Lang['StudentRegistry']['ListOfResume'] = "复学名单";
$Lang['StudentRegistry']['StatisticsOfResume'] = "复学率";
$Lang['StudentRegistry']['GenerateReport'] = "制作报告";
$Lang['StudentRegistry']['PercentageOfLeft'] = "退学率";
$Lang['StudentRegistry']['StatisticsOfLeftReason'] = "退学原因统计";
$Lang['StudentRegistry']['PercentageOfSuspend'] = "休学率";
$Lang['StudentRegistry']['StatisticsOfSuspendReason'] = "休学原因统计";
$Lang['StudentRegistry']['PercentageOfResume'] = "复学率";
$Lang['StudentRegistry']['NotYetAttended'] = "未报到";
$Lang['StudentRegistry']['Amount'] = "人数";
$Lang['StudentRegistry']['SubTotal'] = "小计";
$Lang['StudentRegistry']['Total'] = "总计";
$Lang['StudentRegistry']['TotalStudents'] = "全校人数";
$Lang['StudentRegistry']['Percentage'] = "巴仙率";
$Lang['StudentRegistry']['HinHuaHighSchool'] = "兴华中学";
$Lang['StudentRegistry']['Of'] = "";
$Lang['StudentRegistry']['StudentStatistics_Type_Malaysia'] = array("LODGING"=>$Lang['StudentRegistry']['HousingState'], "RACE"=>$Lang['StudentRegistry']['Race'], "FAMILY_LANG"=>$Lang['StudentRegistry']['FamilyLang'], "GENDER"=>$Lang['StudentRegistry']['Gender'], "NATION"=>$Lang['StudentRegistry']['Nationality'], "PAYMENT_TYPE"=>$Lang['StudentRegistry']['PayType'], "B_PLACE"=>$Lang['StudentRegistry']['BirthPlace'], "RELIGION"=>$Lang['StudentRegistry']['Religion']);
$Lang['StudentRegistry']['StudentGrouping1'] = "学生分类比较 1";
$Lang['StudentRegistry']['StudentGrouping2'] = "学生分类比较 2";
$Lang['StudentRegistry']['NA'] = "无";
$Lang['StudentRegistry']['NewStatusApplyDateError'] = "<font color=red>新申请日期必须等于或比之前的申请日期迟</font>";
$Lang['StudentRegistry']['TransferredStudents'] = "插班生";

# Student Registry (Access Rights - Group Settings)
$Lang['StudentRegistry']['GroupNameDuplicateWarning'] = "*小组名称重复或空白";

# Student Registry Statistic
$Lang['StudentRegistry']['Options'] = "选项";
$Lang['StudentRegistry']['ReportType'] = "报告类型";
$Lang['StudentRegistry']['ParentType'] = "家长类型";
$Lang['StudentRegistry']['Father'] = "父亲";
$Lang['StudentRegistry']['Mother'] = "母亲";
$Lang['StudentRegistry']['Guardian'] = "监护人";
$Lang['StudentRegistry']['ProfessionStat'] = "职业性质统计";
$Lang['StudentRegistry']['TitleStat'] = "职称统计";
$Lang['StudentRegistry']['GenerateReport'] = "制作报告";
$Lang['StudentRegistry']['ParentJobNatureStat'] = "家长职业性质统计";
$Lang['StudentRegistry']['ParentJobTitleStat'] = "家长职称统计";
$Lang['StudentRegistry']['JobNatureGroup'] = "职业性质分类";
$Lang['StudentRegistry']['JobTitleGroup'] = "职称分类";
$Lang['StudentRegistry']['NoOfParent'] = "人数";
$Lang['StudentRegistry']['ShowStatOption'] = "显示统计选项";
$Lang['StudentRegistry']['HideStatOption'] = "隐藏统计选项";

$Lang['StudentRegistry']['PrintReport']['ParentProfession'] = "家长职业性质";
$Lang['StudentRegistry']['PrintReport']['ParentJobTitle'] = "家长职称";
$Lang['StudentRegistry']['PrintReport']['And'] = "及";
$Lang['StudentRegistry']['PrintReport']['Statistics'] = "统计";

# Student Registry Report
$Lang['StudentRegistry']['ShowReportOption'] = "显示报告选项";
$Lang['StudentRegistry']['HideReportOption'] = "隐藏报告选项";
$Lang['StudentRegistry']['EntireClassList'] = "全班学生列表";
$Lang['StudentRegistry']['PersonalReport'] = "个人报告";
$Lang['StudentRegistry']['BlankForm'] = "空白表格";
$Lang['StudentRegistry']['Student'] = "学生";
$Lang['StudentRegistry']['InfoShown'] = "显示资料";
$Lang['StudentRegistry']['StudentBasicInfo_s'] = "学生基本资料";
$Lang['StudentRegistry']['StudentAdvInfo_s'] = "学生进阶资料";
$Lang['StudentRegistry']['FatherInfo_s'] = "父亲资料";
$Lang['StudentRegistry']['MotherInfo_s'] = "母亲资料";
$Lang['StudentRegistry']['GuardianInfo_s'] = "监护人资料";
$Lang['StudentRegistry']['CtrlSelectAll'] = "可按Ctrl键选择多项";
$Lang['StudentRegistry']['StudentRegistryForm'] = "学籍资料表格";
$Lang['StudentRegistry']['SelectDateWithinSameAcademicYear'] = "日期选项须于同一学年内";

# Student Registry Report - Student Registry Information
$Lang['StudentRegistry']['InfoReportTitle']['Stud_Adv']   = array("学号", "中文姓名", "英文姓名", "性別", "出生日期", "出生地点", "国籍", "祖籍", "种族", "身份证明文件编号", "报生纸号码", "宗教", "家庭用语", "缴费类别", "住家电话", "电子邮件", "住宿情况", "居住地址 – 门牌号码", "居住地址 – 路名", "居住地址 – 花园", "居住地址 – 市镇区", "居住地址 – 州属", "居住地址 – 邮区编号", "入学日期", "目前就读之兄姐 ", "小学就读学校");
$Lang['StudentRegistry']['InfoReportTitle']['Stud_Basic'] = array("学号", "中文姓名", "英文姓名", "性別", "国籍", "住家电话");
$Lang['StudentRegistry']['InfoReportTitle']['FatherInfo'] = array("父亲中文姓名", "父亲英文姓名", "父亲职业", "父亲职业性质", "父亲职称", "父亲婚姻状况", "父亲教育程度", "父亲住家电话", "父亲流动电话", "父亲办公电话", "父亲通讯地址 – 门牌号码", "父亲通讯地址 – 路名", "父亲通讯地址 – 花园", "父亲通讯地址 – 市镇区", "父亲通讯地址 – 州属", "父亲通讯地址 – 邮区号码", "父亲邮区号码", "父亲电子邮件");
$Lang['StudentRegistry']['InfoReportTitle']['MotherInfo'] = array("母亲中文姓名", "母亲英文姓名", "母亲职业", "母亲职业性质", "母亲职称", "母亲婚姻状况", "母亲教育程度", "母亲住家电话", "母亲流动电话", "母亲办公电话", "母亲通讯地址 – 门牌号码", "母亲通讯地址 – 路名", "母亲通讯地址 – 花园", "母亲通讯地址 – 市镇区", "母亲通讯地址 – 州属", "母亲通讯地址 – 邮区号码", "母亲邮区号码", "母亲电子邮件");
$Lang['StudentRegistry']['InfoReportTitle']['GuardInfo']  = array("监护人中文姓名", "监护人英文姓名", "监护人性別", "监护人与学生关系", "监护人与学生关系(其他)", "监护人与学生同住", "监护人职业", "监护人职业性质", "监护人职称", "监护人教育程度", "监护人住家电话", "监护人流动电话", "监护人办公电话", "监护人通讯地址 – 门牌号码", "监护人通讯地址 – 路名", "监护人通讯地址 – 花园", "监护人通讯地址 – 市镇区", "监护人通讯地址 – 州属", "监护人通讯地址 – 邮区号码", "监护人邮区号码", "监护人电子邮件");

# Student Registry Report - Student Registry Information - export
$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Adv'][0]   = array("Student No.", "Chinese Name", "English Name", "Gender", "Date of Birth", "Birth Place", "Nationality", "Ancestral Home", "Race", "ID Card No.", "Birth Cert. No.", "Religion", "Family Language", "Payment Type", "Phone No.(Home)", "Email", "Housing Status", "Home Address – (Door)", "Home Address – (Road)", "Home Address – (Garden)", "Home Address – (Town)", "Home Address – (State)", "Home Address – (Postal Code)", "Date of Entry", "Elder Brother/Sister in School", "Primary School");
$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Basic'][0] = array("Student No.", "Chinese Name", "English Name", "Gender", "Nationality", "Phone No.(Home)");
$Lang['StudentRegistry']['InfoReportTitle']['export']['FatherInfo'][0] = array("Father Chinese Name", "Father English Name", "Father Job Occupation", "Father Job Nature", "Father Job Title", "Father Marital Status", "Father Level of Education", "Father Phone No.(Home)", "Father Phone No.(Mobile)", "Father Phone No.(Office)", "Father Contact Address – (Door)", "Father Contact Address – (Road)", "Father Contact Address – (Garden)", "Father Contact Address – (Town)", "Father Contact Address – (State)", "Father Contact Address – (Postal Code)", "Father Postal Code", "Father Email");
$Lang['StudentRegistry']['InfoReportTitle']['export']['MotherInfo'][0] = array("Mother Chinese Name", "Mother English Name", "Mother Job Occupation", "Mother Job Nature", "Mother Job Title", "Mother Marital Status", "Mother Level of Education", "Mother Phone No.(Home)", "Mother Phone No.(Mobile)", "Mother Phone No.(Office)", "Mother Contact Address – (Door)", "Mother Contact Address – (Road)", "Mother Contact Address – (Garden)", "Mother Contact Address – (Town)", "Mother Contact Address – (State)", "Mother Contact Address – (Postal Code)", "Mother Postal Code", "Mother Email");
$Lang['StudentRegistry']['InfoReportTitle']['export']['GuardInfo'][0]  = array("Guardian Chinese Name", "Guardian English Name", "Guardian Gender", "Guardian Relationship", "Guardian Relationship (Other)", "Guardian Live with Student", "Guardian Job Occupation", "Guardian Job Nature", "Guardian Job Title", "Guardian Level of Education", "Guardian Phone No.(Home)", "Guardian Phone No.(Mobile)", "Guardian Phone No.(Office)", "Guardian Contact Address – (Door)", "Guardian Contact Address – (Road)", "Guardian Contact Address – (Garden)", "Guardian Contact Address – (Town)", "Guardian Contact Address – (State)", "Guardian Contact Address – (Postal Code)", "Guardian Postal Code", "Guardian Email");

$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Adv'][1]   = array("学号", "中文姓名", "英文姓名", "性別", "出生日期", "出生地点", "国籍", "祖籍", "种族", "身份证明文件编号", "报生纸号码", "宗教", "家庭用语", "缴费类别", "住家电话", "电子邮件", "住宿情况", "居住地址 – 门牌号码", "居住地址 – 路名", "居住地址 – 花园", "居住地址 – 市镇区", "居住地址 – 州属", "居住地址 – 邮区编号", "入学日期", "目前就读之兄姐 ", "小学就读学校");
$Lang['StudentRegistry']['InfoReportTitle']['export']['Stud_Basic'][1] = array("学号", "中文姓名", "英文姓名", "性別", "国籍", "住家电话");
$Lang['StudentRegistry']['InfoReportTitle']['export']['FatherInfo'][1] = array("父亲中文姓名", "父亲英文姓名", "父亲职业", "父亲职业性质", "父亲职称", "父亲婚姻状况", "父亲教育程度", "父亲住家电话", "父亲流动电话", "父亲办公电话", "父亲通讯地址 – 门牌号码", "父亲通讯地址 – 路名", "父亲通讯地址 – 花园", "父亲通讯地址 – 市镇区", "父亲通讯地址 – 州属", "父亲通讯地址 – 邮区号码", "父亲邮区号码", "父亲电子邮件");
$Lang['StudentRegistry']['InfoReportTitle']['export']['MotherInfo'][1] = array("母亲中文姓名", "母亲英文姓名", "母亲职业", "母亲职业性质", "母亲职称", "母亲婚姻状况", "母亲教育程度", "母亲住家电话", "母亲流动电话", "母亲办公电话", "母亲通讯地址 – 门牌号码", "母亲通讯地址 – 路名", "母亲通讯地址 – 花园", "母亲通讯地址 – 市镇区", "母亲通讯地址 – 州属", "母亲通讯地址 – 邮区号码", "母亲邮区号码", "母亲电子邮件");
$Lang['StudentRegistry']['InfoReportTitle']['export']['GuardInfo'][1]  = array("监护人中文姓名", "监护人英文姓名", "监护人性別", "监护人与学生关系", "监护人与学生关系(其他)", "监护人与学生同住", "监护人职业", "监护人职业性质", "监护人职称", "监护人教育程度", "监护人住家电话", "监护人流动电话", "监护人办公电话", "监护人通讯地址 – 门牌号码", "监护人通讯地址 – 路名", "监护人通讯地址 – 花园", "监护人通讯地址 – 市镇区", "监护人通讯地址 – 州属", "监护人通讯地址 – 邮区号码", "监护人邮区号码", "监护人电子邮件");


## eBooking Words
$Lang['eBooking']['Management']['FieldTitle']['Management'] = "管理";
$Lang['eBooking']['Management']['FieldTitle']['BookingRequest'] = "预订管理";
$Lang['eBooking']['Management']['FieldTitle']['Reserve'] = "优先预订";
$Lang['eBooking']['Management']['FieldTitle']['RoomBooking'] = "房间预订记录";
$Lang['eBooking']['Management']['FieldTitle']['ItemBooking'] = "物品预订记录";
$Lang['eBooking']['Management']['FieldTitle'][''] = "物品预订记录";
$Lang['eBooking']['Management']['FieldTitle']['MonthDisplay'] = array('','一月','二月','三月','四月','五月','六月','七月','八月','九月','十月','十一月','十二月');
$Lang['eBooking']['Management']['FieldTitle']['Dates'] = "日期";
$Lang['eBooking']['Management']['FieldTitle']['Time'] = "时间";
$Lang['eBooking']['Management']['FieldTitle']['RelatedBooking'] = "相关预订";
$Lang['eBooking']['Management']['FieldTitle']['Responsible'] = "负责人";
$Lang['eBooking']['Management']['FieldTitle']['BookedBy'] = "预订人";
$Lang['eBooking']['Management']['FieldTitle']['ApprovedBy'] = "批核人";

$Lang['eBooking']['Settings']['FieldTitle']['ListView'] = "列表";
$Lang['eBooking']['Settings']['FieldTitle']['WeekView'] = "星期";
$Lang['eBooking']['Settings']['FieldTitle']['Settings'] = "设定";
$Lang['eBooking']['Settings']['FieldTitle']['BookingPeriodsSettings'] = "预订时期设定";
$Lang['eBooking']['Settings']['FieldTitle']['GeneralPermission'] = "一般权限";
$Lang['eBooking']['Settings']['FieldTitle']['ManagementGroup'] = "管理小组";
$Lang['eBooking']['Settings']['FieldTitle']['FollowUpGroup'] = "跟进小组";
$Lang['eBooking']['Settings']['FieldTitle']['BasicBookingPeriod'] = "基本预订时期";
$Lang['eBooking']['Settings']['FieldTitle']['ItemOrRoomBookingPeriod'] = "场地 / 物品预订时期";
$Lang['eBooking']['Settings']['FieldTitle']['AvailableBookingPeriod'] = "可预订时期";
$Lang['eBooking']['Settings']['FieldTitle']['UserBookingRule'] = "用户预订权限";
$Lang['eBooking']['Settings']['FieldTitle']['ItemBookingRule'] = "物品预订权限";
$Lang['eBooking']['Settings']['FieldTitle']['BasicCondition'] = "基本条件";
$Lang['eBooking']['Settings']['FieldTitle']['SpecificCondition'] = "特定条件";
$Lang['eBooking']['Settings']['FieldTitle']['Category'] = "类别";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['New'] = "新增可预订时期";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Edit'] = "修改可预订时期";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Delete'] = "删除可预订时期";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['To'] = "至";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['DateRange'] = "日期范围";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Repeat'] = "重复频率";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Time'] = "时间";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Daily'] = "每天";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['CycleDay'] = "循环日";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Weekday'] = "周日";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['DoesNotRepeat'] = "不重复";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['WholeDay'] = "全日";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Duration'] = "持续时间";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['StartTime'] = "开始时间";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['EndTime'] = "结束时间";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['hours'] = "小时";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['mins'] = "分钟";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Period'] = "时段";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['SelectRepeatOption'] = "请选择";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['AvaliablePeriod'] = "有效";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['NonAvaliablePeriod'] = "无效";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['FacilityType'] = "设施类别";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Locations'] = "场地";
$Lang['eBooking']['Settings']['DefaultSettings']['BookingPeriod']['FieldTitle']['Facility']['Items'] = "物品";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InputDateRange'] = "请输入日期范围";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidDurationTime'] = "持续时间无效";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidSettingBookingPeriodTime'] = "预订时间无效";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['InvalidDateRange'] = "日期范围无效";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['DateRangebNotInSamePeriod'] = "所以选日期范围并不是在同一时区内";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['CycleDayIsNotSet'] = "* 并未设定循环日";
$Lang['eBooking']['Settings']['DefaultSettings']['JSWarning']['BookingPeriodIsExist'] = "系统已有相似的可预订时期。";
$Lang['eBooking']['Settings']['DefaultSettings']['JSMsg']['SureToDeleteBookingPerion'] = "是否确定要删除该可预订时期？";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['WeekdayNotInDateRange'] = "所选之星期并不在日期范围之内";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodCreatedSuccessfully'] = "1|=|新增可预订时期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodCreatedFailed'] = "0|=|新增可预订时期失败";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodEditSuccessfully'] = "1|=|修改可预订时期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodEditFailed'] = "0|=|修改可预订时期失败";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodDeletedSuccessfully'] = "1|=|删除可预订时期成功";
$Lang['eBooking']['Settings']['DefaultSettings']['ReturnMsg']['BookingPeriodDeletedFailed'] = "0|=|删除可预订时期失败";
$Lang['eBooking']['Settings']['ManagementGroup']['NewGroup'] = "新增小组";
$Lang['eBooking']['Settings']['ManagementGroup']['EditGroup'] = "编辑小组";
$Lang['eBooking']['Settings']['ManagementGroup']['MgmtGroupList'] = "管理小组列表";
$Lang['eBooking']['Settings']['ManagementGroup']['GroupName'] = "小组名称";
$Lang['eBooking']['Settings']['ManagementGroup']['Description'] = "描述";
$Lang['eBooking']['Settings']['ManagementGroup']['NumOfMembers'] = "成员人数";
$Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step1'] = "小组资料";
$Lang['eBooking']['Settings']['ManagementGroup']['NewGroupStepArr']['Step2'] = "选择小组成员";
$Lang['eBooking']['Settings']['ManagementGroup']['ChooseMember'] = "选择成员";
$Lang['eBooking']['Settings']['ManagementGroup']['FromClassOrGroup'] = "由班别或小组";
$Lang['eBooking']['Settings']['ManagementGroup']['SearchByLoginID'] = "搜寻登入编号";
$Lang['eBooking']['Settings']['ManagementGroup']['SelectedMembers'] = "已选择成员";
$Lang['eBooking']['Settings']['ManagementGroup']['EditGroup'] = "编辑小组";
$Lang['eBooking']['Settings']['ManagementGroup']['SelectMember'] = "选择成员";
$Lang['eBooking']['Settings']['ManagementGroup']['ViewMemberList'] = "检视成员列表";
$Lang['eBooking']['Settings']['ManagementGroup']['MemberList'] = "成员列表";
$Lang['eBooking']['Settings']['ManagementGroup']['AddMember'] = "增加会员";
$Lang['eBooking']['Settings']['ManagementGroup']['ChooseUser'] = "选择用户";
$Lang['eBooking']['Settings']['ManagementGroup']['SelectedUser'] = "已选择用户";
$Lang['eBooking']['Settings']['ManagementGroup']['JSWarningArr']['DeleteManagementGroup'] = "系统将会同时删除所有小组成员。你是否确定要删除已选择的小组？";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['SelectionContainsMember'] = "已选择的用户中包含些小组的成员";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameInUse'] = "已有其他小组使用此名称";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['GroupNameBlank'] = "小组名称空白";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['DataChangeAffect_eInventory'] = "资产管理行政系统及电子资源预订系统使用相同的类别设定，如你更新类别设定，更新的设定将会同时影响资产管理行政系统及电子资源预订系统。";
$Lang['eBooking']['Settings']['ManagementGroup']['WarningArr']['CannotDeleteCategoryWithItem'] = "如类别已设有物品，你将不能删除有关类别。";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupSuccess'] = "1|=|新增管理小组成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupFailed'] = "0|=|新增管理小组失败";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['EditMgmtGroupSuccess'] = "1|=|编辑管理小组成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['EditMgmtGroupFailed'] = "0|=|编辑管理小组失败";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupSuccess'] = "1|=|删除管理小组成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupFailed'] = "0|=|删除管理小组失败";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupMemberSuccess'] = "1|=|新增管理小组成员成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['AddMgmtGroupMemberFailed'] = "0|=|新增管理小组成员失败";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupMemberSuccess'] = "1|=|删除管理小组成员成功";
$Lang['eBooking']['Settings']['ManagementGroup']['ReturnMsg']['DeleteMgmtGroupMemberFailed'] = "0|=|删除管理小组成员失败";
$Lang['eBooking']['Settings']['Category']['AddedFrom_eBooking'] = "于电子资源预订系统新增";
$Lang['eBooking']['Settings']['Category']['AddedFrom_eInventory'] = "于资产管理行政系统新增";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['RuleTitle'] = "权限名称";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserType'] = "用户类别";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['TeacherAndStaff'] = "教师 / 非教学职务员工";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Students'] = "学生";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Parents'] = "家长";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Target'] = "目标";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['All'] = "所有";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['UserGroups'] = "-用户组别-";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingRules'] = "-预订权限-";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NeedApproval'] = "需要批核";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookingWithin'] = "只可预定 (指定时期内)";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['CanBookForOthers'] = "可帮其他用户预定";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['NoLimit'] = "没有限制";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][1] = "1 星期 (7日)";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][2] = "1 Month (30日)";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['BookWithinPeriod'][3] = "1 Year (365日)";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['Yes'] = "是";
$Lang['eBooking']['Settings']['BookingRule']['FieldTitle']['No'] = "否";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['InsertBookingRuleSuccessfully'] = "1|=|新增用户预订权限成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['InsertBookingRuleFailed'] = "0|=|新增用户预订权限失败";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditBookingRuleSuccessfully'] = "1|=|编辑用户预订权限成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditBookingRuleFailed'] = "0|=|编辑用户预订权限失败";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['DeleteBookingRuleSuccessfully'] = "1|=|删除用户预订权限成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['DeleteBookingRuleFailed'] = "0|=|删除用户预订权限失败";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditGeneralPermissionSuccessfully'] = "1|=|编辑一般权限成功";
$Lang['eBooking']['Settings']['BookingRule']['ReturnMsg']['EditGeneralPermissionFailed'] = "0|=|编辑一般权限失败";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['BookingRuleNameEmpty'] = "请输入户预预订权限名称";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['BookingRuleNameIsUsed'] = "户预预订权限名称已被使用";
$Lang['eBooking']['Settings']['BookingRule']['JSWarning']['PleaseSelectUserTarget'] = "请选择最少一顶目标";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Room'] = "房间";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Item'] = "物品";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['ItemList'] = "物品列表";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['NewItem'] = "新增物品";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Code'] = "编码";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['NameEn'] = "名称 (英文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['NameCh'] = "名称 (中文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Description'] = "描述";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['DescriptionEn'] = "描述 (英文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['DescriptionCh'] = "描述 (中文)";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Location'] = "位置";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingPermission'] = "预订权限";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['AllowBooking'] = "允许预订";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDayBeforehand'] = "需于多少日前预订";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['AvailableForBooking'] = "可预订";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Instruction'] = "指示";
$Lang['eBooking']['Settings']['GeneralPermission']['SelectCategory'] = "选择类别";
$Lang['eBooking']['Settings']['GeneralPermission']['SelectSubCategory'] = "选择子类别";
$Lang['eBooking']['Settings']['GeneralPermission']['AllSubCategory'] = "所有子类别";
$Lang['eBooking']['Settings']['GeneralPermission']['RemarksArr']['ZeroBookingDayBeforehandMeansNoLimit'] = "\"0\" 意指没有限制。";
$Lang['eBooking']['Settings']['GeneralPermission']['Set'] = "设定";
$Lang['eBooking']['Settings']['GeneralPermission']['Management'] = "管理";
$Lang['eBooking']['Settings']['GeneralPermission']['FollowUp'] = "跟进";
$Lang['eBooking']['Settings']['GeneralPermission']['DayBefore'] = "日前";
$Lang['eBooking']['Settings']['GeneralPermission']['Group(s)'] = "组";
$Lang['eBooking']['Settings']['GeneralPermission']['NoGroupSelected'] = "没有选择小组";
$Lang['eBooking']['Settings']['GeneralPermission']['Rule(s)'] = "项";
$Lang['eBooking']['Settings']['GeneralPermission']['NoRuleSelected'] = "没有选择权限";
$Lang['eBooking']['Settings']['GeneralPermission']['Available'] = "可预订";
$Lang['eBooking']['Settings']['GeneralPermission']['NotAvailable'] = "不可预订";
$Lang['eBooking']['Settings']['GeneralPermission']['BatchSelect'] = "批次选项";
$Lang['eBooking']['Settings']['GeneralPermission']['DeleteItem'] = "删除物品";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['AddItemSuccess'] = "1|=|新增物品成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['AddItemFailed'] = "0|=|新增物品失败";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['EditItemSuccess'] = "1|=|编辑物品成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['EditItemFailed'] = "0|=|编辑物品失败";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemSuccess'] = "1|=|删除物品成功";
$Lang['eBooking']['Settings']['GeneralPermission']['ReturnMsg']['DeleteItemFailed'] = "0|=|删除物品失败";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectCategory'] = "请选择类别。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectSubCategory'] = "请选择子类别。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectLocation'] = "请选择位置。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneMgmtGroup'] = "请最少选择一个管理小组。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAtLeastOneUserBookingRule'] = "请最少选择一个用户预订权限。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputItemCode'] = "请输入编码。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputNameEng'] = "请输入英文名称。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['InputNameChi'] = "请输入中文名称。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['CodeIsInUse'] = "已有其他物品使用此编码。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['CannotGoToEditMode'] = "因没有物品在此类别，所以未能进入编辑模式。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['DeleteItem'] = "你是否确定你要删除此物品？";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['SelectAllowBooking'] = "请选择一个选项。";
$Lang['eBooking']['Settings']['GeneralPermission']['JSWarningArr']['ItemDeleteFailed_AlreadyExistInPandingBooking'] = "删除物品失败 - 物品已被用户预定。";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Facilities'] = "设备";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['UserBookingRule'] = "用户预订权限";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['RoomBookingRule'] = "场地预订规则";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['Quantity'] = "数量";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['IncludedWhenBooking'] = "已包含在内";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['ShowForReference'] = "可选物品";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['PleaseSelectCategory'] = "请选择类别。";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['PleaseSelectLocation'] = "请选择位置。";
$Lang['eBooking']['Settings']['GeneralPermission']['FieldTitle']['BookingDay'] = array(
																						array(0,"0日"),
																						array(1,"1日"),
																						array(2,"2日"),
																						array(3,"3日"),
																						array(4,"4日"),
																						array(5,"5日"),
																						array(6,"6日"),
																						array(7,"7日"),
);
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Remark'] = "备注";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RoomOrItem'] = "房间 / 物品";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Date(s)'] = "日期";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Time'] = "时间";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ResponsiblePerson'] = "负责人";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['BookedBy'] = "预订人";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status'] = "状况";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_WaitingForApproval'] = "等待批核";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Approved'] = "批核";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Rejected'] = "不批核";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_iCalTemporyRecord'] = "iCalendar暂存记录";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['Status_Tempory'] = "iCalendar暂存记录";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['DaysAgo'] = "日前";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['SingleBooking'] = "单次预订";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['RepeatedBooking'] = "重复预订";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllComingRequest'] = "所有将来预订";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['PastRequest'] = "已过预订";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllStatus'] = "所有状况";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['AllManagementGroup'] = "所有管理小组";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['System'] = "系统";
$Lang['eBooking']['Settings']['BookingRequest']['FieldTitle']['ReservedByAdmin'] = "<font color='red'>*</font> - 由eBooking Admin预订";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedSuccessfully'] = "1|=|已成功批核有关预订。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordApprovedFailed'] = "0|=|未能成功批核有关预订。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedSuccessfully'] = "1|=|已成功拒绝有关预订。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordRejectedFailed'] = "0|=|未能成功拒绝有关预订。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordDeleted'] = "1|=|已成功删除有关预订。";
$Lang['eBooking']['Settings']['BookingRequest']['ReturnMsg']['RecordDeletedFailed'] = "0|=|未能成功删除有关预订。";
$Lang['eBooking']['Settings']['BookingRequest']['JSWarning']['RequestRejected_TimeOverLapped'] = "预订未能批核 - 该时段已被预订。";
$Lang['eBooking']['Settings']['BookingRequest']['JSWarning']['RequestRejected_AlreadyBooked'] = "预订未能批核 - 物品/房间已被预订";
$Lang['eBooking']['Settings']['BookingRequest']['JSWarning']['DeleteRejected_AlreadyApproved'] = "预订未能删除 - 该预订已被批核";
$Lang['eBooking']['eService']['FieldTitle']['MyBookingRecord'] = "我的预订纪录";
$Lang['eBooking']['eService']['FieldTitle']['NewBooking'] = "新增预订";
$Lang['eBooking']['eService']['FieldTitle']['AllBooking'] = "所有预订";
$Lang['eBooking']['eService']['FieldTitle']['Item'] = "物品";
$Lang['eBooking']['eService']['FieldTitle']['Room'] = "房间";
$Lang['eBooking']['eService']['FieldTitle']['SelectStatus'] = "选择状况";
$Lang['eBooking']['eService']['FieldTitle']['PastRecord'] = "过去预订纪录";
$Lang['eBooking']['eService']['FieldTitle']['ComingBooking'] = "将来预订纪录";
$Lang['eBooking']['eService']['FieldTitle']['SelectedDate'] = "已选择日期";
$Lang['eBooking']['eService']['FieldTitle']['Date(s)'] = "日期";
$Lang['eBooking']['eService']['FieldTitle']['Time'] = "时间";
$Lang['eBooking']['eService']['FieldTitle']['SelectedTime'] = "已选择时间";
$Lang['eBooking']['eService']['FieldTitle']['AvailablePeriod'] = "可用时段";
$Lang['eBooking']['eService']['FieldTitle']['TimeSlotIsNotAvailablePeriod'] = "不是可用时段";
$Lang['eBooking']['eService']['FieldTitle']['SpecificTimeRange'] = "指定时间范围";
$Lang['eBooking']['eService']['FieldTitle']['ItemsIncluded'] = "已包括物品";
$Lang['eBooking']['eService']['FieldTitle']['YouMayAlsoLikeToBook'] = "你也可预订以下物品";
$Lang['eBooking']['eService']['FieldTitle']['SelectMore'] = "更多选择";
$Lang['eBooking']['eService']['FieldTitle']['CancelAllBookingsIfRejectedByAdmin'] = "如管理员拒绝任何一件物品的预订，所有预订纪录将自动取消。";
$Lang['eBooking']['eService']['FieldTitle']['CancelDayBookingIfOneItemIsNA'] = "如任何一件物品未能预订，当天的预订将自动取消。";
$Lang['eBooking']['eService']['FieldTitle']['ResponsiblePerson'] = "负责人";
$Lang['eBooking']['eService']['FieldTitle']['BookedBy'] = "已预订";
$Lang['eBooking']['eService']['FieldTitle']['DoNotHaveItemBookingRight'] = "你没有此物品的预订权限。";
$Lang['eBooking']['eService']['FieldTitle']['ShowDetails'] = "显示详细资料";
$Lang['eBooking']['eService']['FieldTitle']['Period'] = "时段";
$Lang['eBooking']['eService']['FieldTitle']['Status_WaitingForApproval'] = "等待批核";
$Lang['eBooking']['eService']['FieldTitle']['Status_Approved'] = "已批核";
$Lang['eBooking']['eService']['FieldTitle']['Status_Rejected'] = "已拒绝";
$Lang['eBooking']['eService']['FieldTitle']['ExistingBookingRecord'] = "现有预订纪录";
$Lang['eBooking']['eService']['FieldTitle']['RequestsList'] = "预订列表";
$Lang['eBooking']['eService']['FieldTitle']['Day'] = "日";
$Lang['eBooking']['eService']['FieldTitle']['Week'] = "周";
$Lang['eBooking']['eService']['FieldTitle']['FacilityType'] = "预订类别";
$Lang['eBooking']['eService']['FieldTitle']['StartTime'] = "开始时间";
$Lang['eBooking']['eService']['FieldTitle']['EndTime'] = "结束时间";
$Lang['eBooking']['eService']['FieldTitle']['SelectDateOfBooking'] = "选择要预订的日期";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectTimeSlot'] = "请选择时段。";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectDate'] = "请选择日期。";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectItem'] = "请选择物品";
$Lang['eBooking']['eService']['jsWarningArr']['PleaseSelectRoom'] = "请选择房间";
$Lang['eBooking']['eService']['jsWarningArr']['GoBackViewModeAlert'] = "如现在转回检视模式，所有刚更改的资料将会失去。你是否确定要转回检视模式？";
$Lang['eBooking']['eService']['JSWarningArr']['InvalidDurationTime'] = "持续时间无效";
$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotEdit'] = "预订不能修改 - 预订已被批核。";
$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotCancel'] = "预订不能取消 - 预订等待批核中。";
$Lang['eBooking']['eService']['JSWarningArr']['BookingRequestCannotDelete'] = "预订不能删除 - 预订已被批核。";
$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectOneRequestOnly'] = "只可选择一个预订记录。";
$Lang['eBooking']['eService']['JSWarningArr']['PleaseSelectAtLeaseOneRequest'] = "请选择其中一个预订记录。";
$Lang['eBooking']['eService']['JSWarningArr']['PleaseInputCalendarEventTitle'] = "请输入事件名称";
$Lang['eBooking']['eService']['jsMsg']['ConfirmRemoveDates'] = "已选择的日期会被移除,继续?";
$Lang['eBooking']['eService']['WarningArr']['NoTimetableSettings'] = "所选择的日期未有设定时间表。";
$Lang['eBooking']['eService']['WarningArr']['NotimeSlotInTheTimetable'] = "所选择日期的时间表未有设定时段。";
$Lang['eBooking']['eService']['WarningArr']['SelectedDatesInDifferentTimezone'] = "所选择日期在不同的时区。";
$Lang['eBooking']['eService']['ReturnMsg']['NewBookingSuccess'] = "1|=|新增预订成功";
$Lang['eBooking']['eService']['ReturnMsg']['NewBookingFailed'] = "0|=|新增预订失败";
$Lang['eBooking']['eService']['ReturnMsg']['CancelBookingSuccess'] = "1|=|取消预订成功";
$Lang['eBooking']['eService']['ReturnMsg']['CancelBookingFailed'] = "0|=|取消预订失败";
$Lang['eBooking']['eService']['StepArr']['NewBooking'][0] = "选择日期";
$Lang['eBooking']['eService']['StepArr']['NewBooking'][1] = "选择时段";
$Lang['eBooking']['eService']['StepArr']['NewBooking'][2] = "选择物品";
$Lang['eBooking']['eService']['Cycle'] = "循环日";
$Lang['eBooking']['eService']['CycleDay'] = "循环日";
$Lang['eBooking']['eService']['Select'] = "选择";
$Lang['eBooking']['eService']['Cancel'] = "取消";
$Lang['eBooking']['eService']['Edit'] = "修改";
$Lang['eBooking']['eService']['Ref'] = "参考";
$Lang['eBooking']['eService']['Available'] = "可用";
$Lang['eBooking']['eService']['NotAvailable'] = "不可用";
$Lang['eBooking']['eService']['ViewAvaliablePeriod'] = "检视可用时段";
$Lang['eBooking']['eService']['Search'] = "搜寻";
$Lang['eBooking']['eService']['SelectCategorie(s)'] = "选择子类别";
$Lang['eBooking']['eService']['hrs'] = "小时";
$Lang['eBooking']['eService']['mins'] = "分钟";
$Lang['eBooking']['eService']['CheckAvailable'] = "检查";
$Lang['eBooking']['eService']['FieldTitle']['CreateCalendarEvent'] = "在\"我的行事历\"中新增事件";
$Lang['eBooking']['eService']['FieldTitle']['CalendarEventTitle'] = "事件名称";
$Lang['eBooking']['eService']['FieldTitle']['ReservedByAdmin'] = "<font color='red'>*</font> - 由eBooking Admin预订";
$Lang['eBooking']['Mail']['FieldTitle']['DearSirOrMadam'] = "Dear Sir / Madam";
$Lang['eBooking']['Mail']['FieldTitle']['BookingDate'] = "预订日期";
$Lang['eBooking']['Mail']['FieldTitle']['StartTime'] = "开始时间";
$Lang['eBooking']['Mail']['FieldTitle']['EndTime'] = "结束时间";
$Lang['eBooking']['Mail']['FieldTitle']['RoomsResult'] = "房间结果";
$Lang['eBooking']['Mail']['FieldTitle']['RoomsResult'] = "物品结果";

$Lang['eBooking']['iCal']['FieldTitle']['SelectFromEbooking'] = "由eBooking选择";
$Lang['eBooking']['iCal']['FieldTitle']['PleaseSelectLocation'] = " -- 选择地点 -- ";
$Lang['eBooking']['iCal']['FieldTitle']['Approved'] = "已批核";
$Lang['eBooking']['iCal']['FieldTitle']['Pending'] = "等待批核";
$Lang['eBooking']['iCal']['FieldTitle']['Rejected'] = "已拒绝";
$Lang['eBooking']['iCal']['FieldTitle']['SomeApproved'] = "部份已批核";
$Lang['eBooking']['iCal']['FieldTitle']['SomePending'] = "部份等待批核";
$Lang['eBooking']['iCal']['FieldTitle']['SomeRejected'] = "部份已拒绝";
$Lang['eBooking']['iCal']['FieldTitle']['AllApproved'] = "全部已批核";
$Lang['eBooking']['iCal']['FieldTitle']['AllPending'] = "全部等待批核";
$Lang['eBooking']['iCal']['FieldTitle']['AllRejected'] = "全部已拒绝";
# Payment Notice Wordings
$Lang['eNotice']['FieldTitle']['NoOfStudentPaidSuccessfully'] = "已成功缴费人数";
$Lang['eNotice']['FieldTitle']['NoOfStudentNeedToPaid'] = "需要缴费人数";
$Lang['eNotice']['FieldTitle']['NoOfItemPaidSuccessfully'] = "已成功缴费项目";
$Lang['eNotice']['FieldTitle']['NoOfItemNeedToPaid'] = "需要缴费项目";
$Lang['eNotice']['ExportTitle']['NoOfStudentPaidSuccessfully'] = "No. of students paid successfully";
$Lang['eNotice']['ExportTitle']['NoOfStudentNeedToPaid'] = "No. of students need to paid";
$Lang['eNotice']['ExportTitle']['NoOfItemPaidSuccessfully'] = "No. of items paid successfully";
$Lang['eNotice']['ExportTitle']['NoOfItemNeedToPaid'] = "No. of items need to paid";
$Lang['eNotice']['FieldTitle']['DebitMethod']['Title'] = "缴费方式";
$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitDirectly'] = "呈交时自动扣除所选数额";
$Lang['eNotice']['FieldTitle']['DebitMethod']['DebitLLater'] = "由校方处理 / 学生拍咭缴付";
$Lang['eNotice']['PaymentTitle'] = "费目名称";
$Lang['eNotice']['PaymentCategory'] = "缴费类别";
$Lang['eNotice']['Amount'] = "金额";
$Lang['eNotice']['Description'] = "选项";
$Lang['eNotice']['Pay'] = "缴付";
$Lang['eNotice']['NotPaying'] = "不缴付";
$Lang['eNotice']['AccountBalance'] = "帐户结余";

## Module License
$Lang['ModuleLicense']['ModuleCode'] = '模组码';
$Lang['ModuleLicense']['Quota'] = '配额';
$Lang['ModuleLicense']['Description'] = '说明';
$Lang['ModuleLicense']['ModuleCreateDate'] = '模组创建日';
$Lang['ModuleLicense']['AssignedQuota'] = '已分配配额';
$Lang['ModuleLicense']['AvailableQuota'] = '可用配额';
$Lang['ModuleLicense']['LicensedStudents'] = '已发牌学生';
$Lang['ModuleLicense']['Title'] = '标题';
$Lang['ModuleLicense']['LicenseAuthoirzation'] = '牌照授权';
$Lang['ModuleLicense']['Confirm'] = '确定';
$Lang['ModuleLicense']['Close'] = '关闭';
$Lang['ModuleLicense']['StudentSelected'] = '已选学生';
$Lang['ModuleLicense']['RemainingQuota'] = '余额';
$Lang['ModuleLicense']['Success'] = '成功';
$Lang['ModuleLicense']['Fail'] = '失败';
$Lang['ModuleLicense']['Total'] = '总共';
$Lang['ModuleLicense']['LastAdded'] = '最后新增';
$Lang['ModuleLicense']['ClassNumber'] = '班号';
$Lang['ModuleLicense']['ClassName'] = '班名';
$Lang['ModuleLicense']['StudentName'] = '学生姓名';
$Lang['ModuleLicense']['ConfirmRemove'] = "你是否确定要取消已选择学生的授权？";
$Lang['ModuleLicense']['DelInstruction'] = "注：只有过往48小时内获授权的用户可以被取消授权。";
$Lang['ModuleLicense']['CancelLicense'] = '取消牌照';

$Lang['ModuleLicense']['NoMoreAvailableQuota'] = "尚余配额满";///"No more available quota.";
$Lang['ModuleLicense']['ConfirmAssignStudent'] = "你是否确定要授权已选择的学生？";
$Lang['ModuleLicense']['ConfirmRemoveAssignStudent'] = "你是否确定要取消已选择学生的授权？";
$Lang['ModuleLicense']['Err_NoStudentsSelected'] = "请选择最少一名学生。";
$Lang['ModuleLicense']['Err_ExistInvalidQuota'] = "<font color='red'>配额由于未知的修改无效</font>";

# School Settings > Class
$Lang['SysMgr']['FormClassMapping']['Import'] = "汇入";
$Lang['SysMgr']['FormClassMapping']['Export'] = "汇出";
$Lang['SysMgr']['FormClassMapping']['ExportClassStudent'] = "汇出班学生";
$Lang['SysMgr']['FormClassMapping']['ExportClassTeacher'] = "汇出班主任";
$Lang['SysMgr']['FormClassMapping']['CopyFromOtherAcademicYear'] = "从其他年度复制";
$Lang['SysMgr']['FormClassMapping']['CopyClass'] = "复制班别";
$Lang['SysMgr']['FormClassMapping']['WebSAMS_IsBlank'] = "WebSAMS代号必须输入";
$Lang['SysMgr']['FormClassMapping']['WebSAMS_Duplicate'] = "WebSAMS代号不能重复";
$Lang['formClassMapping']['ImportExport_Optional']['EN'] = "[Optional]";
$Lang['formClassMapping']['ImportExport_Reference']['EN'] = "[Ref]";
$Lang['formClassMapping']['ImportExport_Optional']['B5'] = "[非必须的]";
$Lang['formClassMapping']['ImportExport_Reference']['B5'] = "[参考用途]";
$Lang['formClassMapping']['ImportTeacher_Column'][] = "<font color='red'>*</font>老师内联网帐号";
$Lang['formClassMapping']['ImportTeacher_Column'][] = "<font color='red'>^</font>老师姓名";
$Lang['formClassMapping']['ImportTeacher_Column'][] = "<font color='red'>*</font>班别(英文)";
$Lang['formClassMapping']['ImportStudent_Column'][] = "<font color='red'>*</font>学生内联网帐号";
$Lang['formClassMapping']['ImportStudent_Column'][] = "<font color='red'>^</font>学生姓名";
$Lang['formClassMapping']['ImportStudent_Column'][] = "<font color='red'>*</font>班别(英文)";
$Lang['formClassMapping']['ImportStudent_Column'][] = "班号";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>级别";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>级别 WebSAMS 代号";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>班别(英文)";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>班别(中文)";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>班别 WebSAMS 代号";
$Lang['formClassMapping']['ImportFormClass_Column'][] = "<font color='red'>*</font>组别代号";
$Lang['formClassMapping']['ExportClassTeacher_Column'][0][] = "Teacher Login ID";
$Lang['formClassMapping']['ExportClassTeacher_Column'][0][] = "Teacher Name ".$Lang['formClassMapping']['ImportExport_Reference']['EN'];
$Lang['formClassMapping']['ExportClassTeacher_Column'][0][] = "Class Name (EN)";
$Lang['formClassMapping']['ExportClassTeacher_Column'][1][] = "(老师内联网帐号)";
$Lang['formClassMapping']['ExportClassTeacher_Column'][1][] = "(老师姓名) ".$Lang['formClassMapping']['ImportExport_Reference']['B5'];
$Lang['formClassMapping']['ExportClassTeacher_Column'][1][] = "(班别(英文))";
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Student Login ID";
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Student Name ".$Lang['formClassMapping']['ImportExport_Reference']['EN'];
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Class Name (EN)";
$Lang['formClassMapping']['ExportClassStudent_Column'][0][] = "Class Number ".$Lang['formClassMapping']['ImportExport_Optional']['EN'];
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(学生内联网帐号)";
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(学生姓名) ".$Lang['formClassMapping']['ImportExport_Reference']['B5'];
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(班别(英文))";
$Lang['formClassMapping']['ExportClassStudent_Column'][1][] = "(班号) ".$Lang['formClassMapping']['ImportExport_Optional']['B5'];
$Lang['formClassMapping']['Reference'] = "附有「<span class='tabletextrequire'>^</span>」的项目参考用途";
$Lang['SysMgr']['FormClassMapping']['ClassCopySuccessfully'] = "1|=|班别复制成功.";

$eLib['SystemMsg']['AccessDenied'] = "访问被拒绝";

$Lang['IP_FILE']['ModuleCode'] = "模块代码";
$Lang['IP_FILE']['Description'] = "描述";
$Lang['IP_FILE']['InputDate'] = "输入日期";
$Lang['IP_FILE']['ModifiedDate'] = "更新日期";
$Lang['IP_FILE']['MaximumStorageSize'] = "最大存储大小";
$Lang['IP_FILE']['PleaseEnterANumber'] = "请输入一个数字";
$Lang['IP_FILE']['IPortalFileSettings'] = " iPortal文件设定";

$Lang['IntranetModule']['Using'] = "已使用";
$Lang['itextbook']['itextbook'] = "电子課本";
####################### This should be placed at the bottom
?>