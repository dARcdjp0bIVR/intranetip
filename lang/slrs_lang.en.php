<?php
// using:
/*
 * Remarks:
 * 1) Arrange the lang variables in alphabetical order
 * 2) Keep the same line break between en and b5 lang files (so that same lang variable is in the same line in different lang files)
 * 3) Use single quote for the array keys and double quote for the lang value
 */

// A
$Lang['SLRS']['AvailablePeriods'] = "Substitution period";
$Lang["SLRS"]["AvailablePeriodsTo"] = " to ";

// B
$Lang['SLRS']['BasicSettings'] = "Basic Settings";
$Lang['SLRS']['BtnPrint'] = "Print";
$Lang['SLRS']['BtnExport'] = "Export";

// C
$Lang["SLRS"]["cancelBtn"] = "Cancel";
$Lang["SLRS"]["confirmBtn"] = "Confirm";
$Lang["SLRS"]["changeTo"] = "Change to";

$Lang['SLRS']['CancelLesson'] = "Cancel Lesson";
$Lang['SLRS']['CancelLessonDes']['CancelDate'] = "Date";
$Lang['SLRS']['CancelLessonDes']['DateTimeInput'] = "Created at";
$Lang['SLRS']['CancelLessonDes']['Delete'] = "Delete";
$Lang['SLRS']['CancelLessonDes']['LessonInfo'] = "Cancel Lesson";
$Lang['SLRS']['CancelLessonDes']['Remark'] = "Remark";
$Lang['SLRS']['CancelLessonDes']['Target'] = "Target";
$Lang['SLRS']['CancelLessonDes']['TeacherName'] = "Name";
$Lang['SLRS']['CancelLessonDes']['Timeslot'] = "Timeslot";

$Lang['SLRS']['CancelLessonDes']['msg']["noTimeslotRecord"] = "Did not have lesson to cancel.";
$Lang['SLRS']['CancelLessonDes']['msg']["alreadyExchange"] = "This lesson is already exchanged.";
$Lang['SLRS']['CancelLessonDes']['msg']["alreadyCancel"] = "This lesson is already cancelled";
$Lang['SLRS']['CancelLessonDes']['msg']["alreadyArrange"] = "This lesson have substitution arrangement.";

$Lang['SLRS']['CycleWeekMaximunLessons'] = "Teaching limit in cycle/week";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["0"] = "unlimited";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["1"] = "10 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["2"] = "11 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["3"] = "12 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["4"] = "13 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["5"] = "14 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["6"] = "15 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["7"] = "16 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["8"] = "17 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["9"] = "18 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["10"] = "19 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["11"] = "20 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["12"] = "21 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["13"] = "22 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["14"] = "23 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["15"] = "24 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["16"] = "25 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["17"] = "26 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["18"] = "27 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["19"] = "28 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["20"] = "29 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["21"] = "30 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["22"] = "31 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["23"] = "32 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["24"] = "33 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["25"] = "34 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["26"] = "35 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["27"] = "36 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["28"] = "37 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["29"] = "38 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["30"] = "39 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["31"] = "40 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["32"] = "41 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["33"] = "42 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["34"] = "43 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["35"] = "44 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["36"] = "45 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["37"] = "46 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["38"] = "47 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["39"] = "48 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["40"] = "49 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsDes']["41"] = "50 lessons";

$Lang['SLRS']['CycleWeekMaximunLessonsVal']["0"] = "unlimited";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["1"] = "10 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["2"] = "11 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["3"] = "12 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["4"] = "13 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["5"] = "14 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["6"] = "15 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["7"] = "16 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["8"] = "17 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["9"] = "18 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["10"] = "19 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["11"] = "20 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["12"] = "21 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["13"] = "22 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["14"] = "23 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["15"] = "24 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["16"] = "25 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["17"] = "26 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["18"] = "27 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["19"] = "28 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["20"] = "29 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["21"] = "30 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["22"] = "31 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["23"] = "32 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["24"] = "33 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["25"] = "34 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["26"] = "35 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["27"] = "36 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["28"] = "37 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["29"] = "38 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["30"] = "39 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["31"] = "40 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["32"] = "41 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["33"] = "42 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["34"] = "43 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["35"] = "44 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["36"] = "45 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["37"] = "46 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["38"] = "47 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["39"] = "48 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["40"] = "49 lessons";
$Lang['SLRS']['CycleWeekMaximunLessonsVal']["41"] = "50 lessons";

$Lang['SLRS']['ClosedLocation'] = "Settings of Location Closed";
$Lang['SLRS']['ClosedLocationTH']["th_Location"] = "Location";
$Lang['SLRS']['ClosedLocationTH']["th_StartDate"] = "Start Date";
$Lang['SLRS']['ClosedLocationTH']["th_EndDate"] = "End Date";
$Lang['SLRS']['ClosedLocationTH']["th_Reasons"] = "Reason";
$Lang['SLRS']['ClosedLocationTH']["please-select"] = "Please Select";

$Lang["SLRS"]["ClosedLocationErr"]["Location"] = "Please select";
$Lang["SLRS"]["ClosedLocationErr"]["StartDate"] = "Please select";
$Lang["SLRS"]["ClosedLocationErr"]["EndDate"] = "Please select";
$Lang["SLRS"]["ClosedLocationErr"]["Reasons"] = "Please select";

// D
$Lang['SLRS']['DailyMaximunLessons'] = "Limit of teaching loading per day";
$Lang['SLRS']['DailyMaximunLessonsDes']["0"] = "unlimited";
$Lang['SLRS']['DailyMaximunLessonsDes']["1"] = "3 lessons";
$Lang['SLRS']['DailyMaximunLessonsDes']["2"] = "4 lessons";
$Lang['SLRS']['DailyMaximunLessonsDes']["3"] = "5 lessons";
$Lang['SLRS']['DailyMaximunLessonsDes']["4"] = "6 lessons";
$Lang['SLRS']['DailyMaximunLessonsDes']["5"] = "7 lessons";
$Lang['SLRS']['DailyMaximunLessonsDes']["6"] = "8 lessons";
$Lang['SLRS']['DailyMaximunLessonsDes']["7"] = "9 lessons";
$Lang['SLRS']['DailyMaximunLessonsDes']["8"] = "10 lessons";

$Lang['SLRS']['DailyMaximunLessonsVal']["0"] = "999";
$Lang['SLRS']['DailyMaximunLessonsVal']["1"] = "3";
$Lang['SLRS']['DailyMaximunLessonsVal']["2"] = "4";
$Lang['SLRS']['DailyMaximunLessonsVal']["3"] = "5";
$Lang['SLRS']['DailyMaximunLessonsVal']["4"] = "6";
$Lang['SLRS']['DailyMaximunLessonsVal']["5"] = "7";
$Lang['SLRS']['DailyMaximunLessonsVal']["6"] = "8";
$Lang['SLRS']['DailyMaximunLessonsVal']["7"] = "9";
$Lang['SLRS']['DailyMaximunLessonsVal']["8"] = "10";

$Lang['SLRS']['DailyMaximunSubstitution'] = "Substitution limit per day";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["0"] = "unlimited";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["1"] = "1 lessons";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["2"] = "2 lessons";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["3"] = "3 lessons";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["4"] = "4 lessons";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["5"] = "5 lessons";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["6"] = "6 lessons";
$Lang['SLRS']['DailyMaximunSubstitutionDes']["7"] = "7 lessons";

$Lang['SLRS']['DailyMaximunSubstitutionVal']["0"] = "999";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["1"] = "1";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["2"] = "2";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["3"] = "3";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["4"] = "4";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["5"] = "5";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["6"] = "6";
$Lang['SLRS']['DailyMaximunSubstitutionVal']["7"] = "7";

$Lang['SLRS']['DailyTeacherSubstitutionReport']['assignedTeacher'] = "Assigned Teacher";
$Lang['SLRS']['DailyTeacherSubstitutionReport']["target"] = "Target";
$Lang['SLRS']['DailyTeacherSubstitutionReport']["teacher"] = "Teacher";

$Lang['SLRS']['Display'] = "Display";

// E
$Lang['SLRS']['ExternalTeacher'] = "Supply Teacher";
$Lang['SLRS']['ExternalTeacherDes']["Teacher"] = "Supply teacher";
$Lang['SLRS']['ExternalTeacherDes']["StartDate"] = "Start date";
$Lang['SLRS']['ExternalTeacherDes']["EndDate"] = "End date";
$Lang['SLRS']['ExternalTeacherDes']["ToTeacher"] = "Substitute for";
$Lang['SLRS']['ExternalTeacherDes']["ModifiedDate"] = "Last update";
$Lang['SLRS']['ExternalTeacherDes']["Remark"] = "Remarks";
$Lang['SLRS']['ExchangeLocation'] = "Changed Location";

// F
$Lang['SLRS']['FirstLessonSubstituion'] = "Substitution happens in the first lesson";
$Lang['SLRS']['FirstLessonSubstituionDes']["0"] = "Yes";
$Lang['SLRS']['FirstLessonSubstituionDes']["1"] = "No";

$Lang['SLRS']['FormSixTeacherSubstition'] = "Will F6 teachers available for substitution after mock exam?";
$Lang['SLRS']['FormSixTeacherSubstitionDes']["0"] = "yes";
$Lang['SLRS']['FormSixTeacherSubstitionDes']["1"] = "no";

$Lang['SLRS']['ForcedExchangeMsg'] = "Please click the another subject group as below to forced Exchanged with same lesson";
$Lang['SLRS']['ForcedExchangeMsg'] = "Lesson as below can be combined exchange";
$Lang['SLRS']['ForcedExchange'] = "Combined Exchange for Another Subject Group at Same Timeslot";
$Lang['SLRS']['ForcedExchangePrompt'] = "After confirmed, combined exchange record will be disallowed to update.";
$Lang['SLRS']['ForcedExchangeMaster'] = "Combined Exchange Record";
$Lang['SLRS']['ForcedExchangeSlave'] = "Combined Exchange Record (Slave)";
$Lang['SLRS']['ForcedExchangeSlaveAlert'] = "Combined Exchange Record did not allow editing";

// G


// H
$Lang['SLRS']['HolidaySettings'] = "Leave Types & Sub. Balance Relationship";

// I
$Lang['SLRS']['InputValidation']["AdjustedType"] = "Adjustment option";
$Lang['SLRS']['InputValidation']["AdjustedValue"] = "Adjustment amount";
// J


// K


// L
$Lang['SLRS']['LessonRearrangement'] = "Lesson Re-arrangement";
$Lang["SLRS"]["LessonRemarks"] = "Remarks";
$Lang['SLRS']['LessonRemarksForOriginalLessonMarks'] = "Remarks<br>[<span class='lesson_remarks_txt'>%s</span>]";
$Lang['SLRS']['LessonRemarksForOriginalLesson'] = "Remarks<br>[<span class='lesson_remarks_txt'>Original lesson</span>]";
$Lang['SLRS']['LocationNameDisplay'] = "Venue display";
$Lang['SLRS']['LocationNameDisplayDes']["0"] = "Name ";
$Lang['SLRS']['LocationNameDisplayDes']["1"] = "Code";
$Lang['SLRS']['LastModified'] = "Last Modified";

$Lang['SLRS']['LessonExchange'] = "Lesson Exchange";
$Lang['SLRS']['LessonExchangeTitle'] = "Arrange Lesson Exchange";
$Lang['SLRS']['LessonExchangeDes']['DateTimeModified'] = "Last Modified";
$Lang['SLRS']['LessonExchangeDes']['ExchangeLessonTeacher'] = "Swapped with";
$Lang['SLRS']['LessonExchangeDes']['ExchangeLesson'] = "Lessons involved";
$Lang['SLRS']['LessonExchangeDes']['TimeslotIsNotEmpty'] = "This timeslot is not empty";
$Lang['SLRS']['LessonExchangeDes']['CannotExchangeMyLesson'] = "Can not exchange same lesson";

$Lang['SLRS']['LessonExchangeDes']['OriginalLesson'] = "Original lesson";
$Lang['SLRS']['LessonExchangeDes']['ExchangedLesson'] = "New lesson";
$Lang['SLRS']['LessonExchangeDes']['Date'] = 'Date';
$Lang['SLRS']['LessonExchangeDes']['Teacher'] = "Teacher";
$Lang['SLRS']['LessonExchangeDes']['Classroom'] = "Lesson";
$Lang['SLRS']['LessonExchangeDes']['Location'] = "Venue";
$Lang['SLRS']['LessonExchangeDes']['ToBeChanged'] = "Change";
$Lang['SLRS']['LessonExchangeDes']['Cancel'] = "Cancel";
$Lang['SLRS']['LessonExchangeDes']['AnotherClass'] = "Lesson Exchange for Another Subject Group at Same Timeslot";

// M
$Lang['SLRS']['Menu']['Management'] = 'Management';
$Lang['SLRS']['Menu']['Reports'] = 'Reports';
$Lang['SLRS']['Menu']['Settings'] = 'Settings';
// N
$Lang['SLRS']['NoSubstitutionCalBalance'] = "When selected 'No Substutition' for Leave, affect substitution balance";
$Lang['SLRS']['NoSubstitutionCalBalanceDes'][0] = "Enable";
$Lang['SLRS']['NoSubstitutionCalBalanceDes'][1] = "Disable";
$Lang['SLRS']['NotAvailableTeacher'] = "Unable Substitution";
$Lang['SLRS']['NotAvailableTeacherRecord'] = "Unable Substitution Record";
$Lang['SLRS']['NotAvailableTeachers'] = "Teacher On Leave";
$Lang['SLRS']['NotAvailableTeachersTH']["th_UserName"] = "Teacher Name";
$Lang['SLRS']['NotAvailableTeachersTH']["th_Teacher"] = "Teacher";
$Lang['SLRS']['NotAvailableTeachersTH']["th_StartDate"] = "Start Date";
$Lang['SLRS']['NotAvailableTeachersTH']["th_EndDate"] = "End Date";
$Lang['SLRS']['NotAvailableTeachersTH']["th_Reasons"] = "Reason";
$Lang['SLRS']['NotAvailableTeachersForm']["please-select"] = "Please Select";

$Lang["SLRS"]["NotAvailableTeachersErr"]["teacher"] = "Please Select";
$Lang["SLRS"]["NotAvailableTeachersErr"]["StartDate"] = "Please Select";
$Lang["SLRS"]["NotAvailableTeachersErr"]["EndDate"] = "Please Select";
$Lang["SLRS"]["NotAvailableTeachersErr"]["Reasons"] = "Please input Reasons";
$Lang['SLRS']['NotAvailableTeachersErr']["NotAvailable"] = "This Teacher is not available now. Please select another.";
// O
$Lang['SLRS']['OfficialLeavelAO'] = "Official leave";
$Lang['SLRS']['OfficialLeavelWS'] = "Leave for seminar/conference";
$Lang['SLRS']['OfficialLeavelSLP'] = "Sick leave";
$Lang['SLRS']['OfficialLeavelPL'] = "Annual leave";
$Lang['SLRS']['OfficialLeavelSTL'] = "Study leave";
$Lang['SLRS']['OfficialLeavelOptDes']["0"] = "affect substitution balance of the teacher";
$Lang['SLRS']['OfficialLeavelOptDes']["1"] = "no affection on substitution balance of the teacher";
$Lang['SLRS']['Or'] = "或";

$Lang['SLRS']['OfficialLeavelAOVal'] = "AO";
$Lang['SLRS']['OfficialLeavelWSVal'] = "WS";
$Lang['SLRS']['OfficialLeavelSLPVal'] = "SLP";
$Lang['SLRS']['OfficialLeavelPLVal'] = "PL";
$Lang['SLRS']['OfficialLeavelSTLVal'] = "STL";

$Lang["SLRS"]["OutOfMaxLessonLimitation"] = "Beyond Limit of teaching loading per day";
$Lang["SLRS"]["OutOfMaxSubstitutionLimitation"] = "Beyond substitution limit per day";

// P
$Lang['SLRS']['PresetSubstitutionLocation'] = "Preset Substitution Location";
$Lang['SLRS']['PresetSubstitutionLocationTH']["SubstitutionLimitPerDay"] = "Substitution limit per day for Preset Location";
$Lang['SLRS']['PresetSubstitutionLocationTH']["PresetLocation"] = "Preset Substitution Location";

// Q


// R
$Lang['SLRS']['ReportGeneration']="Generate";
$Lang['SLRS']['ReportDate'] = 'Date';
$Lang['SLRS']['ReportWarning'] = '* Mandatory field(s)';
$Lang['SLRS']['ReportDateWarning'] = 'Please fill in the date';
$Lang['SLRS']['ReportDailyTeacherSubstituion']="Daily Substitution Report";
$Lang['SLRS']['ReportLessonExchangeSummary']="Lesson Exchange Summary";
$Lang['SLRS']['ReportSummaryofClassroomChangeforLessons']="Summary of Classroom Change";
$Lang['SLRS']['ReportYearlyStatisticsofTeacherSubstitution']="Yearly Substitution Statistics";
$Lang['SLRS']['ReportTeachersAbsenceSubstitutionSummary']="Summary of Substitution Related to Leaves";

$Lang['SLRS']['ReportPeriod']="Period";
$Lang['SLRS']['ReportAssignedTo']="Assigned To";
$Lang['SLRS']['ReportClass']="Class";
$Lang['SLRS']['ReportSubject']="Subject";
$Lang['SLRS']['ReportRoom']="Room";
$Lang['SLRS']['ReportPeriod']="Period";
$Lang['SLRS']['ReportTeacher']="Teacher";
$Lang['SLRS']['ReportRoom']="Room";
$Lang['SLRS']['ReportTo']="To";
$Lang['SLRS']['ReportAcademicYear']="School Year";
$Lang['SLRS']['ReportSemester']="Term";
$Lang['SLRS']['ReportTC']="TC";
// $Lang['SLRS']['ReportPeriodSickLeave']="No. of period Leave (A)";
$Lang['SLRS']['ReportPeriodSickLeave']="No. of period Leave (excluding Leave Types is no affection on balance) (A) ";
$Lang['SLRS']['ReportPeriodSubstitition']="No. of period Substitution (B)";
$Lang['SLRS']['ReportCarriedPreviousYear']="Carried from previous year (C)";
$Lang['SLRS']['ReportBalance']="Balance (B-A+C) No Substitution";
$Lang['SLRS']['ReportSubTC']="Sub. TC";
$Lang['SLRS']['ReportVolunteered']="Volunteered?";
$Lang['SLRS']['ReportExemption']="Exemption (write the teacher code)";
$Lang['SLRS']['ReportOverall']="Overall (Substitution)";

$Lang['SLRS']['ReportSubstitutionFor']="Substitute for";
$Lang['SLRS']['ReportDay']="Day";
$Lang['SLRS']['ReportDate']="Date";
$Lang['SLRS']['ReportLargeText']="Increase font size";
$Lang['SLRS']['ReportSmallText']="Decrease font size";
$Lang['SLRS']['ReportPrint']="Print";
$Lang['SLRS']['ReportProcessing']="Processing ...";

$Lang['SLRS']['ReportSendRecord']="Send Time";
$Lang['SLRS']['ReportSendBy']="Sent By";
$Lang['SLRS']['ReportRecipient']="Sent to";
$Lang['SLRS']['ReportRecipientStatus']="Status";
$Lang['SLRS']['ReportGenerationPushMessageBtn'] = "Send Notification";
$Lang['SLRS']['ReportGenerationRePushMessageBtn'] = "Re-send Notification";
$Lang['SLRS']['ReportGenerationSending'] = "Sending";
$Lang['SLRS']['ReportGenerationSuccess'] = "A notification has been sent";
$Lang['SLRS']['ReportGenerationFailed'] = "Failed. Please try again";
$Lang['SLRS']['ReportGenerationNoRecord'] = "No record";
$Lang['SLRS']['ReportGenerationSLRSNoticationEN'] = "今日代課安排 \n Today's subsitution arrangement";
$Lang['SLRS']['ReportNotificationLog'] = "過往通知紀錄";
// $Lang['SLRS']['ReportGenerationSLRSNoticationTo'] = "致 To: ";
// $Lang['SLRS']['ReportGenerationSLRSNoticationDate'] = "日期  Date:";
$Lang['SLRS']['ReportGenerationSLRSNoticationText'] = "致: __Name__
日期: __DateChi__
請留意 以下的代課安排:
		
To: __Name__
Date: __DateEng__
Please be informed that you are assigned for below subsitution(s):
";
// $Lang['SLRS']['ReportGenerationSLRSNoticationThanks'] = "Thank you for your cooperation~";
$Lang['SLRS']['ReportLoading'] = "更新中...";

// S
$Lang['SLRS']['SLRS'] = "Substituttion & Lesson Re-arrangement System (SLRS)";
$Lang['SLRS']['SystemProperties'] = "System Properties";
$Lang['SLRS']['SystemChari'] = "System Properties";
$Lang['SLRS']['SpecialClassroomLocation'] = "If the original venue is a special room";
$Lang['SLRS']['SpecialClassroomLocationDes']["0"] = "no need to relocate";
$Lang['SLRS']['SpecialClassroomLocationDes']["1"] = "relocate to library";
$Lang['SLRS']['SpecialClassroomLocationDes']["2"] = "relocate to classroom";

$Lang['SLRS']['StudentDailyReport'] = "Daily Substitution Report for Student";

$Lang['SLRS']['SubstituteBalance'] = "Substitution Balance";

$Lang['SLRS']['SubstitutionAfterSickLeave'] = "Can the teacher who comes back from sick leave be arranged for substitution?";
$Lang['SLRS']['SubstitutionAfterSickLeaveDes']["0"] = "yes ";
$Lang['SLRS']['SubstitutionAfterSickLeaveDes']["1"] = "no";

$Lang['SLRS']['SubjectDisplay'] = "Subject display";
$Lang['SLRS']['SubjectDisplayDes']["0"] = "Name";
$Lang['SLRS']['SubjectDisplayDes']["1"] = "Abbreviation ";
$Lang['SLRS']['SubjectDisplayDes']["2"] = "Short-form ";
$Lang['SLRS']['SubjectDisplayDes']["3"] = "WebSams code";

$Lang['SLRS']['SubjectGroupDisplay'] = "Subject group display";
$Lang['SLRS']['SubjectGroupDisplayDes']["0"] = "Name ";
$Lang['SLRS']['SubjectGroupDisplayDes']["1"] = "Code";

$Lang['SLRS']['SubstitutionBalanceDisplay'] = "Substitution balance display";
$Lang['SLRS']['SubstitutionBalanceDisplayDes']["0"] = "credit for substitute, debit for absentee";
$Lang['SLRS']['SubstitutionBalanceDisplayDes']["1"] = "debit for substitute, credit for absentee";

$Lang['SLRS']['SubstituteTeacherSettings'] = "Teacher Available";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['UserName'] = "Name ";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['Balance'] = "Substitution Balance ";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['Priority'] = "Substitution sequence";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['JoinDate'] = "Input date";
$Lang['SLRS']['SubstituteTeacherSettingsDes']['EditSelectionOrder'] = "Edit substitution sequence";


$Lang['SLRS']['SubstitutePrioritySettings'] = "Substitution Criteria";


$Lang['SLRS']['SubstitutePrioritySettingsDes']['FirstPriority'] = "Top criteria";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SecondPriority'] = "Second priority";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['Adjustment'] = "(re-order)";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['UpperLimitmation'] = "Upper limit";

$Lang['SLRS']['SubstitutePrioritySettingsDes']['ExternalTeachers'] = "Supply teacher";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['InternalTeachers'] = "Teacher";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['DailyTotalLessons'] = "Day load (including substitution)";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['DailySubstituteLessons'] = "Day substitution (less first)";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SubstituteBalance'] = "Substitution Balance";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SubstituteFilterOrder'] = "Substitution sequence";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['ClassTeacher'] = "Class teacher of the class";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SubjectTeacher'] = "Subject teacher of the class";
$Lang['SLRS']['SubstitutePrioritySettingsDes']['SameSubjectTeacher'] = "Teaching same subject";

$Lang['SLRS']['SelectSubstituteTeachers'] = "Please select subsitute";
$Lang['SLRS']['SelectedSubstituteTeachers'] = "Subsitute assigned";
$Lang['SLRS']['SearchByLoginID'] = "Search by Login Name";

$Lang['SLRS']['SubstitutionBalance'] = "Substitution Balance";
$Lang['SLRS']['SubstitutionBalanceDes']['UserName'] = 'Teacher';
$Lang['SLRS']['SubstitutionBalanceDes']['YearlyAdd'] = "Accumulated by substitution";
$Lang['SLRS']['SubstitutionBalanceDes']['YearlyReduce'] = "Absence lessons";
$Lang['SLRS']['SubstitutionBalanceDes']['Balance'] = 'Current balance';
$Lang['SLRS']['SubstitutionBalanceDes']['LastModified'] = 'Last adjustment';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedType'] = 'Adjustment type';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedValue'] = 'Value';
$Lang['SLRS']['SubstitutionBalanceDes']['Reason'] = 'Reason';
$Lang['SLRS']['SubstitutionBalanceDes']['BalanceReview'] = 'Teacher (balance preview)';
$Lang['SLRS']['SubstitutionBalanceDes']['BalanceTeacher'] = 'Teacher';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedLogReset'] = "Reset Substitution Balance record(s) arrangement";
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedLogResetAppend'] = 'After updated, APPEND all Substitution record(s) and calculate Balance Adjustment of Current School Year';
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustedLogResetIgnore'] = 'After updated, IGNORE all Substitution record(s) of Current School Year';

$Lang['SLRS']['SubstitutionBalanceAdjTyp']['AddBalance'] = 'Increase';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['ReduceBalance'] = 'Reduce';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetBalance'] = 'Reset Adjustment Value';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceInfo'] = 'Reset Starting date and Balance Value for this school year';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceInfoReason'] = 'Updated Starting date: __RESET_DATE__';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalance'] = 'Select the starting date for the current school year, adjust the start balance value (only the balance calculation)';
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] = "<ul>";
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] .= "<li><u>This settings will delete history log of balance adjustment.</u></li>";
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] .= "<li>E.G. Starting day of this school year is <u>September 1st</u>, now reset to <u>October 1st</u> and set balance value to '0'.<br>System will reset and calculate the balance records <u>after Octcber 1st with <b>current settings</b></u>. (<i>Ignore the records between September 1st and 30th</i>)</li>";
$Lang['SLRS']['SubstitutionBalanceAdjTyp']['SetYearBalanceEG'] .= "</ul>";
$Lang['SLRS']['SubstitutionBalanceDes']['AdjustmentRecord'] = 'Adjustment log';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['Date'] = 'Date';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['BeforeBalance'] = 'Balance before';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['Adjustment'] = 'Adjust';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['AfterBalance'] = 'Balance after';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['AdjustedType'] = 'Adjustment type';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['Reason'] = 'Reason';
$Lang['SLRS']['SubstitutionBalanceRecordDes']['InputUser'] = 'Record by';

$Lang['SLRS']['SubstitutionArrangement'] = 'Substitution for Leaves';
$Lang['SLRS']['SubstitutionArrangementCreate'] = 'Add Substitution record';
$Lang['SLRS']['SubstitutionArrangementUpdate'] = 'Update Substitution record';
$Lang['SLRS']['SubstitutionArrangementDelete'] = 'Delete Substitution record';
$Lang['SLRS']['SubstitutionArrangementHasLeaveRecord'] = "Have leave record";
$Lang['SLRS']['SubstitutionArrangementHasRecord'] = "Have substitution for leave record";
// $Lang['SLRS']['SubstitutionArrangementHasAlreadySRRecord'] = "Have leave record and already substituted";
$Lang['SLRS']['SubstitutionArrangementHasAlreadySRRecord'] = "Have leave record";
$Lang['SLRS']['SubstitutionArrangementHasExchangeRecord'] = "Have lesson exchange record";
$Lang['SLRS']['SubstitutionArrangementFromExchangeRecord'] = "From lesson exchange";
$Lang['SLRS']['SubstitutionArrangementHasExchange'] = "Already lesson exchange";
$Lang['SLRS']['SubstitutionArrangementAlreadySubstitution'] = "Already substituted";
$Lang['SLRS']['SubstitutionArrangementHasRecordDisableExchange'] = "No lesson";

$Lang['SLRS']['TempSubstitutionArrangementCreate'] = 'Add Ad-hoc Substitution record';
$Lang['SLRS']['TempSubstitutionArrangementUpdate'] = 'Update Ad-hoc Substitution record';
$Lang['SLRS']['TempSubstitutionArrangementDelete'] = 'Delete Ad-hoc Substitution record';

$Lang['SLRS']['SubstitutionArrangementSubTC'] = ' (Sub. TC)';

$Lang['SLRS']['SubstitutionArrangementDes']['Teacher']= 'Teacher';
$Lang['SLRS']['SubstitutionArrangementDes']['Lesson']= 'Lesson';
$Lang['SLRS']['SubstitutionArrangementDes']['Action']= 'Arrange Subsitution';
$Lang['SLRS']['SubstitutionArrangementDes']['DateTimeModified']= 'Last Modified';
$Lang['SLRS']['SubstitutionArrangementDes']['DeleteSubstition']= 'Delete';
$Lang['SLRS']['SubstitutionArrangementDes']['AddLeave']="New";
$Lang['SLRS']['SubstitutionArrangementDes']['Duration']="Session";
$Lang['SLRS']['SubstitutionArrangementDes']['AffectedLesson']= 'No. of Lesson(s) Involved';
$Lang['SLRS']['SubstitutionArrangementDes']['InitialDate']= 'Start Value of this school year';
$Lang['SLRS']['SubstitutionArrangementDes']['isExchangeRecord']= 'Already exchanged this Lesson';

$Lang['SLRS']['SubstitutionArrangementDes']['Date']='Date';
$Lang['SLRS']['SubstitutionArrangementDes']['LeaveTeacher']="Teacher";
$Lang['SLRS']['SubstitutionArrangementDes']['Reason']="Reason";
$Lang['SLRS']['SubstitutionArrangementDes']['Substitution']="Substitution";
$Lang['SLRS']['SubstitutionArrangementDes']['TeacherMandatory']="Please select Teacher";
$Lang['SLRS']['SubstitutionArrangementDes']['ReasonMandatory']="Please select Reason";
$Lang['SLRS']['SubstitutionArrangementDes']['SessionMandatory']="Please select session";
$Lang['SLRS']['SubstitutionArrangementDes']['DurationMandatory']="Please select Duration";
$Lang['SLRS']['SubstitutionArrangementDes']['ArrangementMandatory']="Please select Arrangement";
$Lang['SLRS']['SubstitutionArrangementDes']['InitialDate']= 'Start date for this school year';

$Lang['SLRS']['SubstitutionArrangementDes']['Session']="Session";

$Lang['SLRS']['SubstitutionSessionDes']['0']="AM";
$Lang['SLRS']['SubstitutionSessionDes']['1']="PM";
$Lang['SLRS']['SubstitutionSessionDes']['2']="Whole day";
$Lang['SLRS']['SubstitutionSessionVal']['0']="am";
$Lang['SLRS']['SubstitutionSessionVal']['1']="pm";
$Lang['SLRS']['SubstitutionSessionVal']['2']="wd";

$Lang['SLRS']['SubstitutionActionDes']['0']="Arrange LATER";
$Lang['SLRS']['SubstitutionActionDes']['1']="Arrange NOW";

$Lang['SLRS']['SubstitutionActionVal']['0']="LATER";
$Lang['SLRS']['SubstitutionActionVal']['1']="NOW";

$Lang['SLRS']['SubstitutionArrangementDes']['Location']="Location";
$Lang['SLRS']['SubstitutionArrangementDes']['Balance']="Substitute Balance";
$Lang['SLRS']['SubstitutionArrangementDes']['DailyLesson']="Day load";
$Lang['SLRS']['SubstitutionArrangementDes']['TimeTable']="Timetable";
$Lang['SLRS']['SubstitutionArrangementDes']['Reserve']="available";
$Lang['SLRS']['SubstitutionArrangementDes']['View']="View";
$Lang['SLRS']['SubstitutionArrangementDes']['Arrange']="Arrange";
$Lang['SLRS']['SubstitutionArrangementDes']['ToBeChanged']="Change";
$Lang['SLRS']['SubstitutionArrangementDes']['Cancel']="Cancel";
$Lang['SLRS']['SubstitutionArrangementDes']['Timetable']="Timetable";
$Lang['SLRS']['SubstitutionArrangementDes']['Room']="Lesson";
$Lang['SLRS']['SubstitutionArrangementDes']['SubstitutedLesson']="Substitution";

$Lang['SLRS']['SubstitutionArrangementDes']['StrongSubstitutedTeacher']="Bold: supply teacher ";
$Lang['SLRS']['SubstitutionArrangementDes']['EmClassTeacher']="Italic: class teacher  ";
$Lang['SLRS']['SubstitutionArrangementDes']['More']="More ...";

$Lang['SLRS']['SubstitutionArrangementDes']['AMError']="cannot apply for AM nor full day";
$Lang['SLRS']['SubstitutionArrangementDes']['PMError']="cannot apply for PM nor full day";
$Lang['SLRS']['SubstitutionArrangementDes']['WDError']="cannot apply for full day";
$Lang['SLRS']['SubstitutionArrangementDes']['NoLesson']="there are no lessons within this timeslot";
$Lang['SLRS']['SubstitutionArrangementDes']['WholeDayError'] = "Already arranged substitution for Leave on this day";
$Lang['SLRS']['SubstitutionArrangementDes']['AMExistError'] = "Already arranged this AM duration";
$Lang['SLRS']['SubstitutionArrangementDes']['PMExistError'] = "Already arranged this PM duration";

$Lang['SLRS']['SubstitutionArrangementDes']['ExistSupplyTeacherSubstitution'] = "Arranged supply teacher already. Cannot arrnage another supply teacher.";
$Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitution'] = "Arranged as a substitute teacher, so can not add leave record and arrange substitution, please change the substitute teacher first.";
$Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitutionList'] = "Arranged as a substitute teacher.";
$Lang['SLRS']['SubstitutionArrangementDes']['ExistTeacherSubstitutionListArrangeAgain'] = "Please change the substitute teacher.";

$Lang['SLRS']['SubstitutionArrangementDes']['isVolunteer'] ='Volunteer';
$Lang['SLRS']['SubstitutionArrangementDes']['notArranged'] ='Not Arranged';
$Lang['SLRS']['SubstitutionArrangementDes']['ignoreThisRec'] ='No Substitution';
$Lang['SLRS']['SubstitutionArrangementDes']['noSubstitutedTeacher'] ='No Substitute Arrangement';
$Lang['SLRS']['SubstitutionArrangementDes']['defaultSubjectGroupNoSubstitution'] ='This Subject Group is preset to no substitute arrangement';
$Lang['SLRS']['SubstitutionArrangementDes']['subjectGroupNoSubstitution'] ='Allow Subject Group is preset to no substitute arrangement';
$Lang['SLRS']['SubstitutionArrangementDes']['InformationUpdated'] ='Information has been changed, please pay attention to the latest arrangements';
// T
$Lang['SLRS']['TemporarySubstitutionArrangement']="Ad-hoc Substitution";
$Lang['SLRS']['TempSubstitutionArrangement']['StrongSubstitutedTeacher']="Bold: supply teacher ";
$Lang['SLRS']['TempSubstitutionArrangement']['EmClassTeacher']="Italic: class teacher ";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Room']="Lesson";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Date']='Date';
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['LeaveTeacher']="Absentee";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Location']="Venue";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Balance']="Substitution balance";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['DailyLesson']="Day load";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['TimeTable']="Timetable";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Reserve']="available";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['ToBeChanged']="Change";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Cancel']="Cancel";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['View']="View";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Teacher'] = "Absentee";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['ToTeacher'] = "Substitute";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Action'] = "Arrange";
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['DateTimeModified']='Last modified';
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['Lesson']= 'Lessons';
$Lang['SLRS']['TemporarySubstitutionArrangementDes']['AddArrangment']= 'New arrangement';
// U
$Lang['Sys']['MsgUpdate'] = " record has been updated. ";
$Lang["SLRS"]["User_Suspended"] = "<span style='color:#f00'>Already Suspended</span>";
// V
$Lang['SLRS']['VoluntarySubstitute'] = "Volunteer indication";
$Lang['SLRS']['VoluntarySubstituteDes']["0"] = "Enable";
$Lang['SLRS']['VoluntarySubstituteDes']["1"] = "Disable";

$Lang['SLRS']['PrintDetailedOption']['Signature'] = "Signature";
$Lang['SLRS']['PrintDetailedOption']['Remark'] = "Remark";

// W


// X


// Y

// Z


?>