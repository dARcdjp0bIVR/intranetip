<?php
# using: 

# this file is for the template editor in Discipline 1.2 only!
$i_Discipline_System_Template_Variable['default_value'] = "N.A.";
$i_Discipline_System_Template_Variable['student_name'] = "Student Name";
$i_Discipline_System_Template_Variable['StudentNameChi'] = "Student Name(Chi)";
$i_Discipline_System_Template_Variable['StudentNameEng'] = "Student Name(Eng)";
$i_Discipline_System_Template_Variable['class_name'] = "Class Name";
$i_Discipline_System_Template_Variable['ClassNameChi'] = "Class Name(Chi)";
$i_Discipline_System_Template_Variable['ClassNameEng'] = "Class Name(Eng)";
$i_Discipline_System_Template_Variable['class_number'] = "Class Number";
$i_Discipline_System_Template_Variable['issue_date'] = "Issue Date";
// Added [2015-0106-1649-27066]
$i_Discipline_System_Template_Variable['IssueDateChi'] = "Issue Date(Chi)";
$i_Discipline_System_Template_Variable['additional_info'] = "Additional Info";
$i_Discipline_System_Template_Variable['school_year'] = "School Year";
$i_Discipline_System_Template_Variable['semester'] = "Semester";
$i_Discipline_System_Template_Variable['detention_reason'] = "Detention Reason";
$i_Discipline_System_Template_Variable['session_details'] = "Session Details";
$i_Discipline_System_Template_Variable['event_date'] = "Event Date";
$i_Discipline_System_Template_Variable['merit_record'] = "Merit Record";
$i_Discipline_System_Template_Variable['conduct_mark_increment'] = "Conduct Mark (Increment)";
$i_Discipline_System_Template_Variable['PIC'] = "PIC";
$i_Discipline_System_Template_Variable['award_reason'] = "Award Reason";
$i_Discipline_System_Template_Variable['remark'] = "Remark";
$i_Discipline_System_Template_Variable['event_date'] = "Event Date";
$i_Discipline_System_Template_Variable['demerit_record'] = "Demerit Record";
$i_Discipline_System_Template_Variable['content_type'] = "Category";
$i_Discipline_System_Template_Variable['warning_content'] = "Warning Letter (Records)";
$i_Discipline_System_Template_Variable['demerit_content'] = "Demerit Notification (Records)";
// Added: 2015-01-19
$i_Discipline_System_Template_Variable['demerit_recordChi'] = "Demerit Record(Chi)";
$i_Discipline_System_Template_Variable['demerit_recordEng'] = "Demerit Record(Eng)";
$i_Discipline_System_Template_Variable['conduct_mark (Decrement)'] = "Conduct Mark (Decrement)";
$i_Discipline_System_Template_Variable['punishment_reason'] = "Punishment Reason";
$i_Discipline_System_Template_Variable['current_conduct_mark'] = "Current Conduct Mark";
$i_Discipline_System_Template_Variable['warning_reminderpoint'] = "Warning Reminder Point";
$i_Discipline_System_Template_Variable['subscore_warning_reminderpoint'] = "Study Score Warning Reminder Point";
// Added: 2017-11-13
$i_Discipline_System_Template_Variable['IssueDateEng'] = "Issue Date(Eng)";

$i_Discipline_System_Template_Variable['study_score_increment'] = "Study Score (Increment)";
$i_Discipline_System_Template_Variable['attitude_score_increment'] = "Attitude Score (Increment)";
$i_Discipline_System_Template_Variable['activity_score_increment'] = "Activity Score (Increment)";
$i_Discipline_System_Template_Variable['study_mark (Decrement)'] = "Study Score (Decrement)";
$i_Discipline_System_Template_Variable['attitude_mark (Decrement)'] = "Attitude Score (Decrement)";
$i_Discipline_System_Template_Variable['activity_mark (Decrement)'] = "Activity Score (Decrement)";
$i_Discipline_System_Template_Variable['current_study_score'] = "Current Study Score";
$i_Discipline_System_Template_Variable['current_attitude_score'] = "Current Attitude Score";
$i_Discipline_System_Template_Variable['current_activity_score'] = "Current Activity Score";
$i_Discipline_System_Template_Variable['attitude_score_warning_reminderpoint'] = "Attitude Score Warning Reminder Point";
$i_Discipline_System_Template_Variable['actscore_warning_reminderpoint'] = "Activity Score Warning Reminder Point";

$i_Discipline_System_Template_Variable['goodconduct_reason'] = "Good Conduct Reason";
$i_Discipline_System_Template_Variable['misconduct_reason'] = "Misconduct Reason";

$i_Discipline_System_Template_Variable['AccuNumber'] = "Accumulated Good Conduct / Misconduct Times";

$i_Discipline_System_Template_Variable['PeriodDescription'] = "Period Description";
$i_Discipline_System_Template_Variable['GoodConductAwardRecord'] = "Table of Good Conduct & Award Records";
$i_Discipline_System_Template_Variable['AmountOfMerit'] = "Amount of Merit";
$i_Discipline_System_Template_Variable['AmountOfMinorMerit'] = "Amount of Minor Merit";
$i_Discipline_System_Template_Variable['AmountOfMajorMerit'] = "Amount of Major Merit";
$i_Discipline_System_Template_Variable['AmountOfGoodConduct'] = "Amout of Good Conduct";

$i_Discipline_System_Template_Variable['ParentName'] ="Parent Name"; 
?>
