<?php
if ($sys_custom['StemPL2']) {
	$Lang['StemPL2']['Or'] = 'or';
	$Lang['StemPL2']['Username'] = 'Username';
	$Lang['StemPL2']['Password'] = 'Password';
	$Lang['StemPL2']['Login'] = 'Login';
	$Lang['StemPL2']['Pin'] = 'PIN';
	$Lang['StemPL2']['PleaseEnterValidUsernameOrPassword'] = 'Please enter a valid username or password.';
	$Lang['StemPL2']['PleaseEnterValidPin'] = 'Please enter a valid PIN.';
}
?>