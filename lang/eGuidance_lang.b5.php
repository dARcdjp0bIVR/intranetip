<?
/*
* using by : 
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
* 1. Create a new $Lang variable in every single line (No empty line between $Lang variable)
* $Lang['Header']['Menu']['Home'] = "Home";
* $Lang['Header']['Menu']['eLearning'] = "eLearning";
* $Lang['Header']['Menu']['eAdmin'] = "eAdmin";
*
* 2. Use “\r\n” to represent a new line
* $Lang['EmailNotification']['eNotice']['Content'] = "A new electronic notice (__TITLE__) __TERM__ issued on __STARTDATE__.<br>\r\n Please kindly sign the notice on or before __ENDDATE__.<br>\r\n You may login __WEBSITE__, then go to eService > eNotice to view the notice.<br>\r\n";
*
* 3. Avoid using \’ or \”. Use single quotes to enclose the string and make use of the concatenation operator to include single quote in your $Lang.
* $Lang['ePost']['OptionalAttachVideoFiles'] = '(Optional) Attach ".FLV" ".MOV" or ".MP4" File';
*
* 4. This is UTF-8 File. Please edit and save as UTF8 中文字
*
* ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!! ATTENTION PLEASE!!!
*
*/

$Lang['eGuidance']['AddAttachment'] = "加入附件";

$Lang['eGuidance']['advanced_class']['AddLesson'] = "新增課堂";
$Lang['eGuidance']['advanced_class']['EditLesson'] = "編輯課堂";
$Lang['eGuidance']['advanced_class']['LessonList'] = "課堂列表";
$Lang['eGuidance']['advanced_class']['LessonManagement'] = "課堂管理";
$Lang['eGuidance']['advanced_class']['Remind']['ClickDateToAddNew'] = "請點選日期以新增課堂";
$Lang['eGuidance']['advanced_class']['TeacherComment'] = "老師課堂評語及結果";
$Lang['eGuidance']['advanced_class']['Warning']['DeleteStudentWithLesson'] = "當你更新此版面時,刪除的學生的課堂評語及結果將一併刪除,你是否確定更新?";
$Lang['eGuidance']['advanced_class']['Warning']['InputTeacherComment'] = "請輸入最少一項老師課堂評語及結果";

$Lang['eGuidance']['AllStudent'] = "所有學生";
$Lang['eGuidance']['AllClass'] = "所有班別";
$Lang['eGuidance']['AlreadyNotifyReferral'] = "已通知同事個案已被轉介";
$Lang['eGuidance']['AlreadyNotifyUpdate'] = "已通知同事檔案已更新";
$Lang['eGuidance']['Attachment'] = "附件";
$Lang['eGuidance']['Class'] = "班別";
$Lang['eGuidance']['Confidential'] = "機密資料";

$Lang['eGuidance']['contact']['AllContactCategory'] = "所有類別";
$Lang['eGuidance']['contact']['ContactCategory'] = "類別";
$Lang['eGuidance']['contact']['Details'] = "對話內容及結果";
$Lang['eGuidance']['contact']['Warning']['InputDetails'] = "請輸入對話內容及結果";
$Lang['eGuidance']['contact']['Warning']['SelectCategory'] = "請選擇類別";

$Lang['eGuidance']['ctmeeting']['Comments'] = "評語";
$Lang['eGuidance']['ctmeeting']['Export']['WebSAMSRegNo'] = "Ad No";
$Lang['eGuidance']['ctmeeting']['Export']['ClassName'] = "Class %s";
$Lang['eGuidance']['ctmeeting']['Export']['ClassNo'] = "No";
$Lang['eGuidance']['ctmeeting']['Export']['StudentName'] = "Name";
$Lang['eGuidance']['ctmeeting']['Export']['Gender'] = "Sex";
$Lang['eGuidance']['ctmeeting']['Export']['Followup'] = "%s Follow-up";
$Lang['eGuidance']['ctmeeting']['Followup'] = "跟進建議";
$Lang['eGuidance']['ctmeeting']['Import']['StudentName'] = "學生姓名";
$Lang['eGuidance']['ctmeeting']['Import']['WebSAMSRegNo'] = "WebSAMS 學生註冊編號";
$Lang['eGuidance']['ctmeeting']['ImportColumns'] = array (
"<font color = red>*</font>WebSAMS 學生註冊編號(\"#\" 號是WebSAMS 學生註冊編號格式的一部份)",
"學生姓名",
"<font color = red>@</font>第一次會議評語",
"<font color = red>^</font>第一次會議跟進建議",
"<font color = red>@</font>第二次會議評語",
"<font color = red>^</font>第二次會議跟進建議"
);
$Lang['eGuidance']['ctmeeting']['ImportError']['DuplicateRecord'] = "該學生紀錄已經存在";
$Lang['eGuidance']['ctmeeting']['ImportError']['StudentNotFound'] = "找不到該學生";
$Lang['eGuidance']['ctmeeting']['ImportError']['StudentNotRegisterInTheYear'] = "該學生在該學年還未註冊";
$Lang['eGuidance']['ctmeeting']['ImportError']['WebSAMSRegNoNotFound'] = "沒有設定 WebSAMS 學生註冊編號";
$Lang['eGuidance']['ctmeeting']['ImportRemarks'] = array (
"附有「<font color = red>*</font>」的項目必須填寫",
"<font color = red>@</font>欄位名稱請填上會議日期 (YYYY-MM-DD 或 D/M/YYYY 或 DD/MM/YYYY)",
"<font color = red>^</font>欄位名稱請填上會議日期 Follow-up (YYYY-MM-DD Follow-up 或 D/M/YYYY Follow-up 或 DD/MM/YYYY Follow-up)",
"如有多過兩次會議,請在緊接著第二次會議跟進建議的欄位加上每次會議的評語及跟進建議"
);
$Lang['eGuidance']['ctmeeting']['ImportReturnMsg']['DateNotExist'] = "0|=|會議日期超越所有學年範圍";
$Lang['eGuidance']['ctmeeting']['ImportTitle'] = "各級班主任會議匯入紀錄";
$Lang['eGuidance']['ctmeeting']['MeetingDate'] = "會議日期";
$Lang['eGuidance']['ctmeeting']['StudentName'] = "學生姓名";
$Lang['eGuidance']['ctmeeting']['StudentRegNo'] = "學生編號";

$Lang['eGuidance']['Date'] = "日期";
$Lang['eGuidance']['error']['ajax'] = "AJAX 錯誤!";
$Lang['eGuidance']['FollowupAdvice'] = "跟進建議";

$Lang['eGuidance']['guidance']['AllContactStage'] = "所有面談階段";
$Lang['eGuidance']['guidance']['CaseType'] = "個案類型";
$Lang['eGuidance']['guidance']['ContactContent'] = "面談內容";
$Lang['eGuidance']['guidance']['ContactDate'] = "接見日期";
$Lang['eGuidance']['guidance']['ContactFinding'] = "面談發現";
$Lang['eGuidance']['guidance']['ContactPlace'] = "地點";
$Lang['eGuidance']['guidance']['ContactPurpose'] = "面見目的";
$Lang['eGuidance']['guidance']['ContactStage'] = "面談階段";
$Lang['eGuidance']['guidance']['ContactTime'] = "時間";
$Lang['eGuidance']['guidance']['Result'] = "結果";
$Lang['eGuidance']['guidance']['Warning']['SelectCaseType'] = "請選擇個案類型";
$Lang['eGuidance']['guidance']['Warning']['InputCaseTypeName'] = "請輸入個案類型的名稱";

$Lang['eGuidance']['Import']['InvalidRecord'] = "錯誤紀錄數量";
$Lang['eGuidance']['Import']['Steps'][0] = "選擇CSV檔案";
$Lang['eGuidance']['Import']['Steps'][1] = "資料驗證";
$Lang['eGuidance']['Import']['Steps'][2] = "匯入結果";
$Lang['eGuidance']['Import']['Successful'] = "份資料匯入成功 ";		// need to leave a space or escape back slash character (\) which lower byte is 0x5C
$Lang['eGuidance']['Import']['TotalRecord'] = "總行數";
$Lang['eGuidance']['Import']['ValidRecord'] = "有效紀錄數量";
$Lang['eGuidance']['Item'] = "項目";

// parent
$Lang['eGuidance']['menu']['CaseFollow'] = "個案跟進";

// admin
/* !!! Keep these menu in physical order as it use in loop of access right settings: start */
$Lang['eGuidance']['menu']['main']['Management'] = "管理";
$Lang['eGuidance']['menu']['main']['Reports'] = "報告";
$Lang['eGuidance']['menu']['main']['Settings'] = "設定";
$Lang['eGuidance']['menu']['Management']['Contact'] = "接見/聯絡紀錄";
$Lang['eGuidance']['menu']['Management']['AdvancedClass'] = "奮進班";
$Lang['eGuidance']['menu']['Management']['Therapy'] = "輔導組治療";
$Lang['eGuidance']['menu']['Management']['Guidance'] = "輔導個案";
$Lang['eGuidance']['menu']['Management']['SEN'] = "SEN";
$Lang['eGuidance']['menu']['Management']['Suspend'] = "停課";
$Lang['eGuidance']['menu']['Management']['Transfer'] = "轉介";
$Lang['eGuidance']['menu']['Management']['Personal'] = "個案記錄表";
$Lang['eGuidance']['menu']['Management']['SelfImprove'] = "自省自強計劃";
$Lang['eGuidance']['menu']['Management']['ClassteacherMeeting'] = "各級班主任會議";
$Lang['eGuidance']['menu']['Reports']['StudentReport'] = "學生報告";
$Lang['eGuidance']['menu']['Reports']['TeacherReport'] = "老師報告";
$Lang['eGuidance']['menu']['Reports']['ClassteacherReport'] = "班主任報告";
$Lang['eGuidance']['menu']['Reports']['StudentProblemReport'] = "學生問題報告";
$Lang['eGuidance']['menu']['Settings']['PermissionSetting'] = "權限設定";
$Lang['eGuidance']['menu']['Settings']['FineTuneSetting'] = "SEN 設定";
$Lang['eGuidance']['menu']['Settings']['GeneralSetting'] = "系統設定";
$Lang['eGuidance']['menu']['Settings']['IPAccessRightSetting'] = "指定可存取電腦 IP";
/* !!! Keep the above menu in physical order as it use in loop of access right settings: end */

$Lang['eGuidance']['name'] = "輔導管理";

$Lang['eGuidance']['NotifyItem']['Contact'] = "接見/聯絡紀錄";
$Lang['eGuidance']['NotifyItem']['CTMeeting'] = "各級班主任會議紀錄";
$Lang['eGuidance']['NotifyItem']['Guidance'] = "輔導檔案";
$Lang['eGuidance']['NotifyItem']['Personal'] = "個案記錄表";
$Lang['eGuidance']['NotifyItem']['SelfImprove'] = "自省自強計劃";
$Lang['eGuidance']['NotifyItem']['SENCase'] = "SEN個案";
$Lang['eGuidance']['NotifyItem']['Suspend'] = "停課紀錄";
$Lang['eGuidance']['NotifyReferral'] = "通知同事個案已被轉介";
$Lang['eGuidance']['NotifyReferralContent'] = "[class]班[name]同學的個案已於[yy]年[mm]月[dd]日轉介至輔導組,同事可於eClass輔導管理系統查閱。(學校行政管理工具>學生管理>輔導管理>管理>轉介)";
$Lang['eGuidance']['NotifySubject']['Contact'] = "接見/聯絡紀錄更新通知";
$Lang['eGuidance']['NotifySubject']['CTMeeting'] = "各級班主任會議紀錄更新通知";
$Lang['eGuidance']['NotifySubject']['Guidance'] = "輔導檔案更新通知";
$Lang['eGuidance']['NotifySubject']['Personal'] = "個案記錄表更新通知";
$Lang['eGuidance']['NotifySubject']['Referral'] = "個案轉介通知";
$Lang['eGuidance']['NotifySubject']['SelfImprove'] = "自省自強計劃更新通知";
$Lang['eGuidance']['NotifySubject']['SENCase'] = "SEN個案更新通知";
$Lang['eGuidance']['NotifySubject']['Suspend'] = "停課紀錄更新通知";
$Lang['eGuidance']['NotifyUpdate'] = "通知同事檔案已更新";
$Lang['eGuidance']['NotifyUpdateContent'] = "[class]班[name]同學的[notifyItem]已於[yy]年[mm]月[dd]日更新,同事可於eClass輔導管理系統查閱。(學校行政管理工具>學生管理>輔導管理>管理>[module])";

$Lang['eGuidance']['personal']['BasicInfo'] = "基本資料";
$Lang['eGuidance']['personal']['BornInHongKong'] = "香港出生";
$Lang['eGuidance']['personal']['BornInOther'] = "來自(國家)";
$Lang['eGuidance']['personal']['BrotherAndSister'] = "兄弟姊妹人數";
$Lang['eGuidance']['personal']['ContactTel'] = "聯絡電話";
$Lang['eGuidance']['personal']['ElderBrother'] = "兄";
$Lang['eGuidance']['personal']['ElderSister'] = "姊";
$Lang['eGuidance']['personal']['FamilyStatus'] = "學生現時家庭關係狀況";
$Lang['eGuidance']['personal']['Father'] = "父親";
$Lang['eGuidance']['personal']['FatherWorkingStatus'] = "學生父親工作狀況";
$Lang['eGuidance']['personal']['FollowupStatus'] = "跟進情況";
$Lang['eGuidance']['personal']['Gender'] = "學生性別";
$Lang['eGuidance']['personal']['GenderFemale'] = "女";
$Lang['eGuidance']['personal']['GenderMale'] = "男";
$Lang['eGuidance']['personal']['Guidance'] = "輔導組";
$Lang['eGuidance']['personal']['GuidanceTeacher'] = "輔導老師";
$Lang['eGuidance']['personal']['MainProblem'] = "主要及其他問題 (可選多項)";
$Lang['eGuidance']['personal']['Mother'] = "母親";
$Lang['eGuidance']['personal']['MotherWorkingStatus'] = "學生母親工作狀況";
$Lang['eGuidance']['personal']['Occupatiion'] = "職業";
$Lang['eGuidance']['personal']['ParentStatus'] = "父母狀況";
$Lang['eGuidance']['personal']['People'] = "人";
$Lang['eGuidance']['personal']['PersonalRecord'] = "個案記錄表";
$Lang['eGuidance']['personal']['PersonLivedWithStudent'] = "與學生同住人士 (可選多項)";
$Lang['eGuidance']['personal']['PlaceOfBirth'] = "出生地點";
$Lang['eGuidance']['personal']['ReferralFromAndReason'] = "個案來自及轉介原因";
$Lang['eGuidance']['personal']['ReferralPerson'] = "轉介人";
$Lang['eGuidance']['personal']['ReferredToReason'] = "原因";
$Lang['eGuidance']['personal']['ReferredToSocialWorker'] = "已轉介給社工";
$Lang['eGuidance']['personal']['Student'] = "學生";
$Lang['eGuidance']['personal']['Total'] = "總共";
$Lang['eGuidance']['personal']['ViewPersonalRecord'] = "檢視個案記錄表";
$Lang['eGuidance']['personal']['YearInHongKong'] = "居港年期";
$Lang['eGuidance']['personal']['YoungerBrother'] = "弟";
$Lang['eGuidance']['personal']['YoungerSister'] = "妹";

$Lang['eGuidance']['PleaseSelectFiles'] = "請選擇檔案";
$Lang['eGuidance']['PushMessage']['SendToStaff'] = "推送到教職員";
$Lang['eGuidance']['PushMessage']['ViewStatus'] = "檢視推送訊息結果";
$Lang['eGuidance']['Remark'] = "備註";
$Lang['eGuidance']['RemoveAttachment'] = "移除附件";

$Lang['eGuidance']['report']['basicInformation'] = "基本資料";
$Lang['eGuidance']['report']['eGuidanceReport'] = "輔導管理報告";
$Lang['eGuidance']['report']['GenerateReport'] = "產生報告";
$Lang['eGuidance']['report']['NotToShowStudentWithoutRecord'] = "不顯示沒有紀錄的學生";
$Lang['eGuidance']['report']['NotShowSectionWithoutRecord'] = "不顯示沒有紀錄的部分";
$Lang['eGuidance']['report']['PrintTime'] = "列印時間";
$Lang['eGuidance']['report']['ReportOption'] = "報告選項";
$Lang['eGuidance']['report']['ReportType'] = "報告類型";
$Lang['eGuidance']['report']['SENCase']['Add'] = "加";
$Lang['eGuidance']['report']['SENCase']['ClassTeacher'] = "班主任";
$Lang['eGuidance']['report']['SENCase']['IsNeedAdjustmentNextYear'] = "來年是否需要以上的調適安排(請在適當的";
$Lang['eGuidance']['report']['SENCase']['Need'] = "需要";
$Lang['eGuidance']['report']['SENCase']['NotNeed'] = "不需要";
$Lang['eGuidance']['report']['SENCase']['Organizer'] = "統籌主任";
$Lang['eGuidance']['report']['SENCase']['ParentSignature'] = "家長簽署";
$Lang['eGuidance']['report']['SENCase']['SchoolSocialWorker'] = "學校社工";
$Lang['eGuidance']['report']['SENCase']['Signature'] = "負責人員簽署";
$Lang['eGuidance']['report']['SENCase']['SignatureDate'] = "日期";
$Lang['eGuidance']['report']['SENCase']['SubjectChi'] = "中文科";
$Lang['eGuidance']['report']['SENCase']['SubjectEng'] = "英文科";
$Lang['eGuidance']['report']['SENCase']['SubjectGes'] = "常識科";
$Lang['eGuidance']['report']['SENCase']['SubjectMat'] = "數學科";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherChi'] = "中文科任";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherEng'] = "英文科任";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherGes'] = "常識科任";
$Lang['eGuidance']['report']['SENCase']['SubjectTeacherMat'] = "數學科任";
$Lang['eGuidance']['report']['SENCase']['TeacherSuggestion'] = "老師建議(請於下學期末填寫)";
$Lang['eGuidance']['report']['SENStudentAnnualReport'] = "為有特殊教育需要學生的支援服務計劃年度匯報";
$Lang['eGuidance']['report']['StudentPivateInfo'] = "學生個人資料";
$Lang['eGuidance']['report']['StudentPivateInfoDetails'] = "學生編號(STRN), 性別, 出生日期";

$Lang['eGuidance']['ReturnMessage']['AddAndNotifySuccess'] = "1|=|紀錄已經新增並且通知同事";
$Lang['eGuidance']['ReturnMessage']['AddSuccessNotifyFail'] = "0|=|紀錄已經新增但不能通知同事";
$Lang['eGuidance']['ReturnMessage']['UpdateAndNotifySuccess'] = "1|=|紀錄已經更新並且通知同事";
$Lang['eGuidance']['ReturnMessage']['UpdateSuccessNotifyFail'] = "0|=|紀錄已經更新但不能通知同事";
$Lang['eGuidance']['SelectColleague'] = "選擇同事";
$Lang['eGuidance']['SelectDate'] = "選擇日期";

$Lang['eGuidance']['selfimprove']['AllResult'] = "所有結果";
$Lang['eGuidance']['selfimprove']['Result'] = "結果";
$Lang['eGuidance']['selfimproveResult']['Delay'] = "延期";
$Lang['eGuidance']['selfimproveResult']['Successful'] = "成功";
$Lang['eGuidance']['selfimproveResult']['Unsuccessful'] = "不成功";

$Lang['eGuidance']['sen']['AllCase'] = "所有個案";
$Lang['eGuidance']['sen']['Confirm'] = "已確診";
$Lang['eGuidance']['sen']['NotConfirm'] = "未確診";
$Lang['eGuidance']['sen']['Intention'] = "傾向";
$Lang['eGuidance']['sen']['Quit'] = "已退組";
$Lang['eGuidance']['sen']['RD']['Chi'] = "中文";
$Lang['eGuidance']['sen']['RD']['Eng'] = "英文";
$Lang['eGuidance']['sen']['SENStudent'] = "SEN 學生";
$Lang['eGuidance']['sen']['SENType'] = "SEN 類型";
$Lang['eGuidance']['sen']['Tab']['Case'] = "個案";
$Lang['eGuidance']['sen']['Tab']['Service'] = "服務";

$Lang['eGuidance']['sen_case']['AddAdjustmentItem'] = "新增調適項目";
$Lang['eGuidance']['sen_case']['AddSupportStudyItem'] = "新增學習方面的支援服務";
$Lang['eGuidance']['sen_case']['AddSupportOtherItem'] = "新增情緒、社交、自理方面的支援服務";
$Lang['eGuidance']['sen_case']['BasicInfo'] = "基本資料";
$Lang['eGuidance']['sen_case']['ConfirmDate'] = "同意書日期";
$Lang['eGuidance']['sen_case']['Filter']['LogicAnd'] = "符合所有選擇類型";
$Lang['eGuidance']['sen_case']['Filter']['LogicOr'] = "符合任一選擇類型";
$Lang['eGuidance']['sen_case']['FilterSelection'] = "選擇條件";
$Lang['eGuidance']['sen_case']['FineTuneArrangement'] = "調適安排";
$Lang['eGuidance']['sen_case']['FineTuneSupportOther'] = "支援服務(情緒、社交、自理方面)";
$Lang['eGuidance']['sen_case']['FineTuneSupportStudy'] = "支援服務(學習方面)";
$Lang['eGuidance']['sen_case']['NumberOfReport'] = "報告數量";
$Lang['eGuidance']['sen_case']['Remark'] = "額外資料";
$Lang['eGuidance']['sen_case']['SelectSENType'] = "選擇 SEN 類型";
$Lang['eGuidance']['sen_case']['SENTypeConfirmDate'] = "確診日期";
$Lang['eGuidance']['sen_case']['Tier']['Tier1'] = "第一層";
$Lang['eGuidance']['sen_case']['Tier']['Tier2'] = "第二層";
$Lang['eGuidance']['sen_case']['Tier']['Tier3'] = "第三層";
$Lang['eGuidance']['sen_case']['Tier']['TierName'] = "支援模式";
$Lang['eGuidance']['sen_case']['Warning']['DuplicateRecord'] = "所選學生已經有SEN個案紀錄,不能覆寫牠";
$Lang['eGuidance']['sen_case']['Warning']['SelectCaseType'] = "請選擇個案類型";
$Lang['eGuidance']['sen_case']['Warning']['SelectIsSEN'] = "請選擇是否已確診為SEN學生";

$Lang['eGuidance']['sen_service']['ActivityList'] = "活動列表";
$Lang['eGuidance']['sen_service']['ActivityManagement'] = "活動管理";
$Lang['eGuidance']['sen_service']['AddActivity'] = "新增活動";
$Lang['eGuidance']['sen_service']['AllServiceType'] = "所有服務類型";
$Lang['eGuidance']['sen_service']['EditActivity'] = "編輯活動";
$Lang['eGuidance']['sen_service']['ExtraInfo'] = "額外資料";
$Lang['eGuidance']['sen_service']['SenCase'] = "SEN個案";
$Lang['eGuidance']['sen_service']['SenService'] = "SEN服務";
$Lang['eGuidance']['sen_service']['ServiceDate'] = "活動日期";
$Lang['eGuidance']['sen_service']['ServiceManagement'] = "服務管理";
$Lang['eGuidance']['sen_service']['ServiceType'] = "服務類型";
$Lang['eGuidance']['sen_service']['Remind']['ClickDateToAddNew'] = "請點選日期以新增活動";
$Lang['eGuidance']['sen_service']['Warning']['CannotDeleteSENService'] = "因為已經有活動，不能刪除SEN服務!";
$Lang['eGuidance']['sen_service']['Warning']['DeleteStudentWithActivity'] = "當你更新此版面時,刪除的學生的活動內容及結果將一併刪除,你是否確定更新?";
$Lang['eGuidance']['sen_service']['Warning']['InputServiceTypeName'] = "請輸入服務類型的名稱";
$Lang['eGuidance']['sen_service']['Warning']['InputExtraInfo'] = "請輸入最少一項額外資料";
$Lang['eGuidance']['sen_service']['Warning']['SelectServiceType'] = "請選擇服務類型";

$Lang['eGuidance']['settings']['FineTune']['AdjustItemChineseName'] = "調適項目中文名稱";
$Lang['eGuidance']['settings']['FineTune']['AdjustItemEnglishName'] = "調適項目英文名稱";
$Lang['eGuidance']['settings']['FineTune']['AdjustType'] = "調適類別";
$Lang['eGuidance']['settings']['FineTune']['AdjustTypeChineseName'] = "調適類別中文名稱";
$Lang['eGuidance']['settings']['FineTune']['AdjustTypeEnglishName'] = "調適類別英文名稱";
$Lang['eGuidance']['settings']['FineTune']['AllServiceScope'] = "所有服務範疇";
$Lang['eGuidance']['settings']['FineTune']['CaseSubTypeChineseName'] = "個案子類型中文名稱";
$Lang['eGuidance']['settings']['FineTune']['CaseSubTypeEnglishName'] = "個案子類型英文名稱";
$Lang['eGuidance']['settings']['FineTune']['CaseTypeChineseName'] = "個案類型中文名稱";
$Lang['eGuidance']['settings']['FineTune']['CaseTypeCode'] = "代碼";
$Lang['eGuidance']['settings']['FineTune']['CaseTypeEnglishName'] = "個案類型英文名稱";
$Lang['eGuidance']['settings']['FineTune']['NumberOfSubType'] = "個案子類型數目";
$Lang['eGuidance']['settings']['FineTune']['ServiceScope'] = "支援服務範疇";
$Lang['eGuidance']['settings']['FineTune']['ServiceType'] = "支援服務類別";
$Lang['eGuidance']['settings']['FineTune']['ServiceTypeChineseName'] = "支援服務類別中文名稱";
$Lang['eGuidance']['settings']['FineTune']['ServiceTypeEnglishName'] = "支援服務類別英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Support']['AllSupportType'] = "所有服務類別";
$Lang['eGuidance']['settings']['FineTune']['Support']['ChineseName'] = "支援服務中文名稱";
$Lang['eGuidance']['settings']['FineTune']['Support']['EnglishName'] = "支援服務英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Support']['Service'] = "支援服務";
$Lang['eGuidance']['settings']['FineTune']['Support']['Type'] = "支援服務類別";
$Lang['eGuidance']['settings']['FineTune']['SupportType']['Study'] = "學習方面";
$Lang['eGuidance']['settings']['FineTune']['SupportType']['Other'] = "情緒、社交、自理方面";
$Lang['eGuidance']['settings']['FineTune']['Tab']['Arrangement'] = "調適安排";
$Lang['eGuidance']['settings']['FineTune']['Tab']['CaseType'] = "個案類型";
$Lang['eGuidance']['settings']['FineTune']['Tab']['Support'] = "支援服務";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier'] = "支援層次";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier1'] = "第一層";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier2'] = "第二層";
$Lang['eGuidance']['settings']['FineTune']['Tier']['Tier3'] = "第三層";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteAdjustItem'] = "因為所選調適項目已經有SEN個案採用,所以不能刪除";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteAdjustType'] = "因為所選調適類別的項目已經有SEN個案採用,所以不能刪除";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSenCaseSubType'] = "因為所選個案子類型已經有SEN個案採用,所以不能刪除";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSenCaseType'] = "因為所選個案類型已經有SEN個案採用,所以不能刪除";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteServiceType'] = "因為所選支援服務類別的項目已經有SEN個案採用,所以不能刪除";
$Lang['eGuidance']['settings']['FineTune']['Warning']['CannotDeleteSupportService'] = "因為所選支援服務已經有SEN個案採用,所以不能刪除";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateAdjustItem'] = "調適項目的名稱不能重複";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateAdjustType'] = "調適類別的名稱不能重複";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateCaseSubType'] = "個案子類型的名稱及代碼不能重複";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateCaseType'] = "個案類型的名稱及代碼不能重複";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateServiceType'] = "支援服務類別的名稱不能重複";
$Lang['eGuidance']['settings']['FineTune']['Warning']['DuplicateSupport'] = "支援服務的名稱不能重複";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemChineseName'] = "請輸入調適項目的中文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustItemEnglishName'] = "請輸入調適項目的英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustTypeChineseName'] = "請輸入調適類別的中文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputAdjustTypeEnglishName'] = "請輸入調適類別的英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeChineseName'] = "請輸入個案類型的中文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeCode'] = "請輸入個案子類型的代碼";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseSubTypeEnglishName'] = "請輸入個案子類型的英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseTypeChineseName'] = "請輸入個案子類型的中文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseTypeCode'] = "請輸入個案類型的代碼";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputCaseTypeEnglishName'] = "請輸入個案類型的英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputServiceTypeChineseName'] = "請輸入支援服務類別的中文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputServiceTypeEnglishName'] = "請輸入支援服務類別的英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputSupportChineseName'] = "請輸入支援服務的中文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['InputSupportEnglishName'] = "請輸入支援服務的英文名稱";
$Lang['eGuidance']['settings']['FineTune']['Warning']['PleaseSelectServiceScope'] = "請選擇服務範疇";
$Lang['eGuidance']['settings']['FineTune']['WithText'] = "是否有文字輸入?";
$Lang['eGuidance']['settings']['MoveToReorder'] = "移動滑鼠以排列顯示次序";
$Lang['eGuidance']['settings']['Permission']['GeneralAccessRight'] = "基本權限";
$Lang['eGuidance']['settings']['Permission']['GroupDescription'] = "描述";
$Lang['eGuidance']['settings']['Permission']['GroupInfo'] = "小組資訊";
$Lang['eGuidance']['settings']['Permission']['GroupList'] = "小組列表";
$Lang['eGuidance']['settings']['Permission']['GroupPermission'] = "小組權限";
$Lang['eGuidance']['settings']['Permission']['GroupTitle'] = "小組名稱";
$Lang['eGuidance']['settings']['Permission']['InputDate'] = "加入日期";
$Lang['eGuidance']['settings']['Permission']['Management'] = "管理";
$Lang['eGuidance']['settings']['Permission']['MemberList'] = "成員列表";
$Lang['eGuidance']['settings']['Permission']['MemberName'] = "成員姓名";
$Lang['eGuidance']['settings']['Permission']['NoRights'] = "沒有權限";
$Lang['eGuidance']['settings']['Permission']['NumOfMember'] = "組員數目";
$Lang['eGuidance']['settings']['Permission']['SelectTeacher'] = "請選擇教職員";
$Lang['eGuidance']['settings']['Permission']['View'] = "檢視";
$Lang['eGuidance']['settings']['Reorder'] = "排列顯示次序";
$Lang['eGuidance']['settings']['SeqNo'] = "顯示次序";
$Lang['eGuidance']['settings']['System']['AllowClassTeacherViewSEN'] = "允許班主任檢視相關班別學生的SEN資料";
$Lang['eGuidance']['settings']['System']['AllowSubjectTeacherViewSEN'] = "允許科主任檢視相關科組學生的SEN資料";
$Lang['eGuidance']['settings']['System']['PrintSenCasePICinStudentReport'] = "學生報告列印SEN個案負責老師";
$Lang['eGuidance']['settings']['System']['General'] = "一般";
$Lang['eGuidance']['settings']['Warning']['DuplicateGroupName'] = "小組名稱不能重複";
$Lang['eGuidance']['settings']['Warning']['InputGroupName'] = "請輸入小組名稱";
$Lang['eGuidance']['settings']['Warning']["Symbols"] = "!\"#$%&\'*,/:;<=>?@^`|~";
$Lang['eGuidance']['settings']['Warning']["SymbolsExclude"] = "代碼不能包含這些符號 ".$Lang['eGuidance']['settings']['Warning']["Symbols"];

$Lang['eGuidance']['StartDate'] = "起始日期";
$Lang['eGuidance']['StudentList'] = "學生列表";
$Lang['eGuidance']['StudentName'] = "學生姓名";

$Lang['eGuidance']['student_problem']['AdditionalMaterial'] = "補充資料";
$Lang['eGuidance']['student_problem']['StudentRegNo'] = "學生編號";
$Lang['eGuidance']['student_problem']['StudentName'] = "學生姓名";
$Lang['eGuidance']['student_problem']['Type'] = "類型";
$Lang['eGuidance']['student_problem']['Warning']['PleaseSelectAtLeastOne'] = "請選擇最少一項條件";

$Lang['eGuidance']['SubmitAndNotifyReferral'] = "呈送並通知同事個案已被轉介";
$Lang['eGuidance']['SubmitAndNotifyUpdate'] = "呈送並通知同事檔案已更新";

$Lang['eGuidance']['suspend']['AllSuspendType'] = "所有停課類別";
$Lang['eGuidance']['suspend']['InSchool'] = "校內停課";
$Lang['eGuidance']['suspend']['OutSchool'] = "校外停課";
$Lang['eGuidance']['suspend']['Result'] = "結果";
$Lang['eGuidance']['suspend']['SuspendType'] = "停課類別";
$Lang['eGuidance']['suspend']['Warning']['SelectSuspendType'] = "請選擇停課類別";

$Lang['eGuidance']['TeacherName'] = "負責老師";

$Lang['eGuidance']['therapy']['ActivityList'] = "活動列表";
$Lang['eGuidance']['therapy']['ActivityManagement'] = "活動管理";
$Lang['eGuidance']['therapy']['ActivityName'] = "活動名稱";
$Lang['eGuidance']['therapy']['AddActivity'] = "新增活動";
$Lang['eGuidance']['therapy']['ContentAndResult'] = "內容及結果";
$Lang['eGuidance']['therapy']['EditActivity'] = "編輯活動";
$Lang['eGuidance']['therapy']['GroupName'] = "小組名稱";
$Lang['eGuidance']['therapy']['Remind']['ClickDateToAddNew'] = "請點選日期以新增活動";
$Lang['eGuidance']['therapy']['Warning']['DeleteStudentWithActivity'] = "當你更新此版面時,刪除的學生的活動內容及結果將一併刪除,你是否確定更新?";
$Lang['eGuidance']['therapy']['Warning']['InputAtLeastOne'] = "請輸入活動名稱,備註或最少一項內容及結果";
$Lang['eGuidance']['therapy']['Warning']['InputGroupName'] = "請輸入小組名稱";

$Lang['eGuidance']['transfer']['IsClassTeacherKnow'] = "班主任是否知悉事件?";
$Lang['eGuidance']['transfer']['KnowDate'] = "知悉日期";
$Lang['eGuidance']['transfer']['Remark'] = "其他補充資料";
$Lang['eGuidance']['transfer']['TransferDate'] = "轉介日期";
$Lang['eGuidance']['transfer']['TransferFrom'] = "轉介來自";
$Lang['eGuidance']['transfer']['TransferReason'] = "轉介原因";
$Lang['eGuidance']['transfer']['TransferTo'] = "轉介至";
$Lang['eGuidance']['transfer']['Warning']['InputTransferReason'] = "請輸入轉介原因";
$Lang['eGuidance']['transfer']['Warning']['SelectTransferReason'] = "請選擇轉介原因";
$Lang['eGuidance']['transfer']['Warning']['TransferFromClassmateButSelf'] = "轉介來自同學,但選了自己";

$Lang['eGuidance']['Warning']['AreYouSureYouWouldLikeToDeleteThisFile'] = "你確定要刪除此檔案？";
$Lang['eGuidance']['Warning']['IllegalFileType'] = "附件類型不合法。";
$Lang['eGuidance']['Warning']['InputInteger'] = "請輸入整數";
$Lang['eGuidance']['Warning']['InputNumber'] = "請輸入數字";
$Lang['eGuidance']['Warning']['InvalidDateEarly'] = "揀選日期不能早於開始日期。";
$Lang['eGuidance']['Warning']['MeetingDate'] = "會議日期必須在該學年之間%s ~ %s";
$Lang['eGuidance']['Warning']['SelectAcademicYear'] = "請選擇學年";
$Lang['eGuidance']['Warning']['SelectAuthorizedUser'] = "請選擇授權人士";
$Lang['eGuidance']['Warning']['SelectClass'] = "請選擇班別";
$Lang['eGuidance']['Warning']['SelectConfidentialViewer'] = "請選擇機密資料可閱人士";
$Lang['eGuidance']['Warning']['SelectNotifier'] = "請選擇同事";
$Lang['eGuidance']['Warning']['SelectReportType'] = "請選擇報告類型";
$Lang['eGuidance']['Warning']['SelectStudent'] = "請選擇學生";
$Lang['eGuidance']['Warning']['SelectTeacher'] = "請選擇負責老師";

if (is_file("$intranet_root/lang/cust/eGuidance_lang.$intranet_session_language.customized.php"))
{
	include("$intranet_root/lang/cust/eGuidance_lang.$intranet_session_language.customized.php");
}
