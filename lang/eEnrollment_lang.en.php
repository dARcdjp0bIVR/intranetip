<?php
//using: 

// $Lang['eEnrolment']['Hour'] = "Hours";
$Lang['eEnrolment']['Hour'] = "Hour(s)";
$Lang['eEnrolment']['Unit'] = "";
$Lang['eEnrolment']['AllDay'] = "All Days";
$Lang['eEnrolment']['WeekDay'][1] = "Monday";
$Lang['eEnrolment']['WeekDay'][2] = "Tuesday";
$Lang['eEnrolment']['WeekDay'][3] = "Wednesday";
$Lang['eEnrolment']['WeekDay'][4] = "Thursday";
$Lang['eEnrolment']['WeekDay'][5] = "Friday";
$Lang['eEnrolment']['WeekDay'][6] = "Saturday";
$Lang['eEnrolment']['WeekDay'][7] = "Sunday";


$Lang['eEnrolment']['Number'][1] = "One";
$Lang['eEnrolment']['Number'][2] = "Two";
$Lang['eEnrolment']['Number'][3] = "Three";
$Lang['eEnrolment']['Number'][4] = "Four";
$Lang['eEnrolment']['Number'][5] = "Five";
$Lang['eEnrolment']['Number'][6] = "Six";
$Lang['eEnrolment']['Number'][7] = "Seven";
$Lang['eEnrolment']['Number'][8] = "Eight";
$Lang['eEnrolment']['Number'][9] = "Nine";
$Lang['eEnrolment']['Number'][10] = "Ten";
$Lang['eEnrolment']['Number'][11] = "Eleven";
$Lang['eEnrolment']['Number'][12] = "Twelve";

$Lang['eEnrolment']['ClubTypeAry'] = array ("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T");


$Lang['eEnrolment']['ShortWeekday'][1] = "Mon";
$Lang['eEnrolment']['ShortWeekday'][2] = "Tue";
$Lang['eEnrolment']['ShortWeekday'][3] = "Wed";
$Lang['eEnrolment']['ShortWeekday'][4] = "Thu";
$Lang['eEnrolment']['ShortWeekday'][5] = "Fri";
$Lang['eEnrolment']['ShortWeekday'][6] = "Sat";
$Lang['eEnrolment']['ShortWeekday'][7] = "Sun";

$Lang['eEnrolment']['AfterDayDisplay'][0] = "Today";
$Lang['eEnrolment']['AfterDayDisplay'][1] = "Tomorrow";
// $Lang['eEnrolment']['AfterDayDisplay'][2] = "Next Tomorrow";
$Lang['eEnrolment']['AfterDayDisplay'][2] = "The Day After Tomorrow";


$Lang['eEnrolment']['Total'] = "Total";
$Lang['eEnrolment']['Times'] = "Time(s)";
$Lang['eEnrolment']['CountClub'] = " ";
// $Lang['eEnrolment']['AfterDay'] = "Day(s) After";
$Lang['eEnrolment']['AfterDay'] = "Day(s) Later";
$Lang['eEnrolment']['NextMeeting'] = "Next Activity";
$Lang['eEnrolment']['Finished'] = "Finished";
$Lang['eEnrolment']['FinishedClub'] = "Finished Club";
$Lang['eEnrolment']['FinishedMeeting'] = "Finished";
// $Lang['eEnrolment']['InParticipating'] = "In Participating";
$Lang['eEnrolment']['InParticipating'] = "Participating";
$Lang['eEnrolment']['Header']['ClubInfo'] = "Club Info";
$Lang['eEnrolment']['Header']['ActivityInfo'] = "Activity Info";
$Lang['eEnrolment']['PerformanceandComment']  = "Performance and Teacher's Comment";
$Lang['eEnrolment']['TeacherComment'] = "Teacher's Comment";
$Lang['eEnrolment']['Performance'] = "Performance";
$Lang['eEnrolment']['ShowMore'] = "Show more";
$Lang['eEnrolment']['attendance_percent'] = "Attendance (%)";
$Lang['eEnrolment']['SelectMaxClub1'] = "Number chosen as the maximum number of club(s) you want to participate can only be the number from ";
$Lang['eEnrolment']['SelectMaxClub2'] = " to ";
$Lang['eEnrolment']['ErrorMaxClubTitle'] = "Attention";

$Lang['eEnrolment']['Gender'] = "Gender";
$Lang['eEnrolment']['Quato'] = "Quota";
$Lang['eEnrolment']['TargetAge'] = "Target Age";
$Lang['eEnrolment']['Tentative'] = "Tentative";

$Lang['eEnrolment']['TargetForm'] = "Target Form";
$Lang['eEnrolment']['ClubContent'] = "Club Content";
$Lang['eEnrolment']['Details'] = "Details";
$Lang['eEnrolment']['OLECategory'] = "Other Learning Experience Category";
$Lang['eEnrolment']['Fee'] = "Fee";

$Lang['eEnrolment']['AttendanceRecord'] = "Attendance Record";
$Lang['eEnrolment']['Deadline']= "Deadline";
$Lang['eEnrolment']['FromTodayTo'] = "From Today To";
$Lang['eEnrolment']['InApplication'] = "In Application";
$Lang['eEnrolment']['InApplication2'] = "Processing";

$Lang['eEnrolment']['ViewApplicationInstructionsAndStartApply'] = "View Application Instructions and Start Apply";
$Lang['eEnrolment']['ViewApplicationInstructionsAndStartApply2'] = "View Application Instructions and Apply Now";
$Lang['eEnrolment']['StartApply'] = "Start Apply";
$Lang['eEnrolment']['StartApply2'] = "Apply Now";
$Lang['eEnrolment']['EditClubApply'] = "Edit club application";
$Lang['eEnrolment']['OnlyApplyOneClub'] = "You can only apply for one club.";
$Lang['eEnrolment']['ConsiderApplyClashClub'] = "Please consider whether apply at same time.";
$Lang['eEnrolment']['ConsiderApplyClashClub2'] = "Please consider whether to apply for the selected clubs at the same time.";
$Lang['eEnrolment']['ClubHaveTimeClash'] =" have time clash,";
$Lang['eEnrolment']['ClubHaveTimeClash2'] = "There is a time clash with ";
$Lang['eEnrolment']['YouHaveChoose'] = "You have chosen ";
$Lang['eEnrolment']['SuretoApplyClashClub'] = "Are you sure to apply these club at same time?";
$Lang['eEnrolment']['OnlyCanApplyOneClub'] = "You can only apply one of them.";
$Lang['eEnrolment']['YouNeedAppliedClubMin'] = "You need apply at least [__count__]";
$Lang['eEnrolment']['YouNeedAppliedClubMin2'] = "You need to apply at least [__count__]";
$Lang['eEnrolment']['YouMustAppliedClub'] = "You must apply [__count__]";
$Lang['eEnrolment']['YouNeedChoose'] = "You need to choose ";
$Lang['eEnrolment']['NowYouOnlyChoose']= "now you have only chosen ";
$Lang['eEnrolment']['NoOfClubMustBeApply'] = "Number of club(s) you must be apply";
$Lang['eEnrolment']['Club2'] = "club(s)";

$Lang['eEnrolment']['AlreadyAchieveMaxClubCount']= "already reached max club count";
$Lang['eEnrolment']['AlreadyAchieveMaxClubCountTitle']= "Already reached max club count";
$Lang['eEnrolment']['AlreadyExceededMaxClubCount']= "already exceeded max club count";
$Lang['eEnrolment']['ExceededMaxClubCount']= "Have exceeded max club count";
$Lang['eEnrolment']['NotAchieveMinClubCount']= "Not yet reach min club count";
$Lang['eEnrolment']['AreYouSureToSubmitApplication'] = "Are you sure to submit application?";



$Lang['eEnrolment']['EditMaxClubNumber'] = "Edit Max Club Number";
$Lang['eEnrolment']['YouHaveFinishClubApply'] = "You Have Finished Club Application.";
$Lang['eEnrolment']['EditClubOrder'] = "Edit Club Order";
$Lang['eEnrolment']['CheckandHandleTimeClash'] = "Check And Handle Time Clash";
$Lang['eEnrolment']['FindandHandleTimeClash'] = "Find And Handle Time Clash";
$Lang['eEnrolment']['YouHaveSelected']  = "You have selected";
$Lang['eEnrolment']['ShowApplyInstruction'] = "Show Club Application Instruction";
$Lang['eEnrolment']['EachClubTimeClashTimes'] = "Each club time clash time(s) :";
$Lang['eEnrolment']['AreYouSureToRemoveFromList'] = "Are you sure to remove 「[__ClubTitle__]」 from list?";
// $Lang['eEnrolment']['ClubApplyResultWillShow'] = "Club application result will show on [__date__]";
$Lang['eEnrolment']['ClubApplyResultWillShow'] = "Club application result will be shown on [__date__]";
$Lang['eEnrolment']['YouHaveTotalAppliedClubCount'] = "You have total applied [__ClubCount__] club(s).";
$Lang['eEnrolment']['YouHaveTotalAppliedClubCount2'] = "You have applied [__ClubCount__] club(s) in total.";
$Lang['eEnrolment']['ExitClubApplicationWrningMessage'] = "You application have not sent yet,
will lose records if exit,
    
are you sure to give up application, and exit?";
$Lang['eEnrolment']['ClubApplicationResult'] = "Club Application Result";
$Lang['eEnrolment']['NoResultFound'] = "No Result Found";
$Lang['eEnrolment']['ReturntoEdit'] = "Return to Edit";

$Lang['eEnrolment']['FullStop'] = ".";
$Lang['eEnrolment']['Comma'] = ",";
$Lang['eEnrolment']['AlertArchivedMaxCanEnrolledClub'] = "The number of joined club has reached maximum, no need apply more club.";
$Lang['eEnrolment']['CannotEnrolledClub'] = "Cannot make application";
?>