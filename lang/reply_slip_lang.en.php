<?php
$Lang['ReplySlip']['WarningMsg']['SubmitReplySlip'] = "Are you sure you want to submit the reply slip?";
//$Lang['ReplySlip']['WarningMsg']['ReplySlipQuestionNotAnswered'] = "Question(s) <!-questionNumList-> is(are) blank. Are you sure you want to submit the reply slip?";
$Lang['ReplySlip']['WarningMsg']['ReplySlipQuestionNotAnswered'] = "You have not filled in all the answers yet. Are you sure you want to submit the reply slip?";
?>