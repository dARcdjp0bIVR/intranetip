/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// Need save as utf-8!!!!!!
/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////
// global messages
var globalAlertMsg1 = "Please check only one item.";
var globalAlertMsg2 = "Please check at least one item.";
var globalAlertMsg3 = "Are you sure you want to delete?";
var globalAlertMsg4 = "Are you sure you want to approve?";
var globalAlertMsg5 = "Are you sure you want to suspend?";
var globalAlertMsg6 = "Please check only two items.";
var globalAlertMsg7 = "Are you sure you want to swap?";
var globalAlertMsg8 = "Enter the new folder name:";
var globalAlertMsg9 = "Please enter an integer.";
var globalAlertMsg10 = "Please enter a new name:";
var globalAlertMsg11 = "Filename already exists.";
var globalAlertMsg12 = "Are you sure you want to unzip?";
var globalAlertMsg13 = " is not a zip file.";
var globalAlertMsg14 = "Please check at least one item.";
var globalAlertMsg15 = "Please check at least one item.";
var globalAlertMsg16 = "Are you sure you want to logout?";
var globalAlertMsg17 = "Please check the group category.";
var globalAlertMsg18 = "Please select one item.";
var globalAlertMsg19 = "Enter the new period title:";
var globalAlertMsg20 = "Invalid Time End and Time Start.";
var globalAlertMsg21 = "Are you sure you want to archive?";
var globalAlertMsg25 = "Invalid file name.";
var globalAlertMsg26 = "Are you sure you want to publish?";
var globalAlertMsg27 = "Are you sure you want to unpublish?";
var globalAlertMsg28 = "Are you sure you want to set the first page blank?";
var globalAlertMsg29 = "Are you sure you want to unset the blank first page ?";
var globalAlertMsg30 = "Are you sure you want to set the navigation to vertical?";
var globalAlertMsg31 = "Are you sure you want to set the navigation back to horizontal?";
var globalAlertMsg32 = "Are you sure you want to transfer?";
var globalAlertActivate = "Are you sure you want to activate?";
var NonIntegerWarning = "You need to enter a positive integer.";
var globalAlertMsgRemoveAll = "Are you sure you want to delete ALL records? (including not shown records)";
var globalAlertMsgSendPassword = "Are you sure you want to send password to user via email?";
var globalAlertMsgNewBatch = "Input New Name";
var globalAlertMsgSetBatch = "Are you sure you want to set this as the resources-booking timetable?\n(All resource items using the timetable will be using this new timetable. Booking records will remain unaffected,\n yet you will need to check whether these booking records are still valid.)";

var globalAlertMsgReject = "Are you sure you want to reject? Rejected record(s) will be deleted.";
var globalAlertMsgRejectNoDel = "Are you sure you want to reject?";

var globalAlertMsgEnable = "Are you sure you want to enable the item(s)?";
var globalAlertMsgDisable = "Are you sure you want to disable the item(s)?";

var globalAlertMsgTransferToWebSAMS = "Are you sure you want to transfer these record(s) to WebSAMS?";
var globalClearMsgForUpdateStatus = "Clear";

//var ForgotPasswordMsg = "1. Please input user login, then reset password mail will be sent to your preset email address.\n2. If no reset password mail received, kindly contact school IT administrator.";
//var ForgotPasswordMsg = "1. Input UserLogin, then reset pwd mail will be sent to your preset address.\n2. If no reset password mail received, kindly contact school IT administrator.";
var ForgotPasswordMsg = "1. Input UserLogin, then reset pwd mail will be sent to your preset address.\n2. If no mail received, kindly contact  your school / organization.";

var JSLang = Array();
JSLang['WarningFirefoxNotAllowPaste'] = 'Only IE and Firefox 15 or older version support the copy and paste function. If you are using Firefox 15 or older version but the browser warn you about the copy and paste function is disabled by the browser, please enter "about:config" in the URL field and set "signed.applets.codebase_principal_support" as "true".';
JSLang['Loading'] = 'Loading...';
JSLang['Close'] = 'Close';	// used in thickbox.js
JSLang['OK'] = 'OK';		// used in jquery.alerts.js
JSLang['Cancel'] = 'Cancel';
JSLang['Alert'] = 'Alert';
JSLang['Confirm'] = 'Confirm';
JSLang['Prompt'] = 'Prompt';
JSLang['RefreshOverload'] = 'The Malicious Refresh action is being detected. Please do not refresh the same page again.';