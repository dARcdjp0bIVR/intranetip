<?php

$ccym_ole_report['activity']['head'][0] = "課外活動<br />ECA";
$ccym_ole_report['activity']['head'][1] = "職位<br />Post";
$ccym_ole_report['activity']['head'][2] = "備註<br />Remarks";
$ccym_ole_report['activity_title'] = "甲. 課外活動 Extra-Curricular Activities";

$ccym_ole_report['class_name'] = "班別 Class Name";
$ccym_ole_report['class_number'] = "班號 Class No.";

$ccym_ole_report['doi'] = "派發日期 Date of Issue";

$ccym_ole_report['female'] = "女 Female";

$ccym_ole_report['end'] = "- 完 -";

$ccym_ole_report['hour'] = "小時";

$ccym_ole_report['male'] = "男 Male";

$ccym_ole_report['no_records'] = "沒有紀錄";

$ccym_ole_report['ole']['head'][0] = "經歷時數<br />Hours";
$ccym_ole_report['ole']['head'][1] = "備註<br />Remarks";
$ccym_ole_report['ole_title'] = "丙. 其他學習經歷 Other Learning Experiences";
$ccym_ole_report['other_title'] = "丁. 獎項及成就 Awards & Achievement";

$ccym_ole_report['reg_no'] = "註冊編號 Reg. No.";

$ccym_ole_report['service']['head'][0] = "服務<br />Service";
$ccym_ole_report['service']['head'][1] = "職位<br />Post";
$ccym_ole_report['service']['head'][2] = "備註<br />Remarks";
$ccym_ole_report['service_plan'] = "服務多多FUN計劃<br />'Service is FUN' Scheme";
$ccym_ole_report['service_title'] = "乙. 學校服務 School Service";
$ccym_ole_report['service_total_hour'] = "服務總時數";
$ccym_ole_report['sex'] = "性別 Sex";
$ccym_ole_report['class_number'] = "班號 Class No.";
$ccym_ole_report['star'] = "星級";
$ccym_ole_report['star_award'] = "活動星級積分獎勵計劃<br />Activity Star Point Award Scheme";
$ccym_ole_report['star_highest'] = "最高 5 星級";
$ccym_ole_report['star_got'] = "獲取星級";
$ccym_ole_report['student_name'] = "學生姓名 Name";

$ccym_ole_report['total_hour'] = "總時數 Total Hours";

?>