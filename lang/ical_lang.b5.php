<?php
# Editing by 

$i_Calendar_Undo = "還原";
$i_Calendar_Apply = "套用";
$i_Calendar_Am = "上午";
$i_Calendar_Pm = "下午";
$i_Calendar_At = "在";
$i_Calendar_RemoveAll = "全部移除";
$i_Calendar_UndoAll = "全部還原";
$i_Calendar_Loading = "載入中...";
$i_Calendar_InvalidSearch_NoKeyword = "無法搜尋，請輸入關鍵字。";
$i_Calendar_NoCalSelect = "請最少選擇一個日曆。";

$i_Calendar_Subject = "主旨";
$i_Calendar_StartDate = "開始日期";
$i_Calendar_StartTime = "開始時間";
$i_Calendar_EndDate = "結束日期";
$i_Calendar_EndTime = "結束時間";
$i_Calendar_AllDayEvent = "全日事件";

$i_Calendar_Conflict_Warning = "你選擇的時間 <strong>%s</strong> 已被下列事件佔用:";
$i_Calendar_Conflict_Warning2 = "此事件的時間 <strong>%s</strong> 與下列事件發生衝突:";
$i_Calendar_Conflict_Ignore = "確定儲存";

$i_Calendar_ImportCalendar = "匯入事件";
$i_Calendar_ImportCalendar_ChooseCalendar = "選擇日曆";
$i_Calendar_ImportCalendar_IncorrectValue = "附有「<span class='tabletextrequire'>*</span>」的項目不正確";
$i_Calendar_ImportCalendar_Instruction1 = "
第一欄 : Subject<br />
第二欄 : Start Date (YYYY-MM-DD or MM/DD/YYYY)<br />
第三欄 : Start Time (HH:mm)<br />
第四欄 : End Date (YYYY-MM-DD or MM/DD/YYYY)<br />
第五欄 : End Time (HH:mm)<br />
第六欄 : All Day Event<br />
第七欄 : Description<br />
第八欄 : Location<br />
";

$i_Calendar_ExportCalendar = "匯出事件";
$i_Calendar_ExportCalendar_Range = "選擇時間範圍";
$i_Calendar_ExportCalendar_Onward = "由今天起的所有事件";

$i_Calendar_Settings_PreferredView = "選擇觀看";
$i_Calendar_Settings_TimeFormat = "時間格式";
$i_Calendar_Settings_12hr = "12時制";
$i_Calendar_Settings_24hr = "24時制";
$i_Calendar_Settings_WeekStartOn = "每星期的首日";
$i_Calendar_Settings_WorkingHours = "工作時間";
$i_Calendar_Settings_DisableRepeat = "停用重複事件";
//$i_Calendar_Settings_DisableGuest = "停用參與者";
$i_Calendar_Settings_DisableGuest = "停用參加者";

$i_Calendar_AccessRight = "使用權";
$i_Calendar_ForceRemoval = "強制移除";
$i_Calendar_New_RemoveBySearch = "移除搜尋所得項目";
$i_Calendar_ForceRemoval_Instruction = "請輸入開始及結束日期，所有於此段期間的事件將被移除: ";
$i_Calendar_SearchRemoval_Type = "搜尋類型";
$i_Calendar_SearchRemoval_TypeOption1 = "事件";
$i_Calendar_SearchRemoval_TypeOption2 = "日曆";
$i_Calendar_SearchRemoval_Phase = "搜尋字句";

$iCalendar_Main_Title = "iCalendar";

$iCalendar_NewEvent_Header = "建立新事件";
$iCalendar_EditEvent_Header = "編輯事件";

$iCalendar_ToolLink_ChooseDate = "選擇日期";
$iCalendar_ToolLink_QuickAddEvent = "快速新增事件";
$iCalendar_ToolLink_Agenda = "事件列表";
$iCalendar_ToolLink_Preference = "設定";

$iCalendar_Preference_View = "預設日曆顯示";

$iCalendar_SchoolEvent_RecordType = array("學校","教學","假期","小組");
$iCalendar_SchoolEvent_Color = array("4F9313","0D98B6","FF0000","FF8401");
$iCalendar_SchoolEvent_Venue = "場地";
$iCalendar_SchoolEvent_Nature = "性質";
$iCalendar_SchoolEvent_PostedBy = "張貼者";
$iCalendar_SchoolEvent_Type = "類型";

$iCalendar_Agenda_Status = "狀況";
$iCalendar_Agenda_YourResponse = "你的回應";
$iCalendar_Agenda_NoEvent = "這段期間沒有找到任何事件。";

$iCalendar_QuickAdd_Required = "必需填寫。";
$iCalendar_QuickAdd_InvalidDate = "格式錯誤。";

$iCalendar_NewEvent_EventDate = "事件日期";
$iCalendar_NewEvent_EventTime = "事件時間";

$iCalendar_NewEvent_HeaderOption = "選項";
$iCalendar_NewEvent_HeaderShare = "分享";
$iCalendar_NewEvent_Title = "標題";
$iCalendar_NewEvent_DateTime = "日期/時間";
$iCalendar_NewEvent_StartTime = "開始:";
$iCalendar_NewEvent_Duration = "持續時間:";
$iCalendar_NewEvent_DurationMin = "分鐘";
$iCalendar_NewEvent_DurationMins = "分鐘";
$iCalendar_NewEvent_DurationHr = "小時";
$iCalendar_NewEvent_DurationHrs = "小時";
$iCalendar_NewEvent_DurationDay = "日";
$iCalendar_NewEvent_DurationDays = "日";
$iCalendar_NewEvent_DurationWeek = "星期";
$iCalendar_NewEvent_DurationWeeks = "星期";
$iCalendar_NewEvent_DurationMonth = "月";
$iCalendar_NewEvent_DurationMonths = "月";
$iCalendar_NewEvent_DurationYear = "年";
$iCalendar_NewEvent_DurationYears = "年";
$iCalendar_NewEvent_Repeats = "重複";
$iCalendar_NewEvent_Repeats_Not = "不會重複";
$iCalendar_NewEvent_Repeats_Every = "多久重複一次? ";
$iCalendar_NewEvent_Repeats_On = "重複在: ";
$iCalendar_NewEvent_Repeats_By = "重複: ";
$iCalendar_NewEvent_Repeats_Daily = "每日";
$iCalendar_NewEvent_Repeats_DailyMsg1 = "每 ";
$iCalendar_NewEvent_Repeats_DailyMsg2 = ", 直到 ";
$iCalendar_NewEvent_Repeats_EveryWeekday = "每個工作日 (一至五)";
$iCalendar_NewEvent_Repeats_EveryWeekday2 = "每星期在工作日";
$iCalendar_NewEvent_Repeats_EveryMonWedFri = "逢星期一、三、五";
$iCalendar_NewEvent_Repeats_EveryMonWedFri2 = "逢星期一、三、五";
$iCalendar_NewEvent_Repeats_EveryTuesThur = "逢星期二、四";
$iCalendar_NewEvent_Repeats_EveryTuesThur2 = "逢星期二、四";
$iCalendar_NewEvent_Repeats_Weekly = "每星期";
$iCalendar_NewEvent_Repeats_Weekly2 = "每星期在 ";
$iCalendar_NewEvent_Repeats_Weekly3 = " 星期在 ";
$iCalendar_NewEvent_Repeats_CheckWeekday = "星期";
$iCalendar_NewEvent_Repeats_WeekdayArray = array("日", "一", "二", "三", "四", "五", "六");
$iCalendar_NewEvent_Repeats_WeekdayArray2 = array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六");
$iCalendar_NewEvent_Repeats_Count = array("第一個", "第二個", "第三個", "第四個", "第五個", "第六個");
$iCalendar_NewEvent_Repeats_Monthly = "每月";
$iCalendar_NewEvent_Repeats_Monthly2 = "每月的";
$iCalendar_NewEvent_Repeats_Monthly2b = "日";

$iCalendar_NewEvent_Repeats_Monthly_general = "每%N%個月的第%D%日";

$iCalendar_NewEvent_Repeats_Monthly3 = "每月的一日";
$iCalendar_NewEvent_Repeats_Monthly4 = "每星期的一日";
$iCalendar_NewEvent_Repeats_Monthly5 = "個月的 ";
$iCalendar_NewEvent_Repeats_MonthArray = array("一月","二月","三月","四月","五月","六月","七月","八月","九月","十月","十一月","十二月");
$iCalendar_NewEvent_Repeats_Yearly = "年度";
$iCalendar_NewEvent_Repeats_Yearly2 = "每年在 ";
$iCalendar_NewEvent_Repeats_Yearly3 = " 年在 ";
$iCalendar_NewEvent_Repeats_Range = "範圍:";
$iCalendar_NewEvent_Repeats_RangeStart = "開始:";
$iCalendar_NewEvent_Repeats_RangeEnd = "完結:";
$iCalendar_NewEvent_Repeats_RangeNever = "永不";
$iCalendar_NewEvent_Repeats_RangeUntil = "直到";
$iCalendar_NewEvent_Repeats_SaveTitle = "編輯重複發生的事件";
$iCalendar_NewEvent_Repeats_SaveMsg = "你想編輯此事件、同一系列中所有事件、定或此事件與及同一系列中所有未來的事件?";
$iCalendar_NewEvent_Repeats_SaveThisOnly = "只獨立更改這個事件";
$iCalendar_NewEvent_Repeats_SaveAll = "系列中的所有事件";
$iCalendar_NewEvent_Repeats_SaveFollow = "所有接著會發生的事件";
$iCalendar_NewEvent_IsImportant = "重要";
$iCalendar_NewEvent_IsAllDay = "全日事件";
$iCalendar_NewEvent_Calendar = "日曆";
$iCalendar_NewEvent_Description = "描述";
$iCalendar_NewEvent_Location = "地點";
$iCalendar_NewEvent_Link = "連結:";
$iCalendar_NewEvent_Reminder = "提示";
$iCalendar_NewEvent_Reminder_Popup = "彈出式視窗";
$iCalendar_NewEvent_Reminder_Email = "電子郵件";
$iCalendar_NewEvent_Reminder_NotSet = "未有設定提示";
$iCalendar_NewEvent_Reminder_Remove = "移除";
$iCalendar_NewEvent_Reminder_Add = "新增提示";
$iCalendar_NewEvent_Access = "權限";
$iCalendar_NewEvent_AccessDefault = "預設";
$iCalendar_NewEvent_AccessPublic = "公開";
$iCalendar_NewEvent_AccessPrivate = "私人";
//$iCalendar_NewEvent_Share = "新增參與者";
$iCalendar_NewEvent_Share = "新增參加者";
$iCalendar_NewEvent_ShareResponse = "你會出席嗎？";
//$iCalendar_NewEvent_ShareCount = "參與者人數";
$iCalendar_NewEvent_ShareCount = "參加者人數";
$iCalendar_NewEvent_ShareCountMsgX = "你尚未回覆";
$iCalendar_NewEvent_ShareCountMsg1 = "還未回應";
$iCalendar_NewEvent_ShareCountMsg2 = "可能出席";
$iCalendar_NewEvent_ShareCountMsg3 = "會出席";
$iCalendar_NewEvent_ShareCountMsg4 = "不會出席";
//$iCalendar_NewEvent_SelectGuest = "選擇參與者";
$iCalendar_NewEvent_SelectGuest = "選擇參加者";

$iCalendar_Calendar_MyCalendar = "我的日曆";
$iCalendar_Calendar_OtherCalendar = "其他日曆";
$iCalendar_Calendar_SystemCalendar = "系統日曆";
$iCalendar_Calendar_SchoolCalendar = "校曆";
$iCalendar_Calendar_InvolveEvent = "已參與事件";
$iCalendar_Calendar_ManageCalendar = "管理日曆";
$iCalendar_Calendar_NoCalendar = "沒有日曆";

$iCalendar_EditEvent_CreatedBy = "建立者";
$iCalendar_DeleteEvent_ConfirmTitle1 = "刪除重複發生的事件";
$iCalendar_DeleteEvent_ConfirmTitle2 = "刪除事件";
$iCalendar_DeleteEvent_ConfirmMsg1 = "你是否確定要刪除此事件、同一系列中所有事件、或此事件與及同一系列中所有未來的事件?";
$iCalendar_DeleteEvent_ConfirmMsg2 = "確認刪除\"#%EventTitle%#\"?";

$iCalendar_CheckForm_NoTitle = "請輸入事件標題。";
$iCalendar_CheckForm_NoDate = "請輸入事件日期。";
$iCalendar_CheckForm_NoRepeatEnd = "請輸入重複事件的完結日期。";
$iCalendar_CheckForm_DateWrong = "日期格式錯誤，正確的格式是 YYYY-MM-DD。";
$iCalendar_CheckForm_UrlWrong = "請輸入正確的網址。";
$iCalendar_CheckForm_NoCalName = "請輸入日曆名稱。";

$iCalendar_NewCalendar_Header = "建立新日曆";
$iCalendar_NewCalendar_Name = "日曆名稱";
$iCalendar_NewCalendar_Description = "描述";
$iCalendar_NewCalendar_PeopleShare = "分享予";
$iCalendar_NewCalendar_ShareWithAll = "與所有人分享";
$iCalendar_NewCalendar_GroupShare = "分享予(小組)";
$iCalendar_NewCalendar_AddPeopleShare = "新增分享對象";
$iCalendar_NewCalendar_ShareTablePerson = "分享對象";
$iCalendar_NewCalendar_ShareTablePermission = "權限";
$iCalendar_NewCalendar_ShareTableDelete = "刪除";
$iCalendar_NewCalendar_SharePermissionFull = "所有權限";
//$iCalendar_NewCalendar_SharePermissionEdit = "查看和編輯";
$iCalendar_NewCalendar_SharePermissionEdit = "查看和創建";
$iCalendar_NewCalendar_SharePermissionRead = "只能查看";
$iCalendar_NewEvent_ShareSelectViewer = "選擇分享對象";
$iCalendar_NewEvent_ShareColor = "顯示顏色";

$iCalendar_AddPublicCalendar_Header = "新增公開日曆";
$iCalendar_SearchPublicCalendar = "關鍵字搜尋";

$iCalendar_EditCalendar_Header = "編輯日曆";
$iCalendar_EditCalendar_Owner = "日曆擁有者";
$iCalendar_EditCalendar_Group = "小組";

$iCalendar_ManageCalendar_Sharing = "分享";
$iCalendar_ManageCalendar_SharedMsg1 = "已分享";
$iCalendar_ManageCalendar_SharedMsg2 = "未分享";
$iCalendar_ManageCalendar_Setting = "日曆設定";
$iCalendar_ManageCalendar_ConfirmTitle1 = "刪除日曆";
$iCalendar_ManageCalendar_ConfirmMsg1 = "永久刪除名為<strong>#%calName%#</strong>的日曆?";
$iCalendar_ManageCalendar_ConfirmMsg2 = "不再檢視名為 <strong>#%calName%#</strong> 的日曆?";

$iCalendar_CalendarList_Title = "日曆列表";

$iCalendar_ChooseViewer_ParentSuffix = "的家長";
$iCalendar_ChooseViewer_Course = "課程";
$iCalendar_ChooseViewer_Group = "小組";

$iCalendar_Agenda_NoEventsForToday = "這段期間沒有任何事件";
$iCalendar_ActiveEvent_Event = "事件";
$iCalendar_Agenda_NoEventsForThisMonment = "這段期間沒有任何事件";
$iCalendar_Calendar_Today = "今天";
$iCalendar_addUser = "新增用戶";
$iCalendar_NewCalendar_ShareTo = "分享給";
$iCalendar_NewCalendar_Type = "日曆種類";
$iCalendar_Calendar_PersonalCalendar = "個人日曆";
$iCalendar_SetSchoolCalender_Remark = "所有一般用戶均不能刪除、修改此校曆， 也不能在此校曆新增、修改或刪除事件，除非用戶是DBA用戶。";
$iCalendar_Calendar_Staff = "職員";
$iCalendar_Calendar_Student = "學生";
$iCalendar_NewCalendar_PermissionSetting = "權限設置";
//$iCalendar_NewEvent_EventType_Compulsory = "強制性加入(通知)";
$iCalendar_NewEvent_EventType_Compulsory = "強制參加";
//$iCalendar_NewEvent_EventType_Optional = "選擇性加入(邀請)";
$iCalendar_NewEvent_EventType_Optional = "邀請參加";
$iCalendar_NewEvent_EndTime = "結束:";
$i_Calendar_Today = "今天";
$i_Calendar_NextSevenDays = "未來七日";
$i_Calendar_ThisWeek = "這星期";
$iCalendar_ImportCalendarEvent_SelectFile = "選擇檔案";
$iCalendar_ImportCalendarEvent_Format = "格式";
$iCalendar_ImportCalendarEvent_DownloadSample = "按此下載範例";
$iCalendar_Calendar_CalendarFound = "找到日曆。";
$iCalendar_Calendar_NoCalendarFound = "找不到日曆。";
$iCalendar_Search="尋找";
$iCalendar_SharedCalendar ="共享日曆";
$iCalendar_PublicCalendar = "公共日曆";
$iCalendar_Calendar_MyCalendar = "個人";
$iCalendar_BackTo_Calendar = "返回日曆";
$iCalendar_SchoolCalendar="校曆";
$iCalendar_Name = "日曆名稱";
$iCalendar_Calendar_Subscribe = "同意";
$iCalendar_Calendar_Unsubscribe = "不同意";
$iCalendar_Calendar_OtherCalendar_School = "學校";
$iCalendar_NewEvent_SyncUrl = "日曆同步:";
$iCalendar_Yes = "是";
$iCalendar_No = "否";
$iCalendar_New_Event = "新增事件";
$iCalendar_Print= "列印";
$iCalendar_invitation = "邀請";
$iCalendar_notification = "通知";
$iCalendar_NoActiveEvent = "這段期間沒有有效的事件。";
$iCalendar_OK  = "確定";
$iCalendar_more = "更多"; 
$iCalendar_new= "新增";
$iCalendar_NewEvent_Personal_Note = "個人備忘";
$iCalendar_add= "增加";
$iCalendar_close="關閉";
$iCalendar_meeting_Check_Availability = "檢查可行時間";
$iCalendar_meeting_Check = "檢查";
$iCalendar_NewEvent_ShareCountMsgA = "已確認";
//$iCalendar_NewEvent_InviteGuests = "邀請賓客";
$iCalendar_NewEvent_InviteGuests = "邀請參加者";
//$iCalendar_NewEvent_Guests = "賓客";
$iCalendar_NewEvent_Guests = "參加者";
$iCalendar_DeleteEvent_ConfirmMsg2 = "你是否決定要刪除 \"#%EventTitle%#\"?";
$iCalendar_DeleteEvent_ConfirmMsg2_js = "你是否決定要刪除 #%EventTitle%#?";
$iCalendar_ManageCalenadar_syncDes = "(Google日曆提示: 如果你並未發佈你的Google日曆,請複製 <span style='text-decoration:underline'>私人網址</span> 內的超連結.)";
$icalender_deleteCalendar_existGuest_remark = "因為此日曆(<strong>#%calName%#</strong>)是與別人分享的，刪除此日曆只會影響你個人的設定。其他的使用者仍可看到此日曆，除非他們自行刪除此日曆。";
$iCalendar_NewEvent_MultipleRepeat_Overlap = "是, 建立重疊的事件";
$iCalendar_NewEvent_MultipleRepeat_One = "不, 只建立一個事件";
$iCalendar_OverlapWarning = "這是一個%numDay%日的重複事件。你是不是決定建立一組%numDay%日的重複事件？";
$iCalendar_PopupAlert= "提示";
$iCalendar_EventType = "事件種類";
//$iCalendar_AddToGuest = "加入到賓客日暦";
$iCalendar_AddToGuest = "加入到參加者日暦";
$iCalendar_OfficeHR = "辦公時間";
//$iCalendar_meeting_Reminder = "當檢查可行時間時，請最少選擇一個賓客並指明日期/時間。";
$iCalendar_meeting_Reminder = "當檢查可行時間時，請最少選擇一個參加者並指明日期/時間。";
$iCalendar_Import_Error1 = "不能連接伺服器，無法滙入。";
$iCalendar_Import_Error2 = "iCal文件格式錯誤，無法滙入。";
$iCalendar_Import_Success = "日曆滙入成功。";
$i_Calendar_Synchronizing = "同步中...";
$iCalendar_ExteranlEvent_StartDate = "開始日期";
$iCalendar_ExteranlEvent_DueDate = "截止日期";
$iCalendar_group = "社群";
$iCalendar_course = "課程";
$iCalendar_systemAdmin = "系統管理員";
$iCalendar_selectedTime = "已指定的時間";
$iCalendar_available = '有空';
$iCalendar_unavailable = '沒有空';
$iCalendar_searchTab = '搜尋結果';
$iCalendar_eventSearch = '事件搜尋';
$iCalendar_eventSearch_noRecordFound = '找不到任何記錄。';
$iCalendar_eventSearch_searchingFor ='%%value%% 的搜尋結果';
$iCalendar_evtRequest = '待回應事件';

### ESF en.php
$Lang['btn_export'] = "滙出csv";
$Lang['btn_export_ical'] = "滙出ical";
$Lang['syncURL'] = "同步";
$Lang['btn_edit'] = "修改";
$Lang['btn_delete'] = "刪除";
$Lang['btn_remove'] = "移除";
$Lang['btn_restore'] = "回復";
$Lang['btn_submit'] = "提交";
$Lang['btn_cancel'] = "取消";
$Lang['btn_save'] = "儲存";
$Lang['general']['SetToAll_SelectionBox'] = "--- 全選 ---";
$Lang['ReturnMsg']['CalendarCreateSuccess'] = "1|=|建立日曆成功。";
$Lang['ReturnMsg']['CalendarCreateUnSuccess'] = "0|=|建立日曆失敗。";
$Lang['ReturnMsg']['CalendarEditSuccess'] = "1|=|修改日曆成功。";
$Lang['ReturnMsg']['CalendarEditUnSuccess'] = "0|=|修改日曆失敗。";
$Lang['ReturnMsg']['CalendarUpdateSuccess'] = "1|=|更新日曆成功。";
$Lang['ReturnMsg']['CalendarUpdateUnSuccess'] = "0|=|更新日曆失敗。";
$Lang['ReturnMsg']['PersonalCalendarDeleteSuccess'] = "1|=|刪除個人日曆成功。";
$Lang['ReturnMsg']['PersonalCalendarDeleteUnSuccess'] = "0|=|刪除日曆失敗。";
$Lang['ReturnMsg']['OptionalCalendarRemoveSuccess'] = "1|=|移除其他日曆成功。";
$Lang['ReturnMsg']['OptionalCalendarRemoveUnSuccess'] = "1|=|移除其他日曆失敗。";
$Lang['ReturnMsg']['EventImportSuccess'] = "1|=|滙入事件成功。";
$Lang['ReturnMsg']['EventImportUnSuccess'] = "0|=|滙入事失敗。";
$Lang['ReturnMsg']['FormatOfImportedFileNotValid'] = "0|=|滙入的文件格式錯誤。";
$Lang['ReturnMsg']['TypeOfImportedFileNotSupport'] = "0|=|不支援滙入的文件種類。";
$Lang['ReturnMsg']['EventDeleteSuccess'] = "1|=|刪除事件成功。";
$Lang['ReturnMsg']['EventDeleteUnSuccess'] = "0|=|刪除事件失敗。";
$Lang['ReturnMsg']['EventCreateSuccess'] = "1|=|建立事件成功。";
$Lang['ReturnMsg']['EventCreateUnSuccess'] = "0|=|建立事件失敗。";
$Lang['ReturnMsg']['EventUpdateSuccess'] = "1|=|事件更新成功。";
$Lang['ReturnMsg']['EventUpdateUnSuccess'] = "0|=|事件更新失敗。";
$Lang['ReturnMsg']['SettingUpdatedSuccess'] = "1|=|學年設定更新成功。";
$Lang['ReturnMsg']['SettingUpdatedUnSuccess'] = "0|=|學年設定更失敗。";
$Lang['ReturnMsg']['UserPreferenceUpdateSuccess'] = "1|=|個人選項設定更新成功。";
$Lang['ReturnMsg']['UserPreferenceUpdateUnSuccess'] = "0|=|個人選項設定更失敗。";
$Lang['btn_submitAndSend'] = "提交並發送電郵";
$Lang['btn_select'] = "選擇";
$Lang['btn_reset'] = "重置";

#newly added 
$iCalendar_submitNsend_title = "邀請參加新事件";
$iCalendar_submitNsend_msg= "你被邀請參加 %EventTitle%。請登入內聯網選擇接受/拒絕邀請。";
$iCalendar_submitNsend_msgShort= "你被邀請參加 %EventTitle%。";

$iCalendar_seachType_all = "搜尋所有事件";
$iCalendar_seachType_page = "搜尋這一頁";
$iCalendar_seach_Keyword = "關鍵字";
$i_Calendar_Edit = "修改";
$i_Calendar_modifiedDate = "修改日期";
$iCalendar_NewEvent_AddAlert = "新增提示";
$iCalendar_viewMode_simple = "簡單顯示";
$iCalendar_viewMode_full = "完整顯示";
$iCalendar_User_Preference = "個人選項";
$iCalendar_UserPref_DefaultMonthView = '預設顯示模式';
$iCalendar_UserPref_DefaultCalendar = '預設日曆 (新增事件)';

$i_Calendar_ImportEvent_InvalidRecords = '錯誤的紀錄';
$i_Calendar_ImportEvent_ValidRecords = '正確的紀錄';
$i_Calendar_NoRecords_Remark = '沒有任何紀錄';
$i_Calendar_eBookingEvent_TimeClashWarning = '你所選擇的地點於該時間已被預約，你的預約將會設定為「等待批核」。';
$i_Calendar_eBookingEvent_cancelBookingFirstWarning = '此事件已有地點預約紀錄。如要更改事件的日期或時間，請先清除有關地點預約紀錄。';
$i_Calendar_eBookingEvent_clearBookingWarning = '你是否確定要清除地點預約？';
$i_Calendar_eBookingEvent_titleDescRemarks = '事件的「標題」和「描述」將於電子資源預訂系統的紀錄中顯示為「備註」。';

$Lang['iCalendar']['SkipHoliday'] = "略過假日";
$Lang['iCalendar']['WarningArr']['AllDaysInDateRangeAreHolidays'] = "所選日期範圍全部都是假日，請選擇其他日期範圍，或者取消 [".$Lang['iCalendar']['SkipHoliday']."] 選項。";

$Lang['iCalendar']['RemoveSharedCalendarWarning'] = "此日曆(<strong>#%calName%#</strong>)是與別人分享的，你要移除日曆並保留其他分享者的檢視權，抑或徹底移除日曆?";
$Lang['iCalendar']['RemoveAndKeepSharersViewRight'] = "移除日曆並保留其他分享者的檢視權";
$Lang['iCalendar']['RemoveCalendarCompletely'] = "徹底移除日曆";

$Lang['iCalendar']['TheLast'] = "最後的一個";

$Lang['iCalendar']['CheckAvailability'] = "檢查可行時間";
$Lang['iCalendar']['CheckFor'] = "檢查";
$Lang['iCalendar']['AvailableOrNot'] = "時間是否可行";
$Lang['iCalendar']['EventTimeline'] = "整天事件時間表";
$Lang['iCalendar']['IncludingEventType'] = "包括事件類型";
$Lang['iCalendar']['Available'] = "可行";
$Lang['iCalendar']['Unavailable'] = "不可行";
$Lang['iCalendar']['RequestSelectGuest'] = "請至少選擇一個參加者。";
$Lang['iCalendar']['ParticipationOption'] = "選項";
$Lang['iCalendar']['PersonalEvent'] = "私人事項";
$Lang['iCalendar']['NParticipantsInvolved'] = " (<!--N-->位參與者)";
$Lang['iCalendar']['SubmitAndSendPushMessage'] = "提交並發送推送訊息";
$Lang['iCalendar']['CompulsoryEventPushNotificationTitle'] = "Calendar event (<!--DATE-->) from <!--NAME-->";
$Lang['iCalendar']['CompulsoryEventPushNotificationContentTimeStatement'] = " which will start at <!--TIME-->";
$Lang['iCalendar']['CompulsoryEventPushNotificationContentVenueStatement'] = " (venue: <!--VENUE-->)";
$Lang['iCalendar']['CompulsoryEventPushNotificationContent'] = "You have been added to an event titled \"<!--TITLE-->\"<!--TIME_STATEMENT--><!--VENUE_STATEMENT--> on <!--DATE-->. ";
$Lang['iCalendar']['InvitationEventPushNotificationTitle'] = "Calendar event (<!--DATE-->) from <!--NAME-->";
$Lang['iCalendar']['InvitationEventPushNotificationContentTimeStatement'] = " which will start at <!--TIME-->";
$Lang['iCalendar']['InvitationEventPushNotificationContentVenueStatement'] = " (venue: <!--VENUE-->)";
$Lang['iCalendar']['InvitationEventPushNotificationContent'] = "You are invited to join an event titled \"<!--TITLE-->\"<!--TIME_STATEMENT--><!--VENUE_STATEMENT--> on <!--DATE-->. Please response using iCalendar. ";

$Lang['iCalendar']['TheRoomIsNotAvailableOnTheFollowingDates'] = "這個房間在下列日期不能使用:";
$Lang['iCalendar']['TheRoomIsAvailableOnTheFollowingDates'] = "這個房間在下列日期可以使用:";

?>