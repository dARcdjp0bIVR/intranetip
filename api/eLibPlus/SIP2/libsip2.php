<?php
/*
 * Sample input message send from SC:
 * - Login						9300CNLoginUserID|COLoginPassword|CPLocationCode|AY5AZEC78<CR>
 * - SC Status 					9900401.00AY1AZFCA5<CR>
 * - Request ACS Resend			97<CR>
 * - Patron Status Request		2300119960212 100239AOid_21|104000000105|AC|AD|AY2AZF400<CR>
 * - Patron Information Message	6300119980723 091905Y AOInstitutionID|AAPatronID|BP00001|BQ00005|AY1AZEA83<CR>
 * - Block Patron				01N19960213 162352AO|ALCARD BLOCK TEST| AA104000000705|AC|AY2AZF02F<CR>
 * - Patron Enable				2519980723 094240AOCertification Institute ID|AAPatronID|AY4AZEBF1<CR>
 * - Item Information			1719980723 100000AOCertification Institute ID|ABItemBook|AY1AZEBEB
 * - Checkout					11YN19960212 10051419960212 100514AO|AA104000000105|AB000000000005792|AC|AY3AZEDB7<CR>
 * - Checkin					09N19980821 085721 APCertification Terminal Location|AOCertification Institute ID|ABCheckInBook|ACTPWord|BIN|AY2AZD6A5
 * - Fee Paid Message			3719980723 0932110401USD BV111.11|AOCertification Institute ID|AAPatronID|BKTransactionID|AY2AZE1EF<CR>
 * - End Patron Session			3519980723 094014AOCertification Institute ID|AAPatronID|AY3AZEBF2<CR>
 * - Item status update			19
 * - hold						15
 * - renew						29
 * - renew all					65
 * 
 * Sample output message send to SC:
 * - Login Response					941AY3AZFDFA<CR>
 * - ACS Status						98YYYNYN01000319960214 1447001.00AOID_21|AMCentral Library|ANtty30|AFScreen Message|AGPrint Message|AY1AZDA74<CR>
 * - Request SC Resend				96<CR>
 * - Patron Status Response			24 00119960212 100239AO|AA104000000105|AEJohn Doe|AFScreen Message|AGCheck Print message|AY2AZDFC4<CR>
 *  (also Block Patron Response)
 * - Patron Information Response	64 00119980723 104009000100000002000100020000AOInstitutionID for
 * 									PatronID|AAPatronID|AEPatron Name|BZ0002|CA0003|CB0010|BLY|ASItemID1 for PatronID
 * 									|AUChargeItem1|AUChargeItem2|BDHome Address|BEE Mail Address|BFHome Phone for
 * 									PatronID|AFScreenMessage 0 for PatronID, Language 1|AFScreen Message 1 for PatronID, Language
 * 									1|AFScreen Message 2 for PatronID, Language 1|AGPrint Line 0 for PatronID, Language 1|AGPrint Line 1 for
 * 									PatronID, Language 1|AGPrint Line 2 for PatronID, language 1|AY4AZ608F
 * - Patron Enable Response			26 00119980723 111413AOInstitutionID for PatronID|AAPatronID|AEPatron
 * 									Name|BLY|AFScreenMessage 0 for PatronID, Language 1|AFScreen Message 1 for PatronID, Language 1
 * 									|AGPrint Line 0 for PatronID, Language 1|AY7AZ8EA6
 * - Item Information Response		1808000119980723 115615CF00000|ABItemBook|AJTitle For Item Book|
 * 									CK003|AQPermanent Location for ItemBook, Language 1|APCurrent Location ItemBook|CHFree-form text 
 * 									with new item property|AY0AZC05B
 * - Checkout Response				120NNY19960212 100514AO|AA104000000105|AB000000000005792|AJ|AH|AFItem cannot be charged : see
 * 									help desk|AGItem can not be charged : see help desk|AY3AZD2A1<CR>
 * - Checkin Response				101YNN19980821 085721AOCertification Institute ID|ABCheckInBook|AQPermanent Location for
 *									CheckinBook, Language 1|AJTitle For CheckinBook|AAGoodPatron1|CK001|CHCheckinBook
 *									Properties|CLsort bin A1|AFScreen Message for CheckInBook|AGPrint Line for CheckInBook|AY2AZA3FF
 * - Fee Paid Response				38Y19980723 111035AOInstitutionID for PatronID|AAPatronID|AFScreenMessage 0 for PatronID,
 *									Language 1|AGPrint Line 0 for PatronID, Language 1|AY6AZ9716
 * - End Session Response			36Y19980723 110658AOInstitutionID for PatronID|AAPatronID|AFScreenMessage 0 for PatronID,
 *									Language 1|AFScreen Message 1 for PatronID, Language 1|AFScreen Message 2 for PatronID, Language
 *									1|AGPrint Line 0 for PatronID, Language 1|AGPrint Line 1 for PatronID, Language 1|AGPrint Line 2 for
 *									PatronID, language 1|AY5AZ970F
 * - Item status update Response	20
 * - hold Response					16
 * - renew Response					30
 * - renew all Response				66
 */

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/pdump/pdump.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/libms.lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."home/library_sys/lib/liblibrarymgmt.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/BookManager.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/RightRuleManager.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/RecordManager.php");
include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/TimeManager.php");
//include_once($PATH_WRT_ROOT."home/library_sys/management/circulation/User.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");

intranet_opendb();

class sip2 
{

///* Sequence number counter */
//public $seq   = -1;
//
///* Private variables for building messages */
//public $AO = 'WohlersSIP';
//public $AN = 'SIPCHK';

/*terminal password */
public $AC = ''; /*AC */
    
/* Maximum number of resends allowed before get_message gives up */
public $maxretry     = 3;

/* Terminator s */
public $fldTerminator = '|';
public $msgTerminator = "\r\n";

/* Login Variables */
public $UIDalgorithm = 0;   /* 0    = unencrypted, default */
public $PWDalgorithm = 0;   /* undefined in documentation */
public $scLocation   = '';  /* Location Code */
 
    /* Debug */
public $debug        = false;

/* Private variables for building messages */
public $AO = 'WohlersSIP';
public $AN = 'SIPCHK';

/* Private variable to hold socket connection */
private $socket;

/* Sequence number counter */
    private $seq   = -1;
 
    /* resend counter */
private $retry = 0;

/* Workarea for building a message */
private $msgBuild = '';
private $noFixed = false;

private $libms;
private $RecordManager;

function __construct(){
	$this->libms =  new liblms();
	$this->RecordManager = new RecordManager($this->libms);
}

function _parsevariabledata($response, $start){ 
    $result = array();
    $result['Raw'] = explode("|", substr($response,$start,-6));
    foreach ($result['Raw'] as $item) {
        $field = substr($item,0,2);
        $value = substr($item,2);
        /* SD returns some odd values on ocassion, Unable to locate the purpose in spec, so I strip from 
        * the parsed array. Orig values will remain in ['raw'] element
        */
        $clean = trim($value, "\x00..\x1F");
        if (trim($clean) <> '') {
            $result[$field][] = $clean;
        }
    }        
    $result['AZ'][] = substr($response,-4);
 	$this->seq = $result['AY'][0];
 	
    return ($result);
}

/* Core local utility functions */    
    function _datestamp($timestamp = '') 
    {
        /* generate a SIP2 compatable datestamp */
        /* From the spec:
        * YYYYMMDDZZZZHHMMSS. 
        * All dates and times are expressed according to the ANSI standard X3.30 for date and X3.43 for time. 
        * The ZZZZ field should contain blanks (code $20) to represent local time. To represent universal time, 
        *  a Z character(code $5A) should be put in the last (right hand) position of the ZZZZ field. 
        * To represent other time zones the appropriate character should be used; a Q character (code $51) 
        * should be put in the last (right hand) position of the ZZZZ field to represent Atlantic Standard Time. 
        * When possible local time is the preferred format.
        */
        if ($timestamp != '') {
            /* Generate a proper date time from the date provided */
            return date('Ymd    His', $timestamp);
        } else {
            /* Current Date/Time */
            return date('Ymd    His');
        }
    }
	
	//-- charCodeAt From eqcode (http://eqcode.com/wiki/CharCodeAt) [start] 
	function charCodeAt($str, $num) { return $this->utf8_ord($this->utf8_charAt($str, $num)); }
	function utf8_ord($ch) {
	  $len = strlen($ch);
	  if($len <= 0) return false;
	  $h = ord($ch{0});
	  if ($h <= 0x7F) return $h;
	  if ($h < 0xC2) return false;
	  if ($h <= 0xDF && $len>1) return ($h & 0x1F) <<  6 | (ord($ch{1}) & 0x3F);
	  if ($h <= 0xEF && $len>2) return ($h & 0x0F) << 12 | (ord($ch{1}) & 0x3F) << 6 | (ord($ch{2}) & 0x3F);          
	  if ($h <= 0xF4 && $len>3) return ($h & 0x0F) << 18 | (ord($ch{1}) & 0x3F) << 12 | (ord($ch{2}) & 0x3F) << 6 | (ord($ch{3}) & 0x3F);
	  return false;
	}
	function utf8_charAt($str, $num) { return mb_substr($str, $num, 1, 'UTF-8'); }
 	//-- charCodeAt From eqcode (http://eqcode.com/wiki/CharCodeAt) [end]
 	
    function _crc($buf) 
    {
    	//32421 40043
//    	$chineseText = '中國學生綜合能力培養必讀-告訴學生聰慧機敏的機智故事';
//    	$chineseText= iconv('UTF-8',"CP850",$chineseText);
//    	$buf = '121NNY20150810    133325AO|AAbroadlearning|ABA+1000002|AJ'.$chineseText.'|AH20150817    133325|AFCannot borrow this book!|AGPlease return item on of before 2015-08-17|AY6AZ';
//        $buf = iconv('UTF-8',"UTF-16",$buf);
//       $buf = mb_convert_encoding ($buf,"ISO-8859-1", 'UTF-8');
        /* Calculate CRC  */
        //return mb_detect_encoding ($buf);
//        static char ascii_checksum[5];
//		unsigned int checksum = 0;
//		int i = 0;
//		while (message[i] != ‘\0’)
//		{
//		checksum += (unsigned) message[i];
//		i++;
//		}
//		checksum = -(checksum & 0xFFFF);
//		sprintf (ascii_checksum, “%4.4X”, checksum);
//		return (ascii_checksum);
        
        $sum = 0;
 
        $len = strlen($buf);
        for ($n = 0; $n < $len; $n++) {
            //$sum = $sum + ord(substr($buf, $n, 1));
            $sum = $sum + $this->charCodeAt($buf,$n);
        } 
 
        $crc = ($sum & 0xFFFF) * -1;
 		//return $crc;
        /* 2008.03.15 - Fixed a bug that allowed the checksum to be larger then 4 digits */
        return substr(sprintf ("%4X", $crc), -4, 4);
    } /* end crc */    
 
    function _getseqnum() 
    {
        /* Get a sequence number for the AY field */
        /* valid numbers range 0-9 */
//        $this->seq++;
//        if ($this->seq > 9 ) {
//            $this->seq = 0;
//        }
        return ($this->seq);
    }
    
    function _debugmsg($message) 
    {
        /* custom debug function,  why repeat the check for the debug flag in code... */
        if ($this->debug) { 
            trigger_error( $message, E_USER_NOTICE); 
        }    
    }
    
    function _check_crc($message) 
    {
        /* test the recieved message's CRC by generating our own CRC from the message */
        $test = preg_split('/(.{4})$/',trim($message),2,PREG_SPLIT_DELIM_CAPTURE);
 
        if ($this->_crc($test[0]) == $test[1]) {
            return true;
        } else {
            return false;
        }
    }
    
    function _newMessage($code) 
    {
        /* resets the msgBuild variable to the value of $code, and clears the flag for fixed messages */
        $this->noFixed  = false;
        $this->msgBuild = $code;
    }
    
    function _addFixedOption($value, $len) 
    {
        /* adds afixed length option to the msgBuild IF no variable options have been added. */
        if ( $this->noFixed ) {
            return false;
        } else {
            $this->msgBuild .= sprintf("%{$len}s", substr($value,0,$len));
            return true;
        }
    }
    
    function _addVarOption($field, $value, $optional = false) 
    {
        /* adds a varaiable length option to the message, and also prevents adding addtional fixed fields */
        if ($optional == true && $value == '') {
            /* skipped */
            $this->_debugmsg( "SIP2: Skipping optional field {$field}");
        } else {
            $this->noFixed  = true; /* no more fixed for this message */
            $this->msgBuild .= $field . substr($value, 0, 255) . $this->fldTerminator;
        }
        return true;
    }
    
    function _returnMessage($withSeq = true, $withCrc = true) 
    {
        /* Finalizes the message and returns it.  Message will remain in msgBuild until newMessage is called */
        if ($withSeq) {
            $this->msgBuild .= 'AY' . $this->_getseqnum();
        }
        if ($withCrc) {
            $this->msgBuild .= 'AZ';
            $this->msgBuild .= $this->_crc($this->msgBuild);
        }
        $this->msgBuild .= $this->msgTerminator;
 
        return $this->msgBuild;
    }


function Get_Login_Response($in){ // Message from 93
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'UIDalgorithm'      => substr($in,2,1),
	        'PWDalgorithm'      => substr($in,3,1),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 4);
        //return $result;
        
        /* 000 – Default (English)
		 001 – English
		 019 – Simplified Chinese
		 027 – Traditional Chinese */
        $LG = $result['variable']['LG'][0];
        $_SESSION["language"][$_SERVER["REMOTE_ADDR"]] = $LG;
        if($LG == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        //-- Add function here [Start]
        $ok = 1;
        //-- Add function here [End]
        
        $this->_newMessage('94');
        $this->_addFixedOption($ok, 1);
        return $this->_returnMessage();
}
function Get_ACS_Status($in){ // Message from 99
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'status'    => substr($in,2,1),
	        'width'     => substr($in,3,3),
	        'version'	=> substr($in,6,4),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 10);
        //return $result;
        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        //-- Add function here [Start]
        $global_lockout = (BOOL)$this->libms->get_system_setting('global_lockout');		
		$TM = new TimeManager();
		if ($global_lockout ||!$TM -> isOpenNow(time())) {
			$Online = 'N';
	        $Checkin = 'N';
	        $Checkout = 'N';
	        $Renewal = 'N';
	        $PatronUpdate = 'N';
	        $Offline = 'N'; // should modify later...
	        $Timeout = '01'; // 1 sec
	        $Retries = '0003';
	        $TransactionDate = $this->_datestamp();
	        $Protocol = '2.00';
	        $AO = 'ID_21';
	        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
	        	$AM = '綜合圖書館';
	        	$AF = '閉館中!';
	        	$AG = '閉館中!';
	        }
	        else{
	        	$AM = 'eLibPlus Library';
	        	$AF = 'Library is closed!';
	        	$AG = 'Library is closed!';
	        }
	        $AN = 'tty30';
		}
		else{
	        $Online = 'Y';
	        $Checkin = 'Y';
	        $Checkout = 'Y';
	        $Renewal = 'Y';
	        $PatronUpdate = 'Y';
	        $Offline = 'N'; // should modify later...
	        $Timeout = '01'; // 1 sec
	        $Retries = '0003';
	        $TransactionDate = $this->_datestamp();
	        $Protocol = '2.00';
	        $AO = 'ID_21';
	        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
	        	$AM = '綜合圖書館';
		        $AF = '歡迎使用綜合圖書館!';
		        $AG = '歡迎使用綜合圖書館!';
	        }
	        else{
	        	$AM = 'eLibPlus Library';
		        $AF = 'Welcome to eLibPlus!';
		        $AG = 'Welcome to eLibPlus!';
	        }
	        $AN = 'tty30';
		}
        //-- Add function here [End]
        
        $this->_newMessage('98');
        $this->_addFixedOption($Online, 1);
        $this->_addFixedOption($Checkin, 1);
        $this->_addFixedOption($Checkout, 1);
        $this->_addFixedOption($Renewal, 1);
        $this->_addFixedOption($PatronUpdate, 1);
        $this->_addFixedOption($Offline, 1);
        $this->_addFixedOption($Timeout, 2);
        $this->_addFixedOption($Retries, 4);
        $this->_addFixedOption($TransactionDate, 18);
        $this->_addFixedOption($Protocol, 4);
        
        $this->_addVarOption('AO',$AO);
        $this->_addVarOption('AM',$AM);
        $this->_addVarOption('AN',$AN);
        $this->_addVarOption('AF',$AF);
        $this->_addVarOption('AG',$AG);
        
        return $this->_returnMessage();
}
function Get_Request_SC_Resend($in){ // Message from 97
	// resend the previous message
}
function Get_Patron_Status_Response($in){ // Message from 23 | 01
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'language'    => substr($in,2,3),
	        'datestamp'     => substr($in,5,18),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 23);
        
        $AO = $result['variable']['AO'][0];
        $AA = $result['variable']['AA'][0];
        $AC = $result['variable']['AC'][0];
        $AD = $result['variable']['AD'][0];
        
        if($result['fixed']['language'] == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        // -- should be modified
        
        $libms = new liblms();
        $sql = "Select UserID, EnglishName, ChineseName From LIBMS_USER WHERE BarCode = '".$AA."' ";
        $patronInfo = $libms->returnArray($sql);
        
        if(!$patronInfo[0][0]){
        	$PropertiesOk = 'YYYY          ';
        	$Language = $result['fixed']['language'];
	        $TransactionDate = $this->_datestamp();
	        $AE = '';
        }
        else{
	        $PropertiesOk = '              ';
	        $Language = $result['fixed']['language'];
	        $TransactionDate = $this->_datestamp();
	        $AE = ($result['fixed']['language']=='027'?$patronInfo[0]['ChineseName']:$patronInfo[0]['EnglishName']);
        }
		$this->_newMessage('24'); 
        $this->_addFixedOption($PropertiesOk, 14);
        $this->_addFixedOption($Language, 3);
        $this->_addFixedOption($TransactionDate, 18);
        if($result['fixed']['language']=='027'){
	        $AF = '讀者螢幕訊息';
	        $AG = '讀者列印訊息';
        }
        else{
        	$AF = 'ScreenMessage 0 for PatronID';
	        $AG = 'Print Line 1 for PatronID, Language 1';
        }
        $this->_addVarOption('AO',$AO);
        $this->_addVarOption('AA',$AA);
        $this->_addVarOption('AE',$AE);
        $this->_addVarOption('AF',$AF);
        $this->_addVarOption('AG',$AG);    
 
        return $this->_returnMessage();
        // -- should be modified
}
function Get_Patron_Information_Response($in){ // Message from 63
	global $intranet_hardcode_lang;
//	$libms = new liblms();
	
	
	$result['fixed'] = 
        array( 
	        'language'    => substr($in,2,3),
	        'datestamp'     => substr($in,5,18),
	        'summary'     => substr($in,23,10),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 33);
        if($result['fixed']['language'] == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        
//        return $result;
        
        // -- should be modified
/* 64 00119980723 104009000100000002000100020000AOInstitutionID for
 * PatronID|AAPatronID|AEPatron Name|BZ0002|CA0003|CB0010|BLY|ASItemID1 for PatronID
 * |AUChargeItem1|AUChargeItem2|BDHome Address|BEE Mail Address|BFHome Phone for
 * PatronID|AFScreenMessage 0 for PatronID, Language 1|AFScreen Message 1 for PatronID, Language
 * 1|AFScreen Message 2 for PatronID, Language 1|AGPrint Line 0 for PatronID, Language 1|AGPrint Line 1 for
 * PatronID, Language 1|AGPrint Line 2 for PatronID, language 1|AY4AZ608F        
 */
        //-- Add function here [Start]
        $AO = $result['variable']['AO'][0];//$AO = 'InstitutionID for PatronID';
        $AA = $result['variable']['AA'][0];//$AA = 'PatronID';
        
        $AC = $result['variable']['AC'][0];//terminal Password
        $AD = $result['variable']['AD'][0];//patron Password
        
        $BP = $result['variable']['BP'][0];//start item
        $BQ = $result['variable']['BQ'][0];//end item
        
        // -- get the userid of input barcode
        //$libms = new liblms();
        $libms = $this->libms;
        $sql = "Select UserID, EnglishName, ChineseName, UserEmail From LIBMS_USER WHERE BarCode = '".$AA."' ";
        $patronInfo = $libms->returnArray($sql);
        
        $passwordMatch = $this->Check_User_Password($AA,$AD);
    	
    	$BL = 'N';
	    if(!$passwordMatch){
	    	if($patronInfo[0][0]){
	    		$BL = 'Y';
	    		$CQ = 'N';
	    	}
	    	$patronInfo = false;
	    }
        
        $User = new User($patronInfo[0]['UserID'], $libms);
//	    debug_pr($patronInfo[0]['UserID']);
//	    debug_pr($User);
//        $User = new User($patronInfo[0][0], $libms);
//	
//		$recordManager=new RecordManager($libms);
//	
//		$userInfo = $User->userInfo;
        
        if(!$patronInfo[0][0]){
        	$PatronStatus = 'YYYY          ';
        	$Language = $result['fixed']['language'];
	        $TransactionDate = $this->_datestamp();
	        $HoldCount = '0000';
	        $OverdueCount = '0000';
	        $ChargedCount = '0000';
	        $FineCount = '0000';
	        $RecallCount = '0000';
	        $UnavailableHoldsCount = '    ';
	        
	        $AE = '';
	        //$AE = 'Henry Chan';
	        //(hold limit is 2)(overdue limit is 3)(charged limit is 10)(valid patron)
	        // -- optional below
//	        $BZ = '0000';
//	        $CA = '0000';
//	        $CB = '0000';
//	        $BL = 'N';
	        $BE = '';
	        
	        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
				$AG = '條碼或密碼不正確!';
			}
			else{
				$AG = 'Barcode or password incorrect!';
			}
			$AF = $AG;
        }
        else{
	        $summaryArr = str_split($result['fixed']['summary']);
	        $Language = $result['fixed']['language'];
	        $TransactionDate = $this->_datestamp();
	        $HoldCount = 0;
	        $UnavailableHoldsCount = 0;
	        
	        //-- developing [start]
	        $patronStatusArr = array(' ',' ',' ',' ',' ',
									' ',' ',' ',' ',' ',
									' ',' ',' ',' ');
	        //-- developing [end]
	        
	        $reservedBooks = $User->reservedBooks;
	        foreach($reservedBooks as $aUserReserve){
	        	if($aUserReserve['RecordStatus'] == 'READY'){
	        		$HoldCount++;
	        		$holdBooks[] = $aUserReserve;
	        	}
	        	else{
	        		$UnavailableHoldsCount++;
	        		$unavailableHoldBooks[] = $aUserReserve;
	        	}
	        }
	        //$HoldCount = sprintf('%04d', $HoldCount);
	        $UnavailableHoldsCount = sprintf('%04d', $UnavailableHoldsCount);
	        $borrowedBooks = $User->borrowedBooks;
	        $HoldCount = sprintf('%04d', $HoldCount);
	        $User->getOverdueRecords();
	        $fineRecords = $User->overdueRecords;
	        $i = 0;
	        $overduedBooks = array();
			if (!empty($borrowedBooks)){
				foreach ($borrowedBooks as $book){
					$duetime = strtotime($book['DueDate']);
					$nowtime = time();
					if ( time() - $duetime > 60*60*24 ){
//						$is_overdue = true;
//						$tm = new TimeManager($libms);			
//						$overdue_day_count = $tm->dayForPenalty($duetime, $nowtime);
						$overduedBooks[] = $book;
					}
//					else{
//						$is_overdue = false;
//					}
					$i++;
				}
			}
	        $OverdueCount = sprintf('%04d', count($overduedBooks));
	        $ChargedCount = sprintf('%04d', count($borrowedBooks));
	        $FineCount = sprintf('%04d', count($fineRecords));
	        $RecallCount = '0000';
	        
	        $AE = ($result['fixed']['language']=='027'?$patronInfo[0]['ChineseName']:$patronInfo[0]['EnglishName']);
	        //$AE = 'Henry Chan';
	        //(hold limit is 2)(overdue limit is 3)(charged limit is 10)(valid patron)
	        // -- optional below
	        $patronLimit = $User->rightRuleManager->get_limits();
	        $BZ = sprintf('%04d', $patronLimit['LimitReserve']); //hold item limit
	        $CA = '9999'; //overdue item limit
	        $CB = sprintf('%04d', $patronLimit['LimitBorrow']); //charged item limit
	        $BL = 'Y';
	        $CQ = 'Y';
	        $BE = $patronInfo[0]['UserEmail'];
	        
	        if($summaryArr[0] == 'Y'){ //hold items AS
    			$start = $BP?($BP-1):0;
    			$end = $BQ?$BQ:count($holdBooks);
    			for($i=$start; $i<$end; $i++){
    				$AS[] = $holdBooks[$i]['BarCode'];
    			}
	        }
	        if($summaryArr[1] == 'Y'){ //overdue items AT
	        	$start = $BP?($BP-1):0;
    			$end = $BQ?$BQ:count($overduedBooks);
    			for($i=$start; $i<$end; $i++){
    				$AT[] = $overduedBooks[$i]['BarCode'];
    			}
	        }
	        if($summaryArr[2] == 'Y'){ //charged items AU
	        	$start = $BP?($BP-1):0;
    			$end = $BQ?$BQ:count($borrowedBooks);
    			for($i=$start; $i<$end; $i++){
    				$AU[] = $borrowedBooks[$i]['BarCode'];
    			}
	        }
	        if($summaryArr[3] == 'Y'){ //fine items AV
	        	$start = $BP?($BP-1):0;
    			$end = $BQ?$BQ:count($fineRecords);
    			for($i=$start; $i<$end; $i++){
    				$AV[] = $fineRecords[$i]['BarCode'];
    			}
	        }
	        if($summaryArr[4] == 'Y'){ //recall items BU
	        	
	        }
	        if($summaryArr[5] == 'Y'){ //unavailable holds CD
	        	$start = $BP?($BP-1):0;
    			$end = $BQ?$BQ:count($unavailableHoldBooks);
    			for($i=$start; $i<$end; $i++){
    				$CD[] = $unavailableHoldBooks[$i]['BarCode'];
    			}
	        }
	        
        	if($result['fixed']['language']=='027'){
		        $AF = '讀者螢幕訊息';
		        $AG = '讀者列印訊息';
	        }
	        else{
	        	$AF = 'ScreenMessage 0 for PatronID';
		        $AG = 'Print Line 1 for PatronID, Language 1';
	        }
        }
        //-- Add function here [End]
        
		$this->_newMessage('64'); 
		
		$this->_addFixedOption($PatronStatus, 14);
		$this->_addFixedOption($Language, 3);
		$this->_addFixedOption($TransactionDate, 18);
		$this->_addFixedOption($HoldCount, 4);
		$this->_addFixedOption($OverdueCount, 4);
		$this->_addFixedOption($ChargedCount, 4);
		$this->_addFixedOption($FineCount, 4);
		$this->_addFixedOption($RecallCount, 4);
		$this->_addFixedOption($UnavailableHoldsCount, 4);
		
		$this->_addVarOption('AO',$AO);
        $this->_addVarOption('AA',$AA);
        $this->_addVarOption('AE',$AE);
        $this->_addVarOption('BZ',$BZ);
        $this->_addVarOption('CA',$CA);
        $this->_addVarOption('CB',$CB);
        $this->_addVarOption('BL',$BL);
        if($CQ){
        	$this->_addVarOption('CQ',$CQ);
        }
        $this->_addVarOption('BE',$BE);
        
        foreach($AS as $aAS){
        	$this->_addVarOption('AS',$aAS);
        }
        foreach($AT as $aAT){
        	$this->_addVarOption('AT',$aAT);
        }
        foreach($AU as $aAU){
        	$this->_addVarOption('AU',$aAU);
        }
        foreach($AV as $aAV){
        	$this->_addVarOption('AV',$aAV);
        }
        foreach($CD as $aCD){
        	$this->_addVarOption('CD',$aCD);
        }
        
        $this->_addVarOption('AF',$AF);
        $this->_addVarOption('AG',$AG);    
 
        return $this->_returnMessage();
		// -- should be modified
}
function Get_Patron_Enable_Response($in){ // Message from 25
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'datestamp'    => substr($in,2,18),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 20);
        
        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        
        //return $result;
        $AO = $result['variable']['AO'][0];//$AO = 'InstitutionID for PatronID';
        $AA = $result['variable']['AA'][0];//$AA = 'PatronID';
        $TransactionDate = $this->_datestamp();
        $Language = ($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]?$_SESSION["language"][$_SERVER["REMOTE_ADDR"]]:'000');
        // -- should be modified
        $this->_newMessage('26'); //38 from the manual
        $this->_addFixedOption($PatronStatus, 14);
        $this->_addFixedOption($Language, 3);
        $this->_addFixedOption($TransactionDate, 18);
        
        $libms = new liblms();
        $sql = "Select UserID, EnglishName, ChineseName From LIBMS_USER WHERE BarCode = '".$AA."' ";
        $patronInfo = $libms->returnArray($sql);
        $AE = ($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'?$patronInfo[0]['ChineseName']:$patronInfo[0]['EnglishName']);
        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
	        $AF = '讀者螢幕訊息';
	        $AG = '讀者列印訊息';
        }
        else{
        	$AF = 'ScreenMessage 0 for PatronID';
	        $AG = 'Print Line 1 for PatronID, Language 1';
        }
        $this->_addVarOption('AO',$AO);
        $this->_addVarOption('AA',$AA);
        $this->_addVarOption('AE',$AE);
        //$this->_addVarOption('BL',$BL);
        $this->_addVarOption('AF',$AF);
        $this->_addVarOption('AG',$AG);
        
        return $this->_returnMessage();
        // -- should be modified
}
function Get_Item_Information_Response($in){ // Message from 17
	global $intranet_hardcode_lang, $sys_custom;
	$result['fixed'] = 
        array( 
	        'datestamp'    => substr($in,2,18),
        );
        
    $result['variable'] = $this->_parsevariabledata($in, 20);
    //return $result;
    if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
        	$intranet_hardcode_lang = 'b5';
    }
    else{
    	$intranet_hardcode_lang = 'en';
    }
    
	$AO = $result['variable']['AO'][0]; //Certification Institute ID
	$AB = $result['variable']['AB'][0]; //ItemBook
	$AH = '';
	
	$libms = new liblms();
    $sql = "SELECT lbu.UniqueID, lbu.BarCode, lb.BookTitle, lbu.RecordStatus, lb.CallNum, lb.CallNum2, lbu.LocationCode, lb.SeriesNum FROM LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID  WHERE lbu.BarCode = '".$AB."' ";
    $BookInfo = $libms->returnArray($sql);
	
	$sql = "Select DescriptionEn, DescriptionChi From LIBMS_LOCATION Where LocationCode = '".$BookInfo[0]['LocationCode']."'";
	$LocationInfo = current($libms->returnArray($sql));
	/* circulation status
	1  other 
    2  on order 
    3  available 
    4  charged 
    5  charged; not to be recalled until earliest recall date 
    6  in process 
    7  recalled 
    8  waiting on hold shelf 
    9  waiting to be re-shelved 
    10 in transit between library locations 
    11 claimed returned 
    12 lost 
    13 missing 
    
    $Lang["libms"]["book_status"]["NORMAL"] = "Available";
	$Lang["libms"]["book_status"]["SHELVING"] = "Processing";
	$Lang["libms"]["book_status"]["BORROWED"] = "on Loan";
	$Lang["libms"]["book_status"]["DAMAGED"] = "Damaged";
	$Lang["libms"]["book_status"]["REPAIRING"] = "Repairing";
	$Lang["libms"]["book_status"]["RESERVED"] = "on Reserve";
	$Lang["libms"]["book_status"]["RESTRICTED"] = "on Display";
	$Lang["libms"]["book_status"]["WRITEOFF"] = "Write-off";
	$Lang["libms"]["book_status"]["SUSPECTEDMISSING"] = "Suspected missing";
	$Lang["libms"]["book_status"]["LOST"] = "Lost";
	$Lang["libms"]["book_status"]["ORDERING"] = "on Order";
	
	$Lang["libms"]["book_status"]["NORMAL"] = "可借出";
$Lang["libms"]["book_status"]["SHELVING"] = "待上架";
$Lang["libms"]["book_status"]["BORROWED"] = "借出中";
$Lang["libms"]["book_status"]["DAMAGED"] = "損壞";
$Lang["libms"]["book_status"]["REPAIRING"] = "修復中";
$Lang["libms"]["book_status"]["RESERVED"] = "預留";
$Lang["libms"]["book_status"]["RESTRICTED"] = "展出中";
$Lang["libms"]["book_status"]["WRITEOFF"] = "註銷";
$Lang["libms"]["book_status"]["SUSPECTEDMISSING"] = "懷疑不見";
$Lang["libms"]["book_status"]["LOST"] = "遺失";
$Lang["libms"]["book_status"]["ORDERING"] = "訂購中";
	*/
	if($BookInfo[0]['UniqueID']){
		switch ($BookInfo[0]['RecordStatus']) {
		    case 'NORMAL':
		        $CirculationStatus = '03';
		        break;
		    case 'SHELVING':
		        $CirculationStatus = '09';
		        break;
		    case 'BORROWED':
		        $CirculationStatus = '04';
		        // get the due date [developed]
		        // -- get the due date [start]
				$sql = "Select DueDate From LIBMS_BORROW_LOG where RecordStatus = 'BORROWED' AND UniqueID = '".$BookInfo[0]['UniqueID']."' ORDER BY DueDate desc";
				$duedateResult = current($this->libms->returnArray($sql));
				$AH = $this->_datestamp(strtotime($duedateResult['DueDate']));
		    	// -- get the due date [end]
		        break;
		    case 'DAMAGED':
		        $CirculationStatus = '01';
		        break;
		    case 'REPAIRING':
		        $CirculationStatus = '01';
		        break;
		    case 'RESERVED':
		        $CirculationStatus = '08';
		        break;
		    case 'RESTRICTED':
		        $CirculationStatus = '01';
		        break;
		    case 'WRITEOFF':
		        $CirculationStatus = '01';
		        break;
		    case 'SUSPECTEDMISSING':
		        $CirculationStatus = '13';
		        break;
		    case 'LOST':
		        $CirculationStatus = '12';
		        break;
		    case 'ORDERING':
		        $CirculationStatus = '02';
		        break;
		    default:
		       $CirculationStatus = '01';
		}
	}
//	if($BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'NORMAL'){ 
//    	$CirculationStatus = '03';
//	}
//	else{
//		$CirculationStatus = '07';
//	}
    $SecurityMarker = '01';
    $FeeType = '01';
    $TransactionDate =  $this->_datestamp();
    $CF = '00000';
    $AJ = $BookInfo[0]['BookTitle']; //item title
    $CK = '001';
    if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
    	$AQ = $LocationInfo['DescriptionChi']; //Permanent Location for ItemBook, Language 1
	    $AP = $LocationInfo['DescriptionChi']; //Current Location ItemBook
	    //$CH = '項目特性';
	    
	    /* Collection code (field ID: CR)
	  Call number (field ID: CS)
	  Loan period (field ID: LP) */
	    $CR = $BookInfo[0]['SeriesNum'];
	    $CS = $BookInfo[0]['CallNum'].($BookInfo[0]['CallNum2']?' '.$BookInfo[0]['CallNum2']:'');
	    $LP = 'Loan period';
    }
    else{
    	$AQ = $LocationInfo['DescriptionEn']; //Permanent Location for ItemBook, Language 1
	    $AP = $LocationInfo['DescriptionEn']; //Current Location ItemBook
	    //$CH = 'Free-form text with new item property';
	    
	    /* Collection code (field ID: CR)
	  Call number (field ID: CS)
	  Loan period (field ID: LP) */
	    $CR = $BookInfo[0]['SeriesNum'];
	    $CS = $BookInfo[0]['CallNum'].($BookInfo[0]['CallNum2']?' '.$BookInfo[0]['CallNum2']:'');
	    $LP = 'Loan period';
    }
    $CH = 'CR'.$CR.'^CS'.$CS.'^LP'.$LP;
    
    // -- should be modified
    $this->_newMessage('18');
    $this->_addFixedOption($CirculationStatus, 2);
    $this->_addFixedOption($SecurityMarker, 2);
    $this->_addFixedOption($FeeType, 2);
    $this->_addFixedOption($TransactionDate, 18);
    
    $this->_addVarOption('CF',$CF);
    $this->_addVarOption('AH',$AH);
    $this->_addVarOption('AB',$AB);
    $this->_addVarOption('AJ',$AJ);
    $this->_addVarOption('CK',$CK);
    $this->_addVarOption('AQ',$AQ);
    $this->_addVarOption('AP',$AP);
    $this->_addVarOption('CH',$CH);
    //$this->_addVarOption('CS',$CS);
        
    return $this->_returnMessage();
   // -- should be modified
}
function Get_Checkout_Response($in){ // Message from 11
	global $sys_custom, $Lang, $intranet_hardcode_lang, $User;
	
	$result['fixed'] = 
        array( 
	        'scRenewal'    => substr($in,2,1),
	        'noBlock'     => substr($in,3,1),
	        'datestamp'     => substr($in,4,18),
	        'nbDateDue'     => substr($in,22,18),
        );
        
    $result['variable'] = $this->_parsevariabledata($in, 40);
    //return $result;
    if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
    	$intranet_hardcode_lang = 'b5';
    }
    else{
    	$intranet_hardcode_lang = 'en';
    }
        
    $AO = $result['variable']['AO'][0];
    $AA = $result['variable']['AA'][0]; //user barcode
    $AB = $result['variable']['AB'][0];	//item barcode
    $AC = $result['variable']['AC'][0];
    $AD = $result['variable']['AD'][0];
    $BI = $result['variable']['BI'][0];
        
    // -- should be modified
    //$libms = new liblms();
    $sql = "SELECT lbu.UniqueID, lbu.BookID, lbu.BarCode, lb.BookTitle, lbu.RecordStatus FROM LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID  WHERE lbu.BarCode = '".$AB."' ";
    $BookInfo = $this->libms->returnArray($sql);
    
    $result2 = false;
    
    $sql = "Select UserID, EnglishName, ChineseName From LIBMS_USER WHERE BarCode = '".$AA."' ";
    if($result['fixed']['scRenewal'] == 'Y' && $BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'BORROWED')
    	$sql = "Select lu.UserID, lu.EnglishName, lu.ChineseName From LIBMS_USER lu JOIN LIBMS_BORROW_LOG lbl ON lu.UserID = lbl.UserID WHERE lu.BarCode = '".$AA."' AND lbl.UniqueID = '".$BookInfo[0]['UniqueID']."' AND lbl.RecordStatus = 'BORROWED' ";
    $patronInfo = $this->libms->returnArray($sql);
    
    $passwordMatch = $this->Check_User_Password($AA,$AD);
    
    if(!$passwordMatch){
    	$patronInfo = false;
    }
    
    // -- check the open for loan to the user [added on 20170224]
    $is_open_loan = $this->libms->IsSystemOpenForLoan($patronInfo[0]['UserID']);
    if(!$is_open_loan){
    	$patronInfo = false;
    }
    
    // -- cancel the checkin
    if($BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'NORMAL' && $patronInfo && $BI == 'Y'){
    	//code here...
    	$sql = "Select BorrowLogID, DueDate From LIBMS_BORROW_LOG 
					Where UniqueID = '".$BookInfo[0]['UniqueID']."' 
					AND UserID = '".$patronInfo[0]['UserID']."' Order By BorrowLogID desc";
		$borrowRecord = current($this->libms->returnArray($sql));
		$borrowLogID = $borrowRecord['BorrowLogID'];
		
		if($borrowLogID > 0){
			$sql = "UPDATE LIBMS_BORROW_LOG SET RecordStatus = 'BORROWED', ReturnedTime = '0000-00-00 00:00:00' WHERE BorrowLogID ='".$borrowLogID."'";
			$result2 = $this->libms->db_db_query($sql);
			
			$sql = "Select Payment From LIBMS_OVERDUE_LOG where BorrowLogID ='".$borrowLogID."'";
			$payment = current($this->libms->returnArray($sql));
			$payment = $payment['Payment'];
			
			if($payment > 0){
				$sql = "UPDATE LIBMS_USER Set Balance = Balance + ".$payment." Where UserID = '".$patronInfo[0]['UserID']."' ";
				$result2 = $this->libms->db_db_query($sql);
				$sql = "DELETE FROM LIBMS_OVERDUE_LOG WHERE BorrowLogID ='".$borrowLogID."'";
				$result2 = $this->libms->db_db_query($sql);
			}
			
			//update number of book
			$result2 = $this->RecordManager->updateBook($BookInfo[0]['BookID'],array( 'NoOfCopyAvailable' => '`NoOfCopyAvailable` - 1'));
			$result2 = $this->RecordManager->updateUnquieBook($BookInfo[0]['UniqueID'],array('RecordStatus' =>  PHPToSQL('BORROWED')));
		}
    }
    else if($BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'NORMAL' && $patronInfo){
    	$User = new User($patronInfo[0]['UserID'], $this->libms);
    	
//    	$sql = "UPDATE LIBMS_BOOK_UNIQUE Set RecordStatus = 'BORROWED' Where UniqueID = '".$BookInfo[0]['UniqueID']."' ";//NORMAL
//    	$result = $libms->db_db_query($sql);
		$MSG = '';
		$canBorrow=$User->rightRuleManager->canBorrow($BookInfo[0]['UniqueID'],$MSG);
		if ($canBorrow)
		{
			$errormsg =	$Lang['libms']['CirculationManagement']['y_borrow'];
			$reserves = $User->recordManager->getReservationByBookID($BookInfo[0]['BookID'],array('WAITING','READY'), "asc", $BookInfo[0]['UniqueID']);
			if ($reserves["ReserveTime"]!="")
			{
				$DateReservedOn = date("Y-m-d", strtotime($reserves["ReserveTime"]));
			}
			if ($DateReservedOn!="")
			{
				$errormsg .= ' ('.$Lang["libms"]["CirculationManagement"]["booked_alert"]." [".$DateReservedOn."]".')';
			}
		} 
		else
		{
			$errormsg =	$Lang['libms']['CirculationManagement']['n_borrow'].' ('.$MSG.')';
		}
		if($result['fixed']['noBlock'] == 'Y'/* && $result['fixed']['nbDateDue']*/){
			$borrowDateTime = strtotime($result['fixed']['datestamp']);
		}
    	$result2 = $User->borrow_book($BookInfo[0]['UniqueID'], false, 0, 0, $borrowDateTime);
    }
    else if($result['fixed']['scRenewal'] == 'Y' && $BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'BORROWED' && $patronInfo){
    	$User = new User($patronInfo[0]['UserID'], $this->libms);
		// do renew here ...
		$errormsg = '';
		if($result['fixed']['noBlock'] == 'Y'/* && $result['fixed']['nbDateDue']*/){
			$borrowDateTime = strtotime($result['fixed']['datestamp']);
		}
		$book['canRenew'] = $User->rightRuleManager->canRenew($BookInfo[0]['UniqueID'], $errormsg, $borrowDateTime);
		if($book['canRenew'])
			$result2 = $User->renew_book($BookInfo[0]['UniqueID'],true,$borrowDateTime);
    }
//    debug_pr($result['fixed']['scRenewal'] == 'Y');
    if($result2){
    	$Ok = 1;
    	$Desensitize = 'Y';
    	
    	if($BI == 'Y'){
    		$dueDate = strtotime($borrowRecord['DueDate']);
    	}
    	else{
    	// -- get the due date [start]
    	$timeManager = new TimeManager();
		$strToday =  ($borrowDateTime?date( 'Y-m-d',$borrowDateTime ):date( 'Y-m-d' ));
		
		$BookManager = new BookManager($this->libms);
		$circulationTypeCode = $BookManager->getCirculationTypeCodeFromUniqueBookID($BookInfo[0]['UniqueID']);
		
		$limits = $User->rightRuleManager->get_limits($circulationTypeCode);
		
		
		# special customization for a KIS client tbcpk
		if ($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday']!="" && isset($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday']))
		{
			$weekday_today = ($borrowDateTime?date( "w",$borrowDateTime ):date("w"));
			if ($weekday_today<$sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'])
			{
				$daysDifference = $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
			} else
			{
				$daysDifference = 7 + $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
			}
			
			$limits['ReturnDuration'] = $daysDifference;
		}
		
		if ($this->libms->get_system_setting("circulation_duedate_method"))
		{
			$dueDate = $timeManager->findOpenDateSchoolDays(strtotime($strToday), $limits['ReturnDuration']);
		} else
		{
			$dueDate = $timeManager->findOpenDate(strtotime("{$strToday} + {$limits['ReturnDuration']} days"), 0, '+1 day', $borrowDateTime);
		}
    	// -- get the due date [end]
    	
    	// -- handle no block due date [start]
    	if($result['fixed']['noBlock'] == 'Y' && trim($result['fixed']['nbDateDue'])){
    		$updateSuccess = $this->Update_Due_Date($BookInfo[0]['UniqueID'], date("Y-m-d", strtotime(substr($result['fixed']['nbDateDue'], 0, 8))));
    		if($updateSuccess){
    			$dueDate = strtotime(substr($result['fixed']['nbDateDue'], 0, 8));
    		}
    	}
    	// -- handle no block due date [end]
    	}
    	
//    	$AH = $this->_datestamp(strtotime("+7 day"));
//    	$AG = 'Borrow Success! Please return item on of before '.date('Y-m-d',strtotime("+7 day"));
    	$AH = $this->_datestamp($dueDate);
    	if($result['fixed']['scRenewal'] == 'Y' && $BookInfo[0]['RecordStatus'] == 'BORROWED'){
    		if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
    			$AG = '續借成功! 請於到期日或以前歸還: '.date('Y-m-d',$dueDate);
    		}
    		else{
    			$AG = 'Renew Success! Please return item on of before '.date('Y-m-d',$dueDate);
    		}
    		$RenewalOk = 'Y';
    	}
    	else{
    		if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
    			$AG = '借出成功! 請於到期日或以前歸還: '.date('Y-m-d',$dueDate);
    		}
    		else{
    			$AG = 'Borrow Success! Please return item on of before '.date('Y-m-d',$dueDate);
    		}
    		$RenewalOk = 'N';
    	}
    	$AF = $AG;
	}
	else{
		$Ok = 0;
		$Desensitize = 'N';
		//$AH = $this->_datestamp(strtotime("+7 day"));
		if(!$is_open_loan){ //[added on 20170224]
			$AG = strip_tags($this->libms->CloseMessage);
		} 
		else if($passwordMatch && $BookInfo[0]['UniqueID'] && $result['fixed']['scRenewal'] == 'Y' && $BookInfo[0]['RecordStatus'] == 'BORROWED'){
//			$AG = 'Cannot borrow this item! The item is '.$Lang["libms"]["book_status"][$BookInfo[0]['RecordStatus']].'!';
			if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
				$AG = '不能借出此項目! '.$errormsg;
			}
			else{
				$AG = 'Cannot borrow this item! '.$errormsg;
			}
		}
		else if($passwordMatch && $BookInfo[0]['UniqueID']){
			if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
				$AG = '不能續借此項目! 項目狀態 '.$Lang["libms"]["book_status"][$BookInfo[0]['RecordStatus']].'!';
			}
			else{
				$AG = 'Cannot renew this item! The item is '.$Lang["libms"]["book_status"][$BookInfo[0]['RecordStatus']].'!';
			}
		}
		else{
			if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
				$AG = '條碼或密碼不正確!';
			}
			else{
				$AG = 'Barcode or password incorrect!';
			}
		}
		$AF = $AG;
		$RenewalOk = 'N';
	}
    //$RenewalOk = 'N';
    $Magnetic = 'N';
    //$Desensitize = 'Y';
    $TransactionDate = $this->_datestamp();
    $AJ = $BookInfo[0]['BookTitle'];
    //$AH = $this->_datestamp(strtotime("+7 day"));
//    $AF = 'Cannot borrow this book!';
//    $AG = 'Borrow Success! Please return item on of before '.date('Y-m-d',strtotime("+7 day"));
    
    $this->_newMessage('12');
    $this->_addFixedOption($Ok, 1);
    $this->_addFixedOption($RenewalOk, 1);
    $this->_addFixedOption($Magnetic, 1);
    $this->_addFixedOption($Desensitize, 1);
    $this->_addFixedOption($TransactionDate, 18);
    
    $this->_addVarOption('AO',$AO);
    $this->_addVarOption('AA',$AA);
    $this->_addVarOption('AB',$AB);
    $this->_addVarOption('AJ',$AJ); // boot title
    $this->_addVarOption('AH',$AH); //due date
    $this->_addVarOption('AF',$AF);
    $this->_addVarOption('AG',$AG);
    
    return $this->_returnMessage();
    // -- should be modified
}
function Get_Checkin_Response($in){ // Message from 09
	global $Lang, $intranet_hardcode_lang;
	
	$result['fixed'] = 
        array( 
	        'noBlock'    => substr($in,2,1),
	        'datestamp'     => substr($in,3,18),
	        'itmReturnDate'     => substr($in,21,18),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 39);
        
        $AP = $result['variable']['AP'][0];
        $AO = $result['variable']['AO'][0];
        $AB = $result['variable']['AB'][0]; // item barcode
        $AC = $result['variable']['AC'][0];
        $BI = $result['variable']['BI'][0];
        //return $result;
        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        // -- retrun book process [start]
	    $sql = "SELECT lbu.UniqueID, lbu.BarCode, lb.BookTitle, lbu.RecordStatus, lbu.BookID FROM LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID  WHERE lbu.BarCode = '".$AB."' ";
	    $BookInfo = $this->libms->returnArray($sql);
	    
	    $returnResult = false;
	    
	    // -- cancel the checkout
	    if($BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'BORROWED' && $BI == 'Y'){
	    	//code here...
	    	$sql = "Select BorrowLogID, DueDate From LIBMS_BORROW_LOG 
					Where UniqueID = '".$BookInfo[0]['UniqueID']."' 
					AND RecordStatus = 'BORROWED' Order By BorrowLogID desc";
			$borrowRecord = current($this->libms->returnArray($sql));
			$borrowLogID = $borrowRecord['BorrowLogID'];
			
			if($borrowLogID > 0){
				$sql = "DELETE FROM LIBMS_BORROW_LOG WHERE BorrowLogID ='".$borrowLogID."'";
				$returnResult = $this->libms->db_db_query($sql);
				
				//update number of book
			$returnResult = $this->RecordManager->updateBook($BookInfo[0]['BookID'],array( 'NoOfCopyAvailable' => '`NoOfCopyAvailable` + 1'));
			$returnResult = $this->RecordManager->updateUnquieBook($BookInfo[0]['UniqueID'],array('RecordStatus' =>  PHPToSQL('NORMAL')));
			}
	    }
	    else if($BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'BORROWED'){
	    	$uID = $this->RecordManager->getUserIDByBookUniqueID($BookInfo[0]['UniqueID']);
	    	$HandleOverdue = 0;
	    	if($result['fixed']['noBlock'] == 'Y'/* && $result['fixed']['itmReturnDate']*/){
	    		$today = strtotime(trim($result['fixed']['itmReturnDate'])?$result['fixed']['itmReturnDate']:$result['fixed']['datestamp']);
	    		$HandleOverdue = 1;
	    	}
			$result = $this->RecordManager->returnBook($BookInfo[0]['UniqueID'], 'return_book', 'ByBatch', $uID, $today, $HandleOverdue);
			extract($result);
//			debug_pr($result);
//			$barcode = $bookInfo['UniqueBarCode'];
//			if ($returnResult == false) {
////				$error = true;
////				$errorBookUniqueID[] = $bookUniqueID;
//			}
//			else {
//				$json['success'] = true;
//			}
//			if (empty($ID)) {
//				$ID = $rowNo;
//			}
//			$rowNo++;
//			$view_data = compact('bookInfo','borrowLog','return_row_class','IsReturnBookWithoutBorrowLog','borrow_by_user','alert_text','not_allow_overdue_return','isOverDue','overDue','BookLocation','reserves','User','ID','barcode','payment_text_link','bookUniqueID','junior_mck');
////error_log("\n\n bookUniqueID-->".print_r($bookUniqueID,true)."<---- ".date("Y-m-d H:i:s")." f:".__FILE__." fun:".__FUNCTION__." line : ".__LINE__."\n", 3, "/tmp/ccc.txt");
//			
//			$list .= $this->RecordManager->gen_return_book_list($view_data);
	    }
	    // -- retrun book process [end]
	    
	    if($returnResult){
	    	$Ok = 1;
	        $Resensitize = 'N';
	        $Magnetic = 'N';
	        $Alert = 'N';
	        
	        $LocationInfo = $this->libms->GET_LOCATION_INFO($bookInfo['LocationCode']);
	        
//	        $AQ = "Permanent Location for CheckinBook, Language 1|";
			if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
	        	$AQ = $LocationInfo[0]['DescriptionChi'];
	        	$CL = "分類箱 A1";
		        $AF = "還書的螢幕訊息";
		        $AG = "還書的列印訊息";
			}
			else{
				$AQ = $LocationInfo[0]['DescriptionEn'];
				$CL = "sort bin A1";
		        $AF = "Screen Message for CheckInBook";
		        $AG = "Print Line for CheckInBook";
			}
	        $AJ = $bookInfo['BookTitle'];
	        $User = new User($uID, $this->libms);
	        
	        $AA = $User->userInfo['BarCode'];
	        $CK = "001"; //book
	        //$CH = "CheckinBook Properties";
	        $CH = $bookInfo['RemarkToUser'].($bookInfo['AccompanyMaterial']!=''?' ('.$bookInfo['AccompanyMaterial'].')':'');
	    }
	    else{
	    	$Ok = 0;
	        $Resensitize = 'N';
	        $Magnetic = 'N';
	        $Alert = 'N';
	        $AQ = '';
	        $AJ = '';
	        $AA = '';
	        $CK = '';
	        //$CH = "CheckinBook Properties";
	        $CH = '';
	        $CL = '';
	        if($BookInfo[0]['UniqueID']){
	        	if($isOverDue){
	        		if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
	        			$isOverDueStr = ' 項目已逾期!';
	        		}
		        	else{
		        		$isOverDueStr = ' This item is overdue!';
		        	}
	        	}
	        	else if($isSameDayReturn){
	        		if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
	        			$isOverDueStr = ' 不能即日借還!';
	        		}
		        	else{
		        		$isOverDueStr = ' Return at the same day is not allowed!';
		        	}
	        	}
	        	if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
					$AF = '不能歸還此項目! 項目狀態 '.$Lang["libms"]["book_status"][$BookInfo[0]['RecordStatus']].'!'.$isOverDueStr;
	        	}
	        	else{
	        		$AF = 'Cannot check-in the item! The item is '.$Lang["libms"]["book_status"][$BookInfo[0]['RecordStatus']].'!'.$isOverDueStr;
	        	}
	        }
			else{
				if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
					$AF = '條碼不正確!';
				}
				else{
					$AF = 'Barcode incorrect!';
				}
			}
	        $AG = $AF;
	    }

        $TransactionDate =  $this->_datestamp();
        
        
        // -- should be modified
    	$this->_newMessage('10');
    	$this->_addFixedOption($Ok, 1);
    	$this->_addFixedOption($Resensitize, 1);
    	$this->_addFixedOption($Magnetic, 1);
    	$this->_addFixedOption($Alert, 1);
    	$this->_addFixedOption($TransactionDate, 18);
    	
    	$this->_addVarOption('AO',$AO);
    	$this->_addVarOption('AB',$AB);
    	$this->_addVarOption('AQ',$AQ);
    	$this->_addVarOption('AJ',$AJ);
    	$this->_addVarOption('AA',$AA);
    	$this->_addVarOption('CK',$CK);
    	$this->_addVarOption('CH',$CH);
    	$this->_addVarOption('CL',$CL);
    	$this->_addVarOption('AF',$AF);
    	$this->_addVarOption('AG',$AG);
    	
        return $this->_returnMessage();
        // -- should be modified
}
function Get_Fee_Paid_Response($in){ // Message from 37
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'datestamp'    => substr($in,2,18),
	        'feeType'     => substr($in,20,2),
	        'pmtType'     => substr($in,22,2),
	        'curType'     => substr($in,24,3),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 27);
        return $result;
        
        // -- should be modified
        $this->_newMessage('38');
        $this->_addFixedOption($PaymentAccepted, 1);
        $this->_addFixedOption($TransactionDate, 18);
        
        $this->_addVarOption('AO',$AO);
        $this->_addVarOption('AA',$AA);
        $this->_addVarOption('AF',$AF);
        $this->_addVarOption('AG',$AG);
 
        return $this->_returnMessage();
        // -- should be modified
}
function Get_End_Session_Response($in){ // Message from 35
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'datestamp'    => substr($in,2,18),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 20);
        //return $result;
        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        
        $EndSession = "Y";
        $TransactionDate = $this->_datestamp();
        $AO = $result['variable']['AO'][0];
    	$AA = $result['variable']['AA'][0]; //user barcode
    	if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
	        $AF = "讀者的螢幕訊息";
		    $AG = "讀者的列印訊息";
    	}
    	else{
    		$AF = "Screen Message 0 for PatronID, Language 1";
	        $AG = "Print Line 0 for PatronID, Language 1";
    	}
        // -- should be modified
        $this->_newMessage('36');
        $this->_addFixedOption($EndSession, 1);
        $this->_addFixedOption($TransactionDate, 18);
        
		$this->_addVarOption('AO',$AO);
		$this->_addVarOption('AA',$AA);
		$this->_addVarOption('AF',$AF);
		$this->_addVarOption('AG',$AG);
        
        return $this->_returnMessage();
        // -- should be modified
}
function Get_Item_status_update_Response($in){ // Message from 19
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'datestamp'    => substr($in,2,18),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 20);
        return $result;
        
        // -- should be modified
        $this->_newMessage('20');
        $result['fixed'] = 
        array( 
        'PropertiesOk'      => substr($response, 2, 1),
        'TransactionDate'   => substr($response, 3, 18),
        );    
 
        $result['variable'] = $this->$this->_parsevariabledata($response, 21);
        return $result;
        // -- should be modified
}
function Get_Hold_Response($in){ // Message from 15
	global $intranet_hardcode_lang;
	$result['fixed'] = 
        array( 
	        'mode'    => substr($in,2,1),
	        'datestamp'    => substr($in,3,18),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 21);
        return $result;
    
    // -- should be modified
    $this->_newMessage('16');    
    $result['fixed'] = 
        array( 
        'Ok'                => substr($response, 2, 1),
        'available'         => substr($response, 3, 1),
        'TransactionDate'   => substr($response, 4, 18),
        'ExpirationDate'    => substr($response, 22, 18)            
        );    
 
 
        $result['variable'] = $this->$this->_parsevariabledata($response, 40);
 
        return $result;
    // -- should be modified
}
function Get_Renew_Response($in){ // Message from 29
	global $sys_custom, $Lang, $intranet_hardcode_lang, $User;

	$result['fixed'] = 
        array( 
	        'thirdParty'    => substr($in,2,1),
	        'noBlock'    => substr($in,3,1),
	        'datestamp'    => substr($in,4,18),
	        'nbDateDue'    => substr($in,22,18),
        );
        
        $result['variable'] = $this->_parsevariabledata($in, 40);
        
        if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
        	$intranet_hardcode_lang = 'b5';
        }
        else{
        	$intranet_hardcode_lang = 'en';
        }
        
        $AO = $result['variable']['AO'][0];
		$AA = $result['variable']['AA'][0];
		$AD = $result['variable']['AD'][0];
		$AB = $result['variable']['AB'][0];
		$AJ = $result['variable']['AJ'][0];
		$AC = $result['variable']['AC'][0];
		
    // -- should be modified
    $sql = "SELECT lbu.UniqueID, lbu.BookID, lbu.BarCode, lb.BookTitle, lbu.RecordStatus FROM LIBMS_BOOK_UNIQUE as lbu JOIN LIBMS_BOOK as lb ON lbu.BookID = lb.BookID  WHERE lbu.BarCode = '".$AB."' ";
    $BookInfo = $this->libms->returnArray($sql);
    
    $result2 = false;
    
    $sql = "Select lu.UserID, lu.EnglishName, lu.ChineseName From LIBMS_USER lu JOIN LIBMS_BORROW_LOG lbl ON lu.UserID = lbl.UserID WHERE lu.BarCode = '".$AA."' AND lbl.UniqueID = '".$BookInfo[0]['UniqueID']."' AND lbl.RecordStatus = 'BORROWED' ";
    $patronInfo = $this->libms->returnArray($sql);
    
    $passwordMatch = $this->Check_User_Password($AA,$AD);
    
    if(!$passwordMatch){
    	$patronInfo = false;
    }
    
    if($BookInfo[0]['UniqueID'] && $BookInfo[0]['RecordStatus'] == 'BORROWED' && $patronInfo){
    	$User = new User($patronInfo[0]['UserID'], $this->libms);
		// do renew here ...
		$errormsg = '';
		if($result['fixed']['noBlock'] == 'Y'/* && $result['fixed']['nbDateDue']*/){
			$renewDateTime = strtotime($result['fixed']['datestamp']);
		}
		$book['canRenew'] = $User->rightRuleManager->canRenew($BookInfo[0]['UniqueID'], $errormsg, $renewDateTime);
		if($book['canRenew'])
			$result2 = $User->renew_book($BookInfo[0]['UniqueID'],true,$renewDateTime);
    }
//    debug_pr($result['fixed']['scRenewal'] == 'Y');
    if($result2){
    	$Ok = 1;
    	$Desensitize = 'Y';
    	// -- get the due date [start]
    	$timeManager = new TimeManager();
		$strToday =  ($renewDateTime?date( 'Y-m-d',$renewDateTime ):date( 'Y-m-d' ));
		
		$BookManager = new BookManager($this->libms);
		$circulationTypeCode = $BookManager->getCirculationTypeCodeFromUniqueBookID($BookInfo[0]['UniqueID']);
		
		$limits = $User->rightRuleManager->get_limits($circulationTypeCode);
		
		
		# special customization for a KIS client tbcpk
		if ($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday']!="" && isset($sys_custom['eLibraryPlus']['fixed_return_day_on_weekday']))
		{
			$weekday_today = ($renewDateTime?date( "w",$renewDateTime ):date("w"));
			if ($weekday_today<$sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'])
			{
				$daysDifference = $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
			} else
			{
				$daysDifference = 7 + $sys_custom['eLibraryPlus']['fixed_return_day_on_weekday'] - $weekday_today;
			}
			
			$limits['ReturnDuration'] = $daysDifference;
		}
		
		if ($this->libms->get_system_setting("circulation_duedate_method"))
		{
			$dueDate = $timeManager->findOpenDateSchoolDays(strtotime($strToday), $limits['ReturnDuration']);
		} else
		{
			$dueDate = $timeManager->findOpenDate(strtotime("{$strToday} + {$limits['ReturnDuration']} days"), 0, '+1 day', $renewDateTime);
		}
    	// -- get the due date [end]
    	
    	// -- handle no block due date [start]
    	if($result['fixed']['noBlock'] == 'Y' && trim($result['fixed']['nbDateDue'])){
    		$updateSuccess = $this->Update_Due_Date($BookInfo[0]['UniqueID'], date("Y-m-d", strtotime(substr($result['fixed']['nbDateDue'], 0, 8))));
    		if($updateSuccess){
    			$dueDate = strtotime(substr($result['fixed']['nbDateDue'], 0, 8));
    		}
    	}
    	// -- handle no block due date [end]
    	
//    	$AH = $this->_datestamp(strtotime("+7 day"));
//    	$AG = 'Borrow Success! Please return item on of before '.date('Y-m-d',strtotime("+7 day"));
    	$AH = $this->_datestamp($dueDate);
    	if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
    		$AG = '續借成功! 請於到期日或以前歸還: '.date('Y-m-d',$dueDate);
    	}
    	else{
    		$AG = 'Renew Success! Please return item on of before '.date('Y-m-d',$dueDate);
    	}
    	$AF = $AG;
    	$RenewalOk = 'Y';
	}
	else{
		$Ok = 0;
		$Desensitize = 'N';
		//$AH = $this->_datestamp(strtotime("+7 day"));
		if($passwordMatch && $BookInfo[0]['UniqueID']){
			if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
				$AG = '不能續借此項目! 項目狀態 '.$Lang["libms"]["book_status"][$BookInfo[0]['RecordStatus']].'!';
			}
			else{
				$AG = 'Cannot renew this item! The item is '.$Lang["libms"]["book_status"][$BookInfo[0]['RecordStatus']].'!';
			}
		}
		else{
			if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
				$AG = '條碼或密碼不正確!';
			}
			else{
				$AG = 'Barcode or password incorrect!';
			}
		}
		$AF = $AG;
		$RenewalOk = 'N';
	}
    $Magnetic = 'N';
    //$Desensitize = 'Y';
    $TransactionDate = $this->_datestamp();
    $AJ = $BookInfo[0]['BookTitle'];
    
    $this->_newMessage('30');
    $this->_addFixedOption($Ok, 1);
    $this->_addFixedOption($RenewalOk, 1);
    $this->_addFixedOption($Magnetic, 1);
    $this->_addFixedOption($Desensitize, 1);
    $this->_addFixedOption($TransactionDate, 18);
    
    $this->_addVarOption('AO',$AO);
    $this->_addVarOption('AA',$AA);
    $this->_addVarOption('AB',$AB);
    $this->_addVarOption('AJ',$AJ); // boot title
    $this->_addVarOption('AH',$AH); //due date
    $this->_addVarOption('AF',$AF);
    $this->_addVarOption('AG',$AG);
    
    return $this->_returnMessage();
    // -- should be modified
}
function Get_Renew_All_Response($in){  // Message from 65
	global $intranet_hardcode_lang, $User;
	$result['fixed'] = 
        array( 
	        'datestamp'    => substr($in,2,18)
      	);
	$result['variable'] = $this->_parsevariabledata($in, 20);
	
	if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]] == '027'){
    	$intranet_hardcode_lang = 'b5';
    }
    else{
    	$intranet_hardcode_lang = 'en';
    }
        
    $AO = $result['variable']['AO'][0];
	$AA = $result['variable']['AA'][0];
	$AD = $result['variable']['AD'][0];
	$AC = $result['variable']['AC'][0];
	$BO = $result['variable']['BO'][0];
    
    // -- get patron info
    $sql = "Select lu.UserID, lu.EnglishName, lu.ChineseName, lbl.UniqueID, lbu.BarCode From LIBMS_BORROW_LOG lbl JOIN LIBMS_BOOK_UNIQUE lbu ON lbu.UniqueID = lbl.UniqueID JOIN LIBMS_USER lu ON lbl.UserID = lu.UserID where lbl.RecordStatus = 'BORROWED' AND lu.BarCode = '".$AA."' ";  
    //$sql = "Select UserID, EnglishName, ChineseName From LIBMS_USER WHERE BarCode = '".$AA."' ";
    $patronInfo = $this->libms->returnArray($sql);
    
    if(!$patronInfo[0][0]){
    	$Ok = '0';
    	$Renewed = '0000';
    	$Unrenewed = '0000';
    }
    else{
    	$User = new User($patronInfo[0][0], $this->libms);
		
		$Renewed = array();
		$Unrenewed = array();
		$errormsg = '';
		
		foreach ($patronInfo as $aPatronInfo){
			if(!empty($aPatronInfo['UniqueID']) && $User->rightRuleManager->canRenew($aPatronInfo['UniqueID'], $errormsg)){
				$result2[$aPatronInfo['UniqueID']]=$User->renew_book($aPatronInfo['UniqueID'],true);
				if($result2[$aPatronInfo['UniqueID']]){
					$Renewed[] = $aPatronInfo['BarCode'];
				}
				else{
					$Unrenewed[] = $aPatronInfo['BarCode'];
				}
			}
			else{
				$Unrenewed[] = $aPatronInfo['BarCode'];
			}
		}
    }
    
    $this->_newMessage('66');
    $this->_addFixedOption($Ok, 1); //Ok
    $this->_addFixedOption(count($Renewed), 1); //Renewed
    $this->_addFixedOption(count($Unrenewed), 1); //Unrenewed
    $this->_addFixedOption($this->_datestamp(), 18); //TransactionDate   
 
	$this->_addVarOption('AO',$AO);
	foreach($Renewed as $aRenew){
		$this->_addVarOption('BM',$aRenew);
	}
	foreach($Unrenewed as $aUnrenew){
		$this->_addVarOption('BN',$aUnrenew);
	}
	if($_SESSION["language"][$_SERVER["REMOTE_ADDR"]]=='027'){
		$AF = '已續借頂目條碼: '.implode(', ',$Renewed).'; 未能續借頂目條碼: '.implode(', ',$Unrenewed);
	}
	else{
		$AF = 'Renewed Book Barcode(s): '.implode(', ',$Renewed).'; Unrenewed Book Barcode(s): '.implode(', ',$Unrenewed);
	}
	$AG = $AF;
	$this->_addVarOption('AF',$AF);
    $this->_addVarOption('AG',$AG);
	 
    return $this->_returnMessage();
}

function Check_User_Password($barcode, $password){
	global $intranet_db, $intranet_password_salt;
	$sql = "Select UserLogin From ".$intranet_db.".INTRANET_USER WHERE BarCode = '".$barcode."'";
	$UserLogin = current($this->libms->returnArray($sql));
	
   // debug_pr($UserLogin);
    if($UserLogin){
		if(strtolower($UserLogin['UserLogin'])=="broadlearning"){              	
			# get the password from central server
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_REFERER, $_SERVER["SERVER_NAME"]);
			@curl_setopt($ch, CURLOPT_URL, "http://eclassupdate.broadlearning.com/api/blpswd.php?ptype=3");
			
			// grab URL and pass it to the browser
			$password_central = trim(curl_exec($ch));
			
			//$account_name = (strtolower($user)=="broadlearning") ? "ac_broadlearning" : "ac_btw";
			$account_name =  "ac_broadlearning";
		
			if ($password_central!="" && strlen($password_central)>3)
			{
				if (md5("2012allschools".$password)==$password_central) {
					return true;
				}
				else {
					return false;
				}
			}
	    }
    	
		$sql = "Select UserID From ".$intranet_db.".INTRANET_USER WHERE BarCode = '".$barcode."' AND HashedPass = '".MD5($UserLogin['UserLogin'].$password.$intranet_password_salt)."'";
    	$UserInfo = current($this->libms->returnArray($sql));
    	//debug_pr(MD5($UserLogin['UserLogin'].$password.$intranet_password_salt));
    	if($UserInfo){
    		return true;
    	}
    	else{
    		return false;
    	}
    }
    else{
    	return false;
    }
}

function Update_Due_Date($BookUniqueID, $DueDate){
	$sql = "UPDATE LIBMS_BORROW_LOG Set DueDate = '".$DueDate."' WHERE RecordStatus='BORROWED' AND UniqueID='{$BookUniqueID}'";
	return $this->libms->db_db_query($sql);
}

function Handle_SIP2_Message($in){
	global $intranet_hardcode_lang;
	$out = '';
	# get the message code (first 2 character)
	$msg_code = substr($in, 0, 2);
	//$msg_content = substr($in, 2, strlen($in)-2);
	$intranet_hardcode_lang = 'en';
	switch($msg_code){
	    case "93":
	        $out = $this->Get_Login_Response($in);
	        break;
	    case "99":
	        $out = $this->Get_ACS_Status($in);
	        break;
	    case "97":
	        //$out = $this->Handle_SIP2_Message($_SESSION["prev_input"][$_SERVER["REMOTE_ADDR"]]);
	        $out = $_SESSION["prev_input"][$_SERVER["REMOTE_ADDR"]];
	        break;
	    case "23":
	    case "01":
	        $out = $this->Get_Patron_Status_Response($in);
	        break;
	    case "63":
	    	//return "64              00119980723 104009000100000002000100020000AOInstitutionID for PatronID|AAPatronID|AEPatron Name|BZ0002|CA0003|CB0010|BLY|ASItemID1 for PatronID|AUChargeItem1|AUChargeItem2|BDHome Address|BEE Mail Address|BFHome Phone for PatronID|AFScreenMessage 0 for PatronID, Language 1|AFScreen Message 1 for PatronID, Language 1|AFScreen Message 2 for PatronID, Language 1|AGPrint Line 0 for PatronID, Language 1|AGPrint Line 1 for PatronID, Language 1|AGPrint Line 2 for PatronID, language 1|AY4AZ608F";
	        $out = $this->Get_Patron_Information_Response($in);
	        break;
	    case "25":
	        $out = $this->Get_Patron_Enable_Response($in);
	        break;
	    case "17":
	        $out = $this->Get_Item_Information_Response($in);
	        break;
	    case "11":
	        $out = $this->Get_Checkout_Response($in);
	        break;
	    case "09":
	        $out = $this->Get_Checkin_Response($in);
	        break;
	    case "37":
	        $out = $this->Get_Fee_Paid_Response($in);	        
	        break;	        
	    case "35":
	        $out = $this->Get_End_Session_Response($in);	        
	        break;    
	    case "19":
	        $out = $this->Get_Item_status_update_Response($in);	        
	        break;
	    case "15":
	        $out = $this->Get_Hold_Response($in);	        
	        break;
	    case "29":
	        $out = $this->Get_Renew_Response($in);	        
	        break;
	    case "65":
	        $out = $this->Get_Renew_All_Response($in);	        
	        break;	        	        	       
	    default:
	        $out = '';
	}
	
	if($msg_code != '97'){
		$_SESSION["prev_input"][$_SERVER["REMOTE_ADDR"]] = $out;
	}
	
	return $out;
}
}

//$sip2 = new sip2();
//
//echo $sip2->Handle_SIP2_Message($_REQUEST['in']);
//echo "<i>[debug]</i>\n<PRE>\n";
//     print_r($sip2->Handle_SIP2_Message($_REQUEST['in']));
//echo "\n</PRE>\n<br>\n";
intranet_closedb();
?>