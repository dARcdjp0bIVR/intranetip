<?php
//error_reporting(~E_NOTICE);
error_reporting(0);
set_time_limit (0);

include_once('libsip2.php');
intranet_opendb();
$sip2 = new sip2();
$address = $server_ip;
$port = 5533;
$max_clients = 20;

if(!($sock = socket_create(AF_INET, SOCK_STREAM, 0)))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
     
    die("Couldn't create socket: [$errorcode] $errormsg \n");
}
 
echo "Socket created \n";
 
// Bind the source address
if( !socket_bind($sock, $address , $port) )
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
     
    die("Could not bind socket : [$errorcode] $errormsg \n");
}

if( !socket_set_option($sock, SOL_SOCKET, SO_RCVTIMEO, array('sec' => 60, 'usec' => 0)))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
     
    die("Could not set RCVTIMEO socket : [$errorcode] $errormsg \n");
}

if( !socket_set_option($sock, SOL_SOCKET, SO_SNDTIMEO, array('sec' => 60, 'usec' => 0)))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
     
    die("Could not set SO_SNDTIMEO socket : [$errorcode] $errormsg \n");
}
 
echo "Socket bind OK \n";
 
if(!socket_listen ($sock , 20))
{
    $errorcode = socket_last_error();
    $errormsg = socket_strerror($errorcode);
     
    die("Could not listen on socket : [$errorcode] $errormsg \n");
}
 
echo "Socket listen OK \n";
 
echo "Waiting for incoming connections... \n";
 
//array of client sockets
$client_socks = array();
 
//array of sockets to read
$read = array();
 
//start loop to listen for incoming connections and process existing connections
while (true) 
{
    //prepare array of readable client sockets
    $read = array();
     
    //first socket is the master socket
    $read[0] = $sock;
     
    //now add the existing client sockets
    for ($i = 0; $i < $max_clients; $i++)
    {
        if($client_socks[$i] != null)
        {
            $read[$i+1] = $client_socks[$i];
        }
    }
     
    //now call select - blocking call
    if(socket_select($read , $write , $except , null) === false)
    {
        $errorcode = socket_last_error();
        $errormsg = socket_strerror($errorcode);
     
        die("Could not listen on socket : [$errorcode] $errormsg \n");
    }
     
    //if ready contains the master socket, then a new connection has come in
    if (in_array($sock, $read)) 
    {
        for ($i = 0; $i < $max_clients; $i++)
        {
            if ($client_socks[$i] == null) 
            {
                $client_socks[$i] = socket_accept($sock);
                
                if( !socket_set_option($client_socks[$i], SOL_SOCKET, SO_RCVTIMEO, array('sec' => 60, 'usec' => 0)))
				{
				    $errorcode = socket_last_error();
				    $errormsg = socket_strerror($errorcode);
				     
				    die("Could not set RCVTIMEO socket : [$errorcode] $errormsg \n");
				}
				
				if( !socket_set_option($client_socks[$i], SOL_SOCKET, SO_SNDTIMEO, array('sec' => 60, 'usec' => 0)))
				{
				    $errorcode = socket_last_error();
				    $errormsg = socket_strerror($errorcode);
				     
				    die("Could not set SO_SNDTIMEO socket : [$errorcode] $errormsg \n");
				}
                 
                //display information about the client who is connected
                if(socket_getpeername($client_socks[$i], $address, $port))
                {
                    echo "Client $address : $port is now connected to us. \n";
                }
                 
                //Send Welcome message to client
//                $message = "Welcome to php socket server version 1.0 \n";
//                $message .= "Enter a message and press enter, and i shall reply back \n";
//                socket_write($client_socks[$i] , $message);
                break;
            }
        }
    }
 
    //check each client if they send any data
    for ($i = 0; $i < $max_clients; $i++)
    {
        if (in_array($client_socks[$i] , $read))
        {
            $input = socket_read($client_socks[$i] , 1024);
             
            if ($input == null) 
            {
                if(socket_getpeername($client_socks[$i], $address, $port))
                {
                //zero length string meaning disconnected, remove and close the socket
                socket_close($client_socks[$i]);
                if(socket_read($client_socks[$i] , 1024) == null){
                	unset($client_socks[$i]);
                }
                 echo "Client $address : $port is now desconnected to us. \n";
                }
                continue;
            }
 
            $n = trim($input);
            echo "Receiving input from client: $n \n";
            
            //Handle the SIP2 Message [Start]
            intranet_closedb();
            intranet_opendb();
			$output = $sip2->Handle_SIP2_Message($n);
			//Handle the SIP2 Message [End]
			
            echo "Sending output to client: $output \n";

            //send response to client
            socket_write($client_socks[$i] , $output ."\r");
            
        }
    }
}
intranet_closedb();
?>