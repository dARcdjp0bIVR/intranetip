<?php
	$PATH_WRT_ROOT = "../";
	include_once($PATH_WRT_ROOT."includes/global.php");
	include_once($PATH_WRT_ROOT."includes/libdb.php");
	include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
	include_once($PATH_WRT_ROOT."includes/libinterface.php");
	include_once($PATH_WRT_ROOT."includes/libelibrary.php");
	include_once($PATH_WRT_ROOT."includes/libelibrary_plus.php");
	include_once($PATH_WRT_ROOT."includes/settings.php");
	
	global $intranet_session_language, $intranet_root, $UserID, $US_Intranet_IDType, $config_school_code, $eclass40_httppath;
	
	function encrypt($str){
		$Encrypt1 = base64_encode($str);
		$MidPos = ceil(strlen($Encrypt1)/2);
		$Encrypta = substr($Encrypt1, 0, $MidPos);
		$Encryptb = substr($Encrypt1, $MidPos);
	
		return base64_encode($Encryptb.$Encrypta."=".(strlen($Encrypt1)-$MidPos));	
	}
	
	function decrypt($str){
		$Encrypt1 = base64_decode($str);
		$SplitSizePos = strrpos($Encrypt1, "=");
		$MidPos = (int) substr($Encrypt1, $SplitSizePos+1);
		$Encrypt1 = substr($Encrypt1, 0, $SplitSizePos);
		$Encrypta = substr($Encrypt1, 0, $MidPos);
		$Decryptb = substr($Encrypt1, $MidPos);
	
		return base64_decode($Decryptb.$Encrypta);
	}
	
	$date1 = date("Y-m-d");
	$currenttime = time(); 
	
	if($intranet_db != "ip25b5demo_intranet"){		
	   intranet_auth();			  
	   $eClassUserID = $UserID;
	   $SchoolCode = $config_school_code;
	   $TimeStamp = date("Y-m-d H:i:s");
	   $Key = encrypt($eClassUserID."|===@===|".$SchoolCode."|===@===|".$TimeStamp);
	   
	   if(!$plugin['elib_cosmos'] || $plugin['elib_cosmos_expiry_date'] <= $date1 ){ 
	   		echo(" This plugin is not installed or the access right is expired.");
	   }else{		  
	   		echo("<script>window.location='http://ip25-tc-demo.eclass.com.hk/api/cosmos_access.php?eClassUserID=$eClassUserID&SchoolCode=$SchoolCode&Key=$Key'</script>;"); 	
	   }
	}else{
		$loginArr["SchoolCode"] = $_REQUEST['SchoolCode'];
		$loginArr["eClassUserID"] = $_REQUEST['eClassUserID'];
		$loginArr["Key"] = isset($_REQUEST['Key']) && $_REQUEST['Key']!=''? decrypt($_REQUEST['Key']) : 0;
		$loginArr["Key"] = explode("|===@===|",$loginArr["Key"]);
		
		$original=strtotime($loginArr["Key"][2]);
	
		$hr  = 0;
		$min = 30;
		$sec = 0;
		
		$limit = $original+$sec+($min*60)+($hr*60*60);
		
		$currenttime = time(); 
		
		$valid_access = ($loginArr["Key"][1] == $loginArr["SchoolCode"]) && ($loginArr["Key"][0] == $loginArr["eClassUserID"]) 
						 && ($currenttime < $limit ) || 
	   					($_SERVER['HTTP_REFERER'] == "http://ip25-tc-demo.eclass.com.hk/home/eLearning/elibplus/magazine_list.php");
				
	   if(!$valid_access ){ 	   
	   		echo(" This plugin is not installed or the access right is expired.");
	   }else{		
	   		echo("<script>window.location='http://e.cosmosmagazine.com'</script>;"); 	
	   }		
	}

?>