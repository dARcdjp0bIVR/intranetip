<?php
// using : Bill

/********************** Change Log ***********************
 * 20190510 Bill
 *  - prevent SQL Injection
 *  - apply intranet_auth()
 * 20190306 Bill
 *  - for auth checking - get token from returnSimpleTokenByDateTime()     ($special_feature['download_attachment_add_auth_checking'])
 * 20170712 Carlos 
 *  - $sys_custom['DHL'] for DHL, if reply slip is empty, do not display reply slip related words and UI elements.
 * 20151204	Bill	[2015-0611-1642-49207]
 * 	- not allow staff (except eCircular Admin) to resign after deadline
 * 20140827 Ivan
 * 	- copy most logic from /home/web/eclass40/intranetIP25/home/eService/circular/sign.php
 */
/********************** end of Change Log ***********************/

# set as default
if (!isset($sys_custom['enotice_sign_with_password'])) {
	$sys_custom['enotice_sign_with_password'] = false;
}

$NoUTF8 = true;
$PATH_WRT_ROOT = "../";

//set language
@session_start();
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
$leClassApp_init = new libeClassApp_init();
$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
$_SESSION['intranet_hardcode_lang'] = $leClassApp_init->getPageLang($parLang);

//# see if there is any custom setting
//if (file_exists($PATH_WRT_ROOT."file/eclass_app.php"))
//{
//	session_start();
//	include_once($PATH_WRT_ROOT."file/eclass_app.php");
//	$intranet_session_language = $eclass_app_language;
//} else
//{
//	$parLang = isset($_GET['parLang'])? $_GET['parLang'] : '';
//	if ($parLang != '') {
//		switch (strtolower($parLang)) {
//			case 'en':
//				$intranet_hardcode_lang = 'en';
//				break;
//			default:
//				$intranet_hardcode_lang = 'b5';
//		}
//	}
//}

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libcircular.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libadminjob.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/user_right_target.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");

### Handle SQL Injection + XSS [START]
$CircularID = IntegerSafe($CircularID);
$CurUserID = IntegerSafe($CurUserID);
### Handle SQL Injection + XSS [END]

$UserID = $CurUserID;
$_SESSION['UserID'] = $CurUserID;

intranet_auth();
intranet_opendb();

$lform = new libform();
$lcircular = new libcircular($CircularID);
$luser = new libuser($CurUserID);

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$CircularID.$delimiter.$CurUserID;

# Generate token for auth checking
$dl_auth_token = '';
if($special_feature['download_attachment_add_auth_checking'])
{
    $dl_auth_token = returnSimpleTokenByDateTime();
    
//     unset($_SESSION['dlToken']);
//    
//     $dl_auth_token = md5($keyStr.$delimiter.time());
}

/* testing 
$CircularID = 183;
$TargetUserID = 1664;
*/
/* testing  
 */
$MODULE_OBJ['title'] = $i_Circular_Circular;
$linterface = new interface_html("popup6.html");
$linterface->LAYOUT_START();

if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

if(!$special_feature['circular'] || $lcircular->disabled || $luser->RecordType != USERTYPE_STAFF) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

if ($lcircular->DateInput == '') {
	$warningMsg = $Lang['Circular']['NoticeDeleted'];
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}

if (date('Y-m-d H:i:s') < $lcircular->DateStart) {
	$warningMsg = $Lang['eNotice']['NoticeNotIssuedYet'];
	$warningMsg = str_replace('<!--startDate-->', $lcircular->DateStart, $warningMsg);
	
	echo '<div style="padding:5px;">'.$warningMsg.'</div>';
	die();
}

# Check admin level
$LibAdminJob = new libadminjob($CurUserID);
$UserRightTarget = new user_right_target();
	
# check is Circular admin (both full control or normal control)
$_SESSION["SSV_USER_ACCESS"] = $UserRightTarget->Load_User_Right($CurUserID);
$_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] = $LibAdminJob->isCircularAdmin();
if($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"]) {
	$_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"] = $LibAdminJob->returnAdminLevel($CurUserID, $LibCircular->admin_id_type);
}

$actualTargetUserID = $CurUserID;
$isAdmin = false;
// [2015-0611-1642-49207]
$isCircularAdmin = false;
if ($_SESSION["SSV_PRIVILEGE"]["circular"]["is_admin"] || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])
{
    $adminlevel = $_SESSION["SSV_PRIVILEGE"]["circular"]["AdminLevel"];
    if ($adminlevel==1 || $lcircular->IssueUserID == $CurUserID || $_SESSION["SSV_USER_ACCESS"]["eAdmin-eCircular"])     # Allow if Full admin or issuer
    {
        if ($targetUserID != "")
        {
        	// [2015-0611-1642-49207]
            $isCircularAdmin = true;
            $actualTargetUserID = $targetUserID;
            if ($targetUserID != $CurUserID)
            {
                $isAdmin = true;
            }
        }
        else
        {
            $actualTargetUserID = $CurUserID;
        }
    }
}

# Check Reply involved
if (!$lcircular->retrieveReply($actualTargetUserID))
{
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

$temp_que =  str_replace("&amp;", "&", $lcircular->Question);
$queString = $lform->getConvertedString($temp_que);
$queString=trim($queString)." ";

# Check View/Edit Mode
$pos = 0;
if ($isAdmin)
{
    $js_edit_mode = ($lcircular->isHelpSignAllow);
}
else
{
    $sign_allowed = false;
    $time_allowed = false;
    
	// [2015-0611-1642-49207] check if eCircular after deadline or not
	$today = time();
	$deadline = strtotime($lcircular->DateEnd);
	$afterDeadline = compareDate($today,$deadline)>0;
	
    if ($lcircular->isReplySigned())
    {
    	// [2015-0611-1642-49207] before deadline / eCircular Admin
        //if ($lcircular->isResignAllow)
        if ($lcircular->isResignAllow && ($isCircularAdmin || !$afterDeadline))
        {
            $signed_allowed = true;
        }
    }
    else
    {
        $signed_allowed = true;
    }

    //$today = time();
    //$deadline = strtotime($lcircular->DateEnd);
    //if (compareDate($today,$deadline)>0)
    if($afterDeadline)
    {
        if ($lcircular->isLateSignAllow)
        {
            $time_allowed = true;

        }
    }
    else
    {
        $time_allowed = true;
    }

    $js_edit_mode = ($signed_allowed && $time_allowed);
}

$sys_custom['DHL'] = 1;
$display_reply_slip = !$sys_custom['DHL'] || ($sys_custom['DHL'] && trim($queString)!='');
   
$temp_ans =  str_replace("&amp;", "&", $lcircular->answer);
$ansString = $lform->getConvertedString($temp_ans);
$ansString.=trim($ansString)." ";

// include_once("../../templates/fileheader.php");
$targetType = array("",$i_Circular_RecipientTypeAllStaff,
                    $i_Circular_RecipientTypeAllTeaching,
                    $i_Circular_RecipientTypeAllNonTeaching,
                    $i_Circular_RecipientTypeIndividual);

$attachment = $lcircular->displayAttachment_showImage($dl_auth_token);

# Store token to session
// if($special_feature['download_attachment_add_auth_checking'] && $dl_auth_token != '')
// {
//     $_SESSION['dlToken'] = $dl_auth_token;
// }

$reqFillAllFields = $lcircular->AllFieldsReq;
?>

<style type="text/css">
.tabletext {
	font-size: 16px;
}
.tabletext2 {
	font-size: 16px;
}
.tabletextremark {
	font-size: 16px;
}
</style>
<script language="javascript" src="/templates/forms/layer.js"></script>
<script language="Javascript">
var replyslip = '<?=$Lang['eCircular']['ReplySlip']?>';
</script>

<? if ($js_edit_mode) {
# Fill form mode
?>
<script language="javascript" src="/templates/forms/form_edit.js"></script>
<? } else { ?>
<script language="javascript" src="/templates/forms/form_view.js"></script>
<? } ?>
				
<script language="javascript">
jQuery(document).ready(function($) {
	
   	textboxes = $("input:text");
	
	if ($.browser.mozilla) {
		$(textboxes).keypress(checkForEnter);
	} else {
		$(textboxes).keydown(checkForEnter);
	}
	
	function checkForEnter(event) {
		if (event.keyCode == 13) {
			currentTextboxNumber = textboxes.index(this);
			
			if (textboxes[currentTextboxNumber + 1] != null) {
				nextTextbox = textboxes[currentTextboxNumber + 1];
				nextTextbox.select();
			}
			
			event.preventDefault();
			return false;
		}
	}
	
	$('div#descriptionDiv').css('width', Get_Screen_Width() - 10);
});

// Set true if need to fill in all fields before submit
var need2checkform = <? if($reqFillAllFields){ echo "true"; } else { echo "false"; }  ?>;
function SubmitForm(obj)
{
	var obj = document.getElementById('form1');
	
	if (checkform(document.form1))
  	{   
  		$('input#submitbtn').attr('disabled', true);
  		//$('div#submitBtnDiv').html(submittingDivContent);
  		$('div#submitBtnDiv').hide();
		$('div#submittingDiv').show();
  		document.form1.submit();
  	}
}

function checkform(obj)
{
	 if (!need2checkform || formAllFilled)
	 {
	      return true;
	 }
	 else //return true;
	 {
	      alert("<?=$Lang['Notice']['AnswerAllQuestions']?>");
	      return false;
	 }
	 
	var alert_msg = "";
	switch(obj.test.value)
	{
		case "1":
			alert_msg = "<?=$i_Circular_Alert_Sign?>";
			break;
		case "2":
			alert_msg = "<?=$i_Circular_Alert_Sign_AddStar?>";
			break;
		case "3":
			alert_msg = "<?=$i_Circular_Alert_UnStar?>";
			break;
		case "4":
			alert_msg = "<?=$i_Circular_Alert_AddStar?>";
			break;
	}
	
	if (!confirm(alert_msg))
		return false;
}

function click_print()
{
	with(document.form1)
	{
		action = "print_preview.php";
		target = "_blank";
		submit();
		action = "sign_update.php";
		target = "_self";
	}
    
}
</script>
<div id="spanTemp"></div>

<script src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.11.1.min.js"></script>
<script src="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.js"></script>
<link rel="stylesheet" href="<?=$PATH_WRT_ROOT?>templates/jquery.mobile/jquery.mobile.min.css">

<div data-role="page" id="pageone">
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<?
					if (!$lcircular->isReplySigned())
					{
						$statusString = "$i_Circular_Unsigned";
					}
					else
					{
						$signer = $luser->getNameWithClassNumber($lcircular->signerID);
						$signby = ($lcircular->replyType==1? $i_Circular_Signer:$i_Circular_Editor);
						$statusString = "$i_Circular_Signed <i>($signby $signer $i_Circular_At ".$lcircular->signedTime.")</i>";
					}
				?>
				<tr valign='top'>					
                    <td bgcolor='#FFFFFF' style="font-size:18px; padding:10px;">
						<span class='tabletext' style="font-size:14px;"><?=$lcircular->CircularNumber?></span>
				    <br>
					<span class='tabletext' style="font-size:22px;"><strong><?=$lcircular->Title?></strong></span>
					<?php if(!$lcircular->isReplySigned()){?>
						<span id="rcorners_red" style="color:white;text-shadow: 0px 0px;font-size:16px;vertical-align:text-top;"><?=$i_Circular_Unsigned?></span>						
					<?php }else{?>
						<span id="rcorners_green" style="color:white;text-shadow: 0px 0px;font-size:16px;vertical-align:text-top;"><?=$i_Circular_Signed?></span>
					<?php }?>
					</td>												
				</tr>
				<tr valign='top'>
				    <td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
				        <span><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_date.png" align="absmiddle" style="width:20px; height:20px;" />
						<span style="vertical-align:bottom;"><?=$lcircular->DateStart?></span></span>
				        <br>
				        <span><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/icon_deadline.png" align="absmiddle" style="width:20px; height:20px;" />
						<span style="vertical-align:bottom;"><font color="red"><?=$lcircular->DateEnd?></font></span></span>				        
				    </td>
				</tr>
				<tr valign='top'>
					<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;"><div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;"><span class='tabletext2'><?=HTML_Transform(str_replace("&nbsp;&nbsp;", "&nbsp; ", htmlspecialchars_decode($lcircular->Description)))?></span></div></td>
				</tr>
				<? if ($attachment != "") { ?>
				     <tr valign='top'>
				     	<td bgcolor='#FFFFFF' style="font-size:16px; padding:10px;">
				        <div id="descriptionDiv" style="width: 100%; overflow-x: auto; overflow-y: hidden;"><span><?=$attachment?></span></div>
				        </td>
				     </tr>
				<? } ?>		 
			</table>
		</td>
	</tr>
</table>
<div data-role="collapsible" data-collapsed="true" style="font-size:18px">
   <h1><?=$Lang['General']['Misc']?></h1>
   <div class="ui-grid-a">
		<? $issuer = $lcircular->returnIssuerName(); ?>
		<div class="ui-block-a" style="width:100%;">
			<br>
			<span><strong><?=$i_Circular_Issuer?></strong></span>
			<br>
			<span><?=$issuer?></span>
		</div>
		
		<?  if ($lcircular->isReplySigned()) { ?>		
		<div class="ui-block-a" style="width:100%;">
			<br>
			<span><strong><?=$i_Circular_SignStatus?></strong></span>
			<br>
			<span><?=$statusString?></span>
		</div>
		<? } ?>
		
		<? if ($isAdmin) { ?>
			<div class="ui-block-a" style="width:100%;">
				<br>
				<span><strong><?=$i_Circular_Recipient?></strong></span>
				<br>
				<span><?=$lu->getNameWithClassNumber($actualTargetUserID)?></span>
			</div>
			<div class="ui-block-a" style="width:100%;">
				<br>
				<span><strong><?=$i_Circular_RecipientType?></strong></span>
				<br>
				<span><?=$targetType[$lcircular->RecordType]?></span>
			</div>
		<? } ?>
		
		<? if ($reqFillAllFields) { ?>
			<div class="ui-block-a" style="width:100%;">
				<br>
				<br>
				<span><strong>[<?=$i_Survey_AllRequire2Fill?>]</strong></span>
			</div>
		<? } ?>
	</div>
	<br>
</div>
<?php if($display_reply_slip){ ?>
<!-- Break -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/scissors_line.gif"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/eOffice/icons_scissors.gif"></td>
	</tr>
</table>
<?php } ?>
<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td>
			<table width="90%" border="0" cellspacing="0" cellpadding="5" align="center">
				<!--tr valign="top">
					<td class='eNoticereplytitle' align='center'><?=$Lang['eCircular']['ReplySlipForm']?></td>
				</tr-->
				<tr valign="top">
					<td class="tabletext">
						
						<form name="ansForm" method="post" action="update.php">
							<input type=hidden name="qStr" value="">
							<input type=hidden name="aStr" value="">
						</form>
						<script language="Javascript">
						<?=$lform->getWordsInJS()?>
							background_image = "/images/layer_bg.gif";
							<? if ($js_edit_mode) {
							# Fill form mode
							?>
							var DisplayQuestionNumber = '<?=$lcircular->DisplayQuestionNumber?>';
							var sheet= new Answersheet();
							// attention: MUST replace '"' to '&quot;'
							sheet.qString="<?=$queString?>";
							sheet.aString="<?=$ansString?>";	
							
							//edit submitted application
							sheet.mode=1;
							sheet.answer=sheet.sheetArr();
							//sheet.templates=form_templates;
							<?php if($display_reply_slip){ ?>
							document.write(editPanel('0'));
							<?php } ?>
							
							<? } else {
							# View answer only
							?>
							myQue="<?=$queString?>";
							myAns="<?=$ansString?>";
							document.write(viewForm(myQue, myAns));
						<? } ?>			
						</script>
						<script language="javascript">
							function copyback(option)
							{
								//Sign & Close
								if (option == 1)
								{
									document.form1.test.value = 1;
								}
								//Sign & Add Star
								else if(option == 2)
								{
									document.form1.test.value = 2;
								}
								//Unstar
								else if (option == 3)
								{
									document.form1.test.value = 3;
								}
								//Add Star
								else if (option == 4)
								{
									document.form1.test.value = 4;
								}
								
								//if(document.form1.qStr.value.length > 0) 
								//{
									finish();
									document.form1.qStr.value = 							   document.ansForm.qStr.value;
									document.form1.aStr.value = 							   document.ansForm.aStr.value;
								//}
								
							}

						</script>
							
						<? if ($js_edit_mode && $display_reply_slip) {
							echo ($lcircular->replyStatus==2? $i_Circular_SignInstruction2:$i_Circular_SignInstruction);
						} ?>
						
					</td>
				</tr>
				<tr>
					<td class="dotline" colspan="2"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/10x10.gif" width="10" height="1" /></td>
				</tr>
				<form name="form1" id="form1" action="ecircular_sign_eclassApp_update.php" method="post" onSubmit="return checkform(this)">
				<tr>
					<td align="center" colspan="2">
						<div id="submitBtnDiv">
							<? if ($js_edit_mode) { ?>
							<!--?= $linterface->GET_ACTION_BTN($i_Circular_Sign, "submit", "copyback();","submit") ?-->
	
								<?= $linterface->GET_ACTION_BTN($i_Notice_Sign, "button", "copyback(1);SubmitForm(this.form);","submitbtn") ?>
								<? if($special_feature['circular_star']) {?>
								<?= $linterface->GET_ACTION_BTN($eCircular["SignAddStar"], "button", "copyback(2);SubmitForm(this.form);","submitbtn") ?>
								<? } ?>
	
							<? }else{ ?>
									<? if($special_feature['circular_star']) {?>
									<?//Check whether circular is starred
									$star = $lcircular->getStar($lcircular->CircularID);
	
									if ($star == 1){?>
										<?= $linterface->GET_ACTION_BTN($eCircular["RemoveStar"], "button", "copyback(3);SubmitForm(this.form);","submitbtn") ;}
									else{?>
										<?= $linterface->GET_ACTION_BTN($eCircular["AddStar"], "button", "copyback(4);SubmitForm(this.form);","submitbtn") ;}
									}?>
							<? } ?>
						</div>
						<div id="submittingDiv" style="width:100%; text-align:center; display:none;"><?=$linterface->Get_Ajax_Loading_Image($noLang=1).' <span style="color:red; font-weight:bold;">'.$Lang['General']['Submitting...']?></span></div>

						<input type=hidden name=test value="">
						<input type=hidden name=qStr value="">
						<input type=hidden name=aStr value="">
						<input type=hidden name=CircularID value="<?=$CircularID?>">
						<input type=hidden name=actualTargetUserID value="<?=$actualTargetUserID?>">
						<input type=hidden name=CurUserID value="<?=$CurUserID?>">
						<input type=hidden name=adminMode value="<?=($isAdmin? "1":"0")?>">
						<input type=hidden name=token id="token" value="<?=$token?>">
					</td>
				</tr>
				</form>
			</table>
		</td>
	</tr>
</table>
</div>

<?php
intranet_closedb();
$linterface->LAYOUT_STOP();
?>