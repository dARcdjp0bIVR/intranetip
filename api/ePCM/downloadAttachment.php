<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ePCM_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_cfg.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_ui.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_db.php");

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();
$libPCM_db = new libPCM_db();

$attachmentID = decryptAttachmentID($_GET['i']);

$attachmentInfo = $libPCM_db->getFileById($attachmentID);
$caseID = $attachmentInfo[0]['CaseID'];
$fileName = $attachmentInfo[0]['FileName'];

$path = '../../'.$ePCMcfg['INTERNATDATA'].'api/'.sha1($caseID).'/'.$fileName;
if(is_file($path)){
	output2browserByPath($path, $fileName);
}


intranet_closedb();



function decryptAttachmentID($enc_id){
	$key = 'ePCM_key_for_encrpt_aid'; //Don't edit
	return AES_128_Decrypt($enc_id,$key);
}
?>