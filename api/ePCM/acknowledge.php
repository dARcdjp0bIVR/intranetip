<?php 

/**
 *
 * Date: 2016-06-29	Kenneth
 * - added check is token expired logic
 *
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/ePCM_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_cfg.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_ui.php");
include_once($PATH_WRT_ROOT."includes/ePCM/libPCM_db.php");

intranet_opendb();

$token=$_GET['t'];
$msg = $_GET['msg'];
$db = new libPCM_db();


$quotation_id=$db->getAndUpdateQuotationByToken($token);
if($quotation_id===false){
	$quotation_id=$db->getQuotationIdByToken($token);
}

$school_name=GET_SCHOOL_NAME();
if($quotation_id=='EXPIRED'){
	//2016-06-29
	// quotation Exist, but not valid => expired
	$html='<center>已過期</center>';
}else if($quotation_id===false){
	$html='<center>無效的網址</center>';
}else{

	$tokenInfo = $db->getTokenInfo($token,$quotationID='');
	if(!empty($tokenInfo['Pw'])){
		$pwEnabled = true;
	}else{
		$pwEnabled = false;
	}
	
	$html='<center>';
	$html.= '歡迎使用eClass 採購系統<br>Welcome to eProcurement Systems';
	
	
	$html.='</center>';
	
	if($pwEnabled){
		$html.='<br><center>請輸入電郵中8位數字密碼，以下載有關採購文件<br>Please input 8-digits password attached in the email, <br>for downloading related documents</center>';
		$html.= '<br><center><form method="post" id="password_field" action="password_auth.php">';
		$html.= '<label for="password">密碼 Password : </label>';
		$html.= '<input type="password" name="password" id="password"/>';
		if($msg=='wpw'){
			$html.= '<br><span style="color:red">密碼錯誤 Incorrect Password</span>';
		}
		$html.= '<br><input type="Submit" value="呈送 Submit"/>';
		$html.= '<input type="hidden" name="token" value="'.$token.'"/>';
		$html.= '</form></center>';
	}else{
		$rows_quotation=$db->getCaseQuotation('',$quotation_id);
		$row_quotation=$rows_quotation[0];
		
		$case_id=$row_quotation['CaseID'];
		
		
		
		$rows_case=$db->getCaseInfo($case_id);
		$row_case=$rows_case[0];
		
		$supplier_id=$row_quotation['SupplierID'];
		$array_supplier=$db->getSupplierInfo($supplier_id);
		$rows_supplier=$array_supplier['MAIN'];
		$row_supplier=$rows_supplier[0];
		
		$case_name=$row_case['CaseName'];
		$case_number=$row_case['Code'];
		$supplier_name=$row_supplier['SupplierName'];
		
		$html='<center>' .
				'系統成功記錄你的回應，<br/>
				確認 貴公司 ('.$supplier_name.')已收到本校「'.$case_name.'」(編號'.$case_number.') 報價邀請。<br/>
				</br>
				謝謝!<br/>' .
				'</center>';
	}
	
	
	
}

//UI
$MODULE_OBJ['title'] = $school_name;
$linterface = new interface_html("popup_index.html");
$linterface->LAYOUT_START();
echo $html;
$linterface->LAYOUT_STOP();

intranet_closedb();




?>
