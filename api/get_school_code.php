<?
/*
 * This API is for eClass App license central server to check if the school code is the same between central server and school config
 */
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");

if (!isset($config_school_code) || $config_school_code == '') {
	$config_school_code = 'MISSING_CONFIG';
}

echo $config_school_code;
?>