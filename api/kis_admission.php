<?php
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once ($PATH_WRT_ROOT ."includes/json.php");
include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_cust.php");
//include_once($PATH_WRT_ROOT."includes/admission/".$setting_path_ip_rel."/libadmission_ui_cust.php");

include_once($PATH_WRT_ROOT."lang/lang.".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/lang_common_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/kis/apps/lang_admission_".$intranet_session_language.".php");
include_once($PATH_WRT_ROOT."lang/admission_lang.".$intranet_session_language.".php");
intranet_opendb();
$lac = new admission_cust();


//$objJson = new JSON_obj();
//$jsonContent = file_get_contents('php://input');
//$array = $objJson->decode($jsonContent);


//exit();
//$currentAcademicId = Get_Current_Academic_Year_ID();
//$objYear = new year_class();

//$classRs = $objYear->Get_All_Class_List($currentAcademicId,'en');
//debug_r($objYear);
if($setting_path_ip_rel == 'icms.eclass.hk')
	$application_setting = getApplicationSetting("all_year");
else
	$application_setting = getApplicationSetting();

//$class_level_selection = array();
////To get the class level which is available
//if($application_setting){
//	$hasClassLevelApply = 0;
//	foreach($application_setting as $key => $value){
//		//debug_pr($value['StartDate']);
//		//if(date('Y-m-d') >= $value['StartDate'] && date('Y-m-d') <= $value['EndDate']){
//			$hasClassLevelApply = 1;
//			$class_level_selection[] = $application_setting;
//		//}			
//	}
//}
$classJson = serialize($application_setting);
//$classJson = unserialize($classJson);
echo $classJson;
//debug_r($classJson);

function getApplicationSetting($schoolYearID=''){
	global $lac;
	$schoolYearID = $schoolYearID?$schoolYearID:$lac->schoolYearID;

	$sql = "
		SELECT
 			a.ClassLevelID,
			CONCAT(y.YearName,' (',ay.YearNameEN,')') As FormName,
		    IF(a.StartDate,DATE_FORMAT(a.StartDate,'%Y-%m-%d %H:%i:%s'),'') As StartDate,
			IF(a.EndDate,DATE_FORMAT(a.EndDate,'%Y-%m-%d %H:%i:%s'),'') As EndDate,
			'0' as IsBriefing,
			y.Sequence as Sequence
		FROM
			ADMISSION_APPLICATION_SETTING AS a
		JOIN 
			YEAR AS y
		ON 
			a.ClassLevelID = y.YearID
		JOIN
			ACADEMIC_YEAR AS ay
		ON
			a.SchoolYearID = ay.AcademicYearID
		WHERE 1 ";
	if($schoolYearID != "all_year")		
		$sql .= " AND
			a.SchoolYearID = '".$schoolYearID."'";
	
	$sql .= " AND StartDate > 0 AND EndDate > 0";
	
	$sql .= "
		UNION
		SELECT
		 	b.BriefingID,
			b.Title As FormName,
			IF(b.EnrolmentStartDate,DATE_FORMAT(b.EnrolmentStartDate,'%Y-%m-%d %H:%i:%s'),'') As StartDate,
			IF(b.EnrolmentEndDate,DATE_FORMAT(b.EnrolmentEndDate,'%Y-%m-%d %H:%i:%s'),'') As EndDate,
			'1' as IsBriefing,
			b.BriefingID as Sequence
		FROM
			ADMISSION_BRIEFING_SETTING AS b
		JOIN
			ACADEMIC_YEAR AS ay
		ON
			b.SchoolYearID = ay.AcademicYearID
		WHERE 1 ";
		
//	if($schoolYearID != "all_year")		
//		$sql .= " AND
//			b.SchoolYearID = '".$schoolYearID."'";
	
	$sql .= " AND b.EnrolmentStartDate > 0 AND b.EnrolmentEndDate > 0 ORDER BY IsBriefing, Sequence, StartDate, EndDate";
	
	//debug_pr($sql);			
	$setting = $lac->returnArray($sql);
	
	return $setting;    	
} 

intranet_closedb();
?>