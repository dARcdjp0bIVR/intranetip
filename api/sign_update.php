<?php
// Modifying by : Bill

############ Change log ################
#
#   Date    :   2020-01-15  Bill
#               allow resign notice if Notice Settings - "Do not allow parents and students to edit replies before deadline" : NO
#
#   Date    :   2019-12-12  Ray
#               Added isTargetNoticeSigned check
#
#	Date	:	2017-08-16	Bill	[2017-0206-1342-27225]
#				Insert new replies even Users signed before ($sys_custom['eNotice']['SubmitNewReplyMulitpleTimes'])
#
#	Date	:	2015-03-10 [Ivan] (ip.2.5.6.5.1)
#				Added record auth code for reply logic
#
#	Date	:	2013-05-09 [Yuen]
#				customized to notify the parent if signed successfully - control by flag $sys_custom['enotice_sign_notify_by_app']
#
#	Date 	:	2010-10-12 [YatWoon]
#				update reload parent javascript
#	
########################################

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$NoticeID.$delimiter.$StudentID.$delimiter.$CurID;

/* testing 
$NoticeID = 7285;
$StudentID = 1668;
$CurID = 1707;
*/
/* testing  
 */
if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

$authCode = trim(stripslashes($_POST['authCode']));

//intranet_auth();
intranet_opendb();

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice;
$linterface = new interface_html("popup.html");
//$linterface->LAYOUT_START();

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lu = new libuser($CurID);

$valid = true;
$editAllowed = true;
$isParent = 1;
/*
if ($lu->isStudent())
{
    $valid = false;
}
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
if ($StudentID!="")
{
    if ($isParent)
    {
        $children = $lu->getChildren();
        if (!in_array($StudentID,$children))
        {
             $valid = false;
        }
        else
        {
            $editAllowed = true;
        }
    }
    else # Teacher
    {
	    if($lnotice->IsModule)
	    {
			$editAllowed = true;    
	    }
	    else
	    {
        	$editAllowed = $lnotice->isEditAllowed($StudentID);
    	}
    }
}
else
{
    if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
}
*/
if ($editAllowed)
{
    if ($StudentID == "")
    {
        $valid = false;
    }
    else
    {
        // check if notice after deadline
        $today = time();
        $deadline = strtotime($lnotice->DateEnd);
        $after_deadline = ($today > $deadline);

		$notice_already_sign = $lnotice->isTargetNoticeSigned($NoticeID,$StudentID);
		$is_allow_resign = !$lnotice->NotAllowReSign && !$after_deadline;

        $ansString = intranet_htmlspecialchars(trim($aStr));
        $type = ($isParent? 1: 2);
		
		# [2017-0206-1342-27225] Insert
		if($sys_custom['eNotice']['SubmitNewReplyMulitpleTimes'] && strtoupper($lnotice->Module) != "PAYMENT") {
			// Get User Info
			$TargetID = "U".$StudentID."";
			$TargetUser = $lnotice->returnTargetUserIDArray($TargetID, "0,1");
			if(isset($TargetUser[0])) {
	      		$StudentName = $TargetUser[0][1];
			}
			
			// Remove NULL reply first
			$sql = "DELETE FROM INTRANET_NOTICE_REPLY WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID' AND RecordStatus = '0'";
			$lnotice->db_db_query($sql);
	      	
			$sql = "INSERT IGNORE INTO INTRANET_NOTICE_REPLY (NoticeID, StudentID, StudentName, Answer, RecordType, RecordStatus, SignerID, SignerAuthCode, DateInput, DateModified)
					 VALUES ('$NoticeID', '$StudentID', '".addslashes($StudentName)."', '$ansString', '$type', 2, '$CurID', '".$lnotice->Get_Safe_Sql_Query($authCode)."', NOW(), NOW()) ";
			$lnotice->db_db_query($sql);
		}
        # Update
		else {
			if($notice_already_sign == false || $is_allow_resign) {
				$sql = "UPDATE INTRANET_NOTICE_REPLY 
					SET Answer = '$ansString', RecordType = '$type', RecordStatus = 2, SignerID = '$CurID', DateModified = now(), SignerAuthCode = '" . $lnotice->Get_Safe_Sql_Query($authCode) . "'
	                WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
				$lnotice->db_db_query($sql);
			}
		}
        
		if($sys_custom['eNotice']['SubmitNewReplyMulitpleTimes'] && strtoupper($lnotice->Module) != "PAYMENT") {
			// [2017-0206-1342-27225]
			// not send out push message
			// do nothing
		}
		else if($notice_already_sign == false || $is_allow_resign) {
	        if($role=="s") {
		        # notify and confirm the student by sending push message via Student App
		        $MsgNotifyDateTime = date("Y-m-d H:i:s");
		        if ($plugin['eClassStudentApp']) {
		        	if (in_array($StudentID, $lu->getStudentWithStudentUsingStudentApp())) {
		        		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
						$leClassApp = new libeClassApp();
			        	
			        	$MessageTitle = $Lang['AppNotifyMessage']['eNoticeSigned'];
						$MessageTitle = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lnotice->NoticeNumber), $MessageTitle);
			        	$MessageContent = str_replace("[sign_datetime]", $MsgNotifyDateTime, $Lang['AppNotifyMessage']['eNoticeSignedContent_eClassApp']);
						$MessageContent = str_replace("[sign_notice_title]", intranet_undo_htmlspecialchars($lnotice->Title), $MessageContent);
						$MessageContent = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lnotice->NoticeNumber), $MessageContent);
						
						$messageInfoAry = array();
						$messageInfoAry[0]['relatedUserIdAssoAry'][$StudentID] = array($StudentID);;
						$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=0);
		        	}
		        }
	        }
	        else {
		        # notify and confirm the parent by sending push message via Parent App
		        $MsgNotifyDateTime = date("Y-m-d H:i:s");
		        if ($plugin['eClassApp']) {
		        	if (in_array($StudentID, $lu->getStudentWithParentUsingParentApp())) {
		        		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
						$leClassApp = new libeClassApp();
			        	
			        	$MessageTitle = $Lang['AppNotifyMessage']['eNoticeSigned'];
						$MessageTitle = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lnotice->NoticeNumber), $MessageTitle);
			        	$MessageContent = str_replace("[sign_datetime]", $MsgNotifyDateTime, $Lang['AppNotifyMessage']['eNoticeSignedContent_eClassApp']);
						$MessageContent = str_replace("[sign_notice_title]", intranet_undo_htmlspecialchars($lnotice->Title), $MessageContent);
						$MessageContent = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lnotice->NoticeNumber), $MessageContent);
						
						### get parent student mapping
						$sql = "SELECT 
										ip.ParentID, ip.StudentID
								from 
										INTRANET_PARENTRELATION AS ip 
										Inner Join INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
								WHERE 
										ip.StudentID = '".$StudentID."'
										AND iu.RecordStatus = 1";
						$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
						
						$messageInfoAry = array();
						$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
						$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=0);
		        	}
		        }
		        //if ($sys_custom['enotice_sign_notify_by_app']) 
		        //if (true || $sys_custom['enotice_sign_notify_by_app'])
			    else if ($plugin['ASLParentApp'])
			    {
			    	include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
					include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");
					$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);
					
					$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
					
					$sql = "SELECT UserLogin, ChineseName, Englishname FROM INTRANET_USER where UserID='{$CurID}'";
					$row_user = $lnotice->returnArray($sql);
					$parent_userlogin = $row_user[0]["UserLogin"];
					if ($row_user[0]["ChineseName"]!=$row_user[0]["Englishname"])
					{
						$parent_name = $row_user[0]["ChineseName"] . " ".$row_user[0]["Englishname"];	
					} else
					{
						$parent_name = (trim($row_user[0]["ChineseName"])!="") ? $parent_name = $row_user[0]["ChineseName"] : $row_user[0]["Englishname"];
					}
					
					$notic_content = str_replace("[sign_notice_title]", $lnotice->Title,str_replace("[sign_datetime]", $MsgNotifyDateTime, $Lang['AppNotifyMessage']['eNoticeSignedContent']));
					
					$ASLParam = array(
							"SchID" => $SchoolID,
							"MsgNotifyDateTime" => $MsgNotifyDateTime,
							"MessageTitle" => $Lang['AppNotifyMessage']['eNoticeSigned'],
							"MessageContent" => $notic_content,
							"NotifyFor" => "",
							"IsPublic" => "N",
							"NotifyUser" => $parent_name,
							"TargetUsers" => array(
								"TargetUser" => array($parent_userlogin)
							)
						);
						//debug_r($ASLParam);die();
					$ReturnMessage = "";
					$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);
					
					$debug_log = $sys_custom['ASLParentAppDebugLog'];
					if ($debug_log)
					{
						$log_file = $file_path."/file/asl_app_test.txt";
						$file_handle = fopen($log_file,"a+");
						fwrite($file_handle,$asl->postData."\n".$Result."\n".$ReturnMessage."\n\n");
						fclose($file_handle);
					}
			    }
	        }
		}
    }

    $lnotice->retrieveReply($StudentID);
    $ansString = $lnotice->answer;
    $ansString =  str_replace("&amp;", "&", $ansString);
	$ansString = $lform->getConvertedString($ansString);
	$ansString.=trim($ansString)." ";
}
if (!$valid)
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
	exit;
}

$targetType = array("",$i_Notice_RecipientTypeAllStudents, $i_Notice_RecipientTypeLevel, $i_Notice_RecipientTypeClass, $i_Notice_RecipientTypeIndividual);
$attachment = $lnotice->displayAttachment();
$reqFillAllFields = $lnotice->AllFieldsReq;

intranet_closedb();
//$linterface->LAYOUT_STOP();

header("Location: enotice_sign_update.php");
?>