<?php
// Editing by 
/*
Required parameters:
$token: eClass App token

Optional parameters: 
$ContactType : "group" or "person"
$AliasID : array if selected AliasID
$AliasEmailID : array of selected address book contacts, in format ["user name" <email>]
*/
/*
Sample json output:
[
  {
    "name": "ContactType",
    "option": [
      {
        "display": "External Recipient Group",
        "value": "group",
        "selected": true
      },
      {
        "display": "External Recipient",
        "value": "person",
        "selected": false
      }
    ]
  },
  {
    "name": "AliasID[]",
    "option": [
      {
        "display": "Colleagues",
        "name": "AliasID[]",
        "value": "5",
        "selected": false,
        "values": "\"ccleung6\" <ccleung6@gmail.com>"
      },
      {
        "display": "Developer Group",
        "name": "AliasID[]",
        "value": "13",
        "selected": false,
        "values": "\"Carlos Leung\" <carlosleung@broadlearning.com>"
      },
      {
        "display": "External Group 1",
        "name": "AliasID[]",
        "value": "10",
        "selected": true,
        "values": "\"May\" <May@testing.broadlearning.com>; \"Peter\" <Peter@testing.broadlearning.com>; \"Test&#039;s Name Special\" <carlos_t4@testmail2.broadlearning.com>"
      },
      {
        "display": "External Group 2",
        "name": "AliasID[]",
        "value": "11",
        "selected": true,
        "values": "\"John\" <John@testing2.broadlearning.com>"
      },
      {
        "display": "Friends",
        "name": "AliasID[]",
        "value": "7",
        "selected": false,
        "values": "\"Carlos\" <challengenowhk@gmail.com>; \"Carlos Leung\" <carlosleung@broadlearning.com>; \"ccleung6\" <ccleung6@gmail.com>; \"slashtest\" <test@gmail.com>; \"special chars \/&#039;|[]{}%!@#$^&amp;*()-+=:.,\" <test@abc.com>"
      },
      {
        "display": "Slash \/&quot;&#039;|\\[]{}%!@#$^&amp;*()-+=;:., Group",
        "name": "AliasID[]",
        "value": "8",
        "selected": false,
        "values": ""
      }
    ]
  },
  {
    "name": "AliasEmailID[]",
    "option": [
      {
        "display": "\"John\" <John@testing2.broadlearning.com>",
        "name": "AliasEmailID[]",
        "value": "\"John\" <John@testing2.broadlearning.com>",
        "selected": false
      },
      {
        "display": "\"May\" <May@testing.broadlearning.com>",
        "name": "AliasEmailID[]",
        "value": "\"May\" <May@testing.broadlearning.com>",
        "selected": false
      },
      {
        "display": "\"Peter\" <Peter@testing.broadlearning.com>",
        "name": "AliasEmailID[]",
        "value": "\"Peter\" <Peter@testing.broadlearning.com>",
        "selected": false
      },
      {
        "display": "\"Test&#039;s Name Special\" <carlos_t4@testmail2.broadlearning.com>",
        "name": "AliasEmailID[]",
        "value": "\"Test&#039;s Name Special\" <carlos_t4@testmail2.broadlearning.com>",
        "selected": false
      }
    ]
  }
]

*/

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/role_manage.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libgrouping.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/user_right_target.php");
include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_api.php");
include_once($PATH_WRT_ROOT."includes/json.php");

$json_obj = new JSON_obj();
$return_ary = array();

header("Content-Type: application/json;charset=utf-8");

if(!isset($_POST['token']) || $_POST['token']=='')
{
	$return_ary['Error'] = 'Invalid parameters.';
	echo $json_obj->encode($return_ary);
	exit;
}

intranet_opendb();

$libdb = new libdb();
$libAppApi = new libeClassAppApi();

$is_magic_quotes_on = $libdb->isMagicQuotesOn();

function fast_stripslashes($value)
{
	if(is_array($value) && count($value)>0){
		foreach($value as $key => $val){
			$value[$key] = fast_stripslashes($val);
		}
	}else if(!is_array($value) && is_string($value)){
		$value = stripslashes($value);
	}
	
	return $value;
}

if($is_magic_quotes_on && isset($_POST['AliasEmailID'])){
	$AliasEmailID = fast_stripslashes($_POST['AliasEmailID']);
	$_POST['AliasEmailID'] = fast_stripslashes($_POST['AliasEmailID']);
}

$token = trim($_POST['token']);
$apiResult = $libAppApi->retrieveMailUserInfoByToken($token);

if($apiResult == '-999'){
	$return_ary['Error'] = 'Invalid token.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

$user_id = $apiResult['userId'];
$sql = "SELECT UserID,UserLogin,ImapUserEmail,RecordType,RecordStatus FROM INTRANET_USER WHERE UserID='".$user_id."'";
$user_records= $libdb->returnResultSet($sql);
if(count($user_records)==0){
	$return_ary['Error'] = 'User does not exist.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

if($user_records[0]['RecordStatus']!=1){
	$return_ary['Error'] = 'Inactive user.';
	echo $json_obj->encode($return_ary);
	intranet_closedb();
	exit;
}

$email = $user_records[0]['ImapUserEmail'];
$user_id = $user_records[0]['UserID'];
$user_type = $user_records[0]['RecordType'];
$UserID = $user_id; // some functions implicitly used $UserID as global variable

if(isset($_POST['lang']) && in_array($_POST['lang'],array('en','b5','gb'))){
	$_SESSION['intranet_session_language'] = $_POST['lang'];
	$intranet_session_language = $_SESSION['intranet_session_language'];
}
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");

$IMap = new imap_gamma(true);

$level1_ary = array('name'=>'ContactType',
					'option'=>array(
								array('display'=>$Lang['Gamma']['ExternalRecipientGroup'],'value'=>'group','selected'=>$ContactType=='group'?true:false),
								array('display'=>$Lang['Gamma']['ExternalRecipient'],'value'=>'person','selected'=>$ContactType=='person'?true:false)
							));

$return_ary[] = $level1_ary;

if($ContactType == 'group')
{
	$level2_ary = array('name'=>'AliasID[]','option'=>array());
	
	$sql = "SELECT AliasID, AliasName, NumberOfEntry FROM INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL WHERE OwnerID = '$UserID' ORDER BY AliasName";
	$alias_array = $libdb->returnResultSet($sql);
	
	$aliasIdToEmails = array();
	$alias_ids = Get_Array_By_Key($alias_array,'AliasID');
	if(isset($AliasID) && count($AliasID)>0){
		$level3_ary = array('name'=>'AliasEmailID[]','option'=>array());
	}
	if (count($alias_ids)>0)
	{
		$AliasImplode = implode(",",$alias_ids);
		
	  	$sql = "SELECT 
					b.AddressID, b.TargetName, b.TargetAddress, a.AliasID  
				FROM 
					INTRANET_CAMPUSMAIL_GROUPALIAS_EXTERNAL_ENTRY AS a,
					INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL AS b
				WHERE 
					a.AliasID IN ($AliasImplode) AND a.TargetID = b.AddressID
				ORDER BY 
					b.TargetName ";
		$emails = $libdb->returnResultSet($sql);
		$email_size = count($emails);
		for($i=0;$i<$email_size;$i++){
			$emails[$i]['Email'] = '"'.$IMap->replaceQuotesForDisplayName($emails[$i]['TargetName']).'" <'.$emails[$i]['TargetAddress'].'>';
			if(!isset($aliasIdToEmails[$emails[$i]['AliasID']])){
				$aliasIdToEmails[$emails[$i]['AliasID']] = array();
			}
			$aliasIdToEmails[$emails[$i]['AliasID']][] = $emails[$i];
			
			if(isset($AliasID) && in_array($emails[$i]['AliasID'],$AliasID)){
				$level3_ary['option'][] = array('display'=>$emails[$i]['Email'],'name'=>'AliasEmailID[]','value'=>$emails[$i]['Email'],'selected'=> isset($AliasEmailID) && in_array($emails[$i]['Email'],$AliasEmailID)? true:false);
			}
		}
	}
	
	for($i=0;$i<count($alias_array);$i++){
		$level2_ary['option'][] = array('display'=>$alias_array[$i]['AliasName'],'name'=>'AliasID[]',
										'value'=>$alias_array[$i]['AliasID'],
										'selected'=>(isset($AliasID) && in_array($alias_array[$i]['AliasID'],$AliasID)?true:false),
										'values'=>(isset($aliasIdToEmails[$alias_array[$i]['AliasID']])? implode('; ',Get_Array_By_Key($aliasIdToEmails[$alias_array[$i]['AliasID']],'Email')) : ''));
	}
	
	$return_ary[] = $level2_ary;
	if(isset($level3_ary)){
		$return_ary[] = $level3_ary;
	}
}

if($ContactType == 'person')
{	
	$level2_ary = array('name'=>'AliasEmailID[]','option'=>array());
	$sql = "SELECT 
				AddressID, TargetName, TargetAddress   
			FROM 
				INTRANET_CAMPUSMAIL_ADDRESS_EXTERNAL
			WHERE 
				OwnerID = '$UserID' 
        	ORDER BY
        		TargetAddress ASC";
	$ary = $libdb->returnResultSet($sql);
	//debug_pr($AliasEmailID);
	for($i=0;$i<count($ary);$i++){
		$ary[$i]['Email'] = '"'.$IMap->replaceQuotesForDisplayName($ary[$i]['TargetName']).'" <'.$ary[$i]['TargetAddress'].'>';
		$level2_ary['option'][] = array('display'=>$ary[$i]['Email'],'name'=>'AliasEmailID[]','value'=>$ary[$i]['Email'],'selected'=> isset($AliasEmailID) && in_array($ary[$i]['Email'],$AliasEmailID)? true:false);
	}
	$return_ary[] = $level2_ary;
}

intranet_closedb();
//debug_pr($_POST);
echo $json_obj->encode($return_ary);
die;
?>