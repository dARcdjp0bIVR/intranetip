<?php
// Modifying by : 

############ Change log ################
#   Date    :   2020-11-09 Ray
#               added getENoticePaymentStatus checking
#   Date    :   2020-10-27 Ray
#               added alipaycn
#   Date    :   2020-08-11 Ray
#               added wechat
#   Date    :   2020-07-15 Ray
#               added visamaster, multi gateway
#   Date    :   2020-03-24  Bill    [2020-0323-1606-16207]
#               Fixed merchant account checking > perform checking only if eWallet enabled
#   Date    :   2019-12-16  Bill    [2019-1216-0938-05207]
#               Fixed cannot direct pay due to empty item id
#	Date	:	2019-11-12  Carlos
#				Check student subsidy identity with both notice start date and end date. 
#	Date	:	2019-10-29  Carlos
#				(1) Handle new question type [multiple choices with input quantity].
#				(2) Use eWallet to pay would always add students into payment items even amount is $0. Only total amount to pay greater than $0 would trigger eWallet to pay for.
#	Date	:	2019-10-24  Carlos
#				Check if notice record has set merchant account, then it should use eWallet to pay, if notice payment method is not set, then there should be something wrong at sign page, redirect back to sign again.
#	Date	:	2019-09-30  Carlos
#				Fixed fail to clean previous answers generated payment items issue due to changed final anwsers do not involve any payment item. 
#	Date	:	2019-09-24  Carlos
#				Added CSRF token checking to prevent fast repeated submit.
#	Date	:	2019-09-16  Carlos
#				Update payment item amount in case of change anwser if reuse the same payment item student record.
#	Date	:	2019-09-04	Carlos
#				Modified to keep existing payment item student records and reuse as possible, to avoid change of central payment server transaction record change. 
#	Date	:	2019-08-21  Carlos
#				Record payment item original amount and subsidy identity to payment item student record for reporting usage.
#	Date	:	2019-08-05  Carlos
#				Added Tap and Go payment method.
#	Date	:	2019-06-14  Carlos
#				Added payment method and cater pay at school.
#	Date	:	2019-06-06	Carlos
#				Handle pay at school - only sign the replyslip and generate payment item student records for paying later at ePayment.
#	Date	:	2019-05-21 	Carlos
#				Can only sign when the notice is active/distributed.
#	Date    :   2019-05-14  Bill
#               prevent SQL Injection
#	Date	:	2019-04-08  Carlos
#				Improved reply slip with new js script /templates/forms/eNoticePayment_replyslip.js and php library eNoticePaymentReplySlip.php
#	Date	:	2019-01-09  Carlos
#				Double check the calculated total amount is enough to pay with remaining balance. 
#	Date	:	2018-11-15 [Carlos]
#				Check late submission.
#	Date    :	2018-09-28	(Bill)	[2018-0809-0935-01276]
#				Support Sign New Payment Question Type - Must Pay
#	Date	:	2018-08-31 [Carlos]
#				Added flag $sys_custom['PaymentNoticeDoNotCheckBalance'] to ignore checking balance.
#	Date	:	2018-08-21 [Carlos]
#				$sys_custom['ePayment']['Alipay'] - support Alipay.
#	Date	:	2018-03-13 [Carlos]
#				Initialize payment account record if ePayment module is lately installed and record not exist.
#	Date	:	2018-03-08 [Carlos]
#				$sys_custom['PaymentNoticeAlwaysGenerateItemStudents'] always generate payment item student records even amount is $0.
#	Date	:	2018-01-26 [Carlos] 
#				For TNG payment, only update answe but do not sign the notice. Would sign the notice in TNG callback to school.
#	Date	:	2017-12-21 [Carlos] (ip.2.5.9.1.1)
#				Added TNG payment processing logic.
#	Date	:	2017-05-09 [Bill] (ip.2.5.8.7.1)	[2017-0428-1605-58206]
#				Add Balance Checking
#	Date	:	2015-03-10 [Ivan] (ip.2.5.6.5.1)
#				Added record auth code for reply logic
#
########################################

$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libnotice.php");
include_once($PATH_WRT_ROOT."includes/libform.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libfiletable.php");
include_once($PATH_WRT_ROOT."includes/libpayment.php");
include_once($PATH_WRT_ROOT."includes/eNoticePaymentReplySlip.php");

### Handle SQL Injection + XSS [START]
$NoticeID = IntegerSafe($NoticeID);
$StudentID = IntegerSafe($StudentID);
$CurID = IntegerSafe($CurID);
### Handle SQL Injection + XSS [END]

$delimiter = "###";
$keyStr = $special_feature['ParentApp_PrivateKey'].$delimiter.$NoticeID.$delimiter.$StudentID.$delimiter.$CurID;
if($token != md5($keyStr)) {
	include_once($PATH_WRT_ROOT."lang/lang.en.php");
	echo $i_general_no_access_right	;
	exit;
}

if(!verifyCsrfToken($_POST['csrf_token'], $_POST['csrf_token_key'])){
	echo "Invalid request.";
	exit;
}

//intranet_auth();
intranet_opendb();

$authCode = trim(stripslashes($_POST['authCode']));

$UserID = $CurID;
$_SESSION['UserID'] = $CurID;

$MODULE_OBJ['title'] = $i_Notice_ElectronicNotice;
$linterface = new interface_html("popup.html");
//$linterface->LAYOUT_START();

$lform = new libform();
$lnotice = new libnotice($NoticeID);
$lpayment = new libpayment();
$eNoticePaymentReplySlip = new eNoticePaymentReplySlip();

$lu = new libuser($CurID);
$valid = true;
$editAllowed = true;
$isParent = 1;

# Check whether user has enough balance to pay
$currentBalance = $lpayment->checkBalance($StudentID, 1);
$currentBalance = str_replace(",", "", $currentBalance[0]);

$student_subsidy_identity_id = '';
$student_subsidy_identity_records = $lpayment->getSubsidyIdentityStudents(array('StudentID'=>$StudentID,'StartDateWithinEffectiveDates'=>$lnotice->DateStart,'EndDateWithinEffectiveDates'=>$lnotice->DateEnd));
if(count($student_subsidy_identity_records)>0){
	$student_subsidy_identity_id = $student_subsidy_identity_records[0]['IdentityID'];
}
$NoticePaymentMethod = trim($_POST['NoticePaymentMethod']);
if(!in_array($NoticePaymentMethod,array('TNG','AlipayHK','FPS','TAPANDGO','PayAtSchool','VISAMASTER','WECHAT','AlipayCN'))){
	$NoticePaymentMethod = '';
}
$pay_at_school = $NoticePaymentMethod == 'PayAtSchool';

$use_tng = $lpayment->isTNGDirectPayEnabled() && $NoticePaymentMethod=='TNG';
$use_alipay = $lpayment->isAlipayDirectPayEnabled() && $NoticePaymentMethod=='AlipayHK';
$use_hsbc_fps = $lpayment->isFPSDirectPayEnabled() && $NoticePaymentMethod=='FPS';
$use_tapandgo = $lpayment->isTapAndGoDirectPayEnabled() && $NoticePaymentMethod=='TAPANDGO';
$use_visamaster = $lpayment->isVisaMasterDirectPayEnabled() && $NoticePaymentMethod == 'VISAMASTER';
$use_wechat = $lpayment->isWeChatDirectPayEnabled() && $NoticePaymentMethod == 'WECHAT';
$use_alipaycn = $lpayment->isAlipayCNDirectPayEnabled() && $NoticePaymentMethod == 'AlipayCN';

$use_payment_system = $use_tng || $use_alipay || $use_hsbc_fps || $use_tapandgo || $use_visamaster || $use_wechat ||  $use_alipaycn;
$do_not_check_balance = $sys_custom['PaymentNoticeDoNotCheckBalance'] || $use_payment_system || $pay_at_school;


$sql = "SELECT MerchantAccountID FROM INTRANET_NOTICE WHERE NoticeID='$NoticeID'";
$notice_merchant_account_record = $lnotice->returnResultSet($sql);
$notice_use_ewallet_to_pay = $notice_merchant_account_record[0]['MerchantAccountID'] != '';
if($notice_use_ewallet_to_pay == false) {
	if($sys_custom['ePayment']['MultiPaymentGateway']) {
		$sql = "SELECT
			a.ServiceProvider
			FROM INTRANET_NOTICE as s
			LEFT JOIN INTRANET_NOTICE_PAYMENT_GATEWAY as g ON g.NoticeID=s.NoticeID
			LEFT JOIN PAYMENT_MERCHANT_ACCOUNT as a ON a.AccountID=g.MerchantAccountID
			WHERE s.NoticeID='$NoticeID'";
		$rs_notice_merchant_account_record = $lnotice->returnResultSet($sql);
		$notice_merchant_account_record = array();
		foreach($rs_notice_merchant_account_record as $temp) {
			if($temp['ServiceProvider'] != '') {
				$notice_use_ewallet_to_pay = true;
				$notice_merchant_account_record[] = $temp['ServiceProvider'];
			}
		}
		if($notice_use_ewallet_to_pay) { //selected payment method not in list
			if(in_array($NoticePaymentMethod, $notice_merchant_account_record) == false) {
				$NoticePaymentMethod = '';
			}
		}
	}
}

// [2020-0323-1606-16207] check if enabled eWallet
$use_merchant_account = $lpayment->useEWalletMerchantAccount();

// [2020-0323-1606-16207]
// check if notice record has set merchant account, then it should use eWallet to pay, if notice payment method is not set, then there should be something wrong at sign page.
//if($notice_use_ewallet_to_pay && $NoticePaymentMethod == ''){
if($use_merchant_account && $notice_use_ewallet_to_pay && $NoticePaymentMethod == ''){
	intranet_closedb();
	header("Location: enotice_sign_eclassApp.php?NoticeID=".$NoticeID."&StudentID=".$StudentID."&CurID=".$CurID."&token=".$token);
	die();
}

$is_distributed_notice = $lnotice->RecordStatus == DISTRIBUTED_NOTICE_RECORD;

if($use_merchant_account) {
	if (!$is_distributed_notice || ($lnotice->DebitMethod != 2 && (!$do_not_check_balance))) {
		echo '<div style="padding:5px;">' . $Lang['ePayment']['PaymentProcessFail'] . '</div>';
		intranet_closedb();
		die();
	}
} else {
// [2017-0428-1605-58206] Debit Directly - Display error message
	if (!$is_distributed_notice || ($lnotice->DebitMethod != 2 && (!$do_not_check_balance && $currentBalance < $thisAmountTotal))) {
		echo '<div style="padding:5px;">' . $Lang['eNotice']['NotEnoughBalance'] . '</div>';
		intranet_closedb();
		die();
	}
}

// check late submission
if(!$lnotice->isLateSignAllow){
	$today_ts = time();
	$deadline_ts = strtotime($lnotice->DateEnd);
	$after_deadline = ($today_ts > $deadline_ts);
	if($after_deadline){
		echo '<div style="padding:5px;">'.$Lang['eNotice']['LateSubmission'].'</div>';
		intranet_closedb();
		die();
	}
}

//$lpayment->Start_Trans();

/*
if ($lu->isStudent())
{
    $valid = false;
}
$isTeacher = $lu->isTeacherStaff();
$isParent = $lu->isParent();
if ($StudentID!="")
{
    if ($isParent)
    {
        $children = $lu->getChildren();
        if (!in_array($StudentID,$children))
        {
             $valid = false;
        }
        else
        {
            $editAllowed = true;
        }
    }
    else # Teacher
    {
	    if($lnotice->IsModule)
	    {
			$editAllowed = true;    
	    }
	    else
	    {
        	$editAllowed = $lnotice->isEditAllowed($StudentID);
    	}
    }
}
else
{
    if ($isParent)
    {
        $valid = false;
    }
    else
    {
        $editAllowed = false;
    }
}
*/

$generate_item_students_for_zero_amount = $sys_custom['PaymentNoticeAlwaysGenerateItemStudents'] || $use_payment_system;

if ($editAllowed)
{
    if ($StudentID == "")
    {
        $valid = false;
    }
    else
    {
    	// initialize payment account record if ePayment module is lately installed
    	$sql = "INSERT IGNORE INTO PAYMENT_ACCOUNT (StudentID, Balance) VALUES ('$StudentID', 0)";
		$lnotice->db_db_query($sql);
    	
    	//$sql = "SELECT RecordStatus FROM INTRANET_NOTICE_REPLY WHERE NoticeID = $NoticeID AND StudentID = $StudentID";
    	//$isNoticeReplied = $lnotice->returnVector($sql);
    	if(!$lnotice->isTargetNoticeSigned($NoticeID, $StudentID))
    	{
			if($sys_custom['ePayment']['MultiPaymentGateway']) {
				if($use_merchant_account && $notice_use_ewallet_to_pay) {
					$requestData = array('StudentID'=> $StudentID,'NoticeID'=>$NoticeID);
					$returnJsonString = $lpayment->getENoticePaymentStatus($requestData);

					include_once($intranet_root."/includes/json.php");
					$jsonObj = new JSON_obj();
					$decodedJsonData = $jsonObj->decode($returnJsonString);

					if(isset($decodedJsonData['ErrorCode']) && !is_array($decodedJsonData['ErrorCode']) && !empty($decodedJsonData['ErrorCode'])) {
						$_SESSION['ENOTICE_PAYMENT_ERROR_RETURN'] = $decodedJsonData['ErrorCode'];

						intranet_closedb();
						if (isset($_SERVER['HTTP_REFERER'])) {
							header("Location: ".$_SERVER['HTTP_REFERER']);
						} else {
							echo '<script type="text/javascript">window.history.go(-1);</script>';
						}
						exit;
					}
				}
			}

    		## for Payment Notice, if no reply before, start processing.
    		## Otherwise, no need to process again.
	        $ansString = intranet_htmlspecialchars(trim($aStr));
	        $sql = "SELECT Question, DebitMethod FROM INTRANET_NOTICE WHERE NoticeID = '$NoticeID'";
	        $arrQuestion = $lnotice->returnArray($sql);
	        $qStr = $arrQuestion[0][0];
	        $debitMethod = $arrQuestion[0][1];
	        
	        /*
	        $arrPaymentItemDetails = $lnotice->returnPaymentItemDetails($qStr);
	        $arrAnswer = explode("#ANS#",$ansString);
			array_shift($arrAnswer);
			
			$notice_array = $lnotice->splitQuestion($qStr);
			$thisTotalAmount = 0;
			if(sizeof($arrPaymentItemDetails)>0)
			{
				foreach($arrPaymentItemDetails as $key=>$details)
				{
					$seletected_answer = $arrAnswer[$key-1];
					$payment_type = $notice_array[$key-1][0];
					switch($payment_type)
					{
					    case 1:
					    case 12:
							$PaymentItemName = $details['PaymentItem'];
							$PaymentItemID = $details['PaymentItemID'];
							$PaymentCategroyID = $details['PaymentCategory'];
							$PaymentAmount = $details['PaymentAmount'];
							
							if($PaymentAmount > 0 || $generate_item_students_for_zero_amount){
								if($seletected_answer != "")
								{
									if($seletected_answer == "0"){
										//$arrPaymentProcess[$key-1]['PaymentItemID'] = $PaymentItemID;
										//$arrPaymentProcess[$key-1]['PaymentAmount'] = $PaymentAmount;
										$arrPaymentProcess[] = $PaymentItemID;
										$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
										$thisTotalAmount += $PaymentAmount;
									}
								}
							}
							//echo $thisTotalAmount.'(1)'; exit;
							break;
						case 2:
							if($details['PaymentAmount'][$seletected_answer]!=0)
							{
								$PaymentItemName = $details['PaymentItem'];
								$PaymentItemID = $details['PaymentItemID'];
								$PaymentCategroyID = $details['PaymentCategory'];
								$PaymentAmount = $details['PaymentAmount'][$seletected_answer];
								
								if($PaymentAmount > 0 || $generate_item_students_for_zero_amount){
									//$arrPaymentProcess[$key-1]['PaymentItemID'][] = $PaymentItemID;
									//$arrPaymentProcess[$key-1]['PaymentAmount'][] = $PaymentAmount;
									$arrPaymentProcess[] = $PaymentItemID;
									$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
									$thisTotalAmount += $PaymentAmount;
								}
							}
							//echo $thisTotalAmount.'(2)'; exit;
							break;
						case 3:
							$final_ans = explode(",",$seletected_answer);
							if(sizeof($final_ans)>0)
							{
								for($i=0; $i<sizeof($final_ans); $i++)
								{
									$PaymentItemName = $details['PaymentItem'][$final_ans[$i]];
									$PaymentAmount = $details['PaymentAmount'][$final_ans[$i]];
									$PaymentItemID = $details['PaymentItemID'][$final_ans[$i]];
									if($PaymentAmount != 0 || $generate_item_students_for_zero_amount)
									{
										$arrPaymentProcess[] = $PaymentItemID;
										$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
										$thisTotalAmount += $PaymentAmount;
									}
								}
							}
							//echo $thisTotalAmount.'(3)'; exit;
							break;
						case 4:
							break;
						case 5:
							break;
						case 6:
							break;
						case 7:
							break;
						case 8:
							break;
						case 9:					
							$PaymentItemName = $details['PaymentItem'][$seletected_answer];
							$PaymentAmount = $details['PaymentAmount'][$seletected_answer];
							$PaymentItemID = $details['PaymentItemID'][$seletected_answer];
							
							//$arrPaymentProcess[$key-1]['PaymentItemID'] = $PaymentItemID;
							//$arrPaymentProcess[$key-1]['PaymentAmount'] = $PaymentAmount;
							if($PaymentAmount != 0 || $generate_item_students_for_zero_amount)
							{
								$arrPaymentProcess[] = $PaymentItemID;
								$arrPaymentAmount[$PaymentItemID]['Amount'] = $PaymentAmount;
								$thisTotalAmount += $PaymentAmount;
							}
							//echo $thisTotalAmount.'(9)'; exit;
							break;
					}
				}
			}
			*/
			
			$thisTotalAmount = 0;
			$arrPaymentProcess = array();
			$arrPaymentAmount = array();
			$questions = $eNoticePaymentReplySlip->parseQuestionString($qStr);
			$questions = $eNoticePaymentReplySlip->applySubsidySettingsToQuestions($questions,$StudentID,$student_subsidy_identity_id);
			$questions_with_answers = $eNoticePaymentReplySlip->parseAnswerString($aStr, $questions);
			$all_payment_item_ids = $eNoticePaymentReplySlip->getAllPaymentItemID($qStr);
			for($i=0;$i<count($questions_with_answers);$i++)
			{
				$q = $questions_with_answers[$i];
				$ans = $questions_with_answers[$i]['Answer'];
				if(!$eNoticePaymentReplySlip->isPaymentTypeQuestion($q['Type'])){
					// skip processing non-payment questions
					continue;
				}
				
				$tmp_payment_item_id = '';
				$tmp_amount = 0;
				$tmp_original_amount = 0;
				if(is_array($ans))
				{
					if(count($ans)==0){
						// empty answer, must be skipped question
						continue;
					}
					foreach($ans as $tmp_ans){
						if($q['Type'] == QTYPE_FEW_PAY_FEW_CAT_WITH_INPUT_QUANTITY){
							$tmp_ans_index = $tmp_ans['Option'];
							$tmp_ans_quantity = $tmp_ans['Quantity'];
						}else{
							$tmp_ans_index = $tmp_ans;
							$tmp_ans_quantity = 1;
						}
						$tmp_payment_item_id = $q['Options'][$tmp_ans_index]['PaymentItemID'];
						$tmp_amount = $q['Options'][$tmp_ans_index]['Amount'];
						$tmp_original_amount = $q['Options'][$tmp_ans_index]['OriginalAmount'];

                        // [2019-1216-0938-05207] skip if item id is empty
                        //if($tmp_amount != 0 || $generate_item_students_for_zero_amount){
						if($tmp_payment_item_id != '' && ($tmp_amount != 0 || $generate_item_students_for_zero_amount)){
							$arrPaymentProcess[] = $tmp_payment_item_id;
							$arrPaymentAmount[$tmp_payment_item_id]['Amount'] = $tmp_amount * $tmp_ans_quantity;
							$arrPaymentAmount[$tmp_payment_item_id]['OriginalAmount'] = $tmp_original_amount * $tmp_ans_quantity;
							$thisTotalAmount += $tmp_amount * $tmp_ans_quantity;
						}
					}
				}
				else if($ans != '')
				{
					if(isset($q['Options'])){
						$tmp_amount = $q['Options'][$ans]['Amount'];
						$tmp_original_amount = $q['Options'][$ans]['OriginalAmount'];
					}else{
						$tmp_amount = $q['Amount'];
						$tmp_original_amount = $q['OriginalAmount'];
					}
					if(isset($q['PaymentItemID'])){
						if($q['Type'] != QTYPE_WHETHER_TO_PAY || ($q['Type'] == QTYPE_WHETHER_TO_PAY && $ans == '0')){
							$tmp_payment_item_id = $q['PaymentItemID'];
						}
					}else{
						$tmp_payment_item_id = $q['Options'][$ans]['PaymentItemID'];
					}
					if($tmp_payment_item_id != '' && ($tmp_amount != 0 || $generate_item_students_for_zero_amount)){
						$arrPaymentProcess[] = $tmp_payment_item_id;
						$arrPaymentAmount[$tmp_payment_item_id]['Amount'] = $tmp_amount;
						$arrPaymentAmount[$tmp_payment_item_id]['OriginalAmount'] = $tmp_original_amount;
						$thisTotalAmount += $tmp_amount;
					}
				}
			}
			
			// do not just believe the submitted value $thisAmountTotal(which is calculated by js) from frontend, double check the calculated total amount to pay again
			if($lnotice->DebitMethod != 2 && (!$do_not_check_balance && $currentBalance < $thisTotalAmount)) {
				echo '<div style="padding:5px;">'.$Lang['eNotice']['NotEnoughBalance'].'</div>';
				intranet_closedb();
				die();
			}

			/*
			if(count($all_payment_item_ids)>0){
				// try to remove the previous generated payment item student records that are failed to pay 
				$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ('".implode("','",$all_payment_item_ids)."') AND StudentID = '$StudentID' AND RecordStatus <> '1'";
				$lpayment->db_db_query($sql);
			}
			*/
			
			$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ('".implode("','",$all_payment_item_ids)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
			$existing_payment_ids = $lpayment->returnVector($sql);
			
			// if use TNG to pay and have items to pay for, run the following code to get QRCode of TNG
			// or if use Alipay to pay for, call the alipay app
			if(!$pay_at_school && $use_payment_system && sizeof($arrPaymentProcess)>0)
			{
				include_once($intranet_root."/includes/json.php");
				$jsonObj = new JSON_obj();

				/*
				$tmp_all_payment_item_ids = Get_Array_By_Key($arrPaymentItemDetails,'PaymentItemID');
				$all_payment_item_ids = array();
				for($i=0;$i<count($tmp_all_payment_item_ids);$i++){
					if(is_array($tmp_all_payment_item_ids[$i])){
						foreach($tmp_all_payment_item_ids[$i] as $tmp_item_id){
							$all_payment_item_ids[] = $tmp_item_id;
						}
					}else{
						$all_payment_item_ids[] = $tmp_all_payment_item_ids[$i];
					}
				}
				if(count($all_payment_item_ids)>0){
					// try to remove the previous generated payment item student records that are failed to pay 
					$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ('".implode("','",$all_payment_item_ids)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
					$lpayment->db_db_query($sql);
				}
				*/

				// prepare the item student records
				$payment_id_ary = array();
				foreach($arrPaymentProcess as $cur_paymentItemID)
				{
					$tmp_payment_id = '';
					$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$cur_paymentItemID' AND StudentID = '$StudentID'";
					$tmp_records = $lpayment->returnArray($sql);
					if(count($tmp_records)>0){
						$tmp_payment_id = $tmp_records[0]['PaymentID'];
						$payment_id_ary[] = $tmp_records[0]['PaymentID'];
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount='".$arrPaymentAmount[$cur_paymentItemID]['Amount']."' WHERE PaymentID='$tmp_payment_id' AND ItemID = '$cur_paymentItemID' AND StudentID = '$StudentID' ";
						$lpayment->db_db_query($sql);
						//$lpayment->Commit_Trans();
					}else{
						$sql = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT (ItemID,StudentID,Amount,RecordType,RecordStatus,DateInput,DateModified)
								VALUES ('$cur_paymentItemID','$StudentID','".$arrPaymentAmount[$cur_paymentItemID]['Amount']."','0','0',NOW(),NOW())";
						$insert_success = $lpayment->db_db_query($sql);
						if($insert_success){
							$inserted_payment_id = $lpayment->db_insert_id();
							$tmp_payment_id = $inserted_payment_id;
							$payment_id_ary[] = $inserted_payment_id;
						}
						//$lpayment->Commit_Trans();
					}
					if($tmp_payment_id != '' && $tmp_payment_id > 0){
						// record original amount and subsidy identity to payment record
						$is_subsidized = ($arrPaymentAmount[$cur_paymentItemID]['OriginalAmount'] - $arrPaymentAmount[$cur_paymentItemID]['Amount']) > 0 && $student_subsidy_identity_id != '';
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET OriginalAmount='".$arrPaymentAmount[$cur_paymentItemID]['OriginalAmount']."' ".($is_subsidized?",SubsidyIdentityID='$student_subsidy_identity_id'":",SubsidyIdentityID=NULL")." WHERE PaymentID='$tmp_payment_id'";
						$lpayment->db_db_query($sql);
					}
				}
				
				// Mark down the payment method in payment item student records
				//$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET NoticePaymentMethod='".$lpayment->Get_Safe_Sql_Query($NoticePaymentMethod)."' WHERE PaymentID IN (".implode(",",$payment_id_ary).")";
				//$lpayment->db_db_query($sql);
				
				// clean up those payment item student records that not in use anymore
				if(count($payment_id_ary)>=0 && count($existing_payment_ids)>0){
					$payment_ids_to_remove = count($payment_id_ary)>0 ? array_values(array_diff($existing_payment_ids,$payment_id_ary)) : $existing_payment_ids;
					if(count($payment_ids_to_remove)>0){
						$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
						$lpayment->db_db_query($sql);
					}
				}
				//$lpayment->Commit_Trans();
				
				if($thisTotalAmount > 0) // only need to pay by eWallet when greater than $0
				{
					if($use_tng) {
						$requestData = array('StudentID'=> $StudentID,'PaymentID'=>$payment_id_ary[0],'PayerUserID'=>$CurID,'Amount'=>$thisTotalAmount,'PaymentTitle'=>$lnotice->Title,'NoticeID'=>$NoticeID,'NoticePaymentID'=>implode(',',$payment_id_ary),'NoticeReplyAnswer'=>$ansString);
						$returnJsonString = $lpayment->getTngQrcode($requestData);
						$decodedJsonData = $jsonObj->decode($returnJsonString);
					}
					if($use_hsbc_fps) {
						$qrcode_title = '';
						if($sys_custom['ePayment']['FPSUseQRCode']) {
							$userInfo = $lu->returnUser($StudentID);
							$qrcode_title = $userInfo[0]['ClassName'].'-'.$userInfo[0]['ClassNumber'];
						}
						$requestData = array('StudentID'=> $StudentID, 'qrCodeTitle'=>$qrcode_title, 'ServiceProvider'=>'FPS','PaymentID'=>$payment_id_ary[0],'PayerUserID'=>$CurID,'Amount'=>$thisTotalAmount,'PaymentTitle'=>$lnotice->Title,'NoticeID'=>$NoticeID,'NoticePaymentID'=>implode(',',$payment_id_ary),'NoticeReplyAnswer'=>$ansString);
						$returnJsonString = $lpayment->getTngQrcode($requestData);
						$decodedJsonData = $jsonObj->decode($returnJsonString);
					}

					if($use_visamaster) {
						$requestData = array('StudentID'=> $StudentID, 'ServiceProvider'=>'VISAMASTER','PaymentID'=>$payment_id_ary[0],'PayerUserID'=>$CurID,'Amount'=>$thisTotalAmount,'PaymentTitle'=>$lnotice->Title,'NoticeID'=>$NoticeID,'NoticePaymentID'=>implode(',',$payment_id_ary),'NoticeReplyAnswer'=>$ansString);
						$returnJsonString = $lpayment->getTngQrcode($requestData);
						$decodedJsonData = $jsonObj->decode($returnJsonString);

					}
					if($use_wechat) {
						$requestData = array('StudentID'=> $StudentID, 'ServiceProvider'=>'WECHAT','PaymentID'=>$payment_id_ary[0],'PayerUserID'=>$CurID,'Amount'=>$thisTotalAmount,'PaymentTitle'=>$lnotice->Title,'NoticeID'=>$NoticeID,'NoticePaymentID'=>implode(',',$payment_id_ary),'NoticeReplyAnswer'=>$ansString);
						$returnJsonString = $lpayment->getTngQrcode($requestData);
						$decodedJsonData = $jsonObj->decode($returnJsonString);

					}

/*
					debug_pr($requestData);
					var_dump($returnJsonString);
					debug_pr($decodedJsonData);
					debug_pr($_POST);
					intranet_closedb();
					exit;
*/
					if($use_alipay || $use_alipaycn || $use_tapandgo || ($use_tng && isset($decodedJsonData['MethodResult']) && isset($decodedJsonData['MethodResult']['qrCode']) && $decodedJsonData['MethodResult']['qrCode']!='')
					 || ($use_hsbc_fps && isset($decodedJsonData['MethodResult']) && isset($decodedJsonData['MethodResult']['qrCode']) && $decodedJsonData['MethodResult']['qrCode']!='' && isset($decodedJsonData['MethodResult']['paymentRequestObjectUrl']) && $decodedJsonData['MethodResult']['paymentRequestObjectUrl']!='')
					 || ($use_visamaster && isset($decodedJsonData['MethodResult']) && isset($decodedJsonData['MethodResult']['paymentUrl']) && $decodedJsonData['MethodResult']['paymentUrl']!='')
					 || ($use_wechat && isset($decodedJsonData['MethodResult']) && isset($decodedJsonData['MethodResult']['response_data']) && $decodedJsonData['MethodResult']['response_data']!='')
					){
						// Mark down the payment method in payment item student records
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET NoticePaymentMethod='".$lpayment->Get_Safe_Sql_Query($NoticePaymentMethod)."' WHERE PaymentID IN (".implode(",",$payment_id_ary).")";
						$lpayment->db_db_query($sql);

						//echo '<a href="'.$decodedJsonData['MethodResult']['qrCode'].'" target="_blank">'.$decodedJsonData['MethodResult']['qrCode'].'</a>';
						$type = ($isParent? 1: 2);

				        # Update
				        /*
				        $sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer = '$ansString',
				                RecordType = '$type', RecordStatus = 2, SignerID=$CurID, DateModified=now(), SignerAuthCode = '".$lnotice->Get_Safe_Sql_Query($authCode)."'
				                WHERE NoticeID = $NoticeID AND StudentID = $StudentID";
				        */
				        $sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer = '$ansString',
				                RecordType = '$type', SignerID = '$CurID', DateModified = now(), SignerAuthCode = '".$lnotice->Get_Safe_Sql_Query($authCode)."'
				                WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
				        $temp = $lnotice->db_db_query($sql);
				        /*
				        if($temp) {
				        	$lpayment->Commit_Trans();
				        }
				        else {
				        	$lpayment->RollBack_Trans();
				        }
				        */

				        intranet_closedb();
				        if($use_tng){
							header("Location: enotice_payment_by_tng.php?qrcode=".base64_encode($decodedJsonData['MethodResult']['qrCode']));
				        }
				        if($use_alipay){
				        	header("Location: enotice_payment_by_alipay.php?paymentID=".$payment_id_ary[0]."&amount=".$thisTotalAmount."&paymentTitle=".urlencode($lnotice->Title).'&NoticeID='.$NoticeID.'&NoticePaymentID='.implode(',',$payment_id_ary).'&NoticeReplyAnswer='.urlencode($ansString));
				        }
						if($use_alipaycn){
							header("Location: enotice_payment_by_alipaycn.php?paymentID=".$payment_id_ary[0]."&amount=".$thisTotalAmount."&paymentTitle=".urlencode($lnotice->Title).'&NoticeID='.$NoticeID.'&NoticePaymentID='.implode(',',$payment_id_ary).'&NoticeReplyAnswer='.urlencode($ansString));
						}
				        if($use_hsbc_fps){
				        	if($sys_custom['ePayment']['FPSUseQRCode']) {
								header("Location: " . $decodedJsonData['MethodResult']['qrCodeUrl']);
							} else {
								header("Location: enotice_payment_by_fps.php?url=" . base64_encode($decodedJsonData['MethodResult']['paymentRequestObjectUrl']));
							}
				        }
				        if($use_visamaster) {
							header("Location: ".$decodedJsonData['MethodResult']['paymentUrl']);
						}
						if($use_wechat){
							header("Location: enotice_payment_by_wechat.php?data=".$decodedJsonData['MethodResult']['response_data']);
						}
				        if($use_tapandgo){
				        	header("Location: enotice_payment_by_tapandgo.php?paymentID=".$payment_id_ary[0]."&amount=".$thisTotalAmount."&paymentTitle=".urlencode($lnotice->Title).'&NoticeID='.$NoticeID.'&NoticePaymentID='.implode(',',$payment_id_ary).'&NoticeReplyAnswer='.urlencode($ansString));
				        }
					}else{
						//show alert
						intranet_closedb();
						@session_start();
						if(is_array($decodedJsonData['ErrorCode'])) {
							$_SESSION['ENOTICE_PAYMENT_ERROR_RETURN'] = $decodedJsonData['MethodResult']['errorCode'];
						} else {
							$_SESSION['ENOTICE_PAYMENT_ERROR_RETURN'] = $decodedJsonData['ErrorCode'];
						}
						if(isset($_SERVER['HTTP_REFERER'])){
							header("Location: ".$_SERVER['HTTP_REFERER']);
						}else{
							echo '<script type="text/javascript">window.history.go(-1);</script>';
						}
					}
					exit;
				}
			}
			// otherwise, just sign the notice
			
			//echo $thisTotalAmount; exit;
			if($pay_at_school)
			{
				// prepare the item student records
				$payment_id_ary = array();
				foreach($arrPaymentProcess as $cur_paymentItemID)
				{
					$tmp_payment_id = '';
					$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID = '$cur_paymentItemID' AND StudentID='$StudentID'";
					$tmp_records = $lpayment->returnArray($sql);
					if(count($tmp_records)>0){
						$tmp_payment_id = $tmp_records[0]['PaymentID'];
						$payment_id_ary[] = $tmp_records[0]['PaymentID'];
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET Amount='".$arrPaymentAmount[$cur_paymentItemID]['Amount']."' WHERE PaymentID='$tmp_payment_id' AND ItemID = '$cur_paymentItemID' AND StudentID = '$StudentID' ";
						$lpayment->db_db_query($sql);
					}else{
						$sql = "INSERT INTO PAYMENT_PAYMENT_ITEMSTUDENT (ItemID,StudentID,Amount,RecordType,RecordStatus,DateInput,DateModified)
								VALUES ('$cur_paymentItemID','$StudentID','".$arrPaymentAmount[$cur_paymentItemID]['Amount']."','0','0',NOW(),NOW())";
						$insert_success = $lpayment->db_db_query($sql);
						if($insert_success){
							$inserted_payment_id = $lpayment->db_insert_id();
							$tmp_payment_id = $inserted_payment_id;
							$payment_id_ary[] = $inserted_payment_id;
						}
						//$lpayment->Commit_Trans();
					}
					if($tmp_payment_id != '' && $tmp_payment_id > 0){
						// record original amount and subsidy identity to payment record
						$is_subsidized = ($arrPaymentAmount[$cur_paymentItemID]['OriginalAmount'] - $arrPaymentAmount[$cur_paymentItemID]['Amount']) > 0 && $student_subsidy_identity_id != '';
						$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET OriginalAmount='".$arrPaymentAmount[$cur_paymentItemID]['OriginalAmount']."' ".($is_subsidized?",SubsidyIdentityID='$student_subsidy_identity_id'":",SubsidyIdentityID=NULL")." WHERE PaymentID='$tmp_payment_id'";
						$lpayment->db_db_query($sql);
					}
				}
				
				if(count($payment_id_ary)){
					// Mark down the payment method in payment item student records
					$sql = "UPDATE PAYMENT_PAYMENT_ITEMSTUDENT SET NoticePaymentMethod='".$lpayment->Get_Safe_Sql_Query($NoticePaymentMethod)."' WHERE PaymentID IN (".implode(",",$payment_id_ary).")";
					$lpayment->db_db_query($sql);
				}
				
				// clean up those payment item student records that not in use anymore
				if(count($payment_id_ary)>=0 && count($existing_payment_ids)>0){
					$payment_ids_to_remove = count($payment_id_ary)>0 ? array_values(array_diff($existing_payment_ids,$payment_id_ary)) : $existing_payment_ids;
					if(count($payment_ids_to_remove)>0){
						$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
						$lpayment->db_db_query($sql);
					}
				}
			}
			else if(sizeof($arrPaymentProcess)>0)
			{
				$paid_item_ids = array();
				foreach($arrPaymentProcess as $key=>$paymentItemID)
				{
					$paid_item_ids[] = $paymentItemID;
					$targetPaymentItemID = $paymentItemID;
					$targetPaymentAmount = $arrPaymentAmount[$paymentItemID]['Amount'];	
					
					$result[] = $lpayment->Process_Notice_Payment_Item($targetPaymentItemID,$StudentID,$targetPaymentAmount,$lnotice->DebitMethod,true);				
				}

				// find the paid payment items to exclude
				$paid_payment_ids = array();
				if(count($paid_item_ids)>0)
				{
					$sql = "SELECT PaymentID FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID IN ('".implode("','",$paid_item_ids)."') AND StudentID='$StudentID' AND RecordStatus='1'";
					$paid_payment_ids = $lpayment->returnVector($sql);
				}

				// clean previous answer(s) generated payment items, exclude the paid payment items
				if(count($existing_payment_ids)>0){
					$payment_ids_to_remove = count($paid_payment_ids)>0 ? array_values(array_diff($existing_payment_ids,$paid_payment_ids)) : $existing_payment_ids;
					if(count($payment_ids_to_remove)>0){
						$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
						$lpayment->db_db_query($sql);
					}
				}
				/*
				if(!in_array(false, $result))
				{
					$lpayment->Commit_Trans();
				}
				else
				{
					$lpayment->RollBack_Trans();
				}
				*/
			}
			else if(count($existing_payment_ids)>0)  // final answers no payment items involved, clean all previous generated non-paid payment items
			{
				$payment_ids_to_remove = $existing_payment_ids;
				if(count($payment_ids_to_remove)>0){
					$sql = "DELETE FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE PaymentID IN ('".implode("','",$payment_ids_to_remove)."') AND StudentID='$StudentID' AND RecordStatus<>'1'";
					$lpayment->db_db_query($sql);
				}
			}
			
	        $type = ($isParent? 1: 2);
	
	        # Update
	        $sql = "UPDATE INTRANET_NOTICE_REPLY SET Answer = '$ansString',
	                RecordType = '$type', RecordStatus = 2, SignerID = '$CurID', DateModified=now(), SignerAuthCode = '".$lnotice->Get_Safe_Sql_Query($authCode)."' ".($NoticePaymentMethod!=''?",NoticePaymentMethod='".$lnotice->Get_Safe_Sql_Query($NoticePaymentMethod)."' ":"")." 
	                WHERE NoticeID = '$NoticeID' AND StudentID = '$StudentID'";
	        $temp = $lnotice->db_db_query($sql);
	        /*
	        if($temp) {
	        	$lpayment->Commit_Trans();
	        }
	        else {
	        	$lpayment->RollBack_Trans();
	        }
	        */

	        # Notify and confirm the parent by sending push message via Parent App
	        if ($plugin['eClassApp'])
	        {
	        	if (in_array($StudentID, $lu->getStudentWithParentUsingParentApp())) {
	        		include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");
					$leClassApp = new libeClassApp();
					
		        	$MsgNotifyDateTime = date("Y-m-d H:i:s");
		        	$MessageTitle = $Lang['AppNotifyMessage']['eNoticeSigned'];
					$MessageTitle = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lnotice->NoticeNumber), $MessageTitle);
		        	$MessageContent = str_replace("[sign_datetime]", $MsgNotifyDateTime, $Lang['AppNotifyMessage']['eNoticeSignedContent_eClassApp']);
					$MessageContent = str_replace("[sign_notice_title]", intranet_undo_htmlspecialchars($lnotice->Title), $MessageContent);
					$MessageContent = str_replace("[sign_notice_number]", intranet_undo_htmlspecialchars($lnotice->NoticeNumber), $MessageContent);
				
					### get parent student mapping
					$sql = "SELECT 
									ip.ParentID, ip.StudentID
							FROM 
									INTRANET_PARENTRELATION AS ip 
									INNER JOIN INTRANET_USER AS iu ON (ip.ParentID=iu.UserID) 
							WHERE 
									ip.StudentID = '".$StudentID."'
									AND iu.RecordStatus = 1";
					$parentStudentAssoAry = BuildMultiKeyAssoc($lnotice->returnResultSet($sql), 'ParentID', array('StudentID'), $SingleValue=1, $BuildNumericArray=1);
					
					$messageInfoAry = array();
					$messageInfoAry[0]['relatedUserIdAssoAry'] = $parentStudentAssoAry;
					$notifyMessageId = $leClassApp->sendPushMessage($messageInfoAry, $MessageTitle, $MessageContent, $isPublic='', $recordStatus=0);
	        	}
	        }
	        //if (true || $sys_custom['enotice_sign_notify_by_app'])
	        else if ($plugin['ASLParentApp'])
	        {
	        	include_once($PATH_WRT_ROOT."plugins/asl_conf.php");
				include_once($PATH_WRT_ROOT."includes/libaslparentapp.php");
				$asl = new ASLParentApp($asl_parent_app["host"], $asl_parent_app["path"]);
				
				$SchoolID = isset($asl_parent_app['SchoolID'])? $asl_parent_app['SchoolID'] : $_SERVER['SERVER_NAME'];
				$MsgNotifyDateTime = date("Y-m-d H:i:s");
				
				$sql = "SELECT UserLogin, ChineseName, Englishname FROM INTRANET_USER where UserID = '{$CurID}'";
				$row_user = $lnotice->returnArray($sql);
				$parent_userlogin = $row_user[0]["UserLogin"];
				if ($row_user[0]["ChineseName"]!=$row_user[0]["Englishname"])
				{
					$parent_name = $row_user[0]["ChineseName"] . " ".$row_user[0]["Englishname"];	
				}
				else
				{
					$parent_name = (trim($row_user[0]["ChineseName"])!="") ? $parent_name = $row_user[0]["ChineseName"] : $row_user[0]["Englishname"];
				}
				
				$notic_content = str_replace("[sign_notice_title]", $lnotice->Title,str_replace("[sign_datetime]", $MsgNotifyDateTime, $Lang['AppNotifyMessage']['eNoticeSignedContent']));
				
				$ASLParam = array(
						"SchID" => $SchoolID,
						"MsgNotifyDateTime" => $MsgNotifyDateTime,
						"MessageTitle" => $Lang['AppNotifyMessage']['eNoticeSigned'],
						"MessageContent" => $notic_content,
						"NotifyFor" => "",
						"IsPublic" => "N",
						"NotifyUser" => $parent_name,
						"TargetUsers" => array(
							"TargetUser" => array($parent_userlogin)
						)
					);
				//debug_r($ASLParam);die();
				
				$ReturnMessage = "";
				$Result = $asl->NotifyMessage($ASLParam, $ReturnMessage);
				
				$debug_log = $sys_custom['ASLParentAppDebugLog'];
				if ($debug_log)
				{
					$log_file = $file_path."/file/asl_app_test.txt";
					$file_handle = fopen($log_file,"a+");
					fwrite($file_handle,$asl->postData."\n".$Result."\n".$ReturnMessage."\n\n");
					fclose($file_handle);
				}
	        }
	    }
    }
    $lnotice->retrieveReply($StudentID);

    $ansString = $lnotice->answer;
    $ansString =  str_replace("&amp;", "&", $ansString);
	$ansString = $lform->getConvertedString($ansString);
	$ansString .= trim($ansString)." ";
	
	$lnotice->retrieveAmount($StudentID);
	if($lnotice->countAmount != "")
		$countAmt = implode(',', $lnotice->countAmount);
	if($lnotice->AmountValue != "")
		$amountValue = implode(',', $lnotice->AmountValue);
}

if (!$valid)
{
    include_once($PATH_WRT_ROOT."includes/libaccessright.php");
	$laccessright = new libaccessright();
	$laccessright->NO_ACCESS_RIGHT_REDIRECT($i_general_no_access_right);
	exit;
}

intranet_closedb();
//$linterface->LAYOUT_STOP();

header("Location: enotice_sign_update.php");
?>