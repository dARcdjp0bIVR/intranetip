<?
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

//action value from URL 
$act= trim($act);

$returnStr = "Invalid Action";//init as invalid action as default 


if($act == ''){
	//$act is empty , echo error message
	echo $returnStr;
	exit();
}



intranet_opendb();
switch(strtoupper($act)){
	case "RA":// Ipf Remain Account
		$returnStr = getIPFRemainAccount();			
		break;
	default:
		$returnStr = "Invalid Action";
		break;
}
echo $returnStr ;

intranet_closedb();


exit();

function getIPFRemainAccount(){
	global $PATH_WRT_ROOT, $intranet_root;
	include_once($PATH_WRT_ROOT."includes/portfolio25/libpf-accountmanager.php");
	$lpf_acm = new libpf_accountmanager();
	$lpf_acm->SET_LICENSE_KEYS();
	$returnVal = $lpf_acm->GET_NUMBER_FREE_LICENSE();
	return $returnVal;
}
?>