<?php
// Using:
/*
 *  2020-08-11 Bill     [2020-0811-0932-16235]
 *      - skip login secure token checking for all SSO access
 *
 *  2020-08-05 Bill     [2020-0805-0918-34235]
 *      - skip login secure token checking in login.php
 *
 *  2020-03-26 Bill
 *      - fixed checking when append 'App' to $onlyModule
 *
 *  2020-02-20 Cameron
 *      - append 'App' to $onlyModule for eInventory 
 */

$NoLangWordings = true;

$PATH_WRT_ROOT = '../../';
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp.php");

intranet_opendb();

$p = standardizeFormGetValue($_GET['p']);
$parLang = standardizeFormGetValue($_GET['parLang']);
$apiToken = standardizeFormGetValue($_GET['at']);
$parRecordID = standardizeFormGetValue($_GET['RecordID']);
$parRecordDate = standardizeFormGetValue($_GET['RecordDate']);
$parStudentID = standardizeFormGetValue($_GET['StudentID']);
$parAppType = standardizeFormGetValue($_GET['AppType']);

$leClassApp = new libeClassApp();

$paramAry = $leClassApp->getSSOParamAry($p);
$userLogin = $paramAry['ul'];
$hashPassword = $paramAry['hp'];
$apiKey = $paramAry['ak'];
$schoolCode = $paramAry['sc'];
$onlyModule = $paramAry['m'];

if (empty($userLogin) || empty($hashPassword) || empty($apiKey) || empty($schoolCode) || empty($onlyModule)) {
	die();
}

session_register_intranet("tmp_UserLogin", $userLogin);
session_register_intranet("tmp_UserPassword", $hashPassword);
session_register_intranet("tmp_APIKey", $apiKey);
session_register_intranet("tmp_SchoolCode", $schoolCode);
session_register_intranet("tmp_From_eClassApp", 1);
session_register_intranet("tmp_ApiToken", $apiToken);

if ($parLang != '') {
	include_once($PATH_WRT_ROOT."includes/eClassApp/libeClassApp_init.php");
	$leClassAppInit = new libeClassApp_init();
	
	$tmpHardCodeLang = $leClassAppInit->getPageLang($parLang);
	session_register_intranet("intranet_hardcode_lang", $tmpHardCodeLang);
}

if ($parRecordID != '') {
	session_register_intranet("tmp_appSSO_recordID", $parRecordID);
}
if ($parRecordDate != '') {
	session_register_intranet("tmp_appSSO_recordDate", $parRecordDate);
}
if ($parStudentID != '') {
    session_register_intranet("tmp_appSSO_studentId", $parStudentID);
}

if ($parAppType != '') {
    session_register_intranet("tmp_appSSO_appType", $parAppType);
}

//if ($onlyModule == $eclassAppConfig['moduleCode']['eDiscipline'] || $onlyModule == $eclassAppConfig['moduleCode']['iMail'] || $onlyModule == $eclassAppConfig['moduleCode']['iPortfolio'] || $eclassAppConfig['moduleCode']['eInventory']) {
if ($onlyModule == $eclassAppConfig['moduleCode']['eDiscipline'] || $onlyModule == $eclassAppConfig['moduleCode']['iMail'] || $onlyModule == $eclassAppConfig['moduleCode']['iPortfolio'] || $onlyModule == $eclassAppConfig['moduleCode']['eInventory']) {
	$onlyModule .= 'App';

    // [2020-0805-0918-34235] no need to check secure token in login.php
    //session_register_intranet("tmp_appSSO_skipSecureTokenCheck", true);
}

// [2020-0811-0932-16235] apply for all access
session_register_intranet("tmp_appSSO_skipSecureTokenCheck", true);

intranet_closedb();
header("location: /login.php?OnlyModule=".$onlyModule);
die();
?>