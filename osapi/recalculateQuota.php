<?php
/*
 * @Creation date: 2015-07-23
 * @Param string email : email address with @domain
 * @Param string module : iMail
 * @Param string actype : postfix
 * @Description: This script is used to force remove existing maildirsize file and let system recalculate the usaed quota of the email account.
 * @Return 1: success, 0: fail
 */
include_once("includes/global.php");
include_once("includes/liblinux.php");

$email = strtolower(urldecode($_REQUEST['email']));
$module = $_REQUEST['module'];
$actype = $_REQUEST['actype'];
$at_pos = strpos($email,'@');

$llinux = new liblinux($actype);

if($actype == 'postfix' && $module=='iMail' && $at_pos > 0)
{
	$userlogin = substr($email,0,$at_pos);
	$domain = substr($email,$at_pos+1);
	//$maildirsize_path = '/home/domains/'.$domain.'/'.$userlogin.'/Maildir/maildirsize';
	//shell_exec("sudo rm -f '".$maildirsize_path."'");
	
	$maildir_path = '/home/domains/'.$domain.'/'.$userlogin.'/Maildir';
	$maildir_trash_path = '/home/domains/'.$domain.'/'.$userlogin.'/Maildir/.Trash'; // exclude .Trash folder
	$maildir_reportspam_path = '/home/domains/'.$domain.'/'.$userlogin.'/Maildir/.reportSpam'; // exclude .reportSpam folder
	$maildir_reportnonspam_path = '/home/domains/'.$domain.'/'.$userlogin.'/Maildir/.reportNonSpam'; // exclude .reportNonSpam folder
	$maildirsize_path = '/home/domains/'.$domain.'/'.$userlogin.'/Maildir/maildirsize';
	$tmp_maildirsize_path = '/tmp/maildirsize_'.$userlogin;
	
	$cmd_peek = escapeshellcmd('sudo cat '.$maildirsize_path);
	$peek_content = trim(shell_exec($cmd_peek));
	$lines = explode("\n",$peek_content);
	$need_recalculate = count($lines)>2;
	if(!$need_recalculate){
		echo "1";
		exit;
	}
	
	$cmd = 'sudo du -sb --exclude="'.$maildir_trash_path.'" --exclude="'.$maildir_reportspam_path.'" --exclude="'.$maildir_reportnonspam_path.'" "'.$maildir_path.'"';
	$cmd_output = shell_exec($cmd);
	$used_quota_in_bytes = intval($cmd_output);
	
	$total_quota = $llinux->getTotalQuota($email,$module); // get total quota in MB
	$total_quota_in_bytes = $total_quota * 1024 * 1024;
	// prepare maildirsize content
	$file_content = $total_quota_in_bytes."S\n";
	$file_content .= sprintf("%12s%14s", $used_quota_in_bytes, "1");
	
	// write the content to tmp file
	shell_exec('sudo touch '.escapeshellcmd($tmp_maildirsize_path));
	shell_exec('sudo chown apache:apache '.escapeshellcmd($tmp_maildirsize_path));
	shell_exec('sudo chmod 0777 '.escapeshellcmd($tmp_maildirsize_path));
	shell_exec('sudo echo -e "'.$file_content.'" > '.escapeshellcmd($tmp_maildirsize_path));
	shell_exec('sudo cp -rf \''.$tmp_maildirsize_path.'\' \''.$maildirsize_path.'\''); // copy the tmp file to user email account's maildirsize
	shell_exec('sudo rm -f \''.$tmp_maildirsize_path.'\'');

	//$path = LIBLINUX_PATH . "/recalculateQuota.pl";
	//$result = shell_exec("sudo perl $path $email");
	
	echo "1";
}else{
	echo "0";
}
?>