#!/usr/bin/perl
# vim: set ci et ts=4 sw=4:
# reportusage.pl: a script to report storage usage of a specific domain or
#                 all domain(s), can send user a email about it.
#
#      Author: He zhiqiang <hzqbbc@hzqbbc.com>
# Last Update: Tue Feb 06 2007 23:19:00
#     Version: 0.2

use vars qw($Ext::Config::PF $DIR $base);

BEGIN {
    my $path = $0;
    if ($path =~ s/tools\/addAlias\.pl$//) {
        if ($path !~ /^\//) {
            $DIR = "./$path";
        } else {
            $DIR = $path;
        }
    } else {
        $DIR = '../';
    }

    unshift @INC, $DIR .'libs';
    unshift @INC, $DIR .'../libs';
    $Ext::Config::PF= "$DIR/webman.cf";

    select((select(STDOUT), $| = 1)[0]);
};

#use strict;
use POSIX qw(strftime);
use Ext::Config;
use Ext::Mgr;
use Ext::Utils qw(human_size lock unlock haslock);
use File::Path;

die "Usage: $0 [Sender] [recipient]\n" unless $#ARGV == 1;

#die "Warning: you need to install extmail at the same top direcotry\n".
#    "         in order to call extmail modules.\n\n".
#    "Usage: $0 [domain|-all] mailbase [recipient]\n" unless $#ARGV == 2;

if (!$SYS_CFG) {
    Ext::Config::import;
}

# to check wheather another process is handling
# the same job?
open (my $fh, "< $0") or die "Error: $!\n";

if (haslock ($fh)) {
    warn "There is another process working, abort\n";
    exit 255;
} else {
    lock ($fh);
}

my $c = $SYS_CFG;
my $backend = $c->{SYS_BACKEND_TYPE};
my $mgr; # the backend object
my $SENDMAIL = '/usr/sbin/sendmail -t -oi';

    $mgr = Ext::Mgr->new(
        type => 'mysql',
        host => $c->{SYS_MYSQL_HOST},
        socket => $c->{SYS_MYSQL_SOCKET},
        dbname => $c->{SYS_MYSQL_DB},
        dbuser => $c->{SYS_MYSQL_USER},
        dbpw => $c->{SYS_MYSQL_PASS},
        table => $c->{SYS_MYSQL_TABLE},
        table_attr_username => $c->{SYS_MYSQL_ATTR_USERNAME},
        table_attr_passwd => $c->{SYS_MYSQL_ATTR_PASSWD},
        crypt_type => $c->{SYS_CRYPT_TYPE},
        psize => $c->{SYS_PSIZE} || 10,
    );

my $opt_domain =  $c->{SYS_EMAIL_DOMAIN};
my $opt_fromAlias = $ARGV[0];
my $opt_toAlias = $ARGV[1];
#my @data = split(/@/, $opt_username);
#my $opt_uid = @data[0];
#my $opt_clearpw = "";
#my $opt_name = "";
#my $opt_mailhost = "";
#my $opt_maildir = "$opt_domain/$opt_uid/Maildir/";
#my $opt_homedir = "$opt_domain/$opt_uid";
#my $opt_quota = $c->{SYS_USER_DEFAULT_QUOTA} * $c->{SYS_QUOTA_MULTIPLIER};
#my $opt_netdiskquota = "0";
#my $opt_uidnumber = $c->{SYS_DEFAULT_UID};
#my $opt_gidnumber = $c->{SYS_DEFAULT_GID};;
my $opt_create = "2100-11-08 15:10:04";
my $opt_expire = "2100-11-08 15:10:04";
my $opt_active = "1";

my $data = $mgr->add_alias(alias => $opt_fromAlias , 
			goto => $opt_toAlias, 
			domain => $opt_domain,
			create => $opt_create,
#			expire => $opt_expire,
			active => $opt_active,
	);

print $data;

1;


