#!/usr/bin/perl
# vim: set ci et ts=4 sw=4:
# reportusage.pl: a script to report storage usage of a specific domain or
#                 all domain(s), can send user a email about it.
#
#      Author: He zhiqiang <hzqbbc@hzqbbc.com>
# Last Update: Tue Feb 06 2007 23:19:00
#     Version: 0.2

use vars qw($Ext::Config::PF $DIR $base);

BEGIN {
    my $path = $0;
    if ($path =~ s/tools\/removeMailForward\.pl$//) {
        if ($path !~ /^\//) {
            $DIR = "./$path";
        } else {
            $DIR = $path;
        }
    } else {
        $DIR = '../';
    }

    unshift @INC, $DIR .'libs';
    unshift @INC, $DIR .'../extmail/libs';
    $Ext::Config::PF= "$DIR/webman.cf";

    select((select(STDOUT), $| = 1)[0]);
};

use strict;
use POSIX qw(strftime);
use Ext::Config;
use Ext::Mgr;
use Ext::Utils qw(human_size lock unlock haslock);
use Ext::Storage::Maildir;
use Ext::BL_App;

die "Usage: $0 [email_address] \n" unless $#ARGV == 0;

#die "Warning: you need to install extmail at the same top direcotry\n".
#    "         in order to call extmail modules.\n\n".
#    "Usage: $0 [domain|-all] mailbase [recipient]\n" unless $#ARGV == 2;

if (!$SYS_CFG) {
    Ext::Config::import;
}

# to check wheather another process is handling
# the same job?
open (my $fh, "< $0") or die "Error: $!\n";

if (haslock ($fh)) {
    warn "There is another process working, abort\n";
    exit 255;
} else {
    lock ($fh);
}

my $c = $SYS_CFG;
my $mgr; # the backend object

my $backend = $c->{SYS_BACKEND_TYPE};
my $userName = $ARGV[0];
#my $homePath = $ARGV[1];

#my $homePath = "/home/gary/spam_virus/tools";
   $mgr = Ext::Mgr->new(
        type => 'mysql',
        host => $c->{SYS_MYSQL_HOST},
        socket => $c->{SYS_MYSQL_SOCKET},
        dbname => $c->{SYS_MYSQL_DB},
        dbuser => $c->{SYS_MYSQL_USER},
        dbpw => $c->{SYS_MYSQL_PASS},
        table => $c->{SYS_MYSQL_TABLE},
        table_attr_username => $c->{SYS_MYSQL_ATTR_USERNAME},
        table_attr_passwd => $c->{SYS_MYSQL_ATTR_PASSWD},
        crypt_type => $c->{SYS_CRYPT_TYPE},
        psize => $c->{SYS_PSIZE} || 10,
    );


#my $opt_domain =  $c->{SYS_EMAIL_DOMAIN};
my @splitValues = split('@', $userName);
my $opt_domain = $splitValues[1];


my $userDetails = $mgr->get_user_info($userName);
my $result = $userDetails->{homedir};
my $userPath = "$c->{SYS_MAILDIR_BASE}/$result";

my $result = Ext::BL_App->remove_mail_forward($userName, $userPath);
`/bin/chmod -R 700 $userPath 2>/dev/null`;
`/bin/chown -R vuser.vgroup $userPath 2>/dev/null`;
print $result;


1;
