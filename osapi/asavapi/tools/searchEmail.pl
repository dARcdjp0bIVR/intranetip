#!/usr/bin/perl
use vars qw($Ext::Config::PF $DIR $base);
use MIME::Base64;
use Encode;
use MIME::Words qw(:all);


# BL - 20100614
BEGIN {
    my $path = $0;
    if ($path =~ s/tools\/searchEmail\.pl$//) {
        if ($path !~ /^\//) {
            $DIR = "./$path";
        } else {
            $DIR = $path;
        }
    } else {
        $DIR = '../';
    }

    unshift @INC, $DIR .'libs';
    unshift @INC, $DIR .'../extmail/libs';
    $Ext::Config::PF= "$DIR/webman.cf";

    select((select(STDOUT), $| = 1)[0]);
};

#use strict;
use warnings;
use Cwd;

use File::Find;
use File::Basename;
use Mail::Header;

# BL - 20100614
use Ext::Config;

if (!$SYS_CFG) {
    Ext::Config::import;
}

my $c = $SYS_CFG;
my $backend = $c->{SYS_BACKEND_TYPE};


my ($in_rgx,$in_files,$simple,$matches,$cwd);
my ($messagefile,$header,$subject);
#BL
my $backupLocation = "/tmp";
my ($sec,$min,$hour,$mday,$mon,$year)=(localtime)[0..5];
($sec,$min,$hour,$mday,$mon,$year)=($sec,$min,$hour,$mday,$mon+1,$year+1900);
$timestamp=$year.$mon.$mday.$hour.$min;
my $finalOutput = $backupLocation."/".$timestamp."/result.txt";



sub trim($) {
  my $string = shift;
  $string =~ s/[\r\n]+//g;
  $string =~ s/\s+$//;
  return $string;
}

                                      # 1: Get input arguments
if ($#ARGV == 0) {                    # *** ONE ARGUMENT *** (search pattern)
  ($in_rgx,$in_files,$simple) = ($ARGV[0],".",1);
}
elsif ($#ARGV == 1) {                 # *** TWO ARGUMENTS *** (search pattern + filename or flag)
  if (($ARGV[1] eq '-e') || ($ARGV[1] eq '-E')) { # extended
    ($in_rgx,$in_files,$simple) = ($ARGV[0],".",0);
  }
  else { # simple
    ($in_rgx,$in_files,$simple) = ($ARGV[0],$ARGV[1],1);
  }
}
elsif ($#ARGV == 2) {                 # *** THREE ARGUMENTS *** (search pattern + filename + flag)
  ($in_rgx,$in_files,$simple) = ($ARGV[0],$ARGV[1],0);
}
else {                                # *** HELP *** (either no arguments or more than three)
  print "Usage:  ".basename($0)." [search pattern]\n\n";
  exit;
}


#if ($in_files eq '.') {               # 2: Output search header
#  print basename($0).": Searching all files for \"${in_rgx}\"... (".(($simple) ? "simple" : "extended").")\n";
#}
#else {
#  print basename($0).": Searching files matching \"${in_files}\" for \"${in_rgx}\"... (".(($simple) ? "simple" : "extended").")\n";
#}


if ($simple) { print "\n"; }          # 3: Traverse directory tree using subroutine 'findfiles'

($matches,$cwd) = (0,cwd);
#$cwd  = "/home/domains/avasbl.bl.com";
# BL - 20100614
$cwd = $c->{EMAIL_DOMAIN_ROOT};
find(\&findfiles, $cwd);

sub findfiles {                       # 4: Used to iterate through each result
  my $file = $File::Find::name;       # complete path to the file

#  $file =~ s,/,\\,g;                  # substitute all / with \

  return unless -f $file;             # process files (-f), not directories
  return unless $_ =~ m/$in_files/io; # check if file matches input regex
                                      # /io = case-insensitive, compiled
                                      # $_ = just the file name, no path

                                      # 5: Open file and search for matching contents
  open F, $file or print "\n* Couldn't open ${file}\n\n" && return;

  # BL
  # Create Folder
  mkdir ("$backupLocation/$timestamp",0777);
  open(SR, ">>$finalOutput") || die ("Could not open file. <br> $!");

  if ($simple) {                      # *** SIMPLE OUTPUT ***
    while (<F>) {
      if (m/($in_rgx)/o) {            # /o = compile regex
                 # file matched!
          $matches++;

	# Extract email subject
	open(MESSAGE,"$file") or die "Unable to open $messagefile:$!\n";
	$header = new Mail::Header \*MESSAGE;

#	$header =~ decode_base64($header);
#	$header =~ decode('MIME-Header', $header);
#	$header =~ decode_mimewords($header);
#        $subject_temp = $header->get("Subject");
#	$subject = $header->get("Subject");
#	$subject =~ s/\?=\s\n/\?=\n/g;
#	$subject =~ s/\n[ |\t]//g;
#	$header =~ Encode::decode('MIME-Header', $header);
#       $subject_temp =~ s/\?=\s\n/\?=\n/g;
#       $subject_temp =~ s/\n[ |\t]//g;
#kkk
#	$subject = decode('iso-8859-1', $subject_temp);
#	$subject = decode('utf8', $subject_temp);
#	$subject = decode_base64($subject_temp);

	$subject = $header->get("Subject");
	$subject =~ s/\?=\s\n/\?=\n/g;
	$subject =~ s/\n[ |\t]//g;

	close(MESSAGE);

	# BL
	#print "$matches###$file###$subject";
	#print "$matches###$subject<BR>";
	print SR "$matches###$file###$subject";

        last;                         # go on to the next file
      }
    }
  }                                   # *** END OF SIMPLE OUTPUT ***
  else {                              # *** EXTENDED OUTPUT ***
    my $found = 0;                    # used to keep track of first match
    my $binary = (-B $file) ? 1 : 0;  # don't show contents if file is bin
    $file =~ s/^\Q$cwd//g;            # remove current working directory
                                      # \Q = quotemeta, escapes string

    while (<F>) {
      if (m/($in_rgx)/o) {            # /o = compile regex
                                      # file matched!
        if (!$found) {                # first matching line for the file
          $found = 1;
          $matches++;
          print "\n---" .             # begin printing file header
          sprintf("%04d", $matches) . # file number, padded with 4 zeros
          "--- ".uc($file)."\n";      # file name, converted to uppercase
                                      # end of file header
          if ($binary) {              # file is binary, do not show content
            print "Binary file.\n";
            last;
          }
        }
        print "[$.]".trim($_)."\n";   # print line number and contents
        #last;                        # uncomment to only show first line
      }
    }
  }                                   # *** END OF EXTENDED OUTPUT ***

  # 6: Close the file and move on to the next result
  close SR;
  close F;
}

if (  ${matches} > 0 ) {
	print "1,$finalOutput";
} else {
	print "0,No Match";
}

#7: Show search statistics
#print "\nMatches: ${matches}\n";

# Search Engine Source: http://www.adp-gmbh.ch/perl/find.html
# Rewritten by Christopher Hilding, Dec 02 2006
# Formatting adjusted to my liking by Rene Nyffenegger, Dec 22 2006

