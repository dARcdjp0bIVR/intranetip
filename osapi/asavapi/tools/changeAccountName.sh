#!/bin/sh
# Change email address and add alias for old email address.
# @param string valid old email address in lower case
# @param string valid new email address in lower case
# @return string return "1" if change email address succesffully! Otherwise, it shows fail message!
# @author Bob
# @date 20130411
#
# USAGE : ./changeAccountName.sh old@bl.com new@bl.com
# Log   : /usr/local/sbin/changeAccountName_log.txt

CURRENT_PATH=`pwd`
#OSAPI_PATH=`echo $CURRENT_PATH|sed "s/asavapi\/tools//"`
TIMESTAMP=`date +%Y%m%d_%H%S`

OLD_EMAIL=$1
NEW_EMAIL=$2

DOMAIN=`echo $OLD_EMAIL|cut -d@ -f2`
DOMAIN_PATH="/home/domains/$DOMAIN"

OLD_USER=`echo $OLD_EMAIL|cut -d@ -f1`
NEW_USER=`echo $NEW_EMAIL|cut -d@ -f1`

# check parameter number
if [ $# -ne 2 ];then
    echo "USAGE : ./changeAccountName.sh old@bl.com new@bl.com"
    exit 0
fi

# check domain
if [ ! -d "$DOMAIN_PATH" ];then
    echo "Fail,Domain not found!!"
    exit 0
fi

# check email address end
if [[ $OLD_EMAIL =~ .*@.* ]] && [[ $NEW_EMAIL =~ .*@.* ]];then

    # check old user mail folder 
    if [ ! -d "$DOMAIN_PATH/$OLD_USER" ];then
        echo "Fail,Old user : ${OLD_USER}@${DOMAIN} :  email folder not found!!"
        echo "Fail,Old user : ${OLD_USER}@${DOMAIN} :  email folder not found!!" >> /usr/local/sbin/changeAccountName_log_not_found.txt
        exit 0
    fi

    # rename user folder and update .mailfilter
    /bin/mv $DOMAIN_PATH/$OLD_USER $DOMAIN_PATH/$NEW_USER
    /bin/cp -rpf $DOMAIN_PATH/$NEW_USER/.mailfilter $DOMAIN_PATH/$NEW_USER/.mailfilter_$TIMESTAMP
    /bin/sed -i -e "s/${OLD_USER}@${DOMAIN}/${NEW_USER}@${DOMAIN}/g" $DOMAIN_PATH/$NEW_USER/.mailfilter
    echo "$TIMESTAMP : Renamed ${OLD_USER}@${DOMAIN} to ${NEW_USER}@${DOMAIN} done!" >> /usr/local/sbin/changeAccountName_log.txt

    # remove and add alias for old email address
    $CURRENT_PATH/asavapi/tools/removeAlias.pl $OLD_EMAIL >/dev/null 2>&1
    $CURRENT_PATH/asavapi/tools/removeAlias.pl $NEW_EMAIL >/dev/null 2>&1
    $CURRENT_PATH/asavapi/tools/addAlias.pl $OLD_EMAIL $NEW_EMAIL >/dev/null 2>&1

#Update Mysql paths
mysql -uextmail -pext2009mailBL extmail<<EOFMYSQL

update mailbox set uid ="$NEW_USER" where username="$OLD_USER@$DOMAIN";
update mailbox set maildir = replace(maildir,'$OLD_USER','$NEW_USER') where username="$OLD_USER@$DOMAIN";
update mailbox set homedir = replace(homedir,'$OLD_USER','$NEW_USER') where username="$OLD_USER@$DOMAIN";
update mailbox set username = replace(username,'$OLD_USER','$NEW_USER') where username="$OLD_USER@$DOMAIN";

update alias set goto="$NEW_USER@$DOMAIN" where goto="$OLD_USER@$DOMAIN";

EOFMYSQL

echo "1"

else
    echo "Fail,Invalid email address input!!"
    exit 0
fi # check email address end 
