#!/bin/bash

folderType=$1
keep_day=$2

# go to intranetdata folder
cd ../file/
intranet_file=`pwd`
mkdir $intranet_file/gamma_mail
chown apache:apache $intranet_file/gamma_mail
txt_folder="$intranet_file/gamma_mail"

###
# 1 -> Junk folder
# 2 -> Trash folder
###

if [ $folderType -eq 1 ]; then

	echo "$folderType,$keep_day" > $txt_folder/changeJunkDay.txt

fi

if [ $folderType -eq 2 ]; then

	echo "$folderType,$keep_day" > $txt_folder/changeTrashDay.txt

fi

chown apache:apache -R $txt_folder
