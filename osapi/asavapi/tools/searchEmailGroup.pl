#!/usr/bin/perl
use vars qw($Ext::Config::PF $DIR $base);
use MIME::Base64;
use Encode;
use MIME::Words qw(:all);


# BL - 20100614
BEGIN {
    my $path = $0;
    if ($path =~ s/tools\/searchEmailGroup\.pl$//) {
        if ($path !~ /^\//) {
            $DIR = "./$path";
        } else {
            $DIR = $path;
        }
    } else {
        $DIR = '../';
    }

    unshift @INC, $DIR .'libs';
    unshift @INC, $DIR .'../extmail/libs';
    $Ext::Config::PF= "$DIR/webman.cf";

    select((select(STDOUT), $| = 1)[0]);
};

#use strict;
use warnings;
use Cwd;

use File::Find;
use File::Basename;
use Mail::Header;

# BL - 20100614
use Ext::Config;

if (!$SYS_CFG) {
    Ext::Config::import;
}

my $c = $SYS_CFG;
my $backend = $c->{SYS_BACKEND_TYPE};


my ($in_rgx,$in_files,$simple,$matches,$cwd);
my ($messagefile,$header,$subject);
#BL
my $backupLocation = "/tmp";
my ($sec,$min,$hour,$mday,$mon,$year)=(localtime)[0..5];
($sec,$min,$hour,$mday,$mon,$year)=($sec,$min,$hour,$mday,$mon+1,$year+1900);
$timestamp=$year.$mon.$mday.$hour.$min;
my $finalOutput = $backupLocation."/".$timestamp."/result.txt";



sub trim($) {
  my $string = shift;
  $string =~ s/[\r\n]+//g;
  $string =~ s/\s+$//;
  return $string;
}

                                      # 1: Get input arguments
if ($#ARGV == 0) {                    # *** ONE ARGUMENT *** (search pattern)
#  ($in_rgx,$in_files,$simple) = ($ARGV[0],".",1);
  print "Usage:  ".basename($0)." [search pattern] [Users] \n\n";
  exit;

}
elsif ($#ARGV == 1) {                 # *** TWO ARGUMENTS *** (search pattern + filename or flag)
  if (($ARGV[1] eq '-e') || ($ARGV[1] eq '-E')) { # extended
    ($in_rgx,$in_files,$simple) = ($ARGV[0],$ARGV[1],1);
  }
  else { # simple
    ($in_rgx,$in_files,$simple) = ($ARGV[0],$ARGV[1],1);
  }
}
else {                                # *** HELP *** (either no arguments or more than three)
  print "Usage:  ".basename($0)." [search pattern] [Users] \n\n";
  exit;
}


if ($simple) { print "\n"; }          # 3: Traverse directory tree using subroutine 'findfiles'

($matches,$cwd) = (0,cwd);
@pairs = split(/,/, $in_files); 

foreach $pair (@pairs) 
{ 
	$pair = $c->{EMAIL_DOMAIN_ROOT} . "/" . $pair;
} 
 
find(\&findfiles, @pairs);

sub findfiles {                       # 4: Used to iterate through each result
  my $file = $File::Find::name;       # complete path to the file

  return unless -f $file;             # process files (-f), not directories

  open F, $file or print "\n* Couldn't open ${file}\n\n" && return;

  # BL
  # Create Folder
  mkdir ("$backupLocation/$timestamp",0777);
  open(SR, ">>$finalOutput") || die ("Could not open file. <br> $!");

  if ($simple) {                      # *** SIMPLE OUTPUT ***
    while (<F>) {
      if (m/($in_rgx)/o) {            # /o = compile regex
                 # file matched!
          $matches++;

	# Extract email subject
	open(MESSAGE,"$file") or die "Unable to open $messagefile:$!\n";
	$header = new Mail::Header \*MESSAGE;

        $subject = $header->get("Subject");
	close(MESSAGE);

	# BL
	#print "$matches###$file###$subject";
	print SR "$matches###$file###$subject";

        last;                         # go on to the next file
      }
    }
  }                                   # *** END OF SIMPLE OUTPUT ***

  # 6: Close the file and move on to the next result
  close SR;
  close F;
}

if (  ${matches} > 0 ) {
	print "1,$finalOutput";
} else {
	print "0,No Match";
}

#7: Show search statistics
#print "\nMatches: ${matches}\n";

# Search Engine Source: http://www.adp-gmbh.ch/perl/find.html
# Rewritten by Christopher Hilding, Dec 02 2006
# Formatting adjusted to my liking by Rene Nyffenegger, Dec 22 2006

