# vim: set cindent expandtab ts=4 sw=4:
#
# Copyright (c) 1998-2005 Chi-Keung Ho. All rights reserved.
#
# This programe is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# Extmail - a high-performance webmail to maildir
# $Id$
package Ext::BL_App;

use strict;
use Exporter;
use Fcntl qw(:flock);
use Ext::Storage::Maildir;
use Ext::MIME; # _set_msgs_cache use it
use Ext::Utils qw(untaint _index _substr _length human_size);
use Ext::DateTime qw(datefield2dateserial);


use vars qw(@ISA $VERSION);
@ISA = qw(Exporter);
$VERSION = '1.1';

sub new {
    my $class = shift;
    my %opt = @_;

    my $self = {
        file => $opt{file} ? $opt{file} : $ENV{HOME}.'/.mailfilter',
        lock => $opt{lock} ? 1:0
    };

    bless $self, $class;

    $self->parse; # XXX auto
    $self;
}

# XXX new design:
#
# will rename $self->{filter} to $self->{rules}
#
# $rules ----> HASH ref element
#              |---> header,value,folder,from,name,options
#
# $whitelist ---> { on => yes/not, path => $path }
# $blacklist ---> { on => yes/not, path => $path }
# $spam2junk ---> { on => yes/not }
# $autoreply ---> { on => yes/not }
# $forward   ---> { on => yes/not, addr => $addr }
#
# Priority: whitelist > blacklist > forward > rules > spam2junk
sub parse {
    my $self = shift;
    my $file = $self->{file};
    my $new = 1;
    my $ref = ();
    my @rules;

    # extension
    my $whitelist = {};
    my $blacklist = {};
    my $spam2junk = 0;
    my $autoreply = 0;
    my $forward = 0;

    # ignore opening file error, set the $self->{rules} to empty ARRAY
    # ref, or perl will complain, :-)
    open(FD, "< $file") or $self->{rules} = [] and return;
    while(<FD>) {
        chomp;
        my $line = $_;

        # extension parsing
        if (substr($line, 0, 2) eq '#*') {
            my $res = substr($line, 2);
            if ($res eq 'whitelist') {
                $whitelist = { on => 1, path => 'whitelist.cf'};
                $self->{whitelist} = $whitelist;
            } elsif ($res eq 'blacklist') {
                $blacklist = { on => 1, path => 'blacklist.cf'};
                $self->{blacklist} = $blacklist;
            } elsif ($res eq 'spam2junk') {
                $spam2junk = 1;
                $self->{spam2junk} = $spam2junk;
            } elsif ($res eq 'autoreply') {
                $autoreply = 1;
                $self->{autoreply} = $autoreply;
            } elsif ($res =~ /^forward: (.*)/) {
                # forward: user@domain.tld
                $forward = 1;
                $self->{forward} = $1; # the forward addr
            } elsif ($res eq 'forwardcc') {
                $self->{forwardcc} = 1;
            }
        }

        # format to design
        # example:
        #
        # ##Name:test rule
        # ##From:hzqbbc@hzqbbc.com
        # ##Folder:!foo@bar.com
        # ##Folder:.Junk
        # ##Delete
        # ##Continue
        # ##Recipient:bbc@aaa.com
        # ##Notcontains:haha
        # ##Contains:xixi
        #
        # if ((/^From: .*hzqbbc\@hzqbbc\.com.*/) AND
        #     (/^To: .*bbc\@aaa\.com.*/) AND
        #     (/xixi/:b) AND
        #     (!/haha/:b))
        # {
        #     cc "foo@bar.com"
        #     EXITCODE=0
        #     exit
        #     to "$HOME/.Junk/."
        # }
        if (!$new) {
            if (substr($line, 0, 2) eq '##') {
                my $res = substr($line, 2, 5);
                if ($res eq 'Recip') {
                    $ref->{recipient} = substr($line, 12);
                } elsif ($res eq 'Folde') {
                    my $val = substr($line, 9);
                    next unless $val;
                    if ($ref->{folder}) {
                        push @{$ref->{folder}}, $val;
                    } else {
                        $ref->{folder} = [$val];
                    }
                } elsif ($res eq 'From:') {
                    $ref->{from} = substr($line, 7);
                } elsif ($res eq 'Subje') {
                    $ref->{subject} = substr($line, 10);
                } elsif ($res eq 'Notco') {
                    $ref->{notcontains} = substr($line, 14);
                } elsif ($res eq 'Conta') {
                    $ref->{contains} = substr($line, 11);
                } else {
                    next if ($line =~ /:/);
                    $ref->{options} .= substr($line, 2).' ';
                }
            } else {
                if($ref->{options}) {
                    $ref->{options} =~ s/\s+$//;
                }
                push @rules, $ref;
                $new = 1;
                $ref = ();
            }
        }

        if ($new && substr($line, 0, 6) eq '##Name') {
            $new = 0;
            $ref->{name} = substr($line, 7);
        }
    }
    close FD;
    $self->{rules} = \@rules; # save the ref
}

sub save {
    my $self = shift;
    my $file = $self->{file};
    my $rr = $self->{rules};
    my @rules = @$rr;
    my $buf = '';
    my $username = $ENV{USERNAME};
    my $maildir = $ENV{HOME};
    my $autoreply = "cc \"| mailbot -A 'X-Sender: \$FROM' -A 'From: \$FROM' ";

    # advoid coding too long
    $autoreply .= "-m '\$HOME/Maildir/autoreply.cf' \$SENDMAIL -t -f ''\"";

    $buf .= "#MFMAILDROP=2\n";
    $buf .= "#\n";
    $buf .= "# DO NOT EDIT THIS FILE.  This is an automatically generated filter.\n";
    $buf .= "# Generated by ExtMail $VERSION\n\n";

    $buf .= "FROM='$username'\n";
    $buf .= "import SENDER\n";
    $buf .= "if (\$SENDER eq \"\")\n";
    $buf .= "{\n";
    $buf .= " SENDER=\$FROM\n";
    $buf .= "}\n\n";

    if ($self->{whitelist}) {
        $buf .= "#*whitelist\n";
        $buf .= "foreach /^(Return-path|From): .*/\n";
        $buf .= "{\n";
        $buf .= "  if (lookup( getaddr(\$MATCH), \"\$HOME/Maildir/whitelist.cf\" ))\n";
        $buf .= "  {\n";
        if ($self->{autoreply}) {
            # must append the autoreply code, or the 'to' operator will
            # terminate the deliver process and ignore autoreply code
            # following the whitelist!
            $buf .= "    $autoreply\n";
        }
        if ($self->{forward}) {
            # must append the forwarding code, or the 'to' operator will
            # terminate the deliver process and ignore forwarding code
            # behide the whitelist:)
            $buf .= "    ".($self->{forwardcc}?'cc':'to').
                    " \"| \$SENDMAIL -f \" '\"\$SENDER\"' \" $self->{forward}\"\n";
        }
        $buf .= "    to \"\$HOME/Maildir/.\"\n";
        $buf .= "  }\n";
        $buf .= "}\n\n";
    }

    if ($self->{blacklist}) {
        $buf .= "#*blacklist\n";
        $buf .= "foreach /^(Return-path|From): .*/\n";
        $buf .= "{\n";
        $buf .= "  if (lookup( getaddr(\$MATCH), \"\$HOME/Maildir/blacklist.cf\" ))\n";
        $buf .= "  {\n";
        $buf .= "    EXITCODE=0\n";
        $buf .= "    exit\n"; # XXX discard
        $buf .= "  }\n";
        $buf .= "}\n\n";
    }

    if ($self->{autoreply}) {
        $buf .= "#*autoreply\n";
        $buf .= "$autoreply\n\n";
    }

    if ($self->{forward}) {
        my $dist = 'to';
        $buf .= "#*forward: $self->{forward}\n";
        if ($self->{forwardcc}) {
            $buf .= "#*forwardcc\n";
            $dist = 'cc';
        }
        $buf .= "$dist \"| \$SENDMAIL -f \" '\"\$SENDER\"' \" $self->{forward}\"\n\n";
    }

    for (my $i=0; $i <scalar @rules; $i++) {
        my $rule = $rules[$i];
        my $dist = 'to';
        my $delete = 0;
        my $hasattach = 0;
        my $folder = $rule->{folder};
        my @statements;

        $buf .= "##Name:$rule->{name}\n";
        $buf .= "##From:$rule->{from}\n";
        $buf .= "##Recipient:$rule->{recipient}\n";
        $buf .= "##Subject:$rule->{subject}\n";

        if ($folder) {
            $buf .= "##Folder:$_\n" for (@$folder);
        } else {
            $buf .= "##Folder:\n";
        }

        $buf .= "##Notcontains:$rule->{notcontains}\n"
            if ($rule->{notcontains});
        $buf .= "##Contains:$rule->{contains}\n"
            if ($rule->{contains});

        if ($rule->{options}) {
            for my $o (split(/ /, $rule->{options})) {
                $buf .= "##$o\n";
                if ($o eq 'Continue') {
                    $dist = 'cc';
                } elsif ($o eq 'Delete') {
                    $delete = 1;
                } elsif ($o eq 'Hasattach') {
                    $hasattach = 1;
                }
            }
        }

        $buf .= "\n";
        $buf .= "if (";

        if ($rule->{contains}) {
            push @statements, "(/".slashes($rule->{contains})."/:b)";
        }
        if ($rule->{notcontains}) {
            push @statements, "(!/".slashes($rule->{notcontains})."/:b)";
        }
        if ($rule->{from}) {
            push @statements, "(/^(From|Sender|Return-Path):.*".slashes($rule->{from})."/)";
        }
        if ($rule->{recipient}) {
            push @statements, "(/^To:.*".slashes($rule->{recipient})."/)";
        }
        if ($rule->{subject}) {
            push @statements, "(/^Subject:.*".slashes($rule->{subject})."/)";
        }
        if ($hasattach) {
            push @statements, "(/^Content-Type: *multipart\\/mixed/)";
        }

        $buf .= join(" || \\\n", @statements);
        $buf .= ")\n";
        $buf .= "{\n";

        if ($folder) {
            my $hasfolder = '';
            my $hasforward = '';
            my $hasbounce = '';
            my $hasdelete = '';
            my $hasautoreply = '';

            for my $dir (@$folder) {
                # * reject with message
                # ! forward
                # + autoreply
                my $flag = substr($dir,0,1); # generate the flag
                if ($flag eq '!') {
                    $hasforward = "\"| \$SENDMAIL -f \" '\"\$SENDER\"' \" ".substr($dir,1)."\"\n";
                    # $buf .= "  ".$dist." \"| \$SENDMAIL -f \" '\"\$SENDER\"' \" ".substr($dir,1)."\"\n";
                } elsif ($flag eq '+') {
                    # disable autoreply in rules since extmail 1.0beta3
                    $hasautoreply .= "  AUTOREPLYFROM=\$SENDER\n";
                    $hasautoreply .= "  `/usr/bin/mailbot -A \"X-Sender: \$SENDER\" -A \"From: \$AUTOREPLYFROM\" ";
                    $hasautoreply .= "  -M \"\$SENDER\" -m \"\$HOME/Maildir/autoresponses/".substr($dir,1)."\" \$SENDMAIL -t -f \"\"`\n";
                } elsif ($flag eq "*") {
                    # reject code
                    $hasbounce .= "  echo \"".substr($dir, 1)."\"\n";
                    $hasbounce .= "  EXITCODE=77\n";
                    $hasbounce .= "  exit\n";
                } elsif ($flag eq '.') {
                    # $buf .= "  ".$dist." \"\$HOME/Maildir/";
                    $hasfolder .= "\"\$HOME/Maildir/";
                    if ($dir eq '.') {
                        # The Inbox (.) so only prepend the dot(.)
                        $hasfolder .= ".\"\n";
                    } else {
                        $hasfolder .= "$dir/.\"\n";
                    }
                } elsif ($dir eq 'exit' || $delete) {
                    $hasdelete .= "  EXITCODE=0\n";
                    $hasdelete .= "  exit\n";
                }
            }

            # assemble main excution rules in order
            if ($hasfolder) {
                $buf .= "  ".($hasforward || $hasbounce ? 'cc':'to');
                $buf .= " $hasfolder";
            }
            if ($hasforward) {
                $buf .= "  ".($hasbounce ? 'cc':'to');
                $buf .= " $hasforward";
            }
            if ($hasbounce) {
                $buf .= $hasbounce;
            }
            if ($hasdelete) {
                $buf .= ($hasbounce?'': $hasdelete);
            }
        }

        $buf .= "}\n\n";
    }
    # XXX the end of loop

    if ($self->{spam2junk}) {
        $buf .= "#*spam2junk\n";
        $buf .= "if (/^X-Spam-Flag:.*YES/)\n";
        $buf .= "{\n";
        $buf .= "  to \"\$HOME/Maildir/.Junk/.\"\n";
        $buf .= "}\n\n";
    }

    $buf .= "to \"\$HOME/Maildir/.\"\n";

    eval {
        open(FD, "> $file.tmp") or die "Can't write to $file.tmp, $!\n";
        flock(FD, LOCK_EX);
        print FD $buf;
        flock(FD, LOCK_UN);
        close FD;
        rename("$file.tmp", $file) or die "Rename err, $!\n";
    };

    if ($@) {
        return $@;
    } else {
        return 0;
    }
}

sub slashes {
    $_ = shift;
    s/ /\\ /g;
    s/-/\\-/g;
    s/_/\\_/g;
    s/\+/\\+/g;
    s/:/\\:/g;
    s/'/\\'/g;
    s/>/\\>/g;
    s/\//\\\//g;
    s/\./\\./g;
    s/@/\\@/g;
    s/\[/\\[/g;
    s/]/\\]/g;
    s/</\\</g;
    return $_;
}

sub rules_up {
    my $self = shift;
    my $rules = $self->{rules};
    my $id = $_[0];

    return 1 if ($id <= 0 || $id >scalar @$rules -1);

    my $tmp_ref = ();
    $tmp_ref = $rules->[$id-1];
    $rules->[$id-1] = $rules->[$id];
    $rules->[$id] = $tmp_ref;
    $self->{rules} = $rules;
    0; # success
}

sub rules_down {
    my $self = shift;
    my $rules = $self->{rules};
    my $id = $_[0];

    return 1 if ($id<0 || $id>=scalar @$rules -1);

    my $tmp_ref = ();
    $tmp_ref = $rules->[$id+1];
    $rules->[$id+1] = $rules->[$id];
    $rules->[$id] = $tmp_ref;
    $self->{rules} = $rules;
    0;
}

sub rules_remove {
    my $self = shift;
    my $rules = $self->{rules};
    my $id = $_[0];

    return 1 if($id<0 || $id>scalar @$rules -1);

    my $new_rules = []; # ARRAY ref
    for (my $i=$id; $i< scalar @$rules-1;$i++) {
        $rules->[$i]=$rules->[$i+1];
    }
    pop @$rules;
    $self->{rules} = $rules;
}

sub rules_append {
    my $self = shift;
    my $rules = $self->{rules};
    my $ref = $_[0];

    push @$rules, $ref;
}

sub save_list {
    my $self = shift;
    my $type = $_[0];
    my $list = $_[1]; # must ARRAY ref

    die "Malformed input data!\n" unless (ref $list eq 'ARRAY');

    if ($type eq 'blacklist') {
        open (FD, "> blacklist.cf.tmp") or die "Can't write to $type.cf.tmp\n";
        flock (FD, LOCK_EX);
        for (@$list) {
            print FD $_, "\n";
        }
        flock (FD, LOCK_UN);
        close FD;
        rename ('blacklist.cf.tmp', 'blacklist.cf') or return $!;
    } elsif ($type eq 'whitelist') {
        open (FD, "> whitelist.cf.tmp") or die "Can't write to $type.cf.tmp\n";
        flock (FD, LOCK_EX);
        for (@$list) {
            print FD $_, "\n";
        }
        flock (FD, LOCK_UN);
        close FD;
        rename ('whitelist.cf.tmp', 'whitelist.cf') or return $!;
    } else {
        return "$type not support yet!\n";
    }
    return 0;
}

sub read_list {
    my $self = shift;
    my $type = $_[0];

    unless ($type =~ /^(black|white)list$/) {
        die "$type not support yet!\n";
    }

    open (FD, "< $type.cf") or return []; # ignore error
    my @arr;
    while(<FD>) {
        chomp;
        s/^\s*//g;
        s/\s*$//g;
        push @arr, $_;
    }
    close FD;
    return \@arr;
}

#sub read_autoreply {
#    my $self = shift;
#    my $buf = '';
#    my $crlf = $/;

#    open (FD, "< autoreply.cf") or return $buf; # ignore error
#    local $/ = "\n\n";
#    <FD>; # strip the header
#    local $/ = undef;
#    $buf = <FD>;
#    close FD;

#    return $buf;
#}

#sub save_autoreply {
#    my $self = shift;
#    my $buf = $_[0];

#    open (FD, "> autoreply.cf") or return "Error: $!\n";
#    flock (FD, LOCK_EX);
#    print FD $buf;
#    flock (FD, LOCK_UN);
#    close FD;
#    return 0;
#}

# XXX this func is useful
sub dir_inrule {
    my $self = shift;
    my $dir = $_[0];
    my $rules = $self->{rules};

    return 0 unless(valid_dirname($dir));
    $dir = _name2mdir($dir);

    foreach my $ref (@$rules) {
        if ($ref->{folder}) {
            for (@{$ref->{folder}}) {
                return $ref->{name} || '1'
                    if ($_ eq $dir);
            }
        }
    }
    0;
}

sub modify_global_score {
    my $self = shift; 1;
    my %opt = @_;
    my $db = $self->{dbh};
    my $active = $opt{active} ? 1 : 0;
    if ($opt{passwd}) {
        my $ctype = $self->{crypt_type};
        my $passwd = $self->encrypt($ctype, $opt{passwd});
        my $SQL = "UPDATE mailbox set password='$passwd'";
        if ($self->{opt}->{'table_attr_clearpw'}) {
            $SQL .= ",clearpwd='$opt{passwd}'";
        }
        $SQL .=" WHERE username='$opt{user}'";
        $db->do($SQL);
        return $db->errstr if ($db->err);
    }
    $db->do("UPDATE mailbox set
            name='$opt{cn}',
            quota='$opt{quota}',
            netdiskquota='$opt{netdiskquota}',
            uidnumber='$opt{uidnumber}',
            gidnumber='$opt{gidnumber}',
            expiredate='$opt{expire}',
            active='$active',
            disablepwdchange='$opt{disablepwdchange}',
            disablesmtpd='$opt{disablesmtpd}',
            disablesmtp='$opt{disablesmtp}',
            disablewebmail='$opt{disablewebmail}',
            disablenetdisk='$opt{disablenetdisk}',
            disableimap='$opt{disableimap}',
            disablepop3='$opt{disablepop3}' WHERE username='$opt{user}'"
    );
    if ($db->err) {
        return $db->errstr;
    } else {
        return 0;
    }
}

#

sub set_mail_forward {
    my $self = shift; 1;
    my ($fromEmail, $q_targetEmail, $q_keepCopy, $path) = @_;
    my $counter = 0;
    my $tempFlag = 0;
    my $lines = 0;
	
    if ( ! -e "$path/.mailfilter")
    {

	#    open(FD, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
	    open(FD, "> $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
	    my $s = <FD>;# omit the first line
	    seek(FD,0,0) unless($s=~/S|C/); # unget if no quota limit
	    local $/= undef;
	    $s = <FD>;
	    close FD;
	    open(FD, "> $path/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";
	    print FD "#MFMAILDROP=2\n";
	    print FD "#\n";
	    print FD "# DO NOT EDIT THIS FILE.  This is an automatically generated filter.\n";
	    print FD "# Generated by ExtMail 1.1\n";
	    print FD "\n";
	    print FD "FROM='$fromEmail'\n";
	    print FD "import SENDER\n";
	    print FD "if (\$SENDER eq \"\")\n";
	    print FD "{\n";
	    print FD " SENDER=\$FROM\n";
	    print FD "}\n";
	    print FD "\n";
	    print FD "#*forward: $q_targetEmail\n";
	
	    if($q_keepCopy) {
			print FD "#*forwardcc\n";
			print FD "cc \"| \$SENDMAIL -f \" '\"\$SENDER\"' \" $q_targetEmail\"\n";
	    } else {
			print FD "to \"| \$SENDMAIL -f \" '\"\$SENDER\"' \" $q_targetEmail\"\n";
	    }
	    print FD "\n"; # newline
	    print FD "to \"\$HOME/Maildir/.\"\n"; # newline
	
	    close FD;
	    unlink untaint("$path/.mailfilter") || die "unlink fail: $!\n";
	    rename untaint($path."/.mailfilter.tmp"), untaint($path."/.mailfilter") or die "Can't rename:$!\n";
    }
    else # Append
    {
	    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
	    while (<$infile>) {
	        if ( ($_ =~ /to \"\$HOME\/Maildir\/.\"/) ) {
	            last;
	        }else{
	        	$lines++;
	        }
	    }
	    close $infile;
	
	    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
	    open(my $outfile, "> $path/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";
	
	    while (<$infile>)
	    {
	        $counter++;
	
	        print $outfile $_;
	
	        #if ($_ =~ /#*forward/) {
	        #        $tempFlag  = 1;
	        #}
			if ($_ =~ m/\s$q_targetEmail/) {
                $tempFlag  = 1;
        	}
			
	        if (($tempFlag == 0) && ($counter == $lines - 1)) {
	            print $outfile "\n"; # newline
		    	print $outfile "#*forward: $q_targetEmail\n";
				
			    if($q_keepCopy) {
		            print $outfile "#*forwardcc\n";
		        	print $outfile "cc \"| \$SENDMAIL -f \" '\"\$SENDER\"' \" $q_targetEmail\"\n";
			    } else {
			        print $outfile "to \"| \$SENDMAIL -f \" '\"\$SENDER\"' \" $q_targetEmail\"\n";
			    }
			    print $outfile "\n"; # newline
	        }
	    }
	
	    close $infile;
	    close $outfile;
	    unlink untaint("$path/.mailfilter") || die "unlink fail: $!\n";
	    rename untaint($path."/.mailfilter.tmp"), untaint($path."/.mailfilter") or die "Can't rename:$!\n";

    }

    0;
}


sub remove_mail_forward {
    my $self = shift; 1;
    my ($userName, $homePath) = @_;

    open(my $infile, "< $homePath/.mailfilter") or die "Can't open .mailfilter, $!\n";
    open(my $outfile, "> $homePath/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

    while (<$infile>)
    {
	if ($_ !~/(cc \"\| \$SENDMAIL)|(to \"\| \$SENDMAIL)|#*forward|#*forwardcc/) 
    	{
       		print $outfile $_;
    	}
    }
    close $infile;
    close $outfile;
    unlink untaint("$homePath/.mailfilter") || die "unlink fail: $!\n";
    rename untaint($homePath."/.mailfilter.tmp"), untaint($homePath."/.mailfilter") or die "Can't rename:$!\n";


    0;
}

sub get_mail_forward {
    my $self = shift; 1;
    my ($userName, $homePath) = @_;
    my @data = ();
    my $outData = "";

    open(my $infile, "< $homePath/.mailfilter") or die "Can't open .mailfilter, $!\n";

    while (<$infile>)
    {
		if ($_ =~ /(cc \"\| \$SENDMAIL -f )/ || $_ =~ /(to \"\| \$SENDMAIL -f )/) {
			@data = split(/ /, $_);
			chomp @data[7];
			chop @data[7];
			if($outData eq ""){
				$outData = $data[7];
			}else{
				$outData = $outData . "," . @data[7];
			}
		}
    }
    close $infile;

	return $outData;
#    my $inData = @data[7];
#    $inData =~ s/"//g;

#    return $inData;
	
    0;
}


sub add_white_list {
    my $self = shift;
    my $list = $_[0]; # must ARRAY ref
    my $listpath = $_[1]; 
    my $recordExist = 0;

    die "Malformed input data!\n" unless (ref $list eq 'ARRAY');

#    open (FD, ">> $listpath/white.lst") or die "Can't write to $listpath/white.lst\n";
#    open(my $infile, "< $listpath/white.lst") or die "Can't open $listpath/white.lst\n";
    open (FD, ">> $listpath/sender_whitelist") or die "Can't write to $listpath/sender_whitelist\n";
    open(my $infile, "< $listpath/sender_whitelist") or die "Can't open $listpath/sender_whitelist\n";
    flock (FD, LOCK_EX);

    # Add record    
    for (@$list) {
    my $checkRecord = $_;    

    while (<$infile>)
    {
	if($_ =~ m/$checkRecord/) { 
		$recordExist = 1;
	}
    }

    # Check record exist

    if ($recordExist == 1){
	#print "Record Exist!!\n";
	return 1;
    }
    else{
	print FD $checkRecord, "\n";
	#print FD $checkRecord;
	#print "Record Not Exist!!\n";
	return 0;
    }

    }
    flock (FD, LOCK_UN);
    close FD;
    close $infile;

#    return 0;
}


sub read_white_list {
    my $self = shift;
    my $listpath = $_[0]; 

#    open (FD, "< $listpath/white.lst") or return []; # ignore error
    open (FD, "< $listpath/sender_whitelist") or return []; # ignore error
    my @arr;
    while(<FD>) {
        chomp;
        s/^\s*//g;
        s/\s*$//g;
        push @arr, $_;
    }
    close FD;
    return \@arr;

}

sub remove_whitelist_record {
    my $self = shift; 1;
    my ($removeRecord, $listpath) = @_;

#    open(my $infile, "< $listpath/white.lst") or die "Can't open .mailfilter, $!\n";
#    open(my $outfile, "> $listpath/white.lst.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";
    open(my $infile, "< $listpath/sender_whitelist") or die "Can't open .mailfilter, $!\n";
    open(my $outfile, "> $listpath/sender_whitelist.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

    while (<$infile>)
    {
        if ($_ !~/$removeRecord/)
        {
                print $outfile $_;
        }
    }
    close $infile;
    close $outfile;
    unlink untaint("$listpath/sender_whitelist") || die "unlink fail: $!\n";
    rename untaint($listpath."/sender_whitelist.tmp"), untaint($listpath."/sender_whitelist") or die "Can't rename:$!\n";
#    unlink untaint("$listpath/white.lst") || die "unlink fail: $!\n";
#    rename untaint($listpath."/white.lst.tmp"), untaint($listpath."/white.lst") or die "Can't rename:$!\n";


    0;
}


sub add_black_list {
    my $self = shift;
    my $list = $_[0]; # must ARRAY ref
    my $listpath = $_[1];
    my $recordExist = 0;

    die "Malformed input data!\n" unless (ref $list eq 'ARRAY');

#    open (my $infile, "< $listpath/black.lst") or die "Can't write to $listpath/black.lst\n";
#    open (FD, ">> $listpath/black.lst") or die "Can't write to $listpath/black.lst\n";
    open (my $infile, "< $listpath/sender_blacklist") or die "Can't write to $listpath/sender_blacklist\n";
    open (FD, ">> $listpath/sender_blacklist") or die "Can't write to $listpath/sender_blacklist\n";
    flock (FD, LOCK_EX);

    # Add record
    for (@$list) {
	my $checkRecord = $_;

	while (<$infile>)
	{
		if($_ =~ m/$checkRecord/) {
                	$recordExist = 1;
        	}
    	}

    	# Check record exist

    	if ($recordExist == 1){
        	#print "Record Exist!!\n";
        	return 1;
    	}
    	else{
	        print FD "$checkRecord\n";
		#print "Record Not Exist!!\n";
        	return 0;
                #$recordExist = 0;

    	}
    }
    flock (FD, LOCK_UN);
    close FD;
    #return  $recordExist;
}

sub read_black_list {
    my $self = shift;
    my $listpath = $_[0];

#    open (FD, "< $listpath/black.lst") or return []; # ignore error
    open (FD, "< $listpath/sender_blacklist") or return []; # ignore error
    my @arr;
    while(<FD>) {
        chomp;
        s/^\s*//g;
        s/\s*$//g;
        push @arr, $_;
    }
    close FD;
    return \@arr;

}


sub remove_blacklist_record {
    my $self = shift; 1;
    my ($removeRecord, $listpath) = @_;

#    open(my $infile, "< $listpath/black.lst") or die "Can't open .mailfilter, $!\n";
#    open(my $outfile, "> $listpath/black.lst.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";
    open(my $infile, "< $listpath/sender_blacklist") or die "Can't open .mailfilter, $!\n";
    open(my $outfile, "> $listpath/sender_blacklist.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

    while (<$infile>)
    {
        if ($_ !~/$removeRecord/)
        {
                print $outfile $_;
        }
    }
    close $infile;
    close $outfile;
    unlink untaint("$listpath/sender_blacklist") || die "unlink fail: $!\n";
    rename untaint($listpath."/sender_blacklist.tmp"), untaint($listpath."/sender_blacklist") or die "Can't rename:$!\n";
#    unlink untaint("$listpath/black.lst") || die "unlink fail: $!\n";
#    rename untaint($listpath."/black.lst.tmp"), untaint($listpath."/black.lst") or die "Can't rename:$!\n";

    0;
}


sub add_autoreply {
    my $self = shift; 1;
    my ($fromEmail, $message, $homePath) = @_;
    my $counter = 0;
    my $tempFlag = 0;
    my $lines = 0;

    open(FD, "> $homePath/Maildir/autoreply.cf") or die "Can't open autoreply.cf, $!\n";
    flock (FD, LOCK_EX);

    print FD "Content-type: text/plain; charset=UTF-8\n";
    print FD "Content-Transfer-Encoding: 8bit\n";
    print FD "\n";
    print FD "$message\n";

    flock (FD, LOCK_UN);
    close FD;

    if ( ! -e "$homePath/.mailfilter")
    { 
	open(FD, "> $homePath/.mailfilter") or die "Can't open .mailfilter****, $!\n";
	print FD "#MFMAILDROP=2\n";
	print FD "#\n";
	print FD "# DO NOT EDIT THIS FILE.  This is an automatically generated filter.\n";
	print FD "# Generated by ExtMail 1.1\n";
	print FD "\n";
	print FD "FROM='$fromEmail'\n";
	print FD "import SENDER\n";
	print FD "if (\$SENDER eq \"\")\n";
	print FD "{\n";
	print FD " SENDER=\$FROM\n";
	print FD "}\n";
	print FD "\n";
	print FD "to \"\$HOME/Maildir/.\"\n"; # newline
	close FD;	
    }


    open(my $infile, "< $homePath/.mailfilter") or die "Can't open .mailfilter, $!\n";
    while (<$infile>) {
        if ($_ !~ /to \"\$HOME\/Maildir\/.\"/) {
		$lines++;
	}
    }
    close $infile;

    open(my $infile, "< $homePath/.mailfilter") or die "Can't open .mailfilter, $!\n";
    open(my $outfile, "> $homePath/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

    while (<$infile>)
    {
        $counter++;

        print $outfile $_;

        if ($_ =~ /#*autoreply/) {
		$tempFlag  = 1;
        }

	if (($tempFlag == 0) && ($counter == $lines - 1)) {
		print $outfile "#\*autoreply\n";
		print $outfile "cc \"| mailbot -A 'X-Sender: \$FROM' -A 'From: \$FROM' -m '\$HOME/Maildir/autoreply.cf' \$SENDMAIL -t -f ''\"\n";
#		print $outfile "cc \"| mailbot -A 'X-Sender: \$FROM' -A 'From: \$FROM' -m '\$HOME/Maildir/autoreply.cf' \$SENDMAIL -t -f ''\"";
	}
    }

    close $infile;
    close $outfile;
    unlink untaint("$homePath/.mailfilter") || die "unlink fail: $!\n";
    rename untaint($homePath."/.mailfilter.tmp"), untaint($homePath."/.mailfilter") or die "Can't rename:$!\n";

    return 0;

}

sub read_autoreply {
    my $self = shift;
    my $homePath = $_[0];
#    my $data = '';
    my @data;
    my $crlf = $/;

    open(FD, "< $homePath/autoreply.cf") or die "Can't open autoreply.cf, $!\n";

    while (<FD>) {
        if ($_ !~ /(Content)/) {
	        push @data, $_;
#                print $_, "\n";
        }
    }

    close FD;
    return \@data;
#    return $data;
}

#gg
sub remove_autoreply {
    my $self = shift; 1;
    my ($homePath) = @_;

    open(my $infile, "< $homePath/.mailfilter") or die "Can't open .mailfilter, $!\n";
    open(my $outfile, "> $homePath/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

    while (<$infile>)
    {
        if ($_ !~/cc \"\| mailbot -A |#*autoreply/)
        {
                print $outfile $_;
        }
    }
    close $infile;
    close $outfile;
    unlink untaint("$homePath/.mailfilter") || die "unlink fail: $!\n";
    rename untaint($homePath."/.mailfilter.tmp"), untaint($homePath."/.mailfilter") or die "Can't rename:$!\n";

#    unlink untaint("$listpath/autoreply.cf") || die "unlink fail: $!\n";
    unlink untaint("$homePath/Maildir/autoreply.cf") || die "unlink fail: $!\n";
	
}

##20091207
#sub add_changeReturnPath {
sub change_return_path{
    my $self = shift; 1;
    my ($fromEmail, $path) = @_;
    my $counter = 0;
    my $tempFlag = 0;
    my $lines = 0;
    my $recordExist = 0;

    if ( ! -e "$path/.mailfilter")
    {

    open(FD, "> $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    my $s = <FD>;# omit the first line
    seek(FD,0,0) unless($s=~/S|C/); # unget if no quota limit
    local $/= undef;
    $s = <FD>;
    close FD;
    open(FD, "> $path/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";
    print FD "#MFMAILDROP=2\n";
    print FD "#\n";
    print FD "# DO NOT EDIT THIS FILE.  This is an automatically generated filter.\n";
    print FD "# Generated by ExtMail 1.1\n";
    print FD "\n";
    print FD "FROM='$fromEmail'\n";
    print FD "import SENDER\n";
    print FD "if (\$SENDER eq \"\")\n";
    print FD "{\n";
    print FD " SENDER=\$FROM\n";
    print FD "}\n";
    print FD "\n";
    print FD "#xfilter - Return-Path\n";
    print FD "/^From:.\*/\n";
    print FD "getaddr(\$MATCH) =~ /^.\*/;\n";
    print FD "xfilter \"reformail -I 'New-Return-Path: <\$MATCH>'\"\n";
#    print FD "xfilter \"sed -e 's/^Return-Path:.*/Return-Path: <\$MATCH> /'\"\n";
    print FD "\n"; # newline
    print FD "to \"\$HOME/Maildir/.\"\n"; # newline

    close FD;
    unlink untaint("$path/.mailfilter") || die "unlink fail: $!\n";
    rename untaint($path."/.mailfilter.tmp"), untaint($path."/.mailfilter") or die "Can't rename:$!\n";
    }

    else # Append
    {

    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    while (<$infile>) {
        if($_ =~ m/\/\^From:.\*\//) {
                $recordExist = 1;
        }
    }
    close $infile;

    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    while (<$infile>) {
	if ($_ =~ m/SENDER=/) {
		#print $.;
		$lines = $.;
	}
    }
    close $infile;

    # Check record exist
    if ($recordExist == 1){
#        print "Record Exist!!\n";
        return 1;
    }
    else{

	open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
	open(my $outfile, "> $path/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

	while (<$infile>)
	{
		$counter++;
		if ($counter == $lines + 2) {
			print $outfile "\n";
			print $outfile "#xfilter - Return-Path\n";
			print $outfile "/^From:.\*/\n";
			print $outfile "getaddr(\$MATCH) =~ /^.\*/\;\n";
#			print $outfile "xfilter \"sed -e 's/^Return-Path:.*/Return-Path: <\$MATCH> /'\"\n";
			print $outfile "xfilter \"reformail -I 'New-Return-Path: <\$MATCH>'\"\n";

		}
	        print $outfile $_;
	}

	    close $infile;
	    close $outfile;
	    unlink untaint("$path/.mailfilter") || die "unlink fail: $!\n";
	    rename untaint($path."/.mailfilter.tmp"), untaint($path."/.mailfilter") or die "Can't rename:$!\n";

	    }
	    return 0;
    }
}


sub remove_return_path {
    my $self = shift; 1;
    my ($userName, $homePath) = @_;

    open(my $infile, "< $homePath/.mailfilter") or die "Can't open .mailfilter, $!\n";
    open(my $outfile, "> $homePath/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

    while (<$infile>)
    {
#        if ($_ !~/(#xfilter - Return-Path)|\/\^From:.\*\/|getaddr\(\$MATCH\)|xfilter \"sed/)
        if ($_ !~/(#xfilter - Return-Path)|\/\^From:.\*\/|getaddr\(\$MATCH\)|xfilter \"reformail/)
        {
                print $outfile $_;
        }
    }
    close $infile;
    close $outfile;
    unlink untaint("$homePath/.mailfilter") || die "unlink fail: $!\n";
    rename untaint($homePath."/.mailfilter.tmp"), untaint($homePath."/.mailfilter") or die "Can't rename:$!\n";

    0;
}


#20100614
sub block_external_email {
    my $self = shift; 1;
    my ($fromEmail, $targetEmail, $path) = @_;
    my $counter = 0;
    my $tempFlag = 0;
    my $lines = 0;
    my $recordExist = 0;

    if ( ! -e "$path/.mailfilter")
    {

    open(FD, "> $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    my $s = <FD>;# omit the first line
    seek(FD,0,0) unless($s=~/S|C/); # unget if no quota limit
    local $/= undef;
    $s = <FD>;
    close FD;
    open(FD, "> $path/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";
    print FD "#MFMAILDROP=2\n";
    print FD "#\n";
    print FD "# DO NOT EDIT THIS FILE.  This is an automatically generated filter.\n";
    print FD "# Generated by ExtMail 1.1\n";
    print FD "\n";
    print FD "FROM='$fromEmail'\n";
    print FD "import SENDER\n";
    print FD "if (\$SENDER eq \"\")\n";
    print FD "{\n";
    print FD " SENDER=\$FROM\n";
    print FD "}\n";
    print FD "\n#*Block external email\n";
    print FD "if (\/^From: (.*\\@".slashes($targetEmail).")/)\n";
    print FD "  to \"\$HOME/Maildir/.\"\n";
    print FD "else\n";
    print FD "  to /dev/null\n\n";
    print FD "to \"\$HOME/Maildir/.\"\n"; # newline

    close FD;
    unlink untaint("$path/.mailfilter") || die "unlink fail: $!\n";
    rename untaint($path."/.mailfilter.tmp"), untaint($path."/.mailfilter") or die "Can't rename:$!\n";
    }

    else # Append
    {

    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    while (<$infile>) {
#        if($_ =~ m/\/\^From:.\*\//) {
        if($_ =~ /#*Block external email/) {
                $recordExist = 1;
        }
    }
    close $infile;

    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    while (<$infile>) {
        if ($_ =~ m/SENDER=/) {
                #print $.;
                $lines = $.;
        }
    }
    close $infile;

    # Check record exist
    if ($recordExist == 1){
#        print "Record Exist!!\n";
        return 1;
    }
    else{
        open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
        open(my $outfile, "> $path/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";

        while (<$infile>)
        {
                $counter++;
                if ($counter == $lines + 2) {
                        $targetEmail =~ s/\./\\./g;
                        $targetEmail =~ s/-/\\-/g;
                        $targetEmail =~ s/_/\\_/g;
                        print $outfile "\n#*Block external email\n";
                        print $outfile "if (\/^From: (.*\\@".$targetEmail.")/)\n";
#                       print $outfile "if (\/^From: (.*\\@".slashes($targetEmail).")/)\n";
                        print $outfile "    to \"\$HOME/Maildir/.\"\n";
                        print $outfile "else\n";
                        print $outfile "    to /dev/null\n\n";
                }
                print $outfile $_;
        }

            close $infile;
            close $outfile;
            unlink untaint("$path/.mailfilter") || die "unlink fail: $!\n";
            rename untaint($path."/.mailfilter.tmp"), untaint($path."/.mailfilter") or die "Can't rename:$!\n";

            }
            return 0;
    }
}


sub remove_block_external_email {
    my $self = shift; 1;
    my ($fromEmail, $path) = @_;
    my $counter = 0;
    my $tempFlag = 0;
    my $lines = 0;
    my $recordExist = 0;

    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    while (<$infile>) {
        if ($_ =~ m/#*Block external email/) {
                $lines = $.;
        }
    }
    close $infile;

    open(my $infile, "< $path/.mailfilter") or die "Can't open .mailfilter, $!\n";
    open(my $outfile, "> $path/.mailfilter.tmp") or die "Can't open .mailfilter.tmp for write, $!\n";


    # Check record exist
    if ($lines == 0){
#        print "No Record Exist!!\n";
        return 1;
    }
    else{
        while (<$infile>)
        {
                $counter++;
#               print $counter . "<>" . $lines . "\n";
                if (($counter == $lines) || ($counter == $lines + 1) || ($counter == $lines + 2) || ($counter == $lines + 3) || ($counter == $lines + 4)) {
#                       print "**$counter* \n";
                } else {
                        print $outfile $_;
                }
        }

    close $infile;
    close $outfile;
    unlink untaint("$path/.mailfilter") || die "unlink fail: $!\n";
    rename untaint($path."/.mailfilter.tmp"), untaint($path."/.mailfilter") or die "Can't rename:$!\n";

    return 1;
    }
}



##20101115
sub change_maildirsize{
    my $self = shift; 1;
    my ($quota, $path) = @_;
    my $counter = 0;
    my $tempFlag = 0;
    my $lines = 0;
    my $recordExist = 0;

        if ( ! -e "$path/Maildir/maildirsize")
        {

            open(FD, "> $path/Maildir/maildirsize") or die "Can't open maildirsize, $!\n";
            my $s = <FD>;# omit the first line
            seek(FD,0,0) unless($s=~/S|C/); # unget if no quota limit
            local $/= undef;
            $s = <FD>;
            close FD;
            open(FD, "> $path/Maildir/maildirsize.tmp") or die "Can't open maildirsize.tmp for write, $!\n";
            print FD "$quota", "S\n";
            close FD;
            unlink untaint("$path/Maildir/maildirsize") || die "unlink fail: $!\n";
            rename untaint($path."/Maildir/maildirsize.tmp"), untaint($path."/Maildir/maildirsize") or die "Can't rename:$!\n";
        }

        else # Append
        {

                $counter = 1;

                open(my $infile, "< $path/Maildir/maildirsize") or die "Can't open maildirsize, $!\n";
                open(my $outfile, "> $path/Maildir/maildirsize.tmp") or die "Can't open maildirsize.tmp for write, $!\n";

                while (<$infile>)
                {
                        if ($counter == 1) {
                                print $outfile "$quota", "S\n";
                        }
                        else
                        {
                                print $outfile $_;
                        }
                        $counter++;
                }

                close $infile;
                close $outfile;
                unlink untaint("$path/Maildir/maildirsize") || die "unlink fail: $!\n";
                rename untaint($path."/Maildir/maildirsize.tmp"), untaint($path."/Maildir/maildirsize") or die "Can't rename:$!\n";

            return 0;
    }
}

