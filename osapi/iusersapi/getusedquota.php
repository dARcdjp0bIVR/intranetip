<?
/*
Using By :
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Get user used quota
*/
/********************************** Change Log ***********************************/
#
#	Date	: 20100119 (Ronald Yeung)
#	Details : when count the quota with linux command - du , it must use the vuser permission
#
#	Date	: 20091211 (Ronald Yeung)
#	Details	: change the userlogin to lower case
#
/******************************* End OF Change Log *******************************/


include("includes/global.php");

$loginID = $_GET['loginID'];
$loginID = strtolower($loginID);
$errorMessage = "";

if (! checkIP($client_ip))
{
	
	if ($loginID == null) {
        	//$errorMessage = "0, Paratmeter cannot be empty";
		$errorMessage = "0";
	}
	else 
	{
		$userHomeDir = escapeshellcmd("$homedir/$loginID");
		//$job = shell_exec("du -b -s $userHomeDir"); //summary
		//$job = shell_exec("sudo -u vuser /usr/bin/du -b -s $userHomeDir"); //summary
		$job = shell_exec("sudo /usr/bin/du -b -s $userHomeDir"); //summary
		$finalQuota = explode("\t", $job);
		$UsedQuota = $finalQuota[0] / 1024 / 1024;
	}
}
else
{
	//$errorMessage = "0, Connection Refused!";
	$errorMessage = "0";
}

if ( $errorMessage ==  "")
	//print "1,$finalQuota[0]";
	print $UsedQuota;
else
	print $errorMessage;

?>
