<?
/*
Using By : 
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Change user password
*/

/********************************** Change Log ***********************************/
#	2014-07-04 (Carlos): decrypt password from $Pwd if set and store hashed password into database
#
#	Date	: 20091211 (Ronald Yeung)
#	Details	: change the userlogin to lower case
#
/******************************* End OF Change Log *******************************/


include("includes/global.php");

$loginID = $_GET['loginID'];
$loginID = strtolower($loginID);
$newPassword = $_GET['newPassword'];
$errorMessage = "";

if(isset($pwd) && $pwd != ""){
	$newPassword = decrypt(base64_decode($pwd));
}

if (! checkIP($client_ip))
{
	if ($loginID == null || $newPassword == null) {
        	//$errorMessage = "0, Paratmeters cannot be empty";
		$errorMessage = "0";
	}
	else 
	{
		$hashed_pwd = doSHA256($newPassword);
		// Change User record
		$sql_user = "UPDATE ftpuser set passwd = \"$hashed_pwd\" where userid = \"$loginID\"";
		if (!($db->Execute($sql_user))) {
			//$errorMessage = "0, Error updating in ftpuser: " . $db->ErrorMsg();
			$errorMessage = "0";
		} 
	}
}
else
{
	//$errorMessage = "0, Connection Refused!";
	$errorMessage = "0";
}

if ( $errorMessage ==  "")
	//print "1,Password change successfully";
	print "1";
else
	print $errorMessage;

?>
