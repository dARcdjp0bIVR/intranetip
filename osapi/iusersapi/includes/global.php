<?
include("libraries/adodb_lite/adodb.inc.php");
include("config.php");

include("includes/functions.php");
include("includes/database.php");

// global registers does not work anymore after PHP5.4+ 
if(isset($_GET) && count($_GET))
{
	foreach($_GET as $k => $v)
	{
		global $$k;
		$$k = $v;
	}
}

if(isset($_POST) && count($_POST))
{
	foreach($_POST as $k => $v)
	{
		global $$k;
		$$k = $v;
	}
}

if(isset($_REQUEST) && count($_REQUEST))
{
	foreach($_REQUEST as $k => $v)
	{
		global $$k;
		$$k = $v;
	}
}
?>
