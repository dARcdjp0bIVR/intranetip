<?
/*
Create Date: 25 Oct 2008
Modified Date: 28 Oct 2008
Description: Get user quota list
*/
/********************************** Change Log ***********************************/
#
#	Date	: 2015-11-11 (Carlos) - check folder existenceto avoid error output. 
#								  - sudo does not specify vuser to execute command, it may ask for password entry that stop the flow.
#	
#	Date	: 20100119 (Ronald Yeung)
#	Details : when count the quota with linux command - du , it must use the vuser permission
#
/******************************* End OF Change Log *******************************/

include("includes/global.php");

if (! checkIP($client_ip))
{
	// Select Database
	$result = $db->Execute("SELECT name,bytes_in_avail FROM ftpquotalimits");
	if ($result === false) die("failed");
	while (!$result->EOF) {
	        $userHomeDir = "$homedir/" . $result->fields[0];
	    
	    if(!file_exists($userHomeDir)) {
	    	 $result->MoveNext();
	    	 continue;
	    }    
		
		$userHomeDir = escapeshellcmd($userHomeDir);
        	//$job = shell_exec("du -s -k $userHomeDir"); //summary
		//$job = shell_exec("du -b -s $userHomeDir"); //summary
		//$job = shell_exec("sudo -u vuser /usr/bin/du -b -s $userHomeDir"); //summary
		$job = shell_exec("sudo /usr/bin/du -b -s $userHomeDir"); //summary

		if ($job == ""){ 
	        	//$line = $result->fields[0] . ",0," . $result->fields[1] . "," . $result->fields[1];
			$soft_quota = $result->fields[1] / 1024 / 1024;
			$hard_quota = $result->fields[1] / 1024 / 1024;
			//$soft_quota = $result->fields[1] / 1024;
			//$hard_quota = $result->fields[1] / 1024;
			$line = $result->fields[0] . ",0," . $soft_quota . "," . $hard_quota;

		}
		else 
		{
		       $finalQuota = explode("\t", $job);
			//$line = $result->fields[0] . "," . $finalQuota[0] . "," . $result->fields[1] . "," . $result->fields[1];
			$used_quota = $finalQuota[0] / 1024 / 1024;
			$soft_quota = $result->fields[1] / 1024 / 1024;
			$hard_quota = $result->fields[1] / 1024 / 1024;
			//$used_quota = $finalQuota[0] / 1024;
			//$soft_quota = $result->fields[1] / 1024;
			//$hard_quota = $result->fields[1] / 1024;

        		$line = $result->fields[0] . "," . $used_quota . "," . $soft_quota . "," . $hard_quota;
		}
	        //print $line . "<br>";
		  print $line . "###";
	        $result->MoveNext();
	}
}
else
{
	//print "0, Connection Refused!";
	print "0";
}


?>
