<?php
# read file function
function get_file_content($file){
        if(file_exists($file)){
                $x = ((is_file($file)) && ($fd = fopen($file, "r"))) ? fread($fd,filesize($file)) : "";
                fclose ($fd);
        }
        return $x;
}

# write file function
function write_file_content($body, $file)
{
        $x = (($fd = fopen($file, "w")) && (fputs($fd, $body))) ? 1 : 0;
        fclose ($fd);
        return $x;
}

# Generate unique and unpredictable session key
function generateSessionKey()
{
         $original = session_id().time();
         $hash_value = md5($original);
         return $hash_value;
}

# Check the secret is matched with stored one
function checkSecretMatched($ip,$secret)
{
	 return true; //as the flow is changed, skip checking first Joe Tse 2008/05/31

         if ($secret == "") return false;
         global $file_dir,$file_prefix,$time_limit;
         #echo "from file: ";
         $content = get_file_content("$file_dir/$file_prefix$ip");
         $data = explode("\n",$content);
         list($ts,$key) = $data;
         $current = time();
         #print_r($data);
         #echo "to match : $secret";
         if ($key != $secret) return false;
         #echo "matched\n";
         if ($current - $ts > $time_limit * 1000)
         {
             #echo "expired\n";
             return false;
         }
         else return true;
}

# Check the IP address is allowed or not
function checkIP($ip)
{
         global $allowed_ip;
#         print_r($allowed_ip);
#         echo "to match: $ip\n";
         for ($i=0; $i<sizeof($allowed_ip); $i++)
         {
              if ($allowed_ip[$i]==$ip) return true;
              if (testip($allowed_ip[$i],$ip)) return true;
         }
         return false;
}
function testip($range,$ip) {
  $result = 1;

  # IP Pattern Matcher
  # J.Adams <jna@retina.net>
  #
  # Matches:
  #
  # xxx.xxx.xxx.xxx        (exact)
  # xxx.xxx.xxx.[yyy-zzz]  (range)
  # xxx.xxx.xxx.xxx/nn     (nn = # bits, cisco style -- i.e. /24 = class C)
  #
  # Does not match:
  # xxx.xxx.xxx.xx[yyy-zzz]  (range, partial octets not supported)


  	//if(ereg("([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)/([0-9]+)",$range,$regs))
   	if(preg_match("/^([0-9]+)\.([0-9]+)\.([0-9]+)\.([0-9]+)\/([0-9]+)$/i",$range,$regs))
	{

      # perform a mask match
      $ipl = ip2long($ip);
      $rangel = ip2long($regs[1] . "." . $regs[2] .
"." . $regs[3] . "." . $regs[4]);

      $maskl = 0;

      for ($i = 0; $i< 31; $i++) {
          if ($i < $regs[5]-1) {
              $maskl = $maskl + pow(2,(30-$i));
          }
      }

      if (($maskl & $rangel) == ($maskl & $ipl)) {
          return 1;
      } else {
          return 0;
      }
   } else {

      # range based
      $maskocts = explode(".",$range);
      $ipocts = explode(".",$ip);

      # perform a range match
      for ($i=0; $i<4; $i++) {
          //if(ereg("\[([0-9]+)\-([0-9]+)\]",$maskocts[$i],$regs))
          if(preg_match("/^\[([0-9]+)\-([0-9]+)\]$/i",$maskocts[$i],$regs))
          {
            if ( ($ipocts[$i] > $regs[2]) || ($ipocts[$i] < $regs[1])) {
                  $result = 0;
              }
          }
          else
          {
              if ($maskocts[$i] <> $ipocts[$i]) {
                  $result = 0;
              }
          }
      }
  }
  return $result;
}

# Reserved Account name
$system_reserved_account = array (
"adm","admin","amanda","apache","bin","canna","daemon","eclass","eclassadm","ftp",
"games","gdm","gopher","halt","ident","junior","ldap","lp","mail","mailman",
"mailnull","mysql","named","netdump","news","nfsnobody","nobody","nscd","ntp",
"operator","pcap","postfix","postgres","privoxy","root","rpc","rpcuser",
"rpm","shutdown","smmsp","squid","sshd","sync","uucp","vcsa","webalizer","webmailadm","wnn","xfs"
,"irc"
);
?>