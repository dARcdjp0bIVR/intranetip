<?php
include_once("includes/global.php");
include_once("includes/liblinux.php");

$email = strtolower(urldecode($_REQUEST['email']));
$module = strtolower($_REQUEST['module']);
$actype = $_REQUEST['actype'];
$at_pos = strpos($email,'@');

$llinux = new liblinux($actype);

if($actype == 'postfix' && $module=='imail' && $at_pos > 0)
{
	$userlogin = substr($email,0,$at_pos);
	$domain = substr($email,$at_pos+1);
	
	$mail_dir = '/home/domains/'.$domain.'/'.$userlogin.'/Maildir';

	$junk_new_path = $mail_dir.'/.Junk/new';
	$junk_cur_path = $mail_dir.'/.Junk/cur';
	$to_path = $mail_dir.'/new';
	
	$find_junkmails = 'sudo find "'.$junk_new_path.'" -type f';
	$find_junkmails_text = trim(shell_exec($find_junkmails));
	$junkmail_paths = explode("\n",$find_junkmails_text);
	$junkmail_paths_count = count($junkmail_paths);
	
	for($i=0;$i<$junkmail_paths_count;$i++)
	{
		$mailfile = trim($junkmail_paths[$i]);
		if($mailfile == '') continue;
		$cmd = 'sudo mv \''.$mailfile.'\' \''.$to_path.'\'';
		shell_exec($cmd);
	}
	
	$find_junkmails_cur = 'sudo find "'.$junk_cur_path.'" -type f';
	$find_junkmails_cur_text = trim(shell_exec($find_junkmails_cur));
	$junkmail_paths_cur = explode("\n",$find_junkmails_cur_text);
	$junkmail_paths_cur_count = count($junkmail_paths_cur);
	
	for($i=0;$i<$junkmail_paths_cur_count;$i++)
	{
		$mailfile = trim($junkmail_paths_cur[$i]);
		if($mailfile == '') continue;
		$cmd = 'sudo mv \''.$mailfile.'\' \''.$to_path.'\'';
		shell_exec($cmd);
	}
	
	echo "1";
}else{
	echo "0";
}

?>