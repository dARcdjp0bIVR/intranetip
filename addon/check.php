<?php
# using:  
/*
 *	Purpose:	back end login logic checking
 * 
 */

@session_start();

$_config["ExpireTime"] = 1800;	// expire after half an hour
 
if (!isset($_SESSION['BE_AdminLogin']) || ($_SESSION['BE_AdminLogin']==""))
{
	if(isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != ''){
		$uri_parts = explode('?',$_SERVER['REQUEST_URI']);
		$request_uri = $uri_parts[0]; // abandom the query string
		$exclude_urls = array('/addon/check.php','/addon/login.php');
		if(!in_array($request_uri,$exclude_urls)){
			// this session variable REDIRECT_URL will be used at /addon/login.php and assign to DirectLink
			$_SESSION['BE_REDIRECT_URL'] = $_SERVER['REQUEST_URI'];
		}
	}
    header("Location: /addon/login.php");
    exit();
}
else if ((time()-$_SESSION['BE_LoginTime'] > $_config["ExpireTime"])) {
	@session_unset();
	@session_destroy();
    header("Location: /addon/login.php");
    exit();
}
else {
	//	print "pass and return<br>";
}
?>
