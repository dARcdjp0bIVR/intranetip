<?
# modifying by:  

################# Please note ###########################
# CREATE TABLE	<=== "IF NOT EXISTS"
# CREATE TABLE	<===ENGINE=InnoDB DEFAULT CHARSET=utf8
#########################################################

############################################################################################
################################# IntranetIP schema update #################################
############################################################################################


$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_GRADING_SCHEME for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_GRADING_SCHEME (
 SchemeID int(8) NOT NULL auto_increment,
 SchemeTitle varchar(128),
 Description varchar(255),
 SchemeType varchar(2),
 FullMark float,
 Pass varchar(16),
 Fail varchar(16),
 Weighting float default 1,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SchemeID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_GRADING_SCHEME_GRADEMARK for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_GRADING_SCHEME_GRADEMARK (
 GradeMarkID int(8) NOT NULL auto_increment,
 SchemeID int(8),
 Nature varchar(2),
 LowerLimit float,
 Grade varchar(4),
 GradePoint float,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GradeMarkID),
 INDEX SchemeID (SchemeID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_SUBJECT_GRADING_SCHEME for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_SUBJECT_GRADING_SCHEME (
 SubjectGradingID int(11) NOT NULL auto_increment,
 ParentSubjectGradingID int(11),
 SubjectID int(11),
 SchemeID int(8),
 Scale varchar(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SubjectGradingID),
 INDEX SubjectID (SubjectID),
 INDEX SchemeID (SchemeID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_SUBJECT_GRADING_SCHEME_FORM for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_SUBJECT_GRADING_SCHEME_FORM (
 SubjectGradingID int(11),
 ClassLevelID int(11),
 DateInput datetime,
 DateModified datetime,
 UNIQUE SubjectForm (SubjectGradingID, ClassLevelID),
 INDEX SubjectGradingID (SubjectGradingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_REPORT_TEMPLATE for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_REPORT_TEMPLATE (
 ReportID int(8) NOT NULL auto_increment,
 ReportTitle varchar(255),
 ReportType varchar(64),
 Description text,
 DisplaySettings longblob,
 ShowFullMark tinyint default 0,
 RecordStatus tinyint default 0,
 LastGenerated datetime,
 ShowPosition tinyint default 0,
 PositionRange varchar(10),
 HideNotEnrolled tinyint default 0,
 NoHeader int(2) default -1,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ReportID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_REPORT_TEMPLATE_FORM for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_REPORT_TEMPLATE_FORM (
 ReportID int(8),
 ClassLevelID int(11),
 DateInput datetime,
 DateModified datetime,
 UNIQUE ReportForm (ReportID, ClassLevelID),
 INDEX ReportID (ReportID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_REPORT_TEMPLATE_SUBJECT for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_REPORT_TEMPLATE_SUBJECT (
 ReportSubjectID int (8) NOT NULL auto_increment,
 ReportID int(8),
 SubjectID int(11),
 DisplayOrder int(2),
 HidePosition tinyint default 0,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ReportSubjectID),
 UNIQUE ReportSubject (ReportID, SubjectID),
 INDEX ReportID (ReportID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_REPORT_TEMPLATE_COLUMN for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_REPORT_TEMPLATE_COLUMN (
 ReportColumnID int (8) NOT NULL auto_increment,
 ReportID int (8),
 ColumnTitle varchar (128),
 Weight float,
 DisplayOrder int (2),
 ShowPosition tinyint default 0,
 PositionRange varchar(10),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ReportColumnID),
 INDEX ReportID (ReportID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_REPORT_TEMPLATE_CELL for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_REPORT_TEMPLATE_CELL (
 ReportColumnID int(8),
 ReportSubjectID int(8),
 Setting varchar(8) default 'N/A',
 DateInput datetime,
 DateModified datetime,
 UNIQUE ColumnSubject (ReportColumnID, ReportSubjectID),
 INDEX ReportColumnID (ReportColumnID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_MARKSHEET_COLLECTION for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_MARKSHEET_COLLECTION (
 StartDate date,
 EndDate date,
 Year varchar(50),
 Semester varchar(50),
 AllowClassTeacherComment tinyint default 0,
 AllowSubjectTeacherComment tinyint default 0,
 DateInput datetime,
 DateModified datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_MARKSHEET_SCORE for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_MARKSHEET_SCORE (
 MarksheetScoreID int(11) NOT NULL auto_increment,
 StudentID int(11),
 SubjectID int(8),
 ReportColumnID int(8),
 Mark float,
 Grade varchar(8),
 IsWeighted tinyint default 0,
 IsOverall tinyint default 0,
 TeacherID int(11),
 Year varchar(50),
 Semester varchar(50),
 OrderMeritClass int(11),
 OrderMeritForm int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (MarksheetScoreID),
 INDEX StudentID (StudentID),
 INDEX SubjectID (SubjectID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_SUB_MARKSHEET_COLUMN for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_SUB_MARKSHEET_COLUMN (
 ColumnID int(11) NOT NULL auto_increment,
 SubjectID int(8),
 ReportColumnID int(8),
 ColumnTitle varchar(128),
 Weight float,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ColumnID),
 INDEX ReportColumnID (ReportColumnID),
 INDEX SubjectID (SubjectID),
 INDEX ColumnTitle (ColumnTitle)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_SUB_MARKSHEET_SCORE for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_SUB_MARKSHEET_SCORE (
 ColumnID int(11),
 SubjectID int(8),
 ReportColumnID int(8),
 StudentID int(11),
 Mark float,
 Grade varchar(8),
 IsOverall tinyint default 0,
 IsOverallWeighted tinyint default 0,
 TeacherID int(11),
 Year varchar(50),
 Semester varchar(50),
 DateInput datetime,
 DateModified datetime,
 INDEX ColumnID (ColumnID),
 INDEX SubjectID (SubjectID),
 INDEX ReportColumnID (ReportColumnID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_MARKSHEET_NOTIFICATION for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_MARKSHEET_NOTIFICATION (
 NotificationID int(8) NOT NULL auto_increment,
 SubjectID int(8),
 ClassID int(11),
 ShowMainMarksheet tinyint default 0,
 ShowSubMarksheet tinyint default 0,
 TeacherID int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (NotificationID),
 INDEX SubjectID (SubjectID),
 INDEX ClassID (ClassID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-02-21",
"Add a table RC_MARKSHEET_FEEDBACK for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_MARKSHEET_FEEDBACK (
 FeedbackID int (8) NOT NULL auto_increment,
 SubjectID int(8),
 Year varchar(50),
 Semester varchar(50),
 StudentID int (11),
 IsComment tinyint default 0,
 Comment text,
 IsTeacherRead tinyint default 0,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (FeedbackID),
 INDEX SubjectID (SubjectID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-03-21",
"Add a table RC_SUBJECT_TEACHER for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_SUBJECT_TEACHER (
 SubjectTeacherID int(8) NOT NULL auto_increment,
 UserID int(11),
 SubjectID int(8),
 ClassID int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (SubjectTeacherID),
 UNIQUE TeacherSubjectClass (UserID, SubjectID, ClassID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-04-13",
"Add a table RC_REPORT_RESULT for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_REPORT_RESULT (
 ResultID int(8) NOT NULL auto_increment,
 StudentID int(11),
 ReportID int(8),
 GrandTotal float,
 AverageMark float,
 GPA float,
 OrderMeritClass int(11),
 OrderMeritClassTotal int(11),
 OrderMeritForm int(11),
 OrderMeritFormTotal int(11),
 Year varchar(50),
 Semester varchar(50),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ResultID),
 UNIQUE ReportStudent (StudentID, ReportID, Year, Semester),
 INDEX StudentID (StudentID),
 INDEX ReportID (ReportID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-03",
"Add a table RC_MARKSHEET_VERIFICATION for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_MARKSHEET_VERIFICATION (
 StartDate date,
 EndDate date,
 DateInput datetime,
 DateModified datetime
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
#########################################################################################

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add UpperAge int(2) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add LowerAge int(2) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add Gender varchar(2) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add AttachmentLink1 varchar(255) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add AttachmentLink2 varchar(255) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add AttachmentLink3 varchar(255) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add AttachmentLink4 varchar(255) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Alter table INTRANET_ENROL_GROUPINFO for eEnrollment",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add AttachmentLink5 varchar(255) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_GROUPSTAFF for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPSTAFF (
 GroupStaffID int(11) NOT NULL auto_increment,
 EnrolGroupID int(11) default 0,
 UserID int(11) default 0,
 StaffType varchar(10) default NULL,
 INDEX EnrolGroupID (EnrolGroupID),
 PRIMARY KEY (GroupStaffID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_GROUPCLASSLEVEL for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUPCLASSLEVEL (
 GroupClassLvlID int(11) NOT NULL auto_increment,
 EnrolGroupID int(11) default 0,
 ClassLevelID int(11) default 0,
 INDEX EnrolGroupID (EnrolGroupID),
 INDEX ClassLevelID (ClassLevelID),
 PRIMARY KEY (GroupClassLvlID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_GROUP_DATE for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUP_DATE (
 GroupDateID int(11) NOT NULL auto_increment,
 EnrolGroupID int(11) default 0,
 ActivityDateStart datetime default NULL,
 ActivityDateEnd datetime default NULL,
 INDEX EnrolGroupID (EnrolGroupID),
 PRIMARY KEY (GroupDateID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_EVENTINFO for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENTINFO (
 EnrolEventID int(11) NOT NULL auto_increment,
 Quota int(11) default 0,
 Approved int(11) default 0,
 EventTitle varchar(255) default NULL,
 Description text default NULL,
 UpperAge int(2) default NULL,
 LowerAge int(2) default NULL,
 Gender varchar(2) default NULL,
 AttachmentLink1 varchar(255) default NULL,
 AttachmentLink2 varchar(255) default NULL,
 AttachmentLink3 varchar(255) default NULL,
 AttachmentLink4 varchar(255) default NULL,
 AttachmentLink5 varchar(255) default NULL,
 ApplyStartTime datetime default NULL,
 ApplyEndTime datetime default NULL,
 ApplyUserType varchar(2) default NULL,
 ApplyMethod int(2) default NULL,
 DateInput datetime default NULL,
 DateModified datetime default NULL,
 PRIMARY KEY (EnrolEventID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_EVENTSTUDENT for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENTSTUDENT (
 EventStudentID int(11) NOT NULL auto_increment,
 StudentID int(11) NOT NULL default 0,
 EnrolEventID int(11) NOT NULL,
 RecordType char(2) default NULL,
 RecordStatus char(2) default NULL,
 DateInput datetime default NULL,
 DateModified datetime default NULL,
 PRIMARY KEY (EventStudentID),
 INDEX StudentID (StudentID),
 INDEX EnrolEventID (EnrolEventID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_EVENTSTAFF for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENTSTAFF (
 EventStaffID int(11) NOT NULL auto_increment,
 EnrolEventID int(11) default 0,
 UserID int(11) default 0,
 StaffType varchar(10) default NULL,
 INDEX EnrolGroupID (EnrolEventID),
 PRIMARY KEY (EventStaffID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_EVENTCLASSLEVEL for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENTCLASSLEVEL (
 EventClassLvlID int(11) NOT NULL auto_increment,
 EnrolEventID int(11) default 0,
 ClassLevelID int(11) default 0,
 INDEX EnrolEventID (EnrolEventID),
 INDEX ClassLevelID (ClassLevelID),
 PRIMARY KEY (EventClassLvlID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-05-16",
"Add a table INTRANET_ENROL_EVENT_DATE for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENT_DATE (
 EventDateID int(11) NOT NULL auto_increment,
 EnrolEventID int(11) default 0,
 ActivityDateStart datetime default NULL,
 ActivityDateEnd datetime default NULL,
 INDEX EnrolEventID (EnrolEventID),
 PRIMARY KEY (EventDateID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2007-05-17",
"Add a table RC_MARKSHEET_COMMENT for Report Card System",
"CREATE TABLE IF NOT EXISTS RC_MARKSHEET_COMMENT (
 CommentID int (8) NOT NULL auto_increment,
 StudentID int (11),
 SubjectID int(8),
 Year varchar(50),
 Semester varchar(50),
 Comment text,
 TeacherID int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (CommentID),
 INDEX SubjectID (SubjectID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2007-06-01",
"Add a table INTRANET_ENROL_GROUP_ATTENDANCE for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUP_ATTENDANCE (
 GroupAttendanceID int(11) NOT NULL auto_increment,
 GroupDateID int(11) NOT NULL,
 GroupID int(11) NOT NULL,
 StudentID int(11) NOT NULL,
 PRIMARY KEY (GroupAttendanceID),
 INDEX GroupDateID (GroupDateID),
 INDEX GroupID (GroupID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-01",
"Add a table INTRANET_ENROL_EVENT_ATTENDANCE for eEnrollment",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENT_ATTENDANCE (
 EventAttendanceID int(11) NOT NULL auto_increment,
 EventDateID int(11) NOT NULL,
 EnrolEventID int(11) NOT NULL,
 StudentID int(11) NOT NULL,
 PRIMARY KEY (EventAttendanceID),
 INDEX EventDateID (EventDateID),
 INDEX EnrolEventID (EnrolEventID),
 INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-06",
"Speed up ClassName query of INTRANET_USER",
"alter table INTRANET_USER add INDEX ClassName (ClassName);"
);

$sql_eClassIP_update[] = array(
"2007-06-06",
"Speed up ClassLevelID query of INTRANET_CLASS",
"alter table INTRANET_CLASS add INDEX ClassLevelID (ClassLevelID);"
);

$sql_eClassIP_update[] = array(
"2007-06-07",
"add CommentStudent to INTRANET_ENROL_GROUPSTUDENT",
"alter table INTRANET_ENROL_GROUPSTUDENT add CommentStudent text;"
);

$sql_eClassIP_update[] = array(
"2007-06-14",
"add CommentStudent to INTRANET_ENROL_EVENTSTUDENT",
"alter table INTRANET_ENROL_EVENTSTUDENT add CommentStudent text;"
);

$sql_eClassIP_update[] = array(
"2007-06-18",
"add index Year to RC_REPORT_RESULT",
"alter table RC_REPORT_RESULT add index Year (Year) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-18",
"add index Semester to RC_REPORT_RESULT",
"alter table RC_REPORT_RESULT add index Semester (Semester) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-18",
"add index Year to RC_MARKSHEET_SCORE",
"alter table RC_MARKSHEET_SCORE add index Year (Year) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-18",
"add index Semester to RC_MARKSHEET_SCORE",
"alter table RC_MARKSHEET_SCORE add index Semester (Semester) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add EventCategory to INTRANET_ENROL_EVENTINFO",
"alter table INTRANET_ENROL_EVENTINFO add EventCategory varchar(128);");

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassNumber to RC_MARKSHEET_SCORE",
"alter table RC_MARKSHEET_SCORE add column ClassNumber varchar(16) after Semester"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassName to RC_MARKSHEET_SCORE",
"alter table RC_MARKSHEET_SCORE add column ClassName varchar(50) after Semester"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassNumber to RC_MARKSHEET_FEEDBACK",
"alter table RC_MARKSHEET_FEEDBACK add column ClassNumber varchar(16) after Semester"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassName to RC_MARKSHEET_FEEDBACK",
"alter table RC_MARKSHEET_FEEDBACK add column ClassName varchar(50) after Semester"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassNumber to RC_REPORT_RESULT",
"alter table RC_REPORT_RESULT add column ClassNumber varchar(16) after Semester"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassName to RC_REPORT_RESULT",
"alter table RC_REPORT_RESULT add column ClassName varchar(50) after Semester"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassNumber to RC_MARKSHEET_COMMENT",
"alter table RC_MARKSHEET_COMMENT add column ClassNumber varchar(16) after Semester"
);

$sql_eClassIP_update[] = array(
"2007-06-20",
"add the field ClassName to RC_MARKSHEET_COMMENT",
"alter table RC_MARKSHEET_COMMENT add column ClassName varchar(50) after Semester"
);


$sql_eClassIP_update[] = array(
"2007-06-20",
"create table INTRANET_ENROL_USER_ACL for eEnrollment role",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_USER_ACL (
 UserID int(11) NOT NULL,
 UserLevel int(11) default 0,
 UNIQUE UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-22",
"add the field ProcessingUserID to PAYMENT_PAYMENT_ITEMSTUDENT",
"alter table PAYMENT_PAYMENT_ITEMSTUDENT add column ProcessingUserID int(11)"
);

$sql_eClassIP_update[] = array(
"2007-06-22",
"add the field LastUpdateByUser to PAYMENT_ACCOUNT",
"alter table PAYMENT_ACCOUNT add column LastUpdateByUser varchar(255)"
);

$sql_eClassIP_update[] = array(
"2007-06-25",
"add the field ProcessingUserID to PAYMENT_PAYMENT_ITEM",
"alter table PAYMENT_PAYMENT_ITEM add column ProcessingUserID int(11)"
);

$sql_eClassIP_update[] = array(
"2007-06-25",
"add the field AddToPayment to INTRANET_ENROL_GROUPINFO",
"alter table INTRANET_ENROL_GROUPINFO add column AddToPayment int(2) default 0;"
);

$sql_eClassIP_update[] = array(
"2007-06-25",
"add the field AddToPayment to INTRANET_ENROL_EVENTINFO",
"alter table INTRANET_ENROL_EVENTINFO add column AddToPayment int(2) default 0;"
);

$sql_eClassIP_update[] = array(
"2007-06-25",
"add the field Performance to INTRANET_ENROL_EVENTSTUDENT",
"alter table INTRANET_ENROL_EVENTSTUDENT add column Performance text;"
);

$sql_eClassIP_update[] = array(
"2007-06-28",
"add the field DateInput to INTRANET_ENROL_EVENT_ATTENDANCE",
"alter table INTRANET_ENROL_EVENT_ATTENDANCE add column DateInput datetime;"
);

$sql_eClassIP_update[] = array(
"2007-06-28",
"add the field DateModified to INTRANET_ENROL_EVENT_ATTENDANCE",
"alter table INTRANET_ENROL_EVENT_ATTENDANCE add column DateModified datetime;"
);

$sql_eClassIP_update[] = array(
"2007-06-28",
"add the field SiteName to INTRANET_ENROL_EVENT_ATTENDANCE",
"alter table INTRANET_ENROL_EVENT_ATTENDANCE add column SiteName varchar(255);"
);

$sql_eClassIP_update[] = array(
"2007-06-29",
"create table INTRANET_ENROL_CATEGORY for eEnrollment category",
"CREATE TABLE IF NOT EXISTS INTRANET_ENROL_CATEGORY (
 CategoryID int (8) NOT NULL auto_increment,
 CategoryName varchar(255),
 DisplayOrder int(2),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (CategoryID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-06-28",
"alter the field EventCategory in INTRANET_ENROL_EVENTINFO",
"alter table INTRANET_ENROL_EVENTINFO change EventCategory EventCategory int(11);"
);

$sql_eClassIP_update[] = array(
"2007-06-28",
"add the GroupCategory in INTRANET_ENROL_GROUPINFO",
"alter table INTRANET_ENROL_GROUPINFO add GroupCategory int(11);"
);

$sql_eClassIP_update[] = array(
"2007-07-03",
"add the field DateInput to INTRANET_ENROL_GROUP_ATTENDANCE",
"alter table INTRANET_ENROL_GROUP_ATTENDANCE add column DateInput datetime;"
);

$sql_eClassIP_update[] = array(
"2007-07-03",
"add the field DateModified to INTRANET_ENROL_GROUP_ATTENDANCE",
"alter table INTRANET_ENROL_GROUP_ATTENDANCE add column DateModified datetime;"
);

$sql_eClassIP_update[] = array(
"2007-07-03",
"add the field SiteName to INTRANET_ENROL_GROUP_ATTENDANCE",
"alter table INTRANET_ENROL_GROUP_ATTENDANCE add column SiteName varchar(255);"
);

$sql_eClassIP_update[] = array(
"2007-07-03",
"add the field GroupID to INTRANET_ENROL_EVENTINFO",
"alter table INTRANET_ENROL_EVENTINFO add column GroupID int(11) default 0;"
);

$sql_eClassIP_update[] = array(
"2007-07-23",
"add the field PaymentAmount to INTRANET_ENROL_GROUPINFO",
"alter table INTRANET_ENROL_GROUPINFO add column PaymentAmount int(5) default 0;"
);

$sql_eClassIP_update[] = array(
"2007-07-23",
"add the field PaymentAmount to INTRANET_ENROL_EVENTINFO",
"alter table INTRANET_ENROL_EVENTINFO add column PaymentAmount int(5) default 0;"
);

$sql_eClassIP_update[] = array(
"2007-07-30",
"add the field Footer to RC_REPORT_TEMPLATE",
"alter table RC_REPORT_TEMPLATE add column Footer text;"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Add a table CALENDAR_EVENT_ENTRY for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_EVENT_ENTRY (
  EventID int(10) NOT NULL auto_increment,
  RepeatID int(8) default NULL,
  UserID int(8) NOT NULL default '0',
  CalID int(10) NOT NULL default '0',
  EventDate datetime NOT NULL default '0000-00-00 00:00:00',
  InputDate datetime default NULL,
  ModifiedDate datetime default NULL,
  Duration int(8) default NULL,
  IsImportant char(1) default NULL,
  IsAllDay char(1) default NULL,
  Access char(1) default NULL,
  Title varchar(255) NOT NULL default '',
  Description text,
  Location varchar(255) default NULL,
  Url text,
  PRIMARY KEY  (EventID),
  KEY RepeatID (RepeatID),
  KEY UserID (UserID),
  KEY CalID (CalID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Add a table CALENDAR_EVENT_ENTRY_REPEAT for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_EVENT_ENTRY_REPEAT (
  RepeatID int(8) NOT NULL auto_increment,
  RepeatType varchar(20) default NULL,
  EndDate datetime default NULL,
  Frequency int(2) default '1',
  Detail varchar(7) default NULL,
  PRIMARY KEY  (RepeatID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Add a table CALENDAR_CALENDAR for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_CALENDAR (
  CalID int(8) NOT NULL auto_increment,
  Owner int(8) NOT NULL default '0',
  Name varchar(255) NOT NULL default '',
  Description text,
  PRIMARY KEY  (CalID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Add a table CALENDAR_CALENDAR_VIEWER for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_CALENDAR_VIEWER (
  CalID int(8) NOT NULL default '0',
  UserID int(8) NOT NULL default '0',
  GroupID int(8) default NULL,
  GroupType char(1) default NULL,
  Access char(3) default NULL,
  Color char(6) default NULL,
  Visible char(1) default '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Add a table CALENDAR_REMINDER for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_REMINDER (
  ReminderID int(10) NOT NULL auto_increment,
  EventID int(8) NOT NULL default '0',
  UserID int(8) NOT NULL default '0',
  ReminderType char(1) NOT NULL default '',
  ReminderDate datetime NOT NULL default '0000-00-00 00:00:00',
  ReminderBefore int(8) default '0',
  LastSent datetime default NULL,
  TimesSent int(10) NOT NULL default '0',
  PRIMARY KEY  (ReminderID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Add a table CALENDAR_CONFIG for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_CONFIG (
  Setting varchar(50) NOT NULL default '',
  Value varchar(100) default NULL,
  PRIMARY KEY  (Setting)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Add a table CALENDAR_USER_PREF for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_USER_PREF (
  UserID int(8) NOT NULL default '0',
  Setting varchar(50) NOT NULL default '',
  Value varchar(100) default NULL,
  KEY UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"Alter table RC_REPORT_TEMPLATE for eReportCard",
"alter table RC_REPORT_TEMPLATE add column NoHeader int(2) default -1;"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"alter the field PaymentAmount in INTRANET_ENROL_GROUPINFO",
"ALTER TABLE INTRANET_ENROL_GROUPINFO CHANGE PaymentAmount PaymentAmount float default 0;"
);

$sql_eClassIP_update[] = array(
"2007-09-14",
"alter the field PaymentAmount in INTRANET_ENROL_EVENTINFO",
"ALTER TABLE INTRANET_ENROL_EVENTINFO CHANGE PaymentAmount PaymentAmount float default 0;"
);

$sql_eClassIP_update[] = array
(
"2007-10-12",
"add powervoice for announcement",
"ALTER TABLE INTRANET_ANNOUNCEMENT ADD COLUMN VoiceFile VARCHAR(255);"
);

$sql_eClassIP_update[] = array
(
"2007-10-24",
"add line height for eReportCard",
"alter table RC_REPORT_TEMPLATE add column LineHeight int(2) default 20;"
);

$sql_eClassIP_update[] = array
(
"2007-10-24",
"add Signature Width for eReportCard",
"alter table RC_REPORT_TEMPLATE add column SignatureWidth int(3) default 120;"
);

$sql_eClassIP_update[] = array
(
"2007-10-24",
"add AllowClassTeacherUploadCSV option for eReportCard",
"alter table RC_MARKSHEET_COLLECTION add column AllowClassTeacherUploadCSV tinyint default 0;"
);

$sql_eClassIP_update[] = array(
"2007-11-06",
"Add a table CALENDAR_EVENT_USER for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_EVENT_USER (
  EventID int(8) NOT NULL default '0',
  UserID int(8) NOT NULL default '0',
  GroupID int(8) default NULL,
  GroupType char(1) default NULL,
  Status char(1) default 'A',
  Access char(1) default 'R',
  KEY EventID (EventID),
  KEY UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2007-11-07",
"add IssueDate option for eReportCard",
"alter table RC_REPORT_TEMPLATE add column IssueDate date;"
);

$sql_eClassIP_update[] = array
(
"2007-11-07",
"add AcademicYear option for eReportCard",
"alter table RC_REPORT_TEMPLATE add column AcademicYear varchar(4);"
);


$sql_eClassIP_update[] = array
(
"2007-11-21",
"add FileType for eCommunity (File format)",
"alter table INTRANET_FILE add FileType char(2);"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"add PublicStatus for eCommunity (File)",
"alter table INTRANET_FILE add PublicStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"add FolderID for eCommunity (File)",
"alter table INTRANET_FILE add FolderID int(8);"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"add CoverImg for eCommunity (File)",
"alter table INTRANET_FILE add CoverImg tinyint(1);"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"add UserTitle for eCommunity (File)",
"alter table INTRANET_FILE add UserTitle varchar(255);"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"add ComnentFlag for eCommunity (File)",
"alter table INTRANET_FILE add ComnentFlag tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"Add a table INTRANET_FILE_COMMENT for eCommunity (File comment)",
"CREATE TABLE IF NOT EXISTS INTRANET_FILE_COMMENT
(
	CommentID INT(8) NOT NULL AUTO_INCREMENT,
	FileID INT(8) NOT NULL,
	UserID INT(8) NOT NULL,
	UserName varchar(255) NOT NULL,
	Comment text,
	RecordType char(2),
	RecordStatus char(2),
	DateInput datetime,
	PRIMARY KEY (CommentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"Add a table INTRANET_FILE_FOLDER for eCommunity (Folder)",
"CREATE TABLE IF NOT EXISTS INTRANET_FILE_FOLDER
(
	FolderID INT(8) NOT NULL AUTO_INCREMENT,
	GroupID INT(8) NOT NULL,
	FolderType char(2) NOT NULL,
	UserID INT(8) NOT NULL,
	UserName varchar(255) NOT NULL,
	Title varchar(255),
	PublicStatus tinyint(1) default 1,
	RecordType char(2),
	RecordStatus char(2),
	DateInput datetime,
	PRIMARY KEY (FolderID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"add PublicStatus for eCommunity (Forum)",
"alter table INTRANET_BULLETIN add PublicStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2007-11-21",
"add Attachment for eCommunity (Forum)",
"alter table INTRANET_BULLETIN add Attachment varchar(255);"
);

$sql_eClassIP_update[] = array
(
"2007-11-22",
"add Attachment size for eCommunity (Forum)",
"alter table INTRANET_BULLETIN add AttSize int(11);"
);

$sql_eClassIP_update[] = array
(
"2007-11-23",
"add status Public/Private to eCommunity Group",
"alter table INTRANET_GROUP add PublicStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2007-11-23",
"add Group Logo to eCommunity Group",
"alter table INTRANET_GROUP add GroupLogoLink varchar(255);"
);

$sql_eClassIP_update[] = array(
"2007-11-23",
"add table for HKU medical research",
"CREATE TABLE IF NOT EXISTS SPECIAL_STUDENT_ABSENCE_MEDICAL_REASON (
 RecordID int(11) NOT NULL auto_increment,
 StudentID int(11),
 RecordDate date,
 DayType int,
 MedicalReasonType int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 UNIQUE StudentDateDayType (StudentID, RecordDate, DayType),
 INDEX RecordDate (RecordDate)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2007-11-26",
"add Public Status to eCommunity Announcement",
"alter table INTRANET_ANNOUNCEMENT add PublicStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2007-11-27",
"add announcement number in index page to eCommunity",
"alter table INTRANET_GROUP add IndexAnnounceNo int(11) default 5;"
);

$sql_eClassIP_update[] = array
(
"2007-11-29",
"add Approved status for eCommunity (File)",
"alter table INTRANET_FILE add Approved tinyint(1) default 0;"
);

$sql_eClassIP_update[] = array
(
"2007-11-29",
"add ReplyFlag for eCommunity (Forum)",
"alter table INTRANET_BULLETIN add ReplyFlag tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2007-11-29",
"add onTop for eCommunity (announcement)",
"alter table INTRANET_ANNOUNCEMENT add onTop tinyint(1) default 0;"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_CATEGORY for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_CATEGORY (
 CategoryID int(11) NOT NULL auto_increment,
 Code char(10),
 NameChi varchar(255),
 NameEng varchar(255),
 DisplayOrder int(11),
 PhotoLink varchar(255),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (CategoryID),
 UNIQUE CategoryName (NameEng),
 UNIQUE CategoryCode (Code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_CATEGORY_LEVEL2 for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_CATEGORY_LEVEL2 (
 Category2ID int(11) NOT NULL auto_increment,
 CategoryID int(11) NOT NULL,
 Code char(10),
 NameChi varchar(255),
 NameEng varchar(255),
 DisplayOrder int(11),
 PhotoLink varchar(255),
 HasSoftwareLicenseModel tinyint,
 HasWarrantyExpiryDate tinyint,
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (Category2ID),
 INDEX ByCategoryID (CategoryID),
 UNIQUE CategoryIDWithCode (CategoryID,Code),
 UNIQUE CategoryIDWithNameChi (CategoryID, NameChi),
 UNIQUE CategoryIDWithNameEng (CategoryID, NameEng)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_LOCATION_LEVEL for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION_LEVEL (
 LocationLevelID int(11) NOT NULL auto_increment,
 NameChi varchar(255),
 NameEng varchar(255),
 DisplayOrder int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (LocationLevelID),
 UNIQUE ChineseName (NameChi),
 UNIQUE EnglishName (NameEng)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_LOCATION for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION (
 LocationID int(11) NOT NULL auto_increment,
 LocationLevelID int(11),
 NameChi varchar(255),
 NameEng varchar(255),
 DisplayOrder int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (LocationID),
 INDEX ByLocationLevelID (LocationLevelID),
 UNIQUE ChineseName (NameChi),
 UNIQUE EnglishName (NameEng)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ADMIN_GROUP for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ADMIN_GROUP (
 AdminGroupID int(11) NOT NULL auto_increment,
 NameChi varchar(255),
 NameEng varchar(255),
 IntranetGroupID int(11),
 DisplayOrder int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (AdminGroupID),
 UNIQUE ChineseName (NameChi),
 UNIQUE EnglishName (NameEng)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ADMIN_GROUP_MEMBER for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ADMIN_GROUP_MEMBER (
 UserID int(11),
 AdminGroupID int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 UNIQUE UserInAdminGroup (UserID, AdminGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ITEM for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM (
 ItemID int(11) NOT NULL auto_increment,
 ItemType int(11),
 CategoryID int(11),
 Category2ID int(11),
 NameChi varchar(255),
 NameEng varchar(255),
 DescriptionChi text,
 DescriptionEng text,
 ItemCode varchar(10),
 Ownership int(11),
 PhotoLink varchar(255),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (ItemID),
 INDEX InventoryItemType (ItemType),
 INDEX InventoryCategoryID (CategoryID),
 INDEX InventoryCategory2ID (Category2ID),
 UNIQUE InventoryItemCode (ItemCode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ITEM_SINGLE_EXT for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_SINGLE_EXT (
 ItemID int(11),
 PurchaseDate date,
 PurchasedPrice float,
 SupplierName varchar(255),
 SupplierContact text,
 SupplierDescription text,
 InvoiceNo varchar(255),
 QuotationNo varchar(255),
 TenderNo varchar(255),
 TagCode varchar(100),
 Brand varchar(100),
 GroupInCharge int(11),
 LocationID int(11),
 FundingSource int(11),
 WarrantyExpiryDate date,
 SoftwareLicenseModel varchar(100),
 UNIQUE SingleItemID (ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ITEM_BULK_EXT for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_BULK_EXT (
 ItemID int(11),
 Quantity int(11),
 UNIQUE BulkItemID (ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ITEM_ATTACHMENT_PART for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_ATTACHMENT_PART (
 ItemID int(11),
 AttachmentPath varchar(255),
 Filename varchar(255),
 DateInput datetime,
 DateModified datetime,
 INDEX AttachmentItemID (ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ITEM_BULK_LOCATION for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_BULK_LOCATION (
 RecordID int(11) NOT NULL auto_increment,
 ItemID int(11),
 GroupInCharge int(11),
 LocationID int(11),
 Quantity int(11),
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ITEM_BULK_LOG for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_BULK_LOG (
 RecordID int(11) NOT NULL auto_increment,
 ItemID int(11),
 RecordDate date,
 Action int(11),
 QtyChange int(11),
 PurchaseDate date,
 PurchasedPrice float,
 FundingSource int(11),
 SupplierName varchar(255),
 SupplierContact text,
 SupplierDescription text,
 InvoiceNo varchar(255),
 QuotationNo varchar(255),
 TenderNo varchar(255),
 PersonInCharge int(11),
 Remark text,
 LocationID int(11),
 StockCheckQty int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX BulkLogItemID (ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_ITEM_SINGLE_STATUS_LOG for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_SINGLE_STATUS_LOG (
 RecordID int(11) NOT NULL auto_increment,
 ItemID int(11),
 RecordDate date,
 Action int(11),
 PastStatus int(11),
 NewStatus int(11),
 PersonInCharge int(11),
 Remark text,
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX SignleLogItemID (ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_FUNDING_SOURCE for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_FUNDING_SOURCE (
 FundingSourceID int NOT NULL auto_increment,
 NameChi varchar(255),
 NameEng varchar(255),
 DisplayOrder int(11),
 RecordType int(11),
 RecordStatus int(11),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (FundingSourceID),
 INDEX ByFundingSourceID (FundingSourceID),
 UNIQUE ChineseName (NameChi),
 UNIQUE EnglishName (NameEng)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-04",
"add table INVENTORY_PHOTO_PART for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_PHOTO_PART (
 PartID int(11) NOT NULL auto_increment,
 CategoryID int(11),
 Category2ID int(11),
 ItemID int(11),
 PhotoPath varchar(255),
 PhotoName varchar(255),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (PartID),
 INDEX Cat_ID (CategoryID),
 INDEX Cat2_ID (Category2ID),
 INDEX Item_ID (ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2007-12-06",
"alter table RC_REPORT_TEMPLATE_COLUMN for eReportCard",
"ALTER TABLE RC_REPORT_TEMPLATE_COLUMN add IsColumnSum tinyint(1) default 0;"
);

$sql_eClassIP_update[] = array(
"2007-12-06",
"alter table RC_REPORT_TEMPLATE_COLUMN for eReportCard",
"ALTER TABLE RC_REPORT_TEMPLATE_COLUMN add MarkSummaryWeight int(2) default NULL;"
);

$sql_eClassIP_update[] = array(
"2007-12-06",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN GroupInCharge int(11) AFTER LocationID;"
);

$sql_eClassIP_update[] = array
(
"2007-12-06",
"add onTop for eCommunity Forum",
"alter table INTRANET_BULLETIN add onTop tinyint(1) default 0;"
);
$sql_eClassIP_update[] = array
(
"2007-12-07",
"add ForumAttStatus number in index page to eCommunity",
"alter table INTRANET_GROUP add ForumAttStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2007-12-14",
"add DecimalPoint for eReportCard",
"alter table RC_REPORT_TEMPLATE add column DecimalPoint int(1) default 2;"
);

$sql_eClassIP_update[] = array
(
"2007-12-18",
"add Set ID to DISCIPLINE_ACCU_PERIOD",
"alter table DISCIPLINE_ACCU_PERIOD add SetID int(11) default 0;"
);

$sql_eClassIP_update[] = array
(
"2007-12-18",
"create table DISCIPLINE_ACCU_CATEGORY_PERIOD (for Accumulative Misconduct)",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_ACCU_CATEGORY_PERIOD (
	ID INT(8) NOT NULL AUTO_INCREMENT,
	CategoryID INT(11) NOT NULL,
	SetID INT(11) NOT NULL,
	RecordType int(11),
	RecordStatus int(11),
	DateInput datetime,
	PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2007-12-19",
"add onTop for eCommunity (folder)",
"alter table INTRANET_FILE_FOLDER add onTop tinyint(1) default 0;"
);

$sql_eClassIP_update[] = array
(
"2007-12-19",
"add onTop for eCommunity (file)",
"alter table INTRANET_FILE add onTop tinyint(1) default 0;"
);

$sql_eClassIP_update[] = array
(
"2007-12-19",
"add latest reocrd number to eCommunity (sharing)",
"alter table INTRANET_GROUP add SharingLatestNo int(11) default 10;"
);

$sql_eClassIP_update[] = array
(
"2007-12-20",
"add ShareToAll field to iCalendar for adding share to everyone feature",
"alter table CALENDAR_CALENDAR add column ShareToAll char(1) not null default 0 after description;"
);

$sql_eClassIP_update[] = array
(
"2008-01-02",
"add approval field to eCommunity (sharing)",
"alter table INTRANET_GROUP add needApproval tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2008-01-03",
"add default view mode setting to eCommunity (sharing)",
"alter table INTRANET_GROUP add DefaultViewMode tinyint(1) default 2;"
);

$sql_eClassIP_update[] = array
(
"2008-01-03",
"add Allow image type setting to eCommunity (sharing)",
"alter table INTRANET_GROUP add AllowedImageTypes varchar(255) default 'ALL';"
);

$sql_eClassIP_update[] = array
(
"2008-01-03",
"add Allow file type setting to eCommunity (sharing)",
"alter table INTRANET_GROUP add AllowedFileTypes varchar(255) default 'ALL';"
);

$sql_eClassIP_update[] = array
(
"2008-01-03",
"add Allow video file type setting to eCommunity (sharing)",
"alter table INTRANET_GROUP add AllowedVideoTypes varchar(255) default 'ALL';"
);

$sql_eClassIP_update[] = array
(
"2008-01-09",
"add calendar ID to eCommunity group",
"alter table INTRANET_GROUP add CalID int(8);"
);

$sql_eClassIP_update[] = array
(
"2008-01-09",
"add calendar setting to eCommunity group",
"alter table INTRANET_GROUP add CalDisplayIndex tinyint(1) default 1, add CalPublicStatus tinyint(1) default 1;"
);

$sql_eClassIP_update[] = array
(
"2008-01-09",
"add organization to student activity record",
"alter table PROFILE_STUDENT_ACTIVITY add column Organization varchar(200) AFTER PERFORMANCE;"
);

$sql_eClassIP_update[] = array
(
"2008-01-09",
"add organization to student award record",
"alter table PROFILE_STUDENT_AWARD add column Organization varchar(200) AFTER Remark;"
);

$sql_eClassIP_update[] = array
(
"2008-01-09",
"add organization to student service record",
"alter table PROFILE_STUDENT_SERVICE add column Organization varchar(200) AFTER PERFORMANCE;"
);


$sql_eClassIP_update[] = array
(
"2008-01-10",
"change the default value of IsImportant field to 0 of CALENDAR_EVENT_ENTRY",
"ALTER TABLE CALENDAR_EVENT_ENTRY CHANGE COLUMN IsImportant IsImportant char(1) DEFAULT '0';"
);

$sql_eClassIP_update[] = array
(
"2008-01-21",
"add archieve table for enrolment",
"
CREATE TABLE IF NOT EXISTS INTRANET_ARCHIVE_ENROL_GROUPINFO
(
  EnrolGroupID int(11) NOT NULL auto_increment,
  GroupID int(11) NOT NULL default '0',
  Quota int(11) default NULL,
  Approved int(11) default '0',
  Description text,
  DateInput datetime default NULL,
  DateModified datetime default NULL,
  UpperAge int(2) default NULL,
  LowerAge int(2) default NULL,
  Gender char(2) default NULL,
  AttachmentLink1 varchar(255) default NULL,
  AttachmentLink2 varchar(255) default NULL,
  AttachmentLink3 varchar(255) default NULL,
  AttachmentLink4 varchar(255) default NULL,
  AttachmentLink5 varchar(255) default NULL,
  AddToPayment int(2) default '0',
  GroupCategory int(11) default NULL,
  PaymentAmount float default '0',
  PRIMARY KEY  (EnrolGroupID),
  UNIQUE KEY GroupID (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2008-01-22",
"add subject area to student award record",
"alter table PROFILE_STUDENT_AWARD add column SubjectArea varchar(200) AFTER Organization;"
);

$sql_eClassIP_update[] = array
(
"2008-01-24",
"Record details for Copier Quota Transaction content (paper size and num)",
"CREATE TABLE IF NOT EXISTS PAYMENT_COPIER_TRANSACTION_DETAIL (
 RecordID int NOT NULL auto_increment,
 TransactionLogID int(11) NOT NULL,
 PaperType varchar(40),
 NumberOfPaper int,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX TransactionLogID (TransactionLogID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2008-01-24",
"Record for Copier Quota Transaction",
"CREATE TABLE IF NOT EXISTS PAYMENT_PRINTING_QUOTA_CHANGE_LOG (
 RecordID int NOT NULL auto_increment,
 UserID int NOT NULL,
 QuotaChange int default 0,
 QuotaAfter int default 0,
 RecordType int,
 RecordStatus int,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID),
 INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array
(
"2008-02-05",
"SMS reply messages table",
"CREATE TABLE IF NOT EXISTS INTRANET_SMS2_REPLY_MESSAGE 
(
    ReplyID int(11) NOT NULL auto_increment,
    MessageID int(11) default NULL,
    MessageCode int(11) default NULL,
    ReplyMessage varchar(255) default NULL,
    ReplyPhoneNumber varchar(100) default NULL,
    RelatedUserID int(11) default NULL,
    RecordType int(11) default NULL,
    RecordStatus int(11) default NULL,
    DateInput datetime default NULL,
    DateModified datetime default NULL,
    PRIMARY KEY (ReplyID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-02-14",
"alter table RC_REPORT_TEMPLATE_COLUMN for eReportCard",
"ALTER TABLE RC_REPORT_TEMPLATE_COLUMN add NotForInput tinyint(1) default 0;"
);


$sql_eClassIP_update[] = array(
"2008-02-21",
"eDiscipline Case Report table",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_CASE_REPORT (
  RecordID int(11) NOT NULL auto_increment,
  CaseType int(11) default NULL,
  RefNo varchar(10) default NULL,
  RecordDate date default NULL,
  RecordTime varchar(5) default NULL,
  StudentID int(11) default NULL,
  ClassName varchar(20) default NULL,
  ClassNumber varchar(20) default NULL,
  UserID int(11) default NULL,
  Reasons text,
  OthersReason text,
  DatesTimes text,
  Details text,
  Punishment text,
  RecordInBlackBook tinyint(1) default NULL,
  Remark text,
  Problems text,
  OthersProblem text,
  ActionTaken text,
  ActionFields text,
  OtherAction text,
  Venue text,
  VenueFields text,
  OtherVenue text,
  StudentsInvolved text,
  TeachersInvolved text,
  CaseHandledby text,
  FollowUp text,
  DateInput datetime default NULL,
  DateModified datetime default NULL,
  PRIMARY KEY  (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-02-28",
"insert default system setting into CALENDAR_CONFIG",
"INSERT IGNORE INTO CALENDAR_CONFIG VALUES 
	('PreferredView', 'monthly'),
	('TimeFormat', '12'),
	('WorkingHoursStart', '6'),
	('WorkingHoursEnd', '22'),
	('DisableRepeat', 'no'),
	('DisableGuest', 'no')
	;"
);

###### added on 26 Mar 2008 by Ronald ######
$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN UnitPrice float AFTER PurchasedPrice;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN QtyNormal int(11) AFTER QtyChange;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN QtyDamage int(11) AFTER QtyNormal;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN QtyRepair int(11) AFTER QtyDamage;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM for eInventory",
"ALTER TABLE INVENTORY_ITEM MODIFY ItemCode varchar(25);"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN UserRemark text AFTER Remark;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ADMIN_GROUP for eInventory",
"ALTER TABLE INVENTORY_ADMIN_GROUP ADD COLUMN Code varchar(10) AFTER AdminGroupID, ADD UNIQUE AdminGroupIDWithCode (AdminGroupID, Code);"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_FUNDING_SOURCE for eInventory",
"ALTER TABLE INVENTORY_FUNDING_SOURCE ADD COLUMN Code varchar(10) AFTER FundingSourceID, ADD UNIQUE FundingSourceIDWithCode (FundingSourceID, Code);"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_LOCATION_LEVEL for eInventory",
"ALTER TABLE INVENTORY_LOCATION_LEVEL ADD COLUMN Code varchar(10) AFTER LocationLevelID, ADD UNIQUE LocationLevelIDWithCode (LocationLevelID, Code);"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_LOCATION for eInventory",
"ALTER TABLE INVENTORY_LOCATION ADD COLUMN Code varchar(10) AFTER LocationLevelID, ADD UNIQUE LocationIDWithCode (LocationID, Code);"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"create table INVENTORY_WRITEOFF_REASON_TYPE for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_WRITEOFF_REASON_TYPE (
  ReasonTypeID int(11) NOT NULL auto_increment, 
  ReasonTypeNameEng varchar(255) default NULL, 
  ReasonTypeNameChi varchar(255) default NULL, 
  DisplayOrder int(11) default NULL, 
  RecordType int(11) default NULL, 
  RecordStatus int(11) default NULL, 
  DateInput datetime default NULL, 
  DateModified datetime default NULL, 
  PRIMARY KEY (ReasonTypeID))  ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"create table INVENTORY_ITEM_WRITE_OFF_RECORD for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_WRITE_OFF_RECORD (
  RecordID int(11) NOT NULL auto_increment, 
  ItemID int(11), 
  RecordDate date, 
  LocationID int(11), 
  AdminGroupID int(11), 
  WriteOffQty int(11), 
  WriteOffReason varchar(255), 
  RequestDate date, 
  RequestPerson int(11), 
  ApproveDate date, 
  ApprovePerson int(11), 
  RecordType int(11), 
  RecordStatus int(11), 
  DateInput datetime, 
  DateModified datetime, 
  PRIMARY KEY (RecordID)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_SINGLE_EXT for eInventory",
"ALTER TABLE INVENTORY_ITEM_SINGLE_EXT ADD COLUMN MaintainInfo text AFTER SupplierDescription;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_SINGLE_EXT for eInventory",
"ALTER TABLE INVENTORY_ITEM_SINGLE_EXT ADD COLUMN UnitPrice text AFTER PurchasedPrice;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN MaintainInfo text AFTER SupplierDescription;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_LOG for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG ADD COLUMN UnitPrice float AFTER PurchasedPrice, ADD COLUMN MaintainInfo text AFTER SupplierDescription;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"create table INVENTORY_ITEM_BULK_MISSING_RECORD for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_BULK_MISSING_RECORD (
  RecordID int(11) NOT NULL auto_increment, 
  ItemID int(11), 
  LocationID int(11), 
  AdminGroupID int(11), 
  RecordDate date, 
  MissingQty int(11), 
  PersonInCharge int(11), 
  RecordType int(11), 
  RecordStatus int(11), 
  DateInput datetime, 
  DateModified datetime, 
  PRIMARY KEY (RecordID)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"create table INVENTORY_ITEM_BULK_SURPLUS_RECORD for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_BULK_SURPLUS_RECORD (
  RecordID int(11) NOT NULL auto_increment, 
  ItemID int(11), 
  LocationID int(11), 
  AdminGroupID int(11), 
  RecordDate date, 
  SurplusQty int(11), 
  PersonInCharge int(11), 
  RecordType int(11), 
  RecordStatus int(11), 
  DateInput datetime, 
  DateModified datetime, 
  PRIMARY KEY (RecordID)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"create table INVENTORY_ITEM_WRITE_OFF_ATTACHMENT for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_ITEM_WRITE_OFF_ATTACHMENT (
  AttachmentID int(11) NOT NULL auto_increment, 
  ItemID int(11), 
  RequestWriteOffID int(11), 
  PhotoPath varchar(255), 
  PhotoName varchar(255), 
  DateInput datetime, 
  DateModified datetime, 
  PRIMARY KEY (AttachmentID)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_MISSING_RECORD for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_MISSING_RECORD RENAME INVENTORY_ITEM_MISSING_RECORD;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_BULK_SURPLUS_RECORD for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_SURPLUS_RECORD RENAME INVENTORY_ITEM_SURPLUS_RECORD;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_FUNDING_SOURCE for eInventory",
"ALTER TABLE INVENTORY_FUNDING_SOURCE ADD FundingType int(11) AFTER FundingSourceID;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"create table INVENTORY_VARIANCE_HANDLING_REMINDER for eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_VARIANCE_HANDLING_REMINDER
 (ReminderID int(11) NOT NULL auto_increment,
  HandlerID int(11) NOT NULL,
  ItemID int(11) NOT NULL,
  Quantity int(11),
  OriginalLocationID int(11),
  NewLocationID int(11),
  SendDate datetime,
  FinishDate datetime,
  RecordType int(11),
  RecordStatus int(11),
  DateInput datetime,
  DateModified datetime,
  PRIMARY KEY (ReminderID),
  INDEX ByUserID (HandlerID)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"alter table INVENTORY_ITEM_SINGLE_EXT for eInventory",
"ALTER TABLE INVENTORY_ITEM_SINGLE_EXT ADD COLUMN ItemRemark text after UnitPrice;"
);

$sql_eClassIP_update[] = array(
"2008-03-26",
"create table PPC eDiscipline",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_MERIT_ITEM_NOTICE (
	NoticeID int(11) NOT NULL auto_increment,
	MeritID int(11) default NULL,
	RecordType int(11) default NULL,
	StudentID int(11) default NULL,
	PICID int(11) default NULL,
	CONTENT longtext,
	AttachList longtext,
	DateInput datetime default NULL,
	DatePrint datetime default NULL,
	PRIMARY KEY (NoticeID),
	KEY MeritID (MeritID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2008-04-14",
"alter table INVENTORY_ITEM_BULK_EXT for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_EXT ADD COLUMN BulkItemAdmin int(11) after Quantity;"
);

$sql_eClassIP_update[] = array(
"2008-04-14",
"alter table INVENTORY_VARIANCE_HANDLING_REMINDER for eInventory",
"ALTER TABLE INVENTORY_VARIANCE_HANDLING_REMINDER ADD COLUMN SenderID int(11) after HandlerID;"
);

$sql_eClassIP_update[] = array(
"2008-04-18",
"Create table for group attendance 1",
"CREATE TABLE IF NOT EXISTS `CARD_STUDENT_ATTENDANCE_GROUP` (
  `GroupID` int(11) NOT NULL default '0',
  `Mode` int(11) default '0',
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);


$sql_eClassIP_update[] = array(
"2008-04-18",
"Create table for group attendance 2",
"CREATE TABLE IF NOT EXISTS `CARD_STUDENT_GROUP_PERIOD_TIME` (
  `RecordID` int(11) NOT NULL auto_increment,
  `GroupID` int(11) NOT NULL default '0',
  `DayType` int(11) default NULL,
  `DayValue` char(5) default NULL,
  `MorningTime` time default NULL,
  `LunchStart` time default NULL,
  `LunchEnd` time default NULL,
  `LeaveSchoolTime` time default NULL,
  `NonSchoolDay` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`RecordID`),
  UNIQUE KEY `GroupTypeValue` (`GroupID`,`DayType`,`DayValue`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2008-04-18",
"Create table for group attendance 3",
"CREATE TABLE IF NOT EXISTS `CARD_STUDENT_SPECIFIC_DATE_TIME_GROUP` (
  `RecordID` int(11) NOT NULL auto_increment,
  `RecordDate` date default NULL,
  `GroupID` int(11) NOT NULL default '0',
  `MorningTime` time default NULL,
  `LunchStart` time default NULL,
  `LunchEnd` time default NULL,
  `LeaveSchoolTime` time default NULL,
  `NonSchoolDay` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`RecordID`),
  UNIQUE KEY `DateGroup` (`RecordDate`,`GroupID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2008-04-18",
"Create table for group attendance 4",
"CREATE TABLE IF NOT EXISTS `CARD_STUDENT_TIME_SESSION_DATE_GROUP` (
  `RecordID` int(11) NOT NULL auto_increment,
  `GroupID` int(11) NOT NULL default '0',
  `RecordDate` date default NULL,
  `SessionID` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`RecordID`),
  UNIQUE KEY `DateGroup` (`RecordDate`,`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);


$sql_eClassIP_update[] = array(
"2008-04-18",
"Create table for group attendance 5",
"CREATE TABLE IF NOT EXISTS `CARD_STUDENT_TIME_SESSION_REGULAR_GROUP` (
  `RecordID` int(11) NOT NULL auto_increment,
  `GroupID` int(11) NOT NULL default '0',
  `DayType` int(11) default NULL,
  `DayValue` varchar(50) default NULL,
  `SessionID` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`RecordID`),
  UNIQUE KEY `GroupDayTypeValue` (`GroupID`,`DayType`,`DayValue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);


$sql_eClassIP_update[] = array(
"2008-04-25",
"Alter table INVENTORY_CATEGORY_LEVEL2 for eInventory",
"ALTER TABLE INVENTORY_CATEGORY_LEVEL2 ADD COLUMN HasSerialNumber tinyint(4) AFTER HasWarrantyExpiryDate;"
);

$sql_eClassIP_update[] = array(
"2008-04-25",
"Alter table INVENTORY_ITEM_SINGLE_EXT for eInventory",
"ALTER TABLE INVENTORY_ITEM_SINGLE_EXT ADD COLUMN SerialNumber varchar(100) AFTER SoftwareLicenseModel;"
);

$sql_eClassIP_update[] = array(
"2008-05-06",
"Add a table CALENDAR_COURSE for iCalendar",
"CREATE TABLE IF NOT EXISTS `CALENDAR_COURSE` (
  `CourseID` int(8) NOT NULL auto_increment,
  `CourseCode` varchar(10) default NULL,
  `CourseName` varchar(100) default NULL,
  `CourseDesc` text,
  `Institution` varchar(100) default NULL,
  `IsGuest` varchar(10) default NULL,
  `Language` varchar(10) default 'chib5',
  `NoUsers` int(4) default NULL,
  `RightsStudent` text,
  `RightsAssistant` text,
  `RightsGuest` text,
  `InputDate` datetime default NULL,
  `Modified` datetime default NULL,
  `MaxUser` int(4) default NULL,
  `MaxStorage` int(4) default NULL,
  `Settings` text,
  `CreatorID` int(8) default NULL,
  `RoomType` varchar(8) default '0',
  `ReferenceID` varchar(32) default NULL,
  `StartDate` date default NULL,
  `EndDate` date default NULL,
  `Remark` varchar(255) default NULL,
  `IsParent` tinyint(1) default '0',
  PRIMARY KEY  (`CourseID`),
  KEY `CourseID` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-06",
"Add a table CALENDAR_USER_COURSE for iCalendar",
"CREATE TABLE IF NOT EXISTS `CALENDAR_USER_COURSE` (
  `UserCourseID` int(8) NOT NULL auto_increment,
  `CourseID` int(8) default NULL,
  `UserID` int(8) default NULL,
  PRIMARY KEY  (`UserCourseID`),
  KEY `CourseID` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

#####################   eLibrary     ########################
$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK
(
	BookID int(11) NOT NULL auto_increment,
	Title varchar(255),
	Author varchar(50),
	AuthorID int(11),
	Publisher varchar(100),
	Preface text,
	Document varchar(50),
	InputBy varchar(10),
	DateModified datetime,
	Source varchar(100),
	Language varchar(5),
	Category varchar(100),
	Level varchar(5),
	PageTitle varchar(50),
	SubCategory varchar(100),
	SeriesEditor varchar(50),
	HitRate int(11),
	Publish char(2),
	ISBN varchar(100),
	AdultContent varchar(5),
	Copyright text,
	primary key(BookID),
	KEY AuthorID (AuthorID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_AUTHOR for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_AUTHOR
(
	AuthorID int(11) NOT NULL auto_increment,
	Author varchar(50),
	Description text,
	InputBy varchar(10),
	DateModified datetime,	
	primary key(AuthorID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_CHAPTER for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_CHAPTER
(
	BookChapterID int(11) NOT NULL auto_increment,
	BookID int(11),
	ChapterID int(11),
	SubChapterID int(11),
	Title varchar(255),
	OrigPageID int(11),
	IsChapter char,
	DateModified datetime,
	primary key(BookChapterID),
	KEY BookID (BookID),
	KEY ChapterID (ChapterID),
	KEY SubChapterID (SubChapterID),
	KEY OrigPageID (OrigPageID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_HISTORY for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_HISTORY 
(
    HistoryID int(11) NOT NULL auto_increment,
    UserID int(11),
    BookID int(11),
    DateModified datetime,
    primary key(HistoryID),
    KEY UserID (UserID),
    KEY BookID (BookID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_MY_FAVOURITES for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_MY_FAVOURITES 
(
 	FavouriteID int(11) NOT NULL auto_increment,
 	UserID int(11),
 	BookID int(11),
 	DateModified datetime,
 	primary key(FavouriteID),
 	KEY UserID (UserID),
    KEY BookID (BookID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_PAGE for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_PAGE
(
	BookPageID int(11) NOT NULL auto_increment,
	BookID int(11),
	ChapterID int(11),
	IsChapterStart char,	
	PageID int(11), 
	PageType varchar(5), 
	OrigPageID int(11),
	Content text,
	ContentXML text,
	DateModified datetime,
	primary key(BookPageID),
	KEY ChapterID (ChapterID),
    KEY BookID (BookID),
    KEY PageID (PageID),
    KEY OrigPageID (OrigPageID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_PARAGRAPH for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_PARAGRAPH
(
	BookParagraphID int(11) NOT NULL auto_increment,
	BookID int(11),
	ChapterID int(11),
	ParagraphID int(11),
	Content text,
	ContentXML text,
	DateModified datetime,
	primary key(BookParagraphID),
	KEY ChapterID (ChapterID),
    KEY BookID (BookID),
    KEY ParagraphID (ParagraphID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_RECOMMEND for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_RECOMMEND 
(
    RecommendID int(11) NOT NULL auto_increment,
    UserID int(11),
    BookID int(11),
    RecommendOrder int(11),
    RecommendGroup varchar(100),
    Description TEXT,
    DateModified datetime,
    primary key(RecommendID),
    KEY UserID (UserID),
    KEY BookID (BookID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_REVIEW for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_REVIEW
(
	ReviewID int(11) NOT NULL auto_increment,
	BookID int(11),
	UserID int(11),
	PageID int(11),
	Rating int(11),
	Content text,
	DateModified datetime,
	PRIMARY KEY (ReviewID),
	KEY UserID (UserID),
    KEY BookID (BookID),
    KEY PageID (PageID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_REVIEW_HELPFUL for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_REVIEW_HELPFUL
(
	ReviewCommentID int(11) NOT NULL auto_increment,
	ReviewID int(11),
	BookID int(11),
	UserID int(11),
	PageID int(11),
	Choose char,
	DateModified datetime,
	PRIMARY KEY (ReviewCommentID),
	KEY UserID (UserID),
    KEY BookID (BookID),
    KEY PageID (PageID),
    KEY ReviewID (ReviewID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_BOOK_SETTINGS for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_BOOK_SETTINGS
(
 	SettingID int(11) NOT NULL auto_increment,
 	UserID int(11),
 	DisplayReviewer int(3) NOT NULL Default 10,
 	DisplayReview int(3) NOT NULL Default 10,
 	DisplayRecommendBook int(3) NOT NULL Default 4,
 	DisplayWeeklyHitBook int(3) NOT NULL Default 1,
 	DisplayHitBook int(3) NOT NULL Default 9,
 	PRIMARY KEY (SettingID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_USER_BOOKMARK for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_USER_BOOKMARK 
(
  BookmarkID int(11) NOT NULL auto_increment,
  BookID int(11),
  UserID int(11),
  PageID int(11),
  DateModified datetime,
  PRIMARY KEY (BookmarkID),
  KEY UserID (UserID),
  KEY BookID (BookID),
  KEY PageID (PageID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_USER_FORMAT for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_USER_FORMAT 
(
  FormatID int(11) NOT NULL auto_increment,
  BookID int(11),
  UserID int(11),
  PageID int(11),
  StartIndex int(11),
  EndIndex int(11),
  FormatType varchar(15),
  FormatValue varchar(30),
  DateModified datetime,
  PRIMARY KEY (FormatID),
  KEY UserID (UserID),
  KEY BookID (BookID),
  KEY PageID (PageID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-05-08",
"Add a table INTRANET_ELIB_USER_MYNOTES for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_USER_MYNOTES
(
  NoteID int(11) NOT NULL auto_increment,
  BookID int(11),
  UserID int(11),
  PageID int(11),
  Content text,
  NoteType varchar(5),
  Category varchar(100),
  DateModified datetime,
  PRIMARY KEY (NoteID),
  KEY UserID (UserID),
  KEY BookID (BookID),
  KEY PageID (PageID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add column to INTRANET_ELIB_BOOK_REVIEW",
"alter table INTRANET_ELIB_BOOK_REVIEW add PageID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK - AuthorID",
"ALTER TABLE INTRANET_ELIB_BOOK ADD INDEX AuthorID (AuthorID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_PAGE - OrigPageID",
"ALTER TABLE INTRANET_ELIB_BOOK_PAGE ADD INDEX OrigPageID (OrigPageID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_PAGE - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_PAGE ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_PAGE - ChapterID",
"ALTER TABLE INTRANET_ELIB_BOOK_PAGE ADD INDEX ChapterID (ChapterID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_PAGE - PageID",
"ALTER TABLE INTRANET_ELIB_BOOK_PAGE ADD INDEX PageID (PageID);"
);


$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_PARAGRAPH - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_PARAGRAPH ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_PARAGRAPH - ChapterID",
"ALTER TABLE INTRANET_ELIB_BOOK_PARAGRAPH ADD INDEX ChapterID (ChapterID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_PARAGRAPH - ParagraphID",
"ALTER TABLE INTRANET_ELIB_BOOK_PARAGRAPH ADD INDEX ParagraphID (ParagraphID);"
);


$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_CHAPTER - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_CHAPTER - ChapterID",
"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER ADD INDEX ChapterID (ChapterID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_CHAPTER - SubChapterID",
"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER ADD INDEX SubChapterID (SubChapterID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_CHAPTER - OrigPageID",
"ALTER TABLE INTRANET_ELIB_BOOK_CHAPTER ADD INDEX OrigPageID (OrigPageID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_FORMAT - BookID",
"ALTER TABLE INTRANET_ELIB_USER_FORMAT ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_FORMAT - UserID",
"ALTER TABLE INTRANET_ELIB_USER_FORMAT ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_FORMAT - PageID",
"ALTER TABLE INTRANET_ELIB_USER_FORMAT ADD INDEX PageID (PageID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_HISTORY - UserID",
"ALTER TABLE INTRANET_ELIB_BOOK_HISTORY ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_HISTORY - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_HISTORY ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_RECOMMEND - UserID",
"ALTER TABLE INTRANET_ELIB_BOOK_RECOMMEND ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_RECOMMEND - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_RECOMMEND ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW - UserID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW - PageID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW ADD INDEX PageID (PageID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW - ReviewID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW ADD INDEX ReviewID (ReviewID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_BOOKMARK - PageID",
"ALTER TABLE INTRANET_ELIB_USER_BOOKMARK ADD INDEX PageID (PageID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_BOOKMARK - BookID",
"ALTER TABLE INTRANET_ELIB_USER_BOOKMARK ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_BOOKMARK - UserID",
"ALTER TABLE INTRANET_ELIB_USER_BOOKMARK ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_MYNOTES - UserID",
"ALTER TABLE INTRANET_ELIB_USER_MYNOTES ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_MYNOTES - BookID",
"ALTER TABLE INTRANET_ELIB_USER_MYNOTES ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_USER_MYNOTES - PageID",
"ALTER TABLE INTRANET_ELIB_USER_MYNOTES ADD INDEX PageID (PageID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_MY_FAVOURITES - UserID",
"ALTER TABLE INTRANET_ELIB_BOOK_MY_FAVOURITES ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_MY_FAVOURITES - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_MY_FAVOURITES ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW_HELPFUL - UserID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW_HELPFUL ADD INDEX UserID (UserID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW_HELPFUL - BookID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW_HELPFUL ADD INDEX BookID (BookID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW_HELPFUL - PageID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW_HELPFUL ADD INDEX PageID (PageID);"
);

$sql_eClassIP_update[] = array(
"2008-10-29",
"eLibrary - Add index to INTRANET_ELIB_BOOK_REVIEW_HELPFUL - ReviewID",
"ALTER TABLE INTRANET_ELIB_BOOK_REVIEW_HELPFUL ADD INDEX ReviewID (ReviewID);"
);

$sql_eClassIP_update[] = array(
"2008-11-04",
"Add field ISBN to INTRANET_ELIB_BOOK",
"ALTER TABLE INTRANET_ELIB_BOOK ADD ISBN varchar(100) AFTER Publish;"
);

$sql_eClassIP_update[] = array(
"2008-11-04",
"Add field AdultContent to INTRANET_ELIB_BOOK",
"ALTER TABLE INTRANET_ELIB_BOOK ADD AdultContent varchar(5) AFTER ISBN;"
);

$sql_eClassIP_update[] = array(
"2008-11-04",
"Add field Copyright to INTRANET_ELIB_BOOK",
"ALTER TABLE INTRANET_ELIB_BOOK ADD Copyright text AFTER AdultContent;"
);

########### End Elibrary ################

$sql_eClassIP_update[] = array(
"2008-05-09",
"Add field for skipping email checking to allow POP3 email client",
"ALTER TABLE INTRANET_IMAIL_PREFERENCE ADD SkipCheckEmail int AFTER DaysInTrash"
);

### eInventory ###
$sql_eClassIP_update[] = array(
"2008-05-15",
"Alter table INVENTORY_ITEM_BULK_EXT for eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_EXT ALTER BulkItemAdmin SET Default 0;"
);

$sql_eClassIP_update[] = array(
"2008-05-15",
"Alter table INVENTORY_ITEM_ATTACHMENT_PART for eInventory",
"ALTER TABLE INVENTORY_ITEM_ATTACHMENT_PART ADD COLUMN AttachmentID int(11) NOT NULL auto_increment FIRST, ADD PRIMARY KEY(AttachmentID);"
);

$sql_eClassIP_update[] = array(
"2008-05-28",
"Alter table INVENTORY_VARIANCE_HANDLING_REMINDER for eInventory",
"ALTER TABLE INVENTORY_VARIANCE_HANDLING_REMINDER ADD COLUMN OriginalQty int(11) AFTER ItemID;"
);

$sql_eClassIP_update[] = array(
"2008-07-14",
"Alter table INTRANET_USER for User Info (add HKID field)",
"ALTER TABLE INTRANET_USER ADD HKID varchar(10) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-08-18",
"Add fields for Spam filtering",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD SpamFlag char(6) AFTER Deleted, 
ADD SpamScore float AFTER SpamFlag,
ADD SpamLevel char(100) AFTER SpamScore, 
ADD SpamStatus text AFTER SpamLevel"
);

/*
$sql_eClassIP_update[] = array(
"2008-08-18",
"increase performance of iMail/Campusmail",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX UserFolderID_2 (UserFolderID) "
);

$sql_eClassIP_update[] = array(
"2008-08-18",
"increase performance of iMail/Campusmail",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX MailType_2 (MailType) "
);

$sql_eClassIP_update[] = array(
"2008-08-18",
"increase performance of iMail/Campusmail",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX CampusMailFromID_2 (CampusMailFromID) "
);

$sql_eClassIP_update[] = array(
"2008-08-18",
"increase performance of iMail/Campusmail",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX CampusMailFromID_3 (CampusMailFromID) "
);

$sql_eClassIP_update[] = array(
"2008-08-18",
"increase performance of iMail/Campusmail",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX CampusMailFromID_4 (CampusMailFromID) "
);

$sql_eClassIP_update[] = array(
"2008-08-18",
"increase performance of iMail/Campusmail",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX CampusMailFromID_5 (CampusMailFromID) "
);
*/

$sql_eClassIP_update[] = array(
"2008-08-18",
"increase performance of iMail/Campusmail",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX IsImportant (IsImportant) "
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - add OLE linkage to Club",
"alter table INTRANET_GROUP add OLE_ProgramID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - add OLE linkage to Club's member",
"alter table INTRANET_USERGROUP add OLE_STUDENT_RecordID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - add OLE linkage to Activity",
"alter table INTRANET_ENROL_EVENTINFO add OLE_ProgramID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - add OLE linkage to Activity's member",
"alter table INTRANET_ENROL_EVENTSTUDENT add OLE_STUDENT_RecordID int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - added column CommentStudent(text) in table INTRANET_USERGROUP",
"ALTER TABLE INTRANET_USERGROUP ADD CommentStudent text;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - Added column RoleTitle to store the title entered by the user",
"ALTER TABLE INTRANET_ENROL_EVENTSTUDENT ADD RoleTitle varchar(100);"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - Club Own active member attendance % value",
"alter table INTRANET_GROUP add ActiveMemberPercentage float(5,2) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - Club Active Member Field",
"alter table INTRANET_USERGROUP add isActiveMember int(1) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - Activity Own active member attendance % value",
"alter table INTRANET_ENROL_EVENTINFO add ActiveMemberPercentage float(5,2) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - Activity Active Member Field ",
"alter table INTRANET_ENROL_EVENTSTUDENT add isActiveMember int(1) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - UserID of the one who approved the student to be a member",
"alter table INTRANET_USERGROUP add ApprovedBy int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - UserID of the one who changed the status of the student (for both approval and rejection)",
"alter table INTRANET_ENROL_GROUPSTUDENT add StatusChangedBy int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-03",
"eEnrolment - UserID of the one who approved the student to join the activity",
"alter table INTRANET_ENROL_EVENTSTUDENT add ApprovedBy int(8) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-09-12",
"eEnrolment - record status",
"ALTER TABLE INTRANET_ENROL_GROUPINFO add RecordStatus char(2)"
);

$sql_eClassIP_update[] = array(
"2008-09-12",
"eEnrolment - record status",
"ALTER TABLE INTRANET_ENROL_GROUP_DATE add RecordStatus char(2)"
);

$sql_eClassIP_update[] = array(
"2008-09-12",
"eEnrolment - record status",
"ALTER TABLE INTRANET_ENROL_EVENT_DATE add RecordStatus char(2)"
);

$sql_eClassIP_update[] = array(
"2008-09-19",
"eEnrolment - maximum number of activities which a student would like to join",
"ALTER TABLE INTRANET_ENROL_STUDENT ADD MaxEvent int(11);"
);

$sql_eClassIP_update[] = array(
"2008-09-19",
"eInventory - Map table for Single Item as Resource Item",
"CREATE TABLE IF NOT EXISTS INTRANET_RESOURCE_INVENTORY_ITEM_LIST (
  ResourceID int(11) NOT NULL default 0,
  ItemID int(11) NOT NULL default 0,
  PRIMARY KEY (ResourceID),
  UNIQUE KEY ResourceItemMapKey (ResourceID,ItemID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2008-09-25",
"eSports - add usertype for user (access right)",
"alter table SPORTS_ADMIN_USER_ACL add UserType varchar(10) default 'ADMIN' after UserLevel;"
);


$sql_eClassIP_update[] = array(
"2008-09-30",
"eHomework - Insert 0.25 hour loading ",
"alter table INTRANET_HOMEWORK change Loading Loading float"
);

$sql_eClassIP_update[] = array(
"2008-10-08",
"eHomework - homework collection features. Indicate if homework will be collected by teacher ",
"alter table INTRANET_HOMEWORK add column CollectRequired int(11)"
);

$sql_eClassIP_update[] = array(
"2008-10-09",
"eEnrolment - Record UserID of the user who modify the club attendance record",
"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE add LastModifiedBy int(11);"
);

$sql_eClassIP_update[] = array(
"2008-10-09",
"eEnrolment - Record the Role of the user who modify the club attendance record: A=Admin, H=Helper",
"ALTER TABLE INTRANET_ENROL_GROUP_ATTENDANCE add LastModifiedRole char(2);"
);

$sql_eClassIP_update[] = array(
"2008-10-09",
"eEnrolment - Record UserID of the user who modify the event attendance record",
"ALTER TABLE INTRANET_ENROL_EVENT_ATTENDANCE add LastModifiedBy int(11);"
);

$sql_eClassIP_update[] = array(
"2008-10-09",
"eEnrolment - Record the Role of the user who modify the event attendance record: A=Admin, H=Helper",
"ALTER TABLE INTRANET_ENROL_EVENT_ATTENDANCE add LastModifiedRole char(2);"
);

$sql_eClassIP_update[] = array(
"2008-10-22",
"Addon Log error mysql query table",
"CREATE TABLE IF NOT EXISTS INTRANET_LOGGING_TABLE
(
    RecordID int(11) NOT NULL auto_increment,
    UserID int(11),
    CourseID int(11),
    SqlQuery TEXT,
    SourcePath TEXT,
    InputDate datetime,
    IPAddress varchar(255),
    ErrorMsg TEXT,
    primary key(RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-05",
"Add access right group table",
"CREATE TABLE IF NOT EXISTS ACCESS_RIGHT_GROUP (
 GroupID int(11) NOT NULL auto_increment,
 GroupType char(1),
 GroupTitle varchar(128),
 GroupDescription text,
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-05",
"Add access right group table - setting",
"CREATE TABLE IF NOT EXISTS ACCESS_RIGHT_GROUP_SETTING (
 GroupSettingID int(11) NOT NULL auto_increment,
 GroupID int(11),
 Module varchar(50),
 Section varchar(50),
 Function varchar(50),
 Action varchar(50),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (GroupSettingID),
 FOREIGN KEY (GroupID) REFERENCES ACCESS_RIGHT_GROUP(GroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-05",
"Add access right group member table",
"CREATE TABLE IF NOT EXISTS ACCESS_RIGHT_GROUP_MEMBER (
 GroupMemberID int(11) NOT NULL auto_increment,
 GroupID int(11) default 0,
 UserID int(11) default 0,
 DateInput datetime,
 PRIMARY KEY (GroupMemberID),
 UNIQUE (GroupID, UserID),
 FOREIGN KEY (GroupID) REFERENCES ACCESS_RIGHT_GROUP(GroupID),
 FOREIGN KEY (UserID) REFERENCES INTRANET_USER(UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-17",
"eSports - Add Class Relay Lane Table",
"CREATE TABLE IF NOT EXISTS SPORTS_CLASS_RELAY_LANE_ARRANGEMENT 
(
  EventGroupID int(11) default NULL,
  ClassID int(11) default NULL,
  ArrangeOrder int(11) default NULL,
  ResultMin int(11) default NULL,
  ResultSec int(11) default NULL,
  ResultMs int(11) default NULL,
  RecordType int(11) default NULL,
  RecordStatus int(11) default NULL,
  Score float default NULL,
  Rank int(11) default NULL,
  DateModified datetime default NULL,
  KEY EventGroupID (EventGroupID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-20",
"Alter table INTRANET_USER for User Info (add STRN field)",
"ALTER TABLE INTRANET_USER ADD STRN varchar(8) default NULL;"
);


$sql_eClassIP_update[] = array(
"2008-11-22",
"New table for eDiscipline v1.2 - Semester Ratio",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_SEMESTER_RATIO (
 RecordID int(8) NOT NULL auto_increment,
 Year varchar(100),
 Semester varchar(100),
 Ratio int(4),
 DateInput datetime,
 DateModified datetime,
 PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-24",
"New table for eDiscipline - eNotice Template",
"CREATE TABLE IF NOT EXISTS INTRANET_NOTICE_MODULE_TEMPLATE 
(
	TemplateID int(11) NOT NULL auto_increment,
	Module varchar(100),
	CategoryID varchar(50),
	Title varchar(255) default NULL,
	Content text,
	ReplySlip text,
	RecordType char(2) default NULL,
	RecordStatus char(2) default NULL,
	DateInput datetime default NULL,
	DateModified datetime default NULL,
	PRIMARY KEY  (TemplateID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-24",
"Add TemplateID for merit/demeirt record",
"alter table DISCIPLINE_MERIT_RECORD add TemplateID int(11) after hasSocialContact;"
);

$sql_eClassIP_update[] = array(
"2008-11-24",
"Add TemplateOtherInfo for merit/demeirt record",
"alter table DISCIPLINE_MERIT_RECORD add TemplateOtherInfo text after TemplateID;"
);

$sql_eClassIP_update[] = array(
"2008-11-25",
"Add Sound starting cue for every book page in eLibrary",
"alter table INTRANET_ELIB_BOOK_PAGE add SoundStartCue time default '00:00:00';"
);

$sql_eClassIP_update[] = array(
"2008-11-25",
"Add Sound ending cue for every book page in eLibrary",
"alter table INTRANET_ELIB_BOOK_PAGE add SoundEndCue time default '00:00:00';"
);

$sql_eClassIP_update[] = array(
"2008-11-26",
"eDiscipline - Conduct Adjustment",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_CONDUCT_ADJUSTMENT
(
	RecordID int(11) NOT NULL auto_increment,
	StudentID int(11) NOT NULL,
	Year varchar(100) default NULL,
	Semester varchar(100) default NULL,
	IsAnnual int(11) default NULL,
	AdjustMark int(11) default NULL,
	Reason varchar(100) default NULL,
	PICID int(11) NOT NULL,
	DateInput datetime,
	DateModified datetime,
	PRIMARY KEY (RecordID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-11-28",
"eDiscipline - update the DISCIPLINE_MERIT_RECORD PIC field from single to support multiple ",
"alter table DISCIPLINE_MERIT_RECORD modify PICID VARCHAR(255);"
);

$sql_eClassIP_update[] = array(
"2008-11-28",
"eDiscipline - update the PROFILE_STUDENT_MERIT PIC field from single to support multiple ",
"alter table PROFILE_STUDENT_MERIT modify PersonInCharge VARCHAR(255);"
);

$sql_eClassIP_update[] = array(
"2008-11-29",
"Add NoticeID for merit/demeirt record",
"alter table DISCIPLINE_MERIT_RECORD add NoticeID int(11) after WaivedBy;"
);

$sql_eClassIP_update[] = array
(
"2008-12-01",
"add internal/external type to student activity record in admin console(1 for internal, 2 for external)",
"ALTER TABLE PROFILE_STUDENT_ACTIVITY ADD RecordType tinyint(4) default NULL;"
);

$sql_eClassIP_update[] = array(
"2008-11-29",
"Add Modifiedby (UserID) for merit/demeirt record",
"alter table DISCIPLINE_MERIT_RECORD add ModifiedBy int(11) after DateModified;"
);

$sql_eClassIP_update[] = array(
"2008-12-01",
"Add ReleaseStatus for merit/demeirt record",
"alter table DISCIPLINE_MERIT_RECORD add ReleaseStatus int(11) after ModifiedBy;"
);

$sql_eClassIP_update[] = array(
"2008-12-01",
"Add LockStatus for merit/demeirt record",
"alter table DISCIPLINE_MERIT_RECORD add LockStatus int(11) after ReleaseStatus;"
);

$sql_eClassIP_update[] = array(
"2008-12-02",
"eLibrary - add PdfID to record the file path infomation of pdf's swf file",
"alter table INTRANET_ELIB_BOOK_PAGE add PdfID int(11) after OrigPageID;"
);

$sql_eClassIP_update[] = array(
"2008-12-02",
"eDiscipline - Case Record table",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_CASE (
	CaseID int(11) NOT NULL auto_increment,
	CaseNumber varchar(50),
	CaseTitle varchar(128),
	Category varchar(255),
	Year varchar(50),
	Semester varchar(50),
	EventDate date,
	Location varchar(255),
	PICID varchar(255),
	Attachment varchar(255),
	RecordStatus int(11),
	LockStatus int(11),
	DateInput datetime,
	DateModified datetime,
	PRIMARY KEY (CaseID),
	UNIQUE CaseNumber (CaseNumber)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-12-02",
"eDiscipline v1.2 - add PICID to Good conduct and misconduct record",
"alter table DISCIPLINE_ACCU_RECORD add PICID varchar(255) after UpgradedRecordID;"
);

$sql_eClassIP_update[] = array(
"2008-12-04",
"Add check the Demerit record is accumulated from misconduct records",
"alter table DISCIPLINE_MERIT_RECORD add fromConductRecords tinyint(1) after LockStatus;"
);

$sql_eClassIP_update[] = array(
"2008-12-04",
"eDiscipline v1.2 - add count number to DISCIPLINE_ACCU_RECORD to check these records are counted as which setting",
"alter table DISCIPLINE_ACCU_RECORD add CountNo int(11) default 0 after DateModified;"
);

$sql_eClassIP_update[] = array(
"2008-12-04",
"eDiscipline - Detention session table",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_DETENTION_SESSION (
	DetentionID      INT(11)         NOT NULL AUTO_INCREMENT PRIMARY KEY,
	DetentionDate    DATE            NOT NULL,
	StartTime        TIME            NOT NULL,
	EndTime          TIME            NOT NULL,
	Location         VARCHAR(255)    NOT NULL,
	Vacancy          SMALLINT(4)     NOT NULL,
	DateInput        DATETIME        NOT NULL,
	DateModified     DATETIME
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-12-04",
"eDiscipline - Relationship between detention sessions and class levels table",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_DETENTION_SESSION_CLASSLEVEL (
	RecordID        INT(11)     NOT NULL AUTO_INCREMENT PRIMARY KEY,
	DetentionID     INT(11)     NOT NULL,
	ClassLevelID    INT(11)     NOT NULL,
	DateInput       DATETIME    NOT NULL,
	DateModified    DATETIME,
	UNIQUE DetentionID_UK (DetentionID, ClassLevelID),
	INDEX DetentionID (DetentionID),
	INDEX ClassLevelID (ClassLevelID),
	FOREIGN KEY (DetentionID)
		REFERENCES DISCIPLINE_DETENTION_SESSION (DetentionID),
	FOREIGN KEY (ClassLevelID)
		REFERENCES INTRANET_CLASSLEVEL (ClassLevelID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-12-04",
"eDiscipline - Relationship between detention sessions and PICs table",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_DETENTION_SESSION_PIC (
	RecordID        INT(11)     NOT NULL AUTO_INCREMENT PRIMARY KEY,
	DetentionID     INT(11)     NOT NULL,
	UserID          INT(11)     NOT NULL,
	DateInput       DATETIME    NOT NULL,
	DateModified    DATETIME,
	UNIQUE DetentionID_UK (DetentionID, UserID),
	INDEX DetentionID (DetentionID),
	INDEX UserID (UserID),
	FOREIGN KEY (DetentionID)
		REFERENCES DISCIPLINE_DETENTION_SESSION (DetentionID),
	FOREIGN KEY (UserID)
		REFERENCES INTRANET_USER (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-12-04",
"eDiscipline - Relationship between students and detention sessions table",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_DETENTION_STUDENT_SESSION (
	RecordID               INT(11)     NOT NULL AUTO_INCREMENT PRIMARY KEY,
	StudentID              INT(11)     NOT NULL,
	DetentionID            INT(11),
	DemeritID              INT(11),
	Reason                 VARCHAR(128),
	Remark                 TEXT,
	AttendanceStatus       VARCHAR(3),
	TemplateID             INT(11),
	RequestedBy            INT(11)     NOT NULL,
	ArrangedBy             INT(11),
	ArrangedDate           DATETIME,
	AttendanceRemark       TEXT,
	AttendanceTakenBy      INT(11),
	AttendanceTakenDate    DATETIME,
	DateInput              DATETIME    NOT NULL,
	DateModified           DATETIME,
	UNIQUE StudentID_UK (StudentID, DetentionID),
	INDEX StudentID (StudentID),
	INDEX DetentionID (DetentionID),
	INDEX DemeritID (DemeritID),
	FOREIGN KEY (StudentID)
		REFERENCES INTRANET_USER (UserID),
	FOREIGN KEY (DetentionID)
		REFERENCES DISCIPLINE_DETENTION_SESSION (DetentionID),
	FOREIGN KEY (RequestedBy)
		REFERENCES INTRANET_USER (UserID),
	FOREIGN KEY (ArrangedBy)
		REFERENCES INTRANET_USER (UserID),
	FOREIGN KEY (AttendanceTakenBy)
		REFERENCES INTRANET_USER (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field ApprovedDate to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD ApprovedDate datetime default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field ApprovedBy to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD ApprovedBy int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field ReleasedDate to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD ReleasedDate datetime default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field ReleasedBy to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD ReleasedBy int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field RejectedDate to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD RejectedDate datetime default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field RejectedDate to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD RejectedBy int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field WaivedDate to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD WaivedDate datetime default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field WaivedBy to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD WaivedBy int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field NoticeID to DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD NoticeID int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Add new field MeritType to DISCIPLINE_ACCU_CATEGORY",
"ALTER TABLE DISCIPLINE_ACCU_CATEGORY ADD MeritType int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Store the general setting in eDISCIPLINE_v12",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_GENERAL_SETTING (
	RecordID 		int(11) 		primary key auto_increment, 
	SettingName 	varchar(100), 
	SettingValue 	varchar(50), 
	DateInput 		datetime, 
	DateModified 	datetime, 
	ModifiedBy 		int(11)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
	"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Independent conversion (promotion) setting for Award_and_Punishment",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_MERIT_PROMOTION_BY_CATEGORY (
	RecordID 			int(11) 	primary key auto_increment, 
	CatID 				int(11) 	default 0 not null, 
	MeritType 			int(11) 	default 0 not null, 
	UpgradeFromType 	int(11), 
	UpgradeNum 			int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Trace down the user that last modify the record",
"alter table DISCIPLINE_CASE add ModifiedBy int(11)"
);

$sql_eClassIP_update[] = array(
"2008-12-05",
"eDiscipline_v12 - Classify the notice record if they are from other module",
"alter table INTRANET_NOTICE add Module varchar(100)"
);

$sql_eClassIP_update[] = array(
"2008-12-09",
"Add CaseID for Demerit record",
"alter table DISCIPLINE_MERIT_RECORD add CaseID int(11) after fromConductRecords;"
);

$sql_eClassIP_update[] = array(
"2008-12-10",
"Add Subject for notice subject",
"alter table INTRANET_NOTICE_MODULE_TEMPLATE add Subject varchar(255)"
);


$sql_eClassIP_update[] = array(
"2008-12-22",
"Add fields for case record",
"alter table DISCIPLINE_CASE add ReleaseStatus int(11) after ModifiedBy"
);

$sql_eClassIP_update[] = array(
"2008-12-22",
"Add fields for case record",
"ALTER TABLE DISCIPLINE_CASE ADD ReleasedDate datetime default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-22",
"Add fields for case record",
"ALTER TABLE DISCIPLINE_CASE ADD ReleasedBy int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-22",
"Add fields for case record",
"ALTER TABLE DISCIPLINE_CASE ADD FinishedDate datetime default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-22",
"Add fields for case record",
"ALTER TABLE DISCIPLINE_CASE ADD FinishedBy int(11) default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-22",
"add index to user group table for optimization",
"alter table INTRANET_USERGROUP add index USERID (UserID)"
);

$sql_eClassIP_update[] = array(
"2008-12-23",
"Add FinishedBy/FinishedDate for case record",
"ALTER TABLE DISCIPLINE_CASE ADD FinishedDate datetime default NULL"
);

$sql_eClassIP_update[] = array(
"2008-12-23",
"Add FinishedBy/FinishedDate for case record",
"ALTER TABLE DISCIPLINE_CASE ADD FinishedBy int(11) default NULL"
);


$sql_eClassIP_update[] = array(
"2008-12-24",
"enhace eCircular to indicate important records",
"alter table INTRANET_CIRCULAR_REPLY add HasStar bool default 0"
);

$sql_eClassIP_update[] = array(
"2008-12-30",
"eDisciplinev12 - add School Year field for good/mis conduct record",
"alter table DISCIPLINE_ACCU_RECORD add Year varchar(50)"
);

$sql_eClassIP_update[] = array(
"2008-12-30",
"eDisciplinev12 - add Semester field for good/mis conduct record",
"alter table DISCIPLINE_ACCU_RECORD add Semester varchar(50)"
);

$sql_eClassIP_update[] = array(
"2009-01-02",
"eDisciplinev12 - add record creator for DISCIPLINE_MERIT_RECORD",
"alter table DISCIPLINE_MERIT_RECORD add CreatedBy int(11)"
);

$sql_eClassIP_update[] = array(
"2009-01-02",
"eDisciplinev12 - add record creator for DISCIPLINE_ACCU_RECORD",
"alter table DISCIPLINE_ACCU_RECORD add CreatedBy int(11)"
);

$sql_eClassIP_update[] = array(
"2009-01-02",
"eDisciplinev12 - add record creator for DISCIPLINE_CASE",
"alter table DISCIPLINE_CASE add CreatedBy int(11)"
);

$sql_eClassIP_update[] = array(
"2009-01-02",
"eDisciplinev12 - add record creator for DISCIPLINE_DETENTION_STUDENT_SESSION",
"alter table DISCIPLINE_DETENTION_STUDENT_SESSION add CreatedBy int(11)"
);


$sql_eClassIP_update[] = array(
"2009-01-05",
"eDisciplinev12 - add reason for DISCIPLINE_MERIT_RECORD_WAIVE",
"alter table DISCIPLINE_MERIT_RECORD_WAIVE add Reason varchar(255) after WaiveEndDate"
);

$sql_eClassIP_update[] = array(
"2009-01-06",
"eDisciplinev12 - add IsModule for INTRANET_NOTICE",
"alter table INTRANET_NOTICE add IsModule tinyint(1) default 0 after RecordStatus"
);

$sql_eClassIP_update[] = array(
"2009-01-13",
"eDisciplinev12 - change the SettingValue from varchar(50) to varchar(100)",
"alter table DISCIPLINE_GENERAL_SETTING change column SettingValue SettingValue varchar(100);"
);

$sql_eClassIP_update[] = array(
"2009-01-13",
"eDisciplinev12 - add columns Year and Semester for DISCIPLINE_DETENTION_SESSION",
"ALTER TABLE DISCIPLINE_DETENTION_SESSION ADD (Year VARCHAR(50) NOT NULL, Semester VARCHAR(50) NOT NULL);"
);

$sql_eClassIP_update[] = array(
"2009-01-13",
"eDisciplinev12 - rename column TemplateID to NoticeID for DISCIPLINE_DETENTION_STUDENT_SESSION",
"ALTER TABLE DISCIPLINE_DETENTION_STUDENT_SESSION CHANGE COLUMN TemplateID NoticeID INT(11);"
);

$sql_eClassIP_update[] = array(
"2009-01-13",
"eDisciplinev12 - add RedeemID to record the merit-demerit redeem group for DISCIPLINE_MERIT_RECORD",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD RedeemID int(11);"
);

$sql_eClassIP_update[] = array(
"2009-01-14",
"iCalendar - add CalType to record the calendar type for CALENDAR_CALENDAR",
"ALTER TABLE CALENDAR_CALENDAR ADD CalType tinyint(1) NOT NULL DEFAULT 0;"
);

$sql_eClassIP_update[] = array(
"2009-01-14",
"iCalendar - add DefaultEventAccess to record the calendar default event access for CALENDAR_CALENDAR",
"ALTER TABLE CALENDAR_CALENDAR ADD DefaultEventAccess char(1) DEFAULT NULL;"
);

$sql_eClassIP_update[] = array(
"2009-01-15",
"Add a table CALENDAR_EVENT_PERSONAL_NOTE for iCalendar",
"CREATE TABLE IF NOT EXISTS CALENDAR_EVENT_PERSONAL_NOTE (
  EventID int(10) NOT NULL,
  UserID int(8) NOT NULL,
  PersonalNote text,
  CreateDate datetime default NULL, 
  LastUpdateDate datetime default NULL, 
  KEY KY_EventID_UserID (EventID, UserID), 
  INDEX EventID (EventID), 
  INDEX UserID (UserID) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);


$sql_eClassIP_update[] = array(
"2009-01-19",
"iCalendar - add InviteStatus to record the invite status of each shared event user for CALENDAR_EVENT_USER",
"ALTER TABLE CALENDAR_EVENT_USER ADD InviteStatus tinyint DEFAULT NULL;"
);

$sql_eClassIP_update[] = array(
"2009-01-22",
"eDisciplinev12",
"alter table DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG add index RelatedRecordID (RelatedRecordID);"
);

$sql_eClassIP_update[] = array(
"2009-01-25",
"eDisciplinev12",
"alter table DISCIPLINE_ACCU_RECORD add StudentAttendanceID int(11) default 0;"
);
 
$sql_eClassIP_update[] = array(
"2009-01-30",
"eInventory",
"alter table INVENTORY_ITEM modify ItemCode varchar(100);"
); 

$sql_eClassIP_update[] = array(
"2009-02-02",
"eDisciplinev12",
"alter table DISCIPLINE_STUDENT_CONDUCT_BALANCE add index StudentID(StudentID);"
); 

$sql_eClassIP_update[] = array(
"2009-02-02",
"eDisciplinev12",
"alter table DISCIPLINE_STUDENT_CONDUCT_BALANCE add index Semester(Semester);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_ITEM ADD INDEX InventoryItemNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_ITEM ADD INDEX InventoryItemNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_CATEGORY ADD INDEX CategoryNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_CATEGORY ADD INDEX CategoryNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_CATEGORY_LEVEL2 ADD INDEX SubCatNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_CATEGORY_LEVEL2 ADD INDEX SubCatNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_FUNDING_SOURCE ADD INDEX FundingNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_FUNDING_SOURCE ADD INDEX FundingNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_ITEM_SINGLE_EXT ADD INDEX SingleItemTagCode (TagCode);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_ITEM_SINGLE_EXT ADD INDEX SingleItemBrand (Brand);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOCATION ADD INDEX BulkItemID (ItemID);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOCATION ADD INDEX BulkItemAdminGroup (GroupInCharge);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOCATION ADD INDEX BulkItemLocation (LocationID);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION add index InventoryLocationID (LocationID);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION add index InventorySubLocationID (LocationLevelID);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION add index InventoryLocationCode (Code);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION add index InventoryLocationNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION add index InventoryLocationNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_LOCATION ADD INDEX InventorySubLocationCode (Code);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_LOCATION ADD INDEX  InventorySubLocationNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"ALTER TABLE INVENTORY_LOCATION ADD INDEX InventorySubLocationNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION_LEVEL add index InventoryLocationID (LocationLevelID);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION_LEVEL add index InventoryLocationCode (Code);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION_LEVEL add index InventoryLocationNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_LOCATION_LEVEL add index InventoryLocationNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_ADMIN_GROUP add index InventoryAdminGroupID (AdminGroupID);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_ADMIN_GROUP add index InventoryAdminGroupCode (Code);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_ADMIN_GROUP add index InventoryAdminGroupNameChi (NameChi);"
); 

$sql_eClassIP_update[] = array(
"2009-02-06",
"eInventory",
"alter table INVENTORY_ADMIN_GROUP add index InventoryAdminGroupNameEng (NameEng);"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_LOCATION DROP KEY ChineseName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_LOCATION DROP KEY EnglishName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY ChineseName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_LOCATION_LEVEL DROP KEY EnglishName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_CATEGORY_LEVEL2 DROP Key CategoryIDWithNameChi;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_CATEGORY_LEVEL2  DROP Key CategoryIDWithNameEng;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_CATEGORY DROP Key CategoryName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_ADMIN_GROUP DROP Key ChineseName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_ADMIN_GROUP DROP Key EnglishName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_FUNDING_SOURCE DROP Key EnglishName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-09",
"eInventory",
"ALTER TABLE INVENTORY_FUNDING_SOURCE DROP Key ChineseName;"
); 

$sql_eClassIP_update[] = array(
"2009-02-18",
"Add a table INTRANET_ELIB_USER_PROCESS for eLibrary",
"CREATE TABLE IF NOT EXISTS INTRANET_ELIB_USER_PROGRESS
(
	ProcessID int(11) NOT NULL auto_increment,
	BookID int(11),
	UserID int(11),
	PageID int(11),
	Percentage int(11) Default 0,
	DateModified datetime,
	primary key(ProcessID),
	INDEX BookID (BookID),
	INDEX UserID (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2008-02-23",
"eDiscipline Case Report table",
"alter table DISCIPLINE_CASE ADD UNIQUE KEY (CaseNumber);"
);

$sql_eClassIP_update[] = array(
"2009-02-24",
"Add FinishTime records how many times the student has finished reading this book in eLibrary",
"alter table INTRANET_ELIB_USER_PROGRESS add FinishTime int(11) default 0 after Percentage;"
);

$sql_eClassIP_update[] = array(
"2009-03-05",
"Add the date record of the time transferring to WebSAMS for each discipline record",
"ALTER Table DISCIPLINE_MERIT_RECORD ADD DateOfTransferToWebSAMS datetime DEFAULT NULL;"
);

$sql_eClassIP_update[] = array(
"2009-03-05",
"Add GM reference to detention",
"ALTER Table DISCIPLINE_DETENTION_STUDENT_SESSION ADD GMID  int(11) default NULL;"
);

$sql_eClassIP_update[] = array(
"2009-03-06",
"Add transfer status for discipline records: 0 - Not yet transferred ; 1 - transferred to WebSAMS already",
"ALTER TABLE DISCIPLINE_MERIT_RECORD ADD HasTransferredToWebSAMS tinyint(1) default 0;"
);

$sql_eClassIP_update[] = array(
"2009-03-09",
"Add notice id for good conduct misconduct record",
"ALTER TABLE DISCIPLINE_ACCU_RECORD ADD NoticeID int(11) default 0;"
);

$sql_eClassIP_update[] = array(
"2009-03-24",
"Add Index (SearchSubject) For INTRANET_CAMPUSMAIL",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX SearchSubject (Subject)"
);

$sql_eClassIP_update[] = array(
"2009-03-24",
"Add Index (Message) For INTRANET_CAMPUSMAIL",
"ALTER TABLE INTRANET_CAMPUSMAIL ADD INDEX SearchMessage (Message (100))"
);

$sql_eClassIP_update[] = array(
"2009-03-24",
"Add Remark For Case Record",
"ALTER TABLE DISCIPLINE_CASE ADD Remark text;"
);

$sql_eClassIP_update[] = array(
"2009-03-24",
"Add ItemCode For Category Item",
"ALTER TABLE DISCIPLINE_ACCU_CATEGORY_ITEM ADD ItemCode varchar(10) unique"
);

$sql_eClassIP_update[] = array(
"2009-03-26",
"Change general value data type",
"alter table DISCIPLINE_GENERAL_SETTING change SettingValue SettingValue text;"
);

$sql_eClassIP_update[] = array(
"2009-03-30",
"increase the length of itemcode to 100",
"alter table DISCIPLINE_ACCU_CATEGORY_ITEM change Itemcode ItemCode varchar(100);"
);

$sql_eClassIP_update[] = array(
"2009-03-30",
"Create a Barcode Setting Change Log For eInventory",
"CREATE TABLE IF NOT EXISTS INVENTORY_BARCODE_SETTING_LOG (LogID int NOT NULL auto_increment, MaxLengthChanged int(11), FormatChanged int(11), RecordType int(11), RecordStatus int(11), DateInput datetime, DateModified datetime, PRIMARY KEY (LogID)) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-04-17",
"Change date type of PurchasedPrice & UnitPrice to Double in eInventory",
"ALTER TABLE INVENTORY_ITEM_SINGLE_EXT MODIFY PurchasedPrice double(20,2), MODIFY UnitPrice double(20,2);"
);

$sql_eClassIP_update[] = array(
"2009-04-17",
"Change date type of PurchasedPrice & UnitPrice to Double in eInventory",
"ALTER TABLE INVENTORY_ITEM_BULK_LOG MODIFY PurchasedPrice double(20,2), MODIFY UnitPrice double(20,2);"
);


$sql_eClassIP_update[] = array(
"2009-04-17",
"eDisciplinev12 - create new leaf scheme table",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_NEW_LEAF_SCHEME (
SchemeID int(8) NOT NULL auto_increment,
CategoryID int(8) NOT NULL,
SchemeType varchar(10) NOT NULL,
PeriodID int(8),
Semester varchar(50),
WaiveDay int(8),
WaiveFirst int(8),
DateInput datetime,
DateModified datetime,
PRIMARY KEY (SchemeID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-04-22",
"eDisv12 - Add New Leaf related fields to GM table",
"ALTER TABLE DISCIPLINE_ACCU_RECORD add WaivedByNewLeaf tinyint(1) default NULL;"
);

$sql_eClassIP_update[] = array(
"2009-04-22",
"eDisv12 - Add New Leaf related fields to GM table",
"ALTER TABLE DISCIPLINE_ACCU_RECORD add WaivedBy varchar(11) default NULL;"
);

$sql_eClassIP_update[] = array(
"2009-04-22",
"eDisv12 - Add New Leaf related fields to GM table",
"ALTER TABLE DISCIPLINE_ACCU_RECORD add WaivedDate datetime default NULL;"
);

$sql_eClassIP_update[] = array(
"2009-04-29",
"ePayment - Add index to speed up loading time for cancel cash deposit",
"ALTER TABLE PAYMENT_OVERALL_TRANSACTION_LOG add KEY TransactionType (TransactionType);"
);

$sql_eClassIP_update[] = array(
"2009-04-29",
"ePayment - Add index to speed up loading time for cancel cash deposit",
"ALTER TABLE PAYMENT_OVERALL_TRANSACTION_LOG add KEY RelatedTransactionID (RelatedTransactionID);"
);

$sql_eClassIP_update[] = array(
"2009-04-30",
"eInventory - Add column, FundingSourceID to table INVENTORY_ITEM_BULK_LOCATION",
"ALTER TABLE INVENTORY_ITEM_BULK_LOCATION ADD COLUMN FundingSourceID int(11) NOT NULL AFTER LocationID;"
);

$sql_eClassIP_update[] = array(
"2009-04-30",
"eInventory - Add column, PriceCeiling to table INVENTORY_CATEGORY",
"ALTER TABLE INVENTORY_CATEGORY ADD COLUMN PriceCeiling double(20,2) DEFAULT 0 AFTER PhotoLink;"
);

$sql_eClassIP_update[] = array(
"2009-04-30",
"eInventory - Add column, ApplyPriceCeilingToBulk to table INVENTORY_CATEGORY",
"ALTER TABLE INVENTORY_CATEGORY ADD COLUMN ApplyPriceCeilingToBulk int(11) AFTER PriceCeiling;"
);

$sql_eClassIP_update[] = array(
"2009-05-14",
"eDisv12 - add field fromPPC for Award/Punishment",
"alter table DISCIPLINE_MERIT_RECORD add fromPPC tinyint(1);"
);

$sql_eClassIP_update[] = array(
"2009-05-14",
"eDisv12 - add field fromPPC for GM record",
"alter table DISCIPLINE_ACCU_RECORD add fromPPC tinyint(1);"
);

$sql_eClassIP_update[] = array(
"2009-05-20",
"eSports - setup the init type - Track",
"INSERT IGNORE INTO SPORTS_EVENT_TYPE_NAME (EventTypeID, EnglishName, ChineseName, DateInput, DateModified) VALUES ('1','Track','�|��', now(), now())"
);

$sql_eClassIP_update[] = array(
"2009-05-20",
"eSports - setup the init type - Field",
"INSERT IGNORE INTO SPORTS_EVENT_TYPE_NAME (EventTypeID, EnglishName, ChineseName, DateInput, DateModified) VALUES ('2','Field','����', now(), now())"
);

$sql_eClassIP_update[] = array(
"2009-05-20",
"eSports - setup the init type - House Relay",
"INSERT IGNORE INTO SPORTS_EVENT_TYPE_NAME (EventTypeID, EnglishName, ChineseName, DateInput, DateModified) VALUES ('3','House Relay','���ڱ��O', now(), now())"
);

$sql_eClassIP_update[] = array(
"2009-05-20",
"eSports - setup the init type - Class Relay",
"INSERT IGNORE INTO SPORTS_EVENT_TYPE_NAME (EventTypeID, EnglishName, ChineseName, DateInput, DateModified) VALUES ('4','Class Relay','�Z�ڱ��O', now(), now())"
);

$sql_eClassIP_update[] = array(
"2009-06-11",
"eEnrolment - Consistence the schema between INTRANET_ARCHIVE_ENROL_GROUPINFO and INTRANET_ENROL_GROUPINFO",
"ALTER TABLE INTRANET_ARCHIVE_ENROL_GROUPINFO add RecordStatus char(2) default NULL"
);

$sql_eClassIP_update[] = array(
"2009-06-11",
"Alter table INTRANET_NOTICE_MODULE_TEMPLATE for eNotice Template (reply slip content only)",
"ALTER TABLE INTRANET_NOTICE_MODULE_TEMPLATE add ReplySlipContent text default NULL;"
);

$sql_eClassIP_update[] = array(
"2009-06-11",
"Alter table INTRANET_NOTICE for eNotice Template (reply slip content only)",
"ALTER TABLE INTRANET_NOTICE add ReplySlipContent text default NULL;"
);

$sql_eClassIP_update[] = array(
"2009-06-22",
"create a LSLP user table for license control",
"CREATE TABLE IF NOT EXISTS LS_USER (
  UserID int(11) NOT NULL,
  LsUserID int(11),
  DateInput datetime NOT NULL,
  LastUsed datetime,
  PRIMARY KEY (UserID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-06-24",
"Alter table SPORTS_AGE_GROUP, Add columns to store enrollment info for each age group",
"ALTER TABLE SPORTS_AGE_GROUP
	ADD COLUMN EnrolMinTotal INT(11),
	ADD COLUMN EnrolMaxTotal INT(11),
	ADD COLUMN EnrolMinTrack INT(11),
	ADD COLUMN EnrolMaxTrack INT(11),
	ADD COLUMN EnrolMinField INT(11),
	ADD COLUMN EnrolMaxField INT(11);"
);

$sql_eClassIP_update[] = array(
"2009-06-24",
"Create INTRANET_SUBJECT_LEADER which some clients may missed it",
"CREATE TABLE IF NOT EXISTS INTRANET_SUBJECT_LEADER (
LeaderID int(11) NOT NULL auto_increment,
UserID int(11) default NULL,
ClassID int(11) default NULL,
SubjectID int(11) default NULL,
RecordType char(2) default NULL,
RecordStatus char(2) default NULL,
DateInput datetime default NULL,
DateModified datetime default NULL,
PRIMARY KEY  (LeaderID),
KEY UserID (UserID),
KEY ClassID (ClassID),
KEY SubjectID (SubjectID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_ADMIN_USER_ACL for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_ADMIN_USER_ACL` (
  `AdminUserID` int(11) NOT NULL default '0',
  `UserLevel` int(11) default NULL,
  `UserType` varchar(10) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  UNIQUE KEY `AdminUserID` (`AdminUserID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_AGE_GROUP for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_AGE_GROUP` (
  `AgeGroupID` int(11) NOT NULL auto_increment,
  `GradeChar` varchar(10) default NULL,
  `EnglishName` varchar(255) default NULL,
  `ChineseName` varchar(255) default NULL,
  `Gender` char(2) default NULL,
  `GroupCode` varchar(100) default NULL,
  `DOBUpLimit` date default NULL,
  `DOBLowLimit` date default NULL,
  `DisplayOrder` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  `EnrolMinTrack` int(11) default NULL,
  `EnrolMaxTrack` int(11) default NULL,
  PRIMARY KEY  (`AgeGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_CLASS_RELAY_LANE_ARRANGEMENT for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_CLASS_RELAY_LANE_ARRANGEMENT` (
  `EventGroupID` int(11) default NULL,
  `ClassID` int(11) default NULL,
  `ArrangeOrder` int(11) default NULL,
  `ResultMin` int(11) default NULL,
  `ResultSec` int(11) default NULL,
  `ResultMs` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `Score` float default NULL,
  `Rank` int(11) default NULL,
  `DateModified` datetime default NULL,
  KEY `EventGroupID` (`EventGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_EVENT for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_EVENT` (
  `EventID` int(11) NOT NULL auto_increment,
  `EventType` int(11) default NULL,
  `EnglishName` varchar(255) default NULL,
  `ChineseName` varchar(255) default NULL,
  `DisplayOrder` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  `SpecialLaneArrangement` text,
  PRIMARY KEY  (`EventID`),
  KEY `EventType` (`EventType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_EVENTGROUP for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_EVENTGROUP` (
  `EventGroupID` int(11) NOT NULL auto_increment,
  `EventID` int(11) default NULL,
  `GroupID` int(11) default NULL,
  `IsOnlineEnrol` int(11) default NULL,
  `CountPersonalQuota` int(11) default NULL,
  `CountHouseScore` int(11) default NULL,
  `CountClassScore` int(11) default NULL,
  `CountIndividualScore` int(11) default NULL,
  `ScoreStandardID` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`EventGroupID`),
  KEY `EventID` (`EventID`),
  KEY `GroupID` (`GroupID`),
  KEY `IsOnlineEnrol` (`IsOnlineEnrol`),
  KEY `CountPersonalQuota` (`CountPersonalQuota`),
  KEY `ScoreStandardID` (`ScoreStandardID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_EVENTGROUP_EXT_RELAY for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_EVENTGROUP_EXT_RELAY` (
  `EventGroupID` int(11) default NULL,
  `RecordHolderName` varchar(255) default NULL,
  `RecordMin` int(11) default NULL,
  `RecordSec` int(11) default NULL,
  `RecordMs` int(11) default NULL,
  `RecordYear` varchar(100) default NULL,
  `RecordHouseID` int(11) default NULL,
  `NewRecordHolderName` varchar(255) default NULL,
  `NewRecordMin` int(11) default NULL,
  `NewRecordSec` int(11) default NULL,
  `NewRecordMs` int(11) default NULL,
  `NewRecordHouseID` int(11) default NULL,
  `StandardMin` int(11) default NULL,
  `StandardSec` int(11) default NULL,
  `StandardMs` int(11) default NULL,
  `ClassList` text,
  `CategoryName` varchar(255) default NULL,
  KEY `EventGroupID` (`EventGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_EVENTGROUP_EXT_TRACK for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_EVENTGROUP_EXT_TRACK` (
  `EventGroupID` int(11) default NULL,
  `RecordHolderName` varchar(255) default NULL,
  `RecordMin` int(11) default NULL,
  `RecordSec` int(11) default NULL,
  `RecordMs` int(11) default NULL,
  `RecordYear` varchar(100) default NULL,
  `RecordHouseID` int(11) default NULL,
  `NewRecordHolderUserID` int(11) default NULL,
  `NewRecordHolderName` varchar(255) default NULL,
  `NewRecordMin` int(11) default NULL,
  `NewRecordSec` int(11) default NULL,
  `NewRecordMs` int(11) default NULL,
  `NewRecordHouseID` int(11) default NULL,
  `StandardMin` int(11) default NULL,
  `StandardSec` int(11) default NULL,
  `StandardMs` int(11) default NULL,
  `FirstRoundType` int(11) default NULL,
  `FirstRoundGroupCount` int(11) default NULL,
  `FirstRoundRandom` int(11) default NULL,
  `FirstRoundDay` int(11) default NULL,
  `SecondRoundReq` int(11) default NULL,
  `SecondRoundLanes` int(11) default NULL,
  `SecondRoundGroups` int(11) default NULL,
  `SecondRoundDay` int(11) default NULL,
  `FinalRoundReq` int(11) default NULL,
  `FinalRoundNum` int(11) default NULL,
  `FinalRoundDay` int(11) default NULL,
  KEY `EventGroupID` (`EventGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_EVENT_TYPE_NAME for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_EVENT_TYPE_NAME` (
  `EventTypeID` int(11) default NULL,
  `EnglishName` varchar(255) default NULL,
  `ChineseName` varchar(255) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  UNIQUE KEY `EventTypeID` (`EventTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_HOUSE_RELAY_LANE_ARRANGEMENT for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_HOUSE_RELAY_LANE_ARRANGEMENT` (
  `EventGroupID` int(11) default NULL,
  `HouseID` int(11) default NULL,
  `ArrangeOrder` int(11) default NULL,
  `ResultMin` int(11) default NULL,
  `ResultSec` int(11) default NULL,
  `ResultMs` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `Score` float default NULL,
  `Rank` int(11) default NULL,
  `DateModified` datetime default NULL,
  KEY `EventGroupID` (`EventGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_LANE_ARRANGEMENT for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_LANE_ARRANGEMENT` (
  `EventGroupID` int(11) default NULL,
  `RoundType` int(11) default NULL,
  `StudentID` int(11) default NULL,
  `Heat` int(11) default NULL,
  `ArrangeOrder` int(11) default NULL,
  `ResultMin` int(11) default NULL,
  `ResultSec` int(11) default NULL,
  `ResultMs` int(11) default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `IsAdvanced` int(11) default NULL,
  `Score` float default NULL,
  `Rank` int(11) default NULL,
  `DateModified` datetime default NULL,
  KEY `EventGroupID` (`EventGroupID`),
  KEY `StudentID` (`StudentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_SCORE_STANDARD for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_SCORE_STANDARD` (
  `StandardID` int(11) NOT NULL auto_increment,
  `Name` varchar(255) default NULL,
  `Position1` float default NULL,
  `Position2` float default NULL,
  `Position3` float default NULL,
  `Position4` float default NULL,
  `Position5` float default NULL,
  `Position6` float default NULL,
  `Position7` float default NULL,
  `Position8` float default NULL,
  `Position9` float default NULL,
  `RecordBroken` float default NULL,
  `Qualified` float default NULL,
  `RecordType` int(11) default NULL,
  `RecordStatus` int(11) default NULL,
  `DateInput` datetime default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`StandardID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_STUDENT_ENROL_EVENT for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_STUDENT_ENROL_EVENT` (
  `RecordID` int(11) NOT NULL auto_increment,
  `StudentID` int(11) default NULL,
  `EventGroupID` int(11) default NULL,
  `DateModified` datetime default NULL,
  PRIMARY KEY  (`RecordID`),
  KEY `StudentID` (`StudentID`),
  KEY `EventGroupID` (`EventGroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_STUDENT_ENROL_INFO for eWaterGala",
"
 CREATE TABLE IF NOT EXISTS  `WATERGALA_STUDENT_ENROL_INFO` (
  `StudentID` int(11) NOT NULL default '0',
  `AthleticNum` varchar(100) default NULL,
  `TrackEnrolCount` int(11) default NULL,
  `DateModified` datetime default NULL,
  UNIQUE KEY `StudentID` (`StudentID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
"
);

$sql_eClassIP_update[] = array(
"2009-06-25",
"Create WATERGALA_SYSTEM_SETTING for eWaterGala",
"
CREATE TABLE IF NOT EXISTS  `WATERGALA_SYSTEM_SETTING` (
  `NumberOfLanes` int(11) default NULL,
  `EnrolScore` float default NULL,
  `PresentScore` float default NULL,
  `AbsentScore` float default NULL,
  `EnrolDateStart` date default NULL,
  `EnrolDateEnd` date default NULL,
  `EnrolDetails` text,
  `NumberGenerationType` int(11) default NULL,
  `Rule1` int(11) default NULL,
  `Rule2` int(11) default NULL,
  `Rule3` int(11) default NULL,
  `AutoNumLength` int(11) default NULL,
  `NumOfDays` int(11) default NULL,
  `EnrolMinTrack` int(11) default NULL,
  `EnrolMaxTrack` int(11) default NULL,
  `DefaultLaneArrangement` text,
  `DateModified` datetime default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_ADMIN_USER_ACL 
	RENAME SWIMMINGGALA_ADMIN_USER_ACL               
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_AGE_GROUP 
	RENAME SWIMMINGGALA_AGE_GROUP"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_CLASS_RELAY_LANE_ARRANGEMENT 
	RENAME SWIMMINGGALA_CLASS_RELAY_LANE_ARRANGEMENT 
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_EVENT 
	RENAME SWIMMINGGALA_EVENT                      
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_EVENTGROUP 
	RENAME SWIMMINGGALA_EVENTGROUP              
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_EVENTGROUP_EXT_RELAY 
	RENAME SWIMMINGGALA_EVENTGROUP_EXT_RELAY    
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_EVENTGROUP_EXT_TRACK 
	RENAME SWIMMINGGALA_EVENTGROUP_EXT_TRACK     
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_EVENT_TYPE_NAME 
	RENAME SWIMMINGGALA_EVENT_TYPE_NAME         
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_HOUSE_RELAY_LANE_ARRANGEMENT 
	RENAME SWIMMINGGALA_HOUSE_RELAY_LANE_ARRANGEMENT 
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_LANE_ARRANGEMENT 
	RENAME SWIMMINGGALA_LANE_ARRANGEMENT             
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_SCORE_STANDARD 
	RENAME SWIMMINGGALA_SCORE_STANDARD  
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_STUDENT_ENROL_EVENT 
	RENAME SWIMMINGGALA_STUDENT_ENROL_EVENT
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_STUDENT_ENROL_INFO 
	RENAME SWIMMINGGALA_STUDENT_ENROL_INFO 
"
);

$sql_eClassIP_update[] = array(
"2009-07-06",
"Rename WATERGALA Tables to SWIMMINGGALA",
"
ALTER TABLE WATERGALA_SYSTEM_SETTING 
	RENAME SWIMMINGGALA_SYSTEM_SETTING  "
);



# [Discipline v1.2] Munsang - Conduct Grade customization [Start]
$sql_eClassIP_update[] = array(
"2009-07-07",
"Create Table for Munsang College Discipline Conduct Grade",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_MS_STUDENT_CONDUCT (
RecordID int(11) NOT NULL auto_increment,
Year varchar(100) NOT NULL,
Semester varchar(100) NOT NULL,
StudentID int(11) NOT NULL,
UserID int(11) NOT NULL,
ConductString varchar(100) NOT NULL,
IntegratedConductGrade varchar(10) NOT NULL,
DateInput datetime default NULL,
DateModified datetime default NULL,
PRIMARY KEY (RecordID),
INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-07-07",
"Create Table for Munsang College Discipline Conduct Grade",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_MS_STUDENT_CONDUCT_FINAL (
RecordID int(11) NOT NULL auto_increment,
Year varchar(100) NOT NULL,
Semester varchar(100) NOT NULL,
StudentID int(11) NOT NULL,
ConductString varchar(100) NOT NULL,
IntegratedConductGrade varchar(10) NOT NULL,
isAdjust tinyint(1) NOT NULL default 0,
Rank int(4) default NULL,
UserID int(11) NOT NULL,
DateInput datetime default NULL,
DateModified datetime default NULL,
PRIMARY KEY (RecordID),
INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);
# [Discipline v1.2] Munsang - Conduct Grade customization [End]

$sql_eClassIP_update[] = array(
"2009-07-14",
"Add Rebate session Id on Transaction Log table",
"alter table PAYMENT_OVERALL_TRANSACTION_LOG add RebateSessionID varchar(20) default NULL"
);

$sql_eClassIP_update[] = array(
"2009-07-14",
"Add Index for Rebate session Id on Transaction Log table",
"alter table PAYMENT_OVERALL_TRANSACTION_LOG add KEY RebateSessionID (RebateSessionID)"
);

# [Discipline v1.2] Munsang - Conduct Grade customization 2 [Start]
$sql_eClassIP_update[] = array(
"2009-07-15",
"Create Table for Munsang College Discipline Conduct Grade",
"CREATE TABLE IF NOT EXISTS DISCIPLINE_MS_STUDENT_CONDUCT_SEMI (
RecordID int(11) NOT NULL auto_increment,
Year varchar(100) NOT NULL,
Semester varchar(100) NOT NULL,
StudentID int(11) NOT NULL,
UserID int(11) NOT NULL,
ConductString varchar(100) NOT NULL,
IntegratedConductGrade varchar(10) NOT NULL,
DateInput datetime default NULL,
DateModified datetime default NULL,
PRIMARY KEY (RecordID),
INDEX StudentID (StudentID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8"
);

$sql_eClassIP_update[] = array(
"2009-07-16",
"Alter Table for Munsang College Discipline Conduct Grade",
"ALTER TABLE DISCIPLINE_MS_STUDENT_CONDUCT_FINAL ADD COLUMN needMeeting tinyint(1) DEFAULT 0 NOT NULL AFTER IntegratedConductGrade;"
);
# [Discipline v1.2] Munsang - Conduct Grade customization [End]

?>