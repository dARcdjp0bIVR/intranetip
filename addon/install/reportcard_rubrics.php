<?php
# Editing by 
@SET_TIME_LIMIT(1000);

$eRC_Rubrics_Addon_PATH_WRT_ROOT = ($PATH_WRT_ROOT!="") ? $PATH_WRT_ROOT : "../../";
include_once($eRC_Rubrics_Addon_PATH_WRT_ROOT."includes/global.php");
include_once($eRC_Rubrics_Addon_PATH_WRT_ROOT."includes/libdb.php");
include_once($eRC_Rubrics_Addon_PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($eRC_Rubrics_Addon_PATH_WRT_ROOT."includes/libreportcardrubrics.php");
if (!$CallFromSameDomain)
{
	include_once("../check.php");
}

if ($_REQUEST['direct_run'] == 1)
	intranet_opendb();
	
	
function updateSchema_eRC_Rubrics($updateMode, $lastSchemaUpdateDate, $schemaInputDate, $schemaDesc, $schemaSql) {
	global $li;
	
	if ($updateMode==2 && strtotime($lastSchemaUpdateDate) > strtotime($schemaInputDate)) {
		
	}
	else {
		echo "<b>[Date: ".$schemaInputDate." deployment]</b><br>";
		echo "<b>[Function: ".$schemaDesc."]</b><br>";
		if (!$li->db_db_query($schemaSql)){
			echo "warning: ".$schemaSql." <br><br>";
		}
		else {
			echo "Ok<br><br>";
		}
	}
}

	
$li = new libdb();
$lreportcard = new libreportcardrubrics();

if ($plugin['ReportCard_Rubrics'])
{
	$SuccessArr = array();
	
	# script : create file directory if folder does not exist
	$lo = new libfilesystem();
	$lo->folder_new($intranet_root."/file/reportcard_rubrics/");
	$lo->folder_new($intranet_root."/file/reportcard_rubrics/templates/");
	
	# Set default active academic year
	$ActiveAcademicYearID = $lreportcard->Get_Active_AcademicYearID();
	
	if ($ActiveAcademicYearID == '')
	{
		$SuccessArr['Set_Default_Active_AcademicYear'] = $lreportcard->Update_Active_AcademicYearID(Get_Current_Academic_Year_ID());
		
		$lreportcard->AcademicYearID = $lreportcard->Get_Active_AcademicYearID();
		$lreportcard->DBName = $lreportcard->Get_Database_Name($lreportcard->AcademicYearID);
	}
	
	?>
	<body>
	<h1>Script : Create eReportCard (Rubrics) Database & Tables</h1>
	<?php
	# Create Database
	$reportcard_db = $lreportcard->DBName;
	if ($li->db_create_db($reportcard_db)){
		print("<p>Initial Database created successfully</p>\n");
	} else {
		print("<p>Initial Database exist already: ".mysql_error()."</p>\n");
	}
	
	$sql = "CREATE TABLE IF NOT EXISTS RC_RUBRICS_LOG (
				LogID int(8) NOT NULL auto_increment,
				Functionality varchar(128) default null,
				UserID int(8) default null,
				LogContent text default null,
				InputDate datetime default null,
				PRIMARY KEY (LogID),
				KEY Functionality (Functionality)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
			";
	$li->db_db_query($sql);
	
	$sql = "SHOW DATABASES LIKE '%".$intranet_db."_DB_REPORT_CARD_RUBRICS_%'";
	$DatabaseAry = $li->returnArray($sql);
	
	for($a=0; $a<sizeof($DatabaseAry);$a++)
	{
		$sql_table = array();
		$sql_alter = array();
		
		$reportcard_db = $DatabaseAry[$a][0];
		
		// ignore backup DB like "intranet_DB_REPORT_CARD_2007_UTF82"
		$tmpDBNameArr = explode('_', $reportcard_db);
		$numOfDBNamePart = count($tmpDBNameArr);
		$targetCheckingText = $tmpDBNameArr[$numOfDBNamePart - 1];
		if (strtolower(substr($targetCheckingText, 0, 4)) == 'utf8')
			continue;
			
			
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MODULE (
				ModuleID int(11) NOT NULL auto_increment,
				ModuleCode varchar(128) Default Null,
				ModuleNameEn varchar(128) Default Null,
				ModuleNameCh varchar(128) Default Null,
				YearTermID int(11) Default Null,
				StartDate date Default Null,
				EndDate date Default Null,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (ModuleID),
				KEY ModuleCode (ModuleCode),
				KEY YearTermID (YearTermID),
				KEY RecordStatus (RecordStatus)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_RUBRICS (
				RubricsID int(11) NOT NULL auto_increment,
				RubricsCode varchar(128) Default Null,
				RubricsEn varchar(128) Default Null,
				RubricsCh varchar(128) Default Null,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (RubricsID),
				KEY RubricsCode (RubricsCode),
				KEY RecordStatus (RecordStatus)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_RUBRICS_ITEM (
				RubricsItemID int(11) NOT NULL auto_increment,
				RubricsID int(11) Default Null,
				Level tinyint(3) Default Null,
				LevelNameEn varchar(128) Default Null,
				LevelNameCh varchar(128) Default Null,
				DescriptionEn text Default Null,
				DescriptionCh text Default Null,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (RubricsItemID),
				KEY RubricsID (RubricsID),
				KEY Level (Level)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_RUBRICS_SUBJECT_MAPPING (
				RubricsSubjectMappingID int(11) NOT NULL auto_increment,
				RubricsID int(11) Default Null,
				SubjectID int(11) Default Null,
				YearID int(11) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (RubricsSubjectMappingID),
				UNIQUE KEY SubjectRubricsMapping (SubjectID, RubricsID, YearID),
				KEY RubricsID (RubricsID),
				KEY YearID (YearID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TOPIC (
				TopicID int(11) NOT NULL auto_increment,
				TopicCode varchar(128) Default Null,
				TopicNameEn varchar(128) Default Null,
				TopicNameCh varchar(128) Default Null,
				Level tinyint(3) Default Null,
				DisplayOrder tinyint(3) Default Null,
				PreviousLevelTopicID int(11) Default Null,				
				ParentTopicID int(11) Default Null,
				SubjectID int(11) Default Null,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (TopicID),
				KEY SubjectID (SubjectID),
				KEY PreviousLevelTopicID (PreviousLevelTopicID),
				KEY ParentTopicID (ParentTopicID),
				KEY Level (Level),
				KEY RecordStatus (RecordStatus)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_STUDY_TOPIC (
				StudentStudyTopicID int(11) NOT NULL auto_increment,
				StudentID int(11) Default Null,
				ModuleID int(11) Default Null,
				SubjectID int(11) Default Null,
				SubjectGroupID int(11) Default Null,
				TopicID int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (StudentStudyTopicID),
				UNIQUE KEY StudentSubjectTopic (StudentID, SubjectGroupID, ModuleID, SubjectID, TopicID),
				KEY SubjectGroupID (SubjectGroupID),
				KEY ModuleID (ModuleID),
				KEY SubjectID (SubjectID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_STUDY_TOPIC_BACKUP (
				StudentStudyTopicID int(11) NOT NULL auto_increment,
				StudentID int(11) Default Null,
				ModuleID int(11) Default Null,
				SubjectID int(11) Default Null,
				SubjectGroupID int(11) Default Null,
				TopicID int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (StudentStudyTopicID),
				KEY StudentID (StudentID),
				KEY SubjectGroupID (SubjectGroupID),
				KEY ModuleID (ModuleID),
				KEY SubjectID (SubjectID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_SCORE (
				MarksheetScoreID int(11) NOT NULL auto_increment,
				StudentID int(11) Default Null,
				ModuleID int(11) Default Null,
				SubjectID int(11) Default Null,
				TopicID int(11) Default Null,
				RubricsID int(11) Default Null,
				RubricsItemID int(11) Default Null,
				Level tinyint(3) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (MarksheetScoreID),
				UNIQUE KEY TopicCode (StudentID, ModuleID, SubjectID, TopicID),
				KEY ModuleID (ModuleID),
				KEY SubjectID (SubjectID),
				KEY TopicID (TopicID),
				KEY RubricsItemID (RubricsItemID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MARKSHEET_SCORE_LOG (
				MarksheetScoreLogID int(11) NOT NULL auto_increment,
				StudentID int(11) Default Null,
				ModuleID int(11) Default Null,
				SubjectID int(11) Default Null,
				TopicID int(11) Default Null,
				FromRubricsID int(11) Default Null,
				ToRubricsID int(11) Default Null,
				FromRubricsItemID int(11) Default Null,
				ToRubricsItemID int(11) Default Null,
				FromLevel tinyint(3) Default Null,
				ToLevel tinyint(3) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (MarksheetScoreLogID),
				KEY StudentID (StudentID),
				KEY ModuleID (ModuleID),
				KEY SubjectID (SubjectID),
				KEY TopicID (TopicID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE (
				ReportID int(11) NOT NULL auto_increment,
				ReportTitleEn varchar(255) Default Null,
				ReportTitleCh varchar(255) Default Null,
				ReportType varchar(3) Default Null,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (ReportID),
				KEY RecordStatus (RecordStatus),
				KEY ReportType (ReportType)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_REPORT_TEMPLATE_MODULE_MAPPING (
				ReportFormMappingID int(11) NOT NULL auto_increment,
				ReportID int(11) Default Null,
				ModuleID int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (ReportFormMappingID),
				KEY ReportID (ReportID),
				KEY ModuleID (ModuleID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_ECA_CATEGORY (
				EcaCategoryID int(11) NOT NULL auto_increment,
				EcaCategoryCode varchar(128) Default Null,
				EcaCategoryNameEn varchar(128) Default Null,
				EcaCategoryNameCh varchar(128) Default Null,
				RecordStatus tinyint(3) Default 1,
				DisplayOrder int(11) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (EcaCategoryID),
				KEY RecordStatus (RecordStatus),
				KEY EcaCategoryCode (EcaCategoryCode)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";

		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_ECA_SUBCATEGORY (
				EcaSubCategoryID int(11) NOT NULL auto_increment,
				EcaCategoryID int(11) NOT NULL,
				EcaSubCategoryCode varchar(128) Default Null,
				EcaSubCategoryNameEn varchar(128) Default Null,
				EcaSubCategoryNameCh varchar(128) Default Null,
				RecordStatus tinyint(3) Default 1,
				DisplayOrder int(11) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (EcaSubCategoryID),
				KEY RecordStatus (RecordStatus),
				KEY EcaSubCategoryCode (EcaSubCategoryCode),
				KEY EcaCategoryID (EcaCategoryID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";	
			
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_ECA_ITEM (
				EcaItemID int(11) NOT NULL auto_increment,
				EcaSubCategoryID int(11) Default Null,
				EcaItemCode varchar(128) Default Null,
				EcaItemNameEn varchar(128) Default Null,
				EcaItemNameCh varchar(128) Default Null,
				RecordStatus tinyint(3) Default 1,
				DisplayOrder int(11) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (EcaItemID),
				KEY RecordStatus (RecordStatus),
				KEY EcaSubCategoryID (EcaSubCategoryID),
				KEY EcaItemCode (EcaItemCode)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_ECA (
				StudentEcaID int(11) NOT NULL auto_increment,
				StudentID int(11) Default Null,
				ReportID int(11) Default Null,
				EcaItemID int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (StudentEcaID),
				KEY StudentID (StudentID),
				KEY ReportID (ReportID),
				KEY EcaItemID (EcaItemID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_SERVICE (
				ServiceID int(11) NOT NULL auto_increment,
				ServiceCode varchar(128) Default Null,
				ServiceNameEn varchar(128) Default Null,
				ServiceNameCh varchar(128) Default Null,
				RecordStatus tinyint(3) Default 1,
				DisplayOrder int(11) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (ServiceID),
				KEY RecordStatus (RecordStatus),
				KEY ServiceCode (ServiceCode)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_STUDENT_SERVICE (
				StudentEcaID int(11) NOT NULL auto_increment,
				StudentID int(11) Default Null,
				ReportID int(11) Default Null,
				ServiceID int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (StudentEcaID),
				KEY StudentID (StudentID),
				KEY ReportID (ReportID),
				KEY ServiceID (ServiceID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_COMMENT_BANK (
				CommentID int(11) NOT NULL auto_increment,
				SubjectID int(11) Default Null,
				CommentCode varchar(128) Default Null,
				CommentEn text Default Null,
				CommentCh text Default Null,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (CommentID),
				KEY RecordStatus (RecordStatus),
				KEY CommentCode (CommentCode),
				KEY SubjectID (SubjectID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEACHER_COMMENT (
				TeacherCommentID int(11) NOT NULL auto_increment,
				StudentID int(11) Default Null,
				ReportID int(11) Default Null,
				SubjectID int(11) Default Null,
				Comment text Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (TeacherCommentID),
				KEY StudentID (StudentID),
				KEY ReportID (ReportID),
				KEY SubjectID (SubjectID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_OTHER_INFO_CATEGORY (
				CategoryID int(11) NOT NULL auto_increment,
				CategoryCode varchar(128) Default Null,
				CategoryNameEn varchar(128) Default Null,
				CategoryNameCh varchar(128) Default Null,
				SchoolName varchar(128) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (CategoryID),
				KEY SchoolCategoryCode (SchoolName, CategoryCode),
				KEY SchoolName (SchoolName)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";

		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_OTHER_INFO_ITEM (
				ItemID int(11) NOT NULL auto_increment,
				ItemCode varchar(128) Default Null,
				ItemNameEn varchar(128) Default Null,
				ItemNameCh varchar(128) Default Null,
				RecordType int(1) Default Null,
				Length int(11) Default Null,
				Sample1 text  Default Null,
				Sample2 text  Default Null,
				Sample3 text  Default Null,
				CategoryID int(11) NOT NULL,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (ItemID),
				KEY CategoryID (CategoryID),
				KEY ItemCode (ItemCode)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";		
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_OTHER_INFO_STUDENT_RECORD (
				RecordID int(11) NOT NULL auto_increment,
				ReportID int(11) NOT NULL,
				ItemID int(11) NOT NULL,
				StudentID int(11) NOT NULL,
				Information text Default Null,
				CategoryID int(11) NOT NULL,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				PRIMARY KEY (RecordID),
				KEY ReportID (ReportID),
				KEY StudentID (StudentID),
				KEY ItemID (ItemID),
				KEY CategoryID (CategoryID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_MODULE_SUBJECT (
				ModuleSubjectID int(11) NOT NULL auto_increment,
				ModuleID int(11) NOT NULL,
				SubjectID int(11) NOT NULL,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (ModuleSubjectID),
				KEY ModuleSubject (ModuleID, SubjectID),
				KEY SubjectID (SubjectID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE $reportcard_db.RC_REPORT_TEMPLATE_SETTINGS (
			    SettingsID int(11) NOT NULL auto_increment,
				SettingsCode varchar(128) default NULL,
			    SettingsNameEn varchar(128) default NULL,
				SettingsNameCh varchar(128) default NULL,
				DisplayOrder int(11) Default Null, 
			    SettingsValue text,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
			    DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
			    PRIMARY KEY (SettingsID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE $reportcard_db.TEMP_IMPORT_TOPIC (
			    TempID int(11) NOT NULL auto_increment,
				ImportUserID int(11),
				RowNumber int(11),
				SubjectID int(11),
				SubjectCode varchar(20),
				SubjectNameEn varchar(255),
				SubjectNameCh varchar(255),
				TopicID_1 int(11),
				TopicCode_1 varchar(128),
				TopicNameEn_1 varchar(128),
				TopicNameCh_1 varchar(128),
				TopicRemarks_1 text,
				TopicID_2 int(11),
				TopicCode_2 varchar(128),
				TopicNameEn_2 varchar(128),
				TopicNameCh_2 varchar(128),
				TopicRemarks_2 text,
				TopicID_3 int(11),
				TopicCode_3 varchar(128),
				TopicNameEn_3 varchar(128),
				TopicNameCh_3 varchar(128),
				TopicRemarks_3 text,
				DateInput datetime,
				PRIMARY KEY (TempID),
				KEY ImportUserID (ImportUserID),
				KEY RowNumber (RowNumber)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_ADMIN_GROUP (
				AdminGroupID int(11) NOT NULL auto_increment,
				AdminGroupCode varchar(128) Default Null,
				AdminGroupNameEn varchar(128) Default Null,
				AdminGroupNameCh varchar(128) Default Null,
				DisplayOrder int(11) Default Null,
				RecordStatus tinyint(3) Default 1,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (AdminGroupID),
				KEY RecordStatus (RecordStatus),
				KEY AdminGroupCode (AdminGroupCode)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_ADMIN_GROUP_RIGHT (
				AdminGroupRightID int(11) NOT NULL auto_increment,
				AdminGroupID int(11) Default Null,
				AdminGroupRightName varchar(128) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (AdminGroupRightID),
				KEY AdminGroupRight (AdminGroupID, AdminGroupRightName),
				KEY AdminGroupRightName (AdminGroupRightName)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_ADMIN_GROUP_USER (
				AdminGroupUserID int(11) NOT NULL auto_increment,
				AdminGroupID int(11) Default Null,
				UserID int(11) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (AdminGroupUserID),
				KEY AdminGroupUser (AdminGroupID, UserID),
				KEY UserID (UserID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] = 
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TOPIC_APPLICABLE_FORM_MAPPING (
				TopicApplicableFormMappingID int(11) NOT NULL auto_increment,
				TopicID int(11) Default Null,
				YearID int(11) Default Null,
				DateInput datetime Default Null,
				InputBy int(11) Default Null,
				DateModified datetime Default Null,
				LastModifiedBy int(11) Default Null,
				PRIMARY KEY (TopicApplicableFormMappingID),
				UNIQUE KEY TopicFormMapping (TopicID, YearID),
				KEY YearID (YearID)
			)ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		$sql_table[] =
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.TEMP_IMPORT_STUDENT_ECA (
        		TempID int(11) NOT NULL auto_increment,
        		ImportUserID int(11),
    		    RowNumber int(11),
        		StudentID int(11),
        		ItemID int(11),
                isSelected tinyint(1) DEFAULT 0,
        		DateInput datetime,
        		PRIMARY KEY (TempID),
        		KEY ImportUserID (ImportUserID),
        		KEY RowNumber (RowNumber)
    		) ENGINE=InnoDB DEFAULT CHARSET=utf8
    		";
		
		$sql_table[] =
		"	CREATE TABLE IF NOT EXISTS $reportcard_db.TEMP_IMPORT_MARKSHEET_SCORE (
        		TempID int(11) NOT NULL auto_increment,
        		ImportUserID int(11),
        		RowNumber int(11),
                StudentID int(11) DEFAULT NULL,
                ModuleID int(11) DEFAULT NULL,
                SubjectID int(11) DEFAULT NULL,
                TopicID int(11) DEFAULT NULL,
                RubricsID int(11) DEFAULT NULL,
                RubricsItemID int(11) DEFAULT NULL,
                Level tinyint(3) DEFAULT NULL,
                Remarks varchar(255) DEFAULT NULL,
        		DateInput datetime,
        		PRIMARY KEY (TempID),
        		KEY ImportUserID (ImportUserID),
        		KEY RowNumber (RowNumber)
    		) ENGINE=InnoDB DEFAULT CHARSET=utf8
		";
		
		// 2012-03-06 [Please follow this format afterwards]
//		$sql_table[] = array(
//			"2012-03-06",
//			"Create test eRC DB schema table",
//			"CREATE TABLE IF NOT EXISTS $reportcard_db.RC_TEMP (
//			     TempID int(8) NOT NULL auto_increment,
//			     PRIMARY KEY (TempID)
//			) ENGINE=InnoDB DEFAULT CHARSET=utf8"
//		);
			
		print("<p>Creating tables for ".$DatabaseAry[$a][0]."...</p>\n");
		
		# create new tables
		$numOfCreateTable = count((array)$sql_table);
		for($i=0; $i<$numOfCreateTable; $i++){
			$sql = $sql_table[$i];
			
			if (is_array($sql)) {
				// new logic => have input date, desc, and the sql statement
				updateSchema_eRC_Rubrics($flag, $last_schema_date_updated, $sql[0], $sql[1], $sql[2]);
			}
			else {
				// old logic => only have the sql statement
				if ($flag==2) {
					// will not include old schema update for "update new schema only"
				}
				else {
					if($li->db_db_query($sql)){
						echo "<p>Created (or skip creating) a new table:<br />".$sql."</p>\n";
					} else {
						echo "<p>Failed to create table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
					}
				}
			}
		}
		
		
		### 20100804 Ivan - add Marksheet Submission Schedule info for Module
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MODULE Add Column MarksheetSubmissionStartDate Date Default Null After EndDate;";
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_MODULE Add Column MarksheetSubmissionEndDate Date Default Null After MarksheetSubmissionStartDate;";
		
		$sql_alter[] = "ALTER TABLE $reportcard_db.RC_OTHER_INFO_CATEGORY DROP KEY SchoolCategoryCode, ADD UNIQUE KEY SchoolCategoryCode (SchoolName, CategoryCode), ADD KEY SchoolCategoryID (SchoolName, CategoryID)";
		
		### 20101027 Ivan - Add Remarks field for each mark input
		$sql_alter[] = "Alter Table $reportcard_db.RC_MARKSHEET_SCORE Add Column Remarks varchar(255) Default Null After Level;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_MARKSHEET_SCORE_LOG Add Column FromRemarks varchar(255) Default Null After ToLevel;";
		$sql_alter[] = "Alter Table $reportcard_db.RC_MARKSHEET_SCORE_LOG Add Column ToRemarks varchar(255) Default Null After FromRemarks;";
		
		### 20101112 Ivan - Add Remarks field for Learning Topic
		$sql_alter[] = "Alter Table $reportcard_db.RC_TOPIC Add Column Remarks text Default Null After TopicNameCh;";
		
		### 20110221 Ivan - Add ApplicableFormList field for Learning Topic Import
		$sql_alter[] = "Alter Table $reportcard_db.TEMP_IMPORT_TOPIC Add ApplicableFormIDList text Default Null After TopicRemarks_3;";
		$sql_alter[] = "Alter Table $reportcard_db.TEMP_IMPORT_TOPIC Add ApplicableFormNameList text Default Null After ApplicableFormIDList;";
		
		### 20111129 Ivan - Change SubjectID to TopicID for Subject Teacher Comment
		$sql_alter[] = "Alter Table $reportcard_db.RC_COMMENT_BANK CHANGE SubjectID TopicID INT(11) DEFAULT NULL;";
		
		// 2012-03-06 [Please follow this format afterwards]
//		$sql_alter[] = array(
//			"2012-03-06",
//			"Drop test eRC DB schema table",
//			"Drop Table $reportcard_db.RC_TEMP"
//		);
		
		$sql_alter[] = array(
			"2013-01-24",
			"Change field SubjectID to TopicID in table RC_TEACHER_COMMENT",
			"Alter Table $reportcard_db.RC_TEACHER_COMMENT CHANGE SubjectID TopicID INT(11) DEFAULT NULL;"
		);
		
		
		# alter tables
//		for($i=0; $i<sizeof($sql_alter); $i++){
//			$sql = $sql_alter[$i];
//			if($li->db_db_query($sql)){
//				echo "<p>Altered table: ".$sql."</p>\n";
//			} else {
//				echo "<p>Failed to alter table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
//			}
//		}
		$numOfAlterSql = count((array)$sql_alter);
		for($i=0; $i<$numOfAlterSql; $i++){
			$sql = $sql_alter[$i];
			
			if (is_array($sql)) {
				// new logic => have input date, desc, and the sql statement
				updateSchema_eRC_Rubrics($flag, $last_schema_date_updated, $sql[0], $sql[1], $sql[2]);
			}
			else {
				// old logic => only have the sql statement
				if ($flag==2) {
					// will not include old schema update for "update new schema only"
				}
				else {
					if($li->db_db_query($sql)){
						echo "<p>Altered table: ".$sql."</p>\n";
					} else {
						echo "<p>Failed to alter table:<br />".$sql."<br />Error: ".mysql_error()."</p>\n";
					}
				}
			}
		}
		
		print("<p>=============================== Finish Creating tables for ".$DatabaseAry[$a][0]."... =============================== </p><br /><br />\n");
	}
	?>
	</body>
	<? 
		if ($_REQUEST['direct_run'] == 1) 
			intranet_closedb(); 
	?>
<? } else { ?>
	<html>
	<body>
		The client has not purchased eRC (Rubrics).
	</body>
	</html>
<? } ?>