<?php
# Editing by
/*
 * 	edit in utf8
 * 
 *	2017-08-22 Cameron
 *		- create this file
 * 
 */ 
@SET_TIME_LIMIT(1000);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

if (!$CallFromSameDomain)
{
	include_once("../check.php");
}

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

if ($plugin['eGuidance'])
{
	$li = new libdb();
	
	if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
		$isApply = false;
		echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
	}
	else {
		$isApply = true;
	}

	if ($isApply) {
		$result = array();
		
		$sql_data = array();
		
		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_ADJUST_TYPE (TypeID,EnglishName,ChineseName) VALUES
(1,'Homework Adjustment','功課調適'),
(2,'Dictation Adjustment','默書調適'),
(3,'Exam Adjustment(Independent)','考試調適(抽離)'),
(4,'Exam Adjustment(in classroom)','考試調適(留在課室進行)')";
		
		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_ADJUST_ITEM (ItemID,TypeID,EnglishName,ChineseName,WithText) VALUES 
(1,1,'Chinese','中',1),
(2,1,'English','英',1),
(3,1,'Maths','數',1),
(4,1,'General Studies','常',1)";
		
		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_ADJUST_ITEM (ItemID,TypeID,EnglishName,ChineseName) VALUES
(5,2,'Chinese','中'),
(6,2,'English','英'),
(7,3,'Extra Time','加時'),
(8,3,'Read Exam Paper (Maths & GS)','讀卷(數&常)'),
(9,3,'Enlarge Exam Paper','放大試卷'),
(10,3,'Independent Exam Room','獨立考室'),
(11,4,'Sit in front','坐於最前'),
(12,4,'Knock desk when lose focus','不專注時輕敲桌面'),
(13,4,'Remind on every 15 minutes','每15分鐘提醒餘下時間'),
(14,4,'Remind to read question carefully','需要時提醒學生小心閱讀題目')";
		
		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_SERVICE_TYPE (TypeID,EnglishName,ChineseName,Type) VALUES
(1,'Intensive Tutorial','加強輔導','Study'),
(2,'Tutorial','輔導','Study'),
(3,'Other','其他','Study'),
(4,'Emotional','情緒','Other'),
(5,'Social','社交','Other'),
(6,'Selfmanagement','自理','Other'),
(7,'Other','其他','Other')";
		
		$sql_data[] = "truncate INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE";
		
		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE (ServiceID,TypeID,EnglishName,ChineseName,WithText) VALUES 
(1,1,'Chinese','中',0),
(2,1,'English','英',0),
(3,1,'Maths','數',0),
(4,2,'Chinese','中',0),
(5,2,'English','英',0),
(6,2,'Maths','數',0),
(7,3,'Language Improvement','言語治療',0),
(8,3,'Homework Tutorial','功課輔導',0),
(9,3,'Kind Angel','愛心天使',0),
(10,3,'Study Skill Training Group','學習技巧訓練小組',0),
(11,3,'Little Teacher (on rest)','小老師(小息)',0),
(12,3,'Volunteer of Parent (Lunch)','家長義工(午膳)',0),
(13,5,'Social Skill Training Group','社交技巧訓練小組',0),
(14,6,'Smart Kids Growing Group','Smart Kids成長小組',0),
(15,6,'Smart Kids Star Group','Smart Kids星級小組',0),
(16,4,'Mindfulness Training Group','「童‧飛躍」專注力訓練小組',0),
(17,7,'5S Special Group','5S特功隊',0),
(18,7,'Sky of Growing','成長的天空',0),
(19,7,'Accompanying Scheme','喜伴同行計劃',0),
(20,7,'Mentor Scheme','師友計劃',0),
(21,7,'Cherish Scheme','惜福兵團',0),
(22,5,'\"LEGO\" Communication Group','「LEGO 溝通樂」小組',0)";
		
		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_ITEM (Form, Type, Code, EnglishName, ChineseName, SeqNo) VALUES
('Guidance','ContactStage','Stage_1','Build relationship','建立關係',1),
('Guidance','ContactStage','Stage_2','Get idea and thinking','檢查信念及想法',6),
('Guidance','ContactStage','Stage_3','Object to change and action stage','駁斥認知改變及行動階段',11),
('Guidance','ContactStage','Stage_4','Build new belief stage','建立新信念階段',16),
('Guidance','CaseType','Emotion','Emotional Problem','情緒問題',1),
('Guidance','CaseType','Behaviour','Behaviour Problem','行為問題',6),
('Guidance','CaseType','Study','Study Problem','學業問題',11),
('Guidance','CaseType','Family','Family Problem','家庭',16),
('Guidance','CaseType','Peer','Peer Relationship Problem','朋輩關係',21),
('Guidance','CaseType','Other','Other Problem','其他',26),
('Guidance','FollowupAdvice','Continue','Continue','繼續跟進',1),
('Guidance','FollowupAdvice','Suspend','Suspend','暫緩跟進',6),
('Guidance','FollowupAdvice','Stop','Not need to follow up','毋須跟進',11),
('Guidance','FollowupAdvice','ContactParent','Contact Parent','與家長會面',16),
('Guidance','FollowupAdvice','ByTeacher','Monitored by class teacher or subject teacher','請班主任或科任老師觀察學生情況',21),
('Guidance','FollowupAdvice','BySocialWorker','Referral to social worker','轉介社工',26),
('Guidance','FollowupAdvice','ByPsychologist','Advise social worker refer the case to education psychologist','建議社工將個案轉介教育心理學家',31),
('Guidance','FollowupAdvice','ByPsychiatrist','Advise to see psychiatrist','建議同學看精神料醫生',36),
('Guidance','FollowupAdvice','Other','Other','其他',41),
('Personal','ReferralFrom','Teacher','Teacher','教職員轉介',1),
('Personal','ReferralFrom','AcademicGroup','Academic Group','學務組轉介',6),
('Personal','ReferralFrom','DisciplineGroup','Discipline Group','訓導組轉介',11),
('Personal','ReferralFrom','FromOther','Other','其他',16),
('Personal','RefAcaReason','Repeater','Repeater','重讀生',1),
('Personal','RefAcaReason','LastThree','Last three in the form','全級倒數三名',6),
('Personal','RefAcaReason','AcaOther','Other','其他',11),
('Personal','RefDisReason','OftenLate','Often late for school','經常遲到',1),
('Personal','RefDisReason','ContAbsent','Continuously absent for three days or above','連續三日或以上缺席',6),
('Personal','RefDisReason','DisOther','Other','其他',11),
('Personal','ParentStatus','Normal','Normal Family','完好家庭',1),
('Personal','ParentStatus','Separate','Separated','分居',6),
('Personal','ParentStatus','Devoice','Devoiced','離婚',11),
('Personal','ParentStatus','Abandon','Abandoned','父/母被遺棄',16),
('Personal','ParentStatus','Widow','Widow/Widower','鰥寡',21),
('Personal','ParentStatus','LiveWithOther','Father/Mother Live with other','父或母與人同居',26),
('Personal','ParentStatus','PSOther','Other','其他',31),
('Personal','LiveWith','Father','Father','父親',1),
('Personal','LiveWith','Mother','Mother','母親',6),
('Personal','LiveWith','StepParent','Step Parent','繼父/母',11),
('Personal','LiveWith','ParentPartner','Parent\'s Partner','父/母的同居伴侶',16),
('Personal','LiveWith','GrandFather','Grand Father','祖父',21),
('Personal','LiveWith','GrandMother','Grand Mother','祖母',26),
('Personal','LiveWith','BrotherAndSister','Brother and Sister','兄弟姐妹',31),
('Personal','LiveWith','Relative','Relative','親屬',36),
('Personal','LiveWith','Single','Single','獨居',41),
('Personal','LiveWith','Friend','Friend','朋友',46),
('Personal','LiveWith','Spouse','Spouse / Partner','配偶/同居伴侶',51),
('Personal','WorkingStatus','FullTime','Full Time','全職',1),
('Personal','WorkingStatus','PartTime','Part Time','兼職',6),
('Personal','WorkingStatus','Unemployment','Unemployment','失業',11),
('Personal','WorkingStatus','Retired','Retired','退休',16),
('Personal','WorkingStatus','Unstable','Unstable','工作不穩定',21),
('Personal','Category','Health','Health','健康',1),
('Personal','Category','Study','Study/Education','學習/教育',6),
('Personal','Category','Peer','Peer','朋輩',11),
('Personal','Category','Grow','Difficulty in growing','成長過程上適應的困難',16),
('Personal','Category','Emotion','Emotion','情緒',21),
('Personal','Category','Sex','Sex','性方面',26),
('Personal','Category','Behavior','Behavior','行為',31),
('Personal','Category','Family','Family','家庭',36),
('Personal','Category','Other','Other','其他',41),
('Personal','Health','PhysicalDisability','肢體殘缺障','肢體殘缺障',1),
('Personal','Health','SuspectMental','懷疑/証實患上精神病','懷疑/証實患上精神病',6),
('Personal','Health','Illness','患病','患病',11),
('Personal','Study','LackofStudySkill','學習技巧不足','學習技巧不足',1),
('Personal','Study','NoStudyMotivation','欠缺學習動機','欠缺學習動機',6),
('Personal','Study','Absent','缺課','缺課',11),
('Personal','Study','Truancy','逃學','逃學',16),
('Personal','Study','Drop','輟學','輟學',21),
('Personal','Study','Adaption','學校適應','學校適應',26),
('Personal','Study','TeacherAndStudent','師生問題','師生問題',31),
('Personal','Study','Parttime','因兼職而影響學習','因兼職而影響學習',36),
('Personal','Peer','BadFriend','受不良友儕影響','受不良友儕影響',1),
('Personal','Peer','LackOfSocialSkill','欠缺朋輩社交技巧','欠缺朋輩社交技巧',6),
('Personal','Peer','PeerConflict','朋輩之間有衝突','朋輩之間有衝突',11),
('Personal','Peer','DifferentSex','與異性朋友相處問題','與異性朋友相處問題',16),
('Personal','Grow','PhysicalChange','青少年生理轉變方面的適應','青少年生理轉變方面的適應',1),
('Personal','Grow','Value','價值觀/身份迷惘','價值觀/身份迷惘',6),
('Personal','Grow','LowerSelfesteem','低自尊感','低自尊感',11),
('Personal','Grow','Love','愛情問題','愛情問題',16),
('Personal','Emotion','Suicide','曾經自殺/有自殺傾向','曾經自殺/有自殺傾向',1),
('Personal','Emotion','UnstableEmotion','情緒不穩定','情緒不穩定',6),
('Personal','Emotion','Inhibition','抑制/退縮行為','抑制/退縮行為',11),
('Personal','Emotion','SuspectEmotionalDisease','懷疑情緒病','懷疑情緒病',16),
('Personal','Emotion','Worry','在壓力下極度憂慮','在壓力下極度憂慮',21),
('Personal','Sex','Ppen','性開放','性開放',1),
('Personal','Sex','Aid','援交','援交',6),
('Personal','Sex','PremaritalSex','婚前性行為','婚前性行為',11),
('Personal','Sex','UnmarriedPregnancy','未婚懷孕','未婚懷孕',16),
('Personal','Sex','SexualAssault','被性侵犯','被性侵犯',21),
('Personal','Sex','Homosexuality','同性戀','同性戀',26),
('Personal','Sex','SexualCriminal','與性有關的刑事罪行','與性有關的刑事罪行',31),
('Personal','Behavior','DamageInSchool','在學校有破壞問題','在學校有破壞問題',1),
('Personal','Behavior','ViolateSchoolRule','違反校規','違反校規',6),
('Personal','Behavior','ViolentTreatment','暴力對待人物或財物','暴力對待人物或財物',11),
('Personal','Behavior','Nightout','經常徹夜不歸/深宵外出','經常徹夜不歸/深宵外出',16),
('Personal','Behavior','Pornography','經常光顧/任職色情場所','經常光顧/任職色情場所',21),
('Personal','Behavior','Triad','參與朋黨/黑社會','參與朋黨/黑社會',26),
('Personal','Behavior','Drug','吸毒/藏毒/運毒','吸毒/藏毒/運毒',31),
('Personal','Behavior','Steal','偷竊','偷竊',36),
('Personal','Behavior','LeaveHome','失蹤/離家出走','失蹤/離家出走',41),
('Personal','Behavior','CriminalOffense','其他刑事罪行','其他刑事罪行',46),
('Personal','Behavior','Cults','參與邪教','參與邪教',51),
('Personal','Family','BrotherSister','與兄弟姐妹的關係','與兄弟姐妹的關係',1),
('Personal','Family','ParentChild','親子關係','親子關係',6),
('Personal','Family','LackOfDiscipline','管教子女技巧不足或不善幼兒照顧問題','管教子女技巧不足或不善幼兒照顧問題',11),
('Personal','Family','ChildAbuse','虐兒/懷疑虐兒','虐兒/懷疑虐兒',16),
('Personal','Family','ParentalMarriage','父母婚姻問題','父母婚姻問題',21),
('Personal','Family','FamilyMember','與學生其他家庭成員有關的問題','與學生其他家庭成員有關的問題',26),
('Personal','Family','Living','住屋問題','住屋問題',31),
('Personal','Family','EconomicDifficulties','經濟困難','經濟困難',36),
('Personal','Family','FamilyCrisis','對家庭危機的適應','對家庭危機的適應',41),
('Personal','Other','OtherProblem','Other','其他',1),
('Personal','RefToReason','IntSuicide','有自殺念頭','有自殺念頭',1),
('Personal','RefToReason','Punishment','牽涉罰事','牽涉罰事',6),
('Personal','RefToReason','UnstableEmotion','情緒十分波動','情緒十分波動',11),
('Personal','RefToReason','ChildAbuse','牽涉虐兒','牽涉虐兒',16),
('Personal','RefToReason','SuspectMental','懷疑精神病','懷疑精神病',21),
('Personal','RefToReason','ParentRelationship','父母關係的問題','父母關係的問題',26),
('Personal','RefToReason','FamilyProblem','嚴重家庭問題','嚴重家庭問題',31),
('Personal','RefToReason','BadBehavior','嚴重行為問題','嚴重行為問題',36),
('Personal','RefToReason','Bereavement','喪親','喪親',41),
('Personal','RefToReason','RejectContact','學生多次拒絕面談','學生多次拒絕面談',46),
('Personal','RefToReason','RefToOther','其他','其他',51),
('SENService','ServiceType','SocialEmotion','Emotion & Social','情緒社交',1),
('SENService','ServiceType','StudyMotivation','Study Motivation','學習動機',6),
('SENService','ServiceType','ImproveService','Improve Service','提升服務',11),
('SENService','ServiceType','Language','Improve Language','言語治療',16),
('SENService','ServiceType','ReadWrite','Read & Write','讀寫班',21),
('SENService','ServiceType','Other','Other','其他',26),
('Transfer','TransferTo','InSocialWorker','School Social Worker','校內社工',1),
('Transfer','TransferTo','OutSocialWorker','Social Worker outside school','校外社工',6),
('Transfer','TransferTo','Psychologist','Education Psychologist','教育心理學家',11),
('Transfer','TransferTo','AbsentGroup','Absent Group','缺課組',16),
('Transfer','TransferTo','MedicalStaff','Medical Staff','醫護',21),
('Transfer','TransferTo','ToOther','Other','其他',26),
('SelfImprove','Result','Successful','Successful','成功',1),
('SelfImprove','Result','Unsuccessful','Unsuccessful','不成功',6),
('SelfImprove','Result','Delay','Delay','延期',11)";
		
		## for Sin Tak
		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_ITEM (Form, Type, Code, EnglishName, ChineseName, SeqNo) VALUES
('Contact','Category','TeacherMeetStudent','Teacher Meet Student','老師接見學生',1),
('Contact','Category','TeacherMeetParent','Teacher Meet Parent','老師接見家長',6),
('Contact','Category','ClassTeacherContactParent','Class Teacher Contact Parent','班主任聯絡家長',11),
('Contact','Category','GuiderMeetStudent','Form Guider Meet Student','級訓輔接見學生',16),
('Transfer','TransferFrom','DisciplineTeacher','Discipline Teacher','訓導老師',1),
('Transfer','TransferFrom','Self','Self','自我轉介',6),
('Transfer','TransferFrom','Teacher','Teacher','老師',11),
('Transfer','TransferFrom','SocialWorker','Social Worker','由社工辨識',16),
('Transfer','TransferFrom','Classmate','Classmate','同學',21),
('Transfer','TransferFrom','Organization','Organization','其他機構',26),
('Transfer','TransferFrom','Relation','Parent/Family/Relation','父母/家人/親屬提出',31),
('Transfer','TransferFrom','Other','Other','其他',36),
('Transfer','TransferReason','SEN','SEN','SEN',1),
('Transfer','TransferReason','Study','Study Problem','學習困擾',6),
('Transfer','TransferReason','EmotionUnstable','Unstable Emotion','情緒不穩',11),
('Transfer','TransferReason','EmotionalProblem','Emotional Problem','情緒困擾',16),
('Transfer','TransferReason','Sad','Too Sad','過份哀傷失落',21),
('Transfer','TransferReason','SelfHurt','Hurt by self','自我傷害',26),
('Transfer','TransferReason','FamilyViolence','Family Violence','家庭暴力',31),
('Transfer','TransferReason','FamilyEvent','Family Event','家中出現事故',36),
('Transfer','TransferReason','PeerRelation','Poor Peer Relationship','朋輩關係欠佳',41),
('Transfer','TransferReason','OverdueHomework','Frequently Overdue Homework','經常欠交功課',46),
('Transfer','TransferReason','LateFrequently','Late Frequently','多次遲到',51),
('Transfer','TransferReason','AbsentFrequently','Absent Frequently','缺課嚴重',56),
('Transfer','TransferReason','PoorBehavior','Poor Behavior','偏差行為',61),
('Transfer','TransferReason','Other','Other','其他',66),
('SENCase','SENType','LAA','Low Academic Achievement','成績稍遜',1),
('SENCase','SENType','SLD','Specific Learning Difficulties','特殊學習困難',2),
('SENCase','SENType','ID','Intellectual Disability','智障',3),
('SENCase','SENType','SLI','Speech & Language Impairment','言語障礙',4),
('SENCase','SENType','RD','Reading Disorder (RD)','讀寫障礙',5),
('SENCase','SENType','ASD','Autism Spectrum Disorders (ASD)','自閉症譜系',6),
('SENCase','SENType','HD','Hyperactivity Disorder (HD)','過度活躍症',7),
('SENCase','SENType','AD','Attention Deficit (AD)','專注力缺乏症',8),
('SENCase','SENType','HI','Hearing Impairment (HI)','聽力有特殊需要',9),
('SENCase','SENType','VI','Visual Impairment (VI)','視力有特殊需要',10),
('SENCase','SENType','PD','Physical Disability','肢體有特殊需要',11),
('SENCase','SENType','GF','Gifted','資優有特殊需要',12),
('SENCase','SENType','MH','Mental Health','精神健康',13),
('SENCase','SENType','Other','Other','其他',14)";
		
		## for Tuen Mun Catholic Secondary School
/*		$sql_data[] = "INSERT IGNORE INTO INTRANET_GUIDANCE_SETTING_ITEM (Form, Type, Code, EnglishName, ChineseName, SeqNo) VALUES
('Contact','Category','TeacherMeetStudent','Teacher Meet Student','老師接見學生',1),
('Contact','Category','TeacherMeetParent','Teacher Meet Parent','老師接見家長',6),
('Contact','Category','ClassTeacherContactParent','Teacher Contact Parent','老師聯絡家長',11),
('Contact','Category','GuiderMeetStudent','Form Guider Meet Student','級訓輔接見學生',16),
('Transfer','TransferFrom','Teacher','Teacher','老師',1),
('Transfer','TransferFrom','Parent','Parent','家長',6),
('Transfer','TransferFrom','Self','Self','同學本人',11),
('Transfer','TransferFrom','Other','Other','其他',16),
('Transfer','TransferReason','Study','Study Problem','學習問題',1),
('Transfer','TransferReason','EmotionalProblem','Emotional Problem','情緒問題',6),
('Transfer','TransferReason','PoorBehavior','Behavior Problem','行為問題',11),
('Transfer','TransferReason','PeerRelation','Peer Problem','朋輩問題',16),
('Transfer','TransferReason','FamilyProblem','Family Problem','家庭問題',21),
('Transfer','TransferReason','Attendance','Attendance Problem','出勤',26),
('Transfer','TransferReason','Other','Other','其他',31),
('SENCase','SENType','ADHD','Attention Deficit / Hyperactivity Disorder (ADHD)','過度活躍症',1),
('SENCase','SENType','ASD','Autism Spectrum Disorders (ASD)','自閉症譜系',2),
('SENCase','SENType','RD','Reading Disorder (RD)','讀寫困難',3),
('SENCase','SENType','SLD','Specific Learning Difficulties (SLD)','特殊學習困難',4),
('SENCase','SENType','HI','Hearing Impairment (HI)','聽力有特殊需要',5),
('SENCase','SENType','VI','Visual Impairment (VI)','視力有特殊需要',6),
('SENCase','SENType','ID','Intellectual Disability','學習遲緩',7),
('SENCase','SENType','PD','Physical Disability','肢體有特殊需要',8),
('SENCase','SENType','Other','Other','其他',9)";
*/		
		foreach((array)$sql_data as $sql) {
			$res = $li->db_db_query($sql);
			$result[] = $res;
			if (!$res) {
				$error[] = $sql;	
			}
		}
		
		if (!in_array(false,$result)) {
			echo "<span style=\"color:green; font-weight:bold\">Successfully initialize eGuidance</span><br>";
		}
		else {
			echo "<span style=\"color:red; font-weight:bold\">Fail to initialize eGuidance</span><br>";
			print_r($error);
		}
	}	// isApply
}	// plugin
		
		
if (!$FromCentralUpdate) {
	intranet_closedb();
}	

