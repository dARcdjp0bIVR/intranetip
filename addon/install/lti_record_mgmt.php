<?php 
$PATH_WRT_ROOT = '../../';
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/liblti.php');

intranet_opendb();
$liblti = new liblti();

switch($action){
	case 'add':
		$liblti->addNewLtiApps($school_code, $app_name, $launch_url, $launch_key, $launch_secret);
		break;
	case 'remove':
		$liblti->deleteLtiAppsByRecordID($app_record_id);
		break;
	default:
		break;
}
header('Location: lti_access_control.php');
?>