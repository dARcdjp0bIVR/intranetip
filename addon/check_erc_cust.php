<?php
// editing by 
$PATH_WRT_ROOT = "../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");

intranet_opendb();
if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
	include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
	$libreportcardcustom = new libreportcardcustom();
}
$linterface = new interface_html();


$ShowEnabledOnly = (isset($_GET['ShowEnabledOnly']))? $_GET['ShowEnabledOnly'] : 1;


### Restructure the customization array
$CustArr = array();

# Cust Menu
$CustArr['CustMenu']['Management_ReExam']['Flag'] = '$sys_custom[\'eRC\'][\'Management\'][\'ReExam\']';
$CustArr['CustMenu']['Management_ReExam']['Enabled'] = $sys_custom['eRC']['Management']['ReExam'];
$CustArr['CustMenu']['Management_ReExam']['Description'][] = 'For 東南';

$CustArr['CustMenu']['Management_AcademicProgress']['Flag'] = '$sys_custom[\'eRC\'][\'Management\'][\'AcademicProgress\']';
$CustArr['CustMenu']['Management_AcademicProgress']['Enabled'] = $sys_custom['eRC']['Management']['AcademicProgress'];
$CustArr['CustMenu']['Management_AcademicProgress']['Description'][] = 'For 上智';

$CustArr['CustMenu']['Settings_PromotionCriteria']['Flag'] = '$sys_custom[\'eRC\'][\'Settings\'][\'PromotionCriteria\']';
$CustArr['CustMenu']['Settings_PromotionCriteria']['Enabled'] = $sys_custom['eRC']['Settings']['PromotionCriteria'];
$CustArr['CustMenu']['Settings_PromotionCriteria']['Description'][] = 'For Munsang College';

$CustArr['CustMenu']['Settings_CurriculumExpectation']['Flag'] = '$sys_custom[\'eRC\'][\'Settings\'][\'CurriculumExpectation\']';
$CustArr['CustMenu']['Settings_CurriculumExpectation']['Enabled'] = $sys_custom['eRC']['Settings']['CurriculumExpectation'];
$CustArr['CustMenu']['Settings_CurriculumExpectation']['Description'][] = 'Form-based and Term-based Subject Description input';

$CustArr['CustMenu']['Settings_PersonalCharacteristics']['Flag'] = '$sys_custom[\'eRC\'][\'Settings\'][\'PersonalCharacteristics\']';
$CustArr['CustMenu']['Settings_PersonalCharacteristics']['Enabled'] = $sys_custom['eRC']['Settings']['PersonalCharacteristics'];
$CustArr['CustMenu']['Settings_PersonalCharacteristics']['Description'][] = '';

$CustArr['CustMenu']['Report_ExtraReport']['Flag'] = '$sys_custom[\'eRC\'][\'ExtraReport\']';
$CustArr['CustMenu']['Report_ExtraReport']['Enabled'] = $sys_custom['eRC']['ExtraReport'];
$CustArr['CustMenu']['Report_ExtraReport']['Description'][] = 'For UCCKE, enable Honour Report, Subject Prize Report, Ranking Report, Conduct Award Report and Conduct Progress Report by one flag';

$CustArr['CustMenu']['Report_HonourReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'HonourReport\']';
$CustArr['CustMenu']['Report_HonourReport']['Enabled'] = $sys_custom['eRC']['Report']['HonourReport'];
$CustArr['CustMenu']['Report_HonourReport']['Description'][] = 'Honour Report';

$CustArr['CustMenu']['Report_SubjectPrizeReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'SubjectPrizeReport\']';
$CustArr['CustMenu']['Report_SubjectPrizeReport']['Enabled'] = $sys_custom['eRC']['Report']['SubjectPrizeReport'];
$CustArr['CustMenu']['Report_SubjectPrizeReport']['Description'][] = 'Class-based, Form-based and Subject-Group-based Subject Prize Report';

$CustArr['CustMenu']['Report_RankingReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'RankingReport\']';
$CustArr['CustMenu']['Report_RankingReport']['Enabled'] = $sys_custom['eRC']['Report']['RankingReport'];
$CustArr['CustMenu']['Report_RankingReport']['Description'][] = 'Class-based and Form-based Grand Score Ranking Report';

$CustArr['CustMenu']['Report_ConductAwardReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'ConductAwardReport\']';
$CustArr['CustMenu']['Report_ConductAwardReport']['Enabled'] = $sys_custom['eRC']['Report']['ConductAwardReport'];
$CustArr['CustMenu']['Report_ConductAwardReport']['Description'][] = 'For UCCKE';
$CustArr['CustMenu']['Report_ConductAwardReport']['Description'][] = 'Conduct Award Report';

$CustArr['CustMenu']['Report_ConductProgressReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'ConductProgressReport\']';
$CustArr['CustMenu']['Report_ConductProgressReport']['Enabled'] = $sys_custom['eRC']['Report']['ConductProgressReport'];
$CustArr['CustMenu']['Report_ConductProgressReport']['Description'][] = 'For UCCKE';
$CustArr['CustMenu']['Report_ConductProgressReport']['Description'][] = 'Conduct Progress Report';

$CustArr['CustMenu']['Report_SubMarksheetReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'SubMarksheetReport\']';
$CustArr['CustMenu']['Report_SubMarksheetReport']['Enabled'] = $sys_custom['eRC']['Report']['SubMarksheetReport'];
$CustArr['CustMenu']['Report_SubMarksheetReport']['Description'][] = 'Generate reports from Sub-marksheet Report data';

$CustArr['CustMenu']['Report_AssessmentReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'AssessmentReport\']';
$CustArr['CustMenu']['Report_AssessmentReport']['Enabled'] = $sys_custom['eRC']['Report']['AssessmentReport'];
$CustArr['CustMenu']['Report_AssessmentReport']['Description'][] = 'Generate reports from Assessmenet Data (support without report generate if the Assessment Report does not involve any Ranking logic)';

$CustArr['CustMenu']['Report_ExaminationSummary']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'ExaminationSummary\']';
$CustArr['CustMenu']['Report_ExaminationSummary']['Enabled'] = $sys_custom['eRC']['Report']['ExaminationSummary'];
$CustArr['CustMenu']['Report_ExaminationSummary']['Description'][] = 'For Munsang College';
$CustArr['CustMenu']['Report_ExaminationSummary']['Description'][] = 'A Summary Sheet to let students to check the marks of all Subjects';

$CustArr['CustMenu']['Report_StudentResultSummary']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'StudentResultSummary\']';
$CustArr['CustMenu']['Report_StudentResultSummary']['Enabled'] = $sys_custom['eRC']['Report']['StudentResultSummary'];
$CustArr['CustMenu']['Report_StudentResultSummary']['Description'][] = 'For UCCKE';
$CustArr['CustMenu']['Report_StudentResultSummary']['Description'][] = 'A Summary Sheet to let students to check the marks of all Subjects';

$CustArr['CustMenu']['Report_SubjectSummarySheet']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'SubjectSummarySheet\']';
$CustArr['CustMenu']['Report_SubjectSummarySheet']['Enabled'] = $sys_custom['eRC']['Report']['SubjectSummarySheet'];
$CustArr['CustMenu']['Report_SubjectSummarySheet']['Description'][] = 'Class-based and Subject-Group-based Report showing students\' results for each Subjects';

$CustArr['CustMenu']['Report_GradeDistribution']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'GradeDistribution\']';
$CustArr['CustMenu']['Report_GradeDistribution']['Enabled'] = $sys_custom['eRC']['Report']['GradeDistribution'];
$CustArr['CustMenu']['Report_GradeDistribution']['Description'][] = 'A Form-based and Subject-based report to show the Grade Range Statistics';

$CustArr['CustMenu']['Report_MarkGradeConversion']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'MarkGradeConversion\']';
$CustArr['CustMenu']['Report_MarkGradeConversion']['Enabled'] = $sys_custom['eRC']['Report']['MarkGradeConversion'];
$CustArr['CustMenu']['Report_MarkGradeConversion']['Description'][] = 'A Form-based and Subject-based report mainly for 拉 curve client to show the Mark Range of each Grades';

$CustArr['CustMenu']['Report_MarkPositionConversion']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'MarkPositionConversion\']';
$CustArr['CustMenu']['Report_MarkPositionConversion']['Enabled'] = $sys_custom['eRC']['Report']['MarkPositionConversion'];
$CustArr['CustMenu']['Report_MarkPositionConversion']['Description'][] = 'A Form-based and Subject-based report mainly for 拉 curve client to show the Mark Range of each Rankings';

$CustArr['CustMenu']['Report_RawMarkDistribution']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'RawMarkDistribution\']';
$CustArr['CustMenu']['Report_RawMarkDistribution']['Enabled'] = $sys_custom['eRC']['Report']['RawMarkDistribution'];
$CustArr['CustMenu']['Report_RawMarkDistribution']['Description'][] = 'A Form-based and Subject-based report to show the Statistics of each Classes';

$CustArr['CustMenu']['Report_RemarksReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'RemarksReport\']';
$CustArr['CustMenu']['Report_RemarksReport']['Enabled'] = $sys_custom['eRC']['Report']['RemarksReport'];
$CustArr['CustMenu']['Report_RemarksReport']['Description'][] = 'A Class-based report to show the Class Teacher Comment of students';

$CustArr['CustMenu']['Report_SubjectPercentagePassReport']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'SubjectPercentagePassReport\']';
$CustArr['CustMenu']['Report_SubjectPercentagePassReport']['Enabled'] = $sys_custom['eRC']['Report']['SubjectPercentagePassReport'];
$CustArr['CustMenu']['Report_SubjectPercentagePassReport']['Description'][] = 'A Form-based report to show the Statistics of each Subjects';

$CustArr['CustMenu']['Report_HKUGAGradeList']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'HKUGAGradeList\']';
$CustArr['CustMenu']['Report_HKUGAGradeList']['Enabled'] = $sys_custom['eRC']['Report']['HKUGAGradeList'];
$CustArr['CustMenu']['Report_HKUGAGradeList']['Description'][] = 'For HKUGA College';


# Comment
$CustArr['Comment']['CommentMaxChar']['Flag'] = '$libreportcardcustom->textAreaMaxChar';
$CustArr['Comment']['CommentMaxChar']['Enabled'] = $libreportcardcustom->textAreaMaxChar;
$CustArr['Comment']['CommentMaxChar']['Description'][] = '';
$CustArr['Comment']['CommentMaxChar']['Value'] = $libreportcardcustom->textAreaMaxChar;

$CustArr['Comment']['SubjectTeacherCommentMaxChar']['Flag'] = '$libreportcardcustom->textAreaMaxChar_SubjectTeacherComment';
$CustArr['Comment']['SubjectTeacherCommentMaxChar']['Enabled'] = $libreportcardcustom->textAreaMaxChar_SubjectTeacherComment;
$CustArr['Comment']['SubjectTeacherCommentMaxChar']['Description'][] = '';
$CustArr['Comment']['SubjectTeacherCommentMaxChar']['Value'] = $libreportcardcustom->textAreaMaxChar_SubjectTeacherComment;

$CustArr['Comment']['UnlimitSubjectTeacherComment']['Flag'] = '$eRCTemplateSetting[\'UnlimitSubjectTeacherComment\']';
$CustArr['Comment']['UnlimitSubjectTeacherComment']['Enabled'] = $eRCTemplateSetting['UnlimitSubjectTeacherComment'];
$CustArr['Comment']['UnlimitSubjectTeacherComment']['Description'][] = 'Unlimit the length of the Subject Teacher Comment';

$CustArr['Comment']['ShowCompleteSentence']['Flag'] = '$eRCCommentSetting[\'ShowCompleteSentence\']';
$CustArr['Comment']['ShowCompleteSentence']['Enabled'] = $eRCCommentSetting['ShowCompleteSentence'];
$CustArr['Comment']['ShowCompleteSentence']['Description'][] = 'When choosing comment in the pop-up window, complete sentence of the comment will be shown even if the Comment is too long.';

$CustArr['Comment']['ClassTeacherComment_AddFromComment']['Flag'] = '$sys_custom[\'eRC\'][\'Management\'][\'ClassTeacherComment\'][\'AddFromComment\']';
$CustArr['Comment']['ClassTeacherComment_AddFromComment']['Enabled'] = $sys_custom['eRC']['Management']['ClassTeacherComment']['AddFromComment'];
$CustArr['Comment']['ClassTeacherComment_AddFromComment']['Description'][] = 'Cust for 上智';
$CustArr['Comment']['ClassTeacherComment_AddFromComment']['Description'][] = 'Allow to add a Comment to multiple students';

$CustArr['Comment']['ClassTeacherComment_AdditionalComment']['Flag'] = '$eRCTemplateSetting[\'ClassTeacherComment_AdditionalComment\']';
$CustArr['Comment']['ClassTeacherComment_AdditionalComment']['Enabled'] = $eRCTemplateSetting['ClassTeacherComment_AdditionalComment'];
$CustArr['Comment']['ClassTeacherComment_AdditionalComment']['Description'][] = 'An extra field "Additional Comment" in Class Teacher Comment';


# Grading Scheme
$CustArr['GradingScheme']['AdditionalCriteria']['Flag'] = '$eRCTemplateSetting[\'AdditionalCriteria\']';
$CustArr['GradingScheme']['AdditionalCriteria']['Enabled'] = $eRCTemplateSetting['AdditionalCriteria'];
$CustArr['GradingScheme']['AdditionalCriteria']['Description'][] = 'For Munsang College';

$CustArr['GradingScheme']['SelectSubjectDisplayLang']['Flag'] = '$eRCTemplateSetting[\'SelectSubjectDisplayLang\']';
$CustArr['GradingScheme']['SelectSubjectDisplayLang']['Enabled'] = $eRCTemplateSetting['SelectSubjectDisplayLang'];
$CustArr['GradingScheme']['SelectSubjectDisplayLang']['Description'][] = 'Settings for the client to choose the display language of the Subject in the Report Card';

$CustArr['GradingScheme']['SelectDisplaySubjectGroup']['Flag'] = '$eRCTemplateSetting[\'SelectDisplaySubjectGroup\']';
$CustArr['GradingScheme']['SelectDisplaySubjectGroup']['Enabled'] = $eRCTemplateSetting['SelectDisplaySubjectGroup'];
$CustArr['GradingScheme']['SelectDisplaySubjectGroup']['Description'][] = 'Settings for the client to choose to display the Subject Group Name in the Report Card or not';

$CustArr['GradingScheme']['GradingSchemeGradeDescription']['Flag'] = '$eRCTemplateSetting[\'GradingSchemeGradeDescription\']';
$CustArr['GradingScheme']['GradingSchemeGradeDescription']['Enabled'] = $eRCTemplateSetting['GradingSchemeGradeDescription'];
$CustArr['GradingScheme']['GradingSchemeGradeDescription']['Description'][] = 'For 宏信書院';
$CustArr['GradingScheme']['GradingSchemeGradeDescription']['Description'][] = 'Max Length = '.$eRCTemplateSetting['GradingSchemeGradeDiscriptionLength'];

$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Flag'] = '$eRCTemplateSetting[\'SpecialGradeConvertingForSmallClassGroup\']';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Enabled'] = $eRCTemplateSetting['SpecialGradeConvertingForSmallClassGroup'];
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = 'For Munsang College';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = 'Scheme: 10% A,   20% B,   40% C,    20% D   , 5% E,   5% F';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = 'No. of A: ceil(6*10%) = 1, so 1 student gets A';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = 'No. of B: ceil(6*20%) = 2, so 2 students get B';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = 'No. of C: ceil(6*40%) = 3, so 3 students get C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = 'Original:';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '% = (Form Ranking / No. of Student Taken the Subject) * 100%';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '1/6 *100% = 16.7% --- over 10%, so grade is B';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '2/6 *100% = 33.3% --- over 30% and not over 70%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '3/6 *100% = 50%    --- over 30% and not over 70%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '4/6 *100% = 66.6% --- over 30% and not over 70%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '5/6 *100% = 83.3% --- not over 90%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup']['Description'][] = '6/6 *100% = 100%  --- equal to 100%, so grade is F';

$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Flag'] = '$eRCTemplateSetting[\'SpecialGradeConvertingForSmallClassGroup2\']';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Enabled'] = $eRCTemplateSetting['SpecialGradeConvertingForSmallClassGroup2'];
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = 'For 上智';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = 'Scheme: 10% A,   20% B,   40% C,    20% D   , 5% E,   5% F';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '% = ((Form Ranking - 1) / No. of Student Taken the Subject) * 100%';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '(1-1)/6 *100% = 0% --- grade is A';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '(2-1)/6 *100% = 16.7% --- grade is B';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '(3-1)/6 *100% = 33.3%  --- grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '(4-1)/6 *100% = 50% --- grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '(5-1)/6 *100% = 66.6% --- grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '(6-1)/6 *100% = 83.3%  ---  grade is D';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = 'Original:';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '% = (Form Ranking / No. of Student Taken the Subject) * 100%';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '1/6 *100% = 16.7% --- over 10%, so grade is B';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '2/6 *100% = 33.3% --- over 30% and not over 70%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '3/6 *100% = 50%    --- over 30% and not over 70%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '4/6 *100% = 66.6% --- over 30% and not over 70%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '5/6 *100% = 83.3% --- not over 90%, so grade is C';
$CustArr['GradingScheme']['SpecialGradeConvertingForSmallClassGroup2']['Description'][] = '6/6 *100% = 100%  --- equal to 100%, so grade is F';


# Marksheet
$CustArr['Marksheet']['ManualInputParentSubjectMark']['Flag'] = '$eRCTemplateSetting[\'ManualInputParentSubjectMark\']';
$CustArr['Marksheet']['ManualInputParentSubjectMark']['Enabled'] = $eRCTemplateSetting['ManualInputParentSubjectMark'];
$CustArr['Marksheet']['ManualInputParentSubjectMark']['Description'][] = 'Control the marksheet input of parent subject';
$CustArr['Marksheet']['ManualInputParentSubjectMark']['Description'][] = 'If the setting is on, user can input the mark of parent subject manually even if all the component subjects are input in Mark Scale';
$CustArr['Marksheet']['ManualInputParentSubjectMark']['Description'][] = 'And in the generation of report card, the input marks will be used instead of calculated marks from component subjects';

$CustArr['Marksheet']['DisableAutoFillInNA']['Flag'] = '$eRCTemplateSetting[\'Disable_Auto_Fill_In_NA\']';
$CustArr['Marksheet']['DisableAutoFillInNA']['Enabled'] = $eRCTemplateSetting['Disable_Auto_Fill_In_NA'];
$CustArr['Marksheet']['DisableAutoFillInNA']['Description'][] = 'For HKUGA College';
$CustArr['Marksheet']['DisableAutoFillInNA']['Description'][] = 'Disable the auto fill in n.a. function to make sure all grades have been input by the teacher';

$CustArr['Marksheet']['BypassParentSubjectMarksheetChecking']['Flag'] = '$eRCTemplateSetting[\'Bypass_Parent_Subject_Marksheet_Checking\']';
$CustArr['Marksheet']['BypassParentSubjectMarksheetChecking']['Enabled'] = $eRCTemplateSetting['Bypass_Parent_Subject_Marksheet_Checking'];
$CustArr['Marksheet']['BypassParentSubjectMarksheetChecking']['CRM'] = '2011-0120-1549-39096';
$CustArr['Marksheet']['BypassParentSubjectMarksheetChecking']['Description'][] = 'For HKUGA College';
$CustArr['Marksheet']['BypassParentSubjectMarksheetChecking']['Description'][] = 'By-pass the parent subject mark input checking when completing the marksheet';

$CustArr['Marksheet']['DefaultUsePercentageRatioInSubMS']['Flag'] = '$eRCTemplateSetting[\'DefaultUsePercentageRatioInSubMS\']';
$CustArr['Marksheet']['DefaultUsePercentageRatioInSubMS']['Enabled'] = $eRCTemplateSetting['DefaultUsePercentageRatioInSubMS'];
$CustArr['Marksheet']['DefaultUsePercentageRatioInSubMS']['Description'][] = 'Default selected to use the percentage ratio in Sub-marksheet';

$CustArr['Marksheet']['PreviewReportSubjectSectionInMarksheet']['Flag'] = '$eRCTemplateSetting[\'PreviewReportSubjectSectionInMarksheet\']';
$CustArr['Marksheet']['PreviewReportSubjectSectionInMarksheet']['Enabled'] = $eRCTemplateSetting['PreviewReportSubjectSectionInMarksheet'];
$CustArr['Marksheet']['BypassParentSubjectMarksheetChecking']['CRM'] = '2011-0428-1734-00126';
$CustArr['Marksheet']['PreviewReportSubjectSectionInMarksheet']['Description'][] = 'For HKUGA College';
$CustArr['Marksheet']['PreviewReportSubjectSectionInMarksheet']['Description'][] = 'Added a function to preview the Subject Section of the Report Card in Marksheet Revision';

$CustArr['Marksheet']['HideZeroWeightSubjectComponent']['Flag'] = '$eRCTemplateSetting[\'Marksheet\'][\'HideZeroWeightSubjectComponent\']';
$CustArr['Marksheet']['HideZeroWeightSubjectComponent']['Enabled'] = $eRCTemplateSetting['Marksheet']['HideZeroWeightSubjectComponent'];
$CustArr['Marksheet']['HideZeroWeightSubjectComponent']['CRM'] = '2011-0511-1655-08096';
$CustArr['Marksheet']['HideZeroWeightSubjectComponent']['Description'][] = 'For HKUGA College';
$CustArr['Marksheet']['HideZeroWeightSubjectComponent']['Description'][] = 'If the Subject Component Weight is zero, the Component Column will be hidden in the marksheet instead of showing "---" by default';

$CustArr['Marksheet']['DisableMarksheetImport']['Flag'] = '$eRCTemplateSetting[\'DisableMarksheetImport\']';
$CustArr['Marksheet']['DisableMarksheetImport']['Enabled'] = $eRCTemplateSetting['DisableMarksheetImport'];
$CustArr['Marksheet']['DisableMarksheetImport']['Description'][] = 'For Sha-tin Methodist';
$CustArr['Marksheet']['DisableMarksheetImport']['Description'][] = 'Hide all import icons in the Marksheet Revision';

$CustArr['Marksheet']['EstimatedMark']['Flag'] = '$eRCTemplateSetting[\'Marksheet\'][\'EstimatedMark\']';
$CustArr['Marksheet']['EstimatedMark']['Enabled'] = $eRCTemplateSetting['Marksheet']['EstimatedMark'];
$CustArr['Marksheet']['EstimatedMark']['Description'][] = 'For 粉嶺救恩書院';
$CustArr['Marksheet']['EstimatedMark']['Description'][] = 'Estimated mark logic by inputing "60#"';

$CustArr['Marksheet']['ShowClearManualPositionButton']['Flag'] = '$sys_custom[\'eRC\'][\'Marksheet\'][\'ShowClearManualPositionButton\']';
$CustArr['Marksheet']['ShowClearManualPositionButton']['Enabled'] = $sys_custom['eRC']['Marksheet']['ShowClearManualPositionButton'];
$CustArr['Marksheet']['ShowClearManualPositionButton']['Description'][] = 'For UCCKE';
$CustArr['Marksheet']['ShowClearManualPositionButton']['Description'][] = 'A function to clear all adjusted position (NOT the manual adjustment in IP25) in Marksheet Revision';

$CustArr['Marksheet']['CopyLastColumnGradeToOverallColumn']['Flag'] = '$sys_custom[\'eRC\'][\'Marksheet\'][\'CopyLastColumnGradeToOverallColumn\']';
$CustArr['Marksheet']['CopyLastColumnGradeToOverallColumn']['Enabled'] = $sys_custom['eRC']['Marksheet']['CopyLastColumnGradeToOverallColumn'];
$CustArr['Marksheet']['CopyLastColumnGradeToOverallColumn']['Description'][] = 'Copy the last Report Column Grade to the Overall Column when saving the marksheet so that the client need not the enter the grades twice';

$CustArr['Marksheet']['SubMS_UseSymbolAsOverallMark']['Flag'] = '$sys_custom[\'eRC\'][\'SubMS\'][\'UseSymbolAsOverallMark\']';
$CustArr['Marksheet']['SubMS_UseSymbolAsOverallMark']['Enabled'] = $sys_custom['eRC']['SubMS']['UseSymbolAsOverallMark'];
$CustArr['Marksheet']['SubMS_UseSymbolAsOverallMark']['Description'][] = '';

$CustArr['Marksheet']['class_teacher_as_subject_teacher']['Flag'] = '$special_feature[\'eRC_class_teacher_as_subject_teacher\']';
$CustArr['Marksheet']['class_teacher_as_subject_teacher']['Enabled'] = $special_feature['eRC_class_teacher_as_subject_teacher'];
$CustArr['Marksheet']['class_teacher_as_subject_teacher']['Description'][] = 'In marksheet revision, ignore the access right of Class Teacher and treats them as normal Subject Teacher so that they can only view the Subject Groups they taught';


# Personal Characteristics
$CustArr['PersonalCharacteristics']['PersonalCharacteristicSubmissionSchedule']['Flag'] = '$eRCTemplateSetting[\'PersonalCharacteristicSubmissionSchedule\']';
$CustArr['PersonalCharacteristics']['PersonalCharacteristicSubmissionSchedule']['Enabled'] = $eRCTemplateSetting['PersonalCharacteristicSubmissionSchedule'];
$CustArr['PersonalCharacteristics']['PersonalCharacteristicSubmissionSchedule']['Description'][] = 'For Sha-tin Methodist';
$CustArr['PersonalCharacteristics']['PersonalCharacteristicSubmissionSchedule']['Description'][] = 'Add a separate settings to set the deadline to submit Personal Characteristics';

$CustArr['PersonalCharacteristics']['PersonalCharacteristicComment']['Flag'] = '$eRCTemplateSetting[\'PersonalCharacteristicComment\']';
$CustArr['PersonalCharacteristics']['PersonalCharacteristicComment']['Enabled'] = $eRCTemplateSetting['PersonalCharacteristicComment'];
$CustArr['PersonalCharacteristics']['PersonalCharacteristicComment']['Description'][] = 'For 宏信書院';
$CustArr['PersonalCharacteristics']['PersonalCharacteristicComment']['Description'][] = 'Allow to input Comment in Personal Characteristics';
$CustArr['PersonalCharacteristics']['PersonalCharacteristicComment']['Description'][] = 'Max Length: '.$eRCTemplateSetting['PersonalCharacteristicCommentLength'];

$CustArr['PersonalCharacteristics']['ShowPersonalCharInMarksheetRevision']['Flag'] = '$eRCTemplateSetting[\'ShowPersonalCharInMarksheetRevision\']';
$CustArr['PersonalCharacteristics']['ShowPersonalCharInMarksheetRevision']['Enabled'] = $eRCTemplateSetting['ShowPersonalCharInMarksheetRevision'];
$CustArr['PersonalCharacteristics']['ShowPersonalCharInMarksheetRevision']['CRM'] = '2011-0428-1734-00126';
$CustArr['PersonalCharacteristics']['ShowPersonalCharInMarksheetRevision']['Description'][] = 'Add "Personal Characteristics" link in marksheet';

$CustArr['PersonalCharacteristics']['ShowPersonalCharInMarksheetRevision']['Flag'] = '$eRCTemplateSetting[\'MasterReport\'][\'DisplayPersonalCharacteristicsInMasterReport\']';
$CustArr['PersonalCharacteristics']['ShowPersonalCharInMarksheetRevision']['Enabled'] = $eRCTemplateSetting['MasterReport']['DisplayPersonalCharacteristicsInMasterReport'];
$CustArr['PersonalCharacteristics']['ShowPersonalCharInMarksheetRevision']['Description'][] = 'Add "Personal Characteristics" Option to Master Report';


# Rounding
$CustArr['Rounding']['RoundSubjectMarkBeforeAddToOverallMark']['Flag'] = '$eRCTemplateSetting[\'RoundSubjectMarkBeforeAddToOverallMark\']';
$CustArr['Rounding']['RoundSubjectMarkBeforeAddToOverallMark']['Enabled'] = $eRCTemplateSetting['RoundSubjectMarkBeforeAddToOverallMark'];
$CustArr['Rounding']['RoundSubjectMarkBeforeAddToOverallMark']['CRM'] = '2009-0204-1701';
$CustArr['Rounding']['RoundSubjectMarkBeforeAddToOverallMark']['Description'][] = 'Control the rounding of the subject mark before calculating subject overall';

$CustArr['Rounding']['RoundWeightedSubjectMarkBeforeAddToOverallMark']['Flag'] = '$eRCTemplateSetting[\'RoundWeightedSubjectMarkBeforeAddToOverallMark\']';
$CustArr['Rounding']['RoundWeightedSubjectMarkBeforeAddToOverallMark']['Enabled'] = $eRCTemplateSetting['RoundWeightedSubjectMarkBeforeAddToOverallMark'];
$CustArr['Rounding']['RoundWeightedSubjectMarkBeforeAddToOverallMark']['CRM'] = '2009-0212-0934';
$CustArr['Rounding']['RoundWeightedSubjectMarkBeforeAddToOverallMark']['Description'][] = 'Control the rounding of the WEIGHTED subject mark before calculating Parent Subject Overall';

$CustArr['Rounding']['RoundSubjectMarkBeforeConvertToGrade']['Flag'] = '$eRCTemplateSetting[\'RoundSubjectMarkBeforeConvertToGrade\']';
$CustArr['Rounding']['RoundSubjectMarkBeforeConvertToGrade']['Enabled'] = $eRCTemplateSetting['RoundSubjectMarkBeforeConvertToGrade'];
$CustArr['Rounding']['RoundSubjectMarkBeforeConvertToGrade']['CRM'] = '2009-0213-1454';
$CustArr['Rounding']['RoundSubjectMarkBeforeConvertToGrade']['Description'][] = 'For St. Clare\'s Primary';
$CustArr['Rounding']['RoundSubjectMarkBeforeConvertToGrade']['Description'][] = 'Control the rounding of the subject mark before converting to grade';

$CustArr['Rounding']['CustomizedConsolidatedSubjectTotalRounding']['Flag'] = '$eRCTemplateSetting[\'CustomizedConsolidatedSubjectTotalRounding\']';
$CustArr['Rounding']['CustomizedConsolidatedSubjectTotalRounding']['Enabled'] = $eRCTemplateSetting['CustomizedConsolidatedSubjectTotalRounding'];
$CustArr['Rounding']['CustomizedConsolidatedSubjectTotalRounding']['Description'][] = 'Consolidated Subject Total Rounding = '.$eRCTemplateSetting['ConsolidatedSubjectTotalRounding'];


# Special Case
$CustArr['SpecialCase']['ExemptionEqualsZeroMarkButExemptFullMarkForOverallColumn']['Flag'] = '$eRCTemplateSetting[\'ExemptionEqualsZeroMarkButExemptFullMarkForOverallColumn\']';
$CustArr['SpecialCase']['ExemptionEqualsZeroMarkButExemptFullMarkForOverallColumn']['Enabled'] = $eRCTemplateSetting['ExemptionEqualsZeroMarkButExemptFullMarkForOverallColumn'];
$CustArr['SpecialCase']['ExemptionEqualsZeroMarkButExemptFullMarkForOverallColumn']['CRM'] = '2009-0519-1043';
$CustArr['SpecialCase']['ExemptionEqualsZeroMarkButExemptFullMarkForOverallColumn']['Description'][] = 'Treat exempted subjects as zero mark but exempted the subject full mark for the overall column (ReportColumnID=0)';

$CustArr['SpecialCase']['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']['Flag'] = '$eRCTemplateSetting[\'OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase\']';
$CustArr['SpecialCase']['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']['Enabled'] = $eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase'];
if ($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCaseArr']=='' || count($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCaseArr'])==0) {
	$CustArr['SpecialCase']['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']['Description'][] = 'Applicable Special Case: All';
}
else {
	$CustArr['SpecialCase']['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']['Description'][] = 'Applicable Special Case: ';
	
	$numOfSpecialCase = count($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCaseArr']);
	for ($i=0; $i<$numOfSpecialCase; $i++) {
		$thisSpecialCase = $eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCaseArr'][$i];
		
		if (isset($eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ReportTypeArr'][$thisSpecialCase])) {
			if (in_array('T', $eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ReportTypeArr'][$thisSpecialCase])) {
				$CustArr['SpecialCase']['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']['Description'][] = $thisSpecialCase.': For Term Report only';
			}
			else if (in_array('W', $eRCTemplateSetting['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase_ReportTypeArr'][$thisSpecialCase])) {
				$CustArr['SpecialCase']['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']['Description'][] = $thisSpecialCase.': For Consolidated Report only';
			}
		}
		else {
			$CustArr['SpecialCase']['OverallMarkEqualsSpecialCaseIfOneOfTheColumnHasSpecialCase']['Description'][] = $thisSpecialCase.': For all Reports';	
		}
	}
}	

$CustArr['SpecialCase']['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn']['Flag'] = '$eRCTemplateSetting[\'ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn\']';
$CustArr['SpecialCase']['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn']['Enabled'] = $eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn'];
$CustArr['SpecialCase']['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn']['Item'] = 'Exclude From Grand Calculation If There Are Any Special Case In Column';
$thisDescription = '';
$thisDescription .= 'Applicable Special Case: ';
if ($eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumnArr']=='' || count($eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumnArr'])==0) {
	$thisDescription .= 'All';
}
else {
	$thisDescription .= implode(', ', (array)$eRCTemplateSetting['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumnArr']);
}
$CustArr['SpecialCase']['ExcludeFromGrandCalculationIfThereAreAnySpecialCaseInColumn']['Description'][] = $thisDescription;

$CustArr['SpecialCase']['NoGrandCalculationIfOneOfTheColumnGrandIsExcluded']['Flag'] = '$eRCTemplateSetting[\'NoGrandCalculationIfOneOfTheColumnGrandIsExcluded\']';
$CustArr['SpecialCase']['NoGrandCalculationIfOneOfTheColumnGrandIsExcluded']['Enabled'] = $eRCTemplateSetting['NoGrandCalculationIfOneOfTheColumnGrandIsExcluded'];
$CustArr['SpecialCase']['NoGrandCalculationIfOneOfTheColumnGrandIsExcluded']['Description'][] = 'If true, Term 1: 100 and Term 2: -1 => GrandTotal = -1';
$CustArr['SpecialCase']['NoGrandCalculationIfOneOfTheColumnGrandIsExcluded']['Description'][] = 'If false, Term 1: 100 and Term 2: -1 => GrandTotal = 100';

$CustArr['SpecialCase']['LastColumnTermNotInSubjectGroupEqualsDropped']['Flag'] = '$eRCTemplateSetting[\'LastColumnTermNotInSubjectGroupEqualsDropped\']';
$CustArr['SpecialCase']['LastColumnTermNotInSubjectGroupEqualsDropped']['Enabled'] = $eRCTemplateSetting['LastColumnTermNotInSubjectGroupEqualsDropped'];
$CustArr['SpecialCase']['LastColumnTermNotInSubjectGroupEqualsDropped']['CRM'] = '2011-0706-0902-04067';
$CustArr['SpecialCase']['LastColumnTermNotInSubjectGroupEqualsDropped']['Description'][] = 'System will auto add a "N.A." to the Subject Overall Grade if the Student did not take this Subject\'s Subject Group in the last Term in the Consolidated Report';
$CustArr['SpecialCase']['LastColumnTermNotInSubjectGroupEqualsDropped']['Description'][] = '* Implemented "Vertical > Horizontal" Calculation only';


# Calculation
$CustArr['Calculation']['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']['Flag'] = '$eRCTemplateSetting[\'ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup\']';
$CustArr['Calculation']['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']['Enabled'] = $eRCTemplateSetting['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup'];
$CustArr['Calculation']['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']['Description'][] = 'If the student has been removed from the Subject Group, system will change the score to "N.A." automatically even if the teacher has entered score for the student before.';
$CustArr['Calculation']['ExcludeMarkOfStudentWhoHasDeletedFromSubjectGroup']['Description'][] = 'The score input before will NOT be removed and therefore the score will be appeared again if the client add back the student to the Subject Group.';

$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForTermReport']['Flag'] = '$eRCTemplateSetting[\'UseOverallColumnToCalculateGrandMarksForTermReport\']';
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForTermReport']['Enabled'] = $eRCTemplateSetting['UseOverallColumnToCalculateGrandMarksForTermReport'];
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForTermReport']['CRM'] = '2009-0220-1527';
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForTermReport']['Description'][] = 'Use the Overall Result Column Marks to calculate the GrandTotal, GrandAverage and GPA even if the Calculation is "Vertical-Horizontal"';
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForTermReport']['Description'][] = 'i.e. Use the "Horizontal-Vertical" calculation of Grand Marks even if the setting is "Vertical-Horizontal"';

$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']['Flag'] = '$eRCTemplateSetting[\'UseOverallColumnToCalculateGrandMarksForConsolidatedReport\']';
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']['Enabled'] = $eRCTemplateSetting['UseOverallColumnToCalculateGrandMarksForConsolidatedReport'];
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']['Item'] = 'Use Overall Column To Calculate Grand Marks For Consolidated Report';
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']['CRM'] = '2009-0220-1527';
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']['Description'][] = 'Use the Overall Result Column Marks to calculate the GrandTotal, GrandAverage and GPA even if the Calculation is "Vertical-Horizontal"';
$CustArr['Calculation']['UseOverallColumnToCalculateGrandMarksForConsolidatedReport']['Description'][] = 'i.e. Use the "Horizontal-Vertical" calculation of Grand Marks even if the setting is "Vertical-Horizontal"';

$CustArr['Calculation']['UseConsolidatedReportOverallScoreToCalculateSubjectOverall']['Flag'] = '$eRCTemplateSetting[\'UseConsolidatedReportOverallScoreToCalculateSubjectOverall\']';
$CustArr['Calculation']['UseConsolidatedReportOverallScoreToCalculateSubjectOverall']['Enabled'] = $eRCTemplateSetting['UseConsolidatedReportOverallScoreToCalculateSubjectOverall'];
$CustArr['Calculation']['UseConsolidatedReportOverallScoreToCalculateSubjectOverall']['CRM'] = '2009-0224-1115';
$CustArr['Calculation']['UseConsolidatedReportOverallScoreToCalculateSubjectOverall']['Description'][] = 'If the report includes 4 terms, then use the mark of the consolidated reports instead of re-calculate from the 4 terms';

$CustArr['Calculation']['UseSumOfCmpMarkToCalculateParentSubjectMark']['Flag'] = '$eRCTemplateSetting[\'UseSumOfCmpMarkToCalculateParentSubjectMark\']';
$CustArr['Calculation']['UseSumOfCmpMarkToCalculateParentSubjectMark']['Enabled'] = $eRCTemplateSetting['UseSumOfCmpMarkToCalculateParentSubjectMark'];
$CustArr['Calculation']['UseSumOfCmpMarkToCalculateParentSubjectMark']['CRM'] = '';
$CustArr['Calculation']['UseSumOfCmpMarkToCalculateParentSubjectMark']['Description'][] = 'For St. Stephen College';
$CustArr['Calculation']['UseSumOfCmpMarkToCalculateParentSubjectMark']['Description'][] = 'Use the sum, instead of the average, of the components to be the parent subject mark.';

$CustArr['Calculation']['UseSumOfAssessmentToBeTheTermOverallMark']['Flag'] = '$eRCTemplateSetting[\'UseSumOfAssessmentToBeTheTermOverallMark\']';
$CustArr['Calculation']['UseSumOfAssessmentToBeTheTermOverallMark']['Enabled'] = $eRCTemplateSetting['UseSumOfAssessmentToBeTheTermOverallMark'];
$CustArr['Calculation']['UseSumOfAssessmentToBeTheTermOverallMark']['CRM'] = '';
$CustArr['Calculation']['UseSumOfAssessmentToBeTheTermOverallMark']['Description'][] = 'For Kadoorie (West Kolwoon)';
$CustArr['Calculation']['UseSumOfAssessmentToBeTheTermOverallMark']['Description'][] = 'Honrizontal Calculation - ignore all weighting and sum up all assessment mark as the term overall of the subject';

$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['Flag'] = '$eRCTemplateSetting[\'DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark\']';
$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['Enabled'] = $eRCTemplateSetting['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark'];
$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['CRM'] = '';
$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['Description'][] = 'For Cheung Chuk Shan College';
$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['Description'][] = 'For calculation of parent subject score';
$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['Description'][] = '1st: Sum up all marks for the component subjects';
$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['Description'][] = '2nd: Divided by the sum of Full Mark of the component subjects (excluding the exempted component subjects)';
$CustArr['Calculation']['DividedByCmpFullMark_AndThen_MultiplyParentSubjectFullMark']['Description'][] = '3rd: Multiply by the Parent Subject Full Mark';

$CustArr['Calculation']['CalculationMethod']['Flag'] = '$eRCTemplateSetting[\'CalculationMethod\']';
$CustArr['Calculation']['CalculationMethod']['Enabled'] = $eRCTemplateSetting['CalculationMethod'];
$CustArr['Calculation']['CalculationMethod']['CRM'] = '';
$thisDescription = '';
$thisDescription .= 'Fixed: ';
$thisDescription .= ($eRCTemplateSetting['CalculationMethod']=='1')? 'Horizontal > Vertical' : 'Vertical > Horizontal';
$CustArr['Calculation']['CalculationMethod']['Description'][] = 'Set Compulsory Calculation Method (Basic Settings - cannot change calculation method)';
$CustArr['Calculation']['CalculationMethod']['Description'][] = $thisDescription;

$CustArr['Calculation']['UseRawMarkToCalculateSDScore']['Flag'] = '$eRCTemplateSetting[\'UseRawMarkToCalculateSDScore\']';
$CustArr['Calculation']['UseRawMarkToCalculateSDScore']['Enabled'] = $eRCTemplateSetting['UseRawMarkToCalculateSDScore'];
$CustArr['Calculation']['UseRawMarkToCalculateSDScore']['CRM'] = '';
$CustArr['Calculation']['UseRawMarkToCalculateSDScore']['Description'][] = 'Default use Mark. If enabled, use RawMark to calculate SD Score.';

$CustArr['Calculation']['ExcludeDisplayGradeSubjectInGrandMark']['Flag'] = '$eRCTemplateSetting[\'ExcludeDisplayGradeSubjectInGrandMark\']';
$CustArr['Calculation']['ExcludeDisplayGradeSubjectInGrandMark']['Enabled'] = $eRCTemplateSetting['ExcludeDisplayGradeSubjectInGrandMark'];
$CustArr['Calculation']['ExcludeDisplayGradeSubjectInGrandMark']['CRM'] = '';
$CustArr['Calculation']['ExcludeDisplayGradeSubjectInGrandMark']['Description'][] = 'Exclude subjects which are displayed in Grade in the Grand Mark calculation';

$CustArr['Calculation']['ParentSubjectCalculationMethod']['Flag'] = '$eRCTemplateSetting[\'ParentSubjectCalculationMethod\']';
$CustArr['Calculation']['ParentSubjectCalculationMethod']['Enabled'] = $eRCTemplateSetting['ParentSubjectCalculationMethod'];
$CustArr['Calculation']['ParentSubjectCalculationMethod']['CRM'] = '';
$thisDescription = '';
$thisDescription .= 'Fixed: ';
$thisDescription .= ($eRCTemplateSetting['CalculationMethod']=='1')? 'Horizontal > Vertical' : 'Vertical > Horizontal';
$CustArr['Calculation']['ParentSubjectCalculationMethod']['Description'][] = 'Always use a specific calculation for Parent Subject';
$CustArr['Calculation']['ParentSubjectCalculationMethod']['Description'][] = $thisDescription;
$CustArr['Calculation']['ParentSubjectCalculationMethod']['Description'][] = '* only implemented "Vertical > Horizontal"';


$CustArr['Calculation']['DoNotMinusOneWhenCalculateSD']['Flag'] = '$eRCTemplateSetting[\'DoNotMinusOneWhenCalculateSD\']';
$CustArr['Calculation']['DoNotMinusOneWhenCalculateSD']['Enabled'] = $eRCTemplateSetting['DoNotMinusOneWhenCalculateSD'];
$CustArr['Calculation']['DoNotMinusOneWhenCalculateSD']['CRM'] = '';
$CustArr['Calculation']['DoNotMinusOneWhenCalculateSD']['Description'][] = '';

$CustArr['Calculation']['CalculateAssessmentSDScore']['Flag'] = '$eRCTemplateSetting[\'CalculateAssessmentSDScore\']';
$CustArr['Calculation']['CalculateAssessmentSDScore']['Enabled'] = $eRCTemplateSetting['CalculateAssessmentSDScore'];
$CustArr['Calculation']['CalculateAssessmentSDScore']['CRM'] = '';
$CustArr['Calculation']['CalculateAssessmentSDScore']['Description'][] = 'By default the calculation of Assessment SD Score is skipped to save generation time.';
$CustArr['Calculation']['CalculateAssessmentSDScore']['Description'][] = 'If this flag is on, system will calculate the Assessment SD Score as well.';

$CustArr['Calculation']['ConsolidatedReportUseGPAToCalculate']['Flag'] = '$eRCTemplateSetting[\'ConsolidatedReportUseGPAToCalculate\']';
$CustArr['Calculation']['ConsolidatedReportUseGPAToCalculate']['Enabled'] = $eRCTemplateSetting['ConsolidatedReportUseGPAToCalculate'];
$CustArr['Calculation']['ConsolidatedReportUseGPAToCalculate']['CRM'] = '';
$CustArr['Calculation']['ConsolidatedReportUseGPAToCalculate']['Description'][] = 'For UCCKE';


# Ranking
$CustArr['Ranking']['RankingMethod']['Flag'] = '$eRCTemplateSetting[\'RankingMethod\']';
$CustArr['Ranking']['RankingMethod']['Enabled'] = $eRCTemplateSetting['RankingMethod'];
$CustArr['Ranking']['RankingMethod']['CRM'] = '2009-0603-1030';
$CustArr['Ranking']['RankingMethod']['Description'][] = 'Control the ranking if the students got the same mark.';
$CustArr['Ranking']['RankingMethod']['Description'][] = 'Default: 1 > 2 > 2 > 4';
$CustArr['Ranking']['RankingMethod']['Description'][] = 'If enabled: 1 > 2 > 2 > 3';

$CustArr['Ranking']['DoNotCountExcludedStudentInStudentNumOfClassAndForm']['Flag'] = '$eRCTemplateSetting[\'DoNotCountExcludedStudentInStudentNumOfClassAndForm\']';
$CustArr['Ranking']['DoNotCountExcludedStudentInStudentNumOfClassAndForm']['Enabled'] = $eRCTemplateSetting['DoNotCountExcludedStudentInStudentNumOfClassAndForm'];
$CustArr['Ranking']['DoNotCountExcludedStudentInStudentNumOfClassAndForm']['CRM'] = '2009-0423-1026';
$CustArr['Ranking']['DoNotCountExcludedStudentInStudentNumOfClassAndForm']['Description'][] = 'Do not count excluded student in the number of student in class and form';

$CustArr['Ranking']['OrderingPositionBy']['Flag'] = '$eRCTemplateSetting[\'OrderingPositionBy\']';
$CustArr['Ranking']['OrderingPositionBy']['Enabled'] = $eRCTemplateSetting['OrderingPositionBy'];
$CustArr['Ranking']['OrderingPositionBy']['CRM'] = '2009-0507-1745';
$CustArr['Ranking']['OrderingPositionBy']['Description'][] = 'Set the Data Field to determine the positioning';
$CustArr['Ranking']['OrderingPositionBy']['Description'][] = 'possible setting: "", "GrandTotal", "GrandAverage", "GPA", "GrandSDScore"';
$CustArr['Ranking']['OrderingPositionBy']['Value'] = $eRCTemplateSetting['OrderingPositionBy'];

$CustArr['Ranking']['SubjectPositionDisplaySettings']['Flag'] = '$eRCTemplateSetting[\'DisplayPosition\'][\'SubjectPositionDisplaySettings\']';
$CustArr['Ranking']['SubjectPositionDisplaySettings']['Enabled'] = $eRCTemplateSetting['DisplayPosition']['SubjectPositionDisplaySettings'];
$CustArr['Ranking']['SubjectPositionDisplaySettings']['CRM'] = '';
$CustArr['Ranking']['SubjectPositionDisplaySettings']['Description'][] = '';

$CustArr['Ranking']['OrderPositionMethod']['Flag'] = '$eRCTemplateSetting[\'OrderPositionMethod\']';
$CustArr['Ranking']['OrderPositionMethod']['Enabled'] = $eRCTemplateSetting['OrderPositionMethod'];
$CustArr['Ranking']['OrderPositionMethod']['CRM'] = '';
$CustArr['Ranking']['OrderPositionMethod']['Description'][] = 'using SD or RawMarks to order the position';
$CustArr['Ranking']['OrderPositionMethod']['Description'][] = 'possible setting: "", "Raw Mark", "WeightedSD"';
$CustArr['Ranking']['OrderPositionMethod']['Value'] = $eRCTemplateSetting['OrderPositionMethod'];

$CustArr['Ranking']['OrderMeritSubjectGroupManualAdjustment']['Flag'] = '$eRCTemplateSetting[\'OrderMeritSubjectGroupManualAdjustment\']';
$CustArr['Ranking']['OrderMeritSubjectGroupManualAdjustment']['Enabled'] = $eRCTemplateSetting['OrderMeritSubjectGroupManualAdjustment'];
$CustArr['Ranking']['OrderMeritSubjectGroupManualAdjustment']['CRM'] = '';
$CustArr['Ranking']['OrderMeritSubjectGroupManualAdjustment']['Description'][] = 'Manual Adjust Subject Group Rank';


# Data Transfer
$CustArr['DataTransfer']['PassComponentMarks']['Flag'] = '$eRCTemplateSetting[\'PassComponentMarks\']';
$CustArr['DataTransfer']['PassComponentMarks']['Enabled'] = $eRCTemplateSetting['PassComponentMarks'];
$CustArr['DataTransfer']['PassComponentMarks']['CRM'] = '';
$CustArr['DataTransfer']['PassComponentMarks']['Description'][] = 'Pass Component Subject Marks to iPortfolio even if Calculate Method is Vertical-horizontal ';
$CustArr['DataTransfer']['PassComponentMarks']['Value'] = '';

$CustArr['DataTransfer']['TransferGPA']['Flag'] = '$eRCTemplateSetting[\'TransferToiPortfolio\'][\'TransferGPA\']';
$CustArr['DataTransfer']['TransferGPA']['Enabled'] = $eRCTemplateSetting['TransferToiPortfolio']['TransferGPA'];
$CustArr['DataTransfer']['TransferGPA']['CRM'] = '';
$CustArr['DataTransfer']['TransferGPA']['Description'][] = 'For UCCKE';
$CustArr['DataTransfer']['TransferGPA']['Value'] = '';


# Others
$CustArr['Others']['HideMenu_Management_ClassTeacherComment']['Flag'] = '$eRCTemplateSetting[\'HideMenu\'][\'Management\'][\'ClassTeacherComment\']';
$CustArr['Others']['HideMenu_Management_ClassTeacherComment']['Enabled'] = $eRCTemplateSetting['HideMenu']['Management']['ClassTeacherComment'];
$CustArr['Others']['HideMenu_Management_ClassTeacherComment']['CRM'] = '';
$CustArr['Others']['HideMenu_Management_ClassTeacherComment']['Description'][] = 'For UCCKE';
$CustArr['Others']['HideMenu_Management_ClassTeacherComment']['Value'] = '';

$CustArr['Others']['HideMenu_Management_PersonalCharacteristics']['Flag'] = '$eRCTemplateSetting[\'HideMenu\'][\'Management\'][\'PersonalCharacteristics\']';
$CustArr['Others']['HideMenu_Management_PersonalCharacteristics']['Enabled'] = $eRCTemplateSetting['HideMenu']['Management']['PersonalCharacteristics'];
$CustArr['Others']['HideMenu_Management_PersonalCharacteristics']['CRM'] = '';
$CustArr['Others']['HideMenu_Management_PersonalCharacteristics']['Description'][] = 'For HKUGA College';
$CustArr['Others']['HideMenu_Management_PersonalCharacteristics']['Value'] = '';

$CustArr['Others']['HideMenu_Settings_CommentBank']['Flag'] = '$eRCTemplateSetting[\'HideMenu\'][\'Settings\'][\'CommentBank\']';
$CustArr['Others']['HideMenu_Settings_CommentBank']['Enabled'] = $eRCTemplateSetting['HideMenu']['Settings']['CommentBank'];
$CustArr['Others']['HideMenu_Settings_CommentBank']['CRM'] = '';
$CustArr['Others']['HideMenu_Settings_CommentBank']['Description'][] = 'For UCCKE';
$CustArr['Others']['HideMenu_Settings_CommentBank']['Value'] = '';

$CustArr['Others']['ReportGeneration_Promotion']['Flag'] = '$sys_custom[\'eRC\'][\'Report\'][\'ReportGeneration\'][\'Promotion\']';
$CustArr['Others']['ReportGeneration_Promotion']['Enabled'] = $sys_custom['eRC']['Report']['ReportGeneration']['Promotion'];
$CustArr['Others']['ReportGeneration_Promotion']['CRM'] = '';
$CustArr['Others']['ReportGeneration_Promotion']['Description'][] = 'For 上智';
$CustArr['Others']['ReportGeneration_Promotion']['Value'] = '';

$CustArr['Others']['ReportGeneration_AwardGeneration']['Flag'] = '$eRCTemplateSetting[\'Report\'][\'ReportGeneration\'][\'AwardGeneration\']';
$CustArr['Others']['ReportGeneration_AwardGeneration']['Enabled'] = $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration'];
$CustArr['Others']['ReportGeneration_AwardGeneration']['CRM'] = '';
$CustArr['Others']['ReportGeneration_AwardGeneration']['Description'][] = 'For Sha-tin Methodist';
$CustArr['Others']['ReportGeneration_AwardGeneration']['Value'] = '';

$CustArr['Others']['ReportGeneration_AwardGeneration_CustomizedLogic']['Flag'] = '$eRCTemplateSetting[\'Report\'][\'ReportGeneration\'][\'AwardGeneration_CustomizedLogic\']';
$CustArr['Others']['ReportGeneration_AwardGeneration_CustomizedLogic']['Enabled'] = $eRCTemplateSetting['Report']['ReportGeneration']['AwardGeneration_CustomizedLogic'];
$CustArr['Others']['ReportGeneration_AwardGeneration_CustomizedLogic']['CRM'] = '';
$CustArr['Others']['ReportGeneration_AwardGeneration_CustomizedLogic']['Description'][] = 'For 粉嶺救恩書院';
$CustArr['Others']['ReportGeneration_AwardGeneration_CustomizedLogic']['Description'][] = 'Customized award generation logic';
$CustArr['Others']['ReportGeneration_AwardGeneration_CustomizedLogic']['Value'] = '';

$CustArr['Others']['DisablePreviewTemplate']['Flag'] = '$eRCTemplateSetting[\'DisablePreviewTemplate\']';
$CustArr['Others']['DisablePreviewTemplate']['Enabled'] = $eRCTemplateSetting['DisablePreviewTemplate'];
$CustArr['Others']['DisablePreviewTemplate']['CRM'] = '';
$CustArr['Others']['DisablePreviewTemplate']['Description'][] = 'Hide all preview template buttons in eReportCard module';
$CustArr['Others']['DisablePreviewTemplate']['Value'] = '';

$CustArr['Others']['CustomizaedTermDateRange']['Flag'] = '$eRCTemplateSetting[\'CustomizaedTermDateRange\']';
$CustArr['Others']['CustomizaedTermDateRange']['Enabled'] = $eRCTemplateSetting['CustomizaedTermDateRange'];
$CustArr['Others']['CustomizaedTermDateRange']['CRM'] = '';
$CustArr['Others']['CustomizaedTermDateRange']['Description'][] = 'Customized the Term Date Range for each Term Report (usually for F5 and F7) to retrieve the data from different modules.';
$CustArr['Others']['CustomizaedTermDateRange']['Value'] = '';

$CustArr['Others']['TemplateSettings_ShowOtherInfo']['Flag'] = '$eRCTemplateSetting[\'TemplateSettings\'][\'ShowOtherInfo\']';
$CustArr['Others']['TemplateSettings_ShowOtherInfo']['Enabled'] = count((array)$eRCTemplateSetting['CustomizaedTermDateRange']['ShowOtherInfo']) > 0;
$CustArr['Others']['TemplateSettings_ShowOtherInfo']['CRM'] = '';
$CustArr['Others']['TemplateSettings_ShowOtherInfo']['Description'][] = 'For Wah Yan College Kowloon';
$CustArr['Others']['TemplateSettings_ShowOtherInfo']['Description'][] = 'Show Other Template Info In Template Settings';
$CustArr['Others']['TemplateSettings_ShowOtherInfo']['Value'] = implode(', ',(array)$eRCTemplateSetting['TemplateSettings']['ShowOtherInfo']);

$CustArr['Others']['MasterReport_PassingNumberDetermineByPercentage']['Flag'] = '$eRCTemplateSetting[\'MasterReport\'][\'PassingNumberDetermineByPercentage\']';
$CustArr['Others']['MasterReport_PassingNumberDetermineByPercentage']['Enabled'] = $eRCTemplateSetting['MasterReport']['PassingNumberDetermineByPercentage'];
$CustArr['Others']['MasterReport_PassingNumberDetermineByPercentage']['CRM'] = '';
$CustArr['Others']['MasterReport_PassingNumberDetermineByPercentage']['Description'][] = 'For UCCKE';
$CustArr['Others']['MasterReport_PassingNumberDetermineByPercentage']['Value'] = '';

$CustArr['Others']['AllowGenerateAtAnyTime']['Flag'] = '$eRCTemplateSetting[\'AllowGenerateAtAnyTime\']';
$CustArr['Others']['AllowGenerateAtAnyTime']['Enabled'] = $eRCTemplateSetting['AllowGenerateAtAnyTime'];
$CustArr['Others']['AllowGenerateAtAnyTime']['CRM'] = '';
$CustArr['Others']['AllowGenerateAtAnyTime']['Description'][] = 'For HKUGA College';
$CustArr['Others']['AllowGenerateAtAnyTime']['Description'][] = 'Allow client to generate and print report card at any time without checking';
$CustArr['Others']['AllowGenerateAtAnyTime']['Value'] = '';

$CustArr['Others']['Management_ClassTeacherComment_ShowGeneratedReportCard']['Flag'] = '$eRCTemplateSetting[\'Management\'][\'ClassTeacherComment\'][\'ShowGeneratedReportCard\']';
$CustArr['Others']['Management_ClassTeacherComment_ShowGeneratedReportCard']['Enabled'] = $eRCTemplateSetting['Management']['ClassTeacherComment']['ShowGeneratedReportCard'];
$CustArr['Others']['Management_ClassTeacherComment_ShowGeneratedReportCard']['CRM'] = '2011-0920-1743-45067';
$CustArr['Others']['Management_ClassTeacherComment_ShowGeneratedReportCard']['Description'][] = 'for Wesley Methodist School';
$CustArr['Others']['Management_ClassTeacherComment_ShowGeneratedReportCard']['Description'][] = 'Show Generated Report Card in Class Teacher Comment Page';
$CustArr['Others']['Management_ClassTeacherComment_ShowGeneratedReportCard']['Value'] = '';

$CustArr['Others']['Management_ClassTeacherComment_ShowSubjectTeacherComment']['Flag'] = '$eRCTemplateSetting[\'Management\'][\'ClassTeacherComment\'][\'ShowSubjectTeacherComment\']';
$CustArr['Others']['Management_ClassTeacherComment_ShowSubjectTeacherComment']['Enabled'] = $eRCTemplateSetting['Management']['ClassTeacherComment']['ShowSubjectTeacherComment'];
$CustArr['Others']['Management_ClassTeacherComment_ShowSubjectTeacherComment']['CRM'] = '2011-0920-1743-45067';
$CustArr['Others']['Management_ClassTeacherComment_ShowSubjectTeacherComment']['Description'][] = 'for Wesley Methodist School';
$CustArr['Others']['Management_ClassTeacherComment_ShowSubjectTeacherComment']['Description'][] = 'Show Subject Teacher Comment in Student / Parent MS Verification UI';
$CustArr['Others']['Management_ClassTeacherComment_ShowSubjectTeacherComment']['Value'] = '';



### Construct the Contents
// Move to Section
$MoveToSectionTable = '';
$MoveToSectionTable .= '<table border="0" cellpadding="0" cellspacing="0">'."\r\n";
	$MoveToSectionTable .= '<tr><td>Jump To Section:</td></tr>'."\r\n";
	foreach ((array)$CustArr as $thisSectionCode => $thisSectionInfoArr) {
		$MoveToSectionTable .= '<tr><td><a href="javascript:void(0);" onclick="js_Jump_to_Section(\''.$thisSectionCode.'\');">'.$thisSectionCode.'</a></td></tr>'."\r\n";
	}
$MoveToSectionTable .= '</table>'."\r\n";

// Content Table
$ContentTable = '';
foreach ((array)$CustArr as $thisSectionCode => $thisSectionInfoArr) {
	$ContentTable .= Get_Section_Table_Display($thisSectionCode, $thisSectionInfoArr);
}

function Get_Section_Table_Display($thisSectionTitle, $InfoArr) {
	global $ShowEnabledOnly;
	
	$JumpToTopLink = '<a href="javascript:void(0);" onclick="js_Jump_to_Top();">(Move to top)</a>';
	
	$x = '';
	
	$x .= '<div id="SectionTitleDiv_'.$thisSectionTitle.'" class="SectionTitleDiv">'.$thisSectionTitle.' '.$JumpToTopLink.'</div>'."\r\n";
	$x .= '<div>'."\r\n";
		$x .= '<table class="InfoTable" cellpadding="5" cellspacing="0">'."\r\n";
			$x .= '<thead>'."\r\n";
				$x .= '<tr>'."\r\n";
					$x .= '<th style="width:4%;">#</th>'."\r\n";
					$x .= '<th style="width:32%; text-align:left;" class="border_left">Flag</th>'."\r\n";
					$x .= '<th style="width:8%;" class="border_left">Enabled<br />(Value)</th>'."\r\n";
					$x .= '<th style="width:41%; text-align:left;" class="border_left">Description</th>'."\r\n";
					$x .= '<th style="width:10%; text-align:left;" class="border_left">CRM</th>'."\r\n";
				$x .= '</tr>'."\r\n";
			$x .= '<thead>'."\r\n";
			
			$x .= '<tbody>'."\r\n";
				$RowCounter = 0;
				foreach ((array)$InfoArr as $thisCode => $thisItemInfo) {
					$thisFlag = $thisItemInfo['Flag'];
					$thisItem = ($thisItemInfo['Item'] == '')? $thisCode : $thisItemInfo['Item'];
					$thisCRM = ($thisItemInfo['CRM']=='')? '&nbsp;' : $thisItemInfo['CRM'];
					
					$thisDescription = implode('<br />', (array)$thisItemInfo['Description']);
					$thisDescription = ($thisDescription == '')? '&nbsp;' : $thisDescription;
					
					$thisValue = $thisItemInfo['Value'];
					$thisValueDisplay = ($thisValue=='')? '' : '<br />('.$thisValue.')';
					
					$thisEnabled = $thisItemInfo['Enabled'];
					if ($thisEnabled) {
						$thisTrClass = 'row_enabled';
						$thisTdEnableClass = 'tabletext_enabled';
						$thisEnabledDisplay = "TRUE";
					}
					else {
						if ($ShowEnabledOnly) {
							continue;
						}
						
						$thisTrClass = '';
						$thisTdEnableClass = '';
						$thisEnabledDisplay = "FALSE";
					}
					
					$x .= '<tr class="'.$thisTrClass.'">'."\r\n";
						$x .= '<td class="border_top" style="text-align:center; font-weight:bold;">'.(++$RowCounter).'</td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$thisFlag.'</td>'."\r\n";
						$x .= '<td class="border_top border_left '.$thisTdEnableClass.'" style="text-align:center;">'.$thisEnabledDisplay.$thisValueDisplay.'</td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$thisDescription.'</td>'."\r\n";
						$x .= '<td class="border_top border_left">'.$thisCRM.'</td>'."\r\n";
					$x .= '</tr>'."\r\n";
				}
			$x .= '</tbody>'."\r\n";
		$x .= '</table>'."\r\n";
	$x .= '</div>'."\r\n";
	$x .= '<br />'."\r\n";
	
	if ($RowCounter == 0) {
		return '';
	}
	else {
		return $x;
	}
}
intranet_closedb();
?>
<br />
<div id="MainDiv" style="width:100%;">
	<div id="ToolbarDiv">
		<div>
			<?=$linterface->Get_Radio_Button('ShowEnabledCustRadio_Yes', 'ShowEnabledCustRadio', $Value, $isChecked=$ShowEnabledOnly, $Class="", "Show enabled only", $Onclick="js_Reload(1);")?>
			&nbsp;&nbsp;
			<?=$linterface->Get_Radio_Button('ShowEnabledCustRadio_No', 'ShowEnabledCustRadio', $Value, $isChecked=!$ShowEnabledOnly, $Class="", "Show all customizations", $Onclick="js_Reload(0);")?>
		</div>
		<br />
		
		<div>
			<?=$MoveToSectionTable?>
		</div>
		<br style="clear:both;" />
	</div>
	
	<hr />
	<br />
	
	<div id="ContentDiv">
		<?=$ContentTable?>
	</div>
</div>


<style type="text/css">
a {
	color: #2286C5;
	text-decoration: none;
}

.SectionTitleDiv {
	color: #000000;
	font-weight: bold;
}

.InfoTable {
	width: 100%;
	border-top: 1px solid #000000;
	border-bottom: 1px solid #000000;
	border-left: 1px solid #000000;
	border-right: 1px solid #000000;
}

.InfoTable thead tr th {
	background-color: #000000;
	color: #FFFFFF;
}

.InfoTable tbody tr td {
	vertical-align: top;
}

.border_left {
	border-left: 1px solid #000000;
}
.border_top {
	border-top: 1px solid #000000;
}

.row_enabled {
	background-color: #00BB00;
	color: white;
}

.tabletext_enabled {
	font-weight: bold;
}
</style>

<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/jquery.scrollTo-min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

});

function js_Reload(jsEnabledOnly) {
	window.location = "?ShowEnabledOnly=" + jsEnabledOnly;
}

function js_Jump_to_Section(jsSectionCode) {
	var jsTargetDivID = 'SectionTitleDiv_' + jsSectionCode;
	$(window).scrollTo('div#' + jsTargetDivID, 800, {queue:true});
}

function js_Jump_to_Top() {
	$(window).scrollTo(0, 800, {queue:true} );
}
</script>