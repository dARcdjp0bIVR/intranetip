<?php
// Date in the past
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
// always modified
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
// HTTP/1.1
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
// HTTP/1.0
header("Pragma: no-cache");

@session_start();


#############################################################################################
### Settings

# Display Settings
$DefaultSettingsArr['Display']['Charset'] = 'utf-8';		// big5 or utf-8
$DefaultSettingsArr['Display']['NewVerLastDate'] = '2011-12-16';	// If today is after this setting's date, the new icon will be hidden


# Security IP Settings
$DefaultSettingsArr['Security']['AuthorizedIP'] = array();
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.108';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.78';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.211';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.81';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.19';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.28';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.39';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.40';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.41';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.46';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.50';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.53';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.55';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.56';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.57';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.59';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.60';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.65';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.67';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.68';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.70';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.71';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.73';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.75';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.77';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.84';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.86';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.89';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.91';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.92';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.94';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.95';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.99';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.103';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.104';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.106';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.107';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.109';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.110';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.111';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.112';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.113';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.119';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.124';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.127';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.131';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.134';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.135';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.137';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.138';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.141';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.142';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.146';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.147';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.149';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.150';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.151';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.153';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.155';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.158';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.160';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.165';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.171';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.175';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.178';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.121';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.250';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.95';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.120';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.121';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.144';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.163';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.173';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.178';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.186';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.43';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.12.1';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.100.210.21';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.103.192.16';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.54';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.116.64.28';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.120.240.2';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.123.112.23';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.127.1.254';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.129.192.1';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.132.144.239';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.132.255.254';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '118.142.70.130';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '118.142.70.139';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '172.16.0.1';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '172.16.1.12';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '172.20.0.3';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '172.28.1.163';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '172.28.1.254';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.0.1';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.0.254';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.1.100';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.12';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.1.135';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.1.181';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.2.1';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.99.1';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '202.82.10.67';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '203.80.242.177';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '203.80.242.183';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '203.198.166.11';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '210.0.211.211';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.135';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.136';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.20';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.83';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.45';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.49';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.126.32.9';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.150';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.111.32.253';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.132';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.91';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.62';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.169';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.145';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.123';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.122';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.111';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.82';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.64';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.96';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.120.240.1';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.127';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.133';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.132.48.25';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.79';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.98';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.144';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.153';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.63';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.66';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.120';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.129.208.27';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.10.2';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.116';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.133.0.254';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.165';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.128.77.4';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.174';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.118';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.210';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.140';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.140';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.101.240.3';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.61';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.148';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.80';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.129';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.101';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.148';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.97';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.58';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.231';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.122';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.196';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.189';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.96';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.170';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.71';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.178.16.3';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.97';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.224';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '203.80.242.188';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.74';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.242';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.102';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.136';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.44';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.128';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.52';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '192.168.0.172';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.74';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.80';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.113.192.14';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.61';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.43';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.141';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '218.255.117.229';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.51';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.69';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.72';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.143';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.132';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.4.230';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.87';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.85';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.36';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.117';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.90';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.42';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.160';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.154';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.159';
$DefaultSettingsArr['Security']['AuthorizedIP'][] = '10.0.3.156';

$DefaultSettingsArr['Security']['CheckPermission']['View'] = '1';
$DefaultSettingsArr['Security']['CheckPermission']['UpdateDB'] = '1';

# DB Settings
$IntranetSettingsPathRoot = $PATH_WRT_ROOT.'../../';
//$IntranetSettingsFilePath = $IntranetSettingsPathRoot.'includes/settings.php';
$IntranetSettingsFilePath = $IntranetSettingsPathRoot.'includes/global.php';

$NoLangWordings = true;
$BLOCK_LIB_LOADING = true;
if (is_file($IntranetSettingsFilePath))
	include_once($IntranetSettingsFilePath);

$Default_Database = ($intranet_db != '')? $intranet_db : '';
$Sql_UserName = ($intranet_db_user != '')? $intranet_db_user : '';
$Sql_Password = ($intranet_db_pass != '')? $intranet_db_pass : '';
$Sql_Host = ($sys_custom['MySQL_Server_Host'] != '')? $sys_custom['MySQL_Server_Host'] : '';

$DefaultSettingsArr['Database']['Default_Database'] = $Default_Database;
$DefaultSettingsArr['Database']['MySQL_UserName'] = $Sql_UserName;
$DefaultSettingsArr['Database']['MySQL_Password'] = $Sql_Password;
$DefaultSettingsArr['Database']['Database_Intranet'] = $intranet_db;
$DefaultSettingsArr['Database']['Database_eClass'] = $eclass_db;
$DefaultSettingsArr['Database']['Database_LMS'] = $eclass_prefix."eClass_LIBMS";
$DefaultSettingsArr['Database']['MySQL_Host'] = $Sql_Host;
$DefaultSettingsArr['Database']['SqlHistorySeparator'] = "|||---|||";


# Log file path
$DefaultSettingsArr['Log']['FilePath'] = $IntranetSettingsPathRoot."file/import_temp/query_log/";
$DefaultSettingsArr['Log']['FilePathArr'][] = "import_temp";	// For create folder use (EJ server cannot create folder recursively)
$DefaultSettingsArr['Log']['FilePathArr'][] = "query_log";


# Functional default Settings
$DefaultSettingsArr['Functional']['RepeatHeader'] = '0';
$DefaultSettingsArr['Functional']['RepeatHeaderCount'] = '20';
$DefaultSettingsArr['Functional']['KeepQuery'] = '1';
$DefaultSettingsArr['Functional']['DisableDataTable'] = '0';
$DefaultSettingsArr['Functional']['AutoDisableDataTable'] = '1';
$DefaultSettingsArr['Functional']['AutoDisableDataTableCount'] = '100';
$DefaultSettingsArr['Functional']['Delimiter'] = ';';



#############################################################################################
?>
