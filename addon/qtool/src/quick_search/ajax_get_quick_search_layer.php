<?php
// using ivan
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."library/common_function.php");
include_once($PATH_WRT_ROOT."settings.php");
include_once($PATH_WRT_ROOT."library/libquicksearch_ui.php");


// default searching - Intranet User
$libquicksearch_ui = new libquicksearch_ui($Database_Intranet);
$MenuArr = $libquicksearch_ui->Get_Left_Menu_Structure_Array();


$x = '';
$x .= '<table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0">'."\n";
	$x .= '<tr>'."\n";
		$x .= '<td style="vertical-align:top;width:170px;height:100%;">'."\n";
			$x .= '<div style="height:100%;padding-top:0px;padding-bottom:0px;padding-right:0px;padding-left:0px;">'."\n";
				$x .= '<ul id="QuickSearchLeftMenuList" class="menu collapsible">'."\n";
					foreach ((array)$MenuArr as $thisMenuKey => $thisMenuInfoArr)
					{
						$thisMenuTitle = $thisMenuInfoArr['Title'];
						$thisSubMenuArr = $thisMenuInfoArr['SubMenuArr'];
						$thisNumOfSubMenu = count($thisSubMenuArr);
						
						if ($thisNumOfSubMenu == 1)
						{
							foreach ((array)$thisSubMenuArr as $thisSubMenuKey => $thisSubMenuInfoArr)
							{
								$thisSubMenuTitle = $thisSubMenuInfoArr['Title'];
								$x .= '<li><a href="javascript:js_Get_Quick_Search_Input_Table(\''.$thisMenuKey.'\', \''.$thisSubMenuKey.'\')">'.$thisSubMenuTitle.'</a></li>'."\n";
							}	
						}
						else
						{
							$x .= '<li>'."\n";
								$x .= '<a href="#">'.$thisMenuTitle.'</a>'."\n";
								$x .= '<ul>'."\n";
									foreach ((array)$thisSubMenuArr as $thisSubMenuKey => $thisSubMenuInfoArr)
									{
										$thisSubMenuTitle = $thisSubMenuInfoArr['Title'];
										$x .= '<li><a href="javascript:js_Get_Quick_Search_Input_Table(\''.$thisMenuKey.'\', \''.$thisSubMenuKey.'\')">'.$thisSubMenuTitle.'</a></li>'."\n";
									}
								$x .= '</ul>'."\n";
							$x .= '</li>'."\n";
						}
					}
				$x .= '</ul>'."\n";
			$x .= '</div>'."\n";
		$x .= '</td>'."\n";
		
		$x .= '<td style="width:5px">&nbsp;</td>'."\n";
		
		$x .= '<td style="vertical-align:top;text-align:left;">'."\n";
			$x .= '<div id="quick_search_input_div" style="width:96%;vertical-align:top;">'."\n";
				$x .= '<table style="width:100%;vertical-align:top;" border="0" cellspacing="0" cellpadding="5" id="quick_search_input_table">'."\n";
					$x .= $libquicksearch_ui->Get_Search_Rows('IntranetUser', 'intranet_user');
				$x .= '</table>'."\n";
			$x .= '</div>'."\n";
		$x .= '</td>'."\n";
		
	$x .= '</tr>'."\n";
$x .= '</table>'."\n";

$x .= '<script language="javascript">';
	$x .= 'initMenu();';
$x .= '</script>';
	
echo $x;

?>