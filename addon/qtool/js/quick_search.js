function js_Get_Quick_Search_Input_Table(jsSearchType, jsSearchSubType)
{
	$('div#quick_search_input_div').fadeOut('fast', function () { 
		$(this).load(
			"src/quick_search/ajax_get_quick_search_input_layer.php", 
			{ 
				SearchType: jsSearchType,
				SearchSubType: jsSearchSubType,
				Database_Intranet: $('input#Database_Intranet').val()
			},
			function(ReturnData)
			{
				$('div#quick_search_input_div').fadeIn('fast');
				$('.focus_tb').focus();
			}
		)
	});
}

function js_Submit_Quick_Search(jsSearchType, jsTextboxID)
{
	var jsSQL = $('input#' + jsSearchType + '_sql').val();
	var jsSearchValue = Trim($('input#' + jsTextboxID).val());
	var jsWords = ['<!--SearchValue-->'];
	var jsTargetSQL = jsSQL.replace(new RegExp(jsWords.join('|'), 'g'), jsSearchValue);
	
	formSubmit(jsTargetSQL, 0, '');
}

function js_Check_Quick_Search_Submit(e, jsSearchType, jsTextboxID)
{
	var keynum = Get_KeyNum(e);
	var keychar = String.fromCharCode(keynum);
	
	if (keynum==13)		//Enter => Submit
		js_Submit_Quick_Search(jsSearchType, jsTextboxID);
}

function initMenu() {
	/*** http://www.i-marco.nl/weblog/jquery-accordion-3/ ***/
	
	$('ul.menu ul').hide();
	
//	$.each($('ul.menu'), function(){
//		var cookie = $.cookie(this.id);
//		if(cookie === null || String(cookie).length < 1) {
//			$('#' + this.id + '.expandfirst ul:first').show();
//		}
//		else {
//			$('#' + this.id + ' .' + cookie).next().show();
//		}
//	});
	
	$('ul.menu li a').click( function() {
		var checkElement = $(this).next();
		var parent = this.parentNode.parentNode.id;
//		if($('#' + parent).hasClass('noaccordion')) {
//			if((String(parent).length > 0) && (String(this.className).length > 0)) {
//				if($(this).next().is(':visible')) {
//					$.cookie(parent, null);
//				}
//				else {
//					$.cookie(parent, this.className);
//				}
//				$(this).next().slideToggle('slow');
//			}
//		}
		if((checkElement.is('ul')) && (checkElement.is(':visible'))) {
			if($('#' + parent).hasClass('collapsible')) {
				$('#' + parent + ' ul:visible').slideUp('slow');
			}
			return false;
		}
		
		if((checkElement.is('ul')) && (!checkElement.is(':visible'))) {
			$('#' + parent + ' ul:visible').slideUp('slow');
//			if((String(parent).length > 0) && (String(this.className).length > 0)) {
//				$.cookie(parent, this.className);
//			}
			checkElement.slideDown('normal');
			return false;
		}
	});
}