<?php

if (!defined("LIBEXPORTTEXT_DEFINED"))                     // Preprocessor directive
{
  define("LIBEXPORTTEXT_DEFINED", true);

  //global $import_coding, $g_encoding_unicode;
  //global $g_encoding_unicode;
  
  class libexport 
  {
		/*
		* Initialize
		*/
		function libexport()
		{
			
		}
		
		/*
		* Prepare Content for Export
		* 	$data : 2D array of data to export
		* 	$ColumnDef: header, i.e. the first row
		*		$Delimiter: delimiter, e.g. comma, tab
		*		$LineBreak: linebreak, can be vary due to different system
		*		$ColumnDefDelimiter: set the delimiter of header different than the default
		*		$DataSize: need to specify if number of column header is different from the data OR no column header
		*		$Quoted: double-quoted the column and/or data item, "00"=both not quoted, "11"=both quoted
		*/
		function GET_EXPORT_TXT($data, $ColumnDef, $Delimiter="", $LineBreak="\r\n", $ColumnDefDelimiter="", $DataSize=0, $Quoted="00")
		{
			$Delimiter = ($Delimiter=="")?"\t":"$Delimiter";
			$ColumnDefDelimiter = ($ColumnDefDelimiter=="")?"\t":"$ColumnDefDelimiter";
			
			for ($i=0; $i<sizeof($ColumnDef); $i++) {
				if ($Quoted == "11" || $Quoted == "10")
					$clabel[] = "\"".$ColumnDef[$i]."\"";
				else
					$clabel[] = $ColumnDef[$i];
			}
			
			// Header
			// $ColumnDef can be empty, i.e. no column header
			if ($ColumnDef != "")
				$x = implode($ColumnDefDelimiter, $clabel).$LineBreak;
			
			// Provide $DataSize (number of column of data retrieved form DB) if
			// 1) number of column header is different from the data
			// 2) no column header
			if ($DataSize == 0)
				$DataSize = sizeof($clabel);
	
			// Body
			for ($i=0; $i<sizeof($data); $i++)
			{
				unset($row);
				for ($j=0; $j<sizeof($data[$i]); $j++) {
					if ($Quoted == "11" || $Quoted == "10"){
						$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), intranet_htmlspecialchars($data[$i][$j])), ENT_QUOTES))."\"";
						//$row[] = "\"".str_replace("\"", "\"\"", html_entity_decode($data[$i][$j], ENT_QUOTES))."\"";
					}
					else{
						$row[] = str_replace("\"", "\"", html_entity_decode(str_replace(array("\n", "\r", "\t"), array(" ", " ", ""), intranet_htmlspecialchars($data[$i][$j])), ENT_QUOTES));
						//$row[] = str_replace("\"", "\"\"", html_entity_decode($data[$i][$j], ENT_QUOTES));
					}
				}
				//debug_r($row);
				$x .= implode($Delimiter, $row).$LineBreak;
				//debug_r($x);
			}
			return $x;
		}
		
		/*
		* Export File
		*/
		function EXPORT_FILE($filename, $data, $charset)
		{
			//debug_r(mb_list_encodings());
			if (strtolower($charset) != 'big5')
			{
				$data = mb_convert_encoding($data,'UTF-16LE','UTF-8');
				// Add BOM (Byte Order Mark) to identify the file is in UTF-16LE encoding
				$data = "\xFF\xFE".$data;	
			}
			
			// BOM header for UTF-8
			//$data = "\xEF\xBB\xBF".$data;	
			header('Pragma: public');
			header('Expires: 0');
			header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	
			header('Content-type: application/octet-stream');
			header('Content-Length: '.strlen($data));
	
			header('Content-Disposition: attachment; filename="'.$filename.'";');
	
			print $data;
		}
	}
}        // End of directive
?>
