<?php
class libfilesystem {

	function libfilesystem(){
		
	}
	
	######################################################################
	
	function file_write($body, $file){
		$x = (($fd = fopen($file, "w")) && (fputs($fd, $body))) ? 1 : 0;
		fclose ($fd);
		return $x;
	}
	
	function file_append($newdata, $file) {
		$f = fopen($file, "a");
		$success = fwrite($f, $newdata);
		fclose($f);
		
		return $success;
	}

	
	function file_read($file){
		clearstatcache();
		if(file_exists($file) && is_file($file) && filesize($file)!=0){
			$x = ($fd = fopen($file, "r")) ? fread($fd,filesize($file)) : "";
			if ($fd)
				fclose ($fd);
		}
		return $x;
	}
	
	function file_remove($file)
	{
		return (file_exists($file)) ? unlink($file) : 0;
	}
	
	function file_copy($source, $dest){
		$dest .= (is_dir($dest)) ? "/".basename($source) : "";
		return copy($source, $dest);
	}
	
	function file_rename($oldfile, $newfile){
		return rename($oldfile, $newfile);
	}
	
	################################ Folder function ################################
	
	function folder_new($location){
		#umask(0);
		return (file_exists($location)) ? 0 : mkdir($location, 0777);
	}
	
	function folder_remove($file){
		return (file_exists($file)) ? rmdir($file) : 0;
	}
	
	######################################################################

	function chmod_R($path, $filemode)
	{
		clearstatcache();
		if (!is_dir($path))
		{
			return chmod($path, $filemode);
		}
		
		$dh = opendir($path);
		while ($file = readdir($dh))
		{
			if ($file != '.' && $file != '..')
			{
				$fullpath = $path.'/'.$file;
				chmod($fullpath, $filemode);
				if(!is_dir($fullpath))
				{
					if (!chmod($fullpath, $filemode))
						return FALSE;
				} 
				else
				{
					if (!$this->chmod_R($fullpath, $filemode))
						return FALSE;
				}
			}
		}
		
		closedir($dh);
		
		if (chmod($path, $filemode))
			return TRUE;
		else
			return FALSE;
	}
}
?>