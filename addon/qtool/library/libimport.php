<?php
if (!defined("LIBIMPORTTEXT_DEFINED"))                     // Preprocessor directive
{
  define("LIBIMPORTTEXT_DEFINED", true);

  class libimport extends libdb
  {		
		/*
		* Initialize
		*/
		function libimport()
		{
			
		}
		
		/*
		* Check Byte Order Mark
		*/
		function CHECK_BOM ($Buffer) {
			$charset[1] = substr($Buffer, 0, 1);
			$charset[2] = substr($Buffer, 1, 1);
			$charset[3] = substr($Buffer, 2, 1);  
			if (ord($charset[1]) == 255 && ord($charset[2]) == 254)
				return "UTF-16LE";
			else if (ord($charset[1]) == 254 && ord($charset[2]) == 255)
				return "UTF-16BE";
			else if (ord($charset[1]) == 239 && ord($charset[2]) == 187 && ord($charset[3]) == 191)
				return "UTF-8";
			else
				return "Unknown";
		}
		
		/*
		* Extract data from file
		*/
		/*
		remark:
		shall add
		$x = addslash($x);
		if using the B5 mode, either at the calling side or here, which is now at the calling side
		*/
		function GET_IMPORT_TXT($ParFilePath, $incluedEmptyRow=0, $lineBreakReplacement='') {
	
			global $intranet_default_lang, $import_coding, $g_encoding_unicode;			
			#$g_encoding_unicode = true;
			
			$Handle = @fopen($ParFilePath, 'r');
			$Buffer = fread($Handle, filesize($ParFilePath));
			$TempFile = tmpfile();
			$BytesLength = 4096; // Modified by key(2008-10-22) - enlarge the bytes length - default 2048
			
			// get file coding input from interface
			// if file coding is b5 / gb, change the coding to utf-8
			if (function_exists(iconv)) {
				
				$CheckBom = $this->CHECK_BOM($Buffer);
				if ($CheckBom == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16LE', 'UTF-8', $NewFileContent);
				} else if ($CheckBom == "UTF-16BE") {
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16BE', 'UTF-8', $NewFileContent);
				} else if ($CheckBom == "UTF-8") {
					$NewFileContent = substr($Buffer, 3);
				} else {
					if ($import_coding == "utf") {
						$NewFileContent = $Buffer;
					} else {
						if (mb_detect_encoding(substr($Buffer,0,1)) == "ASCII") {
							$NewFileContent = iconv("Big5", 'UTF-8//TRANSLIT', $Buffer);
						} else {
							/* Added By Ronald on 20090724 */
							## convert the $Buffer to UTF8 if the original encoding is in unknow format
							$NewFileContent = iconv(mb_detect_encoding($Buffer), 'UTF-8//TRANSLIT', $Buffer);
						}
					}					
				}
			} else {
				$NewFileContent = $Buffer;
			}
			
			// Added by : Ronald (20090827)
			// Purpose : remove the line break if it is occur inside the csv cell //
			// Old Method (this one will have error, so do not use)
			//$NewFileContent = preg_replace('/"([^"]+)(?:\n\r|\n)([^"]+)"/','/"$1$2/"',$NewFileContent);
			// New method 
			/*
			$file = $NewFileContent;
			function removecrlf($match){
				return preg_replace('/\r\n/',' ',$match[0]);
			}
			$file=preg_replace_callback('/(?<!\\\\)".*?(?<!\\\\)"/s','removecrlf',$file);
			$NewFileContent = $file;
			*/
			
			$file = $NewFileContent;
			
			### Added $lineBreakReplacement by Ivan (20091109)
			### Purpose: eRC need to keep the line break for display by passing $lineBreakReplacement = "<br />"
			$lineBreakReplacement = ($lineBreakReplacement=='')? ' ' : $lineBreakReplacement;
			$file = preg_replace_callback(
							'/(?<!\\\\)".*?(?<!\\\\)"/s',
							create_function(
								'$match',
								'return preg_replace("/\n/", "'.$lineBreakReplacement.'", $match[0]);'
							),
							$file
						);
			$NewFileContent = $file;
			
			# remove first 3-byte used for Byte Order Mark
			#$NewFileContent = substr($NewFileContent, 3);
			//echo $NewFileContent;
			fwrite($TempFile, $NewFileContent);
			fseek($TempFile, 0);
			
			while (!feof($TempFile)) {
				//$Data = trim(fgets($TempFile, $BytesLength));		//tempory disable by Ronald
				
				$Data = fgets($TempFile, $BytesLength);
				$DataPieces = explode("\t", $Data);
				for ($i=0; $i<sizeof($DataPieces); $i++) {
					
					$DataPieces[$i] = $this->REPLACE_DQ_IN_STR($DataPieces[$i]);
					
					// check for pattern : ""{DATA}""
					if(substr($DataPieces[$i],0) == "\"\"")
						$DataPieces[$i] = str_replace("\"\"","\"",$DataPieces[$i]);
					
					// check for pattern : "{DATA_1""DATA_2}"
					if(strpos($DataPieces[$i],"\"\"")>0)
						$DataPieces[$i] = str_replace("\"\"","\"",$DataPieces[$i]);
					
					// check for pattern : "{DATA_1\rDATA_2}"
					if(strpos($DataPieces[$i],"\r"))
						$DataPieces[$i] = str_replace("\r","",$DataPieces[$i]);
				}
				
				if($incluedEmptyRow)	# Yat Woon: some function need dislay the empty row [20090427]
				{
					$ReturnArr[] = $DataPieces;
				}
				else
				{
					# Eric Yip : To get rid of empty row (20080905)
					if(count(array_filter($DataPieces)) > 0)
						$ReturnArr[] = $DataPieces;
				}
			}
			
			fclose($TempFile);
			fclose($Handle);

			return $ReturnArr;
		}
		
		function GET_IMPORT_TXT_BIG5($ParFilePath, $incluedEmptyRow=0, $lineBreakReplacement='') {
	
			global $intranet_default_lang, $import_coding, $g_encoding_unicode;			
			#$g_encoding_unicode = true;
			
			
			$Handle = @fopen($ParFilePath, 'r');
			$Buffer = fread($Handle, filesize($ParFilePath));
			$TempFile = tmpfile();
			$BytesLength = 4096; // Modified by key(2008-10-22) - enlarge the bytes length - default 2048
			
			/* Big5 encoding cannot be used in mb_convert_encoding in PHP version < 4.3.0
			if (function_exists(mb_convert_encoding)) {
				if ($this->CHECK_BOM($Buffer) == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'UTF-16LE');
					$NewFileContent = substr($NewFileContent, 2);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-16BE") {
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'UTF-16BE');
					$NewFileContent = substr($NewFileContent, 2);
				} else {
					$NewFileContent = mb_convert_encoding($Buffer, 'UTF-8', 'auto');
				}
			} else
			*/
			
			// get file coding input from interface
			// if file coding is b5 / gb, change the coding to utf-8
			
			if ((function_exists(iconv)) && ($g_encoding_unicode)) {
				
				if ($this->CHECK_BOM($Buffer) == "UTF-16LE") {	// UTF-16LE
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16LE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-16BE") {
					$NewFileContent = substr($Buffer, 2);
					$NewFileContent = iconv('UTF-16BE', 'UTF-8', $NewFileContent);
				} else if ($this->CHECK_BOM($Buffer) == "UTF-8") {
					$NewFileContent = substr($Buffer, 3);
				} else {
					if ($import_coding == "utf") {
						$NewFileContent = $Buffer;
					} else {
						if ($intranet_default_lang == "b5") {
							$NewFileContent = iconv('BIG5', 'UTF-8', $Buffer);						
						} else {
							$NewFileContent = iconv('GB2312', 'UTF-8', $Buffer);
						}
					}					
				}
			} else {
				$NewFileContent = $Buffer;
			}
			//debug_pr($NewFileContent);
			////////////////////////////////////////////////////////////////////////////
			// For imail address testing
			////////////////////////////////////////////////////////////////////////////
			//$NewFileContent = str_replace("\"","",$NewFileContent);
			////////////////////////////////////////////////////////////////////////////
			
			# remove first 3-byte used for Byte Order Mark
			#$NewFileContent = substr($NewFileContent, 3);
			
			//if (($import_coding != "utf")||(!$g_encoding_unicode)) {
				
				// use old get csv function
				
			//} else {
				
				$file = $NewFileContent;
			
				### Added $lineBreakReplacement by Ivan (20091109)
				### Purpose: eRC need to keep the line break for display by passing $lineBreakReplacement = "<br />"
				$lineBreakReplacement = ($lineBreakReplacement=='')? ' ' : $lineBreakReplacement;
				$file = preg_replace_callback(
								'/(?<!\\\\)".*?(?<!\\\\)"/s',
								create_function(
									'$match',
									'return preg_replace("/\r\n/", "'.$lineBreakReplacement.'", $match[0]);'
								),
								$file
							);
				$NewFileContent = $file;
				//debug_pr($NewFileContent);
				
				
				fwrite($TempFile, $NewFileContent);
				
				fseek($TempFile, 0);
				//$contents .= fread($TempFile, 8192);
				//echo $contents; exit;

				if (($import_coding != "utf")||(!$g_encoding_unicode)) {
					
					# fgetcsv() have problem with certain double quotted chinese words
					/*
					while (($Data = fgetcsv($TempFile, 2048, ","))!==FALSE)
					{
						$ReturnArr[] = $Data;
					}
					*/
					
					while (!feof($TempFile)) {
						
						//$TempData = trim(fgets($TempFile, $BytesLength));		# modify by Henry on 20090930 (minimise the "monster" code of Parent name)
						//$Data = explode(",", $TempData);						# end of Henry modification on 20090930  [CRM Ref No.: 2009-0922-1615]
						
						//$Data = trim(fgets($TempFile, $BytesLength));			
						$Data = fgetcsv($TempFile, $BytesLength, ",");		 # yatwoon 20090608 (cater the last cell is empty without "")
						# Henry Chow 20101027 , minimie "monster code" of big5 wording
						setlocale(LC_ALL, 'zh_TW.BIG5');

						# 20090803 yat
						//debug_pr($Data);
						# combine contents that contain link break
						
						if(trim($Data=="")) continue;
						//$Data = '"' . str_replace(",","\",\"",$Data) .'"';
						$Data = '"' . implode("\",\"",$Data) .'"';		# 20090803 yat
						//debug_pr($Data);
						
						$LastCell = substr(strrchr($Data, ","), 1);
			            if(substr($LastCell, 0, 1)=="\"") {
			              while(!feof($TempFile) && substr($Data, -1)!="\"") {
			              	$Data .= "\n".trim(fgets($TempFile, $BytesLength));
			              }
			            }
	            		
			            if(function_exists("mb_substr") && function_exists("mb_strlen")){
							$DataPieces = $this->mb_csv_split($Data);
							
						}else{
							#### Function mb_csv_split() is used to handle special characters
							#### and remove double quotes
							$RawDataPieces = explode(",", $Data);
							$DataPieces = array();
							for ($i=0; $i<sizeof($RawDataPieces); $i++) {
								//direct (first char not '"') or combine 
								$tmpDataPiece = $RawDataPieces[$i];
								$StrLength = strlen($tmpDataPiece);
								$Str = trim($tmpDataPiece);
								
								# remark the empty function, bug fix for extra field (yat woon 20081010)
								/*
								if (substr($Str, 0, 1) == "\"" && (substr($Str, $StrLength-1) != "\"" || substr($Str, $StrLength-2, 1) != "\"" || substr($Str, $StrLength-3, 1) != "\"")) {
									while (substr($RawDataPieces[$i+1], $StrLength-1) != "\"") {
										
									}
								}
								*/
								
								#### Disabled this block of code on 22-11-2007 by Andy Chan
								#### It cause problem whenever data are enclosed by double quotes
								# Eric Yip (20090706): Check if quotes pair up in raw data pieces before searching following data pieces
								if(!empty($RawDataPieces[$i]) && $RawDataPieces[$i][0] == '"' && $RawDataPieces[$i][strlen($RawDataPieces[$i])-1] != '"'){
									//look for combine end
									for($j=$i+1; $j<sizeof($RawDataPieces);$j++){
										$len = strlen($RawDataPieces[$j]);
										$tmpDataPiece .= ','.$RawDataPieces[$j];
										$i++;	# bug fix for extra field (yat woon 20081010)
										if($len > 0 && $RawDataPieces[$j][$len-1] == '"'){
											break;
										}
									}
								}
								
								$DataPieces[] = $this->REPLACE_DQ_IN_STR($tmpDataPiece);
							}
						}
						
						if($incluedEmptyRow)	# Yat Woon: some function need dislay the empty row [20090427]
						{
							$ReturnArr[] = $DataPieces;
						}
						else
						{
							# Eric Yip : To get rid of empty row (20080905)
							if(count(array_filter($DataPieces)) > 0)
								$ReturnArr[] = $DataPieces;
						}
					}
					//exit;
					
				} else {
					# fgetcsv() have problem with certain double-quotted chinese words
					/*
					while (($Data = fgetcsv($TempFile, 2048, "\t"))!==FALSE)
					{
						$ReturnArr[] = $Data;
					}
					*/
					while (!feof($TempFile)) {
						$Data = trim(fgets($TempFile, $BytesLength));
						# combine contents that contain link break
						$LastCell = substr(strrchr($Data, "\t"), 1);
			            if(substr($LastCell, 0, 1)=="\"") {
			              while(!feof($TempFile) && substr($Data, -1)!="\"") {
			              	$Data .= "\n".trim(fgets($TempFile, $BytesLength));
			              }
			            }
						$DataPieces = explode("\t", $Data);
						for ($i=0; $i<sizeof($DataPieces); $i++) {
							$DataPieces[$i] = $this->REPLACE_DQ_IN_STR($DataPieces[$i]);
						}

						if($incluedEmptyRow)	# Yat Woon: some function need dislay the empty row [20090427]
						{
							$ReturnArr[] = $DataPieces;
						}
						else
						{
							# Eric Yip : To get rid of empty row (20080905)
							if(count(array_filter($DataPieces)) > 0)
								$ReturnArr[] = $DataPieces;
						}
					}
				}
				
				fclose($TempFile);
				fclose($Handle);
			//}
//echo '<pre>';
//print_r($ReturnArr);
//echo '<pre>';
//die;	


			return $ReturnArr;
		}
		
		/*
		* Replace Double quotation mark of the string (1st character & last character)
		*/
		function REPLACE_DQ_IN_STR($Str) {
			$StrLength = strlen($Str);
			$Str = trim($Str);
			if (substr($Str, 0, 1) == "\"" && (substr($Str, $StrLength-1) == "\"" || substr($Str, $StrLength-2, 1) == "\"" || substr($Str, $StrLength-3, 1) == "\"")) {
				$Str = substr($Str, 1);
				$Str = substr($Str, 0, strlen($Str)-1);
			}
			return $Str;
		}
		
	    ## Modified: by Ronald (20090331)
	    ## Changed:	Now can handle if there is a double quote inside the cell element
	    function mb_csv_split($line, $delim = ',', $removeQuotes = true) {
	    	global $g_encoding_unicode;
	    
		    $fields = array();
		    $fldCount = 0;
		    $inQuotes = false;
		    
		    # Eric Yip (20081204) : set internal encoding for mb functions
		    if($g_encoding_unicode)
		    	mb_internal_encoding("UTF-8");
		    else
		    	mb_internal_encoding("pass");
		
		    for ($i = 0; $i < mb_strlen($line); $i++) {
			    if (!isset($fields[$fldCount])) $fields[$fldCount] = "";
			    $tmp = mb_substr($line, $i, mb_strlen($delim));
			    if ($tmp === $delim && !$inQuotes) {
				    $fldCount++;
				    $i+= mb_strlen($delim) - 1;
			    }
			    else if ($fields[$fldCount] == "" && mb_substr($line, $i, 1) == '"' && !$inQuotes) {
				    if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
				    //echo " A:".mb_substr($line, $i, 1);
				    $inQuotes = true;
			    }
			    else if (mb_substr($line, $i, 1) == '"') {
				    if (mb_substr($line, $i+1, 1) == '"') {
					    $i++;
					    $fields[$fldCount] .= mb_substr($line, $i, 1);
					    //echo " B:".mb_substr($line, $i, 1);
				    } else {
					    //if (!$removeQuotes) $fields[$fldCount] .= mb_substr($line, $i, 1);
					    
					    if((mb_substr($line, $i+1, 1) != $delim) && ($i!=mb_strlen($line)-1)){
						    $fields[$fldCount] .= mb_substr($line, $i, 1);
						    $inQuotes = true;
					    }
						else
						{
							$inQuotes = false;
						}
				    }
			    }
			    else {
				    $fields[$fldCount] .= mb_substr($line, $i, 1);
			    }
		    }
			//print_r($fields);
		    return $fields;
	    }
		
	}	// End of class definition

}        // End of directive
?>