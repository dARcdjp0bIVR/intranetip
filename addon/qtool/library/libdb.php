<?php

class libdb {

	var $db;
	var $rs;
	var $connectSuccess;
	var $executionTime;
	
	function libdb($Database){
		$this->db = $Database;
	}
	
	# For calculate sql execulation time
	function Get_Micro_Time(){ 
		list($usec, $sec) = explode(" ",microtime()); 
		return ((float)$usec + (float)$sec); 
	} 
	function Calculate_Execution_Time($StartTime, $EndTime){ 
		return $EndTime - $StartTime; 
	} 
	function Get_Execution_Time()
	{
		return number_format($this->executionTime, 4);
	}
	
	# database function
	function Open_DB($CurSettingsArr)
	{
		$Host = ($CurSettingsArr == '')? 'localhost' : $Host;
		$this->connectSuccess = mysql_connect($CurSettingsArr['MySQL_Host'], $CurSettingsArr['MySQL_UserName'], $CurSettingsArr['MySQL_Password']);
		
//		debug_pr($CurSettingsArr['MySQL_Host']);
//		debug_pr($CurSettingsArr['MySQL_UserName']);
//		debug_pr($CurSettingsArr['MySQL_Password']);
		
		$charset_db = str_replace('-', '', $CurSettingsArr['Charset']);
		//if ($charset_db == 'utf8' && $this->connectSuccess != '' && $CurSettingsArr['Platform'] != 'ej')
		if ($charset_db == 'utf8' && $this->connectSuccess != '')
		{
			mysql_query("set character_set_database='$charset_db'");
			
			$setName = true;
			if ($CurSettingsArr['Platform']=='ej') {
				$libmsPos = strpos($this->db, 'eClass_LIBMS');
				$isLibmsDb = true;
				if ($libmsPos === false) {
					$isLibmsDb = false;
				}
				if ($isLibmsDb) {
					$setName = false;
				}
			}
			
			if ($setName) {
				mysql_query("set names $charset_db");
			}
		}
	}
	
	function Close_DB(){
		if ($this->connectSuccess != '')
			mysql_close();
	}
	
	function db_insert_id(){
		return mysql_insert_id();
	}
	
	function db_num_rows(){
		return mysql_num_rows($this->rs);
	}
	
	function db_create_db($database){
		$time_start = $this->Get_Micro_Time();
		
		$sql = 'CREATE DATABASE `'.$database.'`';
		
		$time_end = $this->Get_Micro_Time();
		$this->executionTime = $this->Calculate_Execution_Time($time_start, $time_end);
		
		return mysql_query($sql);
	}
	
	function db_db_query($query){
		$time_start = $this->Get_Micro_Time();
		
		if ($this->connectSuccess != '') {
			//$q_result = mysql_db_query($this->db, $query);
			mysql_select_db($this->db, $this->connectSuccess);
			$q_result = mysql_query($query, $this->connectSuccess);
		}
		
		$time_end = $this->Get_Micro_Time();
		$this->executionTime = $this->Calculate_Execution_Time($time_start, $time_end);
		
		return $q_result;
	}
	
	function db_data_seek($row_number){
		return mysql_data_seek($this->rs, $row_number);
	}
	
	function db_fetch_array($ResultArrayType=0){
    		switch ($ResultArrayType) {
    			case 1:
    				$result_type = MYSQL_ASSOC;
    				break;
    			case 2:
    				$result_type = MYSQL_NUM;
    				break;
    			default:
    				$result_type = MYSQL_BOTH;
    		}
    		
            return mysql_fetch_array($this->rs, $result_type);
    }
	
	function db_fetch_assoc(){
		return mysql_fetch_assoc($this->rs);
	}
	
	function db_free_result(){
		return mysql_free_result($this->rs);
	}
	
	function db_affected_rows(){
		return mysql_affected_rows();
	}
	
	function returnResultSet($sql,$ResultArrayType=1){
		return $this->returnArray($sql,null,$ResultArrayType);
	}
	
	function returnArray($sql, $field_no=null, $ResultArrayType=0){
		$i = 0;
		$this->rs = $this->db_db_query($sql);
		$x = array();
		
		if ($this->rs && $this->db_num_rows()!=0)
		{
			while ($row = $this->db_fetch_array($ResultArrayType))
			{
				$x[] = $row;
			}
			$this->db_free_result();
		}
		return $x;
	}
	
	function returnAssoArray($sql, $field_no=null){
		$i = 0;
		$this->rs = $this->db_db_query($sql);
		$x = array();
		if ($this->rs && $this->db_num_rows()!=0)
		{
			while ($row = $this->db_fetch_assoc())
			{
				$x[] = $row;
			}
			$this->db_free_result();
		}
		return $x;
	}
	
	#########################################################################
	# return 1-D array to store the sql result of selecting 1 column only
	#
	function returnVector($sql){
		$i = 0;
		$x = array();
		$this->rs = $this->db_db_query($sql);
		if($this->rs && $this->db_num_rows()!=0){
			while($row = $this->db_fetch_array()){
				$x[$i] = $row[0];
				$i++;
			}
		}
		if ($this->rs)
			$this->db_free_result();
		return $x;
	}
	
	function Get_Safe_Sql_Query($value) {
		return str_replace(array("\\","'"),array("\\\\","\\'"), $value);
	}
	
	function Start_Trans() {
		mysql_query("START TRANSACTION");
	}
	
	function Commit_Trans() {
		mysql_query("COMMIT");
	}
	
	function RollBack_Trans() {
		mysql_query("ROLLBACK");
	}
	
	function With_Nolock_Trans() {
		mysql_query("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
	}
	
	function Get_Analyzed_Final_Sql($sqlAry) {
		$finalSqlAry = array();
		
		$numOfSql = count($sqlAry);
		for ($i=0; $i<$numOfSql; $i++) {
			$_sql = $sqlAry[$i];
			
			$_primaryKeySqlSymbol = getPresetSqlSymbol('primaryKey');
			$_primaryKeyPos = strpos($_sql, $_primaryKeySqlSymbol);
			if ($_primaryKeyPos === false) {
				$finalSqlAry[] = $_sql;
			}
			else {
				$_sqlPieces = explode(' ', $_sql);
				$_tableName = $_sqlPieces[3];
				
				$_descAry = $this->returnResultSet('Desc '.$_tableName);
				$_primaryKey = $_descAry[0]['Field'];
				
				$finalSqlAry[] = str_replace($_primaryKeySqlSymbol, $_primaryKey, $_sql);
			}
		}
		
		return $finalSqlAry;
	}
}

?>
