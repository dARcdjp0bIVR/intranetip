<?
// kenenth chugn
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StudentAttendanceDisDuplicateProfileDataPatch'] != 1 && $plugin['attendancestudent']) {
	echo 'Start on StudentAttendanceDisDuplicateProfileDataPatch';
	# class used
	$db = new libdb();
	
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	$StudentAttend = new libcardstudentattend2();
	
	if ($plugin['Disciplinev12']){
		include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
		$ldisciplinev12 = new libdisciplinev12();
	}
	
	$sql = 'show create table PROFILE_STUDENT_ATTENDANCE';
	$TableCreateScript = $db->returnVector($sql);
	if (stristr($TableCreateScript[0],"StudentDateTypeDay") === FALSE) {
		$sql = 'select
						  UserID,
						  AttendanceDate,
						  DayType,
						  RecordType, 
						  count(1) as total
						from
						  PROFILE_STUDENT_ATTENDANCE
						Group by
						  UserID,AttendanceDate,DayType,RecordType
						having
						  count(1) > 1 
						order by 
							UserID, attendanceDate';
		$DuplicatedRecord = $db->returnArray($sql);
		
		$TotalBrokenProfile = 0;
		for ($i=0; $i< sizeof($DuplicatedRecord); $i++) {
			list($StudentID,$AttendanceDate,$DayType,$RecordType) = $DuplicatedRecord[$i];
			
			if ($plugin['Disciplinev12'] && $RecordType == CARD_STATUS_LATE){
				$sql = 'select 
									StudentAttendanceID 
								from 
									PROFILE_STUDENT_ATTENDANCE psa 
									LEFT JOIN 
									CARD_STUDENT_PROFILE_RECORD_REASON prr 
									on psa.StudentAttendanceID = prr.ProfileRecordID 
								where 
									psa.UserID = \''.$StudentID.'\' 
									and 
									psa.AttendanceDate = \''.$AttendanceDate.'\' 
									and 
									psa.DayType = \''.$DayType.'\' 
									and 
									psa.RecordType = \''.$RecordType.'\' 
									and 
									prr.RecordID IS NULL
									';
				$BrokenProfile = $db->returnVector($sql);
				
				for ($j=0; $j < sizeof($BrokenProfile); $j++) {
					$sql = "select RecordID from DISCIPLINE_ACCU_RECORD where StudentAttendanceID='".$BrokenProfile[$j]."'";
					$Temp = $db->returnVector($sql);
					if (sizeof($Temp) > 0)
						$ldisciplinev12->DELETE_LATE_MISCONDUCT_RECORD($BrokenProfile[$j]);
				}
			}
			
			$sql = 'delete psa from 
								PROFILE_STUDENT_ATTENDANCE psa 
								LEFT JOIN 
								CARD_STUDENT_PROFILE_RECORD_REASON prr 
								on psa.StudentAttendanceID = prr.ProfileRecordID 
							where 
								psa.UserID = \''.$StudentID.'\' 
								and 
								psa.AttendanceDate = \''.$AttendanceDate.'\' 
								and 
								psa.DayType = \''.$DayType.'\' 
								and 
								psa.RecordType = \''.$RecordType.'\' 
								and 
								prr.RecordID IS NULL';
			$StudentRemoveResult[] = $db->db_db_query($sql);
			
			$TotalBrokenProfile += sizeof($BrokenProfile);
		}
		
		$sql = 'alter ignore table PROFILE_STUDENT_ATTENDANCE ADD UNIQUE KEY StudentDateTypeDay (UserID,AttendanceDate,DayType,RecordType)';
		$StudentRemoveResult[$sql] = $db->db_db_query($sql);
	}
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StudentAttendanceDisDuplicateProfileDataPatch', 1, now())";
	$Result['UpdateGeneralSettings'] = $db->db_db_query($sql);
	
	echo 'end of StudentAttendanceDisDuplicateProfileDataPatch';
}
?>