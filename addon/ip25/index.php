<?php
# using :
###############################################
##### includes Chinese character!!!!!!!!!!! 與工作有關的經驗, 總分,過程
##### Pls open this file with utf-8 editor!!!!!!!!!!!!!!!!!!!!!!!!
###############################################
###############################################
###############################################



/*******************************************
*	modification log
*       2020-11-24 Bill     [IP30 DM#1014]
*           - Added data patch: eDiscipline_Update_All_Case_Record_YearTermID
 *       2020-10-28 Cameron
 *           - Repair System - build FollowupPerson table and update LastFollowupGroupID [case X190084]
*       2020-09-18 Bill
*           - Added Power Portfolio schema update script
*       2020-09-17 Bill     [2020-0604-1821-16170]
*           - Added data patch: eNotice_Clone_Approval_Users_To_Payment_Notice
*       2020-06-19 Cameron
*           - set default value for PrintSenCasePICinStudentReport in GENERAL_SETTING
*       2020-06-11 Ray
*           - Added data patch: AddLeaveTypeIDFieldLessonAttendance
*       2020-05-13 Cameron
*           - patch SeqNo for SENType and SENSubType in INTRANET_GUIDANCE_SETTING_ITEM (eGuidance) [case #M151032]
*       2020-03-20 Pun
*           - Added Init eLearning timetable
* 		2020-03-16 Henry
* 			- App request log table reset [case #M175337]
*       2020-03-03 Cameron
*           - eGuidance - patch SENType in INTRANET_GUIDANCE_SEN_CASE to allow text and sub-type co-exist [case #D178788]
*       2020-02-21 Cameron
*           - patch data: eGuidance_Swap_DateModified_DateInput_for_AllowAccessIP [case #D180421]
*       2019-09-23 Cameron
*           - patch data: copy RecordStatus and Attendance Info from INTRANET_SCH_BUS_APPLY_LEAVE to INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT [case #Y170676]
*       2019-08-15 Cameron
*           - patch data: add Migrate_student_registry_submitted_status_hk [case #Y164458]
*		2019-08-15 Carlos
*			-  Added data migration patch for [Student Attendance]: Migrate "Absent Preset Teacher's Remarks" text data to "Preset Teacher's Remarks" DB data.
*       2019-07-26 Ray
*           - Added data patch: ePayment Add Default Push Message
*		2019-06-21 Carlos
*			- Added data patch: Clear INTRANET_USER.UserPassword and INTRANET_ARCHIVE_USER.UserPassword to NULL
*       2019-05-13 Cameron
*           - enclosed var with apostrophe in all sql
*       2019-04-18 Cameron
*           - Insert default value to PRESET_CODE_OPTION_Kentville for Student Data Record [case #Y140316]
*       2019-02-18 Cameron
*           - eSchoolBus - update RouteTeacher to be multiple
*		2019-01-21 Carlos
*			- added patch for Student Attendance patching preset absence records approval time and approval user from apply leave records.
*       2018-10-02 Cameron
*           - update TransAmountBefore and TransAmountAfter for LIBMS_BALANCE_LOG [case #T139412]
*       2018-05-23 Cameron
*           - update SeqNo for SEN adjustment type, adjustment item, service type and service item in eGuidance
*       2018-03-20 Bill
*           - exclude client with Report Card System (Kindergarten Version) from building .htaccess file
*       2018-03-02 Cameron
*           - update DateInput and InputBy for event, event followup, case, award scheme and revisit for medical [case #F135176]
*		2018-01-17 Omas
*			- ePCM patch old data to support new approval logic
*		2018-01-05 Henry
*			- break eLibPlus group access right from "return" into two rights: return and lost [case #F131622]
*		2017-12-13 Cameron
*			- sync BookCategoryType from LIBMS db to INTRANET_ELIB_BOOK [case #F130026]
*		2017-12-06 Omas
*			- added patch for ePCM approval rules
*		2017-12-05 Carlos
*			- added patch for patching wrong record type for imported attendance profiles and merit profiles.
*		2017-11-22 Ivan
*			- add push message device data patch of "did not login for long" devices
*		2017-11-15 Paul
*			- add ta_20171115_insert_new_default_forms to insert new forms for teacher appraisal
*		2017-08-22 Cameron
*			- add eGuidance_InitDataSetting to install eGuidance
*		2017-06-29 Carlos
*			- Modified /file/.htaccess to prevent http access *log files.
*		2017-06-06 Cameron
*			- break eLibPlus group access right from "overdue report" into two rights: overdue report and penalty report [case #C117984]
*		2017-05-25 Henry
*			- bug fixed for Unknown database 'XXX_eClass_LIBMS' when new installation cause by $_SESSION['LIBMS']['admin']['check_ip_for_circulation']
*		2017-04-18 Paul
*			- Update eBook Page Info
*		2017-04-13 Carlos
*			- Student Attendance - Initialize values for TeacherCanManageXXX settings.
*		2017-03-10 Carlos
*			- create /file/.htaccess for preventing user from direct access photo from browser.
*		2017-02-07 Ronald
*			- add module kisworksheets
*		2017-01-25 Cameron
*			- Sync Min(LIBMS_BOOK_UNIQUE.CreationDate) by BookID to newly added INTRANET_ELIB_BOOK.AcccountDate for eLib+
*		2017-01-17 Cameron
*			- disable pBook related settings in Portal Settings if $plugin['eLib_Lite']=true in eLib+
*		2017-01-16 Cameron
*			- disable eBook related settings in Portal Settings if client hasn't ever purchased eBook in eLib+
*		2016-10-05 Villa
*			-ePCM -  Insert first RULE information into INTRANET_PCM_RULE with quotation code 'O'
*		2016-10-04 Carlos
*			- added data patch to setup cron job for HKUSPH.
*		2016-09-28 Pun
*			- added data patch for classroom that missing intranet_user_id
*		2016-09-14 Cameron
*			- set INTRANET_ELIB_BOOK.Publish=0 for eLib+ lost book in LIBMS_BOOK if NoOfCopy=0
*		2016-08-24 Kenneth
*			- eProcurement: add role of vice principal
*			- eProcurement: data patch for type == 'N', endorsment
*			- eProcurement: data patch for type != 'N', endorsment (when flag turn on only)
*		2016-07-21 Ivan
*			- remove old student app login records due to using parent app to login student accounts
*		2016-07-18 Bill
*			- replace split() by explode(), for PHP 5.4
*		2016-07-18 Paul
*			- UPDATE SBA default scheme by Mr Yeung
*		2016-07-12 Cameron
*			- break eLibPlus group access right from "stock-take and write-off" into two rights: stock-take and write-off [case #K94933]
*			- sync elib+ LIBMS_BOOK.Subject to IP INTRANET_ELIB_BOOK.RelevantSubject
*		2016-06-06	Kenneth
*			- Update Patch, eNotice End Date with 23:59:59
*		2016-06-02 Carlos
*			- Lesson Attendance - add reason field to all SUBJECT_GROUP_STUDENT_ATTENDANCE_[ACADEMIC_YEAR_ID] tables
*		2016-05-18 Kenneth
*			- added data patch for the initialization of new module - eProcurement (ePCM)
*				- add rules, role, defaultAcademicYear
*		2016-05-12 Carlos
*			- moved Student Attendance data patch that involves create new tables to the top before general table schema update is run.
*		2016-01-07 Carlos
*			- moved Staff Attendance V3.0 and ePOS migration scripts to the top before general sql schema update because the migration create new tables.
*		2015-10-07 Siuwan
*			- create flipped channels special classroom to store builder content from flipped channels
*		2015-08-26 Henry [ej.5.0.5.10.1]
*			- bug fixed for Unknown database 'junior20_eClass_LIBMS' when new installation [Case#U79554]
*		2015-07-09 Henry
*			- added data patch for syn the Item RecordStatus from LIBMS_BOOK_UNIQUE to LIBMS_STOCKTAKE_LOG
*		2015-06-19 Siuwan
*			- added data patch for sba new rubrics (require includes/sba/initSettings/initRubricSQL2.php and $plugin['SBA_HKEAA_2017_Rubrics'] = true)
*		2015-05-07 Omas
*			- added data patch for deleting duplicated eEnrolment Attendance Record
*		2015-05-06 Carlos
*			- added schema update patch for ePayment, all tables float data type are changed to decimal(13,2)
*		2015-04-30 Pun
*			- added @ini_set('memory_limit', -1); for this file
*		2015-04-01 Carlos
*			- added data patch to clean remaining shared mailbox member mapping records.
*		2015-03-27 Pun
*			- Add data patch to calculate SD for annual score
*		2015-03-20 Henry
*			- change flag to syn the BookSubTitle field between LIBMS_BOOK and INTRANET_ELIB_BOOK
*			- change flag to add data patch for syn the BookLanguageCode field between LIBMS_BOOK_LANGUAGE and LIBMS_BOOK
*		2015-02-25 Omas
*			- add data patch for re-assign OLE order to student who have record with Order = 0
*		2015-02-11 Henry
*			- add syn the BookSubTitle field between LIBMS_BOOK and INTRANET_ELIB_BOOK
*		2015-02-10 Henry
*			- add data patch for syn the BookLanguageCode field between LIBMS_BOOK_LANGUAGE and LIBMS_BOOK
*		2015-02-09 Henry
*			- add data patch for update the barcode of broadlearning account (INTRANET_USER and LIBMS_USER)
*		2015-01-19 Ivan
*			- App: added data patch to fill default value "pushMessage" to Functionality of INTRANET_APP_CENTRAL_SERVER_SETTINGS
*		2015-01-02 Henry
*			- eLibPlus - data patch to syn the gender field between LIBMS_USER and INTRANET_USER
*		2014-12-16 Bill
*			- iPortfolio - Transfer Submission Period Settings in text file to DB
*		2014-12-05 Charles Ma [IP - ip.2.5.5.12.1] 20141205-updateebookdata
*			- Update eBook data mechanism
*		2014-11-14	YatWoon
*			- Update if no $sys_custom['AddonSchema']['ModuleDeleteLogCleanPeriod'] setting, set default to 12
*		2014-11-13	Omas
*			- [Enrol]Added patch for migrating performance comment from admin console to front-end, Assign record type for existing achievement comment bank
*		2014-08-28	Carlos
*			- added patch for changing osapi file permission to rwxr-xr-x
*		2014-07-14	YatWoon [G63826]
*			- Improved: [eInventory] Requested by CS, set 3 default barcode label templates for NON-SKH clients
*			- deploy: ip.2.5.5.8.1
*		2014-07-02	Carlos
*			- add data patch /addon/script/patch_secure_data.php for copying existing encrypted passwords to splitted data fields
*		2014-05-13 	Henry
*			- add data patch script 'preset_eLib_stocktake_period'
*		2014-05-12 	Carlos
*			- eEnrollment - $sys_custom['eEnrolment']['TWGHCYMA'] added script to transform activity single category to multiple categories
*		2014-04-11	Pun
*			- Medical - move "Split Convulsion & Syndrome > Eye > Red(Tear)" to includes/cust/medical/install/initStudentLogLev3Lev4Data.php
*		2014-04-09	Carlos
*			- Change HKID field to varchar(14) for Malaysia clients
*
*		2014-02-14  Cameron
*			- Medical - Split Convulsion & Syndrome > Eye > Red(Tear) into two items: Red Eye and Tear
*
*		2014-01-10	Carlos
*			- House keeping of delete log MODULE_RECORD_DELETE_LOG, exclude modules 'DISCIPLINE' and 'eInventory'
*
*		2013-11-01	YatWoon
*			- eInventory - data patch for Bulk item offline stocktake date
*
*		2013-10-22	Carlos
*			- add data transform script /addon/ip25/epayment_transform_payment_item_subsidy_sources.php
*
*		2013-05-14	YatWoon
*			- add default data to PRESET_CODE_OPTION table
*
*		2013-04-16  Carlos
*			- Student Attendance - added data patch script student_attendance_daily_remark_add_daytype.php
*
*		2013-03-19 	Carlos
*			- iCalendar - added data patch script icalendar_fix_group_shared_calendars.php
*
*		2013-02-07	Carlos
*			- eInventory - added script to generate barcode for Resource Mgmt Groups and Funding Sources
*
*		2013-01-28  Carlos:
*			- Staff Attendance V3 - added script to add back missing full day outing records
*
*		2013-01-22	Carlos:
*			- iMail : Add data patch to recalculate all users used quota storage
*
*		2013-01-10 	Carlos:
*			- clean MODULE_RECORD_DELETE_LOG records that are older than $sys_custom['AddonSchema']['ModuleDeleteLogCleanPeriod'] months,
*			  if no $sys_custom['AddonSchema']['ModuleDeleteLogCleanPeriod'], default 3
*
*		2012-10-11	Bill:
*			- JUPAS : Add two columns to OEA_STUDENT_ACADEMIC
*
*		2012-08-27	Bill:
*			- SBA : Check and Update default SBA Scheme
*
*		2012-08-30	Fai:
*			- SBA : Set default survey question
*
*		2012-08-27	Bill:
*			- iPortfolio : Generate Role iPortfolio Admin, assign ipf admin right and copy iPF admin from corresponding grouping_function table to table ROLE_MEMBER
*
*		2012-08-06	Henry:
*			- eDiscipline : Change data type of "DISCIPLINE_MERIT_RECORD.ConductScoreChange" from int to float [CRM : 2012-0322-1710-27067]
*
*		2012-08-06	Henry/Bill:
*			- Student Mgmt : Sync "Nationality", "Place of birth" and "Admission Date" from iPortfolio to Account Mgmt (Student Mgmt) if any
*
*		2012-07-03	YatWoon:
*			- eInventory: set Category and Sub-category record status to "in use"
*
*		2012-06-07 YatWoon:
*			- add script for "eInventory bulk items modification"
*
*		2012-05-08 Carlos:
*			- add data patch script /addon/ip25/student_attendance_fix_nocardaction.php to fix no card entrance records
*
*		2012-04-16 Henry Chow:
*			- added script to preset category setting to "eDis Approval Group (AP)"
*
*		2012-04-12 Carlos:
*			- add script /addon/ip25/staff_attend_v3_default_waive_leave_record.php to fix auto set dailylog and profile waived if reason is deafult waived
*
*		2012-02-10 fai:
*			- move "iPortfolioPatchOLEAYearID" to a function f: PatchOLEAYearID and place under includes/libpf-slp.php
*
*		2011-12-16 Ivan:
*			- added script for iPorfolio Dynmaic Report default template
*
*		2011-12-16 Carlos:
*			- add script /addon/ip25/student_attendance_add_pm_modify_fields.php
*
*		2011-09-15 Yuen:
*			- implemented a mechanism to force full DB schema update even the attempt is for new schema only
*
*		2011-05-25	YatWoon:
*			- Inventory - Transfer the settings data from /file/inventory/xxx.txt to table GENERAL_SETTING
*
*		2011-05-25 Yuen:
*			- loged end/complete time and version number of source code
*			- improved the detection of variable with the latest date
*
*		2011-04-21	(Henry Chow)
*			- re-run the script /addon/script/sync_edis_to_profile.php
*
*		2011-02-24	YatWoon
*			- add pre-set eCircular / eNotice / eSurvey reply slip max option to 50
*
*		2011-02-11	[YatWoon]
*			- add update broadlearning password script
*
*		2011-02-09 (FAI)
*			- IES ReInit for the stage marking criteria , mainly for ENG stage 2 and stage 3
*		2011-02-09 (Thomas)
*			- Added Reset IES Scheme DOC Export Setting to Default
*		2011-02-07 (Jason)
*			- modify the checking logic of eClass41 with SettingName = 'ModuleVersion_eClass_4.1'
*		2010-12-09 (Henry Chow):
*			- Re-calculate conduct balance
*			- sync records between eDisv12 & Student Profile
*
*		2010-09-24 YatWoon:
*			- Display "Data Patch" also in "only new updates"
*
*		2010-09-01 Yuen:
*			- fixed the problem of checking new schema by comparing dates in time instead of string
*			- also, added flag2=1 for updating new schema
*
*
******************************************/

# this can force to run full DB schema update
# when there is a need to perform full update compulsory, append array entry with date
$EnableCompulsory = true;
$COMPULSORY_FULLUPDATE[] = "2011-09-21";





@SET_TIME_LIMIT(216000);
@ini_set('memory_limit', -1);
$PATH_WRT_ROOT = ($PATH_WRT_ROOT!="") ? $PATH_WRT_ROOT : "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
if (!$CallFromSameDomain)
{
	include_once($eclass_filepath.'/addon/check.php');
}
include_once($eclass_filepath.'/src/includes/php/lib-filesystem.php');
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

include_once($PATH_WRT_ROOT."includes/libportfolio.php");
include_once($PATH_WRT_ROOT."includes/libportfolio2007a.php");
include_once($PATH_WRT_ROOT."includes/libpf-slp.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates_ipf.php");

include_once('sql_table_update.php');
intranet_opendb();


function GetGreatestDate($arr_schema)
{
	$last_date = "2000-01-01";
	for ($i=0; $i<sizeof($arr_schema); $i++)
	{
		if (strtotime($arr_schema[$i][0])>strtotime($last_date))
		{
			$last_date = $arr_schema[$i][0];
		}
	}
	return $last_date;
}

function DeteminNewUpdate($sql_arr, $logs)
{
	for ($i=0; $i<sizeof($sql_arr); $i++)
	{
		$sql = $sql_arr[$i];

		if (is_array($sql))
		{
			$SqlCodeByHash = MD5($sql[2]);
			$WasExcuted = (is_array($logs) && is_array($logs[$SqlCodeByHash]));

			if (!$WasExcuted)
			{
				return true;
			} else
			{
				//echo $i."-".$WasExcuted.";";
			}

		}
	}

	return false;
}


function ReDetermineResult($ErrorMsg)
{
	$ErrorMsg = strtoupper($ErrorMsg);
	if (strstr($ErrorMsg, strtoupper("1304 : FUNCTION")))
	{
		# 1304 : FUNCTION MINIMUM already exists
		return "1";
	}  elseif (strstr($ErrorMsg, strtoupper("1146 : Table")))
	{
		# 146 : Table 'eclass40_c551.web_portfolio' doesn't exist
		return "1";
	}  elseif (strstr($ErrorMsg, strtoupper("1050 : Table")))
	{
		# 1050 : Table 'ATTEND_DEFAULT_PLAN' already exists
		return "1";
	}  elseif (strstr($ErrorMsg, strtoupper("1054 : Unknown column")))
	{
		# 1054 : Unknown column 'RemarkInternal' in 'LIBMS_CSV_TMP'
		return "1";
	} elseif (strstr($ErrorMsg, strtoupper("1060 : Duplicate column name")))
	{
		# 1060 : Duplicate column name 'AcademicYearID'
		return "1";
	} elseif (strstr($ErrorMsg, strtoupper("1061 : Duplicate key name")))
	{
		# 1061 : Duplicate key name 'DateStudentDayType'
		return "1";
	} elseif (strstr($ErrorMsg, strtoupper("1062 : Duplicate entry")))
	{
		# 1061 : Duplicate key name 'DateStudentDayType'
		return "1";
	} elseif (strstr($ErrorMsg, strtoupper("1068 : Multiple primary key defined")))
	{
		# 1068 : Multiple primary key defined
		return "1";
	} elseif (strstr($ErrorMsg, strtoupper("1091 : Can't DROP")))
	{
		# 1061 : Duplicate key name 'DateStudentDayType'
		return "1";
	}

	return "0";
}


$last_schema_date = "0000-00-00";
function updateSchema($lo, $sql_arr, $course_id, $logs, $IsRunAll=false){
	global $update_count, $last_schema_date, $intranet_db;

	$counter["new_run"] = array();
	$counter["re_run"] = array();
	for ($i=0; $i<sizeof($sql_arr); $i++)
	{
		$sql = $sql_arr[$i];
		if (is_array($sql))
		{
			$SqlCodeByHash = MD5($sql[2]);
			$WasExcuted = (is_array($logs) && is_array($logs[$SqlCodeByHash]));
			if (!$WasExcuted)
			{
				$result = $lo->db_db_query($sql[2]);

				$ErrorMsg = ($result) ? "" : mysql_errno()." : ".mysql_error();

				if (trim($ErrorMsg)!="")
				{
					$result = ReDetermineResult($ErrorMsg);
				}

				if (!$result)
				{
					# log important error
					$counter["new_run"][3][] = "<font face='Arial'>{$ErrorMsg}<font><br /><font color='#2C4BC6' face='Courier'>".$sql[2]."</font></b> <br />\n";
				}

				# insert log
				$sql = "INSERT INTO {$intranet_db}.INTRANET_SCHEMA_LOG (SqlCodeByHash, SqlDate, SqlDescription, DBName, DateRun, Result, ErrorMsg)" .
						" VALUES ('".addslashes($SqlCodeByHash)."', '".addslashes($sql[0])."','".addslashes($sql[1])."','".addslashes($lo->db)."', now(), '{$result}','".addslashes($ErrorMsg)."')";
				$result_log = $lo->db_db_query($sql);
				if (!$result_log)
				{
					echo "<font color='purple'>FAILED to Log</font> - $sql <br />\n";
				}
				$counter["new_run"][$result][] = $sql[2];
			} else
			{
				# for compulsory option, run the sql which might be failed to run before (i.e. result==0)
				if ($IsRunAll && !$logs[$SqlCodeByHash]["Result"])
				{
					$result = $lo->db_db_query($sql[2]);
					$ErrorMsg = ($result) ? "" : mysql_errno()." : ".mysql_error();

					if (trim($ErrorMsg)!="")
					{
						$result = ReDetermineResult($ErrorMsg);
					}

					if (!$result)
					{
						# log important error
						$counter["re_run"][3][] = "<font face='Arial'>{$ErrorMsg}<font><br /><font color='#2C4BC6' face='Courier'>".$sql[2]."</font></b> <br />\n";
					} else
					{
						$sql = "UPDATE {$intranet_db}.INTRANET_SCHEMA_LOG SET DateRun=now(), Result='{$result}', ErrorMsg='".addslashes($ErrorMsg)."' WHERE SqlCodeByHash='".addslashes($SqlCodeByHash)."' ";
						$result_log = $lo->db_db_query($sql);
						if (!$result_log)
						{
							echo "<font color='purple'>FAILED to update Log</font> - $sql <br />\n";
						}
					}

					$counter["re_run"][$result][] = $sql[2];
				}
			}

		}
	}

	# display the result ############################################################################

	if (is_array($counter["new_run"][1]) && sizeof($counter["new_run"][1])>0)
	{
		echo "<hr />[<b>".$lo->db."</b>] <font color='green' size='5'>".sizeof($counter["new_run"][1]) ." schema updates are excuted successfully.</font><hr />";
		$update_count += sizeof($counter["new_run"][1]);
	}
	if (is_array($counter["new_run"][0]) && sizeof($counter["new_run"][0])>0)
	{
		echo "<hr />[<b>".$lo->db."</b>] <font color='red' size='5'>".sizeof($counter["new_run"][0]) ." schema updates are excuted but may not work.</font><hr />";

		if (is_array($counter["new_run"][3]) && sizeof($counter["new_run"][3])>0)
		{
			echo implode("<br /> ", $counter["new_run"][3])."<br /><br />";
		}
	}


	if (is_array($counter["re_run"][1]) && sizeof($counter["re_run"][1])>0)
	{
		echo "<hr />[<b>".$lo->db."</b>] <font color='green' size='5'>".sizeof($counter["re_run"][1]) ." schema updates are re-excuted successfully.</font><hr />";
		$update_count += sizeof($counter["re_run"][1]);
	}
	if (is_array($counter["re_run"][0]) && sizeof($counter["re_run"][0])>0)
	{
		echo "<hr />[<b>".$lo->db."</b>] <font color='red' size='5'>".sizeof($counter["re_run"][0]) ." schema updates are re-excuted but may not work.</font><hr />";

		if (is_array($counter["re_run"][3]) && sizeof($counter["re_run"][3])>0)
		{
			echo implode("<br /> ", $counter["re_run"][3])."<br /><br />";
		}
	}

	if (!(is_array($counter["new_run"][1]) && sizeof($counter["new_run"][1])>0) &&
		!(is_array($counter["new_run"][0]) && sizeof($counter["new_run"][0])>0) &&
		!(is_array($counter["re_run"][1]) && sizeof($counter["re_run"][1])>0) &&
		!(is_array($counter["re_run"][0]) && sizeof($counter["re_run"][0])>0))
	{
		echo "<hr />[<b>".$lo->db."</b>] <font color='pink'>No schema update is run.</font><hr />";
	}


	return;
}

$li = new libdb();
$li->db = $intranet_db;

//debug_r(MD5($sql_eClassIP_update[0][2]));

# new mechanism in 201311
$sql_log_table = "CREATE TABLE IF NOT EXISTS INTRANET_SCHEMA_LOG (
	  LogID int(11) NOT NULL auto_increment,
	  SqlCodeByHash varchar(255) default NULL,
	  SqlDate date NOT NULL,
	  SqlDescription varchar(255),
	  DBName varchar(64),
	  DateRun datetime NOT NULL,
	  Result tinyint(1),
	  ErrorMsg varchar(255),
	  PRIMARY KEY  (LogID)
	) ";
$li->db_db_query($sql_log_table);


$sql = "SELECT DISTINCT SqlCodeByHash, Result FROM INTRANET_SCHEMA_LOG ORDER BY DateRun";
$rows = $li->returnResultSet($sql);
for ($i=0; $i<sizeof($rows); $i++)
{
	$logs[$rows[$i]["SqlCodeByHash"]]["Result"] = $rows[$i]["Result"];
}
//debug_r($logs);


# load history file
$history_file = $PATH_WRT_ROOT."file/db_update_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	//$tmp_arr = split("\n", $history_content);
	$tmp_arr = explode("\n", $history_content);
	//list($last_schema_date, $last_update, $last_update_ip, $run_on_src_version, $schema_update_type, $data_patch_type, $status_final, $complete_time) = split(",", $tmp_arr[sizeof($tmp_arr)-1]);
	list($last_schema_date, $last_update, $last_update_ip, $run_on_src_version, $schema_update_type, $data_patch_type, $status_final, $complete_time) = explode(",", $tmp_arr[sizeof($tmp_arr)-1]);
	$last_schema_date_updated = $last_schema_date;

	$intranet_db_last_date = GetGreatestDate($sql_eClassIP_update);
	$eclass_db_last_date = GetGreatestDate($sql_eClass_update);
	$libms_db_last_date = GetGreatestDate($sql_LIBMS_update);
	if (strtotime($intranet_db_last_date)>strtotime($last_schema_date))
	{
		$is_new_available = true;
		$last_schema_date = $intranet_db_last_date;
	}
	if (strtotime($eclass_db_last_date)>strtotime($last_schema_date))
	{
		$is_new_available = true;
		$last_schema_date = $eclass_db_last_date;
	}
	if (strtotime($libms_db_last_date)>strtotime($last_schema_date))
	{
		$is_new_available = true;
		$last_schema_date = $libms_db_last_date;
	}

	# new mechanism
	$is_new_available = false;
	$is_new_available = DeteminNewUpdate($sql_eClassIP_update, $logs);
	if (!$is_new_available)
	{
		$is_new_available = DeteminNewUpdate($sql_eClass_update, $logs);
	}
	if (!$is_new_available)
	{
		$is_new_available = DeteminNewUpdate($sql_LIBMS_update, $logs);
	}
	$new_update = ($is_new_available) ? "Yes" : "No";

	if ($status_final=="[STARTED]")
	{
		$last_update .= " <font color='red'>".$status_final."</font>";
	} elseif ($status_final=="[COMPLETED]")
	{
		$last_update .= " <font color='green'>".$status_final."</font>";
	}
} else
{
	$new_update = "-";
	$last_update = "-";
	$last_update_ip = "-";
}


############################################################################
if (isset($flag))
{

	if (file_exists($PATH_WRT_ROOT."/includes/version.php"))
	{
		$JustWantVersionData = true;
		include_once($PATH_WRT_ROOT."/includes/version.php");
		$src_version = $versions[0][0];
	}

	# Schema update and data patch starts:
	# log the starttime and update info (latest schema date, time of update, IP of the client)
	$time_now = date("Y-m-d H:i:s");
	$client_ip = getenv("REMOTE_ADDR");
	if (trim($client_ip)=="")
	{
		$client_ip = $_SERVER["REMOTE_ADDR"];
	}

	$lf = new phpduoFileSystem();

	$history_file_compulsory = $PATH_WRT_ROOT."file/db_update_history_compulsory.txt";
	$history_content_compulsory = trim(get_file_content($history_file_compulsory));
	$FullUpdateBefore = strstr($history_content_compulsory, $COMPULSORY_FULLUPDATE[sizeof($COMPULSORY_FULLUPDATE)-1]);

	if ($EnableCompulsory && !$FullUpdateBefore && $flag!=1)
	{
		$flag = 1;
	}

	$history_content .= ($history_content!="") ? "\n" : "";
	$update_type = ($flag==1) ? "full schema update" : "only new schema update";
	$data_patch_type = ($flag2==1) ? "with data patch" : "no data patch";
	$history_content_start = "$last_schema_date,$time_now,$client_ip,$src_version,$update_type,$data_patch_type,[STARTED]";
	$history_content .= $history_content_start;

	$lf->writeFile($history_content, $history_file);


	$update_count = 0;


	$IsReRun = ($flag==1 && $flag!=2);

	/* run the new installation or migration scripts before general schema update because it may create new tables there */
	### pos schema update
	include_once($PATH_WRT_ROOT."addon/ip25/pos_migration_script_temp.php");

	### Staff Attendance V3 Mirgration script
	include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_migration_script_temp.php");

	### Student Attendance - School wish list enhancement list db modification and data patch
	include_once($PATH_WRT_ROOT."addon/ip25/student_attendance_wish_list_data_patch_201007.php");

	/* update intranetIP schema */
	$after_date = ($flag==2 && $last_schema_date_updated!="") ? $last_schema_date_updated : "";
	updateSchema($li, $sql_eClassIP_update, $intranet_db, $logs, $IsReRun);

	/* update eClass schema */
	$li->db = $eclass_db;	 # assign to eClass DB
	$after_date = ($flag==2 && $last_schema_date_updated!="") ? $last_schema_date_updated : "";
	updateSchema($li, $sql_eClass_update, $eclass_db, $logs, $IsReRun);


	// to be considered later when library system is done (also need to create that database)
	if (file_exists($PATH_WRT_ROOT."includes/liblibrarymgmt.php"))
	{
		$temp_var_session = $_SESSION['LIBMS']['admin']['current_right'];
		$temp_var_session2 = $_SESSION['LIBMS']['admin']['class_mgt_right'];
		$temp_var_session3 = $_SESSION['LIBMS']['admin']['check_ip_for_circulation'];
		$_SESSION['LIBMS']['admin']['current_right'] = true;
		$_SESSION['LIBMS']['admin']['class_mgt_right'] = true;
		$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] = true;
		include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");
		$liblms = new liblms();
		$li->db_create_db($liblms->db);
		$li->db = $liblms->db;
		updateSchema($li, $sql_LIBMS_update, '', $logs, $IsReRun);
		$_SESSION['LIBMS']['admin']['current_right'] = $temp_var_session;
		$_SESSION['LIBMS']['admin']['class_mgt_right'] = $temp_var_session2;
		$_SESSION['LIBMS']['admin']['check_ip_for_circulation'] = $temp_var_session3;
	}

	# assign back to IP25 DB
	$li->db = $intranet_db;



	if($flag2==1)
	{
		$lf = new libfilesystem();

		$x .= "<br>\r\n<br>\r\n<br>\r\n<br>\r\n------------------------- Data Patch [Start] ---------------------<br>\r\n<br>\r\n";

		include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
		$lgs = new libgeneralsettings();
		$ModuleName = "InitSetting";
		$GSary = $lgs->Get_General_Setting($ModuleName);

		############################################################
		# eDiscipline v1.2 [Start]
		############################################################
		if($plugin['Disciplinev12'])
		{
			if(!$GSary['eDisciplinev12_Assign_ID_to_APGM_record'] || !$GSary['eDisciplinev12_Sync_AP_Record_with_Student_Profile']) {
				include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
				$ldiscipline = new libdisciplinev12();
			}

			$x .= "===================================================================<br>\r\n";
			$x .= "eDiscipline v1.2 [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			if(!$GSary['eDisciplinev12_Assign_ID_to_APGM_record']) {


				# Award & Punishment Record (eDisciplinev12)
				$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_MERIT_RECORD');

				if(!is_numeric($leftRecord)) {		# already transferred
					$x .= "eDisciplinev12...Award & Punishment Records <font color='blue'>already transferred</font><br>\r\n";
				}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
					$x .= "eDisciplinev12...Transfer Award & Punishment Records to IP25...<font color='blue'>Done</font><br>\r\n";
				} else {							# all Award & Punishment data transfer to IP25 successfully
					$x .= "eDisciplinev12...Transfer Award & Punishment Records to IP25...$leftRecord record(s) <font color='red'>cannot be transferred</font> to IP25<br>\r\n";
				}

				# Good Conduct & Misconduct Record (eDisciplinev12)
				$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_ACCU_RECORD');

				if(!is_numeric($leftRecord)) {		# already transferred
					$x .= "eDisciplinev12...Good Conduct & Misconduct Records <font color='blue'>already transferred</font><br>\r\n";
				}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
					$x .= "eDisciplinev12...Transfer Good Conduct & Misconduct Records to IP25...<font color='blue'>Done</font><br>\r\n";
				} else {							# all Award & Punishment data transfer to IP25 successfully
					$x .= "eDisciplinev12...Transfer Good Conduct & Misconduct Records to IP25...$leftRecord record(s) <font color='red'>cannot be transferred</font> to IP25<br>\r\n";
				}

				# Case Record (eDisciplinev12)
				$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_CASE');

				if(!is_numeric($leftRecord)) {		# already transferred
					$x .= "eDisciplinev12...Case Records already transferred<br>";
				}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
					$x .= "eDisciplinev12...Transfer Case Records to IP25...Done<br>";
				} else {							# all Award & Punishment data transfer to IP25 successfully
					$x .= "eDisciplinev12...Transfer Case Records to IP25...$leftRecord record(s) cannot be transferred to IP25<br>";
				}


				# DISCIPLINE_CONDUCT_ADJUSTMENT / DISCIPLINE_STUDENT_CONDUCT_BALANCE / DISCIPLINE_STUDENT_SUBSCORE_BALANCE
				# DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG / DISCIPLINE_SUB_SCORE_CHANGE_LOG / DISCIPLINE_SEMESTER_RATIO
				# Year > AcademicYearID
				# udpate AcademicYearID first
				$Tary = array("DISCIPLINE_CONDUCT_ADJUSTMENT", "DISCIPLINE_STUDENT_CONDUCT_BALANCE", "DISCIPLINE_STUDENT_SUBSCORE_BALANCE", "DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG", "DISCIPLINE_SUB_SCORE_CHANGE_LOG", "DISCIPLINE_SEMESTER_RATIO");
				include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
				$ac = new academic_year();

				foreach($Tary as $t=>$TmpTableName)
				{
					$sql = "select distinct(Year) from $TmpTableName where AcademicYearID is NULL";
					$result = $li->returnVector($sql);
					if($result)
					{
						foreach($result as $k=>$year)
						{
							$YearTmp = $year;
							$acInfo = $ac->Get_Academic_Year_Info_By_YearName($YearTmp, "", 1);
							if($acInfo && strlen($YearTmp)>1)
							{
								$thisAcademicYearID = $acInfo[0]['AcademicYearID'];
								$update_sql1 = "update $TmpTableName set AcademicYearID=$thisAcademicYearID where Year='$YearTmp' and AcademicYearID is NULL";
								$li->db_db_query($update_sql1);

								$x .= "eDisciplinev12...$TmpTableName [AcademicYearID] transfer year information: " . $YearTmp ." <font color=blue>Success.</font><br>";
							}
							else
							{
								$x .= "eDisciplinev12...$TmpTableName [AcademicYearID] <font color=red>CANNOT</font> transfer this year information: <font color=red>" . $YearTmp ."</font>&nbsp;&nbsp;Please check this year is enter in School Settings<br>";
							}
						}
					}
					else
					{
						$x .= "eDisciplinev12...$TmpTableName [AcademicYearID] no need transfer (or transferred already)<br>";
					}

					# Semester > YearTermID
					$sql = "select distinct(AcademicYearID), Semester from $TmpTableName where YearTermID is NULL and AcademicYearID is not NULL";
					$result = $li->returnArray($sql);
					if($result)
					{
						foreach($result as $k=>$d)
						{
							list($AcademicYearIDTmp, $SemesterTmp) = $d;
							# retrieve YearTermID according to the AcademicYearID
							$sql1 = "select YearTermID from ACADEMIC_YEAR_TERM where AcademicYearID=$AcademicYearIDTmp and (YearTermNameEN='$SemesterTmp' or YearTermNameB5='$SemesterTmp')";
							$result = $li->returnVector($sql1);
							if($result)
							{
								$thisYearTermID = $result[0];
								$update_sql1 = "update $TmpTableName set YearTermID=$thisYearTermID where YearTermID is NULL and AcademicYearID=$AcademicYearIDTmp and Semester = '$SemesterTmp'";
								$li->db_db_query($update_sql1);

								$x .= "eDisciplinev12...$TmpTableName [YearTermID] transfer year term information: AcademicYearID=" . $AcademicYearIDTmp .", Semester=" . $SemesterTmp ."  <font color=blue>Success.</font><br>";
							}
							else
							{
								$x .= "eDisciplinev12...$TmpTableName [YearTermID] <font color=red>CANNOT</font> transfer this year term information: AcademicYearID=<font color=red>" . $AcademicYearIDTmp ."</font>, Semester=<font color=red>" . $SemesterTmp ."</font>&nbsp;&nbsp;Please check this year is enter in School Settings<br>";
							}

						}
					}
					else
					{
						$x .= "eDisciplinev12...$TmpTableName [YearTermID] no need transfer (or transferred already)<br>";
					}
				}

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eDisciplinev12_Assign_ID_to_APGM_record', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			if(!$GSary['eDisciplinev12_Sync_AP_Record_with_Student_Profile']) {
				include_once($PATH_WRT_ROOT."includes/libuser.php");
				include_once($PATH_WRT_ROOT."includes/libpf-slp.php");

				$currentAcademicYearID = Get_Current_Academic_Year_ID();

				$sql = "SELECT YearNameEN FROM ACADEMIC_YEAR WHERE AcademicYearID='$currentAcademicYearID'";
				$result = $ldiscipline->returnVector($sql);
				$thisEngYearName = $result[0];

				# remove existing records in PROFILE_STUDENT_MERIT
				$sql = "DELETE FROM PROFILE_STUDENT_MERIT WHERE AcademicYearID='$currentAcademicYearID'";
				$ldiscipline->db_db_query($sql);

				# get Approved & Released AP records
				$sql = "SELECT StudentID, Year, Semester, RecordDate, ProfileMeritCount, ItemText, PICID, ProfileMeritType, Remark, AcademicYearID, YearTermID FROM DISCIPLINE_MERIT_RECORD WHERE AcademicYearID='$currentAcademicYearID' AND RecordStatus=1 AND ReleaseStatus=1 ORDER BY RecordDate";
				$apRecordAry = $ldiscipline->returnArray($sql);

				for($i=0; $i<sizeof($apRecordAry); $i++) {

					$lu = new libuser($apRecordAry[0]['StudentID']);
					$thisClassNumber = $lu->ClassNumber;
					$thisClassName = $lu->ClassName;

					$dataAry = array();
					$dataAry['UserID'] = $apRecordAry[$i]['StudentID'];
					$dataAry['Year'] = $thisEngYearName;
					$dataAry['Semester'] = $apRecordAry[$i]['Semester'];

					$dataAry['MeritDate'] = $apRecordAry[$i]['RecordDate'];
					$dataAry['NumberOfUnit'] = $apRecordAry[$i]['ProfileMeritCount'];
					$dataAry['Reason'] = $apRecordAry[$i]['ItemText'];
					$dataAry['PersonInCharge'] = $apRecordAry[$i]['PICID'];
					$dataAry['RecordType'] = $apRecordAry[$i]['ProfileMeritType'];
					$dataAry['RecordStatus'] = 1;
					$dataAry['Remark'] = $apRecordAry[$i]['Remark'];
					$dataAry['ClassName'] = $thisClassName;
					$dataAry['ClassNumber'] = $thisClassNumber;

					$dataAry['AcademicYearID'] = $apRecordAry[$i]['AcademicYearID'];
					$dataAry['YearTermID'] = $apRecordAry[$i]['YearTermID'];

					# insert into PROFILE_STUDENT_MERIT
					$ProfileMeritRecordID = $ldiscipline->INSERT_PROFILE_MERIT_RECORD($dataAry);

				}

				# sync with iPortfolio
				$lpf = new libpf_slp();
				$lpf->updateMeritFromIP($thisEngYearName, "");

				$x .= "Sync of eDis v1.2 & iPortfolio success.<br>\r\n";

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eDisciplinev12_Sync_AP_Record_with_Student_Profile', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());


			}


			$x .= "===================================================================<br>\r\n";
			$x .= "eDiscipline v1.2 [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		############################################################
		# eDiscipline v1.2 [End]
		############################################################

		############################################################
		# iMail v1.2 [Start]
		############################################################
		if(!$GSary['iMailQuotaUpdate'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "iMail v1.2 User Used Quota Update [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "CREATE TABLE IF NOT EXISTS INTRANET_CAMPUSMAIL_USED_STORAGE (
						UserID INT(8) NOT NULL,
						QuotaUsed float default NULL,
						PRIMARY KEY  (UserID)
					) ENGINE InnoDB DEFAULT CHARSET=utf8";
			$result = $li->db_db_query($sql);
			if($result)
				$x .= "iMail 1.2...Create table for user used storeage...<font color='blue'>Done</font><br>\r\n";
			else
				$x .= "iMail 1.2...Create table for user used storeage...<font color='red'>Failed</font><br>\r\n";

			$sql = "SELECT UserID, IFNULL(SUM(AttachmentSize),0) FROM INTRANET_CAMPUSMAIL GROUP BY UserID";
			$result = $li->returnArray($sql,2);
			if(sizeof($result)>0){
				$final = array();
				for($i=0; $i<sizeof($result); $i++){
					list($UserID, $UsedQuota) = $result[$i];
					$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ('$UserID', '$UsedQuota') ON DUPLICATE KEY UPDATE QuotaUsed = '$UsedQuota'";
					$final[] = $li->db_db_query($sql);
				}

				if(in_array(1,$final))
					$x .= "iMail 1.2 - User Used Quota Update...<font color='blue'>Done</font>.<br>\r\n";
				else
					$x .= "iMail 1.2 - User Used Quota Update...<font color='red'>Failed</font>.<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iMailQuotaUpdate', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "iMail v1.2 User Used Quota Update [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		if(!$GSary['iMailCheckFolder'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "iMail v1.2 check Folder  [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			### iMail - check CampusMail Folder ###
			$sql = "SELECT FolderID, OwnerID FROM INTRANET_CAMPUSMAIL_FOLDER ORDER BY FolderID DESC";
			$array = $li->returnArray($sql);
			$DefaultFolderArray = array(0,1,2);
			### check if ALL the default folder is installed successfully ###
			for ($i=0; $i < sizeof($array); $i++){
				list($OldFolderID, $OwnerID) = $array[$i];
				if($OwnerID == NULL){
					if(in_array($OldFolderID,$DefaultFolderArray))
					{
						$CheckingCnt++;
					}
				}
			}

			if($CheckingCnt != 3)
			{
				$sql = "DELETE FROM INTRANET_CAMPUSMAIL_FOLDER WHERE OwnerID IS NULL";
				$li->db_db_query($sql);

				$sql = "SELECT FolderID, OwnerID FROM INTRANET_CAMPUSMAIL_FOLDER ORDER BY FolderID DESC";
				$array = $li->returnArray($sql);
				for ($i=0; $i < sizeof($array); $i++){
					list($OldFolderID, $OwnerID) = $array[$i];

					$sql = "UPDATE INTRANET_CAMPUSMAIL_FOLDER SET FolderID = FolderID + 2 WHERE FolderID = '$OldFolderID'";
					$result = $li->db_db_query($sql);

					if($result)
						$x .= "iMail 1.2 - Update INTRANET_CAMPUSMAIL_FOLDER <font color='blue'>Successfully</font><BR>";
					else
						$x .= "iMail 1.2 - Update INTRANET_CAMPUSMAIL_FOLDER <font color='red'>Failed</font><BR>";


					if(($OldFolderID == 1) || ($OldFolderID == 2))
					{
						continue;
					}
					else
					{
						$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = $OldFolderID + 2 WHERE UserID = '$OwnerID' AND UserFolderID = '$OldFolderID'";
						$result2 = $li->db_db_query($sql);

						if($result2)
							$x .= "iMail 1.2 - Update INTRANET_CAMPUSMAIL <font color='blue'>Successfully</font><BR>";
						else
							$x .= "iMail 1.2 - Update INTRANET_CAMPUSMAIL <font color='red'>Failed</font><BR>";
					}

					$x .= "<BR><BR>";
				}

				$sql = "SELECT MAX(FolderID) FROM INTRANET_CAMPUSMAIL_FOLDER";
				$result3 = $li->returnVector($sql);

				$sql = "ALTER TABLE INTRANET_CAMPUSMAIL_FOLDER AUTO_INCREMENT = ".$result3[0];
				$li->db_db_query($sql);

				$DefaultFolderNum = 3;
				for($i=0; $i < $DefaultFolderNum; $i++)
				{
					if($i == 0)
						$FolderName = "Outbox";
					else if($i == 1)
						$FolderName = "Draft";
					else if($i == 2)
						$FolderName = "Inbox";
					else
						continue;

					$sql = "INSERT INTO INTRANET_CAMPUSMAIL_FOLDER (OwnerID, FolderName, RecordType, DateInput, DateModified)
							VALUES
							(NULL, '$FolderName', 0, now(),now())";
					$li->db_db_query($sql);
					$TmpFolderID = $li->db_insert_id();

					if($i==0)
						$sql2 = "UPDATE INTRANET_CAMPUSMAIL_FOLDER SET FolderID = 0 WHERE FolderID = '$TmpFolderID'";
					else if($i==1)
						$sql2 = "UPDATE INTRANET_CAMPUSMAIL_FOLDER SET FolderID = 1 WHERE FolderID = '$TmpFolderID'";
					else if($i==2)
						$sql2 = "UPDATE INTRANET_CAMPUSMAIL_FOLDER SET FolderID = 2 WHERE FolderID = '$TmpFolderID'";
					else
						continue;

					$result = $li->db_db_query($sql2);

					$sql = "SELECT MAX(FolderID) FROM INTRANET_CAMPUSMAIL_FOLDER";
					$result4 = $li->returnVector($sql);

					$sql = "ALTER TABLE INTRANET_CAMPUSMAIL_FOLDER AUTO_INCREMENT = '".$result4[0]."'";
					$li->db_db_query($sql);

					if($result)
						$x .= "iMail Folder - <font color='blue'>".$FolderName."</font> created <font color='blue'>Successfully</font><BR>";
					else
						$x .= "iMail Folder - <font color='blue'>".$FolderName."</font> created <font color='red'>Failed</font><BR>";

					$x .= "<BR><BR>";
				}
			}else{
				$x .= "iMail Folder - All Folders is <font color='blue'>Correct</font>.<br><br>";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iMailCheckFolder', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "iMail v1.2 check Folder  [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		if(!$GSary['iMailUpdateDateInFolder'])
		{
			### Update iMail DateInFolder ###
			$sql = "SELECT DISTINCT UserID FROM INTRANET_CAMPUSMAIL WHERE Deleted = 1 AND (DateInFolder IS NULL OR DateInFolder = '')";
			$Array = $li->returnVector($sql);

			if(sizeof($Array)>0)
			{
				$x .= "===================================================================<br>\r\n";
				$x .= "iMail v1.2 Update Date In Folder  [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				$targetUserID = implode (",",$Array);

				$sql = "UPDATE INTRANET_CAMPUSMAIL SET DateInFolder = DateInput WHERE UserID IN ($targetUserID) and Deleted = 1";
				$result = $li->db_db_query($sql);

				if($result)
				{
					$x .= "<b>[iMail 1.2]</b> - Update DateInFolder <font color='blue'>Successfully</font>.<br><br>";
				}
				else
				{
					$x .= "<b>[iMail 1.2]</b> - Update DateInFolder <font color='red'>Failed</font>.<br><br>";
				}

				$x .= "===================================================================<br>\r\n";
				$x .= "iMail v1.2 Update Date In Folder  [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iMailUpdateDateInFolder', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}

		if(!$GSary['iMailUpdateUserFolderIDInTrashBox'])
		{
			### iMail - Update FolderID For the mail in the trash folder ###
			$sql = "select UserID, CampusmailID, CampusMailFromID, SenderID, SenderEmail, RecipientID, InternalCC, InternalBCC, ExternalTo, ExternalCC, ExternalBCC, SpamFlag from INTRANET_CAMPUSMAIL WHERE USERFOLDERID = -1";
			$FolderIDArray = $li->returnArray($sql,12);
			if(sizeof($FolderIDArray)>0)
			{
				$x .= "===================================================================<br>\r\n";
				$x .= "iMail v1.2 Update UserFolderID In TrashBox  [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				for($i=0; $i<sizeof($FolderIDArray); $i++)
				{
					list($UID, $MailID, $FromMailID, $SenderID, $SenderMail, $RecipientID, $InteralCC, $InternalBCC, $ExternalTo, $ExternalCC, $ExternalBCC, $IsSpam) = $FolderIDArray[$i];

					if($IsSpam == "YES")
					{
						//echo $MailID." - From SPAM<BR><BR>";
						$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = -2 WHERE CampusmailID = '$MailID'";
						$result[] = $li->db_db_query($sql);
					}
					else
					{
						if(($SenderID != $UID)||($SenderMail != ''))
						{
							//echo $MailID." - From Inbox<BR><BR>";
							$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = 2 WHERE CampusmailID = '$MailID'";
							$result[] = $li->db_db_query($sql);
						}
						else
						{
							if($FromMailID == "")
							{
								//echo $MailID." - From DRAFT<BR><BR>";
								$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = 1 WHERE CampusmailID = '$MailID'";
								$result[] = $li->db_db_query($sql);
							}
							else
							{
								if( ($RecipientID!="") || ($InteralCC!="") || ($InternalBCC!="") || ($ExternalTo != "") || ($ExternalCC != "") || ($ExternalBCC != ""))
								{
									//echo $MailID." - From Outbox<BR><BR>";
									$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = 0 WHERE CampusmailID = '$MailID'";
									$result[] = $li->db_db_query($sql);
								}
								else
								{
									//echo $MailID." - From DRAFT<BR><BR>";
									$sql = "UPDATE INTRANET_CAMPUSMAIL SET UserFolderID = 1 WHERE CampusmailID = '$MailID'";
									$result[] = $li->db_db_query($sql);
								}
							}
						}
					}
				}
				if(!in_array(false,$result)){
					$x .= "iMail 1.2 - Update UserFolderID in TrashBox <font color='blue'>Successfully</font>.<br><br>";
				}else{
					$x .= "iMail 1.2 - Update UserFolderID in TrashBox <font color='red'>Failed</font>.<br><br>";
				}

				$x .= "===================================================================<br>\r\n";
				$x .= "iMail v1.2 Update UserFolderID In TrashBox  [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iMailUpdateUserFolderIDInTrashBox', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());


		}
		############################################################
		# iMail v1.2 [End]
		############################################################

		############################################################
		# Lesson Attendance [Start]
		############################################################
		if(!$GSary['AddLateForFieldLessonAttendance'])
		{
			$db = new libdb();

			echo 'Alter auto generate table SUBJECT_GROUP_STUDENT_ATTENDANCE_{AcademicYearID}, add column LateFor....';
			// get current academic year in school
			$sql = 'show tables like \'%SUBJECT_GROUP_STUDENT_ATTENDANCE_%\'';
			$Temp = $li->returnArray($sql);

			for ($i=0; $i< sizeof($Temp); $i++) {
				$sql = 'alter table '.$Temp[$i][0].' add LateFor int(8) default 0 after OutTime';
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			echo 'OK<br>';

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'AddLateForFieldLessonAttendance', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		############################################################
		# Lesson Attendance [End]
		############################################################


		############################################################
		# Learning Category (set display order as default code) [Start]
		############################################################
		$x .= "<br />";
		$x .= "===================================================================<br>\r\n";
		$x .= "Learning Category (set display order as default Code) [Start]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";

		$sql = "UPDATE LEARNING_CATEGORY Set Code = DisplayOrder Where (Code IS NULL OR Code = '') And RecordStatus = 1";
		$successArr['LearningCategory'] = $li->db_db_query($sql);

		if ($successArr['LearningCategory'] == true)
		{
			$x .= "Learning Category (set display order as default Code) <font color='blue'>Success</font>.<br><br>\r\n";
		}
		else
		{
			$x .= "Learning Category (set display order as default Code) is <font color='red'>Failed</font>.<br><br>\r\n";
		}

		$x .= "===================================================================<br>\r\n";
		$x .= "Learning Category (set display order as default Code) [End]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";

		############################################################
		# Learning Category (set display order as default code) [End]
		############################################################

		###################################################
		### Change file content in "De/Merit customized Wordings" [Start]
		###################################################
		$x .= "===================================================================<br>\r\n";
		$x .= "Change file content in 'De/Merit customized Wordings' [Start]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
		$temp_ary = array("b5", "en");

		foreach($temp_ary as $k=>$d)
		{

			$file_target = $intranet_root."/file/merit.". $d .".customized.txt";
			$file_content = get_file_content($file_target);

			if(!empty($file_content))
			{
				// $lf = new libfilesystem();

				//$cur_encoding = mb_detect_encoding($file_content) ;
				//if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
				if(!(isUTF8($file_content) && mb_check_encoding($file_content,"UTF-8")))
				{
					$new_content = Big5ToUnicode($file_content);
					$lf->file_write($new_content,"$file_target");
					$x .=  $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>";
				}
				else
				{
					$x .=  $file_target .".... no need to convert to utf-8.<br>";
				}
			}
			else
			{
				$x .=  $file_target .".... file empty.<br>";
			}
		}
		$x .= "===================================================================<br>\r\n";
		$x .= "Change file content in 'De/Merit customized Wordings' [End]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
		###################################################
		### Change file content in "De/Merit customized Wordings" [End]
		###################################################

		###################################################
		### Create temp folder for eCommunity group (if files already exists in IP20 Group) [Start]
		###################################################
		if(!$GSary['eCommunityCopyFileFromIP20WithTempFolder'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eCommunity - create TempFolder for files created in IP20 [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# found out which Group includes files (no FolderID)
			$sql = "select distinct(GroupID) from INTRANET_FILE where FolderID is NULL";
			$result = $li->returnArray($sql);
			foreach($result as $k=>$d)
			{
				$thisGroupID = $d['GroupID'];

				# create temp folder for this Group
				$sql1 = "insert into INTRANET_FILE_FOLDER (GroupID, FolderType, UserID, UserName, Title, PublicStatus, DateInput) values ('". $thisGroupID ."', 'F', 0, '(System Default)', '(Temp Folder)', 1, now())";
				$li->db_db_query($sql1);
				$TmpFolderID = $li->db_insert_id();

				# assign the NULL FolderID to $TmpFolderID
				$sql2 = "update INTRANET_FILE set FolderID='$TmpFolderID', UserTitle=Title, Approved=1 where FolderID is NULL and GroupID='$thisGroupID'";
				$li->db_db_query($sql2);

				$x .=  "Temp Folder is created to Group ". $thisGroupID ." (Group ID)<br>";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eCommunityCopyFileFromIP20WithTempFolder', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eCommunity - create TempFolder for files created in IP20 [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Create temp folder for eCommunity group (if files already exists in IP20 Group) [End]
		###################################################
		/*
		###################################################
		### Transfer iPortfolio photo (file/official_photo/) to /file/user_photo/ [Start]
		###################################################
		if(!$GSary['TransferUserPhoto'])
		{
			$fs = new libfilesystem();
			$iPF_photolink = $intranet_root."/file/official_photo/";
			$user_photolink = $intranet_root."/file/user_photo/";

			if(file_exists($iPF_photolink))
			{
				$x .= "===================================================================<br>\r\n";
				$x .= "Transfer iPortfolio photo to /file/user_photo/ and update to use userlogin as filename [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				$c = 0;
				$ipf_files = opendir($iPF_photolink);
				while (false !== ($file = readdir($ipf_files)))
				{
					if(strpos($file, ".jpg"))
					{
						$this_websams = str_replace(".jpg", "", $file);

						# retrieve user login by websams
						$sql = "select UserLogin from INTRANET_USER where WebSAMSRegNo='#". $this_websams ."'";
						$result = $li->returnVector($sql);

						if($result)
						{
							$this_userlogin = $result[0];
							$ipf_photo = $iPF_photolink.$file;
							$user_photo = $user_photolink.$this_userlogin.".jpg";
							$up = $fs->item_copy($ipf_photo, $user_photo);
							if($up)		$c++;
						}
					}
				}

				$x .=  $c. " photos are transferred<br>";
				$x .= "===================================================================<br>\r\n";
				$x .= "Transfer iPortfolio photo to /file/user_photo/ and update to use userlogin as filename [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'TransferUserPhoto', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		*/

		###################################################
		### Transfer iPortfolio photo (file/official_photo/) to /file/user_photo/ [End]
		###################################################

		############################################################
		# iMail v1.2 [Start]
		############################################################
		if(!$GSary['StudAttendLunchSettingMigration'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Student Attendance Lunch setting migration [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$AttendLunchSetting = explode("\n",get_file_content("$intranet_root/file/stattend_lunch_misc.txt"));
			$SettingList['NoRecordLunchOut'] = $AttendLunchSetting[0];
			$SettingList['LunchOutOnce'] = $AttendLunchSetting[1];
			$SettingList['AllAllowGoOut'] = $AttendLunchSetting[2];

			$lgs->Save_General_Setting('StudentAttendance', $SettingList);

			echo "Student Attendance...Setting already transferred<br>";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudAttendLunchSettingMigration', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Student Attendance Lunch setting migration [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		############################################################
		# iCalendar Temp Cal event Table[Start]
		############################################################
		if(!$GSary['TempCalEventTable'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "iCalendar Temp Cal event Table [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT ENGINE, TABLE_COLLATION FROM information_schema.tables
				where table_schema = '{$intranet_db}' and
				table_name = 'TEMP_CALENDAR_EVENT_ENTRY'";
			$result = $li->returnArray($sql);
			if (empty($result) || trim($result[0]['ENGINE']) != 'InnoDB' || !stristr($result[0]['TABLE_COLLATION'],'utf')){
				$sql = "drop table `TEMP_CALENDAR_EVENT_ENTRY`";
				$li->db_db_query($sql);

				$sql = " CREATE TABLE `TEMP_CALENDAR_EVENT_ENTRY` (
					  `SID` varchar(64) default NULL,
					  `Title` varchar(255)  default NULL,
					  `EventDate` datetime default NULL,
					  `ImportDate` datetime default NULL,
					  `Duration` int(8) default NULL,
					  `IsAllDay` char(1) default NULL,
					  `Description` text,
					  `Location` varchar(255) default NULL,
					  `Private` char(1) default NULL
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";
				$li->db_db_query($sql);
			}


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'TempCalEventTable', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "iCalendar Temp Cal event Table [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}


		############################################################
		# ReportCard Semester Patch [Start]
		############################################################
		/* Run the data patch only if requested

		if($plugin['ReportCard2008'] && !$GSary['eRC_UpdateSemester'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "ReportCard Semester Patch [Start]</b><br>\r\n";
			$x .= "===================================================================<br><br>\r\n";

			$sql = "SHOW DATABASES LIKE '%".$intranet_db."_DB_REPORT_CARD%'";
			$DatabaseArr = $li->returnArray($sql);

			for($i=0; $i<sizeof($DatabaseArr);$i++)
			{
				$reportcard_db = $DatabaseArr[$i][0];

				$x .= "============ ".$reportcard_db." Start ============</b><br><br>\r\n";

				### Update Semester in RC_REPORT_TEMPLATE (for Term Report ONLY)
				$x .= "Update Semester in RC_REPORT_TEMPLATE (for Term Report ONLY)<br>\r\n";

				$sql = "Select ReportID From $reportcard_db.RC_REPORT_TEMPLATE Where Semester != 'F' And Semester Is Not Null";
				$TermReportIDArr = $li->returnVector($sql);
				$numOfTermReport = count($TermReportIDArr);

				if ($numOfTermReport == 0)
				{
					$x .= "There is no term report.<br>\r\n";
				}
				else
				{
					$x .= "There are $numOfTermReport term report(s).<br>\r\n";

					$TermReportIDList = implode(',', $TermReportIDArr);
					$x .= $TermReportIDList."<br><br>\r\n";

					$sql = "Update $reportcard_db.RC_REPORT_TEMPLATE Set Semester = Semester + 1 Where ReportID In ($TermReportIDList)";
					$x .= $sql."<br>\r\n";

					$successArr[$reportcard_db]['RC_REPORT_TEMPLATE'] = $li->db_db_query($sql);
					if ($successArr[$reportcard_db]['RC_REPORT_TEMPLATE'])
					{
						$x .= "Update <font color='blue'><b>Success</b></font><br><br>\r\n";
					}
					else
					{
						$x .= "Update <font color='red'><b>Failed</b></font><br><br>\r\n";
					}
				}


				### Update SemesterNum in RC_REPORT_TEMPLATE_COLUMN (for Consolidate Report ONLY)
				$x .= "Update SemesterNum in RC_REPORT_TEMPLATE_COLUMN (for Consolidate Report ONLY)<br>\r\n";

				$sql = "Select ReportID From $reportcard_db.RC_REPORT_TEMPLATE Where Semester = 'F'";
				$ConsolidatedReportIDArr = $li->returnVector($sql);
				$numOfConsolidatedReport = count($ConsolidatedReportIDArr);

				if ($numOfConsolidatedReport == 0)
				{
					$x .= "There is no consolidated report.<br>\r\n";
				}
				else
				{
					$x .= "There are $numOfConsolidatedReport consolidated report(s).<br>\r\n";

					$ConsolidatedReportIDList = implode(',', $ConsolidatedReportIDArr);
					$x .= $ConsolidatedReportIDList."<br><br>\r\n";

					$sql = "Update $reportcard_db.RC_REPORT_TEMPLATE_COLUMN Set SemesterNum = SemesterNum + 1 Where ReportID In ($ConsolidatedReportIDList)";
					$x .= $sql."<br>\r\n";

					$successArr[$reportcard_db]['RC_REPORT_TEMPLATE_COLUMN'] = $li->db_db_query($sql);
					if ($successArr[$reportcard_db]['RC_REPORT_TEMPLATE_COLUMN'])
					{
						$x .= "Update <font color='blue'><b>Success</b></font><br><br>\r\n";
					}
					else
					{
						$x .= "Update <font color='red'><b>Failed</b></font><br><br>\r\n";
					}
				}

				$x .= "============ ".$reportcard_db." End ============</b><br><br><br>\r\n";
			}

			$x .= "===================================================================<br>\r\n";
			$x .= "ReportCard Semester Patch [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eRC_UpdateSemester', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		*/
		############################################################
		# ReportCard Semester Patch [End]
		############################################################

		############################################################
		# Copy IP20 admin to IP25 Role (Create Default) [Start]
		############################################################
		if(!$GSary['CopyIP20AdminToIP25Role'])
		{
			# check any roles, if role is created, then no need to re-declare the role
			$sql = "select count(*) from ROLE";
			$result = $li->returnVector($sql);
			if(!$result[0])		# only run the script if there is no other Role setting
			{
				$x .= "===================================================================<br>\r\n";
				$x .= "Copy IP20 admin to IP25 Role [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				include_once($PATH_WRT_ROOT."addon/ip25/copy_ip20admin_to_ip25role.php");

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CopyIP20AdminToIP25Role', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());

				$x .= "===================================================================<br>\r\n";
				$x .= "Copy IP20 admin to IP25 Role [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}

		}
		############################################################
		# Copy IP20 admin to IP25 Role (Create Default) [End]
		############################################################

		###########################################################################
		# Create Role iPortfolio Admin, move existing ipf admin to the role [Start]
		###########################################################################
		if($plugin['iPortfolio']&&!$GSary['MoveIpfAdminToRole'])
		{
			$x .= "==========================================================================<br>\r\n";
			$x .= "Create Role iPortfolio Admin, move existing ipf admin to the role [Start]</b><br>\r\n";
			$x .= "==========================================================================<br>\r\n";

			$objLibportfolio = new libportfolio();
			$objLibportfolio->Move_iPF_Admin_from_to_Role();

	 		# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'MoveIpfAdminToRole', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=======================================================================<br>\r\n";
			$x .= "Create Role iPortfolio Admin, move existing ipf admin to the role [End]</b><br>\r\n";
			$x .= "=======================================================================<br>\r\n";

		}
		###########################################################################
		# Create Role iPortfolio Admin, move existing ipf admin to the role [End]
		###########################################################################

		###########################################################################
		# Import SBA Default Scheme [Start]
		###########################################################################
		if($plugin['SBA'])
		{
			$x .= "==========================================================================<br>\r\n";
			$x .= "Import SBA Default Scheme [Start]</b><br>\r\n";
			$x .= "==========================================================================<br>\r\n";
			include_once($PATH_WRT_ROOT."includes/sba/importDefaultScheme.php");
			$importSBADefaultScheme = new importSBADefaultScheme();
			$importSBADefaultScheme->startImportDefaultScheme();

			$x .= "=======================================================================<br>\r\n";
			$x .= "Import SBA Default Scheme [End]</b><br>\r\n";
			$x .= "=======================================================================<br>\r\n";

		}
		###########################################################################
		# Import SBA Default Scheme [End]
		###########################################################################

		###########################################################################
		# iPortfolio Jupas Student Academic Performance Remarks Implementation [Start]
		###########################################################################
		if($plugin['iPortfolio'] && !$GSary['iPf_Jupas_Student_Academic_Performance_Remarks'])
		{
			$x .= "==========================================================================<br>\r\n";
			$x .= "iPortfolio Jupas Student Academic Performance Remarks Implementation [Start]</b><br>\r\n";
			$x .= "==========================================================================<br>\r\n";

			$sql = "Alter Table {$eclass_db}.OEA_STUDENT_ACADEMIC add PercentileRemark text not NULL after `OverallRating`";
			$li->db_db_query($sql) or debug_pr(mysql_error());
			$sql = "Alter Table {$eclass_db}.OEA_STUDENT_ACADEMIC add OverallRatingRemark text not NULL after `PercentileRemark`";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPf_Jupas_Student_Academic_Performance_Remarks', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=======================================================================<br>\r\n";
			$x .= "iPortfolio Jupas Student Academic Performance Remarks Implementation [End]</b><br>\r\n";
			$x .= "=======================================================================<br>\r\n";

		}
		###########################################################################
		# iPortfolio Jupas Student Academic Performance Remarks Implementation [End]
		###########################################################################

		############################################################
		# Check Group Category ID 0 for Identity [Start]
		############################################################
		/*
		if(!$GSary['CheckGroupCategoryID0'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Check Group Category ID 0 for 'Identity' [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "select * from INTRANET_GROUP_CATEGORY where RecordType=1 order by GroupCategoryID";
			$result = $li->returnArray($sql);

			# Condition:
			# 1. The first group category id is not 0
			# 2. There should be 6 default categories (RecordType=1)
			if($result[0]['GroupCategoryID']!=0 && sizeof($result)==6)
			{
				# found out the last GroupCategoryID
				$LastGroupCategoryID = $result[sizeof($result)-1]['GroupCategoryID'];

				# change the last Group Category to 0  (Assume this should be Identity)
				$sql = "update INTRANET_GROUP_CATEGORY set GroupCategoryID=0 where GroupCategoryID=$LastGroupCategoryID";
 				debug_pr($sql);
				/////$li->db_db_query($sql) or die(mysql_error());

				# update INTRANET_GROUP RecordType
				$sql = "update INTRANET_GROUP set RecordType=0 where RecordType=$LastGroupCategoryID";
 				debug_pr($sql);
				/////$li->db_db_query($sql) or die(mysql_error());

				$x .= "Check Group Category ID 0 for 'Identity'... Updated!!<br>\r\n";
			}
			else
			{
				$x .= "Check Group Category ID 0 for 'Identity'... no need to update<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CheckGroupCategoryID0', 1, now())";
			/////$li->db_db_query($sql) or die(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Check Group Category ID 0 for 'Identity' [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		*/
		############################################################
		# Check Group Category ID 0 for Identity [End]
		############################################################

		###################################################
		### Change file content in Message of the Day[Start]
		###################################################
		$x .= "===================================================================<br>\r\n";
		$x .= "Change file content in Message of the Day [Start]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";

		$file_target = $intranet_root."/file/motd.txt";
		$file_content = get_file_content($file_target);

		if(!empty($file_content))
		{
			// $lf = new libfilesystem();

			if(!(isUTF8($file_content) && mb_check_encoding($file_content,"UTF-8")))
			{
				$new_content = Big5ToUnicode($file_content);
				$lf->file_write($new_content,"$file_target");
				$x .=  $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>";
			}
			else
			{
				$x .=  $file_target .".... no need to convert to utf-8.<br>";
			}
		}
		else
		{
			$x .=  $file_target .".... file empty.<br>";
		}

		$x .= "===================================================================<br>\r\n";
		$x .= "Change file content in Message of the Day [End]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
		###################################################
		### Change file content in "De/Merit customized Wordings" [End]
		###################################################

		############################################################
		# Sorting SBS records and Weblog records in handin [Start]
		############################################################
		if(!$GSary['iPortfolioSortHandin'])
		{
			# check if there is iPortfolio room
			$sql = "SELECT course_id FROM {$eclass_db}.course WHERE RoomType = 4";
			$result = $li->returnVector($sql);
			if(count($result)>0)		# only run the script if there is iPortfolio room
			{
				$x .= "===================================================================<br>\r\n";
				$x .= "Sorting SBS records and Weblog records in handin [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

        $ipf_db = $eclass_prefix."c".$result[0];

        $sql = "ALTER TABLE {$ipf_db}.handin ADD COLUMN type varchar(4)";
        $li->db_db_query($sql);
        $sql = "UPDATE {$ipf_db}.handin SET type = 'blog'";
        $li->db_db_query($sql) or debug_pr(mysql_error());
        $sql = "UPDATE {$ipf_db}.handin SET type = 'sbs' WHERE INSTR(answer, '#ANS#') <> 0";
        $li->db_db_query($sql) or debug_pr(mysql_error());
        $sql = "UPDATE {$ipf_db}.handin SET type = NULL WHERE INSTR(answer, '#QUE#') <> 0";
        $li->db_db_query($sql) or debug_pr(mysql_error());

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioSortHandin', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());

				$x .= "===================================================================<br>\r\n";
				$x .= "Sorting SBS records and Weblog records in handin [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}

		}
		############################################################
		# Sorting SBS records and Weblog records in handin [End]
		############################################################

		############################################################
		# Migrating Preset Programme from file to DB [Start]
		############################################################
		if(!$GSary['iPortfolioMigratePresetProgramme'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Migrating Preset Programme from file to DB [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

      $lpf = new libpf_slp();

      // $lf = new libfilesystem();
      $lwf = new libwordtemplates_ipf(3);
      $file_array = $lwf->file_array;

      for($i=0; $i<count($file_array); $i++)
      {
        $t_category_id = $file_array[$i][0];

        $data = $lwf->getTemplatesContent($t_category_id);

		$encoding = DetectDataEncoding($data);
        $data = mb_convert_encoding($data,'UTF-8',$encoding);

        if (!empty($data))
        {
        	$DataArray = explode("\n", $data);

        	for($j=0; $j<sizeof($DataArray); $j++)
        	{
        		$p_title = trim($DataArray[$j]);

        		if(!empty($p_title))
        		{
        			$TempArray = explode("\t", $p_title);
        			$PresetTitle = $TempArray[0];
        			$PresetELEList = $TempArray[1];

              $sql = "INSERT INTO {$eclass_db}.OLE_PROGRAM_TITLE ";
              $sql .= "(CategoryID, EngTitle, RecordStatus, ELE, InputDate, ModifiedDate) ";
              $sql .= "VALUES ";
              $sql .= "('".$t_category_id."', '".addslashes($PresetTitle)."', 1, '".$PresetELEList."', NOW(), NOW())";

              $success = $li->db_db_query($sql);

              if($success)
              {
                $success_count[$t_category_id] += mysql_affected_rows();
              }
              else
              {
                $fail_msg[$t_category_id][] = array(
                                                "sql" => $sql,
                                                "error" => mysql_error()
                                              );
              }
        		}
        	}
        }
      }

      # logging
      $outstr = "";
      if(is_array($success_count)){
        $outstr .= serialize($success_count)."\n";
      }
      if(is_array($fail_msg)){
        $outstr .= serialize($fail_msg)."\n";
      }
      $lf->folder_new($intranet_root."/file/migratelog");
      $lf->folder_new($intranet_root."/file/migratelog/iPortfolio");
      $lf->file_write($outstr, $intranet_root."/file/migratelog/iPortfolio/PresetProgrammeMigrate_".date("Ymd-His").".txt");

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioMigratePresetProgramme', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Migrating Preset Programme from file to DB [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

		}
		############################################################
		# Migrating Preset Programme from file to DB [End]
		############################################################

		############################################################
		# Patching OLE Program Academic Year ID [Start]
		############################################################
		if(!$GSary['iPortfolioPatchOLEAYearID'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Patching OLE Program Academic Year ID [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

      # Insert OLE Programmes if they do not exist in OLE_PROGRAM
      $lpf = new libpf_slp();
      $lpf->INSERT_STUDENT_RECORD();

      // $lf = new libfilesystem();
      $outstr = $lpf->PatchOLEAYearID();

      $lf->folder_new($intranet_root."/file/migratelog");
      $lf->folder_new($intranet_root."/file/migratelog/iPortfolio");
      $lf->file_write($outstr, $intranet_root."/file/migratelog/iPortfolio/OLE_AYID_Patch_".date("Ymd-His").".txt");

			//check have run the patch before
			// select * from GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
			//rerun the patching
			//delete from  GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioPatchOLEAYearID', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Patching OLE Program Academic Year ID [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

		}
		############################################################
		# Patching OLE Program Academic Year ID [End]
		############################################################

    ############################################################
    # Patching OLE Program Program Type [Start]
    ############################################################
    if(!$GSary['iPortfolioPatchOLEProgramType'])
    {
    	$x .= "===================================================================<br>\r\n";
    	$x .= "Patching OLE Program Program Type [Start]</b><br>\r\n";
    	$x .= "===================================================================<br>\r\n";

      $sql = "UPDATE {$eclass_db}.OLE_PROGRAM op SET op.ProgramType = (SELECT CASE iu.RecordType WHEN 1 THEN 'T' WHEN 2 THEN 'S' ELSE op.ProgramType END FROM {$intranet_db}.INTRANET_USER iu WHERE iu.UserID = op.CreatorID) WHERE op.ProgramType = '' OR op.ProgramType IS NULL";
      $li->db_db_query($sql) or debug_pr(mysql_error());

    	//check have run the patch before
    	// select * from GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
    	//rerun the patching
    	//delete from  GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
    	# update General Settings - mark the script is executed
    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioPatchOLEProgramType', 1, now())";
    	$li->db_db_query($sql) or debug_pr(mysql_error());

    	$x .= "===================================================================<br>\r\n";
    	$x .= "Patching OLE Program Program Type [End]</b><br>\r\n";
    	$x .= "===================================================================<br>\r\n";

    }
    ############################################################
    # Patching OLE Program Program Type [End]
    ############################################################

    ############################################################
    # Patching encoded path in weblog [Start]
    ############################################################
    if(!$GSary['iPortfolioPatchWeblogEncodedPath'])
    {
    	$x .= "===================================================================<br>\r\n";
    	$x .= "Patching encoded path in weblog [Start]</b><br>\r\n";
    	$x .= "===================================================================<br>\r\n";

      // $lf = new libfilesystem();

      $sql = "SELECT course_id FROM {$eclass_db}.course WHERE RoomType = 4";
      $course_db = $eclass_prefix."c".current($li->returnVector($sql));

      $sql = "SELECT handin_id, answer FROM ".$course_db.".handin WHERE type = 'blog' AND answer <> ''";
      $weblog_arr = $li->returnArray($sql);
      $rec_all = count($weblog_arr);

      ob_start();
      echo "start time => ".date("Y-m-d H:i:s")."<br />\n";
      echo "==============================================================<br /><br />\n\n";

      $rec_update = 0;
      $rec_skip = 0;

      for($k=0; $k<count($weblog_arr); $k++)
      {
        $handin_id = $weblog_arr[$k]['handin_id'];
        $str = $weblog_arr[$k]['answer'];

        preg_match_all ("/&lt;IMG.+src=&quot;(.+)&quot;.+&gt;/U", $str, $matches);

        $pattern = array();
        $replacement = array();
        for($i=0; $i<count($matches[1]); $i++)
        {
          $tmp_path_arr = explode("/", $matches[1][$i]);
          for($j=0; $j<count($tmp_path_arr); $j++)
          {
            $tmp_path_arr[$j] = ($tmp_path_arr[$j] == "http:") ? $tmp_path_arr[$j] : rawurldecode($tmp_path_arr[$j]);
          }
          $tmp_path = implode("/", $tmp_path_arr);

          if(mb_detect_encoding($tmp_path) != "UTF-8")
          {
            $pattern[] = "/".str_replace("/", "\/", $matches[1][$i])."/";

            $target = iconv("Big5", "UTF-8", $tmp_path);

            $tmp_path_arr = explode("/", $target);
            for($j=0; $j<count($tmp_path_arr); $j++)
            {
              $tmp_path_arr[$j] = ($tmp_path_arr[$j] == "http:") ? $tmp_path_arr[$j] : rawurlencode($tmp_path_arr[$j]);
            }
            $tmp_path = implode("/", $tmp_path_arr);

            $replacement[] = $tmp_path;
          }
        }
        $replace_str = preg_replace($pattern, $replacement, $str);
        if($str == $replace_str){
          $rec_skip++;
          continue;
        }

        $sql = "UPDATE ".$course_db.".handin SET answer = '".addslashes($replace_str)."' WHERE handin_id = '".$handin_id."'";
        $li->db_db_query($sql);

        echo "handin id => ".$handin_id."<br />\n";
        echo "pattern => ";
        echo("<PRE>");
        print_r($pattern);
        echo("</PRE>\n");
        echo "replacement => ";
        echo("<PRE>");
        print_r($replacement);
        echo("</PRE>\n");
        echo "original str =><br />\n".$str."<br /><br />\n";
        echo "updated str =><br />\n".$replace_str."<br /><br />\n";
        echo "update sql =><br />\n".$sql."<br /><br />\n\n";
        echo("==============================================================<br /><br />\n\n");

        $rec_update++;

        unset($pattern);
        unset($target);
        unset($replacement);
      }
      echo "end time => ".date("Y-m-d H:i:s")."<br />\n";
      echo "# records => ".$rec_all."<br />\n";
      echo "# records updated => ".$rec_update."<br />\n";
      echo "# records skipped => ".$rec_skip."<br />\n";

      $logstr .= ob_get_contents();
      ob_end_clean();

      $logdir = $intranet_root."/file/iportfolio/log";
      @mkdir($logdir, 0777, true);
      $logpath = $logdir."/patch_weblog_result_".date("Ymd_His").".txt";
      $lf->file_write($logstr, $logpath);


    	//check have run the patch before
    	// select * from GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
    	//rerun the patching
    	//delete from  GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
    	# update General Settings - mark the script is executed
    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioPatchWeblogEncodedPath', 1, now())";
    	$li->db_db_query($sql) or debug_pr(mysql_error());

    	$x .= "===================================================================<br>\r\n";
    	$x .= "Patching encoded path in weblog [End]</b><br>\r\n";
    	$x .= "===================================================================<br>\r\n";

    }
    ############################################################
    # Patching encoded path in weblog [End]
    ############################################################

		############################################################
		# eDiscipline - set all the group/user GM/AP items are accessible [Start]
		############################################################
		if(!$GSary['eDis_SetGMAP_AllAccessible'])
		{
			include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
			$ldiscipline = new libdisciplinev12();

			$x .= "===================================================================<br>\r\n";
			$x .= "eDiscipline - set all the group/user GM/AP items are accessible [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# found out all the GM items (all in use and not in use)
			$sql1 = "select CategoryID, ItemID from DISCIPLINE_ACCU_CATEGORY_ITEM";
			$GM_Ary = $li->returnArray($sql1);

			# found out all the AP items (all in use and not in use)
			$sql1 = "select CatID, ItemID, ConductScore, SubScore1, NumOfMerit, RelatedMeritType from DISCIPLINE_MERIT_ITEM";
			$AP_Ary = $li->returnArray($sql1);

			# homework
			$use_intranet_homework = $ldiscipline->accumulativeUseIntranetSubjectList();
			if($use_intranet_homework)
			{
				$CategoryID = 2;
				$sql = "select RecordID from ASSESSMENT_SUBJECT WHERE RecordStatus = 1  and (CMP_CODEID is NULL or CMP_CODEID = '')";
				$HW_Ary = $li->returnVector($sql);
			}

			# loop groups (GroupType=A)
			$sql = "select GroupID from ACCESS_RIGHT_GROUP where GroupType='A'";
			$result = $li->returnVector($sql);

			$GM_values = array();
			$AP_values = array();

			foreach($result as $k=>$GroupID)
			{
				# GM
				foreach($GM_Ary as $k1=>$GM_data)
				{
					$GM_values[] = "(".$GroupID.", ".$GM_data['CategoryID'].", ".$GM_data['ItemID'] .")";
				}

				#AP
				foreach($AP_Ary as $k1=>$AP_data)
				{
					list($this_CatID, $this_ItemID, $this_conduct, $this_subscore, $this_num, $this_merittype) = $AP_data;

					$AP_values[] = "(".$GroupID.", ".$this_CatID.", ".$this_ItemID.", '".$this_conduct."', '".$this_subscore."', '".$this_num."', '".$this_merittype."')";
				}

				# Homework
				if($use_intranet_homework)
				{
					foreach($HW_Ary as $k1=>$HW_data)
					{
						$HW_values[] = "(".$GroupID.", 2, ".$HW_data .")";
					}
				}
			}

			if(!empty($AP_values))
			{
				$GM = implode(",", $GM_values);
				$sql = "insert into DISCIPLINE_GROUP_ACCESS_GM (GroupID, CategoryID, ItemID) values $GM";
				$set_gm_access_right = $li->db_db_query($sql);
				if ($set_gm_access_right)
				{
					$x .= "Set all the GM access rights for all eDiscipline groups ...  <font color='blue'><b>Success</b></font><br><br>\r\n";
				}
				else
				{
					$x .= "Set all the GM access rights for all eDiscipline groups ...  <font color='red'><b>Failed</b></font><br><br>\r\n";
				}
			}
			else
			{
				$x .= "No GM item.<br><br>\r\n";
			}

			if(!empty($AP_values))
			{
				$AP = implode(",", $AP_values);
				$sql = "insert into DISCIPLINE_GROUP_ACCESS_AP (GroupID, CategoryID, ItemID, ConductScore, Subscore, MeritCount, MeritType) values $AP";
				$set_ap_access_right = $li->db_db_query($sql);
				if ($set_ap_access_right)
				{
					$x .= "Set all the AP access rights for all eDiscipline groups ...  <font color='blue'><b>Success</b></font><br><br>\r\n";
				}
				else
				{
					$x .= "Set all the AP access rights for all eDiscipline groups ...  <font color='red'><b>Failed</b></font><br><br>\r\n";
				}
			}
			else
			{
				$x .= "No AP item.<br><br>\r\n";
			}

			if(!empty($HW_values))
			{
				$HW = implode(",", $HW_values);
				$sql = "insert into DISCIPLINE_GROUP_ACCESS_GM (GroupID, CategoryID, ItemID) values $HW";
				$set_hw_access_right = $li->db_db_query($sql);
				if ($set_hw_access_right)
				{
					$x .= "Set all the GM(homework) access rights for all eDiscipline groups ...  <font color='blue'><b>Success</b></font><br><br>\r\n";
				}
				else
				{
					$x .= "Set all the GM(homework) access rights for all eDiscipline groups ...  <font color='red'><b>Failed</b></font><br><br>\r\n";
				}
			}
			else
			{
				$x .= "No GM(homework) item.<br><br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eDis_SetGMAP_AllAccessible', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eDiscipline - set all the group/user GM/AP items are accessible [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		############################################################
		# eDiscipline - set all the group/user GM/AP items are accessible [End]
		############################################################

		###################################################
		### Sync Academic Year ID and TermID to Student Profile [Start]
		### Table: PROFILE_STUDENT_ATTENDANCE, PROFILE_STUDENT_MERIT
		###################################################
		if(!$GSary['StudentProfile_YearID_TermID'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Sync Academic Year ID and TermID to Student Profile [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "select YearTermID,AcademicYearID,TermStart,TermEnd from ACADEMIC_YEAR_TERM";
			$result = $li->returnArray($sql);

			include_once($PATH_WRT_ROOT."includes/form_class_manage.php");

			foreach($result as $k=>$d)
			{
				list($YearTermID, $AcademicYearID, $TermStart, $TermEnd) = $d;

				$ayt = new academic_year_term($YearTermID);
				$YearNameEn = $ayt->YearNameEN;
				$YearTermNameEN = $ayt->YearTermNameEN;
				$thisStartDate = substr($TermStart, 0,10);
				$thisEndDate = substr($TermEnd, 0,10);

				# PROFILE_STUDENT_ATTENDANCE
				$sql1 = "update PROFILE_STUDENT_ATTENDANCE set Year='$YearNameEn', Semester='$YearTermNameEN', AcademicYearID='$AcademicYearID', YearTermID='$YearTermID' where AttendanceDate >= '". $thisStartDate ."' and AttendanceDate<='". $thisEndDate ."'";
				$li->db_db_query($sql1) or debug_pr(mysql_error());

				# PROFILE_STUDENT_MERIT
				$sql1 = "update PROFILE_STUDENT_MERIT set Year='$YearNameEn', Semester='$YearTermNameEN', AcademicYearID='$AcademicYearID', YearTermID='$YearTermID' where MeritDate >= '". $thisStartDate ."' and MeritDate<='". $thisEndDate ."'";
				$li->db_db_query($sql1) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudentProfile_YearID_TermID', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "PROFILE_STUDENT_ATTENDANCE, PROFILE_STUDENT_MERIT sync Year ID and Term ID ...  <font color='blue'><b>Success</b></font><br><br>\r\n";

			$x .= "===================================================================<br>\r\n";
			$x .= "Sync Academic Year ID and TermID to Student Profile [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

		}
		###################################################
		### Sync Academic Year ID and TermID to PROFILE_STUDENT_ATTENDANCE [End]
		###################################################


		################################################
		# Award & Punishment Record Conversion (Start) #
		################################################

		if(!$GSary['eDis_APConversion_PeriodType_0308']) {
			$sql = "SELECT COUNT(*) FROM DISCIPLINE_GENERAL_SETTING WHERE SettingName='AwardConversionPeriod'";
			$result = $li->returnVector($sql);
			$isExist = $result[0]>0;

			if(!$isExist) {
				$sql = "INSERT INTO DISCIPLINE_GENERAL_SETTING(SettingName, SettingValue, DateInput, DateModified, ModifiedBy) VALUES ('AwardConversionPeriod', '0', NOW(), NOW(), '')";
				$li->db_db_query($sql);

				$x .= "<b>[eDisciplinev12]</b> - Update Conversion Period of Award  <font color='blue'>Successfully</font>.<br><br>";
			}

			$sql = "SELECT COUNT(*) FROM DISCIPLINE_GENERAL_SETTING WHERE SettingName='PunishConversionPeriod'";
			$result = $li->returnVector($sql);
			$isExist = $result[0]>0;

			if(!$isExist) {
				$sql = "INSERT INTO DISCIPLINE_GENERAL_SETTING(SettingName, SettingValue, DateInput, DateModified, ModifiedBy) VALUES ('PunishConversionPeriod', '0', NOW(), NOW(), '')";
				$li->db_db_query($sql);

				$x .= "<b>[eDisciplinev12]</b> - Update Conversion Period of Punishment <font color='blue'>Successfully</font>.<br><br>";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eDis_APConversion_PeriodType_0308', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		##############################################
		# Award & Punishment Record Conversion (End) #
		##############################################

		################################################
		# Promotion Method (Start) 					   #
		################################################

		if(!$GSary['eDis_APPromotion_PeriodType']) {
			$sql = "SELECT COUNT(*) FROM DISCIPLINE_GENERAL_SETTING WHERE SettingName='AwardPromotionMethod'";
			$result = $li->returnVector($sql);
			$isExist = $result[0]>0;

			if(!$isExist) {
				$sql = "INSERT INTO DISCIPLINE_GENERAL_SETTING(SettingName, SettingValue, DateInput, DateModified, ModifiedBy) VALUES ('AwardPromotionMethod', 'global', NOW(), NOW(), '')";
				$li->db_db_query($sql);

				$x .= "<b>[eDisciplinev12]</b> - Update Conversion Period of Award  <font color='blue'>Successfully</font>.<br><br>";
			}

			$sql = "SELECT COUNT(*) FROM DISCIPLINE_GENERAL_SETTING WHERE SettingName='PunishPromotionMethod'";
			$result = $li->returnVector($sql);
			$isExist = $result[0]>0;

			if(!$isExist) {
				$sql = "INSERT INTO DISCIPLINE_GENERAL_SETTING(SettingName, SettingValue, DateInput, DateModified, ModifiedBy) VALUES ('PunishPromotionMethod', 'global', NOW(), NOW(), '')";
				$li->db_db_query($sql);

				$x .= "<b>[eDisciplinev12]</b> - Update Conversion Period of Punishment <font color='blue'>Successfully</font>.<br><br>";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eDis_APPromotion_PeriodType', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		##############################################
		# Promotion Method (End) 					 #
		##############################################

		#################################################
		# Copy Location Code to Location Barcode (Start)#
		#################################################
		if(!$GSary['CampusLocation_CopyLocationCodeToBarcode'])
		{
			/*
			$sql = "SELECT BuildingID, Code FROM INVENTORY_LOCATION_BUILDING WHERE RecordStatus = 1";
			$arrBuilding = $li->returnArray($sql,2);

			if(sizeof($arrBuilding)>0)
			{
				for($i=0; $i<sizeof($arrBuilding); $i++)
				{
					list($building_id, $building_code) = $arrBuilding[$i];

					$building_barcode = strtoupper(str_replace("_","/",trim($building_code)));

					$sql = "UPDATE INVENTORY_LOCATION_BUILDING SET Barcode = '$building_barcode' WHERE BuildingID = $building_id";
					$ResultBuildingBarcode[] = $li->db_db_query($sql);
				}

				if(!in_array(false,$ResultBuildingBarcode))
				{
					$x .= "<b>[Campus Location]</b> - Copy Building Code to Building Barcode <font color='blue'>Successfully</font>.<br><br>";
				}
				else
				{
					$x .= "<b>[Campus Location]</b> - Copy Building Code to Building Barcode <font color='red'>Failed</font>.<br><br>";
				}
			}

			$sql = "SELECT LocationLevelID, Code FROM INVENTORY_LOCATION_LEVEL WHERE RecordStatus = 1";
			$arrLocationLevel = $li->returnArray($sql);

			if(sizeof($arrLocationLevel)>0)
			{
				for($i=0; $i<sizeof($arrLocationLevel); $i++)
				{
					list($location_level_id, $location_level_code) = $arrLocationLevel[$i];

					$location_level_barcode = strtoupper(str_replace("_","/",trim($location_level_code)));

					$sql = "UPDATE INVENTORY_LOCATION_LEVEL SET Barcode = '$location_level_barcode' WHERE LocationLevelID = $location_level_id";
					$ResultFloorBarcode[] = $li->db_db_query($sql);
				}

				if(!in_array(false,$ResultFloorBarcode))
				{
					$x .= "<b>[Campus Location]</b> - Copy Floor Code to Floor Barcode <font color='blue'>Successfully</font>.<br><br>";
				}
				else
				{
					$x .= "<b>[Campus Location]</b> - Copy Floor Code to Floor Barcode <font color='red'>Failed</font>.<br><br>";
				}
			}

			$sql = "SELECT LocationID, Code FROM INVENTORY_LOCATION WHERE RecordStatus = 1";
			$arrLocation = $li->returnArray($sql);

			if(sizeof($arrLocation)>0)
			{
				for($i=0; $i<sizeof($arrLocation); $i++)
				{
					list($location_id, $location_code) = $arrLocation[$i];

					$location_barcode = strtoupper(str_replace("_","/",trim($location_code)));

					$sql = "UPDATE INVENTORY_LOCATION SET Barcode = '$location_barcode' WHERE LocationID = $location_id";
					$ResultRoomBarcode[] = $li->db_db_query($sql);
				}

				if(!in_array(false,$ResultRoomBarcode))
				{
					$x .= "<b>[Campus Location]</b> - Copy Room Code to Room Barcode <font color='blue'>Successfully</font>.<br><br>";
				}
				else
				{
					$x .= "<b>[Campus Location]</b> - Copy Room Code to Room Barcode <font color='red'>Failed</font>.<br><br>";
				}
			}
			*/

			$x .= "===================================================================<br>\r\n";
			$x .= "Copy Location Code to Location Barcode (Start)</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT BuildingID, Code FROM INVENTORY_LOCATION_BUILDING WHERE RecordStatus = 1";
			$arrBuilding = $li->returnArray($sql,2);

			if(sizeof($arrBuilding)>0)
			{
				for($i=0; $i<sizeof($arrBuilding); $i++)
				{
					list($building_id, $building_code) = $arrBuilding[$i];

					$building_barcode = strtoupper(str_replace("_","/",trim($building_code)));
					$building_barcode = strtoupper(str_replace("\"","|",trim($building_barcode)));
					$building_barcode = strtoupper(str_replace("'","|",trim($building_barcode)));

					$sql = "UPDATE INVENTORY_LOCATION_BUILDING SET Barcode = '$building_barcode' WHERE BuildingID = '$building_id'";
					$ResultBuildingBarcode[] = $li->db_db_query($sql);
				}

				if(!in_array(false,$ResultBuildingBarcode))
				{
					$x .= "<b>[Campus Location]</b> - Copy Building Code to Building Barcode <font color='blue'>Successfully</font>.<br><br>";
				}
				else
				{
					$x .= "<b>[Campus Location]</b> - Copy Building Code to Building Barcode <font color='red'>Failed</font>.<br><br>";
				}
			}

			$sql = "SELECT LocationLevelID, Code FROM INVENTORY_LOCATION_LEVEL WHERE RecordStatus = 1";
			$arrLocationLevel = $li->returnArray($sql);

			if(sizeof($arrLocationLevel)>0)
			{
				for($i=0; $i<sizeof($arrLocationLevel); $i++)
				{
					list($location_level_id, $location_level_code) = $arrLocationLevel[$i];

					$location_level_barcode = strtoupper(str_replace("_","/",trim($location_level_code)));
					$location_level_barcode = strtoupper(str_replace("\"","|",trim($location_level_barcode)));
					$location_level_barcode = strtoupper(str_replace("'","|",trim($location_level_barcode)));

					$sql = "UPDATE INVENTORY_LOCATION_LEVEL SET Barcode = '$location_level_barcode' WHERE LocationLevelID = '$location_level_id'";
					$ResultFloorBarcode[] = $li->db_db_query($sql);
				}

				if(!in_array(false,$ResultFloorBarcode))
				{
					$x .= "<b>[Campus Location]</b> - Copy Floor Code to Floor Barcode <font color='blue'>Successfully</font>.<br><br>";
				}
				else
				{
					$x .= "<b>[Campus Location]</b> - Copy Floor Code to Floor Barcode <font color='red'>Failed</font>.<br><br>";
				}
			}

			$sql = "SELECT LocationID, Code FROM INVENTORY_LOCATION WHERE RecordStatus = 1";
			$arrLocation = $li->returnArray($sql);

			if(sizeof($arrLocation)>0)
			{
				for($i=0; $i<sizeof($arrLocation); $i++)
				{
					list($location_id, $location_code) = $arrLocation[$i];

					$location_barcode = strtoupper(str_replace("_","/",trim($location_code)));
					$location_barcode = strtoupper(str_replace("\"","|",trim($location_barcode)));
					$location_barcode = strtoupper(str_replace("'","|",trim($location_barcode)));

					$sql = "UPDATE INVENTORY_LOCATION SET Barcode = '$location_barcode' WHERE LocationID = '$location_id'";
					$ResultRoomBarcode[] = $li->db_db_query($sql);
				}

				if(!in_array(false,$ResultRoomBarcode))
				{
					$x .= "<b>[Campus Location]</b> - Copy Room Code to Room Barcode <font color='blue'>Successfully</font>.<br><br>";
				}
				else
				{
					$x .= "<b>[Campus Location]</b> - Copy Room Code to Room Barcode <font color='red'>Failed</font>.<br><br>";
				}
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CampusLocation_CopyLocationCodeToBarcode', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Copy Location Code to Location Barcode (End)</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################
		# Copy Location Code to Location Barcode (End)#
		#################################################


		#############################################################################
		# UPDATE ALL Teacher account - Set ClassName and ClassNumber to NULL (Start)#
		#############################################################################
		if(!$GSary['IntranetUser_UpdateTeacherClassNameClassNumber'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "UPDATE ALL Teacher account - Set ClassName and ClassNumber to NULL (Start)</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "UPDATE INTRANET_USER SET CLASSNAME = NULL, CLASSNUMBER = NULL WHERE RECORDTYPE = 1";
			$UpdateTeacherResult = $li->db_db_query($sql);
			if($UpdateTeacherResult)
			{
				$x .= "<b>[Intranet User]</b> - Update ALL Teachers ClassName and ClassNumber <font color='blue'>Successfully</font>.<br><br>";
			}
			else
			{
				$x .= "<b>[Intranet User]</b> - Update ALL Teachers ClassName and ClassNumber <font color='red'>Failed</font>.<br><br>";
			}
			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IntranetUser_UpdateTeacherClassNameClassNumber', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "UPDATE ALL Teacher account - Set ClassName and ClassNumber to NULL (End)</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################################################
		# UPDATE ALL Teacher account - Set ClassName and ClassNumber to NULL (End)	#
		#############################################################################


		#############################################
		### eEnrol - Term-based club data patch start
		#############################################
		include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
		$libclubsenrol = new libclubsenrol();
		$libclubsenrol->db = $intranet_db;
		if ($plugin['eEnrollment'] && $libclubsenrol->isUsingYearTermBased == 1 && !$GSary['eEnrol_Year_Term_Based_Data_Patch'])
		{
			$eEnrol_term_based_club_output = '';
			$eEnrol_term_based_club_output .= "<br /><br />";
			$eEnrol_term_based_club_output .= "<b>[eEnrol 1.2]</b> - Term based club enhancement Start<br />";

			## add INTRANET_ENROL_GROUPINFO if there is no record
			$sql = "SELECT
							a.GroupID, b.EnrolGroupID
					FROM
							INTRANET_GROUP as a
							LEFT OUTER JOIN
							INTRANET_ENROL_GROUPINFO as b
							ON (a.GroupID = b.GroupID)
					WHERE
							a.RecordType = 5
					";
			$GroupArr = $libclubsenrol->returnArray($sql);

			$numOfGroup = count($GroupArr);
			for ($i=0; $i<$numOfGroup; $i++)
			{
				$thisGroupID = $GroupArr[$i]['GroupID'];
				$thisEnrolGroupID = $GroupArr[$i]['EnrolGroupID'];

				if ($thisEnrolGroupID == '')
					$libclubsenrol->Insert_Year_Based_Club($thisGroupID);
			}

			## update Club info
			$sql = "SELECT
							b.EnrolGroupID,
							b.GroupID,
							b.AttachmentLink1,
							b.AttachmentLink2,
							b.AttachmentLink3,
							b.AttachmentLink4,
							b.AttachmentLink5,
							a.Title
					FROM
							INTRANET_ENROL_GROUPINFO as b
							INNER JOIN
							INTRANET_GROUP as a
							ON (a.GroupID = b.GroupID)
					";
			$ClubArr = $libclubsenrol->returnArray($sql);
			$numOfClub = count($ClubArr);

			$eEnrol_term_based_club_output .= "eEnrolment_term_based_script.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";

			if ($numOfClub > 0)
			{
				$SuccessArr = array();
				$fs = new libfilesystem();
				for ($i=0; $i<$numOfClub; $i++)
				{
					$thisEnrolGroupID = $ClubArr[$i]['EnrolGroupID'];
					$thisGroupID = $ClubArr[$i]['GroupID'];
					$thisTitle = $ClubArr[$i]['Title'];

					$eEnrol_term_based_club_output .= "================= Start Club ".$thisTitle." (GroupID = ".$thisGroupID.", EnrolGroupID = ".$thisEnrolGroupID.") =================<br>\r\n";

					## Activity Record
					$sql = "Select EnrolEventID From INTRANET_ENROL_EVENTINFO Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' Or EnrolGroupID Is Null)";
					$activityArr = $libclubsenrol->returnVector($sql);
					$numOfActivity = count($activityArr);

					if ($numOfActivity > 0)
					{
						$activityList = implode(',', $activityArr);

						$sql = "Update INTRANET_ENROL_EVENTINFO set EnrolGroupID = '".$thisEnrolGroupID."' Where EnrolEventID In (".$activityList.")";
						$SuccessArr[$thisGroupID]['Activity'] = $libclubsenrol->db_db_query($sql);

						if ($SuccessArr[$thisGroupID]['Activity'])
							$eEnrol_term_based_club_output .= "Update Activity <font color='blue'><b>Success</b></font><br>\r\n";
						else
							$eEnrol_term_based_club_output .= "Update Activity <font color='red'><b>Failed</b></font><br>\r\n";
					}
					else
					{
						$eEnrol_term_based_club_output .= "No activities for this club to update<br>\r\n";
					}

					## If have Main info => club added after enhancement / has run data patch already
					$sql = "SELECT * FROM INTRANET_ENROL_GROUPINFO_MAIN Where GroupID = '".$thisGroupID."'";
					$thisMainInfo = $libclubsenrol->returnArray($sql);

					# INTARNET_ENROL_GROUP_ATTENDANCE
					$sql = "UPDATE INTRANET_ENROL_GROUP_ATTENDANCE Set EnrolGroupID = '".$thisEnrolGroupID."'
							Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' OR EnrolGroupID Is Null)";
					$SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUP_ATTENDANCE'] = $libclubsenrol->db_db_query($sql);

					if ($SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUP_ATTENDANCE'])
						$eEnrol_term_based_club_output .= "Update INTRANET_ENROL_GROUP_ATTENDANCE <font color='blue'><b>Success</b></font><br>\r\n";
					else
						$eEnrol_term_based_club_output .= "Update INTRANET_ENROL_GROUP_ATTENDANCE <font color='red'><b>Failed</b></font><br>\r\n";


					if (count($thisMainInfo) > 0)
					{
						$eEnrol_term_based_club_output .= "This Club has been updated before.<br>\r\n";
						$eEnrol_term_based_club_output .= "================== End Club ".$thisTitle." (GroupID = ".$thisGroupID.", EnrolGroupID = ".$thisEnrolGroupID.") ==================<br><br>\r\n";

						continue;
					}

					# INTARNET_ENROL_GROUPSTUDENT
					$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT Set EnrolGroupID = '".$thisEnrolGroupID."'
							Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' OR EnrolGroupID Is Null)";
					$SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPSTUDENT'] = $libclubsenrol->db_db_query($sql) or debug_pr(mysql_error());

					if ($SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPSTUDENT'])
						$eEnrol_term_based_club_output .= "Update INTRANET_ENROL_GROUPSTUDENT <font color='blue'><b>Success</b></font><br>\r\n";
					else
						$eEnrol_term_based_club_output .= "Update INTRANET_ENROL_GROUPSTUDENT <font color='red'><b>Failed</b></font><br>\r\n";

					# INTARNET_USERGROUP
					$sql = "Select UserGroupID From INTRANET_USERGROUP Where GroupID = '".$thisGroupID."'";
					$memberArr = $libclubsenrol->returnVector($sql);
					$numOfMember = count($memberArr);

					if ($numOfMember)
					{
						// delete duplicated member records
						for ($j=0; $j<$numOfMember; $j++)
						{
							$thisUserGroupID = $memberArr[$j]['UserGroupID'];
							$thisDeleteGroupID = $memberArr[$j]['GroupID'];
							$thisStudentID = $memberArr[$j]['UserID'];

							$sql = "Select
											*
									From
											INTRANET_USERGROUP
									Where
											GroupID = '".$thisDeleteGroupID."'
											And
											UserID = '".$thisStudentID."'
											And
											(EnrolGroupID = '' Or EnrolGroupID is Null)
											And
											UserGroupID != '".$thisUserGroupID."'
									";
							$tmpArr = $libclubsenrol->returnArray($sql);

							if (count($tmpArr) > 0)
							{
								$sql = "Delete From INTRANET_USERGROUP Where UserGroupID = '".$thisUserGroupID."'";
								$SuccessArr[$thisGroupID]['Delete_Duplicated_INTRANET_USERGROUP'][$thisUserGroupID] = $libclubsenrol->db_db_query($sql);

								if ($SuccessArr[$thisGroupID]['Delete_Duplicated_INTRANET_USERGROUP'][$thisUserGroupID])
									$eEnrol_term_based_club_output .= "<font color='blue'>Success (UserGroupID = ".$thisUserGroupID.")</font><br /><br />\r\n\r\n";
								else
									$eEnrol_term_based_club_output .= "<font color='red'>Failed (UserGroupID = ".$thisUserGroupID.")</font><br /><br />\r\n\r\n";
							}
						}

						// update EnrolGroupID Field
						$sql = "UPDATE INTRANET_USERGROUP Set EnrolGroupID = '".$thisEnrolGroupID."'
								Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' OR EnrolGroupID Is Null)";
						$SuccessArr[$thisGroupID]['INTRANET_USERGROUP'] = $libclubsenrol->db_db_query($sql);

						if ($SuccessArr[$thisGroupID]['INTRANET_USERGROUP'])
							$eEnrol_term_based_club_output .= "Update INTRANET_USERGROUP <font color='blue'><b>Success</b></font><br>\r\n";
						else
							$eEnrol_term_based_club_output .= "Update INTRANET_USERGROUP <font color='red'><b>Failed</b></font><br>\r\n";
					}
					else
					{
						$eEnrol_term_based_club_output .= "No record in INTRANET_USERGROUP for this club<br>\r\n";
					}


					# Add Default Main Info
					$sql = "INSERT INTO INTRANET_ENROL_GROUPINFO_MAIN
									(GroupID, ClubType, ApplyOnceOnly, FirstSemEnrolOnly, DateInput)
							VALUES
									('$thisGroupID', 'Y', 0, 0, now())
							";
					$SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPINFO_MAIN'] = $libclubsenrol->db_db_query($sql);

					if ($SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPINFO_MAIN'])
						$eEnrol_term_based_club_output .= "Insert INTRANET_ENROL_GROUPINFO_MAIN <font color='blue'><b>Success</b></font><br>\r\n";
					else
						$eEnrol_term_based_club_output .= "Insert INTRANET_ENROL_GROUPINFO_MAIN <font color='red'><b>Failed</b></font><br>\r\n";

					# Attachment Transfer
					for ($j=1; $j<=5; $j++)
					{
						$thisAttachment = $ClubArr[$i]['AttachmentLink'.$j];

						if ($thisAttachment == '')
							continue;

						## move the file to the EnrolGroupID folder
						$AttachmentInfoArr = explode('/', $thisAttachment);
						$timestamp_folder = $AttachmentInfoArr[2];
						$file_name = $AttachmentInfoArr[3];

						$new_folder_prefix = $intranet_root."/file/enroll_info/enrolgroup".$thisEnrolGroupID;
						if (!file_exists($new_folder_prefix))
							$fs->folder_new($new_folder_prefix);

						$new_folder_prefix .= "/".$timestamp_folder;
						if (!file_exists($new_folder_prefix))
							$fs->folder_new($new_folder_prefix);

						$old_file = $intranet_root."/file/".$thisAttachment;
						$new_file = $new_folder_prefix."/".$file_name;

						if (is_file($old_file))
							$Success['EnrolGroupID_' + $thisEnrolGroupID]['Copy_File'] = $fs->file_copy($old_file, $new_file);

						//if ($Success['EnrolGroupID_' + $thisEnrolGroupID]['Update_DB'])
						//{
							$eEnrol_term_based_club_output .= "Copy Attachment ".$file_name." to EnrolGroupID folder <font color='blue'><b>Success</b></font><br>\r\n";
						//}
						//else
						//{
						//	$eEnrol_term_based_club_output .= "Copy Attachment ".$file_name." to EnrolGroupID folder <font color='red'><b>Failed</b></font><br>\r\n";
						//}

						## update the path in DB
						$new_file_path = "enroll_info/enrolgroup".$thisEnrolGroupID."/".$timestamp_folder."/".$file_name;
						$sql = "UPDATE
										INTRANET_ENROL_GROUPINFO
								SET
										AttachmentLink".$j." = '".$libclubsenrol->Get_Safe_Sql_Query($new_file_path)."'
								WHERE
										EnrolGroupID = '".$thisEnrolGroupID."'
								";
						$Success['EnrolGroupID_' + $thisEnrolGroupID]['Update_DB'] = $libclubsenrol->db_db_query($sql);

						if ($Success['EnrolGroupID_' + $thisEnrolGroupID]['Update_DB'])
						{
							$eEnrol_term_based_club_output .= "Update Attachment ".$file_name." in DB <font color='blue'><b>Success</b></font><br>\r\n";
						}
						else
						{
							$eEnrol_term_based_club_output .= "Update Attachment ".$file_name." in DB <font color='red'><b>Failed</b></font><br>\r\n";
						}
					}

					## OLE Record
					$sql = "SELECT OLE_ProgramID FROM INTRANET_GROUP Where GroupID = '".$thisGroupID."'";
					$resultSet = $libclubsenrol->returnVector($sql);
					$thisProgramID = $resultSet[0];

					$sql = "UPDATE INTRANET_ENROL_GROUPINFO Set OLE_ProgramID = '".$thisProgramID."' Where EnrolGroupID = '".$thisEnrolGroupID."'";
					$SuccessArr[$thisGroupID]['OLE_ProgramID'] = $libclubsenrol->db_db_query($sql);

					if ($SuccessArr[$thisGroupID]['OLE_ProgramID'])
						$eEnrol_term_based_club_output .= "Update OLE_ProgramID <font color='blue'><b>Success</b></font><br>\r\n";
					else
						$eEnrol_term_based_club_output .= "Update OLE_ProgramID <font color='red'><b>Failed</b></font><br>\r\n";

					$eEnrol_term_based_club_output .= "================== End Club ".$thisTitle." (GroupID = ".$thisGroupID.", EnrolGroupID = ".$thisEnrolGroupID.") ==================<br><br>\r\n";
				}
			}
			else
			{
				$eEnrol_term_based_club_output .= "No Club set up in eEnrolment yet.";
			}

			$eEnrol_term_based_club_output .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";

			#  save log
			$log_folder = $PATH_WRT_ROOT."file/enroll_info";
			if(!file_exists($log_folder))
				mkdir($log_folder);
			$log_file = $log_folder."/term_based_data_patch_log_". date("YmdHis") .".html";
			// $lf = new libfilesystem();
			$lf->file_write($eEnrol_term_based_club_output, $log_file);
			$eEnrol_term_based_club_output .=  "<br>File Log: ". $log_file."<br>";

			$eEnrol_term_based_club_output .= "<b>[eEnrol 1.2]</b> - Term based club enhancement End<br /><br /><br />";

			$x .= $eEnrol_term_based_club_output;

			# update General Settings - mark the script is executed
	    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrol_Year_Term_Based_Data_Patch', 1, now())";
	    	$li->db_db_query($sql) or debug_pr(mysql_error());

			#############################################
			### eEnrol - Term-based club data patch end
			#############################################
		}

		#############################################
		### eEnrol - Clear old admin records start
		#############################################
		if ($plugin['eEnrollment'] && !$GSary['eEnrol_Clear_Old_Admin_Records'])
		{
			$SuccessArr = array();

			# Add Default Main Info
			$sql = "Delete From INTRANET_ENROL_USER_ACL Where UserLevel = 2";
			$SuccessArr['Clear_Old_Admin_Records'] = $libclubsenrol->db_db_query($sql);

			if ($SuccessArr['Clear_Old_Admin_Records'])
				$x .= "Clear Old Admin Records <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";
			else
				$x .= "Clear Old Admin Records <font color='red'><b>Failed</b></font><br>\r\n<br>\r\n";

			# update General Settings - mark the script is executed
	    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrol_Clear_Old_Admin_Records', 1, now())";
	    	$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		#############################################
		### eEnrol - Clear old admin records end
		#############################################

		#############################################
		### eEnrol - Set old attendance data to present
		#############################################
		if ($plugin['eEnrollment'] && !$GSary['eEnrol_Set_Old_Attendance_As_Present'])
		{
			$SuccessArr = array();

			# Club
			$sql = "Update INTRANET_ENROL_GROUP_ATTENDANCE set RecordStatus = 1 Where RecordStatus is Null";
			$SuccessArr['INTRANET_ENROL_GROUP_ATTENDANCE'] = $libclubsenrol->db_db_query($sql);

			if ($SuccessArr['INTRANET_ENROL_GROUP_ATTENDANCE'])
				$x .= "Club Attendance Records Update <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";
			else
				$x .= "Club Attendance Records Update <font color='red'><b>Failed</b></font><br>\r\n<br>\r\n";

			# Activity
			$sql = "Update INTRANET_ENROL_EVENT_ATTENDANCE set RecordStatus = 1 Where RecordStatus is Null";
			$SuccessArr['INTRANET_ENROL_EVENT_ATTENDANCE'] = $libclubsenrol->db_db_query($sql);

			if ($SuccessArr['INTRANET_ENROL_EVENT_ATTENDANCE'])
				$x .= "Activity Attendance Records Update <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";
			else
				$x .= "Activity Attendance Records Update <font color='red'><b>Failed</b></font><br>\r\n<br>\r\n";

			# update General Settings - mark the script is executed
	    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrol_Set_Old_Attendance_As_Present', 1, now())";
	    	$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		#############################################
		### eEnrol - Set old attendance data to present
		#############################################

		#############################################
		### eCommunity - Transfer weblink to INTRANET_FILE [Start]
		#############################################
		if(!$GSary['eComm_FileTransferFromGroup'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eCommunity - Transfer weblink to INTRANET_FILE [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			##### Web Link
			$sql = "select * from INTRANET_LINK";
			$result = $li->returnArray($sql);

			foreach($result as $k=>$d)
			{
				list($LinkID, $GroupID, $UserID, $UserName, $UserEmail, $Category, $Title, $URL, $Keyword, $Description, $ReadFlag, $VoteNum, $VoteTot, $RecordType, $RecourdStatus, $DateInput, $DateModified) = $d;

				# check default folder exists or not
				$sql2 = "select FolderID from INTRANET_FILE_FOLDER where GroupID='$GroupID' and FolderType='W' order by FolderID";
				$result2 = $li->returnVector($sql2);
				$FolderID = $result2[0];

				if(empty($FolderID))
				{
					$sql3 = "insert into INTRANET_FILE_FOLDER (GroupID, FolderType, UserID, UserName, Title, PublicStatus, DateInput)
							values
							('$GroupID', 'W', 0, '(System Default)', '(Temp Folder)', 1, now())";
					$li->db_db_query($sql3);
					$FolderID= $li->db_insert_id();
				}

				# insert record in INTRANET_FILE
				$sql4 = "insert into INTRANET_FILE
							(GroupID, UserID, UserName, UserEmail, Title, Location, Description, ReadFlag, DateInput, DateModified, FileType, PublicStatus, FolderID, UserTitle, ComnentFlag, Approved)
							values
							('$GroupID', '$UserID', '$UserName', '$UserEmail', '$URL', '$URL', '$Description', '$ReadFlag', '$DateInput', '$DateModified', 'W', 1, '$FolderID', '$Title', 1, 1)";
				$li->db_db_query($sql4);
			}
			$x .= "eCommunity copy data from INTRANET_FILE ... ". sizeof($result) ." <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";

			##### File (Set the FileType NULL > "F")
			$sql5 = "update INTRANET_FILE set FileType='F' where FileType is NULL";
			$li->db_db_query($sql5);
			$x .= "eCommunity set FileType='F' if FileType is NULL <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";

			# update General Settings - mark the script is executed
	    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eComm_FileTransferFromGroup', 1, now())";
	    	$li->db_db_query($sql) or debug_pr(mysql_error());

	    	$x .= "===================================================================<br>\r\n";
			$x .= "eCommunity - Transfer weblink to INTRANET_FILE [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### eCommunity - Transfer weblink to INTRANET_FILE [End]
		#############################################

		#############################################
		### Swimming gala - update event type from 'Track' to 'Individual Event' [Start]
		#############################################
		if(!$GSary['SwimmingGala_event_type_wording'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Swimming gala - update event type from 'Track' to 'Individual Event' [Start] </b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "update SWIMMINGGALA_EVENT_TYPE_NAME set EnglishName='Individual Event', ChineseName='個人賽事' where EnglishName='Track'";
			$li->db_db_query($sql);
			$x .= "Swimming gala - update event type from 'Track' to 'Individual Event' <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";

			# update General Settings - mark the script is executed
	    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SwimmingGala_event_type_wording', 1, now())";
	    	$li->db_db_query($sql) or debug_pr(mysql_error());

	    	$x .= "===================================================================<br>\r\n";
			$x .= "Swimming gala - update event type from 'Track' to 'Individual Event't  [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### Swimming gala - update event type from 'Track' to 'Individual Event' [End]
		#############################################

		#############################################
		### Staff Attendance - migrate student IP setting to staff IP setting
		#############################################
		if(!$GSary['StaffAttendanceTerminalIPMigrationFromStudentAttend'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Staff Attendance - migrate student IP setting to staff IP setting [Start] </b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$StudentAttendIPSetting = $lgs->Get_General_Setting('StudentAttendance',array("'TerminalIP'"));
			$StaffAttendIPSetting = $lgs->Get_General_Setting('StaffAttendance',array("'TerminalIP'"));
			if (trim($StaffAttendIPSetting['TerminalIP']) == "") {
				$StaffSettings['TerminalIP'] = $StudentAttendIPSetting['TerminalIP'];
				$lgs->Save_General_Setting('StaffAttendance',$StaffSettings);
			}

			# update General Settings - mark the script is executed
    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StaffAttendanceTerminalIPMigrationFromStudentAttend', 1, now())";
    	$li->db_db_query($sql) or debug_pr(mysql_error());

	    $x .= "===================================================================<br>\r\n";
			$x .= "Staff Attendance - migrate student IP setting to staff IP setting  [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### Staff Attendance - migrate student IP setting to staff IP setting [End]
		#############################################

		#############################################
		### Double check the "Identity" default group's Academic year is NULL [Start]
		#############################################
		if(!$GSary['SetIdentityDefaultGroupAcadmicYearNULL'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Double check the 'Identity' default group's Academic year is NULL [Start] </b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
			$sql = "update INTRANET_GROUP set AcademicYearID = NULL where RecordType=0";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			# update General Settings - mark the script is executed
	    	$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SetIdentityDefaultGroupAcadmicYearNULL', 1, now())";
	    	$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Double check the 'Identity' default group's Academic year is NULL [End] </b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### Double check the "Identity" default group's Academic year is NULL [End]
		#############################################

		#############################################
		### iCalendar agenda by default [Start]
		#############################################
		if(!$GSary[' iCalendarAgendaByDefault'])
		{
			$db = new libdb();

			$x .= 'iCalendar Agenda By Default';
			// get current academic year in school
			$sql = "update CALENDAR_CONFIG set value = 'agenda' where setting = 'PreferredView'";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= 'OK<br>';

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', ' iCalendarAgendaByDefault', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		#############################################
		###  iCalendar agenda by default  [End]
		#############################################

		#############################################
		### eEnrol - Term-based club data patch 2 - relink member records start
		#############################################
		// $libclubsenrol has been initialized in the upper part already
		if ($plugin['eEnrollment'] && $libclubsenrol->isUsingYearTermBased == 1 && !$GSary[' eEnrol_Year_Term_Based_Data_Patch_2'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Term-based club data patch 2 - relink member records [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "Update
							INTRANET_USERGROUP as iug
							Inner Join
							INTRANET_ENROL_GROUPINFO as iegi
							On (iug.GroupID = iegi.GroupID)
					Set
							iug.EnrolGroupID = iegi.EnrolGroupID
					Where
							(iug.EnrolGroupID is Null Or iug.EnrolGroupID = '')
					";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', ' eEnrol_Year_Term_Based_Data_Patch_2', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Term-based club data patch 2 - relink member records [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### eEnrol - Term-based club data patch 2 - relink member records end
		#############################################

		#############################################
		### eEnrol - Term-based club data patch 3 - Update the Term Name to YearTermID
		#############################################
		// $libclubsenrol has been initialized in the upper part already
		if ($plugin['eEnrollment'] && $libclubsenrol->isUsingYearTermBased == 1 && !$GSary[' eEnrol_Update_TermName_To_ID'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Term-based club data patch 3 - Update the Term Name to YearTermID [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$SuccessArr = array();

			### Get all clubs which semester is not ID
			$sql = "Select
							iegi.EnrolGroupID, iegi.Semester, ig.AcademicYearID
					From
							INTRANET_ENROL_GROUPINFO as iegi
							Inner Join
							INTRANET_GROUP as ig
							On (iegi.GroupID = ig.GroupID)
					Where
							iegi.Semester != ''
							And
							iegi.Semester Is Not Null
							And
							iegi.Semester REGEXP '^-?[0-9]+$' = 0
					";
			$ClubArr = $li->returnArray($sql);
			$numOfClub = count($ClubArr);

			### Build Term Info Array
			$sql = "Select
							YearTermID, AcademicYearID, YearTermNameEN, YearTermNameB5
					From
							ACADEMIC_YEAR_TERM
					";
			$YearTermArr = $li->returnArray($sql);
			$numOfYearTerm = count($YearTermArr);

			$YearTermAsso = array();
			for ($i=0; $i<$numOfYearTerm; $i++)
			{
				$thisYearTermID 	= $YearTermArr[$i]['YearTermID'];
				$thisAcademicYearID = $YearTermArr[$i]['AcademicYearID'];
				$thisYearTermNameEN = $YearTermArr[$i]['YearTermNameEN'];
				$thisYearTermNameB5 = $YearTermArr[$i]['YearTermNameB5'];

				$YearTermAsso[$thisAcademicYearID][$thisYearTermNameEN] = $thisYearTermID;
				$YearTermAsso[$thisAcademicYearID][$thisYearTermNameB5] = $thisYearTermID;
			}

			for ($i=0; $i<$numOfClub; $i++)
			{
				$thisEnrolGroupID 	= $ClubArr[$i]['EnrolGroupID'];
				$thisSemester 		= $ClubArr[$i]['Semester'];
				$thisAcademicYearID = $ClubArr[$i]['AcademicYearID'];
				$thisYearTermID 	= $YearTermAsso[$thisAcademicYearID][$thisSemester];

				if ($thisYearTermID != '' && $thisYearTermID > 0)
				{
					$sql = "Update INTRANET_ENROL_GROUPINFO Set Semester = '$thisYearTermID' Where EnrolGroupID = '$thisEnrolGroupID' ";
					$SuccessArr[$thisEnrolGroupID]['INTRANET_ENROL_GROUPINFO'] = $li->db_db_query($sql);

					$sql = "Select
									EnrolGroupStudentID, EnrolSemester
							From
									INTRANET_ENROL_GROUPSTUDENT
							Where
									EnrolGroupID = '$thisEnrolGroupID'
									And
									EnrolSemester REGEXP '^-?[0-9]+$' = 0
							";
					$EnrolStudentArr = $li->returnArray($sql);
					$numOfEnrolStudent = count($EnrolStudentArr);

					for ($j=0; $j<$numOfEnrolStudent; $j++)
					{
						$thisEnrolGroupStudentID 	= $EnrolStudentArr[$j]['EnrolGroupStudentID'];
						$thisEnrolSemester 			= $EnrolStudentArr[$j]['EnrolSemester'];
						$thisYearTermID 			= $YearTermAsso[$thisAcademicYearID][$thisEnrolSemester];

						$sql = "Update INTRANET_ENROL_GROUPSTUDENT Set EnrolSemester = '$thisYearTermID' Where EnrolGroupStudentID = '$thisEnrolGroupStudentID' ";
						$SuccessArr[$thisEnrolGroupID]['INTRANET_ENROL_GROUPSTUDENT'][$thisEnrolGroupStudentID] = $li->db_db_query($sql);
					}
				}
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', ' eEnrol_Update_TermName_To_ID', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Term-based club data patch 3 - Update the Term Name to YearTermID [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### eEnrol - Term-based club data patch 3 - relink member records end
		#############################################

		#############################################
		### Class History - copy current class info in the Class History Table
		#############################################
		if (!$GSary['ClassHistory_CopyCurrentClassInfoAsClassHistory'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "<b>Class History - copy current class info in the Class History Table [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
			$SuccessClassHistoryArr = array();

			### Get all current student class info
			$sql = "Select
							ycu.UserID as StudentID,
							yc.ClassTitleEN as ClassName,
							ycu.ClassNumber,
							ay.YearNameEN as AcademicYearName,
							yc.YearClassID,
							yc.AcademicYearID
					From
							YEAR_CLASS_USER as ycu
							Inner Join
							YEAR_CLASS as yc
							On (ycu.YearClassID = yc.YearClassID)
							Inner Join
							ACADEMIC_YEAR as ay
							On (yc.AcademicYearID = ay.AcademicYearID)
					Where
							yc.AcademicYearID = '".$CurrentAcademicYearID."'
					";
			$StudentClassInfoArr = $li->returnArray($sql);
			$numOfStudentClassInfo = count($StudentClassInfoArr);

			for ($i=0; $i<$numOfStudentClassInfo; $i++)
			{
				$thisStudentID 			= $StudentClassInfoArr[$i]['StudentID'];
				$thisClassName 			= $li->Get_Safe_Sql_Query($StudentClassInfoArr[$i]['ClassName']);
				$thisClassNumber 		= $StudentClassInfoArr[$i]['ClassNumber'];
				$thisAcademicYearName 	= $li->Get_Safe_Sql_Query($StudentClassInfoArr[$i]['AcademicYearName']);
				$thisYearClassID 		= $StudentClassInfoArr[$i]['YearClassID'];
				$thisAcademicYearID 	= $StudentClassInfoArr[$i]['AcademicYearID'];

				$sql = "Select RecordID From PROFILE_CLASS_HISTORY Where UserID = '$thisStudentID' And AcademicYear = '$thisAcademicYearName'";
				$thisResultArr = $li->returnArray($sql);
				$thisRecordID = $thisResultArr[0]['RecordID'];

				if ($thisRecordID != '')
				{
					# update
					$sql = "Update
								PROFILE_CLASS_HISTORY
							Set
								ClassName = '$thisClassName', ClassNumber = '$thisClassNumber', DateModified = now(), YearClassID = '$thisYearClassID', AcademicYearID = '$thisAcademicYearID'
							Where
								RecordID = '$thisRecordID'
							";
					$SuccessClassHistoryArr['Update'][$thisStudentID] = $li->db_db_query($sql);
				}
				else
				{
					# insert
					$sql = "Insert Into PROFILE_CLASS_HISTORY
								(UserID, ClassName, ClassNumber, AcademicYear, DateInput, DateModified, YearClassID, AcademicYearID)
							Values
								('$thisStudentID', '$thisClassName', '$thisClassNumber', '$thisAcademicYearName', now(), now(), '$thisYearClassID', '$thisAcademicYearID')
							";
					$SuccessClassHistoryArr['Insert'][$thisStudentID] = $li->db_db_query($sql);
				}
			}

			if (in_array(false, $SuccessClassHistoryArr))
			{
				$x .= "Copy current class info to Class History <font color='red'><b>Failed</b></font><br>\r\n";
				$x .= "Update Failed : ";
				foreach((array)$SuccessClassHistoryArr['Update'] as $thisStudentID => $thisSuccess)
				{
					if ($thisSuccess == false)
						$x .= $thisStudentID.', ';
				}

				$x .= "<br>\r\n";

				$x .= "Insert Failed : ";
				foreach((array)$SuccessClassHistoryArr['Insert'] as $thisStudentID => $thisSuccess)
				{
					if ($thisSuccess == false)
						$x .= $thisStudentID.', ';
				}
				$x .= "<br>\r\n<br>\r\n";
			}
			else
			{
				$x .= "Copy current class info to Class History <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ClassHistory_CopyCurrentClassInfoAsClassHistory', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "<b>Class History - copy current class info in the Class History Table [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### Class History - copy current class info in the Class History Table end
		#############################################

		#############################################
		### eEnrol - Add Academic Year field for Activity
		#############################################
		if (!$GSary['eEnrol_Add_Academic_Year_To_Activity'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Add Academic Year field for Activity [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$CurrentAcademicYearID = Get_Current_Academic_Year_ID();
			$SuccessAddYearToActivityArr = array();

			$sql = "Select EnrolEventID, EnrolGroupID From INTRANET_ENROL_EVENTINFO";
			$ActivityInfoArr = $li->returnArray($sql);
			$numOfActivity = count($ActivityInfoArr);

			for ($i=0; $i<$numOfActivity; $i++)
			{
				$thisEnrolEventID = $ActivityInfoArr[$i]['EnrolEventID'];
				$thisEnrolGroupID = $ActivityInfoArr[$i]['EnrolGroupID'];

				if ($thisEnrolGroupID=='' || $thisEnrolGroupID==0)
				{
					### update to current academic year if the activity is not linked to clubs
					$thisAcademicYearID = $CurrentAcademicYearID;
				}
				else
				{
					### update to the club's group's academic year
					$sql = "Select
									ig.AcademicYearID
							From
									INTRANET_ENROL_GROUPINFO as iegi
									Inner Join
									INTRANET_GROUP as ig
									On (iegi.GroupID = ig.GroupID)
							Where
									iegi.EnrolGroupID = '$thisEnrolGroupID'
							";
					$ResultArr = $li->returnArray($sql);
					$thisAcademicYearID = $ResultArr[0]['AcademicYearID'];

					// Club cannot be found
					if ($thisAcademicYearID=='' || $thisAcademicYearID==0)
						$thisAcademicYearID = $CurrentAcademicYearID;
				}

				$sql = "Update INTRANET_ENROL_EVENTINFO Set AcademicYearID = '$thisAcademicYearID' Where EnrolEventID = '$thisEnrolEventID'";
				$SuccessAddYearToActivityArr[$thisEnrolEventID] = $li->db_db_query($sql);
			}

			if (in_array(false, $SuccessAddYearToActivityArr))
			{
				$x .= "Add Academic Year to Activity <font color='red'><b>Failed</b></font><br>\r\n";
				$x .= "Failed EnrolEventID : ";
				foreach((array)$SuccessAddYearToActivityArr as $thisEnrolEventID => $thisSuccess)
				{
					if ($thisSuccess == false)
						$x .= $thisEnrolEventID.', ';
				}
				$x .= "<br>\r\n<br>\r\n";
			}
			else
			{
				$x .= "Add Academic Year to Activity <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrol_Add_Academic_Year_To_Activity', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Add Academic Year field for Activity [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### eEnrol - Add Academic Year field for Activity end
		#############################################

		#############################################
		### iCalendar add missed user [Start]
		#############################################
		if(!$GSary['iCalendarAddMissedUser'])
		{
			$x .= 'iCalendar Add Missed User<br>';
			include_once($PATH_WRT_ROOT."includes/icalendar.php");

			$iCal = new icalendar();

			$sql = "select course_id,CalID,course_name from {$eclass_db}.course where RoomType = 0";
			$all_course = $li->returnArray($sql);

			$result = Array();

			if (!empty($all_course)){
				foreach($all_course as $course){
					$sql = "select i.UserID from {$eclass_db}.user_course as u
					inner join INTRANET_USER as i on i.UserEmail = u.user_email
					where u.course_id =".$course[0];
					$all_user = $li->returnVector($sql);
					$calID = $course[1];
					if (empty($course[1])){
						$calID = $iCal->createSystemCalendar($course[2],3,"P",'');
						$sql = "update {$eclass_db}.course set CalID = '$calID' where course_id = '".$course[0]."'";
						$li->db_db_query($sql);
					}
					$sql ="select distinct UserID from CALENDAR_CALENDAR_VIEWER where calid = '$calID'";
					$exist_user = $li->returnVector($sql);
					$exist_user = empty($exist_user)?array():$exist_user;
					$missed_user = array_diff($all_user,$exist_user);
					$result['insertViewer_'. $course[0]] = $iCal->insertCalendarViewer($calID, $missed_user, "W", $course[0], 'C');

				}
			}
			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iCalendarAddMissedUser', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
			$x .= 'OK<br>';
		}
		#############################################
		###  iCalendar add missed user  [End]
		#############################################

		#############################################
		### eRC - Change csv file name start
		#############################################
		if ($plugin['ReportCard2008'] && !$GSary['eRC_Change_CSV_File_Name'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eRC - Change csv file name [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT.'includes/libreportcard2008.php');
			if (isset($ReportCardCustomSchoolName) && $ReportCardCustomSchoolName != "") {
				include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
				$lreportcard = new libreportcardcustom();
			} else {
				$lreportcard = new libreportcard();
			}
			$CsvFilePath = $lreportcard->dataFilesPath;
			$otherInfoTypesArr = $lreportcard->getOtherInfoType();
			$numOfInfoType = count($otherInfoTypesArr);

			// $lf = new libfilesystem();

			### Backup CSV First
			$CsvBackupFilePath = $CsvFilePath.'bak_'.date('Ymd_His');
			$lf->folder_copy($CsvFilePath, $CsvBackupFilePath);

			$SuccessRenameCSVArr = array();
			$FileCount = 0;
			for ($i=0; $i<$numOfInfoType; $i++)
			{
				$thisInfoType = $otherInfoTypesArr[$i];
				$thisFilePath = $CsvFilePath.$thisInfoType.'/';
				$thisFileArr = array();

				### Get File list
				if (file_exists($thisFilePath))
				{
					$handle = opendir($thisFilePath);

					while (($file = readdir($handle))!==false) {
						if($file!="." && $file!="..") {

							### New format => no need update
							if (substr($file, 0, 4) != $lreportcard->schoolYear)
								continue;

							### Update csv file only
							$SplitArr = explode(".", $file);
							$ext = $SplitArr[count($SplitArr)-1];

							if($ext=="csv") {
								$thisFileArr[] = $file;
							}
						}
					}
				}

				$numOfFile = count($thisFileArr);
				for ($j=0; $j<$numOfFile; $j++)
				{
					$thisFileName = $thisFileArr[$j];
					$thisFileNameArr = explode('_', $thisFileName);
					$thisYearTermID = $thisFileNameArr[1];
					$thisYearClassID = $thisFileNameArr[3];

					$sql = "Select YearID From YEAR_CLASS Where YearClassID = '$thisYearClassID'";
					$TmpArr = $li->returnArray($sql);
					$thisYearID = $TmpArr[0]['YearID'];

					if ($thisYearClassID == '' || $thisYearID == '')
						continue;

					$oldfile = $thisFilePath.$thisFileName;
					$backupfile = $thisFilePath.'bak/'.$thisFileName;
					$newfile = $thisFilePath.$thisYearTermID.'_'.$thisYearID.'_'.$thisYearClassID.'_unicode.csv';

					$SuccessRenameCSVArr[$FileCount] = $lf->file_rename($oldfile, $newfile);

					if ($SuccessRenameCSVArr[$FileCount])
						$x .= "From \"$oldfile\" to \"$newfile\" <font color='blue'><b>Success</b></font><br>\r\n";
					else
						$x .= "From \"$oldfile\" to \"$newfile\" <font color='red'><b>Failed</b></font><br>\r\n";

					$FileCount++;
				}
			}

			if (!in_array(false, $SuccessRenameCSVArr))
				$x .= "Rename all csv files <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";
			else
				$x .= "Rename all csv files <font color='red'><b>Failed</b></font><br>\r\n<br>\r\n";


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eRC_Change_CSV_File_Name', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eRC - Change csv file name [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### eRC - Change csv file name end
		#############################################

		#############################################
		### Update user 'Title' data to 'TitleEnglish' and 'TitleChinese'  (Only Staff) [Start]
		#############################################
		if(!$GSary['UpdateUserTitle'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Update user 'Title' data to 'TitleEnglish' and 'TitleChinese' (Only Staff) [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$TitleE_ary = array("Mr.", "Miss", "Mrs.", "Ms.", "Dr.", "Prof.");
			include_once($PATH_WRT_ROOT."lang/lang.b5.ip20.php");
			$TitleC_ary = array($i_title_mr, $i_title_miss, $i_title_mrs, $i_title_ms, $i_title_dr, $i_title_prof);

			$sql = "select UserID, Title, TitleEnglish, TitleChinese from INTRANET_USER where (Title>=0 and Title<>'') and ((TitleEnglish='' or TitleEnglish is NULL) or (TitleChinese='' or TitleChinese is NULL)) and RecordType=1";
			$result = $li->returnArray($sql);

			foreach($result as $k=>$d)
			{
				list($thisUserID, $thisTitle, $thisTitleE, $thisTitleC) = $d;

				$con = array();
				if(trim($thisTitleE)=="")	$con[]= "TitleEnglish='". $TitleE_ary[$thisTitle]."'";	// only update TitleEnglish if TitleEnglish is empty
				if(trim($thisTitleC)=="")	$con[]= "TitleChinese='". $TitleC_ary[$thisTitle]."'";	// only update TitleChinese if TitleChinese is empty
				$con_str = implode(",",$con);

				$update_sql = "update INTRANET_USER set $con_str where UserID=$thisUserID";
				$li->db_db_query($update_sql);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'UpdateUserTitle', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "Update user 'Title' data to 'TitleEnglish' and 'TitleChinese' (Only Staff) <font color='blue'><b>Success</b></font><br>\r\n<br>\r\n";

			$x .= "===================================================================<br>\r\n";
			$x .= " Update user 'Title' data to 'TitleEnglish' and 'TitleChinese' [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		###  Update user 'Title' data to 'TitleEnglish' and 'TitleChinese'  (Only Staff) [End]
		#############################################

		#############################################
		### eEnrol - Copy Group Active Member Percentage to Club
		#############################################
		if (!$GSary['eEnrol_Copy_Active_Member_Percentage_From_Group_To_Club'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Copy Group Active Member Percentage to Club [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "Update
							INTRANET_ENROL_GROUPINFO as iegi
							Inner Join INTRANET_GROUP as ig
							On (iegi.GroupID = ig.GroupID)
					Set
							iegi.ActiveMemberPercentage = ig.ActiveMemberPercentage
					Where
							iegi.GroupID = ig.GroupID
					";
			$SuccessCopyActiveMemberPercentage = $li->db_db_query($sql);

			if ($SuccessCopyActiveMemberPercentage)
				$x .= "Copy Group Active Member Percentage to Club <font color='blue'><b>Success</b></font><br>\r\n";
			else
				$x .= "Copy Group Active Member Percentage to Club <font color='red'><b>Failed</b></font><br>\r\n";


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrol_Copy_Active_Member_Percentage_From_Group_To_Club', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "<b>eEnrol - Copy Group Active Member Percentage to Club [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################
		### eEnrol - Copy Group Active Member Percentage to Club end
		#############################################


		###################################################
		### Change file content for iPortfolio "Preset Participation Role" "/home/web/eclass40/eclass30/files//portfolio_olr_role.txt" [Start]
		###################################################
		$x .= "===================================================================<br>\r\n";
		$x .= "Change file content for iPortfolio 'Preset Participation Role'  [Start]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
		$temp_ary = array("b5", "en");


		$file_target = $eclass_filepath."/files/portfolio_olr_role.txt";
		$file_content = get_file_content($file_target);

		if(!empty($file_content))
		{
			// $lf = new libfilesystem();

			//$cur_encoding = mb_detect_encoding($file_content) ;
			//if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
			if(!(isUTF8($file_content) && mb_check_encoding($file_content,"UTF-8")))
			{
				$new_content = Big5ToUnicode($file_content);
				$lf->file_write($new_content,"$file_target");
				$x .=  $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>";
			}
			else
			{
				$x .=  $file_target .".... no need to convert to utf-8.<br>";
			}
		}
		else
		{
			$x .=  $file_target .".... file empty.<br>";
		}

		$x .= "===================================================================<br>\r\n";
		$x .= "Change file content for iPortfolio 'Preset Participation Role' [End]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
		###################################################
		### Change file content for iPortfolio "Preset Participation Role" "/home/web/eclass40/eclass30/files//portfolio_olr_role.txt" [End]
		###################################################

		###############################################
		# iPortfolio update ELE wording to all school #
		###############################################
		$x .= 'iPortfolio update ELE wording to all school<br>';
		if(!$GSary['iPf_udateELEComponent_CE']) {
			$sql = "update {$eclass_db}.OLE_ELE set ChiTitle='與工作有關的經驗' where DefaultID='[CE]'";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}

		# update General Settings - mark the script is executed
		$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPf_udateELEComponent_CE', 1, now())";
		$li->db_db_query($sql) or debug_pr(mysql_error());
		$x .= 'OK<br>';
		####################################################
		# iPortfolio update ELE wording to all school (end)#
		####################################################

		###################################################
		### Transfer User Info Setting data from admin console text file to database [Start]
		###################################################
		if (!$GSary['TransferUserInfoSettings'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Transfer User Info Setting data from admin console text file to database [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			// $lf = new libfilesystem();

			for($thisUserType=1; $thisUserType<=3; $thisUserType++)
			{
				if(file_exists($intranet_root."/file/user_info_settings_".$thisUserType.".txt"))
				{
					$data = array();
					$misc = $lf->file_read($intranet_root."/file/user_info_settings_".$thisUserType.".txt");
					$line = explode("\n",$misc);

					##### Personal Info
					$pi = explode(",",$line[0]);
					array_shift($pi);
					list($title, $nickname, $photo, $gender, $dob) = $pi;
					$data['CanUpdateNickName_'.$thisUserType] = $nickname ? 0 : 1;
					$data['CanUpdatePhoto_'.$thisUserType] = $photo ? 0 : 1;
					$data['CanUpdateGender_'.$thisUserType] = $gender ? 0 : 1;
					$data['CanUpdateDOB_'.$thisUserType] = $dob ? 0 : 1;

					##### Contact Info
					$ci = explode(",",$line[1]);
					array_shift($ci);
					list($hometel, $officetel, $mobile, $fax, $icq, $address, $country, $url, $email) = $ci;
					$data['CanUpdateHomeTel_'.$thisUserType] = $hometel ? 0 : 1;
					$data['CanUpdateOfficeTel_'.$thisUserType] = $officetel ? 0 : 1;
					$data['CanUpdateMobile_'.$thisUserType] = $mobile ? 0 : 1;
					$data['CanUpdateFax_'.$thisUserType] = $fax ? 0 : 1;
					$data['CanUpdateAddress_'.$thisUserType] = $address ? 0 : 1;
					$data['CanUpdateCountry_'.$thisUserType] = $country ? 0 : 1;
					$data['CanUpdateURL_'.$thisUserType] = $url ? 0 : 1;
					$data['CanUpdateEmail_'.$thisUserType] = $email ? 0 : 1;

					##### Message
					$msg = explode(",",$line[2]);
					array_shift($msg);
					$CanUpdateMessage = $msg[0];
					$data['CanUpdateMessage_'.$thisUserType] = $CanUpdateMessage ? 0 : 1;

					##### Password
					$pwd = explode(",",$line[3]);
					array_shift($pwd);
					$CanUpdatePassword = $pwd[0];
					$data['CanUpdatePassword_'.$thisUserType] = $CanUpdatePassword ? 0 : 1;

					if($thisUserType != 3)
					{
						##### Display
						$dis = explode(",",$line[4]);
						array_shift($dis);
						list($display_title, $display_email, $display_icq, $display_hometel, $display_fax, $display_dob, $display_address, $display_country) = $dis;
						$data['DisplayDOB_'.$thisUserType] = $display_dob ? 0 : 1;
						$data['DisplayHomeTel_'.$thisUserType] = $display_hometel ? 0 : 1;
						$data['DisplayFax_'.$thisUserType] = $display_fax ? 0 : 1;
						$data['DisplayAddress_'.$thisUserType] = $display_address ? 0 : 1;
						$data['DisplayCountry_'.$thisUserType] = $display_country ? 0 : 1;
						$data['DisplayEmail_'.$thisUserType] = $display_email ? 0 : 1;
					}

					$lgs->Save_General_Setting("UserInfoSettings", $data);
				}
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'TransferUserInfoSettings', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Transfer User Info Setting data from admin console text file to database [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

		}
		###################################################
		### Transfer User Info Setting data from admin console text file to database [End]
		###################################################

		###################################################
		### Copy offical_photo to user_photo [Start]
		###################################################
		if (!$GSary['CopyOfficialPhoto'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Copy offical_photo to user_photo [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			// $lf = new libfilesystem();

			$file_target = $intranet_root."/file/official_photo";
			$ipf_photos_ary = $lf->return_folderlist($file_target);

			$copy_count = 0;
			if(sizeof($ipf_photos_ary))
			{
				foreach($ipf_photos_ary as $k=>$ipf_photo)
				{
					$temp_pos = strrpos($ipf_photo, "/");
					$websams_no = "#".substr($ipf_photo, $temp_pos+1,-4);

					# retrieve student login
					$sql = "select UserLogin from INTRANET_USER where WebSAMSRegNo='$websams_no'";
					$thisUserLogin_ary = $li->returnVector($sql);
					$thisUserLogin = $thisUserLogin_ary[0];

					# check is student exists or not
					if($thisUserLogin=="")	continue;

					# check is there any latest photo in new photo path /file/user_photo
					# if no, then copy, otherwise skip
					$new_photo = $intranet_root."/file/user_photo/" . $thisUserLogin . ".jpg";
					if(file_exists($new_photo))	continue;

					# copy and rename file
					if($lf->file_copy($ipf_photo, $new_photo))
					{
						$PhotoLink = "/file/user_photo/" . $thisUserLogin . ".jpg";
						# update INTRANET_USER field PhotoLink
						$sql = "update INTRANET_USER set PhotoLink='$PhotoLink' where UserLogin='$thisUserLogin'";
						$li->db_db_query($sql);

						$copy_count++;
					}
				}
			}

			# clear photolink with IP20 iAccount photo
			$sql = "select UserID from INTRANET_USER where PhotoLink not like '/file/user_photo/%'";
			$result = $li->returnVector($sql);
			if(sizeof($result))
			{
				foreach($result as $k=>$d)
				{
					$sql2 = "update INTRANET_USER set PhotoLink='' where UserID='$d'";
					$li->db_db_query($sql2);
				}
			}

			$x .= "Copy offical_photo to user_photo <font color='blue'>". $copy_count ." photo(s) is/are copied</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CopyOfficialPhoto', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Copy offical_photo to user_photo [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Copy offical_photo to user_photo [End]
		###################################################

		###################################################
		### Sync. Photo Link data with user_photo [Start]
		###################################################
		if (!$GSary['SyncPhotoLink'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Sync. Photo Link data with user_photo [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			// $lf = new libfilesystem();

			$file_target = $intranet_root."/file/user_photo";
			$photos_ary = $lf->return_folderlist($file_target);

			foreach($photos_ary as $k=>$user_photo)
			{
				$temp_pos = strrpos($user_photo, "/");
				$thisUserLogin = substr($user_photo, $temp_pos+1,-4);

				$PhotoLink = "/file/user_photo/" . $thisUserLogin . ".jpg";
				# update INTRANET_USER field PhotoLink
				$sql = "update INTRANET_USER set PhotoLink='$PhotoLink' where UserLogin='$thisUserLogin'";
				$li->db_db_query($sql);
			}

			$x .= "Sync. Photo Link data with user_photo <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SyncPhotoLink', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Sync. Photo Link data with user_photo [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Sync. Photo Link data with user_photo [End]
		###################################################

		###################################################
		### Sync. Personal Photo Link data with photo/personal/ [Start]
		###################################################
		if (!$GSary['SyncPersonalPhotoLink'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Sync. Personal Photo Link data with photo/personal/ [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			// $lf = new libfilesystem();

			$file_target = $intranet_root."/file/photo/personal";
			$photos_ary = $lf->return_folderlist($file_target);

			foreach($photos_ary as $k=>$user_photo)
			{
				$temp_pos = strrpos($user_photo, "/");
				$thisUserID = substr($user_photo, $temp_pos+2,-4);
				$ext = substr($user_photo, -3);
				if(strtolower($ext)!="jpg")	continue;
				if($thisUserID=="") continue;

				$PhotoLink = "/file/photo/personal/p" . $thisUserID . ".".$ext;
				# update INTRANET_USER field PhotoLink
				$sql = "update INTRANET_USER set PersonalPhotoLink='$PhotoLink' where UserID='$thisUserID'";
				$li->db_db_query($sql);
			}

			$x .= "Sync. Personal Photo Link data with photo/personal <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SyncPersonalPhotoLink', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Sync. Photo Link data with user_photo [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Sync. Personal Photo Link data with photo/personal/ [End]
		###################################################

		###################################################
		### IES Scheme XML Synchronize [Start]
		###################################################
		$IS_IES_SYNC_ON = true;
		if (isset($plugin['IES']) && $plugin['IES'] && $IS_IES_SYNC_ON) {
			$x .= "===================================================================<br>\r\n";
			$x .= "IES Scheme XML Synchronize [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$env = $plugin['IES_ENV'];
			include_once($PATH_WRT_ROOT."includes/ies/libies.php");
			include_once($PATH_WRT_ROOT."includes/ies/libies_xml_sync.php");
			$libies_xml_sync = new libies_xml_sync();

			$now = date("YmdGis");
			$libies_xml_sync->writeLog("Start Synchronize [Time: {$now}]....");
			$mode = ini_get('zend.ze1_compatibility_mode');
			ini_set('zend.ze1_compatibility_mode', '0');

			$libdb = new libdb();

			$libdb->Start_Trans();
			$syncFinalResult = $libies_xml_sync->SyncScheme();
			if ($syncFinalResult) {
				$libies_xml_sync->writeLog("");
				$libies_xml_sync->writeLog("Updated IDs:");
				if (is_array($syncFinalResult)) {
					foreach($syncFinalResult as $finalKey => $finalElement) {
						$libies_xml_sync->writeLog($finalKey."[ID]: ");
						$libies_xml_sync->writeLog(implode(",",$finalElement));
					}
				}
				$libies_xml_sync->writeLog("");
				$x .= "Success<br/>\r\n";
				$libdb->Commit_Trans();
			} else {
//				$x .= "Some error pls check log<br/>\r\n";
				$x .= "Process completed pls check log file in [file/ies/log/scheme_sync_log_yyyymmdd.log]<br/>\r\n";
		    	$libdb->RollBack_Trans();
			}

			ini_set('zend.ze1_compatibility_mode', $mode);
			$now = date("YmdGis");
			$libies_xml_sync->writeLog("End Synchronize [Time: {$now}]....");

			$x .= "===================================================================<br>\r\n";
			$x .= "IES Scheme XML Synchronize [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES Scheme XML Synchronize [End]
		###################################################


		###################################################
		### IES Stage Weighting Patch Stage 1 and 2, 3 without en[Start]
		###################################################
		$IES_Weighting_Patch_1_2_3noEN = true;
		if (isset($plugin['IES']) && $plugin['IES'] && $IES_Weighting_Patch_1_2_3noEN && !$GSary['IES_Stage_Weighting_Patch_1_2_3noEN']) {
			$x .= "===================================================================<br>\r\n";
			$x .= " IES Stage Weighting Patch Stage 1 and 2, 3 without en [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";


			include_once($PATH_WRT_ROOT."home/eLearning/ies/admin/settings/stage_weight_patching.php");
			IES_WEIGHT_PATCH_1_2();


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_Stage_Weighting_Patch_1_2_3noEN', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " IES Stage Weighting Patch Stage 1 and 2, 3 without en [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES Stage Weighting  Stage 1 and 2, 3 without en [End]
		###################################################

		###################################################
		### IES Stage Weighting Patch Stage 3[Start]
		###################################################
		$IES_Weighting_Patch_3en = false;
		if (isset($plugin['IES']) && $plugin['IES'] && $IES_Weighting_Patch_3en && !$GSary['IES_Stage_Weighting_Patch_3']) {
			$x .= "===================================================================<br>\r\n";
			$x .= " IES Stage Weighting Patch Stage 3 [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";


			include_once($PATH_WRT_ROOT."home/eLearning/ies/admin/settings/stage_weight_patching_stage3en.php");
			IES_WEIGHT_PATCH_3();


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_Stage_Weighting_Patch_3', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " IES Stage Weighting Patch Stage 3 [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES Stage Weighting  Stage 3 [End]
		###################################################

		###################################################
		### IES - Import Default Comment Bank
		### 20101119 commented to avoid uploaded by others causing error due to not updated db schema
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_ImportDefaultCommentBank'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " IES - Import Default Comment Bank</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
			include_once($PATH_WRT_ROOT."includes/ies/importDefaultCommentBank.php");
			$ImportDefaultCommentBank = new ImportDefaultCommentBank();
			if ($ImportDefaultCommentBank->import()) {
				$x .= "Import Success<br>\r\n";
				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_ImportDefaultCommentBank', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			} else {
				$x .= "Import Failed<br>\r\n";
			}
			$x .= "Please check log file in [file/ies/log/import_default_comments_yyyymmdd.log]<br>\r\n";



			$x .= "===================================================================<br>\r\n";
			$x .= " IES - Import Default Comment Bank [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		###  Remove Duplicate User in UserGroup (Class Group) [End]
		###################################################

		###################################################
		### SBA - Import Default Comment Bank 20120911 [Start]
		###################################################
		if (count($plugin['SBA']) > 0 && !$GSary['SBA_ImportDefaultCommentBank'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " SBA - Import Default Comment Bank</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
			include_once($PATH_WRT_ROOT."includes/sba/importDefaultCommentBank.php");
			$ImportDefaultCommentBank = new ImportDefaultCommentBank();
			if ($ImportDefaultCommentBank->import()) {
				$x .= "Import Success<br>\r\n";
				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SBA_ImportDefaultCommentBank', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			} else {
				$x .= "Import Failed<br>\r\n";
			}
			$x .= "Please check log file in [file/sba/log/import_default_comments_yyyymmdd.log]<br>\r\n";



			$x .= "===================================================================<br>\r\n";
			$x .= " SBA - Import Default Comment Bank [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### SBA - Import Default Comment Bank 20120911 [End]
		###################################################

		###################################################
		### Remove Duplicate User in UserGroup (Class Group) [Start]
		###################################################
		if (!$GSary['RemoveDuplicateUserGroupUserOfAllGroup'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Remove Duplicate User in UserGroup (Class Group) [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "
				SELECT
					ig.GroupID,
					iug.UserID,
					MIN(iug.UserGroupID)
				FROM
					INTRANET_USERGROUP iug
					INNER JOIN INTRANET_GROUP ig ON ig.GroupID = iug.GroupID
				GROUP BY
					ig.GroupID, iug.UserID
				HAVING
					COUNT(UserGroupID) > 1
			";

			$UserArr = $li->returnArray($sql);

			foreach($UserArr as $thisUser)
			{
				list($thisGroupID, $thisUserID, $keepUserGroupID) = $thisUser;
				$sql = "
					UPDATE
						INTRANET_USERGROUP
					SET
						GroupID = GroupID + 10000000,
						UserID = UserID + 10000000
					WHERE
						GroupID = '$thisGroupID'
						AND UserID = '$thisUserID'
						AND UserGroupID <> '$keepUserGroupID'
				";

				$Success[$thisGroupID][$thisUserID] = $li->db_db_query($sql);
			}
			$x .= "Remove Duplicate User in UserGroup (Class Group) <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'RemoveDuplicateUserGroupUserOfAllGroup', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Remove Duplicate User in UserGroup (Class Group) [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		###  Remove Duplicate User in UserGroup (Class Group) [End]
		###################################################

		###################################################
		### Copy Group Admin Right [Start]
		###################################################
		if (!$GSary['CopyGroupAdminRight'])
		{
			$PreviousAcademicYearID = Get_Previous_Academic_Year_ID();
			$AcademicYearID = Get_Current_Academic_Year_ID();

			$x .= "===================================================================<br>\r\n";
			$x .= " Copy Group Admin Right [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$Result = array();

			$sql = "
				SELECT
					ig.GroupID,
					ig.Title
				FROM
					INTRANET_GROUP ig
					INNER JOIN INTRANET_USERGROUP iug ON ig.GroupID = iug.GroupID
				WHERE
					AcademicYearID = '$AcademicYearID'
				GROUP BY
					ig.GroupID
				HAVING
					MAX(iug.RecordType) IS NULL
					OR TRIM(MAX(iug.RecordType)) = ''
			";

			$NewGroupList = $li->returnArray($sql);

			$sql = "
				SELECT
					GroupID,
					Title
				FROM
					INTRANET_GROUP
				WHERE
					AcademicYearID = '$PreviousAcademicYearID'
			";
			$OldGroupList = $li->returnArray($sql);

			$NewGroupList = BuildMultiKeyAssoc($NewGroupList,"Title","GroupID",1);

			for($i=0; $i<sizeof($OldGroupList); $i++)
			{
				list($OldGroupID, $Title) = $OldGroupList[$i];
				$NewGroupID = $NewGroupList[$Title];
				if($NewGroupID=='')
					continue;

				$txt .= "$Title: $OldGroupID > $NewGroupID \n";
				$sql = "
					SELECT
						GroupID,
						UserID,
						AdminAccessRight,
						RecordType
					FROM
						INTRANET_USERGROUP
					WHERE
						GroupID = '$OldGroupID'
						AND
						(
							(
								AdminAccessRight <> 0
							)
							OR
							(
								RecordType <> ''
								AND RecordType IS NOT NULL
							)
						)

				";
				$tmp = $li->returnArray($sql);

				if(!empty($tmp))
				{
					$GroupUserRight[$NewGroupID] = BuildMultiKeyAssoc($tmp,"UserID",array("AdminAccessRight","RecordType"));
					$txt .= implode(",",(array)Get_Array_By_Key($tmp,"UserID"))." \n";
				}
				else
					$txt .= "No Admin In Old Group \n\n";

			}

			foreach((array)$GroupUserRight as $NewGroupID => $UserRight)
			{
				foreach((array)$UserRight as $thisUserID => $Right)
				{
					$SetSqlArr = array();
					if(!empty($Right['AdminAccessRight']))
						$SetSqlArr[] = "AdminAccessRight = '".$Right['AdminAccessRight']."'";
					if(!empty($Right['RecordType']))
						$SetSqlArr[] = "RecordType = '".$Right['RecordType']."'";

					$SetSql = implode(",",$SetSqlArr);

					$sql = "
						UPDATE
							INTRANET_USERGROUP
						SET
							$SetSql
						WHERE
							GroupID = '$NewGroupID'
							AND UserID = '$thisUserID'
					";

					 $success = $li->db_db_query($sql);
					 $Result[] = $success;

					if(!$success)
						$txt .= "FAIL!!!!!!!!!!!!!!!!!\n$sql\n\n";
					else
						$txt .= "Success!!!!!!!!!!!!!!!!!\n\n";
				}
			}

			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$lfile = new libfilesystem();
			$success = $lfile->file_write($txt,$PATH_WRT_ROOT."file/eCommCopyAdminInIP25Index.txt");

			$x .= "Copy Group Admin Right (Class Group) <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CopyGroupAdminRight', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Copy Group Admin Right [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		###  Copy Group Admin Right [End]
		###################################################

		###################################################
		### Sync Group ID for New School Years' Group [Start]
		###################################################
		if (!$GSary['SyncGroupID'])
		{
			$PreviousAcademicYearID = Get_Previous_Academic_Year_ID();
			$AcademicYearID = Get_Current_Academic_Year_ID();

			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			$lfile = new libfilesystem();

			$x .= "===================================================================<br>\r\n";
			$x .= " Sync Group ID for New School Years' Group [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$Result = array();
			$sql = "
				SELECT
					GroupID,
					Title
				FROM
					INTRANET_GROUP
				WHERE
					AcademicYearID = '$AcademicYearID'
			";
			$NewGroupList = $li->returnArray($sql);

			$sql = "
				SELECT
					GroupID,
					Title
				FROM
					INTRANET_GROUP
				WHERE
					AcademicYearID = '$PreviousAcademicYearID'
			";
			$OldGroupList = $li->returnArray($sql);

			$NewGroupList = BuildMultiKeyAssoc($NewGroupList,"Title","GroupID",1);

			for($i=0; $i<sizeof($OldGroupList); $i++)
			{
				list($GroupID, $Title) = $OldGroupList[$i];
				if($NewGroupList[$Title]=='')
					continue;

				$GroupIDMapping[$GroupID] = $NewGroupList[$Title];
			}

			$eCommRelatedTable = array(
				"INTRANET_FILE",
				"INTRANET_FILE_FOLDER",
				"INTRANET_GROUPANNOUNCEMENT",
				"INTRANET_BULLETIN",
				"INTRANET_ANNOUNCEMENT",
				"INTRANET_HOUSE",
				"INTRANET_GROUPRESOURCE"
			);

			foreach((array)$GroupIDMapping as $OldGroupID => $NewGroupID)
			{
				$FailSql = '';
				foreach($eCommRelatedTable as $table)
				{
					if($table == 'INTRANET_ANNOUNCEMENT')
						$GroupID = 'OwnerGroupID';
					else
						$GroupID = 'GroupID';

					$sql = "
						UPDATE
							$table
						SET
							$GroupID = '$NewGroupID'
						WHERE
							$GroupID = '$OldGroupID'
					";

					if($Result[$OldGroupID."->".$NewGroupID] = $li->db_db_query($sql))
						$AffectedRow++;
					else
						$FailSql .= "Failed:\n".$sql."\n";
				}

				$old_file_path = $PATH_WRT_ROOT."file/group/g$OldGroupID";
				if(file_exists($old_file_path))
				{
					$new_file_path = $PATH_WRT_ROOT."file/group/g$NewGroupID";

					if(file_exists($new_file_path))
						$lfile->folder_content_copy($old_file_path,$new_file_path);
					else
						rename($old_file_path, $new_file_path);
				}

				$txt.= "$OldGroupID => $NewGroupID\n";
				if($FailSql)
					$txt.= $FailSql;

			}


			$success = $lfile->file_write($txt,$PATH_WRT_ROOT."file/eCommConvInScript.txt");

			$x .= "Sync Group ID for New School Years' Group <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SyncGroupID', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Sync Group ID for New School Years' Group [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		###  Sync Group ID for New School Years' Group  [End]
		###################################################

		###################################################
		### Fix Wrong Teaching In INTRANET_USER [Start]
		###################################################
		if (!$GSary['FixStaffTeaching'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong Teaching In INTRANET_USER [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "UPDATE INTRANET_USER SET Teaching=0 WHERE RecordType=1 AND (Teaching <> 1 OR Teaching IS NULL)";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "Fix Wrong Teaching In INTRANET_USER <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'FixStaffTeaching', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong Teaching In INTRANET_USER [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Fix Wrong Teaching In INTRANET_USER  [End]
		###################################################


		###############################################################
		### Fix Wrong USERGROUP of staff In INTRANET_USERGROUP [Start]
		###############################################################
		if (!$GSary['FixStaffInUserGroup_20110822'])
		{
			// previous update : FixStaffInUserGroup, FixStaffInUserGroup_20101007

			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong USERGROUP of staff In INTRANET_USERGROUP [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# change Teaching group to -999
			$sql = "UPDATE INTRANET_USERGROUP SET GroupID=-999 WHERE GroupID=1";
			$li->db_db_query($sql);
			# change Non-Teaching group to -1000
			$sql = "UPDATE INTRANET_USERGROUP SET GroupID=-1000 WHERE GroupID=3";
			$li->db_db_query($sql);

			# teaching staff
			$sql = "SELECT UserID FROM INTRANET_USER WHERE Teaching=1 AND RecordType=1 AND RecordStatus=1";
			$result = $li->returnVector($sql);
			if(sizeof($result)>0) {
				for($i=0; $i<sizeof($result); $i++) {
					$uid = $result[$i];
					# remove user from Non-Teaching group
					$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID=-1000 AND UserID='$uid'";
					$li->db_db_query($sql);

					$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE GroupID=-999 AND UserID='$uid'";
					$temp = $li->returnVector($sql);

					if($temp[0]==0) {
						# insert user in Teaching group
						$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES (1,'$uid', NOW(), NOW())";
						$li->db_db_query($sql);
					} else {
						# update user in Teaching group
						$sql = "UPDATE INTRANET_USERGROUP SET GroupID=1 WHERE GroupID=-999 AND UserID='$uid'";
						$li->db_db_query($sql);
					}
				}
			}
			# non-teaching staff
			$sql = "SELECT UserID FROM INTRANET_USER WHERE Teaching!=1 AND RecordType=1 AND RecordStatus=1";
			$result = $li->returnVector($sql);
			if(sizeof($result)>0) {
				for($i=0; $i<sizeof($result); $i++) {
					$uid = $result[$i];
					# remove user from Non-Teaching group
					$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID=-999 AND UserID='$uid'";
					$li->db_db_query($sql);

					$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE GroupID=-1000 AND UserID='$uid'";
					$temp = $li->returnVector($sql);

					if($temp[0]==0) {
						# insert user in Teaching group
						$sql = "INSERT INTO INTRANET_USERGROUP (GroupID, UserID, DateInput, DateModified) VALUES (3,'$uid', NOW(), NOW())";
						$li->db_db_query($sql);
					} else {
						# update user in Teaching group
						$sql = "UPDATE INTRANET_USERGROUP SET GroupID=3 WHERE GroupID=-1000 AND UserID='$uid'";
						$li->db_db_query($sql);
					}
				}
			}
			# delete the rest of records in INTRAN_USERGROUP if not Teaching/Non-Teaching staff
			$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID IN (-999,-1000)";
			$li->db_db_query($sql);

			$x .= "Fix Wrong USERGROUP of staff In INTRANET_USERGROUP <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'FixStaffInUserGroup_20110822', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong USERGROUP of staff In INTRANET_USERGROUP [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################################
		### Fix Wrong USERGROUP of staff In INTRANET_USERGROUP  [End]
		#############################################################

		###############################################################
		### Fix Wrong USERGROUP of Parent In INTRANET_USERGROUP [Start]
		###############################################################
		if (!$GSary['FixParentInUserGroup_20101007'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong USERGROUP of parent In INTRANET_USERGROUP [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# change parent group (4) to -9999
			$sql = "UPDATE INTRANET_USERGROUP SET GroupID=-9999 WHERE GroupID=4";
			$li->db_db_query($sql);

			$str = "";

			# checking on parent management
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=3 AND RecordStatus=1";
			$result = $li->returnVector($sql);
			if(sizeof($result)>0) {
				for($i=0; $i<sizeof($result); $i++) {
					$uid = $result[$i];
					$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE GroupID=-9999 AND UserID='$uid'";
					$count = $li->returnVector($sql);

					if($count[0]==0) {	# no existing group member record, then insert
						$sql = "INSERT INTO INTRANET_USERGROUP SET GroupID=4, UserID='$uid', DateInput=NOW(), DateModified=NOW()";
						$li->db_db_query($sql);

					} else {			# has existing group member record, then update
						$sql = "UPDATE INTRANET_USERGROUP SET GroupID=4 WHERE GroupID=-9999 AND UserID='$uid'";
						$li->db_db_query($sql);
					}
				}
			}

			$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID=-9999";
			$li->db_db_query($sql);

			$x .= "Fix Wrong USERGROUP of Parent In INTRANET_USERGROUP <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'FixParentInUserGroup_20101007', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong USERGROUP of Parent In INTRANET_USERGROUP [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################################
		### Fix Wrong USERGROUP of parent In INTRANET_USERGROUP  [End]
		#############################################################

		###############################################################
		### Fix Wrong USERGROUP of Student In INTRANET_USERGROUP [Start]
		###############################################################
		if (!$GSary['FixStudentInUserGroup_20101007'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong USERGROUP of Student In INTRANET_USERGROUP [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# change parent group (4) to -9999
			$sql = "UPDATE INTRANET_USERGROUP SET GroupID=-9999 WHERE GroupID=2";
			$li->db_db_query($sql);

			$str = "";

			# checking on parent management
			$sql = "SELECT UserID FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus=1";
			$result = $li->returnVector($sql);
			if(sizeof($result)>0) {
				for($i=0; $i<sizeof($result); $i++) {
					$uid = $result[$i];
					$sql = "SELECT COUNT(*) FROM INTRANET_USERGROUP WHERE GroupID=-9999 AND UserID='$uid'";
					$count = $li->returnVector($sql);

					if($count[0]==0) {	# no existing group member record, then insert
						$sql = "INSERT INTO INTRANET_USERGROUP SET GroupID=2, UserID='$uid', DateInput=NOW(), DateModified=NOW()";
						$li->db_db_query($sql);

					} else {			# has existing group member record, then update
						$sql = "UPDATE INTRANET_USERGROUP SET GroupID=2 WHERE GroupID=-9999 AND UserID='$uid'";
						$li->db_db_query($sql);
					}
				}
			}

			$sql = "DELETE FROM INTRANET_USERGROUP WHERE GroupID=-9999";
			$li->db_db_query($sql);

			$x .= "Fix Wrong USERGROUP of Student In INTRANET_USERGROUP <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'FixStudentInUserGroup_20101007', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Fix Wrong USERGROUP of Student In INTRANET_USERGROUP [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#############################################################
		### Fix Wrong USERGROUP of Student In INTRANET_USERGROUP  [End]
		#############################################################

		######################################################################################################################
		### Update YearOfLeft in INTRANET_USER if RecordType=2 (student) & RecordStatus=3 (Left) & YearOfLeft Is NULL [Start]
		######################################################################################################################
		if (!$GSary['FixYearOfLeft'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Update YearOfLeft in INTRANET_USER for Left Student [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT UserID, YEAR(DateModified) as YearOfLeft FROM INTRANET_USER WHERE RecordType=2 AND RecordStatus=3 AND (YearOfLeft IS NULL OR YearOfLeft='')";
			$result = $li->returnArray($sql,2);

			for($i=0; $i<sizeof($result); $i++) {
				list($id, $year) = $result[$i];
				$sql = "UPDATE INTRANET_USER SET YearOfLeft='$year' WHERE UserID='$id' AND RecordType=2 AND RecordStatus=3 AND (YearOfLeft IS NULL OR YearOfLeft='')";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			$x .= "Fix Wrong Teaching In INTRANET_USER <font color='blue'>Success</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'FixYearOfLeft', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Update YearOfLeft in INTRANET_USER for Left Student [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		######################################################################################################################
		### Update YearOfLeft in INTRANET_USER if RecordType=2 (student) & RecordStatus=3 (Left) & YearOfLeft Is NULL [End]
		######################################################################################################################

		######################################################################################################################
		### Update Student attendance Daily log add last tap card time record [Start]
		######################################################################################################################
		if ($GSary['StudentAttendanceLastTapCardField'] != 1 && $plugin['attendancestudent']) {
			echo 'Start on StudentAttendanceLastTapCardField<br>';

			$sql = "show tables like 'CARD_STUDENT_DAILY_LOG_%'";
			$DailyLog = $li->returnVector($sql);

			for ($i=0; $i< sizeof($DailyLog); $i++) {
				$sql = 'alter table '.$DailyLog[$i].' add LastTapCardTime time default NULL';
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			unset($DailyLog);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING
								(Module, SettingName, SettingValue, DateInput)
							values
								('$ModuleName', 'StudentAttendanceLastTapCardField', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			echo 'end of StudentAttendanceLastTapCardField';
		}
		######################################################################################################################
		### Update Student attendance Daily log add last tap card time record [End]
		######################################################################################################################

		###################################################
		### Copy user_photo to photo/personal(staff and parent only) [Start]
		###################################################
		if (!$GSary['CopyOfficialPhotoToPersonal_StaffParentOnly'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Copy user_photo to photo/personal(staff and parent only) [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			// $lf = new libfilesystem();

			$file_from =  $intranet_root."/file/user_photo";
			$file_to = $intranet_root."/file/photo/personal";

			$sql = "select UserID, UserLogin from INTRANET_USER where RecordType=1 or RecordType=3";
			$thisUserLogin_ary = $li->returnArray($sql);

			$copy_count = 0;
			if(sizeof($thisUserLogin_ary))
			{
				foreach($thisUserLogin_ary as $k=>$d)
				{
					list($thisUserID, $thisUserLogin) = $d;

					$this_photo = $file_from ."/". $thisUserLogin.".jpg";
					## check the photo is exists or not
					if(file_exists($this_photo))
					{
						# copy to photo/personal
						$to_photo = $file_to ."/p".$thisUserID.".jpg";
						if($lf->file_copy($this_photo, $to_photo))
						{
							$PersonalPhotoLink = "/file/photo/personal/p" . $thisUserID . ".jpg";
							# update INTRANET_USER field PhotoLink and PersonalPhotoLink
							$sql = "update INTRANET_USER set PhotoLink='', PersonalPhotoLink='$PersonalPhotoLink' where UserLogin='$thisUserLogin'";
							$li->db_db_query($sql);

							$copy_count++;

							# remove photo in user_photo
							$lf->file_remove($this_photo);
						}
					}
				}
			}

			$x .= "Copy user_photo to photo/personal(staff and parent only) <font color='blue'>". $copy_count ." photo(s) is/are copied</font>.<br><br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CopyOfficialPhotoToPersonal_StaffParentOnly', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Copy user_photo to photo/personal(staff and parent only) [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Copy user_photo to photo/personal(staff and parent only) [End]
		###################################################

		###################################################
		### Remove Temp Deleted UserGroup User[Start]
		###################################################
		if (!$GSary['RemoveTempDeletedUserGroupUser'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Remove Temp Deleted UserGroup User [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# remove soft deleted UserGroup User
			$sql = "DELETE FROM INTRANET_USERGROUP WHERE UserID > 10000000 AND GroupID > 10000000";
			$Success["RemoveUser"] = $li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			# Select User whose UserGroupID will be crashed after - 10000000.
			$sql = "SELECT b.UserGroupID FROM INTRANET_USERGROUP a INNER JOIN INTRANET_USERGROUP b ON a.UserGroupID = b.UserGroupID -10000000";
			$CrashedUserList = $li->returnVector($sql);
			if(count($CrashedUserList)>0)
			{
				$UserSql = implode(',',$CrashedUserList);
				$cond = " AND UserGroupID NOT IN ($UserSql) ";
			}

			# Fix UserGroupID except crashed User.
			$sql = "UPDATE INTRANET_USERGROUP SET UserGroupID = UserGroupID - 10000000 WHERE UserGroupID > 10000000 $cond ";
			$Success["FixID"] = $li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			# Get Max UserGroupID (use to reset AutoIncrement)
			$sql = "SELECT MAX(UserGroupID) FROM INTRANET_USERGROUP WHERE UserGroupID < 10000000";
			$tmp = $li->returnVector($sql);
			$MaxID = $tmp[0];

			# Append crashed users at the end of record
			foreach((array)$CrashedUserList as $k => $CrashedUserGroupID)
			{
				$sql = "UPDATE INTRANET_USERGROUP SET UserGroupID = ".(++$MaxID)." WHERE UserGroupID = '$CrashedUserGroupID'";
				$Success["HandleCrashedUser"][] = $li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
			}

			# Reset Auto Increment
			$sql = "ALTER TABLE INTRANET_USERGROUP AUTO_INCREMENT = ".($MaxID+1);
			$Success["ResetAutoIncrement"] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "Remove Temp Deleted UserGroup User <font color='blue'>Success</font>.<br><br>\r\n";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'RemoveTempDeletedUserGroupUser', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Remove Temp Deleted UserGroup User [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Remove Temp Deleted UserGroup User [End]
		###################################################


		###################################################
		### Remove Removed Classroom Calendar[Start]
		###################################################
		if (!$GSary['RemoveRemovedClassroomCalendar'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Remove Removed Classroom Calendar [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "select distinct v.Calid from CALENDAR_CALENDAR_VIEWER as v where v.groupType = 'C' and
				v.calid not in (
				select distinct c.calid from eclass.course as c where c.calid is not null
				)";
				$r = $li->returnVector($sql);
				if (!empty($r)){
					$cal_sql = implode("','",$r);
					$sql = "delete from CALENDAR_EVENT_ENTRY where Calid in ('$cal_sql') ";
					$li->db_db_query($sql);
					$sql = "delete from CALENDAR_CALENDAR where Calid in ('$cal_sql') ";
					$li->db_db_query($sql);
					$sql = "delete from CALENDAR_CALENDAR_VIEWER where Calid in ('$cal_sql') ";
					$li->db_db_query($sql);
				}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'RemoveRemovedClassroomCalendar', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Remove Removed Classroom Calendar [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Remove Removed Classroom Calendar [End]
		###################################################

		###################################################
		### IES: Add default marking criteria [Start]
		###################################################
		if (!$GSary['IES_AddDefaultMarkCriteria'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Add default marking criteria [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "INSERT IGNORE INTO IES_COMBO_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES (1, '總分', 'Score', 1, NOW(), 1, 1)";
			$li->db_db_query($sql);

			$sql = "INSERT IGNORE INTO IES_COMBO_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES (21, '過程', 'Process', 1, NOW(), 1, 1)";
			$li->db_db_query($sql);

			$sql = "INSERT IGNORE INTO IES_COMBO_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES (22, '課業', 'Task', 1, NOW(), 1, 1)";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_AddDefaultMarkCriteria', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Add default marking criteria [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES: Add default marking criteria [End]
		###################################################

		###################################################
		### IES: Map default marking criteria with stage [Start]
		###################################################
		if (!$GSary['IES_MapDefaultStageCriteria'])
		{

			if(count($plugin['SBA']) > 0) {
				//since SBA share table with IES , for safe , skip this update if client with SBA
				// do nothing
			}else{

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Map default marking criteria with stage [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				$sql = "SELECT StageID FROM IES_STAGE";
				$stage_id_arr = $li->returnVector($sql);

				$sql = "SELECT MarkCriteriaID FROM IES_MARKING_CRITERIA";
				$mark_criteria_id_arr = $li->returnVector($sql);

				for($i=0, $i_max=count($stage_id_arr); $i<$i_max; $i++)
				{
				  for($j=0, $j_max=count($mark_criteria_id_arr); $j<$j_max; $j++)
				  {
					$sql = "INSERT IGNORE INTO IES_STAGE_MARKING_CRITERIA (StageID, MarkCriteriaID, DateInput, InputBy, ModifyBy) ";
					$sql .= "VALUES ({$stage_id_arr[$i]}, {$mark_criteria_id_arr[$j]}, NOW(), 1, 1)";
					$li->db_db_query($sql);
				  }
				}

				$sql = "UPDATE IES_MARKING im ";
				$sql .= "INNER JOIN IES_STAGE_HANDIN_BATCH ishb ON im.AssignmentID = ishb.BatchID AND im.AssignmentType = 2 ";
				$sql .= "INNER JOIN IES_STAGE_MARKING_CRITERIA ismc ON ishb.StageID = ismc.StageID AND im.MarkCriteria = ismc.MarkCriteriaID ";
				$sql .= "SET im.StageMarkCriteriaID = ismc.StageMarkCriteriaID";
				$li->db_db_query($sql);

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_MapDefaultStageCriteria', 1, now())";
				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Map default marking criteria with stage [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}
		}
		###################################################
		### IES: Map default marking criteria with stage [End]
		###################################################

		###################################################
		### IES: Map default marking criteria with stage [Start] (for some stage missing init before ,mainly handle for eng version that do not have stage 2 and stage 3 before)
		###################################################
		if (!$GSary['IES_MapDefaultStageCriteriaForMissingSetting'])
		{

			if(count($plugin['SBA']) > 0) {
				//since SBA share table with IES , for safe , skip this update if client with SBA
				// do nothing
			}else{

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Map default marking criteria with stage [Start] (for some stage missing init before ,mainly handle for eng version that do not have stage 2 and stage 3 before)</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				//find all stage id that do not have any marking setting before in IES_STAGE_MARKING_CRITERIA --> "IES_STAGE_MARKING_CRITERIA.stageid is null"
				$sql ="select s.StageID as `STAGEID` from IES_STAGE as s left join IES_STAGE_MARKING_CRITERIA as c on c.StageID = s.StageID where c.stageid is null";

				$resultSet = $li->returnArray($sql);

				for($i = 0,$i_max = sizeof($resultSet);$i< $i_max; $i++){
					$_stageID = $resultSet[$i]["STAGEID"];

					$sql = "INSERT IGNORE INTO {$intranet_db}.IES_STAGE_MARKING_CRITERIA (StageID, MarkCriteriaID, MaxScore, Weight, DateInput, InputBy, ModifyBy) ";
					$sql .= "SELECT {$_stageID}, MarkCriteriaID, defaultMaxScore, defaultWeight, NOW(), 0, 0 FROM {$intranet_db}.IES_MARKING_CRITERIA";

					$li->db_db_query($sql);
				}

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_MapDefaultStageCriteriaForMissingSetting', 1, now())";
				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Map default marking criteria with stage [End] (for some missing init before ,mainly handle for eng version that do not have stage 2 and stage 3 before)</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}
		}
		###################################################
		### IES: Map default marking criteria with stage [End] (for some stage missing init before ,mainly handle for eng version that do not have stage 2 and stage 3 before)
		###################################################

		###################################################
		### IES: Create classroom for rubric [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_CreateClassroom'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Create classroom for rubric [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			define("CLASS_ROOM_NOT_EXIST", 0);

			include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $ies_cfg['DB_course_RoomType'];

			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) == CLASS_ROOM_NOT_EXIST))
			{
				$max_user = "NULL";
				$max_storage = "NULL";
				$course_code = $ies_cfg["moduleCode"];
				$course_name = $ies_cfg["moduleCode"];
				$course_desc = "eClass classroom for Module {$course_code}";

				$lo = new libeclass();

				$lo->setRoomType($roomType);

				$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);

				$sql = "UPDATE {$eclass_db}.course SET is_guest = 'yes' WHERE course_id = '{$course_id}'";
				$li->db_db_query($sql);

				$subj_id = -9; // $subj_id may not usefull
				$lo->eClassSubjectUpdate($subj_id, $course_id);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_CreateClassroom', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Create classroom for rubric [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES: Create classroom for rubric [End]
		###################################################

		###################################################
		### IES: Set default survey template [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_DEFAULT_SURVEY_TEMPLATE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Set Default Survey Question Template [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";


			include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");


				include_once($PATH_WRT_ROOT."includes/ies/initSettings/initDefaultSurveyTemplateSQL.php"); //<-- this line must be under

				$_sqlArray = $iesInitSQL['surveyDefaultQuestion'];

				for($i = 0,$i_max = count($_sqlArray);$i < $i_max;$i++){
					$_sql = $_sqlArray[$i];
					$li->db_db_query($_sql);
				}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_DEFAULT_SURVEY_TEMPLATE', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Set Default Survey Question Template [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES: Set default survey template [End]
		###################################################


		###################################################
		### IES: Set default rubric [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_ClassroomRubric'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Set default rubric [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			define("CLASS_ROOM_NOT_EXIST", 0);

			include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $ies_cfg['DB_course_RoomType'];

			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) != CLASS_ROOM_NOT_EXIST))
			{

				$classRoomDB = classNamingDB($classRoomID);

				include_once($PATH_WRT_ROOT."includes/ies/initSettings/initSQL.php"); //<-- this line must be under $classRoomDB = classNamingDB($classRoomID); , a global variable $classRoomDB is needed

				$sql = "TRUNCATE TABLE {$classRoomDB}.standard_rubric_set";
				$li->db_db_query($sql);
				$sql = "TRUNCATE TABLE {$classRoomDB}.standard_rubric";
				$li->db_db_query($sql);
				$sql = "TRUNCATE TABLE {$classRoomDB}.standard_rubric_detail";
				$li->db_db_query($sql);

				$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_set WRITE";
				$li->db_db_query($sql);
				$sql = $iesInitSQL['rubric']['sql1_stage'];

				$li->db_db_query($sql);
				$sql = "UNLOCK TABLES";
				$li->db_db_query($sql);

				$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
				$li->db_db_query($sql);
				$sql = $iesInitSQL['rubric']['sql2_stage'];

				$li->db_db_query($sql);
				$sql = "UNLOCK TABLES";
				$li->db_db_query($sql);

				$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
				$li->db_db_query($sql);
				$sql = $iesInitSQL['rubric']['stage_detail1'];

				$li->db_db_query($sql);
				$sql = "UNLOCK TABLES";
				$li->db_db_query($sql);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_ClassroomRubric', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Set default rubric [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES: Set default rubric [End]
		###################################################


		###################################################
		### IES: Add default teacher account to classroom [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_RubricManager'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Add classroom teacher account to manage rubric [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			define("CLASS_ROOM_NOT_EXIST", 0);

			include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $ies_cfg['DB_course_RoomType'];

			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) != CLASS_ROOM_NOT_EXIST))
			{
				$lo = new libeclass($classRoomID);
				$lo->setRoomType($roomType);

				$lo->eClassUserAddFullInfo($ies_cfg['DB_user_course_user_email']["rubric_manager"], "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "T", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL");
				$lo->eClassUserNumber($lo->course_id);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_RubricManager', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Add classroom teacher account to manage rubric [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES: Add default teacher account to classroom [End]
		###################################################

		############################################################
		# Patching OLE Program Last Modifier [Start]
		############################################################
		if(!$GSary['iPortfolioPatchOLEModifyBy'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Patching OLE Program Last Modifier [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "UPDATE {$eclass_db}.OLE_PROGRAM SET ModifyBy = CreatorID";
			$li->db_db_query($sql);

			//check have run the patch before
			// select * from GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
			//rerun the patching
			//delete from  GENERAL_SETTING  where settingname = 'iPortfolioPatchOLEAYearID';
			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioPatchOLEModifyBy', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Patching OLE Program Last Modifier [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

		}
		############################################################
		# Patching OLE Program Last Modifier [End]
		############################################################

		###################################################
		### IES: Patch default criteria weight [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_PatchDefaultCriteriaWeight'])
		{

			if(count($plugin['SBA']) > 0) {
				//since SBA share table with IES , for safe , skip this update if client with SBA
				// do nothing
			}else{

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Patch default criteria weight [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");

				$sql = "UPDATE IES_MARKING_CRITERIA ";
				$sql .= "SET defaultWeight = 0.5 WHERE MarkCriteriaID <> 1";
				$li->db_db_query($sql);

				$sql = "UPDATE IES_STAGE_MARKING_CRITERIA ismc ";
				$sql .= "INNER JOIN IES_MARKING_CRITERIA imc ON ismc.MarkCriteriaID = imc.MarkCriteriaID ";
				$sql .= "SET ismc.Weight = imc.defaultWeight";
				$li->db_db_query($sql);

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_PatchDefaultCriteriaWeight', 1, now())";
				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Patch default criteria weight [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}
		}
		###################################################
		### IES: Patch default criteria weight [End]
		###################################################

		###################################################
		### SBA: Set default survey question [Start]
		###################################################
		if (count($plugin['SBA']) > 0 && !$GSary['SBA_DEFAULT_SURVEY_TEMPLATE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "SBA: Set Default Survey Question Template [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";


			include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");


			include_once($PATH_WRT_ROOT."includes/sba/initSettings/initDefaultSurveyTemplateSQL.php"); //<-- this line must be under

				$_sqlArray = $sbaInitSQL['surveyDefaultQuestion'];

				for($i = 0,$i_max = count($_sqlArray);$i < $i_max;$i++){
					$_sql = $_sqlArray[$i];

					$li->db_db_query($_sql);
				}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SBA_DEFAULT_SURVEY_TEMPLATE', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "SBA: Set Default Survey Question Template [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### SBA: Set default survey question [End]
		###################################################


		#######################################################################################
		### Subject Group - Initialize LockedYearClassID Field from InternalClassCode [Start]
		#######################################################################################
		if (!$GSary['SubjectGroup_LockedYearClassID_Initialize'] && $sys_custom['SubjectGroup']['LockLogic']==true)
		{
			$x .= "======================================================================================<br>\r\n";
			$x .= "Subject Group - Initialize LockedYearClassID Field from InternalClassCode [Start]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";

			### Create a Backup Table First for safety
			$sql = "CREATE TABLE IF NOT EXISTS SUBJECT_TERM_CLASS_BACKUP SELECT * FROM SUBJECT_TERM_CLASS";
			$li->db_db_query($sql);


			### Get all Subject Groups which are created from a Class
			$sql = "Select SubjectGroupID, InternalClassCode From SUBJECT_TERM_CLASS Where InternalClassCode != '' And InternalClassCode Is Not Null";
			$SubjectGroupInfoArr = $li->returnArray($sql);
			$numOfSubjectGroup = count($SubjectGroupInfoArr);

			$LockedYearClassIDSuccessArr = array();
			for ($SubjectGroupCount=0; $SubjectGroupCount<$numOfSubjectGroup; $SubjectGroupCount++)
			{
				$thisSubjectGroupID = $SubjectGroupInfoArr[$SubjectGroupCount]['SubjectGroupID'];
				$thisInternalClassCode = $SubjectGroupInfoArr[$SubjectGroupCount]['InternalClassCode'];

				// $thisInternalClassCode = $AcademicYearID.'-'.$YearTermID.'-'.$SubjectID.'-'.$YearID.'-'.$YearClassID;
				$thisClassCodeArr = explode('-', $thisInternalClassCode);
				$thisLockedYearClassID = $thisClassCodeArr[4];

				if ($thisLockedYearClassID != '')
				{
					$sql = "Update SUBJECT_TERM_CLASS set LockedYearClassID = '".$thisLockedYearClassID."' Where SubjectGroupID = '".$thisSubjectGroupID."'";
					$LockedYearClassIDSuccessArr[$thisSubjectGroupID] = $li->db_db_query($sql);

					if ($LockedYearClassIDSuccessArr[$thisSubjectGroupID])
						$x .= "Subject Group $thisSubjectGroupID ($thisInternalClassCode) lock with Class $thisLockedYearClassID <font color='blue'><b>Success</b></font>.<br>\r\n";
					else
						$x .= "Subject Group $thisSubjectGroupID ($thisInternalClassCode) lock with Class $thisLockedYearClassID <font color='red'><b>Failed</b></font>.<br>\r\n";
				}
			}

			if (!in_array(false, $LockedYearClassIDSuccessArr))
			{
				$sql = "Drop Table SUBJECT_TERM_CLASS_BACKUP";
				$li->db_db_query($sql);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SubjectGroup_LockedYearClassID_Initialize', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "======================================================================================<br>\r\n";
			$x .= "Subject Group - Initialize LockedYearClassID Field from InternalClassCode [End]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";
		}
		#######################################################################################
		### Subject Group - Add the LockedYearClassID Field from InternalClassCode [End]
		#######################################################################################

		###################################################
		### IES: Patch stage weight [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_PatchStageWeight'])
		{
			if(count($plugin['SBA']) > 0) {
				//since SBA share table with IES , for safe , skip this update if client with SBA
			}else{

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Patch stage weight [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				$sql = "UPDATE IES_STAGE SET Weight = 0.25 WHERE Sequence IN (1,2)";
				$li->db_db_query($sql);

				$sql = "UPDATE IES_STAGE SET Weight = 0.5 WHERE Sequence IN (3)";
				$li->db_db_query($sql);

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_PatchStageWeight', 1, now())";
				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Patch stage weight [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}
		}
		###################################################
		### IES: Patch stage weight [End]
		###################################################

		###################################################
		### IES: Patch criteria sequence [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_PatchCriteriaSeq'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Patch criteria sequence [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");

			$sql = "UPDATE IES_MARKING_CRITERIA SET defaultSequence = 9999 WHERE MarkCriteriaID = 1";
			$li->db_db_query($sql);

			$sql = "UPDATE IES_MARKING_CRITERIA SET defaultSequence = 1 WHERE MarkCriteriaID = 21";
			$li->db_db_query($sql);

			$sql = "UPDATE IES_MARKING_CRITERIA SET defaultSequence = 2 WHERE MarkCriteriaID = 22";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_PatchCriteriaSeq', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Patch criteria sequence [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES: Patch criteria sequence [End]
		###################################################


		#######################################################################################
		### Subject Group - Restore all LockedYearClassID Field to Null [Start]
		#######################################################################################
		if (!$GSary['SubjectGroup_LockedYearClassID_RestoreToNull'])
		{
			$x .= "======================================================================================<br>\r\n";
			$x .= "Subject Group - Restore all LockedYearClassID Field to Null [Start]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";

			$RestoreLockLogicSuccessArr = array();

			$sql = "Update SUBJECT_TERM_CLASS set LockedYearClassID = null";
			$RestoreLockLogicSuccessArr['SetLockedYearClassIDToNull'] = $li->db_db_query($sql);

			$sql = "Delete From GENERAL_SETTING Where Module = '$ModuleName' And SettingName = 'SubjectGroup_LockedYearClassID_Initialize'";
			$RestoreLockLogicSuccessArr['DeleteAddonRecord'] = $li->db_db_query($sql);

			if (!in_array(false, $RestoreLockLogicSuccessArr))
				$x .= "Subject Group - Restore all LockedYearClassID Field to Null <font color='blue'><b>Success</b></font>.<br>\r\n";
			else
				$x .= "Subject Group - Restore all LockedYearClassID Field to Null <font color='red'><b>Failed</b></font>.<br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SubjectGroup_LockedYearClassID_RestoreToNull', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "======================================================================================<br>\r\n";
			$x .= "Subject Group - Restore all LockedYearClassID Field to Null [End]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";
		}
		#######################################################################################
		### Subject Group - Restore all LockedYearClassID Field to Null [End]
		#######################################################################################

		#######################################################################################
		### eEnrol - Convert previous no attendance record to Absent [Start]
		#######################################################################################
		if (!$GSary['eEnrol_ConvertPreviousNoAttendanceRecordToAbsent'])
		{
			$x .= "======================================================================================<br>\r\n";
			$x .= "eEnrol - Convert previous no attendance record to Absent [Start]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";

			### Backup Attendance first
			$sql = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_GROUP_ATTENDANCE_ABS_BAK SELECT * FROM INTRANET_ENROL_GROUP_ATTENDANCE";
			$li->db_db_query($sql);
			$sql = "CREATE TABLE IF NOT EXISTS INTRANET_ENROL_EVENT_ATTENDANCE_ABS_BAK SELECT * FROM INTRANET_ENROL_EVENT_ATTENDANCE";
			$li->db_db_query($sql);

			### Club Attendance Processing
			$sql = "Select
							iegi.EnrolGroupID, iu.UserID
					From
							INTRANET_ENROL_GROUPINFO as iegi
							Inner Join
							INTRANET_GROUP as ig On (iegi.GroupID = ig.GroupID)
							Inner Join
							INTRANET_USERGROUP as iug On (iegi.EnrolGroupID = iug.EnrolGroupID)
							Inner Join
							INTRANET_USER as iu On (iug.UserID = iu.UserID)
					Where
							ig.AcademicYearID = '".Get_Current_Academic_Year_ID()."'
							And
							iu.RecordType = 2
					Order By
							iegi.EnrolGroupID, iu.UserID
					";
			$ClubStudentMemberArr = $li->returnArray($sql);
			$numOfMember = count($ClubStudentMemberArr);
			$EnrolGroupIDArr = array_values(array_unique(Get_Array_By_Key($ClubStudentMemberArr, 'EnrolGroupID')));


			$sql = "Select EnrolGroupID, GroupDateID From INTRANET_ENROL_GROUP_DATE Where RecordStatus = 1 And EnrolGroupID In (".implode(',', (array)$EnrolGroupIDArr).") And ActivityDateStart < now()";
			$MeetingDateArr = $li->returnArray($sql);
			$numOfMeetingDate = count($MeetingDateArr);
			$MeetingDateAssoArr = array();
			for ($i=0; $i<$numOfMeetingDate; $i++)
				(array)$MeetingDateAssoArr[$MeetingDateArr[$i]['EnrolGroupID']][] = $MeetingDateArr[$i]['GroupDateID'];


			for ($i=0; $i<$numOfMember; $i++)
			{
				$thisEnrolGroupID = $ClubStudentMemberArr[$i]['EnrolGroupID'];
				$thisStudentID = $ClubStudentMemberArr[$i]['UserID'];
				$thisGroupDateIDArr = (array)$MeetingDateAssoArr[$thisEnrolGroupID];
				$thisNumOfMeeting = count($thisGroupDateIDArr);

				for ($j=0; $j<$thisNumOfMeeting; $j++)
				{
					$thisGroupDateID = $thisGroupDateIDArr[$j];
					$sql = "Select GroupAttendanceID From INTRANET_ENROL_GROUP_ATTENDANCE Where GroupDateID = '".$thisGroupDateID."' And EnrolGroupID = '".$thisEnrolGroupID."' And StudentID = '".$thisStudentID."'";
					$thisAttendanceArr = $li->returnArray($sql);
					if ($thisAttendanceArr[0]['GroupAttendanceID'] == '')
					{
						$sql = "Insert Into INTRANET_ENROL_GROUP_ATTENDANCE
									(GroupDateID, StudentID, DateModified, EnrolGroupID, RecordStatus)
								Values
									('".$thisGroupDateID."', '".$thisStudentID."', now(), '".$thisEnrolGroupID."', '3')
								";
						$thisSuccess = $li->db_db_query($sql);

						if ($thisSuccess)
							$x .= "EnrolGroupID = $thisEnrolGroupID, StudentID = $thisStudentID, GroupDateID = $thisGroupDateID <font color='blue'><b>Success</b></font>.<br>\r\n";
						else
							$x .= "EnrolGroupID = $thisEnrolGroupID, StudentID = $thisStudentID, GroupDateID = $thisGroupDateID <font color='red'><b>Failed</b></font>.<br>\r\n";
					}
				}
			}


			### Activity Attendance Processing
			$sql = "Select
							iees.EnrolEventID, iees.StudentID
					From
							INTRANET_ENROL_EVENTSTUDENT as iees
							Inner Join
							INTRANET_USER as iu On (iees.StudentID = iu.UserID)
					Where
							iees.RecordStatus = 2
							And
							iu.RecordType = 2
					Order By
							iees.EnrolEventID, iees.StudentID
					";
			$ActivityStudentMemberArr = $li->returnArray($sql);
			$numOfMember = count($ActivityStudentMemberArr);
			$EnrolEventIDArr = array_values(array_unique(Get_Array_By_Key($ActivityStudentMemberArr, 'EnrolEventID')));


			$sql = "Select EnrolEventID, EventDateID From INTRANET_ENROL_EVENT_DATE Where RecordStatus = 1 And EnrolEventID In (".implode(',', (array)$EnrolEventIDArr).") And ActivityDateStart < now()";
			$MeetingDateArr = $li->returnArray($sql);
			$numOfMeetingDate = count($MeetingDateArr);
			$MeetingDateAssoArr = array();
			for ($i=0; $i<$numOfMeetingDate; $i++)
				(array)$MeetingDateAssoArr[$MeetingDateArr[$i]['EnrolEventID']][] = $MeetingDateArr[$i]['EventDateID'];


			for ($i=0; $i<$numOfMember; $i++)
			{
				$thisEnrolEventID = $ActivityStudentMemberArr[$i]['EnrolEventID'];
				$thisStudentID = $ActivityStudentMemberArr[$i]['StudentID'];
				$thisEventDateIDArr = (array)$MeetingDateAssoArr[$thisEnrolEventID];
				$thisNumOfMeeting = count($thisEventDateIDArr);

				for ($j=0; $j<$thisNumOfMeeting; $j++)
				{
					$thisEventDateID = $thisEventDateIDArr[$j];
					$sql = "Select EventAttendanceID From INTRANET_ENROL_EVENT_ATTENDANCE Where EventDateID = '".$thisEventDateID."' And EnrolEventID = '".$thisEnrolEventID."' And StudentID = '".$thisStudentID."'";
					$thisAttendanceArr = $li->returnArray($sql);
					if ($thisAttendanceArr[0]['EventAttendanceID'] == '')
					{
						$sql = "Insert Into INTRANET_ENROL_EVENT_ATTENDANCE
									(EventDateID, StudentID, DateModified, EnrolEventID, RecordStatus)
								Values
									('".$thisEventDateID."', '".$thisStudentID."', now(), '".$thisEnrolEventID."', '3')
								";
						$thisSuccess = $li->db_db_query($sql);

						if ($thisSuccess)
							$x .= "EnrolEventID = $thisEnrolEventID, StudentID = $thisStudentID, EventDateID = $thisEventDateID <font color='blue'><b>Success</b></font>.<br>\r\n";
						else
							$x .= "EnrolEventID = $thisEnrolEventID, StudentID = $thisStudentID, EventDateID = $thisEventDateID <font color='red'><b>Failed</b></font>.<br>\r\n";
					}
				}
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrol_ConvertPreviousNoAttendanceRecordToAbsent', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "======================================================================================<br>\r\n";
			$x .= "eEnrol - Convert previous no attendance record to Absent [End]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";
		}
		#######################################################################################
		### eEnrol - Convert previous no attendance record to Absent [End]
		#######################################################################################

		############################################################
		# Patch Request Approver [Start]
		############################################################
		if(!$GSary['iPortfolioPatchRequestApprover'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Patch OLE Request Approver [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

      $sql = "UPDATE {$eclass_db}.OLE_STUDENT SET ";
      $sql .= "RequestApprovedBy = ApprovedBy";
      $li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioPatchRequestApprover', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Patch OLE Request Approver [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		if(!$GSary['iPortfolioPatchRequestApprover2'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Patch OLE Request Approver again for running script sequence problme (IP / eclass) [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

      $sql = "UPDATE {$eclass_db}.OLE_STUDENT SET ";
      $sql .= "RequestApprovedBy = ApprovedBy where RequestApprovedBy is null";
      $li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolioPatchRequestApprover2', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Patch OLE Request Approver [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		############################################################
		# Patch Request Approver [End]
		############################################################


		############################################################
		# Update Conduct Mark Balance in eDisciplinev12 [Start]
		# [Incorrect conduct mark balance in edis due to double release of AP record]
		############################################################
		if($plugin['Disciplinev12'] && !$GSary['updateConductMarkBalance_eDisv12'])
		{
			# check any roles, if role is created, then no need to re-declare the role
			$x .= "===================================================================<br>\r\n";
			$x .= "Update Conduct Mark Balance in eDisciplinev12 [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."addon/script/conduct_balance.php");

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'updateConductMarkBalance_eDisv12', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Update Conduct Mark Balance in eDisciplinev12 [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		############################################################
		# Update Conduct Mark Balance in eDisciplinev12 [End]
		############################################################

		############################################################
		# Synchronize the records in eDisv12 & Student Profile
		# [Unsync of record between edis & student profile due to double release of AP record]
		############################################################
		if($plugin['Disciplinev12'] && !$GSary['sync_edis_to_profile_v2'])
		{
			# check any roles, if role is created, then no need to re-declare the role
			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize the records in eDisv12 & Student Profile [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."addon/script/sync_edis_to_profile.php");

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'sync_edis_to_profile_v2', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize the records in eDisv12 & Student Profile [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		############################################################
		# Update Conduct Mark Balance in eDisciplinev12 [End]
		############################################################

		############################################################
		# Synchronize the Group Title to TitleChinese [Start]
		############################################################
		if(!$GSary['sync_group_title_chinese'])
		{
			# check any roles, if role is created, then no need to re-declare the role
			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize the Group Title to TitleChinese [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "update INTRANET_GROUP set TitleChinese=Title where TitleChinese is NULL";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'sync_group_title_chinese', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize the Group Title to TitleChinese [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		############################################################
		# Synchronize the Group Title to TitleChinese [Start]
		############################################################

		###################################################
		### IES: Patch default criteria weight (Updated) [Start]
		###################################################
		if (isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_PatchDefaultMaxScoreAndWeight'])
		{

			if(count($plugin['SBA']) > 0) {
				//since SBA share table with IES , for safe , skip this update if client with SBA
				// do nothing
			}else{

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Patch default max score and weight [Start]</b><br>\r\n";
				$x .= "(Override some values set in IES_PatchDefaultCriteriaWeight2)<br>\r\n";
				$x .= "===================================================================<br>\r\n";

				include_once($PATH_WRT_ROOT."includes/ies/iesConfig.inc.php");

				$sql = "UPDATE IES_MARKING_CRITERIA ";
				$sql .= "SET defaultWeight = IF(MarkCriteriaID = 1, NULL, 1), defaultMaxScore = IF(MarkCriteriaID = 1, NULL, 9)";
				$li->db_db_query($sql);

				$sql = "UPDATE IES_STAGE_MARKING_CRITERIA ismc ";
				$sql .= "INNER JOIN IES_MARKING_CRITERIA imc ON ismc.MarkCriteriaID = imc.MarkCriteriaID ";
				$sql .= "SET ismc.Weight = imc.defaultWeight, ismc.MaxScore = imc.defaultMaxScore";
				$li->db_db_query($sql);

				$sql = "UPDATE IES_STAGE ";
				$sql .= "SET Weight = IF(Sequence = 3, 2, 1), MaxScore = 9";
				$li->db_db_query($sql);

				$sql = "UPDATE IES_SCHEME ";
				$sql .= "SET MaxScore = 100";
				$li->db_db_query($sql);

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_PatchDefaultMaxScoreAndWeight', 1, now())";
				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

				$x .= "===================================================================<br>\r\n";
				$x .= "IES: Patch default max score and weight [End]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";
			}
		}
		###################################################
		### IES: Patch default criteria weight [End]
		###################################################

		######################################################
		### Update "Form Subject" setting (initial) [Start]
		######################################################
		if(!$GSary['SubjectSetting_FormSubject']) {
			$allSemesters = getSemesters(Get_Current_Academic_Year_ID());
			$allTermID = array_keys($allSemesters);

			$x .= "===================================================================<br>\r\n";
			$x .= "Update \"Form Subject\" setting (initial) [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			if(sizeof($allTermID)>0) {
				$sql = "SELECT st.SubjectID, stcyr.YearID FROM SUBJECT_TERM_CLASS_YEAR_RELATION stcyr INNER JOIN SUBJECT_TERM st ON (st.SubjectGroupID=stcyr.SubjectGroupID) WHERE st.YearTermID IN (".implode(',',$allTermID).") GROUP BY st.SubjectID, stcyr.YearID ORDER BY st.SubjectID, stcyr.YearID";
				$temp = $li->returnArray($sql, 2);

				$values = "";
				$delim = "";
				for($i=0; $i<sizeof($temp); $i++) {
					list($subjectID, $yearID) = $temp[$i];
					$values .= $delim."('$subjectID','$yearID',NOW(),'$UserID',NOW(),'$UserID')";
					$delim = ", ";
				}
				if($values!="") {
					$sql = "INSERT INTO SUBJECT_YEAR_RELATION VALUES $values";
					$li->db_db_query($sql);
				}
			}
			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SubjectSetting_FormSubject', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());
			$X .= $sql;
			$x .= "===================================================================<br>\r\n";
			$x .= "Update \"Form Subject\" setting (initial) [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

		}
		######################################################
		### Update "Form Subject" setting (initial) [End]
		######################################################


		##########################################################################
		### insert eClass 4.1 version into EClASS_MODULE_VERSION_NUMBER [Start]
		##########################################################################

		$x .= "=============================================<br>\r\n";
		$x .= " Store eClass4.1 version into DB [Start]<br>\r\n";
		$x .= "=============================================<br>\r\n";

		if(!$GSary['ModuleVersion_eClass_4.1']) {

			# store most update eClass version to DB
			$sql = "INSERT INTO ECLASS_MODULE_VERSION_LOG(Module, VersionNo, DateInput) VALUES ('eClass', 4.1, NOW())";
			//$x .= $sql;
			$tempFlag = $li->db_db_query($sql);

			# update current version that client is using
			if(file_exists($PATH_WRT_ROOT."../eclass40/system/settings/settings.php")){
				# check settings.php
				include_once($PATH_WRT_ROOT."../eclass40/system/settings/settings.php");
			} else if(file_exists("../../../eclass40/system/settings/global.php")){
				# check global.php
				include_once($PATH_WRT_ROOT."../eclass40/system/settings/global.php");
			} else {
				# none exist, return failure
				die('<font color="red">Store eClass4.1 version into DB [Failed]</font>');
			}
			updateEclassModuleVersion("eClass", $ec_version_no);	# sync with current version with data in DB

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ModuleVersion_eClass_4.1', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

		}
		$x .= "Store eClass4.1 into DB : ";
		$x .= ($tempFlag) ? "Success" : "Fail";
		$x .= "<br>\r\n";
		$x .= "=============================================<br>\r\n";
		$x .= " Store eClass4.1 version into DB [End]<br>\r\n";
		$x .= "=============================================<br>\r\n";
		##########################################################################
		### insert eClass 4.1 version into EClASS_MODULE_VERSION_NUMBER [Start]
		##########################################################################


		###########################################################
		### Reset ALL IES Scheme Doc Settings to Default [Start]
		###########################################################
		if(isset($plugin['IES']) && $plugin['IES'] && !$GSary['IES_Reset_DOC_Export_Settings']) {
			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Reset IES DOC Export Setting to Default [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/ies/libies.php");
			include_once($PATH_WRT_ROOT."includes/ies/importDefaultDocSetting.php");

			$importDefaultDocSetting = new importDefaultDocSetting();
			if(!$importDefaultDocSetting->import()){
				$x .= "<br>\r\nSome Operation Fail - Please check Error Log For Details<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_Reset_DOC_Export_Settings', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES: Reset IES DOC Export Setting to Default [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		### Reset ALL IES Scheme Doc Settings to Default [End]
		###########################################################


		###########################################################
		###  Update "broadlearning" password [Start]
		###########################################################
		# disabled in Jan 2012
		if(false && $intranet_authentication_method!="LDAP" && !$GSary['broadlearning_update_pwd'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Update 'broadlearning' password [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$this_login = 'broadlearning';
			$this_pwd = "ngyyzm383";
			$this_pwd2 = $this_pwd;

			# check account exists
			$sql = "select UserID from INTRANET_USER where UserLogin='$this_login'";
			$temp = $li->returnVector($sql);
			if(sizeof($temp)>0)
			{
				### INTRANET_USER
				if ($intranet_authentication_method == "HASH")
				{
					$this_pwd2 = MD5($this_pwd);
				}
				$sql = "UPDATE INTRANET_USER SET UserPassword='$this_pwd2' WHERE UserLogin='$this_login'";
				$li->db_db_query($sql) or die(mysql_error());

				$x .= "--- INTRANET_USER <br>\r\n";

				### MAIL
				include_once($PATH_WRT_ROOT."includes/libwebmail.php");
				$lwebmail = new libwebmail();
				if ($lwebmail->has_webmail)
				{
					$lwebmail->change_password($this_login,$this_pwd,"iMail");
					$x .= "--- web mail <br>\r\n";
				}

				if($plugin["imail_gamma"]==true)
				{
					include_once($PATH_WRT_ROOT."includes/imap_gamma.php");
					$IMap = new imap_gamma(1);
					$IMapEmail = trim($this_login)."@".$SYS_CONFIG['Mail']['UserNameSubfix'];
					if($IMap->is_user_exist($IMapEmail))
					{
						$IMap->change_password($IMapEmail, $this_pwd);
						$x .= "--- iMail+ <br>\r\n";
					}
				}

				### FTP management
				if ($plugin['personalfile'])
				{
					include_once($PATH_WRT_ROOT."includes/libftp.php");
					$lftp = new libftp();
					if ($lftp->isFTP)
					{
						$lftp->changePassword($this_login,$this_pwd,"iFolder");
						$x .= "--- iFolder <br>\r\n";
					}
				}

				### SchoolNet
				if ($plugin['SchoolNet'])
				{
					$uid = $temp[0];

					include_once($PATH_WRT_ROOT."includes/libschoolnet.php");
					$lschoolnet = new libschoolnet();

					# Param: array in ($userlogin, $password, $DOB, $gender, $cname, $ename, $tel, $mobile, $address, $email, $teaching)
					$sql = "SELECT UserLogin, UserPassword, DATE_FORMAT(DateOfBirth,'%Y-%m-%d'), Gender, Chinesename, EnglishName, HomeTelNo, MobileTelNo, Address, UserEmail, Teaching FROM INTRANET_USER WHERE UserID = '$uid'";
					$data = $li->returnArray($sql,11);
					$data[0][1] = $this_pwd;
					$lschoolnet->addStaffUser($data);
					$x .= "--- SchoolNet <br>\r\n";
				}

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'broadlearning_update_pwd', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			else
			{
				$x .= "no broadlearning account.<br>\r\n";
			}

			$x .= "===================================================================<br>\r\n";
			$x .= "Update 'broadlearning' password [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Update "broadlearning" password [End]
		###########################################################

		###########################################################
		###  Pre-set Max Reply Slip Option [Start]
		###########################################################
		if(!$GSary['Notice_Circular_Survey_MaxOption'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Pre-set Max Reply Slip Option [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$data = array();
			$data['MaxReplySlipOption'] = 50;

			$lgs->Save_General_Setting("CIRCULAR", $data);
			$x .= "eCircular - Max Reply Slip Option pre-set to 50 <br>\r\n";
			$lgs->Save_General_Setting("eSurvey", $data);
			$x .= "eSurvey - Max Reply Slip Option pre-set to 50 <br>\r\n";
			$lgs->Save_General_Setting("eNotice", $data);
			$x .= "eNotice - Max Reply Slip Option pre-set to 50 <br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Notice_Circular_Survey_MaxOption', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Pre-set Max Reply Slip Option [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Update "broadlearning" password [End]
		###########################################################

		/*
		###############################################################################
		### Split Role Right of "AccountMgmt" into 3 (Parent, Staff & Student) [Start]
		###############################################################################
		if(!$GSary['SplitAccountMgmtRightIntoThree']) {
			$x .= "===================================================================<br>\r\n";
			$x .= " Split Role Right of \"AccountMgmt\" into 3 (Parent, Staff & Student) [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT RoleID, RightFlag, InputBy FROM ROLE_RIGHT WHERE FunctionName='eAdmin-AccountMgmt'";
			$IDs = $li->returnArray($sql,2);

			$values = "";
			$delim = "";
			for($i=0; $i<sizeof($IDs); $i++) {
				list($roleID, $rightFlag, $uid) = $IDs[$i];
				$values .= $delim."('$roleID','eAdmin-AccountMgmt_Parent', '$rightFlag', NOW(), '$uid', NOW())";
				$delim = ", ";
				$values .= $delim."('$roleID','eAdmin-AccountMgmt_Staff', '$rightFlag', NOW(), '$uid', NOW())";
				$values .= $delim."('$roleID','eAdmin-AccountMgmt_Student', '$rightFlag', NOW(), '$uid', NOW())";
			}

			if($values!="") {
				$sql = "INSERT INTO ROLE_RIGHT (RoleID, FunctionName, RightFlag, DateInput, InputBy, DateModified) VALUES $values";
				$li->db_db_query($sql);
			}

			$x .= $sql."<br>\r\n";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SplitAccountMgmtRightIntoThree', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());


			$x .= "===================================================================<br>\r\n";
			$x .= "Split Role Right of \"AccountMgmt\" into 3 (Parent, Staff & Student) [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		### Split Role Right of "AccountMgmt" into 3 (Parent, Staff & Student) [End]
		###########################################################
		*/


		###################################################
		### eInventory
		### Transfer the settings data from /file/inventory/xxx.txt to table "GENERAL_SETTING"
		###################################################
		if($plugin['Inventory'])
		{
			if(!$GSary['eInventorySettings'])
			{
				$x .= "===================================================================<br>\r\n";
				$x .= "  Inventory - Transfer the settings data from /file/inventory/xxx.txt to table \"GENERAL_SETTING\" [Start]</b><br>\r\n";
				$x .= "===================================================================<br>\r\n";

				$eInventory_Module = "eInventory";

				### Allow Group Leader Add Items
				$content = trim($lf->file_read($intranet_root."/file/inventory/group_leader_right.txt"));
				if(!empty($content))
	            {
					$addItemRight = $content;
	            }
	            else
	            {
		            $addItemRight = "0";
	            }
	            $ary['Leader_AddItemRight'] = $addItemRight;

	            ### Stocktake Period
				$file_content = trim($lf->file_read($intranet_root."/file/inventory/stocktake_setting.txt"));
	            if(!empty($file_content))
	            {
					$Arr_Stocktake_Period = explode(",", $file_content);
					$StocktakePeriodStart = $Arr_Stocktake_Period[0];
					$StocktakePeriodEnd = $Arr_Stocktake_Period[1];
	            }
	            else
	            {
		            $StocktakePeriodStart = date("Y-m-d");
		            $StocktakePeriodEnd = date("Y-m-d");
	            }
	            $ary['stocktake_period_start'] = $StocktakePeriodStart;
	            $ary['stocktake_period_end'] = $StocktakePeriodEnd;

	            ### Warranty Expiry Reminder
	            ### Barcode maximun length
	            $file_content = trim($lf->file_read($intranet_root."/file/inventory/others_setting.txt"));
	            if(!empty($file_content))
	            {
					$Arr_OthersInfo = explode(",", $file_content);
	                $WarningDayPeriod = $Arr_OthersInfo[0];
	                $BarcodeMaxLength = $Arr_OthersInfo[1];
	                $BarcodeFormat = $Arr_OthersInfo[2];
	            }
	            else
	            {
		            $WarningDayPeriod = 5;
		            $BarcodeMaxLength = 10;
		            $BarcodeFormat = 1;
	            }
	            $ary['warranty_expiry_warning'] = $WarningDayPeriod;
	            $ary['barcode_max_length'] = $BarcodeMaxLength;
	            $ary['barcode_format'] = $BarcodeFormat;

	            ### Item Code
	            $content = trim($lf->file_read($intranet_root."/file/inventory/itemcode_format_settings.txt"));
	            if(!empty($content))
	            {
					$arr_ItemCodeFormat = explode(",", $content);
	            }
	            else
	            {
		            $arr_ItemCodeFormat = "";
	            }
	            $ary['itemcode_setting'] = $arr_ItemCodeFormat;

				# check if there is already containds settings data, if yes, no need to transfer
				$tmp_ary = $lgs->Get_General_Setting($eInventory_Module);

				if(empty($tmp_ary))
				{
					$lgs->Save_General_Setting($eInventory_Module, $ary);
					$x .= "===================================================================<br>\r\n";
					$x .= "  Inventory - Transfer the settings data from /file/inventory/xxx.txt to table \"GENERAL_SETTING\" [End]</b><br>\r\n";
					$x .= "===================================================================<br>\r\n";
				}
				else
				{
					$x .= "===================================================================<br>\r\n";
					$x .= "  eInventory...Data already transferred<br>\r\n";
					$x .= "===================================================================<br>\r\n";
				}

				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eInventorySettings', 1, now())";
				$li->db_db_query($sql) or die(mysql_error());
			}
		}
		###################################################
		### eNotice [End]
		###################################################


		###########################################################
		###  Insert amBook to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_amBook_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert amBook to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'ambook'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('ambook', 'amBook', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_amBook_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert amBook to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert amBook to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Sync CycleDay, PeriodID to INTRANET_CYCLE_DAYS [Start]
		###########################################################
		if(!$GSary['Sync_CycleDay_PeriodID_to_INTRANET_CYCLE_DAYS'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Sync CycleDay, PeriodID to INTRANET_CYCLE_DAYS [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "
				SELECT
					p.PeriodID ,
					p.PeriodStart ,
					p.PeriodEnd,
					p.FirstDay,
					p.PeriodDays,
					d.RecordDate
				FROM
					INTRANET_CYCLE_GENERATION_PERIOD p
					INNER JOIN INTRANET_CYCLE_DAYS d ON d.RecordDate Between p.PeriodStart AND p.PeriodEnd
				";

			$result = $li->returnArray($sql);
			$PeriodInfo = BuildMultiKeyAssoc($result, "PeriodID",array("FirstDay","PeriodDays"));

			$result = BuildMultiKeyAssoc($result, "PeriodID","RecordDate",1,1);

			foreach($result as $PeriodID => $RecordDateArr)
			{
				$Day = $PeriodInfo[$PeriodID]['FirstDay'];
				$PeriodDays = $PeriodInfo[$PeriodID]['PeriodDays'];

				foreach($RecordDateArr as $RecordDate)
				{
					$CycleDay = $Day+1;
					$sql = "UPDATE INTRANET_CYCLE_DAYS SET PeriodID = '$PeriodID', CycleDay = '$CycleDay' WHERE RecordDate = '$RecordDate' ";

					$Day = ++$Day%$PeriodDays;
					$li->db_db_query($sql);
				}

			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Sync_CycleDay_PeriodID_to_INTRANET_CYCLE_DAYS', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Sync CycleDay, PeriodID to INTRANET_CYCLE_DAYS [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert amBook to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Sync OEA Program ID with OLE [Start]
		###########################################################
		if(!$GSary['Sync_OEA_ProgramID_with_OLE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Sync OEA Program ID with OLE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT RecordID, OLE_STUDENT_RecordID FROM {$eclass_db}.OEA_STUDENT WHERE OLE_STUDENT_RecordID!=0 AND OLE_PROGRAM_ProgramID=0";
			$result = $li->returnArray($sql);

			for($i=0, $i_max=count($result); $i<$i_max; $i++) {
				list($oea_rec_id, $ole_rec_id) = $result[$i];
				$sql = "SELECT ProgramID FROM {$eclass_db}.OLE_STUDENT WHERE RecordID='$ole_rec_id'";
				$temp = $li->returnVector($sql);
				if($temp[0] != "") {
					$sql = "UPDATE {$eclass_db}.OEA_STUDENT SET OLE_PROGRAM_ProgramID='".$temp[0]."' WHERE RecordID='$oea_rec_id'";
					$li->db_db_query($sql);
				}
			}


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Sync_OEA_ProgramID_with_OLE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " Sync OEA Program ID with OLE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Sync OEA Program ID with OLE [End]
		###########################################################


		###########################################################
		###  Merge eBooking Booking Details Tables [Start]
		###########################################################
		$FlagName = 'Merge_Booking_Detail_Table';
		if(!$GSary[$FlagName])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Merge eBooking Booking Details Tables [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_EBOOKING_BOOKING_DETAILS";
			$result = $li->returnVector($sql);

			if($result[0]==0)
			{
				$sql = "
					INSERT INTO INTRANET_EBOOKING_BOOKING_DETAILS
					(
						BookingID,
						FacilityType,
						FacilityID,
						PIC,
						ProcessDate,
						BookingStatus,
						InputBy,
						ModifiedBy,
						RecordType,
						RecordStatus,
						CurrentStatus,
						CheckInCheckOutRemark,
						DateInput,
						DateModified
					)
					(
						SELECT
							BookingID,
							1,
							RoomID,
							PIC,
							ProcessDate,
							BookingStatus,
							InputBy,
							ModifiedBy,
							RecordType,
							RecordStatus,
							CurrentStatus,
							CheckInCheckOutRemark,
							DateInput,
							DateModified
						FROM
							INTRANET_EBOOKING_ROOM_BOOKING_DETAILS
					)
					UNION
					(
						SELECT
							BookingID,
							2,
							ItemID,
							PIC,
							ProcessDate,
							BookingStatus,
							InputBy,
							ModifiedBy,
							RecordType,
							RecordStatus,
							CurrentStatus,
							CheckInCheckOutRemark,
							DateInput,
							DateModified
						FROM
							INTRANET_EBOOKING_FACILITIES_BOOKING_DETAILS
					)
					ORDER BY DateInput ASC
				";
				$Success = $li->db_db_query($sql) or debug_pr(mysql_error());
			}

			if($Success || $result[0]>0)
			{
				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$FlagName', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			$x .= "===================================================================<br>\r\n";
			$x .= " Merge eBooking Booking Details Tables [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Merge eBooking Booking Details Tables [End]
		###########################################################

		###########################################################
		###  Update all NULL EnrolGroupID of INTRANET_USERGROUP to 0 and Set Default value of EnrolGroupID to 0 [Start]
		###########################################################
		$FlagName = 'UpdateNULLEnrolGroupIDtoZero';
		if(!$GSary[$FlagName])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " Update all NULL EnrolGroupID of INTRANET_USERGROUP to 0 and Set Default value of EnrolGroupID to 0  [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# find duplicate
			$sql = "
				SELECT
					COUNT(*)
				FROM
					INTRANET_USERGROUP iug1
					INNER JOIN INTRANET_USERGROUP iug2 ON
						iug1.groupID = iug2.GroupID
						AND iug1.UserID = iug2.UserID
						AND (iug1.EnrolGroupID IS NULL OR iug1.EnrolGroupID = 0)
						AND (iug2.EnrolGroupID IS NULL OR iug2.EnrolGroupID = 0)
						AND iug1.UserGroupID <> iug2.UserGroupID
					INNER JOIN INTRANET_USER iu1 ON iu1.UserID = iug1.UserID
					INNER JOIN INTRANET_USER iu2 ON iu2.UserID = iug2.UserID
			";
			$Result = $li->returnVector($sql);

			# if duplicate found
			if($Result[0]>0)
			{
				$li->Start_Trans();

				$date = date("Ymd");
				# backup table
				$sql = "CREATE TABLE IF NOT EXISTS INTRANET_USERGROUP_$date LIKE INTRANET_USERGROUP";
				$SuccessArr['CreateTable'] = $li->db_db_query($sql);

				# copy date to backup table
				$sql = "INSERT INTO INTRANET_USERGROUP_$date SELECT * FROM INTRANET_USERGROUP";
				$SuccessArr['CopyTable'] = $li->db_db_query($sql);

				# Update NULL EnrolGroupID to Zero
				$sql = "UPDATE IGNORE INTRANET_USERGROUP SET EnrolGroupID = 0 WHERE EnrolGroupID IS NULL";
				$SuccessArr['UpdateTable'] = $li->db_db_query($sql);

				# Delete all NULL EnrolGroupID Record
				$sql = "DELETE FROM INTRANET_USERGROUP WHERE EnrolGroupID IS NULL";
				$SuccessArr['DeleteNull'] = $li->db_db_query($sql);

				if(in_array(false, $SuccessArr))
				{
					$li->RollBack_Trans();
					$Success = false;
					$x .= 'Update Failed<br>'."\n";
				}
				else
				{
					$li->Commit_Trans();
					$Success = true;
					$x .= 'Update Succeed<br>'."\n";
				}
			}
			else
			{
				$Success = true;
				$x .= 'No duplicated<br>'."\n";
			}

			if($Success)
			{
				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$FlagName', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			$x .= "===================================================================<br>\r\n";
			$x .= " Update all NULL EnrolGroupID of INTRANET_USERGROUP to 0 and Set Default value of EnrolGroupID to 0  [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Update all NULL EnrolGroupID of INTRANET_USERGROUP to 0 and Set Default value of EnrolGroupID to 0  [End]
		###########################################################


		####################################################################################
		###  Assign default value for "SendReplySlip" in table INTRANET_NOTICE_MODULE_TEMPLATE  [Start]
		####################################################################################
		if(!$GSary['Assign_Default_Value_For_SendReplySlip'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Assign default value for \"SendReplySlip\" in table INTRANET_NOTICE_MODULE_TEMPLATE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# default is 0

			$sql = "UPDATE INTRANET_NOTICE_MODULE_TEMPLATE SET SendReplySlip=1 WHERE ((ReplySlip IS NOT NULL AND ReplySlip!='') OR (ReplySlipContent IS NOT NULL AND ReplySlipContent!=''))";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Assign_Default_Value_For_SendReplySlip', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Assign default value for \"SendReplySlip\" in table INTRANET_NOTICE_MODULE_TEMPLATE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert amBook to INTRANET_MODULE [End]
		###########################################################

		####################################################################################
		###  eBooking - Update RecordType of the INTRANET_EBOOKING_USER_BOOKING_RULE   [Start]
		####################################################################################
		$FlagName = 'UpdateUserBookingRuleRecordType';
		if(!$GSary[$FlagName])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " eBooking - Update RecordType of the INTRANET_EBOOKING_USER_BOOKING_RULE  [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "
				UPDATE
					INTRANET_EBOOKING_USER_BOOKING_RULE ubr
					LEFT JOIN INTRANET_EBOOKING_USER_BOOKING_RULE_TARGET_USER tu ON ubr.RuleID = tu.RuleID
				SET
					ubr.RecordType = IF(ubr.RecordType <> 0, ubr.RecordType, IFNULL(tu.UserType, 5))
			";
			$Success = $li->db_db_query($sql) or debug_pr(mysql_error());

			if($Success)
			{
				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$FlagName', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			$x .= "===================================================================<br>\r\n";
			$x .= " eBooking - Update RecordType of the INTRANET_EBOOKING_USER_BOOKING_RULE  [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  eBooking - Update RecordType of the INTRANET_EBOOKING_USER_BOOKING_RULE  [End]
		###########################################################

		####################################################################################
		###  eBooking - Update RequestDate with InputDate for old records   [Start]
		####################################################################################
		$FlagName = 'UpdateRequestDateWithDateInput';
		if(!$GSary[$FlagName])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " eBooking - Update RequestDate with InputDate for old records  [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "
				UPDATE
					INTRANET_EBOOKING_RECORD ier
				SET
					ier.RequestDate = ier.DateInput
			";
			$Success = $li->db_db_query($sql) or debug_pr(mysql_error());

			if($Success)
			{
				# update General Settings - mark the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$FlagName', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			$x .= "===================================================================<br>\r\n";
			$x .= " eBooking - Update RequestDate with InputDate for old records  [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  eBooking - Update RequestDate with InputDate for old records   [End]
		###########################################################

		####################################################################################
		###  eNotice - store PaymentID to PAYMENT_OVERALL_TRANSACTION_LOG [Start]
		####################################################################################
		if(!$GSary['UpdatePaymentId2PaymentOverallTransactionLog'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " eNotice - store PaymentID to PAYMENT_OVERALL_TRANSACTION_LOG  [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID IS NOT NULL";
			$PaymentItemIdAry = $li->returnVector($sql);

			$NoOfPaymentItem = count($PaymentItemIdAry);

			$a = 0;
			$f = 0;

			for($i=0; $i<$NoOfPaymentItem; $i++) {
				$paymentItemId = $PaymentItemIdAry[$i];
				$flag = "";

				$sql = "SELECT PaymentID, ItemID, StudentID, DateInput FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID='$paymentItemId'";
				$transactionInfo = $li->returnArray($sql);

				$NoOfItemStudent = count($transactionInfo);

				for($j=0; $j<$NoOfItemStudent; $j++) {
					list($payId, $itemId, $student_id, $dateinput) = $transactionInfo[$j];

					$sql = "SELECT StudentID, RelatedTransactionID, Details FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE StudentID='$student_id' AND RelatedTransactionID='$itemId' AND TransactionTime='$dateinput'";
					$result = $li->returnArray($sql);

					if(count($result)>0) {
						$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RelatedTransactionID='$payId' WHERE StudentID='$student_id' AND RelatedTransactionID='$itemId' AND TransactionTime='$dateinput'";
						$flag = $li->db_db_query($sql);
						if(!$flag) $f++;
						$a++;
					}
				}
			}

			$x .= "No. of ItemID from Payment Notice : ".$NoOfPaymentItem."<br>";
			$x .= "No. of transaction log revised : ".$a."<br>";
			$x .= "No. of transaction log fail to change : ".$f."<br>";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'UpdatePaymentId2PaymentOverallTransactionLog', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " eNotice - store PaymentID to PAYMENT_OVERALL_TRANSACTION_LOG  [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  eNotice - store PaymentID to PAYMENT_OVERALL_TRANSACTION_LOG [End]
		###########################################################


		###########################################################
		###  iPortfolio JUPAS - Syn Achievement if not Award Bearing [Start]
		###########################################################
		if($plugin['iPortfolio'] && !$GSary['Sync_iPf_Jupas_Oea_Achievement_If_No_AwardBeraing'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " iPortfolio JUPAS - Syn Achievement if not Award Bearing [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "CREATE TABLE IF NOT EXISTS {$eclass_db}.OEA_STUDENT_".date("YmdHis")." SELECT * FROM {$eclass_db}.OEA_STUDENT";
			$li->db_db_query($sql);

			$sql = "UPDATE {$eclass_db}.OEA_STUDENT SET Achievement='N' WHERE OEA_AwardBearing='N'";
			$Sync_iPf_Jupas_Oea_Achievement_If_No_AwardBeraing_Success = $li->db_db_query($sql);

			if ($Sync_iPf_Jupas_Oea_Achievement_If_No_AwardBeraing_Success) {
				$x .= "Syn Achievement if not Award Bearing <font color='green'>Success</font><br>\r\n";
			}
			else {
				$x .= "Syn Achievement if not Award Bearing <font color='red'><b>FAILED</b></font><br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Sync_iPf_Jupas_Oea_Achievement_If_No_AwardBeraing', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " iPortfolio JUPAS - Syn Achievement if not Award Bearing [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Sync OEA Program ID with OLE [End]
		###########################################################


		###########################################################
		###  iPortfolio - Dynamic Report Default Template [Start]
		###########################################################
		if ($plugin['iPortfolio']) {
			$x .= "===================================================================<br>\r\n";
			$x .= " iPortfolio - Dynamic Report Default Template [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/portfolio25/lib-portfolio_settings.php");
			include_once($PATH_WRT_ROOT."includes/portfolio25/dynReport/script/libDefaultTemplate.php");
			$objScript = new libDefaultTemplate();

			if ($objScript->isLatestVersion()) {
				$x .= "Client is using the most updated version already";
			}
			else {
				$success = $objScript->run();
				$x .= ($success)? "Version ".$objScript->getLatestVersion()." Update <font color='green'>Success</font>" : "Version Update <font color='red'><b>Failed</b></font>";
			}
			$x .= "<br>\r\n";

			$x .= "===================================================================<br>\r\n";
			$x .= " iPortfolio - Dynamic Report Default Template [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  iPortfolio - Dynamic Report Default Template [End]
		###########################################################

		###########################################################################
		###  eDisciplinev12 - Preset Approval Group Right setting (if any) [Start]
		###########################################################################
		if ($plugin['Disciplinev12'] && !$GSary['Preset_Approval_Group_Right']) {
			$x .= "===================================================================<br>\r\n";
			$x .= " eDisciplinev12 - Preset Approval Group Right setting (if any) [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
			$ldiscipline = new libdisciplinev12();

			$sql = "Select GroupID From DISCIPLINE_AP_APPROVAL_GROUP";
			$ApprovalGroupAry = $ldiscipline->returnVector($sql);

			$sql = "SELECT CatID FROM DISCIPLINE_MERIT_ITEM_CATEGORY WHERE RecordStatus IS NULL OR RecordStatus=1";
			$apCategory = $ldiscipline->returnVector($sql);

			$NoOfCategory = count($apCategory);
			$value = "";
			$delim = "";
			if(count($ApprovalGroupAry) > 0 && $NoOfCategory > 0) {
				for($i=0; $i<count($ApprovalGroupAry); $i++) {
					$gpid = $ApprovalGroupAry[$i];
					$ldiscipline->ASSIGN_APPROVAL_GROUP_RIGHT($gpid, $apCategory);
				}
			}


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Preset_Approval_Group_Right', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "<br>\r\n";

			$x .= "===================================================================<br>\r\n";
			$x .= " eDisciplinev12 - Preset Approval Group Right setting (if any) [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################################
		###  eDisciplinev12 - Preset Approval Group Right setting (if any) [End]
		###########################################################################

		if(!$GSary['PresetSLPSetReportPeriod'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Preset SLP Set Report Period in table IPORTFOLIO_SETTING [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$student_ole_config_file = "$eclass_root/files/student_ole_config.txt";
			$filecontent = trim(get_file_content($student_ole_config_file));
			list($starttime, $sh, $sm, $endtime, $eh, $em, $int_on, $ext_on, $ex_starttime, $ex_sh, $ex_sm, $ex_endtime, $ex_eh, $ex_em, $temp) = unserialize($filecontent);

			$starttimeUpToMins = (trim($starttime)!="") ? sprintf($starttime." %02s:%02s:00", $sh, $sm) : "";
			$endtimeUpToMins = (trim($endtime)!="") ? sprintf($endtime." %02s:%02s:00", $eh, $em) : "";

			//$sql ='select * from '.$eclass_db.'.IPORTFOLIO_SETTING';

			//returnArr = $li->returnArray($sql);

			# insert

			$settingName = $ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriod"];
			$settingValue = $starttimeUpToMins.'##'.$endtimeUpToMins;

			$sql = "delete from
						{$eclass_db}.IPORTFOLIO_SETTING
					Where
						SETTING_NAME = '$settingName'
					";
			$success = $li->db_db_query($sql);

			$sql = "Insert Into {$eclass_db}.IPORTFOLIO_SETTING
						(SETTING_NAME, SETTING_VALUE, DATE_INPUT, INPUT_BY, DATE_MODIFIED, MODIFY_BY)
					Values
						('$settingName', '$settingValue', now(), '$UserID', now(),'$UserID')
					";
			$success = $li->db_db_query($sql);


			$settingName = $ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriodAllowSubmit"];

			$settingValue = $int_on;
			$sql = "delete from
						 {$eclass_db}.IPORTFOLIO_SETTING
					Where
						SETTING_NAME = '$settingName'
					";
			$success = $li->db_db_query($sql);

			$sql = "Insert Into {$eclass_db}.IPORTFOLIO_SETTING
						(SETTING_NAME, SETTING_VALUE, DATE_INPUT, INPUT_BY, DATE_MODIFIED, MODIFY_BY)
					Values
						('$settingName', '$settingValue', now(), '$UserID', now(),'$UserID')
					";
			$success = $li->db_db_query($sql);


			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'PresetSLPSetReportPeriod', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Preset SLP Set Report Period in table IPORTFOLIO_SETTING [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		#############################################################
		#############################################################
		###########################################################################
		###  eInventory - Data Patch for bulk items issues (item id + location id + group id + funding source id) [Start]
		###########################################################################
		if($plugin['Inventory'] && !$GSary['eInventoryBulkDataPatch2'])
		{
			$x .= "====================================================================================================================<br>\r\n";
			$x .= "  eInventory - Data Patch for bulk items issues (item id + location id + group id + funding source id) [Start]<br>\r\n";
			$x .= "====================================================================================================================<br>\r\n";


			include_once($PATH_WRT_ROOT."includes/libinventory.php");
			$linventory	= new libinventory();

			$update_table_ary = array();
			$update_table_ary[] = array("INVENTORY_ITEM_SURPLUS_RECORD","RecordID","ItemID","LocationID","AdminGroupID","FundingSourceID");
			$update_table_ary[] = array("INVENTORY_ITEM_MISSING_RECORD","RecordID","ItemID","LocationID","AdminGroupID","FundingSourceID");
			$update_table_ary[] = array("INVENTORY_ITEM_WRITE_OFF_RECORD","RecordID","ItemID","LocationID","AdminGroupID","FundingSourceID");
			$update_table_ary[] = array("INVENTORY_ITEM_BULK_LOG","RecordID","ItemID","LocationID","GroupInCharge","FundingSource");
			$update_table_ary[] = array("INVENTORY_VARIANCE_HANDLING_REMINDER","ReminderID","ItemID","NewLocationID","GroupInCharge","FundingSourceID");

			foreach($update_table_ary as $tk=>$this_table_fields)
			{
				list($this_table, $field_RecordID, $field_ItemID, $field_LocationID, $field_AdminGroupID, $field_FundingSourceID) = $this_table_fields;

				//$sql = "select ". $field_RecordID .", ". $field_ItemID .", ". $field_LocationID.", ". $field_AdminGroupID." from ". $this_table ." where ". $field_FundingSourceID." is NULL";
				$sql = "select ". $field_RecordID .", ". $field_ItemID .", ". $field_LocationID.", ". $field_AdminGroupID." from ". $this_table ." where (". $field_FundingSourceID." is NULL or ". $field_FundingSourceID." = 0)";
				$result = $linventory->returnArray($sql);
				if(!empty($result))
				{
					foreach($result as $k=>$d)
					{
						list($this_RecordID, $this_ItemID, $this_LocationID, $this_AdminGroupID) = $d;

						# check single/bulk
						$sql = "select ItemType from INVENTORY_ITEM where ItemID='$this_ItemID'";
						$temp = $linventory->returnVector($sql);
						$this_ItemType = $temp[0];
						if($this_ItemType)
						{
							if($this_ItemType==1)	# single
							{
								$sql1 = "select FundingSource, GroupInCharge from INVENTORY_ITEM_SINGLE_EXT where ItemID='$this_ItemID'";
							}
							else					# bulk
							{
								if($this_table=="INVENTORY_VARIANCE_HANDLING_REMINDER")
									$sql1 = "select FundingSourceID, GroupInCharge from INVENTORY_ITEM_BULK_LOCATION where ItemID='$this_ItemID' and LocationID='$this_LocationID'";
								else
									$sql1 = "select FundingSourceID from INVENTORY_ITEM_BULK_LOCATION where ItemID='$this_ItemID' and LocationID='$this_LocationID' and GroupInCharge='$this_AdminGroupID'";
							}
							$temp = $linventory->returnArray($sql1);
							list($this_FundingSourceID, $this_GroupID) = $temp[0];

							if($this_table=="INVENTORY_VARIANCE_HANDLING_REMINDER")
								$update_sql = "update ". $this_table ." set ". $field_FundingSourceID ."='$this_FundingSourceID', GroupInCharge='". $this_GroupID ."' where ". $field_RecordID ."='$this_RecordID'";
							else
								$update_sql = "update ". $this_table ." set ". $field_FundingSourceID ."='$this_FundingSourceID' where ". $field_RecordID ."='$this_RecordID'";
							$linventory->db_db_query($update_sql);
						}
					}
				}
			}

			# update General Settings - mark the script is executed
			$li->db = $intranet_db;
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eInventoryBulkDataPatch', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .=  "<span style='color:green'>Success</span>";

			$x .= "===================================================================<br>\r\n";
			$x .= "  eInventory - Data Patch for bulk items issues (item id + location id + group id + funding source id) [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

		}
		###########################################################################
		###  eInventory - Data Patch for bulk items issues (item id + location id + group id + funding source id) [End]
		###########################################################################

		###########################################################################
		###  eInventory - Set category, subcatgegory and funding default status as 'In Use' [START]
		###########################################################################
		if($plugin['Inventory'] && !$GSary['eInventoryFundingAndCategorySetInUse'])
		{
			$x .= "====================================================================================================================<br>\r\n";
			$x .= "  eInventory - Set category, subcatgegory and funding default status as 'In Use' [START]<br>\r\n";
			$x .= "====================================================================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/libinventory.php");
			$linventory	= new libinventory();

			$sql = "update INVENTORY_CATEGORY set RecordStatus=1";
			$linventory->db_db_query($sql);

			$sql = "update INVENTORY_CATEGORY_LEVEL2 set RecordStatus=1";
			$linventory->db_db_query($sql);

			$sql = "update INVENTORY_FUNDING_SOURCE set RecordStatus=1";
			$linventory->db_db_query($sql);

			# update General Settings - mark the script is executed
			$li->db = $intranet_db;
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eInventoryFundingAndCategorySetInUse', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "====================================================================================================================<br>\r\n";
			$x .= "  eInventory - Set category, subcatgegory and funding default status as 'In Use' [END]<br>\r\n";
			$x .= "====================================================================================================================<br>\r\n";

		}
		###########################################################################
		###  eInventory - Set category, subcatgegory and funding default status as 'In Use' [END]
		###########################################################################

		###########################################################################
		###  eInventory - single item need support 2 funding sources [START]
		###########################################################################
		if($plugin['Inventory'] && !$GSary['eInventorySingleItem2Funding'])
		{
			$x .= "====================================================================================================================<br>\r\n";
			$x .= "  eInventory - single item need support 2 funding sources [START]<br>\r\n";
			$x .= "====================================================================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/libinventory.php");
			$linventory	= new libinventory();

			$sql = "update INVENTORY_ITEM_SINGLE_EXT set UnitPrice1=UnitPrice where UnitPrice1 is NULL";
			$linventory->db_db_query($sql);

			# update General Settings - mark the script is executed
			$li->db = $intranet_db;
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eInventorySingleItem2Funding', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "====================================================================================================================<br>\r\n";
			$x .= "  eInventory - single item need support 2 funding sources [END]<br>\r\n";
			$x .= "====================================================================================================================<br>\r\n";

		}
		###########################################################################
		###  eInventory - single item need support 2 funding sources[END]
		###########################################################################


		###################################################
		### IES_COMBO: Add default marking criteria and default criteria weight[Start]
		###################################################
		if (!$GSary['IES_COMBO_AddDefaultMarkCriteria'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Add default marking criteria [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "INSERT IGNORE INTO IES_COMBO_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES (1, '總分', 'Score', 1, NOW(), 1, 1)";
			$li->db_db_query($sql);

			$sql = "INSERT IGNORE INTO IES_COMBO_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES (21, '過程', 'Process', 1, NOW(), 1, 1)";
			$li->db_db_query($sql);

			$sql = "INSERT IGNORE INTO IES_COMBO_MARKING_CRITERIA (MarkCriteriaID, TitleChi, TitleEng, isDefault, DateInput, InputBy, ModifyBy) ";
			$sql .= "VALUES (22, '課業', 'Task', 1, NOW(), 1, 1)";
			$li->db_db_query($sql);


			$sql = "UPDATE IES_COMBO_MARKING_CRITERIA ";
			$sql .= "SET defaultWeight = IF(MarkCriteriaID = 1, NULL, 1), defaultMaxScore = IF(MarkCriteriaID = 1, NULL, 9)";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_COMBO_AddDefaultMarkCriteria', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Add default marking criteria [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES_COMBO: Add default marking criteria and default criteria weight [End]
		###################################################

		###################################################
		### IES_COMBO: Create classroom for rubric [Start]
		###################################################
//		if (isset($plugin['IES_COMBO']) && $plugin['IES_COMBO'] && !$GSary['IES_COMBO_CreateClassroom'])
		if ((count($plugin['SBA']) > 0) && !$GSary['IES_COMBO_CreateClassroom'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Create classroom for rubric [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			define("CLASS_ROOM_NOT_EXIST", 0);

			include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $sba_cfg["IES_COMBO_ClassRoomType"];

			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) == CLASS_ROOM_NOT_EXIST))
			{
				$max_user = "NULL";
				$max_storage = "NULL";
				$course_code = $sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO'];
				$course_name = $sba_cfg['DB_IES_SCHEME_SchemeType']['IES_COMBO'];
				$course_desc = "eClass classroom for Module {$course_code}";

				$lo = new libeclass();

				$lo->setRoomType($roomType);

				$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);

				$sql = "UPDATE {$eclass_db}.course SET is_guest = 'yes' WHERE course_id = {$course_id}";
				$li->db_db_query($sql);

				$subj_id = -9; // $subj_id may not usefull
				$lo->eClassSubjectUpdate($subj_id, $course_id);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_COMBO_CreateClassroom', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Create classroom for rubric [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		###################################################
		### IES_COMBO: Set default rubric [Start]
		###################################################
		if ((count($plugin['SBA']) > 0) && !$GSary['IES_COMBO_ClassroomRubric_round2'])
		{
			//History , first plugin is "IES_COMBO_ClassroomRubric", but due to some problem (include a file wrongly), it cannot be run. Now change to "IES_COMBO_ClassroomRubric_round2" to ensure this plugin can be run again

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Set default rubric [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			define("CLASS_ROOM_NOT_EXIST", 0);

			include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $sba_cfg["IES_COMBO_ClassRoomType"];

			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) != CLASS_ROOM_NOT_EXIST))
			{

				$classRoomDB = classNamingDB($classRoomID);

				include_once($PATH_WRT_ROOT."includes/sba/initSettings/initRubricSQL.php"); //<-- this line must be under $classRoomDB = classNamingDB($classRoomID); , a global variable $classRoomDB is needed

				$sql = "TRUNCATE TABLE {$classRoomDB}.standard_rubric_set";
				$li->db_db_query($sql);
				$sql = "TRUNCATE TABLE {$classRoomDB}.standard_rubric";
				$li->db_db_query($sql);
				$sql = "TRUNCATE TABLE {$classRoomDB}.standard_rubric_detail";
				$li->db_db_query($sql);

				$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_set WRITE";
				$li->db_db_query($sql);
				$sql = $iesInitSQL['rubric']['sql1_stage'];

				$li->db_db_query($sql);
				$sql = "UNLOCK TABLES";
				$li->db_db_query($sql);

				$sql = "LOCK TABLES {$classRoomDB}.standard_rubric WRITE";
				$li->db_db_query($sql);
				$sql = $iesInitSQL['rubric']['sql2_stage'];

				$li->db_db_query($sql);
				$sql = "UNLOCK TABLES";
				$li->db_db_query($sql);

				$sql = "LOCK TABLES {$classRoomDB}.standard_rubric_detail WRITE";
				$li->db_db_query($sql);
				$sql = $iesInitSQL['rubric']['stage_detail1'];

				$li->db_db_query($sql);
				$sql = "UNLOCK TABLES";
				$li->db_db_query($sql);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_COMBO_ClassroomRubric_round2', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Set default rubric [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES_COMBO: Set default rubric [End]
		###################################################

		###################################################
		### IES_COMBO: Add new default rubric [Start]
		###################################################
		if ((count($plugin['SBA']) > 0) && !$GSary['IES_COMBO_ClassroomRubric_round3'] && $plugin['SBA_HKEAA_2017_Rubrics'])
		{

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Add new default rubric [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			define("CLASS_ROOM_NOT_EXIST", 0);

			include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $sba_cfg["IES_COMBO_ClassRoomType"];

			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) != CLASS_ROOM_NOT_EXIST))
			{

				$classRoomDB = classNamingDB($classRoomID);

				include_once($PATH_WRT_ROOT."includes/sba/initSettings/initRubricSQL2.php"); //<-- this line must be under $classRoomDB = classNamingDB($classRoomID); , a global variable $classRoomDB is needed

			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_COMBO_ClassroomRubric_round3', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Add new default rubric [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES_COMBO: Add new default rubric [End]
		###################################################


		###################################################
		### IES_COMBO: Add default teacher account to classroom [Start]
		###################################################
		if ((count($plugin['SBA']) > 0) && !$GSary['IES_COMBO_RubricManager'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Add classroom teacher account to manage rubric [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			define("CLASS_ROOM_NOT_EXIST", 0);

			include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			$roomType = $sba_cfg["IES_COMBO_ClassRoomType"];

			$classRoomID = getEClassRoomID($roomType);

			if((intval($classRoomID) != CLASS_ROOM_NOT_EXIST))
			{
				$lo = new libeclass($classRoomID);
				$lo->setRoomType($roomType);

				//	it is ies combo , but using "$ies_cfg['DB_user_course_user_email']["rubric_manager"]" is also ok
				$lo->eClassUserAddFullInfo($ies_cfg['DB_user_course_user_email']["rubric_manager"], "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "T", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL");
				$lo->eClassUserNumber($lo->course_id);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_COMBO_RubricManager', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Add classroom teacher account to manage rubric [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES_COMBO: Add default teacher account to classroom [End]
		###################################################


		###################################################
		### IES_COMBO: Patch criteria sequence [Start]
		###################################################
		if ((count($plugin['SBA']) > 0) && !$GSary['IES_COMBO_PatchCriteriaSeq'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Patch criteria sequence [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/sba/sbaConfig.inc.php");

			$sql = "UPDATE IES_COMBO_MARKING_CRITERIA SET defaultSequence = 9999 WHERE MarkCriteriaID = 1";
			$li->db_db_query($sql);

			$sql = "UPDATE IES_COMBO_MARKING_CRITERIA SET defaultSequence = 1 WHERE MarkCriteriaID = 21";
			$li->db_db_query($sql);

			$sql = "UPDATE IES_COMBO_MARKING_CRITERIA SET defaultSequence = 2 WHERE MarkCriteriaID = 22";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'IES_COMBO_PatchCriteriaSeq', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "IES_COMBO: Patch criteria sequence [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### IES_COMBO: Patch criteria sequence [End]
		###################################################


		###################################################
		### Synchronize Websams Regno From Ipf to Intranet User [Start]
		###################################################
		if ($plugin['iPortfolio'] && !$GSary['synWebSamsRegnoFromIpfToIntranetUser'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize Websams Regno From Ipf to Intranet User [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$noOfRecordUpdated = synWSRegnoToIntranetUser();
			$x .= "No of Record Updated : ".$noOfRecordUpdated."<br>\r\n";

			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize Websams Regno From Ipf to Intranet User  [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Synchronize Websams Regno From Ipf to Intranet User [Start]
		###################################################



		###################################################
		### Synchronize additional student info from ipf to Account Mgmt > Student Mgmt [Start]
		### Nationality, Place of birth, Admission Date
		###################################################
		if ($plugin['iPortfolio'] && !$GSary['SyncIportfolioStudentInfoToStudentMgmt'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize additional student info from ipf to Account Mgmt > Student Mgmt [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";


			$sql = "INSERT IGNORE INTO INTRANET_USER_PERSONAL_SETTINGS (UserID, Nationality,Nationality_DateModified,PlaceOfBirth,PlaceOfBirth_DateModified,AdmissionDate,AdmissionDate_DateModified)
					SELECT UserID, Nationality, ModifiedDate as d1, PlaceOfBirth, ModifiedDate as d2, AdmissionDate, ModifiedDate as d3
					FROM {$eclass_db}.PORTFOLIO_STUDENT
			";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SyncIportfolioStudentInfoToStudentMgmt', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Synchronize additional student info from ipf to Account Mgmt > Student Mgmt [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Synchronize additional student info from ipf to Account Mgmt > Student Mgmt [End]
		###################################################


		###################################################
		### eDiscipliev12 : Alter Table DISCIPLINE_MERIT_RECORD, DISCIPLINE_STUDENT_CONDUCT_BALANCE, DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG, change Data Type of "ConductScoreChange" to float [Start]
		###################################################
		if ($sys_custom['eDiscipline']['ConductMark1DecimalPlace'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eDiscipliev12 : Alter Table DISCIPLINE_MERIT_RECORD, DISCIPLINE_STUDENT_CONDUCT_BALANCE, DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG, change Data Type of \"ConductScoreChange\" to float [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";

			// change DISCIPLINE_MERIT_RECORD
			$sql = "ALTER TABLE DISCIPLINE_MERIT_RECORD change ConductScoreChange ConductScoreChange float(8,1)";
			$li->db_db_query($sql);

			// change DISCIPLINE_STUDENT_CONDUCT_BALANCE
			$sql = "ALTER TABLE DISCIPLINE_STUDENT_CONDUCT_BALANCE change ConductScore ConductScore float(8,1)";
			$li->db_db_query($sql);

			// change DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG
			$sql = "ALTER TABLE DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG change FromScore FromScore float(8,1), change ToScore ToScore float(8,1)";
			$li->db_db_query($sql);

			// change DISCIPLINE_CONDUCT_ADJUSTMENT
			$sql = "ALTER TABLE DISCIPLINE_CONDUCT_ADJUSTMENT change AdjustMark AdjustMark float(8,1)";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StoreConductMarkInFloat', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eDiscipliev12 : Alter Table DISCIPLINE_MERIT_RECORD, DISCIPLINE_STUDENT_CONDUCT_BALANCE, DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG, change Data Type of \"ConductScoreChange\" to float [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### eDiscipliev12 : Alter Table DISCIPLINE_MERIT_RECORD, DISCIPLINE_STUDENT_CONDUCT_BALANCE, DISCIPLINE_CONDUCT_SCORE_CHANGE_LOG, change Data Type of "ConductScoreChange" to float [End]
		###################################################

		###########################################################
		###  Insert poemsandsongs to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_poemsandsongs_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert poemsandsongs to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'poemsandsongs'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('poemsandsongs', 'Poems and Songs', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_poemsandsongs_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert poemsandsongs to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert poemsandsongs to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  eEnrol - Migrate old quota settings to new quota settings table [Start]
		###########################################################
		if(!$GSary['eEnrol_quota_settings_migration'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eEnrol - Migrate old quota settings to new quota settings table [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
			$libenroll = new libclubsenrol();

			$CategoryIDArr = $libenroll->GET_CATEGORY_LIST();
			$CategoryIDArr = Get_Array_By_Key($CategoryIDArr, "CategoryID");

			$eEnrolSettingsArr = $lgs->Get_General_Setting($libenroll->ModuleTitle);

			$oldQuotaSettingsAry = array();
			$oldQuotaSettingsAry[0]['Club']['DefaultMin'] = $eEnrolSettingsArr['Club_DefaultMin'];
			$oldQuotaSettingsAry[0]['Club']['DefaultMax'] = $eEnrolSettingsArr['Club_DefaultMax'];
			$oldQuotaSettingsAry[0]['Club']['EnrollMax'] = $eEnrolSettingsArr['Club_EnrollMax'];
//			$oldQuotaSettingsAry[0]['Activity']['DefaultMin'] = $eEnrolSettingsArr['Activity_DefaultMin'];
//			$oldQuotaSettingsAry[0]['Activity']['DefaultMax'] = $eEnrolSettingsArr['Activity_DefaultMax'];
//			$oldQuotaSettingsAry[0]['Activity']['EnrollMax'] = $eEnrolSettingsArr['Activity_EnrollMax'];

			foreach((array)$CategoryIDArr as $CategoryID) {
				$oldQuotaSettingsAry[$CategoryID]['Club']['DefaultMin'] = $eEnrolSettingsArr['Club_DefaultMin'.$CategoryID];
				$oldQuotaSettingsAry[$CategoryID]['Club']['DefaultMax'] = $eEnrolSettingsArr['Club_DefaultMax'.$CategoryID];
				$oldQuotaSettingsAry[$CategoryID]['Club']['EnrollMax'] = $eEnrolSettingsArr['Club_EnrollMax'.$CategoryID];
//				$oldQuotaSettingsAry[$CategoryID]['Activity']['DefaultMin'] = $eEnrolSettingsArr['Activity_DefaultMin'.$CategoryID];
//				$oldQuotaSettingsAry[$CategoryID]['Activity']['DefaultMax'] = $eEnrolSettingsArr['Activity_DefaultMax'.$CategoryID];
//				$oldQuotaSettingsAry[$CategoryID]['Activity']['EnrollMax'] = $eEnrolSettingsArr['Activity_EnrollMax'.$CategoryID];
			}

			//$recordTypeAry = array('Club', 'Activity');
			$recordTypeAry = array('Club');
			$numOfRecordType = count($recordTypeAry);

			$CategoryIDArr[] = 0;
			$numOfCategory = count($CategoryIDArr);

			$sql = "Select * From ACADEMIC_YEAR_TERM where AcademicYearID = '".Get_Current_Academic_Year_ID()."'";
			$yearTermAry = $li->returnResultSet($sql);
			$numOfTerm = count($yearTermAry);

			$successAry = array();
			for ($i=0; $i<$numOfCategory; $i++) {
				$_categoryId = $CategoryIDArr[$i];

				for ($j=0; $j<$numOfRecordType; $j++) {
					$__recordType = $recordTypeAry[$j];

					$__applyMin = $oldQuotaSettingsAry[$_categoryId][$__recordType]['DefaultMin'];
					$__applyMax = $oldQuotaSettingsAry[$_categoryId][$__recordType]['DefaultMax'];
					$__enrollMax = $oldQuotaSettingsAry[$_categoryId][$__recordType]['EnrollMax'];

					for ($k=0; $k<$numOfTerm+1; $k++) {
						$___termNum = $k;	// o for whole year, 1 for 1st Term, 2 for 2nd Term

						$sql = "Insert Into INTRANET_ENROL_QUOTA_SETTING
									(TermNumber, CategoryID, SettingName, SettingValue, RecordType, DateInput)
								Values
									('".$___termNum."', '".$_categoryId."', 'ApplyMin', '".$__applyMin."', '".$__recordType."', now()),
									('".$___termNum."', '".$_categoryId."', 'ApplyMax', '".$__applyMax."', '".$__recordType."', now()),
									('".$___termNum."', '".$_categoryId."', 'EnrollMax', '".$__enrollMax."', '".$__recordType."', now())
								";
						$successAry[$_categoryId][$__recordType][$___termNum] = $li->db_db_query($sql);
					}
				}
			}

			if (in_array(false, $successAry)) {
				$x .= "<font color='red'><b>FAILED</b></font>!!!<br>\r\n";
			}
			else {
				$x .= "<font color='green'>Success</font><br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrol_quota_settings_migration', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eEnrol - Migrate old quota settings to new quota settings table [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  eEnrol - Migrate old quota settings to new quota settings table [End]
		###########################################################

		#####################################################################
		### Clean Module Delete Log records older than two months [Start] ###
		#####################################################################

		$delete_log_month_period = isset($sys_custom['AddonSchema']['ModuleDeleteLogCleanPeriod'])? $sys_custom['AddonSchema']['ModuleDeleteLogCleanPeriod'] : 12;
		$x .= "===================================================================<br>\r\n";
		$x .= "Clean Module Delete Log records older than $delete_log_month_period months [Start] <br>\r\n";
		$x .= "===================================================================<br>\r\n";

		$current_ts = time();
		$target_date = date("Y-m-d 00:00:00", mktime(0,0,0,intval(date("n",$current_ts)-$delete_log_month_period),intval(date("j",$current_ts)),intval(date("Y",$current_ts))));
		$sql = "DELETE FROM MODULE_RECORD_DELETE_LOG WHERE Module NOT IN ('DISCIPLINE','eInventory') AND LogDate < '".$target_date."'";
		$li->db_db_query($sql) or debug_pr(mysql_error());

		$x .= "===================================================================<br>\r\n";
		$x .= "Clean Module Delete Log records older than $delete_log_month_period months [End] <br>\r\n";
		$x .= "===================================================================<br>\r\n";

		#####################################################################
		### Clean Module Delete Log records older than two months [End]   ###
		#####################################################################


		###########################################################
		###  Insert default value to PRESET_CODE_OPTION [Start]
		###########################################################
		if(!$GSary['Insert_default_value_PRESET_CODE_OPTION'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert default value to PRESET_CODE_OPTION [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."addon/ip25/create preset_code_option.php");

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_default_value_PRESET_CODE_OPTION', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert default value to PRESET_CODE_OPTION [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert poemsandsongs to PRESET_CODE_OPTION [End]
		###########################################################


		###########################################################
		###  Insert default value to PRESET_CODE_OPTION_Kentville [Start]
		###########################################################
		if(!$GSary['Insert_default_value_PRESET_CODE_OPTION_Kentville'] && $sys_custom['StudentRegistry']['Kentville'])
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "Insert default value to PRESET_CODE_OPTION_Kentville [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    include_once($PATH_WRT_ROOT."addon/ip25/create preset_code_option_2.php");

		    # update General Settings - mark the script is executed
		    $sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_default_value_PRESET_CODE_OPTION_Kentville', 1, now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

		    $x .= "===================================================================<br>\r\n";
		    $x .= "Insert default value to PRESET_CODE_OPTION_Kentville [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert default value to PRESET_CODE_OPTION_Kentville [End]
		###########################################################


		###########################################################
		###  Patch Term End Date Time to 23:59:59 [Start]
		###########################################################
		if(!$GSary['Patch_AcademicYearTerm_EndDate_Time_To_23:59:59'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Patch Term End Date Time to 23:59:59 [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$tmpSuccessAry = array();

			$sql = "Select YearTermID, TermEnd From ACADEMIC_YEAR_TERM";
			$termAry = $li->returnResultSet($sql);
			$numOfTerm = count($termAry);
			for ($i=0; $i<$numOfTerm; $i++) {
				$_yearTermId = $termAry[$i]['YearTermID'];
				$_termEnd = $termAry[$i]['TermEnd'];

				$_time = substr($_termEnd, 11, 8);
				if ($_time == '00:00:00') {
					$_termEnd = str_replace('00:00:00', '23:59:59', $_termEnd);
				}

				$sql = "Update ACADEMIC_YEAR_TERM Set TermEnd = '".$_termEnd."' Where YearTermID = '".$_yearTermId."'";
				$tmpSuccessAry[] = $li->db_db_query($sql);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Patch_AcademicYearTerm_EndDate_Time_To_23:59:59', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			if (in_array(false, $tmpSuccessAry)) {
				$x .= "<font color='red'><b>FAILED</b></font>!!!<br>\r\n";
			}
			else {
				$x .= "<font color='green'>Success</font><br>\r\n";
			}

			$x .= "===================================================================<br>\r\n";
			$x .= "Patch Term End Date Time to 23:59:59 [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Patch Term End Date Time to 23:59:59 [End]
		###########################################################

		###################################################
		### Standardize the subject code and id to null if there is no data (old data may be empty string, null or zero)
		###################################################
		if ($plugin['iPortfolio'] && !$GSary['SyncIportfolioAssessmentResultNoSubjectIdDataToNull'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Standardize the subject code and id to null if there is no data (old data may be empty string, null or zero) [Start]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";


			$sql = "Update {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD Set SubjectComponentCode = null where SubjectComponentCode = '' or SubjectComponentCode = 0";
			$li->db_db_query($sql);
			$sql = "Update {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD Set SubjectComponentID = null where SubjectComponentID = '' or SubjectComponentID = 0";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SyncIportfolioAssessmentResultNoSubjectIdDataToNull', 1, now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Standardize the subject code and id to null if there is no data (old data may be empty string, null or zero) [End]</b><br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###################################################
		### Standardize the subject code and id to null if there is no data (old data may be empty string, null or zero) [End]
		###################################################


		###########################################################
		###  eInventory - data patch for duplicate [ItemID + GroupInCharge + LocationID + FundingSourceID] [Start]
		###########################################################
		if(!$GSary['eInventory_DataPatch_BULK_LOCATION'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eInventory - data patch for duplicate [ItemID + GroupInCharge + LocationID + FundingSourceID] [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# find any duplicate records
			$sql = "select ItemID, GroupInCharge, LocationID, FundingSourceID, sum(Quantity), count(*) as c from INVENTORY_ITEM_BULK_LOCATION group by ItemID, GroupInCharge, LocationId, FundingSourceID having c>1";
			$result = $li->returnArray($sql);
			if(!empty($result))
			{
				foreach($result as $k2=>$d2)
				{
					list($thisItemID, $thisGroupInCharge, $thisLocationID, $thisFundingSourceID, $thisTotalQty) = $d2;

					$sql_del = "delete from INVENTORY_ITEM_BULK_LOCATION where ItemID='$thisItemID' and GroupInCharge='$thisGroupInCharge' and LocationID='$thisLocationID' and FundingSourceID='$thisFundingSourceID'";
					$li->db_db_query($sql_del);

					$sql_insert = "insert into INVENTORY_ITEM_BULK_LOCATION (ItemID, GroupInCharge, LocationID, FundingSourceID, Quantity)
									values
									($thisItemID, $thisGroupInCharge, $thisLocationID, $thisFundingSourceID, $thisTotalQty)";
					$li->db_db_query($sql_insert);
				}
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eInventory_DataPatch_BULK_LOCATION', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eInventory - data patch for duplicate [ItemID + GroupInCharge + LocationID + FundingSourceID] [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  eInventory - data patch for duplicate [ItemID + GroupInCharge + LocationID + FundingSourceID] [End]
		###########################################################



		########################################################
		### Add field IsSystemPublic to EPOST_FOLDER [Start]
		########################################################

		if($plugin['ePost'] && !$GSary['Add_IsSystemPublic_to_EPOST_FOLDER'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Add field IsSystemPublic to EPOST_FOLDER [Start]<br/>\r\n";
			$x .= "===================================================================<br>\r\n";
			$x .= "<br>\r\n";

			$sql = "SHOW COLUMNS FROM EPOST_FOLDER LIKE 'IsSystemPublic'";
			$system_public_field_exist = count($li->returnArray($sql)) > 0;

			if(!$system_public_field_exist){
				$sql = "ALTER TABLE EPOST_FOLDER ADD IsSystemPublic tinyint(1) DEFAULT 0 AFTER Status";
				$add_system_public_field_result = $li->db_db_query($sql);

				if($add_system_public_field_result){
					$sql = "SELECT
								ef.FolderID
							FROM
								EPOST_FOLDER AS ef INNER JOIN
								INTRANET_USER AS iu ON ef.ModifiedBy = iu.UserID
							WHERE
								ef.Title = 'Public' AND
								ef.Status = 0 AND
								iu.UserLogin = 'broadlearning' AND
								iu.RecordStatus = 1
							ORDER BY
								ef.ModifiedDate ASC";
					$system_default_folder_id = current($li->returnVector($sql));

					if($system_default_folder_id){
						$sql = "UPDATE EPOST_FOLDER SET IsSystemPublic = 1 WHERE FolderID = '$system_default_folder_id'";
						$set_system_public_result = $li->db_db_query($sql);

						$x .= ($set_system_public_result? "Update 'IsSystemPublic' to System Public Folder Successfully":"Failed to update 'IsSystemPublic' to System Public Folder")."<br>\r\n";
					}
					else{
						$x .= "No System Public Folder Found<br>\r\n";

						$sql = "SELECT UserID FROM INTRANET_USER WHERE UserLogin = 'broadlearning'";
						$BLUserID = current($li->returnVector($sql));

						$sql = "INSERT INTO EPOST_FOLDER (Title, Status, IsSystemPublic, InputDate, ModifiedBy, ModifiedDate) VALUES ('Public', 0, 1, NOW(), '$BLUserID', NOW())";
						$create_system_public_result = $li->db_db_query($sql);

						$x .= ($create_system_public_result? "Create System Public Folder Successfully":"Fail to Create System Public Folder")."<br>\r\n";
					}
				}
				else{
					$x .= "Failed to Add Field 'IsSystemPublic' to EPOST_FOLDER table<br>\r\n";
				}
			}
			else{
				$x .= "Field 'IsSystemPublic' already exists in EPOST_FOLDER table<br>\r\n";
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Add_IsSystemPublic_to_EPOST_FOLDER', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "<br>\r\n";
			$x .= "===================================================================<br>\r\n";
			$x .= "Add field IsSystemPublic to EPOST_FOLDER [End]<br/>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		########################################################
		### Add field IsSystemPublic to EPOST_FOLDER [End]
		########################################################



		##########################################################
		### Add table EPOST_WRITING_COMMENT [Start]
		##########################################################

		if($plugin['ePost'] && !$GSary['Add_EPOST_WRITING_COMMENT'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Add table EPOST_WRITING_COMMENT [Start]<br/>\r\n";
			$x .= "===================================================================<br>\r\n";
			$x .= "<br>\r\n";

			$sql = "CREATE TABLE `EPOST_WRITING_COMMENT` (
						`CommentID` int(11) NOT NULL auto_increment,
						`WritingID` int(11) NOT NULL default '0',
						`Content` text,
						`Status` tinyint(1) default '0',
						`InputBy` int(11) default '0',
						`InputDate` datetime default NULL,
						`ModifiedBy` int(11) default '0',
						`ModifiedDate` datetime default NULL,
						PRIMARY KEY  (`CommentID`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";

			$result = true;
			$table_name = 'EPOST_WRITING_COMMENT';

			$check_sql    = "SHOW TABLES LIKE '$table_name'";
			$check_result = current($li->returnVector($check_sql));

			if($check_result==$table_name){
				$check_sql = "SELECT COUNT(*) FROM $table_name";
				$check_result = current($li->returnVector($check_sql));

				if($check_result>0){
					$result = false;
					$result_str = "Table already exists with records!";
				}
				else{
					$check_sql = "DROP TABLE $table_name";
					$li->db_db_query($check_sql);

					$result_str = "Table already exists with no records!<br/>Table dropped to ensure table with latest schema is created!<br/>";
				}
			}

			if($result){
				$result = $li->db_db_query($sql);
				$result_str .= $result? "Table created":"Failed to create table";
			}

			$x .= ($result? "Created":"Failed")."<br/>$result_str";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Add_EPOST_WRITING_COMMENT', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "<br>\r\n";
			$x .= "===================================================================<br>\r\n";
			$x .= "Add table EPOST_WRITING_COMMENT [End]<br/>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		########################################################
		### Add table EPOST_WRITING_COMMENT [End]
		########################################################



		##########################################################
		### Add table EPOST_GENERAL_SETTINGS [Start]
		##########################################################

		if($plugin['ePost'] && !$GSary['Add_EPOST_GENERAL_SETTINGS'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Add table EPOST_GENERAL_SETTINGS [Start]<br/>\r\n";
			$x .= "===================================================================<br>\r\n";
			$x .= "<br>\r\n";

			$sql = "CREATE TABLE `EPOST_GENERAL_SETTINGS` (
						`SettingID` int(11) NOT NULL auto_increment,
						`SettingName` varchar(100) NOT NULL default '',
						`SettingValue` text,
						`TargetUserID` int(11) NOT NULL default '0',
						`InputBy` int(11) default '0',
						`InputDate` datetime default NULL,
						`ModifiedBy` int(11) default '0',
						`ModifiedDate` datetime default NULL,
						PRIMARY KEY  (`SettingID`),
						UNIQUE KEY UserSetting (SettingName, TargetUserID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";

			$result = true;
			$table_name = 'EPOST_GENERAL_SETTINGS';

			$check_sql    = "SHOW TABLES LIKE '$table_name'";
			$check_result = current($li->returnVector($check_sql));

			if($check_result==$table_name){
				$check_sql = "SELECT COUNT(*) FROM $table_name";
				$check_result = current($li->returnVector($check_sql));

				if($check_result>0){
					$result = false;
					$result_str = "Table already exists with records!";
				}
				else{
					$check_sql = "DROP TABLE $table_name";
					$li->db_db_query($check_sql);

					$result_str = "Table already exists with no records!<br/>Table dropped to ensure table with latest schema is created!<br/>";
				}
			}

			if($result){
				$result = $li->db_db_query($sql);
				$result_str .= $result? "Table created":"Failed to create table";
			}

			$x .= ($result? "Created":"Failed")."<br/>$result_str";

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Add_EPOST_GENERAL_SETTINGS', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "<br>\r\n";
			$x .= "===================================================================<br>\r\n";
			$x .= "Add table EPOST_GENERAL_SETTINGS [End]<br/>\r\n";
			$x .= "===================================================================<br>\r\n";
		}

		########################################################
		### Add table EPOST_GENERAL_SETTINGS [End]
		########################################################

		###########################################################
		###  eInventory - data patch for Bulk item offline stocktake date[Start]
		###########################################################
		if(!$GSary['eInventory_DataPatch_BULK_offline_stocktake_date'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eInventory - data patch for Bulk item offline stocktake date [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "update INVENTORY_ITEM_BULK_LOG set RecordDate=left(DateInput, 10) where Action=2";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eInventory_DataPatch_BULK_offline_stocktake_date', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eInventory - data patch for Bulk item offline stocktake date [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  eInventory - data patch for Bulk item offline stocktake date [End]
		###########################################################

		###########################################################
		###  Update Publisher of eBook (1596) [Start] // 20140212-updatePublishereBook1596
		###########################################################
		$x .= "===================================================================<br>\r\n";
		$x .= "Update Publisher of eBook (1596) [Start]<br>\r\n";
		$x .= "===================================================================<br>\r\n";

		$sql = "UPDATE INTRANET_ELIB_BOOK Set Publisher = '突破' Where BookID = '1596'";
		$li->db_db_query($sql) or debug_pr(mysql_error());

		$x .= "===================================================================<br>\r\n";
		$x .= "Update Publisher of eBook (1596) [End]<br>\r\n";
		$x .= "===================================================================<br>\r\n";
		###########################################################
		###  Update Publisher of eBook (1596) [End]
		###########################################################

		###########################################################
		###  Update eBook data  [Start]  20141205-updateebookdata
		###########################################################
		$x .= "===================================================================<br>\r\n";
		$x .= "Update eBook data [Start] <br>\r\n";
		$x .= "===================================================================<br>\r\n";
		if(file_exists($intranet_root."/addon/ip25/ebook_info_change.php")){
			include_once($intranet_root."/addon/ip25/ebook_info_change.php");
			updateEbookData("ebook_info_change_20141205");
		}
		$x .= "===================================================================<br>\r\n";
		$x .= "Update eBook data [End]<br>\r\n";
		$x .= "===================================================================<br>\r\n";
		###########################################################
		###  Update eBook data  [End]
		###########################################################

		###########################################################################
		# Change HKID field to varchar(14) for Malaysia clients [Start]
		###########################################################################
		if(get_client_region() == 'zh_MY'){
			$sql = "alter table INTRANET_USER modify column HKID varchar(14) default null";
			$li->db_db_query($sql);
			$x .= "=======================================================================<br>\r\n";
			$x .= "INTRANET_USER.HKID is changed to varchar(14) [End]</b><br>\r\n";
			$x .= "=======================================================================<br>\r\n";
		}
		###########################################################################
		# Change HKID field to varchar(14) for Malaysia clients [End]
		###########################################################################

		###########################################################################
		#  Insert default value to MEDICAL_STUDENT_LOG_LEV3, MEDICAL_STUDENT_LOG_LEV4 [Start]
		###########################################################################
		if($plugin['medical'])
		{
			if(!$GSary['MEDICAL_STUDENT_LOG'])
			{
				$x .= "==========================================================================<br>\r\n";
				$x .= "Insert default value to MEDICAL_STUDENT_LOG_LEV3, MEDICAL_STUDENT_LOG_LEV4 [Start]</b><br>\r\n";
				$x .= "==========================================================================<br>\r\n";

				include_once($PATH_WRT_ROOT."includes/cust/medical/install/initStudentLogLev3Lev4Data.php");

				$x .= "=======================================================================<br>\r\n";
				$x .= "Insert default value to MEDICAL_STUDENT_LOG_LEV3, MEDICAL_STUDENT_LOG_LEV4 [End]</b><br>\r\n";
				$x .= "=======================================================================<br>\r\n";
			}
		}
		###########################################################################
		#  Insert default value to MEDICAL_STUDENT_LOG_LEV3, MEDICAL_STUDENT_LOG_LEV4 [End]
		###########################################################################

		#################################################################################
		###  medical - update DateInput and InputBy for event, event followup, case, award scheme and revisit [Start]
		#################################################################################
		if(!$GSary['medical_update_EventDateInputBy'] && $plugin['medical'])
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "medical - update DateInput and InputBy for event, event followup, case, award scheme and revisit  [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    $sqlAry = array();
		    $sqlAry[] = "UPDATE MEDICAL_EVENT SET DateInput=DateModified WHERE DateInput is null;";
		    $sqlAry[] = "UPDATE MEDICAL_EVENT SET InputBy=LastModifiedBy WHERE InputBy is null;";
		    $sqlAry[] = "UPDATE MEDICAL_EVENT_FOLLOWUP SET DateInput=DateModified WHERE DateInput is null;";
		    $sqlAry[] = "UPDATE MEDICAL_EVENT_FOLLOWUP SET InputBy=LastModifiedBy WHERE InputBy is null;";
		    $sqlAry[] = "UPDATE MEDICAL_CASE SET DateInput=DateModified WHERE DateInput is null;";
		    $sqlAry[] = "UPDATE MEDICAL_CASE SET InputBy=LastModifiedBy WHERE InputBy is null;";
		    $sqlAry[] = "UPDATE MEDICAL_AWARD_SCHEME SET DateInput=DateModified WHERE DateInput is null;";
		    $sqlAry[] = "UPDATE MEDICAL_AWARD_SCHEME SET InputBy=LastModifiedBy WHERE InputBy is null;";
		    $sqlAry[] = "UPDATE MEDICAL_REVISIT SET DateInput=DateModified WHERE DateInput is null;";
		    $sqlAry[] = "UPDATE MEDICAL_REVISIT SET InputBy=LastModifiedBy WHERE InputBy is null;";

		    $li->db = $intranet_db;
		    foreach($sqlAry as $_sql) {
		        $li->db_db_query($_sql);
		    }

		    # update General Settings - marked the script is executed
		    $sql = "insert ignore into GENERAL_SETTING
		    (Module, SettingName, SettingValue, DateInput)
		    values
		    ('Medical', 'medical_update_EventDateInputBy', 1, now())";

		    $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

		    $x .= "===================================================================<br>\r\n";
		    $x .= "medical - update DateInput and InputBy for event, event followup, case, award scheme and revisit  [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  medical - update DateInput and InputBy for event, event followup, case, award scheme and revisit  [End]
		#################################################################################

		# Transform activity single category to multiple categories
		if($plugin['eEnrollment'] && $sys_custom['eEnrolment']['TWGHCYMA']){
			$customize_activity_to_multiple_categories_script = $intranet_root."/addon/script/enrollment/customize_activity_to_multiple_categories.php";
			if(file_exists($customize_activity_to_multiple_categories_script)){
				include_once($customize_activity_to_multiple_categories_script);
			}
		}

		#################################################################################
		###  eLibPlus - Preset Stock-take Period [Start]
		#################################################################################
		if(!$GSary['preset_eLib_stocktake_period'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/lms/preset_eLib_stocktake_period.php"))
		{
			include_once($intranet_root."/addon/script/lms/preset_eLib_stocktake_period.php");

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'preset_eLib_stocktake_period', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());
		}
		#################################################################################
		###  eLibPlus - Preset Stock-take Period [End]
		#################################################################################

		#################################################################################
		###  eLibPlus - syn the gender field between LIBMS_USER and INTRANET_USER [Start]
		#################################################################################
		if(!$GSary['syn_eLib_gender_field'] && $plugin['library_management_system'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - syn the gender field between LIBMS_USER and INTRANET_USER [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/libuser.php");
			$luser = new libuser();
			$successAry = array();
			$successAry['synUser'] = $luser->synUserDataToModules('', array('library_management_system'));
			//debug_pr($successAry);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'syn_eLib_gender_field', 1, now())";

			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - syn the gender field between LIBMS_USER and INTRANET_USER [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - syn the gender field between LIBMS_USER and INTRANET_USER [End]
		#################################################################################

		#################################################################################
		###  General - update the barcode of broadlearning account (INTRANET_USER and LIBMS_USER) [Start]
		#################################################################################
		if(!$GSary['update_broadlearning_account_barcode'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "General - update the barcode of broadlearning account (INTRANET_USER and LIBMS_USER) [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			# update broadlearning account barcode to broadlearning
			$sql = "UPDATE INTRANET_USER Set Barcode = 'broadlearning' Where UserLogin = 'broadlearning'";

			$li->db_db_query($sql) or debug_pr(mysql_error());

			if($plugin['library_management_system']){
				$sql = "UPDATE LIBMS_USER Set Barcode = 'broadlearning' Where UserLogin = 'broadlearning'";
				$liblms->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'update_broadlearning_account_barcode', 1, now())";

			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "General - update the barcode of broadlearning account (INTRANET_USER and LIBMS_USER) [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  General - update the barcode of broadlearning account (INTRANET_USER and LIBMS_USER) [End]
		#################################################################################

		#################################################################################
		### eLibPlus - syn the BookLanguageCode field between LIBMS_BOOK_LANGUAGE and LIBMS_BOOK [Start]
		#################################################################################
		if(!$GSary['patch_eLib_book_language2'] && $plugin['library_management_system'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - syn the BookLanguageCode field between LIBMS_BOOK_LANGUAGE and LIBMS_BOOK [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "select distinct Language from LIBMS_BOOK where Language != ''";
			$BookLanguageCode = $liblms->returnVector($sql);
			//debug_pr($BookLanguageCode);
			for ($i=0; $i<sizeof($BookLanguageCode); $i++)
			{
				$sql = "Select * From LIBMS_BOOK_LANGUAGE WHERE BookLanguageCode = '".$BookLanguageCode[$i]."'";
				$result = $liblms->returnArray($sql);

				if(!$result){
					$sql = "INSERT INTO LIBMS_BOOK_LANGUAGE (BookLanguageCode, DescriptionEn, DescriptionChi, DateModified) Values ('".$BookLanguageCode[$i]."','".$BookLanguageCode[$i]."','".$BookLanguageCode[$i]."',now())";
					//debug_pr($sql);
					$liblms->db_db_query($sql);
				} else
				{
					//debug("found: ", $BookLanguageCode[$i]);
				}
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING
					(Module, SettingName, SettingValue, DateInput)
					values
					('$ModuleName', 'patch_eLib_book_language2', 1, now())";

			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - syn the BookLanguageCode field between LIBMS_BOOK_LANGUAGE and LIBMS_BOOK [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		### eLibPlus - syn the BookLanguageCode field between LIBMS_BOOK_LANGUAGE and LIBMS_BOOK [End]
		#################################################################################

		#################################################################################
		### eLibPlus - syn the BookSubTitle field between LIBMS_BOOK and INTRANET_ELIB_BOOK [Start]
		#################################################################################
		if(!$GSary['syn_eLib_book_sub_title2'] && $plugin['library_management_system'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - syn the BookSubTitle field between LIBMS_BOOK and INTRANET_ELIB_BOOK [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "Select BookID From LIBMS_BOOK where BookSubTitle != ''";
			$BookID = $liblms->returnVector($sql);

			include_once($PATH_WRT_ROOT."includes/libelibrary.php");
			$libel = new elibrary();

			for ($i=0; $i<sizeof($BookID); $i++)
			{
				$libel->SYNC_PHYSICAL_BOOK_TO_ELIB($BookID[$i]);
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING
					(Module, SettingName, SettingValue, DateInput)
					values
					('$ModuleName', 'syn_eLib_book_sub_title2', 1, now())";

			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - syn the BookSubTitle field between LIBMS_BOOK and INTRANET_ELIB_BOOK [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		### eLibPlus - syn the BookSubTitle field between LIBMS_BOOK and INTRANET_ELIB_BOOK [End]
		#################################################################################

		#################################################################################
		### eLibPlus - syn the Item RecordStatus from LIBMS_BOOK_UNIQUE to LIBMS_STOCKTAKE_LOG [Start]
		#################################################################################
		if(!$GSary['syn_eLib_item_status_to_stocktake'] && $plugin['library_management_system'])
		{
		$x .= "===================================================================<br>\r\n";
		$x .= "eLibPlus - syn the Item RecordStatus from LIBMS_BOOK_UNIQUE to LIBMS_STOCKTAKE_LOG [Start]<br>\r\n";
		$x .= "===================================================================<br>\r\n";

		$sql = "select UniqueID, RecordStatus from LIBMS_BOOK_UNIQUE";
		$ItemResult = $liblms->returnArray($sql);

		for ($i=0; $i<sizeof($ItemResult); $i++)
		{
		if($ItemResult[$i]['RecordStatus'] == "BORROWED"){
		$ItemResult[$i]['RecordStatus'] = "NORMAL";
		}
		$sql = "UPDATE LIBMS_STOCKTAKE_LOG Set RecordStatus = '".$ItemResult[$i]['RecordStatus']."' Where BookUniqueID= '".$ItemResult[$i]['UniqueID']."' AND RecordStatus IS NULL";
		$liblms->db_db_query($sql) or debug_pr(mysql_error());
		}

		# update General Settings - mark the script is executed
		$sql = "insert ignore into GENERAL_SETTING
		(Module, SettingName, SettingValue, DateInput)
		values
		('$ModuleName', 'syn_eLib_item_status_to_stocktake', 1, now())";

		$li->db_db_query($sql) or debug_pr(mysql_error());

		$x .= "===================================================================<br>\r\n";
		$x .= "eLibPlus - syn the Item RecordStatus from LIBMS_BOOK_UNIQUE to LIBMS_STOCKTAKE_LOG [End]<br>\r\n";
		$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		### eLibPlus - syn the Item RecordStatus from LIBMS_BOOK_UNIQUE to LIBMS_STOCKTAKE_LOG [End]
		#################################################################################

		###########################################################
		###  Insert gvlistening to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_gvlistening_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert gvlistening to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'gvlistening'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('gvlistening', 'Global Voice Listening', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_gvlistening_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert gvlistening to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert gvlistening to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert gvlistening2 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_gvlistening2_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert gvlistening2 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'gvlistening2'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('gvlistening2', 'Global Voices 2', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_gvlistening2_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert gvlistening2 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert gvlistening2 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert gvlistening3 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_gvlistening3_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert gvlistening3 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'gvlistening3'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('gvlistening3', 'Global Voices 3', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_gvlistening3_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert gvlistening3 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert gvlistening3 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert rsreading1 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_rsreading1_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert rsreading1 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'rsreading1'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('rsreading1', 'Read to Succeed Level 1', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_rsreading1_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert rsreading1 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert rsreading1 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert rsreading2 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_rsreading2_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert rsreading2 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'rsreading2'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('rsreading2', 'Read to Succeed Level 2', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_rsreading2_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert rsreading2 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert rsreading2 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert rsreading3 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_rsreading3_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert rsreading3 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'rsreading3'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('rsreading3', 'Read to Succeed Level 3', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_rsreading3_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert rsreading3 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert rsreading3 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert grammartouch1 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_grammartouch1_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert grammartouch1 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'grammartouch1'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('grammartouch1', 'Grammar Touch Level 1', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_grammartouch1_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert grammartouch1 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert grammartouch1 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert grammartouch2 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_grammartouch2_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert grammartouch2 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'grammartouch2'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('grammartouch2', 'Grammar Touch Level 2', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_grammartouch2_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert grammartouch2 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert grammartouch2 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert grammartouch3 to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_grammartouch3_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert grammartouch3 to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'grammartouch3'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('grammartouch3', 'Grammar Touch Level 3', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_grammartouch3_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert grammartouch3 to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert grammartouch3 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  Insert kisworksheets to INTRANET_MODULE [Start]
		###########################################################
		if(!$GSary['Insert_kisworksheets_to_INTRANET_MODULE'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Insert kisworksheets to INTRANET_MODULE [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "SELECT COUNT(*) FROM INTRANET_MODULE WHERE Code = 'kisworksheets'";
			$insertb4 = current($li->returnVector($sql))? true : false;

			if(!$insertb4){
				$sql = "INSERT INTO INTRANET_MODULE (Code, Description, DateInput, DateModified) VALUE ('kisworksheets', 'KIS WorkSheets', NOW(), NOW())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Insert_kisworksheets_to_INTRANET_MODULE', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Insert kisworksheets to INTRANET_MODULE [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Insert gvlistening2 to INTRANET_MODULE [End]
		###########################################################

		###########################################################
		###  NON-SKH client only
		###  Set default 3 barcode label templates
		###########################################################
		if(!$GSary['NonSKH_set_barcode_templates'] && !$sys_custom['eInventoryCustForSKH'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "[NON-SKH client only] Set default 3 barcode label templates [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$sql = "insert into INVENTORY_LABEL_FORMAT 
						(name, paper_size_width, paper_size_height, metric, lMargin, tMargin, NX, NY, SpaceX, SpaceY, width, height, font_size, lineHeight, printing_margin_h, printing_margin_v, max_barcode_width, max_barcode_height, info_order_1, info_order_2, info_order_3, info_order_4, info_order_5, DateModified)
					values
						('30 Labels/Sheet (3X10) default',210,297,'mm',0,3,3,10,0,0,70,29.7,9,4, 0.5, 0.5, 28, 6, 'item_name', 'item_tag_code', 'item_code', 'item_admin_group', 'item_sublocation', now()),
						('40 Labels/Sheet (4X10) default',210,297,'mm',0,3,4,10,0,0,52.5,29.7, 8, 3.5, 0.5, 0.5, 28, 5, 'item_name', 'item_tag_code', 'item_code', 'item_admin_group', 'item_sublocation', now()),
						('24 Labels/Sheet (3X8) default',210,297,'mm',8,13,3,8,0,0,64.6,33.8,13,5,0.5,0.5, 28,7, 'item_name', 'item_tag_code', 'item_code', 'item_admin_group', 'item_sublocation', now())
					";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'NonSKH_set_barcode_templates', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "[NON-SKH client only] Set default 3 barcode label templates [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Set default 3 barcode label templates [End]
		###########################################################

		###########################################################
		### [Start] Change osapi file permission to rwxr-xr-x
		###########################################################
		if ($plugin['webmail'] || $plugin['imail_gamma'] || $plugin['personalfile'] /*$GSary['ChangeOsapiFilePermission20150130'] != 1*/)
		{
			$osapi_dir = $intranet_root."/osapi";

			$x .= "================================================================================<br />\r\n";
			$x .= " [Start] Change osapi file permission to rwxr-xr-x <br />\r\n";
			$x .= "================================================================================<br />\r\n";

			if(file_exists($osapi_dir)){
				$php_api_path = $osapi_dir."/*.php";
				$result1 = shell_exec("sudo chmod 0755 ".$php_api_path);
				if($result1 != ''){
					$x .= $result1."<br />\r\n";
				}else{
					$x .= "$php_api_path file permission is changed.<br />\r\n";
				}

				$asavapi_path = $osapi_dir."/asavapi/tools";
				if(file_exists($asavapi_path)){
					$result2 = shell_exec("sudo chmod 0755 ".$asavapi_path."/*.pl");
					$result3 = shell_exec("sudo chmod 0755 ".$asavapi_path."/*.sh");

					if($result2 != ''){
						$x .= $result2."<br />\r\n";
					}else{
						$x .= $asavapi_path."/*.pl file permission is changed.<br />\r\n";
					}
					if($result3 != ''){
						$x .= $result3."<br />\r\n";
					}else{
						$x .= $asavapi_path."/*.sh file permission is changed.<br />\r\n";
					}
				}

				$iusersapi_path = $osapi_dir."/iusersapi";
				if(file_exists($iusersapi_path)){
					$result4 = shell_exec("sudo chmod 0755 ".$iusersapi_path."/*.php");
					if($result4 != ''){
						$x .= $result4."<br />\r\n";
					}else{
						$x .= "$iusersapi_path file permission is changed.<br />\r\n";
					}
				}
			}

			# update General Settings - mark the script is executed
			//$sql = "insert ignore into GENERAL_SETTING
			//					(Module, SettingName, SettingValue, DateInput)
			//				values
			//					('$ModuleName', 'ChangeOsapiFilePermission20150130', 1, now())";
			//$li->db_db_query($sql) or debug_pr(mysql_error());
			// end handle daily log schema change

			$x .= "================================================================================<br />\r\n";
			$x .= " [End] Change osapi file permission to rwxr-xr-x <br />\n";
			$x .= "================================================================================<br />\r\n";
		}
		###########################################################
		### [End] Change osapi file permission to rwxr-xr-x
		###########################################################

		###########################################################
		###	[Start] Enrolment Comment Bank Migration
		###########################################################
		if(!$GSary['Enrolment_Comment_Bank_Migration'] )
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "[Enrolment Comment Bank Migration]  [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			//step 1 - Handle existing comments
			$sql = "UPDATE INTRANET_ENROL_COMMENT_BANK Set RecordType = 'A' Where RecordType = '' ";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			//step 2 - Migrating performance from admin console
			include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
			$lword = new libwordtemplates();
			$words = $lword->getWordList(8);

			if (count($words) > 0) {
				//$wordsArr= array();
				$importArr = array();
				$i=0;
				foreach($words as $comments){
					$_code = 'P'.str_pad(($i + 1), 3, "0", STR_PAD_LEFT);

					$importArr[] = " ('', '".$_code."', '".$li->Get_Safe_Sql_Query($comments)."', 'P', '1', now(), now()) ";

					$i++;
				}

				$importStr = implode(', ',$importArr);

				$sql = "insert into INTRANET_ENROL_COMMENT_BANK
							(CommentID, CommentCode, Comment, RecordType, RecordStatus, DateInput, DateModified)
						values
							$importStr
						";
				$thisSuccess = $li->db_db_query($sql);
			}
			else {
				$thisSuccess = true;
			}

			//Last Step - Record in db
			if ($thisSuccess){
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Enrolment_Comment_Bank_Migration', 1, now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			else{
				$x .= "Migration Fail ! <br>\r\n";
			}
			$x .= "===================================================================<br>\r\n";
			$x .= "[Enrolment Comment Bank Migration]  [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###	[End] Migrating Enrolment Comment Bank
		###########################################################

		######################################################################################
		### [Start] iPortfolio - Transfer Submission Period Settings in text file to DB
		######################################################################################

		if ($plugin['iPortfolio'] && !$GSary['TransferSubmissionPeriodSettings'])
		{
			include_once($PATH_WRT_ROOT."includes/libclass.php");

			$lc = new libclass();
			$levels = $lc->getLevelArray();

			$settingsAry = array();
			$SettingsType = array("OLE", "EXT", "SLP");

			$x .= "===============================================================================<br>\r\n";
			$x .= " [Start] iPortfolio - Transfer Submission Period Settings in text file to DB <br>\r\n";
			$x .= "===============================================================================<br>\r\n";

			if(file_exists("$eclass_root/files/student_ole_config.txt"))
			{
				## read Submission Period Settings from config file
				$student_ole_config_file = "$eclass_root/files/student_ole_config.txt";
				$filecontent = trim(get_file_content($student_ole_config_file));
				list($starttime, $sh, $sm, $endtime, $eh, $em, $check_int,
					$check_ext, $ex_starttime, $ex_sh, $ex_sm, $ex_endtime, $ex_eh, $ex_em) = unserialize($filecontent);

				# OLE Submission Settings
				$starttimeFormat = (trim($starttime)!="")? sprintf($starttime." %02s:%02s:00", $sh, $sm) : "";
				$endtimeFormat = (trim($endtime)!="")? sprintf($endtime." %02s:%02s:59", $eh, $em) : "";
				$checkintValue = (trim($check_int)=="on")? '1' : '0';

				# EXT Submission Settings
				$extstarttimeFormat = (trim($ex_starttime)!="")? sprintf($ex_starttime." %02s:%02s:00", $ex_sh, $ex_sm) : "";
				$extendtimeFormat = (trim($ex_endtime)!="")? sprintf($ex_endtime." %02s:%02s:59", $ex_eh, $ex_em) : "";
				$checkextValue = (trim($check_ext)=="on")? '1' : '0';

				$settingsAry[0] = array($starttimeFormat, $endtimeFormat, $checkintValue);
				$settingsAry[1] = array($extstarttimeFormat, $extendtimeFormat, $checkextValue);
			}

			## read SLP Report Selection Record from DB
			$sql = "SELECT SETTING_VALUE, SETTING_NAME FROM {$eclass_db}.IPORTFOLIO_SETTING
					WHERE SETTING_NAME IN ('".$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriod"]."', 
											'".$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriodAllowSubmit"]."')";
			$SLPSettings = $li->returnResultSet($sql);
			$SLPSettings = BuildMultiKeyAssoc($SLPSettings, "SETTING_NAME");

			$SLPPeriod = $SLPSettings[$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriod"]]["SETTING_VALUE"];
			$checkSLPPeriod = $SLPSettings[$ipf_cfg["IPORTFOLIO_SETTING_SETTING_NAME"]["SetRecordToSlpPeriodAllowSubmit"]]["SETTING_VALUE"];
			list($startSLPPeriod, $endSLPPeriod) = (trim($SLPPeriod)!="")? explode("##", $SLPPeriod) : array("", "");
			if(trim($startSLPPeriod)!="")
			{
				$startSLPPeriodArr = explode(" ", $startSLPPeriod);
				$SLPstarttime = $startSLPPeriodArr[0];
				$SLPStartTimeArr = explode(":", $startSLPPeriodArr[1]);
				$SLP_sh = $SLPStartTimeArr[0];
				$SLP_sm = $SLPStartTimeArr[1];
				$starttimeSLPFormat = (trim($SLPstarttime)!="")? sprintf($SLPstarttime." %02s:%02s:00", $SLP_sh, $SLP_sm) : "";
			}
			if(trim($endSLPPeriod)!="")
			{
				$endSLPPeriodArr = explode(" ", $endSLPPeriod);
				$SLPendtime = $endSLPPeriodArr[0];
				$SLPEndTimeArr = explode(":", $endSLPPeriodArr[1]);
				$SLP_eh = $SLPEndTimeArr[0];
				$SLP_em = $SLPEndTimeArr[1];
				$endtimeSLPFormat = (trim($SLPendtime)!="")? sprintf($SLPendtime." %02s:%02s:59", $SLP_eh, $SLP_em) : "";
			}
			$checkSLPValue = (trim($checkSLPPeriod)=="on")? '1' : '0';

			$settingsAry[2] = array($starttimeSLPFormat, $endtimeSLPFormat, $checkSLPValue);

			# Import into DB
			$successAry = array();
			for($i=0; $i<count($SettingsType); $i++){
				$settings = $SettingsType[$i];
				$settings_details = $settingsAry[$i];
				for($j=0; $j<count($levels); $j++){
					$LevelID = $levels[$j][0];
					$sql = "INSERT INTO {$eclass_db}.IPORTFOLIO_SUBMIT_PERIOD (RecordType, ClassLevelID, StartTime, EndTime, AllowSubmit) 
							VALUES ('".$settings."', '".$LevelID."', '".$settings_details[0]."', '".$settings_details[1]."', '".$settings_details[2]."')";
					$successAry = $li->db_db_query($sql);
				}
			}

			if(!in_array(false,$successAry)){
				$x .= "iPortfolio - Transfer Submission Period Settings in text file to DB <font color='blue'>Successfully</font>.<br><br>";
			}else{
				$x .= "iPortfolio - Transfer Submission Period Settings in text file to DB <font color='red'>Failed</font>.<br><br>";
			}

			$x .= "===============================================================================<br>\r\n";
			$x .= " [End] iPortfolio - Transfer Submission Period Settings in text file to DB </b><br>\r\n";
			$x .= "===============================================================================<br>\r\n";

			# Update General Settings - Marked the script is executed
			$sql = "INSERT IGNORE INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) 
					VALUES ('$ModuleName', 'TransferSubmissionPeriodSettings', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

		}

		######################################################################################
		### [End] iPortfolio - Transfer Submission Period Settings in text file to DB
		######################################################################################


		#######################################################################################
		### App - Add Functionality field as "pushMessage" if it is empty [Start]
		#######################################################################################
		if (($plugin['eClassApp'] || $plugin['eClassTeacherApp']) && !$GSary['App_Central_Server_Default_PushMessage'])
		{
			$x .= "======================================================================================<br>\r\n";
			$x .= "App - Add Functionality field as \"pushMessage\" if it is empty [Start]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";

			$successAry_app = array();
			$li->db = $intranet_db;

			$sql = "UPDATE INTRANET_APP_CENTRAL_SERVER_SETTINGS SET Functionality = 'pushMessage' WHERE Functionality = '' OR Functionality is null";
			$successAry_app[] = $li->db_db_query($sql);

			# update General Settings - mark the script is executed
			if (!in_array(false, $successAry_app)) {
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'App_Central_Server_Default_PushMessage', 1, now())";
				$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

				$x .= "Success<br>\r\n";
			}
			else {
				$x .= "FAILED<br>\r\n";
			}

			$x .= "======================================================================================<br>\r\n";
			$x .= "App - Add Functionality field as \"pushMessage\" if it is empty [End]</b><br>\r\n";
			$x .= "======================================================================================<br>\r\n";
		}
		#######################################################################################
		### App - Add Functionality field as "pushMessage" if it is empty [End]
		#######################################################################################

		######################################################################################
        ## App - App request log table reset [Start]
        ######################################################################################
        if (($plugin['eClassApp'] || $plugin['eClassTeacherApp'] || $plugin['eClassStudentApp']) && ! $GSary['App_Request_Log_Table_Reset']) {
            $x .= "======================================================================================<br>\r\n";
            $x .= "App - App request log table reset [Start]</b><br>\r\n";
            $x .= "======================================================================================<br>\r\n";

            $successAry_app = array();
            $li->db = $intranet_db;

            $sql = "DROP TABLE IF EXISTS APP_REQUEST_LOG";
            $successAry_app[] = $li->db_db_query($sql);

            $sql = "CREATE TABLE IF NOT EXISTS APP_REQUEST_LOG (
				     LogID int(11) NOT NULL AUTO_INCREMENT,
				     RequestTextEncrypted text,
				     RequestText mediumtext,
				     ResponseTextEncrypted text,
				     ResponseText mediumtext,
				     DateInput datetime NOT NULL,
				     StartTime datetime DEFAULT NULL,
				     EndTime datetime DEFAULT NULL,
				     ProcessSecond float(6,2) DEFAULT 0,
				     MemoryUsage int(10) DEFAULT 0,
				     MemoryUsageText varchar(16) DEFAULT '0',
				     PRIMARY KEY (LogID),
				     KEY DateInput (DateInput)
				) ENGINE=MyISAM DEFAULT CHARSET=utf8";
            $successAry_app[] = $li->db_db_query($sql);

            # update General Settings - mark the script is executed
            if (! in_array(false, $successAry_app)) {
                $sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'App_Request_Log_Table_Reset', 1, now())";
                $li->db_db_query($sql) or debug_pr($sql . "\n" . mysql_error());

                $x .= "Success<br>\r\n";
            } else {
                $x .= "FAILED<br>\r\n";
            }

            $x .= "======================================================================================<br>\r\n";
            $x .= "App - App request log table reset [Start]</b><br>\r\n";
            $x .= "======================================================================================<br>\r\n";
        }
        ######################################################################################
        ## App - App request log table reset [End]
        ######################################################################################

		###########################################################
		###	[Start] iPortfolio re-assign OLE ordering
		###########################################################
		if(!$GSary['iPortfolio_reassign_OLE_order_25631deployment'] )
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "[Start] iPortfolio re-assign OLE ordering<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			//Step 1: Select Target User
			$sql = "Select UserID, COUNT(*) as numZero From $eclass_db.OLE_STUDENT where SLPOrder = 0 AND IntExt = 'INT' group by UserID";
			$targetStudentIntAry = $li->returnResultSet($sql);
			$sql = "Select UserID, COUNT(*) as numZero From $eclass_db.OLE_STUDENT where SLPOrder = 0 AND IntExt = 'EXT' group by UserID";
			$targetStudentExtAry = $li->returnResultSet($sql);

			//INT
			if(count($targetStudentIntAry) > 0 || count($targetStudentExtAry) > 0){
				//Step 2: Backup
				$sql = "Create Table $eclass_db.OLE_STUDENT_25631DEPLOY Select * from $eclass_db.OLE_STUDENT";
				$Backup = $li->db_db_query($sql) or mysql_error();

				if(count($targetStudentIntAry) > 0){
					//Step 3: Patching Int
					$li->Start_Trans();
					foreach($targetStudentIntAry as $_UserInfoAry){
						$_UserID = $_UserInfoAry['UserID'];
						//---- Reassign Non Zero Order Record
						$sql = "Select RecordID,SLPOrder  From $eclass_db.OLE_STUDENT where userid ='".$_UserID."' AND SLPOrder IS NOT NULL AND SLPOrder>0 AND IntExt = 'INT' order by SLPOrder";
						$_result = $li->returnResultSet($sql);

						foreach($_result as $__key => $__OLErecord){
							$___RecordID = $__OLErecord['RecordID'];
							$___newOrder = $__key + 1 + $_UserInfoAry['numZero'];
							//$AfterAry[$_UserID][$___RecordID] = $___newOrder;

							$sql = "UPDATE $eclass_db.OLE_STUDENT set SLPOrder = '".$___newOrder."' where RecordID='".$___RecordID."' ";
							$successAry[$___RecordID][] = $li->db_db_query($sql);

						}

						//---- Reassign Zero Order Record
						$sql = "Select RecordID,SLPOrder  From $eclass_db.OLE_STUDENT where userid ='".$_UserID."' AND SLPOrder IS NOT NULL AND SLPOrder=0 AND IntExt = 'INT' order by Title";
						$_result = $li->returnResultSet($sql);

						foreach($_result as $__key => $__OLErecord){
							$___RecordID = $__OLErecord['RecordID'];
							$___newOrder = $__key + 1;
							//$AfterAry[$_UserID][$___RecordID] = $___newOrder;

							$sql = "UPDATE $eclass_db.OLE_STUDENT set SLPOrder = '".$___newOrder."' where RecordID='".$___RecordID."' ";
							$successAry[$___RecordID][] = $li->db_db_query($sql);

						}

					}
				}

				if(count($targetStudentExtAry) > 0){
					// Patching EXT
					foreach($targetStudentExtAry as $_UserInfoAry){
						$_UserID = $_UserInfoAry['UserID'];
						//---- Reassign Non Zero Order Record
						$sql = "Select RecordID,SLPOrder  From $eclass_db.OLE_STUDENT where userid ='".$_UserID."' AND SLPOrder IS NOT NULL AND SLPOrder>0 AND IntExt = 'EXT' order by SLPOrder";
						$_result = $li->returnResultSet($sql);

						foreach($_result as $__key => $__OLErecord){
							$___RecordID = $__OLErecord['RecordID'];
							$___newOrder = $__key + 1 + $_UserInfoAry['numZero'];
							//$AfterAry[$_UserID][$___RecordID] = $___newOrder;

							$sql = "UPDATE $eclass_db.OLE_STUDENT set SLPOrder = '".$___newOrder."' where RecordID='".$___RecordID."' ";
							$successAry[$___RecordID][] = $li->db_db_query($sql);

						}

						//---- Reassign Zero Order Record
						$sql = "Select RecordID,SLPOrder  From $eclass_db.OLE_STUDENT where userid ='".$_UserID."' AND SLPOrder IS NOT NULL AND SLPOrder=0 AND IntExt = 'EXT' order by Title";
						$_result = $li->returnResultSet($sql);

						foreach($_result as $__key => $__OLErecord){
							$___RecordID = $__OLErecord['RecordID'];
							$___newOrder = $__key + 1;
							//$AfterAry[$_UserID][$___RecordID] = $___newOrder;

							$sql = "UPDATE $eclass_db.OLE_STUDENT set SLPOrder = '".$___newOrder."' where RecordID='".$___RecordID."' ";
							$successAry[$___RecordID][] = $li->db_db_query($sql);

						}

					}
				}

				if(in_multi_array(false,$successAry)) {
				echo "Reassign user OLE Order FAILED";
				$li->RollBack_Trans();
				$thisSuccess = 0;
				}
				else {
					echo "Reassign user OLE Order Success";
					$li->Commit_Trans();
					$thisSuccess = 1;
				}
			}
			else{
				$x .= 'No Need to Patch! <br>\r\n';
				$thisSuccess = 1;
			}

			$LogContent = 'Backup = '.$Backup.'<br>';
			$LogContent .= 'Num of Affected Student Int = '.count($targetStudentIntAry).'<br>';
			$LogContent .= 'Num of Affected Student Ext = '.count($targetStudentExtAry).'<br>';
			$LogContent .= 'Success ='.$thisSuccess.'<br>';

			//Last Step - Record in db
			if ($thisSuccess){
				$x .= "re-Assign Success ! <br>\r\n";
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolio_reassign_OLE_order_25631deployment', '$LogContent', now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			else{
				$x .= "re-Assign Fail ! <br>\r\n";
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolio_reassign_OLE_order_25631deployment', '$LogContent', now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			$x .= "===================================================================<br>\r\n";
			$x .= "[End] iPortfolio re-assign OLE ordering<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###	[End] iPortfolio re-assign OLE ordering
		###########################################################


		###########################################################
		###	[Start] iPortfolio calculate data patch for annual score
		###########################################################
		if(!$GSary['iPortfolio_calculate_data_patch_for_annual_score'] )
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "[Start] iPortfolio calculate data patch for annual score<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$objLibportfolio = new libportfolio();
			$thisSuccess = true;

			#### Get all Year START ####
			$sql = "SELECT YearID FROM YEAR";
			$yearArr = $li->returnVector($sql);
			#### Get all Year END ####

			#### Get all AcademicYear START ####
			$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR";
			$academicYearArr = $li->returnVector($sql);
			#### Get all AcademicYear END ####

			#### Get all YearTerm START ####
			$sql = "SELECT AcademicYearID, YearTermID FROM ACADEMIC_YEAR_TERM";
			$rs = $li->returnResultSet($sql);
			#### Get all YearTerm END ####

			#### Calculate SD START ####
			foreach($yearArr as $YearID){
				foreach($rs as $r){
					$objLibportfolio->ComputeSDMean($YearID, $r['AcademicYearID'], $r['YearTermID']);
				}
				foreach($academicYearArr as $academicYear){
					$objLibportfolio->ComputeSDMean($YearID, $academicYear, '');
				}
			}
			#### Calculate SD END ####


			$x .= "Calculate Success ! <br>\r\n";
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iPortfolio_calculate_data_patch_for_annual_score', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "[End] iPortfolio calculate data patch for annual score<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###	[End] iPortfolio calculate data patch for annual score
		###########################################################


		##################################################################################################
		### [Begin] iMail plus - Clean remaining shared mailbox member records for previous deleted users
		##################################################################################################
		if(!$GSary['iMailPlus_CleanSharedMailBoxMembersForRemovedUsers'])
		{
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] iMail plus - Clean remaining shared mailbox member records for previous deleted users<br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$sql = "SELECT m.MailBoxMemberID FROM MAIL_SHARED_MAILBOX_MEMBER as m 
					LEFT JOIN INTRANET_USER as u ON u.UserID=m.UserID 
					WHERE u.UserID IS NULL";
			$rs = $li->returnVector($sql);
			if(count($rs)>0){
				$sql = "DELETE FROM MAIL_SHARED_MAILBOX_MEMBER WHERE MailBoxMemberID IN (".implode(",",$rs).")";
				$li->db_db_query($sql);
				$x .= count($rs)." shared mailbox member records were deleted.<br>\r\n";
			}else{
				$x .= "No shared mailbox member records to be deleted.<br>\r\n";
			}
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iMailPlus_CleanSharedMailBoxMembersForRemovedUsers', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] iMail plus - Clean remaining shared mailbox member records for previous deleted users  <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		##################################################################################################
		### [End] Clean remaining shared mailbox member records for previous deleted users
		##################################################################################################


		##################################################################################################
		### [Start] ePayment - Change all ePayment tables float data type to decimal data type.
		##################################################################################################
		if(!$GSary['ePayment_ChangeDataType_FloatToDecimal'])
		{
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] ePayment - Change all ePayment tables float data type to decimal data type.          <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$tmp_epayment_queries = array(
				"ALTER TABLE PAYMENT_ACCOUNT MODIFY COLUMN Balance DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE PAYMENT_CREDIT_TRANSACTION MODIFY COLUMN Amount DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE PAYMENT_MISSED_PPS_BILL MODIFY COLUMN Amount DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE  PAYMENT_OVERALL_TRANSACTION_LOG MODIFY COLUMN Amount DECIMAL(13,2) DEFAULT NULL, MODIFY COLUMN BalanceAfter DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE PAYMENT_PAYMENT_ITEM MODIFY COLUMN DefaultAmount DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE PAYMENT_PAYMENT_ITEMSTUDENT MODIFY COLUMN Amount DECIMAL(13,2) DEFAULT NULL, MODIFY COLUMN SubsidyAmount DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE PAYMENT_PAYMENT_ITEM_SUBSIDY MODIFY COLUMN SubsidyAmount DECIMAL(13,2) NOT NULL",
				"ALTER TABLE PAYMENT_PRINTING_PACKAGE MODIFY COLUMN Price DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE PAYMENT_PURCHASE_DETAIL_RECORD MODIFY COLUMN ItemSubTotal DECIMAL(13,2) DEFAULT NULL",
				"ALTER TABLE PAYMENT_SUBSIDY_UNIT MODIFY COLUMN TotalAmount DECIMAL(13,2) DEFAULT NULL"
			);

			foreach($tmp_epayment_queries as $tmp_epayment_query)
			{
				$li->db_db_query($tmp_epayment_query) or debug_pr(mysql_error());
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePayment_ChangeDataType_FloatToDecimal', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePayment - Change all ePayment tables float data type to decimal data type.            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		##################################################################################################
		### [End] ePayment - Change all ePayment tables float data type to decimal data type.
		##################################################################################################

		##################################################################################################
		### [Start] Create Flipped Channels Builder Classroom
		##################################################################################################
		if(!$GSary['Create_Flipped_Channels_Builder_Classroom']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] Create Flipped Channels Builder Classroom 									        <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/libuser.php");
			include_once($PATH_WRT_ROOT."includes/libeclass40.php");
			include_once($PATH_WRT_ROOT."includes/libaccess.php");
			include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
			include_once($PATH_WRT_ROOT."includes/icalendar.php");

			$fc_course_code = 'fcbuilder';
			$fc_course_name = 'Flipped Channels Builder Classroom';
			$fc_course_desc = 'Flipped Channels Builder Classroom - store questions created in Flipped Channels and eclass students quiz result';

			$lo = new libeclass();
			$sql = "select course_id from ".$eclass_db.".course where course_code='".$fc_course_code."'";
			$fc_count_obj = $lo->returnVector($sql);
			$fc_course_id = $fc_count_obj[0];
			$lo->RoomType = 15;
			if($fc_course_id==''){
				$fc_course_id = $lo->eClassAdd($fc_course_code, $fc_course_name, $fc_course_desc, "NULL", "NULL");
			}
			$lo->db = $eclass_prefix."c".$fc_course_id;
			$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_SUBTITLE (
			     SubtitleID int(10) unsigned NOT NULL auto_increment,
			     Subtitle text NOT NULL,
			     DateInput datetime NOT NULL,
			     CreatedBy int(10) unsigned NOT NULL default '0',
			     DateModified datetime NOT NULL,
			     ModifiedBy int(10) unsigned NOT NULL default '0',
			     PRIMARY KEY (SubtitleID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			$lo->db_db_query($sql);

			$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_REFERENCE (
				     ReferenceID int(10) unsigned NOT NULL auto_increment,
				     Reference longblob NOT NULL,
				     DateInput datetime NOT NULL,
				     CreatedBy int(10) unsigned NOT NULL default '0',
				     DateModified datetime NOT NULL,
				     ModifiedBy int(10) unsigned NOT NULL default '0',
				     PRIMARY KEY (ReferenceID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			$lo->db_db_query($sql);

			$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_VIDEO_BUILDER (
				     VideoBuilderID int(10) unsigned NOT NULL auto_increment,
				     VideoID int(10) unsigned NOT NULL default '0',
				     BuilderType varchar(2) NOT NULL,
				     FunctionID int(10) unsigned default '0',
				     StartTime float(6,2) unsigned NOT NULL default '0.00',
				     EndTime float(6,2) unsigned NOT NULL default '0.00',
				     SequenceNo int(10) unsigned NOT NULL default '0',
				     DateInput datetime NOT NULL,
				     CreatedBy int(10) unsigned NOT NULL default '0',
				     DateModified datetime NOT NULL,
				     ModifiedBy int(10) unsigned NOT NULL default '0',
				     PRIMARY KEY (VideoBuilderID),
					 INDEX VideoID (VideoID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			$lo->db_db_query($sql);

			$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_VIDEO_RECORD (
				     RecordID int(10) unsigned NOT NULL auto_increment,
				     VideoKey varchar(255) default NULL,
				     QuizID int(8) default NULL,
				     CourseID int(8) default NULL,
				     DateInput datetime NOT NULL,
				     CreatedBy int(10) unsigned NOT NULL default '0',
				     DateModified datetime NOT NULL,
				     ModifiedBy int(10) unsigned NOT NULL default '0',
				     PRIMARY KEY (RecordID),
					 INDEX VideoQuizID (VideoKey,QuizID)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			$lo->db_db_query($sql);

			$sql = "INSERT IGNORE INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Create_Flipped_Channels_Builder_Classroom', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] Create Flipped Channels Builder Classroom 									            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		##################################################################################################
		### [End] Create Flipped Channels Builder Classroom
		##################################################################################################

		##################################################################################################
		### [Start] eEnrolment data patch for deleting duplicated Attendance Record
		##################################################################################################
		if(!$GSary['eEnrolment_Patch_duplicated_att_record'])
		{
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] eEnrolment data patch for deleting duplicated Attendance Record.          <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$sql = 'select 
						distinct(a1.GroupDateID),a1.EnrolGroupID
					from 
						INTRANET_ENROL_GROUP_ATTENDANCE as a1
						inner join  INTRANET_ENROL_GROUP_ATTENDANCE as a2 on a1.GroupDateID = a2.GroupDateID 
																		 and a1.StudentID = a2.StudentID 
																		 and a1.EnrolGroupID = a2.EnrolGroupID 
																		 and a1.DateModified != a2.DateModified
																		 and a1.GroupAttendanceID != a2.GroupAttendanceID';
			$problemClubDateAry = $li->returnResultSet($sql);

			if(count($problemClubDateAry) > 0){

				$sql = "create table INTRANET_ENROL_GROUP_ATTENDANCE_IP25651_DEPLOYMENT_BACKUP SELECT * FROM INTRANET_ENROL_GROUP_ATTENDANCE";;
				$backupResult = $li->db_db_query($sql);

				if($backupResult){
					$problemLog .= '-Patch Start-';
					foreach($problemClubDateAry as $AssocAry){
						$_GroupDateID = $AssocAry['GroupDateID'];
						$_EnrolGroupID = $AssocAry['EnrolGroupID'];
						$problemLog .= 'Problem EnrolGroupID ='.$_EnrolGroupID.'<br>';
						$problemLog .= 'Problem GroupDateID ='.$_GroupDateID.'<br>';
						$problemLog .= 'Patching..';
						$sql = "delete from 
								INTRANET_ENROL_GROUP_ATTENDANCE 
								where enrolgroupid = '$_EnrolGroupID' and GroupDateID = '$_GroupDateID' 
									and groupattendanceid not in (select groupattendanceid from (select groupattendanceid, max(datemodified) from INTRANET_ENROL_GROUP_ATTENDANCE where enrolgroupid = '$_EnrolGroupID' and GroupDateID = '$_GroupDateID' group by enrolgroupid, groupdateid, studentid) as maxTable);";

						$success = $li->db_db_query($sql);
						$problemLog .= 'Success:'.$success;
					}
					$problemLog .= '-Patch End-';
				}
				else{
					$problemLog .= 'Backup Failed';
				}
				### Patch Log
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'PatchDuplicatedAttRecordLog', '$problemLog', now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			else{
				$problemLog .= 'No need to Patch';
			}
			$x .= $problemLog;

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrolment_Patch_duplicated_att_record', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] eEnrolment data patch for deleting duplicated Attendance Record.            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		##################################################################################################
		### [End] eEnrolment data patch for deleting duplicated Attendance Record
		##################################################################################################



		##################################################################################################
		### [Start] SDAS Percentile Setting initial data
		##################################################################################################
		if(!$GSary['SDAS_Percentile_Setting_Initial_Data'])
		{
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] SDAS Percentile Setting initial data.          <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$sql = array();
			$sql[] = "INSERT IGNORE INTO ASSESSMENT_PERCENTILE_SETTING (
				Percent, Title, PercentOrder, InputDate, Createdby
			) VALUES (
				'10', 'Rank 1', '0', NOW(), NULL
			);";
			$sql[] = "INSERT IGNORE INTO ASSESSMENT_PERCENTILE_SETTING (
				Percent, Title, PercentOrder, InputDate, Createdby
			) VALUES (
				'15', 'Rank 2', '1', NOW(), NULL
			);";
			$sql[] = "INSERT IGNORE INTO ASSESSMENT_PERCENTILE_SETTING (
				Percent, Title, PercentOrder, InputDate, Createdby
			) VALUES (
				'25', 'Rank 3', '2', NOW(), NULL
			);";
			$sql[] = "INSERT IGNORE INTO ASSESSMENT_PERCENTILE_SETTING (
				Percent, Title, PercentOrder, InputDate, Createdby
			) VALUES (
				'25', 'Rank 4', '3', NOW(), NULL
			);";
			$sql[] = "INSERT IGNORE INTO ASSESSMENT_PERCENTILE_SETTING (
				Percent, Title, PercentOrder, InputDate, Createdby
			) VALUES (
				'25', 'Rank 5', '4', NOW(), NULL
			);";

			foreach($sql as $s){
				$success = $li->db_db_query($s);
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SDAS_Percentile_Setting_Initial_Data', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] SDAS Percentile Setting initial data.            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		##################################################################################################
		### [End] SDAS Percentile Setting initial data
		##################################################################################################

		##################################################################################################
		### [Start] ePCM initial data
		##################################################################################################
		if(!$GSary['ePCM_Initial_Data']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] ePCM initial data          <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
			$sql = array();
			include_once($PATH_WRT_ROOT."lang/ePCM_lang.b5.php");
			$sql[] = "INSERT IGNORE INTO INTRANET_PCM_ROLE  (
				RoleNameChi,RoleName, IsPrincipal, DateInput, DateModified, InputBy, ModifiedBy, ForCaseApproval, ForQuotationApproval, ForTenderAuditing, ForTenderApproval
			) VALUES 
					('校長','Principal', '1', NOW(), NOW(), '0', '0', '1','1','0','0'),
					('標書審核委員會','Tender Auditing Group', '0', NOW(), NOW(), '0', '0', '0','0','1','0'),
					('標書批核委員會','Tender Approval Group', '0', NOW(), NOW(), '0', '0', '0','0','0','1');";
			$sql[] = "INSERT IGNORE INTO INTRANET_PCM_RULE   (
				Code ,FloorPrice, CeillingPrice, ApprovalRoleID, NeedGroupHeadApprove, QuotationType, MinQuotation, QuotationApprovalRoleID, TenderAuditingRoleID, TenderApprovalRoleID, IsTemplate
			) VALUES 
				('N','0', '5000', '0', '1', 'N', '0', '0','0','0','1'),
				('V','5001', '50000', '1', '1', 'V', '2', '1','0','0','1'),
				('W','50001', '200000', '1', '1', 'Q', '5', '1','0','0','1'),
				('T','200001', NULL, '1', '1', 'T', '5', '0','2','3','1');";
			$sql[] = "INSERT IGNORE INTO GENERAL_SETTING   (
				Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy
			) VALUES 
				('ePCM','defaultAcademicYearID','".Get_Current_Academic_Year_ID()."',NOW(),'0',NOW(),0),
				('ePCM','caseCodeGeneration','FUNCTION::Sequence;',NOW(),'0',NOW(),0),
				('ePCM','caseCodeNumberOfDigit','4',NOW(),'0',NOW(),0),
				('ePCM','AccessSetting','1',NOW(),'0',NOW(),'0')
			;";

			$sql[] = "";

			foreach($sql as $s){
				$success = $li->db_db_query($s);
			}
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePCM_Initial_Data', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM initial data            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}

		##################################################################################################
		### [End] ePCM initial data
		##################################################################################################

		#####################################################################################################################
		### [Start] Lesson Attendance - add reason field to all SUBJECT_GROUP_STUDENT_ATTENDANCE_[ACADEMIC_YEAR_ID] tables
		#####################################################################################################################
		if($plugin['attendancelesson'])
		{
			$x .= "==============================================================================================================<br>\r\n";
			$x .= "[Start] Lesson Attendance - add reason field to all SUBJECT_GROUP_STUDENT_ATTENDANCE_[ACADEMIC_YEAR_ID] tables<br>\r\n";
			$x .= "==============================================================================================================<br>\r\n";
			$subject_group_attendance_tables = $li->returnVector("SHOW TABLES LIKE 'SUBJECT_GROUP_STUDENT_ATTENDANCE_%'");
			if(count($subject_group_attendance_tables))
			{
				foreach($subject_group_attendance_tables as $subject_group_attendance_table)
				{
					$success = $li->db_db_query("ALTER TABLE $subject_group_attendance_table ADD COLUMN Reason text default NULL AFTER LateFor");
					$x .= "Alter table $subject_group_attendance_table ".($success?"<span style=\"color:green\">SUCCESS</span>":"<span style=\"color:red\">FAIL</span>")."<br>\r\n";
				}
			}
			$x .= "==============================================================================================================<br>\r\n";
			$x .= "[End] Lesson Attendance - add reason field to all SUBJECT_GROUP_STUDENT_ATTENDANCE_[ACADEMIC_YEAR_ID] tables  <br>\r\n";
			$x .= "==============================================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] Lesson Attendance - add reason field to all SUBJECT_GROUP_STUDENT_ATTENDANCE_[ACADEMIC_YEAR_ID] tables
		#####################################################################################################################

		#####################################################################################################################
		### [Start] eNotice = Data Patch !!!!! DONT Upload before IPv7.1
		#####################################################################################################################
		if(!$GSary['eNotice_Approval_Update_Data_Patch']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] eNotice Approval Update Data Patch         <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
			$sql = array();
			include_once($PATH_WRT_ROOT."lang/ePCM_lang.b5.php");
			$sql[] = "UPDATE  INTRANET_NOTICE SET DateEnd = ADDTIME(DateEnd , '23:59:59') WHERE DATE_FORMAT(DateEnd ,'%H:%i:%s') = '00:00:00';";
			$sql[] = "";

			foreach($sql as $s){
				$success = $li->db_db_query($s);
			}
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eNotice_Approval_Update_Data_Patch', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] eNotice Approval Update Data Patch            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] eNotice = Data Patch
		#####################################################################################################################


		#################################################################################
		###  eLibPlus - break stock-take and write-off right  [Start]
		#################################################################################
		if(!$GSary['elib_break_stock_take_and_write_off_right'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_break_stock_take_and_write_off.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - break 'stock-take and write-off' right into 'stock-take' and 'write-off' right [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/script/patch_elib_break_stock_take_and_write_off.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_break_stock_take_and_write_off_right', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - break 'stock-take and write-off' right into 'stock-take' and 'write-off' right [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - break stock-take and write-off right [End]
		#################################################################################


		#################################################################################
		###  eLibPlus - sync elib+ LIBMS_BOOK.Subject to IP INTRANET_ELIB_BOOK.RelevantSubject  [Start]
		#################################################################################
		if(!$GSary['elib_sync_book_subject'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_book_subject.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - sync elib+ LIBMS_BOOK.Subject to IP INTRANET_ELIB_BOOK.RelevantSubject [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/script/patch_elib_book_subject.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_sync_book_subject', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - sync elib+ LIBMS_BOOK.Subject to IP INTRANET_ELIB_BOOK.RelevantSubject [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - sync elib+ LIBMS_BOOK.Subject to IP INTRANET_ELIB_BOOK.RelevantSubject  [End]
		#################################################################################


		###########################################################################
		# UPDATE SBA default scheme by Mr Yeung [Start]
		###########################################################################

		if($plugin['SBA'] && !$GSary['sba_rm_mr_yeung'])
		{
			$x .= "==========================================================================<br>\r\n";
			$x .= "UPDATE SBA default scheme by Mr Yeung [Start]</b><br>\r\n";
			$x .= "==========================================================================<br>\r\n";

			$sql = "SELECT SchemeID FROM IES_SCHEME WHERE DefaultSchemeCode IN (1, 4, 5) ORDER BY DefaultSchemeCode";
			$default = $li->returnVector($sql);

			if(count($default) > 0){
				$sql = "UPDATE IES_SCHEME SET RecordStatus=0 WHERE DefaultSchemeCode IN (1, 4);";
				$li->db_db_query($sql);
				$sql = "UPDATE IES_SCHEME SET Title='IES 範本 (2017版)', Description='這是根據考評局指引設定的範本。老師可以因應需要複製及修訂此專題。'  WHERE DefaultSchemeCode IN (5);";
				$li->db_db_query($sql);
			}

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'sba_rm_mr_yeung', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=======================================================================<br>\r\n";
			$x .= "UPDATE SBA default scheme by Mr Yeung[End]</b><br>\r\n";
			$x .= "=======================================================================<br>\r\n";

		}
		###########################################################################
		# UPDATE SBA default scheme by Mr Yeung [End]
		###########################################################################

		#####################################################################################################################
		### [Start] Student App - remove old login records due to using parent app login student accounts
		#####################################################################################################################
		if(!$GSary['StudentApp_removeOldLoginRecords']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] Student App - remove old login records due to using parent app login student accounts         <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;

			$sql = "select UserID from INTRANET_USER where RecordType IN (1, 3)";
			$userIdAry = $li->returnVector($sql);

			$sql = "delete from APP_LOGIN_LOG where UserID not in ('".implode("','", (array)$userIdAry)."')";
			$li->db_db_query($sql);

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudentApp_removeOldLoginRecords', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] Student App - remove old login records due to using parent app login student accounts            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] Student App - remove old login records due to using parent app login student accounts
		#####################################################################################################################

		#####################################################################################################################
		### [Start] ePCM - change currentStage of ended case to 10, and changes logic of case with quotation type = N
		#####################################################################################################################
		if(!$GSary['ePCM_2016AugPatch']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM - change currentStage of ended case to 10, and changes logic of case with quotation type = N         <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;

			$sql = "UPDATE INTRANET_PCM_CASE Set CurrentStage = '10' Where CurrentStage = '5'";
			$li->db_db_query($sql);

			$sql = "UPDATE  INTRANET_PCM_CASE AS c 
						INNER JOIN INTRANET_PCM_RULE AS r ON (c.RuleID=r.RuleID)
						SET CurrentStage = '4'
						Where QuotationType = 'N' AND CurrentStage = '2'";
			$li->db_db_query($sql);

			$sql = "UPDATE  INTRANET_PCM_CASE AS c
						INNER JOIN INTRANET_PCM_RULE AS r ON (c.RuleID=r.RuleID)
						SET CurrentStage = '4'
						Where QuotationType = 'N' AND CurrentStage = '1'";
			$li->db_db_query($sql);

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePCM_2016AugPatch', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM - change currentStage of ended case to 10, and changes logic of case with quotation type = N            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] ePCM - change currentStage of ended case to 10, and changes logic of case with quotation type = N
		#####################################################################################################################

		#####################################################################################################################
		### [Start] ePCM - add role (Vice Principal)
		#####################################################################################################################
		if(!$GSary['ePCM_2016AugPatch_2']){
			include_once($PATH_WRT_ROOT."lang/ePCM_lang.b5.php");
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM - change currentStage of ended case to 10, and changes logic of case with quotation type = N         <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;
// 20161018 moved to sql_table_update.php
// 			$sql = "ALTER TABLE INTRANET_PCM_ROLE
// 					ADD COLUMN IsVicePrincipal TINYINT NOT NULL DEFAULT 0 AFTER IsPrincipal";
// 			$li->db_db_query($sql);
// 20161018 moved to sql_table_update.php
// 			$sql = "ALTER TABLE INTRANET_PCM_RULE
// 					ADD COLUMN NeedVicePrincipalApprove TINYINT NOT NULL DEFAULT 0 AFTER NeedGroupHeadApprove";
// 			$li->db_db_query($sql);

			$sql = "INSERT INTO INTRANET_PCM_ROLE 
					(RoleName,RoleNameChi,IsVicePrincipal,ForCaseApproval,ForQuotationApproval,DateInput,DateModified,InputBy,ModifiedBy) 
					Values 
					('Vice Principal','副校長',1,1,1,now(),now(),0,0)";
			$li->db_db_query($sql);

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePCM_2016AugPatch_2', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM - add role (Vice Principal)            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] ePCM - change currentStage of ended case to 10, and changes logic of case with quotation type = N
		#####################################################################################################################


		#####################################################################################################################
		### [Start] ePCM - Patch currentStage = 1 to 2 if flag is on
		#####################################################################################################################
		if($sys_custom['ePCM_skipCaseApproval']){
			if(!$GSary['ePCM_2016AugPatch_3']){
				include_once($PATH_WRT_ROOT."lang/ePCM_lang.b5.php");
				$x .= "=============================================================================================<br>\r\n";
				$x .= "[End] ePCM - Patch currentStage = 1 to 2 if flag is on         <br>\r\n";
				$x .= "=============================================================================================<br>\r\n";

				$li->db = $intranet_db;

				$sql = "UPDATE  INTRANET_PCM_CASE AS c 
						INNER JOIN INTRANET_PCM_RULE AS r ON (c.RuleID=r.RuleID)
						SET CurrentStage = '2'
						Where QuotationType != 'N' AND CurrentStage = '1'";
				$li->db_db_query($sql);

				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePCM_2016AugPatch_3', '1', now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());

				$x .= "=============================================================================================<br>\r\n";
				$x .= "[End] ePCM - Patch currentStage = 1 to 2 if flag is on           <br>\r\n";
				$x .= "=============================================================================================<br>\r\n";
			}
			if(!$GSary['ePCM_2016AugPatch_4']){
				include_once($PATH_WRT_ROOT."lang/ePCM_lang.b5.php");
				$x .= "=============================================================================================<br>\r\n";
				$x .= "[End] ePCM - Remove case approval for unfinished case         <br>\r\n";
				$x .= "=============================================================================================<br>\r\n";

				$li->db = $intranet_db;

				$sql = "Update INTRANET_PCM_CASE_APPROVAL SET RecordStatus = '0' WHERE CaseID IN (Select CaseID From INTRANET_PCM_CASE WHERE CurrentStage != '10')";
				$li->db_db_query($sql);

				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePCM_2016AugPatch_4', '1', now())";
				$li->db_db_query($sql) or debug_pr(mysql_error());

				$x .= "=============================================================================================<br>\r\n";
				$x .= "[End] ePCM - Remove case approval for unfinished case           <br>\r\n";
				$x .= "=============================================================================================<br>\r\n";
			}
		}
		#####################################################################################################################
		### [End] ePCM - Patch currentStage = 1 to 2 if flag is on
		#####################################################################################################################


		#################################################################################
		###  eLibPlus - set INTRANET_ELIB_BOOK.Publish=0 for eLib+ lost book in LIBMS_BOOK if NoOfCopy=0 [Start]
		#################################################################################
		if(!$GSary['elib_sync_intranet_book_publish'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_lost_book_publish.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - set INTRANET_ELIB_BOOK.Publish=0 for eLib+ lost book in LIBMS_BOOK if NoOfCopy=0 [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/script/patch_elib_lost_book_publish.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_sync_intranet_book_publish', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - set INTRANET_ELIB_BOOK.Publish=0 for eLib+ lost book in LIBMS_BOOK if NoOfCopy=0 [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - set INTRANET_ELIB_BOOK.Publish=0 for eLib+ lost book in LIBMS_BOOK if NoOfCopy=0 [End]
		#################################################################################

		#####################################################################################################################
		### [Start] eClass - patch eclass_db.user_course missing intranet_user_id while copy classroom
		#####################################################################################################################
		if(!$GSary['classroom_PatchUserCourseMissingIntranetUserId']){
		    $x .= "=============================================================================================<br>\r\n";
		    $x .= "[Start] eClass - patch eclass_db.user_course missing intranet_user_id while copy classroom         <br>\r\n";
		    $x .= "=============================================================================================<br>\r\n";

		    $li->db = $intranet_db;

		    $sql = "select uc.*, iu.UserID as iUserID from ".$eclass_db.".user_course uc INNER JOIN ".$intranet_db.".INTRANET_USER iu ON uc.user_email = iu.UserEmail where uc.intranet_user_id IS NULL OR uc.intranet_user_id = '0'";
		    $usercourseArr = $li->returnArray($sql);

		    if(count($usercourseArr) > 0){
		        for($i=0 ; $i<count($usercourseArr) ; $i++){

		            $user_course_id = $usercourseArr[$i]['user_course_id'];
		            $i_userid = $usercourseArr[$i]['iUserID'];

		            # update user_course
		            $sql = "UPDATE 
		                {$eclass_db}.user_course 
	                SET
	                    intranet_user_id = '{$i_userid}'
                    WHERE
                        user_course_id = '{$user_course_id}'
		            ";

	                $li->db_db_query($sql);
		        }
		    }


		    $sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'classroom_PatchUserCourseMissingIntranetUserId', '1', now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

		    $x .= "=============================================================================================<br>\r\n";
		    $x .= "[End] eClass - patch eclass_db.user_course missing intranet_user_id while copy classroom            <br>\r\n";
		    $x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] eClass - patch eclass_db.user_course missing intranet_user_id while copy classroom
		#####################################################################################################################

		#####################################################################################################################
		### [Start] Student Attendance - Setup the cron job for HKUSPH to submit daily data on 02:00
		#####################################################################################################################
		if($plugin['attendancestudent'] && $sys_custom['eClassApp']['HKUSPH'] && !$GSary['HKUSPHCronJobSetup']){

			$x .= "=============================================================================================<br>\n";
			$x .= "[Start] Student Attendance - Setup the cron job for HKUSPH to submit daily data on 02:00     <br>\n";
			$x .= "=============================================================================================<br>\n";

			$li->db = $intranet_db;

			include_once($intranet_root."/includes/libcrontab.php");

			$hkusph_settings = $lgs->Get_General_Setting('StudentAttendance',array("'HKUSPH_JobStatus'"));
			if($hkusph_settings['HKUSPH_JobStatus'] != 1){
				$crontab = new libcrontab();
				$hkusph_job_script = "http://".$_SERVER['SERVER_NAME'].($_SERVER["SERVER_PORT"]!= 80?":".$_SERVER["SERVER_PORT"]:"")."/schedule_task/hkusph.php";
				//$crontab->removeJob($hkusph_job_script);
				$hkusph_setup_result = $crontab->setJob('0', '2', '*', '*', '*', $hkusph_job_script);
				if($hkusph_setup_result) $lgs->Save_General_Setting('StudentAttendance',array('HKUSPH_JobStatus'=>1,'HKUSPH_JobExecutionTime'=>'02:00'));
				$x .= "Cron job script ".$hkusph_job_script." has been setup to run daily on 02:00. ".($hkusph_setup_result?"<span style=\"color:green\">[SUCCESS]</span>":"<span style=\"color:red\">[FAILED]</span>")."<br>\n";
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'HKUSPHCronJobSetup', '1', now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\n";
			$x .= "[End] Student Attendance - Setup the cron job for HKUSPH to submit daily data on 02:00       <br>\n";
			$x .= "=============================================================================================<br>\n";
		}
		#####################################################################################################################
		### [End] Student Attendance - Setup the cron job for HKUSPH to submit daily data on 02:00
		#####################################################################################################################


		#################################################################################
		###  eLibPlus - disable eBook related settings in Portal Settings if client hasn't ever purchased eBook  [Start]
		#################################################################################
		if(!$GSary['elib_disable_ebook_settings_in_portal'] && $plugin['library_management_system'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - disable eBook related settings in Portal Settings if client hasn't ever purchased eBook [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			include_once($PATH_WRT_ROOT."includes/libelibrary.php");
			$libel = new elibrary();
			$isPurchasedeBook = $libel->isPurchasedeBook();
			if (!$isPurchasedeBook) {
				$portal_setting_array[] = 'book_category_chi_ebooks';
				$portal_setting_array[] = 'book_category_eng_ebooks';
				$portal_setting_array[] = 'ebook_recommend_book';
				$portal_setting_array[] = 'ebook_most_hit';
				$portal_setting_array[] = 'ebook_new';
				$portal_setting_array[] = 'ebook_best_rated';
				$portal_setting_array[] = 'ebook_ranking_most_active_reviewers';
				$portal_setting_array[] = 'ebook_ranking_most_helpful_reviewers';

				$page_type_array[] = 'internal';
				$page_type_array[] = 'opac';
				foreach((array)$portal_setting_array as $item_name) {
					foreach((array)$page_type_array as $page_type) {
						$sql = "UPDATE `LIBMS_PORTAL_SETTING` SET `value` = '0' WHERE `name` = '{$item_name}' AND page_type='{$page_type}'";
						$Result['UpdatePortalSettings'] = $liblms->db_db_query($sql);
					}
				}
				$sql = "UPDATE `LIBMS_PORTAL_SETTING` SET `value` = '0' WHERE `name` = 'opac_search_ebook' AND page_type='opac'";
				$Result['UpdatePortalSettings'] = $liblms->db_db_query($sql);
			}

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_disable_ebook_settings_in_portal', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - disable eBook related settings in Portal Settings if client hasn't ever purchased eBook [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - disable eBook related settings in Portal Settings if client hasn't ever purchased eBook  [End]
		#################################################################################


		#################################################################################
		###  eLibPlus - disable pBook related settings in Portal Settings if $plugin['eLib_Lite']=true  [Start]
		#################################################################################
		if(!$GSary['elib_disable_pbook_settings_in_portal'] && $plugin['library_management_system'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - disable pBook related settings in Portal Settings if \$plugin['eLib_Lite']=true [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			if ($plugin['eLib_Lite']) {
				$portal_setting_array[] = 'book_category_physical_books';
				$portal_setting_array[] = 'pbook_recommend_book';
				$portal_setting_array[] = 'pbook_most_loan';
				$portal_setting_array[] = 'pbook_new';
				$portal_setting_array[] = 'pbook_best_rated';
				$portal_setting_array[] = 'pbook_ranking_most_active_reviewers';
				$portal_setting_array[] = 'pbook_ranking_most_active_borrowers';
				$portal_setting_array[] = 'pbook_ranking_most_helpful_reviewers';

				$page_type_array[] = 'internal';
				$page_type_array[] = 'opac';
				foreach((array)$portal_setting_array as $item_name) {
					foreach((array)$page_type_array as $page_type) {
						$sql = "UPDATE `LIBMS_PORTAL_SETTING` SET `value` = '0' WHERE `name` = '{$item_name}' AND page_type='{$page_type}'";
						$Result['UpdatePortalSettings'] = $liblms->db_db_query($sql);
					}
				}
			}

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_disable_pbook_settings_in_portal', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - disable pBook related settings in Portal Settings if \$plugin['eLib_Lite']=true [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - disable pBook related settings in Portal Settings if $plugin['eLib_Lite']=true  [End]
		#################################################################################


		#################################################################################
		###  eLibPlus - Sync Min(LIBMS_BOOK_UNIQUE.CreationDate) by BookID to newly added INTRANET_ELIB_BOOK.AcccountDate for eLib+ [Start]
		#################################################################################
		if(!$GSary['elib_sync_intranet_account_date'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_account_date.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - Sync Min(LIBMS_BOOK_UNIQUE.CreationDate) by BookID to newly added INTRANET_ELIB_BOOK.AcccountDate for eLib+ [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/script/patch_elib_account_date.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_sync_intranet_account_date', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - Sync Min(LIBMS_BOOK_UNIQUE.CreationDate) by BookID to newly added INTRANET_ELIB_BOOK.AcccountDate for eLib+ [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - Sync Min(LIBMS_BOOK_UNIQUE.CreationDate) by BookID to newly added INTRANET_ELIB_BOOK.AcccountDate for eLib+ [End]
		#################################################################################


		#####################################################################################################################
		### [Start] Student Attendance - Initialize values for TeacherCanManageXXX settings.
		#####################################################################################################################
		if($plugin['attendancestudent'] && !$GSary['StudentAttendance_Init_TeacherCanManageXXX']){

			$x .= "=============================================================================================<br>\n";
			$x .= "[Start] Student Attendance - Initialize values for TeacherCanManageXXX settings.             <br>\n";
			$x .= "=============================================================================================<br>\n";

			$li->db = $intranet_db;

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('StudentAttendance', 'TeacherCanManageWaiveStatus', '1', now()), ('StudentAttendance', 'TeacherCanManageReason', '1', now()), ('StudentAttendance', 'TeacherCanManageTeacherRemark', '1', now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudentAttendance_Init_TeacherCanManageXXX', '1', now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\n";
			$x .= "[End] Student Attendance - Initialize values for TeacherCanManageXXX settings.               <br>\n";
			$x .= "=============================================================================================<br>\n";
		}
		#####################################################################################################################
		### [End] Student Attendance - Initialize values for TeacherCanManageXXX settings.
		#####################################################################################################################


		#################################################################################
		###  eLibPlus - break report access right "overdue report" into "overdue report" and "penalty report"  [Start]
		#################################################################################
		if(!$GSary['elib_break_overdue_report_right'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_break_overdue_report.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - break 'overdue report' right into 'overdue report' and 'penalty report' right [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/script/patch_elib_break_overdue_report.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_break_overdue_report_right', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - break 'overdue report' right into 'overdue report' and 'penalty report' right [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - break report access right "overdue report" into "overdue report" and "penalty report" [End]
		#################################################################################


		#################################################################################
		###  eGuidance - system data initialized setup [Start]
		#################################################################################
		if(!$GSary['eGuidance_InitDataSetting'] && $plugin['eGuidance'] && file_exists($intranet_root."/addon/install/eGuidance.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eGuidance - system data initialized setup [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/install/eGuidance.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'eGuidance_InitDataSetting', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eGuidance - system data initialized setup [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eGuidance - system data initialized setup [End]
		#################################################################################

		#################################################################################
		###  eGuidance - update SeqNo for SEN adjustment type, adjustment item, service type and service item [Start]
		#################################################################################
		if(!$GSary['eGuidance_add_SeqNo_to_SEN_Adjustment_and_Service'] && $plugin['eGuidance'] )
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - update SeqNo for SEN adjustment type, adjustment item, service type and service item [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    // adjustment
		    $sql = "SELECT TypeID FROM INTRANET_GUIDANCE_SETTING_ADJUST_TYPE ORDER BY TypeID";
		    $typeAry = $li->returnResultSet($sql);
		    $result = array();
		    $li->Start_Trans();
		    for($i=0,$iMax=count($typeAry);$i<$iMax;$i++){
		        $typeID = $typeAry[$i]['TypeID'];
		        $sql = "UPDATE INTRANET_GUIDANCE_SETTING_ADJUST_TYPE SET SeqNo='".($i+1)."' WHERE TypeID='".$typeID."'";
		        $result[] = $li->db_db_query($sql);

		        $sql = "SELECT ItemID FROM INTRANET_GUIDANCE_SETTING_ADJUST_ITEM WHERE TypeID='".$typeID."' ORDER BY ItemID";
		        $itemAry = $li->returnResultSet($sql);
		        for($j=0,$jMax=count($itemAry);$j<$jMax;$j++){
		            $itemID = $itemAry[$j]['ItemID'];
		            $sql = "UPDATE INTRANET_GUIDANCE_SETTING_ADJUST_ITEM SET SeqNo='".($j+1)."' WHERE ItemID='".$itemID."'";
		            $result[] = $li->db_db_query($sql);
		        }
		    }

		    // service
		    $sql = "SELECT TypeID FROM INTRANET_GUIDANCE_SETTING_SERVICE_TYPE ORDER BY TypeID";
		    $typeAry = $li->returnResultSet($sql);
		    for($i=0,$iMax=count($typeAry);$i<$iMax;$i++){
		        $typeID = $typeAry[$i]['TypeID'];
		        $sql = "UPDATE INTRANET_GUIDANCE_SETTING_SERVICE_TYPE SET SeqNo='".($i+1)."' WHERE TypeID='".$typeID."'";
		        $result[] = $li->db_db_query($sql);

		        $sql = "SELECT ServiceID FROM INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE WHERE TypeID='".$typeID."' ORDER BY ServiceID";
		        $itemAry = $li->returnResultSet($sql);
		        for($j=0,$jMax=count($itemAry);$j<$jMax;$j++){
		            $ServiceID = $itemAry[$j]['ServiceID'];
		            $sql = "UPDATE INTRANET_GUIDANCE_SETTING_SUPPORT_SERVICE SET SeqNo='".($j+1)."' WHERE ServiceID='".$ServiceID."'";
		            $result[] = $li->db_db_query($sql);
		        }
		    }

		    if (!in_array(false,$result)) {
		        $li->Commit_Trans();

		        # update General Settings - marked the script is executed
		        $sql = "insert ignore into GENERAL_SETTING
		        (Module, SettingName, SettingValue, DateInput)
		        values
		        ('$ModuleName', 'eGuidance_add_SeqNo_to_SEN_Adjustment_and_Service', 1, now())";

		        $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());
		    }
		    else {
		        $li->RollBack_Trans();
		    }
		    unset($result);


		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - update SeqNo for SEN adjustment type, adjustment item, service type and service item [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eGuidance - update SeqNo for SEN adjustment type, adjustment item, service type and service item [End]
		#################################################################################

		#################################################################################
		###  eGuidance - swap DateModified and DateInput if they exist in general setting for AllowAccessIP [Start]
		#################################################################################
		if(!$GSary['eGuidance_Swap_DateModified_DateInput_for_AllowAccessIP'] && $plugin['eGuidance'] )
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - eGuidance - swap DateModified and DateInput if they exist in general setting for AllowAccessIP [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    // adjustment
		    $sql = "SELECT DateModified, DateInput FROM GENERAL_SETTING WHERE Module='eGuidance' AND SettingName='AllowAccessIP' AND DateInput>DateModified";
		    $settingAry = $li->returnResultSet($sql);
		    if (count($settingAry)>0) {
		        $dateModified = $settingAry[0]['DateInput'];
		        $dateInput = $settingAry[0]['DateModified'];
		        $sql = "UPDATE GENERAL_SETTING SET DateInput='".$dateInput."', DateModified='".$dateModified."' WHERE Module='eGuidance' AND SettingName='AllowAccessIP'";
		        $result = $li->db_db_query($sql);
		        if ($result) {
		            $x .= "eGuidance_Swap_DateModified_DateInput_for_AllowAccessIP success<br>\r\n";
		        }
		    }

	        # update General Settings - marked the script is executed
	        $sql = "insert ignore into GENERAL_SETTING
	        (Module, SettingName, SettingValue, DateInput)
	        values
	        ('$ModuleName', 'eGuidance_Swap_DateModified_DateInput_for_AllowAccessIP', 1, now())";

	        $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

		    unset($result);

		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - eGuidance - swap DateModified and DateInput if they exist in general setting for AllowAccessIP [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eGuidance - swap DateModified and DateInput if they exist in general setting for AllowAccessIP[End]
		#################################################################################

		#################################################################################
		###  eGuidance - patch SENType in INTRANET_GUIDANCE_SEN_CASE to allow text and sub-type co-exist [Start]
		#################################################################################
		if(!$GSary['eGuidance_SENType_Text_and_Subtype_Coexist'] && $plugin['eGuidance'] )
		{
		    include_once ($intranet_root . "/includes/eGuidance/libguidance.php");

		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - patch SENType in INTRANET_GUIDANCE_SEN_CASE to allow text and sub-type co-exist [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    $libguidance = new libguidance();
		    $sen_case_subtype = $libguidance->getAllSENCaseSubType();
		    $senCaseSubTypeAssoc = BuildMultiKeyAssoc($sen_case_subtype, array(
		        'TypeCode',
		        'Code'
		    ), $IncludedDBField = array(
		        'Name'
		    ), $SingleValue = 1);

		    // SENType
		    $sql = "SELECT StudentID, SENType FROM INTRANET_GUIDANCE_SEN_CASE ORDER BY StudentID";
		    $checkAry = $li->returnResultSet($sql);
		    $nrCheck = count($checkAry);

		    if ($nrCheck) {
		        $result = array();
		        $li->Start_Trans();

		        for ($i=0; $i<$nrCheck; $i++) {
		            $studentID = $checkAry[$i]['StudentID'];
		            $SENType = $checkAry[$i]['SENType'];
		            $senTypeAry = explode('^~', $SENType);

		            foreach ((array) $senTypeAry as $k => $v) {
		                if (strpos($v, '^:') !== false) {
		                    list ($senTypeCode, $senItemStr) = explode('^:', $v);
		                    if (strpos($senItemStr, '^#') === false) {
		                        if ($senCaseSubTypeAssoc[$senTypeCode][$senItemStr]) {
		                            $senTypeAry[$k] = $v;
		                        }
		                        else {      // not sub-type
		                            $senTypeAry[$k] = $senTypeCode . '^@' . $senItemStr;
		                        }
		                    } else {
		                        $senTypeAry[$k] = $v;
		                    }
		                }
		                else {
		                    $senTypeAry[$k] = $v;
		                }
		            }

		            $senTypeStr = implode("^~", (array) $senTypeAry);

		            $sql = "UPDATE INTRANET_GUIDANCE_SEN_CASE SET SENType='".$senTypeStr."' WHERE StudentID='".$studentID."'";
		            $result[] = $li->db_db_query($sql);
		        }

		        if(!empty($result) && in_array(false,$result)){
		            $li->RollBack_Trans();
		            $x .= "<p style='color:red'>error in eGuidance patch SENType to allow text and sub-type co-exist</p>";
		        }else{
		            $li->Commit_Trans();

		            # update General Settings - marked the script is executed
		            $sql = "insert ignore into GENERAL_SETTING
		            (Module, SettingName, SettingValue, DateInput)
		            values
		            ('$ModuleName', 'eGuidance_SENType_Text_and_Subtype_Coexist', 1, now())";

		            $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

		            $x .= "eGuidance patch SENType to allow text and sub-type co-exist success<br>\r\n";
		        }
		        unset($result);
		    }
		    else {
		        # update General Settings - marked the script is executed
		        $sql = "insert ignore into GENERAL_SETTING
		        (Module, SettingName, SettingValue, DateInput)
		        values
		        ('$ModuleName', 'eGuidance_SENType_Text_and_Subtype_Coexist', 1, now())";
		        
		        $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());
		    }

		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - patch SENType in INTRANET_GUIDANCE_SEN_CASE to allow text and sub-type co-exist [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eGuidance - patch SENType in INTRANET_GUIDANCE_SEN_CASE to allow text and sub-type co-exist [End]
		#################################################################################

		#################################################################################
		###  eGuidance - patch SeqNo for SENType and SENSubType in INTRANET_GUIDANCE_SETTING_ITEM [Start]
		#################################################################################
		if(!$GSary['eGuidance_Set_SeqNo_for_SENType_and_SENSubType'] && $plugin['eGuidance'] )
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - patch SeqNo for SENType and SENSubType in INTRANET_GUIDANCE_SETTING_ITEM [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		    
		    $sql = "SELECT SettingID FROM INTRANET_GUIDANCE_SETTING_ITEM WHERE Form='SENCase' AND Type='SENType' ORDER BY SettingID";
		    $settingAry = $li->returnResultSet($sql);
		    $result = array();
		    $li->Start_Trans();
		    for($i=0,$iMax=count($settingAry);$i<$iMax;$i++){
		        $settingID = $settingAry[$i]['SettingID'];
		        $sql = "UPDATE INTRANET_GUIDANCE_SETTING_ITEM SET SeqNo='".($i+1)."' WHERE SettingID='".$settingID."'";
		        $result[] = $li->db_db_query($sql);
		        
		        $sql = "SELECT SettingID FROM INTRANET_GUIDANCE_SETTING_ITEM WHERE ParentSettingID='".$settingID."' ORDER BY SettingID";
		        $childSettingAry = $li->returnResultSet($sql);
		        for($j=0,$jMax=count($childSettingAry);$j<$jMax;$j++){
		            $childSettingID = $childSettingAry[$j]['SettingID'];
		            $sql = "UPDATE INTRANET_GUIDANCE_SETTING_ITEM SET SeqNo='".($j+1)."' WHERE SettingID='".$childSettingID."'";
		            $result[] = $li->db_db_query($sql);
		        }
		    }
		    
		    if(!empty($result) && in_array(false,$result)){
		        $li->RollBack_Trans();
		        $x .= "<p style='color:red'>Error occurs when patch SeqNo for SENType and SENSubType in eGuidance</p>";
		    }else{
		        $li->Commit_Trans();
		        
		        # update General Settings - marked the script is executed
		        $sql = "insert ignore into GENERAL_SETTING
		        (Module, SettingName, SettingValue, DateInput)
		        values
		        ('$ModuleName', 'eGuidance_Set_SeqNo_for_SENType_and_SENSubType', 1, now())";
		        
		        $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());
		        
		        $x .= "eGuidance patch SeqNo for SENType and SENSubType success<br>\r\n";
		    }
		    unset($result);
		    
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - patch SeqNo for SENType and SENSubType in INTRANET_GUIDANCE_SETTING_ITEM [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eGuidance - patch SeqNo for SENType and SENSubType in INTRANET_GUIDANCE_SETTING_ITEM [End]
		#################################################################################
		
		#################################################################################
		###  eGuidance - set default value for PrintSenCasePICinStudentReport in GENERAL_SETTING [Start]
		#################################################################################
		if(!$GSary['eGuidance_Set_PrintSenCasePICinStudentReport'] && $plugin['eGuidance'] )
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - set default value for PrintSenCasePICinStudentReport in GENERAL_SETTING [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		    
		    $sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE Module='eGuidance' AND SettingName='MAX'";
		    $settingAry = $li->returnResultSet($sql);
		    if (count($settingAry)) {
		        $settingValue = $settingAry[0]['SettingValue'];
		        $settingValue .= "#PrintSenCasePICinStudentReport:1";
		        $sql = "UPDATE GENERAL_SETTING set SettingValue='".$li->Get_Safe_Sql_Query($settingValue)."', DateModified=now(), ModifiedBy='" . $_SESSION['UserID'] . "' where SettingName='MAX' and Module='eGuidance'";
		    }
		    else {
		        $settingValue = "PrintSenCasePICinStudentReport:1";
		        $sql = "INSERT INTO GENERAL_SETTING (Module,SettingName,SettingValue,DateInput,InputBy,DateModified,ModifiedBy) values ('eGuidance', 'MAX', '".$li->Get_Safe_Sql_Query($settingValue)."',now(), '" . $_SESSION['UserID'] . "',now(), '" . $_SESSION['UserID'] . "')";
		    }
		    $result = $li->db_db_query($sql);
		    
		    if($result){
		        # update General Settings - marked the script is executed
		        $sql = "insert ignore into GENERAL_SETTING
		        (Module, SettingName, SettingValue, DateInput)
		        values
		        ('$ModuleName', 'eGuidance_Set_PrintSenCasePICinStudentReport', 1, now())";
		        
		        $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());
		        
		        $x .= "eGuidance set PrintSenCasePICinStudentReport success<br>\r\n";
		        
		    }else{
		        $x .= "<p style='color:red'>Error occurs when set PrintSenCasePICinStudentReport in eGuidance</p>";
		    }
		    unset($result);
		    
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eGuidance - set default value for PrintSenCasePICinStudentReport in GENERAL_SETTING [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eGuidance - set default value for PrintSenCasePICinStudentReport in GENERAL_SETTING[End]
		#################################################################################
		
		
		#################################################################################
		###  ePCM patch funding source category [Start]
		#################################################################################
		if(!$GSary['ePCM_fundingcategory_20171016'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " ePCM patch funding source category [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$li->db = $intranet_db;
			$sql = "CREATE TABLE INTRANET_PCM_FUNDING_CATEGORY_RELATION_DATAPATCH20171016_BAK SELECT * FROM INTRANET_PCM_FUNDING_CATEGORY_RELATION";
			$li->db_db_query($sql);
			$backupedRow = $li->db_affected_rows();
			if($backupedRow > 0){
				$sql = "UPDATE INTRANET_PCM_FUNDING_SOURCE as funding
						LEFT JOIN INTRANET_PCM_FUNDING_CATEGORY_RELATION as relation ON funding.FundingID = relation.FundingID
						set funding.FundingCategoryID = relation.FundingCategoryID";
				$li->db_db_query($sql);
				$result = $backupedRow.':::'.$li->db_affected_rows();

				$sql = "DROP TABLE INTRANET_PCM_FUNDING_CATEGORY_RELATION";
				$li->db_db_query($sql);
			}else{
				$result = '-1';
			}

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING
			(Module, SettingName, SettingValue, DateInput)
			values
			('$ModuleName', 'ePCM_fundingcategory_20171016', '$result', now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " ePCM patch funding source category [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  ePCM patch funding source category [End]
		#################################################################################

		#####################################################################################################################
		### [Start] ePCM -  Insert first RULE information into INTRANET_PCM_RULE with quotation code 'O'
		#####################################################################################################################
		if(!$GSary['ePCM_20161005_AddOtherApplication']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM - Insert first RULE information into INTRANET_PCM_RULE with quotation code 'O'        <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;

			$sql = "SELECT COUNT(*) as numOfResult FROM INTRANET_PCM_RULE WHERE QuotationType = 'O'";
			$result = $li->returnResultSet($sql);
			if($result[0]['numOfResult'] > 0){
				//do nothing
			}else{
				$sql = "INSERT INTO INTRANET_PCM_RULE (Code,QuotationType) VALUES ('O','O')";
				$li->db_db_query($sql);
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePCM_20161005_AddOtherApplication', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM - Insert first RULE information into INTRANET_PCM_RULE with quotation code 'O'             <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] ePCM -  Insert first RULE information into INTRANET_PCM_RULE with quotation code 'O'
		#####################################################################################################################

		#####################################################################################################################
		### [Start] teacher appraisal -  Insert 3 new forms to be filled in default template
		#####################################################################################################################
		if(!$GSary['ta_20171115_insert_new_default_forms']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] teacher appraisal -  Insert 3 new forms to be filled in default template        <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;

			$sql = "SELECT TemplateID, InputBy_UserID, ModifiedBy_UserID  FROM INTRANET_PA_S_FRMPRE GROUP BY TemplateID";
			$templates = $li->returnArray($sql);

			foreach($templates as $tmp){
				/* Add the new forms */
				$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMPRE WHERE TemplateID='".$tmp['TemplateID']."' AND DataTypeID=13";
				$curr = $li->returnVector($sql);
				if(empty($curr)){
					$sql = "INSERT INTO INTRANET_PA_S_FRMPRE
								(TemplateID, DataTypeID, DescrChi, DescrEng, DisplayOrder, Incl, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified) 
							VALUES
								(".$tmp['TemplateID'].",13,'教學／行政工作目標及評估', 'Teaching / Administrative Duties Objectives and Assessment', 13,1,'".$tmp['InputBy_UserID']."',NOW(),'".$tmp['ModifiedBy_UserID']."',NOW())";
					$li->db_db_query($sql);
				}
				$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMPRE WHERE TemplateID='".$tmp['TemplateID']."' AND DataTypeID=14";
				$curr = $li->returnVector($sql);
				if(empty($curr)){
					$sql = "INSERT INTO INTRANET_PA_S_FRMPRE
								(TemplateID, DataTypeID, DescrChi, DescrEng, DisplayOrder, Incl, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
							VALUES
								(".$tmp['TemplateID'].",14,'現負責的學科工作', 'Present Academic Duties', 14,1,'".$tmp['InputBy_UserID']."',NOW(),'".$tmp['ModifiedBy_UserID']."',NOW())";
					$li->db_db_query($sql);
				}
				$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMPRE WHERE TemplateID='".$tmp['TemplateID']."' AND DataTypeID=15";
				$curr = $li->returnVector($sql);
				if(empty($curr)){
					$sql = "INSERT INTO INTRANET_PA_S_FRMPRE
								(TemplateID, DataTypeID, DescrChi, DescrEng, DisplayOrder, Incl, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified)
							VALUES
								(".$tmp['TemplateID'].",15,'本年度訓練學生參加比賽及成績', 'Current Year Responsible Student Training for Competition and Result', 15,1,'".$tmp['InputBy_UserID']."',NOW(),'".$tmp['ModifiedBy_UserID']."',NOW())";
					$li->db_db_query($sql);
				}
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ta_20171115_insert_new_default_forms', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] teacher appraisal -  Insert 3 new forms to be filled in default template            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] teacher appraisal -  Insert 3 new forms to be filled in default template
		#####################################################################################################################

		#####################################################################################################################
		### [Start] Message Center - user device wrongly deactivated in housekepping
		#####################################################################################################################
		if(!$GSary['MessageCenter_patchWrongDidNotLoginForLongRecords']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] Message Center - user device wrongly deactivated in housekepping         <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;

			$sql = "create table $intranet_db.APP_USER_DEVICE_NOTLOGINBATCH_BAK select * from $intranet_db.APP_USER_DEVICE";
			$li->db_db_query($sql);
			$sql = "update $intranet_db.APP_USER_DEVICE set RecordStatus=1, LoginStatus='in', DisableReason='' Where RecordStatus=0 and LoginStatus='del' and DisableReason='didNotLoginForLong' and DateModified >= '2017-01-01 00:00:00'";
			$li->db_db_query($sql);

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'MessageCenter_patchWrongDidNotLoginForLongRecords', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] Message Center - user device wrongly deactivated in housekepping            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] Message Center - user device wrongly deactivated in housekepping
		#####################################################################################################################

		#####################################################################################################################
		### [Start] Student Profile - Patch wrong record type for imported attendance profiles and merit profiles
		#####################################################################################################################
		if(!$GSary['PatchStudentProfile20171205']){
			$x .= "=====================================================================================================<br>\r\n";
			$x .= "[Start] Student Profile - Patch wrong record type for imported attendance profiles and merit profiles<br>\r\n";
			$x .= "=====================================================================================================<br>\r\n";
			$li->db = $intranet_db;
			$sql = "UPDATE $intranet_db.PROFILE_STUDENT_ATTENDANCE SET RecordType=2 WHERE RecordType='L'";
			$li->db_db_query($sql);
			$sql = "UPDATE $intranet_db.PROFILE_STUDENT_ATTENDANCE SET RecordType=1 WHERE RecordType='A'";
			$li->db_db_query($sql);
			$sql = "UPDATE $intranet_db.PROFILE_STUDENT_ATTENDANCE SET RecordType=3 WHERE RecordType='E'";
			$li->db_db_query($sql);
			$sql = "UPDATE $intranet_db.PROFILE_STUDENT_ATTENDANCE SET DayType=2 WHERE DayType=0";
			$li->db_db_query($sql);
			$sql = "DELETE FROM $intranet_db.PROFILE_STUDENT_ATTENDANCE WHERE Attendancedate='0000-00-00 00:00:00'";
			$li->db_db_query($sql);
			$sql = "UPDATE $intranet_db.PROFILE_STUDENT_MERIT SET RecordType=REPLACE(RecordType,'C','') WHERE RecordType like 'C%'";
			$li->db_db_query($sql);
			$sql = "UPDATE $intranet_db.PROFILE_STUDENT_MERIT SET RecordType=REPLACE(RecordType,'D','-') WHERE RecordType like 'D%'";
			$li->db_db_query($sql);

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'PatchStudentProfile20171205', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=====================================================================================================<br>\r\n";
			$x .= "[End] Student Profile - Patch wrong record type for imported attendance profiles and merit profiles  <br>\r\n";
			$x .= "=====================================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] Student Profile - Patch wrong record type for imported attendance profiles and merit profiles
		#####################################################################################################################

		#################################################################################
		###  ePCM patch Approval Rule [Start]
		#################################################################################
		if(!$GSary['ePCM_CaseApprovalRule_20171206'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= " ePCM patch Approval Rule [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$li->db = $intranet_db;
			$sql = "CREATE TABLE INTRANET_PCM_RULE_DATAPATCH20171206_BAK SELECT * FROM INTRANET_PCM_RULE ";
			$li->db_db_query($sql);

			$sql = "SELECT SettingValue FROM GENERAL_SETTING WHERE mODULE = 'ePCM' and SettingName= 'caseApprovalMethod'";
			$result = $li->returnVector($sql);

			$isOneVicePrincipal = false;
			if($result[0] == 'oneVicePrincipalAndAllOthers'){
				$isOneVicePrincipal = true;
			}

			$sql = "SELECT RoleID FROM INTRANET_PCM_ROLE WHERE IsVicePrincipal= 1 and isDeleted = 0";
			$vicePrinId = $li->returnVector($sql);
			$vicePrinId = $vicePrinId[0];

			$vicePrinNum = 0;
			if($isOneVicePrincipal == 'oneVicePrincipalAndAllOthers'){
				$vicePrinNum = 1;
			}

			$concatRuleJson = "CONCAT('{', 
			IF(NeedGroupHeadApprove = 1, 
				IF(NeedVicePrincipalApprove = 1 or ApprovalRoleID > 0, '\"-1\":\"0\",', '\"-1\":\"0\"'),
				''), 
			IF(NeedVicePrincipalApprove=1, 
				IF(ApprovalRoleID > 0, '\"$vicePrinId\":\"$vicePrinNum\",', '\"$vicePrinId\":\"$vicePrinNum\"'), 
				''), 
			IF(ApprovalRoleID > 0, 
				CONCAT('\"',ApprovalRoleID,'\":\"0\"'),
				''), 
			'}')";
			$sql = "UPDATE INTRANET_PCM_RULE  SET CaseApprovalRule = $concatRuleJson WHERE CaseApprovalRule = ''";
			$result = $li->db_db_query($sql);

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING
			(Module, SettingName, SettingValue, DateInput)
			values
			('$ModuleName', 'ePCM_CaseApprovalRule_20171206', '$result', now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= " ePCM patch Approval Rule [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  ePCM patch Approval Rule [End]
		#################################################################################


		#################################################################################
		###  eLibPlus - sync BookCategoryType from LIBMS db to INTRANET_ELIB_BOOK [Start]
		#################################################################################
		if(!$GSary['elib_sync_book_category_type'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_book_category_type.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - sync BookCategoryType from LIBMS db to INTRANET_ELIB_BOOK [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/script/patch_elib_book_category_type.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_sync_book_category_type', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - sync BookCategoryType from LIBMS db to INTRANET_ELIB_BOOK [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - sync BookCategoryType from LIBMS db to INTRANET_ELIB_BOOK [End]
		#################################################################################

		#################################################################################
		###  eLibPlus - break circulation access right "return" into "return" and "lost"  [Start]
		#################################################################################
		if(!$GSary['elib_break_return_right'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_break_circulation_return.php"))
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - break 'return' right into 'return' and 'lost' right [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$FromCentralUpdate = 1;
			include_once($intranet_root."/addon/script/patch_elib_break_circulation_return.php");

			# update General Settings - marked the script is executed
			$sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'elib_break_return_right', 1, now())";

			$Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "eLibPlus - break 'return' right into 'return' and 'lost' right [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - break circulation access right "return" into "return" and "lost" [End]
		#################################################################################


		#################################################################################
		###  eLibPlus - update TransAmountBefore and TransAmountAfter for LIBMS_BALANCE_LOG [Start]
		#################################################################################
		if(!$GSary['elib_update_overdue_log_transaction'] && $plugin['library_management_system'] && file_exists($intranet_root."/addon/script/patch_elib_overdue_log_transaction.php"))
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eLibPlus - update TransAmountBefore and TransAmountAfter for LIBMS_BALANCE_LOG [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    $FromCentralUpdate = 1;
		    include_once($intranet_root."/addon/script/patch_elib_overdue_log_transaction.php");

		    # update General Settings - marked the script is executed
		    $sql = "insert ignore into GENERAL_SETTING
		    (Module, SettingName, SettingValue, DateInput)
		    values
		    ('$ModuleName', 'elib_update_overdue_log_transaction', 1, now())";

		    $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

		    $x .= "===================================================================<br>\r\n";
		    $x .= "eLibPlus - update TransAmountBefore and TransAmountAfter for LIBMS_BALANCE_LOG [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eLibPlus - update TransAmountBefore and TransAmountAfter for LIBMS_BALANCE_LOG[End]
		#################################################################################


		#####################################################################################################################
		### [Start] teacher appraisal -  Insert CPD form to be filled in default template
		#####################################################################################################################
		if(!$GSary['ta_20180111_insert_CPD_to_default_forms']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] teacher appraisal -  Insert CPD form to be filled in default template       <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;

			$sql = "SELECT TemplateID, InputBy_UserID, ModifiedBy_UserID  FROM INTRANET_PA_S_FRMPRE GROUP BY TemplateID";
			$templates = $li->returnArray($sql);

			foreach($templates as $tmp){
				/* Add the new forms */
				$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMPRE WHERE TemplateID='".$tmp['TemplateID']."' AND DataTypeID=16";
				$curr = $li->returnVector($sql);
				if(empty($curr)){
					$sql = "INSERT INTO INTRANET_PA_S_FRMPRE
								(TemplateID, DataTypeID, DescrChi, DescrEng, DisplayOrder, Incl, InputBy_UserID, DateTimeInput, ModifiedBy_UserID, DateTimeModified) 
							VALUES
								(".$tmp['TemplateID'].",16,'進修資料', 'CPD', 16,1,'".$tmp['InputBy_UserID']."',NOW(),'".$tmp['ModifiedBy_UserID']."',NOW())";
					$li->db_db_query($sql);
				}
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ta_20180111_insert_CPD_to_default_forms', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] teacher appraisal -  Insert CPD form to be filled in default template            <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] teacher appraisal -  Insert CPD form to be filled in default template
		#####################################################################################################################

		##################################################################################################
		### [Start] ePCM initial data
		##################################################################################################
		if(!$GSary['ePCM_patch_old_approval_data']){
			$li->db = $intranet_db;

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] ePCM patch old data to support new approval logic           <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$sql = "SELECT CaseID, ApproverUserID, ApprovalType, ApprovalRoleID, ApprovalSequence FROM INTRANET_PCM_CASE_APPROVAL WHERE ApprovalRoleID = 0 AND ApprovalSequence = 0";
			$recordsNeedPatch = $li->returnResultSet($sql);
			$patch_success = -1;
			if($recordsNeedPatch > 0){
				$caseIDArr = array_unique(Get_Array_By_Key($recordsNeedPatch, 'CaseID'));
				sort($caseIDArr);

				$sql = "SELECT CaseID, RuleID from INTRANET_PCM_CASE WHERE CaseID IN ('".implode("','", $caseIDArr)."') AND RuleID != 0";
				$temp = $li->returnResultSet($sql);
				$caseRuleMappingAssoc = BuildMultiKeyAssoc($temp, 'CaseID', array('RuleID'), 1);
				$ruleIDArr = Get_Array_By_Key($temp, 'RuleID');
				unset($temp);

				$sql = "SELECT * FROM INTRANET_PCM_RULE WHERE RuleID IN ('".implode("','", $ruleIDArr)."')";
				$ruleInfoAssoc = BuildMultiKeyAssoc($li->returnResultSet($sql), 'RuleID');

				$sql = "SELECT RoleID, IF(IsPrincipal=1,'P','VP') as Role from INTRANET_PCM_ROLE WHERE IsPrincipal = 1 or IsVicePrincipal = 1";
				$principalRoleIdAssoc = BuildMultiKeyAssoc($li->returnResultSet($sql), 'Role', array('RoleID'), 1);

				# backup
				$sql = "CREATE TABLE INTRANET_PCM_CASE_APPROVAL_patch_old_data_bak_25911 SELECT * FROM INTRANET_PCM_CASE_APPROVAL";
				$bak_success = $li->db_db_query($sql);

				$patch_success = -2;
				$needPatchCount = count($recordsNeedPatch);
				$successCount = 0;
				if($bak_success){
					foreach($recordsNeedPatch as $_record){

						$_caseId = $_record['CaseID'];
						$_approvalUserId = $_record['ApproverUserID'];
						$_approvalTye = $_record['ApprovalType'];
						$_thisRule = $ruleInfoAssoc[$caseRuleMappingAssoc[$_caseId]];

						if(!empty($_thisRule)){
							$_mode = $_thisRule['ApprovalMode'];
							if($_mode == 'T'){ # T = Together
								$_setSequence = 1;
							}else{
								$_setSequence = 1;
								if($_thisRule['ApprovalRoleID'] > 1){
									$_setSequence = 1;
								}else{
									if($_thisRule['NeedGroupHeadApprove'] == 1
											&& $_thisRule['NeedVicePrincipalApprove'] == 1
											&& $_thisRule['ApprovalRoleID'] == 1){

												if($_approvalTye == 'G'){ # GroupHead
													$_setSequence = 1;
												}else if ($_approvalTye == 'V'){ # VP
													$_setSequence = 2;
												}else if ($_approvalTye == 'R'){  # Other Role (if rule specific principal approve mean Principal)
													$_setSequence = 3;
												}

									}else if($_thisRule['NeedGroupHeadApprove'] == 1
											&& $_thisRule['NeedVicePrincipalApprove'] == 1
											&& $_thisRule['ApprovalRoleID'] == 0){

												if($_approvalTye == 'G'){ # GroupHead
													$_setSequence = 1;
												}else if ($_approvalTye == 'V'){ # VP
													$_setSequence = 2;
												}

									}else if($_thisRule['NeedGroupHeadApprove'] == 1
											&& $_thisRule['NeedVicePrincipalApprove'] == 0
											&& $_thisRule['ApprovalRoleID'] == 1){

												if($_approvalTye == 'G'){ # GroupHead
													$_setSequence = 1;
												}else if ($_approvalTye == 'R'){ # Other Role (if rule specific principal approve mean Principal)
													$_setSequence = 2;
												}

									}else if($_thisRule['NeedGroupHeadApprove'] == 1
											&& $_thisRule['NeedVicePrincipalApprove'] == 0
											&& $_thisRule['ApprovalRoleID'] == 1){
												if($_approvalTye == 'V'){ # VP
													$_setSequence = 1;
												}else if ($_approvalTye == 'R'){ # Other Role (if rule specific principal approve mean Principal)
													$_setSequence = 2;
												}
									}
								}
							}

							if($_approvalTye == 'G'){ # GroupHead
								$_setRoleId = -1;
							}else if ($_approvalTye == 'V'){ # VP
								$_setRoleId = $principalRoleIdAssoc['VP'];
							}else if ($_approvalTye == 'R'){ # Other Role (if rule specific principal approve mean Principal)
								$_setRoleId = $_thisRule['ApprovalRoleID'];
							}else {
								$_setRoleId = -1;
							}

							$sql = "UPDATE INTRANET_PCM_CASE_APPROVAL
							SET ApprovalRoleID = '$_setRoleId', ApprovalSequence = '$_setSequence'
							WHERE CaseID = '$_caseId' AND ApproverUserID = '$_approvalUserId' AND ApprovalType = '$_approvalTye'";
							$success = $li->db_db_query($sql);

							if($success){
								$successCount ++;
							}
						}
					}
					if($successCount == 0){
						$patch_success = 0;
					}else if($successCount == $needPatchCount){
						$patch_success = 1;
					}else if($successCount < $needPatchCount){
						$patch_success = 2;
					}else if($successCount > $needPatchCount){
						$patch_success = 3;
					}
				}
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('ePCM', 'ePCM_patch_old_approval_data', '$patch_success', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] ePCM patch old data to support new approval logic             <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}

		if (!$GSary['PatchNullScoreAssessmentResult'])
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "Patch Null Score Assessment Result [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";


		    $sql = "Update {$eclass_db}.ASSESSMENT_STUDENT_SUBJECT_RECORD Set Score = -1 where Score is NULL";
		    $li->db_db_query($sql);

		    # update General Settings - mark the script is executed
		    $sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'PatchNullScoreAssessmentResult', '".$li->db_affected_rows()."', now())";
		    $li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

		    $x .= "===================================================================<br>\r\n";
		    $x .= "Patch Null Score Assessment Result [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}

		#####################################################################################################################
		### [Start] Student Attendance - Initialize values for TeacherCanManageProveDocumentStatus settings.
		#####################################################################################################################
		if($plugin['attendancestudent'] && !$GSary['StudentAttendance_Init_TeacherCanManageProveDocumentStatus']){

			$x .= "=============================================================================================<br>\n";
			$x .= "[Start] Student Attendance - Initialize values for TeacherCanManageProveDocumentStatus settings.             <br>\n";
			$x .= "=============================================================================================<br>\n";

			$li->db = $intranet_db;

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('StudentAttendance', 'TeacherCanManageProveDocumentStatus', '1', now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudentAttendance_Init_TeacherCanManageProveDocumentStatus', '1', now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\n";
			$x .= "[End] Student Attendance - Initialize values for TeacherCanManageProveDocumentStatus settings.               <br>\n";
			$x .= "=============================================================================================<br>\n";
		}
		#####################################################################################################################
		### [End] Student Attendance - Initialize values for TeacherCanManageProveDocumentStatus settings.
		#####################################################################################################################

		#####################################################################################################################
		### [Start] teacher appraisal -  Insert question data to INTRANET_PA_S_FRMPRE_SUB
		#####################################################################################################################
		if(!$GSary['ta_20180411_insert_question_data_to_default_forms_sub_form']){
			$x .= "=============================================================================================<br>\r\n";
			$x .= "[Start] teacher appraisal -  Insert question data to INTRANET_PA_S_FRMPRE_SUB       <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";

			$li->db = $intranet_db;

			$sql = "SELECT TemplateID, InputBy_UserID, ModifiedBy_UserID  FROM INTRANET_PA_S_FRMPRE GROUP BY TemplateID";
			$templates = $li->returnArray($sql);

			foreach($templates as $tmp){
				/* Add the new forms */
				$sql = "SELECT TemplateID FROM INTRANET_PA_S_FRMPRE_SUB WHERE TemplateID='".$tmp['TemplateID']."'";
				$curr = $li->returnVector($sql);
				if(empty($curr)){
					$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'1' as DataSubTypeID,'姓名(英文)' as DescrChi,'Teacher (Eng.)' as DescrEng,1 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'2' as DataSubTypeID,'姓名(中文)' as DescrChi,'Teacher (Chi.)' as DescrEng,2 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'1' as DataTypeID,'3' as DataSubTypeID,'性別' as DescrChi,'Gender' as DescrEng,3 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'1' as DataSubTypeID,'職級' as DescrChi,'Position/Rank' as DescrEng,1 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'2' as DataSubTypeID,'出任此職級日期' as DescrChi,'Entry Date for this Position' as DescrEng,2 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'3' as DataSubTypeID,'進入本學校服務日期' as DescrChi,'Entry Date to this School' as DescrEng,3 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'4' as DataSubTypeID,'進入本團體服務日期' as DescrChi,'Entry Date to this School Organization' as DescrEng,4 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'5' as DataSubTypeID,'年資(官/津校)' as DescrChi,'Seniority (Gov./Aided)' as DescrEng,5 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'6' as DataSubTypeID,'年資(私校)' as DescrChi,'Seniority (Private)' as DescrEng,6 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				$sql = "INSERT INTO INTRANET_PA_S_FRMPRE_SUB(TemplateID,DataTypeID,DataSubTypeID ,DescrChi,DescrEng,DisplayOrder,Incl,InputBy_UserID,DateTimeInput,ModifiedBy_UserID,DateTimeModified)
						SELECT '".$tmp['TemplateID']."' as TemplateID,'2' as DataTypeID,'7' as DataSubTypeID,'服務本會學校總年資（包括本年度）' as DescrChi,'Seniority in this School Organization（including this year）' as DescrEng,7 as DisplayOrder,1 as Incl,
						".IntegerSafe($_SESSION['UserID'])." as InputBy_UserID,NOW() as DateTimeInput,".IntegerSafe($_SESSION['UserID'])." as ModifiedBy_UserID,NOW() as DateTimeModified";
				$li->db_db_query($sql);
				}
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ta_20180411_insert_question_data_to_default_forms_sub_form', '1', now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "=============================================================================================<br>\r\n";
			$x .= "[End] teacher appraisal -  Insert question data to INTRANET_PA_S_FRMPRE_SUB         <br>\r\n";
			$x .= "=============================================================================================<br>\r\n";
		}
		#####################################################################################################################
		### [End] teacher appraisal -  Insert question data to INTRANET_PA_S_FRMPRE_SUB
		#####################################################################################################################

		#######################################################################################################################
		### [Start] Student Attendance - Patch preset absence records approval time and approval user from apply leave records.
		#######################################################################################################################
		if($plugin['attendancestudent'] && $plugin['eClassApp'] && !$GSary['StudentAttendance_PatchPresetAbsenceRecordsApprovalTimeFromApplyLeave']){

			$x .= "===============================================================================================================================<br>\n";
			$x .= "[Start] Student Attendance - Patch preset absence records approval time and approval user from apply leave records.            <br>\n";
			$x .= "===============================================================================================================================<br>\n";

			$li->db = $intranet_db;

			$sql = "SELECT 
						p.*,a.ModifiedDate,a.ModifiedBy 
					FROM CARD_STUDENT_PRESET_LEAVE as p 
					INNER JOIN CARD_STUDENT_APPLY_LEAVE_RECORD as a ON a.StudentID=p.StudentID AND a.ApprovalStatus='1' AND a.IsDeleted='0' AND p.RecordDate BETWEEN a.StartDate AND a.EndDate 
					WHERE p.ApprovedAt IS NULL AND p.Approvedby IS NULL AND a.InputDate <= p.DateInput";
			$to_patch_records = $li->returnResultSet($sql);
			if(count($to_patch_records)>0){
				foreach($to_patch_records as $patch_record){
					$li->db_db_query("UPDATE CARD_STUDENT_PRESET_LEAVE SET ApprovedAt='".$patch_record['ModifiedDate']."',ApprovedBy='".$patch_record['ModifiedBy']."' WHERE RecordID='".$patch_record['RecordID']."' ");
				}
			}

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudentAttendance_PatchPresetAbsenceRecordsApprovalTimeFromApplyLeave', '1', now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "================================================================================================================================<br>\n";
			$x .= "[End] Student Attendance - Patch preset absence records approval time and approval user from apply leave records.               <br>\n";
			$x .= "================================================================================================================================<br>\n";
		}
		#######################################################################################################################
		### [End] Student Attendance - Patch preset absence records approval time and approval user from apply leave records.
		#######################################################################################################################


		#################################################################################
		###  eSchoolBus - update RouteTeacher to be multiple  [Start]
		#################################################################################
		if(!$GSary['eSchoolBus_Update_RouteTeacher_To_Multiple'] && $plugin['eSchoolBus'] )
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "eSchoolBus - update RouteTeacher to be multiple [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    $result = array();
		    $li->Start_Trans();
		    $sql = "INSERT INTO INTRANET_SCH_BUS_ROUTE_TEACHER SELECT RouteID, TeacherID FROM INTRANET_SCH_BUS_ROUTE WHERE TeacherID IS NOT NULL AND TeacherID<>0";
	        $result[] = $li->db_db_query($sql);

		    if (!in_array(false,$result)) {
		        $li->Commit_Trans();

		        # update General Settings - marked the script is executed
		        $sql = "insert ignore into GENERAL_SETTING
		        (Module, SettingName, SettingValue, DateInput)
		        values
		        ('$ModuleName', 'eSchoolBus_Update_RouteTeacher_To_Multiple', 1, now())";

		        $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());
		    }
		    else {
		        $li->RollBack_Trans();
		    }
		    unset($result);

		    $x .= "===================================================================<br>\r\n";
		    $x .= "eSchoolBus - update RouteTeacher to be multiple [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		#################################################################################
		###  eSchoolBus - update RouteTeacher to be multiple [End]
		#################################################################################

		###############################################################################################
		###  Clear INTRANET_USER.UserPassword and INTRANET_ARCHIVE_USER.UserPassword to NULL  [Start]
		###############################################################################################
		if (!$GSary['ClearUserPassword_20190621'])
		{
		    $x .= "=======================================================================================<br>\r\n";
		    $x .= "Clear INTRANET_USER.UserPassword and INTRANET_ARCHIVE_USER.UserPassword to NULL [Start]<br>\r\n";
		    $x .= "=======================================================================================<br>\r\n";

		    $sql = "UPDATE INTRANET_USER SET UserPassword=NULL";
		    $li->db_db_query($sql);
			$clear_intranet_user_password_affected_rows = $li->db_affected_rows();

			$sql = "UPDATE INTRANET_ARCHIVE_USER SET UserPassword=NULL";
		    $li->db_db_query($sql);
			$clear_intranet_archive_user_password_affected_rows = $li->db_affected_rows();

		    # update General Settings - mark the script is executed
		    $sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ClearUserPassword_20190621', '".($clear_intranet_user_password_affected_rows+$clear_intranet_archive_user_password_affected_rows)."', now())";
		    $li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

		    $x .= "=====================================================================================<br>\r\n";
		    $x .= "Clear INTRANET_USER.UserPassword and INTRANET_ARCHIVE_USER.UserPassword to NULL [End]<br>\r\n";
		    $x .= "=====================================================================================<br>\r\n";
		}
		##############################################################################################
		###  Clear INTRANET_USER.UserPassword and INTRANET_ARCHIVE_USER.UserPassword to NULL  [End]
		##############################################################################################


		###############################################################################################
		### [Start] ePayment Add Default Push Message
		###############################################################################################
		if (!$GSary['ePaymentDefaultPushMessage_20190724'])
		{
			$x .= "=======================================================================================<br>\r\n";
			$x .= "[Start] ePayment Add Default Push Message<br>\r\n";
			$x .= "=======================================================================================<br>\r\n";

			$sql = "INSERT INTO INTRANET_APP_PUSH_MESSAGE_TEMPLATE (`TemplateID`, `Title`, `Content`, `FromModule`, `ModuleSection`, `RecordStatus`, `DateInput`, `InputBy`, `DateModified`, `ModifiedBy`, `CanDelete`) VALUES (NULL, '".$Lang['ePayment']['DefaultPushMessageTitle']."','".$Lang['ePayment']['DefaultPushMessageContent']."', 'ePayment', '', '1', now(), '1', now(), '1', '0');";
			$li->db_db_query($sql);

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePaymentDefaultPushMessage_20190724', '1', now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "=====================================================================================<br>\r\n";
			$x .= "[End] ePayment Add Default Push Message<br>\r\n";
			$x .= "=====================================================================================<br>\r\n";
		}
		##############################################################################################
		### [End] ePayment Add Default Push Message
		##############################################################################################


		#############################################################################################################################
		###  [Student Attendance] Migrate "Absent Preset Teacher's Remarks" text data to "Preset Teacher's Remarks" DB data [Start]
		#############################################################################################################################
		if (!$GSary['StudentAttendanceMigratePresetTeachersRemarks'])
		{
			include_once($intranet_root."/includes/libcardstudentattend2.php");
			$libcardstudentattend2 = new libcardstudentattend2();

			if(method_exists($libcardstudentattend2,"UpsertTeacherRemarkPresetRecords"))
			{
			    $x .= "==========================================================================================================================<br>\r\n";
			    $x .= "[Student Attendance] Migrate \"Absent Preset Teacher's Remarks\" text data to \"Preset Teacher's Remarks\" DB data [Start]<br>\r\n";
			    $x .= "==========================================================================================================================<br>\r\n";

				$preset_teachers_remarks_data_file_path = $intranet_root."/file/student_attendance/preset_absent_reason2.txt";
				$preset_teachers_remarks_data_ary = array();
				if (file_exists($preset_teachers_remarks_data_file_path)){
					$preset_teachers_remarks_data_ary = explode("\n", trim(get_file_content($preset_teachers_remarks_data_file_path)));
				}

				if(count($preset_teachers_remarks_data_ary)>0){
					foreach($preset_teachers_remarks_data_ary as $preset_teachers_remarks_text_data){
						if(trim($preset_teachers_remarks_text_data) != ''){
							$libcardstudentattend2->UpsertTeacherRemarkPresetRecords(array('ReasonText'=>trim($preset_teachers_remarks_text_data)));
						}
					}
				}

			    # update General Settings - mark the script is executed
			    $sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudentAttendanceMigratePresetTeachersRemarks', '1', now())";
			    $li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			    $x .= "========================================================================================================================<br>\r\n";
			    $x .= "[Student Attendance] Migrate \"Absent Preset Teacher's Remarks\" text data to \"Preset Teacher's Remarks\" DB data [End]<br>\r\n";
			    $x .= "========================================================================================================================<br>\r\n";
			}
		}
		###########################################################################################################################
		###  [Student Attendance] Migrate "Absent Preset Teacher's Remarks" text data to "Preset Teacher's Remarks" DB data [End]
		###########################################################################################################################


		###########################################################
		###  Migrate student registry submitted status hk to STUDENT_REGISTRY_SUBMITTED by AcademicYear [Start]
		###########################################################
		if(!$GSary['Migrate_student_registry_submitted_status_hk'] && $plugin['StudentRegistry_HongKong'])
		{
		    $x .= "===================================================================<br>\r\n";
		    $x .= "Migrate student registry submitted status hk to STUDENT_REGISTRY_SUBMITTED by AcademicYear [Start]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";

		    $sql = "SELECT 
                        	UserID,
                        	DATA_CONFIRM_DATE,
                            DATA_CONFIRM_BY
                    FROM 
                        	STUDENT_REGISTRY_STUDENT
                    WHERE 	
                        	DATA_CONFIRM_DATE IS NOT NULL 
                        	ORDER BY UserID";
		    $confirmAry = $li->returnResultSet($sql);
		    if (count($confirmAry)) {
		        foreach((array)$confirmAry as $_confirmAry) {
		            $_userID = $_confirmAry['UserID'];
		            $_confirmDate = $_confirmAry['DATA_CONFIRM_DATE'];
		            $_confirmBy = $_confirmAry['DATA_CONFIRM_BY'];
		            $_academicYearInfo = getAcademicYearInfoAndTermInfoByDate($_confirmDate);
		            $_academicYearID = $_academicYearInfo[0];
		            $sql = "INSERT IGNORE INTO STUDENT_REGISTRY_SUBMITTED (UserID, AcademicYearID, DATA_CONFIRM_DATE, DATA_CONFIRM_BY) VALUES ";
		            $sql .= "('$_userID','$_academicYearID','$_confirmDate','$_confirmBy')";
		            $li->db_db_query($sql);
		        }
		    }
		    # update General Settings - mark the script is executed
		    $sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Migrate_student_registry_submitted_status_hk', 1, now())";
		    $li->db_db_query($sql) or debug_pr(mysql_error());

		    $x .= "===================================================================<br>\r\n";
		    $x .= "Migrate student registry submitted status hk to STUDENT_REGISTRY_SUBMITTED by AcademicYear [End]<br>\r\n";
		    $x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Migrate student registry submitted status hk to STUDENT_REGISTRY_SUBMITTED by AcademicYear [End]
		###########################################################


        #################################################################################
        ###  eSchoolBus - fix RecordStatus to DayTime section level  [Start]
        #################################################################################
        if(!$GSary['eSchoolBus_DayTimeslotRecordStatus'] && $plugin['eSchoolBus'] && file_exists($intranet_root."/addon/script/patch_eSchoolBus_applyLeave.php"))
        {
            $x .= "===================================================================<br>\r\n";
            $x .= "eSchoolBus - fix RecordStatus to DayTime section level  [Start]<br>\r\n";
            $x .= "===================================================================<br>\r\n";

            $FromCentralUpdate = 1;
            include_once($intranet_root."/addon/script/patch_eSchoolBus_applyLeave.php");

            # update General Settings - marked the script is executed
            $sql = "insert ignore into GENERAL_SETTING 
								(Module, SettingName, SettingValue, DateInput) 
							values 
								('$ModuleName', 'eSchoolBus_DayTimeslotRecordStatus', 1, now())";

            $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

            $x .= "===================================================================<br>\r\n";
            $x .= "eSchoolBus - fix RecordStatus to DayTime section level  [End]<br>\r\n";
            $x .= "===================================================================<br>\r\n";
        }
        #################################################################################
        ###  eSchoolBus - fix RecordStatus to DayTime section level  [End]
        #################################################################################


		###########################################################
		###  Add Waived & Reason to CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_ [Start]
		###########################################################
		if(!$GSary['Student_Hostel_Attendance_Waived'] && $plugin['attendancestudent'])
		{
			$x .= "===================================================================<br>\r\n";
			$x .= "Add Waived & Reason to CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_ [Start]<br>\r\n";
			$x .= "===================================================================<br>\r\n";

			$db = new libdb();

			// get current academic year in school
			$sql = 'show tables like \'CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_%\'';
			$Temp = $li->returnArray($sql);

			for ($i=0; $i< sizeof($Temp); $i++) {
				$sql = 'ALTER TABLE '.$Temp[$i][0].' ADD COLUMN Waived int(11) default NULL AFTER Remark;';
				$li->db_db_query($sql) or debug_pr(mysql_error());
				$sql = 'ALTER TABLE '.$Temp[$i][0].' ADD COLUMN Reason mediumtext default NULL AFTER Waived;';
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Student_Hostel_Attendance_Waived', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());

			$x .= "===================================================================<br>\r\n";
			$x .= "Add Waived & Reason to CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_ [End]<br>\r\n";
			$x .= "===================================================================<br>\r\n";
		}
		###########################################################
		###  Add Waived & Reason to CARD_STUDENT_HOSTEL_ATTENDANCE_DAILY_LOG_ [End]
		###########################################################


		##################################################################################################
		### [Start] Init eLearning timetable
		##################################################################################################
		if (!$GSary['eLearningTimetableInit'])
		{
			$x .= "===========================================<br>\r\n";
			$x .= "<b>Init eLearning timetable [Start]</b><br>\r\n";
			$x .= "===========================================<br>\r\n";

			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('eLearningTimetable', 'enableTimetable', '1', now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eLearningTimetableInit', '1', now())";
			$li->db_db_query($sql) or debug_pr($sql."\n".mysql_error());

			$x .= "===========================================<br>\r\n";
			$x .= "<b>Init eLearning timetable [End]</b><br>\r\n";
			$x .= "===========================================<br>\r\n";
		}
		##################################################################################################
		### [End] Init eLearning timetable
		##################################################################################################

		############################################################
		# Lesson Attendance [Start]
		############################################################
		if(!$GSary['AddLeaveTypeIDFieldLessonAttendance'])
		{
			echo 'Alter auto generate table SUBJECT_GROUP_STUDENT_ATTENDANCE_{AcademicYearID}, add column LateFor....';
			$sql = 'show tables like \'%SUBJECT_GROUP_STUDENT_ATTENDANCE_%\'';
			$Temp = $li->returnArray($sql);

			for ($i=0; $i< sizeof($Temp); $i++) {
				$sql = 'alter table '.$Temp[$i][0].' ADD COLUMN LeaveTypeID int(11) default NULL';
				$li->db_db_query($sql) or debug_pr(mysql_error());
			}
			echo 'OK<br>';

			# update General Settings - mark the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'AddLeaveTypeIDFieldLessonAttendance', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());
		}
		############################################################
		# Lesson Attendance [End]
		############################################################

        #####################################################################################################################
        ### [Start] eNotice - clone approval users of school notice to payment notice
        #####################################################################################################################
        if(!$GSary['eNotice_Clone_Approval_Users_To_Payment_Notice'])
        {
            $x .= "=============================================================================================<br>\n";
            $x .= "<b>eNotice - clone approval users of school notice to payment notice [Start]</b><br>\n";
            $x .= "=============================================================================================<br>\n";

            $li->db = $intranet_db;

            $sql = "SELECT COUNT(*) FROM INTRANET_NOTICE_APPROVAL_USER";
            $approvalUserCount = $li->returnVector($sql);
            if($approvalUserCount[0] > 0) {
                $sql = "INSERT IGNORE INTO INTRANET_NOTICE_APPROVAL_USER (UserID, InputUser, Module, DateInput) SELECT UserID, InputUser, 'Payment' as Module, DateInput FROM INTRANET_NOTICE_APPROVAL_USER";
                $li->db_db_query($sql) or debug_pr(mysql_error());

                $x .= "add approval users success<br>\r\n";
            }

            $sql = "INSERT IGNORE INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) VALUES ('$ModuleName', 'eNotice_Clone_Approval_Users_To_Payment_Notice', '1', NOW())";
            $li->db_db_query($sql) or debug_pr(mysql_error());

            $x .= "=============================================================================================<br>\n";
            $x .= "<b>eNotice - clone approval users of school notice to payment notice [End]</b><br>\n";
            $x .= "=============================================================================================<br>\n";
        }
        #####################################################################################################################
        ### [End] eNotice - clone approval users of school notice to payment notice
        #####################################################################################################################

        #################################################################################
        ###  Repair System - build FollowupPerson table and update LastFollowupGroupID [Start]
        #################################################################################
        if(!$GSary['RepairSystem_Build_FollowupPerson_Table'] && $plugin['RepairSystem'] )
        {
            $x .= "===================================================================<br>\r\n";
            $x .= "Repair System - build FollowupPerson table and update LastFollowupGroupID [Start]<br>\r\n";
            $x .= "===================================================================<br>\r\n";

            $result = array();
            $li->Start_Trans();

            $sql = "UPDATE REPAIR_SYSTEM_RECORDS r INNER JOIN REPAIR_SYSTEM_CATEGORY c ON c.CategoryID=r.CategoryID SET r.LastFollowupGroupID=c.GroupID WHERE r.LastFollowupGroupID=0";
            $result['UpdateLastFollowupGroupID'] = $li->db_db_query($sql);

            $sql = "INSERT INTO REPAIR_SYSTEM_FOLLOWUP_PERSON
                            SELECT 
                                RecordID, FollowUpPersonID 
                            FROM
                                REPAIR_SYSTEM_RECORDS
                            WHERE
                                FollowUpPersonID IS NOT NULL 
                                AND FollowUpPersonID>0";
            $result['AddFollowupPersonID'] = $li->db_db_query($sql);

            if (!in_array(false,$result)) {
                $li->Commit_Trans();

                # update General Settings - marked the script is executed
                $sql = "INSERT IGNORE INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) VALUES ('$ModuleName', 'RepairSystem_Build_FollowupPerson_Table', 1, now())";
                $Result['UpdateGeneralSettings'] = $li->db_db_query($sql) or debug_pr(mysql_error());

                $x .= "RepairSystem_Build_FollowupPerson_Table success<br>\r\n";
            }else{
                $li->RollBack_Trans();
                $x .= "<p style='color:red'>Error occurs when set RepairSystem_Build_FollowupPerson_Table</p>";
            }
            unset($result);

            $x .= "===================================================================<br>\r\n";
            $x .= "Repair System - build FollowupPerson table and update LastFollowupGroupID [End]<br>\r\n";
            $x .= "===================================================================<br>\r\n";
        }
        #################################################################################
        ###  Repair System - build FollowupPerson table and update LastFollowupGroupID [End]
        #################################################################################

        #####################################################################################################################
        ### [Start] eDiscipline - update correct YearTermID for case records
        #####################################################################################################################
        if(!$GSary['eDiscipline_Update_All_Case_Record_YearTermID'])
        {
            $x .= "=============================================================================================<br>\n";
            $x .= "<b>eDiscipline - update correct YearTermID for case records [Start]</b><br>\n";
            $x .= "=============================================================================================<br>\n";

            $li->db = $intranet_db;

            $sql = "SELECT COUNT(*) FROM DISCIPLINE_CASE";
            $caseCount = $li->returnVector($sql);
            if($caseCount[0] > 0)
            {
                $sql = "CREATE TABLE DISCIPLINE_CASE_DATAPATCH_20201124 SELECT * FROM DISCIPLINE_CASE ";
                $li->db_db_query($sql);

                $sql = "SELECT CaseID, EventDate FROM DISCIPLINE_CASE";
                $caseRecords = $li->returnArray($sql);
                foreach((array)$caseRecords as $thisCaseRecord)
                {
                    $thisCaseID = $thisCaseRecord['CaseID'];
                    $thisCaseEventDate = trim($thisCaseRecord['EventDate']);
                    if($thisCaseID > 0 && intranet_validateDate($thisCaseEventDate) && !is_date_empty($thisCaseEventDate))
                    {
                        $yearTermInfo = getAcademicYearInfoAndTermInfoByDate($thisCaseEventDate);
                        $yearTermID = $yearTermInfo[2];
                        if($yearTermID > 0)
                        {
                            $sql = "UPDATE DISCIPLINE_CASE SET YearTermID = '$yearTermID' WHERE CaseID = '$thisCaseID' ";
                            $li->db_db_query($sql) or debug_pr(mysql_error());
                        }
                    }
                }

                $x .= "updated YearTermID for case records<br>\r\n";
            }

            $sql = "INSERT IGNORE INTO GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) VALUES ('$ModuleName', 'eDiscipline_Update_All_Case_Record_YearTermID', '1', NOW())";
            $li->db_db_query($sql) or debug_pr(mysql_error());

            $x .= "=============================================================================================<br>\n";
            $x .= "<b>eDiscipline - update correct YearTermID for case records [End]</b><br>\n";
            $x .= "=============================================================================================<br>\n";
        }
        #####################################################################################################################
        ### [End] eDiscipline - update correct YearTermID for case records
        #####################################################################################################################

		###########
		### End of flag2
		###########

		$x .= "------------------------- Data Patch [End] ---------------------<br>\r\n<br>\r\n";
		echo $x;
	}


	### eRC schema update
	if($plugin['ReportCard2008'])
		include_once($PATH_WRT_ROOT."addon/install/reportcard1.2.php");

	if($plugin['ReportCard_Rubrics'])
		include_once($PATH_WRT_ROOT."addon/install/reportcard_rubrics.php");

	### Power Portfolio schema update
    if($plugin['PowerPortfolio']) {
        include_once($PATH_WRT_ROOT."addon/install/power_portfolio.php");
    }

	### pos schema update
	### include_once($PATH_WRT_ROOT."addon/ip25/pos_migration_script_temp.php"); // moved to top

	### Staff Attendance V3 Mirgration script
	### include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_migration_script_temp.php"); // moved to top

	### Student Attendance - 嚙論堆蕭扆| (嚙瘤嚙瘟嚙編) - [IP2.5] eDiscipline --- misconduct problem [CRM Ref No.: 2010-0707-1137]
	### remove duplicate PROFILE_STUDENT_ATTENDANCE as because some client do not have unique key define in the table
	include_once($PATH_WRT_ROOT."addon/ip25/remove_duplicated_broken_profile_and_delete_edis_late_record.php");

	### Student Attendance - School wish list enhancement list db modification and data patch
	//include_once($PATH_WRT_ROOT."addon/ip25/student_attendance_wish_list_data_patch_201007.php"); // moved to top before general table update

	### Student Attendance data patch to migration previos academic year's class time table setting to new academic year classes
	include_once($PATH_WRT_ROOT."addon/ip25/student_attendance_class_setting_migration.php");

	### Student Attendance - Add field LastTapCardTime to existing CARD_STUDENT_DAILY_LOG_YYYY_MM tables
	if($plugin['attendancestudent']){
		include_once($PATH_WRT_ROOT."addon/ip25/student_attendance_dailylog_add_lasttapcardtime.php");
		include_once($PATH_WRT_ROOT."addon/ip25/student_attendance_add_pm_modify_fields.php");
		include_once($PATH_WRT_ROOT."addon/ip25/student_attendance_fix_nocardaction.php");
		include_once($PATH_WRT_ROOT."addon/ip25/student_attendance_daily_remark_add_daytype.php");
	}

	if($plugin['attendancestaff'] && $module_version['StaffAttendance'] == 3.0)
	{
		### Staff Attendance V3 - remove old unique key
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_daily_log_remove_key.php");

		### Staff Attendance V3 - add ModifyBy to CARD DAILY LOG
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_daily_log_add_modifyby.php");

		### Staff Attendance V3 - remove migration slot name "-",":" to "to",""
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_slot_remove_colon_minus.php");

		### Staff Attendance V3 - remove migration slot name "-",":" to "to","" for CARD_STAFF_ATTENDANCE3_SLOT table
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_slot_remove_colon_minus_slot_table.php");

		### Staff Attendance V3 - add DateConfirmed and ConfirmBy fields and copy DateModified and ModifyBy to them
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_daily_log_add_confirm_fields.php");

		### Staff Attendance V3 - recalculate late mins and earlyleave mins
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_daily_log_recalc_mins.php");

		### Staff Attendance V3 - clean duplicated full day holiday/outing records
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_clean_duplicated_fullleave_records.php");

		### Staff Attendance V3 - fix auto set non-confirmed dailylog and profile waived if reason is default waived
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_default_waive_leave_record.php");

		### Staff Attendance V3 - add missing full day outing records ###
		include_once($PATH_WRT_ROOT."addon/ip25/staff_attend_v3_add_missing_fullday_outing_records.php");
	}

	### iMail Gamma - transfer iMail Preference to new table MAIL_PREFERENCE which only iMail Gamma uses
	if($plugin['imail_gamma'] === true)
		include_once($PATH_WRT_ROOT."addon/ip25/imail_gamma_transfer_preference.php");

	### eInventory - rename item photos
	if($plugin['Inventory'])
		include_once($PATH_WRT_ROOT."addon/ip25/einventory_rename_item_photo.php");

	### iPortfolio - add back SubjectID by the SubjectCode field for the academic records
	if ($plugin['iPortfolio'])
		include_once($PATH_WRT_ROOT."addon/ip25/ipf_subjectcode_subjectid_mapping.php");

	### iPortfolio - add back YearClassID by the ClassName field for the academic records
	if ($plugin['iPortfolio'])
		include_once($PATH_WRT_ROOT."addon/ip25/ipf_classname_classid_mapping.php");

	### iMail (Campus Mail and Web Mail) - Recalculate all users used quota for fast access
	if (!$plugin['imail_gamma']) {
		include_once($PATH_WRT_ROOT."addon/ip25/imail_recalculate_used_quota.php");
	}

	### eInventory - generate barcode for Resource Mgmt Groups and Funding Sources
	if ($plugin['Inventory']) {
		include_once($PATH_WRT_ROOT."addon/ip25/einventory_generate_barcode_for_admingroup_fundingsource.php");
	}

	if($plugin['payment'] && file_exists($intranet_root."/addon/ip25/epayment_transform_payment_item_subsidy_sources.php")) {
		include_once($intranet_root."/addon/ip25/epayment_transform_payment_item_subsidy_sources.php");
	}

	include_once($PATH_WRT_ROOT."addon/ip25/icalendar_fix_group_shared_calendars.php");

	### eBook - update eBook Page Info
	if(file_exists($intranet_root."/addon/script/patch_ebook_eng_classic_page_info.php")){
		include_once($intranet_root."/addon/script/patch_ebook_eng_classic_page_info.php");
	}

	# convert password to hash value
	include_once($PATH_WRT_ROOT."addon/ip25/hashpassword.php");

	if(file_exists($intranet_root."/addon/script/patch_secure_data.php")){
		include_once($intranet_root."/addon/script/patch_secure_data.php");
	}

	// exclude client with Report Card System (Kindergarten Version)
	if(!$plugin['ReportCardKindergarten'])
	{
    	# remove the old .htaccess file that prevent user from direct access photo from broswer
    	if(file_exists($file_path.'/file/user_photo/.htaccess')){
    		unlink($file_path.'/file/user_photo/.htaccess');
    	}
    	if(file_exists($file_path.'/file/.htaccess')){
    		unlink($file_path.'/file/.htaccess');
    	}
    	# create the new .htaccess file that prevent user from direct access log files and photo files from browser
    	if(!file_exists($file_path.'/file/.htaccess')){
    		$fh = fopen($file_path.'/file/.htaccess','w');
    		if($fh){
    			fwrite($fh, '<Files ~ "log$">'."\n"); ### prevent http access to *log files
    			fwrite($fh, "Order allow,deny\n");
    			fwrite($fh, "Deny from all\n");
    			fwrite($fh, "</Files>\n");
    			fwrite($fh, "\n");
    			fwrite($fh, "RewriteEngine On\n");
    			fwrite($fh, "RewriteBase /file/\n");
    			fwrite($fh, "\n");
    			fwrite($fh, 'RewriteRule ^(user_photo)/([a-zA-Z0-9_.-]+)$ /get_image.php?image_dir=$1&image_name=$2 [NC,L]'."\n");
    			fwrite($fh, 'RewriteRule ^(official_photo)/([a-zA-Z0-9_.-]+)$ /get_image.php?image_dir=$1&image_name=$2 [NC,L]'."\n");
    			fwrite($fh, 'RewriteRule ^(photo/personal)/([a-zA-Z0-9_.-]+)$ /get_image.php?image_dir=$1&image_name=$2 [NC,L]'."\n");
    			fclose($fh);
    		}
    	}
	}

	# Schema update completes:
	# log the endtime and update info (latest schema date, time of update, IP of the client)
	# load the log again (for safety)
	$history_content = trim(get_file_content($history_file));

	$time_now = date("Y-m-d H:i:s");
	if ($history_content!="" && $history_content_start!="")
	{
		$history_content_complete = str_replace("[STARTED]", "[COMPLETED],$time_now", $history_content_start);
		$history_content = str_replace($history_content_start, $history_content_complete, $history_content);
	}

	$lf = new phpduoFileSystem();
	$lf->writeFile($history_content, $history_file);

	if ($EnableCompulsory && !$FullUpdateBefore)
	{
		$history_content_compulsory .= "\n".$COMPULSORY_FULLUPDATE[sizeof($COMPULSORY_FULLUPDATE)-1]."'s full update was done at ". date("Y-m-d H:i:s")."\n";
		$lf->writeFile($history_content_compulsory, $history_file_compulsory);
	}


	//show progress
	print "<font color=blue><u>".$update_count."</u> table updates are done!</font><br><center><a id='toend' href='./index.php'>Back to Main</a></center>";
}
intranet_closedb();

if (!isset($flag))
{
?>

<HTML>
<HEAD>
<meta http-equiv='content-type' content='text/html; charset=utf-8' />
<TITLE>eClass</TITLE>
<style type="text/css">
BODY, P, TD {
	FONT-FAMILY: Helvetica, Mingliu, Sans-Serif;
}
</style>
</HEAD>

<BODY>

<hr>
<table border="0" cellpadding="5" cellspacing="5" align="center">
<tr><td colspan="2" height="30" bgcolor="#FAF0F0" align="center"><font color="#8826FF"><b>eClassIP Database Schema Update</b></font></td></tr>
<tr><td align="right" nowrap bgcolor="#FEFEFE">&nbsp; &nbsp; Available of schema update:</td><td bgcolor="#FEFEFE"><font color="<?=(($new_update=='Yes') ? '#FF3333' : '#000000')?>"><?=$new_update?></font></td></tr>
<tr><td align="right" bgcolor="#FEFEFE">Last update from:</td><td nowrap bgcolor="#FEFEFE"><?=$last_update_ip?></td></tr>
<tr><td align="right" bgcolor="#FEFEFE">Last update on:</td><td nowrap bgcolor="#FEFEFE"><?=$last_update?> &nbsp; &nbsp;</td></tr>
<tr><td align="center" colspan="2"><br><br><b>Please select to perform:</b></td></tr>
<?php if ($new_update=="Yes") { ?>
<tr><td align="center" colspan="2">==> <a href="./index.php?flag=2&flag2=1#toend">only new updates & Data Patch</a> <==</td></tr>
<?php } ?>
<tr><td align="center" colspan="2">==> <a href="./index.php?flag=1#toend">all updates again</a> <==</td></tr>

<tr><td align="center" colspan="2">==> <a href="./index.php?flag=1&flag2=1#toend">all updates again & Data Patch</a> <==</td></tr>
</table>

</BODY>
</HTML>


<?php
}
?>
