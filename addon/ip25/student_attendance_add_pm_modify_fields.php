<?
// Editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StudentAttendanceDailyLogAddPMModifyFields'] != 1 && $plugin['attendancestudent']) {
	echo "##### Student Attendance - Adding fields PMDateModified, PMModifyBy to existing CARD_STUDENT_DAILY_LOG tables #####<br>";
	$li = new libdb();
	
	$sql = "SHOW TABLES LIKE 'CARD_STUDENT_DAILY_LOG_%' ";
	$tables = $li->returnVector($sql);
	
	for($i=0;$i<count($tables);$i++){
		$sql = "ALTER TABLE ".$tables[$i]." ADD COLUMN PMDateModified datetime";
		$result['AlterTableAddPMDateModified_'.$tables[$i]] = $li->db_db_query($sql);
		$sql = "ALTER TABLE ".$tables[$i]." ADD COLUMN PMModifyBy int(11)";
		$result['AlterTableAddPMModifyBy_'.$tables[$i]] = $li->db_db_query($sql);
		
		$sql = "UPDATE ".$tables[$i]." SET PMDateModified = DateModified, PMModifyBy = ModifyBy";
		$result['UpdateTablePMModifyFields_'.$tables[$i]] = $li->db_db_query($sql);
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StudentAttendanceDailyLogAddPMModifyFields', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	// end handle daily log schema change
	
	echo "##### Student Attendance - End of adding fields PMDateModified and PMModifyBy to existing CARD_STUDENT_DAILY_LOG tables #####<br>";
}
?>