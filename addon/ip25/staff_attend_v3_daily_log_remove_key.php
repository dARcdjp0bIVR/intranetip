<?
# using: kenneth chung
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StaffAttendV3DailyLogRemoveKey'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']) {
	echo 'removing v2 unique key in v2<br>';
	
	// Handle old Daily log schema change
	$sql = 'show tables like \'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%\'';
	$DailyLogTables = $li->returnVector($sql);
	
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {	
		$sql = 'alter table '.$DailyLogTables[$i].' drop KEY StaffDay';
		$Result[$DailyLogTables[$i].':dropUniqueKey'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add InputBy int(11) after DateInput';
		$Result[$DailyLogTables[$i].':AddInputBy'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add column ModifyBy int(11) after DateModified';
		$Result[$DailyLogTables[$i].':addModifyBy'] = $li->db_db_query($sql);
	}
	// end handle daily log schema change
	
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendV3DailyLogRemoveKey', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	//debug_r($Result);
	
	echo 'Staff Attendance V3 Migration ended<br>';
}
?>