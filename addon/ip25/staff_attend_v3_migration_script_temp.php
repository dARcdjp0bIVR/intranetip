<?
# using: 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = ($PATH_WRT_ROOT!="") ? $PATH_WRT_ROOT : "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StaffAttendanceV3Migration'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']) {
	echo 'Staff Attendance V3 Migration started<br>';
	// tidy up staff attendnace v2 data
	echo 'tidy up staff attendance v2 data<br>';
	$sql = 'show tables like \'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%\'';
	$DailyLogTables = $li->returnVector($sql);
	
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {
		$Temp = explode('_',$DailyLogTables[$i]);
		$Year = $Temp[(sizeof($Temp)-2)];
		$Month = $Temp[(sizeof($Temp)-1)];
		$LogDate = strtotime($Year.'-'.$Month.'-01');
		$Today = time();
		// delete all daily log table which is after this script run date
		// or table format was not right
		if ($LogDate > $Today || strlen($Month) != 2) {
			$sql = 'drop table '.$DailyLogTables[$i];
			$Result['DeleteInvalidDailyLog'] = $li->db_db_query($sql);
			
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_PROFILE where RecordDate like \''.$Year.'-'.$Month.'%\'';
			$Result['DeleteProfileRecordFor:'.$Year.'-'.$Month] = $li->db_db_query($sql);
		}
		// tidy profile record which according to the record on daily log
		else {
			// update all InAttendnaceRecordID and OutAttendanceRecordID to NULL
			$sql = 'update '.$DailyLogTables[$i].' set 
								InAttendanceRecordID = NULL,
								OutAttendanceRecordID = NULL';
			$Result['SetToNULLAttendanceRecordID:'.$DaiDailyLogTables[$i]] = $li->db_db_query($sql);
			
			// update all Out School Status and record type when in holiday/ outgoing
			$sql = 'update '.$DailyLogTables[$i].' set 
								Duty = 0,
								StaffPresent = 0,
								DutyStart = NULL,
								DutyEnd = NULL,
								OutSchoolStatus = InSchoolStatus,
								RecordType = InSchoolStatus 
							where 
								InSchoolStatus in (4,5)';
			$Result['RepairCardLogHolidayRecord:'.$DaiDailyLogTables[$i]] = $li->db_db_query($sql);
			
			// update all out school status to NULL if in school status is ABSENT 
			$sql = 'update '.$DailyLogTables[$i].' set 
								OutSchoolStatus = NULL 
							where 
								InSchoolStatus = 1';
			$Result['RepairCardLogAbsentRecord:'.$DaiDailyLogTables[$i]] = $li->db_db_query($sql);
			
			// update all Out school status to early leave when inschool status is normal/ late and out school status is outgoing/ holiday
			// get all record
			$sql = 'select 
								DayNumber,
								StaffID,
								OutSchoolStatus 
							from 
								'.$DailyLogTables[$i].' 
							where 
								InSchoolStatus in (0,2) 
								and 
								OutSchoolStatus in (4,5)';
			$PMOutgoingList = $li->returnArray($sql);
			// create early leave profile
			for ($j=0; $j< sizeof($PMOutgoingList); $j++) {
				$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE (
								RecordDate,
								RecordType,
								StaffID,
								Reason,
								Waived
								)
							values (
								\''.$Year.'-'.$Month.'-'.$PMOutgoingList[$j]['DayNumber'].'\',
								3,
								\''.$PMOutgoingList[$j]['StaffID'].'\',';
				if ($PMOutgoingList[$j]['OutSchoolStatus'] == 4) {
					$sql .= "'Holiday',";
				}
				else {
					$sql .= "'Outgoing',";
				}
				$sql .='
								1
								)';
				$Result['InsertProfileEarlyLeaveRecordsOfPMOutgoingHoliday-StaffID:'.$PMOutgoingList[$j]['StaffID'].'RecordDate:'.$Year.'-'.$Month.'-'.$PMOutgoingList[$j]['DayNumber']] = $li->db_db_query($sql);
			}
			// update cardlog
			$sql = 'update '.$DailyLogTables[$i].' set 
								OutSchoolStatus = 3 
							where 
								InSchoolStatus in (0,2) 
								and 
								OutSchoolStatus in (4,5)';
			$Result['SetCardLogOutOutgoingHolidayToEarlyLeave:'.$DailyLogTables[$i]] = $li->db_db_query($sql);
			// End of update all Out school status to early leave when inschool status is normal/ late and out school status is outgoing/ holiday
						
			// delete all record which Duty start and Duty End = 00:00:00 (no need work, but have card log -- problem data)
			$sql = 'delete from '.$DailyLogTables[$i].' where DutyStart = \'00:00:00\' and DutyEnd = \'00:00:00\'';
			$Result['DeleteNoDutyDate:'.$DaiDailyLogTables[$i]] = $li->db_db_query($sql);
			
			// get profile record that synced with daily log status
			$sql = 'select 
								pro.RecordID,
								pro.RecordType,
								pro.RecordDate,
								pro.StaffID 
							from 
								CARD_STAFF_ATTENDANCE2_PROFILE pro 
								INNER JOIN 
								'.$DailyLogTables[$i].' log 
								on 
									pro.StaffID = log.StaffID 
									and 
									pro.RecordDate = CONCAT(\''.$Year.'-'.$Month.'-\',IF(log.DayNumber<10,CONCAT(\'0\',log.DayNumber),log.DayNumber)) 
									and 
									pro.RecordType in (log.InSchoolStatus,log.OutSchoolStatus)
							';
			$SyncedProfileDetail = $li->returnArray($sql,4);
			
			// put back the linked profile's id to daily log table
			unset($SyncedProfileIDs);
			for ($j=0; $j< sizeof($SyncedProfileDetail); $j++) {
				list($RecordID,$RecordType,$RecordDate,$StaffID) = $SyncedProfileDetail[$j];
				
				$SyncedProfileIDs[] = $RecordID;
				$Temp = explode('-',$RecordDate);
				$Day = $Temp[2] + 0;
				if ($RecordType != 3) {
					$ProfileField = 'InAttendanceRecordID';
				}
				else {
					$ProfileField = 'OutAttendanceRecordID';
				}
					
				$sql = 'update '.$DailyLogTables[$i].' set 
									'.$ProfileField.' = \''.$RecordID.'\' 
								where 
									StaffID = \''.$StaffID.'\' 
									and 
									DayNumber = \''.$Day.'\' 
							 ';				
				$Result['InsertProfileIDToDailyLog-StaffID:'.$StaffID.'-RecordDate:'.$RecordDate] = $li->db_db_query($sql);
			}
			
			// delete all profile record for that month other than the record just found (Synced Record)
			$sql = 'delete from CARD_STAFF_ATTENDANCE2_PROFILE 
							where 
								RecordDate = \''.$Year.'-'.$Month.'-%\' ';
			if (sizeof($SyncedProfileIDs) > 0) {
				$sql .= '
								and 
								RecordID NOT IN ('.implode(',',$SyncedProfileIDs).')
								';
			}
			$Result['RemoveUnsyncedProfile:'.$DailyLogTables[$i]] = $li->db_db_query($sql);
			
			// get all daily records which have the late/absent/early leave/holiday/outgoing status but without any profile record linking to it
			$sql = 'select 
								RecordID,
								StaffID,
								DayNumber,
								InSchoolStatus,
								InAttendanceRecordID,
								OutSchoolStatus,
								OutAttendanceRecordID
							from 
								'.$DailyLogTables[$i].' 
							where 
								(
								InSchoolStatus in (1,2,4,5) 
								OR 
								OutSchoolStatus in (3) 
								)
								AND 
								(
								InAttendanceRecordID IS NULL 
								OR 
								OutAttendanceRecordID IS NULL 
								)';
			$UnlinkedDailyLog = $li->returnArray($sql);
			
			for	($j=0; $j< sizeof($UnlinkedDailyLog); $j++) {
				list($RecordID,$StaffID,$DayNumber,$InSchoolStatus,$InProfileID,$OutSchoolStatus,$OutProfileID) = $UnlinkedDailyLog[$j];
				$DayNumber = ($DayNumber > 9)? $DayNumber: '0'.$DayNumber;
				if (trim($InProfileID) == "" && in_array($InSchoolStatus,array(1,2,4,5))) {
					$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE (
										StaffID,
										RecordDate,
										RecordType,
										DateInput,
										DateModified
										)
									values (
										\''.$StaffID.'\',
										\''.$Year.'-'.$Month.'-'.$DayNumber.'\',
										\''.$InSchoolStatus.'\',
										NOW(),
										NOW()
										)';
					$Result['CreateInProfileFor:StaffID:'.$StaffID.'-RecordDate:'.$Year.'-'.$Month.'-'.$DayNumber] = $li->db_db_query($sql);			
					// get Last Insert ID
					$sql = 'select LAST_INSERT_ID()';
					$Temp = $li->returnVector($sql);
					$InProfileID = $Temp[0];
				}
				
				if (trim($OutProfileID) == "" && in_array($OutSchoolStatus,array(3))) {
					$sql = 'insert into CARD_STAFF_ATTENDANCE2_PROFILE (
										StaffID,
										RecordDate,
										RecordType,
										DateInput,
										DateModified
										)
									values (
										\''.$StaffID.'\',
										\''.$Year.'-'.$Month.'-'.$DayNumber.'\',
										\''.$OutSchoolStatus.'\',
										NOW(),
										NOW()
										)';
					$Result['CreateOutProfileFor:StaffID:'.$StaffID.'-RecordDate:'.$Year.'-'.$Month.'-'.$DayNumber] = $li->db_db_query($sql);			
					// get Last Insert ID
					$sql = 'select LAST_INSERT_ID()';
					$Temp = $li->returnVector($sql);
					$OutProfileID = $Temp[0];
				}
				
				$sql = 'update '.$DailyLogTables[$i].' set 
									InAttendanceRecordID = \''.$InProfileID.'\',
									OutAttendanceRecordID = \''.$OutProfileID.'\'
								where 
									RecordID = \''.$RecordID.'\'';
				$Result['PutProfileIDBackToCardLog-StaffID:'.$StaffID.'-RecordDate:'.$Year.'-'.$Month.'-'.$DayNumber] = $li->db_db_query($sql);
			}
		}
	}
	echo 'tidy up staff attendance v2 data--finished!!<br>';
	// end of tidy up staff attendance v2 data
						
	
	// schema update start
	$sql = "CREATE TABLE CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE (
					  TemplateID int(11) NOT NULL auto_increment,
					  TemplateName varchar(255) NOT NULL default '',
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (TemplateID),
					  UNIQUE KEY TemplateName (TemplateName)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	$Result['Create:CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_TEMPLATE'] = $li->db_db_query($sql);

	$sql = "CREATE TABLE CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET (
					  TemplateID int(11) NOT NULL default '0',
					  SlotID int(11) NOT NULL default '0',
					  GroupOrIndividual char(2) NOT NULL default '',
					  ObjID int(11) NOT NULL default '0',
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (TemplateID,SlotID,GroupOrIndividual,ObjID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	$Result['Create:CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_SLOT_TARGET'] = $li->db_db_query($sql);

	$sql = "CREATE TABLE CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION (
					  TemplateID int(11) NOT NULL default '0',
					  TargetDate date NOT NULL default '0000-00-00',
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  PRIMARY KEY  (TemplateID,TargetDate)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8";
	$Result['Create:CARD_STAFF_ATTENDANCE3_SPECIAL_DUTY_DATE_RELATION'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_LAST_ACCESS (
					  UserID int(11) NOT NULL default \'0\',
					  AccessDate date NOT NULL default \'0000-00-00\',
					  DateInput datetime default NULL,
					  DateModify datetime default NULL,
					  PRIMARY KEY  (UserID),
					  KEY AccessDate (AccessDate)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_LAST_ACCESS'] = $li->db_db_query($sql);
	
	
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_SLOT (
					  SlotID int(11) NOT NULL auto_increment,
					  GroupID int(11) default NULL,
					  UserID int(11) default NULL,
					  TemplateID int(11) default NULL,
					  SlotName varchar(255) NOT NULL default \'\',
					  SlotStart time default NULL,
					  SlotEnd time default NULL,
					  DutyCount double default NULL,
					  InWavie int(1) default NULL,
					  OutWavie int(1) default NULL,
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (SlotID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_SLOT'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY (
					  RecordID int(11) NOT NULL auto_increment,
					  UserID int(11) NOT NULL default \'0\',
					  DutyDate date default NULL,
					  SlotID int(11) default NULL,
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (RecordID),
					  UNIQUE KEY DutyDate (DutyDate,UserID,SlotID),
					  KEY DutyDate_2 (DutyDate,UserID),
					  KEY UserID (UserID,DutyDate)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_LEAVE_RECORD add SlotID int(11) default NULL after RecordDate';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY add SlotID int (11) default NULL after DutyDate';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY (
					  WeekDutyID int(11) NOT NULL auto_increment,
					  GroupSlotPeriodID int(11) default NULL,
					  UserID int(11) default NULL,
					  DayOfWeek int(2) default \'0\',
					  EffectiveStart date default NULL,
					  EffectiveEnd date default NULL,
					  SkipSchoolHoliday int(2) default NULL,
					  SkipPublicHoliday int(2) default NULL,
					  SkipSchoolEvent int(2) default NULL,
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (WeekDutyID),
					  UNIQUE KEY CompsiteKey (GroupSlotPeriodID,UserID,DayOfWeek)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT (
					  WeekDutyID int(11) NOT NULL default \'0\',
					  SlotID int(11) NOT NULL default \'0\',
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  PRIMARY KEY  (WeekDutyID,SlotID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_PROFILE add ProfileCountFor double default NULL after Waived';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_PROFILE:ProfileCountFor'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_GROUP add InputBy int(11) default NULL';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_GROUP:InputBy'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_GROUP add ModifyBy int(11) default NULL';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_GROUP:ModifyBy'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_WORKING_PERIOD (
					  WorkingPeriodID int(11) NOT NULL auto_increment,
					  UserID int(11) default NULL,
					  PeriodStart date NOT NULL default \'1970-01-01\',
					  PeriodEnd date NOT NULL default \'2099-12-31\',
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (WorkingPeriodID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_WORKING_PERIOD'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD (
					  GroupSlotPeriodID int(11) NOT NULL auto_increment,
					  GroupID int(11) default NULL,
					  UserID int(11) default NULL,
					  EffectiveStart date NOT NULL default \'1970-01-01\',
					  EffectiveEnd date NOT NULL default \'2099-12-31\',
					  SkipSchoolHoliday int(2) default NULL,
					  SkipPublicHoliday int(2) default NULL,
					  SkipSchoolEvent int(2) default NULL,
					  SkipAcademicEvent int(2) default NULL,
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (GroupSlotPeriodID)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE CARD_STAFF_ATTENDANCE3_REASON (
					  ReasonID int(11) NOT NULL auto_increment,
					  ReasonType int(2) default NULL,
					  DefaultWavie int(2) default NULL,
					  ReasonText varchar(255) default NULL,
					  ReasonDescription text,
					  ReasonActive int(2),
					  DefaultFlag int(2),
					  ReportSymbol varchar(21) default NULL,
					  DateInput datetime default NULL,
					  InputBy int(11) default NULL,
					  DateModify datetime default NULL,
					  ModifyBy int(11) default NULL,
					  PRIMARY KEY  (ReasonID),
					  UNIQUE KEY ReasonType (ReasonType,ReasonText)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8';
	$Result['Create:CARD_STAFF_ATTENDANCE3_REASON'] = $li->db_db_query($sql);
	
	$sql = 'CREATE TABLE IF NOT EXISTS CARD_STAFF_ATTENDANCE3_USER_SETTING(
				SettingID int(11) NOT NULL auto_increment, 
				UserID int(11) NOT NULL,
				SettingName varchar(100) NOT NULL,
				SettingValue text default NULL,
				DisplayName varchar(255) NOT NULL,
				DateModified datetime default NULL,
				PRIMARY KEY(SettingID),
				UNIQUE UniqueName(UserID, SettingName, DisplayName),
				INDEX UserID (UserID)
			) ENGINE=InnoDB CHARSET=utf8 ';
	$Result['Create:CARD_STAFF_ATTENDANCE3_USER_SETTING'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_PROFILE add ReasonID int(11) default NULL after StaffID';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_PROFILE:ReasonID'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_LEAVE_RECORD add ReasonID int(11) default NULL after SlotID';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD:ReasonID'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_LEAVE_RECORD add InputBy int(11) default NULL after DateInput';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD:InputBy'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_LEAVE_RECORD add ModifyBy int(11) default NULL after DateModified';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD:ModifyBy'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_LEAVE_RECORD drop KEY StaffDate';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD:drop KEY StaffDate'] = $li->db_db_query($sql);
	
	$sql = 'alter table CARD_STAFF_ATTENDANCE2_LEAVE_RECORD add KEY StaffDateSlot (StaffID,RecordDate,SlotID)';
	$Result['Alter:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD:add KEY StaffDateSlot'] = $li->db_db_query($sql);
	
	$sql = "ALTER TABLE CARD_STAFF_ATTENDANCE3_REASON ADD COLUMN ReasonActive int(2) default 1 AFTER ReasonDescription";
	$Result['Alter:CARD_STAFF_ATTENDANCE3_REASON:ADD COLUMN ReasonActive'] = $li->db_db_query($sql);
	
	$sql = "alter table CARD_STAFF_ATTENDANCE3_REASON add DefaultFlag int(2) default NULL after ReasonActive";
	$Result['Alter:CARD_STAFF_ATTENDANCE3_REASON:ADD COLUMN DefaultFlag'] = $li->db_db_query($sql);
	
	$sql = "ALTER TABLE CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD ADD COLUMN DayType TINYINT DEFAULT 1 AFTER SkipAcademicEvent";
	$Result['Alter:CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD:ADD COLUMN DayType'] = $li->db_db_query($sql);
	
	$sql = "ALTER TABLE CARD_STAFF_ATTENDANCE3_REASON ADD COLUMN ReasonColor varchar(7) DEFAULT NULL AFTER ReasonActive";
	$Result['Alter:CARD_STAFF_ATTENDANCE3_REASON:ADD COLUMN ReasonColor'] = $li->db_db_query($sql);
	
	$sql = "ALTER TABLE CARD_STAFF_ATTENDANCE2_LEAVE_RECORD ADD COLUMN Remark varchar(255) DEFAULT NULL AFTER Reason";
	$Result['Alter:CARD_STAFF_ATTENDANCE2_LEAVE_RECORD:ADD COLUMN Remark'] = $li->db_db_query($sql);
	// schema update end
	
	$li->Start_Trans();
	
	// insert default employment period for staffs
	$sql = 'select 
						UserID
					from 
						INTRANET_USER 
					where 
						RecordType = 1 
						and 
						RecordStatus in (0,1)';
	$StaffList = $li->returnVector($sql,2);
	for ($i=0; $i< sizeof($StaffList); $i++) {
		$sql = 'insert into CARD_STAFF_ATTENDANCE3_WORKING_PERIOD (
							UserID,
							PeriodStart,
							DateInput,
							DateModify
							)
						values (
							\''.$StaffList[$i].'\',
							\'2006-01-01\',
							NOW(),
							NOW()
							)';
		$Result['SetDefaultEmploymentPeriod:'.$StaffID] = $li->db_db_query($sql);
	}
	
	// get setting from file to databases
	$CurrentSetting = $lgs->Save_General_Setting('StudentAttendance',array("'TerminalIP'","'IgnorePeriod'"));
	$SettingList['IgnoreOTTime'] = trim(get_file_content("$intranet_root/file/staffattend_ot_ignore.txt"));
	$SettingList['TerminalIP'] = ($CurrentSetting['TerminalIP'] != "")? $CurrentSetting['TerminalIP']:get_file_content("$intranet_root/file/stattend_ip.txt");
	$SettingList['IgnorePeriod'] = ($CurrentSetting['IgnorePeriod'] != "")? $CurrentSetting['IgnorePeriod']:get_file_content("$intranet_root/file/stattend_ignore.txt");
	$lgs->Save_General_Setting('StaffAttendance', $SettingList);
	
	// check if is migrate from v1
	$sql = 'select count(1) from CARD_STAFF_ATTENDANCE2_GROUP';
	$V2GroupCount = $li->returnVector($sql);
	if ($V2GroupCount[0] == 0) { // v2 group table don't have any record, which means is from v1
		$sql = 'select 
							GroupID,
							Title
						from 
							CARD_STAFF_ATTENDANCE_GROUP';
		$V1Groups = $li->returnArray($sql);
		
		for ($i=0; $i< sizeof($V1Groups); $i++) {
			$sql = "insert into CARD_STAFF_ATTENDANCE2_GROUP (
								GroupID,
								GroupName
							)
							values (
								'".$V1Groups[$i]['GroupID']."',
								'".$li->Get_Safe_Sql_Query($V1Groups[$i]['Title'])."'
							)";
			$Result['MigrateV1Group:'.$V1Groups[$i]['Title']] = $li->db_db_query($sql);
		}
	}
	
	// config group duty
	$sql = 'select
				  GroupID,
				  DutySun,
				  DutyStartSun,
				  DutyEndSun,
				  DutyMon,
				  DutyStartMon,
				  DutyEndMon,
				  DutyTue,
				  DutyStartTue,
				  DutyEndTue,
				  DutyWed,
				  DutyStartWed,
				  DutyEndWed,
				  DutyThur,
				  DutyStartThur,
				  DutyEndThur,
				  DutyFri,
				  DutyStartFri,
				  DutyEndFri,
				  DutySat,
				  DutyStartSat,
				  DutyEndSat
				From
				  CARD_STAFF_ATTENDANCE2_GROUP';
	$GroupDuty = $li->returnArray($sql);
	
	for ($i=0; $i< sizeof($GroupDuty); $i++) {
		// get group member
		$sql = 'select 
							UserID 
						from 
							CARD_STAFF_ATTENDANCE_USERGROUP 
						where 
							GroupID = \''.$GroupDuty[$i]['GroupID'].'\'';
		$MemberList = $li->returnVector($sql);
		
		// create default slot period for group
		$sql = 'insert into CARD_STAFF_ATTENDANCE3_NORMAL_GROUP_SLOT_PERIOD (
							GroupID,
							EffectiveStart,
							EffectiveEnd,
							SkipSchoolHoliday,
							SkipPublicHoliday,
							SkipSchoolEvent,
							DateInput
							)
						values (
							\''.$GroupDuty[$i]['GroupID'].'\',
							\''.date('Y-m-d').'\',
							\''.(date('Y')+1).'-12-31\',
							0,
							0,
							0,
							NOW()
							)';
		$Result['InsertSlotPeriod:'.$GroupDuty[$i]['GroupID']] = $li->db_db_query($sql);
		
		// get Last Insert ID
		$sql = 'select LAST_INSERT_ID()';
		$Temp = $li->returnVector($sql);
		$GroupSlotPeriodID = $Temp[0];
		
		// create slot from old setting
		$Duty = array();
		if (trim($GroupDuty[$i]['DutyStartSun']) != 0 && trim($GroupDuty[$i]['DutyStartSun']) != "" && trim($GroupDuty[$i]['DutyEndSun']) != "") {
			$Duty[] = array('DayOfWeek'=> 0,'Start'=> $GroupDuty[$i]['DutyStartSun'],'End'=> $GroupDuty[$i]['DutyEndSun']);
		}
		if (trim($GroupDuty[$i]['DutyStartMon']) != 0 && trim($GroupDuty[$i]['DutyStartMon']) != "" && trim($GroupDuty[$i]['DutyEndMon']) != "") {
			$Duty[] = array('DayOfWeek'=> 1,'Start'=> $GroupDuty[$i]['DutyStartMon'],'End'=> $GroupDuty[$i]['DutyEndMon']);
		}
		if (trim($GroupDuty[$i]['DutyStartTue']) != 0 && trim($GroupDuty[$i]['DutyStartTue']) != "" && trim($GroupDuty[$i]['DutyEndTue']) != "") {
			$Duty[] = array('DayOfWeek'=> 2,'Start'=> $GroupDuty[$i]['DutyStartTue'],'End'=> $GroupDuty[$i]['DutyEndTue']);
		}
		if (trim($GroupDuty[$i]['DutyStartWed']) != 0 && trim($GroupDuty[$i]['DutyStartWed']) != "" && trim($GroupDuty[$i]['DutyEndWed']) != "") {
			$Duty[] = array('DayOfWeek'=> 3,'Start'=> $GroupDuty[$i]['DutyStartWed'],'End'=> $GroupDuty[$i]['DutyEndWed']);
		}
		if (trim($GroupDuty[$i]['DutyStartThur']) != 0 && trim($GroupDuty[$i]['DutyStartThur']) != "" && trim($GroupDuty[$i]['DutyEndThur']) != "") {
			$Duty[] = array('DayOfWeek'=> 4,'Start'=> $GroupDuty[$i]['DutyStartThur'],'End'=> $GroupDuty[$i]['DutyEndThur']);
		}
		if (trim($GroupDuty[$i]['DutyStartFri']) != 0 && trim($GroupDuty[$i]['DutyStartFri']) != "" && trim($GroupDuty[$i]['DutyEndFri']) != "") {
			$Duty[] = array('DayOfWeek'=> 5,'Start'=> $GroupDuty[$i]['DutyStartFri'],'End'=> $GroupDuty[$i]['DutyEndFri']);
		}
		if (trim($GroupDuty[$i]['DutyStartSat']) != 0 && trim($GroupDuty[$i]['DutyStartSat']) != "" && trim($GroupDuty[$i]['DutyEndSat']) != "") {
			$Duty[] = array('DayOfWeek'=> 6,'Start'=> $GroupDuty[$i]['DutyStartSat'],'End'=> $GroupDuty[$i]['DutyEndSat']);
		}
			
		for ($j=0; $j< sizeof($Duty); $j++) {
			$sql = 'select 
								SlotID  
							from 
								CARD_STAFF_ATTENDANCE3_SLOT 
							where 
								GroupID = \''.$GroupDuty[$i]['GroupID'].'\' 
								and 
								SlotStart = \''.$Duty[$j]['Start'].'\' 
								and 
								SlotEnd = \''.$Duty[$j]['End'].'\' 
						 ';
			$Exist = $li->returnArray($sql);
			
			if (sizeof($Exist) == 0) {
				$sql = 'insert into CARD_STAFF_ATTENDANCE3_SLOT (
									GroupID,
									SlotName,
									SlotStart,
									SlotEnd,
									DutyCount,
									InWavie,
									OutWavie,
									DateInput
									)
								values (
									\''.$GroupDuty[$i]['GroupID'].'\', 
									\'Staff Attend V2 slot '.str_replace(':','',$Duty[$j]['Start']).' to '.str_replace(':','',$Duty[$j]['End']).'\',
									\''.$Duty[$j]['Start'].'\',
									\''.$Duty[$j]['End'].'\',
									1,
									0,
									0,
									NOW()
									)';
				$Result['InserSlot-'.$GroupDuty[$i]['GroupID'].':'.$Duty[$j]['Start'].'-'.$Duty[$j]['End']] = $li->db_db_query($sql);
				// get Last Insert ID
				$sql = 'select LAST_INSERT_ID()';
				$Temp = $li->returnVector($sql);
				$SlotID = $Temp[0];
			}
			else {
				$SlotID = $Exist[0]['SlotID'];
			}
			
			for ($k=0; $k< sizeof($MemberList); $k++) {
				$sql = 'insert into CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY (
									GroupSlotPeriodID,
									UserID,
									DayOfWeek,
									DateInput
									)
								values (
									\''.$GroupSlotPeriodID.'\',
									\''.$MemberList[$k].'\',
									\''.$Duty[$j]['DayOfWeek'].'\',
									NOW()
									)';
				$Result['InsertWeeklyDuty:'.$MemberList[$k].'-'.$Duty[$j]['DayOfWeek']] = $li->db_db_query($sql);
				// get Last Insert ID
				$sql = 'select LAST_INSERT_ID()';
				$Temp = $li->returnVector($sql);
				$WeekDutyID = $Temp[0];
				
				$sql = 'insert into CARD_STAFF_ATTENDANCE3_USER_WEEKLY_DUTY_SLOT (
									WeekDutyID,
									SlotID, 
									DateInput
									)
								values (
									\''.$WeekDutyID.'\',
									\''.$SlotID.'\',
									NOW()
									)';
				$Result['InsertWeekDutySlot:'.$MemberList[$k].'-'.$Duty[$j]['DayOfWeek'].'-'.$SlotID] = $li->db_db_query($sql);
			}
		}
	}
	
	// Process Special Day Duty
	$sql = 'select 
						udd.UserID,
						udd.DutyDate,
						udd.DutyStart,
						udd.DutyEnd, 
						ug.GroupID 
					from 
						CARD_STAFF_ATTENDANCE2_USER_DATE_DUTY udd 
						LEFT JOIN 
						CARD_STAFF_ATTENDANCE_USERGROUP ug 
						on udd.UserID = ug.UserID 
					where 
						udd.DutyDate > NOW()';
	$SpecialDayDuty = $li->returnArray($sql);
	for ($i=0; $i< sizeof($SpecialDayDuty); $i++) {
		if (trim($SpecialDayDuty[$i]['GroupID']) != "") {
			$cond = 'GroupID = \''.$SpecialDayDuty[$i]['GroupID'].'\' ';
			$Field = 'GroupID';
			$ID = $SpecialDayDuty[$i]['GroupID'];
		}
		else {
			$cond = 'UserID = \''.$SpecialDayDuty[$i]['UserID'].'\' ';
			$Field = 'UserID';
			$ID = $SpecialDayDuty[$i]['UserID'];
		}
		$sql = 'select 
							SlotID  
						from 
							CARD_STAFF_ATTENDANCE3_SLOT 
						where 
							'.$cond.' 
							and 
							SlotStart = \''.$SpecialDayDuty[$i]['DutyStart'].'\' 
							and 
							SlotEnd = \''.$SpecialDayDuty[$i]['DutyEnd'].'\' 
					 ';
		$Exist = $li->returnArray($sql);
		
		if (sizeof($Exist) > 0) {
			$SlotID = $Exist[0]['SlotID'];
		}
		else {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_SLOT (
								'.$Field.',
								SlotName,
								SlotStart,
								SlotEnd,
								DutyCount,
								InWavie,
								OutWavie,
								DateInput
								)
							values (
								\''.$ID.'\', 
								\'Staff Attend V2 slot '.str_replace(':','',$SpecialDayDuty[$j]['Start']).' to '.str_replace(':','',$SpecialDayDuty[$j]['End']).'\',
								\''.$SpecialDayDuty[$i]['DutyStart'].'\',
								\''.$SpecialDayDuty[$i]['DutyEnd'].'\',
								1,
								0,
								0,
								NOW()
								)';
			$Result['InserSlot-'.$Field.'-'.$ID.':'.$SpecialDayDuty[$i]['DutyStart'].'-'.$SpecialDayDuty[$i]['DutyEnd']] = $li->db_db_query($sql);
			// get Last Insert ID
			$sql = 'select LAST_INSERT_ID()';
			$Temp = $li->returnVector($sql);
			$SlotID = $Temp[0];
		}
		
		$sql = 'insert into CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY (
							UserID,
							DutyDate,
							SlotID,
							DateInput
							)
						value (
							\''.$SpecialDayDuty[$i]['UserID'].'\',
							\''.$SpecialDayDuty[$i]['DutyDate'].'\',
							\''.$SlotID.'\',
							NOW() 
							)';
		$Result['InsertShiftDuty:'.$SpecialDayDuty[$i]['DutyDate'].'-'.$SpecialDayDuty[$i]['UserID']] = $li->db_db_query($sql);
	}
	// end setting special date duty
	
	// processing special group date duty
	$sql = "SELECT 
				d.GroupID, d.DutyDate, d.DutyStart, d.DutyEnd 
			FROM CARD_STAFF_ATTENDANCE2_GROUP_DATE_DUTY as d 
			INNER JOIN CARD_STAFF_ATTENDANCE2_GROUP as g ON g.GroupID = d.GroupID 
			WHERE d.Duty = '1' AND d.RecordStatus = '1' 
				AND d.DutyStart IS NOT NULL AND d.DutyEnd IS NOT NULL 
				AND d.DutyStart <> '' AND d.DutyEnd <> '' 
				AND d.DutyDate > NOW() 
			ORDER BY d.GroupID,d.DutyDate ";
	
	$GroupDateDuty = $li->returnArray($sql);
	 
	for($i=0;$i<sizeof($GroupDateDuty);$i++){
		$sql = 'select 
					SlotID  
				from 
					CARD_STAFF_ATTENDANCE3_SLOT 
				where 
					GroupID = \''.$GroupDateDuty[$i]['GroupID'].'\' 
					and 
					SlotStart = \''.$GroupDateDuty[$i]['DutyStart'].'\' 
					and 
					SlotEnd = \''.$GroupDateDuty[$i]['DutyEnd'].'\' 
			 ';
		$Exist = $li->returnArray($sql);
		
		if (sizeof($Exist) > 0) {
			$SlotID = $Exist[0]['SlotID'];
		}
		else {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_SLOT (
								GroupID,
								SlotName,
								SlotStart,
								SlotEnd,
								DutyCount,
								InWavie,
								OutWavie,
								DateInput
								)
							values (
								\''.$GroupDateDuty[$i]['GroupID'].'\', 
								\'Staff Attend V2 slot '.str_replace(':','',$GroupDateDuty[$i]['DutyStart']).' to '.str_replace(':','',$GroupDateDuty[$i]['DutyEnd']).'\',
								\''.$GroupDateDuty[$i]['DutyStart'].'\',
								\''.$GroupDateDuty[$i]['DutyEnd'].'\',
								1,
								0,
								0,
								NOW()
								)';
			$Result['InserSlot-GroupID-'.$GroupDateDuty[$i]['GroupID'].':'.$GroupDateDuty[$i]['DutyStart'].'-'.$GroupDateDuty[$i]['DutyEnd']] = $li->db_db_query($sql);
			// get Last Insert ID
			$sql = 'select LAST_INSERT_ID()';
			$Temp = $li->returnVector($sql);
			$SlotID = $Temp[0];
		}
		
		$sql = "SELECT ug.UserID 
				FROM CARD_STAFF_ATTENDANCE_USERGROUP as ug 
				INNER JOIN INTRANET_USER as u ON ug.UserID = u.UserID AND ug.GroupID = '".$GroupDateDuty[$i]['GroupID']."'
				WHERE u.RecordStatus = '1' AND u.RecordType = '1' 
					AND ug.GroupID = '".$GroupDateDuty[$i]['GroupID']."' ";
		
		$GroupMember = $li->returnVector($sql);
		
		for($j=0;$j<sizeof($GroupMember);$j++){
		 	$user_id = $GroupMember[$j];
		 	
		 	// if user already has CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY with same DutyDate and SlotID, this query will not overwrite it 
		 	$sql = 'insert into CARD_STAFF_ATTENDANCE3_SHIFT_DATE_DUTY (
							UserID,
							DutyDate,
							SlotID,
							DateInput
							)
						value (
							\''.$user_id.'\',
							\''.$GroupDateDuty[$i]['DutyDate'].'\',
							\''.$SlotID.'\',
							NOW() 
							)';
			$Result['InsertShiftDuty:'.$GroupDateDuty[$i]['DutyDate'].'-'.$user_id] = $li->db_db_query($sql);
		}
	}
	// end of processing special group date duty 
	
	// change record type in Leave record table
	$sql = 'update CARD_STAFF_ATTENDANCE2_LEAVE_RECORD set
					  RecordType = 4
					where
					  RecordType = 1';
	$Result['ResetHolidayLeaveRecordType'] = $li->db_db_query($sql);
	
	$sql = 'update CARD_STAFF_ATTENDANCE2_LEAVE_RECORD set
					  RecordType = 5
					where
					  RecordType = 2';
	$Result['ResetOutgoingLeaveRecordType'] = $li->db_db_query($sql);
	
	// insert Leave Reason to new db table CARD_STAFF_ATTENDANCE3_REASON
	$sql = 'select 
						RecordType,
						Reason
					From 
						CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
					group by 
						RecordType, Reason';
	$ReasonList = $li->returnArray($sql);
	for ($i=0; $i< sizeof($ReasonList); $i++) {
		if (trim($ReasonList[$i]['Reason']) != '') {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_REASON (
								ReasonType,
								ReasonText,
								DateInput
								)
							values (
								\''.$ReasonList[$i]['RecordType'].'\',
								\''.$li->Get_Safe_Sql_Query(trim($ReasonList[$i]['Reason'])).'\',
								NOW()
								)';
			$Result['InsertReason:'.$ReasonList[$i]['RecordType'].'-'.$ReasonList[$i]['Reason']] = $li->db_db_query($sql);
			
			// get Last Insert ID
			$sql = 'select LAST_INSERT_ID()';
			$Temp = $li->returnVector($sql);
			$ReasonID = $Temp[0];
			
			$sql = 'update CARD_STAFF_ATTENDANCE2_PROFILE set 
								ReasonID = \''.$ReasonID.'\' 
							where 
								RecordType = \''.$ReasonList[$i]['RecordType'].'\'
								and 
								Reason = \''.$li->Get_Safe_Sql_Query(trim($ReasonList[$i]['Reason'])).'\'';
			$Result['UpdateReasonIDToProfile:'.$ReasonList[$i]['RecordType'].'-'.$ReasonList[$i]['Reason']] = $li->db_db_query($sql);
		}
	}
	
	// select all past on holiday/leave record and add back to card log
	$sql = 'select 
						StaffID,
						RecordDate,
						RecordType,
						Reason 
					from 
						CARD_STAFF_ATTENDANCE2_LEAVE_RECORD 
					where 
						RecordDate <= NOW()';
	$LeaveRecords = $li->returnArray($sql);
	for ($i=0; $i< sizeof($LeaveRecords); $i++) {
		$RecordDateArr = explode('-',$LeaveRecords[$i]['RecordDate']);
		$Year = $RecordDateArr[0];
		$Month = $RecordDateArr[1];
		$Day = $RecordDateArr[2]+0;
		// remove org daily log if there is any
		$sql = 'delete from CARD_STAFF_ATTENDANCE2_DAILY_LOG_'.$Year.'_'.$Month.' 
						where 
							DayNumber = \''.$Day.'\' 
							and 
							StaffID = \''.$LeaveRecords[$i]['StaffID'].'\'';
		$Result['DeleteOrgDutyForLeave/Holiday-Date:'.$LeaveRecords[$i]['RecordDate'].'User:'.$LeaveRecords[$i]['StaffID']] = $li->db_db_query($sql);
		
		// remove org profile if there is any
		$sql = 'delete from CARD_STAFF_ATTENDANCE2_PROFILE 
						where 
							StaffID = \''.$LeaveRecords[$i]['StaffID'].'\' 
							and 
							RecordDate = \''.$LeaveRecords[$i]['RecordDate'].'\'';
		$Result['DeleteOrgProfileForLeave/Holiday-Date:'.$LeaveRecords[$i]['RecordDate'].'User:'.$LeaveRecords[$i]['StaffID']] = $li->db_db_query($sql);
		
		// get the reason that just inserted
		$sql = 'select 
							ReasonID 
						from 
							CARD_STAFF_ATTENDANCE3_REASON 
						where 
							ReasonType = \''.$LeaveRecords[$i]['RecordType'].'\' 
							and 
							Reason = \''.$li->Get_Safe_Sql_Query(trim($ReasonList[$i]['Reason'])).'\'';
		$Temp = $li->returnVector($sql);
		$ReasonID = $Temp[0];
		
		// create new holidy/ outgoing profile record
		$sql = 'Insert into CARD_STAFF_ATTENDANCE2_PROFILE (
							StaffID,
							ReasonID,
							RecordDate,
							RecordType,
							DateInput,
							DateModified
							) 
						values (
							\''.$LeaveRecords[$i]['StaffID'].'\',
							\''.$ReasonID.'\',
							\''.$LeaveRecords[$i]['RecordDate'].'\',
							\''.$LeaveRecords[$i]['RecordType'].'\',
							NOW(),
							NOW()
							)';
		$Result['InsertNewProfileForLeave/Holiday-Date:'.$LeaveRecords[$i]['RecordDate'].'User:'.$LeaveRecords[$i]['StaffID']] = $li->db_db_query($sql);
		// get Last Insert ID
		$sql = 'select LAST_INSERT_ID()';
		$Temp = $li->returnVector($sql);
		$ProfileID = $Temp[0];
		
		// insert new daily log reocrd for holiday/ outgoing
		$sql = 'insert into CARD_STAFF_ATTENDANCE2_DAILY_LOG_'.$Year.'_'.$Month.' (
							StaffID,
							DayNumber,
							InSchoolStatus,
							InAttendanceRecordID,
							OutSchoolStatus,
							RecordType,
							DateInput
							)
						values (
							\''.$LeaveRecords[$i]['StaffID'].'\',
							\''.$Day.'\',
							\''.$LeaveRecords[$i]['RecordType'].'\',
							\''.$ProfileID.'\',
							\''.$LeaveRecords[$i]['RecordType'].'\',
							\''.$LeaveRecords[$i]['RecordType'].'\',
							NOW()
							)';
		$Result['InsertNewDailyLogForLeave/Holiday-Date:'.$LeaveRecords[$i]['RecordDate'].'User:'.$LeaveRecords[$i]['StaffID']] = $li->db_db_query($sql);
	}
	
	
	// insert Profile reason to new db table CARD_STAFF_ATTENDANCE3_REASON
	$sql = 'select 
						RecordType,
						Reason
					From 
						CARD_STAFF_ATTENDANCE2_PROFILE 
					group by 
						RecordType, Reason';
	$ReasonList = $li->returnArray($sql);
	for ($i=0; $i< sizeof($ReasonList); $i++) {
		if (trim($ReasonList[$i]['Reason']) != '') {
			$sql = 'insert into CARD_STAFF_ATTENDANCE3_REASON (
								ReasonType,
								ReasonText,
								DateInput
								)
							values (
								\''.$ReasonList[$i]['RecordType'].'\',
								\''.$li->Get_Safe_Sql_Query($ReasonList[$i]['Reason']).'\',
								NOW()
								)';
			$Result['InsertReason:'.$ReasonList[$i]['RecordType'].'-'.$ReasonList[$i]['Reason']] = $li->db_db_query($sql);
			
			// get Last Insert ID
			$sql = 'select LAST_INSERT_ID()';
			$Temp = $li->returnVector($sql);
			$ReasonID = $Temp[0];
			
			$sql = 'update CARD_STAFF_ATTENDANCE2_PROFILE set 
								ReasonID = \''.$ReasonID.'\' 
							where 
								RecordType = \''.$ReasonList[$i]['RecordType'].'\'
								and 
								Reason = \''.$li->Get_Safe_Sql_Query($ReasonList[$i]['Reason']).'\'';
			$Result['UpdateReasonIDToProfile:'.$ReasonList[$i]['RecordType'].'-'.$ReasonList[$i]['Reason']] = $li->db_db_query($sql);
		}
	}
	
	$li->Commit_Trans();
	
	// Handle old Daily log schema change
	$sql = 'show tables like \'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%\'';
	$DailyLogTables = $li->returnVector($sql);
	
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {
		$sql = 'alter table '.$DailyLogTables[$i].' add SlotName varchar(255) after DayNumber';
		$Result[$DailyLogTables[$i].':addSlotName'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add DutyCount double after SlotName';
		$Result[$DailyLogTables[$i].':addDutyCount'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add InWavie int(1) after DutyCount';
		$Result[$DailyLogTables[$i].':addInWavie'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add OutWavie int(1) after InWavie';
		$Result[$DailyLogTables[$i].':addOutWavie'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add Remark varchar(255) after RecordStatus';
		$Result[$DailyLogTables[$i].':addRemark'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add InputBy int(11) after DateInput';
		$Result[$DailyLogTables[$i].':AddInputBy'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' add ModifyBy int(11) after DateModified';
		$Result[$DailyLogTables[$i].':AddModifyBy'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' drop KEY StaffDay';
		$Result[$DailyLogTables[$i].':dropUniqueKey'] = $li->db_db_query($sql);
		
		$sql = 'alter table '.$DailyLogTables[$i].' ADD UNIQUE KEY CompositeKey (StaffID,DayNumber,SlotName)';
		$Result[$DailyLogTables[$i].':dropUniqueKey'] = $li->db_db_query($sql);
		
		$sql = 'update '.$DailyLogTables[$i].' set 
							SlotName = \'Staff Attendance V2\', 
							DutyCount = \'1\',
							InWavie = 0,
							OutWavie = 0 
						where 
							RecordType NOT in (4,5) 
							or 
							RecordType IS NULL';
		$Result[$DailyLogTables[$i].':updateDefaultSlotInfo'] = $li->db_db_query($sql);
	}
	// end handle daily log schema change
	
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendanceV3Migration', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	//debug_r($Result);
	
	echo 'Staff Attendance V3 Migration ended<br>';
}
?>