<?php
# using: 
/************************************** Description ****************************************
 * 2014-11-18 (Charles Ma)
 * Purpose: ebook info change
 * 
 * in /addon/ip25/index.php, add :
 * 		###########################################################
		###  Update eBook data - 2014-11-18 [Start] 
		###########################################################
		$x .= "===================================================================<br>\r\n";
		$x .= "Update eBook data [Start] <br>\r\n";
		$x .= "===================================================================<br>\r\n";

		include_once($intranet_root."/addon/schema/ebook_info_change.php");
		updateEbookData("ebook_info_change_20141118");
		
		$x .= "===================================================================<br>\r\n";
		$x .= "Update eBook data [End]<br>\r\n";
		$x .= "===================================================================<br>\r\n";
		###########################################################
		###  Update eBook data 2014-11-18 [End]
		###########################################################
 * 
 *******************************************************************************************/
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libelibrary.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libimporttext.php");
intranet_opendb();

// attributes : $current_ebook_info_change_log - name of csv and the task : e.g. ebook_info_change_20141118
function updateEbookData($current_ebook_info_change_log){
	global $x,$intranet_db,$PATH_WRT_ROOT;
	$li = new libdb();
	$li->db = $intranet_db;
	$lgs = new libgeneralsettings();
	$lelib = new elibrary();
	$limport = new libimporttext();
	
	$ModuleName = "eBookUpdate";
	$GSary = $lgs->Get_General_Setting($ModuleName);
	// For checking if this addon has been applied
	
	$filepath = $PATH_WRT_ROOT."addon/ip25/ebook/$current_ebook_info_change_log.csv";
		
	if($GSary[$current_ebook_info_change_log] != 1 && $current_ebook_info_change_log != "" && file_exists($filepath)){
		$data = $limport->GET_IMPORT_TXT($filepath);
		$revised_col = array_shift($data);
		$attribute_col = array_shift($data);
		
		if(current($attribute_col) == "Book ID|Book name|New Book name|Publisher|Author|Category|SubCategory|ISBN|Edition|Summary"){
			$x .= "eBook data update - $current_ebook_info_change_log [Start]<br />";
			
			foreach($data as $index => $value){
				$new_book_array = explode("|",current($value));
				list($BookID,$orgBookName,$BookName,$Publisher,$Author,$Category,$SubCategory,$ISBN,$Edition,$Summary) = $new_book_array;
				
				if($Author != ""){					
					$TmpAuthorID = $lelib->UPDATE_BOOK_AUTHOR($Author,"","");
					$AuthorSQL = ", Author='".addslashes($Author)."', AuthorID='".$TmpAuthorID."'";
				}
				
				$sql = " UPDATE INTRANET_ELIB_BOOK Set InputBy = InputBy ";
				if($BookName != "") $sql .= ", Title = '".addslashes($BookName)."' ";
				if($Publisher != "") $sql .= ", Publisher = '".addslashes($Publisher)."' ";
				if($Summary != "") $sql .= ", Preface = '".addslashes($Summary)."' ";
				if($Category != "") $sql .= ", Category = '".addslashes($Category)."' ";
				if($SubCategory != "") $sql .= ", SubCategory = '".addslashes($SubCategory)."' ";
				if($ISBN != "") $sql .= ", ISBN = '$ISBN' ";
				if($Edition != "") $sql .= ", Edition = '$Edition' ";
				if($Author != "") $sql .= $AuthorSQL;
				$sql .= " Where BookID = '$BookID'  ";
//				debug_r($sql);
				$result = $li->db_db_query($sql);
			}
			
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', '$current_ebook_info_change_log', 1, now())";
			$li->db_db_query($sql) or debug_pr(mysql_error());			
			
			$x .= "eBook data update - $current_ebook_info_change_log [End]<br />";
		}
	}
}
//
//updateEbookData("ebook_info_change_20141205");

?>