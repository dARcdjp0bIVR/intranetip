<?
# using: 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StaffAttendV3DataPatchV81_20110811'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']){
	include_once($PATH_WRT_ROOT."includes/libstaffattend2.php");
	include_once($PATH_WRT_ROOT."includes/libstaffattend3.php");
	echo 'Applying Staff Attendance V3 v8.1 Data Patch...<br>';
	
	$StaffAttend3 = new libstaffattend3();
	
	
    
    $StartDate = "2011-08-11";
    $StartDateTS = strtotime($StartDate);
    $EndDate = date("Y-m-d");
    $EndDateTS = strtotime($EndDate);
    for($ts = $StartDateTS;$ts<=$EndDateTS;$ts+=86400)
    {
    	$sql = "DROP TABLE TEMP_CARD_STAFF_LOG";
	    $StaffAttend3->db_db_query($sql);
	    $sql = "CREATE TABLE TEMP_CARD_STAFF_LOG (
	             RecordDate date,
	             RecordedTime datetime,
	             UserID int,
	             CardID varchar(255),
	             SiteName varchar(255)
	            )ENGINE=InnoDB DEFAULT CHARSET=utf8 ";
	    $StaffAttend3->db_db_query($sql);
    	
	   //$sql = "SELECT DISTINCT RecordDate FROM TEMP_CARD_STAFF_LOG";
		//$temp = $this->returnArray($sql);
		$RecordDate = date("Y-m-d",$ts);
		//$ts = strtotime($RecordDate);
		$year = date('Y',$ts);
		$month = date('m',$ts);
		$day = date('j',$ts);
		//$weekday = date('w', $ts);
		
		$DailyLogTable = $StaffAttend3->createTable_Card_Staff_Attendance2_Daily_Log($year, $month);
		$RawLogTable = $StaffAttend3->buildStaffRawLogMonthTable($year, $month);
		
		//$sql = 'Select UserID from TEMP_CARD_STAFF_LOG group by UserID';
		//$TempUserIDs = $this->returnVector($sql);
		$TempUserIDs = array();
		$Result = array();
		
		echo "Patching data on $RecordDate<br>";
		
		// get current exists RAW log
		$sql = "select 
							UserID,
							RecordTime as RecordedTime,
							RecordStation as SiteName
						from 
							".$RawLogTable." 
						where 
							DayNumber = '".$day."' 
						";
		$CurRawLog = $StaffAttend3->returnArray($sql);
		$sql = 'insert into TEMP_CARD_STAFF_LOG (
							UserID,
							RecordDate,
							RecordedTime,
							SiteName
						)
						Values ';
		for ($i=0; $i< sizeof($CurRawLog); $i++) {
			$TempUserIDs[] = $CurRawLog[$i]['UserID'];
			$sql .= "('".$CurRawLog[$i]['UserID']."',
								'".$RecordDate."',
								'".$RecordDate." ".$CurRawLog[$i]['RecordedTime']."',
								'".$StaffAttend3->Get_Safe_Sql_Query($CurRawLog[$i]['SiteName'])."'),";
		}
		$sql = substr($sql,0,-1);
		$Result['UnionRawLogAndImportTemp'] = $StaffAttend3->db_db_query($sql);
		
		$TempUserIDs = array_values(array_unique($TempUserIDs));
		
		// remove exists RAW log
		$sql = "delete from ".$RawLogTable." 
						where 
							DayNumber = '".$day."' 
							and 
							UserID in (".implode(',',$TempUserIDs).")";
		$Result['RemoveCurRawLog'] = $StaffAttend3->db_db_query($sql);
		
		// cache old late/absent/ early Profile record
		$sql = "select 
							pro.StaffID,
							dailylog.SlotName,
							pro.RecordType,
							pro.ReasonID,
							pro.Waived  
						From 
							CARD_STAFF_ATTENDANCE2_PROFILE as pro 
							LEFT JOIN 
							".$DailyLogTable." as dailylog 
							on 
								pro.RecordID = dailylog.InAttendanceRecordID 
								or 
								pro.RecordID = dailylog.OutAttendanceRecordID 
						where 
							pro.RecordDate = '".$RecordDate."' 
							and 
							pro.StaffID  in (".implode(',',$TempUserIDs).") 
							and 
							pro.RecordType not in (".CARD_STATUS_NOSETTING.",".CARD_STATUS_HOLIDAY.",".CARD_STATUS_OUTGOING.") 
							and 
							dailylog.SlotName IS NOT NULL
							";
		$CachedProfileRecord = $StaffAttend3->returnArray($sql);
							
		// reset Daily Log Record
		$sql = 'update '.$DailyLogTable.' set 
							InSchoolStatus = NULL,
							OutSchoolStatus = NULL,
							InTime = NULL,
							OutTime = NULL,
							MinLate = NULL,
							MinEarlyLeave = NULL,
							InAttendanceRecordID = NULL,
							OutAttendanceRecordID = NULL,
							InWaived = NULL,
							OutWaived = NULL 
						where 
							InSchoolStatus not in ('.CARD_STATUS_NOSETTING.','.CARD_STATUS_HOLIDAY.','.CARD_STATUS_OUTGOING.') 
							and 
							DayNumber = \''.$day.'\' 
							and 
							StaffID in ('.implode(',',$TempUserIDs).')';
		$Result['ResetDailyLogStatus'] = $StaffAttend3->db_db_query($sql);
		
		// remove Late/ Absent/ Early Leave Profile
		$sql = "delete from CARD_STAFF_ATTENDANCE2_PROFILE 
						where 
							RecordDate = '".$RecordDate."' 
							and 
							RecordType not in (".CARD_STATUS_NOSETTING.",".CARD_STATUS_HOLIDAY.",".CARD_STATUS_OUTGOING.") 
							and 
							StaffID in (".implode(',',$TempUserIDs).")";
		$Result['DeleteOldProfile'] = $StaffAttend3->db_db_query($sql);					
		
		// remove OT records 
		$sql = "delete from CARD_STAFF_ATTENDANCE2_OT_RECORD 
						where 
							StaffID in (".implode(',',$TempUserIDs).") 
							and 
							RecordDate = '".$RecordDate."' ";
		$Result['DeleteOTRecord'] = $StaffAttend3->db_db_query($sql);
		
		// get Records unioned from CSV import file/ Original Raw LOG 
		$sql = "SELECT 
					UserID,
					RecordedTime,
					SiteName 
				FROM
					TEMP_CARD_STAFF_LOG 
				ORDER BY
					UserID, RecordedTime ASC";
		$import_records = $StaffAttend3->returnArray($sql, 3);
		// simulate tap card record again
		for($i=0;$i<sizeof($import_records);$i++)
		{
			list($StaffID, $RecordedTime, $SiteName) = $import_records[$i];
			$StaffAttend3->Record_Tap_Card($StaffID,strtotime($RecordedTime),$SiteName,true);
		}
		
		for ($i=0; $i< sizeof($CachedProfileRecord); $i++) {
			List($StaffID,$SlotName,$RecordType,$ReasonID,$Waived)  = $CachedProfileRecord[$i];
			if ($RecordType == CARD_STATUS_LATE || $RecordType == CARD_STATUS_ABSENT) {
				$WaiveFieldName = "InWaived";
				$StatusField = "InSchoolStatus";
				$ProfileIDFieldName = "InAttendanceRecordID";
			}
			else { // early leave
				$WaiveFieldName = "OutWaived";
				$StatusField = "OutSchoolStatus";
				$ProfileIDFieldName = "OutAttendanceRecordID";
			}
			$sql = "select 
								RecordID as CardLogID, 
								".$ProfileIDFieldName." as ProfileID 
							from 
								".$DailyLogTable." 
							where 
								DayNumber = '".$day."' 
								and 
								SlotName = '".$StaffAttend3->Get_Safe_Sql_Query($SlotName)."' 
								and 
								StaffID = '".$StaffID."' 
								and 
								".$StatusField." = '".$RecordType."'";
			$ProfileFound = $StaffAttend3->returnArray($sql);
			
			if (sizeof($ProfileFound) > 0) { // record reason if same slot found same status
				$sql = "update ".$DailyLogTable." set 
									".$WaiveFieldName." = '".$Waived."' 
								where 
									RecordID = '".$ProfileFound[0]['CardLogID']."'";
				$Result['RecoverDailyLog:StaffID-'.$StaffID.':SlotName-'.$SlotName] = $StaffAttend3->db_db_query($sql);
				
				$sql = "update CARD_STAFF_ATTENDANCE2_PROFILE set 
									ReasonID = '".$ReasonID."' ,
									Waived = '".$Waived."' 
								where 
									RecordID = '".$ProfileFound[0]['ProfileID']."'";
				$Result['RecoverProfileLog:StaffID-'.$StaffID.':SlotName-'.$SlotName] = $StaffAttend3->db_db_query($sql);
			}
		}
    
    	foreach($Result as $kk => $vv){
    		echo "$kk : ".($vv?"success":"fail")."<br>";
    	}
    	echo "<br>";
    }// end for loop
    
    # update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendV3DataPatchV81_20110811', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	// end handle daily log schema change
	//debug_r($Result);
	echo 'Applying Staff Attendance V3 v8.1 Data Patch Ended<br>';
}
?>