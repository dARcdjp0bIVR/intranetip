<?
/*
 *  Purpose: preset code for Student Registry (Kentville) [case #Y140316]
 *
 *  2019-06-24 Cameron
 *      - add additional family language (Canonese)
 *  
 *  2019-04-03 Cameron
 *      - create this file
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

//intranet_auth();
intranet_opendb();

$li = new libdb();
		
###################################
# Relationship
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='RELATIONSHIP'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				    (CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
    				('RELATIONSHIP', '1','Grandfather', '爺爺', 1, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '2','Grandmother', '嫲嫲', 2, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '3','Grandfather', '外公', 3, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '4','Grandmother', '外婆', 4, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '5','Uncle', '舅父', 5, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '6','Uncle', '伯父', 6, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '7','Uncle', '叔父', 7, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '8','Uncle', '姨父', 8, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '9','Uncle', '姑父', 9, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '10','Aunt', '姑母', 10, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '11','Aunt', '姨母', 11, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '12','Aunt', '嬸嬸', 12, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '13','Aunt', '伯母', 13, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '14','Aunt', '舅母', 14, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '15','Helper', '工人', 15, 1, 1, now(), 0, 1),
    				('RELATIONSHIP', '999','Others', '其他', 16, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}


###################################
# SchoolLevel
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='SCHOOLLEVEL'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
				    (CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
    				('SCHOOLLEVEL', 'PRN','Pre-Nursery', '幼兒班', 1, 1, 1, now(), 0, 1),
    				('SCHOOLLEVEL', 'KID','Kindergarten', '幼稚園', 2, 1, 1, now(), 0, 1),
    				('SCHOOLLEVEL', 'PRI','Primary School', '小學', 3, 1, 1, now(), 0, 1),
    				('SCHOOLLEVEL', 'SEC','Secondary School', '中學', 4, 1, 1, now(), 0, 1),
    				('SCHOOLLEVEL', 'UNV','University', '大學', 5, 1, 1, now(), 0, 1),
                    ('SCHOOLLEVEL', '999','Others', '其他', 6, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}

###################################
# Siblings Relationship
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='SIBLINGS'";
$result = $li->returnVector($sql);
if(!$result[0])
{
	$add_sql = "insert into PRESET_CODE_OPTION 
    				(CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
    				('SIBLINGS', 'EB','Elder Brother', '兄', 1, 1, 1, now(), 0, 1),
    				('SIBLINGS', 'YB','Younger Brother', '弟', 2, 1, 1, now(), 0, 1),
    				('SIBLINGS', 'ES','Elder Sister', '姐', 3, 1, 1, now(), 0, 1),
    				('SIBLINGS', 'YS','Younger Sister', '妹', 4, 1, 1, now(), 0, 1),
    				('SIBLINGS', 'TB','Twin Brother', '孿生兄弟', 5, 1, 1, now(), 0, 1),
    				('SIBLINGS', 'TS','Twin Sister', '孿生姐妹', 6, 1, 1, now(), 0, 1),
    				('SIBLINGS', 'BG','Boy and Girl Twins', '龍鳳胎', 7, 1, 1, now(), 0, 1)
				";
	$li->db_db_query($add_sql);
}

###################################
# Place of Birth
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='PLACEOFBIRTH'";
$result = $li->returnVector($sql);
if(!$result[0])
{
    $add_sql = "insert into PRESET_CODE_OPTION
				    (CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
    				('PLACEOFBIRTH', 'HKG','Hong Kong', '香港', 1, 1, 1, now(), 0, 1),
                    ('PLACEOFBIRTH', 'CHN','China', '中國', 2, 1, 1, now(), 0, 1),
                    ('PLACEOFBIRTH', 'MAC','Macau', '澳門', 3, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'AUS','Australia', '澳洲', 4, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'AUT','Austria', '奧地利', 5, 1, 1, now(), 0, 1),
                    ('PLACEOFBIRTH', 'BRA','Brazil', '巴西', 6, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'CAN','Canada', '加拿大', 8, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'FRA','France', '法國', 9, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'DEU','Germany', '德國', 10, 1, 1, now(), 0, 1),
                    ('PLACEOFBIRTH', 'IDN','Indonesia', '印度尼西亞', 11, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'IND','India', '印度', 12, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'JPN','Japan', '日本', 13, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'KOR','Korea', '韓國', 14, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'MYS','Malaysia', '馬來西亞', 15, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'NLD','Netherlands', '荷蘭', 16, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'NPL','Nepal', '尼泊爾', 17, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'NZL','New Zealand', '紐西蘭', 18, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'PAK','Pakistan', '巴基斯坦', 19, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'PHL','Philippines', '菲律賓', 20, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'PRT','Portugal', '葡萄牙', 21, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'SGP','Singapore', '新加坡', 22, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'ESP','Spain', '西班牙', 23, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'CHE','Switzerland', '瑞士', 24, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'THA','Thailand', '泰國', 25, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'TWN','Taiwan', '台灣', 26, 1, 1, now(), 0, 1),
                    ('PLACEOFBIRTH', 'UK','United Kingdom', '英國', 27, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'USA','United States of America', '美國', 28, 1, 1, now(), 0, 1),
    				('PLACEOFBIRTH', 'VNM','Vietnam', '越南', 29, 1, 1, now(), 0, 1),
                    ('PLACEOFBIRTH', '999','Others', '其他', 30, 1, 1, now(), 0, 1)
				";
    $li->db_db_query($add_sql);
}

###################################
# Subdistrict
###################################
$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='SUBDISTRICT'";
$result = $li->returnVector($sql);
if(!$result[0])
{
    $add_sql = "insert into PRESET_CODE_OPTION
				    (CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
				values
                    ('SUBDISTRICT', 'A01','Central', '中環', 1, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'A02','Kennedy Town', '堅尼地城', 2, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'A03','Mid-levels', '半山區', 3, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'A04','Mt. Davis', '摩星嶺', 4, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'A05','Sai Ying Pun', '西營盤', 5, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'A06','Sheung Wan', '上環', 6, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'A07','The Peak', '山頂', 7, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'B01','Causeway Bay Central', '中銅鑼灣', 8, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'B02','Happy Valley', '跑馬地', 9, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'B03','So Kon Po', '掃桿埔', 10, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'B04','Tai Hang', '大坑', 11, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'B05','Wan Chai', '灣仔', 12, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C01','Causeway Bay North', '北銅鑼灣', 13, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C02','Causeway Bay South', '南銅鑼灣', 14, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C03','Chai Wan', '柴灣', 15, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C04','Heng Fa Chuen', '杏花邨', 16, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C05','North Point', '北角', 17, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C06','Quarry Bay', '鰂魚涌', 18, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C07','Sai Wan Ho', '西灣河', 19, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C08','Shau Kei Wan', '筲箕灣', 20, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'C09','Siu Sai Wan', '小西灣', 21, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'D01','Aberdeen', '香港仔', 22, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'D02','Ap Lei Chau', '鴨脷洲', 23, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'D03','Pok Fu Lam', '薄扶林', 24, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'D04','Shek O', '石澳', 25, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'D05','Stanley', '赤柱', 26, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'D06','Wah Fu', '華富', 27, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'D07','Wong Chuk Hang', '黃竹坑', 28, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'E01','Jordan', '佐敦', 29, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'E02','Mong Kok', '旺角', 30, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'E03','Tai Kok Tsui', '大角咀', 31, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'E04','Tsim Sha Tsui', '尖沙咀', 32, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'E05','Yau Ma Tei', '油麻地', 33, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'F01','Cheung Sha Wan', '長沙灣', 34, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'F02','Lai Chi Kok', '荔枝角', 35, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'F03','Sham Shui Po', '深水埗', 36, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'F04','Shek Kip Mei', '石硤尾', 37, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'F05','So Uk', '蘇屋', 38, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'F06','Ta i Wo Ping', '大窩坪', 39, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'F07','Yau Yat Tsuen', '又一村', 40, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'G01','Ho Man Tin', '何文田', 41, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'G02','Hung Hom', '紅磡', 42, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'G03','Kowloon City', '九龍城', 43, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'G04','Kowloon Tong', '九龍塘', 44, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'G05','Ma Tau Wai', '馬頭圍', 45, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'G06','To Kwa Wan', '土瓜灣', 46, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H01','Choi Hung', '彩虹', 47, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H02','Diamond Hill', '鑽石山', 48, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H03','Lok Fu', '樂富', 49, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H04','Ngau Chi Wan', '牛池灣', 50, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H05','San Po Kong', '新蒲崗', 51, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H06','Tsz Wan Shan', '慈雲山', 52, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H07','Tung Tau', '東頭', 53, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H08','Wang Tau Hom', '橫頭磡', 54, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'H09','Wong Tai Sin', '黃大仙', 55, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J01','Jordan Valley', '佐敦谷', 56, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J02','Kowloon Bay', '九龍灣', 57, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J03','Kwun Tong', '觀塘', 58, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J04','Lam Tin', '藍田', 59, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J05','Lei Yue Mun', '鯉魚門', 60, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J06','Ngau Tau Kok', '牛頭角', 61, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J07','Ping Shek', '坪石', 62, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J08','Sau Mau Ping', '秀茂坪', 63, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J09','Shun Lee', '順利', 64, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'J10','Yau Tong', '油塘', 65, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'K01','Lantau North-East', '大嶼山東北', 66, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'K02','Ma Wan', '馬灣', 67, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'K03','Sham Tseng', '深井', 68, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'K04','Tsuen Wan', '荃灣', 69, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'L01','Castle Peak', '青山', 70, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'L02','Tai Lam Chung', '大欖涌', 71, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'L03','Tuen Mun', '屯門', 72, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M01','Au Tau', '凹頭', 73, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M02','Fairview Park', '錦綉花園', 74, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M03','Hung Shui Kiu', '洪水橋', 75, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M04','Kam Tin', '錦田', 76, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M05','Pat Heung', '八鄉', 77, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M06','Ping Shan', '屏山', 78, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M07','Tin Shui Wai', '天水圍', 79, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'M08','Yuen Long', '元朗', 80, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'N01','Fanling', '粉嶺', 81, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'N02','Kwu Tung', '古洞', 82, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'N03','Lo Wu', '羅湖', 83, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'N04','Sha Tau Kok', '沙頭角', 84, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'N05','Sheung Shui', '上水', 85, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'N06','Ta Kwu Ling', '打鼓嶺', 86, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'P01','Tai Po', '大埔', 87, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'Q01','Clear Water Bay', '清水灣', 88, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'Q02','Hang Hau', '坑口', 89, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'Q03','Lohas Park', '日出康城', 90, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'Q04','Po Lam', '寶琳', 91, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'Q05','Sai Kung', '西貢', 92, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'Q06','Tiu Keng Leng', '調景嶺', 93, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'Q07','Tseung Kwan O', '將軍澳', 94, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'R01','Fo Tan', '火炭', 95, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'R02','Ma Liu Shui', '馬料水', 96, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'R03','Ma On Shan', '馬鞍山', 97, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'R04','Sha Tin', '沙田', 98, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'R05','Tai Wai', '大圍', 99, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'R06','Wu Kai Sha', '烏溪沙', 100, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'S01','Kwai Chung', '葵涌', 101, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'S02','Tsing Yi', '青衣', 102, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'T01','Cheung Chau', '長洲', 103, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'T02','Lamma Island', '南丫島', 104, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'T03','Lantau Island', '大嶼山', 105, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'T04','Peng Chau', '坪洲', 106, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', 'T05','Po Toi Island', '蒲台島', 107, 1, 1, now(), 0, 1),
                    ('SUBDISTRICT', '999','Others', '其他', 108, 1, 1, now(), 0, 1)
				";
    $li->db_db_query($add_sql);
}


###################################
# Update Family Lang Chinese Name and Nationality display order, Nationality English Name
###################################
$sql = "UPDATE PRESET_CODE_OPTION SET NameChi='普通話' WHERE CodeType='FAMILY_LANG' AND Code='CHI'";
$li->db_db_query($sql);

$sql = "UPDATE PRESET_CODE_OPTION SET DisplayOrder='999' WHERE CodeType='NATIONALITY' AND Code='999'";
$li->db_db_query($sql);

$sql = "UPDATE PRESET_CODE_OPTION SET DisplayOrder='1' WHERE CodeType='NATIONALITY' AND Code='CHI'";
$li->db_db_query($sql);

$sql = "UPDATE PRESET_CODE_OPTION SET DisplayOrder='12' WHERE CodeType='NATIONALITY' AND Code='FRA'";
$li->db_db_query($sql);

$sql = "UPDATE PRESET_CODE_OPTION SET DisplayOrder='13' WHERE CodeType='NATIONALITY' AND Code='GER'";
$li->db_db_query($sql);

$sql = "UPDATE PRESET_CODE_OPTION SET NameEng='Netherlands' WHERE CodeType='NATIONALITY' AND Code='NLD'";
$li->db_db_query($sql);

$sql = "UPDATE PRESET_CODE_OPTION SET NameEng='Philippines' WHERE CodeType='NATIONALITY' AND Code='PHL'";
$li->db_db_query($sql);

$sql = "UPDATE PRESET_CODE_OPTION SET NameEng='United States of America' WHERE CodeType='NATIONALITY' AND Code='USA'";
$li->db_db_query($sql);

###################################
# Add Extra Nationality
###################################

$sql = "select count(*) from PRESET_CODE_OPTION  where CodeType='NATIONALITY' AND Code='IDN'";
$result = $li->returnVector($sql);
if(!$result[0])
{
    $add_sql = "insert into PRESET_CODE_OPTION (CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
                values ('NATIONALITY', 'IDN','Indonesia', '印尼', 14, 1, 1, now(), 0, 1)";
    $li->db_db_query($add_sql);
}

###################################
# Add Extra Family Lang at the beginning
###################################
// shift DisplayOrder first
$sql = "UPDATE PRESET_CODE_OPTION SET DisplayOrder=DisplayOrder+1 WHERE CodeType='FAMILY_LANG'";
$li->db_db_query($sql);

$sql = "INSERT IGNORE INTO PRESET_CODE_OPTION
    (CodeType, Code, NameEng, NameChi, DisplayOrder, RecordType, RecordStatus, DateInput, InputBy, InputMethod)
VALUES
    ('FAMILY_LANG', 'CAN','Cantonese', '廣東話', 1, 1, 1, now(), 0, 1)";
$li->db_db_query($sql);

//intranet_closedb();		
?>