<?
// kenenth chugn
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StudentAttendanceClassSettingMigration'] != 1 && $plugin['attendancestudent']) {
	echo 'Start on StudentAttendanceClassSettingMigration<br>';
	
	$ResultStudentAttend = array();
	### Student Attendance Start ###
	$lgs->Start_Trans();
	// get previous academic years classIDs
	$PreviousYearID = Get_Previous_Academic_Year_ID();
	$AcademicYearID = Get_Current_Academic_Year_ID();
	$sql = "select 
						ClassTitleEN,
						YearClassID 
					from 
						YEAR_CLASS 
					where 
						AcademicYearID = '".$PreviousYearID."'
				 ";
	$PrevClassList = $lgs->returnArray($sql);
	
	$sql = "select 
						ClassTitleEN,
						YearClassID 
					from 
						YEAR_CLASS 
					where 
						AcademicYearID = '".$AcademicYearID."'
				 ";
	$CurClassList = $lgs->returnArray($sql);
	$CurClassList = build_assoc_array($CurClassList);
	
	$sql = "show tables like 'CARD_STUDENT_DAILY_CLASS_CONFIRM_%'";
	$ClassConfirmTable = $lgs->returnVector($sql);
	for ($i=0; $i< sizeof($PrevClassList); $i++) {
		list($ClassTitle,$ClassID) = $PrevClassList[$i];
		
		if ($CurClassList[$ClassTitle] != "") {
			$sql = "update CARD_STUDENT_TIME_SESSION_REGULAR set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_TIME_SESSION_REGULAR:".$ClassTitle] = $lgs->db_db_query($sql);
			$sql = "update CARD_STUDENT_TIME_SESSION_DATE set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_TIME_SESSION_DATE:".$ClassTitle] = $lgs->db_db_query($sql);
			$sql = "update CARD_STUDENT_SPECIFIC_DATE_TIME set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_SPECIFIC_DATE_TIME:".$ClassTitle] = $lgs->db_db_query($sql);
			$sql = "update CARD_STUDENT_CLASS_SPECIFIC_MODE set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_CLASS_SPECIFIC_MODE:".$ClassTitle] = $lgs->db_db_query($sql);
			$sql = "update CARD_STUDENT_CLASS_PERIOD_TIME set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_CLASS_PERIOD_TIME:".$ClassTitle] = $lgs->db_db_query($sql);
			$sql = "update CARD_STUDENT_CLASS_SPECIFIC_MODE set 
								ClassID = '".$CurClassList[$ClassTitle]."' 
							where 
								ClassID = '".$ClassID."'";
			$ResultStudentAttend["Update:CARD_STUDENT_CLASS_SPECIFIC_MODE:".$ClassTitle] = $lgs->db_db_query($sql);
			for ($j=0; $j< sizeof($ClassConfirmTable); $j++) {
				$sql = "update ".$ClassConfirmTable[$j]." set 
									ClassID = '".$CurClassList[$ClassTitle]."' 
								where 
									ClassID = '".$ClassID."'";
				$ResultStudentAttend["Update:".$ClassConfirmTable[$j].":".$ClassTitle] = $lgs->db_db_query($sql);
			}
		}
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StudentAttendanceClassSettingMigration', 1, now())";
	$ResultStudentAttend['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	if (in_array(false,$ResultStudentAttend)) {
		$lgs->RollBack_Trans();
	}
	else {
		$lgs->Commit_Trans();
	}
	### End of Student Attendance ###
	
	echo 'end of StudentAttendanceClassSettingMigration';
}
?>