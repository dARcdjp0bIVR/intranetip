<?php
// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);
$LogOutput = '';

if ($GSary['iPf_ClassName_YearClassID_Mapping'] != 1 && $plugin['iPortfolio']) {
//if (true) {
	$LogOutput .= "iPortfolio - Academic Result ClassName YearClassID Mapping Data Patch [Start] ".date("Y-m-d H:i:s")."<br><br>\r\n\r\n";
	
	# backup tables first
	$ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK = $eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK_FOR_CLASSID";
	$ASSESSMENT_STUDENT_SUBJECT_RECORD = $eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD";
	$sql = "Create Table $ASSESSMENT_STUDENT_SUBJECT_RECORD_BAK Select * From $ASSESSMENT_STUDENT_SUBJECT_RECORD";
	$SuccessArr['CreateBackUp_SubjectRecord'] = $li->db_db_query($sql);
	
	$ASSESSMENT_STUDENT_MAIN_RECORD_BAK = $eclass_db.".ASSESSMENT_STUDENT_MAIN_RECORD_BAK_FOR_CLASSID";
	$ASSESSMENT_STUDENT_MAIN_RECORD = $eclass_db.".ASSESSMENT_STUDENT_MAIN_RECORD";
	$sql = "Create Table $ASSESSMENT_STUDENT_MAIN_RECORD_BAK Select * From $ASSESSMENT_STUDENT_MAIN_RECORD";
	$SuccessArr['CreateBackUp_MainRecord'] = $li->db_db_query($sql);
	
	
	# get ClassName and ClassID Mapping
	$sql = "Select
					yc.YearClassID,
					yc.ClassTitleEN as ClassNameEn,
					yc.ClassTitleB5 as ClassNameCh,
					ay.AcademicYearID
			From
					YEAR_CLASS as yc
					Inner Join
					YEAR as y On (yc.YearID = y.YearID)
					Inner Join
					ACADEMIC_YEAR as ay On (yc.AcademicYearID = ay.AcademicYearID)
			Order By
					y.Sequence, yc.Sequence
			";
	$ClassInfoArr = $li->returnArray($sql);
	$numOfClass = count($ClassInfoArr);
	
	// $ClassInfoAssoArr[$AcademicYearID][$ClassNameEn / $ClassNameCh] = $YearClassID
	$ClassInfoAssoArr = array();
	for ($i=0; $i<$numOfClass; $i++)
	{
		$thisYearClassID = $ClassInfoArr[$i]['YearClassID'];
		$thisClassNameEn = $ClassInfoArr[$i]['ClassNameEn'];
		$thisClassNameCh = $ClassInfoArr[$i]['ClassNameCh'];
		$thisAcademicYearID = $ClassInfoArr[$i]['AcademicYearID'];
		
		$ClassInfoAssoArr[$thisAcademicYearID][$thisClassNameEn] = $thisYearClassID;
		$ClassInfoAssoArr[$thisAcademicYearID][$thisClassNameCh] = $thisYearClassID;
	}
	
	$LogOutput .= "Class Mapping Info: <br>\r\n";
	foreach ((array)$ClassInfoAssoArr as $thisAcademicYearID => $thisYearClassArr) {
		foreach ((array)$thisYearClassArr as $thisClassName => $thisYearClassID) {
			$LogOutput .= "$thisAcademicYearID ### $thisClassName = $thisYearClassID <br>\r\n";
		}
	}
	$LogOutput .= "<br><br>\r\n\r\n";
	
	
	### Map ClassID into the data
	$TotalNumOfRecord = 0;
	$SuccessArr['Mapping']['SuccessCount'] = 0;
	$SuccessArr['Mapping']['FailedCount'] = 0;
	$SuccessArr['UpdateDB']['SuccessCount'] = 0;
	$SuccessArr['UpdateDB']['FailedCount'] = 0;
	$InvalidClassInfoArr = array();
		
	# Subject Data
	$ASSESSMENT_STUDENT_SUBJECT_RECORD = $eclass_db.".ASSESSMENT_STUDENT_SUBJECT_RECORD";
	$sql = "Select
					RecordID, AcademicYearID, ClassName
			From
					$ASSESSMENT_STUDENT_SUBJECT_RECORD
			Where
					YearClassID Is Null
					Or YearClassID = ''
			";
	$StudentSubjectRecordArr = $li->returnArray($sql);
	$numOfRecord = count($StudentSubjectRecordArr);
	$TotalNumOfRecord += $numOfRecord;
	
	$LogOutput .= "Student Subject Record<br>\r\n";
	$LogOutput .= "RecordID\tAcademicYearID\tClassName\tYearClassID\tMappingStatus\tUpdateStatus<br>\r\n";
	for ($i=0; $i<$numOfRecord; $i++)
	{
		$thisRecordID = $StudentSubjectRecordArr[$i]['RecordID'];
		$thisAcademicYearID = $StudentSubjectRecordArr[$i]['AcademicYearID'];
		$thisClassName = $StudentSubjectRecordArr[$i]['ClassName'];
		
		$thisYearClassID = $ClassInfoAssoArr[$thisAcademicYearID][$thisClassName];
		if ($thisYearClassID == '') {
			$thisMappingStatus = '<font color="red"><b>FAILED</b></font>';
			$thisUpdateStatus = '<font color="red"><b>FAILED</b></font>';
			$SuccessArr['Mapping']['FailedCount']++;
			$SuccessArr['UpdateDB']['FailedCount']++;
			
			$InvalidClassInfoArr[] = "$thisAcademicYearID ### $thisClassName";
		}
		else {
			$thisMappingStatus = '<font color="green">Success</font>';
			$SuccessArr['Mapping']['SuccessCount']++;
			
			$sql = "Update $ASSESSMENT_STUDENT_SUBJECT_RECORD Set YearClassID = '$thisYearClassID' Where RecordID = '$thisRecordID'";
			$thisSuccess = $li->db_db_query($sql);
			
			if ($thisSuccess) {
				$thisUpdateStatus = '<font color="green">Success</font>';
				$SuccessArr['UpdateDB']['SuccessCount']++;
			}
			else {
				$thisUpdateStatus = '<font color="red"><b>FAILED</b></font>';
				$SuccessArr['UpdateDB']['FailedCount']++;
			}
		}
		$LogOutput .= "$thisRecordID\t$thisAcademicYearID\t$thisClassName\t$thisYearClassID\t$thisMappingStatus\t$thisUpdateStatus<br>\r\n";
	}
	$LogOutput .= "<br><br>\r\n\r\n";
	
	
	# Main Data
	$ASSESSMENT_STUDENT_MAIN_RECORD = $eclass_db.".ASSESSMENT_STUDENT_MAIN_RECORD";
	$sql = "Select
					RecordID, AcademicYearID, ClassName
			From
					$ASSESSMENT_STUDENT_MAIN_RECORD
			Where
					YearClassID Is Null
					Or YearClassID = ''
			";
	$StudentSubjectRecordArr = $li->returnArray($sql);
	$numOfRecord = count($StudentSubjectRecordArr);
	$TotalNumOfRecord += $numOfRecord;
	
	$LogOutput .= "Student Main Record<br>\r\n";
	$LogOutput .= "RecordID\tAcademicYearID\tClassName\tYearClassID\tMappingStatus\tUpdateStatus<br>\r\n";
	for ($i=0; $i<$numOfRecord; $i++)
	{
		$thisRecordID = $StudentSubjectRecordArr[$i]['RecordID'];
		$thisAcademicYearID = $StudentSubjectRecordArr[$i]['AcademicYearID'];
		$thisClassName = $StudentSubjectRecordArr[$i]['ClassName'];
		
		$thisYearClassID = $ClassInfoAssoArr[$thisAcademicYearID][$thisClassName];
		if ($thisYearClassID == '') {
			$thisMappingStatus = '<font color="red"><b>FAILED</b></font>';
			$thisUpdateStatus = '<font color="red"><b>FAILED</b></font>';
			$SuccessArr['Mapping']['FailedCount']++;
			$SuccessArr['UpdateDB']['FailedCount']++;
			
			$InvalidClassInfoArr[] = "$thisAcademicYearID ### $thisClassName";
		}
		else {
			$thisMappingStatus = '<font color="green">Success</font>';
			$SuccessArr['Mapping']['SuccessCount']++;
			
			$sql = "Update $ASSESSMENT_STUDENT_MAIN_RECORD Set YearClassID = '$thisYearClassID' Where RecordID = '$thisRecordID'";
			$thisSuccess = $li->db_db_query($sql);
			
			if ($thisSuccess) {
				$NumOfSuccessRecord++;
				$thisUpdateStatus = '<font color="green">Success</font>';
				$SuccessArr['UpdateDB']['SuccessCount']++;
			}
			else {
				$thisUpdateStatus = '<font color="red"><b>FAILED</b></font>';
				$SuccessArr['UpdateDB']['FailedCount']++;
			}
		}
		$LogOutput .= "$thisRecordID\t$thisAcademicYearID\t$thisClassName\t$thisYearClassID\t$thisMappingStatus\t$thisUpdateStatus<br>\r\n";
	}
	$LogOutput .= "<br><br>\r\n\r\n";
	
	
	$LogOutput .= "Total Number of Records = $TotalNumOfRecord <br>\r\n";
	$LogOutput .= "Number of Success Mapping Records = <font color='green'>".$SuccessArr['Mapping']['SuccessCount']."</font> <br>\r\n";
	$LogOutput .= "Number of Failed Mapping Records = <font color='red'><b>".$SuccessArr['Mapping']['FailedCount']."</b></font> <br>\r\n";
	$LogOutput .= "Number of Success DB Update Records = <font color='green'>".$SuccessArr['UpdateDB']['SuccessCount']."</font> <br>\r\n";
	$LogOutput .= "Number of Failed DB Update Records = <font color='red'><b>".$SuccessArr['UpdateDB']['FailedCount']."</b></font> <br>\r\n";
	$LogOutput .= "<br><br>\r\n\r\n";
	
	
	$InvalidClassInfoArr = array_values(array_unique($InvalidClassInfoArr));
	$numOfInvalidClass = count($InvalidClassInfoArr);
	if ($numOfInvalidClass > 0) {
		$LogOutput .= "The following Class cannot be mapped: <br>\r\n";
		
		for ($i=0; $i<$numOfInvalidClass; $i++) {
			$LogOutput .= $InvalidClassInfoArr[$i]."<br>\r\n";
		}
		
		$LogOutput .= "<br>\r\n";
	}
	$LogOutput .= "<br><br>\r\n\r\n";
	
	
	$PatchResult = ($SuccessArr['UpdateDB']['SuccessCount'] == $TotalNumOfRecord)? "<font color='green'>Pass</font>" : "<font color='red'><b>FAILED</b></font>";
	$LogOutput .= "Patch Result = $PatchResult <br>\r\n";
	$LogOutput .= "<br><br>\r\n\r\n";
	
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'iPf_ClassName_YearClassID_Mapping', 1, now())";
	$SuccessArr['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	$LogOutput .= "iPortfolio - ClassName YearClassID Mapping Data Patch [End] ".date("Y-m-d H:i:s")."<br>";
	
	# show log
	echo $LogOutput;
	
	# save log
	$log_folder = $PATH_WRT_ROOT."file/iportfolio/log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/classname_yearclassid_mapping_log_". date("YmdHis") .".log";
	$lf = new libfilesystem();
	$lf->file_write(strip_tags($LogOutput), $log_file);
}

?>