<?
// Editing by 
@SET_TIME_LIMIT(60*60*10);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

intranet_opendb();

$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StudentAttendanceDailyRemarkAddDayType'] != 1 && $plugin['attendancestudent']) {
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	
	$li = new libdb();
	$lc = new libcardstudentattend2();
	
	$fix = 1;
	
	$Settings = $lgs->Get_General_Setting('StudentAttendance');
	$attendance_mode = $Settings['AttendanceMode']; // 0 - AM only, 1 - PM only , 2 - whole day with lunch, 3 - whole day without lunch
	
	echo "########## Student Attendance - Start patching CARD_STUDENT_DAILY_REMARK Records ... ###################<br>\n";
	
	$sql_schema_update = "ALTER TABLE CARD_STUDENT_DAILY_REMARK ADD COLUMN DayType int(2) AFTER RecordDate";
	$li->db_db_query($sql_schema_update);
	
	$sql_schema_update = "ALTER TABLE CARD_STUDENT_DAILY_REMARK ADD INDEX DayType (DayType)";
	$li->db_db_query($sql_schema_update);
	
	$sql_schema_update = "ALTER TABLE CARD_STUDENT_DAILY_REMARK DROP KEY StudentDate";
	$li->db_db_query($sql_schema_update);
	
	$success_msg = '<span style="color:green">success</span>';
	$fail_msg = '<span style="color:red">failed</span>';
	
	$success = array();
	if($attendance_mode == '0') {
		$sql = "UPDATE CARD_STUDENT_DAILY_REMARK SET DayType='".PROFILE_DAY_TYPE_AM."'";
		if($fix) {
			$success['UpdateAllDayTypeToAM'] = $li->db_db_query($sql);
		}
		echo "Update CARD_STUDENT_DAILY_REMARK.DayType to AM ".($success['UpdateAllDayTypeToAM']?$success_msg:$fail_msg)."<br>\n";
	} else if($attendance_mode == '1') {
		$sql = "UPDATE CARD_STUDENT_DAILY_REMARK SET DayType='".PROFILE_DAY_TYPE_PM."'";
		if($fix) {
			$success['UpdateAllDayTypeToPM'] = $li->db_db_query($sql);
		}
		echo "Update CARD_STUDENT_DAILY_REMARK.DayType to PM ".($success['UpdateAllDayTypeToPM']?$success_msg:$fail_msg)."<br>\n";
	}else {
		$sql = "SELECT * FROM CARD_STUDENT_DAILY_REMARK ORDER BY RecordDate,StudentID";
		$remark_records = $li->returnResultSet($sql);
		$record_count = count($remark_records);
		
		for($i=0;$i<$record_count;$i++) {
			$record_id = $remark_records[$i]['RecordID'];
			
			$sql = "UPDATE CARD_STUDENT_DAILY_REMARK SET DayType='".PROFILE_DAY_TYPE_AM."' WHERE RecordID='".$record_id."'";
			if($fix) {
				$success['UpdateRecord_'.$record_id.'_ToAM'] = $li->db_db_query($sql);
			}
			echo "Update RecordID $record_id CARD_STUDENT_DAILY_REMARK.DayType to AM ".($success['UpdateRecord_'.$record_id.'_ToAM']?$success_msg:$fail_msg)."<br>\n";
			$sql = "INSERT INTO CARD_STUDENT_DAILY_REMARK (StudentID,RecordDate,DayType,Remark) 
						SELECT StudentID,RecordDate,'".PROFILE_DAY_TYPE_PM."' as DayType,Remark FROM CARD_STUDENT_DAILY_REMARK WHERE RecordID='".$record_id."'";
			if($fix) {
				$success['CopyAMRecord_'.$record_id.'_ToPM'] = $li->db_db_query($sql);
			}
			echo "Copy RecordID $record_id CARD_STUDENT_DAILY_REMARK as PM Record ".($success['CopyAMRecord_'.$record_id.'_ToPM']?$success_msg:$fail_msg)."<br>\n";
		}
	}
	
	//echo "<PRE>";print_r($success)."</PRE><br>\n";
	
	$sql_schema_update = "ALTER TABLE CARD_STUDENT_DAILY_REMARK ADD UNIQUE KEY StudentDate (StudentID,RecordDate,DayType)";
	$li->db_db_query($sql_schema_update);
	
	if($fix) {
		# update General Settings - markd the script is executed
		$sql = "insert ignore into GENERAL_SETTING 
							(Module, SettingName, SettingValue, DateInput) 
						values 
							('$ModuleName', 'StudentAttendanceDailyRemarkAddDayType', 1, now())";
		$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
		// end handle daily log schema change
	}
	echo "########## Student Attendance - End patching CARD_STUDENT_DAILY_REMARK Records. ###################<br><br>\n";
}
?>