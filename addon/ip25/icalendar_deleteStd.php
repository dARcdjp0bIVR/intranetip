<?php
$POS_PATH_WRT_ROOT = "../../";
include_once($POS_PATH_WRT_ROOT."includes/global.php");
include_once($POS_PATH_WRT_ROOT."includes/libdb.php");
intranet_opendb();
$li = new libdb();
echo "remove student shared calendar started ...<br>";
// $li->Start_Trans_For_Lock_Table_Procedure() ;
// $li->Start_Trans();

$sql = "select UserID from INTRANET_USER where recordType = 2";
$allStudnet = $li->returnVector($sql);
$std_sql = implode(',',$allStudnet);

$sql = "select c.CalID from CALENDAR_CALENDAR as c where c.Owner in ($std_sql) 
and (c.shareToAll = 1 or EXISTS (
	select v.CalID, count(distinct v.userID) as cnt 
	from CALENDAR_CALENDAR_VIEWER as v where v.UserID in ($std_sql) and c.CalID = v.CalID
	group by v.CalID having cnt > 1
) or c.CalID in (
	select v2.CalID from CALENDAR_CALENDAR_VIEWER as v2 where v2.UserID in ($std_sql)
	and v2.GroupPath is not null
) )";

$allCal = $li->returnVector($sql);
$cal_sql = implode(',',$allCal);
$result=array();
$sql = "delete from CALENDAR_CALENDAR where CalID in ($cal_sql)";
$result['delete_cal']=$li->db_db_query($sql);
$sql = "delete from CALENDAR_CALENDAR_VIEWER where CalID in ($cal_sql)";
$result['delete_cal_view']=$li->db_db_query($sql);
$sql = "delete from CALENDAR_EVENT_PERSONAL_NOTE where EventID in (
	select EventID from CALENDAR_EVENT_ENTRY where CalID in ($cal_sql)
	)";
$result['delete_personal_note']=$li->db_db_query($sql);
$sql = "delete from CALENDAR_REMINDER where EventID in (
	select EventID from CALENDAR_EVENT_ENTRY where CalID in ($cal_sql)
	)";
$result['delete_reminder']=$li->db_db_query($sql);
$sql = "delete from CALENDAR_EVENT_USER where EventID in (
	select EventID from CALENDAR_EVENT_ENTRY where CalID in ($cal_sql)
	)";
$result['delete_event_user']=$li->db_db_query($sql);
$sql = "delete from CALENDAR_EVENT_ENTRY_REPEAT where RepeatID in (
	select RepeatID from CALENDAR_EVENT_ENTRY where CalID in ($cal_sql)
	)";
$result['delete_event_repeat']=$li->db_db_query($sql);
$sql = "delete from CALENDAR_EVENT_ENTRY where CalID in ($cal_sql)";
$result['delete_event']=$li->db_db_query($sql);

// debug_r($result);
// $li->RollBack_Trans();
echo "pocess ended<br>";
intranet_closedb();
?>