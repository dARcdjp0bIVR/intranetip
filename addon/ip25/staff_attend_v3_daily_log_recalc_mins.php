<?php
// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);
if ($GSary['StaffAttendV3DailyLogRecalcMins'] != 1 && $module_version['StaffAttendance'] == 3.0 && $plugin['attendancestaff']) {
	echo 'Staff Attendance V3 - Start recalculate late mins and earlyleave mins in CARD_STAFF_ATTENDANCE2_DAILY_LOG_YYYY_MM <br>';
	
	$sql = 'show tables like \'CARD_STAFF_ATTENDANCE2_DAILY_LOG_%\'';
	$DailyLogTables = $li->returnVector($sql);
	$Result=array();
	for ($i=0; $i< sizeof($DailyLogTables); $i++) {	
		$sql = "SELECT 
					RecordID, StaffID, DayNumber, 
					DutyStart, DutyEnd, InSchoolStatus, OutSchoolStatus, 
					InTime, OutTime, MinLate, MinEarlyLeave 
				FROM ".$DailyLogTables[$i]." 
				WHERE 
					(InSchoolStatus = '2' AND DutyStart IS NOT NULL AND DutyStart <> '' AND InTime IS NOT NULL AND InTime <> '')
					 OR (OutSchoolStatus = '3' AND DutyEnd IS NOT NULL AND DutyEnd <> '' AND OutTime IS NOT NULL AND OutTime <> '') ";
		$temp_result = $li->returnArray($sql);
		$result_size = sizeof($temp_result);
		for($j=0;$j<$result_size;$j++){
			list($record_id,$staff_id,$day_number,$duty_start,$duty_end,$in_status,$out_status,$in_time,$out_time,$min_late, $min_earlyleave)=$temp_result[$j];
			// late
			if($in_status==2){
				if(trim($duty_start)!="" && trim($in_time)!=""){
					$new_min_late = floor((strtotime($in_time)-strtotime($duty_start))/60);
					if($new_min_late!=$min_late){
						if($new_min_late<0) $min_late_str="NULL";
						$min_late_str = "'$new_min_late'";
						$sql = "UPDATE ".$DailyLogTables[$i]." SET MinLate=$min_late_str WHERE RecordID = '$record_id' and StaffID = '$staff_id' ";
						$Result['Update_'.$record_id] = $li->db_db_query($sql);
						echo $DailyLogTables[$i]."_".$day_number."_RecordID[$record_id]_UserID[$staff_id]_MinLate_Recalculated[InTime $in_time DutyStart $duty_start From $min_late (mins) To $new_min_late (mins)]<br />";
					}
				}
			}
			// early leave
			if($out_status==3){
				if(trim($duty_end)!="" && trim($out_time)!=""){
					$new_min_earlyleave = floor((strtotime($duty_end)-strtotime($out_time))/60);
					if($new_min_earlyleave!=$min_earlyleave){
						if($new_min_earlyleave<0) $min_earlyleave_str="NULL";
						$min_earlyleave_str = "'$new_min_earlyleave'";
						$sql = "UPDATE ".$DailyLogTables[$i]." SET MinEarlyLeave=$min_earlyleave_str WHERE RecordID = '$record_id' and StaffID = '$staff_id' ";
						$Result['Update_'.$record_id] = $li->db_db_query($sql);
						echo $DailyLogTables[$i]."_".$day_number."_RecordID[$record_id]_UserID[$staff_id]_MinEarlyLeave_Recalculated[Duty End $duty_end OutTime $out_time From $min_earlyleave (mins) To $new_min_earlyleave (mins)]<br />";
					}
				}
			}
		}
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StaffAttendV3DailyLogRecalcMins', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	echo 'Staff Attendance V3 - recalculating late mins and earlyleave mins in CARD_STAFF_ATTENDANCE2_DAILY_LOG_YYYY_MM ended<br>';
}
?>