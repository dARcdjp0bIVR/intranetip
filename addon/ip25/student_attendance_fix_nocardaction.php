<?
// Editing by 
@SET_TIME_LIMIT(60*60*10);
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($GSary['StudentAttendanceFixNoCardAction20120508'] != 1 && $plugin['attendancestudent']) {
	include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");
	
	$li = new libdb();
	$lc = new libcardstudentattend2();
	
	echo "########## Student Attendance - Start patching No Card Entrance Records ... ###################<br>\n";
	
	$sql = "SHOW TABLES LIKE 'CARD_STUDENT_DAILY_LOG_%'";
	$CARD_STUDENT_DAILY_LOG_TABLES = $li->returnVector($sql);
	
	for($i=0;$i<sizeof($CARD_STUDENT_DAILY_LOG_TABLES);$i++)
	{
		$CARD_STUDENT_DAILY_LOG_TABLE = $CARD_STUDENT_DAILY_LOG_TABLES[$i];
		$parts = explode("_",$CARD_STUDENT_DAILY_LOG_TABLE);
		$YEAR = $parts[4];
		$MONTH = $parts[5];
		
		// find wrong excess no card action records
		$sql = "SELECT 
					u.UserID,u.EnglishName,u.ClassName,u.ClassNumber,d.RecordID,d.DayNumber,
					d.AMStatus,d.PMStatus,d.InSchoolTime,d.LunchOutTime,d.LunchBackTime,d.LeaveSchoolTime,
					b.RecordID as BadActionRecordID 
				FROM $CARD_STUDENT_DAILY_LOG_TABLE as d 
				INNER JOIN INTRANET_USER as u ON u.UserID = d.UserID
				INNER JOIN CARD_STUDENT_BAD_ACTION as b ON d.UserID = b.StudentID 
					AND DATE_FORMAT(b.RecordDate,'%Y-%m-%d') = DATE_FORMAT(CONCAT('$YEAR-$MONTH-',d.DayNumber),'%Y-%m-%d') 
					AND b.RecordType = '".CARD_BADACTION_NO_CARD_ENTRANCE."' 
				WHERE u.RecordStatus = 1 AND u.RecordType = 2 
					AND (d.InSchoolTime IS NOT NULL OR d.LunchBackTime IS NOT NULL ) 
					AND (d.AMStatus = '".CARD_STATUS_PRESENT."' OR d.AMStatus = '".CARD_STATUS_LATE."' 
					OR d.PMStatus = '".CARD_STATUS_PRESENT."' OR d.PMStatus = '".CARD_STATUS_LATE."')
				ORDER BY d.DayNumber, u.ClassName, u.ClassNumber ";
		
		$data = $li->returnArray($sql);
		
		if(count($data)>0){
			$fixed_count = 0;
			for($j=0;$j<count($data);$j++){
				$BadActionRecordID = trim($data[$j]['BadActionRecordID']);
				$record_date = $YEAR."-".$MONTH."-".((int)$data[$j]['DayNumber']<10 ? '0'.$data[$j]['DayNumber']:$data[$j]['DayNumber']);
			
				if($BadActionRecordID != ''){
					// fix here
					$lc->removeBadActionNoCardEntrance($data[$j]['UserID'], $record_date);
					$fixed_count++;
				}else{
					continue;
				}
			}
			if($fixed_count > 0){
				echo $CARD_STUDENT_DAILY_LOG_TABLE." removed $fixed_count wrong CARD_STUDENT_BAD_ACTION records <br>\n";
			}
		}
		
		###################################################
		// find missing no card action records
		$sql = "SELECT 
					u.UserID,u.EnglishName,u.ClassName,u.ClassNumber,d.RecordID,d.DayNumber,
					d.AMStatus,d.PMStatus,d.InSchoolTime,d.LunchOutTime,d.LunchBackTime,d.LeaveSchoolTime,
					b.RecordID as BadActionRecordID 
				FROM $CARD_STUDENT_DAILY_LOG_TABLE as d 
				INNER JOIN INTRANET_USER as u ON u.UserID = d.UserID
				LEFT JOIN CARD_STUDENT_BAD_ACTION as b ON d.UserID = b.StudentID 
					AND DATE_FORMAT(b.RecordDate,'%Y-%m-%d') = DATE_FORMAT(CONCAT('$YEAR-$MONTH-',d.DayNumber),'%Y-%m-%d') 
					AND b.RecordType = '".CARD_BADACTION_NO_CARD_ENTRANCE."' 
				WHERE u.RecordStatus = 1 AND u.RecordType = 2 
				 AND (d.InSchoolTime IS NULL OR d.InSchoolTime = '') 
				 AND (d.LunchBackTime IS NULL OR d.LunchBackTime = '') 
				 AND (d.AMStatus = '".CARD_STATUS_PRESENT."' OR d.AMStatus = '".CARD_STATUS_LATE."' 
					OR d.PMStatus = '".CARD_STATUS_PRESENT."' OR d.PMStatus = '".CARD_STATUS_LATE."') 
				ORDER BY d.DayNumber, u.ClassName, u.ClassNumber ";
		
		$data = $li->returnArray($sql);
		
		if(count($data)>0){
			$fixed_count = 0;
			for($j=0;$j<count($data);$j++){
				$BadActionRecordID = trim($data[$j]['BadActionRecordID']);
				$record_date = $YEAR."-".$MONTH."-".((int)$data[$j]['DayNumber']<10 ? '0'.$data[$j]['DayNumber']:$data[$j]['DayNumber']);
				if($BadActionRecordID == ''){
					// apply fix here
					$lc->addBadActionNoCardEntrance($data[$j]['UserID'], $record_date);
					$fixed_count++;
				}else{
					continue;
				}
			}
			if($fixed_count > 0){
				echo $CARD_STUDENT_DAILY_LOG_TABLE." add $fixed_count missing CARD_STUDENT_BAD_ACTION records <br>\n";
			}
		}
	}
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'StudentAttendanceFixNoCardAction20120508', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	// end handle daily log schema change
	
	echo "########## Student Attendance - End patching No Card Entrance Records. ###################<br><br>\n";
}