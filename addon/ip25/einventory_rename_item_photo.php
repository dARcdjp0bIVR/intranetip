<?php
// editing by 
/**************************************
 * Created on 2011-03-30 by Carlos 
 **************************************/
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();
$li->db = $intranet_db;
$lgs = new libgeneralsettings();
$lf = new libfilesystem();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($plugin['Inventory'] && $GSary['eInventoryRenameItemPhoto']!=1)
{
	echo "############################ eInventory - Start renaming item photo(s). ##########################<br />";
	
	$sql = "SELECT 
				p.PartID, p.ItemID, p.PhotoPath, p.PhotoName, i.ItemCode, i.NameChi, i.NameEng  
			FROM INVENTORY_ITEM as i 
			INNER JOIN INVENTORY_PHOTO_PART as p ON i.PhotoLink = p.PartID 
			WHERE i.RecordStatus = 1 AND p.PhotoPath <> '' AND p.PhotoPath IS NOT NULL AND p.PhotoName <> '' AND p.PhotoName IS NOT NULL 
			ORDER BY i.ItemCode ";
	
	$records = $li->returnArray($sql);
	
	$numOfRecords = sizeof($records);
	
	$numOfRenameSuccess = 0;
	$numOfRenameFail = 0;
	$numOfNotRequireRename = 0;
	
	for($i=0;$i<$numOfRecords;$i++)
	{
		list($part_id, $item_id, $photo_path, $photo_name, $item_code, $name_chi, $name_eng) = $records[$i];
	    $oldFullFilePath = $intranet_root.'/'.$photo_path.'/'.$photo_name;
	    $file_ext = $lf->file_ext($oldFullFilePath);
	    $new_photo_name = $item_code.$file_ext;
	    $newFullFilePath = $intranet_root.'/'.$photo_path.'/'.$new_photo_name;
	    $item_display = "Item ".$name_eng." (ItemCode: ".$item_code."):";
	    if(!file_exists($oldFullFilePath)){
	    	$numOfRenameFail+=1;
	    	echo "<font color=\"red\">".$item_display." Photo \"".$oldFullFilePath."\" does NOT exist.</font><br />";
	    	continue;
	    }
	    
	    if(strcasecmp($oldFullFilePath,$newFullFilePath)!=0){
	    	$rename_success = $lf->file_rename($oldFullFilePath,$newFullFilePath);
	    	if($rename_success){
	    		$sql = "UPDATE INVENTORY_PHOTO_PART SET PhotoName = '".$li->Get_Safe_Sql_Query($new_photo_name)."' WHERE PartID = '$part_id' ";
	    		$update_db_success = $li->db_db_query($sql);
	    		if($update_db_success){
	    			$numOfRenameSuccess+=1;
	    			echo $item_display." renamed from \"$oldFullFilePath\" to \"$newFullFilePath\" successfully.<br />";
	    		}else{// should rarely occur
	    			$lf->file_rename($newFullFilePath,$oldFullFilePath);
	    			$numOfRenameFail+=1;
	    			echo "<font color=\"red\">".$item_display." rename success but failed to sync db name as \"$newFullFilePath\".</font><br />";
	    		}
	    	}else{
	    		$numOfRenameFail+=1;
	    		echo "<font color=\"red\">".$item_display." failed to rename as \"$newFullFilePath\".</font><br />";
	    	}
	    }else{
	    	$numOfNotRequireRename+=1;
	    	echo $item_display." photo name \"$oldFullFilePath\" does not required to rename.<br />";
	    }
	}
	
	echo "<br />";
	echo "Item photo rename summary:<br />";
	echo "Total number of records: ".$numOfRecords."<br />";
	echo "Number of records renamed successfully: ".$numOfRenameSuccess."<br />";
	echo "Number of records failed to rename:".$numOfRenameFail."<br />";
	echo "Number of records not required to rename:".$numOfNotRequireRename."<br />";
	echo "<br />";
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', 'eInventoryRenameItemPhoto', 1, now())";
	$Result['UpdateGeneralSettings'] = $li->db_db_query($sql);
	
	echo "######################## eInventory - Rename item photo(s) ended.#########################<br />";
}

?>