<?php

@SET_TIME_LIMIT(316000);


$Compare = ($Compare!="") ? $Compare : "eclass";


# for EJ only
// EJ 5.0 $data_file_path = "../../../junior20/intranet20";

$data_file_http = "http://192.168.0.146:31002/file/deploy_dev_arr.txt";


include_once("../includes/global.php");
include_once("../includes/libfilesystem.php");


function compareFiles($IntranetSrcFT, $IntranetSrcFH, $filestimearrIntranet, $fileshasharrIntranet, $DeploymentDay="", $Output="HTML")
{
	if (!is_array($IntranetSrcFT) || sizeof($IntranetSrcFT)<=0)
	{
		die("[IntranetSrcFT] No file from development site! Please run the script and copy the data file to testing site.");
	}
	
	if (!is_array($IntranetSrcFH) || sizeof($IntranetSrcFH)<=0)
	{
		die("[IntranetSrcFH] No file from development site! Please run the script and copy the data file to testing site.");
	}
	
	if (!is_array($filestimearrIntranet) || sizeof($filestimearrIntranet)<=0)
	{
		die("[filestimearrIntranet] No file from testing site! Please check if the correct file path is given.");
	}
	
	if (!is_array($fileshasharrIntranet) || sizeof($fileshasharrIntranet)<=0)
	{
		die("[fileshasharrIntranet] No file from testing site! Please check if the correct file path is given.");
	}
	
	$ResultMissing = array();
	$ResultDifferent = array();
	
	# if no give, 90 days by default
	$DeploymentDayTS = ($DeploymentDay!="") ? strtotime($DeploymentDay) : strtotime("Today") - 60*60*24*90;
	foreach ($IntranetSrcFT as $FilePath => $FileTime)
	{
		if (strstr($FilePath, ".php.") || strstr($FilePath, "/bak/") || strstr($FilePath, "settings.php") || strstr($FilePath, "debug")
			 || strstr($FilePath, "libcyo") || strstr($FilePath, "/cyo/") || strstr($FilePath, "/addon/script/") || strstr($FilePath, "sql_tool_test/")
			 || strstr($FilePath, "check_lang_var/") || strstr($FilePath, "/index_old_mechanism.php") || strstr($FilePath, "/sql_tool/") || strstr($FilePath, "powerboard5/clipart")
			 || strstr($FilePath, "src/econtent/minigame/") || strstr($FilePath, "/w2/") || strstr($FilePath, ".png")  || strstr($FilePath, ".gif") 
			 || strstr($FilePath, "/eAdmin/StudentMgmt/medical/") || strstr($FilePath, "/tcpdf/") || strstr($FilePath, "/temp/"))
		{
			continue;
		}
		if ($DeploymentDayTS=="" || $FileTime>=$DeploymentDayTS)
		{
			if ($FileTime==$filestimearrIntranet[$FilePath])
			{
				# should be the same
			} else
			{
				# check hash value
				if ($IntranetSrcFH[$FilePath]!=$fileshasharrIntranet[$FilePath])
				{
					# different!
					if (trim($fileshasharrIntranet[$FilePath])=="")
					{
						$ResultMissing[] = array($FilePath);
					} else
					{
						$ResultDifferent[] = array($FilePath, $FileTime, $filestimearrIntranet[$FilePath]);
					}
				}
			}
		}
	}
	
	$rxHtml = "<table cellspacing='1' bgcolor='#555555' cellpadding='8'>";
	if (sizeof($ResultMissing)>0)
	{
		$rxHtml .= "<tr><td colspan='4' bgcolor='orange'><H1>Some files are missing in testing site!</H1></td></tr>";
		# display missing files
		
		for ($i=0; $i<sizeof($ResultMissing); $i++)
		{
			$row_file_path = str_replace("../", "", $ResultMissing[$i][0]);
			$row_file_time_dev = date("Y-m-d h:i:s",  $ResultMissing[$i][1]);
			$row_file_time_test = date("Y-m-d h:i:s",  $ResultMissing[$i][2]);
			$bgcolor = ($i%2==1) ? "#EEFFEE" : "#FFFFFF";
			$rxHtml .= "<tr><td nowrap='nowrap' bgcolor='{$bgcolor}' colspan='4'>{$row_file_path}</td></tr>\n";
			$rxCSV .= "\"".$row_file_path."\",\"missing\"\n";
		}
	}

	if (sizeof($ResultDifferent)>0)
	{
		$rxHtml .= "<tr><td colspan='4' bgcolor='yellow'><H1>Some files are different!</H1></td></tr>";
		
		$rxCSV .= "\n\n\n";
		
		
		# display missing files
		for ($i=0; $i<sizeof($ResultDifferent); $i++)
		{
			$row_file_path = str_replace("../", "", $ResultDifferent[$i][0]);
			$row_file_time_dev = date("Y-m-d h:i:s",  $ResultDifferent[$i][1]);
			$row_file_time_test = date("Y-m-d h:i:s",  $ResultDifferent[$i][2]);
			
			$diffValue = ($ResultDifferent[$i][1] - $ResultDifferent[$i][2])/(60*60);
			if (abs($diffValue)>24)
			{
				# to day unit
				$diffValue = $diffValue / 24;
				$row_different = round($diffValue, 0) . " day(s)";
			} else
			{
				$row_different = round($diffValue, 2) . " hour(s)";
			}
			
			$bgcolor = ($i%2==1) ? "#EEFFEE" : "#FFFFFF";
			if ($diffValue>=0)
			{
				$date_color_dev = "red";
				$date_color_test = "black";	
			} else
			{
				$date_color_dev = "black";
				$date_color_test = "red";				
			}
			
			$rxHtml .= "<tr><td nowrap='nowrap' bgcolor='{$bgcolor}' >{$row_file_path}</td><td nowrap='nowrap' bgcolor='{$bgcolor}' >{$row_different}</td><td nowrap='nowrap' bgcolor='{$bgcolor}'><font color='{$date_color_dev}'>{$row_file_time_dev}</font></td><td nowrap='nowrap' bgcolor='{$bgcolor}' ><font color='{$date_color_test}'>{$row_file_time_test}</font></td></tr>\n";
			
			$rxCSV .= "\"".$row_file_path."\",\"different\",\"".$row_different."\",\"".$row_file_time_dev."\",\"".$row_file_time_test."\"\n";
		}
	}
	$rxHtml .= "</table>";
	
	if ($Output=="HTML" || $Output=="")
	{
		echo $rxHtml;
	} elseif ($Output=="CSV")
	{
		echo $rxCSV;
	}
}



if ($Output!="CSV")
{
	echo "<h2><b>NOTE: This script only check those source codes (PHP, js ..etc) recently modified in 60 days.</b></h2><br />\n";
}


	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	# authentication
	curl_setopt($ch, CURLOPT_URL, $data_file_http);
	
	$data_from_checking = curl_exec($ch);	
	
// EJ 5.0 list($IntranetSrcFT, $IntranetSrcFH) = unserialize(get_file_content("{$data_file_path}/file/deploy_dev_arr.txt"));
	list($IntranetSrcFT, $IntranetSrcFH) = unserialize($data_from_checking);
		
		
		//debug_r($IntranetSrcFH);die();
		
	$fs = new libfilesystem();
	$fs->set_get_filetime(true);
	$fs->set_get_filehash(true);
	
if ($Compare=="intranet") 
{
	
	# Intranet
	$filesarr = $fs->return_folderlist("../addon");
	$filesarr = $fs->return_folderlist("../includes");
	$filesarr = $fs->return_folderlist("../admin");
	$filesarr = $fs->return_folderlist("../api");
	$filesarr = $fs->return_folderlist("../cardapi");
	$filesarr = $fs->return_folderlist("../home");
	$filesarr = $fs->return_folderlist("../lang");
	$filesarr = $fs->return_folderlist("../plugins");
	$filesarr = $fs->return_folderlist("../rifolder");
	$filesarr = $fs->return_folderlist("../templates");
	$filesarr = $fs->return_folderlist("../tool");
	$filesarr = $fs->return_folderlist("../webserviceapi");
	$fs->set_subfolder_recursive(true);
	$filesarr = $fs->return_folderlist("..");
} elseif ($Compare=="eclass")
{

	# eClass
	$fs->set_subfolder_recursive(false);
	$filesarr = $fs->return_folderlist("../../eclass40/addon", true);
	$filesarr = $fs->return_folderlist("../../eclass40/src");
	$filesarr = $fs->return_folderlist("../../eclass40/system");
	$filesarr = $fs->return_folderlist("../../eclass40/css");
	$filesarr = $fs->return_folderlist("../../eclass40/js");	
	$fs->set_subfolder_recursive(true);
	$filesarr = $fs->return_folderlist("../../eclass40");
	
}
	
	$filestimearrIntranet = $fs->return_filetime();
	$fileshasharrIntranet = $fs->return_filehash();
	
	compareFiles($IntranetSrcFT, $IntranetSrcFH, $filestimearrIntranet, $fileshasharrIntranet, "", $Output);
	


?>