<?

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once("liberd.php");
include_once("liberd_ui.php");

intranet_opendb();

$linterface 	= new interface_html("imail_archive.html");
$lerd= new liberd();
$lerd_ui= new liberd_ui();

$MODULE_OBJ['title'] = "ERD";
$linterface->LAYOUT_START();  

echo $lerd_ui->Include_JS_CSS(); 
echo $lerd_ui->Get_Database_Table_Selection_Table();

?>

<script src="<?=$PATH_WRT_ROOT?>/templates/jquery/ui.core.js"></script>
<script src="<?=$PATH_WRT_ROOT?>/templates/jquery/ui.draggable.js"></script>
<script>
function printDB(bySql){
	js_Select_All('selectedTable');
//	document.form1.print_tmp_erd.value = bySql?1:0;
	document.form1.target="_blank";
	document.form1.submit();
	document.form1.target="";
//	var win = window.open()
//	win.document.write("<html><?=$display?></html>")
//	win.document.close();
}

var LastKeyUpVal = '';
function js_Search_Table(){
	jsVal = $("#table_filter").val().Trim();
	
	if(LastKeyUpVal == '' || LastKeyUpVal!=jsVal)
	{
		Block_Element("DBTableSelectDiv");
		$.post(
			"ajax_task.php",
			{
				Action:"ReloadDBTable",
				Keyword:jsVal
			},
			function(ReturnData){
				$("div#DBTableSelectDiv").html(ReturnData);	
				js_Select_All('DBTable[]');
				UnBlock_Element("DBTableSelectDiv");
			}
		)
		LastKeyUpVal = jsVal;
	}
	
}

var DisplayArr = new Object();
function js_Add_Table()
{
	if(!$("#DBTable\\[\\]").val())
		return false;
		
	$("#DBTable\\[\\]").find("option:selected").each(function(){
		//$("#selectedTable").append($(this).clone());
		if(!DisplayArr[$(this).val()])
		{
			$("#selectedTable").append($(this).clone());
			DisplayArr[$(this).val()] = true;
		}
	});
	js_Select_All('selectedTable');
	
//	$("#selectedTable").html("");
//	for(x in DisplayArr)
//	{
//		
//	}
	
//	var SelectedArr = new Array();
//	for(x in DisplayArr)
//	{
//		if(DisplayArr[x])
//			SelectedArr.push(x);
//	}
//	
//	$.post(
//		"ajax_task.php",
//		{
//			Action:"LoadERDTable",
//			"DisplayArr":SelectedArr.toString()
//		},
//		function(ReturnArr){
//			$("div#erd").html(ReturnArr);
//			js_Init_Hover();
//			js_Init_Table_Name_Select();
//			js_Init_Table_Draggable();
//		}
//		
//	)
}

function js_Delete_Table(TableName)
{
	TableName = TableName.replace(".","\\.")
	$("#Table"+TableName).remove();
	DisplayArr[TableName] = false;
}

function js_Remove_Selected_Table()
{
	$("#selectedTable").find("option:selected").each(function(){
		$(this).remove();
		DisplayArr[$(this).val()] = false;
	});
}

var timer;
function js_Filter_Key_Up(jsVal)
{
	clearTimeout(timer);
   	timer=setTimeout("js_Search_Table()",500);
}

function js_Init_Hover()
{
	$("td.dbfield").hover(function(){
		$("td.dbfield:contains('"+$(this).html()+"')").css("background-color","#ffff00");
	}).mouseout(function(){
      $("td.dbfield").css("background-color","#ffffff");
    });
}

function js_Init_Table_Name_Select()
{
    $("table.DBTableBox>thead>td>b").dblclick(function(){
    	
    	$(this).siblings().css("width",$(this).width()) 
    	$(this).hide().siblings().show().select().blur(
    		function()
    		{ 
    			$(this).hide().siblings().show();
    		}
    	)
    });
}

function js_Init_Table_Draggable()
{
	$("table.DBTableBox").draggable();
}

function js_Change_Type()
{
	var type = $("input.gen_type:checked").val();
	if(type == "erd")
	{
		$(".show_create_table_tr").hide();
		document.form1.action="print_erd.php";
	}
	else
	{
		$(".show_create_table_tr").show();
		document.form1.action="print_sql.php";
	}
}

$().ready(function(){
	js_Search_Table();
	js_Change_Type();
})
</script>
<style>
.DBTableBox{
	border:1px solid #000; 
	float:left; 
	margin:5px;
	font-size: 10px;
}
</style>
<?
$linterface->LAYOUT_STOP();

intranet_closedb();
?>
