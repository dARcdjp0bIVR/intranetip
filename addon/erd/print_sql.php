<?

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once("liberd.php");
include_once("liberd_ui.php");

intranet_opendb();

$lerd= new liberd();
$lerd_ui= new liberd_ui();

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_header.php");   

echo $lerd_ui->Include_JS_CSS(); 
echo $lerd_ui->Gen_Show_Create_Table_Sql($SelectedTable, $charset, $charset_other_input, $engine, $engine_other_input, $template);

include_once($PATH_WRT_ROOT."templates/{$LAYOUT_SKIN}/layout/print_footer.php");   

intranet_closedb();