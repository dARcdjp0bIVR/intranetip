<?
class liberd_ui extends interface_html {

	function liberd_ui(){
		parent::interface_html();
	}
	
	function Get_Database_Table_Selection_Table()
	{
		global $lerd, $Lang, $intranet_db;
		
		$SelectAllDBTableBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('DBTable[]');");
		$AddBtn = $this->GET_SMALL_BTN($Lang["Btn"]["Add"], "button", "js_Add_Table()");
		
		$SelectAllSelectedTableBtn = $this->GET_SMALL_BTN($Lang["Btn"]["SelectAll"], "button", "js_Select_All('selectedTable');");
		$RemoveSelectedTableBtn = $this->GET_SMALL_BTN($Lang["Btn"]["Remove"], "button", "js_Remove_Selected_Table();");
		
		$GenerateBtn =  $this->GET_ACTION_BTN($Lang["Btn"]["Generate"], "button", "printDB();");
		
		$x .= '<form name="form1" id="form1" method="POST">';
		$x .= '<table id="html_body_frame" width="100%" border="0" cellspacing="0" cellpadding="5">'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					
					$x .= '<div id="erd"></div>'."\n";

					$x.= '<table class="form_table_select_student">'."\n";
						$x.= '<tbody>'."\n";
							$x.= '<tr>'."\n";
								$x.= '<td>DB Table:</td>'."\n";
								$x.= '<td>Selected Table :</td>'."\n";
							$x.= '</tr>'."\n";
							$x.= '<tr>'."\n";
								$x.= '<th>'."\n";
									$x .= 'table filter'."\n";
									$x .= '<input type="text" autocomplete=off id="table_filter" onkeyup="js_Filter_Key_Up();" size="40" value="'.$intranet_db.'">';
									$x .= '<div id="DBTableSelectDiv"></div>';
									$x .= $AddBtn."&nbsp;";
									$x .= $SelectAllDBTableBtn;
								$x.= '</th>'."\n";
								$x.= '<td>'."\n";
									$x .= '<select class="select_studentlist" id="selectedTable" name="SelectedTable[]" multiple size=20>'."\n";
									$x .= '</select>';
									$x.= '<br>'."\n";
									$x.= '<br>'."\n";
									$x.= $RemoveSelectedTableBtn."\n";
									$x.= $SelectAllSelectedTableBtn."\n";
								$x.= '</td>'."\n";
							$x.= '</tr>'."\n";
						$x.= '</tbody>'."\n";
					$x.= '</table>'."\n";

					$x.= '<table width="80%" style="margin-left:auto; margin-right:auto;">'."\n";
						$x.= '<tbody>'."\n";
							$x.= '<col width="25%">'."\n";
							$x.= '<col>'."\n";
							$x.= '<tr>'."\n";
								$x.= '<td>Type</td>'."\n";
								$x.= '<td>'."\n";
									$x.= '<input id="gen_type_erd" type="radio" name="gen_type" class="gen_type" value="erd" checked onclick="js_Change_Type()">'."\n";
									$x.= '<label for="gen_type_erd">erd</label>'."\n";
									$x.= '<input id="gen_type_sql" type="radio" name="gen_type" class="gen_type" value="sql" onclick="js_Change_Type()">'."\n";
									$x.= '<label for="gen_type_sql">show create table sql</label>'."\n";
								$x.= '</td>'."\n";
							$x.= '</tr>'."\n";
							
							$x.= '<tr class="show_create_table_tr">'."\n";
								$x.= '<td>Charset</td>'."\n";
								$x.= '<td>'."\n";
									$x.= '<input id="charset_latin" type="radio" name="charset" class="charset" value="latin" checked>'."\n";
									$x.= '<label for="charset_latin">latin1</label>'."\n";
									$x.= '<input id="charset_utf8" type="radio" name="charset" class="charset" value="utf8">'."\n";
									$x.= '<label for="charset_utf8">utf8</label>'."\n";
									$x.= '<input id="charset_other" type="radio" name="charset" class="charset" value="other">'."\n";
									$x.= '<label for="charset_other">other</label><input id="charset_other_input" type="text" name="charset_other_input" onfocus="document.getElementById(\'charset_other\').checked=\'checked\'">'."\n";
								$x.= '</td>'."\n";
							$x.= '</tr>'."\n";
							$x.= '<tr class="show_create_table_tr">'."\n";
								$x.= '<td>Engine</td>'."\n";
								$x.= '<td>'."\n";
									$x.= '<input id="engine_INNODB" type="radio" name="engine" class="engine" value="INNODB" checked>'."\n";
									$x.= '<label for="engine_INNODB">INNODB</label>'."\n";
									$x.= '<input id="engine_MYISAM" type="radio" name="engine" class="engine" value="MYISAM">'."\n";
									$x.= '<label for="engine_MYISAM">MYISAM</label>'."\n";
									$x.= '<input id="engine_other" type="radio" name="engine" class="engine" value="other">'."\n";
									$x.= '<label for="engine_other">other</label><input id="engine_other_input" type="text" name="engine_other_input" onfocus="document.getElementById(\'engine_other\').checked=\'checked\'">'."\n";
								$x.= '</td>'."\n";
							$x.= '</tr>'."\n";
							$x.= '<tr valign="top" class="show_create_table_tr">'."\n";
								$x.= '<td>template (For migration)</td>'."\n";
								$x.= '<td>'."\n";
									$x.= '<input id="use_template" type="checkbox" name="use_template" value="other" onclick="$(\'#template\').attr(\'disabled\', !this.checked)">'."\n";
									$x.= '<br>'."\n";
									$x.= '<textarea id="template" name="template" rows=10 cols=150 disabled>'."";
									$x.= '
$sql_ip_update[] = array(
	"{{{Date}}}",
	"Module Name - Create Table {{{TableName}}}",
	"{{{TableSql}}}"
);
									'."";
									$x.= '</textarea><br>'."\n";
									$x.= '{{{Date}}} - '.date("Y-m-d").', {{{TableName}}} - Name of the table, {{{TableSql}}} - Show create table sql '."\n";
								$x.= '</td>'."\n";
							$x.= '</tr>'."\n";
						$x.= '</tbody>'."\n";
					$x.= '</table>'."\n";					

					$x .= '<div class="edit_bottom_v30">';
						$x .= $GenerateBtn;
					$x .= '</div>';
				$x .= '</td>';		
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '</form>'."\n";		
		
		return $x;
	}
	
	function Get_Print_ERD_UI($SelectedTable)
	{
		$display = $this->Gen_Simple_ERD_Table($SelectedTable);
		
		$x .= '<table width=100% cellspacing=0 cellpadding=0>'."\n";
			$x .= '<tr>'."\n";
				$x .= '<td>'."\n";
					$x .= '<table id="erd" class="erd" style="" >'."\n";
						$x .= '<tr>'."\n";
							$x .= '<td>'."\n";
								$x .= $display."\n";
								$x .= '<div style="clear:both;"></div>'."\n";
							$x .= '</td>'."\n";
						$x .= '</tr>'."\n";
					$x .= '</table>'."\n";
				$x .= '</td>'."\n";
			$x .= '</tr>'."\n";
		$x .= '</table>'."\n";
		$x .= '<div id="PreviewLayer" style="display:none; position:absolute; right:0; bottom:0;"></div>';
		$x .= $this->Get_Print_ERD_Font_Size_Panel();
//		$x .= '<div id="err" class="print_hide" style=\'background-color:#000; color:#f00; z-index:10\'>'.$sql_err.'</div>'."\n";

		return $x;
		
	}
	
	function Get_Print_ERD_Font_Size_Panel()
	{
		global $Lang;
		
		$x .= '<div id="fontsizepanel" class="print_hide">'."\n";
			$x .= '<div id="setting_option">';
				$x .= 'font size [<a href="javascript:chsize(1)">+</a>][<a href="javascript:chsize(-1)">-</a>]'."\n";
				$x .= '<br>';
				$checked= $_COOKIE['CookieDBNameSpan']?"checked":"";
				$x .= '<input id="SetDBNameSpan" type="checkbox" '.$checked.' value=1 onclick="js_Toggle_Display(\'DBNameSpan\', this.checked);"><label for="SetDBNameSpan">show db name </label>'."\n";
				$x .= '<br>';
				$checked= $_COOKIE['CookieDBType']?"checked":"";
				$x .= '<input id="SetDBType" type="checkbox" '.$checked.' value=1 onclick="js_Toggle_Display(\'DBType\', this.checked);"><label for="SetDBType">show type</label>'."\n";
				$x .= '<br>';
				$checked= $_COOKIE['CookieDBNULL']?"checked":"";
				$x .= '<input id="SetDBNULL" type="checkbox" '.$checked.' value=1 onclick="js_Toggle_Display(\'DBNULL\', this.checked);"><label for="SetDBNULL">show not null </label>'."\n";
				$x .= '<br>';
				$checked= $_COOKIE['CookieKeyTable']?"checked":"";
				$x .= '<input id="SetKeyTable" type="checkbox" '.$checked.' value=1 onclick="js_Toggle_Display(\'KeyTable\', this.checked);"><label for="SetKeyTable">show table key </label>'."\n";
				$x .= '<br>';
				$checked= $_COOKIE['CookieShowPreview']?"checked":"";
				$x .= '<input id="SetShowPreview" type="checkbox" '.$checked.' value=1 onclick="js_Toggle_Display(\'ShowPreview\', this.checked);"><label for="SetShowPreview">show preview </label>'."\n";
			$x .= '</div>';
//			$x .= '<br>';
//			$x .= $this->Get_Small_Btn($Lang['Btn']['Print'],'button','initPosition(); window.print();');
			$x .= '<div class="showhidebtn">';
				$x .= '<div onclick="js_Toggle_Option();">option</div>';
			$x .= '</div>';
		$x .= '</div>'."\n";
		
		return $x;
	}
	
	function Get_Database_Selection()
	{
		global $lerd;	
		
		$DB = $lerd->Get_Database();
		
		$optionArr = BuildMultiKeyAssoc($DB, "Database","Database",1);
		
		$select = getSelectByAssoArray($optionArr, " id='database' name='database[]' multiple size=20 ", '', '', 1, '');
		
		return $select;
	}
	
	function Get_Database_Table_Selection($Keyword='')
	{
		global $lerd;	
		
		$optionArr = $lerd->Get_Database_Table_List($Keyword);
		
		$select = getSelectByAssoArray($optionArr, " id='DBTable[]' name='DBTable[]' multiple size=20 ", '', '', 1, '');
		
		return $select;
		
	}
	
	function Gen_ERD_Table($DBTable)
	{
		global $lerd;
			
		foreach($DBTable as $gentables)
		{
			$tablesinfo= $lerd->Get_Table_Info($gentables);
			$display .= "<table border=0 cellspacing=0 cellpadding=2 class='DBTableBox' id='Table$gentables'>";
				$display .= "<thead>";
					$display .= "<td style='border-bottom:1px solid #000' >";
						$display .= "<b>$gentables</b>";
						$display .= "<a href='javascript:void(0);' onclick='js_Delete_Table(\"$gentables\")'>[x]</a>";
						$display .= "<input value='$gentables' class='DBTableName' style='display:none; width:100%'>";
					$display .= "</td>";
				$display .= "</thead>";
				foreach ($tablesinfo as $field)
				{
					$display .= "<tr><td class='dbfield'>".$field[0]."</td></tr>";
					$allfield[] = $field[0];
				}
			$display .= "</table>";
		}
		
		return $display;
	}
	
	function Gen_Simple_ERD_Table($DBTable)
	{
		global $lerd;
		
		foreach($DBTable as $gentables)
		{	
			$tablesinfo= $lerd->Get_Table_Info($gentables);
			$Index =  $lerd->Get_Table_Index($gentables);
			
			list($DB,$TableName) = explode(".",$gentables); 
			$display .= "<table border=0 cellspacing=0 cellpadding=0 class='DBTableBox' id='$TableName'>\n";
				# table name
				$display .= "<thead><tr>\n";
					$display .= "<td style='border-bottom:1px solid #000' align='center'>\n";
						$display .= "<b><span class='DBNameSpan'>$DB.</span>$TableName</b>\n";
//						$display .= "<input value='$gentables' class='DBTableName' style='display:none; width:100%'>";
					$display .= "</td>\n";
				$display .= "</tr></thead>\n";
				
				# table field
				$display .= "<tbody><tr><td class='dbfield'>\n";
					$display .= "<table cellpadding=0 cellspacing=0 border=0 width='100%' class='FieldTable'><tbody>\n";
						foreach ($tablesinfo as $field)
						{
							$display .= "<tr id='{$TableName}_{$field['Field']}' FieldName='{$field['Field']}'  class='FieldRow'>\n";
								$display .= "<td nowrap class='FieldName'>".$field['Field']."&nbsp;</td>\n";
								$display .= "<td nowrap><span class='DBType'>".$field['Type']."&nbsp;</span></td>\n";
								$display .= "<td nowrap><span class='DBNULL'>".($field['Null']=="YES"?"":"NOT NULL")."&nbsp;</span></td>\n";
							$display .= "</tr>\n";
							$allfield[] = $field[0];
						}

					$display .= "</tbody></table>\n";
				$display .= "</td></tr></tbody>\n";
				
				# table key
				$display .= "<tfoot><tr><td class='dbfield'>";
					$display .= "<table cellpadding=0 cellspacing=0 border=0 width='100%' class='KeyTable'>\n";
						foreach ((array)$Index as $nonUnique => $KeyInfo)
						{
							foreach((array)$KeyInfo as $KeyName => $KeyColumnArr )
							{
								if($KeyName=="PRIMARY")
									$KeyType = "PRIMARY KEY";
								else if($nonUnique==0)
									$KeyType = "UNIQUE KEY";
								else
									$KeyType = "KEY";
								
								$KeyFieldList = implode(", ",$KeyColumnArr);
								$display .= "<tr>\n";
									$display .= "<td nowrap>".$KeyType."</td>\n";
									$display .= "<td nowrap title='$KeyType $KeyName ($KeyFieldList)'>".$KeyFieldList."</td>\n";
								$display .= "</tr>\n";
							}
						}

					$display .= "</table>\n";
				$display .= "</td></tr></tfoot>\n";
				
			$display .= "</table>\n";	
		}
		
		return $display;
	}
	
	function Gen_Show_Create_Table_Sql($DBTable, $charset, $charset_other_input, $engine, $engine_other_input, $template)
	{
		global $lerd;
		
		echo "<textarea id='sql' style='width:100%; height:90%;' onfocus='$(this).select();'>";
		foreach($DBTable as $thisTable)
		{
			$CreateTable = $lerd->Get_Create_Table_Sql($thisTable);
			
			$CreateTableSql = str_replace("CREATE TABLE", "CREATE TABLE IF NOT EXISTS", $CreateTable[0][1]);
			
			// take away auto increment
			$RegExp = "/AUTO_INCREMENT=\d+\s/i";
			$CreateTableSql = preg_replace($RegExp, '', $CreateTableSql);
			
			$CreateTableSql = substr($CreateTableSql, 0, stripos($CreateTableSql, "ENGINE="));
			
			if($charset=="other")
				$charset = $charset_other_input;
			
			if($engine=="other")
				$engine = $engine_other_input;
			
			$CreateTableSql .= "ENGINE=$engine DEFAULT CHARSET=$charset";
			
			if(trim($template)!='')
			{
				$Sql = trim(stripslashes($template));
				$Sql = str_replace("{{{Date}}}", date("Y-m-d"), $Sql);
				$Sql = str_replace("{{{TableName}}}", $CreateTable[0][0], $Sql);
				$Sql = str_replace("{{{TableSql}}}", $CreateTableSql, $Sql);
			}
			else
				$Sql = $CreateTableSql;
				
			echo $Sql."\n\n";	
		}
		echo "</textarea>";
		echo "<input type='button' onclick='$(\"#sql\").select()' value='select all'>";

	}	
}
?>