<?
class liberd extends libdb {

	function liberd(){
		parent::libdb();
		
		$this->DBTableLimit = 100;
	}
	
	function Get_Database()
	{
		$sql = "SHOW DATABASES";
		$Result = $this->returnArray($sql);
		
		return $Result;
	}
	
	function Get_Database_Table_List($Keyword='')
	{
		if(trim($Keyword)=='')
			$Keyword = "IP25";
		
		$DBArr = $this->Get_Database();
		$DBNameArr = Get_Array_By_Key($DBArr,"Database");
		foreach($DBNameArr as $thisDB)
		{
			$this->db = $thisDB;
			$sql = " SHOW TABLES ";
			$Result = $this->returnArray($sql);
			
			foreach($Result as $TableName)
			{
				$fullname = $thisDB.".".$TableName[0];
				$Match = true;
				if(trim($Keyword)!='')
				{
					foreach((array)preg_split("/[\s,]+/", $Keyword) as $Token)
					{
						if(!stristr($fullname,$Token))
						{
							$Match = false;
							break;
						}
					}
				}
				
				if($Match)
				{
					$arr[$thisDB][$thisDB.".".$TableName[0]] = $thisDB.".".$TableName[0];
					if(++$table_ctr >=$this->DBTableLimit) break;
				}	
				 
			}
		}
		
		return (array)$arr;
	}
	
	function Get_Table_Info($Table)
	{
		$tablesinfo= $this->returnArray("DESC $Table");
		
		return $tablesinfo;
	}
	
	function Get_Table_Index($Table)
	{
		 $Index = $this->returnArray("SHOW INDEXES IN $Table");
		 $IndexAssoc = BuildMultiKeyAssoc($Index,array("Non_unique","Key_name","Seq_in_index"),"Column_name",1);
		 
		 return $IndexAssoc;
	}
	
	function Get_Create_Table_Sql($Table)
	{
		 $Sql = $this->returnArray("SHOW CREATE TABLE $Table");
		 
		 return $Sql;
	}

	
}
?>