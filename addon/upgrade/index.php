<?php
include ("../../includes/global.php");
include ("../../includes/libdb.php");
include ("../check.php");
?>
<? /* ?>
This page contains script to upgrade eClass Intranet from 2.0 to 2.1 or later.<br>

You have to execute the followings:

<ol>
<li><a href=announcement.php>Announcement Conversion</a></li>
<li><a href=event.php>Event Conversion</a></li>
<li><a href=usertype.php>User type conversion</a></li>
<li><a href=campus_attach.php>Compute Attachment Size (also valid for 1.2 to 2.0 or later)</a></li>
<li><a href=fixclass.php>Class-group relation fixing (for Homework List)</a></li>
<li><a href=payment.php>Open payment accounts for existing teachers and students (Only for new payment module client)</a></li>
<li><a href=imail.php>Update quota table of iMail</a></li>
<li><a href=imail_draft_attach.php>Rebuild iMail draft attachment</a></li>
<li><a href=sports.php>Init Sports System</a></li>
<li><a href=staffattend.php>Init Staff Attendance System</a></li>
<li><a href=payment_reset.php>Reset All stuff in Payment System</a></li>
<li><a href=payment_dead_account_remove.php>Remove All dead payment accounts</a></li>
<li><a href=payment_addvalue_name_fix.php>Fix Add Value Transaction Name</a></li>
</ol>
<? */ ?>

Modify File .htgroup: <br>
Content:
<pre>
 intranet_email:   admin
 intranet_password:   admin
 intranet_account:   admin
 intranet_url:   admin
 intranet_campuslink:   admin
 intranet_cycle:   admin
 intranet_rbps:   admin
 intranet_language:   admin
 intranet_filesystem:   admin
 intranet_user:   admin
 intranet_group:   admin
 intranet_role:   admin
 intranet_motd:   admin
 intranet_polling:   admin
 intranet_announcement:   admin
 intranet_event:   admin
 intranet_campusmail:   admin
 intranet_timetable:   admin
 intranet_groupbulletin:   admin
 intranet_groupfiles:   admin
 intranet_grouplinks:   admin
 intranet_resource:   admin
 intranet_booking:   admin
 intranet_eclass:   admin
 intranet_homework:   admin
 intranet_campusmail_set:   admin
 intranet_tmpfiles:   admin
 intranet_resource_set:   admin
 intranet_basic_settings:   admin
 intranet_campustv:   admin
 intranet_booking_periodic:   admin
 intranet_qb:   admin
 intranet_school_settings:   admin
 intranet_wordtemplates:   admin
 intranet_attendance:   admin
 intranet_merit:   admin
 intranet_service:   admin
 intranet_activity:   admin
 intranet_award:   admin
 intranet_assessment:   admin
 intranet_usage:   admin
 intranet_teaching:   admin
 intranet_groupcategory:   admin
 intranet_clubs_enrollment:   admin
 intranet_enrollment_approval:   admin
 intranet_student_files:   admin
 intranet_notice:   admin
 intranet_quota:   admin
 intranet_sms:   admin
 intranet_adminjob:   admin
 intranet_quota_mail:   admin
 intranet_quota_file:   admin
 intranet_student_attendance:   admin
 intranet_student_promotion:   admin
 intranet_websams:   admin
 intranet_payment:   admin
 intranet_circular:   admin
 intranet_profile_set:   admin
 intranet_student_attendance2:   admin
 intranet_discipline:   admin
 intranet_sports:   admin
 intranet_album:   admin
 intranet_service_mgmt_system:   admin
 intranet_qualied_star_picking:   admin
 intranet_reportcard:   admin
 intranet_guardian:   admin
 intranet_patrol_system:   admin
 intranet_inventory:   admin
 intranet_student_registry:   admin
 intranet_elibrary:   admin
 intranet_user_info_settings:   admin
 intranet_swimming_gala:   admin
 intranet_lunchbox:   admin
 intranet_file_quota:   admin
 intranet_library:   admin
 intranet_eclass_update:   admin
 intranet_lslp:   admin
</pre>
