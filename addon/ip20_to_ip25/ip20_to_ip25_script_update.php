<?
# using: 

#############################################################################################
#############################################################################################
#############################################################################################
## Do not run the script in Dev site (Since the data in dev site is updated already)
#############################################################################################
#############################################################################################
#############################################################################################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath.'/src/includes/php/lib-filesystem.php');
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");

intranet_opendb();

$li = new libdb();

$lgs = new libgeneralsettings();
$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

### Record the setup date
# load history file
$history_file = $PATH_WRT_ROOT."file/ip20_to_ip25_script_update_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = split("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip) = split(",", $tmp_arr[sizeof($tmp_arr)-1]);
	
	if ($sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0]>$last_schema_date)
	{
		$is_new_available = true;
	}
	$new_update = ($is_new_available) ? "Yes" : "No";
} 
else
{
	$new_update = "-";
	$last_update = "-";
	$last_update_ip = "-";
}
$update_count = 0;

/* update intranetIP schema */
$after_date = ($flag==2 && $last_schema_date!="") ? $last_schema_date : "";
//updateSchema($li, $sql_eClassIP_update, $intranet_db, $after_date);

# add update info (latest schema date, time of update, IP of the client)
$time_now = date("Y-m-d H:i:s");
$client_ip = getenv("REMOTE_ADDR");
if (trim($client_ip)=="")
{
	$client_ip = $_SERVER["REMOTE_ADDR"];
}
$history_content .= ($history_content!="") ? "\n" : "";
$history_content .= "$last_schema_date,$time_now,$client_ip";

$lf = new phpduoFileSystem();
$lf->writeFile($history_content, $history_file);

$WordArr['Done'] = '<font color="blue"><b>Done</b></font>';
$WordArr['Failed'] = '<font color="red"><b>Failed</b></font>';
$WordArr['DoneBefore'] = '<font color="green"><b>Done Before</b></font>';

# DB Settings
//$thisDB = 'IP25_TEMP';	//for testing only
$thisDB = $intranet_db;

		###################################################
		### Clone ASSESSMENT_SUBJECT from eClass to IP DB
		###################################################
		if(!$GSary['Clone_ASSESSMENT_SUBJECT'])
		{
			echo "================ Clone Subject ================<br />";
			
			// Disable the auto-increment of table YEAR
			$sql = "Alter table $thisDB.ASSESSMENT_SUBJECT modify RecordID int(11)";
			$success['AutoIncrement']['Disable']['ASSESSMENT_SUBJECT'] = $li->db_db_query($sql) or die(mysql_error());
			
			$get_subject_sql = "SELECT	* FROM $eclass_db.ASSESSMENT_SUBJECT";
			$SubjectArr = $li->returnArray($get_subject_sql);
			$numOfSubject = count($SubjectArr);
			
			if ($numOfSubject > 0)
			{
				$WebSAMSCodeArr = array();
				$DuplicatedWebSAMSCodeArr = array();
				$SubjectValueArr = array();
				for ($i=0; $i<$numOfSubject; $i++)
				{
					$RecordID = $SubjectArr[$i]['RecordID'];
					$CODEID = $SubjectArr[$i]['CODEID'];
					$EN_SNAME = $SubjectArr[$i]['EN_SNAME'];
					$CH_SNAME = $SubjectArr[$i]['CH_SNAME'];
					$EN_DES = $SubjectArr[$i]['EN_DES'];
					$CH_DES = $SubjectArr[$i]['CH_DES'];
					$EN_ABBR = $SubjectArr[$i]['EN_ABBR'];
					$CH_ABBR = $SubjectArr[$i]['CH_ABBR'];
					$DisplayOrder = $SubjectArr[$i]['DisplayOrder'];
					$InputDate = $SubjectArr[$i]['InputDate'];
					$ModifiedDate = $SubjectArr[$i]['ModifiedDate'];
					$CMP_CODEID = $SubjectArr[$i]['CMP_CODEID'];
					$RecordStatus = 1;
					
					if (in_array($CODEID, $WebSAMSCodeArr, true) == true && $CMP_CODEID == '')
					{
						$DuplicatedWebSAMSCodeArr[] = $CODEID;
					}
					else
					{
						if ($CMP_CODEID == '')
							$WebSAMSCodeArr[] = $CODEID;
						
						$SubjectValueArr[] = "( '$RecordID', '".$li->Get_Safe_Sql_Query($CODEID)."', 
												'".$li->Get_Safe_Sql_Query($EN_SNAME)."', '".$li->Get_Safe_Sql_Query($CH_SNAME)."',
												'".$li->Get_Safe_Sql_Query($EN_DES)."', '".$li->Get_Safe_Sql_Query($CH_DES)."',
												'".$li->Get_Safe_Sql_Query($EN_ABBR)."', '".$li->Get_Safe_Sql_Query($CH_ABBR)."',
												'".$li->Get_Safe_Sql_Query($DisplayOrder)."', '".$li->Get_Safe_Sql_Query($InputDate)."',
												'".$li->Get_Safe_Sql_Query($ModifiedDate)."', '".$li->Get_Safe_Sql_Query($CMP_CODEID)."',
												'$RecordStatus') ";
					}
				}
				
				$numOfDuplicatedCode = count($DuplicatedWebSAMSCodeArr);
				if ($numOfDuplicatedCode > 0)
				{
					$DuplicatedCodeList = implode('<br />', $DuplicatedWebSAMSCodeArr);
					echo "Insert Subject ".$WordArr['Failed']." - The following WebSAMS Code(s) duplicated:<br />";
					echo $DuplicatedCodeList;
					
					// Enable the auto-increment of table ASSESSMENT_SUBJECT
					$sql = "Alter table $thisDB.ASSESSMENT_SUBJECT modify column RecordID int(11) auto_increment";
					$success['AutoIncrement']['Enable']['ASSESSMENT_SUBJECT'] = $li->db_db_query($sql) or die(mysql_error());
				}
				else
				{
					$SubjectValueList = implode(',', $SubjectValueArr);
				
					$insert_subject_sql = "
							Insert Into
									$thisDB.ASSESSMENT_SUBJECT
									(RecordID, CODEID, EN_SNAME, CH_SNAME, EN_DES, CH_DES, EN_ABBR, CH_ABBR, DisplayOrder, InputDate, ModifiedDate, CMP_CODEID, RecordStatus)
									Values
									$SubjectValueList
							";
					$success['Insert']['Subject'] = $li->db_db_query($insert_subject_sql) or die(mysql_error());
					
					if ($success['Insert']['Subject'])
					{
						echo "Insert Subject ".$WordArr['Done'].".<br />";
					}
					else
					{
						// Enable the auto-increment of table ASSESSMENT_SUBJECT
						$sql = "Alter table $thisDB.ASSESSMENT_SUBJECT modify column RecordID int(11) auto_increment";
						$success['AutoIncrement']['Enable']['ASSESSMENT_SUBJECT'] = $li->db_db_query($sql) or die(mysql_error());
				
						echo "Insert Subject ".$WordArr['Failed'].".<br />";
					}
				}
			}
			 
			// Enable the auto-increment of table YEAR_CLASS
			$sql = "Alter table $thisDB.ASSESSMENT_SUBJECT modify column RecordID int(11) auto_increment";
			$success['AutoIncrement']['Enable']['ASSESSMENT_SUBJECT'] = $li->db_db_query($sql) or die(mysql_error());
			echo "============== End of Clone Subject =============<br /><br />";
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'Clone_ASSESSMENT_SUBJECT', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Clone Subject ". $WordArr['DoneBefore'] ." ================<br />";
		}
		###################################################
		### End of Clone ASSESSMENT_SUBJECT from eClass to IP DB
		###################################################
		
		###################################################
		### Academic Year and Term Setup
		###################################################
		if(!$GSary['AcademicYear_Term_Setup']) 
		{
			$AcademicYearEn = stripslashes($_POST['AcademicYearEn']);
			$AcademicYearCh = stripslashes($_POST['AcademicYearCh']);
			$YearTermArr = unserialize(rawurldecode($YearTermArr));
			$numOfYearTerm = count($YearTermArr);
		
			# Insert Academic Year
			echo "================ Insert Academic Year ================<br />";
			$insert_academic_year_sql = "
					INSERT INTO 
						$thisDB.ACADEMIC_YEAR 
						(YearNameEN, YearNameB5, Sequence, DateInput, InputBy, DateModified, ModifyBy) 
					VALUES 
						('".$li->Get_Safe_Sql_Query($AcademicYearEn)."', '".$li->Get_Safe_Sql_Query($AcademicYearCh)."', 1, NOW(), NULL, NOW(), NULL)
					";
			$success['Insert']['AcademicYear'] = $li->db_db_query($insert_academic_year_sql) or die(mysql_error());
			
			if ($success['Insert']['AcademicYear'])
			{ 
				echo "Insert Academic Year ".$WordArr['Done'].".<br />";
			}
			else
			{
				echo "Insert Academic Year ".$WordArr['Failed'].".<br />";
				die();
			}
			echo "============== End of Insert Academic Year =============<br /><br />";
			
			# Get the updated academic year id
			$insertedAcademicYearID = $li->db_insert_id();
			
			# Insert Academic Year Term 
			echo "================ Insert Term ================<br />";
			for ($i=0; $i<$numOfYearTerm; $i++)
			{
				$thisYearTermEn = $YearTermArr[$i]['YearTermEn'];
				$thisYearTermCh = $YearTermArr[$i]['YearTermCh'];
				$thisStartDate = $YearTermArr[$i]['StartDate'].' 00:00:00';
				$thisEndDate = $YearTermArr[$i]['EndDate'].' 23:59:59';
				
				$insert_year_term_sql = "
						INSERT INTO 
							$thisDB.ACADEMIC_YEAR_TERM 
							(AcademicYearID,YearTermNameEN,YearTermNameB5,TermStart,TermEnd,DateInput,InputBy,DateModified,ModifyBy) 
						VALUES
							($insertedAcademicYearID,'".$li->Get_Safe_Sql_Query($thisYearTermEn)."','".$li->Get_Safe_Sql_Query($thisYearTermCh)."', '$thisStartDate', '$thisEndDate', NOW(), NULL, NOW(), NULL)
						";
				$success['Insert']['YearTerm'][$i] = $li->db_db_query($insert_year_term_sql) or die(mysql_error());
				
				if ($success['Insert']['YearTerm'][$i])
				{
					echo "Insert Term '".$thisYearTermEn."' ".$WordArr['Done'].".<br />";
				}
				else
				{
					echo "Insert Term '".$thisYearTermEn."' ".$WordArr['Failed'].".<br />";
					die();
				}
			}
			echo "============== End of Insert Term =============<br /><br />";
			 
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'AcademicYear_Term_Setup', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Insert Academic Year ". $WordArr['DoneBefore'] ." ================<br />";
			echo "================ Insert Term ". $WordArr['DoneBefore'] ." ================<br />";
			
			$insertedAcademicYearID = 1;
		}
		###################################################
		### End of Academic Year and Term Setup
		###################################################
		
		###################################################
		### Form, Class, Class Teacher and Class Students Setup
		################################################### 
		if(!$GSary['FormClassClassTeacherClassStudentSetup'])
		{
			## Form
			echo "================ Insert Form ================<br />";
			// Disable the auto-increment of table YEAR
			$sql = "Alter table $thisDB.YEAR modify column YearID int(8)";
			$success['AutoIncrement']['Disable']['YEAR'] = $li->db_db_query($sql) or die(mysql_error());
			
			// Get the Form from IP20 
			$sql = "Select * From $thisDB.INTRANET_CLASSLEVEL Where RecordStatus = 1 ORDER BY LevelName"; 
			$ClassLevelArr = $li->returnArray($sql);
			$numOfClassLevel = count($ClassLevelArr);
			
			$ClassLevelValueArr = array();
			if ($numOfClassLevel > 0)
			{
				for ($i=0; $i<$numOfClassLevel; $i++)
				{
					$thisClassLevelID = $ClassLevelArr[$i]['ClassLevelID'];
					$thisLevelName = $ClassLevelArr[$i]['LevelName'];
					$thisWebSAMSLevel = $ClassLevelArr[$i]['WebSAMSLevel'];
					$thisSequence = $i + 1;
					
					$ClassLevelValueArr[] = "( '$thisClassLevelID', '".$li->Get_Safe_Sql_Query($thisLevelName)."', '".$li->Get_Safe_Sql_Query($thisWebSAMSLevel)."', '$thisSequence', NULL, NULL, NULL, NULL )";
				}
				
				$ClassLevelValueList = implode(',', $ClassLevelValueArr);
				
				$insert_classlevel_sql = "
						Insert Into
								$thisDB.YEAR 
								(YearID, YearName, WEBSAMSCode, Sequence, DateInput, InputBy, DateModified, ModifyBy)
								Values
								$ClassLevelValueList
						";
				$success['Insert']['Form'] = $li->db_db_query($insert_classlevel_sql) or die(mysql_error());
				
				if ($success['Insert']['Form'])
				{
					echo "Insert Form ".$WordArr['Done'].".<br />";
				}
				else
				{
					// Enable the auto-increment of table YEAR
					$sql = "Alter table $thisDB.YEAR modify column YearID int(8) auto_increment";
					$success['AutoIncrement']['Enable']['YEAR'] = $li->db_db_query($sql) or die(mysql_error()); 
			
					echo "Insert Form ".$WordArr['Failed'].".<br />";
					die();
				}
			}
			else
			{
				echo "No form record at this moment<br />";
			}
			
			// Enable the auto-increment of table YEAR
			$sql = "Alter table $thisDB.YEAR modify column YearID int(8) auto_increment";
			$success['AutoIncrement']['Enable']['YEAR'] = $li->db_db_query($sql) or die(mysql_error());
			echo "============== End of Insert Form =============<br /><br />";
			
			## Class
			echo "================ Insert Class ================<br />";
			// Disable the auto-increment of table YEAR_CLASS
			$sql = "Alter table $thisDB.YEAR_CLASS modify column YearClassID int(8)";
			$success['AutoIncrement']['Disable']['YEAR_CLASS'] = $li->db_db_query($sql) or die(mysql_error());
			
			// Get the Class from IP20
			$sql = "Select * From $thisDB.INTRANET_CLASS Where RecordStatus = 1 ORDER BY ClassName";
			$ClassArr = $li->returnArray($sql);
			$numOfClass = count($ClassArr);
			
			$ClassValueArr = array();
			if ($numOfClass > 0) 
			{
				for ($i=0; $i<$numOfClass; $i++)
				{
					$thisClassID = $ClassArr[$i]['ClassID'];
					$thisClassName = $ClassArr[$i]['ClassName'];
					$thisClassLevelID = $ClassArr[$i]['ClassLevelID'];
					$thisGroupID = $ClassArr[$i]['GroupID'];
					$thisWebSAMSClassCode = $ClassArr[$i]['WebSAMSClassCode'];
					$thisSequence = $i + 1;
					
					if ($thisClassLevelID != 0 && $thisClassLevelID != '')
						$ClassValueArr[] = "( '$thisClassID', '$insertedAcademicYearID', '".$li->Get_Safe_Sql_Query($thisClassName)."', '".$li->Get_Safe_Sql_Query($thisClassName)."', '$thisClassLevelID', '$thisGroupID', '$thisWebSAMSClassCode', '$thisSequence', NULL, NULL, NULL, NULL )";
				}
				
				$numOfMigrateClass = count($ClassValueArr);
				if ($numOfMigrateClass > 0)
				{
					$ClassValueList = implode(',', $ClassValueArr);
				
					$insert_class_sql = "
							Insert Into
									$thisDB.YEAR_CLASS
									(YearClassID, AcademicYearID, ClassTitleEN, ClassTitleB5, YearID, GroupID, WEBSAMSCode, Sequence, DateInput, InputBy, DateModified, ModifyBy)
									Values
									$ClassValueList
							";
					$success['Insert']['Class'] = $li->db_db_query($insert_class_sql) or die(mysql_error());
					
					if ($success['Insert']['Class'])
					{
						echo "Insert Class ".$WordArr['Done'].".<br />"; 
					}
					else
					{
						// Enable the auto-increment of table YEAR_CLASS
						$sql = "Alter table $thisDB.YEAR_CLASS modify column YearClassID int(8) auto_increment";
						$success['AutoIncrement']['Enable']['YEAR_CLASS'] = $li->db_db_query($sql) or die(mysql_error());
				
						echo "Insert Class ".$WordArr['Failed'].".<br />";
						die();
					}
				}
				else
				{
					echo "No class record can be migrated<br />";
				}
			}
			else
			{
				echo "No class record at this moment<br />";
			}
			
			// Enable the auto-increment of table YEAR_CLASS
			$sql = "Alter table $thisDB.YEAR_CLASS modify column YearClassID int(8) auto_increment";
			$success['AutoIncrement']['Enable']['YEAR_CLASS'] = $li->db_db_query($sql) or die(mysql_error());
			echo "============== End of Insert Class =============<br /><br />";
			
			## Class Teacher
			echo "================ Insert Class Teacher ================<br />";
			// Disable the auto-increment of table YEAR_CLASS_TEACHER
			$sql = "Alter table $thisDB.YEAR_CLASS_TEACHER modify column YearClassTeacherID int(8)";
			$success['AutoIncrement']['Disable']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql) or die(mysql_error());
			
			// Delete Duplicated Record first
			$sql = "Create Table If Not Exists $thisDB.INTRANET_CLASSTEACHER_MIGRATION_BAK Select * From $thisDB.INTRANET_CLASSTEACHER";
			$li->db_db_query($sql) or die(mysql_error());
			$sql = "Select * From $thisDB.INTRANET_CLASSTEACHER Group By UserID, ClassID";
			$IP20_ClassTeacherArr = $li->returnArray($sql);
			$IP20_CLassTeacherIDArr = Get_Array_By_Key($IP20_ClassTeacherArr, 'ClassTeacherID');
			# prevent error in case $IP20_CLassTeacherIDArr is empty
			if (sizeof($IP20_CLassTeacherIDArr)>0)
			{
				$sql = "Delete From $thisDB.INTRANET_CLASSTEACHER Where ClassTeacherID Not In (".implode(',', $IP20_CLassTeacherIDArr).")";
				$success['Delete']['Duplicated_IP20_ClassTeacher_Data'] = $li->db_db_query($sql) or die(mysql_error());
			}
			
			// Get the Class Teachers from IP20
			$sql = "SELECT * FROM $thisDB.INTRANET_CLASSTEACHER";
			$ClassTeacherArr = $li->returnArray($sql);
			$numOfClassTeacher = count($ClassTeacherArr);
			
			$ClassTeacherValueArr = array();
			if ($numOfClassTeacher > 0)
			{
				for ($i=0; $i<$numOfClassTeacher; $i++)
				{
					$thisClassTeacherID = $ClassTeacherArr[$i]['ClassTeacherID'];
					$thisUserID = $ClassTeacherArr[$i]['UserID'];
					$thisClassID = $ClassTeacherArr[$i]['ClassID'];
					
					$ClassTeacherValueArr[] = "( '$thisClassTeacherID', '$thisUserID', '$thisClassID', NULL, NULL, NULL, NULL )";
				}
				
				$ClassTeacherValueList = implode(',', $ClassTeacherValueArr);
				
				$insert_class_teacher_sql = "
						Insert Into
								$thisDB.YEAR_CLASS_TEACHER
								(YearClassTeacherID, UserID, YearClassID, DateInput, InputBy, DateModified, ModifyBy)
								Values
								$ClassTeacherValueList
						";
				$success['Insert']['Class_Teacher'] = $li->db_db_query($insert_class_teacher_sql) or die(mysql_error());
				
				if ($success['Insert']['Class_Teacher'])
				{
					echo "Insert Class Teacher ".$WordArr['Done'].".<br />";
				}
				else
				{
					// Enable the auto-increment of table YEAR_CLASS_TEACHER
					$sql = "Alter table $thisDB.YEAR_CLASS_TEACHER modify column YearClassTeacherID int(8) auto_increment";
					$success['AutoIncrement']['Enable']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql) or die(mysql_error());
			
					echo "Insert Class Teacher ".$WordArr['Failed'].".<br />";
					die();
				}
			}
			else
			{
				echo "No class teacher record at this moment<br />";
			}
			
			// Enable the auto-increment of table YEAR_CLASS_TEACHER
			$sql = "Alter table $thisDB.YEAR_CLASS_TEACHER modify column YearClassTeacherID int(8) auto_increment";
			$success['AutoIncrement']['Enable']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql) or die(mysql_error());
			echo "============== End of Insert Class Teacher =============<br /><br />";
			
			## Class Student
			echo "================ Insert Class Student ================<br />";
			// Get all classes in IP25
			$sql = "Select * From $thisDB.YEAR_CLASS ORDER BY Sequence";
			$ClassArr = $li->returnArray($sql);
			$numOfClass = count($ClassArr);
			
			if ($numOfClass > 0)
			{
				for ($i=0; $i<$numOfClass; $i++)
				{
					$thisYearClassID = $ClassArr[$i]['YearClassID'];
					$thisClassName = $ClassArr[$i]['ClassTitleEN'];
					
					# Get Student List of the Class in IP20
					$sql = "SELECT UserID, ClassNumber FROM $thisDB.INTRANET_USER WHERE ClassName = '$thisClassName' And RecordType = '2' And RecordStatus = '1'";
					$ClassStudentArr = $li->returnArray($sql);
					$numOfClassStudent = count($ClassStudentArr);
					
					if ($numOfClassStudent > 0)
					{
						$ClassStudentValueArr = array();
						for ($j=0; $j<$numOfClassStudent; $j++)
						{
							$thisUserID = $ClassStudentArr[$j]['UserID'];
							$thisClassNumber = $ClassStudentArr[$j]['ClassNumber'];
							
							$ClassStudentValueArr[] = "( '$thisYearClassID', '$thisUserID', '$thisClassNumber', NULL, NULL, NULL, NULL )";
						}
						
						$ClassStudentValueList = implode(',', $ClassStudentValueArr);
						$insert_class_student_sql = "
								Insert Into
										$thisDB.YEAR_CLASS_USER
										(YearClassID, UserID, ClassNumber, DateInput, InputBy, DateModified, ModifyBy)
										Values
										$ClassStudentValueList
								";
						$success['Insert']['Class_Student'][$thisClassName.'_'.$thisYearClassID] = $li->db_db_query($insert_class_student_sql) or die(mysql_error());
						
						if ($success['Insert']['Class_Student'][$thisClassName.'_'.$thisYearClassID])
						{
							echo "Insert Class Student in ".$thisClassName." ".$WordArr['Done'].".<br />";
						}
						else
						{
							// Enable the auto-increment of table YEAR_CLASS_USER
							$sql = "Alter table $thisDB.YEAR_CLASS_USER modify column YearClassUserID int(8) auto_increment";
							$success['AutoIncrement']['Enable']['YEAR_CLASS_USER'] = $li->db_db_query($sql) or die(mysql_error());
					
							echo "Insert Class Student in ".$thisClassName." ".$WordArr['Failed'].".<br />";
						}
					}
					else
					{
						echo "Class ".$thisClassName." has no student.<br />";
					}
				}
			}
			else
			{
				echo "No class record at this moment. So, no class student is added<br /><br />";
			}
			
			echo "============== End of Insert Class Student =============<br /><br />";
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'FormClassClassTeacherClassStudentSetup', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Insert Form ". $WordArr['DoneBefore'] ." ================<br />";
			echo "================ Insert Class ". $WordArr['DoneBefore'] ." ================<br />";
			echo "================ Insert Class Teacher ". $WordArr['DoneBefore'] ." ================<br />";
			echo "================ Insert Class Student ". $WordArr['DoneBefore'] ." ================<br />";
		}
		###################################################
		### End of Form, Class, Class Teacher and Class Students Setup
		###################################################
		
		#################################################################################################################################
		### eHomework																													#
		### Transfer the settings data from 																							#
		###	"homework_disable.txt", "homework_export.txt", "homework_nonteaching.txt", "homework_parent.txt",							#
		### "homework_past.txt", "homework_startdate.txt", "homework_leader.txt", "homework_bysubject.txt", "homework_byteacher.txt"	#
		### to table "GENERAL_SETTING"																									#
		#################################################################################################################################
		if(!$GSary['eHomeworkSettings'])
		{
			$eHomework_Module = "eHomework";
			
			$setting_file = array();
			$setting_file[0] = "$intranet_root/file/homework_disable.txt";
			$setting_file[1] = "$intranet_root/file/homework_export.txt";
			$setting_file[2] = "$intranet_root/file/homework_nonteaching.txt";
			$setting_file[3] = "$intranet_root/file/homework_parent.txt";
			$setting_file[4] = "$intranet_root/file/homework_past.txt";
			$setting_file[5] = "$intranet_root/file/homework_startdate.txt";
			$setting_file[6] = "$intranet_root/file/homework_leader.txt";
			$setting_file[7] = "$intranet_root/file/homework_bysubject.txt";
			$setting_file[8] = "$intranet_root/file/homework_byteacher.txt";
			
			$setting_Name = array();
			$setting_Name[0] = "disabled";
			$setting_Name[1] = "exportAllowed";
			$setting_Name[2] = "nonteachingAllowed";
			$setting_Name[3] = "parentAllowed";
			$setting_Name[4] = "pastInputAllowed";
			$setting_Name[5] = "startFixed";
			$setting_Name[6] = "subjectLeaderAllowed";
			$setting_Name[7] = "subjectSearchDisabled";
			$setting_Name[8] = "teacherSearchDisabled";
			
			$file_Name = array();
			$file_Name[0] = "homework_disable.txt";
			$file_Name[1] = "homework_export.txt";
			$file_Name[2] = "homework_nonteaching.txt";
			$file_Name[3] = "homework_parent.txt";
			$file_Name[4] = "homework_past.txt";
			$file_Name[5] = "homework_startdate.txt";
			$file_Name[6] = "homework_leader.txt";
			$file_Name[7] = "homework_bysubject.txt";
			$file_Name[8] = "homework_byteacher.txt";
			
			for($i=0; $i<9; $i++){
				if (is_file($setting_file[$i])){
					
					$file_content = get_file_content($setting_file[$i]);
					$dataValue = explode("\n",$file_content);
								
					list($value) = $dataValue;
					if ($value=="") $value = "0";
					
					$data = array($setting_Name[$i]=>$value);
					
					$setting = "'".$setting_Name[$i]."'";
					$settingName = array($setting);
					
					# check if there is already containds settings data, if yes, no need to transfer
					$tmp_ary = $lgs->Get_General_Setting($eHomework_Module, $settingName);
					
					if(empty($tmp_ary))
					{
						$lgs->Save_General_Setting($eHomework_Module, $data);
						echo "eHomework...Transfer the settings data from \"".$file_Name[$i]."\" to table \"GENERAL_SETTING\"... ".$WordArr['Done']."<br>";			  
					}
					else
					{
						echo "eHomework...Data already transferred<br>";
					}
				}
				else
				{
					echo "eHomework...No \"".$file_Name[$i]."\"<br>";	
				}
			}
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eHomeworkSettings', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ eHomework settings data transfer ". $WordArr['DoneBefore'] ."================<br />";
		}	
		#################################################################################################################################
		### eHomework [End]																												#	
		#################################################################################################################################
		
		###################################################
		### eCircular
		### Transfer the settings data from "settings_circular.txt" to table "GENERAL_SETTING"
		###################################################
		if($special_feature['circular'])
		{
			if(!$GSary['eCircularSettings'])
			{
				$Circular_Module = "CIRCULAR";
				
				$setting_file = "$intranet_root/file/settings_circular.txt";
				if (is_file($setting_file))
				{
					$file_content = get_file_content($setting_file);
					$data = explode("\n",$file_content);
					$ary = array();
					list($ary['disabled'],
						$ary['isHelpSignAllow'],
						$ary['isLateSignAllow'],
						$ary['isResignAllow'],
						$ary['defaultNumDays'],
						$ary['showAllEnabled']) = $data;
					if ($ary['defaultNumDays']=="") $ary['defaultNumDays'] = 7;
					
					# check if there is already containds settings data, if yes, no need to transfer
					$tmp_ary = $lgs->Get_General_Setting($Circular_Module);
					
					if(empty($tmp_ary))
					{
						$lgs->Save_General_Setting($Circular_Module, $ary);
						echo "eCircular...Transfer the settings data from \"settings_circular.txt\" to table \"GENERAL_SETTING\"... ".$WordArr['Done']."<br>";
					}
					else
					{
						echo "eCircular...Data already transferred<br>";
					}
				}
				else
				{
					echo "eCircular...No \"settings_circular.txt\"<br>";	
				}
				
				# update General Settings - markd the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eCircularSettings', 1, now())";
				$li->db_db_query($sql) or die(mysql_error());
			}
			else
			{
				echo "================ eCircular settings data transfer ". $WordArr['DoneBefore'] ."================<br />";
			}
		}
		###################################################
		### eCircular [End]
		###################################################
		
		###################################################
		### eNotice
		### Transfer the settings data from "settings_notice.txt" to table "GENERAL_SETTING"
		###################################################
		if($plugin['notice'])
		{
			if(!$GSary['eNoticeSettings'])
			{
				$Notice_Module = "eNotice";
				
				$setting_file = "$intranet_root/file/settings_notice.txt";
				if (is_file($setting_file))
				{
					$file_content = get_file_content($setting_file);
					$data = explode("\n",$file_content);
					$ary = array();
					
					list($ary['disabled'],
						$ary['fullAccessGroupID'],
						$ary['normalAccessGroupID'],
						$ary['isClassTeacherEditDisabled'],
						$ary['isAllAllowed'],
						$ary['defaultNumDays'],
						$ary['showAllEnabled'],
						$ary['DisciplineGroupID']) = $data;
						
					if ($ary['defaultNumDays']=="") $ary['defaultNumDays'] = 4;
					
					# check if there is already containds settings data, if yes, no need to transfer
					$tmp_ary = $lgs->Get_General_Setting($Notice_Module);
					
					if(empty($tmp_ary))
					{
						$lgs->Save_General_Setting($Notice_Module, $ary);
						echo "eNotice...Transfer the settings data from \"settings_notice.txt\" to table \"GENERAL_SETTING\"... ".$WordArr['Done']."<br>";
					}
					else
					{
						echo "eNotice...Data already transferred<br>";
					}
				}
				else
				{
					echo "eNotice...No \"settings_notice.txt\"<br>";	
				}
				
				# update General Settings - markd the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eNoticeSettings', 1, now())";
				$li->db_db_query($sql) or die(mysql_error());
			}
			else
			{
				echo "================ eNotice settings data transfer ". $WordArr['DoneBefore'] ."================<br />";
			}
		}
		###################################################
		### eNotice [End]
		###################################################
		
		###################################################
		### Student Attendance
		### Transfer the settings data to table "GENERAL_SETTING"
		###################################################
		if($plugin['attendancestudent'])
		{
			if(!$GSary['StudentAttendanceSettings'])
			{
				$content_basic = trim(get_file_content("$intranet_root/file/stattend_basic.txt"));
				$SettingList['AttendanceMode'] = $content_basic;
				$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
				$SettingList['TimeTableMode'] = $time_table; 	### 0 - Time Slot, 1 - Time Session
				$SettingList['DisallowProfileInput'] = trim(get_file_content("$intranet_root/file/disallow_student_profile_input.txt"));
				$SettingList['DefaultAttendanceStatus'] = trim(get_file_content("$intranet_root/file/studentattend_default_status.txt"));
				$SettingList['ProfileAttendCount'] = trim(get_file_content("$intranet_root/file/std_profile_attend_count.txt"));
				$SettingList['TerminalIP'] = get_file_content("$intranet_root/file/stattend_ip.txt");
				$SettingList['IgnorePeriod'] = get_file_content("$intranet_root/file/stattend_ignore.txt");
				$SettingList['TeacheriAccountProfileAllowEdit'] = get_file_content("$intranet_root/file/teacher_profile_settings.txt");
				
				$lgs->Save_General_Setting('StudentAttendance', $SettingList);
				
				echo "Student Attendance...Setting already transferred<br>";
				
				# update General Settings - markd the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'StudentAttendanceSettings', 1, now())";
				$li->db_db_query($sql) or die(mysql_error());
			}
			else
			{
				echo "================ Student Attendance settings data transfer ". $WordArr['DoneBefore'] ."================<br />";
			}
		}
		###################################################
		### Student Attendance [End]
		###################################################
		
		###################################################
		### ePayment
		### Transfer the settings data to table "GENERAL_SETTING"
		###################################################
		if($plugin['payment'])
		{
			if(!$GSary['ePaymentSettings'])
			{
				$PaymentLetterSetting['PaymentLetterHeader'] = trim(get_file_content("$intranet_root/file/pay_out_letter_head.txt"));
				$PaymentLetterSetting['PaymentLetterHeader1'] = trim(get_file_content("$intranet_root/file/pay_out_letter_head2.txt"));
				$PaymentLetterSetting['PaymentLetterFooter'] = trim(get_file_content("$intranet_root/file/pay_out_letter_foot.txt"));
				
				//$letter_signature_option_file = "$intranet_root/file/pay_out_letter_signature_option.txt";
				
				// pps
				$pps_charge_from_file = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));
			
				$temp =  explode("\n",$pps_charge_from_file);
				$SettingList['PPSChargeEnabled'] = $temp[0];
				$SettingList['PPSIEPSCharge'] = $temp[1];
				$SettingList['PPSCounterBillCharge'] = $temp[2];
			
				$SettingList['TerminalIPList'] = get_file_content("$intranet_root/file/payment_ip.txt");
				$SettingList['ConnectionTimeout'] = trim(get_file_content("$intranet_root/file/payment_expiry.txt"));
				$SettingList['ConnectionTimeout'] = ($SettingList['ConnectionTimeout']=="" || !is_numeric($SettingList['ConnectionTimeout']))? 60:$SettingList['ConnectionTimeout'];
				
				$authfile = get_file_content("$intranet_root/file/payment_auth.txt");
				$auth = explode("\n",$authfile);
				$SettingList['PaymentNoAuth'] = ($auth[0]==1);
				$SettingList['PurchaseNoAuth'] = ($auth[1]==1);
			
				$lgs->Save_General_Setting('ePayment', $SettingList);
				
				echo "ePayment...Setting already transferred<br>";
				
				# update General Settings - markd the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'ePaymentSettings', 1, now())";
				$li->db_db_query($sql) or die(mysql_error());
			}
			else
			{
				echo "================ ePayment settings data transfer ". $WordArr['DoneBefore'] ."================<br />";
			}
		}
		###################################################
		### ePayment [End]
		###################################################
		
		###################################################
		### Group [Start]
		###################################################
		if(!$GSary['GroupAcademicYearIDSettings'])
		{
			$Current_Academic_Year_ID = $insertedAcademicYearID;
			if($Current_Academic_Year_ID)
			{
				$sql = "update INTRANET_GROUP set AcademicYearID=$Current_Academic_Year_ID where AcademicYearID is NULL";
				$li->db_db_query($sql) or die(mysql_error());
				echo "All the Group's academic year id has set to the preset academic year id<br>";	
			}
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'GroupAcademicYearIDSettings', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ All the Group's academic year id has set to the preset academic year id ". $WordArr['DoneBefore'] ."================<br />";
		}
		###################################################
		### Group [End]
		###################################################
		
		###################################################
		### School Settings (Location)
		### Add Building Table and insert default value for building, floor and room
		###################################################
		
		# Run this script once only!
		if(!$GSary['LocationDefault'])
		{
			echo "============== Location Start ==============<br />";
		
			# 1. Create Building Table (Ensure the table is created before adding the default value)
			$create_building_sql = 
			"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION_BUILDING (
					BuildingID int(11) NOT NULL auto_increment,
					Code varchar(10),
					NameChi varchar(255),
					NameEng varchar(255),
					DisplayOrder int(11),
					RecordType int(11),
					RecordStatus int(11),
					DateInput datetime,
					DateModified datetime,
					PRIMARY KEY (BuildingID),
					INDEX BuildingID (BuildingID),
					INDEX InventoryBuildingNameChi (NameChi),
					INDEX InventoryBuildingNameEng (NameEng),
					UNIQUE ChineseName (NameChi),
					UNIQUE EnglishName (NameEng),
					UNIQUE BuildingIDWithCode (BuildingID, Code)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8
			";
			$li->db_db_query($create_building_sql) or die(mysql_error());
			echo "Building Table has been created successfully<br />";
			
			# 2. Insert a default building
			$default_building_sql = 
			"INSERT IGNORE INTO INVENTORY_LOCATION_BUILDING 
				(Code, NameChi, NameEng, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
			VALUES
				('MB', 'Main Building', 'Main Building', 1, NULL, 1, now(), now());
			";
			$li->db_db_query($default_building_sql) or die(mysql_error());
			echo "Default Building has been created successfully<br />";
			
			# 3. Activate all floors
			$activate_floor_sql = "UPDATE INVENTORY_LOCATION_LEVEL SET RecordStatus = '1';";
			$li->db_db_query($activate_floor_sql) or die(mysql_error());
			echo "All floors have been activated successfully<br />";
			
			# 4. Activate all rooms
			$activate_room_sql = "UPDATE INVENTORY_LOCATION SET RecordStatus = '1';";
			$li->db_db_query($activate_room_sql) or die(mysql_error());
			echo "All rooms have been activated successfully<br />";
			
			# 5. Assign floor, which has not assigned to any buildings yet, to the default building
			$assign_floor_to_default_building_sql = "UPDATE INVENTORY_LOCATION_LEVEL SET BuildingID = 1 WHERE BuildingID IS NULL;";
			$li->db_db_query($assign_floor_to_default_building_sql) or die(mysql_error());
			echo "Assign floors to the default building successfully<br />";
			
			echo "=============== Location End ===============<br /><br />";
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'LocationDefault', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Location settings ". $WordArr['DoneBefore'] ."================<br />";
		}
		###################################################
		### Location [End] 
		###################################################
		
		####################################################
		### Campus Link
		###################################################
		if(!$GSary['CampusLinkData'])
		{
			echo "Update Campus link ... <br>";
			$sql = "Create Table if not exists INTRANET_CAMPUS_LINK (
			  LinkID int(8) NOT NULL auto_increment,
			  Title varchar(255) default NULL,
			  URL varchar(255) default NULL,
			  DisplayOrder int(11) default NULL,
			  Description text,
			  DateInput datetime default NULL,
			  InputBy int(11) default NULL,
			  DateModified datetime default NULL,
			  ModifiedBy int(11) default NULL,
			  PRIMARY KEY  (LinkID)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8";
			
			$li->db_db_query($sql) or die(mysql_error());
			
			$lf = new libfilesystem();
			$links = explode("\n", $lf->file_read($intranet_root."/file/campuslink.txt"));
			$num = sizeof($links)/2;
			$max = max($num+5, 10);
			
			$sql = "select max(DisplayOrder) from INTRANET_CAMPUS_LINK";
			$resultSet = $li->returnVector($sql);
			$order = (empty($resultSet)||empty($resultSet[0]))?0:$resultSet[0];
			
			$sql = "insert into INTRANET_CAMPUS_LINK (Title, URL, DateInput, DateModified,DisplayOrder) values ";
			
			$cnt = 0;
			for($i=0; $i<$max; $i++) {
				if (empty($links[(2*$i)]) || empty($links[(2*$i+1)]))
					continue;
				if ($cnt > 0)
					$sql .= ", ";	
				$sql .= "('". addslashes($links[(2*$i)]) ."','".$links[(2*$i)+1]."', now(), now(), '".($order+1)."')";
				$order++;
				$cnt++;
			} 
			$li->db_db_query($sql) or die(mysql_error());
			
			echo $cnt." records are updated. <br>";
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CampusLinkData', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Campus Link data transfer ". $WordArr['DoneBefore'] ."================<br />";
		}
		#################################################
		
		####################################################
		### inventory
		####################################################
		if(!$GSary['eInventoryFunding'])
		{
			include_once($PATH_WRT_ROOT."includes/libinventory.php");
			$linventory	= new libinventory();
			
			$sql = "SELECT 
							a.ItemID, a.LocationiD, a.FundingSource 
					FROM 
							TEMP_INVENTORY_ITEM_BULK_FUNDING_LOG AS a INNER JOIN 
							INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and a.Locationid = b.LocationID)
					";
			$arr_result = $linventory->returnArray($sql,3);
			
			$result = array();
			if(sizeof($arr_result)>0){
					for($i =0; $i<sizeof($arr_result); $i++){
							list($item_id, $location_id, $funding_source_id) = $arr_result[$i];
							
							$sql = "UPDATE 
										INVENTORY_ITEM_BULK_LOCATION 
									SET 
										FundingSourceID = $funding_source_id
									WHERE
										ItemID = $item_id AND
										LocationID = $location_id
									";
							$result[] = $linventory->db_db_query($sql) or die(mysql_error());
					}
			}
			
			if(!in_array(false,$result)){
				echo "Update Iinventory Bult item funding Successfully";
			}else{
				echo "Update Iinventory Bult item funding Falied";
			}
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eInventoryFunding', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Update Iinventory Bult item funding ". $WordArr['DoneBefore'] ."================<br />";
		}
		
		###################################################
		### Change file content in "Preset Wordings" [Start]
		###################################################
		if(!$GSary['PresetWordingsData'])
		{
			$lf = new libwordtemplates();
			$base_dir = "$intranet_root/file/templates";
			$files = $lf->return_folderlist($base_dir);
			if(!empty($files))
			{
				foreach($files as $k=>$file_target)
				{
					$file_content = get_file_content($file_target);
					
					if(!empty($file_content))
					{
						//$cur_encoding = mb_detect_encoding($file_content) ; 
						//if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
						if(!(isUTF8($file_content) && mb_check_encoding($file_content,"UTF-8")))
						{
							$new_content = Big5ToUnicode($file_content);
							$lf->file_write($new_content,"$file_target");
							echo $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>"; 
						}
						else
						{
							echo $file_target .".... no need to convert to utf-8.<br>";
						}
					}
					else
					{
						echo $file_target .".... file empty.<br>"; 
					}
				}
			}
			echo "Preset Wordings checked complete.<br>";
		
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'PresetWordingsData', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Preset Wordings - file content convert to utf-8 ". $WordArr['DoneBefore'] ."================<br />";
		}
		
		###################################################
		### Change file content in "Present Wordings" [End]
		###################################################
		
		###################################################
		### Change file content in Campus TV schedule
		###################################################
		if(!$GSary['CampusTVscheduleData'])
		{
			include_once($PATH_WRT_ROOT."includes/libcampustv.php");
			$lcampustv = new libcampustv();
			
			$file_target = $lcampustv->programme_file;
			$file_content = get_file_content($file_target);
			
			if(!empty($file_content))
			{
				$lf = new libfilesystem();
				
				//$cur_encoding = mb_detect_encoding($file_content) ; 
				//if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
				if(!(isUTF8($file_content) && mb_check_encoding($file_content,"UTF-8")))
				{
					$new_content = Big5ToUnicode($file_content);
					$lf->file_write($new_content,"$file_target");
					echo $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>"; 
				}
				else
				{
					echo $file_target .".... no need to convert to utf-8.<br>";
				}
			}
			else
			{
				echo $file_target .".... file empty.<br>"; 
			}
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'CampusTVscheduleData', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ Campus TV schedule - file content convert to utf-8 ". $WordArr['DoneBefore'] ."================<br />";
		}
		###################################################
		### Change file content in Campus TV schedule [End]
		###################################################
		
		###################################################
		### Convert School Calendar > Holiday & Event Type
		###################################################
		if(!$GSary['SchoolCalendarData'])
		{
			include_once($PATH_WRT_ROOT."includes/libcycleperiods.php");
			
			$lcycleperiods = new libcycleperiods();
			### Get All the Holiday Event 
			$sql = "SELECT EventID FROM INTRANET_EVENT WHERE RecordType = 2";
			$HolidayArray = $lcycleperiods->returnVector($sql);
			### Get All the Group Event 
			$sql = "SELECT EventID FROM INTRANET_EVENT WHERE RecordType = 3";
			$GroupArray = $lcycleperiods->returnVector($sql);
			
			if(sizeof($HolidayArray)>0)
			{
				## Sync Holiday(IP20) -> School Holiday(IP25)	* RecordType Changed From 2 to 4
				$sql = "UPDATE INTRANET_EVENT SET RecordType = 4 WHERE EventID IN (".implode(",",$HolidayArray).")";
				$result = $lcycleperiods->db_db_query($sql) or die(mysql_error());
				if($result)
				{
					echo "School Calendar...All Holiday convert <font color='blue'>successfully</font>.<br>";
				}else{
					echo "School Calendar...Holiday convert <font color='red'>failed</font>.<br>";
				}
			}else{
				echo "School Calendar...NO Holiday need to convert.<br>";
			}
			
			if(sizeof($GroupArray)>0)
			{
				## Sync Group Event(IP20) -> Group Event(IP25)  * RecordType Changed From 3 to 2
				$sql = "UPDATE INTRANET_EVENT SET RecordType = 2 WHERE EventID IN (".implode(",",$GroupArray).")";
				$result = $lcycleperiods->db_db_query($sql) or die(mysql_error());
				if($result)
				{
					echo "School Calendar...All Group Event convert <font color='blue'>successfully</font>.<br>";
				}else{
					echo "School Calendar...Group Event convert <font color='red'>failed</font>.<br>";
				}
			}else{
				echo "School Calendar...NO Group Event need to convert.<br>";
			}
			
			### Create linkage for the Event Relation ###
			$sql = "UPDATE INTRANET_EVENT SET RelatedTo = EventID";
			$result = $lcycleperiods->db_db_query($sql) or die(mysql_error());
			if($result)
			{
				echo "School Calendar...Event Relationship linkage...<font color='blue'>Done</font>.<br>";
			}else{
				echo "School Calendar...Event Relationship linkage...<font color='red'>Failed</font>.<br>";
			}
		
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SchoolCalendarData', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ School Calendar data convert ". $WordArr['DoneBefore'] ."================<br />";
		}	
		###################################################
		### Convert School Calendar > Holiday & Event Type [End]
		###################################################
		
		###################################################
		### Change file content in "School data"
		###################################################
		if(!$GSary['SchoolDataData'])
		{
			$file_target = "$intranet_root/file/school_data.txt";
			$file_content = get_file_content($file_target);
					
			if(!empty($file_content))
			{
				$lf = new libfilesystem();
				
				//$cur_encoding = mb_detect_encoding($file_content) ; 
				//if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
				if(!(isUTF8($file_content) && mb_check_encoding($file_content,"UTF-8")))
				{
					$new_content = Big5ToUnicode($file_content);
					$lf->file_write($new_content,"$file_target");
					echo $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>"; 
				}
				else
				{
					echo $file_target .".... no need to convert to utf-8.<br>";
				}
			}
			else
			{
				echo $file_target .".... file empty.<br>"; 
			}
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'SchoolDataData', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ School Data - file content convert to utf-8 ". $WordArr['DoneBefore'] ."================<br />";
		}	
		###################################################
		### Change file content in "School data" [End]
		###################################################
		
		################### iCalendar update script ########################
		if(!$GSary['iCalendarUpdate'])
		{
			include_once($PATH_WRT_ROOT.'includes/icalendar.php');
			include_once($PATH_WRT_ROOT.'includes/libeclass.php');
			$iCal = new icalendar();
			$libeclass = new libeclass();
			# Alter table
			
			/*
			
			$sql ="alter table CALENDAR_REMINDER Add Column CalType tinyint(1)";
			$sqlResult["Alter_table_1"] = $iCal->db_db_query($sql) or die(mysql_error());
			
			$sql ="alter table {$eclass_db}.course Add Column CalID int(8)";
			$sqlResult["Alter_table_2"] = $iCal->db_db_query($sql) or die(mysql_error());
			
			$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key EventID";
			$sqlResult["Alter_table_3"] = $iCal->db_db_query($sql) or die(mysql_error());
			
			$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key UserID";
			$sqlResult["Alter_table_4"] = $iCal->db_db_query($sql) or die(mysql_error());
			
			$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Add Column PersonalNoteID int auto_increment primary key";
			$sqlResult["Alter_table_5"] = $iCal->db_db_query($sql) or die(mysql_error());
			
			*/
			
			$sql ="alter table CALENDAR_REMINDER Add Column CalType tinyint(1)";
			$sqlResult["Alter_table_1"] = $iCal->db_db_query($sql);
			
			$sql ="alter table {$eclass_db}.course Add Column CalID int(8)";
			$sqlResult["Alter_table_2"] = $iCal->db_db_query($sql);
			
			$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key EventID";
			$sqlResult["Alter_table_3"] = $iCal->db_db_query($sql);
			
			$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Drop Key UserID";
			$sqlResult["Alter_table_4"] = $iCal->db_db_query($sql);
			
			$sql ="alter table CALENDAR_EVENT_PERSONAL_NOTE Add Column PersonalNoteID int auto_increment primary key";
			$sqlResult["Alter_table_5"] = $iCal->db_db_query($sql);
			
			# Insert Group Calendar
			$sql = "Select * from INTRANET_GROUP where CalID is NULL";
			$result = $iCal->returnArray($sql);
			$subsql = "";
			$eclassDb = $eclass_db;
			
			if (count($result)>0){
				foreach ($result as $r){
					$calID = $iCal->createSystemCalendar($r["Title"], 2, 'P',$r['Description']);
					$sqlResult["Create_ical_$calID"] = $calID;
					$sql = "update INTRANET_GROUP set CalID = $calID 
							where GroupID = ".$r["GroupID"];
					$sqlResult["combine_ical_$calID"] = $iCal->db_db_query($sql) or die(mysql_error());
					$sql = "insert into CALENDAR_CALENDAR_VIEWER 
							(CalID,UserID,Access,Color,GroupID,GroupType) 
							select '$calID', UserID,
							Case When RecordType = 'A' and 
							(AdminAccessRight is NULL OR AdminAccessRight & 16 > 0)
							Then 'W'
							Else 'R'
							End,
							'2f75e9',
							'".$r["GroupID"]."',
							'E'
							From INTRANET_USERGROUP
							where GroupID = ".$r["GroupID"];
					$subsql = $r["GroupID"].",";
					$sqlResult["insert_group_viewer_$calID"] = $iCal->db_db_query($sql) or die(mysql_error());
				}
				$subsql=rtrim($subsql,",");
				#class group
				$sql = "insert into CALENDAR_CALENDAR_VIEWER 
						(CalID,UserID,Access,Color,GroupID,GroupType) 
						select '$calID', t.UserID,
						'W','2f75e9',c.GroupID,'T'
						From
						YEAR_CLASS as c inner join YEAR_CLASS_TEACHER as t on
						c.YearClassID = t.YearClassID
						where c.GroupID in ($subsql)";
			
				$sqlResult["insert_viewer_teacher"] = $iCal->db_db_query($sql) or die(mysql_error());
			}
			
			#insert class calendar
			$sql ="Select course_id, course_name, course_desc from {$eclassDb}.course where 
				CalID is null and RoomType=0"; 
			$result = $iCal->returnArray($sql);
			if (count($result)>0){
				foreach ($result as $r){
					$calID = $iCal->createSystemCalendar($r["course_name"],3,"P",$r["course_desc"]);
					$sqlResult["insert_class_Cal_$calID"] = $calID;
					$dbName = $libeclass->db_prefix."c".$r["course_id"];
					
					$sql2 = "update {$eclass_db}.course set CalID = $calID 
						where course_id = ".$r["course_id"];
					$sqlResult["update_course_$calID"] = $iCal->db_db_query($sql2) or die(mysql_error());
					// $sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$dbName'";
					// $existDB = $iCal->db_db_query($sql);
					$sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbName' AND table_name = 'usermaster'";
					$existDB = $iCal->returnArray($sql);
					if (!empty($existDB)){
					$sql = "
						insert into CALENDAR_CALENDAR_VIEWER 
						(CalID,UserID,Access,Color,GroupID,GroupType)
						select '$calID', i.UserID, 'W', '2f75e9','".$r["course_id"]."','C'
						From {$dbName}.usermaster as u inner join INTRANET_USER as i on
						u.user_email = i.UserEmail
						where u.status is null
						";
						$sqlResult["insert_course_viewer_$calID"] = $iCal->db_db_query($sql) or die(mysql_error());
					}
				}
			}
			#check the event creator when he has no default calendar
			$sql = "select distinct u.UserID
				from {$eclass_db}.event as e 
					inner join {$eclass_db}.course as c on
						e.course_id = c.course_id 
					inner join {$eclass_db}.user_course as uc on
						c.course_id = uc.course_id and
						e.user_id = uc.user_id
					inner join INTRANET_USER as u on
						uc.user_email = u.UserEmail
				where u.UserID not in (select ca.Owner from CALENDAR_CALENDAR as ca inner join 
				CALENDAR_CALENDAR_VIEWER as v on ca.CalID = v.CalID where v.Access='A')
				";
			$eventCreator = $iCal->returnVector($sql);
			if (count($eventCreator)>0){
				foreach($eventCreator as $user){ 
					$tUserID = $user;
					$iCal->insertMyCalendar();
				} 
			}
			#insert original eclass event
			$sql = "select e.title,e.notes,
					e.share_type,
					c.CalID,
					e.is_important,e.is_allday,e.inputdate,
					e.durMi,e.durHr,e.share_type,e.eventdate,
					e.last_modified, e.link, u.UserID
					from {$eclass_db}.event as e 
					inner join {$eclass_db}.course as c on
						e.course_id = c.course_id 
					inner join {$eclass_db}.user_course as uc on
						c.course_id = uc.course_id and
						e.user_id = uc.user_id
					inner join INTRANET_USER as u on
						uc.user_email = u.UserEmail
					";
			$result = $iCal->returnArray($sql);
			
			if (count($result) > 0){
				$calIDset ="";	
				$sql = "insert into CALENDAR_EVENT_ENTRY 
				(UserID,CalID,EventDate,InputDate,ModifiedDate,Duration,IsImportant,IsAllDay
				,Access,Title,Description,Url) values ";
				/*$toEcho = true;
				foreach($result as $r){
					$calID = $r["CalID"];
					if ($r['share_type']==0){
						$tUserID = $r["UserID"];
						$calID = $iCal->returnOwnerDefaultCalendar();
					}
					$duration = $r["is_allday"]?1440:$r["durMi"]+$r["durHr"]*60;
					$sql .="('".$r["user_id"]."','".$calID."','".$r["eventdate"]."',
							'".$r["inputdate"]."','".$r["last_modified"]."','$duration',
							'".$r["is_important"]."','".$r["is_allday"]."','P',
							'".addslashes($r["title"])."','".addslashes($r["notes"])."','".$r["link"]."'
							), ";
					$calIDset .= $calID.",";
					if ($toEcho)
					{
						debug($sql);
						$toEcho = false;
					}
				}	
				$sql = rtrim($sql,", ");
		
				$sqlResult["insert_Event"] =$iCal->db_db_query($sql);
				//debug($sqlResult["insert_Event"]);
				$calIDset = rtrim($calIDset,", ");*/
				$calIDset ="";
				$hsql = "insert into CALENDAR_EVENT_ENTRY 
				(UserID,CalID,EventDate,InputDate,ModifiedDate,Duration,IsImportant,IsAllDay
				,Access,Title,Description,Url) values ";
				$i = 0;
				foreach($result as $r){
					$calID = $r["CalID"];
					if ($r['share_type']==0){
						$tUserID = $r["UserID"];
						$calID = $iCal->returnOwnerDefaultCalendar();
					}
					$duration = $r["is_allday"]?1440:$r["durMi"]+$r["durHr"]*60;
					$sql = $hsql."('".$r["UserID"]."','".$calID."','".$r["eventdate"]."',
							'".$r["inputdate"]."','".$r["last_modified"]."','$duration',
							'".$r["is_important"]."','".$r["is_allday"]."','P',
							'".addslashes($r["title"])."','".addslashes($r["notes"])."','".$r["link"]."'
							)";
					$sqlResult["insert_Event_".$i] = $iCal->db_db_query($sql) or die(mysql_error());			
					$calIDset[] = $calID;
					if ($calID == '' || empty($calID)){
						echo $sql;
						debug_r($r);
					}
					$i++;
				}
				$calIDset = array_unique($calIDset);
				$calIDset = implode("','",$calIDset);
			$sql = "update CALENDAR_EVENT_ENTRY set 
					UID = concat(UserID,'-',CalID,'-',EventID,'@eclass')
					where CalID in ('$calIDset') and UID is NULL;
					";
			$sqlResult["update_Event"] =$iCal->db_db_query($sql) or die(mysql_error());
			}
		
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'iCalendarUpdate', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ iCalendar - update script ". $WordArr['DoneBefore'] ."================<br />";
		}
		################## End iCalendar update script########################
		
		############################################################
		# eEnrolment v1.2 [Start]
		############################################################
		if($plugin['eEnrollment'])
		{
			if(!$GSary['eEnrolmentSettings'])
			{
				include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
				$libenroll = new libclubsenrol();
				
				echo "===================================================================<br>\r\n";
				echo "eEnrolment v1.2 [Start]</b><br>\r\n";
				echo "===================================================================<br>\r\n";
				
				$eEnrolment_Module = "eEnrolment";
				
				# check if there is already containds settings data, if yes, no need to transfer
				$tmp_ary = $lgs->Get_General_Setting($eEnrolment_Module);
				
				if(empty($tmp_ary))
				{
					### Club Enrolment Settings
					$setting_file = "$intranet_root/file/clubenroll_setting.txt";
					if (is_file($setting_file))
					{
						$file_content = get_file_content($setting_file);
						$data = explode("\n",$file_content);
						$ary = array();
						// Config GpStart and GpEnd are not used anymore
						list($ary['Club_EnrolmentMode'],
							$ary['Club_ApplicationStart'],
							$ary['Club_ApplicationEnd'],
							$GpStart,
							$GpEnd,
							$ary['Club_DefaultMin'],
							$ary['Club_DefaultMax'],
							$ary['Club_TieBreak'],
							$ary['Club_EnrollMax'],
							$ary['Club_EnrollPersonType'],
							$ary['Club_DisableStatus'],
							$ary['Club_ActiveMemberPer'],
							$ary['Club_OneEnrol'],
							$ary['Club_CaterPriority']) = $data;
						
						$Success['ClubSettings'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
						
						if ($Success['ClubSettings'])
							echo "Club Enrolment Settings are transferred successfully<br>\r\n";
						else
							echo "Club Enrolment Settings transfer failed<br>\r\n";
					}
					else
					{
						echo "eEnrolment...No \"clubenroll_setting.txt\"<br>\r\n";	
					}
					
					### Club Description
					$desp_file = "$intranet_root/file/clubenroll_desp.txt";
					if (is_file($desp_file))
					{
						$ary = array();
						$ary['Club_EnrollDescription'] = stripslashes(get_file_content($desp_file));
						$Success['ClubDescription'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
						
						if ($Success['ClubDescription'])
							echo "Club Enrolment Description are transferred successfully<br>\r\n";
						else
							echo "Club Enrolment Description transfer failed<br>\r\n";
					}
					else
					{
						echo "eEnrolment...No \"clubenroll_desp.txt\"<br>";	
					}
					
					
					### Activity Enrolment Settings
					$setting_event_file = "$intranet_root/file/clubenroll_setting_event.txt";
					if (is_file($setting_event_file))
					{
						$file_content = get_file_content($setting_event_file);
						$data = explode("\n",$file_content);
						$ary = array();
						// Config GpStart and GpEnd are not used anymore
						// Config Tiebreak and PersonType are not applicable for Activity
						list($ary['Activity_EnrolmentMode'],
							$ary['Activity_ApplicationStart'],
							$ary['Activity_ApplicationEnd'],
							$ActivityGpStart,
							$ActivityGpEnd,
							$ary['Activity_DefaultMin'],
							$ary['Activity_DefaultMax'],
							$ActivityTieBreak,
							$ary['Activity_EnrollMax'],
							$ActivityEnrollPersonType,
							$ary['Activity_DisableStatus'],
							$ary['Activity_ActiveMemberPer'],
							$ary['Activity_OneEnrol']) = $data;
						
						$Success['ActivitySettings'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
						
						if ($Success['ActivitySettings'])
							echo "Activity Enrolment Settings are transferred successfully<br>\r\n";
						else
							echo "Activity Enrolment Settings transfer failed<br>\r\n";
					}
					else
					{
						echo "eEnrolment...No \"clubenroll_setting_event.txt\"<br>\r\n";	
					}
					
					### Activity Description
					$desp_event_file = "$intranet_root/file/clubenroll_desp_event.txt";
					if (is_file($desp_event_file))
					{
						$ary = array();
						$ary['Activity_EnrollDescription'] = stripslashes(get_file_content($desp_event_file));
						$Success['ActivityDescription'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
						
						if ($Success['ActivityDescription'])
							echo "Activity Enrolment Description are transferred successfully<br>\r\n";
						else
							echo "Activity Enrolment Description transfer failed<br>\r\n";
					}
					else
					{
						echo "eEnrolment...No \"clubenroll_desp_event.txt\"<br>";	
					}
					
					
					### Last Archive Info
					$archive_file = "$intranet_root/file/clubenroll_archive.txt";
					if (is_file($archive_file))
					{
						$ary = array();
						$ary['LastArchiveDateTime'] = stripslashes(get_file_content($archive_file));
						$Success['LastArchiveDateTime'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
						
						if ($Success['LastArchiveDateTime'])
							echo "Last Archive Info is transferred successfully<br>\r\n";
						else
							echo "Last Archive Info transfer failed<br>\r\n";
					}
					else
					{
						echo "eEnrolment...No \"clubenroll_archive.txt\"<br>";	
					}
					
					
					### Enable Overall Performance Report 
					$setting_performance_file = "$intranet_root/file/clubenroll_setting_performance.txt";
					if (is_file($setting_performance_file))
					{
						$ary = array();
						$ary['EnableOverallPerformanceReport'] = stripslashes(get_file_content($setting_performance_file));
						$Success['EnableOverallPerformanceReport'] = $lgs->Save_General_Setting($eEnrolment_Module, $ary);
						
						if ($Success['EnableOverallPerformanceReport'])
							echo "Enable Overall PerformanceReport flag is transferred successfully<br>\r\n";
						else
							echo "Enable Overall PerformanceReport flag transfer failed<br>\r\n";
					}
					else
					{
						echo "eEnrolment...No \"clubenroll_setting_performance.txt\"<br>";	
					}
					
				}
				else
				{
					echo "Enrolment Settings has been transferred already. No settings transferred this time<br>\r\n";
				}
				
				echo "===================================================================<br>\r\n";
				echo "eEnrolment v1.2 [End]</b><br>\r\n";
				echo "===================================================================<br><br>\r\n";
				
				# update General Settings - markd the script is executed
				$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eEnrolmentSettings', 1, now())";
				$li->db_db_query($sql) or die(mysql_error());
			}
			else
			{
				echo "================ eEnrolment settigns data transfer ". $WordArr['DoneBefore'] ."================<br />";
			}
		}
		############################################################
		# eEnrolment v1.2 [End]
		############################################################
			
		###################################################
		### eDiscipline (set all the AP records to "Released") [Start]
		###################################################
		if(!$GSary['eDiscipline_AP_ALL_released'])
		{
			echo "================ eDiscipline (set all the AP records to Released) ================<br />";
			
			# if record is Approved, then set the records to Released
			$sql = "update DISCIPLINE_MERIT_RECORD set ReleaseStatus=1, ReleasedDate=now() where RecordStatus=1";
			$li->db_db_query($sql) or die(mysql_error());
			
			echo "eDiscipline (set all the AP records to Released)... ".$WordArr['Done'].".<br />";
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eDiscipline_AP_ALL_released', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
		}
		else
		{
			echo "================ eDiscipline (set all the AP records to Released) ". $WordArr['DoneBefore'] ." ================<br />";
		}
		###################################################
		### eDiscipline (set all the AP records to "Released") [Start]
		###################################################	
		
		############################################################
		# ReportCard Semester Patch [Start]
		############################################################
		if($plugin['ReportCard2008'] && !$GSary['eRC_UpdateSemester'])
		{
			echo "===================================================================<br>\r\n";
			echo "ReportCard Semester Patch [Start]</b><br>\r\n";
			echo "===================================================================<br><br>\r\n\r\n";
			
			$sql = "SHOW DATABASES LIKE '%".$intranet_db."_DB_REPORT_CARD%'";
			$DatabaseArr = $li->returnArray($sql);
			
			for($i=0; $i<sizeof($DatabaseArr);$i++)
			{
				$reportcard_db = $DatabaseArr[$i][0];
				
				echo "============ ".$reportcard_db." Start ============</b><br><br>\r\n";
				
				### Update Semester in RC_REPORT_TEMPLATE (for Term Report ONLY)
				echo "Update Semester in RC_REPORT_TEMPLATE (for Term Report ONLY)<br>\r\n";
				
				$sql = "Select ReportID From $reportcard_db.RC_REPORT_TEMPLATE Where Semester != 'F' And Semester Is Not Null";
				$TermReportIDArr = $li->returnVector($sql);
				$numOfTermReport = count($TermReportIDArr);
				
				if ($numOfTermReport == 0)
				{
					echo "There is no term report.<br>\r\n";
				}
				else
				{
					echo "There are $numOfTermReport term report(s).<br>\r\n";
					
					$TermReportIDList = implode(',', $TermReportIDArr);
					echo $TermReportIDList."<br><br>\r\n\r\n";
					
					$sql = "Update $reportcard_db.RC_REPORT_TEMPLATE Set Semester = Semester + 1 Where ReportID In ($TermReportIDList)";
					echo $sql."<br>\r\n";
					
					$successArr[$reportcard_db]['RC_REPORT_TEMPLATE'] = $li->db_db_query($sql) or die(mysql_error());
					if ($successArr[$reportcard_db]['RC_REPORT_TEMPLATE'])
					{
						echo "Update <font color='blue'>Success</font><br>\r\n";
					}
					else
					{
						echo "Update <font color='red'>Failed</font><br>\r\n";
					}
				}
				
				
				### Update SemesterNum in RC_REPORT_TEMPLATE_COLUMN (for Consolidate Report ONLY)
				echo "<br>\r\nUpdate SemesterNum in RC_REPORT_TEMPLATE_COLUMN (for Consolidate Report ONLY)<br>\r\n";
				
				$sql = "Select ReportID From $reportcard_db.RC_REPORT_TEMPLATE Where Semester = 'F'";
				$ConsolidatedReportIDArr = $li->returnVector($sql);
				$numOfConsolidatedReport = count($ConsolidatedReportIDArr);
				
				if ($numOfConsolidatedReport == 0)
				{
					echo "There is no consolidated report.<br>\r\n";
				}
				else
				{
					echo "There are $numOfConsolidatedReport consolidated report(s).<br>\r\n";
					
					$ConsolidatedReportIDList = implode(',', $ConsolidatedReportIDArr);
					echo $ConsolidatedReportIDList."<br><br>\r\n";
					
					$sql = "Update $reportcard_db.RC_REPORT_TEMPLATE_COLUMN Set SemesterNum = SemesterNum + 1 Where ReportID In ($ConsolidatedReportIDList)";
					echo $sql."<br>\r\n";
					
					$successArr[$reportcard_db]['RC_REPORT_TEMPLATE_COLUMN'] = $li->db_db_query($sql) or die(mysql_error());
					if ($successArr[$reportcard_db]['RC_REPORT_TEMPLATE_COLUMN'])
					{
						echo "Update <font color='blue'>Success</font><br>\r\n";
					}
					else
					{
						echo "Update <font color='red'>Failed</font><br>\r\n";
					}
				}
				
				echo "============ ".$reportcard_db." End ============</b><br><br>\r\n\r\n";
			}
			
			# update General Settings - markd the script is executed
			$sql = "insert ignore into GENERAL_SETTING (Module, SettingName, SettingValue, DateInput) values ('$ModuleName', 'eRC_UpdateSemester', 1, now())";
			$li->db_db_query($sql) or die(mysql_error());
			
			echo "===================================================================<br>\r\n";
			echo "ReportCard Semester Patch [End]</b><br>\r\n";
			echo "===================================================================<br>\r\n";
		}
		############################################################
		# ReportCard Semester Patch [End]
		############################################################

echo "<br /><br />";
echo "<font size='+1' color='blue'><b>The system setup is completed.</b></font><br />";
echo "* To view the old data of eDiscipline1.2, please setup the past school years and terms in School Settings > School Calendar > Academic Year Settings first. ";
echo "And then run data_patch.php for data patch.<br />";

echo "/* Back to main */";

intranet_closedb();
?>