<?
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include('../check.php');

intranet_opendb();

$lgs = new libgeneralsettings();
$ModuleName = "InitSetting";
$ary = $lgs->Get_General_Setting($ModuleName);

?>
<script language="javascript">
<!--
function SelectAll()
{
	var c = document.getElementsByName('setting_name[]');
	for(i=0;i<c.length;i++)
		c[i].checked = true;
}
//-->
</script>


<form name="form1" action="manual_update_general_setting_update.php">

<table border="01" cellpadding="5" cellspacing="0">
<tr>
	<td><input type="checkbox" name="setting_name[]" value="Clone_ASSESSMENT_SUBJECT" <?=$ary['Clone_ASSESSMENT_SUBJECT'] ? "checked disabled" : ""?>></td>
	<td>Clone ASSESSMENT_SUBJECT from eClass to IP DB</td>
	<td>Clone_ASSESSMENT_SUBJECT</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="AcademicYear_Term_Setup" <?=$ary['AcademicYear_Term_Setup'] ? "checked disabled" : ""?>></td>
	<td>Academic Year and Term Setup</td>
	<td>AcademicYear_Term_Setup</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="FormClassClassTeacherClassStudentSetup" <?=$ary['FormClassClassTeacherClassStudentSetup'] ? "checked disabled" : ""?>></td>
	<td>Form, Class, Class Teacher and Class Students Setup</td>
	<td>FormClassClassTeacherClassStudentSetup</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="eHomeworkSettings" <?=$ary['eHomeworkSettings'] ? "checked disabled" : ""?>></td>
	<td>eHomework - Transfer settings data</td>
	<td>eHomeworkSettings</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="eCircularSettings" <?=$ary['eCircularSettings'] ? "checked disabled" : ""?>></td>
	<td>eCircular - Transfer settings data</td>
	<td>eCircularSettings</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="eNoticeSettings" <?=$ary['eNoticeSettings'] ? "checked disabled" : ""?>></td>
	<td>eNotice - Transfer settings data</td>
	<td>eNoticeSettings</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="StudentAttendanceSettings" <?=$ary['StudentAttendanceSettings'] ? "checked disabled" : ""?>></td>
	<td>Student Attendance - Transfer settings data</td>
	<td>StudentAttendanceSettings</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="ePaymentSettings" <?=$ary['ePaymentSettings'] ? "checked disabled" : ""?>></td>
	<td>ePayment - Transfer settings data</td>
	<td>ePaymentSettings</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="GroupAcademicYearIDSettings" <?=$ary['GroupAcademicYearIDSettings'] ? "checked disabled" : ""?>></td>
	<td>Group - Set existings group academic year id to current academic year id</td>
	<td>GroupAcademicYearIDSettings</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="LocationDefault" <?=$ary['LocationDefault'] ? "checked disabled" : ""?>></td>
	<td>School Settings (Location) - Add Building Table and insert default value for building, floor and room</td>
	<td>LocationDefault</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="CampusLinkData" <?=$ary['CampusLinkData'] ? "checked disabled" : ""?>></td>
	<td>Campus Link - Transfer data</td>
	<td>CampusLinkData</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="eInventoryFunding" <?=$ary['eInventoryFunding'] ? "checked disabled" : ""?>></td>
	<td>eInventory - Update Iinventory Bult item funding</td>
	<td>eInventoryFunding</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="PresetWordingsData" <?=$ary['PresetWordingsData'] ? "checked disabled" : ""?>></td>
	<td>Preset Wordings - file content convert to utf-8</td>
	<td>PresetWordingsData</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="CampusTVscheduleData" <?=$ary['CampusTVscheduleData'] ? "checked disabled" : ""?>></td>
	<td>Campus TV schedule - file content convert to utf-8</td>
	<td>CampusTVscheduleData</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="SchoolCalendarData" <?=$ary['SchoolCalendarData'] ? "checked disabled" : ""?>></td>
	<td>School Calendar - Convert School Calendar > Holiday & Event Type</td>
	<td>SchoolCalendarData</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="SchoolDataData" <?=$ary['SchoolDataData'] ? "checked disabled" : ""?>></td>
	<td>School data - file content convert to utf-8</td>
	<td>SchoolDataData</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="iCalendarUpdate" <?=$ary['iCalendarUpdate'] ? "checked disabled" : ""?>></td>
	<td>iCalendar - update script</td>
	<td>iCalendarUpdate</td>
</tr>

<tr>
	<td><input type="checkbox" name="setting_name[]" value="eEnrolmentSettings" <?=$ary['eEnrolmentSettings'] ? "checked disabled" : ""?>></td>
	<td>eEnrolment - Transfer settings data</td>
	<td>eEnrolmentSettings</td>
</tr>



</table>	
	
<input type="button" value="Select All" onClick="SelectAll();">
<input type="submit" value="Submit">
</form>

<? intranet_closedb(); ?>