<?
# using:  kenneth chung 

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include('../check.php');

intranet_opendb();

if ($flag==1)
{
	$x .= "studentattendance_script.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";	

	$li = new libdb();

	############################################################
	# Student Attendance [Start]
	############################################################
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$lgs = new libgeneralsettings();
	
	$SettingList['TeacheriAccountProfileAllowEdit'] = get_file_content("$intranet_root/file/teacher_profile_settings.txt");
	
	$lgs->Save_General_Setting('StudentAttendance', $SettingList);
	############################################################
	# Student Attendance [End]
	############################################################

	$x .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";
	echo $x;

	#  save log
	$lf = new libfilesystem();
	$log_folder = $PATH_WRT_ROOT."file/ip20_to_ip25_studentattendance_script";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/data_atch_log_". date("YmdHis") .".html";
	$lf->file_write($x, $log_file);
	echo "<br>File Log: ". $log_file;

}
else
{ ?>
	<html>
	<body>
	<font color=red size=+1>[Please Make DB Backup First]</font><br><br>
	
	This page will update the eClass IP 2.0 data compatible with eClass IP 2.5<br />
	Updates included in this script:<br /><br />
	
	1. Update settings of the following module(s):<br />
		<ul>
			<li>Student Attendance</li>
		</ul>
		
	<a href=?flag=1>Click here to proceed.</a> <br><br>
	
	<br>
	</body></html>

	
	
<? }

intranet_closedb();
 ?>
	