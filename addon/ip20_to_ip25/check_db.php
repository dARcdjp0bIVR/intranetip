<?
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
//include('../check.php');


function writeFile($body, $file){		
	$x = (($fd = fopen($file, "a+"))&& chmod($file, 0777) && (fputs($fd, $body))) ? 1 : 0;		
	fclose ($fd);
	return $x;
}

function check_dir($log_path){
	if (!is_dir($log_path)){ 	//create dir
		mkdir($log_path, 0777);
	}
}

## Log file info
$log_path = $PATH_WRT_ROOT.'file/';
check_dir($log_path);
$log_path .= "migratelog/";
check_dir($log_path);
$log_path .= "check_db/";
check_dir($log_path);
$log_path .= date('Ymd_H_i_s')."/";
check_dir($log_path);

//$log_file = $log_path.'check_db_schema_'.date('Ymd_H_i_s').'.html';

intranet_opendb();
//$prefix = "eclass40_c";
//$CIDs = array('68','85','82','81','1000','80','31');

$li = new libdb();
$li->db = $intranet_db;

$Target_DB = array();

# get eRC DB
$sql = "show databases like '%".$intranet_db."_DB_REPORT_CARD%'";
$Target_DB = $li->returnVector($sql);
$Target_DB[] = $intranet_db;          # include the Main Intranet DB at last in the array

# get total size of array
$total = sizeof($Target_DB);



$totalError  = 0;
for($i=0; $i < $total; $i++)
{
	$thisDB = $Target_DB[$i];
	$li->db = $thisDB;

	$db_selected = mysql_select_db($li->db);
	if (!$db_selected) {
//		echo "Databaes / ClassRoom : [".$li->db."] not exist, skip checking<br/>";
		$x = "Databaes / ClassRoom : [".$li->db."] not exist, skip checking<br>";
		$log_file = $log_path.$thisDB.'_fail.html';
		exec("touch ".$log_file);
		writeFile($x, $log_file);	  
		continue;
	}

  $sql = "SHOW TABLE STATUS";
  $tablenames = $li->returnArray($sql);
  
  $x .= "DATABASE : ".$thisDB;
  $x .= "<table width='70%' border=1 cellpadding=2 cellspacing=0>";
  $x .= "<tr>";
  $x .= "<td>Table</td><td>Engine</td><td>Collation</td><td>Engine Pass</td><td>Collation Pass</td><td>All Pass</td>";
  $x .= "</tr>";
  
  $fail = 0;
  $totalTable = 0;
  $ErrorLink = "";
  for($k=0; $k < sizeof($tablenames); $k++ )
  {
    $totalTable++;
    $engine = $tablenames[$k]['Engine'];
    $collation = $tablenames[$k]['Collation'];
    
    $enginepass = 'Y';
    $collpass = 'Y';

    if(!strstr($engine,'InnoDB')) 
    {
      $engine = '<font color="red">'.$engine.'</font>';
      $enginepass = '<font color="red">N</font>';
    }
    if(!strstr($collation,'utf8'))
    {
      $collation = '<font color="red">'.$collation.'</font>';
      $collpass = '<font color="red">N</font>';
    }
    
    if($enginepass=='Y' && $collpass == 'Y')
    {
      $allPass = 'Y';
    }
    else 
    {
      $allPass = '<font color="red">N</font>';
	  $totalError= $totalError+1;
      $fail++;
      $ErrorLink = "<li><a href='#".$thisDB."'>".$thisDB."</a></li>";
    }
    
    
    $x .= "<tr>";
    $x .= "<td>".$tablenames[$k]['Name']."</td>";
    $x .= "<td>".$engine."</td>";
    $x .= "<td>".$collation."</td>";
    $x .= "<td>".$enginepass."</td>";
    $x .= "<td>".$collpass."</td>";
    $x .= "<td>".$allPass."</td>";
    $x .= "</tr>";     
  }
  
  $x .= "</table>";
  $x .= "<br />";
  $x .= "<a name='".$thisDB."'></a>";
  $x .= $thisDB." Total Fail : ".$fail."/".$totalTable;
  $x .= "<hr>";
  $x .= "<br /><br />";
  
  $TotalErrorLink .= $ErrorLink;


	if($fail > 0){
		$log_file = $log_path.$thisDB.'_fail.html';
	} else {
		$log_file = $log_path.$thisDB.'.html';
	}
	exec("touch ".$log_file);
	writeFile($x, $log_file);	  

}

if (trim($TotalErrorLink) == ""){
	$TotalErrorLink = "<font color = \"green\"> No DB error found! All Pass</font><br/>";
}

/*
$x .= "Error ClassRoom / Database : <br />";
$x .= "<ol style='list-style-type:decimal'>";
$x .= $TotalErrorLink;
$x .= "</ol>";
echo $x;
*/
echo ($totalError > 0) ? 'false' : 'true';
intranet_closedb();
?>
