<?
# using:  

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include('../check.php');

intranet_opendb();

$x = '';
if ($flag==1)
{
	$x .= "erc_semester_script.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";	

	$li = new libdb();

	############################################################
	# ReportCard Semester Patch [Start]
	############################################################
	if($plugin['ReportCard2008'])
	{
		$x .= "===================================================================<br>\r\n";
		$x .= "ReportCard Semester Patch [Start]</b><br>\r\n";
		$x .= "===================================================================<br><br>\r\n";
		
		$sql = "SHOW DATABASES LIKE '%".$intranet_db."_DB_REPORT_CARD%'";
		$DatabaseArr = $li->returnArray($sql);
		
		for($i=0; $i<sizeof($DatabaseArr);$i++)
		{
			$reportcard_db = $DatabaseArr[$i][0];
			
			$x .= "============ ".$reportcard_db." Start ============</b><br><br>\r\n";
			
			### Update Semester in RC_REPORT_TEMPLATE (for Term Report ONLY)
			$x .= "Update Semester in RC_REPORT_TEMPLATE (for Term Report ONLY)<br>\r\n";
			
			$sql = "Select ReportID From $reportcard_db.RC_REPORT_TEMPLATE Where Semester != 'F' And Semester Is Not Null";
			$TermReportIDArr = $li->returnVector($sql);
			$numOfTermReport = count($TermReportIDArr);
			
			if ($numOfTermReport == 0)
			{
				$x .= "There is no term report.<br>\r\n";
			}
			else
			{
				$x .= "There are $numOfTermReport term report(s).<br>\r\n";
				
				$TermReportIDList = implode(',', $TermReportIDArr);
				$x .= $TermReportIDList."<br><br>\r\n";
				
				$sql = "Update $reportcard_db.RC_REPORT_TEMPLATE Set Semester = Semester + 1 Where ReportID In ($TermReportIDList)";
				$x .= $sql."<br>\r\n";
				
				$successArr[$reportcard_db]['RC_REPORT_TEMPLATE'] = $li->db_db_query($sql);
				if ($successArr[$reportcard_db]['RC_REPORT_TEMPLATE'])
				{
					$x .= "Update <font color='blue'>Success</font><br><br>\r\n";
				}
				else
				{
					$x .= "Update <font color='red'>Failed</font><br><br>\r\n";
				}
			}
			
			
			### Update SemesterNum in RC_REPORT_TEMPLATE_COLUMN (for Consolidate Report ONLY)
			$x .= "Update SemesterNum in RC_REPORT_TEMPLATE_COLUMN (for Consolidate Report ONLY)<br>\r\n";
			
			$sql = "Select ReportID From $reportcard_db.RC_REPORT_TEMPLATE Where Semester = 'F'";
			$ConsolidatedReportIDArr = $li->returnVector($sql);
			$numOfConsolidatedReport = count($ConsolidatedReportIDArr);
			
			if ($numOfConsolidatedReport == 0)
			{
				$x .= "There is no consolidated report.<br>\r\n";
			}
			else
			{
				$x .= "There are $numOfConsolidatedReport consolidated report(s).<br>\r\n";
				
				$ConsolidatedReportIDList = implode(',', $ConsolidatedReportIDArr);
				$x .= $ConsolidatedReportIDList."<br><br>\r\n";
				
				$sql = "Update $reportcard_db.RC_REPORT_TEMPLATE_COLUMN Set SemesterNum = SemesterNum + 1 Where ReportID In ($ConsolidatedReportIDList)";
				$x .= $sql."<br>\r\n";
				
				$successArr[$reportcard_db]['RC_REPORT_TEMPLATE_COLUMN'] = $li->db_db_query($sql);
				if ($successArr[$reportcard_db]['RC_REPORT_TEMPLATE_COLUMN'])
				{
					$x .= "Update <font color='blue'>Success</font><br><br>\r\n";
				}
				else
				{
					$x .= "Update <font color='red'>Failed</font><br><br>\r\n";
				}
			}
			
			$x .= "============ ".$reportcard_db." End ============</b><br><br><br>\r\n";
		}
		
		$x .= "===================================================================<br>\r\n";
		$x .= "ReportCard Semester Patch [End]</b><br>\r\n";
		$x .= "===================================================================<br>\r\n";
	}
	############################################################
	# ReportCard Semester Patch [End]
	############################################################
	
	
	
	$x .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";	
	echo $x;

	#  save log
	$lf = new libfilesystem();
	$log_folder = $PATH_WRT_ROOT."file/reportcard2008/semester_script";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/data_patch_log_". date("YmdHis") .".html";
	$lf->file_write($x, $log_file);
	echo "<br>File Log: ". $log_file;

}
else
{ ?>
	<html>
	<body>
	<font color=red size=+1>[Please Make DB Backup First]<br>This script can be run once only</font><br><br>
	
	This page will update the eReportCard Semester when upgrading from IP20 to IP25<br />
	<br /><br />
		
	<a href=?flag=1>Click here to proceed.</a> <br><br>
	
	<br>
	</body></html>

	
	
<? }

intranet_closedb();
 ?>
	