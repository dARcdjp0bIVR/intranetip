<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT.'includes/icalendar.php');
include_once($PATH_WRT_ROOT.'includes/libeclass.php');
intranet_opendb();

$iCal = new icalendar();
$libeclass = new libeclass();
$iCal->Start_Trans();

$sql = "select distinct CalID from CALENDAR_CALENDAR_VIEWER 
		where GroupType = 'C' and 
		(
			GroupID in (
				select course_id from {$eclass_db}.course where CalID is null
			) OR 
			GroupID not in (select course_id from {$eclass_db}.course)
		) ";
$resultSet = $iCal->returnVector($sql);

if (count($resultSet) > 0){
	foreach($resultSet as $CalID){
		$sqlResult['remove_calendar_'.$CalID] = $iCal->removeCalendar($CalID);
	}
}

#insert class calendar
$sql ="Select course_id, course_name, course_desc from {$eclass_db}.course where 
	CalID is null and RoomType=0"; 
$result = $iCal->returnArray($sql);
if (count($result)>0){
	foreach ($result as $r){
		$calID = $iCal->createSystemCalendar($r["course_name"],3,"P",$r["course_desc"]);
		$sqlResult["insert_class_Cal_$calID"] = $calID;
		
		/*$sql2 = "update {$eclass_db}.course set CalID = $calID 
				where course_id = ".$r["course_id"];
		$sqlResult["update_course_$calID"] = $iCal->db_db_query($sql2);*/
		
		$dbName = $libeclass->db_prefix."c".$r["course_id"];
		// $sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$dbName'";
		// $existDB = $iCal->db_db_query($sql);
		$sql = "SELECT table_name FROM information_schema.tables WHERE table_schema = '$dbName' AND table_name = 'usermaster'";
		$existDB = $iCal->returnArray($sql);
		if (!empty($existDB)){
		$sql = "
			insert into CALENDAR_CALENDAR_VIEWER 
			(CalID,UserID,Access,Color,GroupID,GroupType)
			select '$calID', i.UserID, 'W', '2f75e9','".$r["course_id"]."','C'
			From {$dbName}.usermaster as u inner join INTRANET_USER as i on
			u.user_email = i.UserEmail 
			where u.status is null";
			$sqlResult["insert_course_viewer_$calID"] = $iCal->db_db_query($sql);
		}
	}
}

#check the event creator when he has no default calendar
$sql = "select u.UserID
		from {$eclass_db}.event as e inner join INTRANET_USER as u on
			e.user_email = u.UserEmail
		where u.UserID not in (select Owner from CALENDAR_CALENDAR)
		";
$eventCreator = $iCal->returnVector($sql);
if (count($eventCreator)==0){
	foreach($eventCreator as $user){
		$UserID = $user;
		$iCal->insertMyCalendar();
	}
}
#insert original eclass event
$sql = "select e.title,e.notes,
		e.share_type,
		c.CalID,
		e.is_important,e.is_allday,e.inputdate,
		e.durMi,e.durHr,e.share_type,e.eventdate,
		e.last_modified, e.link, u.UserID
		from {$eclass_db}.event as e 
		inner join {$eclass_db}.course as c on
			e.course_id = c.course_id 
		inner join INTRANET_USER as u on
			e.user_email = u.UserEmail
		";
$result = $iCal->returnArray($sql);

if (count($result) > 0){
	$calIDset ="";
	$sql = "insert into CALENDAR_EVENT_ENTRY 
	(UserID,CalID,EventDate,InputDate,ModifiedDate,Duration,IsImportant,IsAllDay
	,Access,Title,Description,Url) values ";
	foreach($result as $r){
		$calID = $r["CalID"];
		if ($r['share_type']==0){
			$UserID = $r["UserID"];
			$calID = $iCal->returnOwnerDefaultCalendar();
		}
		$duration = $r["is_allday"]?1440:$r["durMi"]+$r["durHr"]*60;
		$sql .="('".$r["UserID"]."','".$calID."','".$r["eventdate"]."',
				'".$r["inputdate"]."','".$r["last_modified"]."','$duration',
				'".$r["is_important"]."','".$r["is_allday"]."','P',
				'".$r["title"]."','".$r["notes"]."','".$r["link"]."'
				), ";
		$calIDset .= $calID.",";
	}	
	$sql = rtrim($sql,", ");
	//$sqlResult["insert_Event"] =$iCal->db_db_query($sql);
	$calIDset = rtrim($calIDset,", ");
	$sql = "update CALENDAR_EVENT_ENTRY set 
			UID = concat(UserID,'-',CalID,'-',EventID,'@eclass')
			where CalID in ($calIDset) and UID is NULL;
			";
	//$sqlResult["update_Event"] =$iCal->db_db_query($sql);
}
 
if (!in_array(false,$sqlResult)){
	$iCal->Commit_Trans();
	echo "update successful";
	debug_r($sqlResult);
}
else{
	$iCal->RollBack_Trans();
	echo "update failure";
}

intranet_closedb();
?>