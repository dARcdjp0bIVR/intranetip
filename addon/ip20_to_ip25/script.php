<?
# using: 

#############################################################################################
#############################################################################################
#############################################################################################
## Do not run the script in Dev site (Since the data in dev site is updated already)
#############################################################################################
#############################################################################################
#############################################################################################

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($eclass_filepath.'/src/includes/php/lib-filesystem.php');
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libwordtemplates.php");

intranet_opendb();

$li = new libdb();


### Record the setup date
# load history file
$history_file = $PATH_WRT_ROOT."file/ip20_to_ip25_script_update_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = split("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip) = split(",", $tmp_arr[sizeof($tmp_arr)-1]);
	
	if ($sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0]>$last_schema_date)
	{
		$is_new_available = true;
	}
	$new_update = ($is_new_available) ? "Yes" : "No";
} 
else
{
	$new_update = "-";
	$last_update = "-";
	$last_update_ip = "-";
}
$update_count = 0;

/* update intranetIP schema */
$after_date = ($flag==2 && $last_schema_date!="") ? $last_schema_date : "";
//updateSchema($li, $sql_eClassIP_update, $intranet_db, $after_date);

# add update info (latest schema date, time of update, IP of the client)
$time_now = date("Y-m-d H:i:s");
$client_ip = getenv("REMOTE_ADDR");
if (trim($client_ip)=="")
{
	$client_ip = $_SERVER["REMOTE_ADDR"];
}
$history_content .= ($history_content!="") ? "\n" : "";
$history_content .= "$last_schema_date,$time_now,$client_ip";

$lf = new phpduoFileSystem();
$lf->writeFile($history_content, $history_file);


$WordArr['Done'] = '<font color="blue"><b>Done</b></font>';
$WordArr['Failed'] = '<font color="red"><b>Failed</b></font>';

# DB Settings
//$thisDB = 'IP25_TEMP';	//for testing only
$thisDB = $intranet_db;



###################################################
### Clone ASSESSMENT_SUBJECT from eClass to IP DB
###################################################
echo "================ Clone Subject ================<br />";
// Disable the auto-increment of table YEAR
$sql = "Alter table $thisDB.ASSESSMENT_SUBJECT modify RecordID int(11)";
$success['AutoIncrement']['Disable']['ASSESSMENT_SUBJECT'] = $li->db_db_query($sql);

$get_subject_sql = "SELECT	* FROM $eclass_db.ASSESSMENT_SUBJECT";
$SubjectArr = $li->returnArray($get_subject_sql);
$numOfSubject = count($SubjectArr);

if ($numOfSubject > 0)
{
	$SubjectValueArr = array();
	for ($i=0; $i<$numOfSubject; $i++)
	{
		$RecordID = $SubjectArr[$i]['RecordID'];
		$CODEID = $SubjectArr[$i]['CODEID'];
		$EN_SNAME = $SubjectArr[$i]['EN_SNAME'];
		$CH_SNAME = $SubjectArr[$i]['CH_SNAME'];
		$EN_DES = $SubjectArr[$i]['EN_DES'];
		$CH_DES = $SubjectArr[$i]['CH_DES'];
		$EN_ABBR = $SubjectArr[$i]['EN_ABBR'];
		$CH_ABBR = $SubjectArr[$i]['CH_ABBR'];
		$DisplayOrder = $SubjectArr[$i]['DisplayOrder'];
		$InputDate = $SubjectArr[$i]['InputDate'];
		$ModifiedDate = $SubjectArr[$i]['ModifiedDate'];
		$CMP_CODEID = $SubjectArr[$i]['CMP_CODEID'];
		$RecordStatus = 1;
		
		$SubjectValueArr[] = "( '$RecordID', '".$li->Get_Safe_Sql_Query($CODEID)."', 
								'".$li->Get_Safe_Sql_Query($EN_SNAME)."', '".$li->Get_Safe_Sql_Query($CH_SNAME)."',
								'".$li->Get_Safe_Sql_Query($EN_DES)."', '".$li->Get_Safe_Sql_Query($CH_DES)."',
								'".$li->Get_Safe_Sql_Query($EN_ABBR)."', '".$li->Get_Safe_Sql_Query($CH_ABBR)."',
								'".$li->Get_Safe_Sql_Query($DisplayOrder)."', '".$li->Get_Safe_Sql_Query($InputDate)."',
								'".$li->Get_Safe_Sql_Query($ModifiedDate)."', '".$li->Get_Safe_Sql_Query($CMP_CODEID)."',
								'$RecordStatus') ";
	}
	$SubjectValueList = implode(',', $SubjectValueArr);
	
	$insert_subject_sql = "
			Insert Into
					$thisDB.ASSESSMENT_SUBJECT
					(RecordID, CODEID, EN_SNAME, CH_SNAME, EN_DES, CH_DES, EN_ABBR, CH_ABBR, DisplayOrder, InputDate, ModifiedDate, CMP_CODEID, RecordStatus)
					Values
					$SubjectValueList
			";
	$success['Insert']['Subject'] = $li->db_db_query($insert_subject_sql);
	
	if ($success['Insert']['Subject'])
	{
		echo "Insert Subject ".$WordArr['Done'].".<br />";
	}
	else
	{
		// Enable the auto-increment of table YEAR_CLASS
		$sql = "Alter table $thisDB.ASSESSMENT_SUBJECT modify column RecordID int(11) auto_increment";
		$success['AutoIncrement']['Enable']['ASSESSMENT_SUBJECT'] = $li->db_db_query($sql);

		echo "Insert Subject ".$WordArr['Failed'].".<br />";
	}
}

// Enable the auto-increment of table YEAR_CLASS
$sql = "Alter table $thisDB.ASSESSMENT_SUBJECT modify column RecordID int(11) auto_increment";
$success['AutoIncrement']['Enable']['ASSESSMENT_SUBJECT'] = $li->db_db_query($sql);
echo "============== End of Clone Subject =============<br /><br />";

###################################################
### End of Clone ASSESSMENT_SUBJECT from eClass to IP DB
###################################################


###################################################
### Academic Year and Term Setup
###################################################

$AcademicYearEn = stripslashes($_POST['AcademicYearEn']);
$AcademicYearCh = stripslashes($_POST['AcademicYearCh']);
$YearTermArr = unserialize(rawurldecode($YearTermArr));
$numOfYearTerm = count($YearTermArr);

echo "================ Insert Academic Year ================<br />";
# Insert Academic Year
$insert_academic_year_sql = "
		INSERT INTO 
			$thisDB.ACADEMIC_YEAR 
			(YearNameEN, YearNameB5, Sequence, DateInput, InputBy, DateModified, ModifyBy) 
		VALUES 
			('".$li->Get_Safe_Sql_Query($AcademicYearEn)."', '".$li->Get_Safe_Sql_Query($AcademicYearCh)."', 1, NOW(), NULL, NOW(), NULL)
		";
$success['Insert']['AcademicYear'] = $li->db_db_query($insert_academic_year_sql);

if ($success['Insert']['AcademicYear'])
{
	echo "Insert Academic Year ".$WordArr['Done'].".<br />";
}
else
{
	echo "Insert Academic Year ".$WordArr['Failed'].".<br />";
	die();
}
echo "============== End of Insert Academic Year =============<br /><br />";


# Get the updated academic year id
$insertedAcademicYearID = $li->db_insert_id();

# Insert Academic Year Term
echo "================ Insert Term ================<br />";
for ($i=0; $i<$numOfYearTerm; $i++)
{
	$thisYearTermEn = $YearTermArr[$i]['YearTermEn'];
	$thisYearTermCh = $YearTermArr[$i]['YearTermCh'];
	$thisStartDate = $YearTermArr[$i]['StartDate'];
	$thisEndDate = $YearTermArr[$i]['EndDate'];
	
	$insert_year_term_sql = "
			INSERT INTO 
				$thisDB.ACADEMIC_YEAR_TERM 
				(AcademicYearID,YearTermNameEN,YearTermNameB5,TermStart,TermEnd,DateInput,InputBy,DateModified,ModifyBy) 
			VALUES
				($insertedAcademicYearID,'".$li->Get_Safe_Sql_Query($thisYearTermEn)."','".$li->Get_Safe_Sql_Query($thisYearTermCh)."', '$thisStartDate', '$thisEndDate', NOW(), NULL, NOW(), NULL)
			";
	$success['Insert']['YearTerm'][$i] = $li->db_db_query($insert_year_term_sql);
	
	if ($success['Insert']['YearTerm'][$i])
	{
		echo "Insert Term '".$thisYearTermEn."' ".$WordArr['Done'].".<br />";
	}
	else
	{
		echo "Insert Term '".$thisYearTermEn."' ".$WordArr['Failed'].".<br />";
		die();
	}
}
echo "============== End of Insert Term =============<br /><br />";
###################################################
### End of Academic Year and Term Setup
###################################################


###################################################
### Form, Class, Class Teacher and Class Students Setup
###################################################

## Form
echo "================ Insert Form ================<br />";
// Disable the auto-increment of table YEAR
$sql = "Alter table $thisDB.YEAR modify column YearID int(8)";
$success['AutoIncrement']['Disable']['YEAR'] = $li->db_db_query($sql);

// Get the Form from IP20
$sql = "Select * From $thisDB.INTRANET_CLASSLEVEL Where RecordStatus = 1 ORDER BY LevelName";
$ClassLevelArr = $li->returnArray($sql);
$numOfClassLevel = count($ClassLevelArr);

$ClassLevelValueArr = array();
if ($numOfClassLevel > 0)
{
	for ($i=0; $i<$numOfClassLevel; $i++)
	{
		$thisClassLevelID = $ClassLevelArr[$i]['ClassLevelID'];
		$thisLevelName = $ClassLevelArr[$i]['LevelName'];
		$thisWebSAMSLevel = $ClassLevelArr[$i]['WebSAMSLevel'];
		$thisSequence = $i + 1;
		
		$ClassLevelValueArr[] = "( '$thisClassLevelID', '".$li->Get_Safe_Sql_Query($thisLevelName)."', '".$li->Get_Safe_Sql_Query($thisWebSAMSLevel)."', '$thisSequence', NULL, NULL, NULL, NULL )";
	}
	
	$ClassLevelValueList = implode(',', $ClassLevelValueArr);
	
	$insert_classlevel_sql = "
			Insert Into
					$thisDB.YEAR
					(YearID, YearName, WEBSAMSCode, Sequence, DateInput, InputBy, DateModified, ModifyBy)
					Values
					$ClassLevelValueList
			";
	$success['Insert']['Form'] = $li->db_db_query($insert_classlevel_sql);
	
	if ($success['Insert']['Form'])
	{
		echo "Insert Form ".$WordArr['Done'].".<br />";
	}
	else
	{
		// Enable the auto-increment of table YEAR
		$sql = "Alter table $thisDB.YEAR modify column YearID int(8) auto_increment";
		$success['AutoIncrement']['Enable']['YEAR'] = $li->db_db_query($sql);

		echo "Insert Form ".$WordArr['Failed'].".<br />";
		die();
	}
}
else
{
	echo "No form record at this moment<br />";
}

// Enable the auto-increment of table YEAR
$sql = "Alter table $thisDB.YEAR modify column YearID int(8) auto_increment";
$success['AutoIncrement']['Enable']['YEAR'] = $li->db_db_query($sql);
echo "============== End of Insert Form =============<br /><br />";


## Class
echo "================ Insert Class ================<br />";
// Disable the auto-increment of table YEAR_CLASS
$sql = "Alter table $thisDB.YEAR_CLASS modify column YearClassID int(8)";
$success['AutoIncrement']['Disable']['YEAR_CLASS'] = $li->db_db_query($sql);

// Get the Class from IP20
$sql = "Select * From $thisDB.INTRANET_CLASS Where RecordStatus = 1 ORDER BY ClassName";
$ClassArr = $li->returnArray($sql);
$numOfClass = count($ClassArr);

$ClassValueArr = array();
if ($numOfClass > 0)
{
	for ($i=0; $i<$numOfClass; $i++)
	{
		$thisClassID = $ClassArr[$i]['ClassID'];
		$thisClassName = $ClassArr[$i]['ClassName'];
		$thisClassLevelID = $ClassArr[$i]['ClassLevelID'];
		$thisGroupID = $ClassArr[$i]['GroupID'];
		$thisWebSAMSClassCode = $ClassArr[$i]['WebSAMSClassCode'];
		$thisSequence = $i + 1;
		
		$ClassValueArr[] = "( '$thisClassID', '$insertedAcademicYearID', '".$li->Get_Safe_Sql_Query($thisClassName)."', '".$li->Get_Safe_Sql_Query($thisClassName)."', '$thisClassLevelID', '$thisGroupID', '$thisWebSAMSClassCode', '$thisSequence', NULL, NULL, NULL, NULL )";
	}
	
	$ClassValueList = implode(',', $ClassValueArr);
	
	$insert_class_sql = "
			Insert Into
					$thisDB.YEAR_CLASS
					(YearClassID, AcademicYearID, ClassTitleEN, ClassTitleB5, YearID, GroupID, WEBSAMSCode, Sequence, DateInput, InputBy, DateModified, ModifyBy)
					Values
					$ClassValueList
			";
	$success['Insert']['Class'] = $li->db_db_query($insert_class_sql);
	
	if ($success['Insert']['Class'])
	{
		echo "Insert Class ".$WordArr['Done'].".<br />";
	}
	else
	{
		// Enable the auto-increment of table YEAR_CLASS
		$sql = "Alter table $thisDB.YEAR_CLASS modify column YearClassID int(8) auto_increment";
		$success['AutoIncrement']['Enable']['YEAR_CLASS'] = $li->db_db_query($sql);

		echo "Insert Class ".$WordArr['Failed'].".<br />";
		die();
	}
}
else
{
	echo "No class record at this moment<br />";
}

// Enable the auto-increment of table YEAR_CLASS
$sql = "Alter table $thisDB.YEAR_CLASS modify column YearClassID int(8) auto_increment";
$success['AutoIncrement']['Enable']['YEAR_CLASS'] = $li->db_db_query($sql);
echo "============== End of Insert Class =============<br /><br />";


## Class Teacher
echo "================ Insert Class Teacher ================<br />";
// Disable the auto-increment of table YEAR_CLASS_TEACHER
$sql = "Alter table $thisDB.YEAR_CLASS_TEACHER modify column YearClassTeacherID int(8)";
$success['AutoIncrement']['Disable']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql);

// Get the Class Teachers from IP20
$sql = "SELECT * FROM $thisDB.INTRANET_CLASSTEACHER";
$ClassTeacherArr = $li->returnArray($sql);
$numOfClassTeacher = count($ClassTeacherArr);

$ClassTeacherValueArr = array();
if ($numOfClassTeacher > 0)
{
	for ($i=0; $i<$numOfClassTeacher; $i++)
	{
		$thisClassTeacherID = $ClassTeacherArr[$i]['ClassTeacherID'];
		$thisUserID = $ClassTeacherArr[$i]['UserID'];
		$thisClassID = $ClassTeacherArr[$i]['ClassID'];
		
		$ClassTeacherValueArr[] = "( '$thisClassTeacherID', '$thisUserID', '$thisClassID', NULL, NULL, NULL, NULL )";
	}
	
	$ClassTeacherValueList = implode(',', $ClassTeacherValueArr);
	
	$insert_class_teacher_sql = "
			Insert Into
					$thisDB.YEAR_CLASS_TEACHER
					(YearClassTeacherID, UserID, YearClassID, DateInput, InputBy, DateModified, ModifyBy)
					Values
					$ClassTeacherValueList
			";
	$success['Insert']['Class_Teacher'] = $li->db_db_query($insert_class_teacher_sql);
	
	if ($success['Insert']['Class_Teacher'])
	{
		echo "Insert Class Teacher ".$WordArr['Done'].".<br />";
	}
	else
	{
		// Enable the auto-increment of table YEAR_CLASS_TEACHER
		$sql = "Alter table $thisDB.YEAR_CLASS_TEACHER modify column YearClassTeacherID int(8) auto_increment";
		$success['AutoIncrement']['Enable']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql);

		echo "Insert Class Teacher ".$WordArr['Failed'].".<br />";
		die();
	}
}
else
{
	echo "No class teacher record at this moment<br />";
}

// Enable the auto-increment of table YEAR_CLASS_TEACHER
$sql = "Alter table $thisDB.YEAR_CLASS_TEACHER modify column YearClassTeacherID int(8) auto_increment";
$success['AutoIncrement']['Enable']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql);
echo "============== End of Insert Class Teacher =============<br /><br />";


## Class Student
echo "================ Insert Class Student ================<br />";
// Get all classes in IP25
$sql = "Select * From $thisDB.YEAR_CLASS ORDER BY Sequence";
$ClassArr = $li->returnArray($sql);
$numOfClass = count($ClassArr);

if ($numOfClass > 0)
{
	for ($i=0; $i<$numOfClass; $i++)
	{
		$thisYearClassID = $ClassArr[$i]['YearClassID'];
		$thisClassName = $ClassArr[$i]['ClassTitleEN'];
		
		# Get Student List of the Class in IP20
		$sql = "SELECT UserID, ClassNumber FROM $thisDB.INTRANET_USER WHERE ClassName = '$thisClassName'";
		$ClassStudentArr = $li->returnArray($sql);
		$numOfClassStudent = count($ClassStudentArr);
		
		if ($numOfClassStudent > 0)
		{
			$ClassStudentValueArr = array();
			for ($j=0; $j<$numOfClassStudent; $j++)
			{
				$thisUserID = $ClassStudentArr[$j]['UserID'];
				$thisClassNumber = $ClassStudentArr[$j]['ClassNumber'];
				
				$ClassStudentValueArr[] = "( '$thisYearClassID', '$thisUserID', '$thisClassNumber', NULL, NULL, NULL, NULL )";
			}
			
			$ClassStudentValueList = implode(',', $ClassStudentValueArr);
			$insert_class_student_sql = "
					Insert Into
							$thisDB.YEAR_CLASS_USER
							(YearClassID, UserID, ClassNumber, DateInput, InputBy, DateModified, ModifyBy)
							Values
							$ClassStudentValueList
					";
			$success['Insert']['Class_Student'][$thisClassName.'_'.$thisYearClassID] = $li->db_db_query($insert_class_student_sql);
			
			if ($success['Insert']['Class_Student'][$thisClassName.'_'.$thisYearClassID])
			{
				echo "Insert Class Student in ".$thisClassName." ".$WordArr['Done'].".<br />";
			}
			else
			{
				// Enable the auto-increment of table YEAR_CLASS_TEACHER
				$sql = "Alter table $thisDB.YEAR_CLASS_TEACHER modify column YearClassTeacherID int(8) auto_increment";
				$success['AutoIncrement']['Enable']['YEAR_CLASS_TEACHER'] = $li->db_db_query($sql);
		
				echo "Insert Class Student in ".$thisClassName." ".$WordArr['Failed'].".<br />";
			}
		}
		else
		{
			echo "Class ".$thisClassName." has no student.<br />";
		}
	}
}
else
{
	echo "No class record at this moment. So, no class student is added<br /><br />";
}

echo "============== End of Insert Class Student =============<br /><br />";

###################################################
### End of Form, Class, Class Teacher and Class Students Setup
###################################################


#################################################################################################################################
### eHomework																													#
### Transfer the settings data from 																							#
###	"homework_disable.txt", "homework_export.txt", "homework_nonteaching.txt", "homework_parent.txt",							#
### "homework_past.txt", "homework_startdate.txt", "homework_leader.txt", "homework_bysubject.txt", "homework_byteacher.txt"	#
### to table "GENERAL_SETTING"																									#
#################################################################################################################################

	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	
	$lgs = new libgeneralsettings();
	
	$eHomework_Module = "eHomework";
	
	$setting_file = array();
	$setting_file[0] = "$intranet_root/file/homework_disable.txt";
	$setting_file[1] = "$intranet_root/file/homework_export.txt";
	$setting_file[2] = "$intranet_root/file/homework_nonteaching.txt";
	$setting_file[3] = "$intranet_root/file/homework_parent.txt";
	$setting_file[4] = "$intranet_root/file/homework_past.txt";
	$setting_file[5] = "$intranet_root/file/homework_startdate.txt";
	$setting_file[6] = "$intranet_root/file/homework_leader.txt";
	$setting_file[7] = "$intranet_root/file/homework_bysubject.txt";
	$setting_file[8] = "$intranet_root/file/homework_byteacher.txt";
	
	$setting_Name = array();
	$setting_Name[0] = "disabled";
	$setting_Name[1] = "exportAllowed";
	$setting_Name[2] = "nonteachingAllowed";
	$setting_Name[3] = "parentAllowed";
	$setting_Name[4] = "pastInputAllowed";
	$setting_Name[5] = "startFixed";
	$setting_Name[6] = "subjectLeaderAllowed";
	$setting_Name[7] = "subjectSearchDisabled";
	$setting_Name[8] = "teacherSearchDisabled";
	
	$file_Name = array();
	$file_Name[0] = "homework_disable.txt";
	$file_Name[1] = "homework_export.txt";
	$file_Name[2] = "homework_nonteaching.txt";
	$file_Name[3] = "homework_parent.txt";
	$file_Name[4] = "homework_past.txt";
	$file_Name[5] = "homework_startdate.txt";
	$file_Name[6] = "homework_leader.txt";
	$file_Name[7] = "homework_bysubject.txt";
	$file_Name[8] = "homework_byteacher.txt";
	
	for($i=0; $i<9; $i++){
		if (is_file($setting_file[$i])){
			
			$file_content = get_file_content($setting_file[$i]);
			$dataValue = explode("\n",$file_content);
						
			list($value) = $dataValue;
			if ($value=="") $value = "0";
			
			$data = array($setting_Name[$i]=>$value);
			
			$setting = "'".$setting_Name[$i]."'";
			$settingName = array($setting);
			
			# check if there is already containds settings data, if yes, no need to transfer
			$tmp_ary = $lgs->Get_General_Setting($eHomework_Module, $settingName);
			
			if(empty($tmp_ary))
			{
				
				$lgs->Save_General_Setting($eHomework_Module, $data);
				echo "eHomework...Transfer the settings data from \"".$file_Name[$i]."\" to table \"GENERAL_SETTING\"... ".$WordArr['Done']."<br>";			  
			}
			else
			{
				echo "eHomework...Data already transferred<br>";
			}
		}
		else
		{
			echo "eHomework...No \"".$file_Name[$i]."\"<br>";	
		}
	}

#################################################################################################################################
### eHomework [End]																												#	
#################################################################################################################################


###################################################
### eCircular
### Transfer the settings data from "settings_circular.txt" to table "GENERAL_SETTING"
###################################################
if($special_feature['circular'])
{
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$lgs = new libgeneralsettings();
	
	$Circular_Module = "CIRCULAR";
	
	$setting_file = "$intranet_root/file/settings_circular.txt";
	if (is_file($setting_file))
	{
		$file_content = get_file_content($setting_file);
		$data = explode("\n",$file_content);
		$ary = array();
		list($ary['disabled'],
			$ary['isHelpSignAllow'],
			$ary['isLateSignAllow'],
			$ary['isResignAllow'],
			$ary['defaultNumDays'],
			$ary['showAllEnabled']) = $data;
		if ($ary['defaultNumDays']=="") $ary['defaultNumDays'] = 7;
		
		# check if there is already containds settings data, if yes, no need to transfer
		$tmp_ary = $lgs->Get_General_Setting($Circular_Module);
		
		if(empty($tmp_ary))
		{
			$lgs->Save_General_Setting($Circular_Module, $ary);
			echo "eCircular...Transfer the settings data from \"settings_circular.txt\" to table \"GENERAL_SETTING\"... ".$WordArr['Done']."<br>";
		}
		else
		{
			echo "eCircular...Data already transferred<br>";
		}
	}
	else
	{
		echo "eCircular...No \"settings_circular.txt\"<br>";	
	}
}
###################################################
### eCircular [End]
###################################################

###################################################
### eNotice
### Transfer the settings data from "settings_notice.txt" to table "GENERAL_SETTING"
###################################################
if($plugin['notice'])
{
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$lgs = new libgeneralsettings();
	
	$Notice_Module = "eNotice";
	
	$setting_file = "$intranet_root/file/settings_notice.txt";
	if (is_file($setting_file))
	{
		$file_content = get_file_content($setting_file);
		$data = explode("\n",$file_content);
		$ary = array();
		
		list($ary['disabled'],
			$ary['fullAccessGroupID'],
			$ary['normalAccessGroupID'],
			$ary['isClassTeacherEditDisabled'],
			$ary['isAllAllowed'],
			$ary['defaultNumDays'],
			$ary['showAllEnabled'],
			$ary['DisciplineGroupID']) = $data;
			
		if ($ary['defaultNumDays']=="") $ary['defaultNumDays'] = 4;
		
		# check if there is already containds settings data, if yes, no need to transfer
		$tmp_ary = $lgs->Get_General_Setting($Notice_Module);
		
		if(empty($tmp_ary))
		{
			$lgs->Save_General_Setting($Notice_Module, $ary);
			echo "eNotice...Transfer the settings data from \"settings_notice.txt\" to table \"GENERAL_SETTING\"... ".$WordArr['Done']."<br>";
		}
		else
		{
			echo "eNotice...Data already transferred<br>";
		}
	}
	else
	{
		echo "eNotice...No \"settings_notice.txt\"<br>";	
	}
}
###################################################
### eNotice [End]
###################################################

###################################################
### Student Attendance
### Transfer the settings data to table "GENERAL_SETTING"
###################################################
if($plugin['attendancestudent'])
{
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$lgs = new libgeneralsettings();

	$content_basic = trim(get_file_content("$intranet_root/file/stattend_basic.txt"));
	$SettingList['AttendanceMode'] = $content_basic;
	$time_table = trim(get_file_content("$intranet_root/file/time_table_mode.txt"));
	$SettingList['TimeTableMode'] = $time_table; 	### 0 - Time Slot, 1 - Time Session
	$SettingList['DisallowProfileInput'] = trim(get_file_content("$intranet_root/file/disallow_student_profile_input.txt"));
	$SettingList['DefaultAttendanceStatus'] = trim(get_file_content("$intranet_root/file/studentattend_default_status.txt"));
	$SettingList['ProfileAttendCount'] = trim(get_file_content("$intranet_root/file/std_profile_attend_count.txt"));
	$SettingList['TerminalIP'] = get_file_content("$intranet_root/file/stattend_ip.txt");
	$SettingList['IgnorePeriod'] = get_file_content("$intranet_root/file/stattend_ignore.txt");
	
	$lgs->Save_General_Setting('StudentAttendance', $SettingList);
	
	echo "Student Attendance...Setting already transferred<br>";
}
###################################################
### Student Attendance [End]
###################################################

###################################################
### ePayment
### Transfer the settings data to table "GENERAL_SETTING"
###################################################
if($plugin['payment'])
{
	include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
	$lgs = new libgeneralsettings();

	$PaymentLetterSetting['PaymentLetterHeader'] = trim(get_file_content("$intranet_root/file/pay_out_letter_head.txt"));
	$PaymentLetterSetting['PaymentLetterHeader1'] = trim(get_file_content("$intranet_root/file/pay_out_letter_head2.txt"));
	$PaymentLetterSetting['PaymentLetterFooter'] = trim(get_file_content("$intranet_root/file/pay_out_letter_foot.txt"));
	
	//$letter_signature_option_file = "$intranet_root/file/pay_out_letter_signature_option.txt";
	
	// pps
	$pps_charge_from_file = trim(get_file_content("$intranet_root/file/pps_charge_deduction.txt"));

	$temp =  explode("\n",$pps_charge_from_file);
	$SettingList['PPSChargeEnabled'] = $temp[0];
	$SettingList['PPSIEPSCharge'] = $temp[1];
	$SettingList['PPSCounterBillCharge'] = $temp[2];

	$SettingList['TerminalIPList'] = get_file_content("$intranet_root/file/payment_ip.txt");
	$SettingList['ConnectionTimeout'] = trim(get_file_content("$intranet_root/file/payment_expiry.txt"));
	$SettingList['ConnectionTimeout'] = ($SettingList['ConnectionTimeout']=="" || !is_numeric($SettingList['ConnectionTimeout']))? 60:$SettingList['ConnectionTimeout'];
	
	$authfile = get_file_content("$intranet_root/file/payment_auth.txt");
	$auth = explode("\n",$authfile);
	$SettingList['PaymentNoAuth'] = ($auth[0]==1);
	$SettingList['PurchaseNoAuth'] = ($auth[1]==1);

	$lgs->Save_General_Setting('ePayment', $SettingList);
	
	echo "ePayment...Setting already transferred<br>";
}
###################################################
### ePayment [End]
###################################################

###################################################
### Group [Start]
###################################################
//$Current_Academic_Year_ID = Get_Current_Academic_Year_ID();
$Current_Academic_Year_ID = $insertedAcademicYearID;
if($Current_Academic_Year_ID)
{
	$sql = "update INTRANET_GROUP set AcademicYearID=$Current_Academic_Year_ID where AcademicYearID is NULL";
	$li->db_db_query($sql) or die(mysql_error());
	//echo "All the Group's academic year id has set to the current academic year id<br>";	
	echo "All the Group's academic year id has set to the preset academic year id<br>";	
}
###################################################
### Group [End]
###################################################


###################################################
### School Settings (Location)
### Add Building Table and insert default value for building, floor and room
###################################################

# Run this script once only!
if ($last_update == '-')
{
	echo "============== Location Start ==============<br />";

	# 1. Create Building Table (Ensure the table is created before adding the default value)
	$create_building_sql = 
	"CREATE TABLE IF NOT EXISTS INVENTORY_LOCATION_BUILDING (
			BuildingID int(11) NOT NULL auto_increment,
			Code varchar(10),
			NameChi varchar(255),
			NameEng varchar(255),
			DisplayOrder int(11),
			RecordType int(11),
			RecordStatus int(11),
			DateInput datetime,
			DateModified datetime,
			PRIMARY KEY (BuildingID),
			INDEX BuildingID (BuildingID),
			INDEX InventoryBuildingNameChi (NameChi),
			INDEX InventoryBuildingNameEng (NameEng),
			UNIQUE ChineseName (NameChi),
			UNIQUE EnglishName (NameEng),
			UNIQUE BuildingIDWithCode (BuildingID, Code)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8
	";
	$li->db_db_query($create_building_sql) or die(mysql_error());
	echo "Building Table has been created successfully<br />";
	
	
	# 2. Insert a default building
	$default_building_sql = 
	"INSERT IGNORE INTO INVENTORY_LOCATION_BUILDING 
		(Code, NameChi, NameEng, DisplayOrder, RecordType, RecordStatus, DateInput, DateModified)
	VALUES
		('MB', '�D�j��', 'Main Building', 1, NULL, NULL, now(), now());
	";
	$li->db_db_query($default_building_sql) or die(mysql_error());
	echo "Default Building has been created successfully<br />";
	
	# 3. Activate all floors
	$activate_floor_sql = "UPDATE INVENTORY_LOCATION_LEVEL SET RecordStatus = '1';";
	$li->db_db_query($activate_floor_sql) or die(mysql_error());
	echo "All floors have been activated successfully<br />";
	
	
	# 4. Activate all rooms
	$activate_room_sql = "UPDATE INVENTORY_LOCATION SET RecordStatus = '1';";
	$li->db_db_query($activate_room_sql) or die(mysql_error());
	echo "All rooms have been activated successfully<br />";
	
	# 5. Assign floor, which has not assigned to any buildings yet, to the default building
	$assign_floor_to_default_building_sql = "UPDATE INVENTORY_LOCATION_LEVEL SET BuildingID = 1 WHERE BuildingID IS NULL;";
	$li->db_db_query($assign_floor_to_default_building_sql) or die(mysql_error());
	echo "Assign floors to the default building successfully<br />";
	
	
	echo "=============== Location End ===============<br /><br />";
}

###################################################
### Location [End] 
###################################################


####################################################
### Campus Link
###################################################
echo "Update Campus link ... <br>";
$sql = "Create Table if not exists INTRANET_CAMPUS_LINK (
  LinkID int(8) NOT NULL auto_increment,
  Title varchar(255) default NULL,
  URL varchar(255) default NULL,
  DisplayOrder int(11) default NULL,
  Description text,
  DateInput datetime default NULL,
  InputBy int(11) default NULL,
  DateModified datetime default NULL,
  ModifiedBy int(11) default NULL,
  PRIMARY KEY  (LinkID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8";

$li->db_db_query($sql);

$lf = new libfilesystem();
$links = explode("\n", $lf->file_read($intranet_root."/file/campuslink.txt"));
$num = sizeof($links)/2;
$max = max($num+5, 10);

$sql = "select max(DisplayOrder) from INTRANET_CAMPUS_LINK";
$resultSet = $li->returnVector($sql);
$order = (empty($resultSet)||empty($resultSet[0]))?0:$resultSet[0];

$sql = "insert into INTRANET_CAMPUS_LINK (Title, URL, DateInput, DateModified,DisplayOrder) values ";

$cnt = 0;
for($i=0; $i<$max; $i++) {
	if (empty($links[(2*$i)]) || empty($links[(2*$i+1)]))
		continue;
	if ($cnt > 0)
		$sql .= ", ";	
	$sql .= "('".$links[(2*$i)]."','".$links[(2*$i)+1]."', now(), now(), '".($order+1)."')";
	$order++;
	$cnt++;
} 
$li->db_db_query($sql);

echo $cnt." records are updated. <br>";

#################################################

####################################################
### inventory
####################################################
include_once($PATH_WRT_ROOT."includes/libinventory.php");
$linventory	= new libinventory();

$sql = "SELECT 
				a.ItemID, a.LocationiD, a.FundingSource 
		FROM 
				TEMP_INVENTORY_ITEM_BULK_FUNDING_LOG AS a INNER JOIN 
				INVENTORY_ITEM_BULK_LOCATION AS b ON (a.ItemID = b.ItemID and a.Locationid = b.LocationID)
		";
$arr_result = $linventory->returnArray($sql,3);

if(sizeof($arr_result)>0){
		for($i =0; $i<sizeof($arr_result); $i++){
				list($item_id, $location_id, $funding_source_id) = $arr_result[$i];
				
				$sql = "UPDATE 
							INVENTORY_ITEM_BULK_LOCATION 
						SET 
							FundingSourceID = $funding_source_id
						WHERE
							ItemID = $item_id AND
							LocationID = $location_id
						";
				$result[] = $linventory->db_db_query($sql);
		}
}

if(!in_array(false,$result)){
	echo "Update Iinventory Bult item funding Successfully";
}else{
	echo "Update Iinventory Bult item funding Falied";
}

###################################################
### Change file content in "Preset Wordings" [Start]
###################################################
$lf = new libwordtemplates();
$base_dir = "$intranet_root/file/templates";
$files = $lf->return_folderlist($base_dir);
if(!empty($files))
{
	foreach($files as $k=>$file_target)
	{
		$file_content = get_file_content($file_target);
		
		if(!empty($file_content))
		{
			$cur_encoding = mb_detect_encoding($file_content) ; 
			if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
			{
				$new_content = Big5ToUnicode($file_content);
				$lf->file_write($new_content,"$file_target");
				echo $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>"; 
			}
			else
			{
				echo $file_target .".... no need to convert to utf-8.<br>";
			}
		}
		else
		{
			echo $file_target .".... file empty.<br>"; 
		}
	}
}
echo "Preset Wordings checked complete.<br>";
###################################################
### Change file content in "Present Wordings" [End]
###################################################

###################################################
### Change file content in Campus TV schedule
###################################################
include_once($PATH_WRT_ROOT."includes/libcampustv.php");
$lcampustv = new libcampustv();

$file_target = $lcampustv->programme_file;
$file_content = get_file_content($file_target);

if(!empty($file_content))
{
	$cur_encoding = mb_detect_encoding($file_content) ; 
	if(!($cur_encoding == "UTF-8" && mb_check_encoding($in_str,"UTF-8")))
	{
		$new_content = Big5ToUnicode($file_content);
		$lf->file_write($new_content,"$file_target");
		echo $file_target .".... file content convert to utf-8 <font color='blue'>successfully</font>.<br>"; 
	}
	else
	{
		echo $file_target .".... no need to convert to utf-8.<br>";
	}
}
else
{
	echo $file_target .".... file empty.<br>"; 
}
###################################################
### Change file content in Campus TV schedule [End]
###################################################



### iMail ###
//$sql = "INSERT IGNORE INTO INTRANET_CAMPUSMAIL_FOLDER (FolderID, OwnerID, FolderName, RecordType, RecordStatus, DateInput, DateModified) VALUES (0,NULL,'Outbox',0,NULL,NOW(),NOW()), (1,NULL,'Draft',0,NULL,NOW(),NOW()), (2,NULL,'Inbox',0,NULL,NOW(),NOW())";
//$result = $li->db_db_query($sql);
//if($result)
//{
//	echo "iMail - Create default folder (Outbox, Draft, Inbox) is success. <br>";
//}else{
//	echo "iMail - Create default folder (Outbox, Draft, Inbox) is failed. <br>";
//}
### End of iMail ###

/*
###################################################
### eDisciplinev12 [Start] 
### add AcademicYearID, YearTermID
### Moved to script2.php as the script need the complete setup of past years and terms
###################################################
if($plugin['Disciplinev12']) {
	include_once($PATH_WRT_ROOT."includes/libdisciplinev12.php");
	$ldiscipline = new libdisciplinev12();

	# Award & Punishment Record (eDisciplinev12)
	$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_MERIT_RECORD');
	
	if(!is_numeric($leftRecord)) {		# already transferred
		echo "eDisciplinev12...Award & Punishment Records already transferred<br>";
	}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
		echo "eDisciplinev12...Transfer Award & Punishment Records to IP25...".$WordArr['Done']."<br>";
	} else {							# all Award & Punishment data transfer to IP25 successfully
		echo "eDisciplinev12...Transfer Award & Punishment Records to IP25...$leftRecord record(s) cannot be transferred to IP25<br>";
	}
	
	# Good Conduct & Misconduct Record (eDisciplinev12)
	$leftRecord = $ldiscipline->ip20DataToIP25Data('DISCIPLINE_ACCU_RECORD');
	
	if(!is_numeric($leftRecord)) {		# already transferred
		echo "eDisciplinev12...Good Conduct & Misconduct Records already transferred<br>";
	}else if($leftRecord==0) {			# not all Award & Punishment data transfer to IP25 successfully
		echo "eDisciplinev12...Transfer Good Conduct & Misconduct Records to IP25...".$WordArr['Done']."<br>";
	} else {							# all Award & Punishment data transfer to IP25 successfully
		echo "eDisciplinev12...Transfer Good Conduct & Misconduct Records to IP25...$leftRecord record(s) cannot be transferred to IP25<br>";
	}
	
}
###################################################
### eDisciplinev12 [End] 
###################################################
*/

echo "<br /><br />";
echo "<font size='+1' color='blue'><b>The system setup is completed.</b></font><br />";
echo "* To view the old data of eDiscipline1.2, please setup the past school years and terms in School Settings > School Calendar > Academic Year Settings first. ";
echo "And then run script2.php for data patch.<br />";

intranet_closedb();
?>