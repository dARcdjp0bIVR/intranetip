<?php
# using : 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();

$ldb = new libdb();

# load history file
$history_file = $PATH_WRT_ROOT."file/ip20_to_ip25_change_db_charset_history.txt";
$history_content = trim(get_file_content($history_file));
if ($history_content!="")
{
	# find if schema should be updated
	$tmp_arr = split("\n", $history_content);
	list($last_schema_date, $last_update, $last_update_ip) = split(",", $tmp_arr[sizeof($tmp_arr)-1]);
	
	if ($sql_eClassIP_update[sizeof($sql_eClassIP_update)-1][0]>$last_schema_date)
	{
		$is_new_available = true;
	}
	$new_update = ($is_new_available) ? "Yes" : "No";
} 
else
{
	$new_update = "-";
	$last_update = "-";
	$last_update_ip = "-";
}


$sql = "show databases like '%".$intranet_db."_DB_REPORT_CARD%'";
$DatabaseArr = $ldb->returnVector($sql);
$DatabaseArr[] = $intranet_db;

$numOfDatabase = count($DatabaseArr);

for ($j=0; $j<$numOfDatabase; $j++)
{
	$thisDB = $DatabaseArr[$j];
	//echo 'Processing DB: <b>'.$thisDB.'</b><br><br>';
	
	# switch database
	$ldb->db = $thisDB;
	
	# get table list
	$sql = 'show tables';
	$TableList = $ldb->returnVector($sql);
	
	for ($i=0; $i< sizeof($TableList); $i++) {
		//echo 'Processing: '.$TableList[$i].'<br><br>';
		
		$sql = 'alter table '.$thisDB.'.'.$TableList[$i].' TYPE=innoDB;';
		$x .= $sql."<br>\r\n";
		
		$sql = 'alter table '.$thisDB.'.'.$TableList[$i].' CONVERT TO CHARACTER SET utf8;';
		$x .= $sql."<br>\r\n";
		
		//$x .= '-------------------------<br>';
	}
	//echo 'End of Processing DB: <b>'.$thisDB.'</b><br>';
	//echo '===================================================================<br><br>';
}
echo $x;

echo "/* Back to main */";

intranet_closedb();
?>
