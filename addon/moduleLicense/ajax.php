<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."lang/elib_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");

intranet_opendb();
###########################################
#Init Variables
$aryFail = array();
$arySucc = array();

## Init Module
$libIntranetModule = new libIntranetModule();

## Get POST Variables
$action 		= (isset($_REQUEST['action']))? trim($_REQUEST['action']) : "";
switch($action){
	## Update the quota for modules
	case "update_module_quota":
		$aryExtraHeader = array();
		$aryExtraInfo = array();
		
		$aryModuleQuota 	 = (isset($_REQUEST['aryModuleQuota']))? $_REQUEST['aryModuleQuota'] :  "";
		$aryModuleID 	 	 = (isset($_REQUEST['aryModuleID']))? $_REQUEST['aryModuleID'] :  "";
		$aryNumberOfUnit 	 = (isset($_REQUEST['aryNumberOfUnit']))? $_REQUEST['aryNumberOfUnit'] : "";
		$aryNumberOfStudnets = (isset($_REQUEST['aryNumberOfStudnets']))? $_REQUEST['aryNumberOfStudnets'] : "";
		$aryExpiryDate		 = (isset($_REQUEST['aryExpiryDate']))? $_REQUEST['aryExpiryDate'] : "";
		$aryCode			 = (isset($_REQUEST['aryCode']))? $_REQUEST['aryCode'] : "";
		
		foreach($aryModuleID as $i => $ModuleID){
			$_expiryDate = empty($aryExpiryDate[$i])? "":date('Y-m-d', strtotime($aryExpiryDate[$i]))." 23:59:59";
			if(strpos($aryCode[$i],'grammartouch') !== false){
				list($ModuleLicenseID,$ModuleCode) = $libIntranetModule->add_module_quota($ModuleID, $aryModuleQuota[$i], $aryNumberOfUnit[$i], $aryNumberOfStudnets[$i], $_expiryDate, true);	
			}else{
				$ModuleCode = $libIntranetModule->add_module_quota($ModuleID, $aryModuleQuota[$i], $aryNumberOfUnit[$i], $aryNumberOfStudnets[$i], $_expiryDate);
			}
			if($ModuleCode){
				if(strpos($ModuleCode,'grammartouch') !== false){
					$libIntranetModule->add_module_quota_to_grammartouch($ModuleCode, $ModuleLicenseID, $aryModuleQuota[$i], $aryNumberOfUnit[$i], $aryNumberOfStudnets[$i], $_expiryDate);	
				}
				$arySucc[$i] = array(
										$Lang['ModuleLicense']['ModuleCode'] => $ModuleCode,
										'Number Of Unit' => ($aryCode[$i]=='ambook'? $aryNumberOfUnit[$i] : 'N/A'),
										'Number Of Students' => ($aryCode[$i]=='ambook'? $aryNumberOfStudnets[$i] : 'N/A'),
										'Expiry Date'=>(in_array($aryCode[$i],$libIntranetModule->showExpiryDateModuleAry)? $aryExpiryDate[$i] : 'N/A'),
										$Lang['ModuleLicense']['Quota']=>$aryModuleQuota[$i]
									);
			}else{
				$aryFail[$i] = array(
										$Lang['ModuleLicense']['ModuleCode'] => $ModuleCode,
										'Number Of Unit' => ($aryCode[$i]=='ambook'? $aryNumberOfUnit[$i] : 'N/A'),
										'Number Of Students' => ($aryCode[$i]=='ambook'? $aryNumberOfStudnets[$i] : 'N/A'),
										'Expiry Date'=>(in_array($aryCode[$i],$libIntranetModule->showExpiryDateModuleAry)? $aryExpiryDate[$i] : 'N/A'),
										$Lang['ModuleLicense']['Quota']=>$aryModuleQuota[$i]
									);
			}			
		}
		## Echo succ & fail results
		echo $libIntranetModule->gen_result_ui($arySucc, $aryFail,true);
		break;
	default:
		break;
}


###########################################
intranet_closedb();
?>