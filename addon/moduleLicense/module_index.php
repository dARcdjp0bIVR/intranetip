<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."includes/libIntranetModule.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");


## Prompt login
include_once("../check.php");


intranet_opendb();
###########################################
$linterface 	= new interface_html();
$lu 			= new libuser($UserID);
$libIntranetModule = new libIntranetModule();

$search = isset($_REQUEST['search'])? trim($_REQUEST['search']) : "";

$CurrentPageArr['Subjects'] = 1;
$MODULE_OBJ['title'] = "Install License";
$TAGS_OBJ[] = array("Install Student Module License");

if(isset($search)){
	$condition = " WHERE Code like '%".$search."%' OR Description like '%".$search."%'";
	
}

	## Get list of modules with invalid quota
	$strInvalid = "";
	$extra = "";
	$sql = "SELECT ModuleId FROM INTRANET_MODULE_LICENSE WHERE md5(CONCAT(ModuleId,'_',NumberOfLicense,'_',InputDate)) != CheckKey";
	$aryCheck = $libIntranetModule->returnVector($sql);
	if($aryCheck != array() && $aryCheck[0] != ""){		
		$strInvalid = implode(",", $aryCheck);
		$extra = ", IF (ml.ModuleId IN ($strInvalid), '&nbsp;*', '')"; 
		
	}
	$sql = "
			SELECT
				CONCAT('<b>', m.Code , '</b>') as Code,				
				m.Description as Description,
				IF (ml.ModuleID is not NULL , CONCAT('<a href=\"license_history.php?ModuleID=', m.ModuleID,'&TB_iframe=true&amp;height=500&amp;width=600\" class=\"thickbox\">',SUM( ml.NumberOfLicense) $extra, '</a>') , '"."<font color=gray>Disabled</font>"."') as enabled,
				CONCAT('<input type=\"checkbox\" name=\"ModuleID[]\" id=\"ModuleID_',m.ModuleId,'\" value=\"',m.ModuleId,'\" />')					
			FROM
				INTRANET_MODULE as m
			LEFT JOIN 
				INTRANET_MODULE_LICENSE as ml
			ON
				m.ModuleID = ml.ModuleID ".$condition."
			GROUP BY 
			    m.ModuleID
		  ";

$page_size 				= (isset($numPerPage) && $numPerPage != "")? $numPerPage : 20;
$pageSizeChangeEnabled = true;
if($field=="") $field = 1;
$order = ($order == '' || $order != 0) ? 1 : 0;
$li = new libdbtable2007($field, $order, $pageNo);

$li->field_array = array("Code", "Description", "SUM( ml.NumberOfLicense)");



$li->sql = $sql;
//echo htmlspecialchars($li->sql);
$li->IsColOff = "2";
$li->no_col = 5;
$li->column_array = array(12,12,12);

// TABLE COLUMN
$pos = 0;
$li->column_list .= "<td class='tabletop tabletopnolink'>"."#"."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $Lang['ModuleLicense']['ModuleCode'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $Lang['ModuleLicense']['Description'])."</td>\n";
$li->column_list .= "<td class='tabletop tabletopnolink'>".$li->column($pos++, $Lang['ModuleLicense']['AvailableQuota'])."</td>\n";

$li->column_list .= "<td width=1 class='tabletoplink'>".$li->check("ModuleID[]")."</td>\n
";
####################################################

$linterface->LAYOUT_START();

?>
<link href="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$PATH_WRT_ROOT?>templates/jquery/thickbox.js"></script>

<BR />
<BR />
<form name="form1" method="POST">
<table width="90%" border="0" cellspacing="0" cellpadding="0">
<tr><td>
	<input type="text" name="search" id="search" value="<?=$search?>" /><input type="button" name="search" id="search" value="search"  onClick="document.form1.submit();"  />
	<span style="float: right"><a href="#" class="menuon" onclick="window.open('../../home/moduleLicense/module_license.php?ModuleCode=hidden','','scrollbars=yes')">Module License</a></span>
</td></tr>
<tr>
	<td align="right">
		<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td width="21"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_01.gif" width="21" height="23" /></td>
			<td background="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_02.gif">
			<table border="0" cellspacing="0" cellpadding="2">
			<tr>
				<td nowrap>
					<!-- STUDENT LICENSE -->
						<a href="install_quota.php?TB_iframe=true&amp;height=500&amp;width=800" class="thickbox tabletool" title="Modify Quota">
						Modify Quota</a>				
				</td>
			</tr>
			</table>
			</td>
			<td width="6"><img src="<?="{$image_path}/{$LAYOUT_SKIN}"?>/table_tool_03.gif" width="6" height="23" /></td>
		</tr>
		</table>
	</td>
</tr>
<tr>
	<td colspan="2">
		<?=$li->display();?>
	</td>
</tr>
</table>

<input type="hidden" name="field" id="field" value="<?=$field?>" />
<input type="hidden" name="pageNo" id="pageNo" value="<?=$pageNo?>" />
<input type="hidden" name="order" id="order" value="<?=$order?>" />
<input type="hidden" name="page_size_change" id="page_size_change" value="<?=$page_size_change?>" />
<input type="hidden" name="numPerPage" id="numPerPage" value="<?=$numPerPage?>" />
	
	
</form>
<?php

$linterface->LAYOUT_STOP();
###########################################
intranet_closedb();

?>