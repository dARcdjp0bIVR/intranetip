<?php
function getCurPageURL($withQueryString=1, $withPageSuffix=1) {
	 $pageSuffix = ($withQueryString==1)? $_SERVER["REQUEST_URI"] : $_SERVER["SCRIPT_NAME"];
	 $pageURL = 'http';
	 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
	 $pageURL .= "://";
	 if ($_SERVER["SERVER_PORT"] != "80") {
	  //$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
	 } else {
	  //$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	  $pageURL .= $_SERVER["SERVER_NAME"];
	 }
	 
	 if ($withPageSuffix) {
	 	$pageURL .= $pageSuffix;
	 }
	 
	 return $pageURL;
}
?>