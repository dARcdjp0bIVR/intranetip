<?php
//$2 for 5mins
echo exec ("cat /proc/loadavg |  awk '{print $2*100}'");
echo "\n";
//$3 for 15mins
echo exec ("cat /proc/loadavg |  awk '{print $3*100}'");
echo "\n";

//Used memory in Mbytes
echo exec ("free -m | grep \"Mem\" | awk '{print $3}'");
echo "\n";
//Used Swap in Mbytes
echo exec ("free -m | grep \"Swap\" | awk '{print $3}'");
echo "\n";

//Current active process
echo exec ("cat /proc/loadavg |  awk '{print $4}' | cut -d '/' -f 1");
echo "\n";

//total process
echo exec ("cat /proc/loadavg |  awk '{print $4}' | cut -d '/' -f 2");
echo "\n";

//Network In / Out
//network in (Rev)
$rev_byte_5min=(double)exec ("cat /proc/net/dev | grep 'eth0' | awk '{print $1}'  | cut -d ':' -f 2");
$rev_byte=round($rev_byte_5min,0);
echo $rev_byte;
echo "\n";

//network out (Send)
$send_byte_5min=(double)exec ("cat /proc/net/dev | grep 'eth0' | awk '{print $9}'");
$send_byte=round($send_byte_5min,0);
echo $send_byte;
echo "\n";


//eClass Stat
//include intranet config variable here
@include('../../includes/global.php');

// connect to the master and grab its current status
$cnx = @mysql_connect('localhost',$eclass_db_user,$eclass_db_pass);
if (!$cnx) {
    $error_msg .= "Critical: Could not connect Master Server: ". mysql_error()."\n";
}//end if
else
{
	$cnx = mysql_select_db ($intranet_db);
	
	// Query total concurrent user session in 30 minutes (Teacher)
	$sql_str="select count(*) as session from INTRANET_LOGIN_SESSION,INTRANET_USER where INTRANET_LOGIN_SESSION.DateModified >= DATE_SUB(NOW(), INTERVAL 30 MINUTE) AND INTRANET_LOGIN_SESSION.UserID=INTRANET_USER.UserID AND INTRANET_USER.RecordType=1  ORDER BY LoginSessionID DESC LIMIT 1000;";
	//echo $sql_str."<br>";
	$ms2 = mysql_query($sql_str);
	$ms = mysql_fetch_array($ms2);
	echo $ms['session'];
	echo "\n";
	
	// Query total concurrent user session in 30 minutes (Student)
	$sql_str="select count(*) as session from INTRANET_LOGIN_SESSION,INTRANET_USER where INTRANET_LOGIN_SESSION.DateModified >= DATE_SUB(NOW(), INTERVAL 30 MINUTE) AND INTRANET_LOGIN_SESSION.UserID=INTRANET_USER.UserID AND INTRANET_USER.RecordType=2  ORDER BY LoginSessionID DESC LIMIT 1000;";
	//echo $sql_str."<br>";
	$ms2 = mysql_query($sql_str);
	$ms = mysql_fetch_array($ms2);
	echo $ms['session'];
	echo "\n";
	
	
}//end else


?>
