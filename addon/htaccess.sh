#!/bin/sh
# check the argument number
if [ $# -lt 1 ]; then
     echo "Pls enter the path of eclass intranet directory."
     echo "e.g. /home/eclass/intranet"
     echo ""
     exit 127
else
     echo ""
     echo ""
     echo ""
     echo "Working Directory is " $1

     # Remove Exisiting permission setting
     echo "[create htaccess setting - Running]"

     # Write for faked logout admin console
     rm -f $1/logout/.htaccess
     FileName="$1/logout/.htaccess"
     echo "AuthType Basic" >> $FileName
     echo "AuthUserFile $1/logout/.htpasswd" >> $FileName
     echo "AuthGroupFile /dev/null" >> $FileName
     echo "AuthName Admin" >> $FileName
     echo "<Limit GET>" >> $FileName
     echo "require valid-user" >> $FileName
     echo "</Limit>" >> $FileName

     # Global Setting for Permission Files
     HTA1="AuthUserFile      $1/file/.htpasswd"
     HTA2="AuthGroupFile     $1/file/.htgroup"
     HTA3="AuthName     Admin"
     HTA4="AuthType     Basic"
     HTA_Bracket1="<Limit GET POST PUT>"
     HTA_Standard="require user admin"
     HTA_Bracket2="</Limit>"

     # Process Admin directory first
     rm -f $1/admin/.ht*

     # admin/
     FileName="$1/admin/.htaccess"
     echo $HTA1 >> $FileName
     echo $HTA2 >> $FileName
     echo $HTA3 >> $FileName
     echo $HTA4 >> $FileName
     echo $HTA_Bracket1 >> $FileName
     echo "require valid-user" >> $FileName
     echo $HTA_Bracket2 >> $FileName

     for paths in "account" "announcement" "basic_settings" "booking" "booking_periodic" "campuslink" "campusmail" "campusmail_set" "campustv" "cycle" "eclass" "email" "event" "filesystem" "group" "groupbulletin" "groupfiles" "grouplinks" "homework" "language" "motd" "password" "polling" "rbps" "resource" "resource_set" "role" "timetable" "tmpfiles" "url" "user" "library" "wordtemplates" "school_settings" "usage" "teaching" "clubs_enrollment" "enrollment_approval" "groupcategory" "notice" "quota" "qb" "sms" "adminjob" "student_attendance" "student_promotion" "payment" "file_quota" "circular" "profile_set" "student_attendance2" "websams" "discipline"
      do
         rm -f $1/admin/$paths/.ht*

         FileName="$1/admin/$paths/.htaccess"
         echo $HTA1 >> $FileName
         echo $HTA2 >> $FileName
         echo $HTA3 >> $FileName
         echo $HTA4 >> $FileName
         echo $HTA_Bracket1 >> $FileName
         echo "require group intranet_$paths" >> $FileName
         echo $HTA_Standard >> $FileName
         echo $HTA_Bracket2 >> $FileName

     done

     # Special handling for sub-directories in academic
     rm -f $1/admin/academic/.ht*
     for paths in "attendance" "merit" "service" "activity" "award" "assessment" "student_files"
     do
         rm -f $1/admin/academic/$paths/.ht*

         FileName="$1/admin/academic/$paths/.htaccess"
         echo $HTA1 >> $FileName
         echo $HTA2 >> $FileName
         echo $HTA3 >> $FileName
         echo $HTA4 >> $FileName
         echo $HTA_Bracket1 >> $FileName
         echo "require group intranet_$paths" >> $FileName
         echo $HTA_Standard >> $FileName
         echo $HTA_Bracket2 >> $FileName

     done


     echo "[Htaccess setting - Done]"


     echo "[Httpd setting] - pls add the following to httpd.conf and the run start.php"
     echo "                         and then run start.php"

     echo "<Directory $1>"
     echo "     Options Includes FollowSymLinks"
     echo "     AllowOverride All"
     echo "</Directory>"

     echo "<Directory $1/file>"
     echo "     php_flag engine off"
     echo "</Directory>"

     echo ""
     exit 0
fi
