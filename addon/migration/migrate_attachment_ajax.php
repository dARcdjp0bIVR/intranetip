<?php

@SET_TIME_LIMIT(60000);

include_once('../check.php');
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libfilesystem.php");

intranet_opendb();

$libdb = new libdb();
$libfs = new libfilesystem();


//$filepath_attachment = "mail_attachment_folder.txt";
//$filepath = "staff_id_mapping.csv";

$source_folder = "../../file/mail_src";
$target_folder = "../../file/mail_target";
//$source_folder = "../../file/group";

if (!file_exists($source_folder))
{
	die($source_folder." does not exist");
} elseif (!file_exists($target_folder))
{
	die($target_folder." does not exist");
}

//yuen
$folders = $libfs->return_folder($source_folder);
$folderNames = array();
for ($i=0; $i<sizeof($folders); $i++)
{
	$folderNow = $folders[$i];
	$folderNow = str_replace($source_folder."/", "", $folderNow);
	list($mailFolder, $throw) = split("/", $folderNow);
	//debug($mailFolder . " ---- ". $folderNow);
	if (!in_array($mailFolder, $folderNames))
	{
		$folderNames[] = $mailFolder;	
	}
}

$data = explode("\n",get_file_content($filepath));

# read mapping of UserIDs
for ($i=0; $i<sizeof($data); $i++)
{
	list($OldUserID,$NewUserID,$UserLogin,$EnglishName) = explode(",", $data[$i]);
	//debug("($OldUserID,$NewUserID,$UserLogin,$EnglishName)");
	$newUserID[$OldUserID] = $NewUserID;
}
//$newUserID[984] = 2361;
debug_r($newUserID);
function reMapUserIDs($orginalIDs, $newUserID)
{
	$arrData = explode(",", $orginalIDs);
	for ($i=0; $i<sizeof($arrData); $i++)
	{
		$IDnow = trim($arrData[$i]);
		$posFound = strpos($IDnow, "u");
		if ($posFound===false)
		{
			$newIDs[] = $IDnow;
		} else
		{
			$thisUserID = substr($IDnow, 1);
			if ($newUserID[$thisUserID]>0 && $newUserID[$thisUserID]!="")
			{
				$newIDs[] = "u".$newUserID[$thisUserID];
			}
		}
	}
	if (sizeof($newIDs)>0)
	{
		return implode(",", $newIDs);
	} else
	{
		return $orginalIDs;
	}
}


//yuen - temp
//$data = get_file_content($filepath_attachment);

$MigFolders = $folderNames;

for ($i=0; $i<sizeof($MigFolders); $i++)
{
		# remap folder "Unnnnnn" using new UserID
	$folder_origin = $MigFolders[$i];
	$folder_target_name = reMapUserIDs($folder_origin,$newUserID);
	$folder_path = $target_folder."/".$folder_target_name;
		
	# check existence in target folder
	if (file_exists($folder_path))
	{
		if ($folder_origin==$folder_target_name)
		{
			# rename and log to a mapping file
			$folder_path .= "_migrated";
			$renamedFolders[$folder_target_name] = $folder_target_name."_migrated";
			# move to target mail folder
			exec("mv -f {$source_folder}/{$folder_origin} $folder_path");
			debug("1 mv -f {$source_folder}/{$folder_origin} $folder_path");
		} else
		{
			# copy all sub-folders of that user
			exec("mv -f  {$source_folder}/{$folder_origin}/* {$folder_path}/");
			exec("rm -fr  {$source_folder}/{$folder_origin}");
			
			debug("2 mv -f  {$source_folder}/{$folder_origin}/* {$folder_path}/");
		}
		//debug($folder_path." exists!");
	} else
	{
		exec("mv {$source_folder}/{$folder_origin} $folder_path");
		debug("3. mv {$source_folder}/{$folder_origin} $folder_path");
		//debug($folder_path." does not exist!");
	}
}

//debug_r($MigFolders);

intranet_closedb();


if (sizeof($renamedFolders)>0)
{
	$file_contents = serialize($renamedFolders);
	$fpt = fopen('../../file/mail_renamed_folder'. date("Ymd_His").'.txt', 'w');
	fwrite($fpt, $file_contents);
	fclose($fpt);
}

unset($MigFolders);
?>