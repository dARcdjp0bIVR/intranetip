<?php

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

$li = new libdb();
intranet_opendb();

$sql = "CREATE TABLE IF NOT EXISTS INTRANET_CAMPUSMAIL_USED_STORAGE (
			UserID INT(8) NOT NULL,
			QuotaUsed float default NULL,
			PRIMARY KEY  (UserID)
		) ENGINE InnoDB DEFAULT CHARSET=utf8";
$li->db_db_query($sql);

$sql = "SELECT UserID, SUM(AttachmentSize) FROM INTRANET_CAMPUSMAIL GROUP BY UserID";
$result = $li->returnArray($sql,2);

if(sizeof($result)>0){
	for($i=0; $i<sizeof($result); $i++){
		list($UserID, $UsedQuota) = $result[$i];
		$sql = "INSERT INTO INTRANET_CAMPUSMAIL_USED_STORAGE (UserID, QuotaUsed) VALUES ($UserID, $UsedQuota) ON DUPLICATE KEY UPDATE QuotaUsed = $UsedQuota";
		echo $sql."<BR><BR>";
		$final[] = $li->db_db_query($sql);
	}
}

if(in_array(1,$final))
	echo "Success - iMail User Storage table is updated.";
else
	echo "Fail - iMail User Storage table cannot update.";
	
intranet_closedb();
?>