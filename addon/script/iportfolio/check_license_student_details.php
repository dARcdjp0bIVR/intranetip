<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../..//includes/libportfolio.php");
include_once("../../../lang/lang.$intranet_session_language.php");
include_once("../../../lang/iportfolio_lang.$intranet_session_language.php");
include_once("../../check.php");

intranet_opendb();

$details = $_GET[target];
if($details == null || $details < 0 || $details > 5){
	intranet_closedb();
	exit;
}

$liport = new libportfolio();
$liport->ACCESS_CONTROL("student_info");

$result = $liport->returnStudentLicenseSummary($details);
//debug_pr($result);
if($details == 1 || $details == 2){
	$x = "<table border=1><tr><td>#</td><td>Class Name</td><td>Class Number</td><td>Student Name</td><td>Student ID</td></tr>";
} else {
	$x = "<table border=1><tr><td>#</td><td>Student Name</td><td>Student ID</td></tr>";
}

$num = 1;
foreach ($result as $eachstudent => $student_details){
	if($details == 1 || $details == 2){
		$x .= "<tr><td>".$num."</td><td>".$student_details[ClassTitleEN]."</td><td>".$student_details[ClassNumber]."</td><td>".$student_details[EnglishName]."</td><td>".$student_details[UserID]."</td></tr>";
	} else {
		$x .= "<tr><td>".$num."</td><td>".$student_details[EnglishName]."</td><td>".$student_details[UserID]."</td></tr>";
	}
	$num++;
}

$x .= "</table>";

$x .= "<a href='check_license_student.php'>Back</a>";

echo $x;

intranet_closedb();
?>