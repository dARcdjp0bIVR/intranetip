<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/form_class_manage.php");
include_once($PATH_WRT_ROOT."includes/libportfolio.php");

if(!$flag){
	echo 'plz add ?flag=1 if you sure to run the script';
	die();
}

intranet_opendb();
set_time_limit(300);
@ini_set('memory_limit', -1);
echo 'START: ' . date('Y-m-d H:i:s') . '<br />';
flush();

$objLibportfolio = new libportfolio();
$thisSuccess = true;

#### backup START ####
$sql = "CREATE TABLE $eclass_db.ASSESSMENT_SUBJECT_SD_MEAN_".date('YmdHi')." SELECT * FROM $eclass_db.ASSESSMENT_SUBJECT_SD_MEAN";
$success = $objLibportfolio->db_db_query($sql);
#### backup END ####

if($success){
	echo "backup success - ASSESSMENT_SUBJECT_SD_MEAN_".date('YmdHi')."";

	#### Get all Year START ####
	$sql = "SELECT YearID FROM YEAR";
	$yearArr = $objLibportfolio->returnVector($sql);
	#### Get all Year END ####
	
	#### Get all AcademicYear START ####
	$sql = "SELECT AcademicYearID FROM ACADEMIC_YEAR ";// . "WHERE AcademicYearID IN ('14')";
	$academicYearArr = $objLibportfolio->returnVector($sql);
	#### Get all AcademicYear END ####
		
	#### Get all YearTerm START ####
	$sql = "SELECT AcademicYearID, YearTermID FROM ACADEMIC_YEAR_TERM ";// . "WHERE AcademicYearID IN ('14')";
	$rs = $objLibportfolio->returnResultSet($sql);
	#### Get all YearTerm END ####
		
	#### Calculate SD START ####
	foreach($yearArr as $YearID){
	    foreach($rs as $r){
	        $objLibportfolio->ComputeSDMean($YearID, $r['AcademicYearID'], $r['YearTermID']);
	        echo "YearID: {$YearID}, AcademicYearID: {$r['AcademicYearID']}, YearTermID: {$r['YearTermID']}<br />";
	        flush();
	    }
	    foreach($rs as $r){
	        $objLibportfolio->ComputeSDMean($YearID, $r['AcademicYearID'], 0);
	        echo "YearID: {$YearID}, AcademicYearID: {$r['AcademicYearID']}, YearTermID: ''<br />";
	        flush();
	    }
	}
	#### Calculate SD END ####
}
else{
	echo "backup fail, aborted.";
}


echo 'END: ' . date('Y-m-d H:i:s') . '<br />';
intranet_closedb();
?>