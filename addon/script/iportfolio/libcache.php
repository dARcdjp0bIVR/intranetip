<?php

class cache{

/*
Class Name: cache
Des cription: control to cache data,$cache_out_time is a array to save cache date time out.
Version: 1.0
Author: 老? cjjer
Last modify:2006-2-26
Author URL:http://www.cjjer.com
*/

  private $cache_dir;
  private $expireTime=180;//?存的??是 60 秒
  
  function __construct($cache_dirname){
    if(!@is_dir($cache_dirname)){
      if(!@mkdir($cache_dirname,0777)){
        $this->warn('?存文件不存在而且不能?建,需要手??建.');
        return false;
      }
    }

    $this->cache_dir = $cache_dirname;
  }

  function __destruct(){
    echo 'Cache class bye.';
  }

  function get_url() {
    if (!isset($_SERVER['REQUEST_URI'])) {
      $url = $_SERVER['REQUEST_URI'];
    }else{
      $url = $_SERVER['SCRIPT_NAME'];
      $url .= (!empty($_SERVER['QUERY_STRING'])) ? '?' . $_SERVER['QUERY_STRING'] : '';
    }

    return $url;
  }

  function warn($errorstring){
    echo "<b><font color='red'>?生??:<pre>".$errorstring."</pre></font></b>";
  }

  function cache_page($pageurl,$pagedata){
  
    if(!$fso=fopen($pageurl,'w')){
      $this->warns('?法打??存文件.');//trigger_error
      return false;
    }

    if(!flock($fso,LOCK_EX)){//LOCK_NB,排它型?定
      $this->warns('?法?定?存文件.');//trigger_error
      return false;
    }

    if(!fwrite($fso,$pagedata)){//?入字?流,serialize?入其他格式
      $this->warns('?法?入?存文件.');//trigger_error
      return false;
    }

    flock($fso,LOCK_UN);//?放?定
    fclose($fso);
    return true;
  }

  function display_cache($cacheFile){
  
    if(!file_exists($cacheFile)){
      $this->warn('?法?取?存文件.');//trigger_error
      return false;
    }

    echo '?取?存文件:'.$cacheFile;

    //return unserialize(file_get_contents($cacheFile));
    $fso = fopen($cacheFile, 'r');
    $data = fread($fso, filesize($cacheFile));
    fclose($fso);
    
    return $data;
  }

  function readData($cacheFile='default_cache.txt'){
  
    $cacheFile = $this->cache_dir."/".$cacheFile;
    
    if(file_exists($cacheFile)&&filemtime($cacheFile)>(time()-$this->expireTime)){
      $data=$this->display_cache($cacheFile);
    }else{
      $data="from here wo can get it from mysql database,update time is <b>".date('l dS \of F Y h:i:s A')."</b>,?期??是:".date('l dS \of F Y h:i:s A',time()+$this->expireTime)."----------";
      $this->cache_page($cacheFile,$data);
    }
    
    return $data;
  }

}

?>