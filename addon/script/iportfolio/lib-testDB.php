<?php
class DBManager
{
	private static $s_dbObject;
	
	public static function getDBObject($tempValue = "")
	{
		global $intranet_db;
		global $eclass_db;
		if(DBManager::$s_dbObject === NULL)
		{
			$objDB = new libdb();
			DBManager::$s_dbObject = $objDB;
			DBManager::$s_dbObject->db = $intranet_db;
			DBManager::$s_dbObject->eclass_db = $eclass_db;			
			lpfun_writeLog("inital DB with value ".$tempValue);
		}
		return DBManager::$s_dbObject;
	}
}
?>