<table>
<tr><td><b>ProgramID :</b></td><td><?=$ProgramID?></td></tr>
<tr><td><b>ProgramType :</b></td><td><?=$ProgramType?></td></tr>
<tr><td><b>Title :</b></td><td><?=$Title?></td></tr>
<tr><td><b>TitleChi :</b></td><td><?=$TitleChi?></td></tr>
<tr><td><b>StartDate :</b></td><td><?=$StartDate?></td></tr>
<tr><td><b>EndDate :</b></td><td><?=$EndDate?></td></tr>
<tr><td><b>Category :</b></td><td><?=$Category?>[<?=$CatEngName?> - <?=$CatChiName?>]</td></tr>
<tr><td><b>SubCategoryID :</b></td><td><?=$SubCategoryID?>[<?=$SubCatEngName?> - <?=$SubCatChiName?>]</td></tr>
<tr><td><b>Organization :</b></td><td><?=$Organization?></td></tr>
<tr><td><b>Details :</b></td><td><?=$Details?></td></tr>
<tr><td><b>DetailsChi :</b></td><td><?=$DetailsChi?></td></tr>
<tr><td><b>SchoolRemarks :</b></td><td><?=$SchoolRemarks?></td></tr>
<tr><td><b>ELE :</b></td><td><?=$ELE?></td></tr>
<tr><td><b>InputDate :</b></td><td><?=$InputDate?></td></tr>
<tr><td><b>ModifiedDate :</b></td><td><?=$ModifiedDate?></td></tr>
<tr><td><b>CreatorID :</b></td><td><?=$CreatorID?> [<?=$CreatorName?>]</td></tr>
<tr><td><b>CreatorType :</b></td><td><?=$CreatorType?></td></tr>
<tr><td><b>IntExt :</b></td><td><?=$IntExt?></td></tr>
<tr><td><b>AcademicYearID :</b></td><td><?=$YearNameEN?>[<?=$AcademicYearID?>]</td></tr>
<tr><td><b>YearTermID :</b></td><td><?=$YearTermNameEN?>[<?=$YearTermID?>]</td></tr>
<tr><td><b>CanJoin :</b></td><td><?=$CanJoin?></td></tr>
<tr><td><b>CanJoinStartDate :</b></td><td><?=$CanJoinStartDate?></td></tr>
<tr><td><b>CanJoinEndDate :</b></td><td><?=$CanJoinEndDate?></td></tr>
<tr><td><b>AutoApprove :</b></td><td><?=$AutoApprove?></td></tr>
<tr><td><b>JoinableYear :</b></td><td><?=$JoinableYear?></td></tr>
<tr><td><b>ComeFrom :</b></td><td><?=$ComeFrom?>[<?=$comeFromMeaning?>]</td></tr>
<tr><td colspan="2"><hr/></td></tr>

<tr>
	<td valign="top"><b>No of Student in this Program :</b></td>
	<td><?=$totalStudentInOLEProgram?><br/><br/>
		Approve Status : <?=$studentStatusForTheProgram?>
	</td></tr>
</table>