<?php

include_once("../../../includes/global.php");
include_once("../../../includes/libdb.php");
include_once("../../../includes/libfilesystem.php");
intranet_opendb();

$li = new libdb();
$lfs = new libfilesystem();

$sql = "SELECT course_id FROM {$eclass_db}.course WHERE RoomType = 4";
$course_db = $eclass_prefix."c".current($li->returnVector($sql));

$sql = "SELECT handin_id, answer FROM ".$course_db.".handin WHERE type = 'blog' AND answer <> ''";
//$sql = "SELECT handin_id, answer FROM ".$course_db.".handin WHERE handin_id IN (2186, 3325, 4611, 5575, 5578, 5579)";
$weblog_arr = $li->returnArray($sql);
$rec_all = count($weblog_arr);

ob_start();
echo "start time => ".date("Y-m-d H:i:s")."<br />\n";
echo "==============================================================<br /><br />\n\n";

$rec_update = 0;
$rec_skip = 0;
for($k=0; $k<count($weblog_arr); $k++)
{
  $handin_id = $weblog_arr[$k]['handin_id'];
  $str = $weblog_arr[$k]['answer'];

  preg_match_all ("/&lt;IMG.+src=&quot;(.+)&quot;.+&gt;/U", $str, $matches);
  
  $pattern = array();
  $replacement = array();
  for($i=0; $i<count($matches[1]); $i++)
  {
    $tmp_path_arr = explode("/", $matches[1][$i]);
    for($j=0; $j<count($tmp_path_arr); $j++)
    {
      $tmp_path_arr[$j] = ($tmp_path_arr[$j] == "http:") ? $tmp_path_arr[$j] : rawurldecode($tmp_path_arr[$j]);
    }
    $tmp_path = implode("/", $tmp_path_arr);
    
    if(mb_detect_encoding($tmp_path) != "UTF-8")
    {
      $pattern[] = "/".str_replace("/", "\/", $matches[1][$i])."/";
    
      $target = iconv("Big5", "UTF-8", $tmp_path);

      $tmp_path_arr = explode("/", $target);
      for($j=0; $j<count($tmp_path_arr); $j++)
      {
        $tmp_path_arr[$j] = ($tmp_path_arr[$j] == "http:") ? $tmp_path_arr[$j] : rawurlencode($tmp_path_arr[$j]);
      }
      $tmp_path = implode("/", $tmp_path_arr);
      
      $replacement[] = $tmp_path;
    }
  }
  $replace_str = preg_replace($pattern, $replacement, $str);
  if($str == $replace_str){
    $rec_skip++;
    continue;
  }
  
  $sql = "UPDATE ".$course_db.".handin SET answer = '".addslashes($replace_str)."' WHERE handin_id = '".$handin_id."'";
  $li->db_db_query($sql);
  
  echo "handin id => ".$handin_id."<br />\n";
  echo "pattern => ";
  echo("<PRE>");
  print_r($pattern);
  echo("</PRE>\n");
  echo "replacement => ";
  echo("<PRE>");
  print_r($replacement);
  echo("</PRE>\n");
  echo "original str =><br />\n".$str."<br /><br />\n";
  echo "updated str =><br />\n".$replace_str."<br /><br />\n";
  echo "update sql =><br />\n".$sql."<br /><br />\n\n";
  echo("==============================================================<br /><br />\n\n");
  
  $rec_update++;
  
  unset($pattern);
  unset($target);
  unset($replacement);
}
echo "end time => ".date("Y-m-d H:i:s")."<br />\n";
echo "# records => ".$rec_all."<br />\n";
echo "# records updated => ".$rec_update."<br />\n";
echo "# records skipped => ".$rec_skip."<br />\n";

$logstr .= ob_get_contents();
ob_end_flush();

$logdir = $intranet_root."/file/iportfolio/log";
@mkdir($logdir, 0777, true);
$logpath = $logdir."/patch_weblog_result_".date("Ymd_His").".txt";
$lfs->file_write($logstr, $logpath);

?>