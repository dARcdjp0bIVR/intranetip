<?
function generateHTMLTableResult($resultSet)
{

	$noOfResult = sizeof($resultSet);
	$HeaderTitleArr  = array();
	$titleStack = array();
	$htmlTitleRow  = "";
	$noOfField  = 0;
	$htmlData  = "";
	if($noOfResult != 0)
	{
		$HeaderTitleArr = array_keys($resultSet[0]);

		$htmlTitleRow .= "<tr>";
		foreach ($HeaderTitleArr as $HeaderTitle)
		{

			if(is_int($HeaderTitle)>0)
			{
				
			}
			else
			{
				$htmlTitleRow .= '<th>'."\n";
				$htmlTitleRow .= trim($HeaderTitle);				
				$htmlTitleRow .= '</th>'."\n";

				$titleStack[] = $HeaderTitle;
			}

		}
		$noOfField = sizeof($titleStack);
		$htmlTitleRow .= "</tr>";
	
		for($i  = 0; $i <$noOfResult;$i++)
		{
			//DISPLAY EACH ROW
//			$htmlData  .= "<tr>";
			$htmlData  .= "<tr onMouseOver=\"this.className='TableRowhighlight'\" onMouseOut=\"this.className=''\">";
			for($j =0;$j < $noOfField;$j++)
			{
				//DISPLAY EACH DATA
				$_data = $resultSet[$i][$titleStack[$j]];
				$htmlData .= "<td>".$_data."&nbsp;</td>";
			}
			$htmlData  .= "</tr>";	
		}

		$htmlSummary = "<tr><td colspan = \"".$noOfField."\">Total Record : ".$noOfResult."</td></tr>";
	}
	
//	$htmlResult = "<div class=\"move_drag\"><table border = \"1\">";
	$htmlResult = "<div class=\"\"><table border = \"1\">";
	$htmlResult .=  $htmlTitleRow;
	$htmlResult .=  $htmlData;
	$htmlResult .=  $htmlSummary;
	$htmlResult .=  "</table></div>";
	return $htmlResult;
}
function logEvent($content , $file)
{
	if($file == "")
	{
		$now = date("Ymd");  
		$file = "/home/web/eclass40/intranetIP25/addon/script/iportfolio/lp_data_log/".$now.".log";
	}		

	//Handle 
	$_now = date("Y-m-d G:i:s");  		
	$uniqid = uniqid();
	$logContent = "Time [".$_now."][".$uniqid."]";
	$logContent .= $content;

	$logBackTraceContent = $logContentHeader.my_debug_print_backtrace(0)."\n";

	$logFp = fopen($file, 'a');
	fwrite($logFp, $logContent);	
	fclose($logFp);

	$logBackTraceFp = fopen($backtrace_file, 'a');
	fwrite($logBackTraceFp, $logBackTraceContent);	
	fclose($logBackTraceFp);

	//check_writeLog($logContent , $logPath);	
}
function lpfun_writeLog($content)
{
	$_logFileDirectory = "/tmp";


	$_now = date("YmdGis");
	$_today = date("Ymd");
	$_logFileName = "iPf_log_".$_today.".log";
	$_logDebug_backtraceFileName = "iPf_log_".$_today."_debug_backtrace.log";

	$_logFile = $_logFileDirectory."/".$_logFileName;
	$_logDebugTraceFile = $_logFileDirectory."/".$_logDebug_backtraceFileName;

	
	$uniqid = uniqid();
	$logContentHeader = "Time [".$_now."] Id [".$uniqid."]\n";
	$logContent = $logContentHeader.$content."\n";

	$logBackTraceContent = $logContentHeader.my_debug_print_backtrace(2)."\n";

	//check dictory is accessable and writeable
	
	//check php version support with debug_backtrace

	
	error_log($logContent, 3, $_logFile);
	error_log($logBackTraceContent, 3, $_logDebugTraceFile);

}
function check_writeLog($content , $logFile)
{

	//$now = date("Ymd");  
	//$logPath = "/home/web/eclass40/intranetIP25/addon/script/iportfolio/lp_data_log/".$now.".log";
	//check_writeLog($logContent , $logPath);

	$_fileName = basename($logFile);
	$_explodeFileNameArray = explode(".",$_fileName);

	$_debug_backtrace_file = $_explodeFileNameArray[0]."_debug_backtrace.".$_explodeFileNameArray[1];

	$_now = date("YmdGis");  	

	//create a new file base on pass in file 
	//xx/bbb/a.log --> xx/bbb/a_debug_backtrace.log 

	$backtrace_file = str_replace($_fileName, $_debug_backtrace_file, $logFile);


	$uniqid = uniqid();
	$logContentHeader = "Time [".$_now."] File [".$_SERVER['SCRIPT_FILENAME']."] Uniq Id [".$uniqid."]\n";
	$logContent = $logContentHeader.$content."\n";
	
	$logBackTraceContent = $logContentHeader.my_debug_print_backtrace(0)."\n";


	$logFp = fopen($logFile, 'a');
	fwrite($logFp, $logContent);	
	fclose($logFp);


	$logBackTraceFp = fopen($backtrace_file, 'a');
	fwrite($logBackTraceFp, $logBackTraceContent);	
	fclose($logBackTraceFp);
	
	
}

function my_debug_print_backtrace($level_to_skip = 2)
{
	$s = '';

	foreach (debug_backtrace() as $k => $v) {

		$i = $k - $level_to_skip;

		if ($i < 0) continue;
		$args = '';
		$arg_separator = '';
		foreach ($v['args'] as $arg) {
			$_arg = (gettype($arg) == 'object') ? 'Object' : "[".trim($arg)."]";
			$args .= $arg_separator.$_arg;
			$arg_separator = ',';
		}
		$s .= "#$i $v[function]($args) called at [$v[file]:$v[line]]\n";
	}

	return $s;
}
?>