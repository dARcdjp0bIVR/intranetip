<?php
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."addon/check.php");
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libpf-sturec.php");

intranet_opendb();

$lf = new libfilesystem();
$li_pf = new libpf_sturec();

### Get all keys from the .dat file
$keys = $li_pf->GET_KEY_LICENSE($li_pf->file_path."/keys.dat");
$UserKeyArr = array();
foreach ((array)$keys['records'] as $thisDate => $thisDateKeyArr) {
	$thisNumOfDateKey = count($thisDateKeyArr);
	for ($i=0; $i<$thisNumOfDateKey; $i++) {
		$UserKeyArr[] = $thisDateKeyArr[$i];
	}
}


### Get all current Student Key Info
$PORTFOLIO_STUDENT = $eclass_db.".PORTFOLIO_STUDENT";
$sql = "Select
				RecordID, UserID, UserKey
		From
				$PORTFOLIO_STUDENT
		Order By
				RecordID
		";
$StudentInfoArr = $li_pf->returnArray($sql);
$numOfStudent = count($StudentInfoArr);

$numOfHaveKeyAfter = 0;
$numOfNotHaveKeyAfter = 0;
$numOfSuccess = 0;
$numOfFailed = 0;
$SuccessArr = array();

$logContent = '';
$logContent .= 'RecordID,UserID,OldUserKey,NewUserKey,Status'."\r\n";
for ($i=0; $i<$numOfStudent; $i++) {
	$thisRecordID = $StudentInfoArr[$i]['RecordID'];
	$thisStudentID = $StudentInfoArr[$i]['UserID'];
	$thisOldUserKey = $StudentInfoArr[$i]['UserKey'];
	$thisNewUserKey = $UserKeyArr[$i];
	
	if ($thisNewUserKey == '') {
		$numOfNotHaveKeyAfter++;
	}
	else {
		$numOfHaveKeyAfter++;
	}
	
	$sql = "Update $PORTFOLIO_STUDENT set UserKey = '".$thisNewUserKey."' Where RecordID = '".$thisRecordID."'";
	$SuccessArr[$thisRecordID] = $li_pf->db_db_query($sql);
	
	if ($SuccessArr[$thisRecordID]) {
		$thisStatus = 'success';
		$numOfSuccess++;
	}
	else {
		$thisStatus = 'failed';
		$numOfFailed++;
	}
	
	$logContent .= $thisRecordID.','.$thisStudentID.','.$thisOldUserKey.','.$thisNewUserKey.','.$thisStatus."\r\n";
}

$logContent .= "\r\n";
$logContent .= 'Total number of records: '.$numOfStudent."\r\n";
$logContent .= 'Number of records have UserKey after the patch: '.$numOfHaveKeyAfter."\r\n";
$logContent .= 'Number of records NOT having UserKey after the patch: '.$numOfNotHaveKeyAfter."\r\n";
$logContent .= 'Number of success update records: '.$numOfSuccess."\r\n";
$logContent .= 'Number of falied update records: '.$numOfFailed."\r\n";

$log_filepath = $PATH_WRT_ROOT."file/iportfolio/log/reset_user_key_".date("Ymd_His").".txt";
$lf->file_write($logContent, $log_filepath);

echo "Logged at $log_filepath";

intranet_closedb();
?>