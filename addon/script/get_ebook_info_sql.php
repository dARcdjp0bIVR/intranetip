<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/libfilesystem.php');
include_once("../check.php");
intranet_opendb();
$db = new libdb();
$fm = new libfilesystem();

$notDisplay = ($bookID==''&&$category=='')?true:false;

$sql = "SELECT Title, BookID FROM INTRANET_ELIB_BOOK WHERE BookID IN (SELECT BookID FROM INTRANET_EBOOK_BOOK) ";
if($bookID !=''){
	$sql .= " AND BookID IN (".$bookID.")";
}
if($category!=''){
	$catArr = explode("|@@@|",$category);
	$sql .= " AND Category='".$catArr[0]."' AND SubCategory='".$catArr[1]."' ";
}

$catSql = "SELECT DISTINCT Category, SubCategory FROM INTRANET_ELIB_BOOK WHERE BookID IN (SELECT BookID FROM INTRANET_EBOOK_BOOK)";
$catList =$db->returnArray($catSql);

if($notDisplay){
	$resultList = "";
}else{
	$resultList = $db->returnArray($sql);	
}

?>
<html>
<head>
	<title>Get eBook Data</title>
	<style>
		/* Pen-specific styles */
		* {
		  box-sizing: border-box;
		}
		
		html, body, div {
		  min-height: 100%;
		}
		
		body { 
		  color: #fff;
		  font-family: sans-serif;
		  font-size: 1.25rem;
		  line-height: 150%;
		  text-shadow: 0 2px 2px #b6701e;
		}
		
		h1 {
		  font-size: 1.75rem;
		  margin: 0 0 0.75rem 0;
		}
		
		/* Pattern styles */
		div {
		  display: inline-block;
		  vertical-align: top;
		  
		  padding: 1rem;
		}
		
		.left-half {
		  background: #ff9e2c;
		  width: 50%;
		}
		
		.right-half {
		  background: #b6701e;
		  width: 45%;
		}
	</style>
	<script>
		function selectAll(obj){
			checkboxes = document.getElementsByName('books[]');
			for(var i=0, n=checkboxes.length;i<n;i++) {
				checkboxes[i].checked = obj.checked;
			}
		}
	</script>
</head>
<body>
	<div id="searchForm" class="left-half">
		<h1>Search by:</h1>
		<form id="main" name="main" method="POST">
			<table>
			<tr><td>BookID:</td><td><input type="text" name="bookID" value="<?=$bookID?>" placeholder="enter BookID to get info, sepearte by ','" size="60"></td><td> <b>OR</b> </td></tr>
			<tr><td>Category:</td><td> 
			<select name="category">
				<option value=''></option>
				<?php
				foreach($catList as $cat){
					$display = $cat['Category'].' - '.$cat['SubCategory'];
					$value = $cat['Category'].'|@@@|'.$cat['SubCategory'];
					if($category == $value){
						$isSelected = "selected";
					}else{
						$isSelected = "";
					}
					echo "<option value='".$value."' $isSelected>".$display."</option>";
				}
				?>
			</select>
			</td></tr>
			</table>
			<input type="submit" name="Submit" value="Generate eBook List">
		</form>
		<hr>
		<div id="resultArea">
			<form id="bookPick" name="bookPick" method="POST">
			<?php
				if($resultList==""){
					echo 'No Books to be selected';
				}else{
					echo "<table>";
					echo "<tr><td></td><td><input type='checkbox' onclick='selectAll(this)'></td></tr>";
					foreach($resultList as $book){
						if(!empty($books)){
							$checked = (in_array($book['BookID'], $books))?'checked':'';
						}
						echo "<tr>";
						echo "<td>".$book['Title']."</td>";
						echo "<td><input type='checkbox' name='books[]' value='".$book['BookID']."' $checked><br></td>";
						echo "</tr>";
					}
					echo "<tr><td colspan='2'>".'<input type="submit" name="Submit" value="Get eBook Patch >>">'."</td></tr>";
					echo "</table>";
				}
			?>
			<input type="hidden" name="bookID" value="<?=$bookID?>">
			<input type="hidden" name="category" value="<?=$category?>">
			</form>
		</div>
	</div>	

	<div id="displayArea" class="right-half">
		<h1>Result Board:</h1>
		<?php
			if(isset($books)&&!empty($books)){
				$mainSql = "";
				$folderToDownload = $intranet_root."/file/temp/ebook_export/";
				$dest1 = $folderToDownload.'eBook';
				$dest2 = $folderToDownload.'elibrary';
				$fm->folder_remove_recursive($folderToDownload);
				
				// pack the folders
				$fm->createFolder($folderToDownload);
				mkdir($dest1,0777);
				mkdir($dest2,0777);
				$dest2 = $dest2.'/content';
				mkdir($dest2,0777);
				foreach($books as $book){
					$src1 = $intranet_root."/file/eBook/".$book."/";
					$src2 = $intranet_root."/file/elibrary/content/".$book."/";
					folder_copy_recursive($src1, $dest1.'/'.$book);
					folder_copy_recursive($src2, $dest2.'/'.$book);					
				}
				
				//pack the sql
				$insert_sql = array();
				$quotation_csv = "";
				foreach($books as $book){
					$book_info_sql = "SELECT * FROM INTRANET_ELIB_BOOK WHERE BookID='".$book."'";
					$ebook_sql = "SELECT * FROM INTRANET_EBOOK_BOOK  WHERE BookID='".$book."'";
					$page_sql = "SELECT * FROM INTRANET_EBOOK_PAGE_INFO WHERE BookID='".$book."'";
					
					$book_info = current($db->returnArray($book_info_sql));
					$ebook = current($db->returnArray($ebook_sql));
					$page_info = $db->returnArray($page_sql);		
					
					$book_info_insert = "INSERT INTO INTRANET_ELIB_BOOK VALUES (";
					for($i=0; $i < count($book_info)/2; $i++){
						$book_info_insert .= "'".$db->Get_Safe_Sql_Query($book_info[$i])."'";
						if($i < count($book_info)/2-1){
							$book_info_insert .= ",";
						}

					}
					$book_info_insert .= ");";
					$insert_sql[] = $book_info_insert;
					
					$ebook_insert = "INSERT INTO INTRANET_EBOOK_BOOK VALUES (";

					for($i=0; $i < count($ebook)/2; $i++){
						$ebook_insert .= "'".$db->Get_Safe_Sql_Query($ebook[$i])."'";
						if($i < count($ebook)/2-1){
							$ebook_insert .= ",";
						}
					}
					$ebook_insert .= ");";
					$insert_sql[] = $ebook_insert;
									
					foreach($page_info as $page){
						$page_insert = "INSERT INTO INTRANET_EBOOK_PAGE_INFO VALUES (";
						for($i=0; $i < count($page)/2; $i++){
							$page_insert .= "'".$db->Get_Safe_Sql_Query($page[$i])."'";
							if($i < count($page)/2-1){
								$page_insert .= ",";
							}
						}
						$page_insert .= ");";
						$insert_sql[] = $page_insert;
					}
					
					$quotation_csv .= $book.",1".PHP_EOL;				
				}
				$content = implode(PHP_EOL,$insert_sql);
				$fp = fopen($folderToDownload."sql_to_insert.sql","wb");
				fwrite($fp,$content.PHP_EOL);
				fclose($fp);
				$fp = fopen($folderToDownload."book_quotation.csv","wb");
				fwrite($fp,$quotation_csv);
				fclose($fp);
								
				//Pack the ebooks and sql files				
				$ZipFileName = "ebook_export_".date('Ymdhis').".zip";
				$export_directory = $intranet_root."/file/temp/";
				$fm->file_remove($export_directory.$ZipFileName);//remove old file
				$fm->file_zip('ebook_export', $ZipFileName, $export_directory);
				echo "<a href='".$intranet_rel_path."file/temp/".$ZipFileName."' target='_blank'>Download</a>";	
			}
			
		?>
	</div>
</body>
</html>
<?php
function folder_copy_recursive($src, $dest){
	$dir = opendir($src); 
    @mkdir($dest); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                folder_copy_recursive($src . '/' . $file,$dest . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dest . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
}
intranet_closedb();
?>