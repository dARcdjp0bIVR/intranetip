<?php
/**
 * Change Log:
 * 2018-03-09 Pun [ip.2.5.9.3.1]
 *  - File created
 */

function getKisTestEclassDbName(){
    global $PATH_WRT_ROOT;
    include($PATH_WRT_ROOT."includes/kis-test.eclass.hk/settings.php");
    return $eclass_db;
}

function getKisTestDbPrefixName(){
    global $PATH_WRT_ROOT;
    include($PATH_WRT_ROOT."includes/kis-test.eclass.hk/settings.php");
    return $eclass_prefix;
}



$course_code = 'kisworksheets';
$db_table = array('assessment','phase','task','question','quiz','quiz_question');
$update_time = '2017-02-23 00:00:00';
//$update_time = '2017-03-21 00:00:00';
//$update_time = '2017-04-20 00:00:00';

$lo = new libeclass();

$sql = "select course_id from ".getKisTestEclassDbName().".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 7;
	echo "<strong> KIS Worksheets Database Update</strong>";
if($course_id!=''){
	$targetDbName = getKisTestDbPrefixName()."c".$course_id;
	echo "<br/><br/><strong>Source Database Name : ".$targetDbName."</strong><br/>";
}

$quizIDString = '';
$sqlArray = array();
foreach ($db_table as $tableName){
	if($tableName == 'quiz_question'){
		$sql = "select * from {$targetDbName}.{$tableName} where quiz_id in ({$quizIDString})";
	}else{
		$sql = "select * from {$targetDbName}.{$tableName} where modified > '$update_time'";
	}


	$returnAry = $lo->returnArray($sql,null,1);

	if($tableName == 'quiz'){
		$quizID = array();
	}

	foreach ($returnAry as $data){
		$fieldList = array();
		$valueList = array();

		foreach ($data as $key=>$value){
			$fieldList[] = "$key";
			$valueList[] = "'".mysql_real_escape_string($value)."'";

			if($key == 'quiz_id'){
				$quizID[] = $value;
			}

		}
		$fieldListString = implode($fieldList,',');
		$valueListString = implode($valueList,',');

		$sqlArray[] = "{$tableName} ({$fieldListString}) Values ({$valueListString})";
	}

	if($tableName == 'quiz'){
		$quizIDString = implode($quizID,',');;
	}
}

//-----Start installation-----

echo "<br/><h3>----- Start installation-----</h3><br/>";

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 7;

if($course_id!=''){
	$targetDbName = $eclass_prefix."c".$course_id;
}else{
	echo "<br/>-----Warning-----<br/>'$domain' has not install KIS Worksheets<br/>-----Warning-----<br/>";
	continue;
}

$errorSql = false;
echo "<br/><br/>----- Start insert {$domain}-----<br/>";

foreach($sqlArray as $sql){

	$insertSql = "Insert into {$targetDbName}.{$sql}";
	//echo "<br/>".$insertSql."<br/>";
	$result = $lo->db_db_query($insertSql);
	if(!$result){
		echo "<br/>Invalid query: {$insertSql}<br/>";
		$errorSql = true;
	}

}
if(!$errorSql){
	echo "Success!";
}
echo "<br/>----- End insert {$domain}-----<br/><br/>";

echo "<br/><h3>----- End installation-----</h3>";

//-----End installation-----
eclass_closedb();
