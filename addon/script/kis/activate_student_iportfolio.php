<?php
error_reporting(E_ERROR | E_WARNING | E_PARSE);
ini_set('display_errors', 1);

@SET_TIME_LIMIT(21600);
@ini_set(memory_limit, -1);

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT.'includes/global.php');
include_once($PATH_WRT_ROOT.'includes/libdb.php');
include_once($PATH_WRT_ROOT.'includes/libpf-sturec.php');

intranet_auth();
intranet_opendb();

$libdb = new libdb();

// get iPortfolio course id
// must get before initialize $lpf_sturec
$sql = "Select * From $eclass_db.course Where RoomType = 4";
$ary = $libdb->returnArray($sql);
$courseId = $ary[0]['course_id'];
$ck_course_id = $courseId;


$lpf_sturec = new libpf_sturec();


// get all student which has no websams number => add those student into iportfolio classroom
$sql = "Select * From INTRANET_USER Where RecordType = 2 And (WebSAMSRegNo = '' Or WebSAMSRegNo is null)";
$studentAry = $libdb->returnResultSet($sql);
$studentIdAry = Get_Array_By_Key($studentAry, 'UserID');


// set WebSAMSRegNo as UserLogin in ITRANET_USER
$sql = "Update INTRANET_USER Set WebSAMSRegNo = concat('#', UserLogin) Where UserID In ('".implode("','", (array)$studentIdAry)."')";
$successAry['updateIntranetUserWebSAMS'] = $libdb->db_db_query($sql);


// activate iPortoflio account
$studentIdList = implode(',', (array)$studentIdAry);
list($key_index, $FailedUserArr) = $lpf_sturec->DO_ACTIVATE_STUDENT($studentIdList);


debug_pr($key_index);
debug_pr($FailedUserArr);

if ($FailedUserArr == '') {
	echo '<font color="green">Success</font>';
}
else {
	echo '<font color="red">FAILED</font>';
}

intranet_closedb();
?>