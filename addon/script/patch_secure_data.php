<?php
// Editing by 
@SET_TIME_LIMIT(0);
//$PATH_WRT_ROOT = "../../";
include_once("../../includes/global.php");
include_once("../../includes/libdb.php");
include_once("../../includes/libgeneralsettings.php");
//include_once("../../includes/libpwm.php");
intranet_opendb();

$lgs = new libgeneralsettings();

$ModuleName = "InitSetting";
$GSary = $lgs->Get_General_Setting($ModuleName);

if ($intranet_authentication_method=="HASH" && $GSary['PatchSecureData_20140702'] != 1){
	include_once("../../includes/libpwm.php");
	
	$libpwm = new libpwm();
	
	$TestUserID = $_REQUEST['TestUserID'];
	
	//echo "######################################## Start patching user secure data ############################################<br />\n";
	echo "===================================================================<br>\r\n";
	echo "Patching user secure data [Start]<br>\r\n";
	echo "===================================================================<br>\r\n";
	
	$sql = "SELECT 
				u.UserID,
				s.EncPassword 
			FROM INTRANET_USER as u 
			LEFT JOIN INTRANET_USER_PERSONAL_SETTINGS as s ON s.UserID=u.UserID
			WHERE s.EncPassword IS NOT NULL AND s.EncPassword <> ''";
	if(count($TestUserID)>0){
		$sql .= " AND u.UserID IN ('".implode("','",$TestUserID)."')";
	}
	$rs = $libpwm->returnResultSet($sql);
	$rs_count = count($rs);
	
	$data_ary = array();
	for($i=0;$i<$rs_count;$i++){
		$uid = $rs[$i]['UserID'];
		$pw = $rs[$i]['EncPassword'];
		//$decrypted = GetDecryptedPassword($pw);
		$decrypted = AES_128_Decrypt($pw, $intranet_password_salt);
		$data_ary[$uid] = $decrypted;
	}
	
	$copy_success = $libpwm->setData($data_ary);
	
	if($copy_success){
		$sql = "UPDATE INTRANET_USER_PERSONAL_SETTINGS SET EncPassword=NULL WHERE EncPassword IS NOT NULL AND EncPassword<>''";
		if(count($TestUserID)>0){
			$sql .= " AND UserID IN ('".implode("','",$TestUserID)."')";
		}
		
		$libpwm->db_db_query($sql);
	}
	
	echo "# ".count($data_ary)." data copied ".($copy_success?"<span style=\"color:green\">successfully</span>":"<span style=\"color:red\">failed</span>").".<br />\n";
	
	//echo "######################################## End of patching user secure data ###########################################<br /><br />\n";
	echo "===================================================================<br>\r\n";
	echo "Patching user secure data [End]<br>\r\n";
	echo "===================================================================<br>\r\n";
	
	if($plugin['imail_gamma']){
		
		//echo "######################################## Start patching shared mail boxes ############################################<br />\n";
		echo "===================================================================<br>\r\n";
		echo "Patching shared mail boxes [Start]<br>\r\n";
		echo "===================================================================<br>\r\n";
		
		
		$sql = "SELECT MailBoxID, MailBoxPassword FROM MAIL_SHARED_MAILBOX WHERE MailBoxPassword IS NOT NULL AND MailBoxPassword<>''";
		$rs2 = $libpwm->returnResultSet($sql);
		$rs2_count = count($rs2);
		
		$data_ary2 = array();
		for($i=0;$i<$rs2_count;$i++){
			$data_ary2[$rs2[$i]['MailBoxID']] = $rs2[$i]['MailBoxPassword'];
		}
		
		$copy_sharemailbox_success = $libpwm->setShareMailboxData($data_ary2);
		
		if($copy_sharemailbox_success){
			$sql = "UPDATE MAIL_SHARED_MAILBOX SET MailBoxPassword=NULL";
			$libpwm->db_db_query($sql);
		}
		
		echo "# ".count($data_ary2)." shared mailboxes copied ".($copy_sharemailbox_success?"<span style=\"color:green\">successfully</span>":"<span style=\"color:red\">failed</span>").".<br />\n";
		
		//echo "######################################## End of patching shared mail boxes ###########################################<br />\n";
		echo "===================================================================<br>\r\n";
		echo "Patching shared mail boxes [End]<br>\r\n";
		echo "===================================================================<br>\r\n";
	}
	
	if(count($TestUserID)==0){
		$sql = "insert ignore into GENERAL_SETTING 
					(Module, SettingName, SettingValue, DateInput) 
				values 
					('$ModuleName', 'PatchSecureData_20140702', 1, now())";
		$libpwm->db_db_query($sql);
	}
}


?>