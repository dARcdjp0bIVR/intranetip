<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

## Prompt login
include_once("../../check.php");

intranet_opendb();

$course_code = 'rsreading3';
$source_db_name = 'ip20_b5_alpha_c644';
//$source_db_name = 'eclass40_c631';

$li = new libdb();

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$course_id = current($li->returnVector($sql));
$li->db = $eclass_prefix."c".$course_id;

$sql = "select question_id,question from question where question like '%".$source_db_name."%'";
$questionAry = $li->returnArray($sql);
$questionNum = count($questionAry);
echo "Current Course DB : ".$li->db."<br/>";
echo "No. of Questions require update : ".$questionNum."<br/>";
if($questionNum>0){
	$success_no = 0;
	$failQuestionAry = array();
	for($i=0;$i<$questionNum;$i++){
		$_question = str_replace($source_db_name,$li->db,$questionAry[$i]['question']);
		$sql = 'update question set question = "'.$_question.'" where question_id = "'.$questionAry[$i]['question_id'].'"';
		$result = $li->db_db_query($sql);
		if($result){
			$success_no += 1;
		}else{
			$failQuestionAry[] = $questionAry[$i]['question_id'];
		}
	}
	echo "No. of Questions updated : ".$success_no."<br/>";
	echo "Questions failed to update : ".(count($failQuestionAry)>0?implode(" , ",$failQuestionAry):0);
}
intranet_closedb();


?>