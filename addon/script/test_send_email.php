<?php

/*
 * @Params: 
 * to : receiver email address
 * subject : [optional] Email subject. Default subject is "Testing email sent on YYYY-MM-DD hh:mm:ss".
 * message : [optional] Email message. If not set would be same as subject.
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libemail.php");
include_once($PATH_WRT_ROOT."includes/libwebmail.php");
include_once($PATH_WRT_ROOT."includes/libsendmail.php");
include("../check.php");

$libdb = new libdb();

if(!isset($_REQUEST['to']) || !$libdb->validateEmail($_REQUEST['to'])){
	echo "Invalid receiver email address.";
	exit;
}

echo "[BEGIN][".date('Y-m-d H:i:s')."]<br />\n";

intranet_opendb();

$lsendmail = new libsendmail();
$webmaster = get_webmaster();

$headers = "From: $webmaster\r\n";

$to = $_REQUEST['to'];

$subject = isset($_REQUEST['subject'])? $_REQUEST['subject'] : "Testing email sent on ".date("Y-m-d H:i:s");
$message = isset($_REQUEST['message'])? $_REQUEST['message'] : $subject;

if($_REQUEST['phpmail'] == 1){
	$success = mail($to,$subject,$message,$headers);
}else{
	$success = $lsendmail->SendMail($to, $subject, $message,"$headers");
	$success = $success == ""? true : false;
}

if($success){
	echo '<p style="color:green">Email was sent to '.$to.'</p>'."\n";
}else{
	echo '<p style="color:red">Failed to send email to '.$to.'.</p>'."\n";
}

intranet_closedb();

echo "[END][".date('Y-m-d H:i:s')."]<br />\n";
?>