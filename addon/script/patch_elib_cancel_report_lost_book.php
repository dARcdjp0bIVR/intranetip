<?php
/*
 * 	Purpose: cancel report lost book by BarCode, allow to cancel record which RecordStatus is OUTSTANDING only
 * 
 *  2017-11-01 [Cameron] create this file [case #F130172]
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
$PATH_WRT_ROOT = $path;
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");
include_once($path."includes/libelibrary.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$liblms = new liblms();
$ret = array();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	$form = "<form method='post'>
				<input value='run script' type='submit'>
				<input type='hidden' name='Flag' value=1>
				BarCode <input type=\"text\" name=\"BarCode\">
			</form>";
	echo $form;
}
else {
	$isApply = true;
}

if ($BarCode) {
	 
	$sql = "select  
						bu.UniqueID, 
						b.BookID,
						br.BorrowLogID,
						br.UserID,
						d.OverdueLogID,
					 	d.Payment,
						u.Balance
			from 
						LIBMS_BOOK b 
			inner join 
						LIBMS_BOOK_UNIQUE bu on bu.BookID=b.BookID
			inner join
						LIBMS_BORROW_LOG br on br.UniqueID=bu.UniqueID and br.RecordStatus='LOST'
			inner join
						LIBMS_OVERDUE_LOG d on d.BorrowLogID=br.BorrowLogID
			inner join
						LIBMS_USER u on u.UserID=br.UserID
 			where 
						bu.BarCode='".$liblms->Get_Safe_Sql_Query($BarCode)."'
			and			d.RecordStatus='OUTSTANDING'
			";

	$rs = $liblms->returnResultSet($sql);
	$nrRec = count($rs);

	debug_pr($sql);
	debug_pr($rs);


	if (count($rs)) {
		$rs = current($rs);
		$uniqueID = $rs['UniqueID'];
		$bookID = $rs['BookID'];
		$borrowLogID = $rs['BorrowLogID'];
		$userID = $rs['UserID'];
		$overdueLogID = $rs['OverdueLogID'];
		$payment = $rs['Payment'];
		$payment = ($payment == '') ? 0: $payment;
		
		$liblms->Start_Trans();

		# 1. delete overdue log
		$sql = "DELETE FROM LIBMS_OVERDUE_LOG WHERE OverDueLogID='".$overdueLogID."'";
		$ret[] = $liblms->db_db_query($sql);
		
		# 2. update Balance of the user
		$sql = "UPDATE LIBMS_USER SET Balance=Balance+$payment WHERE UserID='$userID'";
		$ret[] = $liblms->db_db_query($sql);
		
		# 3. update LIBMS_BOOK_UNIQUE.RecordStatus
		$sql = "UPDATE LIBMS_BOOK_UNIQUE SET RecordStatus='BORROWED' WHERE UniqueID='$uniqueID'";
		$ret[] = $liblms->db_db_query($sql);
		
		# 4. update NoOfCopy in LIBMS_BOOK
		$sql = "UPDATE LIBMS_BOOK SET NoOfCopy=NoOfCopy+1 WHERE BookID='$bookID'";
		$ret[] = $liblms->db_db_query($sql);
		 
		# 5. update RecordStatus in LIBMS_BORROW_LOG
		$sql = "UPDATE LIBMS_BORROW_LOG SET RecordStatus='BORROWED' WHERE BorrowLogID='$borrowLogID'";
		$ret[] = $liblms->db_db_query($sql);
		
		# 6. update Balance Log
		$balanceLogAry = array();
		$balanceLogAry['UserID'] = PHPToSQL($userID);
		$balanceLogAry['BalanceBefore'] = PHPToSQL($rs['Balance']);
		$balanceLogAry['TransAmount'] = PHPToSQL($payment);
		$balanceLogAry['BalanceAfter'] = PHPToSQL($rs['Balance']+$payment);
		$balanceLogAry['TransDesc'] = PHPToSQL('cancel lost book');
		$balanceLogAry['TransType'] = PHPToSQL('OverdueLog');
		$balanceLogAry['TransRefID'] = PHPToSQL($overdueLogID);
		$ret[] = $liblms->INSERT2TABLE('LIBMS_BALANCE_LOG',$balanceLogAry);
		unset($balanceLogAry);
		
		# 7. sync to INTRANET_ELIB_BOOK
		if ($bookID) {
			$lbelib = new elibrary();
			$lbelib->SYNC_PHYSICAL_BOOK_TO_ELIB($bookID, true);
		}
		
	   	if (!in_array(false,$ret)) {
	   		$liblms->Commit_Trans();
	   	}
	   	else {
	   		$liblms->RollBack_Trans();
	   	}
	}
}	// end BarCode

if($isApply){
	$result = (count($ret) && !in_array(false,$ret)) ? "<span style=\"color:green; font-weight:bold\">cancel report lost book success!</span>" : "<span style=\"color:red; font-weight:bold\">Fail to cancel report lost book !</span>";
	if ($FromCentralUpdate) {
		$x .= $result."\r\n";
	}
	else {
		echo $result;				
	}
}
if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>