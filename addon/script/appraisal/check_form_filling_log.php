<?php
$PATH_WRT_ROOT = ($PATH_WRT_ROOT!="") ? $PATH_WRT_ROOT : "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
if (!$CallFromSameDomain)
{
	include_once($eclass_filepath.'/addon/check.php');
}

include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libinterface.php");
include_once($PATH_WRT_ROOT."lang/appraisal_lang.$intranet_session_language.php");
include_once($PATH_WRT_ROOT."includes/appraisal/appraisalConfig.inc.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal.php");
include_once($PATH_WRT_ROOT."includes/appraisal/libappraisal_ui.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libdbtable.php");
include_once($PATH_WRT_ROOT."includes/libdbtable2007a.php");
include_once($PATH_WRT_ROOT."includes/libtimetable.php");
include_once($PATH_WRT_ROOT.'includes/json.php');

$indexVar['libappraisal'] = new libappraisal();
$indexVar['libappraisal_ui'] = new libappraisal_ui();
$indexVar['curUserId'] = $_SESSION['UserID'];
$json = new JSON_OBJ;
intranet_opendb();

// ============================== Includes files/libraries ==============================
// ============================== Transactional data ==============================
//$order = ($order == '') ? 1 : $order;	// 1 => asc, 0 => desc
//$field = ($field == '') ? 7 : $field;
//$pageNo = ($pageNo == '') ? 1 : $pageNo;
//$page_size = ($numPerPage == '') ? 10 : $numPerPage;
//$li = new libdbtable2007($field, $order, $pageNo);
//$li->field_array = array("FormName","StfGrpDescr","IdvArrRmk","MyRole","FormFillAndOrder","SubDate","Action","DisplayOrder");
//$li->field_array = array("LogID","RlsNo","RecordID","BatchID","Function","RecordType","RecordDetail","LogDate","LogName");

// ========================  Filter Setup ===================== //
$typeList = $indexVar['libappraisal']->returnVector("SELECT DISTINCT RecordType FROM INTRANET_PA_T_FILLING_LOG GROUP BY RecordType");
$functionList = $indexVar['libappraisal']->returnVector("SELECT DISTINCT Function FROM INTRANET_PA_T_FILLING_LOG GROUP BY Function");

$_POST['start_date'] = ($_POST['start_date']=='')?date('Y-m-d'):$_POST['start_date'];

$filters = array();
if ($keyword !== '' && $keyword !== null) {
	$condsKeyword = " WHERE 1=1";
}
if($_POST['type']!='' && in_array($_POST['type'], $typeList)){
	$filters[] = " log.RecordType='".$_POST['type']."' ";
}
if($_POST['function']!='' && in_array($_POST['function'], $functionList)){
	$filters[] = " log.Function='".$_POST['function']."' ";
}
if($_POST['start_date']!=''){
	$filters[] = " log.LogDate >='".$_POST['start_date']." 00:00:00' ";
}
if($_POST['end_date']!=''){
	$filters[] = " log.LogDate <='".$_POST['end_date']." 23:59:59' ";
}
// ========================  Filter Setup ===================== //

$logContent = $indexVar['libappraisal']->getFormFillingLog($filters);
//echo $li->sql."<br/><br/>";

//$li->no_col = sizeof($li->field_array);
//$li->IsColOff = "IP25_table";
//$li->count_mode = 1;

$pos = 0;
$table .= "<table class='common_table_list_v30 '>";
$table .= "<tr>";
$table .= "<th width='1' class='tabletoplink'>#</th>\n";
$table .= "<th width='10%' >Form</th>\n";
$table .= "<th width='10%' >Function</th>\n";
$table .= "<th width='10%' >RecordType</th>\n";
$table .= "<th width='50%' >RecordDetail</th>\n";
$table .= "<th width='10%' >LogDate</th>\n";
$table .= "<th width='10%' >LogName</th>\n";
$table .= "</tr>";
foreach($logContent as $idx=>$log){
	$table .= "<tr>";
	$table .= "<td>".($idx+1)."</td>";
	
	$sql = "SELECT ".$indexVar['libappraisal']->getLangSQL("FrmTitleChi","FrmTitleEng")." as FrmTitle,".$indexVar['libappraisal']->getLangSQL("FrmCodChi","FrmCodEng")." as FrmCode,".$indexVar['libappraisal']->getLangSQL("SecTitleChi","SecTitleEng")." as SecTitle,".$indexVar['libappraisal']->getLangSQL("SecCodChi","SecCodEng")." as SecCode FROM INTRANET_PA_T_FRMSUB frmsub
			INNER JOIN INTRANET_PA_T_FRMSUM frmsum ON frmsub.RecordID=frmsum.RecordID
			INNER JOIN INTRANET_PA_S_FRMTPL frmtpl ON frmsum.TemplateID = frmtpl.TemplateID
			INNER JOIN INTRANET_PA_S_FRMSEC frmsec ON frmsub.SecID = frmsec.SecID
			 WHERE frmsub.RlsNo='".$log['RlsNo']."' AND frmsub.RecordID='".$log['RecordID']."' AND frmsub.BatchID='".$log['BatchID']."'";
	$frmNameRecord = current($indexVar['libappraisal']->returnArray($sql));
	$displayFormName = $frmNameRecord['FrmTitle'];
	$displayFormName .= ($frmNameRecord['FrmCode']=="")?"":" - ".$frmNameRecord['FrmCode'];
	$displayFormName .= ($frmNameRecord['SecTitle']=="")?"":" ".$frmNameRecord['SecTitle'];
	$displayFormName .= ($frmNameRecord['SecCode']=="")?"":" - ".$frmNameRecord['SecCode'];
	$displayFormName .= "<br>(RlsNo: ".$log['RlsNo'].";<br> RecordID: ".$log['RecordID'].";<br> BatchID: ".$log['BatchID'].")";
	$table .= "<td>".$displayFormName."</td>";
	
	$table .= "<td>".$log['Function']."</td>";
	$table .= "<td>".$log['RecordType']."</td>";	
	$detailDisplay = "";
	if($log['RecordDetail']=="UNCHANGED"){
		$detailDisplay = $log['RecordDetail'];
	}else{
		$detailDisplayArr = $json->decode($log['RecordDetail']);
		foreach($detailDisplayArr as $detail){
			$detailDisplay .= $detail['DisplayName'].": <br>";
			
			if(isset($detail['MapWithTable'])){
				if(isset($detail['Original'])){
					if(is_array($detail['Original'])){
						$detailDisplay .= "Original: <br>";
						foreach($detail['Original'] as $ori){
							$sql = "SELECT * FROM ".$detail['MapWithTable']." WHERE ".$detail['Parameter']."='".$ori."'";
							$relateData = current($indexVar['libappraisal']->returnArray($sql));
							if(!empty($relateData)){
								$detailDisplay .= $relateData[$detail['MapDisplay']]." (".$detail['Parameter'].": ".$ori.")<br>";
							}
						}
					}else{
						$sql = "SELECT * FROM ".$detail['MapWithTable']." WHERE ".$detail['Parameter']."='".$detail['Original']."'";
						$relateData = current($indexVar['libappraisal']->returnArray($sql));
						if(!empty($relateData)){
							$detailDisplay .= "Original: ".$relateData[$detail['MapDisplay']]." (".$detail['Parameter'].": ".$detail['Original'].")<br>";
						}
					}
				}
				if(isset($detail['New'])){
					if(is_array($detail['New'])){
						$detailDisplay .= "Modified to: <br>";
						foreach($detail['New'] as $new){
							$sql = "SELECT * FROM ".$detail['MapWithTable']." WHERE ".$detail['Parameter']."='".$new."'";
							$relateData = current($indexVar['libappraisal']->returnArray($sql));
							if(!empty($relateData)){
								$detailDisplay .= $relateData[$detail['MapDisplay']]." (".$detail['Parameter'].": ".$new.")<br>";
							}
						}
					}else{
						$sql = "SELECT * FROM ".$detail['MapWithTable']." WHERE ".$detail['Parameter']."='".$detail['New']."'";
						$relateData = current($indexVar['libappraisal']->returnArray($sql));
						if(!empty($relateData)){
							$detailDisplay .= "Modified to: ".$relateData[$detail['MapDisplay']]." (".$detail['Parameter'].": ".$detail['New'].")<br>";
						}
					}
				}
			}else{
				if(isset($detail['Original'])){
					$detailDisplay .= "Original: ".$detail['Original']."<br>";
				}
				if(isset($detail['New'])){
					$detailDisplay .= "Modified to: ".$detail['New']."<br>";
				}
				if(isset($detail['Message'])){
					$detailDisplay .= $detail['Message']."<br>";
				}
			}
			$detailDisplay .= "<br>";
		}
	}
	
	$table .= "<td>".$detailDisplay."</td>";
	$table .= "<td>".$log['LogDate']."</td>";
	$table .= "<td>".$log['LogName']."<br>(UserID: ".$log['LogBy'].")</td>";
	$table .= "</tr>";
}
$table .= "</table>";
intranet_closedb();
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<script language="JavaScript" src="/templates/jquery/jquery-1.3.2.min.js"></script>
<link href="/templates/2009a/css/content_30.css" rel="stylesheet" type="text/css">
<form name="form1" id="form1" method="POST">
<div id="filter">
	<span class="filter-box">
		<span>Function:</span>
		<select id="function" name="function">
			<option value="" <?php echo ($_POST['function']=='')?"selected":""?>>All</option>
			<?php
			foreach($functionList as $function){
				$selectedText = ($_POST['function']==$function)?"selected":"";
			?>					
			<option value="<?php echo $function?>" <?php echo $selectedText?>><?php echo $function?></option>
			<?php
			}
			?>
		</select>
	</span>&nbsp;
	<span class="filter-box">
		<span>RecordType:</span>
		<select id="type" name="type">
			<option value="" <?php echo ($_POST['type']=='')?"selected":""?>>All</option>
			<?php
			foreach($typeList as $type){
				$selectedText = ($_POST['type']==$type)?"selected":"";
			?>					
			<option value="<?php echo $type?>" <?php echo $selectedText?>><?php echo $type?></option>
			<?php
			}
			?>
		</select>
	</span>&nbsp;
	<span class="filter-box">
		<span>Date range:</span>
		<input type="date" name="start_date" id="start_date" value='<?php echo $_POST['start_date']?>'> - 
		<input type="date" name="end_date" id="end_date" value='<?php echo $_POST['end_date']?>'> 
		<input type="submit" value="Search">
	</span>
</div><br>
<?php 
echo $table;
?>
</form>
<script>
	$(document).ready(function(){
		$('#type').change(function(){
			$('#form1').submit();
		});
		$('#function').change(function(){
			$('#form1').submit();
		});
		$('#cycle').change(function(){
			$('#form1').submit();
		});
	});
</script>