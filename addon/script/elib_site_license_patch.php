<?php
/**
 * Note: 
 * 		- DO NOT run script for "New Clients" since ALL ebooks will have Pusblish = 1
 * 		- Run this script ONCE only for clients before 2010-04-30
 * 
 * If school license is by "SITE", create "Book enabled" records for each book that has Publish = 1
 * 
 * After this script has been applied successfully, the action will be recorded in a txt file in :
 * 		- file/elibrary/admin_install_patch.txt (File not exist/empty = Has not run patch yet) 
 *   
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/subject_class_mapping.php");

include_once($PATH_WRT_ROOT."lang/lang.$intranet_session_language.php");
include('../check.php');

intranet_opendb();
########################################################################

$readonly 	= false;
$ResultArr 	= array();
$theData	= "";

## NOT Site license
if($plugin['eLib_license'] != 1){
	echo "No updates required";
	die();
}

//$myFile = "elib_license_patch_history.txt";
$myFile = $PATH_WRT_ROOT."file/elibrary/admin_install_patch.txt";
## Create elibrary directory if does not exists
if(!is_dir($PATH_WRT_ROOT."file/elibrary")){
	mkdir($PATH_WRT_ROOT."file/elibrary", 0755);
}
 
if(file_exists($myFile)){	
	$fh = fopen($myFile, 'r');
	if(filesize($myFile) > 0){
		$theData = fread($fh, filesize($myFile));
	}
	fclose($fh);
}else{	
	$fh = fopen($myFile, 'w+');	
	chmod($myFile, 0777);
}


if(trim($theData) != ""){
	echo "Client has applied this patch already";
	die();
}
	
$li = new libdb();

if($readonly){
	echo "<BR /><B>READ ONLY</b><BR /><BR />";
}else{
	echo "<BR /><B>DB UPDATE</b><BR /><BR />";
}

$uid = $_SESSION['UserID'];

## get all published books 
$sql = "SELECT
			BookID
		FROM
			INTRANET_ELIB_BOOK
		WHERE
			Publish = 1";
$aryPublishedBooks = $li->returnVector($sql);
	
$li->Start_Trans();	
echo "<b>Enable Published Books</b>: <BR />";
## insert book enabled for each already published book
if($aryPublishedBooks != array()){
	foreach($aryPublishedBooks as $i=>$BookID){
		$sql = "INSERT INTO INTRANET_ELIB_BOOK_ENABLED (BookID, InputDate)VALUES (".$BookID.",  NOW())";
					
		if(!$readonly){			
			$is_succ = $li->db_db_query($sql);
			array_push($ResultArr, $is_succ);
			
			if($is_succ){
				echo $is_succ? "<b>[Yes]</b>" : "<b>[NO]</b>";;	
			}			
		}
		echo $sql."<BR />";						
	}
	
	
	
	if (in_array(false, $ResultArr) == true || $readonly){
		$li->RollBack_Trans();
	}else{
		$li->Commit_Trans();
		
		$fh = fopen($myFile, 'a+');
		fwrite($fh, "Patched on : ".Date("Y-m-d H:i:s"));			
		fclose($fh);		
		/*echo "<BR />Update ALL eBook<BR />";
		## Update all ebook Publish = 1
		$sql = "UPDATE INTRANET_ELIB_BOOK set Publish = 1,DateModified=NOW()";
		
		if(!$readonly){
			if($li->db_db_query($sql)){
				echo "<b>[Yes]</b>";		
				$li->Commit_Trans();
				//$li->RollBack_Trans();
			 
			 	
				$fh = fopen($myFile, 'a+');
				fwrite($fh, "Patched on : ".Date("Y-m-d H:i:s"));			
				fclose($fh);			
			}		
		}
		echo $sql;*/
	}
}else{
	## No Books but applied patch
	$fh = fopen($myFile, 'a+');
	fwrite($fh, "Patched on : ".Date("Y-m-d H:i:s"));			
	fclose($fh);
}

########################################################################
intranet_closedb();
?>