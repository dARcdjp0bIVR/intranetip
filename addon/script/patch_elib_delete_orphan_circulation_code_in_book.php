<?php
/*
 * 	Purpose: delete CirculationTypeCode in LIBMS_BOOK and LIBMS_BOOK_UNIQUE if the code not exists in LIBMS_CIRCULATION_TYPE [case #Q110998]
 * 
 *  2017-01-06 [Cameron] create this file
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$liblms = new liblms();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}


## 1. get CirculationTypeCode from LIBMS_BOOK, the code does not exist in LIBMS_CIRCULATION_TYPE 
$sql = "SELECT 
				DISTINCT CirculationTypeCode 
		FROM 
				LIBMS_BOOK 
		WHERE 
				CirculationTypeCode IS NOT NULL 
		AND 	CirculationTypeCode<>'' 
		AND 	CirculationTypeCode NOT IN (SELECT CirculationTypeCode FROM LIBMS_CIRCULATION_TYPE)
		ORDER BY CirculationTypeCode";
debug_r($sql);
$rs = $liblms->returnResultSet($sql);
$nrRec = count($rs);
$error = array();

if ($nrRec>0) {
	for($i=0;$i<$nrRec;$i++) {
		$r = $rs[$i];
		print '<span style="font-weight:bold">Book CirculationTypeCode = '.$r['CirculationTypeCode'].'</span><br>';
		
		$sql = "SELECT 
						BookID, BookTitle FROM LIBMS_BOOK 
				WHERE 
						CirculationTypeCode='".$liblms->Get_Safe_Sql_Query($r['CirculationTypeCode'])."'
				ORDER BY BookID, BookTitle";
		$books = $liblms->returnResultSet($sql);
debug_r($sql);		
		foreach((array) $books as $book) {
			print 'BookID = '.$book['BookID'] . ' BookTitle = ' . $book['BookTitle'].'<br>';
		}

		if($isApply){
			$sql = "UPDATE LIBMS_BOOK SET CirculationTypeCode='' WHERE CirculationTypeCode='".$liblms->Get_Safe_Sql_Query($r['CirculationTypeCode'])."'";
debug_r($sql);						
			$res = $liblms->db_db_query($sql);
			if (!$res) {
				$error[] = "UPDATE LIBMS_BOOK error CirculationTypeCode = ".$r['CirculationTypeCode'];
			}
		}
	}	// end for
	
	if($isApply){
		if (empty($error)) {
			echo "<span style=\"color:green; font-weight:bold\">update CirculationTypeCode in book success</span><br>";
		}
		else {
			echo "<span style=\"color:red; font-weight:bold\">update CirculationTypeCode in book fail</span><br>";
			print_r($error);
		}
	}
}
else {
	print 'No orphan book record<br>';
}


## 2. get CirculationTypeCode from LIBMS_BOOK_UNIQUE, the code does not exist in LIBMS_CIRCULATION_TYPE 
$sql = "SELECT 
				DISTINCT CirculationTypeCode 
		FROM 
				LIBMS_BOOK_UNIQUE 
		WHERE 
				CirculationTypeCode IS NOT NULL 
		AND 	CirculationTypeCode<>'' 
		AND 	CirculationTypeCode NOT IN (SELECT CirculationTypeCode FROM LIBMS_CIRCULATION_TYPE)
		ORDER BY CirculationTypeCode";
debug_r($sql);
$rs = $liblms->returnResultSet($sql);
$nrRec = count($rs);
$error = array();
if ($nrRec>0) {
	for($i=0;$i<$nrRec;$i++) {
		$r = $rs[$i];
		print '<span style="font-weight:bold">Book Item CirculationTypeCode = '.$r['CirculationTypeCode'].'</span><br>';
		
		$sql = "SELECT 
						bu.ACNO, b.BookTitle FROM LIBMS_BOOK b 
				INNER JOIN LIBMS_BOOK_UNIQUE bu ON bu.BookID=b.BookID
				WHERE 
						bu.CirculationTypeCode='".$liblms->Get_Safe_Sql_Query($r['CirculationTypeCode'])."'
				ORDER BY bu.ACNO, b.BookTitle";
debug_r($sql);				
		$books = $liblms->returnResultSet($sql);
		foreach((array) $books as $book) {
			print 'ACNO = '.$book['ACNO'] . ' BookTitle = ' . $book['BookTitle'].'<br>';
		}

		if($isApply){
			$sql = "UPDATE LIBMS_BOOK_UNIQUE SET CirculationTypeCode='' WHERE CirculationTypeCode='".$liblms->Get_Safe_Sql_Query($r['CirculationTypeCode'])."'";			
debug_r($sql);
			$res = $liblms->db_db_query($sql);
			if (!$res) {
				$error[] = "UPDATE LIBMS_BOOK_UNIQUE error CirculationTypeCode = ".$r['CirculationTypeCode'];
			}
		}
	}	// end for
	
	if($isApply){
		if (empty($error)) {
			echo "<span style=\"color:green; font-weight:bold\">update CirculationTypeCode in book item success</span><br>";
		}
		else {
			echo "<span style=\"color:red; font-weight:bold\">update CirculationTypeCode in book item fail</span><br>";
			print_r($error);
		}
	}
}
else {
	print 'No orphan book item record<br>';
}


if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>