<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

## Prompt login
include_once("../../check.php");

eclass_opendb();

$course_code = 'gvlistening';
$course_name = 'Listening iTextbook';
$course_desc = 'Listening iTextbook Classroom';
$max_user = "NULL";
$max_storage = "NULL";

$lo = new libeclass();

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 7;
if($course_id=='')
{
	$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
	$lo->eClassSubjectUpdate($subj_id, $course_id);
	echo 'Listening iTextbook Classroom created!<br/>';
}
else
{
	echo 'Listening iTextbook Classroom already created!<br/>';
}

# Create iTextbook Record
$sql = "SELECT COUNT(*) FROM ".$intranet_db.".ITEXTBOOK_BOOK WHERE BookType = 'gvlistening' AND Status = 0";
$NoOfBook = current($lo->returnVector($sql));

if($NoOfBook==0){
	$sql = "SELECT UserID FROM ".$intranet_db.".INTRANET_USER WHERE UserLogin = 'broadlearning'";
	$UserID = current($lo->returnVector($sql));
	
	$sql = "INSERT INTO
				".$intranet_db.".ITEXTBOOK_BOOK
				(BookType, BookName, BookLang, Chapters, Status, DateInput, DateModified, ModifiedBy)
			VALUES
				('gvlistening', 'Listening iTextbook', 'en', '1', 0, NOW(), NOW(), '$UserID')";
	echo $lo->db_db_query($sql)? "<br/>Listening iTextbook Record Created!":"<br/>Failed to create Listening iTextbook Record";
}	

eclass_closedb();

?>