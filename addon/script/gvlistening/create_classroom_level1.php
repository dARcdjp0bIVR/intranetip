<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

## Prompt login
include_once("../../check.php");

eclass_opendb();

$course_code = 'gvlistening';
$course_name = 'Global Voices 1';
$course_desc = 'Global Voices 1 Classroom';
$max_user = "NULL";
$max_storage = "NULL";

$lo = new libeclass();

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 7;
if($course_id=='')
{
	$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
	$lo->eClassSubjectUpdate($subj_id, $course_id);
	echo 'Global Voices 1 created!<br/>';
}
else
{
	echo 'Global Voices 1 already created!<br/>';
}

# Create iTextbook Record
$sql = "SELECT COUNT(*) FROM ".$intranet_db.".ITEXTBOOK_BOOK WHERE BookType = 'gvlistening' AND Status = 0";
$NoOfBook = current($lo->returnVector($sql));

if($NoOfBook==0){
	$sql = "SELECT UserID FROM ".$intranet_db.".INTRANET_USER WHERE UserLogin = 'broadlearning'";
	$UserID = current($lo->returnVector($sql));
	
	$sql = "INSERT INTO
				".$intranet_db.".ITEXTBOOK_BOOK
				(BookType, BookName, BookLang, Chapters, Status, DateInput, DateModified, ModifiedBy)
			VALUES
				('gvlistening', 'Global Voices 1', 'en', '1', 0, NOW(), NOW(), '$UserID')";
	echo $lo->db_db_query($sql)? "<br/>Global Voices 1 Record Created!":"<br/>Failed to create Global Voices 1 Record";
}	
$lo->db = $eclass_prefix."c".$course_id;		
$sql = "CREATE TABLE IF NOT EXISTS `gvlistening_unit_settings` (
			`record_id` int(8) NOT NULL auto_increment,
			`year_id` int(8) default NULL,
			`academic_year_id` int(8) default NULL,
			`can_access` int(8) default NULL,
			`unit_id` int(8) default NULL,
			`start_date` datetime default NULL,
			`end_date` datetime default NULL,
			`ans_date` datetime default NULL,
			`transcript_date` datetime default NULL,
			`last_modified_by` int(8) default NULL,
			`last_modified_time` datetime default NULL,			
			PRIMARY KEY  (`record_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'gvlistening_unit_settings' created!":"<br/>Table - 'gvlistening_unit_settings' failed to create!";
		
$sql = "CREATE TABLE IF NOT EXISTS `gvlistening_general_settings` (
			`setting_id` int(8) NOT NULL auto_increment,
			`setting_name` varchar(255) NOT NULL,
			`setting_value` varchar(255) NOT NULL,
			`academic_year_id` int(8) default NULL,
			`last_modified_by` int(8) default NULL,
			`last_modified_time` datetime default NULL,			
			PRIMARY KEY  (`setting_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'gvlistening_general_settings' created!":"<br/>Table - 'gvlistening_general_settings' failed to create!";


eclass_closedb();

echo "<br/><br/><strong>Target Database Name : ".$eclass_prefix."c".$course_id."</strong><br/>";

?>