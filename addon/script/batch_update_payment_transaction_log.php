<?
/*
 * 	Error : wrongly store the Payment Item ID to "PAYMENT_OVERALL_TRANSACTION_LOG > RelatedTransactionID"
 * 
 *  Correct : should store "PAYMENT_PAYMENT_ITEMSTUDENT > PaymentID" to "PAYMENT_OVERALL_TRANSACTION_LOG > RelatedTransactionID"
 */

$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/lib.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");


intranet_opendb();
$libdb = new libdb();

$sql = "SELECT ItemID FROM PAYMENT_PAYMENT_ITEM WHERE NoticeID IS NOT NULL";
$PaymentItemIdAry = $libdb->returnVector($sql);

$NoOfPaymentItem = count($PaymentItemIdAry);

$a = 0;
$flag = array();

for($i=0; $i<$NoOfPaymentItem; $i++) {
	$paymentItemId = $PaymentItemIdAry[$i];
	
	$sql = "SELECT PaymentID, ItemID, StudentID, DateInput FROM PAYMENT_PAYMENT_ITEMSTUDENT WHERE ItemID='$paymentItemId'";
	$transactionInfo = $libdb->returnArray($sql);
	//echo count($transactionInfo).'<br>';
	$NoOfItemStudent = count($transactionInfo);
	
	for($j=0; $j<$NoOfItemStudent; $j++) {
		list($payId, $itemId, $student_id, $dateinput) = $transactionInfo[$j];
		
		$sql = "SELECT StudentID, RelatedTransactionID, Details FROM PAYMENT_OVERALL_TRANSACTION_LOG WHERE StudentID='$student_id' AND RelatedTransactionID='$itemId' AND TransactionTime='$dateinput'";
		$result = $libdb->returnArray($sql);
		//echo count($result).'<br>';
		if(count($result)>0) {
			$sql = "UPDATE PAYMENT_OVERALL_TRANSACTION_LOG SET RelatedTransactionID='$payId' WHERE StudentID='$student_id' AND RelatedTransactionID='$itemId' AND TransactionTime='$dateinput'";
			$flag[] = $libdb->db_db_query($sql);	
			//debug_pr($result);
			$a++;
		}
	}
}

echo "No. of ItemID from Payment Notice : ".$NoOfPaymentItem."<br>";
echo "No. of transcation log revised : ".$a;

//debug_pr($flag);
intranet_closedb();
?>