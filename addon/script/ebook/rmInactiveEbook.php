<?php
/*
 * Editing by  Paul
 * 
 */
$PATH_WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_opendb();
$li = new libdb();

$logresult = 1; // 1: success, 0: contains on progress book selection, 9: unexpected error taken place

# conditions for running the script
$internalSiteArray = array();
// MAC address of site which does not require the script to remove books
//array_push($internalSiteArray, "00:16:3E:53:E4:6A", "00:13:4E:3A:6A:52", "00:16:3E:10:62:86"); // 146 sites, 149 site, eclasscloud
$internalSiteArray = array(
								"00:16:3E:53:E4:6A",	// 146 sites 
								"00:13:4E:3A:6A:52", 	// 149 site
								"00:16:3E:10:62:86"		// eclasscloud
						  ); 
$sqlLastModify = 'SELECT RunDate FROM GENERAL_LOG WHERE Process="Remove Expire eBook" ORDER BY RunDate DESC LIMIT 1';
$lastRun = $li->returnArray($sqlLastModify);

$con1 = (empty($lastRun))?true:(date('j') === '1'); // check if it is the first day of a month

$con2 = (($plugin['library_management_system'] == true)&&(($plugin['eLib'] == true)||($plugin['eLib_Lite'] == true))); 
// check if eLib+ is installed

// Check MAC Address
exec("/sbin/ifconfig | grep Ethernet",$output_lines);

$mac_matched = false;
for ($i=0; $i<sizeof($output_lines); $i++){
	$MACaddr = substr($output_lines[$i], -17);
	if(in_array($MACaddr, $internalSiteArray)){
		$mac_matched = true;
	}
}
// prevent the script from deleting the core eBook data in 149 or other internal site
$con3 = !(isset($sys_custom['ebook']['disableRemovalEbook']) || 
			in_array($irc_MAC, $internalSiteArray)|| 
			$mac_matched ); // check if internal site

// If has run already today, prevent running again
if(!empty($lastRun)){
	$con4 = (date("Y-m-d")==date("Y-m-d",strtotime($lastRun[0][0])))?false:true;
}else{
	$con4 = true;
}

$isSchoolBook = "761, 764, 767, 768, 769, 770, 771, 773, 775, 776, 777, 778, 779, 780, 781, 782, 783, 785, 786, 787, 788, 789, 790, 791, 792, 793, 794, 795, 796, 797, 798, 921, 922, 923, 924, 925, 926, 927, 928, 929, 930, 931, 952, 957, 958, 1010, 1011, 1012, 1019, 1096, 1560, 1561, 1562, 1563, 1695, 1699, 1917, 1918, 1919, 1920, 1921, 1922, 1923, 2350";

if($con1 && $con2 && $con3 && $con4){

	# get all quotations which are not still activated if any 
	$inActiveQuotationArr = array();
	$sql = 'SELECT QuotationID FROM INTRANET_ELIB_BOOK_QUOTATION WHERE IsActivated IS NULL';
	$inActiveQuotationArr = $li->returnArray($sql);
	$DEL_LIST = ''; 

	//if (true){ //testing use only
	if (sizeof($inActiveQuotationArr) == 0){
		## case for quotations which all are activated or expried
		
		$logText = '';
		
		# get all quotations which license period is started or belong to the permanent license 
		$sql = "SELECT QuotationID FROM INTRANET_ELIB_BOOK_QUOTATION WHERE PeriodTo > NOW() OR PeriodTo='0000-00-00 00:00:00'";
		$ActiveQuotation = $li->returnVector($sql);
		
		if (empty($ActiveQuotation)){
			## case for no any quotation which is active and belongs to the permanent license 
			$sql = "SELECT BookID FROM INTRANET_EBOOK_BOOK WHERE BookID NOT IN (".$isSchoolBook.")";
			$DEL_ARR_LIST = $li->returnVector($sql);
			$DEL_LIST = implode(',', $DEL_ARR_LIST);
		}else{
			## case for some quotataions which are active and belong to the permanent license 
			
			# get all book_id of ebooks which are valid in use 
			$sql = "SELECT i.BookID FROM INTRANET_ELIB_BOOK_INSTALLATION i
					INNER JOIN INTRANET_ELIB_BOOK_QUOTATION q ON i.QuotationID=q.QuotationID AND
					(q.PeriodTo > NOW() OR q.PeriodTo = '0000-00-00 00:00:00') AND q.IsActivated='1' WHERE (i.IsEnable='1' OR IsCompulsory='1')
					ORDER BY i.BookID;";
			$activeBookList = $li->returnVector($sql);
			$activeBookList = implode(',', $activeBookList);
			$activeBookList = $activeBookList.', '.$isSchoolBook;
			
			# get all book_id of ebooks which are no longer used
			$sql = 'SELECT BookID FROM INTRANET_EBOOK_BOOK WHERE BookID NOT IN ('.$activeBookList.')';
			$DEL_ARR_LIST = $li->returnVector($sql);
			$DEL_LIST = implode(',', $DEL_ARR_LIST);		
		}
		
	//	echo 'BookList to be deleted successfully retrieved'.'<br>';
		$logText .=  "Books to be removed: ".$DEL_LIST." \n";
		
		
		try{
	//		$DEL_ARR_LIST = array('709','710'); //testing use only
			foreach($DEL_ARR_LIST as $delID){
				$path = $intranet_root.'/file/eBook/'.$delID;
				// echo '<br>'.$path.'<br>';
				if (file_exists($path)){
					$cmd = 'rm -r '.$path;
					$dum_out_1 = array();
					$dum_out_2 = array();
					//echo '<br>'.$cmd.'</br>';
					exec($cmd, $dum_out_1, $dum_out_2);
						
					if(empty($dum_out_2)){
					//	echo $path.' removal sucessful'.'<br>';	
						$logText .= "Book ID ".$delID." successfully removed.\n ";	
					}else{
					//	echo $path.' error during removal'.'<br>';
						$logText .= "Book ID ".$delID." has error during removal.\n Error code: ".$dum_out_2." \n";	
					}						
				}else{
					//echo $path.' does not exist'.'<br>';
					$logText .= "Book ID ".$delID." does not exist.\n ";
				}	
			}
		}catch(Exception $e){
		//	echo 'Error: '.$e->getMessage().'<br>';
			$logText .=  'Error: '.$e->getMessage()."\n Script terminated. Currently at Book ID ".$delID." .\n ";
			$logresult = 9; //unexpected exception occurs
		}
		$sql = 'insert into GENERAL_LOG (
								Process,
								Result,
								Remarks 
								)
							values (
								\'Remove Expire eBook\',
								\''.$logresult.'\', 
								\''.$logText.'\'
								)';
		$li->db_db_query($sql);						
	}else{
		## case if some quotations which have been installed but haven't completed the book selection process still exist
		$str = "Inactive Quotation exists. No book removal would take place.\n QuotationID: ";
		for($i=0; $i < sizeof($inActiveQuotationArr); $i++){
			$str .= $inActiveQuotationArr[$i][0]."<br>";
		}
		$sql = 'insert into GENERAL_LOG (
								Process,
								Result,
								Remarks 
								)
							values (
								\'Remove Expire eBook\',
								\'0\', 
								\''.$str.'\'
								)';
		$li->db_db_query($sql);				
	}


}
?>