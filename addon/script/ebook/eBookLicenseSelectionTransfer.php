<?php 
/*
 * Editing by  Paul
 *
 */
$PATH_WRT_ROOT = "{$_SERVER['DOCUMENT_ROOT']}/";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include("../../check.php");

intranet_opendb();
$li = new libdb();

if($flag == 1){
    $sql = "SELECT BookID,IsCompulsory,IsEnable,IsSelected FROM INTRANET_ELIB_BOOK_INSTALLATION WHERE QuotationID='".IntegerSafe($_POST['from_quotation'])."' AND (IsCompulsory=1 OR IsEnable=1 OR IsSelected=1)";
    $originalBookList = $li->returnArray($sql);
    
    $resultAry = array();
    $li->Start_Trans();
    foreach($originalBookList as $oriBook){
        $sql = "UPDATE INTRANET_ELIB_BOOK_INSTALLATION SET IsCompulsory='".$oriBook['IsCompulsory']."', IsEnable='".$oriBook['IsEnable']."', IsSelected='".$oriBook['IsSelected']."' WHERE QuotationID='".IntegerSafe($_POST['to_quotation'])."' AND BookID='".$oriBook['BookID']."' ";
        $resultAry[] = $li->db_db_query($sql);
        $sql = "UPDATE INTRANET_ELIB_BOOK SET Publish=1 WHERE BookID='".$oriBook['BookID']."'";
        $resultAry[] = $li->db_db_query($sql);
    }
    if(empty($resultAry) || in_array(false,$resultAry) || in_array(0,$resultAry)){
        $li->RollBack_Trans();
        echo 'Fail !';
    }else{
        $li->Commit_Trans();
        echo 'Done !';
    }
}else{
    $sql = "SELECT QuotationID,QuotationNumber,PeriodFrom,PeriodTo FROM INTRANET_ELIB_BOOK_QUOTATION WHERE IsActivated=1 AND Type=2 ";
    $activeQuotationList = $li->returnArray($sql);
    $sql = "SELECT QuotationID,QuotationNumber,PeriodFrom,PeriodTo FROM INTRANET_ELIB_BOOK_QUOTATION WHERE IsActivated IS NULL AND Type=2 ";
    $pendingQuotationList = $li->returnArray($sql);
    
?>
<form method="POST" action="?flag=1">
	<div>
    	<label for="from_quotation">From quotation:</label>
    	<select id="from_quotation" name="from_quotation">
    		<?php 
    		foreach($activeQuotationList as $quotation){
    		?>
    			<option value="<?php echo $quotation['QuotationID']?>"><?php echo $quotation['QuotationNumber']." (".$quotation['PeriodFrom']." - ".$quotation['PeriodTo'].")"?></option>
    		<?php
    		}
    		?>
    	</select>
	</div>
	<br><br>
	<div>
    	<label for="to_quotation">To quotation:</label>
    	<select id="to_quotation" name="to_quotation">
    		<?php 
    		foreach($pendingQuotationList as $quotation){
    		?>
    			<option value="<?php echo $quotation['QuotationID']?>"><?php echo $quotation['QuotationNumber']." (".$quotation['PeriodFrom']." - ".$quotation['PeriodTo'].")"?></option>
    		<?php
    		}
    		?>
    	</select>
	</div>
	<br><br>
	<input type="submit">
</form>
<?php 
}

intranet_closedb();
?>