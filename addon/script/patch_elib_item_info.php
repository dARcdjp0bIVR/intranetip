<?php
/*
 * Aim: patch the book info from LIBMS_BOOK to LIBMS_BOO_UNIQUE such as 
 * 			BookCode 				-> ACNO
 * 			ACNO_Prefix				-> ACNO_Prefix
 * 			ACNO_Num				-> ACNO_Num
 * 			ACNO_Bak				-> ACNO_Bak
 * 			Distributor				-> Distributor
 * 			Discount				-> Discount
 * 			PurchaseDate			-> PurchaseDate
 * 			PurchasePrice			-> PurchasePrice
 * 			PurchaseNote			-> PurchaseNote
 * 			PurchaseByDepartment	-> PurchaseByDepartment
 * 			ListPrice				-> ListPrice
 * 			LocationCode			-> LocationCode
 * 
 * Existing Data Problem: 
 * 		1. no ACNO_Prefix / ACNO_Num
 * 		2. ACNO_Num is 00000000 or 0
 * 		3. ACNO / BookCode <> CONCAT(ACNO_Prefix, ACNO_Num) such as BookID = 218
 * 		4. Encoding problem on some fields such as Distibutor
 * 		5. ACNO_Num in _BOOK table is too long
 * 
 */
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();
#########################################################################################################


## Use Library
$libdb = new liblms();


## Init 
$result = array();


## Get Data
if($_POST['Flag']!=1) {
	echo "<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>";
}


# case 1: the book is available but the borrow log is not updated!

echo "<h2>1) Patch eLibrary Book Data. </h2>";
$sql = "select 
			u.UniqueID, u.BookID, u.ACNO, u.ACNO_Prefix, u.ACNO_Num, 
			
			b.BookCode, b.BookCode_Bak, b.ACNO_Prefix as BookACNO_Prefix, b.ACNO_Num as BookACNO_Num, 
			b.PurchaseDate as BookPurchaseDate, b.Distributor as BookDistributor, b.PurchaseNote as BookPurchaseNote,
			b.PurchaseByDepartment as BookPurchaseByDepartment, b.ListPrice as BookListPrice, b.Discount as BookDiscount, 
			b.PurchasePrice as BookPurchasePrice, b.LocationCode as BookLocationCode 
		from LIBMS_BOOK_UNIQUE as u 
		inner join LIBMS_BOOK as b on 
			b.BookID = u.BookID 
		where u.ACNO = '' or u.ACNO is null 
		order by u.BookID, u.UniqueID ";
//$sql .= " limit 6500, 13000";
$itemArr = $libdb->returnArray($sql);

# get the Next Number of ACNO according to each Key into an Array  
$sql = "select `Key` as ACNO_Key, Next_Number from LIBMS_ACNO_LOG ";
$tempArr = $libdb->returnArray($sql);
$acnoKeyArr = array();
$x = '<table border="1" cellpadding="4" cellspacing="0" width="90%">';
$x .= '<tr>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th><td style="background:gray;width:2px;">&nbsp;</td>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th><td style="background:gray;width:2px;">&nbsp;</td>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th><td style="background:gray;width:2px;">&nbsp;</td>';
$x .= '<th>ACNO Prefix</th><th>Next Number</th>';
$x .= '</tr>';
if(count($tempArr) > 0){
	for ($i=0; $i<count($tempArr); $i++){
		$acnoKeyArr[$tempArr[$i]['ACNO_Key']] = $tempArr[$i]['Next_Number'];
		
		$x .= ($i % 4 == 0) ? '<tr>' : '';
		$x .= '<td width="80" align="center">'.$tempArr[$i]['ACNO_Key'].'</td>';
		$x .= '<td width="100" align="center">'.$tempArr[$i]['Next_Number'].'</td>';
		$x .= ($i % 4 == 3) ? '</tr>' : '<td style="background:gray;width:2px;">&nbsp;</td>';
	}
}
$x .= '</table><br>';
echo $x;

for ($i=0; $i<count($itemArr); $i++)
{
	
	$dataArr = array();
	
	## handle the first copy of each Book
	$UniqueID = $itemArr[$i]['UniqueID'];
	$BookID = $itemArr[$i]['BookID'];
	
	# original acno / book code 
	$orig_acno = $itemArr[$i]['BookCode'];
	
	echo '<hr>'.$i.' - BookID: '.$BookID.' - Original ACNO: '.$orig_acno.'<br>';
	
	# split ACNO into an array
	$matches = array();
	preg_match('/^(\w+)(\d+)$/', $itemArr[$i]['BookCode'], $matches);
	preg_match('/^([a-zA-Z]+)(\d+)$/',$itemArr[$i]['BookCode'], $matches);
	list($whole, $term, $number) = $matches;
//	debug_r($matches);
	
	# handle some records which does not have acno_prefx
	if($itemArr[$i]['BookACNO_Prefix'] == ''){
		$itemArr[$i]['BookACNO_Prefix'] = $term;
	}
	# check acno number is valid or not from BookCode
	if((int)$itemArr[$i]['BookACNO_Num'] != (int)$number){
		$itemArr[$i]['BookACNO_Num'] = (int)$number;
	}
	
	# prepare update data
	$dataArr['ACNO'] = $itemArr[$i]['BookCode'];
 	$dataArr['ACNO_Bak'] = $itemArr[$i]['BookCode_Bak'];
 	$dataArr['ACNO_Prefix'] = $itemArr[$i]['BookACNO_Prefix'];
 	$dataArr['ACNO_Num'] = (int)$itemArr[$i]['BookACNO_Num'];
 	if($itemArr[$i]['BookPurchaseDate'] != '0000-00-00' && $itemArr[$i]['BookPurchaseDate'] != ''){
 		$dataArr['PurchaseDate'] = $itemArr[$i]['BookPurchaseDate'];
 	}
 	if($itemArr[$i]['BookDistributor'] != ''){
 		$dataArr['Distributor'] = mysql_real_escape_string($itemArr[$i]['BookDistributor']);
 	}
 	if($itemArr[$i]['BookPurchaseNote'] != ''){
 		$dataArr['PurchaseNote'] = mysql_real_escape_string($itemArr[$i]['BookPurchaseNote']);
 	}
 	if($itemArr[$i]['BookPurchaseByDepartment'] != ''){
 		$dataArr['PurchaseByDepartment'] = mysql_real_escape_string($itemArr[$i]['BookPurchaseByDepartment']);
 	}
 	if($itemArr[$i]['BookListPrice'] != '0.00' && $itemArr[$i]['BookListPrice'] != ''){
 		$dataArr['ListPrice'] = $itemArr[$i]['BookListPrice'];
 	}
 	if($itemArr[$i]['BookDiscount'] != '0.00' && $itemArr[$i]['BookDiscount'] != ''){
 		$dataArr['Discount'] = $itemArr[$i]['BookDiscount'];
 	}
 	if($itemArr[$i]['BookPurchasePrice'] != '0.00' && $itemArr[$i]['BookPurchasePrice'] != ''){
 		$dataArr['PurchasePrice'] = $itemArr[$i]['BookPurchasePrice'];
 	}
 	if($itemArr[$i]['BookLocationCode'] != ''){
 		$dataArr['LocationCode'] = mysql_real_escape_string($itemArr[$i]['BookLocationCode']);
 	}
 	
 	$result['update_book_item_'.$UniqueID] = ITEM_UPDATE2TABLE('LIBMS_BOOK_UNIQUE', $dataArr, array("UniqueID" => $UniqueID));
 	
	for($j=$i+1 ; $j<count($itemArr) ; $j++){
		if($itemArr[$j]['BookID'] == $itemArr[$i]['BookID']){
			$UniqueID = $itemArr[$j]['UniqueID'];
			
			## handle the next copy of the same Book if having more than 1 copy
			if(isset($acnoKeyArr[$itemArr[$i]['BookACNO_Prefix']])){
				## for having next_number of acno in log table
				
				# get new acno from the Next_Number from anco_log table 
				$new_acno_num = $acnoKeyArr[$itemArr[$i]['BookACNO_Prefix']];
				
			} else {
				## for not having next_number of acno in log table
				
				$new_acno_num = $itemArr[$i]['BookACNO_Num'] + 1;
			}
			
			# reset the new Next_Number of that ACNO to be increment 1
			$acnoKeyArr[$itemArr[$i]['BookACNO_Prefix']] = $new_acno_num + 1;
			
			$dataArr['ACNO'] = $itemArr[$i]['BookACNO_Prefix'].str_pad($new_acno_num, 8, "0", STR_PAD_LEFT);
			$dataArr['ACNO_Bak'] = '';
		 	$dataArr['ACNO_Num'] = (int)$new_acno_num;
			
			$result['update_book_item_'.$UniqueID] = ITEM_UPDATE2TABLE('LIBMS_BOOK_UNIQUE', $dataArr, array("UniqueID" => $UniqueID));
		} else {
			break;
		}
	}
	
	$i = $j - 1;
	
}

	/*
	$LogObj = $LogArray[$i];
	
	$sql = "Select * From LIBMS_BORROW_LOG AS log where duedate='".$LogObj['DueDate']."' and userid='".$LogObj['UserID']."' and UniqueID='".$LogObj['UniqueID']."' order by datemodified asc";
	$SuspectedArray = $libdb->returnResultSet($sql);

	# if duplicated by 2 and the latest modified is returned
	if ( sizeof($SuspectedArray) >= 2 && $SuspectedArray[sizeof($SuspectedArray)-1]["RecordStatus"]=="RETURNED")
	{
		# remove the first duplicated record
		echo "<b>BorrowTime: ". $LogObj['BorrowTime']. " and number of records: ".sizeof($SuspectedArray)."</b>";
		debug($sql);
	
		for ($j=0; $j<sizeof($SuspectedArray)-1; $j++)
		{
			$BorrowLogID = $SuspectedArray[$j]['BorrowLogID'];
			$sql = "DELETE From LIBMS_BORROW_LOG WHERE BorrowLogID='".$BorrowLogID."' AND RecordStatus<>'RETURNED'";
			if($_POST['Flag'])
			{
				//$libdb->db_db_query($sql);
				debug("<font color='blue'>Execute: " . $sql."</font>");
			}
		}
	} else
	{
		echo "<p><font color='red'>Plese check if this record(s) need follow-up!</font></p>";
		debug_r($SuspectedArray);
	}
	*/

function ITEM_UPDATE2TABLE($table,$dataAry=array(),$condition=array())
{
	global $UserID, $libdb;
	if (!is_array($dataAry)){

		return false;
	}
	$sql = "UPDATE `{$table}` SET ";

	$b = 0;
	foreach($dataAry as $field=>$value){
		$sql .= ($b > 0) ? ", " : "";
		$sql .= "`".$field . "`= ' ". $value. "' ";
		$b++;
	}
	//$sql .= "`DateModified`=now(), `LastModifiedBy`='{$UserID}'";		// patch script - no need update modified time
	if(!empty($condition)){
		foreach($condition as $field => $value){
			$tmp_cond[] = "`{$field}` = '{$value}' ";
		}
		$sql .= " WHERE ". implode(' AND ',$tmp_cond);
	}
	echo $sql.'<br>';
//	$sql_result = $libdb->db_db_query($sql);
  
	return $sql_result;
}

echo '<style> 
html, body, table, select, input, textarea{ font-size:12px; }
</style>';

debug_r($result);

#########################################################################################################
intranet_closedb();
?>