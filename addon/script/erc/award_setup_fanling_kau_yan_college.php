<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

intranet_opendb();

$lgs = new libgeneralsettings();
$lreportcard = new libreportcardcustom();
$lreportcard_award = new libreportcard_award();

$ReportCardCustomSchoolName = "fanling_kau_yan_college";
//$ModuleName = "eRCDataPatch";
//$SettingName = "AwardItemSetup_FanlingKauYanCollege";
//$GSary = $lgs->Get_General_Setting($ModuleName);


$LogOutput = '';
if ($GSary[$SettingName] != 1 && $plugin['ReportCard2008'] && $ReportCardCustomSchoolName == "fanling_kau_yan_college") {
	$LogOutput .= "eReportCard - Award Setup for Fanling Kau Yan College [Start] ".date("Y-m-d H:i:s")."<br><br>\r\n\r\n";
	
	$ReportID = 0;	// default report settings
	//$AwardID = 0;
	$AwardID = 17;
	$DisplayOrder = 0;
	
	### 優異
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'excellent', 'AwardNameEn'=>'優異', 'AwardNameCh'=>'優異', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'ShowInReportCard'=>1, 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>54, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 良好
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'good', 'AwardNameEn'=>'良好', 'AwardNameCh'=>'良好', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'ShowInReportCard'=>1, 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>54, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 全年總成績
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'annual_academic_result', 'AwardNameEn'=>'全年總成績', 'AwardNameCh'=>'全年總成績', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'ShowInReportCard'=>0, 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>54, 'Quota'=>3);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 獎學金名單
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'annual_subject_scholarship', 'AwardNameEn'=>'獎學金名單', 'AwardNameCh'=>'獎學金名單', 'ApplicableReportType'=>'W', 'AwardType'=>'SUBJECT', 'ShowInReportCard'=>0, 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>54, 'Quota'=>3);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	debug_pr($Success);
	
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', '$SettingName', 1, now())";
	$SuccessArr['UpdateGeneralSettings'] = $lgs->db_db_query($sql);
	
	$LogOutput .= "eReportCard - Award Setup for Fanling Kau Yan College [End] ".date("Y-m-d H:i:s")."<br>";
	
	# show log
	echo $LogOutput;
	
	# save log
	$log_folder = $PATH_WRT_ROOT."file/reportcard2008/script_log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/award_setup_fanling_kau_yan_college_". date("YmdHis") .".log";
	$lf = new libfilesystem();
	$lf->file_write(strip_tags($LogOutput), $log_file);
}
?>