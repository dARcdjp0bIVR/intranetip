<?php
ini_set('display_errors',1);
error_reporting(E_ALL ^ E_NOTICE); 

// editing by 
@SET_TIME_LIMIT(0);
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libgeneralsettings.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008.php");
include_once($PATH_WRT_ROOT."includes/reportcard_custom/".$ReportCardCustomSchoolName.".php");
include_once($PATH_WRT_ROOT."includes/libreportcard2008_award.php");

intranet_opendb();

$lgs = new libgeneralsettings();
$lreportcard = new libreportcardcustom();
$lreportcard_award = new libreportcard_award();

$ModuleName = "eRCDataPatch";
$SettingName = "AwardItemSetup_PooiTo";
$GSary = $lgs->Get_General_Setting($ModuleName);

$LogOutput = '';
if ($GSary[$SettingName] != 1 && $plugin['ReportCard2008'] && $ReportCardCustomSchoolName == "pooi_to_middle_school") {
	$LogOutput .= "eReportCard - Award Setup for Pooi To [Start] ".date("Y-m-d H:i:s")."<br><br>\r\n\r\n";
	
	$ReportID = 0;	// default report settings
	$AwardID = 21;
	$DisplayOrder = 0;
	
	### 1. Diligence Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'diligence', 'AwardNameEn'=>'勤學獎', 'AwardNameCh'=>'勤學獎', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 2. Award for Excellence in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'excellence_in_subject', 'AwardNameEn'=>'各科成績優異獎', 'AwardNameCh'=>'各科成績優異獎', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 3. Award for Improvement in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'improvement_in_subject', 'AwardNameEn'=>'各科成績進步獎', 'AwardNameCh'=>'各科成績進步獎', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 4. Academic Improvement Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'academic_improvement', 'AwardNameEn'=>'學業成績進步獎', 'AwardNameCh'=>'學業成績進步獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 5. Best in Academic Improvement
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'best_in_academic_improvement', 'AwardNameEn'=>'學業成績最進步獎', 'AwardNameCh'=>'學業成績最進步獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 6. Award for Excellence Performance in (Subject)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'excellence_in_subject', 'AwardNameEn'=>'學科優秀表現獎', 'AwardNameCh'=>'學科優秀表現獎', 'ApplicableReportType'=>'T', 'AwardType'=>'SUBJECT', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 7. Outstanding Academic Award (Class)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'outstanding_award_class', 'AwardNameEn'=>'全班學期成績前列獎', 'AwardNameCh'=>'全班學期成績前列獎', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 8. Outstanding Academic Award (Form)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'outstanding_award_form', 'AwardNameEn'=>'全級成績前列獎', 'AwardNameCh'=>'全級成績前列獎', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 9. Scholarship for Outstanding Academic Result (Class)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_outstanding_award_class', 'AwardNameEn'=>'全班全年成績前列獎 (獎學金)', 'AwardNameCh'=>'全班全年成績前列獎 (獎學金)', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 10. Academic Award (Merit)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'academic_award_merit', 'AwardNameEn'=>'學業嘉許獎', 'AwardNameCh'=>'學業嘉許獎', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 11. Academic Award (Good)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'academic_award_good', 'AwardNameEn'=>'學業良好獎', 'AwardNameCh'=>'學業良好獎', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 12. Academic Award (Excellent)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'academic_award_excellent', 'AwardNameEn'=>'學業優異獎', 'AwardNameCh'=>'學業優異獎', 'ApplicableReportType'=>'T', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 13. Scholarship for Excellence in History
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_history', 'AwardNameEn'=>'歷史科優秀表現獎 － 劉小春獎學金', 'AwardNameCh'=>'歷史科優秀表現獎 － 劉小春獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 14. Scholarship for Excellence in Chinese History
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_chin_history', 'AwardNameEn'=>'中國歷史科優秀表現獎 － 盧可可校友獎學金', 'AwardNameCh'=>'中國歷史科優秀表現獎 － 盧可可校友獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 15. Scholarship for Excellence in Chinese Literature
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_chin_lit', 'AwardNameEn'=>'中國文化優才獎 － 盧可可校友獎學金', 'AwardNameCh'=>'中國文化優才獎 － 盧可可校友獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 16. Scholarship for Excellence in Science
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_science', 'AwardNameEn'=>'理科優秀表現獎 － 陳沈呂香獎學金', 'AwardNameCh'=>'理科優秀表現獎 － 陳沈呂香獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 17. Scholarship for Excellence in Economics
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_economics', 'AwardNameEn'=>'經濟科優秀表現獎 － 梁李淑儀獎學金', 'AwardNameCh'=>'經濟科優秀表現獎 － 梁李淑儀獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 18. Scholarship for Excellence in BAFS
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'scholarship_for_bafs', 'AwardNameEn'=>'企業、會計與財務概論科優秀表現獎 － 梁李淑儀獎學金', 'AwardNameCh'=>'企業、會計與財務概論科優秀表現獎品 － 梁李淑儀獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 19. Outstanding Academic Award (HKDSE)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'outstanding_award_dse', 'AwardNameEn'=>'香港中學文憑試成績優異獎', 'AwardNameCh'=>'香港中學文憑試成績優異獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 20. Outstanding Student Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'outstanding_student_award', 'AwardNameEn'=>'品學兼優獎', 'AwardNameCh'=>'品學兼優獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 21. All-round Student Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'all_round_award', 'AwardNameEn'=>'全才獎', 'AwardNameCh'=>'全才獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 22. Sir Edward Youde Memorial Fund
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'sir_edward_memorial_fund', 'AwardNameEn'=>'尤德爵士紀念基金獎學金', 'AwardNameCh'=>'尤德爵士紀念基金獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 23. The Baptist Convention of Hong Kong Outstanding Students Scholarship
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'baptist_outstanding_scholarship', 'AwardNameEn'=>'香港浸信會聯會「傑出學生」獎學金', 'AwardNameCh'=>'香港浸信會聯會「傑出學生」獎學金', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 24. Award for Excellence in Conduct
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'excellence_in_conduct', 'AwardNameEn'=>'全年操行優等獎', 'AwardNameCh'=>'全年操行優等獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 25. Youth Arch Student Improvement Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'youth_arch_improvement', 'AwardNameEn'=>'青苗學界進步獎', 'AwardNameCh'=>'青苗學界進步獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 26. Service Award
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'service_award', 'AwardNameEn'=>'服務獎', 'AwardNameCh'=>'服務獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 27. Service Award (Whole School)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'service_award_whole_sch', 'AwardNameEn'=>'全校服務獎', 'AwardNameCh'=>'全校服務獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 28. Service Award (Community)
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'service_award_community', 'AwardNameEn'=>'社群服務獎', 'AwardNameCh'=>'社群服務獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 29. Award for Learning Atmosphere Promotion
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'learning_atmosphere_promotion', 'AwardNameEn'=>'推動學風獎', 'AwardNameCh'=>'推動學風獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 30. Award for School Ethos Promotion
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'sch_ethos_promotion', 'AwardNameEn'=>'推動校風獎', 'AwardNameCh'=>'推動校風獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 31. Award for Arts
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'award_for_arts', 'AwardNameEn'=>'藝術教育獎', 'AwardNameCh'=>'藝術教育獎', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>1, 'Quota'=>0);
	$DataArr[] = array('YearID'=>2, 'Quota'=>0);
	$DataArr[] = array('YearID'=>3, 'Quota'=>0);
	$DataArr[] = array('YearID'=>4, 'Quota'=>0);
	$DataArr[] = array('YearID'=>5, 'Quota'=>0);
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	### 32. Souvenir for Post-Secondary Education Promotion
	++$AwardID;
	$DataArr = array('AwardID'=>$AwardID, 'AwardCode'=>'souvenir_for_post_secondary', 'AwardNameEn'=>'升讀專上學位課程紀念品', 'AwardNameCh'=>'升讀專上學位課程紀念品', 'ApplicableReportType'=>'W', 'AwardType'=>'OVERALL', 'SchoolCode'=>$ReportCardCustomSchoolName, 'DisplayOrder'=>++$DisplayOrder);
	$Success[$AwardID]['InsertAward'] = $lreportcard_award->Insert_Award_Record($DataArr);
	$DataArr = array();
	$DataArr[] = array('YearID'=>6, 'Quota'=>0);
	$DataArr[] = array('YearID'=>44, 'Quota'=>0);
	$Success[$AwardID]['UpdateApplicableFormInfo'] = $lreportcard_award->Update_Award_Applicable_Form_Info($AwardID, $ReportID, $DataArr);
	
	debug_pr($Success);
	
	# update General Settings - markd the script is executed
	$sql = "insert ignore into GENERAL_SETTING 
						(Module, SettingName, SettingValue, DateInput) 
					values 
						('$ModuleName', '$SettingName', 1, now())";
	$SuccessArr['UpdateGeneralSettings'] = $lgs->db_db_query($sql);
	
	$LogOutput .= "eReportCard - Award Setup for Pooi To [End] ".date("Y-m-d H:i:s")."<br>";
	
	# show log
	echo $LogOutput;
	
	# save log
	$log_folder = $PATH_WRT_ROOT."file/reportcard2008/script_log";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/award_setup_pooi_to_". date("YmdHis") .".log";
	$lf = new libfilesystem();
	$lf->file_write(strip_tags($LogOutput), $log_file);
}
?>