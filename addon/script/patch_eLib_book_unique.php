<?php
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/liblibrarymgmt.php");

intranet_auth();
intranet_opendb();

$libdb = new liblms();


if($_POST['Flag']!=1) {
	die("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}


//$successAry = array();

// Update LIBMS_BOOK
$sql = "Select
				BookID 
		From 	LIBMS_BOOK				
		Where	RecordStatus = 'DELETE'
				and BookCode is not null
		";
$BookArray = $libdb->returnResultSet($sql);										
//debug_r($BookArray);
		
for ($i=0; $i<sizeof($BookArray); $i++)
{
	$BookID = $BookArray[$i]["BookID"];
//	debug_r($BookArray);
	if ( $BookID > 0 )
	{
		//debug($BookID);
		$sql = "UPDATE LIBMS_BOOK SET BookCode_BAK=BookCode, BookCode=null WHERE BookID='$BookID' ";
		debug($sql);		
		$libdb->db_db_query($sql);
	} else
	{
		debug("Invalid BookID");
	}
}

// Update LIBMS_BOOK_UNIQUE
$sql = "Select 
				u.UniqueID,
				b.BookID				 
		From 	LIBMS_BOOK b, 
				LIBMS_BOOK_UNIQUE u 
		Where 	b.BookID=u.BookID 
				and b.RecordStatus = 'DELETE' 
				and u.BarCode is not null 
		Order by u.UniqueID
		";
$BookUniqueArray = $libdb->returnResultSet($sql);


for ($i=0; $i<sizeof($BookUniqueArray); $i++)
{
	$UniqueID = $BookUniqueArray[$i]["UniqueID"];
	$BookID = $BookUniqueArray[$i]["BookID"];
//	debug_r($BookUniqueArray);
	if ( $UniqueID > 0 )
	{
		//debug($UniqueID);
		
		$sql = "UPDATE LIBMS_BOOK_UNIQUE SET Barcode_BAK=BarCode, BarCode=null WHERE UniqueID='$UniqueID'";
		debug($sql);
		$libdb->db_db_query($sql);
	} else
	{
		debug("Invalid UniqueID");
	}
}
//debug_pr($BookUniqueArray);

intranet_closedb();
?>