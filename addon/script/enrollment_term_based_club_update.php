<?
$PATH_WRT_ROOT = "../../";

include_once($PATH_WRT_ROOT."includes/global.php");
//include('../check.php');
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");

intranet_opendb();

$libclubsenrol = new libclubsenrol();
$fs = new libfilesystem();


if ($flag==1)
{
	## add INTRANET_ENROL_GROUPINFO if there is no record
	$sql = "SELECT
					a.GroupID, b.EnrolGroupID
			FROM
					INTRANET_GROUP as a
					LEFT OUTER JOIN
					INTRANET_ENROL_GROUPINFO as b
					ON (a.GroupID = b.GroupID)
			WHERE
					a.RecordType = 5
			";
	$GroupArr = $libclubsenrol->returnArray($sql);
	
	$numOfGroup = count($GroupArr);
	for ($i=0; $i<$numOfGroup; $i++)
	{
		$thisGroupID = $GroupArr[$i]['GroupID'];
		$thisEnrolGroupID = $GroupArr[$i]['EnrolGroupID'];
		
		if ($thisEnrolGroupID == '')
			$libclubsenrol->Insert_Year_Based_Club($thisGroupID);
	}
	
	
	## update Club info
	$sql = "SELECT 
					b.EnrolGroupID, 
					b.GroupID,
					b.AttachmentLink1,
					b.AttachmentLink2,
					b.AttachmentLink3,
					b.AttachmentLink4,
					b.AttachmentLink5,
					a.Title
			FROM 
					INTRANET_ENROL_GROUPINFO as b
					INNER JOIN
					INTRANET_GROUP as a
					ON (a.GroupID = b.GroupID)
					
			";
	$ClubArr = $libclubsenrol->returnArray($sql);
	$numOfClub = count($ClubArr);
	
	$x = '';
	$x .= "eEnrolment_term_based_script.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";	
	
	if ($numOfClub > 0)
	{
		for ($i=0; $i<$numOfClub; $i++)
		{
			$thisEnrolGroupID = $ClubArr[$i]['EnrolGroupID'];
			$thisGroupID = $ClubArr[$i]['GroupID'];
			$thisTitle = $ClubArr[$i]['Title'];
			
			$x .= "================= Start Club ".$thisTitle." (GroupID = ".$thisGroupID.", EnrolGroupID = ".$thisEnrolGroupID.") =================<br>\r\n";
			
			## Activity Record
			$sql = "Select EnrolEventID From INTRANET_ENROL_EVENTINFO Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' Or EnrolGroupID Is Null)";
			$activityArr = $libclubsenrol->returnVector($sql);
			$numOfActivity = count($activityArr);
			
			if ($numOfActivity > 0)
			{
				$activityList = implode(',', $activityArr);
				
				$sql = "Update INTRANET_ENROL_EVENTINFO set EnrolGroupID = '".$thisEnrolGroupID."' Where EnrolEventID In (".$activityList.")";
				$SuccessArr[$thisGroupID]['Activity'] = $libclubsenrol->db_db_query($sql);
			
				if ($SuccessArr[$thisGroupID]['Activity'])
					$x .= "Update Activity <font color='blue'><b>Success</b></font><br>\r\n";
				else
					$x .= "Update Activity <font color='red'><b>Failed</b></font><br>\r\n";
			}
			else
			{
				$x .= "No activities for this club to update<br>\r\n";
			}
			
			## If have Main info => club added after enhancement / has run data patch already
			$sql = "SELECT * FROM INTRANET_ENROL_GROUPINFO_MAIN Where GroupID = '".$thisGroupID."'";
			$thisMainInfo = $libclubsenrol->returnArray($sql);
			
			if (count($thisMainInfo) > 0)
			{
				$x .= "This Club has been updated before.<br>\r\n";
				$x .= "================== End Club ".$thisTitle." (GroupID = ".$thisGroupID.", EnrolGroupID = ".$thisEnrolGroupID.") ==================<br><br>\r\n";
				
				continue;
			}
			
			
			# INTARNET_ENROL_GROUPSTUDENT
			$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT Set EnrolGroupID = '".$thisEnrolGroupID."' 
					Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' OR EnrolGroupID Is Null)";
			$SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPSTUDENT'] = $libclubsenrol->db_db_query($sql) or die(mysql_error());
			
			if ($SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPSTUDENT'])
				$x .= "Update INTRANET_ENROL_GROUPSTUDENT <font color='blue'><b>Success</b></font><br>\r\n";
			else
				$x .= "Update INTRANET_ENROL_GROUPSTUDENT <font color='red'><b>Failed</b></font><br>\r\n";
			
			# INTARNET_USERGROUP
			$sql = "Select UserGroupID From INTRANET_USERGROUP Where GroupID = '".$thisGroupID."'";
			$memberArr = $libclubsenrol->returnVector($sql);
			
			if (count($memberArr) > 0)
			{
				$sql = "UPDATE INTRANET_USERGROUP Set EnrolGroupID = '".$thisEnrolGroupID."' 
						Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' OR EnrolGroupID Is Null)";
				$SuccessArr[$thisGroupID]['INTRANET_USERGROUP'] = $libclubsenrol->db_db_query($sql);
				
				if ($SuccessArr[$thisGroupID]['INTRANET_USERGROUP'])
					$x .= "Update INTRANET_USERGROUP <font color='blue'><b>Success</b></font><br>\r\n";
				else
					$x .= "Update INTRANET_USERGROUP <font color='red'><b>Failed</b></font><br>\r\n";
			}
			else
			{
				$x .= "No record in INTRANET_USERGROUP for this club<br>\r\n";
			}
			
			
			
			# INTARNET_ENROL_GROUP_ATTENDANCE
			$sql = "UPDATE INTRANET_ENROL_GROUP_ATTENDANCE Set EnrolGroupID = '".$thisEnrolGroupID."' 
					Where GroupID = '".$thisGroupID."' And (EnrolGroupID = '' OR EnrolGroupID Is Null)";
			$SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUP_ATTENDANCE'] = $libclubsenrol->db_db_query($sql);
			
			if ($SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUP_ATTENDANCE'])
				$x .= "Update INTRANET_ENROL_GROUP_ATTENDANCE <font color='blue'><b>Success</b></font><br>\r\n";
			else
				$x .= "Update INTRANET_ENROL_GROUP_ATTENDANCE <font color='red'><b>Failed</b></font><br>\r\n";
				
				
			
			# Add Default Main Info
			$sql = "INSERT INTO INTRANET_ENROL_GROUPINFO_MAIN
							(GroupID, ClubType, ApplyOnceOnly, FirstSemEnrolOnly, DateInput)
					VALUES
							('$thisGroupID', 'Y', 0, 0, now())
					";
			$SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPINFO_MAIN'] = $libclubsenrol->db_db_query($sql);
			
			if ($SuccessArr[$thisGroupID]['INTRANET_ENROL_GROUPINFO_MAIN'])
				$x .= "Insert INTRANET_ENROL_GROUPINFO_MAIN <font color='blue'><b>Success</b></font><br>\r\n";
			else
				$x .= "Insert INTRANET_ENROL_GROUPINFO_MAIN <font color='red'><b>Failed</b></font><br>\r\n";
				
		
				
			# Attachment Transfer
			for ($j=1; $j<=5; $j++)
			{
				$thisAttachment = $ClubArr[$i]['AttachmentLink'.$j];
				
				if ($thisAttachment == '')
					continue;
					
				## move the file to the EnrolGroupID folder
				$AttachmentInfoArr = explode('/', $thisAttachment);
				$timestamp_folder = $AttachmentInfoArr[2];
				$file_name = $AttachmentInfoArr[3];
				
				$new_folder_prefix = $intranet_root."/file/enroll_info/enrolgroup".$thisEnrolGroupID;
				if (!file_exists($new_folder_prefix))
					$fs->folder_new($new_folder_prefix);
					
				$new_folder_prefix .= "/".$timestamp_folder;
				if (!file_exists($new_folder_prefix))
					$fs->folder_new($new_folder_prefix);
					
				$old_file = $intranet_root."/file/".$thisAttachment;
				$new_file = $new_folder_prefix."/".$file_name;
				
				if (is_file($old_file))
					$Success['EnrolGroupID_' + $thisEnrolGroupID]['Copy_File'] = $fs->file_copy($old_file, $new_file);
				
				//if ($Success['EnrolGroupID_' + $thisEnrolGroupID]['Update_DB'])
				//{
					$x .= "Copy Attachment ".$file_name." to EnrolGroupID folder <font color='blue'><b>Success</b></font><br>\r\n";
				//}
				//else
				//{
				//	$x .= "Copy Attachment ".$file_name." to EnrolGroupID folder <font color='red'><b>Failed</b></font><br>\r\n";
				//}
				
				## update the path in DB
				$new_file_path = "enroll_info/enrolgroup".$thisEnrolGroupID."/".$timestamp_folder."/".$file_name;
				$sql = "UPDATE 	
								INTRANET_ENROL_GROUPINFO 
						SET		
								AttachmentLink".$j." = '".$libclubsenrol->Get_Safe_Sql_Query($new_file_path)."'
						WHERE	
								EnrolGroupID = '".$thisEnrolGroupID."'
						";	
				$Success['EnrolGroupID_' + $thisEnrolGroupID]['Update_DB'] = $libclubsenrol->db_db_query($sql);
				
				if ($Success['EnrolGroupID_' + $thisEnrolGroupID]['Update_DB'])
				{
					$x .= "Update Attachment ".$file_name." in DB <font color='blue'><b>Success</b></font><br>\r\n";
				}
				else
				{
					$x .= "Update Attachment ".$file_name." in DB <font color='red'><b>Failed</b></font><br>\r\n";
				}
			}
				
			
		
			## OLE Record
			$sql = "SELECT OLE_ProgramID FROM INTRANET_GROUP Where GroupID = '".$thisGroupID."'";
			$resultSet = $libclubsenrol->returnVector($sql);
			$thisProgramID = $resultSet[0];
			
			$sql = "UPDATE INTRANET_ENROL_GROUPINFO Set OLE_ProgramID = '".$thisProgramID."' Where EnrolGroupID = '".$thisEnrolGroupID."'";
			$SuccessArr[$thisGroupID]['OLE_ProgramID'] = $libclubsenrol->db_db_query($sql);
			
			if ($SuccessArr[$thisGroupID]['OLE_ProgramID'])
				$x .= "Update OLE_ProgramID <font color='blue'><b>Success</b></font><br>\r\n";
			else
				$x .= "Update OLE_ProgramID <font color='red'><b>Failed</b></font><br>\r\n";
			
			$x .= "================== End Club ".$thisTitle." (GroupID = ".$thisGroupID.", EnrolGroupID = ".$thisEnrolGroupID.") ==================<br><br>\r\n";
		}
	}
	else
	{
		$x .= "No Club set up in eEnrolment yet.";
	}
	
	
	$x .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";	
	echo $x;

	#  save log
	$lf = new libfilesystem();
	$log_folder = $PATH_WRT_ROOT."file/eEnrolment_term_based_club_script";
	if(!file_exists($log_folder))
		mkdir($log_folder);
	$log_file = $log_folder."/data_atch_log_". date("YmdHis") .".html";
	$lf->file_write($x, $log_file);
	echo "<br>File Log: ". $log_file;

}
else
{ ?>
	<html>
	<body>
	<font color=red size=+1>[Please Make DB Backup First & Run Addon Schema FIRST]</font><br>
	This page will update the eEnrolment1.2 database to support Term-based club<br><br>
	The EnrolGroupID field will be updated according to the GroupID in the following table
	<ul>
		<li>INTRANET_USERGROUP</li>
		<li>INTRANET_ENROL_GROUPSTUDENT</li>
		<li>INTRANET_ENROL_GROUP_ATTENDANCE</li>
	</ul>
	<br>
	Besides, the attachment of the clubs will be copied to the corresponding EnrolGroupID folder also.
	<br>
	<br>
	
	<a href=?flag=1>Click here to proceed.</a> <br><br>
	
	<br>
	</body></html>

	
	
<? }

intranet_closedb();
 ?>
	