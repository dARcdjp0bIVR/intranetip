<?php
/*
 * 	Purpose: cancel overdue payment (resume to not paid status)
 * 
 *  2017-12-01 [Cameron] create this file [case #Z132221]
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
$PATH_WRT_ROOT = $path;
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");
include_once($path."includes/libelibrary.php");
include_once($path."home/library_sys/management/circulation/User.php");
include_once($path."home/library_sys/management/circulation/BookManager.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$liblms = new liblms();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	$form = "<form method='post'>
				<input value='run script' type='submit'>
				<input type='hidden' name='Flag' value=1>
				<div>Un-paid Amount <input type=\"text\" name=\"Amount\"></div>
				<div>OverDueLogID <input type=\"text\" name=\"OverDueLogID\"></div>
			</form>";
	echo $form;
}
else {
	$isApply = true;
}


$uID = isset($_SESSION['LIBMS']['CirculationManagemnt']['UID'])? $_SESSION['LIBMS']['CirculationManagemnt']['UID'] : $_SESSION['UserID'];
$User = new User($uID, $liblms);

if ($isApply && $OverDueLogID && $Amount) {
	$ret=$User->cancel_overdue($Amount, $OverDueLogID, false);	// not by ePayment
}
else {
	$ret = false;
}

if($isApply){
	$result = $ret ? "<span style=\"color:green; font-weight:bold\">cancel overdue payment success!</span>" : "<span style=\"color:red; font-weight:bold\">Fail to cancel overdue payment !</span>";
	if ($FromCentralUpdate) {
		$x .= $result."\r\n";
	}
	else {
		echo $result;				
	}
}
if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>