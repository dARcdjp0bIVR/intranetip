<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");

## Prompt login
include_once("../../check.php");

eclass_opendb();

$course_code = 'fcbuilder';
$course_name = 'Flipped Channels Builder Classroom';
$course_desc = 'Flipped Channels Builder Classroom - store questions created in Flipped Channels and eclass students quiz result';
$max_user = "99999";
$max_storage = "NULL";

$lo = new libeclass();

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 15;
if($course_id=='')
{
	$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
	$lo->eClassSubjectUpdate($subj_id, $course_id);
	echo 'Flipped Channels Builder Classroom created!<br/>';
}
else
{
	echo 'Flipped Channels Builder Classroom already created!<br/>';
}

$lo->db = $eclass_prefix."c".$course_id;		

//in case any tables required to create

$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_SUBTITLE (
	     SubtitleID int(10) unsigned NOT NULL auto_increment,
	     Subtitle text NOT NULL,
	     DateInput datetime NOT NULL,
	     CreatedBy int(10) unsigned NOT NULL default '0',
	     DateModified datetime NOT NULL,
	     ModifiedBy int(10) unsigned NOT NULL default '0',
	     PRIMARY KEY (SubtitleID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'VIDEOLIB_SUBTITLE' created!":"<br/>Table - 'VIDEOLIB_SUBTITLE' failed to create!";

$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_REFERENCE (
	     ReferenceID int(10) unsigned NOT NULL auto_increment,
	     Reference longblob NOT NULL,
	     DateInput datetime NOT NULL,
	     CreatedBy int(10) unsigned NOT NULL default '0',
	     DateModified datetime NOT NULL,
	     ModifiedBy int(10) unsigned NOT NULL default '0',
	     PRIMARY KEY (ReferenceID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'VIDEOLIB_REFERENCE' created!":"<br/>Table - 'VIDEOLIB_REFERENCE' failed to create!";

$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_VIDEO_BUILDER (
	     VideoBuilderID int(10) unsigned NOT NULL auto_increment,
	     VideoID int(10) unsigned NOT NULL default '0',
	     BuilderType varchar(2) NOT NULL,
	     FunctionID int(10) unsigned default '0',
	     StartTime float(6,2) unsigned NOT NULL default '0.00',
	     EndTime float(6,2) unsigned NOT NULL default '0.00',
	     SequenceNo int(10) unsigned NOT NULL default '0',
	     DateInput datetime NOT NULL,
	     CreatedBy int(10) unsigned NOT NULL default '0',
	     DateModified datetime NOT NULL,
	     ModifiedBy int(10) unsigned NOT NULL default '0',
	     PRIMARY KEY (VideoBuilderID),
		 INDEX VideoID (VideoID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'VIDEOLIB_VIDEO_BUILDER' created!":"<br/>Table - 'VIDEOLIB_VIDEO_BUILDER' failed to create!";

$sql = "CREATE TABLE IF NOT EXISTS VIDEOLIB_VIDEO_RECORD (
	     RecordID int(10) unsigned NOT NULL auto_increment,
	     VideoKey varchar(255) default NULL,
	     QuizID int(8) default NULL,
	     CourseID int(8) default NULL,
	     DateInput datetime NOT NULL,
	     CreatedBy int(10) unsigned NOT NULL default '0',
	     DateModified datetime NOT NULL,
	     ModifiedBy int(10) unsigned NOT NULL default '0',
	     PRIMARY KEY (RecordID),
		 INDEX VideoQuizID (VideoKey,QuizID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8";
echo $lo->db_db_query($sql)? "<br/>Table - 'VIDEOLIB_VIDEO_RECORD' created!":"<br/>Table - 'VIDEOLIB_VIDEO_RECORD' failed to create!";
eclass_closedb();
echo "<br/><br/><strong>Target Database Name : ".$eclass_prefix."c".$course_id."</strong><br/>";
?>