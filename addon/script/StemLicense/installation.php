<?php
$PATH_WRT_ROOT = '../../..';

include_once( $PATH_WRT_ROOT . "/includes/global.php");
include_once( $PATH_WRT_ROOT . "/includes/libdb.php");

if($plugin['stem_x_pl2']){
    
    # get the password from central server
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_REFERER, $_SERVER["SERVER_NAME"]);
    curl_setopt($ch, CURLOPT_URL, "http://eclassupdate.broadlearning.com/api/blpswd.php?ptype=3");
    
    $password_central = trim(curl_exec($ch));
    curl_close($ch);
    
    if ($password_central != "" && strlen($password_central) > 3) {
        if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_USER'] != "broadlearning" || md5("2012allschools" . $_SERVER['PHP_AUTH_PW']) != $password_central) {
            $realm = "STEM x PL2 SSO Installation ( " . strftime("%X %Z", time()) . " )";
    
            Header("WWW-Authenticate: Basic realm=\"" . $realm . "\"");
            Header("HTTP/1.0 401 Unauthorized");
            echo "Unauthorized Access\n";
            exit;
        }
    }
    
    intranet_opendb();
    
    $libdb = new libdb();
    
    # Create Database
    $dbName = $eclass_prefix . "powerlesson";
    $result = intval(current($libdb->returnVector("
        SELECT 
            count(*) 
        FROM
            information_schema.SCHEMATA 
        WHERE 
            SCHEMA_NAME = '" . $dbName . "'")));
    
    if($result === 0){
        $libdb->db_create_db($dbName);
    }
    
    # Change Database
    $libdb->db = $dbName;
    
    # Create Table for storing SSO Tokens
    $libdb->db_db_query("
        CREATE TABLE IF NOT EXISTS `stem_license_sso_tokens` 
        ( 
            `user_id` INT NOT NULL,
            `token` varchar(255) DEFAULT NULL,
            `create_date` datetime NOT NULL,
            PRIMARY KEY (`user_id`)
        ) 
        DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB");
    
    intranet_closedb();
    
    echo "<h1>Installation completed.</h1>";
} else {
    echo "<h1>Installation Failed.</h1>";    
}