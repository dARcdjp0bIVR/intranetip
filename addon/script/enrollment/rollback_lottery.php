<?php

$PATH_WRT_ROOT = "../../../";

include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");

intranet_auth();
intranet_opendb();

$li= new libdb();

include_once($PATH_WRT_ROOT."includes/libclubsenrol.php");
$libenroll = new libclubsenrol();

if($_POST['Flag']!=1)
{
	die("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}


$sql = "
select 
	iegs.StudentID, 
	iegs.RecordStatus,
	ieg.EnrolGroupID,
	ieg.GroupCategory 
FROM  
	INTRANET_ENROL_GROUPINFO ieg 
	LEFT JOIN INTRANET_ENROL_GROUPSTUDENT iegs ON ieg.EnrolGroupID = iegs.EnrolGroupID   
WHERE 
	ieg.RecordStatus = 1 
";
$stu_list = $li->returnArray($sql);
$stu_list = BuildMultiKeyAssoc($stu_list, "RecordStatus", array("StudentID", "EnrolGroupID", "GroupCategory"), 0, 1);

$sql = "
UPDATE 
	INTRANET_ENROL_GROUPINFO ieg 
	LEFT JOIN INTRANET_ENROL_GROUPSTUDENT iegs ON ieg.EnrolGroupID = iegs.EnrolGroupID 
SET 
	iegs.RecordStatus =0 
WHERE 
	ieg.RecordStatus =1 AND iegs.RecordStatus=1
";
$Success['Rejected'] = $li->db_db_query($sql);


for($i=0; $i<sizeof($stu_list[2]); $i++)
{
	$StudentID = $stu_list[2][$i]["StudentID"];
	$EnrolGroupID = $stu_list[2][$i]["EnrolGroupID"];
	$CategoryID = $stu_list[2][$i]["GroupCategory"];
	$sql = "DELETE FROM INTRANET_USERGROUP WHERE EnrolGroupID = '$EnrolGroupID' AND UserID = '$StudentID' ";
	$Success[$StudentID][$EnrolGroupID]['INTRANET_USERGROUP'] = $li->db_db_query($sql);
//	debug_pr($sql);
	
	$sql = "UPDATE INTRANET_ENROL_GROUPSTUDENT SET RecordStatus = 0 WHERE EnrolGroupID = '$EnrolGroupID' AND StudentID = '$StudentID' ";
	$Success[$StudentID][$EnrolGroupID]['INTRANET_ENROL_GROUPSTUDENT'] = $li->db_db_query($sql);
//	debug_pr($sql);
	
	if($libenroll->UseCategorySetting)
	{
		$sql = "UPDATE INTRANET_ENROL_STUDENT_CATEGORY_ENROL SET Approved = Approved - 1 WHERE StudentID = '$StudentID' AND CategoryID = '$CategoryID'";
		$Success[$StudentID][$EnrolGroupID]['INTRANET_ENROL_STUDENT_CATEGORY_ENROL'] = $li->db_db_query($sql);
//		debug_pr($sql);
	}
	else
	{
		$sql = "UPDATE INTRANET_ENROL_STUDENT SET Approved = Approved - 1 WHERE StudentID = '$StudentID'";
		$Success[$StudentID][$EnrolGroupID]['INTRANET_ENROL_STUDENT'] = $li->db_db_query($sql);
//		debug_pr($sql);
	}

	$sql = "UPDATE INTRANET_ENROL_GROUPINFO SET Approved = Approved - 1 WHERE EnrolGroupID = '$EnrolGroupID'";
	$Success['INTRANET_ENROL_GROUPINFO'][$EnrolGroupID] = $li->db_db_query($sql);
	
}

debug_pr($Success);

intranet_closedb();

?>