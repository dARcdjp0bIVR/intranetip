<?php
// Editing by 
/*
 * Customization for 
 * TWGHs C Y MA MEMORIAL COLLEGE
 * 東華三院馬振玉紀念中學
 * http://intranet.cyma.edu.hk
 * 
 * @Description: Add original activity events single category to multiple category mapping table
 */
//$PATH_WRT_ROOT = "../../../";
//include_once($intranet_root."/includes/global.php");
//include_once($intranet_root."/includes/libdb.php");

//intranet_auth();
//intranet_opendb();
global $li;
//$libdb = new libdb();

$sql = "INSERT IGNORE INTO INTRANET_ENROL_EVENT_CATEGORY (EnrolEventID,CategoryID) SELECT EnrolEventID,EventCategory as CategoryID FROM INTRANET_ENROL_EVENTINFO";
$success = $li->db_db_query($sql);

echo "[eEnrolment] Customize activity events from single category to multiple categories ";
if($success){
	echo "success.";
}else{
	echo "failed.";
}
echo "<br />";

//intranet_closedb();
?>