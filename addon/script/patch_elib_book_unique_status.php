<?php
/*
 * 	Purpose: correct LIBMS_BOOK_UNIQUE.RecordStatus, re-count NoOfCopyAvailable & NoOfCopy in LIBMS_BOOK
 * 
 *  2016-09-12 [Cameron] create this file
 */

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$liblms = new liblms();


if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}

## 1. wrong RecordStatus (BORROWED) in LIMBS_BOOK_UNIQUE because there's no log record in LIBMS_BORROW_LOG
## should set back to NORMAL
$sql = "select  bu.UniqueID, b.BookID 
from LIBMS_BOOK b inner join LIBMS_BOOK_UNIQUE bu on bu.BookID=b.BookID
LEFT JOIN LIBMS_BORROW_LOG br on br.UniqueID=bu.UniqueID 
where bu.RecordStatus='BORROWED' and br.BorrowLogID is null 
order by bu.UniqueID";

$rs = $liblms->returnResultSet($sql);
$nrRec = count($rs);

debug_pr($sql);

$error = array();
$unique_id_array = array();
$book_id_array = array();
if ($nrRec > 0) {
	foreach((array)$rs as $k=>$v) {
		$unique_id_array[] = $v['UniqueID'];
		$book_id_array[] = $v['BookID'];
	}
} 
debug_r($unique_id_array);


if (count($unique_id_array)) {
	$unique_ids = implode(",",$unique_id_array);
	$sql = "UPDATE LIBMS_BOOK_UNIQUE set RecordStatus='NORMAL' WHERE UniqueID IN (".$unique_ids.")";
	if($isApply){
		$result = $liblms->db_db_query($sql);
		if ($result) {
			$result = "Number of record affected [BORROWED -> NORMAL] :".$nrRec;
			$x .=  $result."\r\n";
			echo $result."<br>";
		}
		else {
			$error[] = $result;	
		}
	}
	debug_pr($sql);
}



## 2. wrong RecordStatus (NORMAL) in LIMBS_BOOK_UNIQUE because of conflict with LIBMS_BORROW_LOG
## should set back to BORROWED
$sql = "select  bu.UniqueID, b.BookID 
from LIBMS_BOOK b inner join LIBMS_BOOK_UNIQUE bu on bu.BookID=b.BookID
INNER JOIN LIBMS_BORROW_LOG br on br.UniqueID=bu.UniqueID 
where bu.RecordStatus='NORMAL' and br.RecordStatus in ('BORROWED') 
order by bu.UniqueID";

$rs = $liblms->returnResultSet($sql);
$nrRec = count($rs);

debug_pr($sql);

$error = array();
$unique_id_array = array();
if ($nrRec > 0) {
	foreach((array)$rs as $k=>$v) {
		$unique_id_array[] = $v['UniqueID'];
		$book_id_array[] = $v['BookID'];
	}
} 
debug_r($unique_id_array);


if (count($unique_id_array)) {
	$unique_ids = implode(",",$unique_id_array);
	$sql = "UPDATE LIBMS_BOOK_UNIQUE set RecordStatus='BORROWED' WHERE UniqueID IN (".$unique_ids.")";
	if($isApply){
		$result = $liblms->db_db_query($sql);
		if ($result) {
			$result = "Number of record affected [NORMAL -> BORROWED ] :".$nrRec;
			$x .=  $result."\r\n";
			echo $result."<br>";
		}
		else {
			$error[] = $result;	
		}
	}
	debug_pr($sql);
}


## 3. wrong RecordStatus (RESERVED) in LIMBS_BOOK_UNIQUE because of conflict status in LIBMS_RESERVATION_LOG
## should set back to NORMAL
$sql = "select bu.UniqueID, b.BookID
from LIBMS_BOOK b inner join LIBMS_BOOK_UNIQUE bu on bu.BookID=b.BookID
INNER JOIN (select r.RecordStatus, r.UserID, r.BookID, r.ReserveTime, r.ReservationID 
			from LIBMS_RESERVATION_LOG r where r.ReserveTime = (select max(ReserveTime) 
				from LIBMS_RESERVATION_LOG where BookID=r.BookID)
				and r.RecordStatus in ('DELETE','BORROWED','EXPIRED')) as br on br.BookID=b.BookID
where bu.RecordStatus='RESERVED' 
order by bu.UniqueID";

$rs = $liblms->returnResultSet($sql);
$nrRec = count($rs);

debug_pr($sql);

$error = array();
$unique_id_array = array();
if ($nrRec > 0) {
	foreach((array)$rs as $k=>$v) {
		$unique_id_array[] = $v['UniqueID'];
		$book_id_array[] = $v['BookID'];
	}
} 
debug_r($unique_id_array);


if (count($unique_id_array)) {
	$unique_ids = implode(",",$unique_id_array);
	$sql = "UPDATE LIBMS_BOOK_UNIQUE set RecordStatus='NORMAL' WHERE UniqueID IN (".$unique_ids.")";
	if($isApply){
		$result = $liblms->db_db_query($sql);
		if ($result) {
			$result = "Number of record affected [RESERVED -> NORMAL ] :".$nrRec;
			$x .=  $result."\r\n";
			echo $result."<br>";
		}
		else {
			$error[] = $result;	
		}
	}
	debug_pr($sql);
}


## 4. update NoOfCopyAvailable & NoOfCopy in LIBMS_BOOK
if (count($book_id_array)) {
	
	$book_id_array = array_unique($book_id_array);
	sort($book_id_array);
	$cond = "BookID in (".implode(",",$book_id_array).")";
	
	$sql = "select BookID, BookTitle from LIBMS_BOOK WHERE ".$cond." order by BookID";
	$bookArr = $liblms->returnResultSet($sql);
	
	# get no of available copy if any into BOOKID 
	$sql = "SELECT BookID, COUNT(*) as cnt FROM  LIBMS_BOOK_UNIQUE
			WHERE RecordStatus IN ('NORMAL', 'SHELVING', 'RESERVED')
			AND ".$cond."  
			group by BookID"; 
	$temp1 = $liblms->returnResultSet($sql);
	$nrRec1 = count($temp1);
	if($nrRec1 > 0){
		for ($i=0; $i<$nrRec1; $i++){
			$noOfAvailCopyArr[$temp1[$i]['BookID']] = $temp1[$i]['cnt'];
		}
	}
	
	# get no of copy if any into BOOKID 
	$sql = "SELECT BookID, COUNT(*) as cnt FROM  LIBMS_BOOK_UNIQUE
			WHERE RecordStatus NOT IN ('LOST',  'WRITEOFF',  'SUSPECTEDMISSING', 'DELETE')
			AND ".$cond."  
			group by BookID"; 
	$temp2 = $liblms->returnArray($sql);
	$nrRec2 = count($temp2);
	if($nrRec2 > 0){
		for ($i=0; $i<$nrRec2; $i++){
			$noOfCopyArr[$temp2[$i]['BookID']] = $temp2[$i]['cnt'];
		}
	}
	
	
	## Main
	for ($i=0,$iMax=count($bookArr); $i<$iMax; $i++)
	{
		
		## handle the first copy of each Book
		$BookID = $bookArr[$i]['BookID'];
		$BookTitle = $bookArr[$i]['BookTitle'];
		
		$noOfCopyAvailable = (isset($noOfAvailCopyArr[$BookID])) ? $noOfAvailCopyArr[$BookID] : 0;
		$noOfCopy = (isset($noOfCopyArr[$BookID])) ? $noOfCopyArr[$BookID] : 0;
		
		$sql = "update LIBMS_BOOK set 
					NoOfCopyAvailable = '".$noOfCopyAvailable."', NoOfCopy= '".$noOfCopy."' 
				where BookID = '".$BookID."' ";
		echo $sql.'<br>';
		if($isApply){
			$result = $liblms->db_db_query($sql);
			if ($result) {
				$result = "update_book_copy_info_{$BookID}:";
				$x .=  $result."\r\n";
				echo $result."<br>";
			}
			else {
				$error[] = $result;	
			}
		}
	}
}	// end update NoOfCopyAvailable & NoOfCopy in LIBMS_BOOK

if($isApply){
	$result = in_array(false,$error) ? "Fail to patch book unique status!<br>" : "<span style=\"color:green; font-weight:bold\">patch book unique status success!</span><br>";
	if ($FromCentralUpdate) {
		$x .= $result."\r\n";
	}
	else {
		echo $result;				
	}
}
if (!$FromCentralUpdate) {
	intranet_closedb();
}
?>