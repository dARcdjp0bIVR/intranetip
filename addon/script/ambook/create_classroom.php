<?php

$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libuser.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");
include_once($PATH_WRT_ROOT."includes/libaccess.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
include_once($eclass40_filepath."/src/plugin/ambook/config.php");

## Prompt login
include_once("../../check.php");

eclass_opendb();

$course_code = 'ambook';
$course_name = 'Junior Secondary Science';
$course_desc = 'ambook classroom';
$max_user = "NULL";
$max_storage = "NULL";

$lo = new libeclass();

$sql = "select course_id from ".$eclass_db.".course where course_code='".$course_code."'";
$count_obj = $lo->returnVector($sql);
$course_id = $count_obj[0];
$lo->RoomType = 7;
if($course_id=='')
{
	$course_id = $lo->eClassAdd($course_code, $course_name, $course_desc, $max_user, $max_storage);
	$lo->eClassSubjectUpdate($subj_id, $course_id);
	echo 'ambook Classroom created!<br/>';
}
else
{
	echo 'ambook Classroom already created!<br/>';
}
# check if there's any notes previously input
$sql = "SELECT count(*) from ".$eclass_prefix."c".$course_id.".notes";
$notes_obj = $lo->returnVector($sql);

if($notes_obj[0]>0)
die('Notes exist in ambook classroom!');

$notes_result = array();
$chapter_ct = 1;
foreach($ab_cfg['chapter'] as $sub=>$sub_obj)
{
	foreach($sub_obj as $unit_code=>$unit_obj)
	{
		$notes_count = 0;
		
		foreach($unit_obj as $chapter_code=>$chapter_obj)
		{
			for($a=0;$a<=sizeof($chapter_obj['Notes']['en']);$a++)
			{
				$code_folder = $sub.$unit_code.$chapter_code."_".$chapter_obj['FolderName'];
				
				$title = ($a==0)?$chapter_obj['ChapterName']['en']:$chapter_obj['Notes']['en'][$a-1]; 
				$sql = "insert into ".$eclass_prefix."c".$course_id.".notes 
							  (Title,url,a_no,b_no,status,inputdate,modified)
						values('".addslashes($title)."','".$code_folder."',".$chapter_ct.",".$a.",1,now(),now())";
						
				$notes_result[] = $lo->db_db_query($sql);		
						
				if($a!=0)
				{	
					
	
					$url = "files_elp/ambook/$code_folder/?PageFile=".$sub.$unit_code.$chapter_code."_".($a);
					
					$notes_id = $lo->db_insert_id();
					$sql = "insert into ".$eclass_prefix."c".$course_id.".notes_section
							(notes_id,notes_section_type,url,inputdate,modified)
						values(".$notes_id.",13,'".$url."',now(),now())";
					$notes_result[] = $lo->db_db_query($sql);
					
				}		
			}
			$chapter_ct++;		
		}
	}
}

if(in_array(false,$notes_result))
	echo 'Error occured when createing amBook notes!';
else
	echo 'amBook notes created!';
		
eclass_closedb();

?>