<?php
// using 
$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libeclass40.php");

intranet_opendb();

//$AcademicYearID = Get_Current_Academic_Year_ID();

$db = new libdb();

//$AcademicYearID = 2;
//$CurrentAcademicYearInDB = 1;
 
$CurrentAcademicYearInDB = Get_Previous_Academic_Year_ID();
$AcademicYearID = Get_Current_Academic_Year_ID();
 
 
//ecomm
$db->Start_Trans();

$Result = array();
$sql = "
	SELECT 
		GroupID,
		Title
	FROM 
		INTRANET_GROUP
	WHERE
		AcademicYearID = '$AcademicYearID'
";
$NewGroupList = $db->returnArray($sql);

$sql = "
	SELECT 
		GroupID,
		Title
	FROM 
		INTRANET_GROUP
	WHERE
		AcademicYearID = '$CurrentAcademicYearInDB'
"; 
$OldGroupList = $db->returnArray($sql);

$NewGroupList = BuildMultiKeyAssoc($NewGroupList,"Title","GroupID",1);

for($i=0; $i<sizeof($OldGroupList); $i++)
{
	list($GroupID, $Title) = $OldGroupList[$i];
	if($NewGroupList[$Title]=='')
		continue;
		
	$GroupIDMapping[$GroupID] = $NewGroupList[$Title];
}

$eCommRelatedTable = array(
	"INTRANET_FILE",
	"INTRANET_FILE_FOLDER",
	"INTRANET_GROUPANNOUNCEMENT",
	"INTRANET_BULLETIN",
	"INTRANET_ANNOUNCEMENT",
	"INTRANET_HOUSE",
	"INTRANET_GROUPRESOURCE",
);

foreach((array)$GroupIDMapping as $OldGroupID => $NewGroupID)
{
	
	$sql_remarks = '';
	foreach($eCommRelatedTable as $table)
	{ 
		if($table == 'INTRANET_ANNOUNCEMENT')
			$GroupID = 'OwnerGroupID';
		else
			$GroupID = 'GroupID';
			
		$sql = "
			UPDATE
				$table
			SET
				$GroupID = '$NewGroupID'
			WHERE
				$GroupID = '$OldGroupID'
		";
			
		$Success = $db->db_db_query($sql);	
		$Result[] = $Success;
		if(!$Success)
		{
			$sql_remarks .= mysql_error()."\n$sql ";
		}
 
	}
		
	$old_file_path = $PATH_WRT_ROOT."file/group/g$OldGroupID";
	if(file_exists($old_file_path))
	{
		$new_file_path = $PATH_WRT_ROOT."file/group/g$NewGroupID";
		
		rename($old_file_path, $new_file_path);
		$file_remarks = "file ok";
	}
	else
	{
		$file_remarks = "$old_file_path not exist.";
	}
	
	$txt.= "$OldGroupID => $NewGroupID\n";
	$txt.= "$sql_remarks\n";
	$txt.= "$file_remarks\n";
	
}

include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
$lfile = new libfilesystem();
$success = $lfile->file_write($txt,$PATH_WRT_ROOT."file/eCommConvInScript.txt");


if (in_array(false,$Result)) {
	$ErrorMsg = mysql_error();
	echo 'Renew eComm GroupID failed';
	$db->RollBack_Trans();
	
	$sql = 'insert into GENERAL_LOG (
						Process,
						Result,
						Remarks 
						)
					values (
						\'Renew eComm GroupID\',
						\'0\', 
						\''.$db->Get_Safe_Sql_Query($ErrorMsg).'\'
						)';
}
else {
	$AffectedRow = $db->db_affected_rows();
	echo 'Renew eComm GroupID successfully';
	$db->Commit_Trans();
	
	$sql = 'insert into GENERAL_LOG (
						Process,
						Result,
						Remarks 
						)
					values (
						\'Renew eComm GroupID\',
						\'1\', 
						\'affected record: '.$AffectedRow.'\'
						)';
}
$db->db_db_query($sql);

//ecomm

intranet_closedb();
?>