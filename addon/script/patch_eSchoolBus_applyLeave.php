<?php
# Editing by
/*
 * 	purpose: copy RecordStatus from INTRANET_SCH_BUS_APPLY_LEAVE to INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT
 * 
 *	2019-09-23 Cameron
 *		- create this file
 * 
 */ 
@SET_TIME_LIMIT(1000);
@ini_set('memory_limit', -1);

$PATH_WRT_ROOT = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/schoolBusConfig.php");
include_once($PATH_WRT_ROOT."includes/eSchoolBus/libSchoolBus_db.php");
include_once($PATH_WRT_ROOT."includes/libcardstudentattend2.php");

if (!$CallFromSameDomain)
{
	include_once("../check.php");
}

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

if ($plugin['eSchoolBus'])
{
    $li = new libSchoolBus_db();
	
	if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
		$isApply = false;
		echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
	}
	else {
		$isApply = true;
	}

	if ($isApply) {
		$result = array();

		### 1. handle one-day leave application
		$sql = "UPDATE 
		            INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT t
		        INNER JOIN 
		            INTRANET_SCH_BUS_APPLY_LEAVE a ON a.ApplicationID=t.ApplicationID
		        SET
    		        t.RecordStatus = a.RecordStatus, 
		            t.AddedFrom = a.AddedFrom, 
		            t.AttendanceRecordID = a.AttendanceRecordID, 
		            t.AttendanceApplyLeaveID = a.AttendanceApplyLeaveID
                WHERE
                    a.StartDate = a.EndDate";
        $result['UpdateOneDayLeaveRecord'] = $li->db_db_query($sql);
//        debug_pr($sql);

        ### 2. handle multiple-day leave application
		$sql = "SELECT 
		            a.ApplicationID,
		            a.StudentID, 
		            a.RecordStatus, 
		            a.AddedFrom, 
		            a.AttendanceRecordID, 
		            a.AttendanceApplyLeaveID,
		            a.StartDate,
		            a.StartDateType,
		            a.EndDate,
		            a.EndDateType
		        FROM
		            INTRANET_SCH_BUS_APPLY_LEAVE a
		        WHERE
		            a.StartDate <> a.EndDate
		            ORDER BY a.ApplicationID";
		$applicationAry = $li->returnResultSet($sql);
        if (count($applicationAry)) {
            foreach((array)$applicationAry as $_application) {
                $_applicationID = $_application['ApplicationID'];
                $_studentID = $_application['StudentID'];
                $_recordStatus = $_application['RecordStatus'];
                $_addedFrom = $_application['AddedFrom'];
                $_attendanceRecordID = $_application['AttendanceRecordID'];
                $_attendanceApplyLeaveID = $_application['AttendanceApplyLeaveID'];
                $_startDate = $_application['StartDate'];
                $_startDateType = $_application['StartDateType'];
                $_endDate = $_application['EndDate'];
                $_endDateType = $_application['EndDateType'];
                $availableTimeSlotAry = $li->getAvailableTimeSlotByDateRange($_startDate, $_endDate, $_startDateType, $_endDateType);

                foreach((array)$availableTimeSlotAry as $__date => $__timeSlotAry) {
                    foreach((array)$__timeSlotAry as $__timeSlot=>$__val) {
                        $__timeSlotVal = $__timeSlot == 'AM' ? '1' : '2';
                        $year = substr($__date,0,4);
                        $month = substr($__date,5,2);
                        $day = (int)substr($__date,8,2);
                        $card_log_table_name = "CARD_STUDENT_DAILY_LOG_".$year."_".$month;
                        $sql = "SELECT 
                                    AMStatus,
                                    PMStatus
                                FROM 
                                    $card_log_table_name 
                                WHERE UserID='".$_studentID."'
                                    AND DayNumber = '$day'
                                    AND IsConfirmed=1";
                        $attendanceRecordAry = $li->returnResultSet($sql);
                        if (count($attendanceRecordAry)) {
                            $amStatus = $attendanceRecordAry[0]['AMStatus'];
                            $pmStatus = $attendanceRecordAry[0]['PMStatus'];
                            if ($_recordStatus == 5 && $amStatus != '' && ($amStatus == 0 || $amStatus == 2)) {        // punctual / late
                                $_recordStatus = 6;     // apply leave before, but at last present, need to set the application setatus to delete, assume take attendance on AM

//                                debug_pr("set to delete for application $_applicationID");
                            }
                        }

                        $sql = "UPDATE 
                                    INTRANET_SCH_BUS_APPLY_LEAVE_TIMESLOT 
                                SET
                                    RecordStatus = '".$_recordStatus."', 
                                    AddedFrom = '".$_addedFrom."'";
                        if ($_attendanceRecordID) {
                            $sql .= ", AttendanceRecordID = '".$_attendanceRecordID."'";
                        }
                        if ($_attendanceApplyLeaveID) {
                            $sql .= ", AttendanceApplyLeaveID = '".$_attendanceApplyLeaveID."'";
                        }
                        $sql .= " WHERE
                                    ApplicationID='".$_applicationID."'
                                    AND LeaveDate='".$__date."'
                                    AND TimeSlot='".$__timeSlotVal."'";
                        $result['UpdateMultipleDayLeaveRecord'] = $li->db_db_query($sql);
//                        debug_pr($sql);
                    }
                }
            }
        }
		
		if (!in_array(false,$result)) {
			echo "<span style=\"color:green; font-weight:bold\">Successfully patch eSchoolBus ApplyLeave Record</span><br>";
		}
		else {
			echo "<span style=\"color:red; font-weight:bold\">Fail to patch eSchoolBus ApplyLeave Record</span><br>";
		}
	}	// isApply
}	// plugin
		
		
if (!$FromCentralUpdate) {
	intranet_closedb();
}	

