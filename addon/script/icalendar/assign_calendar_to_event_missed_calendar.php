<?php
/*
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!! Please Run the Script Only after IPv2.5.6.5.1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/
// take away this in order to run the script.
die();
// using 
$PATH_WRT_ROOT = "../../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/icalendar.php");
#################################################################
##################### Script Config [START] #####################
#################################################################
$debugMode = true;
#################################################################
###################### Script Config [END] ######################
#################################################################
if (!$flag) {
	echo '<html><br><a href=?flag=1>Click here to proceed</a> <br><br><br></body></html>';
	die();
}
##################################################################################
###################### Script Plaease Write Below This Line ######################
##################################################################################
intranet_opendb();
$db = new libdb();
$iCal = new icalendar();
$successAry = array();
$db->Start_Trans();

$sql = "select count(*) from CALENDAR_EVENT_ENTRY  where CalID = 0 OR CalID IS NULL";
$NumHaveProblemDataArr = $db->returnVector($sql);
echo 'Num of problem record : '.$NumHaveProblemDataArr[0].'<br>';

$sql = "select distinct(UserID) from CALENDAR_EVENT_ENTRY  where CalID = 0 OR CalID IS NULL";
$UserIDHaveProblemDataArr = $db->returnVector($sql);

if(count($UserIDHaveProblemDataArr)>0){
	$sql = "select distinct(owner) from CALENDAR_CALENDAR where Owner IN ('".implode("','",$UserIDHaveProblemDataArr)."') and Caltype= 0";
	$UserIDHaveCalendarArr = $db->returnVector($sql);
	
	$UserDWithoutCalendarArr = array_diff($UserIDHaveProblemDataArr, $UserIDHaveCalendarArr);
	
	foreach($UserDWithoutCalendarArr as $_userID){
		$iCal->insertMyCalendar($_userID);
		$AddedCalendarUser[] = $_userID;
	}
	
	$sql = "select Owner,MIN(CalID) as CalID from CALENDAR_CALENDAR where Owner IN ('".implode("','",$UserIDHaveProblemDataArr)."') and Caltype= 0 Group by owner";
	$UserCalendarIDArr = $db->returnResultSet($sql);
	$UserCalendarIDAssoArr = BuildMultiKeyAssoc($UserCalendarIDArr,'Owner',array('CalID'),1);
	
	$checkNumberOfCalendar = (count($UserCalendarIDAssoArr) == count($UserIDHaveProblemDataArr));
	
	if($checkNumberOfCalendar){
		$sql = "select EventID, UserID from CALENDAR_EVENT_ENTRY  where CalID = 0 OR CalID IS NULL";
		$ProblemEventRecordArr = $db->returnResultSet($sql);
		$ProblemEventRecordAssoArr = BuildMultiKeyAssoc($ProblemEventRecordArr,'EventID',array('UserID'),1);
		
		$numRecordRanScript = 0;
		foreach($ProblemEventRecordAssoArr as $_recordId => $_userId){
			$_calId = $UserCalendarIDAssoArr[$_userId];
			$sql = "update CALENDAR_EVENT_ENTRY set CalID = '$_calId' where EventID = '$_recordId' and (CalID = 0 OR CalID IS NULL)";
			$successAry[] = $db->db_db_query($sql);
			
			$numRecordRanScript++;
		}
		
		$sql = "select distinct(UserID) from CALENDAR_EVENT_ENTRY  where CalID = 0 OR CalID IS NULL";
		$CheckHaveProblemDataArr = $db->returnVector($sql);
		if(count($CheckHaveProblemDataArr) > 0){
			$successAry[] = 0;
		}
	
	}
	else{
		echo 'Num Of Calendar do not match. Cannot Ran Script. Fail! <br>';
		$successAry[] = 0;
	}
	echo 'Num of Record Modified : '.$numRecordRanScript.'<br>';
	echo 'User added calendar : '.implode(', ',$AddedCalendarUser).'<br>';
}

if($debugMode || in_multi_array(false,$successAry)) {
	echo "Add missed calendar event to MyCalendar FAILED";
	$db->RollBack_Trans();
}
else {
	echo "Add missed calendar event to MyCalendar Success";
	$db->Commit_Trans();
}

intranet_closedb();
?>