<?php
/*
 * 	Purpose: set ip INTRANET_ELIB_BOOK.Publish=0 for eLib+ lost book in LIBMS_BOOK if NoOfCopy=0
 * 
 *  2016-09-14 [Cameron] create this file
 */

$PATH_WRT_ROOT = "../../"; 
$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";
include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");
include_once($path."includes/libelibrary.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

$liblms = new liblms();
$elib = new elibrary();

if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo("<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>");
}
else {
	$isApply = true;
}

$sql = "SELECT 	DISTINCT BookID
		FROM  	LIBMS_BOOK_UNIQUE
		WHERE 	RecordStatus='LOST'
		ORDER BY BookID";

$rs = $liblms->returnResultSet($sql);
$nrRec = count($rs);

if (!$FromCentralUpdate) {
	echo '<META http-equiv="Content-Type"  content="text/html" Charset="UTF-8"/>';
}
 
//debug_pr($sql);

$error = array();
$book_array = array();
if ($nrRec > 0) {
	echo "Affected BookID<br>";
	foreach((array)$rs as $k=>$v) {
		$bookID = $v['BookID'];
		if ($isApply) {
			$elib->SYNC_PHYSICAL_BOOK_TO_ELIB($bookID);
		}
		echo "{$bookID}<br>";	
	}
} 
//debug_r($book_array);


if($isApply){
	$result = in_array(false,$error) ? "Fail to sync Publish field!<br>" : "<span style=\"color:green; font-weight:bold\">Sync Publish field success!</span><br>";
	if ($FromCentralUpdate) {
		$x .= $result."\r\n";
	}
	else {
		echo $result;				
	}
}
if (!$FromCentralUpdate) {
	intranet_closedb();
}

?>