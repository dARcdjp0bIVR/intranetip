<?
# using:  

#######################################################################################################################
### This script is used to map the className and classNumber from PROFILE_ARCHIVE_CLASS_HISTORY to INTRANET_ARCHIVE_USER
### The className and classNumber was not save to INTRANET_ARCHIVE_USER
### Solution: Map the className and classNumber from PROFILE_ARCHIVE_CLASS_HISTORY to INTRANET_ARCHIVE_USER
#######################################################################################################################


$PATH_WRT_ROOT = "../../";
include_once($PATH_WRT_ROOT."includes/global.php");
include_once($PATH_WRT_ROOT."includes/libdb.php");
include_once($PATH_WRT_ROOT."includes/libfilesystem.php");
include('../check.php');

intranet_opendb();

$x = '';
if ($flag==1)
{
	$x .= "map_archive_class_name_number.php .......... Start at ". date("Y-m-d H:i:s")."<br><br>\r\n";	

	$li = new libdb();
	
	############################################################
	# Map Archive Class Name, Class Number[Start]
	############################################################
	$x .= "===================================================================<br>\r\n";
	$x .= "Map Archive Class Name, Class Number [Start]</b><br>\r\n";
	$x .= "===================================================================<br><br>\r\n";
	
		### Get Class Group Mapping by name
		$sql = "SELECT
			PASH.UserID,
			PASH.ClassName,
			PASH.ClassNumber
		FROM 
			PROFILE_ARCHIVE_CLASS_HISTORY PASH
		INNER JOIN
			(
				SELECT 
					UserID,
					MAX(RecordID) AS MAX_RecordID
				FROM 
					PROFILE_ARCHIVE_CLASS_HISTORY
				GROUP BY
					UserID
			) AS PASH2
		ON
			PASH.UserID = PASH2.UserID
		AND 
			PASH.RecordID = PASH2.MAX_RecordID
		INNER JOIN
			INTRANET_ARCHIVE_USER IAU
		ON
			PASH.UserID = IAU.UserID
		AND (
				IAU.ClassName IS NULL
			OR 
				IAU.ClassNumber IS NULL
			OR
				IAU.ClassName = ''
			OR
				IAU.ClassNumber = ''
		)";
		$ClassDataArr = $li->returnArray($sql);
		$numOfClass = count($ClassDataArr);

		$ResultArr = array();
		$li->Start_Trans();
		
		if ($numOfClass > 0)
		{
			for ($i=0; $i<$numOfClass; $i++)
			{
				list($UserID, $ClassName, $ClassNumber) = $ClassDataArr[$i];
				
				$sql = "UPDATE 
					INTRANET_ARCHIVE_USER
				SET
					ClassName = '{$ClassName}',
					ClassNumber = '{$ClassNumber}'
				WHERE
					UserID = '{$UserID}'";
				$ResultArr[$UserID] = $li->db_db_query($sql);
				
				$x .= $sql."<br>\r\n";
				
				if ($ResultArr[$UserID] == true)
					$x .= "Map User {$UserID} ( ClassName:{$ClassName}, ClassNumber: {$ClassNumber}) <font color='blue'><b>Success</b></font><br><br>\r\n";
				else
					$x .= "Map User {$UserID} ( ClassName:{$ClassName}, ClassNumber: {$ClassNumber}) <font color='red'><b>Failed</b></font><br><br>\r\n";
			}
		}
		else
		{
			$x .= "There is no user mapping<br><br>\r\n";
		}
		
		
		if (in_array(false, $ResultArr) == true)
		{
			$li->RollBack_Trans();
		}
		else
		{
			$li->Commit_Trans();
		}
	
	
	$x .= "===================================================================<br>\r\n";
	$x .= "Map Archive Class Name, Class Number [End]</b><br>\r\n";
	$x .= "===================================================================<br>\r\n";
	############################################################
	# Map Archive Class Name, Class Number [End]
	############################################################
	
	
	
	$x .= "<br>\r\n<br>\r\nEnd at ". date("Y-m-d H:i:s")."<br><br>";	
	echo $x;


}
else
{ ?>
	<html>
	<body>
	<font color=red size=+1>[Please Make DB Backup First]</font><br><br>
	
	This page will update the ClassName and ClassNumber of INTRANET_ARCHIVE_USER if the value is NULL<br />
	<br /><br />
		
	<a href=?flag=1>Click here to proceed.</a> <br><br>
	
	<br>
	</body></html>

	
	
<? }

intranet_closedb();
 ?>
	