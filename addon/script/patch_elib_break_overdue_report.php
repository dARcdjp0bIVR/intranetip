<?php
/*
 * Aim: patch data to break "overdue report" into "overdue report" and "penalty report" permission for eLib+   
 * 
 * 	2017-06-06 [Cameron] create this file		
 * 
 */
 

$path = ($FromCentralUpdate) ? $intranet_root."/" : "../../";	

include_once($path."includes/global.php");
include_once($path."includes/libdb.php");
include_once($path."includes/liblibrarymgmt.php");

if (!$FromCentralUpdate) {
	intranet_auth();
	intranet_opendb();
}

#########################################################################################################


## Use Library
$libdb = new liblms();


## Init 
$result = array();
############## Apply Patch ##############

#########################################
$insertValuesArr = array();
//$groupRightIDArr = array();

## Get Data
if(($_POST['Flag']!=1) && !$FromCentralUpdate) {
	$isApply = false;
	echo "<form method='post'><input value='run script' type='submit'><input type='hidden' name='Flag' value=1></form>";
}
else {
	$isApply = true;
}


$sql = "SELECT GroupRightID, GroupID, IsAllow, DateModified, LastModifiedBy 
		FROM LIBMS_GROUP_RIGHT 
		WHERE `Section`='reports' 
		AND `Function`='overdue report' 
		AND IsAllow=1 order by GroupRightID";
$groupRightArr = $libdb->returnResultSet($sql);
$nrRs = count($groupRightArr);
if($nrRs > 0){
	for ($i=0; $i<$nrRs; $i++){
		$rs = $groupRightArr[$i];
//		$groupRightIDArr[] = $rs['GroupRightID'];
		$insertValuesArr[] = "('".$rs['GroupID']."','reports','penalty report',1,now(),'".$_SESSION['UserID']."')";
	}
	
	## main
//	if (count($groupRightIDArr) > 0) {
//		$sql = "UPDATE LIBMS_GROUP_RIGHT SET `Function`='overdue report' WHERE GroupRightID IN ('".implode("','",$groupRightIDArr)."')";
//		echo $sql.'<br>';
//		if($isApply){
//			$result['overdue'] = $libdb->db_db_query($sql);
//		}
//	}
	
	if (count($insertValuesArr) > 0) {
		$sql = "INSERT INTO LIBMS_GROUP_RIGHT (GroupID, Section, Function, IsAllow, DateModified, LastModifiedBy) VALUES ";
		$sql .= implode(",",$insertValuesArr);
		echo $sql.'<br>';
		if($isApply){
			$result['penalty'] = $libdb->db_db_query($sql);
		}
	}
}


if ($FromCentralUpdate) {
//	if ($result['overdue']) {
//		$x .= "update overdue report right Success<br>\r\n";
//	}
	if ($result['penalty']) {
		$x .= "add penalty report Success<br>\r\n";
	}
}
else {
	echo '<style> 
	html, body, table, select, input, textarea{ font-size:12px; }
	</style>';
	if ($_POST['Flag']) {
		debug_r($result);
	}
	intranet_closedb();	
}

	
#########################################################################################################

?>