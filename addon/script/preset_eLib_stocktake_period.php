<?php
// Editing by 
/*
 * For preset the record start date and record end date from start date and end date in LIBMS_STOCKTAKE_SCHEME
 * 
 */

//$LIBMS_STOCKTAKE_PATH_WRT_ROOT = "../../../";
include_once($intranet_root."includes/global.php");
include_once($intranet_root."includes/libdb.php");
include_once($intranet_root."includes/liblibrarymgmt.php");

//intranet_auth();
intranet_opendb();

$liblms = new liblms();

$LIBMS_STOCKTAKE_SCHEME = $liblms->db.'.LIBMS_STOCKTAKE_SCHEME';
   
   $sql = 'UPDATE '.$LIBMS_STOCKTAKE_SCHEME.' 
			SET RecordStartDate = StartDate, RecordEndDate = EndDate 
			WHERE RecordStartDate IS NULL AND RecordEndDate IS NULL';
		
$eLibPlusStocktakeSuccess = $liblms->db_db_query($sql);

echo "[eLibPlus] Preset the stock-take period in the stock-take settings ";
if($eLibPlusStocktakeSuccess){
	echo "success.";
}else{
	echo "failed.";
}
echo "<br />";

?>